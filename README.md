_**<font color=red> Output 3 在 reconstructions_mem_case 分支上, 该仓库有两个分支 </font>**_
## 1 Project Information

- **Project Name**：OneFlow Pluggable Device Support
- **Scheme Description**：The current OneFlow multi-device support framework incorporates static graphs, and new devices are managed using a registration system for them, requiring the multi-device support framework to incorporate OneFlow's distributed and static graph modules. The current OneFlow main branch only supports CPUs and GPUs. In order to support more chip architectures, the Python layer needs to be removed to add support for new devices by specifying the device type.
- **Time Planning**：Based on familiarity with the OneFlow architecture, learning about deep learning and familiarity with C++ programming specifications. Designing algorithms to align Pytorch, and finally modifying and refining the use of MemoryCase.

## 2 Project Outputs

- [x] Run through arithmetic on AMD by configuring `WITH_ROCM`. All the OneFlow operators that needed to be tested on AMD passed.
- [x] Develop roll op on OneFlow UserOp. Based on the roll op which aligns the pytorch, a backward operation is added to the roll op, it could realize the roll tensor in one and two dimensions.
- [x] Reconstruct registry for Memory Case. The `SetAttr<T>`, `HasAttr<T>`, `Attr<T>` interfaces of Memory Case are provided externally by modifying the original MemoryCase proto struct to a Map protocol structure.

## 3 Relative Programs
#### 

1. **AMD's ops all added ROCM support and tested**

2. **The files involved in the implementation of Roll op are available on Gitlab**

- `oneflow\oneflow\core\functional\impl\array_functor.cpp`

- `oneflow\python\oneflow\test\modules\test_roll\py`

- `oneflow\oneflow\core\autograd\gradient_funcs\roll.cpp`

- `oneflow\oneflow\user\kernels\roll_kernel.cu`

- `oneflow\oneflow\user\kernels\roll_kernel.h`

- `oneflow\oneflow\user\kernels\roll_kernel.cpp`

- `oneflow\oneflow\user\ops\roll_ops.cpp`

- `oneflow\oneflow\core\functional\functional_api.yaml`

3. **The files involved in the reconstructions of MemoryCase** 

**dirs** (included files too many to show)

- ` ../oneflow/core/common`
- ` ../oneflow/core/device`
- ` ../oneflow/core/eager`
- ` ../oneflow/core/framework`
- ` ../oneflow/core/graph`
- ` ../oneflow/core/job`
- ` ../oneflow/core/kernel`
- ` ../oneflow/core/memory`
- ` ../oneflow/core/register`

- ` ../oneflow/user/kernels`
- ` ../oneflow/user/ops `

**individual files**

- ` ../oneflow/core/framework/attr_value_mem_case.cpp `
- ` ../oneflow/core/framework/attr_value_mem_case.h`
- ` ../oneflow/core/memory/memory_case_attr_util.cpp`
- ` ../oneflow/core/memory/memory_case_attr_util.h`

