/*
Copyright 2020 The OneFlow Authors. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Generated from oneflow/core/functional/functional_api.yaml. DO NOT EDIT!

#include <vector>
#include <pybind11/pybind11.h>

#include "oneflow/api/python/of_api_registry.h"
#include "oneflow/api/python/functional/function_def.h"
#include "oneflow/api/python/functional/py_function.h"
#include "oneflow/core/common/maybe.h"
#include "oneflow/core/common/optional.h"
#include "oneflow/core/functional/functional.h"

namespace oneflow {
namespace one {
namespace functional {

struct AddNSchema {
  using FType = decltype(functional::AddN);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AddN;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor AddN(TensorTuple inputs, *, Bool inplace=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AddNSchema::max_args;
constexpr size_t AddNSchema::max_positionals;
constexpr size_t AddNSchema::max_keywords;
constexpr char const* AddNSchema::signature;
ReturnDef AddNSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AddNSchema::argument_def = {ArgumentDef("inputs", ValueTypeOf<TensorTuple>()), ArgumentDef("inplace", bool(false))};

struct BroadcastEqualSchema {
  using FType = decltype(functional::BroadcastEqual);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastEqual;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastEqual(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastEqualSchema::max_args;
constexpr size_t BroadcastEqualSchema::max_positionals;
constexpr size_t BroadcastEqualSchema::max_keywords;
constexpr char const* BroadcastEqualSchema::signature;
ReturnDef BroadcastEqualSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastEqualSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastNotEqualSchema {
  using FType = decltype(functional::BroadcastNotEqual);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastNotEqual;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastNotEqual(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastNotEqualSchema::max_args;
constexpr size_t BroadcastNotEqualSchema::max_positionals;
constexpr size_t BroadcastNotEqualSchema::max_keywords;
constexpr char const* BroadcastNotEqualSchema::signature;
ReturnDef BroadcastNotEqualSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastNotEqualSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastGreaterSchema {
  using FType = decltype(functional::BroadcastGreater);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastGreater;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastGreater(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastGreaterSchema::max_args;
constexpr size_t BroadcastGreaterSchema::max_positionals;
constexpr size_t BroadcastGreaterSchema::max_keywords;
constexpr char const* BroadcastGreaterSchema::signature;
ReturnDef BroadcastGreaterSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastGreaterSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastGreaterEqualSchema {
  using FType = decltype(functional::BroadcastGreaterEqual);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastGreaterEqual;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastGreaterEqual(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastGreaterEqualSchema::max_args;
constexpr size_t BroadcastGreaterEqualSchema::max_positionals;
constexpr size_t BroadcastGreaterEqualSchema::max_keywords;
constexpr char const* BroadcastGreaterEqualSchema::signature;
ReturnDef BroadcastGreaterEqualSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastGreaterEqualSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastLogicalAndSchema {
  using FType = decltype(functional::BroadcastLogicalAnd);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastLogicalAnd;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastLogicalAnd(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastLogicalAndSchema::max_args;
constexpr size_t BroadcastLogicalAndSchema::max_positionals;
constexpr size_t BroadcastLogicalAndSchema::max_keywords;
constexpr char const* BroadcastLogicalAndSchema::signature;
ReturnDef BroadcastLogicalAndSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastLogicalAndSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastLogicalOrSchema {
  using FType = decltype(functional::BroadcastLogicalOr);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastLogicalOr;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastLogicalOr(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastLogicalOrSchema::max_args;
constexpr size_t BroadcastLogicalOrSchema::max_positionals;
constexpr size_t BroadcastLogicalOrSchema::max_keywords;
constexpr char const* BroadcastLogicalOrSchema::signature;
ReturnDef BroadcastLogicalOrSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastLogicalOrSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastLessSchema {
  using FType = decltype(functional::BroadcastLess);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastLess;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastLess(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastLessSchema::max_args;
constexpr size_t BroadcastLessSchema::max_positionals;
constexpr size_t BroadcastLessSchema::max_keywords;
constexpr char const* BroadcastLessSchema::signature;
ReturnDef BroadcastLessSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastLessSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastLessEqualSchema {
  using FType = decltype(functional::BroadcastLessEqual);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastLessEqual;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastLessEqual(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastLessEqualSchema::max_args;
constexpr size_t BroadcastLessEqualSchema::max_positionals;
constexpr size_t BroadcastLessEqualSchema::max_keywords;
constexpr char const* BroadcastLessEqualSchema::signature;
ReturnDef BroadcastLessEqualSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastLessEqualSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReduceSumSchema {
  using FType = decltype(functional::ReduceSum);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ReduceSum;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor ReduceSum(Tensor x, *, Int32List axis, Bool keepdims=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReduceSumSchema::max_args;
constexpr size_t ReduceSumSchema::max_positionals;
constexpr size_t ReduceSumSchema::max_keywords;
constexpr char const* ReduceSumSchema::signature;
ReturnDef ReduceSumSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReduceSumSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("keepdims", bool(false))};

struct RollSchema {
  using FType = decltype(functional::Roll);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Roll;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor Roll(Tensor x, *, Int32List shifts, Int32List dims)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RollSchema::max_args;
constexpr size_t RollSchema::max_positionals;
constexpr size_t RollSchema::max_keywords;
constexpr char const* RollSchema::signature;
ReturnDef RollSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RollSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("shifts", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dims", ValueTypeOf<std::vector<int32_t>>())};

struct ReduceMeanSchema {
  using FType = decltype(functional::ReduceMean);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ReduceMean;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor ReduceMean(Tensor x, *, Int32List axis, Bool keepdims=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReduceMeanSchema::max_args;
constexpr size_t ReduceMeanSchema::max_positionals;
constexpr size_t ReduceMeanSchema::max_keywords;
constexpr char const* ReduceMeanSchema::signature;
ReturnDef ReduceMeanSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReduceMeanSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("keepdims", bool(false))};

struct TransposeSchema {
  using FType = decltype(functional::Transpose);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Transpose;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Transpose(Tensor x, *, Int32List perm)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TransposeSchema::max_args;
constexpr size_t TransposeSchema::max_positionals;
constexpr size_t TransposeSchema::max_keywords;
constexpr char const* TransposeSchema::signature;
ReturnDef TransposeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> TransposeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("perm", ValueTypeOf<std::vector<int32_t>>())};

struct ReciprocalSchema {
  using FType = decltype(functional::Reciprocal);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Reciprocal;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Reciprocal(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReciprocalSchema::max_args;
constexpr size_t ReciprocalSchema::max_positionals;
constexpr size_t ReciprocalSchema::max_keywords;
constexpr char const* ReciprocalSchema::signature;
ReturnDef ReciprocalSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReciprocalSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReciprocalNoNanSchema {
  using FType = decltype(functional::ReciprocalNoNan);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ReciprocalNoNan;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ReciprocalNoNan(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReciprocalNoNanSchema::max_args;
constexpr size_t ReciprocalNoNanSchema::max_positionals;
constexpr size_t ReciprocalNoNanSchema::max_keywords;
constexpr char const* ReciprocalNoNanSchema::signature;
ReturnDef ReciprocalNoNanSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReciprocalNoNanSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ImageFlipSchema {
  using FType = decltype(functional::ImageFlip);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ImageFlip;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor ImageFlip(Tensor x, *, Int32 flip_code)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ImageFlipSchema::max_args;
constexpr size_t ImageFlipSchema::max_positionals;
constexpr size_t ImageFlipSchema::max_keywords;
constexpr char const* ImageFlipSchema::signature;
ReturnDef ImageFlipSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ImageFlipSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("flip_code", ValueTypeOf<int32_t>())};

struct SinSchema {
  using FType = decltype(functional::Sin);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Sin;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Sin(Tensor x, *, Bool inplace=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SinSchema::max_args;
constexpr size_t SinSchema::max_positionals;
constexpr size_t SinSchema::max_keywords;
constexpr char const* SinSchema::signature;
ReturnDef SinSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SinSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("inplace", bool(false))};

struct CosSchema {
  using FType = decltype(functional::Cos);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Cos;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Cos(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t CosSchema::max_args;
constexpr size_t CosSchema::max_positionals;
constexpr size_t CosSchema::max_keywords;
constexpr char const* CosSchema::signature;
ReturnDef CosSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> CosSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct CoshSchema {
  using FType = decltype(functional::Cosh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Cosh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Cosh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t CoshSchema::max_args;
constexpr size_t CoshSchema::max_positionals;
constexpr size_t CoshSchema::max_keywords;
constexpr char const* CoshSchema::signature;
ReturnDef CoshSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> CoshSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BroadcastFModSchema {
  using FType = decltype(functional::BroadcastFMod);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastFMod;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor BroadcastFMod(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastFModSchema::max_args;
constexpr size_t BroadcastFModSchema::max_positionals;
constexpr size_t BroadcastFModSchema::max_keywords;
constexpr char const* BroadcastFModSchema::signature;
ReturnDef BroadcastFModSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastFModSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct LogSchema {
  using FType = decltype(functional::Log);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Log;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Log(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LogSchema::max_args;
constexpr size_t LogSchema::max_positionals;
constexpr size_t LogSchema::max_keywords;
constexpr char const* LogSchema::signature;
ReturnDef LogSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LogSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SqrtSchema {
  using FType = decltype(functional::Sqrt);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Sqrt;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Sqrt(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SqrtSchema::max_args;
constexpr size_t SqrtSchema::max_positionals;
constexpr size_t SqrtSchema::max_keywords;
constexpr char const* SqrtSchema::signature;
ReturnDef SqrtSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SqrtSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct RsqrtSchema {
  using FType = decltype(functional::Rsqrt);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Rsqrt;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Rsqrt(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RsqrtSchema::max_args;
constexpr size_t RsqrtSchema::max_positionals;
constexpr size_t RsqrtSchema::max_keywords;
constexpr char const* RsqrtSchema::signature;
ReturnDef RsqrtSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RsqrtSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SquareSchema {
  using FType = decltype(functional::Square);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Square;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Square(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SquareSchema::max_args;
constexpr size_t SquareSchema::max_positionals;
constexpr size_t SquareSchema::max_keywords;
constexpr char const* SquareSchema::signature;
ReturnDef SquareSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SquareSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReluSchema {
  using FType = decltype(functional::Relu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Relu;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Relu(Tensor x, *, Bool inplace=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReluSchema::max_args;
constexpr size_t ReluSchema::max_positionals;
constexpr size_t ReluSchema::max_keywords;
constexpr char const* ReluSchema::signature;
ReturnDef ReluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("inplace", bool(false))};

struct HardTanhSchema {
  using FType = decltype(functional::HardTanh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::HardTanh;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor HardTanh(Tensor x, *, Double min_val, Double max_val)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t HardTanhSchema::max_args;
constexpr size_t HardTanhSchema::max_positionals;
constexpr size_t HardTanhSchema::max_keywords;
constexpr char const* HardTanhSchema::signature;
ReturnDef HardTanhSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> HardTanhSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("min_val", ValueTypeOf<double>()), ArgumentDef("max_val", ValueTypeOf<double>())};

struct TanSchema {
  using FType = decltype(functional::Tan);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Tan;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Tan(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TanSchema::max_args;
constexpr size_t TanSchema::max_positionals;
constexpr size_t TanSchema::max_keywords;
constexpr char const* TanSchema::signature;
ReturnDef TanSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> TanSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct TanhSchema {
  using FType = decltype(functional::Tanh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Tanh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Tanh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TanhSchema::max_args;
constexpr size_t TanhSchema::max_positionals;
constexpr size_t TanhSchema::max_keywords;
constexpr char const* TanhSchema::signature;
ReturnDef TanhSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> TanhSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct EluSchema {
  using FType = decltype(functional::Elu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Elu;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Elu(Tensor x, *, Double alpha)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t EluSchema::max_args;
constexpr size_t EluSchema::max_positionals;
constexpr size_t EluSchema::max_keywords;
constexpr char const* EluSchema::signature;
ReturnDef EluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> EluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("alpha", ValueTypeOf<double>())};

struct GeluSchema {
  using FType = decltype(functional::Gelu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Gelu;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Gelu(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t GeluSchema::max_args;
constexpr size_t GeluSchema::max_positionals;
constexpr size_t GeluSchema::max_keywords;
constexpr char const* GeluSchema::signature;
ReturnDef GeluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> GeluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SigmoidSchema {
  using FType = decltype(functional::Sigmoid);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Sigmoid;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Sigmoid(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SigmoidSchema::max_args;
constexpr size_t SigmoidSchema::max_positionals;
constexpr size_t SigmoidSchema::max_keywords;
constexpr char const* SigmoidSchema::signature;
ReturnDef SigmoidSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SigmoidSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct HardSigmoidSchema {
  using FType = decltype(functional::HardSigmoid);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::HardSigmoid;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor HardSigmoid(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t HardSigmoidSchema::max_args;
constexpr size_t HardSigmoidSchema::max_positionals;
constexpr size_t HardSigmoidSchema::max_keywords;
constexpr char const* HardSigmoidSchema::signature;
ReturnDef HardSigmoidSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> HardSigmoidSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SoftmaxSchema {
  using FType = decltype(functional::Softmax);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Softmax;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Softmax(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SoftmaxSchema::max_args;
constexpr size_t SoftmaxSchema::max_positionals;
constexpr size_t SoftmaxSchema::max_keywords;
constexpr char const* SoftmaxSchema::signature;
ReturnDef SoftmaxSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SoftmaxSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct HardSwishSchema {
  using FType = decltype(functional::HardSwish);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::HardSwish;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor HardSwish(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t HardSwishSchema::max_args;
constexpr size_t HardSwishSchema::max_positionals;
constexpr size_t HardSwishSchema::max_keywords;
constexpr char const* HardSwishSchema::signature;
ReturnDef HardSwishSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> HardSwishSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct LeakyReluSchema {
  using FType = decltype(functional::LeakyRelu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::LeakyRelu;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor LeakyRelu(Tensor x, *, Float alpha)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LeakyReluSchema::max_args;
constexpr size_t LeakyReluSchema::max_positionals;
constexpr size_t LeakyReluSchema::max_keywords;
constexpr char const* LeakyReluSchema::signature;
ReturnDef LeakyReluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LeakyReluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("alpha", ValueTypeOf<float>())};

struct NormalizationSchema {
  using FType = decltype(functional::Normalization);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Normalization;
  static constexpr size_t max_args = 9;
  static constexpr size_t max_positionals = 9;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor Normalization(Tensor x, Tensor moving_mean=None, Tensor moving_variance=None, Tensor gamma, Tensor beta, *, Int32 axis=1, Float epsilon=1e-5, Float momentum=0.9, Bool is_training=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t NormalizationSchema::max_args;
constexpr size_t NormalizationSchema::max_positionals;
constexpr size_t NormalizationSchema::max_keywords;
constexpr char const* NormalizationSchema::signature;
ReturnDef NormalizationSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> NormalizationSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("moving_mean", Optional<one::Tensor>()), ArgumentDef("moving_variance", Optional<one::Tensor>()), ArgumentDef("gamma", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("beta", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", int32_t(1)), ArgumentDef("epsilon", float(1e-5)), ArgumentDef("momentum", float(0.9)), ArgumentDef("is_training", bool(false))};

struct RangeSchema {
  using FType = decltype(functional::Range);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Range;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor Range(*, Int64 start, Int64 limit, Int64 delta, DataType dtype=kInt64)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RangeSchema::max_args;
constexpr size_t RangeSchema::max_positionals;
constexpr size_t RangeSchema::max_keywords;
constexpr char const* RangeSchema::signature;
ReturnDef RangeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RangeSchema::argument_def = {ArgumentDef("start", ValueTypeOf<int64_t>()), ArgumentDef("limit", ValueTypeOf<int64_t>()), ArgumentDef("delta", ValueTypeOf<int64_t>()), ArgumentDef("dtype", DataType(kInt64))};

struct FlattenSchema {
  using FType = decltype(functional::Flatten);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Flatten;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor Flatten(Tensor x, *, Int32 start_dim=0, Int32 end_dim=-1)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t FlattenSchema::max_args;
constexpr size_t FlattenSchema::max_positionals;
constexpr size_t FlattenSchema::max_keywords;
constexpr char const* FlattenSchema::signature;
ReturnDef FlattenSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> FlattenSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("start_dim", int32_t(0)), ArgumentDef("end_dim", int32_t(-1))};

struct ArgMaxSchema {
  using FType = decltype(functional::ArgMax);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ArgMax;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ArgMax(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ArgMaxSchema::max_args;
constexpr size_t ArgMaxSchema::max_positionals;
constexpr size_t ArgMaxSchema::max_keywords;
constexpr char const* ArgMaxSchema::signature;
ReturnDef ArgMaxSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ArgMaxSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ArgWhereSchema {
  using FType = decltype(functional::ArgWhere);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::ArgWhere;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "TensorTuple ArgWhere(Tensor x, *, DataType dtype=kInt32)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ArgWhereSchema::max_args;
constexpr size_t ArgWhereSchema::max_positionals;
constexpr size_t ArgWhereSchema::max_keywords;
constexpr char const* ArgWhereSchema::signature;
ReturnDef ArgWhereSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> ArgWhereSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dtype", DataType(kInt32))};

struct BroadcastLikeSchema {
  using FType = decltype(functional::BroadcastLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BroadcastLike;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor BroadcastLike(Tensor x, Tensor like, *, Int32List broadcast_axes)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BroadcastLikeSchema::max_args;
constexpr size_t BroadcastLikeSchema::max_positionals;
constexpr size_t BroadcastLikeSchema::max_keywords;
constexpr char const* BroadcastLikeSchema::signature;
ReturnDef BroadcastLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BroadcastLikeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("like", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("broadcast_axes", ValueTypeOf<std::vector<int32_t>>())};

struct CastSchema {
  using FType = decltype(functional::Cast);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Cast;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Cast(Tensor x, *, DataType dtype)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t CastSchema::max_args;
constexpr size_t CastSchema::max_positionals;
constexpr size_t CastSchema::max_keywords;
constexpr char const* CastSchema::signature;
ReturnDef CastSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> CastSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dtype", ValueTypeOf<DataType>())};

struct ConstantSchema {
  using FType = decltype(functional::Constant);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Constant;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor Constant(*, Shape shape, Scalar value, DataType dtype, Device device=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConstantSchema::max_args;
constexpr size_t ConstantSchema::max_positionals;
constexpr size_t ConstantSchema::max_keywords;
constexpr char const* ConstantSchema::signature;
ReturnDef ConstantSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConstantSchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("value", ValueTypeOf<Scalar>()), ArgumentDef("dtype", ValueTypeOf<DataType>()), ArgumentDef("device", Optional<Symbol<Device>>())};

struct ConsistentConstantSchema {
  using FType = decltype(functional::ConsistentConstant);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ConsistentConstant;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor ConsistentConstant(*, Shape shape, Scalar value, DataType dtype, Placement placement, SbpList sbp_tuple)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConsistentConstantSchema::max_args;
constexpr size_t ConsistentConstantSchema::max_positionals;
constexpr size_t ConsistentConstantSchema::max_keywords;
constexpr char const* ConsistentConstantSchema::signature;
ReturnDef ConsistentConstantSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConsistentConstantSchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("value", ValueTypeOf<Scalar>()), ArgumentDef("dtype", ValueTypeOf<DataType>()), ArgumentDef("placement", ValueTypeOf<Symbol<ParallelDesc>>()), ArgumentDef("sbp_tuple", ValueTypeOf<std::vector<Symbol<cfg::SbpParallel>>>())};

struct EmptySchema {
  using FType = decltype(functional::Empty);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Empty;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor Empty(*, Shape shape, DataType dtype, Device device=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t EmptySchema::max_args;
constexpr size_t EmptySchema::max_positionals;
constexpr size_t EmptySchema::max_keywords;
constexpr char const* EmptySchema::signature;
ReturnDef EmptySchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> EmptySchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("dtype", ValueTypeOf<DataType>()), ArgumentDef("device", Optional<Symbol<Device>>())};

struct ConsistentEmptySchema {
  using FType = decltype(functional::ConsistentEmpty);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ConsistentEmpty;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor ConsistentEmpty(*, Shape shape, DataType dtype, Placement placement, SbpList sbp_tuple)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConsistentEmptySchema::max_args;
constexpr size_t ConsistentEmptySchema::max_positionals;
constexpr size_t ConsistentEmptySchema::max_keywords;
constexpr char const* ConsistentEmptySchema::signature;
ReturnDef ConsistentEmptySchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConsistentEmptySchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("dtype", ValueTypeOf<DataType>()), ArgumentDef("placement", ValueTypeOf<Symbol<ParallelDesc>>()), ArgumentDef("sbp_tuple", ValueTypeOf<std::vector<Symbol<cfg::SbpParallel>>>())};

struct ZerosLikeSchema {
  using FType = decltype(functional::ZerosLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ZerosLike;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ZerosLike(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ZerosLikeSchema::max_args;
constexpr size_t ZerosLikeSchema::max_positionals;
constexpr size_t ZerosLikeSchema::max_keywords;
constexpr char const* ZerosLikeSchema::signature;
ReturnDef ZerosLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ZerosLikeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct OnesLikeSchema {
  using FType = decltype(functional::OnesLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::OnesLike;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor OnesLike(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t OnesLikeSchema::max_args;
constexpr size_t OnesLikeSchema::max_positionals;
constexpr size_t OnesLikeSchema::max_keywords;
constexpr char const* OnesLikeSchema::signature;
ReturnDef OnesLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> OnesLikeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct BernoulliSchema {
  using FType = decltype(functional::Bernoulli);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Bernoulli;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Bernoulli(Tensor x, DataType dtype=kFloat, Generator generator=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BernoulliSchema::max_args;
constexpr size_t BernoulliSchema::max_positionals;
constexpr size_t BernoulliSchema::max_keywords;
constexpr char const* BernoulliSchema::signature;
ReturnDef BernoulliSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BernoulliSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dtype", DataType(kFloat)), ArgumentDef("generator", Optional<one::Generator>())};

struct ConcatSchema {
  using FType = decltype(functional::Concat);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Concat;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor Concat(TensorTuple inputs, *, Int64 axis, Int64 max_dim_size)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConcatSchema::max_args;
constexpr size_t ConcatSchema::max_positionals;
constexpr size_t ConcatSchema::max_keywords;
constexpr char const* ConcatSchema::signature;
ReturnDef ConcatSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConcatSchema::argument_def = {ArgumentDef("inputs", ValueTypeOf<TensorTuple>()), ArgumentDef("axis", ValueTypeOf<int64_t>()), ArgumentDef("max_dim_size", ValueTypeOf<int64_t>())};

struct BiasAddSchema {
  using FType = decltype(functional::BiasAdd);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BiasAdd;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor BiasAdd(Tensor x, Tensor bias, *, Int32 axis=1)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BiasAddSchema::max_args;
constexpr size_t BiasAddSchema::max_positionals;
constexpr size_t BiasAddSchema::max_keywords;
constexpr char const* BiasAddSchema::signature;
ReturnDef BiasAddSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BiasAddSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("bias", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", int32_t(1))};

struct Conv1dSchema {
  using FType = decltype(functional::Conv1d);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Conv1d;
  static constexpr size_t max_args = 7;
  static constexpr size_t max_positionals = 7;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor Conv1d(Tensor x, Tensor weight, *, Tensor bias=None, Int32List stride, Int32List padding, Int32List dilation, Int32 groups=1)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Conv1dSchema::max_args;
constexpr size_t Conv1dSchema::max_positionals;
constexpr size_t Conv1dSchema::max_keywords;
constexpr char const* Conv1dSchema::signature;
ReturnDef Conv1dSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Conv1dSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("weight", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("bias", Optional<one::Tensor>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("groups", int32_t(1))};

struct Conv2dSchema {
  using FType = decltype(functional::Conv2d);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Conv2d;
  static constexpr size_t max_args = 7;
  static constexpr size_t max_positionals = 7;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor Conv2d(Tensor x, Tensor weight, *, Tensor bias=None, Int32List stride, Int32List padding, Int32List dilation, Int32 groups=1)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Conv2dSchema::max_args;
constexpr size_t Conv2dSchema::max_positionals;
constexpr size_t Conv2dSchema::max_keywords;
constexpr char const* Conv2dSchema::signature;
ReturnDef Conv2dSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Conv2dSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("weight", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("bias", Optional<one::Tensor>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("groups", int32_t(1))};

struct FakeQuantizationSchema {
  using FType = decltype(functional::FakeQuantization);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::FakeQuantization;
  static constexpr size_t max_args = 6;
  static constexpr size_t max_positionals = 6;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor FakeQuantization(Tensor in, Tensor scale, Tensor zero_point, *, String quantization_formula, Int32 quantization_bit, String quantization_scheme)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t FakeQuantizationSchema::max_args;
constexpr size_t FakeQuantizationSchema::max_positionals;
constexpr size_t FakeQuantizationSchema::max_keywords;
constexpr char const* FakeQuantizationSchema::signature;
ReturnDef FakeQuantizationSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> FakeQuantizationSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("scale", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("zero_point", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("quantization_formula", ValueTypeOf<std::string>()), ArgumentDef("quantization_bit", ValueTypeOf<int32_t>()), ArgumentDef("quantization_scheme", ValueTypeOf<std::string>())};

struct QuantizationSchema {
  using FType = decltype(functional::Quantization);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Quantization;
  static constexpr size_t max_args = 6;
  static constexpr size_t max_positionals = 6;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor Quantization(Tensor in, Tensor scale, Tensor zero_point, *, String quantization_formula, Int32 quantization_bit, String quantization_scheme)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t QuantizationSchema::max_args;
constexpr size_t QuantizationSchema::max_positionals;
constexpr size_t QuantizationSchema::max_keywords;
constexpr char const* QuantizationSchema::signature;
ReturnDef QuantizationSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> QuantizationSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("scale", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("zero_point", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("quantization_formula", ValueTypeOf<std::string>()), ArgumentDef("quantization_bit", ValueTypeOf<int32_t>()), ArgumentDef("quantization_scheme", ValueTypeOf<std::string>())};

struct MinMaxObserverSchema {
  using FType = decltype(functional::MinMaxObserver);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::MinMaxObserver;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "TensorTuple MinMaxObserver(Tensor in, *, String quantization_formula, Int32 quantization_bit, String quantization_scheme, Bool per_layer_quantization)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MinMaxObserverSchema::max_args;
constexpr size_t MinMaxObserverSchema::max_positionals;
constexpr size_t MinMaxObserverSchema::max_keywords;
constexpr char const* MinMaxObserverSchema::signature;
ReturnDef MinMaxObserverSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> MinMaxObserverSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("quantization_formula", ValueTypeOf<std::string>()), ArgumentDef("quantization_bit", ValueTypeOf<int32_t>()), ArgumentDef("quantization_scheme", ValueTypeOf<std::string>()), ArgumentDef("per_layer_quantization", ValueTypeOf<bool>())};

struct MovingAverageMinMaxObserverSchema {
  using FType = decltype(functional::MovingAverageMinMaxObserver);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::MovingAverageMinMaxObserver;
  static constexpr size_t max_args = 10;
  static constexpr size_t max_positionals = 10;
  static constexpr size_t max_keywords = 6;
  static constexpr char const* signature = "TensorTuple MovingAverageMinMaxObserver(Tensor in, Tensor current_train_step, Tensor moving_max, Tensor moving_min, *, Bool training, String quantization_formula, Int64 stop_update_after_iters, Int32 quantization_bit, String quantization_scheme, Float momentum)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MovingAverageMinMaxObserverSchema::max_args;
constexpr size_t MovingAverageMinMaxObserverSchema::max_positionals;
constexpr size_t MovingAverageMinMaxObserverSchema::max_keywords;
constexpr char const* MovingAverageMinMaxObserverSchema::signature;
ReturnDef MovingAverageMinMaxObserverSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> MovingAverageMinMaxObserverSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("current_train_step", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("moving_max", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("moving_min", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("training", ValueTypeOf<bool>()), ArgumentDef("quantization_formula", ValueTypeOf<std::string>()), ArgumentDef("stop_update_after_iters", ValueTypeOf<int64_t>()), ArgumentDef("quantization_bit", ValueTypeOf<int32_t>()), ArgumentDef("quantization_scheme", ValueTypeOf<std::string>()), ArgumentDef("momentum", ValueTypeOf<float>())};

struct Conv3dSchema {
  using FType = decltype(functional::Conv3d);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Conv3d;
  static constexpr size_t max_args = 7;
  static constexpr size_t max_positionals = 7;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor Conv3d(Tensor x, Tensor weight, *, Tensor bias=None, Int32List stride, Int32List padding, Int32List dilation, Int32 groups=1)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Conv3dSchema::max_args;
constexpr size_t Conv3dSchema::max_positionals;
constexpr size_t Conv3dSchema::max_keywords;
constexpr char const* Conv3dSchema::signature;
ReturnDef Conv3dSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Conv3dSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("weight", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("bias", Optional<one::Tensor>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("groups", int32_t(1))};

struct ExpandSchema {
  using FType = decltype(functional::Expand);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Expand;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Expand(Tensor x, *, Shape shape)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ExpandSchema::max_args;
constexpr size_t ExpandSchema::max_positionals;
constexpr size_t ExpandSchema::max_keywords;
constexpr char const* ExpandSchema::signature;
ReturnDef ExpandSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ExpandSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("shape", ValueTypeOf<Shape>())};

struct ExpandDimsSchema {
  using FType = decltype(functional::ExpandDims);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ExpandDims;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor ExpandDims(Tensor x, *, Int32 axis)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ExpandDimsSchema::max_args;
constexpr size_t ExpandDimsSchema::max_positionals;
constexpr size_t ExpandDimsSchema::max_keywords;
constexpr char const* ExpandDimsSchema::signature;
ReturnDef ExpandDimsSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ExpandDimsSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", ValueTypeOf<int32_t>())};

struct ExpSchema {
  using FType = decltype(functional::Exp);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Exp;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Exp(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ExpSchema::max_args;
constexpr size_t ExpSchema::max_positionals;
constexpr size_t ExpSchema::max_keywords;
constexpr char const* ExpSchema::signature;
ReturnDef ExpSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ExpSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct GatherSchema {
  using FType = decltype(functional::Gather);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Gather;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Gather(Tensor x, Tensor indices, *, Int64 axis)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t GatherSchema::max_args;
constexpr size_t GatherSchema::max_positionals;
constexpr size_t GatherSchema::max_keywords;
constexpr char const* GatherSchema::signature;
ReturnDef GatherSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> GatherSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("indices", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", ValueTypeOf<int64_t>())};

struct DimGatherSchema {
  using FType = decltype(functional::DimGather);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::DimGather;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor DimGather(Tensor x, Tensor indices, *, Int32 dim)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t DimGatherSchema::max_args;
constexpr size_t DimGatherSchema::max_positionals;
constexpr size_t DimGatherSchema::max_keywords;
constexpr char const* DimGatherSchema::signature;
ReturnDef DimGatherSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> DimGatherSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("indices", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dim", ValueTypeOf<int32_t>())};

struct GatherNdSchema {
  using FType = decltype(functional::GatherNd);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::GatherNd;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor GatherNd(Tensor params, Tensor indices)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t GatherNdSchema::max_args;
constexpr size_t GatherNdSchema::max_positionals;
constexpr size_t GatherNdSchema::max_keywords;
constexpr char const* GatherNdSchema::signature;
ReturnDef GatherNdSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> GatherNdSchema::argument_def = {ArgumentDef("params", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("indices", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ScatterNdSchema {
  using FType = decltype(functional::ScatterNd);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ScatterNd;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ScatterNd(Tensor indices, Tensor updates, Shape shape)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ScatterNdSchema::max_args;
constexpr size_t ScatterNdSchema::max_positionals;
constexpr size_t ScatterNdSchema::max_keywords;
constexpr char const* ScatterNdSchema::signature;
ReturnDef ScatterNdSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ScatterNdSchema::argument_def = {ArgumentDef("indices", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("updates", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("shape", ValueTypeOf<Shape>())};

struct ScatterNdLikeSchema {
  using FType = decltype(functional::ScatterNdLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ScatterNdLike;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ScatterNdLike(Tensor like, Tensor updates, Tensor indices)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ScatterNdLikeSchema::max_args;
constexpr size_t ScatterNdLikeSchema::max_positionals;
constexpr size_t ScatterNdLikeSchema::max_keywords;
constexpr char const* ScatterNdLikeSchema::signature;
ReturnDef ScatterNdLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ScatterNdLikeSchema::argument_def = {ArgumentDef("like", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("updates", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("indices", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct MatMulSchema {
  using FType = decltype(functional::MatMul);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::MatMul;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor MatMul(Tensor a, Tensor b, *, Bool transpose_a=False, Bool transpose_b=False, Double alpha=1.0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MatMulSchema::max_args;
constexpr size_t MatMulSchema::max_positionals;
constexpr size_t MatMulSchema::max_keywords;
constexpr char const* MatMulSchema::signature;
ReturnDef MatMulSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> MatMulSchema::argument_def = {ArgumentDef("a", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("b", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("transpose_a", bool(false)), ArgumentDef("transpose_b", bool(false)), ArgumentDef("alpha", double(1.0))};

struct BatchMatMulSchema {
  using FType = decltype(functional::BatchMatMul);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::BatchMatMul;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor BatchMatMul(Tensor a, Tensor b, *, Bool transpose_a=False, Bool transpose_b=False, Double alpha=1.0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t BatchMatMulSchema::max_args;
constexpr size_t BatchMatMulSchema::max_positionals;
constexpr size_t BatchMatMulSchema::max_keywords;
constexpr char const* BatchMatMulSchema::signature;
ReturnDef BatchMatMulSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> BatchMatMulSchema::argument_def = {ArgumentDef("a", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("b", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("transpose_a", bool(false)), ArgumentDef("transpose_b", bool(false)), ArgumentDef("alpha", double(1.0))};

struct SparseSoftmaxCrossEntropySchema {
  using FType = decltype(functional::SparseSoftmaxCrossEntropy);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::SparseSoftmaxCrossEntropy;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor SparseSoftmaxCrossEntropy(Tensor logits, Tensor label, *, Int64 depth)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SparseSoftmaxCrossEntropySchema::max_args;
constexpr size_t SparseSoftmaxCrossEntropySchema::max_positionals;
constexpr size_t SparseSoftmaxCrossEntropySchema::max_keywords;
constexpr char const* SparseSoftmaxCrossEntropySchema::signature;
ReturnDef SparseSoftmaxCrossEntropySchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SparseSoftmaxCrossEntropySchema::argument_def = {ArgumentDef("logits", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("label", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("depth", ValueTypeOf<int64_t>())};

struct SmoothL1LossSchema {
  using FType = decltype(functional::SmoothL1Loss);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::SmoothL1Loss;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor SmoothL1Loss(Tensor logits, Tensor label, *, Float beta)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SmoothL1LossSchema::max_args;
constexpr size_t SmoothL1LossSchema::max_positionals;
constexpr size_t SmoothL1LossSchema::max_keywords;
constexpr char const* SmoothL1LossSchema::signature;
ReturnDef SmoothL1LossSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SmoothL1LossSchema::argument_def = {ArgumentDef("logits", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("label", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("beta", ValueTypeOf<float>())};

struct WhereSchema {
  using FType = decltype(functional::Where);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Where;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Where(Tensor condition, Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t WhereSchema::max_args;
constexpr size_t WhereSchema::max_positionals;
constexpr size_t WhereSchema::max_keywords;
constexpr char const* WhereSchema::signature;
ReturnDef WhereSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> WhereSchema::argument_def = {ArgumentDef("condition", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct NegativeSchema {
  using FType = decltype(functional::Negative);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Negative;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Negative(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t NegativeSchema::max_args;
constexpr size_t NegativeSchema::max_positionals;
constexpr size_t NegativeSchema::max_keywords;
constexpr char const* NegativeSchema::signature;
ReturnDef NegativeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> NegativeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct LayerNormAffineSchema {
  using FType = decltype(functional::LayerNormAffine);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::LayerNormAffine;
  static constexpr size_t max_args = 6;
  static constexpr size_t max_positionals = 6;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor LayerNormAffine(Tensor x, Tensor gamma, Tensor beta, *, Int64 begin_norm_axis, Int64 begin_params_axis, Double epsilon)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LayerNormAffineSchema::max_args;
constexpr size_t LayerNormAffineSchema::max_positionals;
constexpr size_t LayerNormAffineSchema::max_keywords;
constexpr char const* LayerNormAffineSchema::signature;
ReturnDef LayerNormAffineSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LayerNormAffineSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("gamma", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("beta", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("begin_norm_axis", ValueTypeOf<int64_t>()), ArgumentDef("begin_params_axis", ValueTypeOf<int64_t>()), ArgumentDef("epsilon", ValueTypeOf<double>())};

struct LayerNormSchema {
  using FType = decltype(functional::LayerNorm);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::LayerNorm;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor LayerNorm(Tensor x, *, Int64 begin_norm_axis, Int64 begin_params_axis, Double epsilon)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LayerNormSchema::max_args;
constexpr size_t LayerNormSchema::max_positionals;
constexpr size_t LayerNormSchema::max_keywords;
constexpr char const* LayerNormSchema::signature;
ReturnDef LayerNormSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LayerNormSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("begin_norm_axis", ValueTypeOf<int64_t>()), ArgumentDef("begin_params_axis", ValueTypeOf<int64_t>()), ArgumentDef("epsilon", ValueTypeOf<double>())};

struct AvgPool2DSchema {
  using FType = decltype(functional::AvgPool2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AvgPool2D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "Tensor AvgPool2D(Tensor x, *, Int32List kernel_size, Int32List stride, String padding, Int32List padding_before, Int32List padding_after, String data_format=\"channels_first\", Bool ceil_mode=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AvgPool2DSchema::max_args;
constexpr size_t AvgPool2DSchema::max_positionals;
constexpr size_t AvgPool2DSchema::max_keywords;
constexpr char const* AvgPool2DSchema::signature;
ReturnDef AvgPool2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AvgPool2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding", ValueTypeOf<std::string>()), ArgumentDef("padding_before", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding_after", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("ceil_mode", bool(false))};

struct MaxPool2DSchema {
  using FType = decltype(functional::MaxPool2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::MaxPool2D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "Tensor MaxPool2D(Tensor x, *, Int32List kernel_size, Int32List stride, String padding, Int32List padding_before, Int32List padding_after, String data_format=\"channels_first\", Bool ceil_mode=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MaxPool2DSchema::max_args;
constexpr size_t MaxPool2DSchema::max_positionals;
constexpr size_t MaxPool2DSchema::max_keywords;
constexpr char const* MaxPool2DSchema::signature;
ReturnDef MaxPool2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> MaxPool2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding", ValueTypeOf<std::string>()), ArgumentDef("padding_before", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("padding_after", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("ceil_mode", bool(false))};

struct AdaptiveAvgPool1DSchema {
  using FType = decltype(functional::AdaptiveAvgPool1D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AdaptiveAvgPool1D;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor AdaptiveAvgPool1D(Tensor x, *, Int64List output_size)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AdaptiveAvgPool1DSchema::max_args;
constexpr size_t AdaptiveAvgPool1DSchema::max_positionals;
constexpr size_t AdaptiveAvgPool1DSchema::max_keywords;
constexpr char const* AdaptiveAvgPool1DSchema::signature;
ReturnDef AdaptiveAvgPool1DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AdaptiveAvgPool1DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("output_size", ValueTypeOf<std::vector<int64_t>>())};

struct AdaptiveAvgPool2DSchema {
  using FType = decltype(functional::AdaptiveAvgPool2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AdaptiveAvgPool2D;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor AdaptiveAvgPool2D(Tensor x, *, Int64List output_size)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AdaptiveAvgPool2DSchema::max_args;
constexpr size_t AdaptiveAvgPool2DSchema::max_positionals;
constexpr size_t AdaptiveAvgPool2DSchema::max_keywords;
constexpr char const* AdaptiveAvgPool2DSchema::signature;
ReturnDef AdaptiveAvgPool2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AdaptiveAvgPool2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("output_size", ValueTypeOf<std::vector<int64_t>>())};

struct AdaptiveAvgPool3DSchema {
  using FType = decltype(functional::AdaptiveAvgPool3D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AdaptiveAvgPool3D;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor AdaptiveAvgPool3D(Tensor x, *, Int64List output_size)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AdaptiveAvgPool3DSchema::max_args;
constexpr size_t AdaptiveAvgPool3DSchema::max_positionals;
constexpr size_t AdaptiveAvgPool3DSchema::max_keywords;
constexpr char const* AdaptiveAvgPool3DSchema::signature;
ReturnDef AdaptiveAvgPool3DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AdaptiveAvgPool3DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("output_size", ValueTypeOf<std::vector<int64_t>>())};

struct Maxpool1DSchema {
  using FType = decltype(functional::Maxpool1D);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::Maxpool1D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "TensorTuple Maxpool1D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Int32List dilation, Bool return_indices=True, Bool ceil_mode=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Maxpool1DSchema::max_args;
constexpr size_t Maxpool1DSchema::max_positionals;
constexpr size_t Maxpool1DSchema::max_keywords;
constexpr char const* Maxpool1DSchema::signature;
ReturnDef Maxpool1DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> Maxpool1DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("return_indices", bool(true)), ArgumentDef("ceil_mode", bool(false))};

struct Maxpool2DSchema {
  using FType = decltype(functional::Maxpool2D);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::Maxpool2D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "TensorTuple Maxpool2D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Int32List dilation, Bool return_indices=True, Bool ceil_mode=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Maxpool2DSchema::max_args;
constexpr size_t Maxpool2DSchema::max_positionals;
constexpr size_t Maxpool2DSchema::max_keywords;
constexpr char const* Maxpool2DSchema::signature;
ReturnDef Maxpool2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> Maxpool2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("return_indices", bool(true)), ArgumentDef("ceil_mode", bool(false))};

struct Maxpool3DSchema {
  using FType = decltype(functional::Maxpool3D);
  using R = Maybe<one::TensorTuple>;

  static constexpr FType* func = &functional::Maxpool3D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "TensorTuple Maxpool3D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Int32List dilation, Bool return_indices=True, Bool ceil_mode=False)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Maxpool3DSchema::max_args;
constexpr size_t Maxpool3DSchema::max_positionals;
constexpr size_t Maxpool3DSchema::max_keywords;
constexpr char const* Maxpool3DSchema::signature;
ReturnDef Maxpool3DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::TensorTuple>>());
std::vector<ArgumentDef> Maxpool3DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("dilation", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("return_indices", bool(true)), ArgumentDef("ceil_mode", bool(false))};

struct PReluSchema {
  using FType = decltype(functional::PRelu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::PRelu;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor PRelu(Tensor x, Tensor alpha)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t PReluSchema::max_args;
constexpr size_t PReluSchema::max_positionals;
constexpr size_t PReluSchema::max_keywords;
constexpr char const* PReluSchema::signature;
ReturnDef PReluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> PReluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("alpha", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReshapeSchema {
  using FType = decltype(functional::Reshape);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Reshape;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Reshape(Tensor x, *, Shape shape)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReshapeSchema::max_args;
constexpr size_t ReshapeSchema::max_positionals;
constexpr size_t ReshapeSchema::max_keywords;
constexpr char const* ReshapeSchema::signature;
ReturnDef ReshapeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReshapeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("shape", ValueTypeOf<Shape>())};

struct SliceSchema {
  using FType = decltype(functional::Slice);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Slice;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor Slice(Tensor x, *, Int64List start, Int64List stop, Int64List step)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SliceSchema::max_args;
constexpr size_t SliceSchema::max_positionals;
constexpr size_t SliceSchema::max_keywords;
constexpr char const* SliceSchema::signature;
ReturnDef SliceSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SliceSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("start", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("stop", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("step", ValueTypeOf<std::vector<int64_t>>())};

struct SliceUpdateSchema {
  using FType = decltype(functional::SliceUpdate);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::SliceUpdate;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor SliceUpdate(Tensor x, Tensor update, *, Int64List start, Int64List stop, Int64List step)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SliceUpdateSchema::max_args;
constexpr size_t SliceUpdateSchema::max_positionals;
constexpr size_t SliceUpdateSchema::max_keywords;
constexpr char const* SliceUpdateSchema::signature;
ReturnDef SliceUpdateSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SliceUpdateSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("update", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("start", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("stop", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("step", ValueTypeOf<std::vector<int64_t>>())};

struct LogicalSliceSchema {
  using FType = decltype(functional::LogicalSlice);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::LogicalSlice;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor LogicalSlice(Tensor x, *, Int64List start, Int64List stop, Int64List step)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LogicalSliceSchema::max_args;
constexpr size_t LogicalSliceSchema::max_positionals;
constexpr size_t LogicalSliceSchema::max_keywords;
constexpr char const* LogicalSliceSchema::signature;
ReturnDef LogicalSliceSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LogicalSliceSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("start", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("stop", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("step", ValueTypeOf<std::vector<int64_t>>())};

struct LogicalSliceAssignSchema {
  using FType = decltype(functional::LogicalSliceAssign);
  using R = Maybe<void>;

  static constexpr FType* func = &functional::LogicalSliceAssign;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Void LogicalSliceAssign(Tensor ref, Tensor value, *, Int64List start, Int64List stop, Int64List step)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LogicalSliceAssignSchema::max_args;
constexpr size_t LogicalSliceAssignSchema::max_positionals;
constexpr size_t LogicalSliceAssignSchema::max_keywords;
constexpr char const* LogicalSliceAssignSchema::signature;
ReturnDef LogicalSliceAssignSchema::return_def = ReturnDef(ValueTypeOf<Maybe<void>>());
std::vector<ArgumentDef> LogicalSliceAssignSchema::argument_def = {ArgumentDef("ref", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("value", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("start", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("stop", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("step", ValueTypeOf<std::vector<int64_t>>())};

struct SqueezeSchema {
  using FType = decltype(functional::Squeeze);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Squeeze;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Squeeze(Tensor x, *, Int32List dim)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SqueezeSchema::max_args;
constexpr size_t SqueezeSchema::max_positionals;
constexpr size_t SqueezeSchema::max_keywords;
constexpr char const* SqueezeSchema::signature;
ReturnDef SqueezeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SqueezeSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dim", ValueTypeOf<std::vector<int32_t>>())};

struct CopySchema {
  using FType = decltype(functional::Copy);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Copy;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor Copy(Tensor x, *, String device_type, Int64 device_id)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t CopySchema::max_args;
constexpr size_t CopySchema::max_positionals;
constexpr size_t CopySchema::max_keywords;
constexpr char const* CopySchema::signature;
ReturnDef CopySchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> CopySchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("device_type", ValueTypeOf<std::string>()), ArgumentDef("device_id", ValueTypeOf<int64_t>())};

struct FlipSchema {
  using FType = decltype(functional::Flip);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Flip;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Flip(Tensor x, *, Int32List dims)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t FlipSchema::max_args;
constexpr size_t FlipSchema::max_positionals;
constexpr size_t FlipSchema::max_keywords;
constexpr char const* FlipSchema::signature;
ReturnDef FlipSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> FlipSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dims", ValueTypeOf<std::vector<int32_t>>())};

struct UpsampleSchema {
  using FType = decltype(functional::Upsample);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Upsample;
  static constexpr size_t max_args = 6;
  static constexpr size_t max_positionals = 6;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor Upsample(Tensor x, *, Float height_scale, Float width_scale, Bool align_corners, String interpolation, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleSchema::max_args;
constexpr size_t UpsampleSchema::max_positionals;
constexpr size_t UpsampleSchema::max_keywords;
constexpr char const* UpsampleSchema::signature;
ReturnDef UpsampleSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("align_corners", ValueTypeOf<bool>()), ArgumentDef("interpolation", ValueTypeOf<std::string>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleLinear1DSchema {
  using FType = decltype(functional::UpsampleLinear1D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleLinear1D;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor UpsampleLinear1D(Tensor x, *, Float scale_factor, Bool align_corners, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleLinear1DSchema::max_args;
constexpr size_t UpsampleLinear1DSchema::max_positionals;
constexpr size_t UpsampleLinear1DSchema::max_keywords;
constexpr char const* UpsampleLinear1DSchema::signature;
ReturnDef UpsampleLinear1DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleLinear1DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("scale_factor", ValueTypeOf<float>()), ArgumentDef("align_corners", ValueTypeOf<bool>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleNearest1DSchema {
  using FType = decltype(functional::UpsampleNearest1D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleNearest1D;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor UpsampleNearest1D(Tensor x, *, Float scale_factor, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleNearest1DSchema::max_args;
constexpr size_t UpsampleNearest1DSchema::max_positionals;
constexpr size_t UpsampleNearest1DSchema::max_keywords;
constexpr char const* UpsampleNearest1DSchema::signature;
ReturnDef UpsampleNearest1DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleNearest1DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("scale_factor", ValueTypeOf<float>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleNearest2DSchema {
  using FType = decltype(functional::UpsampleNearest2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleNearest2D;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor UpsampleNearest2D(Tensor x, *, Float height_scale, Float width_scale, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleNearest2DSchema::max_args;
constexpr size_t UpsampleNearest2DSchema::max_positionals;
constexpr size_t UpsampleNearest2DSchema::max_keywords;
constexpr char const* UpsampleNearest2DSchema::signature;
ReturnDef UpsampleNearest2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleNearest2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleBilinear2DSchema {
  using FType = decltype(functional::UpsampleBilinear2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleBilinear2D;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor UpsampleBilinear2D(Tensor x, *, Float height_scale, Float width_scale, Bool align_corners, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleBilinear2DSchema::max_args;
constexpr size_t UpsampleBilinear2DSchema::max_positionals;
constexpr size_t UpsampleBilinear2DSchema::max_keywords;
constexpr char const* UpsampleBilinear2DSchema::signature;
ReturnDef UpsampleBilinear2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleBilinear2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("align_corners", ValueTypeOf<bool>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleBicubic2DSchema {
  using FType = decltype(functional::UpsampleBicubic2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleBicubic2D;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor UpsampleBicubic2D(Tensor x, *, Float height_scale, Float width_scale, Bool align_corners, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleBicubic2DSchema::max_args;
constexpr size_t UpsampleBicubic2DSchema::max_positionals;
constexpr size_t UpsampleBicubic2DSchema::max_keywords;
constexpr char const* UpsampleBicubic2DSchema::signature;
ReturnDef UpsampleBicubic2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleBicubic2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("align_corners", ValueTypeOf<bool>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleNearest3DSchema {
  using FType = decltype(functional::UpsampleNearest3D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleNearest3D;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor UpsampleNearest3D(Tensor x, *, Float depth_scale, Float height_scale, Float width_scale, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleNearest3DSchema::max_args;
constexpr size_t UpsampleNearest3DSchema::max_positionals;
constexpr size_t UpsampleNearest3DSchema::max_keywords;
constexpr char const* UpsampleNearest3DSchema::signature;
ReturnDef UpsampleNearest3DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleNearest3DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("depth_scale", ValueTypeOf<float>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("data_format", std::string("channels_first"))};

struct UpsampleTrilinear3DSchema {
  using FType = decltype(functional::UpsampleTrilinear3D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::UpsampleTrilinear3D;
  static constexpr size_t max_args = 6;
  static constexpr size_t max_positionals = 6;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor UpsampleTrilinear3D(Tensor x, *, Float depth_scale, Float height_scale, Float width_scale, Bool align_corners, String data_format=\"channels_first\")";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t UpsampleTrilinear3DSchema::max_args;
constexpr size_t UpsampleTrilinear3DSchema::max_positionals;
constexpr size_t UpsampleTrilinear3DSchema::max_keywords;
constexpr char const* UpsampleTrilinear3DSchema::signature;
ReturnDef UpsampleTrilinear3DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> UpsampleTrilinear3DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("depth_scale", ValueTypeOf<float>()), ArgumentDef("height_scale", ValueTypeOf<float>()), ArgumentDef("width_scale", ValueTypeOf<float>()), ArgumentDef("align_corners", ValueTypeOf<bool>()), ArgumentDef("data_format", std::string("channels_first"))};

struct AbsSchema {
  using FType = decltype(functional::Abs);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Abs;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Abs(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AbsSchema::max_args;
constexpr size_t AbsSchema::max_positionals;
constexpr size_t AbsSchema::max_keywords;
constexpr char const* AbsSchema::signature;
ReturnDef AbsSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AbsSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AcosSchema {
  using FType = decltype(functional::Acos);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Acos;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Acos(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AcosSchema::max_args;
constexpr size_t AcosSchema::max_positionals;
constexpr size_t AcosSchema::max_keywords;
constexpr char const* AcosSchema::signature;
ReturnDef AcosSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AcosSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AcoshSchema {
  using FType = decltype(functional::Acosh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Acosh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Acosh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AcoshSchema::max_args;
constexpr size_t AcoshSchema::max_positionals;
constexpr size_t AcoshSchema::max_keywords;
constexpr char const* AcoshSchema::signature;
ReturnDef AcoshSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AcoshSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AsinSchema {
  using FType = decltype(functional::Asin);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Asin;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Asin(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AsinSchema::max_args;
constexpr size_t AsinSchema::max_positionals;
constexpr size_t AsinSchema::max_keywords;
constexpr char const* AsinSchema::signature;
ReturnDef AsinSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AsinSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AsinhSchema {
  using FType = decltype(functional::Asinh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Asinh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Asinh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AsinhSchema::max_args;
constexpr size_t AsinhSchema::max_positionals;
constexpr size_t AsinhSchema::max_keywords;
constexpr char const* AsinhSchema::signature;
ReturnDef AsinhSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AsinhSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AtanSchema {
  using FType = decltype(functional::Atan);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Atan;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Atan(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AtanSchema::max_args;
constexpr size_t AtanSchema::max_positionals;
constexpr size_t AtanSchema::max_keywords;
constexpr char const* AtanSchema::signature;
ReturnDef AtanSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AtanSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AtanhSchema {
  using FType = decltype(functional::Atanh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Atanh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Atanh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AtanhSchema::max_args;
constexpr size_t AtanhSchema::max_positionals;
constexpr size_t AtanhSchema::max_keywords;
constexpr char const* AtanhSchema::signature;
ReturnDef AtanhSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AtanhSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct CeilSchema {
  using FType = decltype(functional::Ceil);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Ceil;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Ceil(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t CeilSchema::max_args;
constexpr size_t CeilSchema::max_positionals;
constexpr size_t CeilSchema::max_keywords;
constexpr char const* CeilSchema::signature;
ReturnDef CeilSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> CeilSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ErfSchema {
  using FType = decltype(functional::Erf);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Erf;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Erf(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ErfSchema::max_args;
constexpr size_t ErfSchema::max_positionals;
constexpr size_t ErfSchema::max_keywords;
constexpr char const* ErfSchema::signature;
ReturnDef ErfSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ErfSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ErfcSchema {
  using FType = decltype(functional::Erfc);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Erfc;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Erfc(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ErfcSchema::max_args;
constexpr size_t ErfcSchema::max_positionals;
constexpr size_t ErfcSchema::max_keywords;
constexpr char const* ErfcSchema::signature;
ReturnDef ErfcSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ErfcSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct Expm1Schema {
  using FType = decltype(functional::Expm1);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Expm1;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Expm1(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Expm1Schema::max_args;
constexpr size_t Expm1Schema::max_positionals;
constexpr size_t Expm1Schema::max_keywords;
constexpr char const* Expm1Schema::signature;
ReturnDef Expm1Schema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Expm1Schema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct FloorSchema {
  using FType = decltype(functional::Floor);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Floor;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Floor(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t FloorSchema::max_args;
constexpr size_t FloorSchema::max_positionals;
constexpr size_t FloorSchema::max_keywords;
constexpr char const* FloorSchema::signature;
ReturnDef FloorSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> FloorSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct LgammaSchema {
  using FType = decltype(functional::Lgamma);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Lgamma;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Lgamma(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LgammaSchema::max_args;
constexpr size_t LgammaSchema::max_positionals;
constexpr size_t LgammaSchema::max_keywords;
constexpr char const* LgammaSchema::signature;
ReturnDef LgammaSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LgammaSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct Log1pSchema {
  using FType = decltype(functional::Log1p);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Log1p;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Log1p(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Log1pSchema::max_args;
constexpr size_t Log1pSchema::max_positionals;
constexpr size_t Log1pSchema::max_keywords;
constexpr char const* Log1pSchema::signature;
ReturnDef Log1pSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Log1pSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct LogSigmoidSchema {
  using FType = decltype(functional::LogSigmoid);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::LogSigmoid;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor LogSigmoid(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t LogSigmoidSchema::max_args;
constexpr size_t LogSigmoidSchema::max_positionals;
constexpr size_t LogSigmoidSchema::max_keywords;
constexpr char const* LogSigmoidSchema::signature;
ReturnDef LogSigmoidSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> LogSigmoidSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct RintSchema {
  using FType = decltype(functional::Rint);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Rint;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Rint(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RintSchema::max_args;
constexpr size_t RintSchema::max_positionals;
constexpr size_t RintSchema::max_keywords;
constexpr char const* RintSchema::signature;
ReturnDef RintSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RintSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct RoundSchema {
  using FType = decltype(functional::Round);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Round;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Round(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RoundSchema::max_args;
constexpr size_t RoundSchema::max_positionals;
constexpr size_t RoundSchema::max_keywords;
constexpr char const* RoundSchema::signature;
ReturnDef RoundSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RoundSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SignSchema {
  using FType = decltype(functional::Sign);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Sign;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Sign(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SignSchema::max_args;
constexpr size_t SignSchema::max_positionals;
constexpr size_t SignSchema::max_keywords;
constexpr char const* SignSchema::signature;
ReturnDef SignSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SignSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SinhSchema {
  using FType = decltype(functional::Sinh);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Sinh;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Sinh(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SinhSchema::max_args;
constexpr size_t SinhSchema::max_positionals;
constexpr size_t SinhSchema::max_keywords;
constexpr char const* SinhSchema::signature;
ReturnDef SinhSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SinhSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SoftplusSchema {
  using FType = decltype(functional::Softplus);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Softplus;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Softplus(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SoftplusSchema::max_args;
constexpr size_t SoftplusSchema::max_positionals;
constexpr size_t SoftplusSchema::max_keywords;
constexpr char const* SoftplusSchema::signature;
ReturnDef SoftplusSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SoftplusSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct TriuSchema {
  using FType = decltype(functional::Triu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Triu;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Triu(Tensor x, Int64 diagonal=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TriuSchema::max_args;
constexpr size_t TriuSchema::max_positionals;
constexpr size_t TriuSchema::max_keywords;
constexpr char const* TriuSchema::signature;
ReturnDef TriuSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> TriuSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("diagonal", int64_t(0))};

struct DropoutSchema {
  using FType = decltype(functional::Dropout);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Dropout;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor Dropout(Tensor x, *, Float p=0.5, Generator generator=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t DropoutSchema::max_args;
constexpr size_t DropoutSchema::max_positionals;
constexpr size_t DropoutSchema::max_keywords;
constexpr char const* DropoutSchema::signature;
ReturnDef DropoutSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> DropoutSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("p", float(0.5)), ArgumentDef("generator", Optional<one::Generator>())};

struct PadSchema {
  using FType = decltype(functional::Pad);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Pad;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor Pad(Tensor x, *, Int64List pad, String mode=\"constant\", Scalar value=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t PadSchema::max_args;
constexpr size_t PadSchema::max_positionals;
constexpr size_t PadSchema::max_keywords;
constexpr char const* PadSchema::signature;
ReturnDef PadSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> PadSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("pad", ValueTypeOf<std::vector<int64_t>>()), ArgumentDef("mode", std::string("constant")), ArgumentDef("value", Scalar(0))};

struct SiluSchema {
  using FType = decltype(functional::Silu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Silu;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Silu(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SiluSchema::max_args;
constexpr size_t SiluSchema::max_positionals;
constexpr size_t SiluSchema::max_keywords;
constexpr char const* SiluSchema::signature;
ReturnDef SiluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SiluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct MishSchema {
  using FType = decltype(functional::Mish);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Mish;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Mish(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MishSchema::max_args;
constexpr size_t MishSchema::max_positionals;
constexpr size_t MishSchema::max_keywords;
constexpr char const* MishSchema::signature;
ReturnDef MishSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> MishSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SeluSchema {
  using FType = decltype(functional::Selu);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Selu;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Selu(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SeluSchema::max_args;
constexpr size_t SeluSchema::max_positionals;
constexpr size_t SeluSchema::max_keywords;
constexpr char const* SeluSchema::signature;
ReturnDef SeluSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SeluSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SoftSignSchema {
  using FType = decltype(functional::SoftSign);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::SoftSign;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor SoftSign(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SoftSignSchema::max_args;
constexpr size_t SoftSignSchema::max_positionals;
constexpr size_t SoftSignSchema::max_keywords;
constexpr char const* SoftSignSchema::signature;
ReturnDef SoftSignSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SoftSignSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct DiagSchema {
  using FType = decltype(functional::Diag);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Diag;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Diag(Tensor x, *, Int32 diagonal=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t DiagSchema::max_args;
constexpr size_t DiagSchema::max_positionals;
constexpr size_t DiagSchema::max_keywords;
constexpr char const* DiagSchema::signature;
ReturnDef DiagSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> DiagSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("diagonal", int32_t(0))};

struct TensorGetItemSchema {
  using FType = decltype(functional::TensorGetItem);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::TensorGetItem;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor TensorGetItem(Tensor x, *, TensorIndex index)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TensorGetItemSchema::max_args;
constexpr size_t TensorGetItemSchema::max_positionals;
constexpr size_t TensorGetItemSchema::max_keywords;
constexpr char const* TensorGetItemSchema::signature;
ReturnDef TensorGetItemSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> TensorGetItemSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("index", ValueTypeOf<TensorIndex>())};

struct DimScatterAddSchema {
  using FType = decltype(functional::DimScatterAdd);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::DimScatterAdd;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor DimScatterAdd(Tensor input, Tensor index, Tensor src, *, Int32 dim)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t DimScatterAddSchema::max_args;
constexpr size_t DimScatterAddSchema::max_positionals;
constexpr size_t DimScatterAddSchema::max_keywords;
constexpr char const* DimScatterAddSchema::signature;
ReturnDef DimScatterAddSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> DimScatterAddSchema::argument_def = {ArgumentDef("input", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("index", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("src", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("dim", ValueTypeOf<int32_t>())};

struct DimScatterAddScalarSchema {
  using FType = decltype(functional::DimScatterAddScalar);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::DimScatterAddScalar;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Tensor DimScatterAddScalar(Tensor input, Tensor index, *, Float src, Int32 dim)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t DimScatterAddScalarSchema::max_args;
constexpr size_t DimScatterAddScalarSchema::max_positionals;
constexpr size_t DimScatterAddScalarSchema::max_keywords;
constexpr char const* DimScatterAddScalarSchema::signature;
ReturnDef DimScatterAddScalarSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> DimScatterAddScalarSchema::argument_def = {ArgumentDef("input", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("index", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("src", ValueTypeOf<float>()), ArgumentDef("dim", ValueTypeOf<int32_t>())};

struct TensorSetItemSchema {
  using FType = decltype(functional::TensorSetItem);
  using R = Maybe<void>;

  static constexpr FType* func = &functional::TensorSetItem;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 2;
  static constexpr char const* signature = "Void TensorSetItem(Tensor x, *, TensorIndex index, Tensor value)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t TensorSetItemSchema::max_args;
constexpr size_t TensorSetItemSchema::max_positionals;
constexpr size_t TensorSetItemSchema::max_keywords;
constexpr char const* TensorSetItemSchema::signature;
ReturnDef TensorSetItemSchema::return_def = ReturnDef(ValueTypeOf<Maybe<void>>());
std::vector<ArgumentDef> TensorSetItemSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("index", ValueTypeOf<TensorIndex>()), ArgumentDef("value", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct Avgpool1DSchema {
  using FType = decltype(functional::Avgpool1D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Avgpool1D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "Tensor Avgpool1D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Bool ceil_mode=False, Bool count_include_pad=True, Int64 divisor_override=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Avgpool1DSchema::max_args;
constexpr size_t Avgpool1DSchema::max_positionals;
constexpr size_t Avgpool1DSchema::max_keywords;
constexpr char const* Avgpool1DSchema::signature;
ReturnDef Avgpool1DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Avgpool1DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("ceil_mode", bool(false)), ArgumentDef("count_include_pad", bool(true)), ArgumentDef("divisor_override", int64_t(0))};

struct Avgpool2DSchema {
  using FType = decltype(functional::Avgpool2D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Avgpool2D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "Tensor Avgpool2D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Bool ceil_mode=False, Bool count_include_pad=True, Int64 divisor_override=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Avgpool2DSchema::max_args;
constexpr size_t Avgpool2DSchema::max_positionals;
constexpr size_t Avgpool2DSchema::max_keywords;
constexpr char const* Avgpool2DSchema::signature;
ReturnDef Avgpool2DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Avgpool2DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("ceil_mode", bool(false)), ArgumentDef("count_include_pad", bool(true)), ArgumentDef("divisor_override", int64_t(0))};

struct Avgpool3DSchema {
  using FType = decltype(functional::Avgpool3D);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Avgpool3D;
  static constexpr size_t max_args = 8;
  static constexpr size_t max_positionals = 8;
  static constexpr size_t max_keywords = 7;
  static constexpr char const* signature = "Tensor Avgpool3D(Tensor x, *, String data_format=\"channels_first\", Int32List padding, Int32List kernel_size, Int32List stride, Bool ceil_mode=False, Bool count_include_pad=True, Int64 divisor_override=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t Avgpool3DSchema::max_args;
constexpr size_t Avgpool3DSchema::max_positionals;
constexpr size_t Avgpool3DSchema::max_keywords;
constexpr char const* Avgpool3DSchema::signature;
ReturnDef Avgpool3DSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> Avgpool3DSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("data_format", std::string("channels_first")), ArgumentDef("padding", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("kernel_size", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("stride", ValueTypeOf<std::vector<int32_t>>()), ArgumentDef("ceil_mode", bool(false)), ArgumentDef("count_include_pad", bool(true)), ArgumentDef("divisor_override", int64_t(0))};

struct MinimumSchema {
  using FType = decltype(functional::Minimum);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Minimum;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Minimum(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MinimumSchema::max_args;
constexpr size_t MinimumSchema::max_positionals;
constexpr size_t MinimumSchema::max_keywords;
constexpr char const* MinimumSchema::signature;
ReturnDef MinimumSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> MinimumSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct MaximumSchema {
  using FType = decltype(functional::Maximum);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Maximum;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Maximum(Tensor x, Tensor y)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t MaximumSchema::max_args;
constexpr size_t MaximumSchema::max_positionals;
constexpr size_t MaximumSchema::max_keywords;
constexpr char const* MaximumSchema::signature;
ReturnDef MaximumSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> MaximumSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("y", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct StackSchema {
  using FType = decltype(functional::Stack);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Stack;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor Stack(TensorTuple inputs, *, Int64 dim=0)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t StackSchema::max_args;
constexpr size_t StackSchema::max_positionals;
constexpr size_t StackSchema::max_keywords;
constexpr char const* StackSchema::signature;
ReturnDef StackSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> StackSchema::argument_def = {ArgumentDef("inputs", ValueTypeOf<TensorTuple>()), ArgumentDef("dim", int64_t(0))};

struct ToConsistentSchema {
  using FType = decltype(functional::ToConsistent);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ToConsistent;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 3;
  static constexpr char const* signature = "Tensor ToConsistent(Tensor x, *, Placement placement, SbpList sbp, Shape shape=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ToConsistentSchema::max_args;
constexpr size_t ToConsistentSchema::max_positionals;
constexpr size_t ToConsistentSchema::max_keywords;
constexpr char const* ToConsistentSchema::signature;
ReturnDef ToConsistentSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ToConsistentSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("placement", ValueTypeOf<Symbol<ParallelDesc>>()), ArgumentDef("sbp", ValueTypeOf<std::vector<Symbol<cfg::SbpParallel>>>()), ArgumentDef("shape", Optional<Shape>())};

struct ConsistentToLocalSchema {
  using FType = decltype(functional::ConsistentToLocal);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ConsistentToLocal;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ConsistentToLocal(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConsistentToLocalSchema::max_args;
constexpr size_t ConsistentToLocalSchema::max_positionals;
constexpr size_t ConsistentToLocalSchema::max_keywords;
constexpr char const* ConsistentToLocalSchema::signature;
ReturnDef ConsistentToLocalSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConsistentToLocalSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct AllReduceSchema {
  using FType = decltype(functional::AllReduce);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::AllReduce;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor AllReduce(Tensor x)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t AllReduceSchema::max_args;
constexpr size_t AllReduceSchema::max_positionals;
constexpr size_t AllReduceSchema::max_keywords;
constexpr char const* AllReduceSchema::signature;
ReturnDef AllReduceSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> AllReduceSchema::argument_def = {ArgumentDef("x", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct SelectFirstSchema {
  using FType = decltype(functional::SelectFirst);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::SelectFirst;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor SelectFirst(TensorTuple inputs)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t SelectFirstSchema::max_args;
constexpr size_t SelectFirstSchema::max_positionals;
constexpr size_t SelectFirstSchema::max_keywords;
constexpr char const* SelectFirstSchema::signature;
ReturnDef SelectFirstSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> SelectFirstSchema::argument_def = {ArgumentDef("inputs", ValueTypeOf<TensorTuple>())};

struct IdentitySchema {
  using FType = decltype(functional::Identity);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::Identity;
  static constexpr size_t max_args = 1;
  static constexpr size_t max_positionals = 1;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor Identity(Tensor in)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t IdentitySchema::max_args;
constexpr size_t IdentitySchema::max_positionals;
constexpr size_t IdentitySchema::max_keywords;
constexpr char const* IdentitySchema::signature;
ReturnDef IdentitySchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> IdentitySchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReshapeLikeSchema {
  using FType = decltype(functional::ReshapeLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ReshapeLike;
  static constexpr size_t max_args = 2;
  static constexpr size_t max_positionals = 2;
  static constexpr size_t max_keywords = 0;
  static constexpr char const* signature = "Tensor ReshapeLike(Tensor in, Tensor like)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReshapeLikeSchema::max_args;
constexpr size_t ReshapeLikeSchema::max_positionals;
constexpr size_t ReshapeLikeSchema::max_keywords;
constexpr char const* ReshapeLikeSchema::signature;
ReturnDef ReshapeLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReshapeLikeSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("like", ValueTypeOf<std::shared_ptr<one::Tensor>>())};

struct ReduceSumLikeSchema {
  using FType = decltype(functional::ReduceSumLike);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ReduceSumLike;
  static constexpr size_t max_args = 3;
  static constexpr size_t max_positionals = 3;
  static constexpr size_t max_keywords = 1;
  static constexpr char const* signature = "Tensor ReduceSumLike(Tensor in, Tensor like, *, Int32List axis)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ReduceSumLikeSchema::max_args;
constexpr size_t ReduceSumLikeSchema::max_positionals;
constexpr size_t ReduceSumLikeSchema::max_keywords;
constexpr char const* ReduceSumLikeSchema::signature;
ReturnDef ReduceSumLikeSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ReduceSumLikeSchema::argument_def = {ArgumentDef("in", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("like", ValueTypeOf<std::shared_ptr<one::Tensor>>()), ArgumentDef("axis", ValueTypeOf<std::vector<int32_t>>())};

struct RandNSchema {
  using FType = decltype(functional::RandN);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::RandN;
  static constexpr size_t max_args = 4;
  static constexpr size_t max_positionals = 4;
  static constexpr size_t max_keywords = 4;
  static constexpr char const* signature = "Tensor RandN(*, Shape shape, DataType dtype=None, Device device=None, Generator generator=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t RandNSchema::max_args;
constexpr size_t RandNSchema::max_positionals;
constexpr size_t RandNSchema::max_keywords;
constexpr char const* RandNSchema::signature;
ReturnDef RandNSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> RandNSchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("dtype", Optional<DataType>()), ArgumentDef("device", Optional<Symbol<Device>>()), ArgumentDef("generator", Optional<one::Generator>())};

struct ConsistentRandNSchema {
  using FType = decltype(functional::ConsistentRandN);
  using R = Maybe<one::Tensor>;

  static constexpr FType* func = &functional::ConsistentRandN;
  static constexpr size_t max_args = 5;
  static constexpr size_t max_positionals = 5;
  static constexpr size_t max_keywords = 5;
  static constexpr char const* signature = "Tensor ConsistentRandN(*, Shape shape, Placement placement, SbpList sbp_tuple, DataType dtype=None, Generator generator=None)";
  static ReturnDef return_def;
  static std::vector<ArgumentDef> argument_def;
};

constexpr size_t ConsistentRandNSchema::max_args;
constexpr size_t ConsistentRandNSchema::max_positionals;
constexpr size_t ConsistentRandNSchema::max_keywords;
constexpr char const* ConsistentRandNSchema::signature;
ReturnDef ConsistentRandNSchema::return_def = ReturnDef(ValueTypeOf<Maybe<one::Tensor>>());
std::vector<ArgumentDef> ConsistentRandNSchema::argument_def = {ArgumentDef("shape", ValueTypeOf<Shape>()), ArgumentDef("placement", ValueTypeOf<Symbol<ParallelDesc>>()), ArgumentDef("sbp_tuple", ValueTypeOf<std::vector<Symbol<cfg::SbpParallel>>>()), ArgumentDef("dtype", Optional<DataType>()), ArgumentDef("generator", Optional<one::Generator>())};

}  // namespace functional
}  // namespace one

namespace functional = one::functional;

ONEFLOW_API_PYBIND11_MODULE("F", m) {
  m.def("add_n", &functional::PyFunction<functional::AddNSchema>);
  m.def("broadcast_equal", &functional::PyFunction<functional::BroadcastEqualSchema>);
  m.def("broadcast_not_equal", &functional::PyFunction<functional::BroadcastNotEqualSchema>);
  m.def("broadcast_greater", &functional::PyFunction<functional::BroadcastGreaterSchema>);
  m.def("broadcast_greater_equal", &functional::PyFunction<functional::BroadcastGreaterEqualSchema>);
  m.def("broadcast_logical_and", &functional::PyFunction<functional::BroadcastLogicalAndSchema>);
  m.def("broadcast_logical_or", &functional::PyFunction<functional::BroadcastLogicalOrSchema>);
  m.def("broadcast_less", &functional::PyFunction<functional::BroadcastLessSchema>);
  m.def("broadcast_less_equal", &functional::PyFunction<functional::BroadcastLessEqualSchema>);
  m.def("reduce_sum", &functional::PyFunction<functional::ReduceSumSchema>);
  m.def("roll", &functional::PyFunction<functional::RollSchema>);
  m.def("reduce_mean", &functional::PyFunction<functional::ReduceMeanSchema>);
  m.def("transpose", &functional::PyFunction<functional::TransposeSchema>);
  m.def("reciprocal", &functional::PyFunction<functional::ReciprocalSchema>);
  m.def("reciprocal_no_nan", &functional::PyFunction<functional::ReciprocalNoNanSchema>);
  m.def("image_flip", &functional::PyFunction<functional::ImageFlipSchema>);
  m.def("sin", &functional::PyFunction<functional::SinSchema>);
  m.def("cos", &functional::PyFunction<functional::CosSchema>);
  m.def("cosh", &functional::PyFunction<functional::CoshSchema>);
  m.def("fmod", &functional::PyFunction<functional::BroadcastFModSchema>);
  m.def("log", &functional::PyFunction<functional::LogSchema>);
  m.def("sqrt", &functional::PyFunction<functional::SqrtSchema>);
  m.def("rsqrt", &functional::PyFunction<functional::RsqrtSchema>);
  m.def("square", &functional::PyFunction<functional::SquareSchema>);
  m.def("relu", &functional::PyFunction<functional::ReluSchema>);
  m.def("hardtanh", &functional::PyFunction<functional::HardTanhSchema>);
  m.def("tan", &functional::PyFunction<functional::TanSchema>);
  m.def("tanh", &functional::PyFunction<functional::TanhSchema>);
  m.def("elu", &functional::PyFunction<functional::EluSchema>);
  m.def("gelu", &functional::PyFunction<functional::GeluSchema>);
  m.def("sigmoid", &functional::PyFunction<functional::SigmoidSchema>);
  m.def("hardsigmoid", &functional::PyFunction<functional::HardSigmoidSchema>);
  m.def("softmax", &functional::PyFunction<functional::SoftmaxSchema>);
  m.def("hardswish", &functional::PyFunction<functional::HardSwishSchema>);
  m.def("leaky_relu", &functional::PyFunction<functional::LeakyReluSchema>);
  m.def("normalization", &functional::PyFunction<functional::NormalizationSchema>);
  m.def("range", &functional::PyFunction<functional::RangeSchema>);
  m.def("flatten", &functional::PyFunction<functional::FlattenSchema>);
  m.def("argmax", &functional::PyFunction<functional::ArgMaxSchema>);
  m.def("argwhere", &functional::PyFunction<functional::ArgWhereSchema>);
  m.def("broadcast_like", &functional::PyFunction<functional::BroadcastLikeSchema>);
  m.def("cast", &functional::PyFunction<functional::CastSchema>);
  m.def("constant", &functional::PyFunction<functional::ConstantSchema>);
  m.def("consistent_constant", &functional::PyFunction<functional::ConsistentConstantSchema>);
  m.def("empty", &functional::PyFunction<functional::EmptySchema>);
  m.def("consistent_empty", &functional::PyFunction<functional::ConsistentEmptySchema>);
  m.def("zeros_like", &functional::PyFunction<functional::ZerosLikeSchema>);
  m.def("ones_like", &functional::PyFunction<functional::OnesLikeSchema>);
  m.def("bernoulli", &functional::PyFunction<functional::BernoulliSchema>);
  m.def("concat", &functional::PyFunction<functional::ConcatSchema>);
  m.def("bias_add", &functional::PyFunction<functional::BiasAddSchema>);
  m.def("conv1d", &functional::PyFunction<functional::Conv1dSchema>);
  m.def("conv2d", &functional::PyFunction<functional::Conv2dSchema>);
  m.def("fake_quantization", &functional::PyFunction<functional::FakeQuantizationSchema>);
  m.def("quantization", &functional::PyFunction<functional::QuantizationSchema>);
  m.def("min_max_observer", &functional::PyFunction<functional::MinMaxObserverSchema>);
  m.def("moving_average_min_max_observer", &functional::PyFunction<functional::MovingAverageMinMaxObserverSchema>);
  m.def("conv3d", &functional::PyFunction<functional::Conv3dSchema>);
  m.def("expand", &functional::PyFunction<functional::ExpandSchema>);
  m.def("expand_dims", &functional::PyFunction<functional::ExpandDimsSchema>);
  m.def("exp", &functional::PyFunction<functional::ExpSchema>);
  m.def("gather", &functional::PyFunction<functional::GatherSchema>);
  m.def("dim_gather", &functional::PyFunction<functional::DimGatherSchema>);
  m.def("gather_nd", &functional::PyFunction<functional::GatherNdSchema>);
  m.def("scatternd", &functional::PyFunction<functional::ScatterNdSchema>);
  m.def("scatterndlike", &functional::PyFunction<functional::ScatterNdLikeSchema>);
  m.def("matmul", &functional::PyFunction<functional::MatMulSchema>);
  m.def("batch_matmul", &functional::PyFunction<functional::BatchMatMulSchema>);
  m.def("sparse_softmax_cross_entropy", &functional::PyFunction<functional::SparseSoftmaxCrossEntropySchema>);
  m.def("smooth_l1_loss", &functional::PyFunction<functional::SmoothL1LossSchema>);
  m.def("where", &functional::PyFunction<functional::WhereSchema>);
  m.def("negative", &functional::PyFunction<functional::NegativeSchema>);
  m.def("layer_norm_affine", &functional::PyFunction<functional::LayerNormAffineSchema>);
  m.def("layer_norm", &functional::PyFunction<functional::LayerNormSchema>);
  m.def("avg_pool_2d", &functional::PyFunction<functional::AvgPool2DSchema>);
  m.def("max_pool_2d", &functional::PyFunction<functional::MaxPool2DSchema>);
  m.def("adaptive_avg_pool1d", &functional::PyFunction<functional::AdaptiveAvgPool1DSchema>);
  m.def("adaptive_avg_pool2d", &functional::PyFunction<functional::AdaptiveAvgPool2DSchema>);
  m.def("adaptive_avg_pool3d", &functional::PyFunction<functional::AdaptiveAvgPool3DSchema>);
  m.def("maxpool_1d", &functional::PyFunction<functional::Maxpool1DSchema>);
  m.def("maxpool_2d", &functional::PyFunction<functional::Maxpool2DSchema>);
  m.def("maxpool_3d", &functional::PyFunction<functional::Maxpool3DSchema>);
  m.def("prelu", &functional::PyFunction<functional::PReluSchema>);
  m.def("reshape", &functional::PyFunction<functional::ReshapeSchema>);
  m.def("slice", &functional::PyFunction<functional::SliceSchema>);
  m.def("slice_update", &functional::PyFunction<functional::SliceUpdateSchema>);
  m.def("logical_slice", &functional::PyFunction<functional::LogicalSliceSchema>);
  m.def("logical_slice_assign", &functional::PyFunction<functional::LogicalSliceAssignSchema>);
  m.def("squeeze", &functional::PyFunction<functional::SqueezeSchema>);
  m.def("copy", &functional::PyFunction<functional::CopySchema>);
  m.def("flip", &functional::PyFunction<functional::FlipSchema>);
  m.def("upsample", &functional::PyFunction<functional::UpsampleSchema>);
  m.def("upsample_linear_1d", &functional::PyFunction<functional::UpsampleLinear1DSchema>);
  m.def("upsample_nearest_1d", &functional::PyFunction<functional::UpsampleNearest1DSchema>);
  m.def("upsample_nearest_2d", &functional::PyFunction<functional::UpsampleNearest2DSchema>);
  m.def("upsample_bilinear_2d", &functional::PyFunction<functional::UpsampleBilinear2DSchema>);
  m.def("upsample_bicubic_2d", &functional::PyFunction<functional::UpsampleBicubic2DSchema>);
  m.def("upsample_nearest_3d", &functional::PyFunction<functional::UpsampleNearest3DSchema>);
  m.def("upsample_trilinear_3d", &functional::PyFunction<functional::UpsampleTrilinear3DSchema>);
  m.def("abs", &functional::PyFunction<functional::AbsSchema>);
  m.def("acos", &functional::PyFunction<functional::AcosSchema>);
  m.def("acosh", &functional::PyFunction<functional::AcoshSchema>);
  m.def("asin", &functional::PyFunction<functional::AsinSchema>);
  m.def("asinh", &functional::PyFunction<functional::AsinhSchema>);
  m.def("atan", &functional::PyFunction<functional::AtanSchema>);
  m.def("atanh", &functional::PyFunction<functional::AtanhSchema>);
  m.def("ceil", &functional::PyFunction<functional::CeilSchema>);
  m.def("erf", &functional::PyFunction<functional::ErfSchema>);
  m.def("erfc", &functional::PyFunction<functional::ErfcSchema>);
  m.def("expm1", &functional::PyFunction<functional::Expm1Schema>);
  m.def("floor", &functional::PyFunction<functional::FloorSchema>);
  m.def("lgamma", &functional::PyFunction<functional::LgammaSchema>);
  m.def("log1p", &functional::PyFunction<functional::Log1pSchema>);
  m.def("log_sigmoid", &functional::PyFunction<functional::LogSigmoidSchema>);
  m.def("rint", &functional::PyFunction<functional::RintSchema>);
  m.def("round", &functional::PyFunction<functional::RoundSchema>);
  m.def("sign", &functional::PyFunction<functional::SignSchema>);
  m.def("sinh", &functional::PyFunction<functional::SinhSchema>);
  m.def("softplus", &functional::PyFunction<functional::SoftplusSchema>);
  m.def("triu", &functional::PyFunction<functional::TriuSchema>);
  m.def("dropout", &functional::PyFunction<functional::DropoutSchema>);
  m.def("pad", &functional::PyFunction<functional::PadSchema>);
  m.def("silu", &functional::PyFunction<functional::SiluSchema>);
  m.def("mish", &functional::PyFunction<functional::MishSchema>);
  m.def("selu", &functional::PyFunction<functional::SeluSchema>);
  m.def("softsign", &functional::PyFunction<functional::SoftSignSchema>);
  m.def("diag", &functional::PyFunction<functional::DiagSchema>);
  m.def("tensor_getitem", &functional::PyFunction<functional::TensorGetItemSchema>);
  m.def("dim_scatter_add", &functional::PyFunction<functional::DimScatterAddSchema>);
  m.def("dim_scatter_add_scalar", &functional::PyFunction<functional::DimScatterAddScalarSchema>);
  m.def("tensor_setitem", &functional::PyFunction<functional::TensorSetItemSchema>);
  m.def("avgpool_1d", &functional::PyFunction<functional::Avgpool1DSchema>);
  m.def("avgpool_2d", &functional::PyFunction<functional::Avgpool2DSchema>);
  m.def("avgpool_3d", &functional::PyFunction<functional::Avgpool3DSchema>);
  m.def("minimum", &functional::PyFunction<functional::MinimumSchema>);
  m.def("maximum", &functional::PyFunction<functional::MaximumSchema>);
  m.def("stack", &functional::PyFunction<functional::StackSchema>);
  m.def("to_consistent", &functional::PyFunction<functional::ToConsistentSchema>);
  m.def("to_local", &functional::PyFunction<functional::ConsistentToLocalSchema>);
  m.def("all_reduce", &functional::PyFunction<functional::AllReduceSchema>);
  m.def("select_first", &functional::PyFunction<functional::SelectFirstSchema>);
  m.def("identity", &functional::PyFunction<functional::IdentitySchema>);
  m.def("reshape_like", &functional::PyFunction<functional::ReshapeLikeSchema>);
  m.def("reduce_sum_like", &functional::PyFunction<functional::ReduceSumLikeSchema>);
  m.def("randn", &functional::PyFunction<functional::RandNSchema>);
  m.def("consistent_randn", &functional::PyFunction<functional::ConsistentRandNSchema>);

}

}  // namespace oneflow
