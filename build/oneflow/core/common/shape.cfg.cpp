#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/shape.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstShapeProto::_ShapeProto_::_ShapeProto_() { Clear(); }
ConstShapeProto::_ShapeProto_::_ShapeProto_(const _ShapeProto_& other) { CopyFrom(other); }
ConstShapeProto::_ShapeProto_::_ShapeProto_(const ::oneflow::ShapeProto& proto_shapeproto) {
  InitFromProto(proto_shapeproto);
}
ConstShapeProto::_ShapeProto_::_ShapeProto_(_ShapeProto_&& other) = default;
ConstShapeProto::_ShapeProto_::~_ShapeProto_() = default;

void ConstShapeProto::_ShapeProto_::InitFromProto(const ::oneflow::ShapeProto& proto_shapeproto) {
  Clear();
  // repeated field: dim
  if (!proto_shapeproto.dim().empty()) {
    for (const int64_t& elem : proto_shapeproto.dim()) {
      add_dim(elem);
    }
  }
    
}

void ConstShapeProto::_ShapeProto_::ToProto(::oneflow::ShapeProto* proto_shapeproto) const {
  proto_shapeproto->Clear();
  // repeated field: dim
  if (!dim().empty()) {
    for (const int64_t& elem : dim()) {
      proto_shapeproto->add_dim(elem);
    }
  }

}

::std::string ConstShapeProto::_ShapeProto_::DebugString() const {
  ::oneflow::ShapeProto proto_shapeproto;
  this->ToProto(&proto_shapeproto);
  return proto_shapeproto.DebugString();
}

void ConstShapeProto::_ShapeProto_::Clear() {
  clear_dim();
}

void ConstShapeProto::_ShapeProto_::CopyFrom(const _ShapeProto_& other) {
  mutable_dim()->CopyFrom(other.dim());
}


// repeated field dim
::std::size_t ConstShapeProto::_ShapeProto_::dim_size() const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->size();
  }
  return dim_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& ConstShapeProto::_ShapeProto_::dim() const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
    return *(default_static_value.get());
  }
  return *(dim_.get());
}
const int64_t& ConstShapeProto::_ShapeProto_::dim(::std::size_t index) const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->Get(index);
  }
  return dim_->Get(index);
}
void ConstShapeProto::_ShapeProto_::clear_dim() {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
  }
  return dim_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_* ConstShapeProto::_ShapeProto_::mutable_dim() {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
  }
  return  dim_.get();
}
int64_t* ConstShapeProto::_ShapeProto_::mutable_dim(::std::size_t index) {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
  }
  return  dim_->Mutable(index);
}
void ConstShapeProto::_ShapeProto_::add_dim(const int64_t& value) {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
  }
  return dim_->Add(value);
}
void ConstShapeProto::_ShapeProto_::set_dim(::std::size_t index, const int64_t& value) {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>();
  }
  return dim_->Set(index, value);
}


int ConstShapeProto::_ShapeProto_::compare(const _ShapeProto_& other) {
  if (!(dim() == other.dim())) {
    return dim() < other.dim() ? -1 : 1;
  }
  return 0;
}

bool ConstShapeProto::_ShapeProto_::operator==(const _ShapeProto_& other) const {
  return true
    && dim() == other.dim()
  ;
}

std::size_t ConstShapeProto::_ShapeProto_::__CalcHash__() const {
  return 0
    ^ dim().__CalcHash__()
  ;
}

bool ConstShapeProto::_ShapeProto_::operator<(const _ShapeProto_& other) const {
  return false
    || !(dim() == other.dim()) ? 
      dim() < other.dim() : false
  ;
}

using _ShapeProto_ =  ConstShapeProto::_ShapeProto_;
ConstShapeProto::ConstShapeProto(const ::std::shared_ptr<_ShapeProto_>& data): data_(data) {}
ConstShapeProto::ConstShapeProto(): data_(::std::make_shared<_ShapeProto_>()) {}
ConstShapeProto::ConstShapeProto(const ::oneflow::ShapeProto& proto_shapeproto) {
  BuildFromProto(proto_shapeproto);
}
ConstShapeProto::ConstShapeProto(const ConstShapeProto&) = default;
ConstShapeProto::ConstShapeProto(ConstShapeProto&&) noexcept = default;
ConstShapeProto::~ConstShapeProto() = default;

void ConstShapeProto::ToProto(PbMessage* proto_shapeproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ShapeProto*>(proto_shapeproto));
}
  
::std::string ConstShapeProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstShapeProto::__Empty__() const {
  return !data_;
}

int ConstShapeProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"dim", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstShapeProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstShapeProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int64_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstShapeProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &dim();
    default: return nullptr;
  }
}

// repeated field dim
::std::size_t ConstShapeProto::dim_size() const {
  return __SharedPtrOrDefault__()->dim_size();
}
const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& ConstShapeProto::dim() const {
  return __SharedPtrOrDefault__()->dim();
}
const int64_t& ConstShapeProto::dim(::std::size_t index) const {
  return __SharedPtrOrDefault__()->dim(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> ConstShapeProto::shared_const_dim() const {
  return dim().__SharedConst__();
}

::std::shared_ptr<ConstShapeProto> ConstShapeProto::__SharedConst__() const {
  return ::std::make_shared<ConstShapeProto>(data_);
}
int64_t ConstShapeProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstShapeProto::operator==(const ConstShapeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstShapeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstShapeProto::operator<(const ConstShapeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ShapeProto_>& ConstShapeProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ShapeProto_> default_ptr = std::make_shared<_ShapeProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_ShapeProto_>& ConstShapeProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _ShapeProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstShapeProto
void ConstShapeProto::BuildFromProto(const PbMessage& proto_shapeproto) {
  data_ = ::std::make_shared<_ShapeProto_>(dynamic_cast<const ::oneflow::ShapeProto&>(proto_shapeproto));
}

ShapeProto::ShapeProto(const ::std::shared_ptr<_ShapeProto_>& data)
  : ConstShapeProto(data) {}
ShapeProto::ShapeProto(const ShapeProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ShapeProto> resize
ShapeProto::ShapeProto(ShapeProto&&) noexcept = default; 
ShapeProto::ShapeProto(const ::oneflow::ShapeProto& proto_shapeproto) {
  InitFromProto(proto_shapeproto);
}
ShapeProto::ShapeProto() = default;

ShapeProto::~ShapeProto() = default;

void ShapeProto::InitFromProto(const PbMessage& proto_shapeproto) {
  BuildFromProto(proto_shapeproto);
}
  
void* ShapeProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_dim();
    default: return nullptr;
  }
}

bool ShapeProto::operator==(const ShapeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ShapeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ShapeProto::operator<(const ShapeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ShapeProto::Clear() {
  if (data_) { data_.reset(); }
}
void ShapeProto::CopyFrom(const ShapeProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ShapeProto& ShapeProto::operator=(const ShapeProto& other) {
  CopyFrom(other);
  return *this;
}

// repeated field dim
void ShapeProto::clear_dim() {
  return __SharedPtr__()->clear_dim();
}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_* ShapeProto::mutable_dim() {
  return __SharedPtr__()->mutable_dim();
}
int64_t* ShapeProto::mutable_dim(::std::size_t index) {
  return __SharedPtr__()->mutable_dim(index);
}
void ShapeProto::add_dim(const int64_t& value) {
  return __SharedPtr__()->add_dim(value);
}
void ShapeProto::set_dim(::std::size_t index, const int64_t& value) {
  return __SharedPtr__()->set_dim(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> ShapeProto::shared_mutable_dim() {
  return mutable_dim()->__SharedMutable__();
}

::std::shared_ptr<ShapeProto> ShapeProto::__SharedMutable__() {
  return ::std::make_shared<ShapeProto>(__SharedPtr__());
}
ConstShapeSignature::_ShapeSignature_::_ShapeSignature_() { Clear(); }
ConstShapeSignature::_ShapeSignature_::_ShapeSignature_(const _ShapeSignature_& other) { CopyFrom(other); }
ConstShapeSignature::_ShapeSignature_::_ShapeSignature_(const ::oneflow::ShapeSignature& proto_shapesignature) {
  InitFromProto(proto_shapesignature);
}
ConstShapeSignature::_ShapeSignature_::_ShapeSignature_(_ShapeSignature_&& other) = default;
ConstShapeSignature::_ShapeSignature_::~_ShapeSignature_() = default;

void ConstShapeSignature::_ShapeSignature_::InitFromProto(const ::oneflow::ShapeSignature& proto_shapesignature) {
  Clear();
  // required_or_optional field: name
  if (proto_shapesignature.has_name()) {
    set_name(proto_shapesignature.name());
  }
  // map field : field2shape_proto
  if (!proto_shapesignature.field2shape_proto().empty()) {
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_&  mut_field2shape_proto = *mutable_field2shape_proto();
    for (const auto& pair : proto_shapesignature.field2shape_proto()) {
      mut_field2shape_proto[pair.first] = ::oneflow::cfg::ShapeProto(pair.second);
    }
  }
    
}

void ConstShapeSignature::_ShapeSignature_::ToProto(::oneflow::ShapeSignature* proto_shapesignature) const {
  proto_shapesignature->Clear();
  // required_or_optional field: name
  if (this->has_name()) {
    proto_shapesignature->set_name(name());
    }
  // map field : field2shape_proto
  if (!field2shape_proto().empty()) {
    auto& mut_field2shape_proto = *(proto_shapesignature->mutable_field2shape_proto());
    for (const auto& pair : field2shape_proto()) {
      ::oneflow::ShapeProto proto_field2shape_proto_value;
      pair.second.ToProto(&proto_field2shape_proto_value);
      mut_field2shape_proto[pair.first] = proto_field2shape_proto_value;
    }
  }

}

::std::string ConstShapeSignature::_ShapeSignature_::DebugString() const {
  ::oneflow::ShapeSignature proto_shapesignature;
  this->ToProto(&proto_shapesignature);
  return proto_shapesignature.DebugString();
}

void ConstShapeSignature::_ShapeSignature_::Clear() {
  clear_name();
  clear_field2shape_proto();
}

void ConstShapeSignature::_ShapeSignature_::CopyFrom(const _ShapeSignature_& other) {
  if (other.has_name()) {
    set_name(other.name());
  } else {
    clear_name();
  }
  mutable_field2shape_proto()->CopyFrom(other.field2shape_proto());
}


// optional field name
bool ConstShapeSignature::_ShapeSignature_::has_name() const {
  return has_name_;
}
const ::std::string& ConstShapeSignature::_ShapeSignature_::name() const {
  if (has_name_) { return name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstShapeSignature::_ShapeSignature_::clear_name() {
  has_name_ = false;
}
void ConstShapeSignature::_ShapeSignature_::set_name(const ::std::string& value) {
  name_ = value;
  has_name_ = true;
}
::std::string* ConstShapeSignature::_ShapeSignature_::mutable_name() {
  has_name_ = true;
  return &name_;
}

::std::size_t ConstShapeSignature::_ShapeSignature_::field2shape_proto_size() const {
  if (!field2shape_proto_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>();
    return default_static_value->size();
  }
  return field2shape_proto_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& ConstShapeSignature::_ShapeSignature_::field2shape_proto() const {
  if (!field2shape_proto_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>();
    return *(default_static_value.get());
  }
  return *(field2shape_proto_.get());
}

_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ * ConstShapeSignature::_ShapeSignature_::mutable_field2shape_proto() {
  if (!field2shape_proto_) {
    field2shape_proto_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>();
  }
  return field2shape_proto_.get();
}

const ::oneflow::cfg::ShapeProto& ConstShapeSignature::_ShapeSignature_::field2shape_proto(::std::string key) const {
  if (!field2shape_proto_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>();
    return default_static_value->at(key);
  }
  return field2shape_proto_->at(key);
}

void ConstShapeSignature::_ShapeSignature_::clear_field2shape_proto() {
  if (!field2shape_proto_) {
    field2shape_proto_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>();
  }
  return field2shape_proto_->Clear();
}



int ConstShapeSignature::_ShapeSignature_::compare(const _ShapeSignature_& other) {
  if (!(has_name() == other.has_name())) {
    return has_name() < other.has_name() ? -1 : 1;
  } else if (!(name() == other.name())) {
    return name() < other.name() ? -1 : 1;
  }
  if (!(field2shape_proto() == other.field2shape_proto())) {
    return field2shape_proto() < other.field2shape_proto() ? -1 : 1;
  }
  return 0;
}

bool ConstShapeSignature::_ShapeSignature_::operator==(const _ShapeSignature_& other) const {
  return true
    && has_name() == other.has_name() 
    && name() == other.name()
    && field2shape_proto() == other.field2shape_proto()
  ;
}

std::size_t ConstShapeSignature::_ShapeSignature_::__CalcHash__() const {
  return 0
    ^ (has_name() ? std::hash<::std::string>()(name()) : 0)
    ^ field2shape_proto().__CalcHash__()
  ;
}

bool ConstShapeSignature::_ShapeSignature_::operator<(const _ShapeSignature_& other) const {
  return false
    || !(has_name() == other.has_name()) ? 
      has_name() < other.has_name() : false
    || !(name() == other.name()) ? 
      name() < other.name() : false
    || !(field2shape_proto() == other.field2shape_proto()) ? 
      field2shape_proto() < other.field2shape_proto() : false
  ;
}

using _ShapeSignature_ =  ConstShapeSignature::_ShapeSignature_;
ConstShapeSignature::ConstShapeSignature(const ::std::shared_ptr<_ShapeSignature_>& data): data_(data) {}
ConstShapeSignature::ConstShapeSignature(): data_(::std::make_shared<_ShapeSignature_>()) {}
ConstShapeSignature::ConstShapeSignature(const ::oneflow::ShapeSignature& proto_shapesignature) {
  BuildFromProto(proto_shapesignature);
}
ConstShapeSignature::ConstShapeSignature(const ConstShapeSignature&) = default;
ConstShapeSignature::ConstShapeSignature(ConstShapeSignature&&) noexcept = default;
ConstShapeSignature::~ConstShapeSignature() = default;

void ConstShapeSignature::ToProto(PbMessage* proto_shapesignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ShapeSignature*>(proto_shapesignature));
}
  
::std::string ConstShapeSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstShapeSignature::__Empty__() const {
  return !data_;
}

int ConstShapeSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"name", 1},
    {"field2shape_proto", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstShapeSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstShapeSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ShapeProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstShapeSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &name();
    case 2: return &field2shape_proto();
    default: return nullptr;
  }
}

// required or optional field name
bool ConstShapeSignature::has_name() const {
  return __SharedPtrOrDefault__()->has_name();
}
const ::std::string& ConstShapeSignature::name() const {
  return __SharedPtrOrDefault__()->name();
}
// used by pybind11 only
// map field field2shape_proto
::std::size_t ConstShapeSignature::field2shape_proto_size() const {
  return __SharedPtrOrDefault__()->field2shape_proto_size();
}

const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& ConstShapeSignature::field2shape_proto() const {
  return __SharedPtrOrDefault__()->field2shape_proto();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> ConstShapeSignature::shared_const_field2shape_proto() const {
  return field2shape_proto().__SharedConst__();
}

::std::shared_ptr<ConstShapeSignature> ConstShapeSignature::__SharedConst__() const {
  return ::std::make_shared<ConstShapeSignature>(data_);
}
int64_t ConstShapeSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstShapeSignature::operator==(const ConstShapeSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstShapeSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstShapeSignature::operator<(const ConstShapeSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ShapeSignature_>& ConstShapeSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ShapeSignature_> default_ptr = std::make_shared<_ShapeSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ShapeSignature_>& ConstShapeSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ShapeSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstShapeSignature
void ConstShapeSignature::BuildFromProto(const PbMessage& proto_shapesignature) {
  data_ = ::std::make_shared<_ShapeSignature_>(dynamic_cast<const ::oneflow::ShapeSignature&>(proto_shapesignature));
}

ShapeSignature::ShapeSignature(const ::std::shared_ptr<_ShapeSignature_>& data)
  : ConstShapeSignature(data) {}
ShapeSignature::ShapeSignature(const ShapeSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ShapeSignature> resize
ShapeSignature::ShapeSignature(ShapeSignature&&) noexcept = default; 
ShapeSignature::ShapeSignature(const ::oneflow::ShapeSignature& proto_shapesignature) {
  InitFromProto(proto_shapesignature);
}
ShapeSignature::ShapeSignature() = default;

ShapeSignature::~ShapeSignature() = default;

void ShapeSignature::InitFromProto(const PbMessage& proto_shapesignature) {
  BuildFromProto(proto_shapesignature);
}
  
void* ShapeSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_name();
    case 2: return mutable_field2shape_proto();
    default: return nullptr;
  }
}

bool ShapeSignature::operator==(const ShapeSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ShapeSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ShapeSignature::operator<(const ShapeSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ShapeSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ShapeSignature::CopyFrom(const ShapeSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ShapeSignature& ShapeSignature::operator=(const ShapeSignature& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field name
void ShapeSignature::clear_name() {
  return __SharedPtr__()->clear_name();
}
void ShapeSignature::set_name(const ::std::string& value) {
  return __SharedPtr__()->set_name(value);
}
::std::string* ShapeSignature::mutable_name() {
  return  __SharedPtr__()->mutable_name();
}
// repeated field field2shape_proto
void ShapeSignature::clear_field2shape_proto() {
  return __SharedPtr__()->clear_field2shape_proto();
}

const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ & ShapeSignature::field2shape_proto() const {
  return __SharedConst__()->field2shape_proto();
}

_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_* ShapeSignature::mutable_field2shape_proto() {
  return __SharedPtr__()->mutable_field2shape_proto();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> ShapeSignature::shared_mutable_field2shape_proto() {
  return mutable_field2shape_proto()->__SharedMutable__();
}

::std::shared_ptr<ShapeSignature> ShapeSignature::__SharedMutable__() {
  return ::std::make_shared<ShapeSignature>(__SharedPtr__());
}
ConstShapeSignatureList::_ShapeSignatureList_::_ShapeSignatureList_() { Clear(); }
ConstShapeSignatureList::_ShapeSignatureList_::_ShapeSignatureList_(const _ShapeSignatureList_& other) { CopyFrom(other); }
ConstShapeSignatureList::_ShapeSignatureList_::_ShapeSignatureList_(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist) {
  InitFromProto(proto_shapesignaturelist);
}
ConstShapeSignatureList::_ShapeSignatureList_::_ShapeSignatureList_(_ShapeSignatureList_&& other) = default;
ConstShapeSignatureList::_ShapeSignatureList_::~_ShapeSignatureList_() = default;

void ConstShapeSignatureList::_ShapeSignatureList_::InitFromProto(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist) {
  Clear();
  // repeated field: shape_signature
  if (!proto_shapesignaturelist.shape_signature().empty()) {
    for (const ::oneflow::ShapeSignature& elem : proto_shapesignaturelist.shape_signature() ) {
      *mutable_shape_signature()->Add() = ::oneflow::cfg::ShapeSignature(elem);
    }
  }
    
}

void ConstShapeSignatureList::_ShapeSignatureList_::ToProto(::oneflow::ShapeSignatureList* proto_shapesignaturelist) const {
  proto_shapesignaturelist->Clear();
  // repeated field: shape_signature
  if (!shape_signature().empty()) {
    for (const ::oneflow::cfg::ShapeSignature& elem : shape_signature() ) {
      ::oneflow::ShapeSignature proto_shape_signature_elem;
      elem.ToProto(&proto_shape_signature_elem);
      *proto_shapesignaturelist->mutable_shape_signature()->Add() = proto_shape_signature_elem;
    }
  }

}

::std::string ConstShapeSignatureList::_ShapeSignatureList_::DebugString() const {
  ::oneflow::ShapeSignatureList proto_shapesignaturelist;
  this->ToProto(&proto_shapesignaturelist);
  return proto_shapesignaturelist.DebugString();
}

void ConstShapeSignatureList::_ShapeSignatureList_::Clear() {
  clear_shape_signature();
}

void ConstShapeSignatureList::_ShapeSignatureList_::CopyFrom(const _ShapeSignatureList_& other) {
  mutable_shape_signature()->CopyFrom(other.shape_signature());
}


// repeated field shape_signature
::std::size_t ConstShapeSignatureList::_ShapeSignatureList_::shape_signature_size() const {
  if (!shape_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
    return default_static_value->size();
  }
  return shape_signature_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& ConstShapeSignatureList::_ShapeSignatureList_::shape_signature() const {
  if (!shape_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
    return *(default_static_value.get());
  }
  return *(shape_signature_.get());
}
const ::oneflow::cfg::ShapeSignature& ConstShapeSignatureList::_ShapeSignatureList_::shape_signature(::std::size_t index) const {
  if (!shape_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
    return default_static_value->Get(index);
  }
  return shape_signature_->Get(index);
}
void ConstShapeSignatureList::_ShapeSignatureList_::clear_shape_signature() {
  if (!shape_signature_) {
    shape_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
  }
  return shape_signature_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_* ConstShapeSignatureList::_ShapeSignatureList_::mutable_shape_signature() {
  if (!shape_signature_) {
    shape_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
  }
  return  shape_signature_.get();
}
::oneflow::cfg::ShapeSignature* ConstShapeSignatureList::_ShapeSignatureList_::mutable_shape_signature(::std::size_t index) {
  if (!shape_signature_) {
    shape_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
  }
  return  shape_signature_->Mutable(index);
}
::oneflow::cfg::ShapeSignature* ConstShapeSignatureList::_ShapeSignatureList_::add_shape_signature() {
  if (!shape_signature_) {
    shape_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>();
  }
  return shape_signature_->Add();
}


int ConstShapeSignatureList::_ShapeSignatureList_::compare(const _ShapeSignatureList_& other) {
  if (!(shape_signature() == other.shape_signature())) {
    return shape_signature() < other.shape_signature() ? -1 : 1;
  }
  return 0;
}

bool ConstShapeSignatureList::_ShapeSignatureList_::operator==(const _ShapeSignatureList_& other) const {
  return true
    && shape_signature() == other.shape_signature()
  ;
}

std::size_t ConstShapeSignatureList::_ShapeSignatureList_::__CalcHash__() const {
  return 0
    ^ shape_signature().__CalcHash__()
  ;
}

bool ConstShapeSignatureList::_ShapeSignatureList_::operator<(const _ShapeSignatureList_& other) const {
  return false
    || !(shape_signature() == other.shape_signature()) ? 
      shape_signature() < other.shape_signature() : false
  ;
}

using _ShapeSignatureList_ =  ConstShapeSignatureList::_ShapeSignatureList_;
ConstShapeSignatureList::ConstShapeSignatureList(const ::std::shared_ptr<_ShapeSignatureList_>& data): data_(data) {}
ConstShapeSignatureList::ConstShapeSignatureList(): data_(::std::make_shared<_ShapeSignatureList_>()) {}
ConstShapeSignatureList::ConstShapeSignatureList(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist) {
  BuildFromProto(proto_shapesignaturelist);
}
ConstShapeSignatureList::ConstShapeSignatureList(const ConstShapeSignatureList&) = default;
ConstShapeSignatureList::ConstShapeSignatureList(ConstShapeSignatureList&&) noexcept = default;
ConstShapeSignatureList::~ConstShapeSignatureList() = default;

void ConstShapeSignatureList::ToProto(PbMessage* proto_shapesignaturelist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ShapeSignatureList*>(proto_shapesignaturelist));
}
  
::std::string ConstShapeSignatureList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstShapeSignatureList::__Empty__() const {
  return !data_;
}

int ConstShapeSignatureList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"shape_signature", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstShapeSignatureList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstShapeSignatureList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeSignature>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstShapeSignatureList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &shape_signature();
    default: return nullptr;
  }
}

// repeated field shape_signature
::std::size_t ConstShapeSignatureList::shape_signature_size() const {
  return __SharedPtrOrDefault__()->shape_signature_size();
}
const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& ConstShapeSignatureList::shape_signature() const {
  return __SharedPtrOrDefault__()->shape_signature();
}
const ::oneflow::cfg::ShapeSignature& ConstShapeSignatureList::shape_signature(::std::size_t index) const {
  return __SharedPtrOrDefault__()->shape_signature(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> ConstShapeSignatureList::shared_const_shape_signature() const {
  return shape_signature().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstShapeSignature> ConstShapeSignatureList::shared_const_shape_signature(::std::size_t index) const {
  return shape_signature(index).__SharedConst__();
}

::std::shared_ptr<ConstShapeSignatureList> ConstShapeSignatureList::__SharedConst__() const {
  return ::std::make_shared<ConstShapeSignatureList>(data_);
}
int64_t ConstShapeSignatureList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstShapeSignatureList::operator==(const ConstShapeSignatureList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstShapeSignatureList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstShapeSignatureList::operator<(const ConstShapeSignatureList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ShapeSignatureList_>& ConstShapeSignatureList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ShapeSignatureList_> default_ptr = std::make_shared<_ShapeSignatureList_>();
  return default_ptr;
}
const ::std::shared_ptr<_ShapeSignatureList_>& ConstShapeSignatureList::__SharedPtr__() {
  if (!data_) { data_.reset(new _ShapeSignatureList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstShapeSignatureList
void ConstShapeSignatureList::BuildFromProto(const PbMessage& proto_shapesignaturelist) {
  data_ = ::std::make_shared<_ShapeSignatureList_>(dynamic_cast<const ::oneflow::ShapeSignatureList&>(proto_shapesignaturelist));
}

ShapeSignatureList::ShapeSignatureList(const ::std::shared_ptr<_ShapeSignatureList_>& data)
  : ConstShapeSignatureList(data) {}
ShapeSignatureList::ShapeSignatureList(const ShapeSignatureList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ShapeSignatureList> resize
ShapeSignatureList::ShapeSignatureList(ShapeSignatureList&&) noexcept = default; 
ShapeSignatureList::ShapeSignatureList(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist) {
  InitFromProto(proto_shapesignaturelist);
}
ShapeSignatureList::ShapeSignatureList() = default;

ShapeSignatureList::~ShapeSignatureList() = default;

void ShapeSignatureList::InitFromProto(const PbMessage& proto_shapesignaturelist) {
  BuildFromProto(proto_shapesignaturelist);
}
  
void* ShapeSignatureList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_shape_signature();
    default: return nullptr;
  }
}

bool ShapeSignatureList::operator==(const ShapeSignatureList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ShapeSignatureList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ShapeSignatureList::operator<(const ShapeSignatureList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ShapeSignatureList::Clear() {
  if (data_) { data_.reset(); }
}
void ShapeSignatureList::CopyFrom(const ShapeSignatureList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ShapeSignatureList& ShapeSignatureList::operator=(const ShapeSignatureList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field shape_signature
void ShapeSignatureList::clear_shape_signature() {
  return __SharedPtr__()->clear_shape_signature();
}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_* ShapeSignatureList::mutable_shape_signature() {
  return __SharedPtr__()->mutable_shape_signature();
}
::oneflow::cfg::ShapeSignature* ShapeSignatureList::mutable_shape_signature(::std::size_t index) {
  return __SharedPtr__()->mutable_shape_signature(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> ShapeSignatureList::shared_mutable_shape_signature() {
  return mutable_shape_signature()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ShapeSignature> ShapeSignatureList::shared_mutable_shape_signature(::std::size_t index) {
  return mutable_shape_signature(index)->__SharedMutable__();
}
::oneflow::cfg::ShapeSignature* ShapeSignatureList::add_shape_signature() {
  return __SharedPtr__()->add_shape_signature();
}

::std::shared_ptr<ShapeSignatureList> ShapeSignatureList::__SharedMutable__() {
  return ::std::make_shared<ShapeSignatureList>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): ::oneflow::cfg::_RepeatedField_<int64_t>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_(data) {}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_() = default;
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_() = default;

void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ShapeProto>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ShapeProto>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_() = default;

bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ShapeProto>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::ShapeProto& Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstShapeProto> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ConstShapeProto> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ConstShapeProto> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ShapeProto>>& data): Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_(data) {}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_() = default;
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_() = default;

void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ShapeProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ShapeProto>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ShapeProto>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::ShapeProto> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ::oneflow::cfg::ShapeProto> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ::oneflow::cfg::ShapeProto> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeSignature>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeSignature>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ShapeSignature>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstShapeSignature> Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeSignature>>& data): Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_(data) {}
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_() = default;
_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_() = default;

void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeSignature>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeSignature>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ShapeSignature>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::ShapeSignature> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ShapeSignature> _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
