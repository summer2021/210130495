#ifndef CFG_ONEFLOW_CORE_COMMON_DEVICE_TYPE_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_DEVICE_TYPE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;

namespace cfg {


enum DeviceType : unsigned int {
  kInvalidDevice = 0,
  kCPU = 1,
  kGPU = 2,
};

inline ::std::string DeviceType_Name(DeviceType value) {
  switch (value) {
  case kInvalidDevice: { return "kInvalidDevice"; }
  case kCPU: { return "kCPU"; }
  case kGPU: { return "kGPU"; }
  default:
    return "";
  }
}




} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::DeviceType> {
  std::size_t operator()(::oneflow::cfg::DeviceType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


}

#endif  // CFG_ONEFLOW_CORE_COMMON_DEVICE_TYPE_CFG_H_