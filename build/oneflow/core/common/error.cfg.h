#ifndef CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum OpcodeType : unsigned int;
}
}

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class FieldValue;
class OneFieldAssertError;
class TwoFieldAssertError;
class ConfigAssertFailedError;
class ConfigResourceUnavailableError;
class JobSetEmptyError;
class DeviceTagNotFoundError;
class JobNameExistError;
class JobNameEmptyError;
class JobNameNotEqualError;
class NoJobBuildAndInferCtxError;
class JobConfFrozenError;
class JobConfNotSetError;
class JobConfRepeatedSetError;
class JobTypeNotSetError;
class LogicalBlobNameNotExistError;
class LogicalBlobNameExistError;
class LogicalBlobNameInvalidError;
class OpNameExistError;
class OpConfDeviceTagNoSetError;
class PlacementError;
class BlobSplitAxisInferError;
class UnknownJobBuildAndInferError;
class ProtoParseFailedError;
class CheckFailedError;
class TodoError;
class UnimplementedError;
class BoxingNotSupportedError;
class GradientFunctionNotFoundError;
class OpKernelNotFoundError;
class MultipleOpKernelsMatchedError;
class MemoryZoneOutOfMemoryError;
class LossBlobNotFoundError;
class RwMutexedObjectNotFoundError;
class UnknownError;
class CompileOptionWrongError;
class InputDeviceNotMatchError;
class ErrorStackFrame;
class SymbolIdUninitializedError;
class ValueError;
class IndexError;
class TimeoutError;
class ErrorProto;

namespace cfg {


class FieldValue;
class ConstFieldValue;

class OneFieldAssertError;
class ConstOneFieldAssertError;

class TwoFieldAssertError;
class ConstTwoFieldAssertError;

class ConfigAssertFailedError;
class ConstConfigAssertFailedError;

class ConfigResourceUnavailableError;
class ConstConfigResourceUnavailableError;

class JobSetEmptyError;
class ConstJobSetEmptyError;

class DeviceTagNotFoundError;
class ConstDeviceTagNotFoundError;

class JobNameExistError;
class ConstJobNameExistError;

class JobNameEmptyError;
class ConstJobNameEmptyError;

class JobNameNotEqualError;
class ConstJobNameNotEqualError;

class NoJobBuildAndInferCtxError;
class ConstNoJobBuildAndInferCtxError;

class JobConfFrozenError;
class ConstJobConfFrozenError;

class JobConfNotSetError;
class ConstJobConfNotSetError;

class JobConfRepeatedSetError;
class ConstJobConfRepeatedSetError;

class JobTypeNotSetError;
class ConstJobTypeNotSetError;

class LogicalBlobNameNotExistError;
class ConstLogicalBlobNameNotExistError;

class LogicalBlobNameExistError;
class ConstLogicalBlobNameExistError;

class LogicalBlobNameInvalidError;
class ConstLogicalBlobNameInvalidError;

class OpNameExistError;
class ConstOpNameExistError;

class OpConfDeviceTagNoSetError;
class ConstOpConfDeviceTagNoSetError;

class PlacementError;
class ConstPlacementError;

class BlobSplitAxisInferError;
class ConstBlobSplitAxisInferError;

class UnknownJobBuildAndInferError;
class ConstUnknownJobBuildAndInferError;

class ProtoParseFailedError;
class ConstProtoParseFailedError;

class CheckFailedError;
class ConstCheckFailedError;

class TodoError;
class ConstTodoError;

class UnimplementedError;
class ConstUnimplementedError;

class BoxingNotSupportedError;
class ConstBoxingNotSupportedError;

class GradientFunctionNotFoundError;
class ConstGradientFunctionNotFoundError;

class OpKernelNotFoundError;
class ConstOpKernelNotFoundError;

class MultipleOpKernelsMatchedError;
class ConstMultipleOpKernelsMatchedError;

class MemoryZoneOutOfMemoryError;
class ConstMemoryZoneOutOfMemoryError;

class LossBlobNotFoundError;
class ConstLossBlobNotFoundError;

class RwMutexedObjectNotFoundError;
class ConstRwMutexedObjectNotFoundError;

class UnknownError;
class ConstUnknownError;

class CompileOptionWrongError;
class ConstCompileOptionWrongError;

class InputDeviceNotMatchError;
class ConstInputDeviceNotMatchError;

class ErrorStackFrame;
class ConstErrorStackFrame;

class SymbolIdUninitializedError;
class ConstSymbolIdUninitializedError;

class ValueError;
class ConstValueError;

class IndexError;
class ConstIndexError;

class TimeoutError;
class ConstTimeoutError;

class ErrorProto;
class ConstErrorProto;

enum OpcodeType : unsigned int {
  kInvalidCompareType = 0,
  kEq = 1,
  kNe = 2,
  kGt = 3,
  kGe = 4,
  kLt = 5,
  kLe = 6,
};

inline ::std::string OpcodeType_Name(OpcodeType value) {
  switch (value) {
  case kInvalidCompareType: { return "kInvalidCompareType"; }
  case kEq: { return "kEq"; }
  case kNe: { return "kNe"; }
  case kGt: { return "kGt"; }
  case kGe: { return "kGe"; }
  case kLt: { return "kLt"; }
  case kLe: { return "kLe"; }
  default:
    return "";
  }
}



class ConstFieldValue : public ::oneflow::cfg::Message {
 public:

  class _FieldValue_ {
   public:
    _FieldValue_();
    explicit _FieldValue_(const _FieldValue_& other);
    explicit _FieldValue_(_FieldValue_&& other);
    _FieldValue_(const ::oneflow::FieldValue& proto_fieldvalue);
    ~_FieldValue_();

    void InitFromProto(const ::oneflow::FieldValue& proto_fieldvalue);

    void ToProto(::oneflow::FieldValue* proto_fieldvalue) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FieldValue_& other);
  
      // optional field field
     public:
    bool has_field() const;
    const ::std::string& field() const;
    void clear_field();
    void set_field(const ::std::string& value);
    ::std::string* mutable_field();
   protected:
    bool has_field_ = false;
    ::std::string field_;
      
      // optional field value
     public:
    bool has_value() const;
    const ::std::string& value() const;
    void clear_value();
    void set_value(const ::std::string& value);
    ::std::string* mutable_value();
   protected:
    bool has_value_ = false;
    ::std::string value_;
           
   public:
    int compare(const _FieldValue_& other);

    bool operator==(const _FieldValue_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FieldValue_& other) const;
  };

  ConstFieldValue(const ::std::shared_ptr<_FieldValue_>& data);
  ConstFieldValue(const ConstFieldValue&);
  ConstFieldValue(ConstFieldValue&&) noexcept;
  ConstFieldValue();
  ConstFieldValue(const ::oneflow::FieldValue& proto_fieldvalue);
  virtual ~ConstFieldValue() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_fieldvalue) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field field
 public:
  bool has_field() const;
  const ::std::string& field() const;
  // used by pybind11 only
  // required or optional field value
 public:
  bool has_value() const;
  const ::std::string& value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstFieldValue> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFieldValue& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFieldValue& other) const;
 protected:
  const ::std::shared_ptr<_FieldValue_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FieldValue_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFieldValue
  void BuildFromProto(const PbMessage& proto_fieldvalue);
  
  ::std::shared_ptr<_FieldValue_> data_;
};

class FieldValue final : public ConstFieldValue {
 public:
  FieldValue(const ::std::shared_ptr<_FieldValue_>& data);
  FieldValue(const FieldValue& other);
  // enable nothrow for ::std::vector<FieldValue> resize 
  FieldValue(FieldValue&&) noexcept;
  FieldValue();
  explicit FieldValue(const ::oneflow::FieldValue& proto_fieldvalue);

  ~FieldValue() override;

  void InitFromProto(const PbMessage& proto_fieldvalue) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FieldValue& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FieldValue& other) const;
  void Clear();
  void CopyFrom(const FieldValue& other);
  FieldValue& operator=(const FieldValue& other);

  // required or optional field field
 public:
  void clear_field();
  void set_field(const ::std::string& value);
  ::std::string* mutable_field();
  // required or optional field value
 public:
  void clear_value();
  void set_value(const ::std::string& value);
  ::std::string* mutable_value();

  ::std::shared_ptr<FieldValue> __SharedMutable__();
};


class ConstOneFieldAssertError : public ::oneflow::cfg::Message {
 public:

  class _OneFieldAssertError_ {
   public:
    _OneFieldAssertError_();
    explicit _OneFieldAssertError_(const _OneFieldAssertError_& other);
    explicit _OneFieldAssertError_(_OneFieldAssertError_&& other);
    _OneFieldAssertError_(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror);
    ~_OneFieldAssertError_();

    void InitFromProto(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror);

    void ToProto(::oneflow::OneFieldAssertError* proto_onefieldasserterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OneFieldAssertError_& other);
  
      // optional field compare_type
     public:
    bool has_compare_type() const;
    const ::oneflow::cfg::OpcodeType& compare_type() const;
    void clear_compare_type();
    void set_compare_type(const ::oneflow::cfg::OpcodeType& value);
    ::oneflow::cfg::OpcodeType* mutable_compare_type();
   protected:
    bool has_compare_type_ = false;
    ::oneflow::cfg::OpcodeType compare_type_;
      
      // optional field left
     public:
    bool has_left() const;
    const ::oneflow::cfg::FieldValue& left() const;
    void clear_left();
    ::oneflow::cfg::FieldValue* mutable_left();
   protected:
    bool has_left_ = false;
    ::std::shared_ptr<::oneflow::cfg::FieldValue> left_;
      
      // optional field right_value
     public:
    bool has_right_value() const;
    const ::std::string& right_value() const;
    void clear_right_value();
    void set_right_value(const ::std::string& value);
    ::std::string* mutable_right_value();
   protected:
    bool has_right_value_ = false;
    ::std::string right_value_;
           
   public:
    int compare(const _OneFieldAssertError_& other);

    bool operator==(const _OneFieldAssertError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OneFieldAssertError_& other) const;
  };

  ConstOneFieldAssertError(const ::std::shared_ptr<_OneFieldAssertError_>& data);
  ConstOneFieldAssertError(const ConstOneFieldAssertError&);
  ConstOneFieldAssertError(ConstOneFieldAssertError&&) noexcept;
  ConstOneFieldAssertError();
  ConstOneFieldAssertError(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror);
  virtual ~ConstOneFieldAssertError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_onefieldasserterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field compare_type
 public:
  bool has_compare_type() const;
  const ::oneflow::cfg::OpcodeType& compare_type() const;
  // used by pybind11 only
  // required or optional field left
 public:
  bool has_left() const;
  const ::oneflow::cfg::FieldValue& left() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFieldValue> shared_const_left() const;
  // required or optional field right_value
 public:
  bool has_right_value() const;
  const ::std::string& right_value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstOneFieldAssertError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOneFieldAssertError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOneFieldAssertError& other) const;
 protected:
  const ::std::shared_ptr<_OneFieldAssertError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OneFieldAssertError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOneFieldAssertError
  void BuildFromProto(const PbMessage& proto_onefieldasserterror);
  
  ::std::shared_ptr<_OneFieldAssertError_> data_;
};

class OneFieldAssertError final : public ConstOneFieldAssertError {
 public:
  OneFieldAssertError(const ::std::shared_ptr<_OneFieldAssertError_>& data);
  OneFieldAssertError(const OneFieldAssertError& other);
  // enable nothrow for ::std::vector<OneFieldAssertError> resize 
  OneFieldAssertError(OneFieldAssertError&&) noexcept;
  OneFieldAssertError();
  explicit OneFieldAssertError(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror);

  ~OneFieldAssertError() override;

  void InitFromProto(const PbMessage& proto_onefieldasserterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OneFieldAssertError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OneFieldAssertError& other) const;
  void Clear();
  void CopyFrom(const OneFieldAssertError& other);
  OneFieldAssertError& operator=(const OneFieldAssertError& other);

  // required or optional field compare_type
 public:
  void clear_compare_type();
  void set_compare_type(const ::oneflow::cfg::OpcodeType& value);
  ::oneflow::cfg::OpcodeType* mutable_compare_type();
  // required or optional field left
 public:
  void clear_left();
  ::oneflow::cfg::FieldValue* mutable_left();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FieldValue> shared_mutable_left();
  // required or optional field right_value
 public:
  void clear_right_value();
  void set_right_value(const ::std::string& value);
  ::std::string* mutable_right_value();

  ::std::shared_ptr<OneFieldAssertError> __SharedMutable__();
};


class ConstTwoFieldAssertError : public ::oneflow::cfg::Message {
 public:

  class _TwoFieldAssertError_ {
   public:
    _TwoFieldAssertError_();
    explicit _TwoFieldAssertError_(const _TwoFieldAssertError_& other);
    explicit _TwoFieldAssertError_(_TwoFieldAssertError_&& other);
    _TwoFieldAssertError_(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror);
    ~_TwoFieldAssertError_();

    void InitFromProto(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror);

    void ToProto(::oneflow::TwoFieldAssertError* proto_twofieldasserterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TwoFieldAssertError_& other);
  
      // optional field compare_type
     public:
    bool has_compare_type() const;
    const ::oneflow::cfg::OpcodeType& compare_type() const;
    void clear_compare_type();
    void set_compare_type(const ::oneflow::cfg::OpcodeType& value);
    ::oneflow::cfg::OpcodeType* mutable_compare_type();
   protected:
    bool has_compare_type_ = false;
    ::oneflow::cfg::OpcodeType compare_type_;
      
      // optional field left
     public:
    bool has_left() const;
    const ::oneflow::cfg::FieldValue& left() const;
    void clear_left();
    ::oneflow::cfg::FieldValue* mutable_left();
   protected:
    bool has_left_ = false;
    ::std::shared_ptr<::oneflow::cfg::FieldValue> left_;
      
      // optional field right
     public:
    bool has_right() const;
    const ::oneflow::cfg::FieldValue& right() const;
    void clear_right();
    ::oneflow::cfg::FieldValue* mutable_right();
   protected:
    bool has_right_ = false;
    ::std::shared_ptr<::oneflow::cfg::FieldValue> right_;
           
   public:
    int compare(const _TwoFieldAssertError_& other);

    bool operator==(const _TwoFieldAssertError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TwoFieldAssertError_& other) const;
  };

  ConstTwoFieldAssertError(const ::std::shared_ptr<_TwoFieldAssertError_>& data);
  ConstTwoFieldAssertError(const ConstTwoFieldAssertError&);
  ConstTwoFieldAssertError(ConstTwoFieldAssertError&&) noexcept;
  ConstTwoFieldAssertError();
  ConstTwoFieldAssertError(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror);
  virtual ~ConstTwoFieldAssertError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_twofieldasserterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field compare_type
 public:
  bool has_compare_type() const;
  const ::oneflow::cfg::OpcodeType& compare_type() const;
  // used by pybind11 only
  // required or optional field left
 public:
  bool has_left() const;
  const ::oneflow::cfg::FieldValue& left() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFieldValue> shared_const_left() const;
  // required or optional field right
 public:
  bool has_right() const;
  const ::oneflow::cfg::FieldValue& right() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFieldValue> shared_const_right() const;

 public:
  ::std::shared_ptr<ConstTwoFieldAssertError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTwoFieldAssertError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTwoFieldAssertError& other) const;
 protected:
  const ::std::shared_ptr<_TwoFieldAssertError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TwoFieldAssertError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTwoFieldAssertError
  void BuildFromProto(const PbMessage& proto_twofieldasserterror);
  
  ::std::shared_ptr<_TwoFieldAssertError_> data_;
};

class TwoFieldAssertError final : public ConstTwoFieldAssertError {
 public:
  TwoFieldAssertError(const ::std::shared_ptr<_TwoFieldAssertError_>& data);
  TwoFieldAssertError(const TwoFieldAssertError& other);
  // enable nothrow for ::std::vector<TwoFieldAssertError> resize 
  TwoFieldAssertError(TwoFieldAssertError&&) noexcept;
  TwoFieldAssertError();
  explicit TwoFieldAssertError(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror);

  ~TwoFieldAssertError() override;

  void InitFromProto(const PbMessage& proto_twofieldasserterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TwoFieldAssertError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TwoFieldAssertError& other) const;
  void Clear();
  void CopyFrom(const TwoFieldAssertError& other);
  TwoFieldAssertError& operator=(const TwoFieldAssertError& other);

  // required or optional field compare_type
 public:
  void clear_compare_type();
  void set_compare_type(const ::oneflow::cfg::OpcodeType& value);
  ::oneflow::cfg::OpcodeType* mutable_compare_type();
  // required or optional field left
 public:
  void clear_left();
  ::oneflow::cfg::FieldValue* mutable_left();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FieldValue> shared_mutable_left();
  // required or optional field right
 public:
  void clear_right();
  ::oneflow::cfg::FieldValue* mutable_right();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FieldValue> shared_mutable_right();

  ::std::shared_ptr<TwoFieldAssertError> __SharedMutable__();
};


class ConstConfigAssertFailedError : public ::oneflow::cfg::Message {
 public:

 // oneof enum oprand_type
 enum OprandTypeCase : unsigned int {
  OPRAND_TYPE_NOT_SET = 0,
    kOneFieldAssertError = 1,
    kTwoFieldAssertError = 2,
   };

  class _ConfigAssertFailedError_ {
   public:
    _ConfigAssertFailedError_();
    explicit _ConfigAssertFailedError_(const _ConfigAssertFailedError_& other);
    explicit _ConfigAssertFailedError_(_ConfigAssertFailedError_&& other);
    _ConfigAssertFailedError_(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror);
    ~_ConfigAssertFailedError_();

    void InitFromProto(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror);

    void ToProto(::oneflow::ConfigAssertFailedError* proto_configassertfailederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConfigAssertFailedError_& other);
  
     // oneof field oprand_type: one_field_assert_error
   public:
    bool has_one_field_assert_error() const;
    void clear_one_field_assert_error();
    const ::oneflow::cfg::OneFieldAssertError& one_field_assert_error() const;
      ::oneflow::cfg::OneFieldAssertError* mutable_one_field_assert_error();
      
     // oneof field oprand_type: two_field_assert_error
   public:
    bool has_two_field_assert_error() const;
    void clear_two_field_assert_error();
    const ::oneflow::cfg::TwoFieldAssertError& two_field_assert_error() const;
      ::oneflow::cfg::TwoFieldAssertError* mutable_two_field_assert_error();
           
   public:
    // oneof oprand_type
    OprandTypeCase oprand_type_case() const;
    bool has_oprand_type() const;
   protected:
    void clear_oprand_type();
    void oprand_type_copy_from(const _ConfigAssertFailedError_& other);
    union OprandTypeUnion {
      // 64-bit aligned
      uint64_t __oprand_type_for_padding_64bit__;
          char one_field_assert_error_[sizeof(::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>)];
            char two_field_assert_error_[sizeof(::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>)];
        } oprand_type_;
    OprandTypeCase oprand_type_case_ = OPRAND_TYPE_NOT_SET;
     
   public:
    int compare(const _ConfigAssertFailedError_& other);

    bool operator==(const _ConfigAssertFailedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConfigAssertFailedError_& other) const;
  };

  ConstConfigAssertFailedError(const ::std::shared_ptr<_ConfigAssertFailedError_>& data);
  ConstConfigAssertFailedError(const ConstConfigAssertFailedError&);
  ConstConfigAssertFailedError(ConstConfigAssertFailedError&&) noexcept;
  ConstConfigAssertFailedError();
  ConstConfigAssertFailedError(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror);
  virtual ~ConstConfigAssertFailedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_configassertfailederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field oprand_type: one_field_assert_error
 public:
  bool has_one_field_assert_error() const;
  const ::oneflow::cfg::OneFieldAssertError& one_field_assert_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOneFieldAssertError> shared_const_one_field_assert_error() const;
 // oneof field oprand_type: two_field_assert_error
 public:
  bool has_two_field_assert_error() const;
  const ::oneflow::cfg::TwoFieldAssertError& two_field_assert_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTwoFieldAssertError> shared_const_two_field_assert_error() const;
 public:
  OprandTypeCase oprand_type_case() const;
 protected:
  bool has_oprand_type() const;

 public:
  ::std::shared_ptr<ConstConfigAssertFailedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConfigAssertFailedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConfigAssertFailedError& other) const;
 protected:
  const ::std::shared_ptr<_ConfigAssertFailedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConfigAssertFailedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConfigAssertFailedError
  void BuildFromProto(const PbMessage& proto_configassertfailederror);
  
  ::std::shared_ptr<_ConfigAssertFailedError_> data_;
};

class ConfigAssertFailedError final : public ConstConfigAssertFailedError {
 public:
  ConfigAssertFailedError(const ::std::shared_ptr<_ConfigAssertFailedError_>& data);
  ConfigAssertFailedError(const ConfigAssertFailedError& other);
  // enable nothrow for ::std::vector<ConfigAssertFailedError> resize 
  ConfigAssertFailedError(ConfigAssertFailedError&&) noexcept;
  ConfigAssertFailedError();
  explicit ConfigAssertFailedError(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror);

  ~ConfigAssertFailedError() override;

  void InitFromProto(const PbMessage& proto_configassertfailederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConfigAssertFailedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConfigAssertFailedError& other) const;
  void Clear();
  void CopyFrom(const ConfigAssertFailedError& other);
  ConfigAssertFailedError& operator=(const ConfigAssertFailedError& other);

  void clear_one_field_assert_error();
  ::oneflow::cfg::OneFieldAssertError* mutable_one_field_assert_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError> shared_mutable_one_field_assert_error();
  void clear_two_field_assert_error();
  ::oneflow::cfg::TwoFieldAssertError* mutable_two_field_assert_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError> shared_mutable_two_field_assert_error();

  ::std::shared_ptr<ConfigAssertFailedError> __SharedMutable__();
};


class ConstConfigResourceUnavailableError : public ::oneflow::cfg::Message {
 public:

  class _ConfigResourceUnavailableError_ {
   public:
    _ConfigResourceUnavailableError_();
    explicit _ConfigResourceUnavailableError_(const _ConfigResourceUnavailableError_& other);
    explicit _ConfigResourceUnavailableError_(_ConfigResourceUnavailableError_&& other);
    _ConfigResourceUnavailableError_(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror);
    ~_ConfigResourceUnavailableError_();

    void InitFromProto(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror);

    void ToProto(::oneflow::ConfigResourceUnavailableError* proto_configresourceunavailableerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConfigResourceUnavailableError_& other);
  
      // optional field field_value
     public:
    bool has_field_value() const;
    const ::oneflow::cfg::FieldValue& field_value() const;
    void clear_field_value();
    ::oneflow::cfg::FieldValue* mutable_field_value();
   protected:
    bool has_field_value_ = false;
    ::std::shared_ptr<::oneflow::cfg::FieldValue> field_value_;
           
   public:
    int compare(const _ConfigResourceUnavailableError_& other);

    bool operator==(const _ConfigResourceUnavailableError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConfigResourceUnavailableError_& other) const;
  };

  ConstConfigResourceUnavailableError(const ::std::shared_ptr<_ConfigResourceUnavailableError_>& data);
  ConstConfigResourceUnavailableError(const ConstConfigResourceUnavailableError&);
  ConstConfigResourceUnavailableError(ConstConfigResourceUnavailableError&&) noexcept;
  ConstConfigResourceUnavailableError();
  ConstConfigResourceUnavailableError(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror);
  virtual ~ConstConfigResourceUnavailableError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_configresourceunavailableerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field field_value
 public:
  bool has_field_value() const;
  const ::oneflow::cfg::FieldValue& field_value() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFieldValue> shared_const_field_value() const;

 public:
  ::std::shared_ptr<ConstConfigResourceUnavailableError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConfigResourceUnavailableError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConfigResourceUnavailableError& other) const;
 protected:
  const ::std::shared_ptr<_ConfigResourceUnavailableError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConfigResourceUnavailableError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConfigResourceUnavailableError
  void BuildFromProto(const PbMessage& proto_configresourceunavailableerror);
  
  ::std::shared_ptr<_ConfigResourceUnavailableError_> data_;
};

class ConfigResourceUnavailableError final : public ConstConfigResourceUnavailableError {
 public:
  ConfigResourceUnavailableError(const ::std::shared_ptr<_ConfigResourceUnavailableError_>& data);
  ConfigResourceUnavailableError(const ConfigResourceUnavailableError& other);
  // enable nothrow for ::std::vector<ConfigResourceUnavailableError> resize 
  ConfigResourceUnavailableError(ConfigResourceUnavailableError&&) noexcept;
  ConfigResourceUnavailableError();
  explicit ConfigResourceUnavailableError(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror);

  ~ConfigResourceUnavailableError() override;

  void InitFromProto(const PbMessage& proto_configresourceunavailableerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConfigResourceUnavailableError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConfigResourceUnavailableError& other) const;
  void Clear();
  void CopyFrom(const ConfigResourceUnavailableError& other);
  ConfigResourceUnavailableError& operator=(const ConfigResourceUnavailableError& other);

  // required or optional field field_value
 public:
  void clear_field_value();
  ::oneflow::cfg::FieldValue* mutable_field_value();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FieldValue> shared_mutable_field_value();

  ::std::shared_ptr<ConfigResourceUnavailableError> __SharedMutable__();
};


class ConstJobSetEmptyError : public ::oneflow::cfg::Message {
 public:

  class _JobSetEmptyError_ {
   public:
    _JobSetEmptyError_();
    explicit _JobSetEmptyError_(const _JobSetEmptyError_& other);
    explicit _JobSetEmptyError_(_JobSetEmptyError_&& other);
    _JobSetEmptyError_(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror);
    ~_JobSetEmptyError_();

    void InitFromProto(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror);

    void ToProto(::oneflow::JobSetEmptyError* proto_jobsetemptyerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobSetEmptyError_& other);
       
   public:
    int compare(const _JobSetEmptyError_& other);

    bool operator==(const _JobSetEmptyError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobSetEmptyError_& other) const;
  };

  ConstJobSetEmptyError(const ::std::shared_ptr<_JobSetEmptyError_>& data);
  ConstJobSetEmptyError(const ConstJobSetEmptyError&);
  ConstJobSetEmptyError(ConstJobSetEmptyError&&) noexcept;
  ConstJobSetEmptyError();
  ConstJobSetEmptyError(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror);
  virtual ~ConstJobSetEmptyError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobsetemptyerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobSetEmptyError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobSetEmptyError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobSetEmptyError& other) const;
 protected:
  const ::std::shared_ptr<_JobSetEmptyError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobSetEmptyError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobSetEmptyError
  void BuildFromProto(const PbMessage& proto_jobsetemptyerror);
  
  ::std::shared_ptr<_JobSetEmptyError_> data_;
};

class JobSetEmptyError final : public ConstJobSetEmptyError {
 public:
  JobSetEmptyError(const ::std::shared_ptr<_JobSetEmptyError_>& data);
  JobSetEmptyError(const JobSetEmptyError& other);
  // enable nothrow for ::std::vector<JobSetEmptyError> resize 
  JobSetEmptyError(JobSetEmptyError&&) noexcept;
  JobSetEmptyError();
  explicit JobSetEmptyError(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror);

  ~JobSetEmptyError() override;

  void InitFromProto(const PbMessage& proto_jobsetemptyerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobSetEmptyError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobSetEmptyError& other) const;
  void Clear();
  void CopyFrom(const JobSetEmptyError& other);
  JobSetEmptyError& operator=(const JobSetEmptyError& other);


  ::std::shared_ptr<JobSetEmptyError> __SharedMutable__();
};


class ConstDeviceTagNotFoundError : public ::oneflow::cfg::Message {
 public:

  class _DeviceTagNotFoundError_ {
   public:
    _DeviceTagNotFoundError_();
    explicit _DeviceTagNotFoundError_(const _DeviceTagNotFoundError_& other);
    explicit _DeviceTagNotFoundError_(_DeviceTagNotFoundError_&& other);
    _DeviceTagNotFoundError_(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror);
    ~_DeviceTagNotFoundError_();

    void InitFromProto(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror);

    void ToProto(::oneflow::DeviceTagNotFoundError* proto_devicetagnotfounderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DeviceTagNotFoundError_& other);
       
   public:
    int compare(const _DeviceTagNotFoundError_& other);

    bool operator==(const _DeviceTagNotFoundError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DeviceTagNotFoundError_& other) const;
  };

  ConstDeviceTagNotFoundError(const ::std::shared_ptr<_DeviceTagNotFoundError_>& data);
  ConstDeviceTagNotFoundError(const ConstDeviceTagNotFoundError&);
  ConstDeviceTagNotFoundError(ConstDeviceTagNotFoundError&&) noexcept;
  ConstDeviceTagNotFoundError();
  ConstDeviceTagNotFoundError(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror);
  virtual ~ConstDeviceTagNotFoundError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_devicetagnotfounderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstDeviceTagNotFoundError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDeviceTagNotFoundError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDeviceTagNotFoundError& other) const;
 protected:
  const ::std::shared_ptr<_DeviceTagNotFoundError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DeviceTagNotFoundError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDeviceTagNotFoundError
  void BuildFromProto(const PbMessage& proto_devicetagnotfounderror);
  
  ::std::shared_ptr<_DeviceTagNotFoundError_> data_;
};

class DeviceTagNotFoundError final : public ConstDeviceTagNotFoundError {
 public:
  DeviceTagNotFoundError(const ::std::shared_ptr<_DeviceTagNotFoundError_>& data);
  DeviceTagNotFoundError(const DeviceTagNotFoundError& other);
  // enable nothrow for ::std::vector<DeviceTagNotFoundError> resize 
  DeviceTagNotFoundError(DeviceTagNotFoundError&&) noexcept;
  DeviceTagNotFoundError();
  explicit DeviceTagNotFoundError(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror);

  ~DeviceTagNotFoundError() override;

  void InitFromProto(const PbMessage& proto_devicetagnotfounderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DeviceTagNotFoundError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DeviceTagNotFoundError& other) const;
  void Clear();
  void CopyFrom(const DeviceTagNotFoundError& other);
  DeviceTagNotFoundError& operator=(const DeviceTagNotFoundError& other);


  ::std::shared_ptr<DeviceTagNotFoundError> __SharedMutable__();
};


class ConstJobNameExistError : public ::oneflow::cfg::Message {
 public:

  class _JobNameExistError_ {
   public:
    _JobNameExistError_();
    explicit _JobNameExistError_(const _JobNameExistError_& other);
    explicit _JobNameExistError_(_JobNameExistError_&& other);
    _JobNameExistError_(const ::oneflow::JobNameExistError& proto_jobnameexisterror);
    ~_JobNameExistError_();

    void InitFromProto(const ::oneflow::JobNameExistError& proto_jobnameexisterror);

    void ToProto(::oneflow::JobNameExistError* proto_jobnameexisterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobNameExistError_& other);
       
   public:
    int compare(const _JobNameExistError_& other);

    bool operator==(const _JobNameExistError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobNameExistError_& other) const;
  };

  ConstJobNameExistError(const ::std::shared_ptr<_JobNameExistError_>& data);
  ConstJobNameExistError(const ConstJobNameExistError&);
  ConstJobNameExistError(ConstJobNameExistError&&) noexcept;
  ConstJobNameExistError();
  ConstJobNameExistError(const ::oneflow::JobNameExistError& proto_jobnameexisterror);
  virtual ~ConstJobNameExistError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobnameexisterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobNameExistError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobNameExistError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobNameExistError& other) const;
 protected:
  const ::std::shared_ptr<_JobNameExistError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobNameExistError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobNameExistError
  void BuildFromProto(const PbMessage& proto_jobnameexisterror);
  
  ::std::shared_ptr<_JobNameExistError_> data_;
};

class JobNameExistError final : public ConstJobNameExistError {
 public:
  JobNameExistError(const ::std::shared_ptr<_JobNameExistError_>& data);
  JobNameExistError(const JobNameExistError& other);
  // enable nothrow for ::std::vector<JobNameExistError> resize 
  JobNameExistError(JobNameExistError&&) noexcept;
  JobNameExistError();
  explicit JobNameExistError(const ::oneflow::JobNameExistError& proto_jobnameexisterror);

  ~JobNameExistError() override;

  void InitFromProto(const PbMessage& proto_jobnameexisterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobNameExistError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobNameExistError& other) const;
  void Clear();
  void CopyFrom(const JobNameExistError& other);
  JobNameExistError& operator=(const JobNameExistError& other);


  ::std::shared_ptr<JobNameExistError> __SharedMutable__();
};


class ConstJobNameEmptyError : public ::oneflow::cfg::Message {
 public:

  class _JobNameEmptyError_ {
   public:
    _JobNameEmptyError_();
    explicit _JobNameEmptyError_(const _JobNameEmptyError_& other);
    explicit _JobNameEmptyError_(_JobNameEmptyError_&& other);
    _JobNameEmptyError_(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror);
    ~_JobNameEmptyError_();

    void InitFromProto(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror);

    void ToProto(::oneflow::JobNameEmptyError* proto_jobnameemptyerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobNameEmptyError_& other);
       
   public:
    int compare(const _JobNameEmptyError_& other);

    bool operator==(const _JobNameEmptyError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobNameEmptyError_& other) const;
  };

  ConstJobNameEmptyError(const ::std::shared_ptr<_JobNameEmptyError_>& data);
  ConstJobNameEmptyError(const ConstJobNameEmptyError&);
  ConstJobNameEmptyError(ConstJobNameEmptyError&&) noexcept;
  ConstJobNameEmptyError();
  ConstJobNameEmptyError(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror);
  virtual ~ConstJobNameEmptyError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobnameemptyerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobNameEmptyError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobNameEmptyError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobNameEmptyError& other) const;
 protected:
  const ::std::shared_ptr<_JobNameEmptyError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobNameEmptyError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobNameEmptyError
  void BuildFromProto(const PbMessage& proto_jobnameemptyerror);
  
  ::std::shared_ptr<_JobNameEmptyError_> data_;
};

class JobNameEmptyError final : public ConstJobNameEmptyError {
 public:
  JobNameEmptyError(const ::std::shared_ptr<_JobNameEmptyError_>& data);
  JobNameEmptyError(const JobNameEmptyError& other);
  // enable nothrow for ::std::vector<JobNameEmptyError> resize 
  JobNameEmptyError(JobNameEmptyError&&) noexcept;
  JobNameEmptyError();
  explicit JobNameEmptyError(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror);

  ~JobNameEmptyError() override;

  void InitFromProto(const PbMessage& proto_jobnameemptyerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobNameEmptyError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobNameEmptyError& other) const;
  void Clear();
  void CopyFrom(const JobNameEmptyError& other);
  JobNameEmptyError& operator=(const JobNameEmptyError& other);


  ::std::shared_ptr<JobNameEmptyError> __SharedMutable__();
};


class ConstJobNameNotEqualError : public ::oneflow::cfg::Message {
 public:

  class _JobNameNotEqualError_ {
   public:
    _JobNameNotEqualError_();
    explicit _JobNameNotEqualError_(const _JobNameNotEqualError_& other);
    explicit _JobNameNotEqualError_(_JobNameNotEqualError_&& other);
    _JobNameNotEqualError_(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror);
    ~_JobNameNotEqualError_();

    void InitFromProto(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror);

    void ToProto(::oneflow::JobNameNotEqualError* proto_jobnamenotequalerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobNameNotEqualError_& other);
       
   public:
    int compare(const _JobNameNotEqualError_& other);

    bool operator==(const _JobNameNotEqualError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobNameNotEqualError_& other) const;
  };

  ConstJobNameNotEqualError(const ::std::shared_ptr<_JobNameNotEqualError_>& data);
  ConstJobNameNotEqualError(const ConstJobNameNotEqualError&);
  ConstJobNameNotEqualError(ConstJobNameNotEqualError&&) noexcept;
  ConstJobNameNotEqualError();
  ConstJobNameNotEqualError(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror);
  virtual ~ConstJobNameNotEqualError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobnamenotequalerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobNameNotEqualError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobNameNotEqualError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobNameNotEqualError& other) const;
 protected:
  const ::std::shared_ptr<_JobNameNotEqualError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobNameNotEqualError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobNameNotEqualError
  void BuildFromProto(const PbMessage& proto_jobnamenotequalerror);
  
  ::std::shared_ptr<_JobNameNotEqualError_> data_;
};

class JobNameNotEqualError final : public ConstJobNameNotEqualError {
 public:
  JobNameNotEqualError(const ::std::shared_ptr<_JobNameNotEqualError_>& data);
  JobNameNotEqualError(const JobNameNotEqualError& other);
  // enable nothrow for ::std::vector<JobNameNotEqualError> resize 
  JobNameNotEqualError(JobNameNotEqualError&&) noexcept;
  JobNameNotEqualError();
  explicit JobNameNotEqualError(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror);

  ~JobNameNotEqualError() override;

  void InitFromProto(const PbMessage& proto_jobnamenotequalerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobNameNotEqualError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobNameNotEqualError& other) const;
  void Clear();
  void CopyFrom(const JobNameNotEqualError& other);
  JobNameNotEqualError& operator=(const JobNameNotEqualError& other);


  ::std::shared_ptr<JobNameNotEqualError> __SharedMutable__();
};


class ConstNoJobBuildAndInferCtxError : public ::oneflow::cfg::Message {
 public:

  class _NoJobBuildAndInferCtxError_ {
   public:
    _NoJobBuildAndInferCtxError_();
    explicit _NoJobBuildAndInferCtxError_(const _NoJobBuildAndInferCtxError_& other);
    explicit _NoJobBuildAndInferCtxError_(_NoJobBuildAndInferCtxError_&& other);
    _NoJobBuildAndInferCtxError_(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror);
    ~_NoJobBuildAndInferCtxError_();

    void InitFromProto(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror);

    void ToProto(::oneflow::NoJobBuildAndInferCtxError* proto_nojobbuildandinferctxerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _NoJobBuildAndInferCtxError_& other);
       
   public:
    int compare(const _NoJobBuildAndInferCtxError_& other);

    bool operator==(const _NoJobBuildAndInferCtxError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _NoJobBuildAndInferCtxError_& other) const;
  };

  ConstNoJobBuildAndInferCtxError(const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& data);
  ConstNoJobBuildAndInferCtxError(const ConstNoJobBuildAndInferCtxError&);
  ConstNoJobBuildAndInferCtxError(ConstNoJobBuildAndInferCtxError&&) noexcept;
  ConstNoJobBuildAndInferCtxError();
  ConstNoJobBuildAndInferCtxError(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror);
  virtual ~ConstNoJobBuildAndInferCtxError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_nojobbuildandinferctxerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstNoJobBuildAndInferCtxError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstNoJobBuildAndInferCtxError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstNoJobBuildAndInferCtxError& other) const;
 protected:
  const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstNoJobBuildAndInferCtxError
  void BuildFromProto(const PbMessage& proto_nojobbuildandinferctxerror);
  
  ::std::shared_ptr<_NoJobBuildAndInferCtxError_> data_;
};

class NoJobBuildAndInferCtxError final : public ConstNoJobBuildAndInferCtxError {
 public:
  NoJobBuildAndInferCtxError(const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& data);
  NoJobBuildAndInferCtxError(const NoJobBuildAndInferCtxError& other);
  // enable nothrow for ::std::vector<NoJobBuildAndInferCtxError> resize 
  NoJobBuildAndInferCtxError(NoJobBuildAndInferCtxError&&) noexcept;
  NoJobBuildAndInferCtxError();
  explicit NoJobBuildAndInferCtxError(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror);

  ~NoJobBuildAndInferCtxError() override;

  void InitFromProto(const PbMessage& proto_nojobbuildandinferctxerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const NoJobBuildAndInferCtxError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const NoJobBuildAndInferCtxError& other) const;
  void Clear();
  void CopyFrom(const NoJobBuildAndInferCtxError& other);
  NoJobBuildAndInferCtxError& operator=(const NoJobBuildAndInferCtxError& other);


  ::std::shared_ptr<NoJobBuildAndInferCtxError> __SharedMutable__();
};


class ConstJobConfFrozenError : public ::oneflow::cfg::Message {
 public:

  class _JobConfFrozenError_ {
   public:
    _JobConfFrozenError_();
    explicit _JobConfFrozenError_(const _JobConfFrozenError_& other);
    explicit _JobConfFrozenError_(_JobConfFrozenError_&& other);
    _JobConfFrozenError_(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror);
    ~_JobConfFrozenError_();

    void InitFromProto(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror);

    void ToProto(::oneflow::JobConfFrozenError* proto_jobconffrozenerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobConfFrozenError_& other);
       
   public:
    int compare(const _JobConfFrozenError_& other);

    bool operator==(const _JobConfFrozenError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobConfFrozenError_& other) const;
  };

  ConstJobConfFrozenError(const ::std::shared_ptr<_JobConfFrozenError_>& data);
  ConstJobConfFrozenError(const ConstJobConfFrozenError&);
  ConstJobConfFrozenError(ConstJobConfFrozenError&&) noexcept;
  ConstJobConfFrozenError();
  ConstJobConfFrozenError(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror);
  virtual ~ConstJobConfFrozenError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobconffrozenerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobConfFrozenError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobConfFrozenError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobConfFrozenError& other) const;
 protected:
  const ::std::shared_ptr<_JobConfFrozenError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobConfFrozenError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobConfFrozenError
  void BuildFromProto(const PbMessage& proto_jobconffrozenerror);
  
  ::std::shared_ptr<_JobConfFrozenError_> data_;
};

class JobConfFrozenError final : public ConstJobConfFrozenError {
 public:
  JobConfFrozenError(const ::std::shared_ptr<_JobConfFrozenError_>& data);
  JobConfFrozenError(const JobConfFrozenError& other);
  // enable nothrow for ::std::vector<JobConfFrozenError> resize 
  JobConfFrozenError(JobConfFrozenError&&) noexcept;
  JobConfFrozenError();
  explicit JobConfFrozenError(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror);

  ~JobConfFrozenError() override;

  void InitFromProto(const PbMessage& proto_jobconffrozenerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobConfFrozenError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobConfFrozenError& other) const;
  void Clear();
  void CopyFrom(const JobConfFrozenError& other);
  JobConfFrozenError& operator=(const JobConfFrozenError& other);


  ::std::shared_ptr<JobConfFrozenError> __SharedMutable__();
};


class ConstJobConfNotSetError : public ::oneflow::cfg::Message {
 public:

  class _JobConfNotSetError_ {
   public:
    _JobConfNotSetError_();
    explicit _JobConfNotSetError_(const _JobConfNotSetError_& other);
    explicit _JobConfNotSetError_(_JobConfNotSetError_&& other);
    _JobConfNotSetError_(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror);
    ~_JobConfNotSetError_();

    void InitFromProto(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror);

    void ToProto(::oneflow::JobConfNotSetError* proto_jobconfnotseterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobConfNotSetError_& other);
       
   public:
    int compare(const _JobConfNotSetError_& other);

    bool operator==(const _JobConfNotSetError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobConfNotSetError_& other) const;
  };

  ConstJobConfNotSetError(const ::std::shared_ptr<_JobConfNotSetError_>& data);
  ConstJobConfNotSetError(const ConstJobConfNotSetError&);
  ConstJobConfNotSetError(ConstJobConfNotSetError&&) noexcept;
  ConstJobConfNotSetError();
  ConstJobConfNotSetError(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror);
  virtual ~ConstJobConfNotSetError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobconfnotseterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobConfNotSetError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobConfNotSetError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobConfNotSetError& other) const;
 protected:
  const ::std::shared_ptr<_JobConfNotSetError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobConfNotSetError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobConfNotSetError
  void BuildFromProto(const PbMessage& proto_jobconfnotseterror);
  
  ::std::shared_ptr<_JobConfNotSetError_> data_;
};

class JobConfNotSetError final : public ConstJobConfNotSetError {
 public:
  JobConfNotSetError(const ::std::shared_ptr<_JobConfNotSetError_>& data);
  JobConfNotSetError(const JobConfNotSetError& other);
  // enable nothrow for ::std::vector<JobConfNotSetError> resize 
  JobConfNotSetError(JobConfNotSetError&&) noexcept;
  JobConfNotSetError();
  explicit JobConfNotSetError(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror);

  ~JobConfNotSetError() override;

  void InitFromProto(const PbMessage& proto_jobconfnotseterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobConfNotSetError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobConfNotSetError& other) const;
  void Clear();
  void CopyFrom(const JobConfNotSetError& other);
  JobConfNotSetError& operator=(const JobConfNotSetError& other);


  ::std::shared_ptr<JobConfNotSetError> __SharedMutable__();
};


class ConstJobConfRepeatedSetError : public ::oneflow::cfg::Message {
 public:

  class _JobConfRepeatedSetError_ {
   public:
    _JobConfRepeatedSetError_();
    explicit _JobConfRepeatedSetError_(const _JobConfRepeatedSetError_& other);
    explicit _JobConfRepeatedSetError_(_JobConfRepeatedSetError_&& other);
    _JobConfRepeatedSetError_(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror);
    ~_JobConfRepeatedSetError_();

    void InitFromProto(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror);

    void ToProto(::oneflow::JobConfRepeatedSetError* proto_jobconfrepeatedseterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobConfRepeatedSetError_& other);
       
   public:
    int compare(const _JobConfRepeatedSetError_& other);

    bool operator==(const _JobConfRepeatedSetError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobConfRepeatedSetError_& other) const;
  };

  ConstJobConfRepeatedSetError(const ::std::shared_ptr<_JobConfRepeatedSetError_>& data);
  ConstJobConfRepeatedSetError(const ConstJobConfRepeatedSetError&);
  ConstJobConfRepeatedSetError(ConstJobConfRepeatedSetError&&) noexcept;
  ConstJobConfRepeatedSetError();
  ConstJobConfRepeatedSetError(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror);
  virtual ~ConstJobConfRepeatedSetError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobconfrepeatedseterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobConfRepeatedSetError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobConfRepeatedSetError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobConfRepeatedSetError& other) const;
 protected:
  const ::std::shared_ptr<_JobConfRepeatedSetError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobConfRepeatedSetError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobConfRepeatedSetError
  void BuildFromProto(const PbMessage& proto_jobconfrepeatedseterror);
  
  ::std::shared_ptr<_JobConfRepeatedSetError_> data_;
};

class JobConfRepeatedSetError final : public ConstJobConfRepeatedSetError {
 public:
  JobConfRepeatedSetError(const ::std::shared_ptr<_JobConfRepeatedSetError_>& data);
  JobConfRepeatedSetError(const JobConfRepeatedSetError& other);
  // enable nothrow for ::std::vector<JobConfRepeatedSetError> resize 
  JobConfRepeatedSetError(JobConfRepeatedSetError&&) noexcept;
  JobConfRepeatedSetError();
  explicit JobConfRepeatedSetError(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror);

  ~JobConfRepeatedSetError() override;

  void InitFromProto(const PbMessage& proto_jobconfrepeatedseterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobConfRepeatedSetError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobConfRepeatedSetError& other) const;
  void Clear();
  void CopyFrom(const JobConfRepeatedSetError& other);
  JobConfRepeatedSetError& operator=(const JobConfRepeatedSetError& other);


  ::std::shared_ptr<JobConfRepeatedSetError> __SharedMutable__();
};


class ConstJobTypeNotSetError : public ::oneflow::cfg::Message {
 public:

  class _JobTypeNotSetError_ {
   public:
    _JobTypeNotSetError_();
    explicit _JobTypeNotSetError_(const _JobTypeNotSetError_& other);
    explicit _JobTypeNotSetError_(_JobTypeNotSetError_&& other);
    _JobTypeNotSetError_(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror);
    ~_JobTypeNotSetError_();

    void InitFromProto(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror);

    void ToProto(::oneflow::JobTypeNotSetError* proto_jobtypenotseterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobTypeNotSetError_& other);
       
   public:
    int compare(const _JobTypeNotSetError_& other);

    bool operator==(const _JobTypeNotSetError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobTypeNotSetError_& other) const;
  };

  ConstJobTypeNotSetError(const ::std::shared_ptr<_JobTypeNotSetError_>& data);
  ConstJobTypeNotSetError(const ConstJobTypeNotSetError&);
  ConstJobTypeNotSetError(ConstJobTypeNotSetError&&) noexcept;
  ConstJobTypeNotSetError();
  ConstJobTypeNotSetError(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror);
  virtual ~ConstJobTypeNotSetError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobtypenotseterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstJobTypeNotSetError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobTypeNotSetError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobTypeNotSetError& other) const;
 protected:
  const ::std::shared_ptr<_JobTypeNotSetError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobTypeNotSetError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobTypeNotSetError
  void BuildFromProto(const PbMessage& proto_jobtypenotseterror);
  
  ::std::shared_ptr<_JobTypeNotSetError_> data_;
};

class JobTypeNotSetError final : public ConstJobTypeNotSetError {
 public:
  JobTypeNotSetError(const ::std::shared_ptr<_JobTypeNotSetError_>& data);
  JobTypeNotSetError(const JobTypeNotSetError& other);
  // enable nothrow for ::std::vector<JobTypeNotSetError> resize 
  JobTypeNotSetError(JobTypeNotSetError&&) noexcept;
  JobTypeNotSetError();
  explicit JobTypeNotSetError(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror);

  ~JobTypeNotSetError() override;

  void InitFromProto(const PbMessage& proto_jobtypenotseterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobTypeNotSetError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobTypeNotSetError& other) const;
  void Clear();
  void CopyFrom(const JobTypeNotSetError& other);
  JobTypeNotSetError& operator=(const JobTypeNotSetError& other);


  ::std::shared_ptr<JobTypeNotSetError> __SharedMutable__();
};


class ConstLogicalBlobNameNotExistError : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobNameNotExistError_ {
   public:
    _LogicalBlobNameNotExistError_();
    explicit _LogicalBlobNameNotExistError_(const _LogicalBlobNameNotExistError_& other);
    explicit _LogicalBlobNameNotExistError_(_LogicalBlobNameNotExistError_&& other);
    _LogicalBlobNameNotExistError_(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror);
    ~_LogicalBlobNameNotExistError_();

    void InitFromProto(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror);

    void ToProto(::oneflow::LogicalBlobNameNotExistError* proto_logicalblobnamenotexisterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobNameNotExistError_& other);
       
   public:
    int compare(const _LogicalBlobNameNotExistError_& other);

    bool operator==(const _LogicalBlobNameNotExistError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobNameNotExistError_& other) const;
  };

  ConstLogicalBlobNameNotExistError(const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& data);
  ConstLogicalBlobNameNotExistError(const ConstLogicalBlobNameNotExistError&);
  ConstLogicalBlobNameNotExistError(ConstLogicalBlobNameNotExistError&&) noexcept;
  ConstLogicalBlobNameNotExistError();
  ConstLogicalBlobNameNotExistError(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror);
  virtual ~ConstLogicalBlobNameNotExistError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobnamenotexisterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstLogicalBlobNameNotExistError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobNameNotExistError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobNameNotExistError& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameNotExistError
  void BuildFromProto(const PbMessage& proto_logicalblobnamenotexisterror);
  
  ::std::shared_ptr<_LogicalBlobNameNotExistError_> data_;
};

class LogicalBlobNameNotExistError final : public ConstLogicalBlobNameNotExistError {
 public:
  LogicalBlobNameNotExistError(const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& data);
  LogicalBlobNameNotExistError(const LogicalBlobNameNotExistError& other);
  // enable nothrow for ::std::vector<LogicalBlobNameNotExistError> resize 
  LogicalBlobNameNotExistError(LogicalBlobNameNotExistError&&) noexcept;
  LogicalBlobNameNotExistError();
  explicit LogicalBlobNameNotExistError(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror);

  ~LogicalBlobNameNotExistError() override;

  void InitFromProto(const PbMessage& proto_logicalblobnamenotexisterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobNameNotExistError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobNameNotExistError& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobNameNotExistError& other);
  LogicalBlobNameNotExistError& operator=(const LogicalBlobNameNotExistError& other);


  ::std::shared_ptr<LogicalBlobNameNotExistError> __SharedMutable__();
};


class ConstLogicalBlobNameExistError : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobNameExistError_ {
   public:
    _LogicalBlobNameExistError_();
    explicit _LogicalBlobNameExistError_(const _LogicalBlobNameExistError_& other);
    explicit _LogicalBlobNameExistError_(_LogicalBlobNameExistError_&& other);
    _LogicalBlobNameExistError_(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror);
    ~_LogicalBlobNameExistError_();

    void InitFromProto(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror);

    void ToProto(::oneflow::LogicalBlobNameExistError* proto_logicalblobnameexisterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobNameExistError_& other);
       
   public:
    int compare(const _LogicalBlobNameExistError_& other);

    bool operator==(const _LogicalBlobNameExistError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobNameExistError_& other) const;
  };

  ConstLogicalBlobNameExistError(const ::std::shared_ptr<_LogicalBlobNameExistError_>& data);
  ConstLogicalBlobNameExistError(const ConstLogicalBlobNameExistError&);
  ConstLogicalBlobNameExistError(ConstLogicalBlobNameExistError&&) noexcept;
  ConstLogicalBlobNameExistError();
  ConstLogicalBlobNameExistError(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror);
  virtual ~ConstLogicalBlobNameExistError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobnameexisterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstLogicalBlobNameExistError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobNameExistError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobNameExistError& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobNameExistError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobNameExistError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameExistError
  void BuildFromProto(const PbMessage& proto_logicalblobnameexisterror);
  
  ::std::shared_ptr<_LogicalBlobNameExistError_> data_;
};

class LogicalBlobNameExistError final : public ConstLogicalBlobNameExistError {
 public:
  LogicalBlobNameExistError(const ::std::shared_ptr<_LogicalBlobNameExistError_>& data);
  LogicalBlobNameExistError(const LogicalBlobNameExistError& other);
  // enable nothrow for ::std::vector<LogicalBlobNameExistError> resize 
  LogicalBlobNameExistError(LogicalBlobNameExistError&&) noexcept;
  LogicalBlobNameExistError();
  explicit LogicalBlobNameExistError(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror);

  ~LogicalBlobNameExistError() override;

  void InitFromProto(const PbMessage& proto_logicalblobnameexisterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobNameExistError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobNameExistError& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobNameExistError& other);
  LogicalBlobNameExistError& operator=(const LogicalBlobNameExistError& other);


  ::std::shared_ptr<LogicalBlobNameExistError> __SharedMutable__();
};


class ConstLogicalBlobNameInvalidError : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobNameInvalidError_ {
   public:
    _LogicalBlobNameInvalidError_();
    explicit _LogicalBlobNameInvalidError_(const _LogicalBlobNameInvalidError_& other);
    explicit _LogicalBlobNameInvalidError_(_LogicalBlobNameInvalidError_&& other);
    _LogicalBlobNameInvalidError_(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror);
    ~_LogicalBlobNameInvalidError_();

    void InitFromProto(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror);

    void ToProto(::oneflow::LogicalBlobNameInvalidError* proto_logicalblobnameinvaliderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobNameInvalidError_& other);
       
   public:
    int compare(const _LogicalBlobNameInvalidError_& other);

    bool operator==(const _LogicalBlobNameInvalidError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobNameInvalidError_& other) const;
  };

  ConstLogicalBlobNameInvalidError(const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& data);
  ConstLogicalBlobNameInvalidError(const ConstLogicalBlobNameInvalidError&);
  ConstLogicalBlobNameInvalidError(ConstLogicalBlobNameInvalidError&&) noexcept;
  ConstLogicalBlobNameInvalidError();
  ConstLogicalBlobNameInvalidError(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror);
  virtual ~ConstLogicalBlobNameInvalidError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobnameinvaliderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstLogicalBlobNameInvalidError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobNameInvalidError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobNameInvalidError& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameInvalidError
  void BuildFromProto(const PbMessage& proto_logicalblobnameinvaliderror);
  
  ::std::shared_ptr<_LogicalBlobNameInvalidError_> data_;
};

class LogicalBlobNameInvalidError final : public ConstLogicalBlobNameInvalidError {
 public:
  LogicalBlobNameInvalidError(const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& data);
  LogicalBlobNameInvalidError(const LogicalBlobNameInvalidError& other);
  // enable nothrow for ::std::vector<LogicalBlobNameInvalidError> resize 
  LogicalBlobNameInvalidError(LogicalBlobNameInvalidError&&) noexcept;
  LogicalBlobNameInvalidError();
  explicit LogicalBlobNameInvalidError(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror);

  ~LogicalBlobNameInvalidError() override;

  void InitFromProto(const PbMessage& proto_logicalblobnameinvaliderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobNameInvalidError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobNameInvalidError& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobNameInvalidError& other);
  LogicalBlobNameInvalidError& operator=(const LogicalBlobNameInvalidError& other);


  ::std::shared_ptr<LogicalBlobNameInvalidError> __SharedMutable__();
};


class ConstOpNameExistError : public ::oneflow::cfg::Message {
 public:

  class _OpNameExistError_ {
   public:
    _OpNameExistError_();
    explicit _OpNameExistError_(const _OpNameExistError_& other);
    explicit _OpNameExistError_(_OpNameExistError_&& other);
    _OpNameExistError_(const ::oneflow::OpNameExistError& proto_opnameexisterror);
    ~_OpNameExistError_();

    void InitFromProto(const ::oneflow::OpNameExistError& proto_opnameexisterror);

    void ToProto(::oneflow::OpNameExistError* proto_opnameexisterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNameExistError_& other);
       
   public:
    int compare(const _OpNameExistError_& other);

    bool operator==(const _OpNameExistError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNameExistError_& other) const;
  };

  ConstOpNameExistError(const ::std::shared_ptr<_OpNameExistError_>& data);
  ConstOpNameExistError(const ConstOpNameExistError&);
  ConstOpNameExistError(ConstOpNameExistError&&) noexcept;
  ConstOpNameExistError();
  ConstOpNameExistError(const ::oneflow::OpNameExistError& proto_opnameexisterror);
  virtual ~ConstOpNameExistError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnameexisterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstOpNameExistError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNameExistError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNameExistError& other) const;
 protected:
  const ::std::shared_ptr<_OpNameExistError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNameExistError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNameExistError
  void BuildFromProto(const PbMessage& proto_opnameexisterror);
  
  ::std::shared_ptr<_OpNameExistError_> data_;
};

class OpNameExistError final : public ConstOpNameExistError {
 public:
  OpNameExistError(const ::std::shared_ptr<_OpNameExistError_>& data);
  OpNameExistError(const OpNameExistError& other);
  // enable nothrow for ::std::vector<OpNameExistError> resize 
  OpNameExistError(OpNameExistError&&) noexcept;
  OpNameExistError();
  explicit OpNameExistError(const ::oneflow::OpNameExistError& proto_opnameexisterror);

  ~OpNameExistError() override;

  void InitFromProto(const PbMessage& proto_opnameexisterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNameExistError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNameExistError& other) const;
  void Clear();
  void CopyFrom(const OpNameExistError& other);
  OpNameExistError& operator=(const OpNameExistError& other);


  ::std::shared_ptr<OpNameExistError> __SharedMutable__();
};


class ConstOpConfDeviceTagNoSetError : public ::oneflow::cfg::Message {
 public:

  class _OpConfDeviceTagNoSetError_ {
   public:
    _OpConfDeviceTagNoSetError_();
    explicit _OpConfDeviceTagNoSetError_(const _OpConfDeviceTagNoSetError_& other);
    explicit _OpConfDeviceTagNoSetError_(_OpConfDeviceTagNoSetError_&& other);
    _OpConfDeviceTagNoSetError_(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror);
    ~_OpConfDeviceTagNoSetError_();

    void InitFromProto(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror);

    void ToProto(::oneflow::OpConfDeviceTagNoSetError* proto_opconfdevicetagnoseterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpConfDeviceTagNoSetError_& other);
       
   public:
    int compare(const _OpConfDeviceTagNoSetError_& other);

    bool operator==(const _OpConfDeviceTagNoSetError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpConfDeviceTagNoSetError_& other) const;
  };

  ConstOpConfDeviceTagNoSetError(const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& data);
  ConstOpConfDeviceTagNoSetError(const ConstOpConfDeviceTagNoSetError&);
  ConstOpConfDeviceTagNoSetError(ConstOpConfDeviceTagNoSetError&&) noexcept;
  ConstOpConfDeviceTagNoSetError();
  ConstOpConfDeviceTagNoSetError(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror);
  virtual ~ConstOpConfDeviceTagNoSetError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opconfdevicetagnoseterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstOpConfDeviceTagNoSetError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpConfDeviceTagNoSetError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpConfDeviceTagNoSetError& other) const;
 protected:
  const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpConfDeviceTagNoSetError
  void BuildFromProto(const PbMessage& proto_opconfdevicetagnoseterror);
  
  ::std::shared_ptr<_OpConfDeviceTagNoSetError_> data_;
};

class OpConfDeviceTagNoSetError final : public ConstOpConfDeviceTagNoSetError {
 public:
  OpConfDeviceTagNoSetError(const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& data);
  OpConfDeviceTagNoSetError(const OpConfDeviceTagNoSetError& other);
  // enable nothrow for ::std::vector<OpConfDeviceTagNoSetError> resize 
  OpConfDeviceTagNoSetError(OpConfDeviceTagNoSetError&&) noexcept;
  OpConfDeviceTagNoSetError();
  explicit OpConfDeviceTagNoSetError(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror);

  ~OpConfDeviceTagNoSetError() override;

  void InitFromProto(const PbMessage& proto_opconfdevicetagnoseterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpConfDeviceTagNoSetError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpConfDeviceTagNoSetError& other) const;
  void Clear();
  void CopyFrom(const OpConfDeviceTagNoSetError& other);
  OpConfDeviceTagNoSetError& operator=(const OpConfDeviceTagNoSetError& other);


  ::std::shared_ptr<OpConfDeviceTagNoSetError> __SharedMutable__();
};


class ConstPlacementError : public ::oneflow::cfg::Message {
 public:

  class _PlacementError_ {
   public:
    _PlacementError_();
    explicit _PlacementError_(const _PlacementError_& other);
    explicit _PlacementError_(_PlacementError_&& other);
    _PlacementError_(const ::oneflow::PlacementError& proto_placementerror);
    ~_PlacementError_();

    void InitFromProto(const ::oneflow::PlacementError& proto_placementerror);

    void ToProto(::oneflow::PlacementError* proto_placementerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PlacementError_& other);
       
   public:
    int compare(const _PlacementError_& other);

    bool operator==(const _PlacementError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PlacementError_& other) const;
  };

  ConstPlacementError(const ::std::shared_ptr<_PlacementError_>& data);
  ConstPlacementError(const ConstPlacementError&);
  ConstPlacementError(ConstPlacementError&&) noexcept;
  ConstPlacementError();
  ConstPlacementError(const ::oneflow::PlacementError& proto_placementerror);
  virtual ~ConstPlacementError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_placementerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstPlacementError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPlacementError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPlacementError& other) const;
 protected:
  const ::std::shared_ptr<_PlacementError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PlacementError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPlacementError
  void BuildFromProto(const PbMessage& proto_placementerror);
  
  ::std::shared_ptr<_PlacementError_> data_;
};

class PlacementError final : public ConstPlacementError {
 public:
  PlacementError(const ::std::shared_ptr<_PlacementError_>& data);
  PlacementError(const PlacementError& other);
  // enable nothrow for ::std::vector<PlacementError> resize 
  PlacementError(PlacementError&&) noexcept;
  PlacementError();
  explicit PlacementError(const ::oneflow::PlacementError& proto_placementerror);

  ~PlacementError() override;

  void InitFromProto(const PbMessage& proto_placementerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PlacementError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PlacementError& other) const;
  void Clear();
  void CopyFrom(const PlacementError& other);
  PlacementError& operator=(const PlacementError& other);


  ::std::shared_ptr<PlacementError> __SharedMutable__();
};


class ConstBlobSplitAxisInferError : public ::oneflow::cfg::Message {
 public:

  class _BlobSplitAxisInferError_ {
   public:
    _BlobSplitAxisInferError_();
    explicit _BlobSplitAxisInferError_(const _BlobSplitAxisInferError_& other);
    explicit _BlobSplitAxisInferError_(_BlobSplitAxisInferError_&& other);
    _BlobSplitAxisInferError_(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror);
    ~_BlobSplitAxisInferError_();

    void InitFromProto(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror);

    void ToProto(::oneflow::BlobSplitAxisInferError* proto_blobsplitaxisinfererror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobSplitAxisInferError_& other);
       
   public:
    int compare(const _BlobSplitAxisInferError_& other);

    bool operator==(const _BlobSplitAxisInferError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobSplitAxisInferError_& other) const;
  };

  ConstBlobSplitAxisInferError(const ::std::shared_ptr<_BlobSplitAxisInferError_>& data);
  ConstBlobSplitAxisInferError(const ConstBlobSplitAxisInferError&);
  ConstBlobSplitAxisInferError(ConstBlobSplitAxisInferError&&) noexcept;
  ConstBlobSplitAxisInferError();
  ConstBlobSplitAxisInferError(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror);
  virtual ~ConstBlobSplitAxisInferError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_blobsplitaxisinfererror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstBlobSplitAxisInferError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobSplitAxisInferError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobSplitAxisInferError& other) const;
 protected:
  const ::std::shared_ptr<_BlobSplitAxisInferError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobSplitAxisInferError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobSplitAxisInferError
  void BuildFromProto(const PbMessage& proto_blobsplitaxisinfererror);
  
  ::std::shared_ptr<_BlobSplitAxisInferError_> data_;
};

class BlobSplitAxisInferError final : public ConstBlobSplitAxisInferError {
 public:
  BlobSplitAxisInferError(const ::std::shared_ptr<_BlobSplitAxisInferError_>& data);
  BlobSplitAxisInferError(const BlobSplitAxisInferError& other);
  // enable nothrow for ::std::vector<BlobSplitAxisInferError> resize 
  BlobSplitAxisInferError(BlobSplitAxisInferError&&) noexcept;
  BlobSplitAxisInferError();
  explicit BlobSplitAxisInferError(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror);

  ~BlobSplitAxisInferError() override;

  void InitFromProto(const PbMessage& proto_blobsplitaxisinfererror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobSplitAxisInferError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobSplitAxisInferError& other) const;
  void Clear();
  void CopyFrom(const BlobSplitAxisInferError& other);
  BlobSplitAxisInferError& operator=(const BlobSplitAxisInferError& other);


  ::std::shared_ptr<BlobSplitAxisInferError> __SharedMutable__();
};


class ConstUnknownJobBuildAndInferError : public ::oneflow::cfg::Message {
 public:

  class _UnknownJobBuildAndInferError_ {
   public:
    _UnknownJobBuildAndInferError_();
    explicit _UnknownJobBuildAndInferError_(const _UnknownJobBuildAndInferError_& other);
    explicit _UnknownJobBuildAndInferError_(_UnknownJobBuildAndInferError_&& other);
    _UnknownJobBuildAndInferError_(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror);
    ~_UnknownJobBuildAndInferError_();

    void InitFromProto(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror);

    void ToProto(::oneflow::UnknownJobBuildAndInferError* proto_unknownjobbuildandinfererror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _UnknownJobBuildAndInferError_& other);
       
   public:
    int compare(const _UnknownJobBuildAndInferError_& other);

    bool operator==(const _UnknownJobBuildAndInferError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _UnknownJobBuildAndInferError_& other) const;
  };

  ConstUnknownJobBuildAndInferError(const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& data);
  ConstUnknownJobBuildAndInferError(const ConstUnknownJobBuildAndInferError&);
  ConstUnknownJobBuildAndInferError(ConstUnknownJobBuildAndInferError&&) noexcept;
  ConstUnknownJobBuildAndInferError();
  ConstUnknownJobBuildAndInferError(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror);
  virtual ~ConstUnknownJobBuildAndInferError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_unknownjobbuildandinfererror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstUnknownJobBuildAndInferError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstUnknownJobBuildAndInferError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstUnknownJobBuildAndInferError& other) const;
 protected:
  const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstUnknownJobBuildAndInferError
  void BuildFromProto(const PbMessage& proto_unknownjobbuildandinfererror);
  
  ::std::shared_ptr<_UnknownJobBuildAndInferError_> data_;
};

class UnknownJobBuildAndInferError final : public ConstUnknownJobBuildAndInferError {
 public:
  UnknownJobBuildAndInferError(const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& data);
  UnknownJobBuildAndInferError(const UnknownJobBuildAndInferError& other);
  // enable nothrow for ::std::vector<UnknownJobBuildAndInferError> resize 
  UnknownJobBuildAndInferError(UnknownJobBuildAndInferError&&) noexcept;
  UnknownJobBuildAndInferError();
  explicit UnknownJobBuildAndInferError(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror);

  ~UnknownJobBuildAndInferError() override;

  void InitFromProto(const PbMessage& proto_unknownjobbuildandinfererror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const UnknownJobBuildAndInferError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const UnknownJobBuildAndInferError& other) const;
  void Clear();
  void CopyFrom(const UnknownJobBuildAndInferError& other);
  UnknownJobBuildAndInferError& operator=(const UnknownJobBuildAndInferError& other);


  ::std::shared_ptr<UnknownJobBuildAndInferError> __SharedMutable__();
};


class ConstProtoParseFailedError : public ::oneflow::cfg::Message {
 public:

  class _ProtoParseFailedError_ {
   public:
    _ProtoParseFailedError_();
    explicit _ProtoParseFailedError_(const _ProtoParseFailedError_& other);
    explicit _ProtoParseFailedError_(_ProtoParseFailedError_&& other);
    _ProtoParseFailedError_(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror);
    ~_ProtoParseFailedError_();

    void InitFromProto(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror);

    void ToProto(::oneflow::ProtoParseFailedError* proto_protoparsefailederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ProtoParseFailedError_& other);
       
   public:
    int compare(const _ProtoParseFailedError_& other);

    bool operator==(const _ProtoParseFailedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ProtoParseFailedError_& other) const;
  };

  ConstProtoParseFailedError(const ::std::shared_ptr<_ProtoParseFailedError_>& data);
  ConstProtoParseFailedError(const ConstProtoParseFailedError&);
  ConstProtoParseFailedError(ConstProtoParseFailedError&&) noexcept;
  ConstProtoParseFailedError();
  ConstProtoParseFailedError(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror);
  virtual ~ConstProtoParseFailedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_protoparsefailederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstProtoParseFailedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstProtoParseFailedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstProtoParseFailedError& other) const;
 protected:
  const ::std::shared_ptr<_ProtoParseFailedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ProtoParseFailedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstProtoParseFailedError
  void BuildFromProto(const PbMessage& proto_protoparsefailederror);
  
  ::std::shared_ptr<_ProtoParseFailedError_> data_;
};

class ProtoParseFailedError final : public ConstProtoParseFailedError {
 public:
  ProtoParseFailedError(const ::std::shared_ptr<_ProtoParseFailedError_>& data);
  ProtoParseFailedError(const ProtoParseFailedError& other);
  // enable nothrow for ::std::vector<ProtoParseFailedError> resize 
  ProtoParseFailedError(ProtoParseFailedError&&) noexcept;
  ProtoParseFailedError();
  explicit ProtoParseFailedError(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror);

  ~ProtoParseFailedError() override;

  void InitFromProto(const PbMessage& proto_protoparsefailederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ProtoParseFailedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ProtoParseFailedError& other) const;
  void Clear();
  void CopyFrom(const ProtoParseFailedError& other);
  ProtoParseFailedError& operator=(const ProtoParseFailedError& other);


  ::std::shared_ptr<ProtoParseFailedError> __SharedMutable__();
};


class ConstCheckFailedError : public ::oneflow::cfg::Message {
 public:

  class _CheckFailedError_ {
   public:
    _CheckFailedError_();
    explicit _CheckFailedError_(const _CheckFailedError_& other);
    explicit _CheckFailedError_(_CheckFailedError_&& other);
    _CheckFailedError_(const ::oneflow::CheckFailedError& proto_checkfailederror);
    ~_CheckFailedError_();

    void InitFromProto(const ::oneflow::CheckFailedError& proto_checkfailederror);

    void ToProto(::oneflow::CheckFailedError* proto_checkfailederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CheckFailedError_& other);
       
   public:
    int compare(const _CheckFailedError_& other);

    bool operator==(const _CheckFailedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CheckFailedError_& other) const;
  };

  ConstCheckFailedError(const ::std::shared_ptr<_CheckFailedError_>& data);
  ConstCheckFailedError(const ConstCheckFailedError&);
  ConstCheckFailedError(ConstCheckFailedError&&) noexcept;
  ConstCheckFailedError();
  ConstCheckFailedError(const ::oneflow::CheckFailedError& proto_checkfailederror);
  virtual ~ConstCheckFailedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_checkfailederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstCheckFailedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCheckFailedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCheckFailedError& other) const;
 protected:
  const ::std::shared_ptr<_CheckFailedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CheckFailedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCheckFailedError
  void BuildFromProto(const PbMessage& proto_checkfailederror);
  
  ::std::shared_ptr<_CheckFailedError_> data_;
};

class CheckFailedError final : public ConstCheckFailedError {
 public:
  CheckFailedError(const ::std::shared_ptr<_CheckFailedError_>& data);
  CheckFailedError(const CheckFailedError& other);
  // enable nothrow for ::std::vector<CheckFailedError> resize 
  CheckFailedError(CheckFailedError&&) noexcept;
  CheckFailedError();
  explicit CheckFailedError(const ::oneflow::CheckFailedError& proto_checkfailederror);

  ~CheckFailedError() override;

  void InitFromProto(const PbMessage& proto_checkfailederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CheckFailedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CheckFailedError& other) const;
  void Clear();
  void CopyFrom(const CheckFailedError& other);
  CheckFailedError& operator=(const CheckFailedError& other);


  ::std::shared_ptr<CheckFailedError> __SharedMutable__();
};


class ConstTodoError : public ::oneflow::cfg::Message {
 public:

  class _TodoError_ {
   public:
    _TodoError_();
    explicit _TodoError_(const _TodoError_& other);
    explicit _TodoError_(_TodoError_&& other);
    _TodoError_(const ::oneflow::TodoError& proto_todoerror);
    ~_TodoError_();

    void InitFromProto(const ::oneflow::TodoError& proto_todoerror);

    void ToProto(::oneflow::TodoError* proto_todoerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TodoError_& other);
       
   public:
    int compare(const _TodoError_& other);

    bool operator==(const _TodoError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TodoError_& other) const;
  };

  ConstTodoError(const ::std::shared_ptr<_TodoError_>& data);
  ConstTodoError(const ConstTodoError&);
  ConstTodoError(ConstTodoError&&) noexcept;
  ConstTodoError();
  ConstTodoError(const ::oneflow::TodoError& proto_todoerror);
  virtual ~ConstTodoError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_todoerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstTodoError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTodoError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTodoError& other) const;
 protected:
  const ::std::shared_ptr<_TodoError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TodoError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTodoError
  void BuildFromProto(const PbMessage& proto_todoerror);
  
  ::std::shared_ptr<_TodoError_> data_;
};

class TodoError final : public ConstTodoError {
 public:
  TodoError(const ::std::shared_ptr<_TodoError_>& data);
  TodoError(const TodoError& other);
  // enable nothrow for ::std::vector<TodoError> resize 
  TodoError(TodoError&&) noexcept;
  TodoError();
  explicit TodoError(const ::oneflow::TodoError& proto_todoerror);

  ~TodoError() override;

  void InitFromProto(const PbMessage& proto_todoerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TodoError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TodoError& other) const;
  void Clear();
  void CopyFrom(const TodoError& other);
  TodoError& operator=(const TodoError& other);


  ::std::shared_ptr<TodoError> __SharedMutable__();
};


class ConstUnimplementedError : public ::oneflow::cfg::Message {
 public:

  class _UnimplementedError_ {
   public:
    _UnimplementedError_();
    explicit _UnimplementedError_(const _UnimplementedError_& other);
    explicit _UnimplementedError_(_UnimplementedError_&& other);
    _UnimplementedError_(const ::oneflow::UnimplementedError& proto_unimplementederror);
    ~_UnimplementedError_();

    void InitFromProto(const ::oneflow::UnimplementedError& proto_unimplementederror);

    void ToProto(::oneflow::UnimplementedError* proto_unimplementederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _UnimplementedError_& other);
       
   public:
    int compare(const _UnimplementedError_& other);

    bool operator==(const _UnimplementedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _UnimplementedError_& other) const;
  };

  ConstUnimplementedError(const ::std::shared_ptr<_UnimplementedError_>& data);
  ConstUnimplementedError(const ConstUnimplementedError&);
  ConstUnimplementedError(ConstUnimplementedError&&) noexcept;
  ConstUnimplementedError();
  ConstUnimplementedError(const ::oneflow::UnimplementedError& proto_unimplementederror);
  virtual ~ConstUnimplementedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_unimplementederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstUnimplementedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstUnimplementedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstUnimplementedError& other) const;
 protected:
  const ::std::shared_ptr<_UnimplementedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_UnimplementedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstUnimplementedError
  void BuildFromProto(const PbMessage& proto_unimplementederror);
  
  ::std::shared_ptr<_UnimplementedError_> data_;
};

class UnimplementedError final : public ConstUnimplementedError {
 public:
  UnimplementedError(const ::std::shared_ptr<_UnimplementedError_>& data);
  UnimplementedError(const UnimplementedError& other);
  // enable nothrow for ::std::vector<UnimplementedError> resize 
  UnimplementedError(UnimplementedError&&) noexcept;
  UnimplementedError();
  explicit UnimplementedError(const ::oneflow::UnimplementedError& proto_unimplementederror);

  ~UnimplementedError() override;

  void InitFromProto(const PbMessage& proto_unimplementederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const UnimplementedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const UnimplementedError& other) const;
  void Clear();
  void CopyFrom(const UnimplementedError& other);
  UnimplementedError& operator=(const UnimplementedError& other);


  ::std::shared_ptr<UnimplementedError> __SharedMutable__();
};


class ConstBoxingNotSupportedError : public ::oneflow::cfg::Message {
 public:

  class _BoxingNotSupportedError_ {
   public:
    _BoxingNotSupportedError_();
    explicit _BoxingNotSupportedError_(const _BoxingNotSupportedError_& other);
    explicit _BoxingNotSupportedError_(_BoxingNotSupportedError_&& other);
    _BoxingNotSupportedError_(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror);
    ~_BoxingNotSupportedError_();

    void InitFromProto(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror);

    void ToProto(::oneflow::BoxingNotSupportedError* proto_boxingnotsupportederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxingNotSupportedError_& other);
       
   public:
    int compare(const _BoxingNotSupportedError_& other);

    bool operator==(const _BoxingNotSupportedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxingNotSupportedError_& other) const;
  };

  ConstBoxingNotSupportedError(const ::std::shared_ptr<_BoxingNotSupportedError_>& data);
  ConstBoxingNotSupportedError(const ConstBoxingNotSupportedError&);
  ConstBoxingNotSupportedError(ConstBoxingNotSupportedError&&) noexcept;
  ConstBoxingNotSupportedError();
  ConstBoxingNotSupportedError(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror);
  virtual ~ConstBoxingNotSupportedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxingnotsupportederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstBoxingNotSupportedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxingNotSupportedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxingNotSupportedError& other) const;
 protected:
  const ::std::shared_ptr<_BoxingNotSupportedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxingNotSupportedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxingNotSupportedError
  void BuildFromProto(const PbMessage& proto_boxingnotsupportederror);
  
  ::std::shared_ptr<_BoxingNotSupportedError_> data_;
};

class BoxingNotSupportedError final : public ConstBoxingNotSupportedError {
 public:
  BoxingNotSupportedError(const ::std::shared_ptr<_BoxingNotSupportedError_>& data);
  BoxingNotSupportedError(const BoxingNotSupportedError& other);
  // enable nothrow for ::std::vector<BoxingNotSupportedError> resize 
  BoxingNotSupportedError(BoxingNotSupportedError&&) noexcept;
  BoxingNotSupportedError();
  explicit BoxingNotSupportedError(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror);

  ~BoxingNotSupportedError() override;

  void InitFromProto(const PbMessage& proto_boxingnotsupportederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxingNotSupportedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxingNotSupportedError& other) const;
  void Clear();
  void CopyFrom(const BoxingNotSupportedError& other);
  BoxingNotSupportedError& operator=(const BoxingNotSupportedError& other);


  ::std::shared_ptr<BoxingNotSupportedError> __SharedMutable__();
};


class ConstGradientFunctionNotFoundError : public ::oneflow::cfg::Message {
 public:

  class _GradientFunctionNotFoundError_ {
   public:
    _GradientFunctionNotFoundError_();
    explicit _GradientFunctionNotFoundError_(const _GradientFunctionNotFoundError_& other);
    explicit _GradientFunctionNotFoundError_(_GradientFunctionNotFoundError_&& other);
    _GradientFunctionNotFoundError_(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror);
    ~_GradientFunctionNotFoundError_();

    void InitFromProto(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror);

    void ToProto(::oneflow::GradientFunctionNotFoundError* proto_gradientfunctionnotfounderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _GradientFunctionNotFoundError_& other);
       
   public:
    int compare(const _GradientFunctionNotFoundError_& other);

    bool operator==(const _GradientFunctionNotFoundError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _GradientFunctionNotFoundError_& other) const;
  };

  ConstGradientFunctionNotFoundError(const ::std::shared_ptr<_GradientFunctionNotFoundError_>& data);
  ConstGradientFunctionNotFoundError(const ConstGradientFunctionNotFoundError&);
  ConstGradientFunctionNotFoundError(ConstGradientFunctionNotFoundError&&) noexcept;
  ConstGradientFunctionNotFoundError();
  ConstGradientFunctionNotFoundError(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror);
  virtual ~ConstGradientFunctionNotFoundError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_gradientfunctionnotfounderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstGradientFunctionNotFoundError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstGradientFunctionNotFoundError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstGradientFunctionNotFoundError& other) const;
 protected:
  const ::std::shared_ptr<_GradientFunctionNotFoundError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_GradientFunctionNotFoundError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstGradientFunctionNotFoundError
  void BuildFromProto(const PbMessage& proto_gradientfunctionnotfounderror);
  
  ::std::shared_ptr<_GradientFunctionNotFoundError_> data_;
};

class GradientFunctionNotFoundError final : public ConstGradientFunctionNotFoundError {
 public:
  GradientFunctionNotFoundError(const ::std::shared_ptr<_GradientFunctionNotFoundError_>& data);
  GradientFunctionNotFoundError(const GradientFunctionNotFoundError& other);
  // enable nothrow for ::std::vector<GradientFunctionNotFoundError> resize 
  GradientFunctionNotFoundError(GradientFunctionNotFoundError&&) noexcept;
  GradientFunctionNotFoundError();
  explicit GradientFunctionNotFoundError(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror);

  ~GradientFunctionNotFoundError() override;

  void InitFromProto(const PbMessage& proto_gradientfunctionnotfounderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const GradientFunctionNotFoundError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const GradientFunctionNotFoundError& other) const;
  void Clear();
  void CopyFrom(const GradientFunctionNotFoundError& other);
  GradientFunctionNotFoundError& operator=(const GradientFunctionNotFoundError& other);


  ::std::shared_ptr<GradientFunctionNotFoundError> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_;

class ConstOpKernelNotFoundError : public ::oneflow::cfg::Message {
 public:

  class _OpKernelNotFoundError_ {
   public:
    _OpKernelNotFoundError_();
    explicit _OpKernelNotFoundError_(const _OpKernelNotFoundError_& other);
    explicit _OpKernelNotFoundError_(_OpKernelNotFoundError_&& other);
    _OpKernelNotFoundError_(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror);
    ~_OpKernelNotFoundError_();

    void InitFromProto(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror);

    void ToProto(::oneflow::OpKernelNotFoundError* proto_opkernelnotfounderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpKernelNotFoundError_& other);
  
      // repeated field op_kernels_not_found_debug_str
   public:
    ::std::size_t op_kernels_not_found_debug_str_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& op_kernels_not_found_debug_str() const;
    const ::std::string& op_kernels_not_found_debug_str(::std::size_t index) const;
    void clear_op_kernels_not_found_debug_str();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_op_kernels_not_found_debug_str();
    ::std::string* mutable_op_kernels_not_found_debug_str(::std::size_t index);
      void add_op_kernels_not_found_debug_str(const ::std::string& value);
    void set_op_kernels_not_found_debug_str(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> op_kernels_not_found_debug_str_;
         
   public:
    int compare(const _OpKernelNotFoundError_& other);

    bool operator==(const _OpKernelNotFoundError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpKernelNotFoundError_& other) const;
  };

  ConstOpKernelNotFoundError(const ::std::shared_ptr<_OpKernelNotFoundError_>& data);
  ConstOpKernelNotFoundError(const ConstOpKernelNotFoundError&);
  ConstOpKernelNotFoundError(ConstOpKernelNotFoundError&&) noexcept;
  ConstOpKernelNotFoundError();
  ConstOpKernelNotFoundError(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror);
  virtual ~ConstOpKernelNotFoundError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opkernelnotfounderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_kernels_not_found_debug_str
 public:
  ::std::size_t op_kernels_not_found_debug_str_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& op_kernels_not_found_debug_str() const;
  const ::std::string& op_kernels_not_found_debug_str(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_op_kernels_not_found_debug_str() const;

 public:
  ::std::shared_ptr<ConstOpKernelNotFoundError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpKernelNotFoundError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpKernelNotFoundError& other) const;
 protected:
  const ::std::shared_ptr<_OpKernelNotFoundError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpKernelNotFoundError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpKernelNotFoundError
  void BuildFromProto(const PbMessage& proto_opkernelnotfounderror);
  
  ::std::shared_ptr<_OpKernelNotFoundError_> data_;
};

class OpKernelNotFoundError final : public ConstOpKernelNotFoundError {
 public:
  OpKernelNotFoundError(const ::std::shared_ptr<_OpKernelNotFoundError_>& data);
  OpKernelNotFoundError(const OpKernelNotFoundError& other);
  // enable nothrow for ::std::vector<OpKernelNotFoundError> resize 
  OpKernelNotFoundError(OpKernelNotFoundError&&) noexcept;
  OpKernelNotFoundError();
  explicit OpKernelNotFoundError(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror);

  ~OpKernelNotFoundError() override;

  void InitFromProto(const PbMessage& proto_opkernelnotfounderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpKernelNotFoundError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpKernelNotFoundError& other) const;
  void Clear();
  void CopyFrom(const OpKernelNotFoundError& other);
  OpKernelNotFoundError& operator=(const OpKernelNotFoundError& other);

  // repeated field op_kernels_not_found_debug_str
 public:
  void clear_op_kernels_not_found_debug_str();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_op_kernels_not_found_debug_str();
  ::std::string* mutable_op_kernels_not_found_debug_str(::std::size_t index);
  void add_op_kernels_not_found_debug_str(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_op_kernels_not_found_debug_str();
  void set_op_kernels_not_found_debug_str(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<OpKernelNotFoundError> __SharedMutable__();
};


class ConstMultipleOpKernelsMatchedError : public ::oneflow::cfg::Message {
 public:

  class _MultipleOpKernelsMatchedError_ {
   public:
    _MultipleOpKernelsMatchedError_();
    explicit _MultipleOpKernelsMatchedError_(const _MultipleOpKernelsMatchedError_& other);
    explicit _MultipleOpKernelsMatchedError_(_MultipleOpKernelsMatchedError_&& other);
    _MultipleOpKernelsMatchedError_(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror);
    ~_MultipleOpKernelsMatchedError_();

    void InitFromProto(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror);

    void ToProto(::oneflow::MultipleOpKernelsMatchedError* proto_multipleopkernelsmatchederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MultipleOpKernelsMatchedError_& other);
  
      // repeated field matched_op_kernels_debug_str
   public:
    ::std::size_t matched_op_kernels_debug_str_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& matched_op_kernels_debug_str() const;
    const ::std::string& matched_op_kernels_debug_str(::std::size_t index) const;
    void clear_matched_op_kernels_debug_str();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_matched_op_kernels_debug_str();
    ::std::string* mutable_matched_op_kernels_debug_str(::std::size_t index);
      void add_matched_op_kernels_debug_str(const ::std::string& value);
    void set_matched_op_kernels_debug_str(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> matched_op_kernels_debug_str_;
         
   public:
    int compare(const _MultipleOpKernelsMatchedError_& other);

    bool operator==(const _MultipleOpKernelsMatchedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MultipleOpKernelsMatchedError_& other) const;
  };

  ConstMultipleOpKernelsMatchedError(const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& data);
  ConstMultipleOpKernelsMatchedError(const ConstMultipleOpKernelsMatchedError&);
  ConstMultipleOpKernelsMatchedError(ConstMultipleOpKernelsMatchedError&&) noexcept;
  ConstMultipleOpKernelsMatchedError();
  ConstMultipleOpKernelsMatchedError(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror);
  virtual ~ConstMultipleOpKernelsMatchedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_multipleopkernelsmatchederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field matched_op_kernels_debug_str
 public:
  ::std::size_t matched_op_kernels_debug_str_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& matched_op_kernels_debug_str() const;
  const ::std::string& matched_op_kernels_debug_str(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_matched_op_kernels_debug_str() const;

 public:
  ::std::shared_ptr<ConstMultipleOpKernelsMatchedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMultipleOpKernelsMatchedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMultipleOpKernelsMatchedError& other) const;
 protected:
  const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMultipleOpKernelsMatchedError
  void BuildFromProto(const PbMessage& proto_multipleopkernelsmatchederror);
  
  ::std::shared_ptr<_MultipleOpKernelsMatchedError_> data_;
};

class MultipleOpKernelsMatchedError final : public ConstMultipleOpKernelsMatchedError {
 public:
  MultipleOpKernelsMatchedError(const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& data);
  MultipleOpKernelsMatchedError(const MultipleOpKernelsMatchedError& other);
  // enable nothrow for ::std::vector<MultipleOpKernelsMatchedError> resize 
  MultipleOpKernelsMatchedError(MultipleOpKernelsMatchedError&&) noexcept;
  MultipleOpKernelsMatchedError();
  explicit MultipleOpKernelsMatchedError(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror);

  ~MultipleOpKernelsMatchedError() override;

  void InitFromProto(const PbMessage& proto_multipleopkernelsmatchederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MultipleOpKernelsMatchedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MultipleOpKernelsMatchedError& other) const;
  void Clear();
  void CopyFrom(const MultipleOpKernelsMatchedError& other);
  MultipleOpKernelsMatchedError& operator=(const MultipleOpKernelsMatchedError& other);

  // repeated field matched_op_kernels_debug_str
 public:
  void clear_matched_op_kernels_debug_str();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_matched_op_kernels_debug_str();
  ::std::string* mutable_matched_op_kernels_debug_str(::std::size_t index);
  void add_matched_op_kernels_debug_str(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_matched_op_kernels_debug_str();
  void set_matched_op_kernels_debug_str(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<MultipleOpKernelsMatchedError> __SharedMutable__();
};


class ConstMemoryZoneOutOfMemoryError : public ::oneflow::cfg::Message {
 public:

  class _MemoryZoneOutOfMemoryError_ {
   public:
    _MemoryZoneOutOfMemoryError_();
    explicit _MemoryZoneOutOfMemoryError_(const _MemoryZoneOutOfMemoryError_& other);
    explicit _MemoryZoneOutOfMemoryError_(_MemoryZoneOutOfMemoryError_&& other);
    _MemoryZoneOutOfMemoryError_(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror);
    ~_MemoryZoneOutOfMemoryError_();

    void InitFromProto(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror);

    void ToProto(::oneflow::MemoryZoneOutOfMemoryError* proto_memoryzoneoutofmemoryerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MemoryZoneOutOfMemoryError_& other);
  
      // repeated field machine_id
   public:
    ::std::size_t machine_id_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& machine_id() const;
    const ::std::string& machine_id(::std::size_t index) const;
    void clear_machine_id();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_machine_id();
    ::std::string* mutable_machine_id(::std::size_t index);
      void add_machine_id(const ::std::string& value);
    void set_machine_id(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> machine_id_;
    
      // repeated field mem_zone_id
   public:
    ::std::size_t mem_zone_id_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& mem_zone_id() const;
    const ::std::string& mem_zone_id(::std::size_t index) const;
    void clear_mem_zone_id();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_mem_zone_id();
    ::std::string* mutable_mem_zone_id(::std::size_t index);
      void add_mem_zone_id(const ::std::string& value);
    void set_mem_zone_id(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> mem_zone_id_;
    
      // repeated field device_tag
   public:
    ::std::size_t device_tag_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& device_tag() const;
    const ::std::string& device_tag(::std::size_t index) const;
    void clear_device_tag();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_device_tag();
    ::std::string* mutable_device_tag(::std::size_t index);
      void add_device_tag(const ::std::string& value);
    void set_device_tag(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> device_tag_;
    
      // repeated field required
   public:
    ::std::size_t required_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& required() const;
    const ::std::string& required(::std::size_t index) const;
    void clear_required();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_required();
    ::std::string* mutable_required(::std::size_t index);
      void add_required(const ::std::string& value);
    void set_required(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> required_;
    
      // repeated field available
   public:
    ::std::size_t available_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& available() const;
    const ::std::string& available(::std::size_t index) const;
    void clear_available();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_available();
    ::std::string* mutable_available(::std::size_t index);
      void add_available(const ::std::string& value);
    void set_available(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> available_;
         
   public:
    int compare(const _MemoryZoneOutOfMemoryError_& other);

    bool operator==(const _MemoryZoneOutOfMemoryError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MemoryZoneOutOfMemoryError_& other) const;
  };

  ConstMemoryZoneOutOfMemoryError(const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& data);
  ConstMemoryZoneOutOfMemoryError(const ConstMemoryZoneOutOfMemoryError&);
  ConstMemoryZoneOutOfMemoryError(ConstMemoryZoneOutOfMemoryError&&) noexcept;
  ConstMemoryZoneOutOfMemoryError();
  ConstMemoryZoneOutOfMemoryError(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror);
  virtual ~ConstMemoryZoneOutOfMemoryError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_memoryzoneoutofmemoryerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field machine_id
 public:
  ::std::size_t machine_id_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& machine_id() const;
  const ::std::string& machine_id(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_machine_id() const;
  // repeated field mem_zone_id
 public:
  ::std::size_t mem_zone_id_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& mem_zone_id() const;
  const ::std::string& mem_zone_id(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_mem_zone_id() const;
  // repeated field device_tag
 public:
  ::std::size_t device_tag_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& device_tag() const;
  const ::std::string& device_tag(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_device_tag() const;
  // repeated field required
 public:
  ::std::size_t required_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& required() const;
  const ::std::string& required(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_required() const;
  // repeated field available
 public:
  ::std::size_t available_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& available() const;
  const ::std::string& available(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_available() const;

 public:
  ::std::shared_ptr<ConstMemoryZoneOutOfMemoryError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMemoryZoneOutOfMemoryError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMemoryZoneOutOfMemoryError& other) const;
 protected:
  const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMemoryZoneOutOfMemoryError
  void BuildFromProto(const PbMessage& proto_memoryzoneoutofmemoryerror);
  
  ::std::shared_ptr<_MemoryZoneOutOfMemoryError_> data_;
};

class MemoryZoneOutOfMemoryError final : public ConstMemoryZoneOutOfMemoryError {
 public:
  MemoryZoneOutOfMemoryError(const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& data);
  MemoryZoneOutOfMemoryError(const MemoryZoneOutOfMemoryError& other);
  // enable nothrow for ::std::vector<MemoryZoneOutOfMemoryError> resize 
  MemoryZoneOutOfMemoryError(MemoryZoneOutOfMemoryError&&) noexcept;
  MemoryZoneOutOfMemoryError();
  explicit MemoryZoneOutOfMemoryError(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror);

  ~MemoryZoneOutOfMemoryError() override;

  void InitFromProto(const PbMessage& proto_memoryzoneoutofmemoryerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MemoryZoneOutOfMemoryError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MemoryZoneOutOfMemoryError& other) const;
  void Clear();
  void CopyFrom(const MemoryZoneOutOfMemoryError& other);
  MemoryZoneOutOfMemoryError& operator=(const MemoryZoneOutOfMemoryError& other);

  // repeated field machine_id
 public:
  void clear_machine_id();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_machine_id();
  ::std::string* mutable_machine_id(::std::size_t index);
  void add_machine_id(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_machine_id();
  void set_machine_id(::std::size_t index, const ::std::string& value);
  // repeated field mem_zone_id
 public:
  void clear_mem_zone_id();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_mem_zone_id();
  ::std::string* mutable_mem_zone_id(::std::size_t index);
  void add_mem_zone_id(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_mem_zone_id();
  void set_mem_zone_id(::std::size_t index, const ::std::string& value);
  // repeated field device_tag
 public:
  void clear_device_tag();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_device_tag();
  ::std::string* mutable_device_tag(::std::size_t index);
  void add_device_tag(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_device_tag();
  void set_device_tag(::std::size_t index, const ::std::string& value);
  // repeated field required
 public:
  void clear_required();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_required();
  ::std::string* mutable_required(::std::size_t index);
  void add_required(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_required();
  void set_required(::std::size_t index, const ::std::string& value);
  // repeated field available
 public:
  void clear_available();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_available();
  ::std::string* mutable_available(::std::size_t index);
  void add_available(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_available();
  void set_available(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<MemoryZoneOutOfMemoryError> __SharedMutable__();
};


class ConstLossBlobNotFoundError : public ::oneflow::cfg::Message {
 public:

  class _LossBlobNotFoundError_ {
   public:
    _LossBlobNotFoundError_();
    explicit _LossBlobNotFoundError_(const _LossBlobNotFoundError_& other);
    explicit _LossBlobNotFoundError_(_LossBlobNotFoundError_&& other);
    _LossBlobNotFoundError_(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror);
    ~_LossBlobNotFoundError_();

    void InitFromProto(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror);

    void ToProto(::oneflow::LossBlobNotFoundError* proto_lossblobnotfounderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LossBlobNotFoundError_& other);
       
   public:
    int compare(const _LossBlobNotFoundError_& other);

    bool operator==(const _LossBlobNotFoundError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LossBlobNotFoundError_& other) const;
  };

  ConstLossBlobNotFoundError(const ::std::shared_ptr<_LossBlobNotFoundError_>& data);
  ConstLossBlobNotFoundError(const ConstLossBlobNotFoundError&);
  ConstLossBlobNotFoundError(ConstLossBlobNotFoundError&&) noexcept;
  ConstLossBlobNotFoundError();
  ConstLossBlobNotFoundError(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror);
  virtual ~ConstLossBlobNotFoundError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_lossblobnotfounderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstLossBlobNotFoundError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLossBlobNotFoundError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLossBlobNotFoundError& other) const;
 protected:
  const ::std::shared_ptr<_LossBlobNotFoundError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LossBlobNotFoundError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLossBlobNotFoundError
  void BuildFromProto(const PbMessage& proto_lossblobnotfounderror);
  
  ::std::shared_ptr<_LossBlobNotFoundError_> data_;
};

class LossBlobNotFoundError final : public ConstLossBlobNotFoundError {
 public:
  LossBlobNotFoundError(const ::std::shared_ptr<_LossBlobNotFoundError_>& data);
  LossBlobNotFoundError(const LossBlobNotFoundError& other);
  // enable nothrow for ::std::vector<LossBlobNotFoundError> resize 
  LossBlobNotFoundError(LossBlobNotFoundError&&) noexcept;
  LossBlobNotFoundError();
  explicit LossBlobNotFoundError(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror);

  ~LossBlobNotFoundError() override;

  void InitFromProto(const PbMessage& proto_lossblobnotfounderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LossBlobNotFoundError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LossBlobNotFoundError& other) const;
  void Clear();
  void CopyFrom(const LossBlobNotFoundError& other);
  LossBlobNotFoundError& operator=(const LossBlobNotFoundError& other);


  ::std::shared_ptr<LossBlobNotFoundError> __SharedMutable__();
};


class ConstRwMutexedObjectNotFoundError : public ::oneflow::cfg::Message {
 public:

  class _RwMutexedObjectNotFoundError_ {
   public:
    _RwMutexedObjectNotFoundError_();
    explicit _RwMutexedObjectNotFoundError_(const _RwMutexedObjectNotFoundError_& other);
    explicit _RwMutexedObjectNotFoundError_(_RwMutexedObjectNotFoundError_&& other);
    _RwMutexedObjectNotFoundError_(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror);
    ~_RwMutexedObjectNotFoundError_();

    void InitFromProto(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror);

    void ToProto(::oneflow::RwMutexedObjectNotFoundError* proto_rwmutexedobjectnotfounderror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RwMutexedObjectNotFoundError_& other);
       
   public:
    int compare(const _RwMutexedObjectNotFoundError_& other);

    bool operator==(const _RwMutexedObjectNotFoundError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RwMutexedObjectNotFoundError_& other) const;
  };

  ConstRwMutexedObjectNotFoundError(const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& data);
  ConstRwMutexedObjectNotFoundError(const ConstRwMutexedObjectNotFoundError&);
  ConstRwMutexedObjectNotFoundError(ConstRwMutexedObjectNotFoundError&&) noexcept;
  ConstRwMutexedObjectNotFoundError();
  ConstRwMutexedObjectNotFoundError(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror);
  virtual ~ConstRwMutexedObjectNotFoundError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_rwmutexedobjectnotfounderror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstRwMutexedObjectNotFoundError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRwMutexedObjectNotFoundError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRwMutexedObjectNotFoundError& other) const;
 protected:
  const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRwMutexedObjectNotFoundError
  void BuildFromProto(const PbMessage& proto_rwmutexedobjectnotfounderror);
  
  ::std::shared_ptr<_RwMutexedObjectNotFoundError_> data_;
};

class RwMutexedObjectNotFoundError final : public ConstRwMutexedObjectNotFoundError {
 public:
  RwMutexedObjectNotFoundError(const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& data);
  RwMutexedObjectNotFoundError(const RwMutexedObjectNotFoundError& other);
  // enable nothrow for ::std::vector<RwMutexedObjectNotFoundError> resize 
  RwMutexedObjectNotFoundError(RwMutexedObjectNotFoundError&&) noexcept;
  RwMutexedObjectNotFoundError();
  explicit RwMutexedObjectNotFoundError(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror);

  ~RwMutexedObjectNotFoundError() override;

  void InitFromProto(const PbMessage& proto_rwmutexedobjectnotfounderror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RwMutexedObjectNotFoundError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RwMutexedObjectNotFoundError& other) const;
  void Clear();
  void CopyFrom(const RwMutexedObjectNotFoundError& other);
  RwMutexedObjectNotFoundError& operator=(const RwMutexedObjectNotFoundError& other);


  ::std::shared_ptr<RwMutexedObjectNotFoundError> __SharedMutable__();
};


class ConstUnknownError : public ::oneflow::cfg::Message {
 public:

  class _UnknownError_ {
   public:
    _UnknownError_();
    explicit _UnknownError_(const _UnknownError_& other);
    explicit _UnknownError_(_UnknownError_&& other);
    _UnknownError_(const ::oneflow::UnknownError& proto_unknownerror);
    ~_UnknownError_();

    void InitFromProto(const ::oneflow::UnknownError& proto_unknownerror);

    void ToProto(::oneflow::UnknownError* proto_unknownerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _UnknownError_& other);
       
   public:
    int compare(const _UnknownError_& other);

    bool operator==(const _UnknownError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _UnknownError_& other) const;
  };

  ConstUnknownError(const ::std::shared_ptr<_UnknownError_>& data);
  ConstUnknownError(const ConstUnknownError&);
  ConstUnknownError(ConstUnknownError&&) noexcept;
  ConstUnknownError();
  ConstUnknownError(const ::oneflow::UnknownError& proto_unknownerror);
  virtual ~ConstUnknownError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_unknownerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstUnknownError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstUnknownError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstUnknownError& other) const;
 protected:
  const ::std::shared_ptr<_UnknownError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_UnknownError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstUnknownError
  void BuildFromProto(const PbMessage& proto_unknownerror);
  
  ::std::shared_ptr<_UnknownError_> data_;
};

class UnknownError final : public ConstUnknownError {
 public:
  UnknownError(const ::std::shared_ptr<_UnknownError_>& data);
  UnknownError(const UnknownError& other);
  // enable nothrow for ::std::vector<UnknownError> resize 
  UnknownError(UnknownError&&) noexcept;
  UnknownError();
  explicit UnknownError(const ::oneflow::UnknownError& proto_unknownerror);

  ~UnknownError() override;

  void InitFromProto(const PbMessage& proto_unknownerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const UnknownError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const UnknownError& other) const;
  void Clear();
  void CopyFrom(const UnknownError& other);
  UnknownError& operator=(const UnknownError& other);


  ::std::shared_ptr<UnknownError> __SharedMutable__();
};


class ConstCompileOptionWrongError : public ::oneflow::cfg::Message {
 public:

  class _CompileOptionWrongError_ {
   public:
    _CompileOptionWrongError_();
    explicit _CompileOptionWrongError_(const _CompileOptionWrongError_& other);
    explicit _CompileOptionWrongError_(_CompileOptionWrongError_&& other);
    _CompileOptionWrongError_(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror);
    ~_CompileOptionWrongError_();

    void InitFromProto(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror);

    void ToProto(::oneflow::CompileOptionWrongError* proto_compileoptionwrongerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CompileOptionWrongError_& other);
       
   public:
    int compare(const _CompileOptionWrongError_& other);

    bool operator==(const _CompileOptionWrongError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CompileOptionWrongError_& other) const;
  };

  ConstCompileOptionWrongError(const ::std::shared_ptr<_CompileOptionWrongError_>& data);
  ConstCompileOptionWrongError(const ConstCompileOptionWrongError&);
  ConstCompileOptionWrongError(ConstCompileOptionWrongError&&) noexcept;
  ConstCompileOptionWrongError();
  ConstCompileOptionWrongError(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror);
  virtual ~ConstCompileOptionWrongError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_compileoptionwrongerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstCompileOptionWrongError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCompileOptionWrongError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCompileOptionWrongError& other) const;
 protected:
  const ::std::shared_ptr<_CompileOptionWrongError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CompileOptionWrongError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCompileOptionWrongError
  void BuildFromProto(const PbMessage& proto_compileoptionwrongerror);
  
  ::std::shared_ptr<_CompileOptionWrongError_> data_;
};

class CompileOptionWrongError final : public ConstCompileOptionWrongError {
 public:
  CompileOptionWrongError(const ::std::shared_ptr<_CompileOptionWrongError_>& data);
  CompileOptionWrongError(const CompileOptionWrongError& other);
  // enable nothrow for ::std::vector<CompileOptionWrongError> resize 
  CompileOptionWrongError(CompileOptionWrongError&&) noexcept;
  CompileOptionWrongError();
  explicit CompileOptionWrongError(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror);

  ~CompileOptionWrongError() override;

  void InitFromProto(const PbMessage& proto_compileoptionwrongerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CompileOptionWrongError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CompileOptionWrongError& other) const;
  void Clear();
  void CopyFrom(const CompileOptionWrongError& other);
  CompileOptionWrongError& operator=(const CompileOptionWrongError& other);


  ::std::shared_ptr<CompileOptionWrongError> __SharedMutable__();
};


class ConstInputDeviceNotMatchError : public ::oneflow::cfg::Message {
 public:

  class _InputDeviceNotMatchError_ {
   public:
    _InputDeviceNotMatchError_();
    explicit _InputDeviceNotMatchError_(const _InputDeviceNotMatchError_& other);
    explicit _InputDeviceNotMatchError_(_InputDeviceNotMatchError_&& other);
    _InputDeviceNotMatchError_(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror);
    ~_InputDeviceNotMatchError_();

    void InitFromProto(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror);

    void ToProto(::oneflow::InputDeviceNotMatchError* proto_inputdevicenotmatcherror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InputDeviceNotMatchError_& other);
  
      // repeated field info
   public:
    ::std::size_t info_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& info() const;
    const ::std::string& info(::std::size_t index) const;
    void clear_info();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_info();
    ::std::string* mutable_info(::std::size_t index);
      void add_info(const ::std::string& value);
    void set_info(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> info_;
         
   public:
    int compare(const _InputDeviceNotMatchError_& other);

    bool operator==(const _InputDeviceNotMatchError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InputDeviceNotMatchError_& other) const;
  };

  ConstInputDeviceNotMatchError(const ::std::shared_ptr<_InputDeviceNotMatchError_>& data);
  ConstInputDeviceNotMatchError(const ConstInputDeviceNotMatchError&);
  ConstInputDeviceNotMatchError(ConstInputDeviceNotMatchError&&) noexcept;
  ConstInputDeviceNotMatchError();
  ConstInputDeviceNotMatchError(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror);
  virtual ~ConstInputDeviceNotMatchError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_inputdevicenotmatcherror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field info
 public:
  ::std::size_t info_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& info() const;
  const ::std::string& info(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_const_info() const;

 public:
  ::std::shared_ptr<ConstInputDeviceNotMatchError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInputDeviceNotMatchError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInputDeviceNotMatchError& other) const;
 protected:
  const ::std::shared_ptr<_InputDeviceNotMatchError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InputDeviceNotMatchError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInputDeviceNotMatchError
  void BuildFromProto(const PbMessage& proto_inputdevicenotmatcherror);
  
  ::std::shared_ptr<_InputDeviceNotMatchError_> data_;
};

class InputDeviceNotMatchError final : public ConstInputDeviceNotMatchError {
 public:
  InputDeviceNotMatchError(const ::std::shared_ptr<_InputDeviceNotMatchError_>& data);
  InputDeviceNotMatchError(const InputDeviceNotMatchError& other);
  // enable nothrow for ::std::vector<InputDeviceNotMatchError> resize 
  InputDeviceNotMatchError(InputDeviceNotMatchError&&) noexcept;
  InputDeviceNotMatchError();
  explicit InputDeviceNotMatchError(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror);

  ~InputDeviceNotMatchError() override;

  void InitFromProto(const PbMessage& proto_inputdevicenotmatcherror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InputDeviceNotMatchError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InputDeviceNotMatchError& other) const;
  void Clear();
  void CopyFrom(const InputDeviceNotMatchError& other);
  InputDeviceNotMatchError& operator=(const InputDeviceNotMatchError& other);

  // repeated field info
 public:
  void clear_info();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* mutable_info();
  ::std::string* mutable_info(::std::size_t index);
  void add_info(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> shared_mutable_info();
  void set_info(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<InputDeviceNotMatchError> __SharedMutable__();
};


class ConstErrorStackFrame : public ::oneflow::cfg::Message {
 public:

  class _ErrorStackFrame_ {
   public:
    _ErrorStackFrame_();
    explicit _ErrorStackFrame_(const _ErrorStackFrame_& other);
    explicit _ErrorStackFrame_(_ErrorStackFrame_&& other);
    _ErrorStackFrame_(const ::oneflow::ErrorStackFrame& proto_errorstackframe);
    ~_ErrorStackFrame_();

    void InitFromProto(const ::oneflow::ErrorStackFrame& proto_errorstackframe);

    void ToProto(::oneflow::ErrorStackFrame* proto_errorstackframe) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ErrorStackFrame_& other);
  
      // optional field file
     public:
    bool has_file() const;
    const ::std::string& file() const;
    void clear_file();
    void set_file(const ::std::string& value);
    ::std::string* mutable_file();
   protected:
    bool has_file_ = false;
    ::std::string file_;
      
      // optional field line
     public:
    bool has_line() const;
    const int64_t& line() const;
    void clear_line();
    void set_line(const int64_t& value);
    int64_t* mutable_line();
   protected:
    bool has_line_ = false;
    int64_t line_;
      
      // optional field function
     public:
    bool has_function() const;
    const ::std::string& function() const;
    void clear_function();
    void set_function(const ::std::string& value);
    ::std::string* mutable_function();
   protected:
    bool has_function_ = false;
    ::std::string function_;
      
      // optional field error_msg
     public:
    bool has_error_msg() const;
    const ::std::string& error_msg() const;
    void clear_error_msg();
    void set_error_msg(const ::std::string& value);
    ::std::string* mutable_error_msg();
   protected:
    bool has_error_msg_ = false;
    ::std::string error_msg_;
           
   public:
    int compare(const _ErrorStackFrame_& other);

    bool operator==(const _ErrorStackFrame_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ErrorStackFrame_& other) const;
  };

  ConstErrorStackFrame(const ::std::shared_ptr<_ErrorStackFrame_>& data);
  ConstErrorStackFrame(const ConstErrorStackFrame&);
  ConstErrorStackFrame(ConstErrorStackFrame&&) noexcept;
  ConstErrorStackFrame();
  ConstErrorStackFrame(const ::oneflow::ErrorStackFrame& proto_errorstackframe);
  virtual ~ConstErrorStackFrame() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_errorstackframe) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field file
 public:
  bool has_file() const;
  const ::std::string& file() const;
  // used by pybind11 only
  // required or optional field line
 public:
  bool has_line() const;
  const int64_t& line() const;
  // used by pybind11 only
  // required or optional field function
 public:
  bool has_function() const;
  const ::std::string& function() const;
  // used by pybind11 only
  // required or optional field error_msg
 public:
  bool has_error_msg() const;
  const ::std::string& error_msg() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstErrorStackFrame> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstErrorStackFrame& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstErrorStackFrame& other) const;
 protected:
  const ::std::shared_ptr<_ErrorStackFrame_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ErrorStackFrame_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstErrorStackFrame
  void BuildFromProto(const PbMessage& proto_errorstackframe);
  
  ::std::shared_ptr<_ErrorStackFrame_> data_;
};

class ErrorStackFrame final : public ConstErrorStackFrame {
 public:
  ErrorStackFrame(const ::std::shared_ptr<_ErrorStackFrame_>& data);
  ErrorStackFrame(const ErrorStackFrame& other);
  // enable nothrow for ::std::vector<ErrorStackFrame> resize 
  ErrorStackFrame(ErrorStackFrame&&) noexcept;
  ErrorStackFrame();
  explicit ErrorStackFrame(const ::oneflow::ErrorStackFrame& proto_errorstackframe);

  ~ErrorStackFrame() override;

  void InitFromProto(const PbMessage& proto_errorstackframe) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ErrorStackFrame& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ErrorStackFrame& other) const;
  void Clear();
  void CopyFrom(const ErrorStackFrame& other);
  ErrorStackFrame& operator=(const ErrorStackFrame& other);

  // required or optional field file
 public:
  void clear_file();
  void set_file(const ::std::string& value);
  ::std::string* mutable_file();
  // required or optional field line
 public:
  void clear_line();
  void set_line(const int64_t& value);
  int64_t* mutable_line();
  // required or optional field function
 public:
  void clear_function();
  void set_function(const ::std::string& value);
  ::std::string* mutable_function();
  // required or optional field error_msg
 public:
  void clear_error_msg();
  void set_error_msg(const ::std::string& value);
  ::std::string* mutable_error_msg();

  ::std::shared_ptr<ErrorStackFrame> __SharedMutable__();
};


class ConstSymbolIdUninitializedError : public ::oneflow::cfg::Message {
 public:

  class _SymbolIdUninitializedError_ {
   public:
    _SymbolIdUninitializedError_();
    explicit _SymbolIdUninitializedError_(const _SymbolIdUninitializedError_& other);
    explicit _SymbolIdUninitializedError_(_SymbolIdUninitializedError_&& other);
    _SymbolIdUninitializedError_(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror);
    ~_SymbolIdUninitializedError_();

    void InitFromProto(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror);

    void ToProto(::oneflow::SymbolIdUninitializedError* proto_symboliduninitializederror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SymbolIdUninitializedError_& other);
       
   public:
    int compare(const _SymbolIdUninitializedError_& other);

    bool operator==(const _SymbolIdUninitializedError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SymbolIdUninitializedError_& other) const;
  };

  ConstSymbolIdUninitializedError(const ::std::shared_ptr<_SymbolIdUninitializedError_>& data);
  ConstSymbolIdUninitializedError(const ConstSymbolIdUninitializedError&);
  ConstSymbolIdUninitializedError(ConstSymbolIdUninitializedError&&) noexcept;
  ConstSymbolIdUninitializedError();
  ConstSymbolIdUninitializedError(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror);
  virtual ~ConstSymbolIdUninitializedError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_symboliduninitializederror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstSymbolIdUninitializedError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSymbolIdUninitializedError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSymbolIdUninitializedError& other) const;
 protected:
  const ::std::shared_ptr<_SymbolIdUninitializedError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SymbolIdUninitializedError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSymbolIdUninitializedError
  void BuildFromProto(const PbMessage& proto_symboliduninitializederror);
  
  ::std::shared_ptr<_SymbolIdUninitializedError_> data_;
};

class SymbolIdUninitializedError final : public ConstSymbolIdUninitializedError {
 public:
  SymbolIdUninitializedError(const ::std::shared_ptr<_SymbolIdUninitializedError_>& data);
  SymbolIdUninitializedError(const SymbolIdUninitializedError& other);
  // enable nothrow for ::std::vector<SymbolIdUninitializedError> resize 
  SymbolIdUninitializedError(SymbolIdUninitializedError&&) noexcept;
  SymbolIdUninitializedError();
  explicit SymbolIdUninitializedError(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror);

  ~SymbolIdUninitializedError() override;

  void InitFromProto(const PbMessage& proto_symboliduninitializederror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SymbolIdUninitializedError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SymbolIdUninitializedError& other) const;
  void Clear();
  void CopyFrom(const SymbolIdUninitializedError& other);
  SymbolIdUninitializedError& operator=(const SymbolIdUninitializedError& other);


  ::std::shared_ptr<SymbolIdUninitializedError> __SharedMutable__();
};


class ConstValueError : public ::oneflow::cfg::Message {
 public:

  class _ValueError_ {
   public:
    _ValueError_();
    explicit _ValueError_(const _ValueError_& other);
    explicit _ValueError_(_ValueError_&& other);
    _ValueError_(const ::oneflow::ValueError& proto_valueerror);
    ~_ValueError_();

    void InitFromProto(const ::oneflow::ValueError& proto_valueerror);

    void ToProto(::oneflow::ValueError* proto_valueerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ValueError_& other);
       
   public:
    int compare(const _ValueError_& other);

    bool operator==(const _ValueError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ValueError_& other) const;
  };

  ConstValueError(const ::std::shared_ptr<_ValueError_>& data);
  ConstValueError(const ConstValueError&);
  ConstValueError(ConstValueError&&) noexcept;
  ConstValueError();
  ConstValueError(const ::oneflow::ValueError& proto_valueerror);
  virtual ~ConstValueError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_valueerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstValueError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstValueError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstValueError& other) const;
 protected:
  const ::std::shared_ptr<_ValueError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ValueError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstValueError
  void BuildFromProto(const PbMessage& proto_valueerror);
  
  ::std::shared_ptr<_ValueError_> data_;
};

class ValueError final : public ConstValueError {
 public:
  ValueError(const ::std::shared_ptr<_ValueError_>& data);
  ValueError(const ValueError& other);
  // enable nothrow for ::std::vector<ValueError> resize 
  ValueError(ValueError&&) noexcept;
  ValueError();
  explicit ValueError(const ::oneflow::ValueError& proto_valueerror);

  ~ValueError() override;

  void InitFromProto(const PbMessage& proto_valueerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ValueError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ValueError& other) const;
  void Clear();
  void CopyFrom(const ValueError& other);
  ValueError& operator=(const ValueError& other);


  ::std::shared_ptr<ValueError> __SharedMutable__();
};


class ConstIndexError : public ::oneflow::cfg::Message {
 public:

  class _IndexError_ {
   public:
    _IndexError_();
    explicit _IndexError_(const _IndexError_& other);
    explicit _IndexError_(_IndexError_&& other);
    _IndexError_(const ::oneflow::IndexError& proto_indexerror);
    ~_IndexError_();

    void InitFromProto(const ::oneflow::IndexError& proto_indexerror);

    void ToProto(::oneflow::IndexError* proto_indexerror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _IndexError_& other);
       
   public:
    int compare(const _IndexError_& other);

    bool operator==(const _IndexError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _IndexError_& other) const;
  };

  ConstIndexError(const ::std::shared_ptr<_IndexError_>& data);
  ConstIndexError(const ConstIndexError&);
  ConstIndexError(ConstIndexError&&) noexcept;
  ConstIndexError();
  ConstIndexError(const ::oneflow::IndexError& proto_indexerror);
  virtual ~ConstIndexError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_indexerror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstIndexError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstIndexError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstIndexError& other) const;
 protected:
  const ::std::shared_ptr<_IndexError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_IndexError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstIndexError
  void BuildFromProto(const PbMessage& proto_indexerror);
  
  ::std::shared_ptr<_IndexError_> data_;
};

class IndexError final : public ConstIndexError {
 public:
  IndexError(const ::std::shared_ptr<_IndexError_>& data);
  IndexError(const IndexError& other);
  // enable nothrow for ::std::vector<IndexError> resize 
  IndexError(IndexError&&) noexcept;
  IndexError();
  explicit IndexError(const ::oneflow::IndexError& proto_indexerror);

  ~IndexError() override;

  void InitFromProto(const PbMessage& proto_indexerror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const IndexError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const IndexError& other) const;
  void Clear();
  void CopyFrom(const IndexError& other);
  IndexError& operator=(const IndexError& other);


  ::std::shared_ptr<IndexError> __SharedMutable__();
};


class ConstTimeoutError : public ::oneflow::cfg::Message {
 public:

  class _TimeoutError_ {
   public:
    _TimeoutError_();
    explicit _TimeoutError_(const _TimeoutError_& other);
    explicit _TimeoutError_(_TimeoutError_&& other);
    _TimeoutError_(const ::oneflow::TimeoutError& proto_timeouterror);
    ~_TimeoutError_();

    void InitFromProto(const ::oneflow::TimeoutError& proto_timeouterror);

    void ToProto(::oneflow::TimeoutError* proto_timeouterror) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TimeoutError_& other);
       
   public:
    int compare(const _TimeoutError_& other);

    bool operator==(const _TimeoutError_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TimeoutError_& other) const;
  };

  ConstTimeoutError(const ::std::shared_ptr<_TimeoutError_>& data);
  ConstTimeoutError(const ConstTimeoutError&);
  ConstTimeoutError(ConstTimeoutError&&) noexcept;
  ConstTimeoutError();
  ConstTimeoutError(const ::oneflow::TimeoutError& proto_timeouterror);
  virtual ~ConstTimeoutError() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_timeouterror) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstTimeoutError> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTimeoutError& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTimeoutError& other) const;
 protected:
  const ::std::shared_ptr<_TimeoutError_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TimeoutError_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTimeoutError
  void BuildFromProto(const PbMessage& proto_timeouterror);
  
  ::std::shared_ptr<_TimeoutError_> data_;
};

class TimeoutError final : public ConstTimeoutError {
 public:
  TimeoutError(const ::std::shared_ptr<_TimeoutError_>& data);
  TimeoutError(const TimeoutError& other);
  // enable nothrow for ::std::vector<TimeoutError> resize 
  TimeoutError(TimeoutError&&) noexcept;
  TimeoutError();
  explicit TimeoutError(const ::oneflow::TimeoutError& proto_timeouterror);

  ~TimeoutError() override;

  void InitFromProto(const PbMessage& proto_timeouterror) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TimeoutError& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TimeoutError& other) const;
  void Clear();
  void CopyFrom(const TimeoutError& other);
  TimeoutError& operator=(const TimeoutError& other);


  ::std::shared_ptr<TimeoutError> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_;
class Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_;

class ConstErrorProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum error_type
 enum ErrorTypeCase : unsigned int {
  ERROR_TYPE_NOT_SET = 0,
    kConfigAssertFailedError = 12,
    kConfigResourceUnavailableError = 13,
    kProtoParseFailedError = 15,
    kCheckFailedError = 16,
    kTodoError = 17,
    kUnimplementedError = 18,
    kBoxingNotSupportedError = 19,
    kGradientFunctionNotFoundError = 20,
    kOpKernelNotFoundError = 21,
    kMultipleOpKernelsMatchedError = 22,
    kMemoryZoneOutOfMemoryError = 23,
    kLossBlobNotFoundError = 24,
    kJobSetEmptyError = 25,
    kDeviceTagNotFoundError = 26,
    kValueError = 27,
    kIndexError = 28,
    kTimeoutError = 29,
    kJobNameExistError = 100,
    kJobNameEmptyError = 101,
    kJobNameNotEqualError = 102,
    kNoJobBuildAndInferCtxError = 200,
    kJobConfFrozenError = 300,
    kJobConfNotSetError = 301,
    kJobConfRepeatedSetError = 302,
    kJobTypeNotSetError = 303,
    kLogicalBlobNameNotExistError = 400,
    kLogicalBlobNameExistError = 401,
    kLogicalBlobNameInvalidError = 402,
    kOpNameExistError = 450,
    kOpConfDeviceTagNoSetError = 460,
    kPlacementError = 470,
    kBlobSplitAxisInferError = 480,
    kUnknownJobBuildAndInferError = 500,
    kRwMutexedObjectNotFoundError = 600,
    kSymbolIdUninitializedError = 700,
    kUnknownError = 900,
    kCompileOptionWrongError = 950,
    kInputDeviceNotMatchError = 1000,
   };

  class _ErrorProto_ {
   public:
    _ErrorProto_();
    explicit _ErrorProto_(const _ErrorProto_& other);
    explicit _ErrorProto_(_ErrorProto_&& other);
    _ErrorProto_(const ::oneflow::ErrorProto& proto_errorproto);
    ~_ErrorProto_();

    void InitFromProto(const ::oneflow::ErrorProto& proto_errorproto);

    void ToProto(::oneflow::ErrorProto* proto_errorproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ErrorProto_& other);
  
      // optional field error_summary
     public:
    bool has_error_summary() const;
    const ::std::string& error_summary() const;
    void clear_error_summary();
    void set_error_summary(const ::std::string& value);
    ::std::string* mutable_error_summary();
   protected:
    bool has_error_summary_ = false;
    ::std::string error_summary_;
      
      // optional field msg
     public:
    bool has_msg() const;
    const ::std::string& msg() const;
    void clear_msg();
    void set_msg(const ::std::string& value);
    ::std::string* mutable_msg();
   protected:
    bool has_msg_ = false;
    ::std::string msg_;
      
      // repeated field stack_frame
   public:
    ::std::size_t stack_frame_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& stack_frame() const;
    const ::oneflow::cfg::ErrorStackFrame& stack_frame(::std::size_t index) const;
    void clear_stack_frame();
    _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_* mutable_stack_frame();
    ::oneflow::cfg::ErrorStackFrame* mutable_stack_frame(::std::size_t index);
      ::oneflow::cfg::ErrorStackFrame* add_stack_frame();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> stack_frame_;
    
     // oneof field error_type: config_assert_failed_error
   public:
    bool has_config_assert_failed_error() const;
    void clear_config_assert_failed_error();
    const ::oneflow::cfg::ConfigAssertFailedError& config_assert_failed_error() const;
      ::oneflow::cfg::ConfigAssertFailedError* mutable_config_assert_failed_error();
      
     // oneof field error_type: config_resource_unavailable_error
   public:
    bool has_config_resource_unavailable_error() const;
    void clear_config_resource_unavailable_error();
    const ::oneflow::cfg::ConfigResourceUnavailableError& config_resource_unavailable_error() const;
      ::oneflow::cfg::ConfigResourceUnavailableError* mutable_config_resource_unavailable_error();
      
     // oneof field error_type: proto_parse_failed_error
   public:
    bool has_proto_parse_failed_error() const;
    void clear_proto_parse_failed_error();
    const ::oneflow::cfg::ProtoParseFailedError& proto_parse_failed_error() const;
      ::oneflow::cfg::ProtoParseFailedError* mutable_proto_parse_failed_error();
      
     // oneof field error_type: check_failed_error
   public:
    bool has_check_failed_error() const;
    void clear_check_failed_error();
    const ::oneflow::cfg::CheckFailedError& check_failed_error() const;
      ::oneflow::cfg::CheckFailedError* mutable_check_failed_error();
      
     // oneof field error_type: todo_error
   public:
    bool has_todo_error() const;
    void clear_todo_error();
    const ::oneflow::cfg::TodoError& todo_error() const;
      ::oneflow::cfg::TodoError* mutable_todo_error();
      
     // oneof field error_type: unimplemented_error
   public:
    bool has_unimplemented_error() const;
    void clear_unimplemented_error();
    const ::oneflow::cfg::UnimplementedError& unimplemented_error() const;
      ::oneflow::cfg::UnimplementedError* mutable_unimplemented_error();
      
     // oneof field error_type: boxing_not_supported_error
   public:
    bool has_boxing_not_supported_error() const;
    void clear_boxing_not_supported_error();
    const ::oneflow::cfg::BoxingNotSupportedError& boxing_not_supported_error() const;
      ::oneflow::cfg::BoxingNotSupportedError* mutable_boxing_not_supported_error();
      
     // oneof field error_type: gradient_function_not_found_error
   public:
    bool has_gradient_function_not_found_error() const;
    void clear_gradient_function_not_found_error();
    const ::oneflow::cfg::GradientFunctionNotFoundError& gradient_function_not_found_error() const;
      ::oneflow::cfg::GradientFunctionNotFoundError* mutable_gradient_function_not_found_error();
      
     // oneof field error_type: op_kernel_not_found_error
   public:
    bool has_op_kernel_not_found_error() const;
    void clear_op_kernel_not_found_error();
    const ::oneflow::cfg::OpKernelNotFoundError& op_kernel_not_found_error() const;
      ::oneflow::cfg::OpKernelNotFoundError* mutable_op_kernel_not_found_error();
      
     // oneof field error_type: multiple_op_kernels_matched_error
   public:
    bool has_multiple_op_kernels_matched_error() const;
    void clear_multiple_op_kernels_matched_error();
    const ::oneflow::cfg::MultipleOpKernelsMatchedError& multiple_op_kernels_matched_error() const;
      ::oneflow::cfg::MultipleOpKernelsMatchedError* mutable_multiple_op_kernels_matched_error();
      
     // oneof field error_type: memory_zone_out_of_memory_error
   public:
    bool has_memory_zone_out_of_memory_error() const;
    void clear_memory_zone_out_of_memory_error();
    const ::oneflow::cfg::MemoryZoneOutOfMemoryError& memory_zone_out_of_memory_error() const;
      ::oneflow::cfg::MemoryZoneOutOfMemoryError* mutable_memory_zone_out_of_memory_error();
      
     // oneof field error_type: loss_blob_not_found_error
   public:
    bool has_loss_blob_not_found_error() const;
    void clear_loss_blob_not_found_error();
    const ::oneflow::cfg::LossBlobNotFoundError& loss_blob_not_found_error() const;
      ::oneflow::cfg::LossBlobNotFoundError* mutable_loss_blob_not_found_error();
      
     // oneof field error_type: job_set_empty_error
   public:
    bool has_job_set_empty_error() const;
    void clear_job_set_empty_error();
    const ::oneflow::cfg::JobSetEmptyError& job_set_empty_error() const;
      ::oneflow::cfg::JobSetEmptyError* mutable_job_set_empty_error();
      
     // oneof field error_type: device_tag_not_found_error
   public:
    bool has_device_tag_not_found_error() const;
    void clear_device_tag_not_found_error();
    const ::oneflow::cfg::DeviceTagNotFoundError& device_tag_not_found_error() const;
      ::oneflow::cfg::DeviceTagNotFoundError* mutable_device_tag_not_found_error();
      
     // oneof field error_type: value_error
   public:
    bool has_value_error() const;
    void clear_value_error();
    const ::oneflow::cfg::ValueError& value_error() const;
      ::oneflow::cfg::ValueError* mutable_value_error();
      
     // oneof field error_type: index_error
   public:
    bool has_index_error() const;
    void clear_index_error();
    const ::oneflow::cfg::IndexError& index_error() const;
      ::oneflow::cfg::IndexError* mutable_index_error();
      
     // oneof field error_type: timeout_error
   public:
    bool has_timeout_error() const;
    void clear_timeout_error();
    const ::oneflow::cfg::TimeoutError& timeout_error() const;
      ::oneflow::cfg::TimeoutError* mutable_timeout_error();
      
     // oneof field error_type: job_name_exist_error
   public:
    bool has_job_name_exist_error() const;
    void clear_job_name_exist_error();
    const ::oneflow::cfg::JobNameExistError& job_name_exist_error() const;
      ::oneflow::cfg::JobNameExistError* mutable_job_name_exist_error();
      
     // oneof field error_type: job_name_empty_error
   public:
    bool has_job_name_empty_error() const;
    void clear_job_name_empty_error();
    const ::oneflow::cfg::JobNameEmptyError& job_name_empty_error() const;
      ::oneflow::cfg::JobNameEmptyError* mutable_job_name_empty_error();
      
     // oneof field error_type: job_name_not_equal_error
   public:
    bool has_job_name_not_equal_error() const;
    void clear_job_name_not_equal_error();
    const ::oneflow::cfg::JobNameNotEqualError& job_name_not_equal_error() const;
      ::oneflow::cfg::JobNameNotEqualError* mutable_job_name_not_equal_error();
      
     // oneof field error_type: no_job_build_and_infer_ctx_error
   public:
    bool has_no_job_build_and_infer_ctx_error() const;
    void clear_no_job_build_and_infer_ctx_error();
    const ::oneflow::cfg::NoJobBuildAndInferCtxError& no_job_build_and_infer_ctx_error() const;
      ::oneflow::cfg::NoJobBuildAndInferCtxError* mutable_no_job_build_and_infer_ctx_error();
      
     // oneof field error_type: job_conf_frozen_error
   public:
    bool has_job_conf_frozen_error() const;
    void clear_job_conf_frozen_error();
    const ::oneflow::cfg::JobConfFrozenError& job_conf_frozen_error() const;
      ::oneflow::cfg::JobConfFrozenError* mutable_job_conf_frozen_error();
      
     // oneof field error_type: job_conf_not_set_error
   public:
    bool has_job_conf_not_set_error() const;
    void clear_job_conf_not_set_error();
    const ::oneflow::cfg::JobConfNotSetError& job_conf_not_set_error() const;
      ::oneflow::cfg::JobConfNotSetError* mutable_job_conf_not_set_error();
      
     // oneof field error_type: job_conf_repeated_set_error
   public:
    bool has_job_conf_repeated_set_error() const;
    void clear_job_conf_repeated_set_error();
    const ::oneflow::cfg::JobConfRepeatedSetError& job_conf_repeated_set_error() const;
      ::oneflow::cfg::JobConfRepeatedSetError* mutable_job_conf_repeated_set_error();
      
     // oneof field error_type: job_type_not_set_error
   public:
    bool has_job_type_not_set_error() const;
    void clear_job_type_not_set_error();
    const ::oneflow::cfg::JobTypeNotSetError& job_type_not_set_error() const;
      ::oneflow::cfg::JobTypeNotSetError* mutable_job_type_not_set_error();
      
     // oneof field error_type: logical_blob_name_not_exist_error
   public:
    bool has_logical_blob_name_not_exist_error() const;
    void clear_logical_blob_name_not_exist_error();
    const ::oneflow::cfg::LogicalBlobNameNotExistError& logical_blob_name_not_exist_error() const;
      ::oneflow::cfg::LogicalBlobNameNotExistError* mutable_logical_blob_name_not_exist_error();
      
     // oneof field error_type: logical_blob_name_exist_error
   public:
    bool has_logical_blob_name_exist_error() const;
    void clear_logical_blob_name_exist_error();
    const ::oneflow::cfg::LogicalBlobNameExistError& logical_blob_name_exist_error() const;
      ::oneflow::cfg::LogicalBlobNameExistError* mutable_logical_blob_name_exist_error();
      
     // oneof field error_type: logical_blob_name_invalid_error
   public:
    bool has_logical_blob_name_invalid_error() const;
    void clear_logical_blob_name_invalid_error();
    const ::oneflow::cfg::LogicalBlobNameInvalidError& logical_blob_name_invalid_error() const;
      ::oneflow::cfg::LogicalBlobNameInvalidError* mutable_logical_blob_name_invalid_error();
      
     // oneof field error_type: op_name_exist_error
   public:
    bool has_op_name_exist_error() const;
    void clear_op_name_exist_error();
    const ::oneflow::cfg::OpNameExistError& op_name_exist_error() const;
      ::oneflow::cfg::OpNameExistError* mutable_op_name_exist_error();
      
     // oneof field error_type: op_conf_device_tag_no_set_error
   public:
    bool has_op_conf_device_tag_no_set_error() const;
    void clear_op_conf_device_tag_no_set_error();
    const ::oneflow::cfg::OpConfDeviceTagNoSetError& op_conf_device_tag_no_set_error() const;
      ::oneflow::cfg::OpConfDeviceTagNoSetError* mutable_op_conf_device_tag_no_set_error();
      
     // oneof field error_type: placement_error
   public:
    bool has_placement_error() const;
    void clear_placement_error();
    const ::oneflow::cfg::PlacementError& placement_error() const;
      ::oneflow::cfg::PlacementError* mutable_placement_error();
      
     // oneof field error_type: blob_split_axis_infer_error
   public:
    bool has_blob_split_axis_infer_error() const;
    void clear_blob_split_axis_infer_error();
    const ::oneflow::cfg::BlobSplitAxisInferError& blob_split_axis_infer_error() const;
      ::oneflow::cfg::BlobSplitAxisInferError* mutable_blob_split_axis_infer_error();
      
     // oneof field error_type: unknown_job_build_and_infer_error
   public:
    bool has_unknown_job_build_and_infer_error() const;
    void clear_unknown_job_build_and_infer_error();
    const ::oneflow::cfg::UnknownJobBuildAndInferError& unknown_job_build_and_infer_error() const;
      ::oneflow::cfg::UnknownJobBuildAndInferError* mutable_unknown_job_build_and_infer_error();
      
     // oneof field error_type: rw_mutexed_object_not_found_error
   public:
    bool has_rw_mutexed_object_not_found_error() const;
    void clear_rw_mutexed_object_not_found_error();
    const ::oneflow::cfg::RwMutexedObjectNotFoundError& rw_mutexed_object_not_found_error() const;
      ::oneflow::cfg::RwMutexedObjectNotFoundError* mutable_rw_mutexed_object_not_found_error();
      
     // oneof field error_type: symbol_id_uninitialized_error
   public:
    bool has_symbol_id_uninitialized_error() const;
    void clear_symbol_id_uninitialized_error();
    const ::oneflow::cfg::SymbolIdUninitializedError& symbol_id_uninitialized_error() const;
      ::oneflow::cfg::SymbolIdUninitializedError* mutable_symbol_id_uninitialized_error();
      
     // oneof field error_type: unknown_error
   public:
    bool has_unknown_error() const;
    void clear_unknown_error();
    const ::oneflow::cfg::UnknownError& unknown_error() const;
      ::oneflow::cfg::UnknownError* mutable_unknown_error();
      
     // oneof field error_type: compile_option_wrong_error
   public:
    bool has_compile_option_wrong_error() const;
    void clear_compile_option_wrong_error();
    const ::oneflow::cfg::CompileOptionWrongError& compile_option_wrong_error() const;
      ::oneflow::cfg::CompileOptionWrongError* mutable_compile_option_wrong_error();
      
     // oneof field error_type: input_device_not_match_error
   public:
    bool has_input_device_not_match_error() const;
    void clear_input_device_not_match_error();
    const ::oneflow::cfg::InputDeviceNotMatchError& input_device_not_match_error() const;
      ::oneflow::cfg::InputDeviceNotMatchError* mutable_input_device_not_match_error();
           
   public:
    // oneof error_type
    ErrorTypeCase error_type_case() const;
    bool has_error_type() const;
   protected:
    void clear_error_type();
    void error_type_copy_from(const _ErrorProto_& other);
    union ErrorTypeUnion {
      // 64-bit aligned
      uint64_t __error_type_for_padding_64bit__;
          char config_assert_failed_error_[sizeof(::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>)];
            char config_resource_unavailable_error_[sizeof(::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>)];
            char proto_parse_failed_error_[sizeof(::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>)];
            char check_failed_error_[sizeof(::std::shared_ptr<::oneflow::cfg::CheckFailedError>)];
            char todo_error_[sizeof(::std::shared_ptr<::oneflow::cfg::TodoError>)];
            char unimplemented_error_[sizeof(::std::shared_ptr<::oneflow::cfg::UnimplementedError>)];
            char boxing_not_supported_error_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>)];
            char gradient_function_not_found_error_[sizeof(::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>)];
            char op_kernel_not_found_error_[sizeof(::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>)];
            char multiple_op_kernels_matched_error_[sizeof(::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>)];
            char memory_zone_out_of_memory_error_[sizeof(::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>)];
            char loss_blob_not_found_error_[sizeof(::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>)];
            char job_set_empty_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>)];
            char device_tag_not_found_error_[sizeof(::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>)];
            char value_error_[sizeof(::std::shared_ptr<::oneflow::cfg::ValueError>)];
            char index_error_[sizeof(::std::shared_ptr<::oneflow::cfg::IndexError>)];
            char timeout_error_[sizeof(::std::shared_ptr<::oneflow::cfg::TimeoutError>)];
            char job_name_exist_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobNameExistError>)];
            char job_name_empty_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>)];
            char job_name_not_equal_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>)];
            char no_job_build_and_infer_ctx_error_[sizeof(::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>)];
            char job_conf_frozen_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>)];
            char job_conf_not_set_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>)];
            char job_conf_repeated_set_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>)];
            char job_type_not_set_error_[sizeof(::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>)];
            char logical_blob_name_not_exist_error_[sizeof(::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>)];
            char logical_blob_name_exist_error_[sizeof(::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>)];
            char logical_blob_name_invalid_error_[sizeof(::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>)];
            char op_name_exist_error_[sizeof(::std::shared_ptr<::oneflow::cfg::OpNameExistError>)];
            char op_conf_device_tag_no_set_error_[sizeof(::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>)];
            char placement_error_[sizeof(::std::shared_ptr<::oneflow::cfg::PlacementError>)];
            char blob_split_axis_infer_error_[sizeof(::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>)];
            char unknown_job_build_and_infer_error_[sizeof(::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>)];
            char rw_mutexed_object_not_found_error_[sizeof(::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>)];
            char symbol_id_uninitialized_error_[sizeof(::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>)];
            char unknown_error_[sizeof(::std::shared_ptr<::oneflow::cfg::UnknownError>)];
            char compile_option_wrong_error_[sizeof(::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>)];
            char input_device_not_match_error_[sizeof(::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>)];
        } error_type_;
    ErrorTypeCase error_type_case_ = ERROR_TYPE_NOT_SET;
     
   public:
    int compare(const _ErrorProto_& other);

    bool operator==(const _ErrorProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ErrorProto_& other) const;
  };

  ConstErrorProto(const ::std::shared_ptr<_ErrorProto_>& data);
  ConstErrorProto(const ConstErrorProto&);
  ConstErrorProto(ConstErrorProto&&) noexcept;
  ConstErrorProto();
  ConstErrorProto(const ::oneflow::ErrorProto& proto_errorproto);
  virtual ~ConstErrorProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_errorproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field error_summary
 public:
  bool has_error_summary() const;
  const ::std::string& error_summary() const;
  // used by pybind11 only
  // required or optional field msg
 public:
  bool has_msg() const;
  const ::std::string& msg() const;
  // used by pybind11 only
  // repeated field stack_frame
 public:
  ::std::size_t stack_frame_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& stack_frame() const;
  const ::oneflow::cfg::ErrorStackFrame& stack_frame(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> shared_const_stack_frame() const;
  ::std::shared_ptr<::oneflow::cfg::ConstErrorStackFrame> shared_const_stack_frame(::std::size_t index) const;
 // oneof field error_type: config_assert_failed_error
 public:
  bool has_config_assert_failed_error() const;
  const ::oneflow::cfg::ConfigAssertFailedError& config_assert_failed_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConfigAssertFailedError> shared_const_config_assert_failed_error() const;
 // oneof field error_type: config_resource_unavailable_error
 public:
  bool has_config_resource_unavailable_error() const;
  const ::oneflow::cfg::ConfigResourceUnavailableError& config_resource_unavailable_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConfigResourceUnavailableError> shared_const_config_resource_unavailable_error() const;
 // oneof field error_type: proto_parse_failed_error
 public:
  bool has_proto_parse_failed_error() const;
  const ::oneflow::cfg::ProtoParseFailedError& proto_parse_failed_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstProtoParseFailedError> shared_const_proto_parse_failed_error() const;
 // oneof field error_type: check_failed_error
 public:
  bool has_check_failed_error() const;
  const ::oneflow::cfg::CheckFailedError& check_failed_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCheckFailedError> shared_const_check_failed_error() const;
 // oneof field error_type: todo_error
 public:
  bool has_todo_error() const;
  const ::oneflow::cfg::TodoError& todo_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTodoError> shared_const_todo_error() const;
 // oneof field error_type: unimplemented_error
 public:
  bool has_unimplemented_error() const;
  const ::oneflow::cfg::UnimplementedError& unimplemented_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstUnimplementedError> shared_const_unimplemented_error() const;
 // oneof field error_type: boxing_not_supported_error
 public:
  bool has_boxing_not_supported_error() const;
  const ::oneflow::cfg::BoxingNotSupportedError& boxing_not_supported_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxingNotSupportedError> shared_const_boxing_not_supported_error() const;
 // oneof field error_type: gradient_function_not_found_error
 public:
  bool has_gradient_function_not_found_error() const;
  const ::oneflow::cfg::GradientFunctionNotFoundError& gradient_function_not_found_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstGradientFunctionNotFoundError> shared_const_gradient_function_not_found_error() const;
 // oneof field error_type: op_kernel_not_found_error
 public:
  bool has_op_kernel_not_found_error() const;
  const ::oneflow::cfg::OpKernelNotFoundError& op_kernel_not_found_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpKernelNotFoundError> shared_const_op_kernel_not_found_error() const;
 // oneof field error_type: multiple_op_kernels_matched_error
 public:
  bool has_multiple_op_kernels_matched_error() const;
  const ::oneflow::cfg::MultipleOpKernelsMatchedError& multiple_op_kernels_matched_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMultipleOpKernelsMatchedError> shared_const_multiple_op_kernels_matched_error() const;
 // oneof field error_type: memory_zone_out_of_memory_error
 public:
  bool has_memory_zone_out_of_memory_error() const;
  const ::oneflow::cfg::MemoryZoneOutOfMemoryError& memory_zone_out_of_memory_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMemoryZoneOutOfMemoryError> shared_const_memory_zone_out_of_memory_error() const;
 // oneof field error_type: loss_blob_not_found_error
 public:
  bool has_loss_blob_not_found_error() const;
  const ::oneflow::cfg::LossBlobNotFoundError& loss_blob_not_found_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLossBlobNotFoundError> shared_const_loss_blob_not_found_error() const;
 // oneof field error_type: job_set_empty_error
 public:
  bool has_job_set_empty_error() const;
  const ::oneflow::cfg::JobSetEmptyError& job_set_empty_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobSetEmptyError> shared_const_job_set_empty_error() const;
 // oneof field error_type: device_tag_not_found_error
 public:
  bool has_device_tag_not_found_error() const;
  const ::oneflow::cfg::DeviceTagNotFoundError& device_tag_not_found_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDeviceTagNotFoundError> shared_const_device_tag_not_found_error() const;
 // oneof field error_type: value_error
 public:
  bool has_value_error() const;
  const ::oneflow::cfg::ValueError& value_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstValueError> shared_const_value_error() const;
 // oneof field error_type: index_error
 public:
  bool has_index_error() const;
  const ::oneflow::cfg::IndexError& index_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstIndexError> shared_const_index_error() const;
 // oneof field error_type: timeout_error
 public:
  bool has_timeout_error() const;
  const ::oneflow::cfg::TimeoutError& timeout_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTimeoutError> shared_const_timeout_error() const;
 // oneof field error_type: job_name_exist_error
 public:
  bool has_job_name_exist_error() const;
  const ::oneflow::cfg::JobNameExistError& job_name_exist_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobNameExistError> shared_const_job_name_exist_error() const;
 // oneof field error_type: job_name_empty_error
 public:
  bool has_job_name_empty_error() const;
  const ::oneflow::cfg::JobNameEmptyError& job_name_empty_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobNameEmptyError> shared_const_job_name_empty_error() const;
 // oneof field error_type: job_name_not_equal_error
 public:
  bool has_job_name_not_equal_error() const;
  const ::oneflow::cfg::JobNameNotEqualError& job_name_not_equal_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobNameNotEqualError> shared_const_job_name_not_equal_error() const;
 // oneof field error_type: no_job_build_and_infer_ctx_error
 public:
  bool has_no_job_build_and_infer_ctx_error() const;
  const ::oneflow::cfg::NoJobBuildAndInferCtxError& no_job_build_and_infer_ctx_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstNoJobBuildAndInferCtxError> shared_const_no_job_build_and_infer_ctx_error() const;
 // oneof field error_type: job_conf_frozen_error
 public:
  bool has_job_conf_frozen_error() const;
  const ::oneflow::cfg::JobConfFrozenError& job_conf_frozen_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobConfFrozenError> shared_const_job_conf_frozen_error() const;
 // oneof field error_type: job_conf_not_set_error
 public:
  bool has_job_conf_not_set_error() const;
  const ::oneflow::cfg::JobConfNotSetError& job_conf_not_set_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobConfNotSetError> shared_const_job_conf_not_set_error() const;
 // oneof field error_type: job_conf_repeated_set_error
 public:
  bool has_job_conf_repeated_set_error() const;
  const ::oneflow::cfg::JobConfRepeatedSetError& job_conf_repeated_set_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobConfRepeatedSetError> shared_const_job_conf_repeated_set_error() const;
 // oneof field error_type: job_type_not_set_error
 public:
  bool has_job_type_not_set_error() const;
  const ::oneflow::cfg::JobTypeNotSetError& job_type_not_set_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobTypeNotSetError> shared_const_job_type_not_set_error() const;
 // oneof field error_type: logical_blob_name_not_exist_error
 public:
  bool has_logical_blob_name_not_exist_error() const;
  const ::oneflow::cfg::LogicalBlobNameNotExistError& logical_blob_name_not_exist_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameNotExistError> shared_const_logical_blob_name_not_exist_error() const;
 // oneof field error_type: logical_blob_name_exist_error
 public:
  bool has_logical_blob_name_exist_error() const;
  const ::oneflow::cfg::LogicalBlobNameExistError& logical_blob_name_exist_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameExistError> shared_const_logical_blob_name_exist_error() const;
 // oneof field error_type: logical_blob_name_invalid_error
 public:
  bool has_logical_blob_name_invalid_error() const;
  const ::oneflow::cfg::LogicalBlobNameInvalidError& logical_blob_name_invalid_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameInvalidError> shared_const_logical_blob_name_invalid_error() const;
 // oneof field error_type: op_name_exist_error
 public:
  bool has_op_name_exist_error() const;
  const ::oneflow::cfg::OpNameExistError& op_name_exist_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpNameExistError> shared_const_op_name_exist_error() const;
 // oneof field error_type: op_conf_device_tag_no_set_error
 public:
  bool has_op_conf_device_tag_no_set_error() const;
  const ::oneflow::cfg::OpConfDeviceTagNoSetError& op_conf_device_tag_no_set_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpConfDeviceTagNoSetError> shared_const_op_conf_device_tag_no_set_error() const;
 // oneof field error_type: placement_error
 public:
  bool has_placement_error() const;
  const ::oneflow::cfg::PlacementError& placement_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPlacementError> shared_const_placement_error() const;
 // oneof field error_type: blob_split_axis_infer_error
 public:
  bool has_blob_split_axis_infer_error() const;
  const ::oneflow::cfg::BlobSplitAxisInferError& blob_split_axis_infer_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobSplitAxisInferError> shared_const_blob_split_axis_infer_error() const;
 // oneof field error_type: unknown_job_build_and_infer_error
 public:
  bool has_unknown_job_build_and_infer_error() const;
  const ::oneflow::cfg::UnknownJobBuildAndInferError& unknown_job_build_and_infer_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstUnknownJobBuildAndInferError> shared_const_unknown_job_build_and_infer_error() const;
 // oneof field error_type: rw_mutexed_object_not_found_error
 public:
  bool has_rw_mutexed_object_not_found_error() const;
  const ::oneflow::cfg::RwMutexedObjectNotFoundError& rw_mutexed_object_not_found_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRwMutexedObjectNotFoundError> shared_const_rw_mutexed_object_not_found_error() const;
 // oneof field error_type: symbol_id_uninitialized_error
 public:
  bool has_symbol_id_uninitialized_error() const;
  const ::oneflow::cfg::SymbolIdUninitializedError& symbol_id_uninitialized_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSymbolIdUninitializedError> shared_const_symbol_id_uninitialized_error() const;
 // oneof field error_type: unknown_error
 public:
  bool has_unknown_error() const;
  const ::oneflow::cfg::UnknownError& unknown_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstUnknownError> shared_const_unknown_error() const;
 // oneof field error_type: compile_option_wrong_error
 public:
  bool has_compile_option_wrong_error() const;
  const ::oneflow::cfg::CompileOptionWrongError& compile_option_wrong_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCompileOptionWrongError> shared_const_compile_option_wrong_error() const;
 // oneof field error_type: input_device_not_match_error
 public:
  bool has_input_device_not_match_error() const;
  const ::oneflow::cfg::InputDeviceNotMatchError& input_device_not_match_error() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInputDeviceNotMatchError> shared_const_input_device_not_match_error() const;
 public:
  ErrorTypeCase error_type_case() const;
 protected:
  bool has_error_type() const;

 public:
  ::std::shared_ptr<ConstErrorProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstErrorProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstErrorProto& other) const;
 protected:
  const ::std::shared_ptr<_ErrorProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ErrorProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstErrorProto
  void BuildFromProto(const PbMessage& proto_errorproto);
  
  ::std::shared_ptr<_ErrorProto_> data_;
};

class ErrorProto final : public ConstErrorProto {
 public:
  ErrorProto(const ::std::shared_ptr<_ErrorProto_>& data);
  ErrorProto(const ErrorProto& other);
  // enable nothrow for ::std::vector<ErrorProto> resize 
  ErrorProto(ErrorProto&&) noexcept;
  ErrorProto();
  explicit ErrorProto(const ::oneflow::ErrorProto& proto_errorproto);

  ~ErrorProto() override;

  void InitFromProto(const PbMessage& proto_errorproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ErrorProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ErrorProto& other) const;
  void Clear();
  void CopyFrom(const ErrorProto& other);
  ErrorProto& operator=(const ErrorProto& other);

  // required or optional field error_summary
 public:
  void clear_error_summary();
  void set_error_summary(const ::std::string& value);
  ::std::string* mutable_error_summary();
  // required or optional field msg
 public:
  void clear_msg();
  void set_msg(const ::std::string& value);
  ::std::string* mutable_msg();
  // repeated field stack_frame
 public:
  void clear_stack_frame();
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_* mutable_stack_frame();
  ::oneflow::cfg::ErrorStackFrame* mutable_stack_frame(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> shared_mutable_stack_frame();
  ::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> shared_mutable_stack_frame(::std::size_t index);
  ::oneflow::cfg::ErrorStackFrame* add_stack_frame();
  void clear_config_assert_failed_error();
  ::oneflow::cfg::ConfigAssertFailedError* mutable_config_assert_failed_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError> shared_mutable_config_assert_failed_error();
  void clear_config_resource_unavailable_error();
  ::oneflow::cfg::ConfigResourceUnavailableError* mutable_config_resource_unavailable_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError> shared_mutable_config_resource_unavailable_error();
  void clear_proto_parse_failed_error();
  ::oneflow::cfg::ProtoParseFailedError* mutable_proto_parse_failed_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError> shared_mutable_proto_parse_failed_error();
  void clear_check_failed_error();
  ::oneflow::cfg::CheckFailedError* mutable_check_failed_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CheckFailedError> shared_mutable_check_failed_error();
  void clear_todo_error();
  ::oneflow::cfg::TodoError* mutable_todo_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TodoError> shared_mutable_todo_error();
  void clear_unimplemented_error();
  ::oneflow::cfg::UnimplementedError* mutable_unimplemented_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::UnimplementedError> shared_mutable_unimplemented_error();
  void clear_boxing_not_supported_error();
  ::oneflow::cfg::BoxingNotSupportedError* mutable_boxing_not_supported_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError> shared_mutable_boxing_not_supported_error();
  void clear_gradient_function_not_found_error();
  ::oneflow::cfg::GradientFunctionNotFoundError* mutable_gradient_function_not_found_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError> shared_mutable_gradient_function_not_found_error();
  void clear_op_kernel_not_found_error();
  ::oneflow::cfg::OpKernelNotFoundError* mutable_op_kernel_not_found_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError> shared_mutable_op_kernel_not_found_error();
  void clear_multiple_op_kernels_matched_error();
  ::oneflow::cfg::MultipleOpKernelsMatchedError* mutable_multiple_op_kernels_matched_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError> shared_mutable_multiple_op_kernels_matched_error();
  void clear_memory_zone_out_of_memory_error();
  ::oneflow::cfg::MemoryZoneOutOfMemoryError* mutable_memory_zone_out_of_memory_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError> shared_mutable_memory_zone_out_of_memory_error();
  void clear_loss_blob_not_found_error();
  ::oneflow::cfg::LossBlobNotFoundError* mutable_loss_blob_not_found_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError> shared_mutable_loss_blob_not_found_error();
  void clear_job_set_empty_error();
  ::oneflow::cfg::JobSetEmptyError* mutable_job_set_empty_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError> shared_mutable_job_set_empty_error();
  void clear_device_tag_not_found_error();
  ::oneflow::cfg::DeviceTagNotFoundError* mutable_device_tag_not_found_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError> shared_mutable_device_tag_not_found_error();
  void clear_value_error();
  ::oneflow::cfg::ValueError* mutable_value_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ValueError> shared_mutable_value_error();
  void clear_index_error();
  ::oneflow::cfg::IndexError* mutable_index_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::IndexError> shared_mutable_index_error();
  void clear_timeout_error();
  ::oneflow::cfg::TimeoutError* mutable_timeout_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TimeoutError> shared_mutable_timeout_error();
  void clear_job_name_exist_error();
  ::oneflow::cfg::JobNameExistError* mutable_job_name_exist_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobNameExistError> shared_mutable_job_name_exist_error();
  void clear_job_name_empty_error();
  ::oneflow::cfg::JobNameEmptyError* mutable_job_name_empty_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError> shared_mutable_job_name_empty_error();
  void clear_job_name_not_equal_error();
  ::oneflow::cfg::JobNameNotEqualError* mutable_job_name_not_equal_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError> shared_mutable_job_name_not_equal_error();
  void clear_no_job_build_and_infer_ctx_error();
  ::oneflow::cfg::NoJobBuildAndInferCtxError* mutable_no_job_build_and_infer_ctx_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError> shared_mutable_no_job_build_and_infer_ctx_error();
  void clear_job_conf_frozen_error();
  ::oneflow::cfg::JobConfFrozenError* mutable_job_conf_frozen_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError> shared_mutable_job_conf_frozen_error();
  void clear_job_conf_not_set_error();
  ::oneflow::cfg::JobConfNotSetError* mutable_job_conf_not_set_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError> shared_mutable_job_conf_not_set_error();
  void clear_job_conf_repeated_set_error();
  ::oneflow::cfg::JobConfRepeatedSetError* mutable_job_conf_repeated_set_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError> shared_mutable_job_conf_repeated_set_error();
  void clear_job_type_not_set_error();
  ::oneflow::cfg::JobTypeNotSetError* mutable_job_type_not_set_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError> shared_mutable_job_type_not_set_error();
  void clear_logical_blob_name_not_exist_error();
  ::oneflow::cfg::LogicalBlobNameNotExistError* mutable_logical_blob_name_not_exist_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError> shared_mutable_logical_blob_name_not_exist_error();
  void clear_logical_blob_name_exist_error();
  ::oneflow::cfg::LogicalBlobNameExistError* mutable_logical_blob_name_exist_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError> shared_mutable_logical_blob_name_exist_error();
  void clear_logical_blob_name_invalid_error();
  ::oneflow::cfg::LogicalBlobNameInvalidError* mutable_logical_blob_name_invalid_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError> shared_mutable_logical_blob_name_invalid_error();
  void clear_op_name_exist_error();
  ::oneflow::cfg::OpNameExistError* mutable_op_name_exist_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpNameExistError> shared_mutable_op_name_exist_error();
  void clear_op_conf_device_tag_no_set_error();
  ::oneflow::cfg::OpConfDeviceTagNoSetError* mutable_op_conf_device_tag_no_set_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError> shared_mutable_op_conf_device_tag_no_set_error();
  void clear_placement_error();
  ::oneflow::cfg::PlacementError* mutable_placement_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PlacementError> shared_mutable_placement_error();
  void clear_blob_split_axis_infer_error();
  ::oneflow::cfg::BlobSplitAxisInferError* mutable_blob_split_axis_infer_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError> shared_mutable_blob_split_axis_infer_error();
  void clear_unknown_job_build_and_infer_error();
  ::oneflow::cfg::UnknownJobBuildAndInferError* mutable_unknown_job_build_and_infer_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError> shared_mutable_unknown_job_build_and_infer_error();
  void clear_rw_mutexed_object_not_found_error();
  ::oneflow::cfg::RwMutexedObjectNotFoundError* mutable_rw_mutexed_object_not_found_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError> shared_mutable_rw_mutexed_object_not_found_error();
  void clear_symbol_id_uninitialized_error();
  ::oneflow::cfg::SymbolIdUninitializedError* mutable_symbol_id_uninitialized_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError> shared_mutable_symbol_id_uninitialized_error();
  void clear_unknown_error();
  ::oneflow::cfg::UnknownError* mutable_unknown_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::UnknownError> shared_mutable_unknown_error();
  void clear_compile_option_wrong_error();
  ::oneflow::cfg::CompileOptionWrongError* mutable_compile_option_wrong_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError> shared_mutable_compile_option_wrong_error();
  void clear_input_device_not_match_error();
  ::oneflow::cfg::InputDeviceNotMatchError* mutable_input_device_not_match_error();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError> shared_mutable_input_device_not_match_error();

  ::std::shared_ptr<ErrorProto> __SharedMutable__();
};



























































































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};








































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ErrorStackFrame> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ErrorStackFrame>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstErrorStackFrame> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_ final : public Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ErrorStackFrame>>& data);
  _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_();
  ~_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::OpcodeType> {
  std::size_t operator()(::oneflow::cfg::OpcodeType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstFieldValue> {
  std::size_t operator()(const ::oneflow::cfg::ConstFieldValue& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FieldValue> {
  std::size_t operator()(const ::oneflow::cfg::FieldValue& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOneFieldAssertError> {
  std::size_t operator()(const ::oneflow::cfg::ConstOneFieldAssertError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OneFieldAssertError> {
  std::size_t operator()(const ::oneflow::cfg::OneFieldAssertError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTwoFieldAssertError> {
  std::size_t operator()(const ::oneflow::cfg::ConstTwoFieldAssertError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TwoFieldAssertError> {
  std::size_t operator()(const ::oneflow::cfg::TwoFieldAssertError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstConfigAssertFailedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstConfigAssertFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConfigAssertFailedError> {
  std::size_t operator()(const ::oneflow::cfg::ConfigAssertFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstConfigResourceUnavailableError> {
  std::size_t operator()(const ::oneflow::cfg::ConstConfigResourceUnavailableError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConfigResourceUnavailableError> {
  std::size_t operator()(const ::oneflow::cfg::ConfigResourceUnavailableError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobSetEmptyError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobSetEmptyError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobSetEmptyError> {
  std::size_t operator()(const ::oneflow::cfg::JobSetEmptyError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDeviceTagNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::ConstDeviceTagNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DeviceTagNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::DeviceTagNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::JobNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobNameEmptyError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobNameEmptyError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobNameEmptyError> {
  std::size_t operator()(const ::oneflow::cfg::JobNameEmptyError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobNameNotEqualError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobNameNotEqualError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobNameNotEqualError> {
  std::size_t operator()(const ::oneflow::cfg::JobNameNotEqualError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstNoJobBuildAndInferCtxError> {
  std::size_t operator()(const ::oneflow::cfg::ConstNoJobBuildAndInferCtxError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::NoJobBuildAndInferCtxError> {
  std::size_t operator()(const ::oneflow::cfg::NoJobBuildAndInferCtxError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobConfFrozenError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobConfFrozenError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobConfFrozenError> {
  std::size_t operator()(const ::oneflow::cfg::JobConfFrozenError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobConfNotSetError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobConfNotSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobConfNotSetError> {
  std::size_t operator()(const ::oneflow::cfg::JobConfNotSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobConfRepeatedSetError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobConfRepeatedSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobConfRepeatedSetError> {
  std::size_t operator()(const ::oneflow::cfg::JobConfRepeatedSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobTypeNotSetError> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobTypeNotSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobTypeNotSetError> {
  std::size_t operator()(const ::oneflow::cfg::JobTypeNotSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobNameNotExistError> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobNameNotExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobNameNotExistError> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobNameNotExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobNameInvalidError> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobNameInvalidError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobNameInvalidError> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobNameInvalidError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNameExistError> {
  std::size_t operator()(const ::oneflow::cfg::OpNameExistError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpConfDeviceTagNoSetError> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpConfDeviceTagNoSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpConfDeviceTagNoSetError> {
  std::size_t operator()(const ::oneflow::cfg::OpConfDeviceTagNoSetError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPlacementError> {
  std::size_t operator()(const ::oneflow::cfg::ConstPlacementError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PlacementError> {
  std::size_t operator()(const ::oneflow::cfg::PlacementError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBlobSplitAxisInferError> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobSplitAxisInferError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobSplitAxisInferError> {
  std::size_t operator()(const ::oneflow::cfg::BlobSplitAxisInferError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstUnknownJobBuildAndInferError> {
  std::size_t operator()(const ::oneflow::cfg::ConstUnknownJobBuildAndInferError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::UnknownJobBuildAndInferError> {
  std::size_t operator()(const ::oneflow::cfg::UnknownJobBuildAndInferError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstProtoParseFailedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstProtoParseFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ProtoParseFailedError> {
  std::size_t operator()(const ::oneflow::cfg::ProtoParseFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCheckFailedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstCheckFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CheckFailedError> {
  std::size_t operator()(const ::oneflow::cfg::CheckFailedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTodoError> {
  std::size_t operator()(const ::oneflow::cfg::ConstTodoError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TodoError> {
  std::size_t operator()(const ::oneflow::cfg::TodoError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstUnimplementedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstUnimplementedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::UnimplementedError> {
  std::size_t operator()(const ::oneflow::cfg::UnimplementedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxingNotSupportedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxingNotSupportedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxingNotSupportedError> {
  std::size_t operator()(const ::oneflow::cfg::BoxingNotSupportedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstGradientFunctionNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::ConstGradientFunctionNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::GradientFunctionNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::GradientFunctionNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpKernelNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpKernelNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpKernelNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::OpKernelNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMultipleOpKernelsMatchedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstMultipleOpKernelsMatchedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MultipleOpKernelsMatchedError> {
  std::size_t operator()(const ::oneflow::cfg::MultipleOpKernelsMatchedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMemoryZoneOutOfMemoryError> {
  std::size_t operator()(const ::oneflow::cfg::ConstMemoryZoneOutOfMemoryError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MemoryZoneOutOfMemoryError> {
  std::size_t operator()(const ::oneflow::cfg::MemoryZoneOutOfMemoryError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLossBlobNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::ConstLossBlobNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LossBlobNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::LossBlobNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRwMutexedObjectNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::ConstRwMutexedObjectNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RwMutexedObjectNotFoundError> {
  std::size_t operator()(const ::oneflow::cfg::RwMutexedObjectNotFoundError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstUnknownError> {
  std::size_t operator()(const ::oneflow::cfg::ConstUnknownError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::UnknownError> {
  std::size_t operator()(const ::oneflow::cfg::UnknownError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCompileOptionWrongError> {
  std::size_t operator()(const ::oneflow::cfg::ConstCompileOptionWrongError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CompileOptionWrongError> {
  std::size_t operator()(const ::oneflow::cfg::CompileOptionWrongError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInputDeviceNotMatchError> {
  std::size_t operator()(const ::oneflow::cfg::ConstInputDeviceNotMatchError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InputDeviceNotMatchError> {
  std::size_t operator()(const ::oneflow::cfg::InputDeviceNotMatchError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstErrorStackFrame> {
  std::size_t operator()(const ::oneflow::cfg::ConstErrorStackFrame& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ErrorStackFrame> {
  std::size_t operator()(const ::oneflow::cfg::ErrorStackFrame& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSymbolIdUninitializedError> {
  std::size_t operator()(const ::oneflow::cfg::ConstSymbolIdUninitializedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SymbolIdUninitializedError> {
  std::size_t operator()(const ::oneflow::cfg::SymbolIdUninitializedError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstValueError> {
  std::size_t operator()(const ::oneflow::cfg::ConstValueError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ValueError> {
  std::size_t operator()(const ::oneflow::cfg::ValueError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstIndexError> {
  std::size_t operator()(const ::oneflow::cfg::ConstIndexError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::IndexError> {
  std::size_t operator()(const ::oneflow::cfg::IndexError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTimeoutError> {
  std::size_t operator()(const ::oneflow::cfg::ConstTimeoutError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TimeoutError> {
  std::size_t operator()(const ::oneflow::cfg::TimeoutError& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstErrorProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstErrorProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ErrorProto> {
  std::size_t operator()(const ::oneflow::cfg::ErrorProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H_