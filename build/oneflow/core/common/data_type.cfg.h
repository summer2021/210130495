#ifndef CFG_ONEFLOW_CORE_COMMON_DATA_TYPE_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_DATA_TYPE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class OptInt64;

namespace cfg {


class OptInt64;
class ConstOptInt64;

enum DataType : unsigned int {
  kInvalidDataType = 0,
  kChar = 1,
  kFloat = 2,
  kDouble = 3,
  kInt8 = 4,
  kInt32 = 5,
  kInt64 = 6,
  kUInt8 = 7,
  kOFRecord = 8,
  kFloat16 = 9,
  kTensorBuffer = 10,
};

inline ::std::string DataType_Name(DataType value) {
  switch (value) {
  case kInvalidDataType: { return "kInvalidDataType"; }
  case kChar: { return "kChar"; }
  case kFloat: { return "kFloat"; }
  case kDouble: { return "kDouble"; }
  case kInt8: { return "kInt8"; }
  case kInt32: { return "kInt32"; }
  case kInt64: { return "kInt64"; }
  case kUInt8: { return "kUInt8"; }
  case kOFRecord: { return "kOFRecord"; }
  case kFloat16: { return "kFloat16"; }
  case kTensorBuffer: { return "kTensorBuffer"; }
  default:
    return "";
  }
}



class ConstOptInt64 : public ::oneflow::cfg::Message {
 public:

  class _OptInt64_ {
   public:
    _OptInt64_();
    explicit _OptInt64_(const _OptInt64_& other);
    explicit _OptInt64_(_OptInt64_&& other);
    _OptInt64_(const ::oneflow::OptInt64& proto_optint64);
    ~_OptInt64_();

    void InitFromProto(const ::oneflow::OptInt64& proto_optint64);

    void ToProto(::oneflow::OptInt64* proto_optint64) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OptInt64_& other);
  
      // optional field value
     public:
    bool has_value() const;
    const int64_t& value() const;
    void clear_value();
    void set_value(const int64_t& value);
    int64_t* mutable_value();
   protected:
    bool has_value_ = false;
    int64_t value_;
           
   public:
    int compare(const _OptInt64_& other);

    bool operator==(const _OptInt64_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OptInt64_& other) const;
  };

  ConstOptInt64(const ::std::shared_ptr<_OptInt64_>& data);
  ConstOptInt64(const ConstOptInt64&);
  ConstOptInt64(ConstOptInt64&&) noexcept;
  ConstOptInt64();
  ConstOptInt64(const ::oneflow::OptInt64& proto_optint64);
  virtual ~ConstOptInt64() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_optint64) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field value
 public:
  bool has_value() const;
  const int64_t& value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstOptInt64> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOptInt64& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOptInt64& other) const;
 protected:
  const ::std::shared_ptr<_OptInt64_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OptInt64_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOptInt64
  void BuildFromProto(const PbMessage& proto_optint64);
  
  ::std::shared_ptr<_OptInt64_> data_;
};

class OptInt64 final : public ConstOptInt64 {
 public:
  OptInt64(const ::std::shared_ptr<_OptInt64_>& data);
  OptInt64(const OptInt64& other);
  // enable nothrow for ::std::vector<OptInt64> resize 
  OptInt64(OptInt64&&) noexcept;
  OptInt64();
  explicit OptInt64(const ::oneflow::OptInt64& proto_optint64);

  ~OptInt64() override;

  void InitFromProto(const PbMessage& proto_optint64) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OptInt64& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OptInt64& other) const;
  void Clear();
  void CopyFrom(const OptInt64& other);
  OptInt64& operator=(const OptInt64& other);

  // required or optional field value
 public:
  void clear_value();
  void set_value(const int64_t& value);
  int64_t* mutable_value();

  ::std::shared_ptr<OptInt64> __SharedMutable__();
};






} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::DataType> {
  std::size_t operator()(::oneflow::cfg::DataType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstOptInt64> {
  std::size_t operator()(const ::oneflow::cfg::ConstOptInt64& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OptInt64> {
  std::size_t operator()(const ::oneflow::cfg::OptInt64& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_COMMON_DATA_TYPE_CFG_H_