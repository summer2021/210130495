#ifndef CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class ShapeProto;
class ShapeSignature_Field2shapeProtoEntry;
class ShapeSignature;
class ShapeSignatureList;

namespace cfg {


class ShapeProto;
class ConstShapeProto;

class ShapeSignature;
class ConstShapeSignature;

class ShapeSignatureList;
class ConstShapeSignatureList;


class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_;
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_;

class ConstShapeProto : public ::oneflow::cfg::Message {
 public:

  class _ShapeProto_ {
   public:
    _ShapeProto_();
    explicit _ShapeProto_(const _ShapeProto_& other);
    explicit _ShapeProto_(_ShapeProto_&& other);
    _ShapeProto_(const ::oneflow::ShapeProto& proto_shapeproto);
    ~_ShapeProto_();

    void InitFromProto(const ::oneflow::ShapeProto& proto_shapeproto);

    void ToProto(::oneflow::ShapeProto* proto_shapeproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeProto_& other);
  
      // repeated field dim
   public:
    ::std::size_t dim_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& dim() const;
    const int64_t& dim(::std::size_t index) const;
    void clear_dim();
    _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_* mutable_dim();
    int64_t* mutable_dim(::std::size_t index);
      void add_dim(const int64_t& value);
    void set_dim(::std::size_t index, const int64_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> dim_;
         
   public:
    int compare(const _ShapeProto_& other);

    bool operator==(const _ShapeProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeProto_& other) const;
  };

  ConstShapeProto(const ::std::shared_ptr<_ShapeProto_>& data);
  ConstShapeProto(const ConstShapeProto&);
  ConstShapeProto(ConstShapeProto&&) noexcept;
  ConstShapeProto();
  ConstShapeProto(const ::oneflow::ShapeProto& proto_shapeproto);
  virtual ~ConstShapeProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapeproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field dim
 public:
  ::std::size_t dim_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& dim() const;
  const int64_t& dim(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> shared_const_dim() const;

 public:
  ::std::shared_ptr<ConstShapeProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeProto& other) const;
 protected:
  const ::std::shared_ptr<_ShapeProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeProto
  void BuildFromProto(const PbMessage& proto_shapeproto);
  
  ::std::shared_ptr<_ShapeProto_> data_;
};

class ShapeProto final : public ConstShapeProto {
 public:
  ShapeProto(const ::std::shared_ptr<_ShapeProto_>& data);
  ShapeProto(const ShapeProto& other);
  // enable nothrow for ::std::vector<ShapeProto> resize 
  ShapeProto(ShapeProto&&) noexcept;
  ShapeProto();
  explicit ShapeProto(const ::oneflow::ShapeProto& proto_shapeproto);

  ~ShapeProto() override;

  void InitFromProto(const PbMessage& proto_shapeproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeProto& other) const;
  void Clear();
  void CopyFrom(const ShapeProto& other);
  ShapeProto& operator=(const ShapeProto& other);

  // repeated field dim
 public:
  void clear_dim();
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_* mutable_dim();
  int64_t* mutable_dim(::std::size_t index);
  void add_dim(const int64_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> shared_mutable_dim();
  void set_dim(::std::size_t index, const int64_t& value);

  ::std::shared_ptr<ShapeProto> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_; 
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_;

class ConstShapeSignature : public ::oneflow::cfg::Message {
 public:

  class _ShapeSignature_ {
   public:
    _ShapeSignature_();
    explicit _ShapeSignature_(const _ShapeSignature_& other);
    explicit _ShapeSignature_(_ShapeSignature_&& other);
    _ShapeSignature_(const ::oneflow::ShapeSignature& proto_shapesignature);
    ~_ShapeSignature_();

    void InitFromProto(const ::oneflow::ShapeSignature& proto_shapesignature);

    void ToProto(::oneflow::ShapeSignature* proto_shapesignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeSignature_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
     public:
    ::std::size_t field2shape_proto_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& field2shape_proto() const;

    _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ * mutable_field2shape_proto();

    const ::oneflow::cfg::ShapeProto& field2shape_proto(::std::string key) const;

    void clear_field2shape_proto();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> field2shape_proto_;
         
   public:
    int compare(const _ShapeSignature_& other);

    bool operator==(const _ShapeSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeSignature_& other) const;
  };

  ConstShapeSignature(const ::std::shared_ptr<_ShapeSignature_>& data);
  ConstShapeSignature(const ConstShapeSignature&);
  ConstShapeSignature(ConstShapeSignature&&) noexcept;
  ConstShapeSignature();
  ConstShapeSignature(const ::oneflow::ShapeSignature& proto_shapesignature);
  virtual ~ConstShapeSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapesignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // map field field2shape_proto
 public:
  ::std::size_t field2shape_proto_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& field2shape_proto() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> shared_const_field2shape_proto() const;

 public:
  ::std::shared_ptr<ConstShapeSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeSignature& other) const;
 protected:
  const ::std::shared_ptr<_ShapeSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeSignature
  void BuildFromProto(const PbMessage& proto_shapesignature);
  
  ::std::shared_ptr<_ShapeSignature_> data_;
};

class ShapeSignature final : public ConstShapeSignature {
 public:
  ShapeSignature(const ::std::shared_ptr<_ShapeSignature_>& data);
  ShapeSignature(const ShapeSignature& other);
  // enable nothrow for ::std::vector<ShapeSignature> resize 
  ShapeSignature(ShapeSignature&&) noexcept;
  ShapeSignature();
  explicit ShapeSignature(const ::oneflow::ShapeSignature& proto_shapesignature);

  ~ShapeSignature() override;

  void InitFromProto(const PbMessage& proto_shapesignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeSignature& other) const;
  void Clear();
  void CopyFrom(const ShapeSignature& other);
  ShapeSignature& operator=(const ShapeSignature& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // repeated field field2shape_proto
 public:
  void clear_field2shape_proto();

  const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ & field2shape_proto() const;

  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_* mutable_field2shape_proto();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> shared_mutable_field2shape_proto();

  ::std::shared_ptr<ShapeSignature> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_;
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_;

class ConstShapeSignatureList : public ::oneflow::cfg::Message {
 public:

  class _ShapeSignatureList_ {
   public:
    _ShapeSignatureList_();
    explicit _ShapeSignatureList_(const _ShapeSignatureList_& other);
    explicit _ShapeSignatureList_(_ShapeSignatureList_&& other);
    _ShapeSignatureList_(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist);
    ~_ShapeSignatureList_();

    void InitFromProto(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist);

    void ToProto(::oneflow::ShapeSignatureList* proto_shapesignaturelist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeSignatureList_& other);
  
      // repeated field shape_signature
   public:
    ::std::size_t shape_signature_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& shape_signature() const;
    const ::oneflow::cfg::ShapeSignature& shape_signature(::std::size_t index) const;
    void clear_shape_signature();
    _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_* mutable_shape_signature();
    ::oneflow::cfg::ShapeSignature* mutable_shape_signature(::std::size_t index);
      ::oneflow::cfg::ShapeSignature* add_shape_signature();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> shape_signature_;
         
   public:
    int compare(const _ShapeSignatureList_& other);

    bool operator==(const _ShapeSignatureList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeSignatureList_& other) const;
  };

  ConstShapeSignatureList(const ::std::shared_ptr<_ShapeSignatureList_>& data);
  ConstShapeSignatureList(const ConstShapeSignatureList&);
  ConstShapeSignatureList(ConstShapeSignatureList&&) noexcept;
  ConstShapeSignatureList();
  ConstShapeSignatureList(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist);
  virtual ~ConstShapeSignatureList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapesignaturelist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field shape_signature
 public:
  ::std::size_t shape_signature_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& shape_signature() const;
  const ::oneflow::cfg::ShapeSignature& shape_signature(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> shared_const_shape_signature() const;
  ::std::shared_ptr<::oneflow::cfg::ConstShapeSignature> shared_const_shape_signature(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstShapeSignatureList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeSignatureList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeSignatureList& other) const;
 protected:
  const ::std::shared_ptr<_ShapeSignatureList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeSignatureList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeSignatureList
  void BuildFromProto(const PbMessage& proto_shapesignaturelist);
  
  ::std::shared_ptr<_ShapeSignatureList_> data_;
};

class ShapeSignatureList final : public ConstShapeSignatureList {
 public:
  ShapeSignatureList(const ::std::shared_ptr<_ShapeSignatureList_>& data);
  ShapeSignatureList(const ShapeSignatureList& other);
  // enable nothrow for ::std::vector<ShapeSignatureList> resize 
  ShapeSignatureList(ShapeSignatureList&&) noexcept;
  ShapeSignatureList();
  explicit ShapeSignatureList(const ::oneflow::ShapeSignatureList& proto_shapesignaturelist);

  ~ShapeSignatureList() override;

  void InitFromProto(const PbMessage& proto_shapesignaturelist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeSignatureList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeSignatureList& other) const;
  void Clear();
  void CopyFrom(const ShapeSignatureList& other);
  ShapeSignatureList& operator=(const ShapeSignatureList& other);

  // repeated field shape_signature
 public:
  void clear_shape_signature();
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_* mutable_shape_signature();
  ::oneflow::cfg::ShapeSignature* mutable_shape_signature(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> shared_mutable_shape_signature();
  ::std::shared_ptr<::oneflow::cfg::ShapeSignature> shared_mutable_shape_signature(::std::size_t index);
  ::oneflow::cfg::ShapeSignature* add_shape_signature();

  ::std::shared_ptr<ShapeSignatureList> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_ : public ::oneflow::cfg::_RepeatedField_<int64_t> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_ final : public Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_();
  ~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ShapeProto> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ShapeProto>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::ShapeProto& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstShapeProto> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ConstShapeProto>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ final : public Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ShapeProto>>& data);
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_();
  ~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::ShapeProto> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, ::oneflow::cfg::ShapeProto>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeSignature> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeSignature>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstShapeSignature> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_ final : public Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeSignature>>& data);
  _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_();
  ~_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::ShapeSignature> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::ShapeSignature> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstShapeProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeProto> {
  std::size_t operator()(const ::oneflow::cfg::ShapeProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstShapeSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeSignature> {
  std::size_t operator()(const ::oneflow::cfg::ShapeSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstShapeSignatureList> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeSignatureList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeSignatureList> {
  std::size_t operator()(const ::oneflow::cfg::ShapeSignatureList& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H_