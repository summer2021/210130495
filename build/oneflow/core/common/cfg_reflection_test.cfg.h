#ifndef CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class ReflectionTestFoo_MapInt32Entry;
class ReflectionTestFoo;
class ReflectionTestBar_MapFooEntry;
class ReflectionTestBar;

namespace cfg {


class ReflectionTestFoo;
class ConstReflectionTestFoo;

class ReflectionTestBar;
class ConstReflectionTestBar;


class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_;
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_;
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_;
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_; 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_;

class ConstReflectionTestFoo : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kOneofInt32 = 6,
    kAnotherOneofInt32 = 7,
   };

  class _ReflectionTestFoo_ {
   public:
    _ReflectionTestFoo_();
    explicit _ReflectionTestFoo_(const _ReflectionTestFoo_& other);
    explicit _ReflectionTestFoo_(_ReflectionTestFoo_&& other);
    _ReflectionTestFoo_(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo);
    ~_ReflectionTestFoo_();

    void InitFromProto(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo);

    void ToProto(::oneflow::ReflectionTestFoo* proto_reflectiontestfoo) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ReflectionTestFoo_& other);
  
      // optional field required_int32
     public:
    bool has_required_int32() const;
    const int32_t& required_int32() const;
    void clear_required_int32();
    void set_required_int32(const int32_t& value);
    int32_t* mutable_required_int32();
   protected:
    bool has_required_int32_ = false;
    int32_t required_int32_;
      
      // optional field optional_string
     public:
    bool has_optional_string() const;
    const ::std::string& optional_string() const;
    void clear_optional_string();
    void set_optional_string(const ::std::string& value);
    ::std::string* mutable_optional_string();
   protected:
    bool has_optional_string_ = false;
    ::std::string optional_string_;
      
      // repeated field repeated_int32
   public:
    ::std::size_t repeated_int32_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& repeated_int32() const;
    const int32_t& repeated_int32(::std::size_t index) const;
    void clear_repeated_int32();
    _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_* mutable_repeated_int32();
    int32_t* mutable_repeated_int32(::std::size_t index);
      void add_repeated_int32(const int32_t& value);
    void set_repeated_int32(::std::size_t index, const int32_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> repeated_int32_;
    
      // repeated field repeated_string
   public:
    ::std::size_t repeated_string_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& repeated_string() const;
    const ::std::string& repeated_string(::std::size_t index) const;
    void clear_repeated_string();
    _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_* mutable_repeated_string();
    ::std::string* mutable_repeated_string(::std::size_t index);
      void add_repeated_string(const ::std::string& value);
    void set_repeated_string(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> repeated_string_;
    
     public:
    ::std::size_t map_int32_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& map_int32() const;

    _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ * mutable_map_int32();

    const int32_t& map_int32(int32_t key) const;

    void clear_map_int32();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> map_int32_;
    
     // oneof field type: oneof_int32
   public:
    bool has_oneof_int32() const;
    void clear_oneof_int32();
    const int32_t& oneof_int32() const;
      void set_oneof_int32(const int32_t& value);
    int32_t* mutable_oneof_int32();
      
     // oneof field type: another_oneof_int32
   public:
    bool has_another_oneof_int32() const;
    void clear_another_oneof_int32();
    const int32_t& another_oneof_int32() const;
      void set_another_oneof_int32(const int32_t& value);
    int32_t* mutable_another_oneof_int32();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _ReflectionTestFoo_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          int32_t oneof_int32_;
            int32_t another_oneof_int32_;
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _ReflectionTestFoo_& other);

    bool operator==(const _ReflectionTestFoo_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ReflectionTestFoo_& other) const;
  };

  ConstReflectionTestFoo(const ::std::shared_ptr<_ReflectionTestFoo_>& data);
  ConstReflectionTestFoo(const ConstReflectionTestFoo&);
  ConstReflectionTestFoo(ConstReflectionTestFoo&&) noexcept;
  ConstReflectionTestFoo();
  ConstReflectionTestFoo(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo);
  virtual ~ConstReflectionTestFoo() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_reflectiontestfoo) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field required_int32
 public:
  bool has_required_int32() const;
  const int32_t& required_int32() const;
  // used by pybind11 only
  // required or optional field optional_string
 public:
  bool has_optional_string() const;
  const ::std::string& optional_string() const;
  // used by pybind11 only
  // repeated field repeated_int32
 public:
  ::std::size_t repeated_int32_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& repeated_int32() const;
  const int32_t& repeated_int32(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> shared_const_repeated_int32() const;
  // repeated field repeated_string
 public:
  ::std::size_t repeated_string_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& repeated_string() const;
  const ::std::string& repeated_string(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> shared_const_repeated_string() const;
  // map field map_int32
 public:
  ::std::size_t map_int32_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& map_int32() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> shared_const_map_int32() const;
 // oneof field type: oneof_int32
 public:
  bool has_oneof_int32() const;
  const int32_t& oneof_int32() const;
  // used by pybind11 only
 // oneof field type: another_oneof_int32
 public:
  bool has_another_oneof_int32() const;
  const int32_t& another_oneof_int32() const;
  // used by pybind11 only
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstReflectionTestFoo> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstReflectionTestFoo& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstReflectionTestFoo& other) const;
 protected:
  const ::std::shared_ptr<_ReflectionTestFoo_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ReflectionTestFoo_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstReflectionTestFoo
  void BuildFromProto(const PbMessage& proto_reflectiontestfoo);
  
  ::std::shared_ptr<_ReflectionTestFoo_> data_;
};

class ReflectionTestFoo final : public ConstReflectionTestFoo {
 public:
  ReflectionTestFoo(const ::std::shared_ptr<_ReflectionTestFoo_>& data);
  ReflectionTestFoo(const ReflectionTestFoo& other);
  // enable nothrow for ::std::vector<ReflectionTestFoo> resize 
  ReflectionTestFoo(ReflectionTestFoo&&) noexcept;
  ReflectionTestFoo();
  explicit ReflectionTestFoo(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo);

  ~ReflectionTestFoo() override;

  void InitFromProto(const PbMessage& proto_reflectiontestfoo) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ReflectionTestFoo& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ReflectionTestFoo& other) const;
  void Clear();
  void CopyFrom(const ReflectionTestFoo& other);
  ReflectionTestFoo& operator=(const ReflectionTestFoo& other);

  // required or optional field required_int32
 public:
  void clear_required_int32();
  void set_required_int32(const int32_t& value);
  int32_t* mutable_required_int32();
  // required or optional field optional_string
 public:
  void clear_optional_string();
  void set_optional_string(const ::std::string& value);
  ::std::string* mutable_optional_string();
  // repeated field repeated_int32
 public:
  void clear_repeated_int32();
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_* mutable_repeated_int32();
  int32_t* mutable_repeated_int32(::std::size_t index);
  void add_repeated_int32(const int32_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> shared_mutable_repeated_int32();
  void set_repeated_int32(::std::size_t index, const int32_t& value);
  // repeated field repeated_string
 public:
  void clear_repeated_string();
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_* mutable_repeated_string();
  ::std::string* mutable_repeated_string(::std::size_t index);
  void add_repeated_string(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> shared_mutable_repeated_string();
  void set_repeated_string(::std::size_t index, const ::std::string& value);
  // repeated field map_int32
 public:
  void clear_map_int32();

  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ & map_int32() const;

  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_* mutable_map_int32();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> shared_mutable_map_int32();
  void clear_oneof_int32();
  void set_oneof_int32(const int32_t& value);
  int32_t* mutable_oneof_int32();
  void clear_another_oneof_int32();
  void set_another_oneof_int32(const int32_t& value);
  int32_t* mutable_another_oneof_int32();

  ::std::shared_ptr<ReflectionTestFoo> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_;
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_;
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_; 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_;

class ConstReflectionTestBar : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kOneofFoo = 5,
    kAnotherOneofFoo = 6,
   };

  class _ReflectionTestBar_ {
   public:
    _ReflectionTestBar_();
    explicit _ReflectionTestBar_(const _ReflectionTestBar_& other);
    explicit _ReflectionTestBar_(_ReflectionTestBar_&& other);
    _ReflectionTestBar_(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar);
    ~_ReflectionTestBar_();

    void InitFromProto(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar);

    void ToProto(::oneflow::ReflectionTestBar* proto_reflectiontestbar) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ReflectionTestBar_& other);
  
      // optional field required_foo
     public:
    bool has_required_foo() const;
    const ::oneflow::cfg::ReflectionTestFoo& required_foo() const;
    void clear_required_foo();
    ::oneflow::cfg::ReflectionTestFoo* mutable_required_foo();
   protected:
    bool has_required_foo_ = false;
    ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> required_foo_;
      
      // optional field optional_foo
     public:
    bool has_optional_foo() const;
    const ::oneflow::cfg::ReflectionTestFoo& optional_foo() const;
    void clear_optional_foo();
    ::oneflow::cfg::ReflectionTestFoo* mutable_optional_foo();
   protected:
    bool has_optional_foo_ = false;
    ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> optional_foo_;
      
      // repeated field repeated_foo
   public:
    ::std::size_t repeated_foo_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& repeated_foo() const;
    const ::oneflow::cfg::ReflectionTestFoo& repeated_foo(::std::size_t index) const;
    void clear_repeated_foo();
    _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_* mutable_repeated_foo();
    ::oneflow::cfg::ReflectionTestFoo* mutable_repeated_foo(::std::size_t index);
      ::oneflow::cfg::ReflectionTestFoo* add_repeated_foo();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> repeated_foo_;
    
     public:
    ::std::size_t map_foo_size() const;
    const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& map_foo() const;

    _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ * mutable_map_foo();

    const ::oneflow::cfg::ReflectionTestFoo& map_foo(int32_t key) const;

    void clear_map_foo();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> map_foo_;
    
     // oneof field type: oneof_foo
   public:
    bool has_oneof_foo() const;
    void clear_oneof_foo();
    const ::oneflow::cfg::ReflectionTestFoo& oneof_foo() const;
      ::oneflow::cfg::ReflectionTestFoo* mutable_oneof_foo();
      
     // oneof field type: another_oneof_foo
   public:
    bool has_another_oneof_foo() const;
    void clear_another_oneof_foo();
    const ::oneflow::cfg::ReflectionTestFoo& another_oneof_foo() const;
      ::oneflow::cfg::ReflectionTestFoo* mutable_another_oneof_foo();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _ReflectionTestBar_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char oneof_foo_[sizeof(::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>)];
            char another_oneof_foo_[sizeof(::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _ReflectionTestBar_& other);

    bool operator==(const _ReflectionTestBar_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ReflectionTestBar_& other) const;
  };

  ConstReflectionTestBar(const ::std::shared_ptr<_ReflectionTestBar_>& data);
  ConstReflectionTestBar(const ConstReflectionTestBar&);
  ConstReflectionTestBar(ConstReflectionTestBar&&) noexcept;
  ConstReflectionTestBar();
  ConstReflectionTestBar(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar);
  virtual ~ConstReflectionTestBar() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_reflectiontestbar) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field required_foo
 public:
  bool has_required_foo() const;
  const ::oneflow::cfg::ReflectionTestFoo& required_foo() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> shared_const_required_foo() const;
  // required or optional field optional_foo
 public:
  bool has_optional_foo() const;
  const ::oneflow::cfg::ReflectionTestFoo& optional_foo() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> shared_const_optional_foo() const;
  // repeated field repeated_foo
 public:
  ::std::size_t repeated_foo_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& repeated_foo() const;
  const ::oneflow::cfg::ReflectionTestFoo& repeated_foo(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> shared_const_repeated_foo() const;
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> shared_const_repeated_foo(::std::size_t index) const;
  // map field map_foo
 public:
  ::std::size_t map_foo_size() const;
  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& map_foo() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> shared_const_map_foo() const;
 // oneof field type: oneof_foo
 public:
  bool has_oneof_foo() const;
  const ::oneflow::cfg::ReflectionTestFoo& oneof_foo() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> shared_const_oneof_foo() const;
 // oneof field type: another_oneof_foo
 public:
  bool has_another_oneof_foo() const;
  const ::oneflow::cfg::ReflectionTestFoo& another_oneof_foo() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> shared_const_another_oneof_foo() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstReflectionTestBar> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstReflectionTestBar& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstReflectionTestBar& other) const;
 protected:
  const ::std::shared_ptr<_ReflectionTestBar_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ReflectionTestBar_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstReflectionTestBar
  void BuildFromProto(const PbMessage& proto_reflectiontestbar);
  
  ::std::shared_ptr<_ReflectionTestBar_> data_;
};

class ReflectionTestBar final : public ConstReflectionTestBar {
 public:
  ReflectionTestBar(const ::std::shared_ptr<_ReflectionTestBar_>& data);
  ReflectionTestBar(const ReflectionTestBar& other);
  // enable nothrow for ::std::vector<ReflectionTestBar> resize 
  ReflectionTestBar(ReflectionTestBar&&) noexcept;
  ReflectionTestBar();
  explicit ReflectionTestBar(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar);

  ~ReflectionTestBar() override;

  void InitFromProto(const PbMessage& proto_reflectiontestbar) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ReflectionTestBar& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ReflectionTestBar& other) const;
  void Clear();
  void CopyFrom(const ReflectionTestBar& other);
  ReflectionTestBar& operator=(const ReflectionTestBar& other);

  // required or optional field required_foo
 public:
  void clear_required_foo();
  ::oneflow::cfg::ReflectionTestFoo* mutable_required_foo();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> shared_mutable_required_foo();
  // required or optional field optional_foo
 public:
  void clear_optional_foo();
  ::oneflow::cfg::ReflectionTestFoo* mutable_optional_foo();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> shared_mutable_optional_foo();
  // repeated field repeated_foo
 public:
  void clear_repeated_foo();
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_* mutable_repeated_foo();
  ::oneflow::cfg::ReflectionTestFoo* mutable_repeated_foo(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> shared_mutable_repeated_foo();
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> shared_mutable_repeated_foo(::std::size_t index);
  ::oneflow::cfg::ReflectionTestFoo* add_repeated_foo();
  // repeated field map_foo
 public:
  void clear_map_foo();

  const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ & map_foo() const;

  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_* mutable_map_foo();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> shared_mutable_map_foo();
  void clear_oneof_foo();
  ::oneflow::cfg::ReflectionTestFoo* mutable_oneof_foo();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> shared_mutable_oneof_foo();
  void clear_another_oneof_foo();
  ::oneflow::cfg::ReflectionTestFoo* mutable_another_oneof_foo();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> shared_mutable_another_oneof_foo();

  ::std::shared_ptr<ReflectionTestBar> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_ : public ::oneflow::cfg::_RepeatedField_<int32_t> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_ final : public Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_();
  ~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> __SharedMutable__();
};

// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ : public ::oneflow::cfg::_MapField_<int32_t, int32_t> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_(const ::std::shared_ptr<::std::map<int32_t, int32_t>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const;
  // used by pybind11 only
  const int32_t& Get(const int32_t& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ final : public Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_(const ::std::shared_ptr<::std::map<int32_t, int32_t>>& data);
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_();
  ~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> __SharedMutable__();

  void Set(const int32_t& key, const int32_t& value);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ReflectionTestFoo> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ReflectionTestFoo>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_ final : public Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ReflectionTestFoo>>& data);
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_();
  ~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> __SharedMutable__(::std::size_t index);
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ : public ::oneflow::cfg::_MapField_<int32_t, ::oneflow::cfg::ReflectionTestFoo> {
 public:
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_(const ::std::shared_ptr<::std::map<int32_t, ::oneflow::cfg::ReflectionTestFoo>>& data);
  Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_();
  ~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::ReflectionTestFoo& Get(const int32_t& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstReflectionTestFoo> __SharedConst__(const int32_t& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ConstReflectionTestFoo>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ final : public Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ {
 public:
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_(const ::std::shared_ptr<::std::map<int32_t, ::oneflow::cfg::ReflectionTestFoo>>& data);
  _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_();
  ~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> __SharedMutable__(const int32_t& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ::oneflow::cfg::ReflectionTestFoo>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstReflectionTestFoo> {
  std::size_t operator()(const ::oneflow::cfg::ConstReflectionTestFoo& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ReflectionTestFoo> {
  std::size_t operator()(const ::oneflow::cfg::ReflectionTestFoo& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstReflectionTestBar> {
  std::size_t operator()(const ::oneflow::cfg::ConstReflectionTestBar& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ReflectionTestBar> {
  std::size_t operator()(const ::oneflow::cfg::ReflectionTestBar& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H_