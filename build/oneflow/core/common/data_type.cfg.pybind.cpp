#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/common/data_type.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.common.data_type", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::DataType> enm(m, "DataType");
    enm.value("kInvalidDataType", ::oneflow::cfg::kInvalidDataType);
    enm.value("kChar", ::oneflow::cfg::kChar);
    enm.value("kFloat", ::oneflow::cfg::kFloat);
    enm.value("kDouble", ::oneflow::cfg::kDouble);
    enm.value("kInt8", ::oneflow::cfg::kInt8);
    enm.value("kInt32", ::oneflow::cfg::kInt32);
    enm.value("kInt64", ::oneflow::cfg::kInt64);
    enm.value("kUInt8", ::oneflow::cfg::kUInt8);
    enm.value("kOFRecord", ::oneflow::cfg::kOFRecord);
    enm.value("kFloat16", ::oneflow::cfg::kFloat16);
    enm.value("kTensorBuffer", ::oneflow::cfg::kTensorBuffer);
    m.attr("kInvalidDataType") = enm.attr("kInvalidDataType");
    m.attr("kChar") = enm.attr("kChar");
    m.attr("kFloat") = enm.attr("kFloat");
    m.attr("kDouble") = enm.attr("kDouble");
    m.attr("kInt8") = enm.attr("kInt8");
    m.attr("kInt32") = enm.attr("kInt32");
    m.attr("kInt64") = enm.attr("kInt64");
    m.attr("kUInt8") = enm.attr("kUInt8");
    m.attr("kOFRecord") = enm.attr("kOFRecord");
    m.attr("kFloat16") = enm.attr("kFloat16");
    m.attr("kTensorBuffer") = enm.attr("kTensorBuffer");
  }


  {
    pybind11::class_<ConstOptInt64, ::oneflow::cfg::Message, std::shared_ptr<ConstOptInt64>> registry(m, "ConstOptInt64");
    registry.def("__id__", &::oneflow::cfg::OptInt64::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOptInt64::DebugString);
    registry.def("__repr__", &ConstOptInt64::DebugString);

    registry.def("has_value", &ConstOptInt64::has_value);
    registry.def("value", &ConstOptInt64::value);
  }
  {
    pybind11::class_<::oneflow::cfg::OptInt64, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OptInt64>> registry(m, "OptInt64");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OptInt64::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptInt64::*)(const ConstOptInt64&))&::oneflow::cfg::OptInt64::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptInt64::*)(const ::oneflow::cfg::OptInt64&))&::oneflow::cfg::OptInt64::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OptInt64::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OptInt64::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OptInt64::DebugString);



    registry.def("has_value", &::oneflow::cfg::OptInt64::has_value);
    registry.def("clear_value", &::oneflow::cfg::OptInt64::clear_value);
    registry.def("value", &::oneflow::cfg::OptInt64::value);
    registry.def("set_value", &::oneflow::cfg::OptInt64::set_value);
  }
}