#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/common/data_type.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstOptInt64::_OptInt64_::_OptInt64_() { Clear(); }
ConstOptInt64::_OptInt64_::_OptInt64_(const _OptInt64_& other) { CopyFrom(other); }
ConstOptInt64::_OptInt64_::_OptInt64_(const ::oneflow::OptInt64& proto_optint64) {
  InitFromProto(proto_optint64);
}
ConstOptInt64::_OptInt64_::_OptInt64_(_OptInt64_&& other) = default;
ConstOptInt64::_OptInt64_::~_OptInt64_() = default;

void ConstOptInt64::_OptInt64_::InitFromProto(const ::oneflow::OptInt64& proto_optint64) {
  Clear();
  // required_or_optional field: value
  if (proto_optint64.has_value()) {
    set_value(proto_optint64.value());
  }
    
}

void ConstOptInt64::_OptInt64_::ToProto(::oneflow::OptInt64* proto_optint64) const {
  proto_optint64->Clear();
  // required_or_optional field: value
  if (this->has_value()) {
    proto_optint64->set_value(value());
    }

}

::std::string ConstOptInt64::_OptInt64_::DebugString() const {
  ::oneflow::OptInt64 proto_optint64;
  this->ToProto(&proto_optint64);
  return proto_optint64.DebugString();
}

void ConstOptInt64::_OptInt64_::Clear() {
  clear_value();
}

void ConstOptInt64::_OptInt64_::CopyFrom(const _OptInt64_& other) {
  if (other.has_value()) {
    set_value(other.value());
  } else {
    clear_value();
  }
}


// optional field value
bool ConstOptInt64::_OptInt64_::has_value() const {
  return has_value_;
}
const int64_t& ConstOptInt64::_OptInt64_::value() const {
  if (has_value_) { return value_; }
  static const int64_t default_static_value =
    int64_t(-1);
  return default_static_value;
}
void ConstOptInt64::_OptInt64_::clear_value() {
  has_value_ = false;
}
void ConstOptInt64::_OptInt64_::set_value(const int64_t& value) {
  value_ = value;
  has_value_ = true;
}
int64_t* ConstOptInt64::_OptInt64_::mutable_value() {
  has_value_ = true;
  return &value_;
}


int ConstOptInt64::_OptInt64_::compare(const _OptInt64_& other) {
  if (!(has_value() == other.has_value())) {
    return has_value() < other.has_value() ? -1 : 1;
  } else if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstOptInt64::_OptInt64_::operator==(const _OptInt64_& other) const {
  return true
    && has_value() == other.has_value() 
    && value() == other.value()
  ;
}

std::size_t ConstOptInt64::_OptInt64_::__CalcHash__() const {
  return 0
    ^ (has_value() ? std::hash<int64_t>()(value()) : 0)
  ;
}

bool ConstOptInt64::_OptInt64_::operator<(const _OptInt64_& other) const {
  return false
    || !(has_value() == other.has_value()) ? 
      has_value() < other.has_value() : false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _OptInt64_ =  ConstOptInt64::_OptInt64_;
ConstOptInt64::ConstOptInt64(const ::std::shared_ptr<_OptInt64_>& data): data_(data) {}
ConstOptInt64::ConstOptInt64(): data_(::std::make_shared<_OptInt64_>()) {}
ConstOptInt64::ConstOptInt64(const ::oneflow::OptInt64& proto_optint64) {
  BuildFromProto(proto_optint64);
}
ConstOptInt64::ConstOptInt64(const ConstOptInt64&) = default;
ConstOptInt64::ConstOptInt64(ConstOptInt64&&) noexcept = default;
ConstOptInt64::~ConstOptInt64() = default;

void ConstOptInt64::ToProto(PbMessage* proto_optint64) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OptInt64*>(proto_optint64));
}
  
::std::string ConstOptInt64::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOptInt64::__Empty__() const {
  return !data_;
}

int ConstOptInt64::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOptInt64::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOptInt64::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOptInt64::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// required or optional field value
bool ConstOptInt64::has_value() const {
  return __SharedPtrOrDefault__()->has_value();
}
const int64_t& ConstOptInt64::value() const {
  return __SharedPtrOrDefault__()->value();
}
// used by pybind11 only

::std::shared_ptr<ConstOptInt64> ConstOptInt64::__SharedConst__() const {
  return ::std::make_shared<ConstOptInt64>(data_);
}
int64_t ConstOptInt64::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOptInt64::operator==(const ConstOptInt64& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOptInt64::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOptInt64::operator<(const ConstOptInt64& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OptInt64_>& ConstOptInt64::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OptInt64_> default_ptr = std::make_shared<_OptInt64_>();
  return default_ptr;
}
const ::std::shared_ptr<_OptInt64_>& ConstOptInt64::__SharedPtr__() {
  if (!data_) { data_.reset(new _OptInt64_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOptInt64
void ConstOptInt64::BuildFromProto(const PbMessage& proto_optint64) {
  data_ = ::std::make_shared<_OptInt64_>(dynamic_cast<const ::oneflow::OptInt64&>(proto_optint64));
}

OptInt64::OptInt64(const ::std::shared_ptr<_OptInt64_>& data)
  : ConstOptInt64(data) {}
OptInt64::OptInt64(const OptInt64& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OptInt64> resize
OptInt64::OptInt64(OptInt64&&) noexcept = default; 
OptInt64::OptInt64(const ::oneflow::OptInt64& proto_optint64) {
  InitFromProto(proto_optint64);
}
OptInt64::OptInt64() = default;

OptInt64::~OptInt64() = default;

void OptInt64::InitFromProto(const PbMessage& proto_optint64) {
  BuildFromProto(proto_optint64);
}
  
void* OptInt64::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool OptInt64::operator==(const OptInt64& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OptInt64::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OptInt64::operator<(const OptInt64& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OptInt64::Clear() {
  if (data_) { data_.reset(); }
}
void OptInt64::CopyFrom(const OptInt64& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OptInt64& OptInt64::operator=(const OptInt64& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field value
void OptInt64::clear_value() {
  return __SharedPtr__()->clear_value();
}
void OptInt64::set_value(const int64_t& value) {
  return __SharedPtr__()->set_value(value);
}
int64_t* OptInt64::mutable_value() {
  return  __SharedPtr__()->mutable_value();
}

::std::shared_ptr<OptInt64> OptInt64::__SharedMutable__() {
  return ::std::make_shared<OptInt64>(__SharedPtr__());
}


}
} // namespace oneflow
