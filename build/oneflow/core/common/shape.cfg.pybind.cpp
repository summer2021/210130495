#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/common/shape.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.common.shape", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>> registry(m, "Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_, std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_>> registry(m, "_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::*)(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::*)(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::*)(const int64_t&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstShapeProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstShapeProto>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstShapeProto> (Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>> registry(m, "_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::*)(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::*)(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ShapeProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ShapeProto>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::ShapeProto> (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>> registry(m, "Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstShapeSignature> (Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstShapeSignature> (Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_, std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_>> registry(m, "_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(const Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(const _CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(const ::oneflow::cfg::ShapeSignature&))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::ShapeSignature> (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::ShapeSignature> (_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstShapeProto, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeProto>> registry(m, "ConstShapeProto");
    registry.def("__id__", &::oneflow::cfg::ShapeProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeProto::DebugString);
    registry.def("__repr__", &ConstShapeProto::DebugString);

    registry.def("dim_size", &ConstShapeProto::dim_size);
    registry.def("dim", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> (ConstShapeProto::*)() const)&ConstShapeProto::shared_const_dim);
    registry.def("dim", (const int64_t& (ConstShapeProto::*)(::std::size_t) const)&ConstShapeProto::dim);
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeProto>> registry(m, "ShapeProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeProto::*)(const ConstShapeProto&))&::oneflow::cfg::ShapeProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeProto::*)(const ::oneflow::cfg::ShapeProto&))&::oneflow::cfg::ShapeProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeProto::DebugString);



    registry.def("dim_size", &::oneflow::cfg::ShapeProto::dim_size);
    registry.def("clear_dim", &::oneflow::cfg::ShapeProto::clear_dim);
    registry.def("mutable_dim", (::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::ShapeProto::*)())&::oneflow::cfg::ShapeProto::shared_mutable_dim);
    registry.def("dim", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::ShapeProto::*)() const)&::oneflow::cfg::ShapeProto::shared_const_dim);
    registry.def("dim", (const int64_t& (::oneflow::cfg::ShapeProto::*)(::std::size_t) const)&::oneflow::cfg::ShapeProto::dim);
    registry.def("add_dim", &::oneflow::cfg::ShapeProto::add_dim);
    registry.def("set_dim", &::oneflow::cfg::ShapeProto::set_dim);
  }
  {
    pybind11::class_<ConstShapeSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeSignature>> registry(m, "ConstShapeSignature");
    registry.def("__id__", &::oneflow::cfg::ShapeSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeSignature::DebugString);
    registry.def("__repr__", &ConstShapeSignature::DebugString);

    registry.def("has_name", &ConstShapeSignature::has_name);
    registry.def("name", &ConstShapeSignature::name);

    registry.def("field2shape_proto_size", &ConstShapeSignature::field2shape_proto_size);
    registry.def("field2shape_proto", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> (ConstShapeSignature::*)() const)&ConstShapeSignature::shared_const_field2shape_proto);

    registry.def("field2shape_proto", (::std::shared_ptr<ConstShapeProto> (ConstShapeSignature::*)(const ::std::string&) const)&ConstShapeSignature::shared_const_field2shape_proto);
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeSignature>> registry(m, "ShapeSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeSignature::*)(const ConstShapeSignature&))&::oneflow::cfg::ShapeSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeSignature::*)(const ::oneflow::cfg::ShapeSignature&))&::oneflow::cfg::ShapeSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeSignature::DebugString);



    registry.def("has_name", &::oneflow::cfg::ShapeSignature::has_name);
    registry.def("clear_name", &::oneflow::cfg::ShapeSignature::clear_name);
    registry.def("name", &::oneflow::cfg::ShapeSignature::name);
    registry.def("set_name", &::oneflow::cfg::ShapeSignature::set_name);

    registry.def("field2shape_proto_size", &::oneflow::cfg::ShapeSignature::field2shape_proto_size);
    registry.def("field2shape_proto", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> (::oneflow::cfg::ShapeSignature::*)() const)&::oneflow::cfg::ShapeSignature::shared_const_field2shape_proto);
    registry.def("clear_field2shape_proto", &::oneflow::cfg::ShapeSignature::clear_field2shape_proto);
    registry.def("mutable_field2shape_proto", (::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__MapField___std__string_ShapeProto_> (::oneflow::cfg::ShapeSignature::*)())&::oneflow::cfg::ShapeSignature::shared_mutable_field2shape_proto);
    registry.def("field2shape_proto", (::std::shared_ptr<ConstShapeProto> (::oneflow::cfg::ShapeSignature::*)(const ::std::string&) const)&::oneflow::cfg::ShapeSignature::shared_const_field2shape_proto);
  }
  {
    pybind11::class_<ConstShapeSignatureList, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeSignatureList>> registry(m, "ConstShapeSignatureList");
    registry.def("__id__", &::oneflow::cfg::ShapeSignatureList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeSignatureList::DebugString);
    registry.def("__repr__", &ConstShapeSignatureList::DebugString);

    registry.def("shape_signature_size", &ConstShapeSignatureList::shape_signature_size);
    registry.def("shape_signature", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> (ConstShapeSignatureList::*)() const)&ConstShapeSignatureList::shared_const_shape_signature);
    registry.def("shape_signature", (::std::shared_ptr<ConstShapeSignature> (ConstShapeSignatureList::*)(::std::size_t) const)&ConstShapeSignatureList::shared_const_shape_signature);
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeSignatureList, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeSignatureList>> registry(m, "ShapeSignatureList");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeSignatureList::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeSignatureList::*)(const ConstShapeSignatureList&))&::oneflow::cfg::ShapeSignatureList::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeSignatureList::*)(const ::oneflow::cfg::ShapeSignatureList&))&::oneflow::cfg::ShapeSignatureList::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeSignatureList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeSignatureList::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeSignatureList::DebugString);



    registry.def("shape_signature_size", &::oneflow::cfg::ShapeSignatureList::shape_signature_size);
    registry.def("clear_shape_signature", &::oneflow::cfg::ShapeSignatureList::clear_shape_signature);
    registry.def("mutable_shape_signature", (::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> (::oneflow::cfg::ShapeSignatureList::*)())&::oneflow::cfg::ShapeSignatureList::shared_mutable_shape_signature);
    registry.def("shape_signature", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_SHAPE_CFG_H__RepeatedField_ShapeSignature_> (::oneflow::cfg::ShapeSignatureList::*)() const)&::oneflow::cfg::ShapeSignatureList::shared_const_shape_signature);
    registry.def("shape_signature", (::std::shared_ptr<ConstShapeSignature> (::oneflow::cfg::ShapeSignatureList::*)(::std::size_t) const)&::oneflow::cfg::ShapeSignatureList::shared_const_shape_signature);
    registry.def("mutable_shape_signature", (::std::shared_ptr<::oneflow::cfg::ShapeSignature> (::oneflow::cfg::ShapeSignatureList::*)(::std::size_t))&::oneflow::cfg::ShapeSignatureList::shared_mutable_shape_signature);
  }
}