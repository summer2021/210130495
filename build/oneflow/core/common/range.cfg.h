#ifndef CFG_ONEFLOW_CORE_COMMON_RANGE_CFG_H_
#define CFG_ONEFLOW_CORE_COMMON_RANGE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class RangeProto;

namespace cfg {


class RangeProto;
class ConstRangeProto;



class ConstRangeProto : public ::oneflow::cfg::Message {
 public:

  class _RangeProto_ {
   public:
    _RangeProto_();
    explicit _RangeProto_(const _RangeProto_& other);
    explicit _RangeProto_(_RangeProto_&& other);
    _RangeProto_(const ::oneflow::RangeProto& proto_rangeproto);
    ~_RangeProto_();

    void InitFromProto(const ::oneflow::RangeProto& proto_rangeproto);

    void ToProto(::oneflow::RangeProto* proto_rangeproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RangeProto_& other);
  
      // optional field begin
     public:
    bool has_begin() const;
    const int64_t& begin() const;
    void clear_begin();
    void set_begin(const int64_t& value);
    int64_t* mutable_begin();
   protected:
    bool has_begin_ = false;
    int64_t begin_;
      
      // optional field end
     public:
    bool has_end() const;
    const int64_t& end() const;
    void clear_end();
    void set_end(const int64_t& value);
    int64_t* mutable_end();
   protected:
    bool has_end_ = false;
    int64_t end_;
           
   public:
    int compare(const _RangeProto_& other);

    bool operator==(const _RangeProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RangeProto_& other) const;
  };

  ConstRangeProto(const ::std::shared_ptr<_RangeProto_>& data);
  ConstRangeProto(const ConstRangeProto&);
  ConstRangeProto(ConstRangeProto&&) noexcept;
  ConstRangeProto();
  ConstRangeProto(const ::oneflow::RangeProto& proto_rangeproto);
  virtual ~ConstRangeProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_rangeproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field begin
 public:
  bool has_begin() const;
  const int64_t& begin() const;
  // used by pybind11 only
  // required or optional field end
 public:
  bool has_end() const;
  const int64_t& end() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRangeProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRangeProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRangeProto& other) const;
 protected:
  const ::std::shared_ptr<_RangeProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RangeProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRangeProto
  void BuildFromProto(const PbMessage& proto_rangeproto);
  
  ::std::shared_ptr<_RangeProto_> data_;
};

class RangeProto final : public ConstRangeProto {
 public:
  RangeProto(const ::std::shared_ptr<_RangeProto_>& data);
  RangeProto(const RangeProto& other);
  // enable nothrow for ::std::vector<RangeProto> resize 
  RangeProto(RangeProto&&) noexcept;
  RangeProto();
  explicit RangeProto(const ::oneflow::RangeProto& proto_rangeproto);

  ~RangeProto() override;

  void InitFromProto(const PbMessage& proto_rangeproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RangeProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RangeProto& other) const;
  void Clear();
  void CopyFrom(const RangeProto& other);
  RangeProto& operator=(const RangeProto& other);

  // required or optional field begin
 public:
  void clear_begin();
  void set_begin(const int64_t& value);
  int64_t* mutable_begin();
  // required or optional field end
 public:
  void clear_end();
  void set_end(const int64_t& value);
  int64_t* mutable_end();

  ::std::shared_ptr<RangeProto> __SharedMutable__();
};






} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstRangeProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstRangeProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RangeProto> {
  std::size_t operator()(const ::oneflow::cfg::RangeProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_COMMON_RANGE_CFG_H_