#include "oneflow/core/common/range.cfg.h"
#include "oneflow/core/common/range.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstRangeProto::_RangeProto_::_RangeProto_() { Clear(); }
ConstRangeProto::_RangeProto_::_RangeProto_(const _RangeProto_& other) { CopyFrom(other); }
ConstRangeProto::_RangeProto_::_RangeProto_(const ::oneflow::RangeProto& proto_rangeproto) {
  InitFromProto(proto_rangeproto);
}
ConstRangeProto::_RangeProto_::_RangeProto_(_RangeProto_&& other) = default;
ConstRangeProto::_RangeProto_::~_RangeProto_() = default;

void ConstRangeProto::_RangeProto_::InitFromProto(const ::oneflow::RangeProto& proto_rangeproto) {
  Clear();
  // required_or_optional field: begin
  if (proto_rangeproto.has_begin()) {
    set_begin(proto_rangeproto.begin());
  }
  // required_or_optional field: end
  if (proto_rangeproto.has_end()) {
    set_end(proto_rangeproto.end());
  }
    
}

void ConstRangeProto::_RangeProto_::ToProto(::oneflow::RangeProto* proto_rangeproto) const {
  proto_rangeproto->Clear();
  // required_or_optional field: begin
  if (this->has_begin()) {
    proto_rangeproto->set_begin(begin());
    }
  // required_or_optional field: end
  if (this->has_end()) {
    proto_rangeproto->set_end(end());
    }

}

::std::string ConstRangeProto::_RangeProto_::DebugString() const {
  ::oneflow::RangeProto proto_rangeproto;
  this->ToProto(&proto_rangeproto);
  return proto_rangeproto.DebugString();
}

void ConstRangeProto::_RangeProto_::Clear() {
  clear_begin();
  clear_end();
}

void ConstRangeProto::_RangeProto_::CopyFrom(const _RangeProto_& other) {
  if (other.has_begin()) {
    set_begin(other.begin());
  } else {
    clear_begin();
  }
  if (other.has_end()) {
    set_end(other.end());
  } else {
    clear_end();
  }
}


// optional field begin
bool ConstRangeProto::_RangeProto_::has_begin() const {
  return has_begin_;
}
const int64_t& ConstRangeProto::_RangeProto_::begin() const {
  if (has_begin_) { return begin_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstRangeProto::_RangeProto_::clear_begin() {
  has_begin_ = false;
}
void ConstRangeProto::_RangeProto_::set_begin(const int64_t& value) {
  begin_ = value;
  has_begin_ = true;
}
int64_t* ConstRangeProto::_RangeProto_::mutable_begin() {
  has_begin_ = true;
  return &begin_;
}

// optional field end
bool ConstRangeProto::_RangeProto_::has_end() const {
  return has_end_;
}
const int64_t& ConstRangeProto::_RangeProto_::end() const {
  if (has_end_) { return end_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstRangeProto::_RangeProto_::clear_end() {
  has_end_ = false;
}
void ConstRangeProto::_RangeProto_::set_end(const int64_t& value) {
  end_ = value;
  has_end_ = true;
}
int64_t* ConstRangeProto::_RangeProto_::mutable_end() {
  has_end_ = true;
  return &end_;
}


int ConstRangeProto::_RangeProto_::compare(const _RangeProto_& other) {
  if (!(has_begin() == other.has_begin())) {
    return has_begin() < other.has_begin() ? -1 : 1;
  } else if (!(begin() == other.begin())) {
    return begin() < other.begin() ? -1 : 1;
  }
  if (!(has_end() == other.has_end())) {
    return has_end() < other.has_end() ? -1 : 1;
  } else if (!(end() == other.end())) {
    return end() < other.end() ? -1 : 1;
  }
  return 0;
}

bool ConstRangeProto::_RangeProto_::operator==(const _RangeProto_& other) const {
  return true
    && has_begin() == other.has_begin() 
    && begin() == other.begin()
    && has_end() == other.has_end() 
    && end() == other.end()
  ;
}

std::size_t ConstRangeProto::_RangeProto_::__CalcHash__() const {
  return 0
    ^ (has_begin() ? std::hash<int64_t>()(begin()) : 0)
    ^ (has_end() ? std::hash<int64_t>()(end()) : 0)
  ;
}

bool ConstRangeProto::_RangeProto_::operator<(const _RangeProto_& other) const {
  return false
    || !(has_begin() == other.has_begin()) ? 
      has_begin() < other.has_begin() : false
    || !(begin() == other.begin()) ? 
      begin() < other.begin() : false
    || !(has_end() == other.has_end()) ? 
      has_end() < other.has_end() : false
    || !(end() == other.end()) ? 
      end() < other.end() : false
  ;
}

using _RangeProto_ =  ConstRangeProto::_RangeProto_;
ConstRangeProto::ConstRangeProto(const ::std::shared_ptr<_RangeProto_>& data): data_(data) {}
ConstRangeProto::ConstRangeProto(): data_(::std::make_shared<_RangeProto_>()) {}
ConstRangeProto::ConstRangeProto(const ::oneflow::RangeProto& proto_rangeproto) {
  BuildFromProto(proto_rangeproto);
}
ConstRangeProto::ConstRangeProto(const ConstRangeProto&) = default;
ConstRangeProto::ConstRangeProto(ConstRangeProto&&) noexcept = default;
ConstRangeProto::~ConstRangeProto() = default;

void ConstRangeProto::ToProto(PbMessage* proto_rangeproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RangeProto*>(proto_rangeproto));
}
  
::std::string ConstRangeProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRangeProto::__Empty__() const {
  return !data_;
}

int ConstRangeProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"begin", 1},
    {"end", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRangeProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRangeProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRangeProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &begin();
    case 2: return &end();
    default: return nullptr;
  }
}

// required or optional field begin
bool ConstRangeProto::has_begin() const {
  return __SharedPtrOrDefault__()->has_begin();
}
const int64_t& ConstRangeProto::begin() const {
  return __SharedPtrOrDefault__()->begin();
}
// used by pybind11 only
// required or optional field end
bool ConstRangeProto::has_end() const {
  return __SharedPtrOrDefault__()->has_end();
}
const int64_t& ConstRangeProto::end() const {
  return __SharedPtrOrDefault__()->end();
}
// used by pybind11 only

::std::shared_ptr<ConstRangeProto> ConstRangeProto::__SharedConst__() const {
  return ::std::make_shared<ConstRangeProto>(data_);
}
int64_t ConstRangeProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRangeProto::operator==(const ConstRangeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRangeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRangeProto::operator<(const ConstRangeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RangeProto_>& ConstRangeProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RangeProto_> default_ptr = std::make_shared<_RangeProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_RangeProto_>& ConstRangeProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _RangeProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRangeProto
void ConstRangeProto::BuildFromProto(const PbMessage& proto_rangeproto) {
  data_ = ::std::make_shared<_RangeProto_>(dynamic_cast<const ::oneflow::RangeProto&>(proto_rangeproto));
}

RangeProto::RangeProto(const ::std::shared_ptr<_RangeProto_>& data)
  : ConstRangeProto(data) {}
RangeProto::RangeProto(const RangeProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RangeProto> resize
RangeProto::RangeProto(RangeProto&&) noexcept = default; 
RangeProto::RangeProto(const ::oneflow::RangeProto& proto_rangeproto) {
  InitFromProto(proto_rangeproto);
}
RangeProto::RangeProto() = default;

RangeProto::~RangeProto() = default;

void RangeProto::InitFromProto(const PbMessage& proto_rangeproto) {
  BuildFromProto(proto_rangeproto);
}
  
void* RangeProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_begin();
    case 2: return mutable_end();
    default: return nullptr;
  }
}

bool RangeProto::operator==(const RangeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RangeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RangeProto::operator<(const RangeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RangeProto::Clear() {
  if (data_) { data_.reset(); }
}
void RangeProto::CopyFrom(const RangeProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RangeProto& RangeProto::operator=(const RangeProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field begin
void RangeProto::clear_begin() {
  return __SharedPtr__()->clear_begin();
}
void RangeProto::set_begin(const int64_t& value) {
  return __SharedPtr__()->set_begin(value);
}
int64_t* RangeProto::mutable_begin() {
  return  __SharedPtr__()->mutable_begin();
}
// required or optional field end
void RangeProto::clear_end() {
  return __SharedPtr__()->clear_end();
}
void RangeProto::set_end(const int64_t& value) {
  return __SharedPtr__()->set_end(value);
}
int64_t* RangeProto::mutable_end() {
  return  __SharedPtr__()->mutable_end();
}

::std::shared_ptr<RangeProto> RangeProto::__SharedMutable__() {
  return ::std::make_shared<RangeProto>(__SharedPtr__());
}


}
} // namespace oneflow
