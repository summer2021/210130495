#include "oneflow/core/common/cfg_reflection_test.cfg.h"
#include "oneflow/core/common/cfg_reflection_test.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstReflectionTestFoo::_ReflectionTestFoo_::_ReflectionTestFoo_() { Clear(); }
ConstReflectionTestFoo::_ReflectionTestFoo_::_ReflectionTestFoo_(const _ReflectionTestFoo_& other) { CopyFrom(other); }
ConstReflectionTestFoo::_ReflectionTestFoo_::_ReflectionTestFoo_(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo) {
  InitFromProto(proto_reflectiontestfoo);
}
ConstReflectionTestFoo::_ReflectionTestFoo_::_ReflectionTestFoo_(_ReflectionTestFoo_&& other) = default;
ConstReflectionTestFoo::_ReflectionTestFoo_::~_ReflectionTestFoo_() = default;

void ConstReflectionTestFoo::_ReflectionTestFoo_::InitFromProto(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo) {
  Clear();
  // required_or_optional field: required_int32
  if (proto_reflectiontestfoo.has_required_int32()) {
    set_required_int32(proto_reflectiontestfoo.required_int32());
  }
  // required_or_optional field: optional_string
  if (proto_reflectiontestfoo.has_optional_string()) {
    set_optional_string(proto_reflectiontestfoo.optional_string());
  }
  // repeated field: repeated_int32
  if (!proto_reflectiontestfoo.repeated_int32().empty()) {
    for (const int32_t& elem : proto_reflectiontestfoo.repeated_int32()) {
      add_repeated_int32(elem);
    }
  }
  // repeated field: repeated_string
  if (!proto_reflectiontestfoo.repeated_string().empty()) {
    for (const ::std::string& elem : proto_reflectiontestfoo.repeated_string()) {
      add_repeated_string(elem);
    }
  }
  // map field : map_int32
  if (!proto_reflectiontestfoo.map_int32().empty()) {
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_&  mut_map_int32 = *mutable_map_int32();
    for (const auto& pair : proto_reflectiontestfoo.map_int32()) {
      mut_map_int32[pair.first] = pair.second;
      }
  }
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_reflectiontestfoo.type_case()));
  switch (type_case) {
    case kOneofInt32: {
      set_oneof_int32(proto_reflectiontestfoo.oneof_int32());
      break;
  }
    case kAnotherOneofInt32: {
      set_another_oneof_int32(proto_reflectiontestfoo.another_oneof_int32());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstReflectionTestFoo::_ReflectionTestFoo_::ToProto(::oneflow::ReflectionTestFoo* proto_reflectiontestfoo) const {
  proto_reflectiontestfoo->Clear();
  // required_or_optional field: required_int32
  if (this->has_required_int32()) {
    proto_reflectiontestfoo->set_required_int32(required_int32());
    }
  // required_or_optional field: optional_string
  if (this->has_optional_string()) {
    proto_reflectiontestfoo->set_optional_string(optional_string());
    }
  // repeated field: repeated_int32
  if (!repeated_int32().empty()) {
    for (const int32_t& elem : repeated_int32()) {
      proto_reflectiontestfoo->add_repeated_int32(elem);
    }
  }
  // repeated field: repeated_string
  if (!repeated_string().empty()) {
    for (const ::std::string& elem : repeated_string()) {
      proto_reflectiontestfoo->add_repeated_string(elem);
    }
  }
  // map field : map_int32
  if (!map_int32().empty()) {
    auto& mut_map_int32 = *(proto_reflectiontestfoo->mutable_map_int32());
    for (const auto& pair : map_int32()) {
      mut_map_int32[pair.first] = pair.second;
    }
  }

  // oneof field: type
  ::oneflow::ReflectionTestFoo::TypeCase type_case = ::oneflow::ReflectionTestFoo::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::ReflectionTestFoo::kOneofInt32: {
      proto_reflectiontestfoo->set_oneof_int32(oneof_int32());
      break;
    }
    case ::oneflow::ReflectionTestFoo::kAnotherOneofInt32: {
      proto_reflectiontestfoo->set_another_oneof_int32(another_oneof_int32());
      break;
    }
    case ::oneflow::ReflectionTestFoo::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstReflectionTestFoo::_ReflectionTestFoo_::DebugString() const {
  ::oneflow::ReflectionTestFoo proto_reflectiontestfoo;
  this->ToProto(&proto_reflectiontestfoo);
  return proto_reflectiontestfoo.DebugString();
}

void ConstReflectionTestFoo::_ReflectionTestFoo_::Clear() {
  clear_required_int32();
  clear_optional_string();
  clear_repeated_int32();
  clear_repeated_string();
  clear_map_int32();
  clear_type();
}

void ConstReflectionTestFoo::_ReflectionTestFoo_::CopyFrom(const _ReflectionTestFoo_& other) {
  if (other.has_required_int32()) {
    set_required_int32(other.required_int32());
  } else {
    clear_required_int32();
  }
  if (other.has_optional_string()) {
    set_optional_string(other.optional_string());
  } else {
    clear_optional_string();
  }
  mutable_repeated_int32()->CopyFrom(other.repeated_int32());
  mutable_repeated_string()->CopyFrom(other.repeated_string());
  mutable_map_int32()->CopyFrom(other.map_int32());
  type_copy_from(other);
}


// optional field required_int32
bool ConstReflectionTestFoo::_ReflectionTestFoo_::has_required_int32() const {
  return has_required_int32_;
}
const int32_t& ConstReflectionTestFoo::_ReflectionTestFoo_::required_int32() const {
  if (has_required_int32_) { return required_int32_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_required_int32() {
  has_required_int32_ = false;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_required_int32(const int32_t& value) {
  required_int32_ = value;
  has_required_int32_ = true;
}
int32_t* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_required_int32() {
  has_required_int32_ = true;
  return &required_int32_;
}

// optional field optional_string
bool ConstReflectionTestFoo::_ReflectionTestFoo_::has_optional_string() const {
  return has_optional_string_;
}
const ::std::string& ConstReflectionTestFoo::_ReflectionTestFoo_::optional_string() const {
  if (has_optional_string_) { return optional_string_; }
  static const ::std::string default_static_value =
    ::std::string("undefined");
  return default_static_value;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_optional_string() {
  has_optional_string_ = false;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_optional_string(const ::std::string& value) {
  optional_string_ = value;
  has_optional_string_ = true;
}
::std::string* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_optional_string() {
  has_optional_string_ = true;
  return &optional_string_;
}

// repeated field repeated_int32
::std::size_t ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_int32_size() const {
  if (!repeated_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->size();
  }
  return repeated_int32_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_int32() const {
  if (!repeated_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
    return *(default_static_value.get());
  }
  return *(repeated_int32_.get());
}
const int32_t& ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_int32(::std::size_t index) const {
  if (!repeated_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->Get(index);
  }
  return repeated_int32_->Get(index);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_repeated_int32() {
  if (!repeated_int32_) {
    repeated_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
  }
  return repeated_int32_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_repeated_int32() {
  if (!repeated_int32_) {
    repeated_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
  }
  return  repeated_int32_.get();
}
int32_t* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_repeated_int32(::std::size_t index) {
  if (!repeated_int32_) {
    repeated_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
  }
  return  repeated_int32_->Mutable(index);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::add_repeated_int32(const int32_t& value) {
  if (!repeated_int32_) {
    repeated_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
  }
  return repeated_int32_->Add(value);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_repeated_int32(::std::size_t index, const int32_t& value) {
  if (!repeated_int32_) {
    repeated_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>();
  }
  return repeated_int32_->Set(index, value);
}

// repeated field repeated_string
::std::size_t ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_string_size() const {
  if (!repeated_string_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return repeated_string_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_string() const {
  if (!repeated_string_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(repeated_string_.get());
}
const ::std::string& ConstReflectionTestFoo::_ReflectionTestFoo_::repeated_string(::std::size_t index) const {
  if (!repeated_string_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return repeated_string_->Get(index);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_repeated_string() {
  if (!repeated_string_) {
    repeated_string_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
  }
  return repeated_string_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_repeated_string() {
  if (!repeated_string_) {
    repeated_string_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
  }
  return  repeated_string_.get();
}
::std::string* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_repeated_string(::std::size_t index) {
  if (!repeated_string_) {
    repeated_string_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
  }
  return  repeated_string_->Mutable(index);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::add_repeated_string(const ::std::string& value) {
  if (!repeated_string_) {
    repeated_string_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
  }
  return repeated_string_->Add(value);
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_repeated_string(::std::size_t index, const ::std::string& value) {
  if (!repeated_string_) {
    repeated_string_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>();
  }
  return repeated_string_->Set(index, value);
}

::std::size_t ConstReflectionTestFoo::_ReflectionTestFoo_::map_int32_size() const {
  if (!map_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>();
    return default_static_value->size();
  }
  return map_int32_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& ConstReflectionTestFoo::_ReflectionTestFoo_::map_int32() const {
  if (!map_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>();
    return *(default_static_value.get());
  }
  return *(map_int32_.get());
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ * ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_map_int32() {
  if (!map_int32_) {
    map_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>();
  }
  return map_int32_.get();
}

const int32_t& ConstReflectionTestFoo::_ReflectionTestFoo_::map_int32(int32_t key) const {
  if (!map_int32_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>();
    return default_static_value->at(key);
  }
  return map_int32_->at(key);
}

void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_map_int32() {
  if (!map_int32_) {
    map_int32_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>();
  }
  return map_int32_->Clear();
}


// oneof field type: oneof_int32
bool ConstReflectionTestFoo::_ReflectionTestFoo_::has_oneof_int32() const {
  return type_case() == kOneofInt32;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_oneof_int32() {
  if (has_oneof_int32()) {
    type_.oneof_int32_ = int32_t();
    type_case_ = TYPE_NOT_SET;
  }
}

const int32_t& ConstReflectionTestFoo::_ReflectionTestFoo_::oneof_int32() const {
  if (has_oneof_int32()) {
      return type_.oneof_int32_;
    } else {
      static const int32_t default_static_value = int32_t();
    return default_static_value;
    }
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_oneof_int32(const int32_t& value) {
  if(!has_oneof_int32()) {
    clear_type();
    }
  type_case_ = kOneofInt32;
    type_.oneof_int32_ = value;
  }
int32_t* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_oneof_int32() {
  if(!has_oneof_int32()) {
    clear_type();
    }
    type_case_ = kOneofInt32;
  return  &type_.oneof_int32_;
  }

// oneof field type: another_oneof_int32
bool ConstReflectionTestFoo::_ReflectionTestFoo_::has_another_oneof_int32() const {
  return type_case() == kAnotherOneofInt32;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_another_oneof_int32() {
  if (has_another_oneof_int32()) {
    type_.another_oneof_int32_ = int32_t();
    type_case_ = TYPE_NOT_SET;
  }
}

const int32_t& ConstReflectionTestFoo::_ReflectionTestFoo_::another_oneof_int32() const {
  if (has_another_oneof_int32()) {
      return type_.another_oneof_int32_;
    } else {
      static const int32_t default_static_value = int32_t();
    return default_static_value;
    }
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::set_another_oneof_int32(const int32_t& value) {
  if(!has_another_oneof_int32()) {
    clear_type();
    }
  type_case_ = kAnotherOneofInt32;
    type_.another_oneof_int32_ = value;
  }
int32_t* ConstReflectionTestFoo::_ReflectionTestFoo_::mutable_another_oneof_int32() {
  if(!has_another_oneof_int32()) {
    clear_type();
    }
    type_case_ = kAnotherOneofInt32;
  return  &type_.another_oneof_int32_;
  }
ConstReflectionTestFoo::TypeCase ConstReflectionTestFoo::_ReflectionTestFoo_::type_case() const {
  return type_case_;
}
bool ConstReflectionTestFoo::_ReflectionTestFoo_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::clear_type() {
  switch (type_case()) {
    case kOneofInt32: {
      type_.oneof_int32_ = int32_t();
      break;
    }
    case kAnotherOneofInt32: {
      type_.another_oneof_int32_ = int32_t();
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstReflectionTestFoo::_ReflectionTestFoo_::type_copy_from(const _ReflectionTestFoo_& other) {
  switch (other.type_case()) {
    case kOneofInt32: {
      set_oneof_int32(other.oneof_int32());
      break;
    }
    case kAnotherOneofInt32: {
      set_another_oneof_int32(other.another_oneof_int32());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstReflectionTestFoo::_ReflectionTestFoo_::compare(const _ReflectionTestFoo_& other) {
  if (!(has_required_int32() == other.has_required_int32())) {
    return has_required_int32() < other.has_required_int32() ? -1 : 1;
  } else if (!(required_int32() == other.required_int32())) {
    return required_int32() < other.required_int32() ? -1 : 1;
  }
  if (!(has_optional_string() == other.has_optional_string())) {
    return has_optional_string() < other.has_optional_string() ? -1 : 1;
  } else if (!(optional_string() == other.optional_string())) {
    return optional_string() < other.optional_string() ? -1 : 1;
  }
  if (!(repeated_int32() == other.repeated_int32())) {
    return repeated_int32() < other.repeated_int32() ? -1 : 1;
  }
  if (!(repeated_string() == other.repeated_string())) {
    return repeated_string() < other.repeated_string() ? -1 : 1;
  }
  if (!(map_int32() == other.map_int32())) {
    return map_int32() < other.map_int32() ? -1 : 1;
  }
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kOneofInt32: {
      if (!(oneof_int32() == other.oneof_int32())) {
        return oneof_int32() < other.oneof_int32() ? -1 : 1;
      }
      break;
    }
    case kAnotherOneofInt32: {
      if (!(another_oneof_int32() == other.another_oneof_int32())) {
        return another_oneof_int32() < other.another_oneof_int32() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstReflectionTestFoo::_ReflectionTestFoo_::operator==(const _ReflectionTestFoo_& other) const {
  return true
    && has_required_int32() == other.has_required_int32() 
    && required_int32() == other.required_int32()
    && has_optional_string() == other.has_optional_string() 
    && optional_string() == other.optional_string()
    && repeated_int32() == other.repeated_int32()
    && repeated_string() == other.repeated_string()
    && map_int32() == other.map_int32()
    && type_case() == other.type_case()
    && (type_case() == kOneofInt32 ? 
      oneof_int32() == other.oneof_int32() : true)
    && (type_case() == kAnotherOneofInt32 ? 
      another_oneof_int32() == other.another_oneof_int32() : true)
  ;
}

std::size_t ConstReflectionTestFoo::_ReflectionTestFoo_::__CalcHash__() const {
  return 0
    ^ (has_required_int32() ? std::hash<int32_t>()(required_int32()) : 0)
    ^ (has_optional_string() ? std::hash<::std::string>()(optional_string()) : 0)
    ^ repeated_int32().__CalcHash__()
    ^ repeated_string().__CalcHash__()
    ^ map_int32().__CalcHash__()
    ^ static_cast<std::size_t>(type_case())
    ^ (has_oneof_int32() ? std::hash<int32_t>()(oneof_int32()) : 0)
    ^ (has_another_oneof_int32() ? std::hash<int32_t>()(another_oneof_int32()) : 0)
  ;
}

bool ConstReflectionTestFoo::_ReflectionTestFoo_::operator<(const _ReflectionTestFoo_& other) const {
  return false
    || !(has_required_int32() == other.has_required_int32()) ? 
      has_required_int32() < other.has_required_int32() : false
    || !(required_int32() == other.required_int32()) ? 
      required_int32() < other.required_int32() : false
    || !(has_optional_string() == other.has_optional_string()) ? 
      has_optional_string() < other.has_optional_string() : false
    || !(optional_string() == other.optional_string()) ? 
      optional_string() < other.optional_string() : false
    || !(repeated_int32() == other.repeated_int32()) ? 
      repeated_int32() < other.repeated_int32() : false
    || !(repeated_string() == other.repeated_string()) ? 
      repeated_string() < other.repeated_string() : false
    || !(map_int32() == other.map_int32()) ? 
      map_int32() < other.map_int32() : false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kOneofInt32) && 
      !(oneof_int32() == other.oneof_int32())) ? 
        oneof_int32() < other.oneof_int32() : false
    || ((type_case() == kAnotherOneofInt32) && 
      !(another_oneof_int32() == other.another_oneof_int32())) ? 
        another_oneof_int32() < other.another_oneof_int32() : false
  ;
}

using _ReflectionTestFoo_ =  ConstReflectionTestFoo::_ReflectionTestFoo_;
ConstReflectionTestFoo::ConstReflectionTestFoo(const ::std::shared_ptr<_ReflectionTestFoo_>& data): data_(data) {}
ConstReflectionTestFoo::ConstReflectionTestFoo(): data_(::std::make_shared<_ReflectionTestFoo_>()) {}
ConstReflectionTestFoo::ConstReflectionTestFoo(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo) {
  BuildFromProto(proto_reflectiontestfoo);
}
ConstReflectionTestFoo::ConstReflectionTestFoo(const ConstReflectionTestFoo&) = default;
ConstReflectionTestFoo::ConstReflectionTestFoo(ConstReflectionTestFoo&&) noexcept = default;
ConstReflectionTestFoo::~ConstReflectionTestFoo() = default;

void ConstReflectionTestFoo::ToProto(PbMessage* proto_reflectiontestfoo) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ReflectionTestFoo*>(proto_reflectiontestfoo));
}
  
::std::string ConstReflectionTestFoo::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstReflectionTestFoo::__Empty__() const {
  return !data_;
}

int ConstReflectionTestFoo::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"required_int32", 1},
    {"optional_string", 2},
    {"repeated_int32", 3},
    {"repeated_string", 4},
    {"map_int32", 5},
    {"oneof_int32", 6},
    {"another_oneof_int32", 7},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstReflectionTestFoo::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstReflectionTestFoo::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int32_t>)
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<int32_t, int32_t>)
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstReflectionTestFoo::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &required_int32();
    case 2: return &optional_string();
    case 3: return &repeated_int32();
    case 4: return &repeated_string();
    case 5: return &map_int32();
    case 6: return &oneof_int32();
    case 7: return &another_oneof_int32();
    default: return nullptr;
  }
}

// required or optional field required_int32
bool ConstReflectionTestFoo::has_required_int32() const {
  return __SharedPtrOrDefault__()->has_required_int32();
}
const int32_t& ConstReflectionTestFoo::required_int32() const {
  return __SharedPtrOrDefault__()->required_int32();
}
// used by pybind11 only
// required or optional field optional_string
bool ConstReflectionTestFoo::has_optional_string() const {
  return __SharedPtrOrDefault__()->has_optional_string();
}
const ::std::string& ConstReflectionTestFoo::optional_string() const {
  return __SharedPtrOrDefault__()->optional_string();
}
// used by pybind11 only
// repeated field repeated_int32
::std::size_t ConstReflectionTestFoo::repeated_int32_size() const {
  return __SharedPtrOrDefault__()->repeated_int32_size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& ConstReflectionTestFoo::repeated_int32() const {
  return __SharedPtrOrDefault__()->repeated_int32();
}
const int32_t& ConstReflectionTestFoo::repeated_int32(::std::size_t index) const {
  return __SharedPtrOrDefault__()->repeated_int32(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> ConstReflectionTestFoo::shared_const_repeated_int32() const {
  return repeated_int32().__SharedConst__();
}
// repeated field repeated_string
::std::size_t ConstReflectionTestFoo::repeated_string_size() const {
  return __SharedPtrOrDefault__()->repeated_string_size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& ConstReflectionTestFoo::repeated_string() const {
  return __SharedPtrOrDefault__()->repeated_string();
}
const ::std::string& ConstReflectionTestFoo::repeated_string(::std::size_t index) const {
  return __SharedPtrOrDefault__()->repeated_string(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> ConstReflectionTestFoo::shared_const_repeated_string() const {
  return repeated_string().__SharedConst__();
}
// map field map_int32
::std::size_t ConstReflectionTestFoo::map_int32_size() const {
  return __SharedPtrOrDefault__()->map_int32_size();
}

const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& ConstReflectionTestFoo::map_int32() const {
  return __SharedPtrOrDefault__()->map_int32();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> ConstReflectionTestFoo::shared_const_map_int32() const {
  return map_int32().__SharedConst__();
}
 // oneof field type: oneof_int32
bool ConstReflectionTestFoo::has_oneof_int32() const {
  return __SharedPtrOrDefault__()->has_oneof_int32();
}
const int32_t& ConstReflectionTestFoo::oneof_int32() const {
  return __SharedPtrOrDefault__()->oneof_int32();
}

// used by pybind11 only
 // oneof field type: another_oneof_int32
bool ConstReflectionTestFoo::has_another_oneof_int32() const {
  return __SharedPtrOrDefault__()->has_another_oneof_int32();
}
const int32_t& ConstReflectionTestFoo::another_oneof_int32() const {
  return __SharedPtrOrDefault__()->another_oneof_int32();
}

// used by pybind11 only
ConstReflectionTestFoo::TypeCase ConstReflectionTestFoo::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstReflectionTestFoo::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstReflectionTestFoo> ConstReflectionTestFoo::__SharedConst__() const {
  return ::std::make_shared<ConstReflectionTestFoo>(data_);
}
int64_t ConstReflectionTestFoo::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstReflectionTestFoo::operator==(const ConstReflectionTestFoo& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstReflectionTestFoo::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstReflectionTestFoo::operator<(const ConstReflectionTestFoo& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ReflectionTestFoo_>& ConstReflectionTestFoo::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ReflectionTestFoo_> default_ptr = std::make_shared<_ReflectionTestFoo_>();
  return default_ptr;
}
const ::std::shared_ptr<_ReflectionTestFoo_>& ConstReflectionTestFoo::__SharedPtr__() {
  if (!data_) { data_.reset(new _ReflectionTestFoo_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstReflectionTestFoo
void ConstReflectionTestFoo::BuildFromProto(const PbMessage& proto_reflectiontestfoo) {
  data_ = ::std::make_shared<_ReflectionTestFoo_>(dynamic_cast<const ::oneflow::ReflectionTestFoo&>(proto_reflectiontestfoo));
}

ReflectionTestFoo::ReflectionTestFoo(const ::std::shared_ptr<_ReflectionTestFoo_>& data)
  : ConstReflectionTestFoo(data) {}
ReflectionTestFoo::ReflectionTestFoo(const ReflectionTestFoo& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ReflectionTestFoo> resize
ReflectionTestFoo::ReflectionTestFoo(ReflectionTestFoo&&) noexcept = default; 
ReflectionTestFoo::ReflectionTestFoo(const ::oneflow::ReflectionTestFoo& proto_reflectiontestfoo) {
  InitFromProto(proto_reflectiontestfoo);
}
ReflectionTestFoo::ReflectionTestFoo() = default;

ReflectionTestFoo::~ReflectionTestFoo() = default;

void ReflectionTestFoo::InitFromProto(const PbMessage& proto_reflectiontestfoo) {
  BuildFromProto(proto_reflectiontestfoo);
}
  
void* ReflectionTestFoo::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_required_int32();
    case 2: return mutable_optional_string();
    case 3: return mutable_repeated_int32();
    case 4: return mutable_repeated_string();
    case 5: return mutable_map_int32();
    case 6: return mutable_oneof_int32();
    case 7: return mutable_another_oneof_int32();
    default: return nullptr;
  }
}

bool ReflectionTestFoo::operator==(const ReflectionTestFoo& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ReflectionTestFoo::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ReflectionTestFoo::operator<(const ReflectionTestFoo& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ReflectionTestFoo::Clear() {
  if (data_) { data_.reset(); }
}
void ReflectionTestFoo::CopyFrom(const ReflectionTestFoo& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ReflectionTestFoo& ReflectionTestFoo::operator=(const ReflectionTestFoo& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field required_int32
void ReflectionTestFoo::clear_required_int32() {
  return __SharedPtr__()->clear_required_int32();
}
void ReflectionTestFoo::set_required_int32(const int32_t& value) {
  return __SharedPtr__()->set_required_int32(value);
}
int32_t* ReflectionTestFoo::mutable_required_int32() {
  return  __SharedPtr__()->mutable_required_int32();
}
// required or optional field optional_string
void ReflectionTestFoo::clear_optional_string() {
  return __SharedPtr__()->clear_optional_string();
}
void ReflectionTestFoo::set_optional_string(const ::std::string& value) {
  return __SharedPtr__()->set_optional_string(value);
}
::std::string* ReflectionTestFoo::mutable_optional_string() {
  return  __SharedPtr__()->mutable_optional_string();
}
// repeated field repeated_int32
void ReflectionTestFoo::clear_repeated_int32() {
  return __SharedPtr__()->clear_repeated_int32();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_* ReflectionTestFoo::mutable_repeated_int32() {
  return __SharedPtr__()->mutable_repeated_int32();
}
int32_t* ReflectionTestFoo::mutable_repeated_int32(::std::size_t index) {
  return __SharedPtr__()->mutable_repeated_int32(index);
}
void ReflectionTestFoo::add_repeated_int32(const int32_t& value) {
  return __SharedPtr__()->add_repeated_int32(value);
}
void ReflectionTestFoo::set_repeated_int32(::std::size_t index, const int32_t& value) {
  return __SharedPtr__()->set_repeated_int32(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> ReflectionTestFoo::shared_mutable_repeated_int32() {
  return mutable_repeated_int32()->__SharedMutable__();
}
// repeated field repeated_string
void ReflectionTestFoo::clear_repeated_string() {
  return __SharedPtr__()->clear_repeated_string();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_* ReflectionTestFoo::mutable_repeated_string() {
  return __SharedPtr__()->mutable_repeated_string();
}
::std::string* ReflectionTestFoo::mutable_repeated_string(::std::size_t index) {
  return __SharedPtr__()->mutable_repeated_string(index);
}
void ReflectionTestFoo::add_repeated_string(const ::std::string& value) {
  return __SharedPtr__()->add_repeated_string(value);
}
void ReflectionTestFoo::set_repeated_string(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_repeated_string(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> ReflectionTestFoo::shared_mutable_repeated_string() {
  return mutable_repeated_string()->__SharedMutable__();
}
// repeated field map_int32
void ReflectionTestFoo::clear_map_int32() {
  return __SharedPtr__()->clear_map_int32();
}

const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_ & ReflectionTestFoo::map_int32() const {
  return __SharedConst__()->map_int32();
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_* ReflectionTestFoo::mutable_map_int32() {
  return __SharedPtr__()->mutable_map_int32();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> ReflectionTestFoo::shared_mutable_map_int32() {
  return mutable_map_int32()->__SharedMutable__();
}
void ReflectionTestFoo::clear_oneof_int32() {
  return __SharedPtr__()->clear_oneof_int32();
}
void ReflectionTestFoo::set_oneof_int32(const int32_t& value) {
  return __SharedPtr__()->set_oneof_int32(value);
}
int32_t* ReflectionTestFoo::mutable_oneof_int32() {
  return  __SharedPtr__()->mutable_oneof_int32();
}
void ReflectionTestFoo::clear_another_oneof_int32() {
  return __SharedPtr__()->clear_another_oneof_int32();
}
void ReflectionTestFoo::set_another_oneof_int32(const int32_t& value) {
  return __SharedPtr__()->set_another_oneof_int32(value);
}
int32_t* ReflectionTestFoo::mutable_another_oneof_int32() {
  return  __SharedPtr__()->mutable_another_oneof_int32();
}

::std::shared_ptr<ReflectionTestFoo> ReflectionTestFoo::__SharedMutable__() {
  return ::std::make_shared<ReflectionTestFoo>(__SharedPtr__());
}
ConstReflectionTestBar::_ReflectionTestBar_::_ReflectionTestBar_() { Clear(); }
ConstReflectionTestBar::_ReflectionTestBar_::_ReflectionTestBar_(const _ReflectionTestBar_& other) { CopyFrom(other); }
ConstReflectionTestBar::_ReflectionTestBar_::_ReflectionTestBar_(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar) {
  InitFromProto(proto_reflectiontestbar);
}
ConstReflectionTestBar::_ReflectionTestBar_::_ReflectionTestBar_(_ReflectionTestBar_&& other) = default;
ConstReflectionTestBar::_ReflectionTestBar_::~_ReflectionTestBar_() = default;

void ConstReflectionTestBar::_ReflectionTestBar_::InitFromProto(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar) {
  Clear();
  // required_or_optional field: required_foo
  if (proto_reflectiontestbar.has_required_foo()) {
  *mutable_required_foo() = ::oneflow::cfg::ReflectionTestFoo(proto_reflectiontestbar.required_foo());      
  }
  // required_or_optional field: optional_foo
  if (proto_reflectiontestbar.has_optional_foo()) {
  *mutable_optional_foo() = ::oneflow::cfg::ReflectionTestFoo(proto_reflectiontestbar.optional_foo());      
  }
  // repeated field: repeated_foo
  if (!proto_reflectiontestbar.repeated_foo().empty()) {
    for (const ::oneflow::ReflectionTestFoo& elem : proto_reflectiontestbar.repeated_foo() ) {
      *mutable_repeated_foo()->Add() = ::oneflow::cfg::ReflectionTestFoo(elem);
    }
  }
  // map field : map_foo
  if (!proto_reflectiontestbar.map_foo().empty()) {
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_&  mut_map_foo = *mutable_map_foo();
    for (const auto& pair : proto_reflectiontestbar.map_foo()) {
      mut_map_foo[pair.first] = ::oneflow::cfg::ReflectionTestFoo(pair.second);
    }
  }
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_reflectiontestbar.type_case()));
  switch (type_case) {
    case kOneofFoo: {
      *mutable_oneof_foo() = ::oneflow::cfg::ReflectionTestFoo(proto_reflectiontestbar.oneof_foo());
      break;
  }
    case kAnotherOneofFoo: {
      *mutable_another_oneof_foo() = ::oneflow::cfg::ReflectionTestFoo(proto_reflectiontestbar.another_oneof_foo());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstReflectionTestBar::_ReflectionTestBar_::ToProto(::oneflow::ReflectionTestBar* proto_reflectiontestbar) const {
  proto_reflectiontestbar->Clear();
  // required_or_optional field: required_foo
  if (this->has_required_foo()) {
    ::oneflow::ReflectionTestFoo proto_required_foo;
    required_foo().ToProto(&proto_required_foo);
    proto_reflectiontestbar->mutable_required_foo()->CopyFrom(proto_required_foo);
    }
  // required_or_optional field: optional_foo
  if (this->has_optional_foo()) {
    ::oneflow::ReflectionTestFoo proto_optional_foo;
    optional_foo().ToProto(&proto_optional_foo);
    proto_reflectiontestbar->mutable_optional_foo()->CopyFrom(proto_optional_foo);
    }
  // repeated field: repeated_foo
  if (!repeated_foo().empty()) {
    for (const ::oneflow::cfg::ReflectionTestFoo& elem : repeated_foo() ) {
      ::oneflow::ReflectionTestFoo proto_repeated_foo_elem;
      elem.ToProto(&proto_repeated_foo_elem);
      *proto_reflectiontestbar->mutable_repeated_foo()->Add() = proto_repeated_foo_elem;
    }
  }
  // map field : map_foo
  if (!map_foo().empty()) {
    auto& mut_map_foo = *(proto_reflectiontestbar->mutable_map_foo());
    for (const auto& pair : map_foo()) {
      ::oneflow::ReflectionTestFoo proto_map_foo_value;
      pair.second.ToProto(&proto_map_foo_value);
      mut_map_foo[pair.first] = proto_map_foo_value;
    }
  }

  // oneof field: type
  ::oneflow::ReflectionTestBar::TypeCase type_case = ::oneflow::ReflectionTestBar::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::ReflectionTestBar::kOneofFoo: {
      ::oneflow::ReflectionTestFoo of_proto_oneof_foo;
      oneof_foo().ToProto(&of_proto_oneof_foo);
      proto_reflectiontestbar->mutable_oneof_foo()->CopyFrom(of_proto_oneof_foo);
      break;
    }
    case ::oneflow::ReflectionTestBar::kAnotherOneofFoo: {
      ::oneflow::ReflectionTestFoo of_proto_another_oneof_foo;
      another_oneof_foo().ToProto(&of_proto_another_oneof_foo);
      proto_reflectiontestbar->mutable_another_oneof_foo()->CopyFrom(of_proto_another_oneof_foo);
      break;
    }
    case ::oneflow::ReflectionTestBar::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstReflectionTestBar::_ReflectionTestBar_::DebugString() const {
  ::oneflow::ReflectionTestBar proto_reflectiontestbar;
  this->ToProto(&proto_reflectiontestbar);
  return proto_reflectiontestbar.DebugString();
}

void ConstReflectionTestBar::_ReflectionTestBar_::Clear() {
  clear_required_foo();
  clear_optional_foo();
  clear_repeated_foo();
  clear_map_foo();
  clear_type();
}

void ConstReflectionTestBar::_ReflectionTestBar_::CopyFrom(const _ReflectionTestBar_& other) {
  if (other.has_required_foo()) {
    mutable_required_foo()->CopyFrom(other.required_foo());
  } else {
    clear_required_foo();
  }
  if (other.has_optional_foo()) {
    mutable_optional_foo()->CopyFrom(other.optional_foo());
  } else {
    clear_optional_foo();
  }
  mutable_repeated_foo()->CopyFrom(other.repeated_foo());
  mutable_map_foo()->CopyFrom(other.map_foo());
  type_copy_from(other);
}


// optional field required_foo
bool ConstReflectionTestBar::_ReflectionTestBar_::has_required_foo() const {
  return has_required_foo_;
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::required_foo() const {
  if (!required_foo_) {
    static const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> default_static_value =
      ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
    return *default_static_value;
  }
  return *(required_foo_.get());
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_required_foo() {
  if (required_foo_) {
    required_foo_->Clear();
  }
  has_required_foo_ = false;
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::mutable_required_foo() {
  if (!required_foo_) {
    required_foo_ = ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
  }
  has_required_foo_ = true;
  return required_foo_.get();
}

// optional field optional_foo
bool ConstReflectionTestBar::_ReflectionTestBar_::has_optional_foo() const {
  return has_optional_foo_;
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::optional_foo() const {
  if (!optional_foo_) {
    static const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> default_static_value =
      ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
    return *default_static_value;
  }
  return *(optional_foo_.get());
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_optional_foo() {
  if (optional_foo_) {
    optional_foo_->Clear();
  }
  has_optional_foo_ = false;
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::mutable_optional_foo() {
  if (!optional_foo_) {
    optional_foo_ = ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
  }
  has_optional_foo_ = true;
  return optional_foo_.get();
}

// repeated field repeated_foo
::std::size_t ConstReflectionTestBar::_ReflectionTestBar_::repeated_foo_size() const {
  if (!repeated_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
    return default_static_value->size();
  }
  return repeated_foo_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& ConstReflectionTestBar::_ReflectionTestBar_::repeated_foo() const {
  if (!repeated_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
    return *(default_static_value.get());
  }
  return *(repeated_foo_.get());
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::repeated_foo(::std::size_t index) const {
  if (!repeated_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
    return default_static_value->Get(index);
  }
  return repeated_foo_->Get(index);
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_repeated_foo() {
  if (!repeated_foo_) {
    repeated_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
  }
  return repeated_foo_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_* ConstReflectionTestBar::_ReflectionTestBar_::mutable_repeated_foo() {
  if (!repeated_foo_) {
    repeated_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
  }
  return  repeated_foo_.get();
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::mutable_repeated_foo(::std::size_t index) {
  if (!repeated_foo_) {
    repeated_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
  }
  return  repeated_foo_->Mutable(index);
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::add_repeated_foo() {
  if (!repeated_foo_) {
    repeated_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>();
  }
  return repeated_foo_->Add();
}

::std::size_t ConstReflectionTestBar::_ReflectionTestBar_::map_foo_size() const {
  if (!map_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>();
    return default_static_value->size();
  }
  return map_foo_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& ConstReflectionTestBar::_ReflectionTestBar_::map_foo() const {
  if (!map_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>();
    return *(default_static_value.get());
  }
  return *(map_foo_.get());
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ * ConstReflectionTestBar::_ReflectionTestBar_::mutable_map_foo() {
  if (!map_foo_) {
    map_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>();
  }
  return map_foo_.get();
}

const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::map_foo(int32_t key) const {
  if (!map_foo_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>();
    return default_static_value->at(key);
  }
  return map_foo_->at(key);
}

void ConstReflectionTestBar::_ReflectionTestBar_::clear_map_foo() {
  if (!map_foo_) {
    map_foo_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>();
  }
  return map_foo_->Clear();
}


// oneof field type: oneof_foo
bool ConstReflectionTestBar::_ReflectionTestBar_::has_oneof_foo() const {
  return type_case() == kOneofFoo;
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_oneof_foo() {
  if (has_oneof_foo()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.oneof_foo_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::oneof_foo() const {
  if (has_oneof_foo()) {
      const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>*>(&(type_.oneof_foo_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> default_static_value = ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
    return *default_static_value;
    }
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::mutable_oneof_foo() {
  if(!has_oneof_foo()) {
    clear_type();
    new (&(type_.oneof_foo_)) ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>(new ::oneflow::cfg::ReflectionTestFoo());
  }
  type_case_ = kOneofFoo;
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>*>(&(type_.oneof_foo_));
  return  (*ptr).get();
}

// oneof field type: another_oneof_foo
bool ConstReflectionTestBar::_ReflectionTestBar_::has_another_oneof_foo() const {
  return type_case() == kAnotherOneofFoo;
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_another_oneof_foo() {
  if (has_another_oneof_foo()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.another_oneof_foo_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::_ReflectionTestBar_::another_oneof_foo() const {
  if (has_another_oneof_foo()) {
      const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>*>(&(type_.another_oneof_foo_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> default_static_value = ::std::make_shared<::oneflow::cfg::ReflectionTestFoo>();
    return *default_static_value;
    }
}
::oneflow::cfg::ReflectionTestFoo* ConstReflectionTestBar::_ReflectionTestBar_::mutable_another_oneof_foo() {
  if(!has_another_oneof_foo()) {
    clear_type();
    new (&(type_.another_oneof_foo_)) ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>(new ::oneflow::cfg::ReflectionTestFoo());
  }
  type_case_ = kAnotherOneofFoo;
  ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>*>(&(type_.another_oneof_foo_));
  return  (*ptr).get();
}
ConstReflectionTestBar::TypeCase ConstReflectionTestBar::_ReflectionTestBar_::type_case() const {
  return type_case_;
}
bool ConstReflectionTestBar::_ReflectionTestBar_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstReflectionTestBar::_ReflectionTestBar_::clear_type() {
  switch (type_case()) {
    case kOneofFoo: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.oneof_foo_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAnotherOneofFoo: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.another_oneof_foo_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstReflectionTestBar::_ReflectionTestBar_::type_copy_from(const _ReflectionTestBar_& other) {
  switch (other.type_case()) {
    case kOneofFoo: {
      mutable_oneof_foo()->CopyFrom(other.oneof_foo());
      break;
    }
    case kAnotherOneofFoo: {
      mutable_another_oneof_foo()->CopyFrom(other.another_oneof_foo());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstReflectionTestBar::_ReflectionTestBar_::compare(const _ReflectionTestBar_& other) {
  if (!(has_required_foo() == other.has_required_foo())) {
    return has_required_foo() < other.has_required_foo() ? -1 : 1;
  } else if (!(required_foo() == other.required_foo())) {
    return required_foo() < other.required_foo() ? -1 : 1;
  }
  if (!(has_optional_foo() == other.has_optional_foo())) {
    return has_optional_foo() < other.has_optional_foo() ? -1 : 1;
  } else if (!(optional_foo() == other.optional_foo())) {
    return optional_foo() < other.optional_foo() ? -1 : 1;
  }
  if (!(repeated_foo() == other.repeated_foo())) {
    return repeated_foo() < other.repeated_foo() ? -1 : 1;
  }
  if (!(map_foo() == other.map_foo())) {
    return map_foo() < other.map_foo() ? -1 : 1;
  }
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kOneofFoo: {
      if (!(oneof_foo() == other.oneof_foo())) {
        return oneof_foo() < other.oneof_foo() ? -1 : 1;
      }
      break;
    }
    case kAnotherOneofFoo: {
      if (!(another_oneof_foo() == other.another_oneof_foo())) {
        return another_oneof_foo() < other.another_oneof_foo() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstReflectionTestBar::_ReflectionTestBar_::operator==(const _ReflectionTestBar_& other) const {
  return true
    && has_required_foo() == other.has_required_foo() 
    && required_foo() == other.required_foo()
    && has_optional_foo() == other.has_optional_foo() 
    && optional_foo() == other.optional_foo()
    && repeated_foo() == other.repeated_foo()
    && map_foo() == other.map_foo()
    && type_case() == other.type_case()
    && (type_case() == kOneofFoo ? 
      oneof_foo() == other.oneof_foo() : true)
    && (type_case() == kAnotherOneofFoo ? 
      another_oneof_foo() == other.another_oneof_foo() : true)
  ;
}

std::size_t ConstReflectionTestBar::_ReflectionTestBar_::__CalcHash__() const {
  return 0
    ^ (has_required_foo() ? std::hash<::oneflow::cfg::ReflectionTestFoo>()(required_foo()) : 0)
    ^ (has_optional_foo() ? std::hash<::oneflow::cfg::ReflectionTestFoo>()(optional_foo()) : 0)
    ^ repeated_foo().__CalcHash__()
    ^ map_foo().__CalcHash__()
    ^ static_cast<std::size_t>(type_case())
    ^ (has_oneof_foo() ? std::hash<::oneflow::cfg::ReflectionTestFoo>()(oneof_foo()) : 0)
    ^ (has_another_oneof_foo() ? std::hash<::oneflow::cfg::ReflectionTestFoo>()(another_oneof_foo()) : 0)
  ;
}

bool ConstReflectionTestBar::_ReflectionTestBar_::operator<(const _ReflectionTestBar_& other) const {
  return false
    || !(has_required_foo() == other.has_required_foo()) ? 
      has_required_foo() < other.has_required_foo() : false
    || !(required_foo() == other.required_foo()) ? 
      required_foo() < other.required_foo() : false
    || !(has_optional_foo() == other.has_optional_foo()) ? 
      has_optional_foo() < other.has_optional_foo() : false
    || !(optional_foo() == other.optional_foo()) ? 
      optional_foo() < other.optional_foo() : false
    || !(repeated_foo() == other.repeated_foo()) ? 
      repeated_foo() < other.repeated_foo() : false
    || !(map_foo() == other.map_foo()) ? 
      map_foo() < other.map_foo() : false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kOneofFoo) && 
      !(oneof_foo() == other.oneof_foo())) ? 
        oneof_foo() < other.oneof_foo() : false
    || ((type_case() == kAnotherOneofFoo) && 
      !(another_oneof_foo() == other.another_oneof_foo())) ? 
        another_oneof_foo() < other.another_oneof_foo() : false
  ;
}

using _ReflectionTestBar_ =  ConstReflectionTestBar::_ReflectionTestBar_;
ConstReflectionTestBar::ConstReflectionTestBar(const ::std::shared_ptr<_ReflectionTestBar_>& data): data_(data) {}
ConstReflectionTestBar::ConstReflectionTestBar(): data_(::std::make_shared<_ReflectionTestBar_>()) {}
ConstReflectionTestBar::ConstReflectionTestBar(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar) {
  BuildFromProto(proto_reflectiontestbar);
}
ConstReflectionTestBar::ConstReflectionTestBar(const ConstReflectionTestBar&) = default;
ConstReflectionTestBar::ConstReflectionTestBar(ConstReflectionTestBar&&) noexcept = default;
ConstReflectionTestBar::~ConstReflectionTestBar() = default;

void ConstReflectionTestBar::ToProto(PbMessage* proto_reflectiontestbar) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ReflectionTestBar*>(proto_reflectiontestbar));
}
  
::std::string ConstReflectionTestBar::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstReflectionTestBar::__Empty__() const {
  return !data_;
}

int ConstReflectionTestBar::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"required_foo", 1},
    {"optional_foo", 2},
    {"repeated_foo", 3},
    {"map_foo", 4},
    {"oneof_foo", 5},
    {"another_oneof_foo", 6},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstReflectionTestBar::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstReflectionTestBar::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ReflectionTestFoo),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstReflectionTestFoo),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ReflectionTestFoo),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstReflectionTestFoo),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ReflectionTestFoo>)
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<int32_t, ::oneflow::cfg::ReflectionTestFoo>)
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ReflectionTestFoo),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstReflectionTestFoo),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ReflectionTestFoo),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstReflectionTestFoo),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstReflectionTestBar::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &required_foo();
    case 2: return &optional_foo();
    case 3: return &repeated_foo();
    case 4: return &map_foo();
    case 5: return &oneof_foo();
    case 6: return &another_oneof_foo();
    default: return nullptr;
  }
}

// required or optional field required_foo
bool ConstReflectionTestBar::has_required_foo() const {
  return __SharedPtrOrDefault__()->has_required_foo();
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::required_foo() const {
  return __SharedPtrOrDefault__()->required_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> ConstReflectionTestBar::shared_const_required_foo() const {
  return required_foo().__SharedConst__();
}
// required or optional field optional_foo
bool ConstReflectionTestBar::has_optional_foo() const {
  return __SharedPtrOrDefault__()->has_optional_foo();
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::optional_foo() const {
  return __SharedPtrOrDefault__()->optional_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> ConstReflectionTestBar::shared_const_optional_foo() const {
  return optional_foo().__SharedConst__();
}
// repeated field repeated_foo
::std::size_t ConstReflectionTestBar::repeated_foo_size() const {
  return __SharedPtrOrDefault__()->repeated_foo_size();
}
const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& ConstReflectionTestBar::repeated_foo() const {
  return __SharedPtrOrDefault__()->repeated_foo();
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::repeated_foo(::std::size_t index) const {
  return __SharedPtrOrDefault__()->repeated_foo(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> ConstReflectionTestBar::shared_const_repeated_foo() const {
  return repeated_foo().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> ConstReflectionTestBar::shared_const_repeated_foo(::std::size_t index) const {
  return repeated_foo(index).__SharedConst__();
}
// map field map_foo
::std::size_t ConstReflectionTestBar::map_foo_size() const {
  return __SharedPtrOrDefault__()->map_foo_size();
}

const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& ConstReflectionTestBar::map_foo() const {
  return __SharedPtrOrDefault__()->map_foo();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> ConstReflectionTestBar::shared_const_map_foo() const {
  return map_foo().__SharedConst__();
}
 // oneof field type: oneof_foo
bool ConstReflectionTestBar::has_oneof_foo() const {
  return __SharedPtrOrDefault__()->has_oneof_foo();
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::oneof_foo() const {
  return __SharedPtrOrDefault__()->oneof_foo();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> ConstReflectionTestBar::shared_const_oneof_foo() const {
  return oneof_foo().__SharedConst__();
}
 // oneof field type: another_oneof_foo
bool ConstReflectionTestBar::has_another_oneof_foo() const {
  return __SharedPtrOrDefault__()->has_another_oneof_foo();
}
const ::oneflow::cfg::ReflectionTestFoo& ConstReflectionTestBar::another_oneof_foo() const {
  return __SharedPtrOrDefault__()->another_oneof_foo();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> ConstReflectionTestBar::shared_const_another_oneof_foo() const {
  return another_oneof_foo().__SharedConst__();
}
ConstReflectionTestBar::TypeCase ConstReflectionTestBar::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstReflectionTestBar::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstReflectionTestBar> ConstReflectionTestBar::__SharedConst__() const {
  return ::std::make_shared<ConstReflectionTestBar>(data_);
}
int64_t ConstReflectionTestBar::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstReflectionTestBar::operator==(const ConstReflectionTestBar& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstReflectionTestBar::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstReflectionTestBar::operator<(const ConstReflectionTestBar& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ReflectionTestBar_>& ConstReflectionTestBar::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ReflectionTestBar_> default_ptr = std::make_shared<_ReflectionTestBar_>();
  return default_ptr;
}
const ::std::shared_ptr<_ReflectionTestBar_>& ConstReflectionTestBar::__SharedPtr__() {
  if (!data_) { data_.reset(new _ReflectionTestBar_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstReflectionTestBar
void ConstReflectionTestBar::BuildFromProto(const PbMessage& proto_reflectiontestbar) {
  data_ = ::std::make_shared<_ReflectionTestBar_>(dynamic_cast<const ::oneflow::ReflectionTestBar&>(proto_reflectiontestbar));
}

ReflectionTestBar::ReflectionTestBar(const ::std::shared_ptr<_ReflectionTestBar_>& data)
  : ConstReflectionTestBar(data) {}
ReflectionTestBar::ReflectionTestBar(const ReflectionTestBar& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ReflectionTestBar> resize
ReflectionTestBar::ReflectionTestBar(ReflectionTestBar&&) noexcept = default; 
ReflectionTestBar::ReflectionTestBar(const ::oneflow::ReflectionTestBar& proto_reflectiontestbar) {
  InitFromProto(proto_reflectiontestbar);
}
ReflectionTestBar::ReflectionTestBar() = default;

ReflectionTestBar::~ReflectionTestBar() = default;

void ReflectionTestBar::InitFromProto(const PbMessage& proto_reflectiontestbar) {
  BuildFromProto(proto_reflectiontestbar);
}
  
void* ReflectionTestBar::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_required_foo();
    case 2: return mutable_optional_foo();
    case 3: return mutable_repeated_foo();
    case 4: return mutable_map_foo();
    case 5: return mutable_oneof_foo();
    case 6: return mutable_another_oneof_foo();
    default: return nullptr;
  }
}

bool ReflectionTestBar::operator==(const ReflectionTestBar& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ReflectionTestBar::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ReflectionTestBar::operator<(const ReflectionTestBar& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ReflectionTestBar::Clear() {
  if (data_) { data_.reset(); }
}
void ReflectionTestBar::CopyFrom(const ReflectionTestBar& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ReflectionTestBar& ReflectionTestBar::operator=(const ReflectionTestBar& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field required_foo
void ReflectionTestBar::clear_required_foo() {
  return __SharedPtr__()->clear_required_foo();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::mutable_required_foo() {
  return __SharedPtr__()->mutable_required_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> ReflectionTestBar::shared_mutable_required_foo() {
  return mutable_required_foo()->__SharedMutable__();
}
// required or optional field optional_foo
void ReflectionTestBar::clear_optional_foo() {
  return __SharedPtr__()->clear_optional_foo();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::mutable_optional_foo() {
  return __SharedPtr__()->mutable_optional_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> ReflectionTestBar::shared_mutable_optional_foo() {
  return mutable_optional_foo()->__SharedMutable__();
}
// repeated field repeated_foo
void ReflectionTestBar::clear_repeated_foo() {
  return __SharedPtr__()->clear_repeated_foo();
}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_* ReflectionTestBar::mutable_repeated_foo() {
  return __SharedPtr__()->mutable_repeated_foo();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::mutable_repeated_foo(::std::size_t index) {
  return __SharedPtr__()->mutable_repeated_foo(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> ReflectionTestBar::shared_mutable_repeated_foo() {
  return mutable_repeated_foo()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> ReflectionTestBar::shared_mutable_repeated_foo(::std::size_t index) {
  return mutable_repeated_foo(index)->__SharedMutable__();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::add_repeated_foo() {
  return __SharedPtr__()->add_repeated_foo();
}
// repeated field map_foo
void ReflectionTestBar::clear_map_foo() {
  return __SharedPtr__()->clear_map_foo();
}

const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_ & ReflectionTestBar::map_foo() const {
  return __SharedConst__()->map_foo();
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_* ReflectionTestBar::mutable_map_foo() {
  return __SharedPtr__()->mutable_map_foo();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> ReflectionTestBar::shared_mutable_map_foo() {
  return mutable_map_foo()->__SharedMutable__();
}
void ReflectionTestBar::clear_oneof_foo() {
  return __SharedPtr__()->clear_oneof_foo();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::mutable_oneof_foo() {
  return __SharedPtr__()->mutable_oneof_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> ReflectionTestBar::shared_mutable_oneof_foo() {
  return mutable_oneof_foo()->__SharedMutable__();
}
void ReflectionTestBar::clear_another_oneof_foo() {
  return __SharedPtr__()->clear_another_oneof_foo();
}
::oneflow::cfg::ReflectionTestFoo* ReflectionTestBar::mutable_another_oneof_foo() {
  return __SharedPtr__()->mutable_another_oneof_foo();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> ReflectionTestBar::shared_mutable_another_oneof_foo() {
  return mutable_another_oneof_foo()->__SharedMutable__();
}

::std::shared_ptr<ReflectionTestBar> ReflectionTestBar::__SharedMutable__() {
  return ::std::make_shared<ReflectionTestBar>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): ::oneflow::cfg::_RepeatedField_<int32_t>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_(data) {}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_() = default;
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_() = default;

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_(const ::std::shared_ptr<::std::map<int32_t, int32_t>>& data): ::oneflow::cfg::_MapField_<int32_t, int32_t>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_() = default;

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<int32_t>();
  const auto& value_hash = std::hash<int32_t>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const int32_t& Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::Get(const int32_t& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>(__SharedPtr__());
}


_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_(const ::std::shared_ptr<::std::map<int32_t, int32_t>>& data): Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_(data) {}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_() = default;
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_() = default;

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) {
  ::oneflow::cfg::_MapField_<int32_t, int32_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) {
  ::oneflow::cfg::_MapField_<int32_t, int32_t>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<int32_t>();
  const auto& value_hash = std::hash<int32_t>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_>(__SharedPtr__());
}

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_int32_t_::Set(const int32_t& key, const int32_t& value) {
  (*this)[key] = value;
}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ReflectionTestFoo>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ReflectionTestFoo>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ReflectionTestFoo>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstReflectionTestFoo> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ReflectionTestFoo>>& data): Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_(data) {}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_() = default;
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_() = default;

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ReflectionTestFoo>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ReflectionTestFoo>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ReflectionTestFoo>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__RepeatedField_ReflectionTestFoo_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_(const ::std::shared_ptr<::std::map<int32_t, ::oneflow::cfg::ReflectionTestFoo>>& data): ::oneflow::cfg::_MapField_<int32_t, ::oneflow::cfg::ReflectionTestFoo>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::~Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_() = default;

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<int32_t>();
  const auto& value_hash = std::hash<::oneflow::cfg::ReflectionTestFoo>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::ReflectionTestFoo& Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::Get(const int32_t& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstReflectionTestFoo> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__SharedConst__(const int32_t& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ConstReflectionTestFoo> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ConstReflectionTestFoo> Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_(const ::std::shared_ptr<::std::map<int32_t, ::oneflow::cfg::ReflectionTestFoo>>& data): Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_(data) {}
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_() = default;
_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::~_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_() = default;

void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) {
  ::oneflow::cfg::_MapField_<int32_t, ::oneflow::cfg::ReflectionTestFoo>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) {
  ::oneflow::cfg::_MapField_<int32_t, ::oneflow::cfg::ReflectionTestFoo>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::operator==(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<int32_t>();
  const auto& value_hash = std::hash<::oneflow::cfg::ReflectionTestFoo>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::operator<(const _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::ReflectionTestFoo> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::__SharedMutable__(const int32_t& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ::oneflow::cfg::ReflectionTestFoo> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_, ::oneflow::cfg::ReflectionTestFoo> _CFG_ONEFLOW_CORE_COMMON_CFG_REFLECTION_TEST_CFG_H__MapField_int32_t_ReflectionTestFoo_::shared_mut_end() { return end(); }

}
} // namespace oneflow
