#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/common/device_type.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.common.device_type", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::DeviceType> enm(m, "DeviceType");
    enm.value("kInvalidDevice", ::oneflow::cfg::kInvalidDevice);
    enm.value("kCPU", ::oneflow::cfg::kCPU);
    enm.value("kGPU", ::oneflow::cfg::kGPU);
    m.attr("kInvalidDevice") = enm.attr("kInvalidDevice");
    m.attr("kCPU") = enm.attr("kCPU");
    m.attr("kGPU") = enm.attr("kGPU");
  }


}