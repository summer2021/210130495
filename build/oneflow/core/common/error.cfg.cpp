#include "oneflow/core/common/error.cfg.h"
#include "oneflow/core/common/error.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstFieldValue::_FieldValue_::_FieldValue_() { Clear(); }
ConstFieldValue::_FieldValue_::_FieldValue_(const _FieldValue_& other) { CopyFrom(other); }
ConstFieldValue::_FieldValue_::_FieldValue_(const ::oneflow::FieldValue& proto_fieldvalue) {
  InitFromProto(proto_fieldvalue);
}
ConstFieldValue::_FieldValue_::_FieldValue_(_FieldValue_&& other) = default;
ConstFieldValue::_FieldValue_::~_FieldValue_() = default;

void ConstFieldValue::_FieldValue_::InitFromProto(const ::oneflow::FieldValue& proto_fieldvalue) {
  Clear();
  // required_or_optional field: field
  if (proto_fieldvalue.has_field()) {
    set_field(proto_fieldvalue.field());
  }
  // required_or_optional field: value
  if (proto_fieldvalue.has_value()) {
    set_value(proto_fieldvalue.value());
  }
    
}

void ConstFieldValue::_FieldValue_::ToProto(::oneflow::FieldValue* proto_fieldvalue) const {
  proto_fieldvalue->Clear();
  // required_or_optional field: field
  if (this->has_field()) {
    proto_fieldvalue->set_field(field());
    }
  // required_or_optional field: value
  if (this->has_value()) {
    proto_fieldvalue->set_value(value());
    }

}

::std::string ConstFieldValue::_FieldValue_::DebugString() const {
  ::oneflow::FieldValue proto_fieldvalue;
  this->ToProto(&proto_fieldvalue);
  return proto_fieldvalue.DebugString();
}

void ConstFieldValue::_FieldValue_::Clear() {
  clear_field();
  clear_value();
}

void ConstFieldValue::_FieldValue_::CopyFrom(const _FieldValue_& other) {
  if (other.has_field()) {
    set_field(other.field());
  } else {
    clear_field();
  }
  if (other.has_value()) {
    set_value(other.value());
  } else {
    clear_value();
  }
}


// optional field field
bool ConstFieldValue::_FieldValue_::has_field() const {
  return has_field_;
}
const ::std::string& ConstFieldValue::_FieldValue_::field() const {
  if (has_field_) { return field_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstFieldValue::_FieldValue_::clear_field() {
  has_field_ = false;
}
void ConstFieldValue::_FieldValue_::set_field(const ::std::string& value) {
  field_ = value;
  has_field_ = true;
}
::std::string* ConstFieldValue::_FieldValue_::mutable_field() {
  has_field_ = true;
  return &field_;
}

// optional field value
bool ConstFieldValue::_FieldValue_::has_value() const {
  return has_value_;
}
const ::std::string& ConstFieldValue::_FieldValue_::value() const {
  if (has_value_) { return value_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstFieldValue::_FieldValue_::clear_value() {
  has_value_ = false;
}
void ConstFieldValue::_FieldValue_::set_value(const ::std::string& value) {
  value_ = value;
  has_value_ = true;
}
::std::string* ConstFieldValue::_FieldValue_::mutable_value() {
  has_value_ = true;
  return &value_;
}


int ConstFieldValue::_FieldValue_::compare(const _FieldValue_& other) {
  if (!(has_field() == other.has_field())) {
    return has_field() < other.has_field() ? -1 : 1;
  } else if (!(field() == other.field())) {
    return field() < other.field() ? -1 : 1;
  }
  if (!(has_value() == other.has_value())) {
    return has_value() < other.has_value() ? -1 : 1;
  } else if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstFieldValue::_FieldValue_::operator==(const _FieldValue_& other) const {
  return true
    && has_field() == other.has_field() 
    && field() == other.field()
    && has_value() == other.has_value() 
    && value() == other.value()
  ;
}

std::size_t ConstFieldValue::_FieldValue_::__CalcHash__() const {
  return 0
    ^ (has_field() ? std::hash<::std::string>()(field()) : 0)
    ^ (has_value() ? std::hash<::std::string>()(value()) : 0)
  ;
}

bool ConstFieldValue::_FieldValue_::operator<(const _FieldValue_& other) const {
  return false
    || !(has_field() == other.has_field()) ? 
      has_field() < other.has_field() : false
    || !(field() == other.field()) ? 
      field() < other.field() : false
    || !(has_value() == other.has_value()) ? 
      has_value() < other.has_value() : false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _FieldValue_ =  ConstFieldValue::_FieldValue_;
ConstFieldValue::ConstFieldValue(const ::std::shared_ptr<_FieldValue_>& data): data_(data) {}
ConstFieldValue::ConstFieldValue(): data_(::std::make_shared<_FieldValue_>()) {}
ConstFieldValue::ConstFieldValue(const ::oneflow::FieldValue& proto_fieldvalue) {
  BuildFromProto(proto_fieldvalue);
}
ConstFieldValue::ConstFieldValue(const ConstFieldValue&) = default;
ConstFieldValue::ConstFieldValue(ConstFieldValue&&) noexcept = default;
ConstFieldValue::~ConstFieldValue() = default;

void ConstFieldValue::ToProto(PbMessage* proto_fieldvalue) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::FieldValue*>(proto_fieldvalue));
}
  
::std::string ConstFieldValue::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstFieldValue::__Empty__() const {
  return !data_;
}

int ConstFieldValue::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"field", 1},
    {"value", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstFieldValue::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstFieldValue::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstFieldValue::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &field();
    case 2: return &value();
    default: return nullptr;
  }
}

// required or optional field field
bool ConstFieldValue::has_field() const {
  return __SharedPtrOrDefault__()->has_field();
}
const ::std::string& ConstFieldValue::field() const {
  return __SharedPtrOrDefault__()->field();
}
// used by pybind11 only
// required or optional field value
bool ConstFieldValue::has_value() const {
  return __SharedPtrOrDefault__()->has_value();
}
const ::std::string& ConstFieldValue::value() const {
  return __SharedPtrOrDefault__()->value();
}
// used by pybind11 only

::std::shared_ptr<ConstFieldValue> ConstFieldValue::__SharedConst__() const {
  return ::std::make_shared<ConstFieldValue>(data_);
}
int64_t ConstFieldValue::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstFieldValue::operator==(const ConstFieldValue& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstFieldValue::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstFieldValue::operator<(const ConstFieldValue& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_FieldValue_>& ConstFieldValue::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_FieldValue_> default_ptr = std::make_shared<_FieldValue_>();
  return default_ptr;
}
const ::std::shared_ptr<_FieldValue_>& ConstFieldValue::__SharedPtr__() {
  if (!data_) { data_.reset(new _FieldValue_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstFieldValue
void ConstFieldValue::BuildFromProto(const PbMessage& proto_fieldvalue) {
  data_ = ::std::make_shared<_FieldValue_>(dynamic_cast<const ::oneflow::FieldValue&>(proto_fieldvalue));
}

FieldValue::FieldValue(const ::std::shared_ptr<_FieldValue_>& data)
  : ConstFieldValue(data) {}
FieldValue::FieldValue(const FieldValue& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<FieldValue> resize
FieldValue::FieldValue(FieldValue&&) noexcept = default; 
FieldValue::FieldValue(const ::oneflow::FieldValue& proto_fieldvalue) {
  InitFromProto(proto_fieldvalue);
}
FieldValue::FieldValue() = default;

FieldValue::~FieldValue() = default;

void FieldValue::InitFromProto(const PbMessage& proto_fieldvalue) {
  BuildFromProto(proto_fieldvalue);
}
  
void* FieldValue::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_field();
    case 2: return mutable_value();
    default: return nullptr;
  }
}

bool FieldValue::operator==(const FieldValue& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t FieldValue::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool FieldValue::operator<(const FieldValue& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void FieldValue::Clear() {
  if (data_) { data_.reset(); }
}
void FieldValue::CopyFrom(const FieldValue& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
FieldValue& FieldValue::operator=(const FieldValue& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field field
void FieldValue::clear_field() {
  return __SharedPtr__()->clear_field();
}
void FieldValue::set_field(const ::std::string& value) {
  return __SharedPtr__()->set_field(value);
}
::std::string* FieldValue::mutable_field() {
  return  __SharedPtr__()->mutable_field();
}
// required or optional field value
void FieldValue::clear_value() {
  return __SharedPtr__()->clear_value();
}
void FieldValue::set_value(const ::std::string& value) {
  return __SharedPtr__()->set_value(value);
}
::std::string* FieldValue::mutable_value() {
  return  __SharedPtr__()->mutable_value();
}

::std::shared_ptr<FieldValue> FieldValue::__SharedMutable__() {
  return ::std::make_shared<FieldValue>(__SharedPtr__());
}
ConstOneFieldAssertError::_OneFieldAssertError_::_OneFieldAssertError_() { Clear(); }
ConstOneFieldAssertError::_OneFieldAssertError_::_OneFieldAssertError_(const _OneFieldAssertError_& other) { CopyFrom(other); }
ConstOneFieldAssertError::_OneFieldAssertError_::_OneFieldAssertError_(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror) {
  InitFromProto(proto_onefieldasserterror);
}
ConstOneFieldAssertError::_OneFieldAssertError_::_OneFieldAssertError_(_OneFieldAssertError_&& other) = default;
ConstOneFieldAssertError::_OneFieldAssertError_::~_OneFieldAssertError_() = default;

void ConstOneFieldAssertError::_OneFieldAssertError_::InitFromProto(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror) {
  Clear();
  // required_or_optional field: compare_type
  if (proto_onefieldasserterror.has_compare_type()) {
  set_compare_type(static_cast<std::remove_reference<std::remove_const<decltype(compare_type())>::type>::type>(proto_onefieldasserterror.compare_type()));
  }
  // required_or_optional field: left
  if (proto_onefieldasserterror.has_left()) {
  *mutable_left() = ::oneflow::cfg::FieldValue(proto_onefieldasserterror.left());      
  }
  // required_or_optional field: right_value
  if (proto_onefieldasserterror.has_right_value()) {
    set_right_value(proto_onefieldasserterror.right_value());
  }
    
}

void ConstOneFieldAssertError::_OneFieldAssertError_::ToProto(::oneflow::OneFieldAssertError* proto_onefieldasserterror) const {
  proto_onefieldasserterror->Clear();
  // required_or_optional field: compare_type
  if (this->has_compare_type()) {
    proto_onefieldasserterror->set_compare_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_onefieldasserterror->compare_type())>::type>::type>(compare_type()));
    }
  // required_or_optional field: left
  if (this->has_left()) {
    ::oneflow::FieldValue proto_left;
    left().ToProto(&proto_left);
    proto_onefieldasserterror->mutable_left()->CopyFrom(proto_left);
    }
  // required_or_optional field: right_value
  if (this->has_right_value()) {
    proto_onefieldasserterror->set_right_value(right_value());
    }

}

::std::string ConstOneFieldAssertError::_OneFieldAssertError_::DebugString() const {
  ::oneflow::OneFieldAssertError proto_onefieldasserterror;
  this->ToProto(&proto_onefieldasserterror);
  return proto_onefieldasserterror.DebugString();
}

void ConstOneFieldAssertError::_OneFieldAssertError_::Clear() {
  clear_compare_type();
  clear_left();
  clear_right_value();
}

void ConstOneFieldAssertError::_OneFieldAssertError_::CopyFrom(const _OneFieldAssertError_& other) {
  if (other.has_compare_type()) {
    set_compare_type(other.compare_type());
  } else {
    clear_compare_type();
  }
  if (other.has_left()) {
    mutable_left()->CopyFrom(other.left());
  } else {
    clear_left();
  }
  if (other.has_right_value()) {
    set_right_value(other.right_value());
  } else {
    clear_right_value();
  }
}


// optional field compare_type
bool ConstOneFieldAssertError::_OneFieldAssertError_::has_compare_type() const {
  return has_compare_type_;
}
const ::oneflow::cfg::OpcodeType& ConstOneFieldAssertError::_OneFieldAssertError_::compare_type() const {
  if (has_compare_type_) { return compare_type_; }
  static const ::oneflow::cfg::OpcodeType default_static_value = ::oneflow::cfg::OpcodeType();
  return default_static_value;
}
void ConstOneFieldAssertError::_OneFieldAssertError_::clear_compare_type() {
  has_compare_type_ = false;
}
void ConstOneFieldAssertError::_OneFieldAssertError_::set_compare_type(const ::oneflow::cfg::OpcodeType& value) {
  compare_type_ = value;
  has_compare_type_ = true;
}
::oneflow::cfg::OpcodeType* ConstOneFieldAssertError::_OneFieldAssertError_::mutable_compare_type() {
  has_compare_type_ = true;
  return &compare_type_;
}

// optional field left
bool ConstOneFieldAssertError::_OneFieldAssertError_::has_left() const {
  return has_left_;
}
const ::oneflow::cfg::FieldValue& ConstOneFieldAssertError::_OneFieldAssertError_::left() const {
  if (!left_) {
    static const ::std::shared_ptr<::oneflow::cfg::FieldValue> default_static_value =
      ::std::make_shared<::oneflow::cfg::FieldValue>();
    return *default_static_value;
  }
  return *(left_.get());
}
void ConstOneFieldAssertError::_OneFieldAssertError_::clear_left() {
  if (left_) {
    left_->Clear();
  }
  has_left_ = false;
}
::oneflow::cfg::FieldValue* ConstOneFieldAssertError::_OneFieldAssertError_::mutable_left() {
  if (!left_) {
    left_ = ::std::make_shared<::oneflow::cfg::FieldValue>();
  }
  has_left_ = true;
  return left_.get();
}

// optional field right_value
bool ConstOneFieldAssertError::_OneFieldAssertError_::has_right_value() const {
  return has_right_value_;
}
const ::std::string& ConstOneFieldAssertError::_OneFieldAssertError_::right_value() const {
  if (has_right_value_) { return right_value_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstOneFieldAssertError::_OneFieldAssertError_::clear_right_value() {
  has_right_value_ = false;
}
void ConstOneFieldAssertError::_OneFieldAssertError_::set_right_value(const ::std::string& value) {
  right_value_ = value;
  has_right_value_ = true;
}
::std::string* ConstOneFieldAssertError::_OneFieldAssertError_::mutable_right_value() {
  has_right_value_ = true;
  return &right_value_;
}


int ConstOneFieldAssertError::_OneFieldAssertError_::compare(const _OneFieldAssertError_& other) {
  if (!(has_compare_type() == other.has_compare_type())) {
    return has_compare_type() < other.has_compare_type() ? -1 : 1;
  } else if (!(compare_type() == other.compare_type())) {
    return compare_type() < other.compare_type() ? -1 : 1;
  }
  if (!(has_left() == other.has_left())) {
    return has_left() < other.has_left() ? -1 : 1;
  } else if (!(left() == other.left())) {
    return left() < other.left() ? -1 : 1;
  }
  if (!(has_right_value() == other.has_right_value())) {
    return has_right_value() < other.has_right_value() ? -1 : 1;
  } else if (!(right_value() == other.right_value())) {
    return right_value() < other.right_value() ? -1 : 1;
  }
  return 0;
}

bool ConstOneFieldAssertError::_OneFieldAssertError_::operator==(const _OneFieldAssertError_& other) const {
  return true
    && has_compare_type() == other.has_compare_type() 
    && compare_type() == other.compare_type()
    && has_left() == other.has_left() 
    && left() == other.left()
    && has_right_value() == other.has_right_value() 
    && right_value() == other.right_value()
  ;
}

std::size_t ConstOneFieldAssertError::_OneFieldAssertError_::__CalcHash__() const {
  return 0
    ^ (has_compare_type() ? std::hash<::oneflow::cfg::OpcodeType>()(compare_type()) : 0)
    ^ (has_left() ? std::hash<::oneflow::cfg::FieldValue>()(left()) : 0)
    ^ (has_right_value() ? std::hash<::std::string>()(right_value()) : 0)
  ;
}

bool ConstOneFieldAssertError::_OneFieldAssertError_::operator<(const _OneFieldAssertError_& other) const {
  return false
    || !(has_compare_type() == other.has_compare_type()) ? 
      has_compare_type() < other.has_compare_type() : false
    || !(compare_type() == other.compare_type()) ? 
      compare_type() < other.compare_type() : false
    || !(has_left() == other.has_left()) ? 
      has_left() < other.has_left() : false
    || !(left() == other.left()) ? 
      left() < other.left() : false
    || !(has_right_value() == other.has_right_value()) ? 
      has_right_value() < other.has_right_value() : false
    || !(right_value() == other.right_value()) ? 
      right_value() < other.right_value() : false
  ;
}

using _OneFieldAssertError_ =  ConstOneFieldAssertError::_OneFieldAssertError_;
ConstOneFieldAssertError::ConstOneFieldAssertError(const ::std::shared_ptr<_OneFieldAssertError_>& data): data_(data) {}
ConstOneFieldAssertError::ConstOneFieldAssertError(): data_(::std::make_shared<_OneFieldAssertError_>()) {}
ConstOneFieldAssertError::ConstOneFieldAssertError(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror) {
  BuildFromProto(proto_onefieldasserterror);
}
ConstOneFieldAssertError::ConstOneFieldAssertError(const ConstOneFieldAssertError&) = default;
ConstOneFieldAssertError::ConstOneFieldAssertError(ConstOneFieldAssertError&&) noexcept = default;
ConstOneFieldAssertError::~ConstOneFieldAssertError() = default;

void ConstOneFieldAssertError::ToProto(PbMessage* proto_onefieldasserterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OneFieldAssertError*>(proto_onefieldasserterror));
}
  
::std::string ConstOneFieldAssertError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOneFieldAssertError::__Empty__() const {
  return !data_;
}

int ConstOneFieldAssertError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"compare_type", 1},
    {"left", 2},
    {"right_value", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOneFieldAssertError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOneFieldAssertError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpcodeType),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldValue),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFieldValue),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOneFieldAssertError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &compare_type();
    case 2: return &left();
    case 3: return &right_value();
    default: return nullptr;
  }
}

// required or optional field compare_type
bool ConstOneFieldAssertError::has_compare_type() const {
  return __SharedPtrOrDefault__()->has_compare_type();
}
const ::oneflow::cfg::OpcodeType& ConstOneFieldAssertError::compare_type() const {
  return __SharedPtrOrDefault__()->compare_type();
}
// used by pybind11 only
// required or optional field left
bool ConstOneFieldAssertError::has_left() const {
  return __SharedPtrOrDefault__()->has_left();
}
const ::oneflow::cfg::FieldValue& ConstOneFieldAssertError::left() const {
  return __SharedPtrOrDefault__()->left();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFieldValue> ConstOneFieldAssertError::shared_const_left() const {
  return left().__SharedConst__();
}
// required or optional field right_value
bool ConstOneFieldAssertError::has_right_value() const {
  return __SharedPtrOrDefault__()->has_right_value();
}
const ::std::string& ConstOneFieldAssertError::right_value() const {
  return __SharedPtrOrDefault__()->right_value();
}
// used by pybind11 only

::std::shared_ptr<ConstOneFieldAssertError> ConstOneFieldAssertError::__SharedConst__() const {
  return ::std::make_shared<ConstOneFieldAssertError>(data_);
}
int64_t ConstOneFieldAssertError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOneFieldAssertError::operator==(const ConstOneFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOneFieldAssertError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOneFieldAssertError::operator<(const ConstOneFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OneFieldAssertError_>& ConstOneFieldAssertError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OneFieldAssertError_> default_ptr = std::make_shared<_OneFieldAssertError_>();
  return default_ptr;
}
const ::std::shared_ptr<_OneFieldAssertError_>& ConstOneFieldAssertError::__SharedPtr__() {
  if (!data_) { data_.reset(new _OneFieldAssertError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOneFieldAssertError
void ConstOneFieldAssertError::BuildFromProto(const PbMessage& proto_onefieldasserterror) {
  data_ = ::std::make_shared<_OneFieldAssertError_>(dynamic_cast<const ::oneflow::OneFieldAssertError&>(proto_onefieldasserterror));
}

OneFieldAssertError::OneFieldAssertError(const ::std::shared_ptr<_OneFieldAssertError_>& data)
  : ConstOneFieldAssertError(data) {}
OneFieldAssertError::OneFieldAssertError(const OneFieldAssertError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OneFieldAssertError> resize
OneFieldAssertError::OneFieldAssertError(OneFieldAssertError&&) noexcept = default; 
OneFieldAssertError::OneFieldAssertError(const ::oneflow::OneFieldAssertError& proto_onefieldasserterror) {
  InitFromProto(proto_onefieldasserterror);
}
OneFieldAssertError::OneFieldAssertError() = default;

OneFieldAssertError::~OneFieldAssertError() = default;

void OneFieldAssertError::InitFromProto(const PbMessage& proto_onefieldasserterror) {
  BuildFromProto(proto_onefieldasserterror);
}
  
void* OneFieldAssertError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_compare_type();
    case 2: return mutable_left();
    case 3: return mutable_right_value();
    default: return nullptr;
  }
}

bool OneFieldAssertError::operator==(const OneFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OneFieldAssertError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OneFieldAssertError::operator<(const OneFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OneFieldAssertError::Clear() {
  if (data_) { data_.reset(); }
}
void OneFieldAssertError::CopyFrom(const OneFieldAssertError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OneFieldAssertError& OneFieldAssertError::operator=(const OneFieldAssertError& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field compare_type
void OneFieldAssertError::clear_compare_type() {
  return __SharedPtr__()->clear_compare_type();
}
void OneFieldAssertError::set_compare_type(const ::oneflow::cfg::OpcodeType& value) {
  return __SharedPtr__()->set_compare_type(value);
}
::oneflow::cfg::OpcodeType* OneFieldAssertError::mutable_compare_type() {
  return  __SharedPtr__()->mutable_compare_type();
}
// required or optional field left
void OneFieldAssertError::clear_left() {
  return __SharedPtr__()->clear_left();
}
::oneflow::cfg::FieldValue* OneFieldAssertError::mutable_left() {
  return __SharedPtr__()->mutable_left();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FieldValue> OneFieldAssertError::shared_mutable_left() {
  return mutable_left()->__SharedMutable__();
}
// required or optional field right_value
void OneFieldAssertError::clear_right_value() {
  return __SharedPtr__()->clear_right_value();
}
void OneFieldAssertError::set_right_value(const ::std::string& value) {
  return __SharedPtr__()->set_right_value(value);
}
::std::string* OneFieldAssertError::mutable_right_value() {
  return  __SharedPtr__()->mutable_right_value();
}

::std::shared_ptr<OneFieldAssertError> OneFieldAssertError::__SharedMutable__() {
  return ::std::make_shared<OneFieldAssertError>(__SharedPtr__());
}
ConstTwoFieldAssertError::_TwoFieldAssertError_::_TwoFieldAssertError_() { Clear(); }
ConstTwoFieldAssertError::_TwoFieldAssertError_::_TwoFieldAssertError_(const _TwoFieldAssertError_& other) { CopyFrom(other); }
ConstTwoFieldAssertError::_TwoFieldAssertError_::_TwoFieldAssertError_(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror) {
  InitFromProto(proto_twofieldasserterror);
}
ConstTwoFieldAssertError::_TwoFieldAssertError_::_TwoFieldAssertError_(_TwoFieldAssertError_&& other) = default;
ConstTwoFieldAssertError::_TwoFieldAssertError_::~_TwoFieldAssertError_() = default;

void ConstTwoFieldAssertError::_TwoFieldAssertError_::InitFromProto(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror) {
  Clear();
  // required_or_optional field: compare_type
  if (proto_twofieldasserterror.has_compare_type()) {
  set_compare_type(static_cast<std::remove_reference<std::remove_const<decltype(compare_type())>::type>::type>(proto_twofieldasserterror.compare_type()));
  }
  // required_or_optional field: left
  if (proto_twofieldasserterror.has_left()) {
  *mutable_left() = ::oneflow::cfg::FieldValue(proto_twofieldasserterror.left());      
  }
  // required_or_optional field: right
  if (proto_twofieldasserterror.has_right()) {
  *mutable_right() = ::oneflow::cfg::FieldValue(proto_twofieldasserterror.right());      
  }
    
}

void ConstTwoFieldAssertError::_TwoFieldAssertError_::ToProto(::oneflow::TwoFieldAssertError* proto_twofieldasserterror) const {
  proto_twofieldasserterror->Clear();
  // required_or_optional field: compare_type
  if (this->has_compare_type()) {
    proto_twofieldasserterror->set_compare_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_twofieldasserterror->compare_type())>::type>::type>(compare_type()));
    }
  // required_or_optional field: left
  if (this->has_left()) {
    ::oneflow::FieldValue proto_left;
    left().ToProto(&proto_left);
    proto_twofieldasserterror->mutable_left()->CopyFrom(proto_left);
    }
  // required_or_optional field: right
  if (this->has_right()) {
    ::oneflow::FieldValue proto_right;
    right().ToProto(&proto_right);
    proto_twofieldasserterror->mutable_right()->CopyFrom(proto_right);
    }

}

::std::string ConstTwoFieldAssertError::_TwoFieldAssertError_::DebugString() const {
  ::oneflow::TwoFieldAssertError proto_twofieldasserterror;
  this->ToProto(&proto_twofieldasserterror);
  return proto_twofieldasserterror.DebugString();
}

void ConstTwoFieldAssertError::_TwoFieldAssertError_::Clear() {
  clear_compare_type();
  clear_left();
  clear_right();
}

void ConstTwoFieldAssertError::_TwoFieldAssertError_::CopyFrom(const _TwoFieldAssertError_& other) {
  if (other.has_compare_type()) {
    set_compare_type(other.compare_type());
  } else {
    clear_compare_type();
  }
  if (other.has_left()) {
    mutable_left()->CopyFrom(other.left());
  } else {
    clear_left();
  }
  if (other.has_right()) {
    mutable_right()->CopyFrom(other.right());
  } else {
    clear_right();
  }
}


// optional field compare_type
bool ConstTwoFieldAssertError::_TwoFieldAssertError_::has_compare_type() const {
  return has_compare_type_;
}
const ::oneflow::cfg::OpcodeType& ConstTwoFieldAssertError::_TwoFieldAssertError_::compare_type() const {
  if (has_compare_type_) { return compare_type_; }
  static const ::oneflow::cfg::OpcodeType default_static_value = ::oneflow::cfg::OpcodeType();
  return default_static_value;
}
void ConstTwoFieldAssertError::_TwoFieldAssertError_::clear_compare_type() {
  has_compare_type_ = false;
}
void ConstTwoFieldAssertError::_TwoFieldAssertError_::set_compare_type(const ::oneflow::cfg::OpcodeType& value) {
  compare_type_ = value;
  has_compare_type_ = true;
}
::oneflow::cfg::OpcodeType* ConstTwoFieldAssertError::_TwoFieldAssertError_::mutable_compare_type() {
  has_compare_type_ = true;
  return &compare_type_;
}

// optional field left
bool ConstTwoFieldAssertError::_TwoFieldAssertError_::has_left() const {
  return has_left_;
}
const ::oneflow::cfg::FieldValue& ConstTwoFieldAssertError::_TwoFieldAssertError_::left() const {
  if (!left_) {
    static const ::std::shared_ptr<::oneflow::cfg::FieldValue> default_static_value =
      ::std::make_shared<::oneflow::cfg::FieldValue>();
    return *default_static_value;
  }
  return *(left_.get());
}
void ConstTwoFieldAssertError::_TwoFieldAssertError_::clear_left() {
  if (left_) {
    left_->Clear();
  }
  has_left_ = false;
}
::oneflow::cfg::FieldValue* ConstTwoFieldAssertError::_TwoFieldAssertError_::mutable_left() {
  if (!left_) {
    left_ = ::std::make_shared<::oneflow::cfg::FieldValue>();
  }
  has_left_ = true;
  return left_.get();
}

// optional field right
bool ConstTwoFieldAssertError::_TwoFieldAssertError_::has_right() const {
  return has_right_;
}
const ::oneflow::cfg::FieldValue& ConstTwoFieldAssertError::_TwoFieldAssertError_::right() const {
  if (!right_) {
    static const ::std::shared_ptr<::oneflow::cfg::FieldValue> default_static_value =
      ::std::make_shared<::oneflow::cfg::FieldValue>();
    return *default_static_value;
  }
  return *(right_.get());
}
void ConstTwoFieldAssertError::_TwoFieldAssertError_::clear_right() {
  if (right_) {
    right_->Clear();
  }
  has_right_ = false;
}
::oneflow::cfg::FieldValue* ConstTwoFieldAssertError::_TwoFieldAssertError_::mutable_right() {
  if (!right_) {
    right_ = ::std::make_shared<::oneflow::cfg::FieldValue>();
  }
  has_right_ = true;
  return right_.get();
}


int ConstTwoFieldAssertError::_TwoFieldAssertError_::compare(const _TwoFieldAssertError_& other) {
  if (!(has_compare_type() == other.has_compare_type())) {
    return has_compare_type() < other.has_compare_type() ? -1 : 1;
  } else if (!(compare_type() == other.compare_type())) {
    return compare_type() < other.compare_type() ? -1 : 1;
  }
  if (!(has_left() == other.has_left())) {
    return has_left() < other.has_left() ? -1 : 1;
  } else if (!(left() == other.left())) {
    return left() < other.left() ? -1 : 1;
  }
  if (!(has_right() == other.has_right())) {
    return has_right() < other.has_right() ? -1 : 1;
  } else if (!(right() == other.right())) {
    return right() < other.right() ? -1 : 1;
  }
  return 0;
}

bool ConstTwoFieldAssertError::_TwoFieldAssertError_::operator==(const _TwoFieldAssertError_& other) const {
  return true
    && has_compare_type() == other.has_compare_type() 
    && compare_type() == other.compare_type()
    && has_left() == other.has_left() 
    && left() == other.left()
    && has_right() == other.has_right() 
    && right() == other.right()
  ;
}

std::size_t ConstTwoFieldAssertError::_TwoFieldAssertError_::__CalcHash__() const {
  return 0
    ^ (has_compare_type() ? std::hash<::oneflow::cfg::OpcodeType>()(compare_type()) : 0)
    ^ (has_left() ? std::hash<::oneflow::cfg::FieldValue>()(left()) : 0)
    ^ (has_right() ? std::hash<::oneflow::cfg::FieldValue>()(right()) : 0)
  ;
}

bool ConstTwoFieldAssertError::_TwoFieldAssertError_::operator<(const _TwoFieldAssertError_& other) const {
  return false
    || !(has_compare_type() == other.has_compare_type()) ? 
      has_compare_type() < other.has_compare_type() : false
    || !(compare_type() == other.compare_type()) ? 
      compare_type() < other.compare_type() : false
    || !(has_left() == other.has_left()) ? 
      has_left() < other.has_left() : false
    || !(left() == other.left()) ? 
      left() < other.left() : false
    || !(has_right() == other.has_right()) ? 
      has_right() < other.has_right() : false
    || !(right() == other.right()) ? 
      right() < other.right() : false
  ;
}

using _TwoFieldAssertError_ =  ConstTwoFieldAssertError::_TwoFieldAssertError_;
ConstTwoFieldAssertError::ConstTwoFieldAssertError(const ::std::shared_ptr<_TwoFieldAssertError_>& data): data_(data) {}
ConstTwoFieldAssertError::ConstTwoFieldAssertError(): data_(::std::make_shared<_TwoFieldAssertError_>()) {}
ConstTwoFieldAssertError::ConstTwoFieldAssertError(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror) {
  BuildFromProto(proto_twofieldasserterror);
}
ConstTwoFieldAssertError::ConstTwoFieldAssertError(const ConstTwoFieldAssertError&) = default;
ConstTwoFieldAssertError::ConstTwoFieldAssertError(ConstTwoFieldAssertError&&) noexcept = default;
ConstTwoFieldAssertError::~ConstTwoFieldAssertError() = default;

void ConstTwoFieldAssertError::ToProto(PbMessage* proto_twofieldasserterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TwoFieldAssertError*>(proto_twofieldasserterror));
}
  
::std::string ConstTwoFieldAssertError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTwoFieldAssertError::__Empty__() const {
  return !data_;
}

int ConstTwoFieldAssertError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"compare_type", 1},
    {"left", 2},
    {"right", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstTwoFieldAssertError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstTwoFieldAssertError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpcodeType),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldValue),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFieldValue),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldValue),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFieldValue),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTwoFieldAssertError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &compare_type();
    case 2: return &left();
    case 3: return &right();
    default: return nullptr;
  }
}

// required or optional field compare_type
bool ConstTwoFieldAssertError::has_compare_type() const {
  return __SharedPtrOrDefault__()->has_compare_type();
}
const ::oneflow::cfg::OpcodeType& ConstTwoFieldAssertError::compare_type() const {
  return __SharedPtrOrDefault__()->compare_type();
}
// used by pybind11 only
// required or optional field left
bool ConstTwoFieldAssertError::has_left() const {
  return __SharedPtrOrDefault__()->has_left();
}
const ::oneflow::cfg::FieldValue& ConstTwoFieldAssertError::left() const {
  return __SharedPtrOrDefault__()->left();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFieldValue> ConstTwoFieldAssertError::shared_const_left() const {
  return left().__SharedConst__();
}
// required or optional field right
bool ConstTwoFieldAssertError::has_right() const {
  return __SharedPtrOrDefault__()->has_right();
}
const ::oneflow::cfg::FieldValue& ConstTwoFieldAssertError::right() const {
  return __SharedPtrOrDefault__()->right();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFieldValue> ConstTwoFieldAssertError::shared_const_right() const {
  return right().__SharedConst__();
}

::std::shared_ptr<ConstTwoFieldAssertError> ConstTwoFieldAssertError::__SharedConst__() const {
  return ::std::make_shared<ConstTwoFieldAssertError>(data_);
}
int64_t ConstTwoFieldAssertError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTwoFieldAssertError::operator==(const ConstTwoFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTwoFieldAssertError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTwoFieldAssertError::operator<(const ConstTwoFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TwoFieldAssertError_>& ConstTwoFieldAssertError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TwoFieldAssertError_> default_ptr = std::make_shared<_TwoFieldAssertError_>();
  return default_ptr;
}
const ::std::shared_ptr<_TwoFieldAssertError_>& ConstTwoFieldAssertError::__SharedPtr__() {
  if (!data_) { data_.reset(new _TwoFieldAssertError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTwoFieldAssertError
void ConstTwoFieldAssertError::BuildFromProto(const PbMessage& proto_twofieldasserterror) {
  data_ = ::std::make_shared<_TwoFieldAssertError_>(dynamic_cast<const ::oneflow::TwoFieldAssertError&>(proto_twofieldasserterror));
}

TwoFieldAssertError::TwoFieldAssertError(const ::std::shared_ptr<_TwoFieldAssertError_>& data)
  : ConstTwoFieldAssertError(data) {}
TwoFieldAssertError::TwoFieldAssertError(const TwoFieldAssertError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TwoFieldAssertError> resize
TwoFieldAssertError::TwoFieldAssertError(TwoFieldAssertError&&) noexcept = default; 
TwoFieldAssertError::TwoFieldAssertError(const ::oneflow::TwoFieldAssertError& proto_twofieldasserterror) {
  InitFromProto(proto_twofieldasserterror);
}
TwoFieldAssertError::TwoFieldAssertError() = default;

TwoFieldAssertError::~TwoFieldAssertError() = default;

void TwoFieldAssertError::InitFromProto(const PbMessage& proto_twofieldasserterror) {
  BuildFromProto(proto_twofieldasserterror);
}
  
void* TwoFieldAssertError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_compare_type();
    case 2: return mutable_left();
    case 3: return mutable_right();
    default: return nullptr;
  }
}

bool TwoFieldAssertError::operator==(const TwoFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TwoFieldAssertError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TwoFieldAssertError::operator<(const TwoFieldAssertError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TwoFieldAssertError::Clear() {
  if (data_) { data_.reset(); }
}
void TwoFieldAssertError::CopyFrom(const TwoFieldAssertError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TwoFieldAssertError& TwoFieldAssertError::operator=(const TwoFieldAssertError& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field compare_type
void TwoFieldAssertError::clear_compare_type() {
  return __SharedPtr__()->clear_compare_type();
}
void TwoFieldAssertError::set_compare_type(const ::oneflow::cfg::OpcodeType& value) {
  return __SharedPtr__()->set_compare_type(value);
}
::oneflow::cfg::OpcodeType* TwoFieldAssertError::mutable_compare_type() {
  return  __SharedPtr__()->mutable_compare_type();
}
// required or optional field left
void TwoFieldAssertError::clear_left() {
  return __SharedPtr__()->clear_left();
}
::oneflow::cfg::FieldValue* TwoFieldAssertError::mutable_left() {
  return __SharedPtr__()->mutable_left();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FieldValue> TwoFieldAssertError::shared_mutable_left() {
  return mutable_left()->__SharedMutable__();
}
// required or optional field right
void TwoFieldAssertError::clear_right() {
  return __SharedPtr__()->clear_right();
}
::oneflow::cfg::FieldValue* TwoFieldAssertError::mutable_right() {
  return __SharedPtr__()->mutable_right();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FieldValue> TwoFieldAssertError::shared_mutable_right() {
  return mutable_right()->__SharedMutable__();
}

::std::shared_ptr<TwoFieldAssertError> TwoFieldAssertError::__SharedMutable__() {
  return ::std::make_shared<TwoFieldAssertError>(__SharedPtr__());
}
ConstConfigAssertFailedError::_ConfigAssertFailedError_::_ConfigAssertFailedError_() { Clear(); }
ConstConfigAssertFailedError::_ConfigAssertFailedError_::_ConfigAssertFailedError_(const _ConfigAssertFailedError_& other) { CopyFrom(other); }
ConstConfigAssertFailedError::_ConfigAssertFailedError_::_ConfigAssertFailedError_(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror) {
  InitFromProto(proto_configassertfailederror);
}
ConstConfigAssertFailedError::_ConfigAssertFailedError_::_ConfigAssertFailedError_(_ConfigAssertFailedError_&& other) = default;
ConstConfigAssertFailedError::_ConfigAssertFailedError_::~_ConfigAssertFailedError_() = default;

void ConstConfigAssertFailedError::_ConfigAssertFailedError_::InitFromProto(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror) {
  Clear();
  // oneof field: oprand_type
  OprandTypeCase oprand_type_case = OprandTypeCase(int(proto_configassertfailederror.oprand_type_case()));
  switch (oprand_type_case) {
    case kOneFieldAssertError: {
      *mutable_one_field_assert_error() = ::oneflow::cfg::OneFieldAssertError(proto_configassertfailederror.one_field_assert_error());
      break;
  }
    case kTwoFieldAssertError: {
      *mutable_two_field_assert_error() = ::oneflow::cfg::TwoFieldAssertError(proto_configassertfailederror.two_field_assert_error());
      break;
  }
    case OPRAND_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstConfigAssertFailedError::_ConfigAssertFailedError_::ToProto(::oneflow::ConfigAssertFailedError* proto_configassertfailederror) const {
  proto_configassertfailederror->Clear();

  // oneof field: oprand_type
  ::oneflow::ConfigAssertFailedError::OprandTypeCase oprand_type_case = ::oneflow::ConfigAssertFailedError::OprandTypeCase(int(this->oprand_type_case()));
  switch (oprand_type_case) {
    case ::oneflow::ConfigAssertFailedError::kOneFieldAssertError: {
      ::oneflow::OneFieldAssertError of_proto_one_field_assert_error;
      one_field_assert_error().ToProto(&of_proto_one_field_assert_error);
      proto_configassertfailederror->mutable_one_field_assert_error()->CopyFrom(of_proto_one_field_assert_error);
      break;
    }
    case ::oneflow::ConfigAssertFailedError::kTwoFieldAssertError: {
      ::oneflow::TwoFieldAssertError of_proto_two_field_assert_error;
      two_field_assert_error().ToProto(&of_proto_two_field_assert_error);
      proto_configassertfailederror->mutable_two_field_assert_error()->CopyFrom(of_proto_two_field_assert_error);
      break;
    }
    case ::oneflow::ConfigAssertFailedError::OPRAND_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstConfigAssertFailedError::_ConfigAssertFailedError_::DebugString() const {
  ::oneflow::ConfigAssertFailedError proto_configassertfailederror;
  this->ToProto(&proto_configassertfailederror);
  return proto_configassertfailederror.DebugString();
}

void ConstConfigAssertFailedError::_ConfigAssertFailedError_::Clear() {
  clear_oprand_type();
}

void ConstConfigAssertFailedError::_ConfigAssertFailedError_::CopyFrom(const _ConfigAssertFailedError_& other) {
  oprand_type_copy_from(other);
}


// oneof field oprand_type: one_field_assert_error
bool ConstConfigAssertFailedError::_ConfigAssertFailedError_::has_one_field_assert_error() const {
  return oprand_type_case() == kOneFieldAssertError;
}
void ConstConfigAssertFailedError::_ConfigAssertFailedError_::clear_one_field_assert_error() {
  if (has_one_field_assert_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(oprand_type_.one_field_assert_error_));
      ptr->~Shared_ptr();
    }
    oprand_type_case_ = OPRAND_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OneFieldAssertError& ConstConfigAssertFailedError::_ConfigAssertFailedError_::one_field_assert_error() const {
  if (has_one_field_assert_error()) {
      const ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>*>(&(oprand_type_.one_field_assert_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError> default_static_value = ::std::make_shared<::oneflow::cfg::OneFieldAssertError>();
    return *default_static_value;
    }
}
::oneflow::cfg::OneFieldAssertError* ConstConfigAssertFailedError::_ConfigAssertFailedError_::mutable_one_field_assert_error() {
  if(!has_one_field_assert_error()) {
    clear_oprand_type();
    new (&(oprand_type_.one_field_assert_error_)) ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>(new ::oneflow::cfg::OneFieldAssertError());
  }
  oprand_type_case_ = kOneFieldAssertError;
  ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>*>(&(oprand_type_.one_field_assert_error_));
  return  (*ptr).get();
}

// oneof field oprand_type: two_field_assert_error
bool ConstConfigAssertFailedError::_ConfigAssertFailedError_::has_two_field_assert_error() const {
  return oprand_type_case() == kTwoFieldAssertError;
}
void ConstConfigAssertFailedError::_ConfigAssertFailedError_::clear_two_field_assert_error() {
  if (has_two_field_assert_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(oprand_type_.two_field_assert_error_));
      ptr->~Shared_ptr();
    }
    oprand_type_case_ = OPRAND_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TwoFieldAssertError& ConstConfigAssertFailedError::_ConfigAssertFailedError_::two_field_assert_error() const {
  if (has_two_field_assert_error()) {
      const ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>*>(&(oprand_type_.two_field_assert_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError> default_static_value = ::std::make_shared<::oneflow::cfg::TwoFieldAssertError>();
    return *default_static_value;
    }
}
::oneflow::cfg::TwoFieldAssertError* ConstConfigAssertFailedError::_ConfigAssertFailedError_::mutable_two_field_assert_error() {
  if(!has_two_field_assert_error()) {
    clear_oprand_type();
    new (&(oprand_type_.two_field_assert_error_)) ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>(new ::oneflow::cfg::TwoFieldAssertError());
  }
  oprand_type_case_ = kTwoFieldAssertError;
  ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>*>(&(oprand_type_.two_field_assert_error_));
  return  (*ptr).get();
}
ConstConfigAssertFailedError::OprandTypeCase ConstConfigAssertFailedError::_ConfigAssertFailedError_::oprand_type_case() const {
  return oprand_type_case_;
}
bool ConstConfigAssertFailedError::_ConfigAssertFailedError_::has_oprand_type() const {
  return oprand_type_case_ != OPRAND_TYPE_NOT_SET;
}
void ConstConfigAssertFailedError::_ConfigAssertFailedError_::clear_oprand_type() {
  switch (oprand_type_case()) {
    case kOneFieldAssertError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OneFieldAssertError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(oprand_type_.one_field_assert_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kTwoFieldAssertError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(oprand_type_.two_field_assert_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case OPRAND_TYPE_NOT_SET: {
      break;
    }
  }
  oprand_type_case_ = OPRAND_TYPE_NOT_SET;
}
void ConstConfigAssertFailedError::_ConfigAssertFailedError_::oprand_type_copy_from(const _ConfigAssertFailedError_& other) {
  switch (other.oprand_type_case()) {
    case kOneFieldAssertError: {
      mutable_one_field_assert_error()->CopyFrom(other.one_field_assert_error());
      break;
    }
    case kTwoFieldAssertError: {
      mutable_two_field_assert_error()->CopyFrom(other.two_field_assert_error());
      break;
    }
    case OPRAND_TYPE_NOT_SET: {
      clear_oprand_type();
    }
  }
}


int ConstConfigAssertFailedError::_ConfigAssertFailedError_::compare(const _ConfigAssertFailedError_& other) {
  if (!(oprand_type_case() == other.oprand_type_case())) {
    return oprand_type_case() < other.oprand_type_case() ? -1 : 1;
  }
  switch (oprand_type_case()) {
    case kOneFieldAssertError: {
      if (!(one_field_assert_error() == other.one_field_assert_error())) {
        return one_field_assert_error() < other.one_field_assert_error() ? -1 : 1;
      }
      break;
    }
    case kTwoFieldAssertError: {
      if (!(two_field_assert_error() == other.two_field_assert_error())) {
        return two_field_assert_error() < other.two_field_assert_error() ? -1 : 1;
      }
      break;
    }
    case OPRAND_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstConfigAssertFailedError::_ConfigAssertFailedError_::operator==(const _ConfigAssertFailedError_& other) const {
  return true
    && oprand_type_case() == other.oprand_type_case()
    && (oprand_type_case() == kOneFieldAssertError ? 
      one_field_assert_error() == other.one_field_assert_error() : true)
    && (oprand_type_case() == kTwoFieldAssertError ? 
      two_field_assert_error() == other.two_field_assert_error() : true)
  ;
}

std::size_t ConstConfigAssertFailedError::_ConfigAssertFailedError_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(oprand_type_case())
    ^ (has_one_field_assert_error() ? std::hash<::oneflow::cfg::OneFieldAssertError>()(one_field_assert_error()) : 0)
    ^ (has_two_field_assert_error() ? std::hash<::oneflow::cfg::TwoFieldAssertError>()(two_field_assert_error()) : 0)
  ;
}

bool ConstConfigAssertFailedError::_ConfigAssertFailedError_::operator<(const _ConfigAssertFailedError_& other) const {
  return false
    || !(oprand_type_case() == other.oprand_type_case()) ? 
      oprand_type_case() < other.oprand_type_case() : false
    || ((oprand_type_case() == kOneFieldAssertError) && 
      !(one_field_assert_error() == other.one_field_assert_error())) ? 
        one_field_assert_error() < other.one_field_assert_error() : false
    || ((oprand_type_case() == kTwoFieldAssertError) && 
      !(two_field_assert_error() == other.two_field_assert_error())) ? 
        two_field_assert_error() < other.two_field_assert_error() : false
  ;
}

using _ConfigAssertFailedError_ =  ConstConfigAssertFailedError::_ConfigAssertFailedError_;
ConstConfigAssertFailedError::ConstConfigAssertFailedError(const ::std::shared_ptr<_ConfigAssertFailedError_>& data): data_(data) {}
ConstConfigAssertFailedError::ConstConfigAssertFailedError(): data_(::std::make_shared<_ConfigAssertFailedError_>()) {}
ConstConfigAssertFailedError::ConstConfigAssertFailedError(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror) {
  BuildFromProto(proto_configassertfailederror);
}
ConstConfigAssertFailedError::ConstConfigAssertFailedError(const ConstConfigAssertFailedError&) = default;
ConstConfigAssertFailedError::ConstConfigAssertFailedError(ConstConfigAssertFailedError&&) noexcept = default;
ConstConfigAssertFailedError::~ConstConfigAssertFailedError() = default;

void ConstConfigAssertFailedError::ToProto(PbMessage* proto_configassertfailederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ConfigAssertFailedError*>(proto_configassertfailederror));
}
  
::std::string ConstConfigAssertFailedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstConfigAssertFailedError::__Empty__() const {
  return !data_;
}

int ConstConfigAssertFailedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"one_field_assert_error", 1},
    {"two_field_assert_error", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstConfigAssertFailedError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstConfigAssertFailedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OneFieldAssertError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOneFieldAssertError),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TwoFieldAssertError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTwoFieldAssertError),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstConfigAssertFailedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &one_field_assert_error();
    case 2: return &two_field_assert_error();
    default: return nullptr;
  }
}

 // oneof field oprand_type: one_field_assert_error
bool ConstConfigAssertFailedError::has_one_field_assert_error() const {
  return __SharedPtrOrDefault__()->has_one_field_assert_error();
}
const ::oneflow::cfg::OneFieldAssertError& ConstConfigAssertFailedError::one_field_assert_error() const {
  return __SharedPtrOrDefault__()->one_field_assert_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOneFieldAssertError> ConstConfigAssertFailedError::shared_const_one_field_assert_error() const {
  return one_field_assert_error().__SharedConst__();
}
 // oneof field oprand_type: two_field_assert_error
bool ConstConfigAssertFailedError::has_two_field_assert_error() const {
  return __SharedPtrOrDefault__()->has_two_field_assert_error();
}
const ::oneflow::cfg::TwoFieldAssertError& ConstConfigAssertFailedError::two_field_assert_error() const {
  return __SharedPtrOrDefault__()->two_field_assert_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTwoFieldAssertError> ConstConfigAssertFailedError::shared_const_two_field_assert_error() const {
  return two_field_assert_error().__SharedConst__();
}
ConstConfigAssertFailedError::OprandTypeCase ConstConfigAssertFailedError::oprand_type_case() const {
  return __SharedPtrOrDefault__()->oprand_type_case();
}

bool ConstConfigAssertFailedError::has_oprand_type() const {
  return __SharedPtrOrDefault__()->has_oprand_type();
}

::std::shared_ptr<ConstConfigAssertFailedError> ConstConfigAssertFailedError::__SharedConst__() const {
  return ::std::make_shared<ConstConfigAssertFailedError>(data_);
}
int64_t ConstConfigAssertFailedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstConfigAssertFailedError::operator==(const ConstConfigAssertFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstConfigAssertFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstConfigAssertFailedError::operator<(const ConstConfigAssertFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ConfigAssertFailedError_>& ConstConfigAssertFailedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ConfigAssertFailedError_> default_ptr = std::make_shared<_ConfigAssertFailedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_ConfigAssertFailedError_>& ConstConfigAssertFailedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _ConfigAssertFailedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstConfigAssertFailedError
void ConstConfigAssertFailedError::BuildFromProto(const PbMessage& proto_configassertfailederror) {
  data_ = ::std::make_shared<_ConfigAssertFailedError_>(dynamic_cast<const ::oneflow::ConfigAssertFailedError&>(proto_configassertfailederror));
}

ConfigAssertFailedError::ConfigAssertFailedError(const ::std::shared_ptr<_ConfigAssertFailedError_>& data)
  : ConstConfigAssertFailedError(data) {}
ConfigAssertFailedError::ConfigAssertFailedError(const ConfigAssertFailedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ConfigAssertFailedError> resize
ConfigAssertFailedError::ConfigAssertFailedError(ConfigAssertFailedError&&) noexcept = default; 
ConfigAssertFailedError::ConfigAssertFailedError(const ::oneflow::ConfigAssertFailedError& proto_configassertfailederror) {
  InitFromProto(proto_configassertfailederror);
}
ConfigAssertFailedError::ConfigAssertFailedError() = default;

ConfigAssertFailedError::~ConfigAssertFailedError() = default;

void ConfigAssertFailedError::InitFromProto(const PbMessage& proto_configassertfailederror) {
  BuildFromProto(proto_configassertfailederror);
}
  
void* ConfigAssertFailedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_one_field_assert_error();
    case 2: return mutable_two_field_assert_error();
    default: return nullptr;
  }
}

bool ConfigAssertFailedError::operator==(const ConfigAssertFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConfigAssertFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConfigAssertFailedError::operator<(const ConfigAssertFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ConfigAssertFailedError::Clear() {
  if (data_) { data_.reset(); }
}
void ConfigAssertFailedError::CopyFrom(const ConfigAssertFailedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ConfigAssertFailedError& ConfigAssertFailedError::operator=(const ConfigAssertFailedError& other) {
  CopyFrom(other);
  return *this;
}

void ConfigAssertFailedError::clear_one_field_assert_error() {
  return __SharedPtr__()->clear_one_field_assert_error();
}
::oneflow::cfg::OneFieldAssertError* ConfigAssertFailedError::mutable_one_field_assert_error() {
  return __SharedPtr__()->mutable_one_field_assert_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OneFieldAssertError> ConfigAssertFailedError::shared_mutable_one_field_assert_error() {
  return mutable_one_field_assert_error()->__SharedMutable__();
}
void ConfigAssertFailedError::clear_two_field_assert_error() {
  return __SharedPtr__()->clear_two_field_assert_error();
}
::oneflow::cfg::TwoFieldAssertError* ConfigAssertFailedError::mutable_two_field_assert_error() {
  return __SharedPtr__()->mutable_two_field_assert_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TwoFieldAssertError> ConfigAssertFailedError::shared_mutable_two_field_assert_error() {
  return mutable_two_field_assert_error()->__SharedMutable__();
}

::std::shared_ptr<ConfigAssertFailedError> ConfigAssertFailedError::__SharedMutable__() {
  return ::std::make_shared<ConfigAssertFailedError>(__SharedPtr__());
}
ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::_ConfigResourceUnavailableError_() { Clear(); }
ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::_ConfigResourceUnavailableError_(const _ConfigResourceUnavailableError_& other) { CopyFrom(other); }
ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::_ConfigResourceUnavailableError_(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror) {
  InitFromProto(proto_configresourceunavailableerror);
}
ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::_ConfigResourceUnavailableError_(_ConfigResourceUnavailableError_&& other) = default;
ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::~_ConfigResourceUnavailableError_() = default;

void ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::InitFromProto(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror) {
  Clear();
  // required_or_optional field: field_value
  if (proto_configresourceunavailableerror.has_field_value()) {
  *mutable_field_value() = ::oneflow::cfg::FieldValue(proto_configresourceunavailableerror.field_value());      
  }
    
}

void ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::ToProto(::oneflow::ConfigResourceUnavailableError* proto_configresourceunavailableerror) const {
  proto_configresourceunavailableerror->Clear();
  // required_or_optional field: field_value
  if (this->has_field_value()) {
    ::oneflow::FieldValue proto_field_value;
    field_value().ToProto(&proto_field_value);
    proto_configresourceunavailableerror->mutable_field_value()->CopyFrom(proto_field_value);
    }

}

::std::string ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::DebugString() const {
  ::oneflow::ConfigResourceUnavailableError proto_configresourceunavailableerror;
  this->ToProto(&proto_configresourceunavailableerror);
  return proto_configresourceunavailableerror.DebugString();
}

void ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::Clear() {
  clear_field_value();
}

void ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::CopyFrom(const _ConfigResourceUnavailableError_& other) {
  if (other.has_field_value()) {
    mutable_field_value()->CopyFrom(other.field_value());
  } else {
    clear_field_value();
  }
}


// optional field field_value
bool ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::has_field_value() const {
  return has_field_value_;
}
const ::oneflow::cfg::FieldValue& ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::field_value() const {
  if (!field_value_) {
    static const ::std::shared_ptr<::oneflow::cfg::FieldValue> default_static_value =
      ::std::make_shared<::oneflow::cfg::FieldValue>();
    return *default_static_value;
  }
  return *(field_value_.get());
}
void ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::clear_field_value() {
  if (field_value_) {
    field_value_->Clear();
  }
  has_field_value_ = false;
}
::oneflow::cfg::FieldValue* ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::mutable_field_value() {
  if (!field_value_) {
    field_value_ = ::std::make_shared<::oneflow::cfg::FieldValue>();
  }
  has_field_value_ = true;
  return field_value_.get();
}


int ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::compare(const _ConfigResourceUnavailableError_& other) {
  if (!(has_field_value() == other.has_field_value())) {
    return has_field_value() < other.has_field_value() ? -1 : 1;
  } else if (!(field_value() == other.field_value())) {
    return field_value() < other.field_value() ? -1 : 1;
  }
  return 0;
}

bool ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::operator==(const _ConfigResourceUnavailableError_& other) const {
  return true
    && has_field_value() == other.has_field_value() 
    && field_value() == other.field_value()
  ;
}

std::size_t ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::__CalcHash__() const {
  return 0
    ^ (has_field_value() ? std::hash<::oneflow::cfg::FieldValue>()(field_value()) : 0)
  ;
}

bool ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_::operator<(const _ConfigResourceUnavailableError_& other) const {
  return false
    || !(has_field_value() == other.has_field_value()) ? 
      has_field_value() < other.has_field_value() : false
    || !(field_value() == other.field_value()) ? 
      field_value() < other.field_value() : false
  ;
}

using _ConfigResourceUnavailableError_ =  ConstConfigResourceUnavailableError::_ConfigResourceUnavailableError_;
ConstConfigResourceUnavailableError::ConstConfigResourceUnavailableError(const ::std::shared_ptr<_ConfigResourceUnavailableError_>& data): data_(data) {}
ConstConfigResourceUnavailableError::ConstConfigResourceUnavailableError(): data_(::std::make_shared<_ConfigResourceUnavailableError_>()) {}
ConstConfigResourceUnavailableError::ConstConfigResourceUnavailableError(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror) {
  BuildFromProto(proto_configresourceunavailableerror);
}
ConstConfigResourceUnavailableError::ConstConfigResourceUnavailableError(const ConstConfigResourceUnavailableError&) = default;
ConstConfigResourceUnavailableError::ConstConfigResourceUnavailableError(ConstConfigResourceUnavailableError&&) noexcept = default;
ConstConfigResourceUnavailableError::~ConstConfigResourceUnavailableError() = default;

void ConstConfigResourceUnavailableError::ToProto(PbMessage* proto_configresourceunavailableerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ConfigResourceUnavailableError*>(proto_configresourceunavailableerror));
}
  
::std::string ConstConfigResourceUnavailableError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstConfigResourceUnavailableError::__Empty__() const {
  return !data_;
}

int ConstConfigResourceUnavailableError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"field_value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstConfigResourceUnavailableError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstConfigResourceUnavailableError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldValue),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFieldValue),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstConfigResourceUnavailableError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &field_value();
    default: return nullptr;
  }
}

// required or optional field field_value
bool ConstConfigResourceUnavailableError::has_field_value() const {
  return __SharedPtrOrDefault__()->has_field_value();
}
const ::oneflow::cfg::FieldValue& ConstConfigResourceUnavailableError::field_value() const {
  return __SharedPtrOrDefault__()->field_value();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFieldValue> ConstConfigResourceUnavailableError::shared_const_field_value() const {
  return field_value().__SharedConst__();
}

::std::shared_ptr<ConstConfigResourceUnavailableError> ConstConfigResourceUnavailableError::__SharedConst__() const {
  return ::std::make_shared<ConstConfigResourceUnavailableError>(data_);
}
int64_t ConstConfigResourceUnavailableError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstConfigResourceUnavailableError::operator==(const ConstConfigResourceUnavailableError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstConfigResourceUnavailableError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstConfigResourceUnavailableError::operator<(const ConstConfigResourceUnavailableError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ConfigResourceUnavailableError_>& ConstConfigResourceUnavailableError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ConfigResourceUnavailableError_> default_ptr = std::make_shared<_ConfigResourceUnavailableError_>();
  return default_ptr;
}
const ::std::shared_ptr<_ConfigResourceUnavailableError_>& ConstConfigResourceUnavailableError::__SharedPtr__() {
  if (!data_) { data_.reset(new _ConfigResourceUnavailableError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstConfigResourceUnavailableError
void ConstConfigResourceUnavailableError::BuildFromProto(const PbMessage& proto_configresourceunavailableerror) {
  data_ = ::std::make_shared<_ConfigResourceUnavailableError_>(dynamic_cast<const ::oneflow::ConfigResourceUnavailableError&>(proto_configresourceunavailableerror));
}

ConfigResourceUnavailableError::ConfigResourceUnavailableError(const ::std::shared_ptr<_ConfigResourceUnavailableError_>& data)
  : ConstConfigResourceUnavailableError(data) {}
ConfigResourceUnavailableError::ConfigResourceUnavailableError(const ConfigResourceUnavailableError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ConfigResourceUnavailableError> resize
ConfigResourceUnavailableError::ConfigResourceUnavailableError(ConfigResourceUnavailableError&&) noexcept = default; 
ConfigResourceUnavailableError::ConfigResourceUnavailableError(const ::oneflow::ConfigResourceUnavailableError& proto_configresourceunavailableerror) {
  InitFromProto(proto_configresourceunavailableerror);
}
ConfigResourceUnavailableError::ConfigResourceUnavailableError() = default;

ConfigResourceUnavailableError::~ConfigResourceUnavailableError() = default;

void ConfigResourceUnavailableError::InitFromProto(const PbMessage& proto_configresourceunavailableerror) {
  BuildFromProto(proto_configresourceunavailableerror);
}
  
void* ConfigResourceUnavailableError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_field_value();
    default: return nullptr;
  }
}

bool ConfigResourceUnavailableError::operator==(const ConfigResourceUnavailableError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConfigResourceUnavailableError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConfigResourceUnavailableError::operator<(const ConfigResourceUnavailableError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ConfigResourceUnavailableError::Clear() {
  if (data_) { data_.reset(); }
}
void ConfigResourceUnavailableError::CopyFrom(const ConfigResourceUnavailableError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ConfigResourceUnavailableError& ConfigResourceUnavailableError::operator=(const ConfigResourceUnavailableError& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field field_value
void ConfigResourceUnavailableError::clear_field_value() {
  return __SharedPtr__()->clear_field_value();
}
::oneflow::cfg::FieldValue* ConfigResourceUnavailableError::mutable_field_value() {
  return __SharedPtr__()->mutable_field_value();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FieldValue> ConfigResourceUnavailableError::shared_mutable_field_value() {
  return mutable_field_value()->__SharedMutable__();
}

::std::shared_ptr<ConfigResourceUnavailableError> ConfigResourceUnavailableError::__SharedMutable__() {
  return ::std::make_shared<ConfigResourceUnavailableError>(__SharedPtr__());
}
ConstJobSetEmptyError::_JobSetEmptyError_::_JobSetEmptyError_() { Clear(); }
ConstJobSetEmptyError::_JobSetEmptyError_::_JobSetEmptyError_(const _JobSetEmptyError_& other) { CopyFrom(other); }
ConstJobSetEmptyError::_JobSetEmptyError_::_JobSetEmptyError_(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror) {
  InitFromProto(proto_jobsetemptyerror);
}
ConstJobSetEmptyError::_JobSetEmptyError_::_JobSetEmptyError_(_JobSetEmptyError_&& other) = default;
ConstJobSetEmptyError::_JobSetEmptyError_::~_JobSetEmptyError_() = default;

void ConstJobSetEmptyError::_JobSetEmptyError_::InitFromProto(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror) {
  Clear();
    
}

void ConstJobSetEmptyError::_JobSetEmptyError_::ToProto(::oneflow::JobSetEmptyError* proto_jobsetemptyerror) const {
  proto_jobsetemptyerror->Clear();

}

::std::string ConstJobSetEmptyError::_JobSetEmptyError_::DebugString() const {
  ::oneflow::JobSetEmptyError proto_jobsetemptyerror;
  this->ToProto(&proto_jobsetemptyerror);
  return proto_jobsetemptyerror.DebugString();
}

void ConstJobSetEmptyError::_JobSetEmptyError_::Clear() {
}

void ConstJobSetEmptyError::_JobSetEmptyError_::CopyFrom(const _JobSetEmptyError_& other) {
}



int ConstJobSetEmptyError::_JobSetEmptyError_::compare(const _JobSetEmptyError_& other) {
  return 0;
}

bool ConstJobSetEmptyError::_JobSetEmptyError_::operator==(const _JobSetEmptyError_& other) const {
  return true
  ;
}

std::size_t ConstJobSetEmptyError::_JobSetEmptyError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobSetEmptyError::_JobSetEmptyError_::operator<(const _JobSetEmptyError_& other) const {
  return false
  ;
}

using _JobSetEmptyError_ =  ConstJobSetEmptyError::_JobSetEmptyError_;
ConstJobSetEmptyError::ConstJobSetEmptyError(const ::std::shared_ptr<_JobSetEmptyError_>& data): data_(data) {}
ConstJobSetEmptyError::ConstJobSetEmptyError(): data_(::std::make_shared<_JobSetEmptyError_>()) {}
ConstJobSetEmptyError::ConstJobSetEmptyError(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror) {
  BuildFromProto(proto_jobsetemptyerror);
}
ConstJobSetEmptyError::ConstJobSetEmptyError(const ConstJobSetEmptyError&) = default;
ConstJobSetEmptyError::ConstJobSetEmptyError(ConstJobSetEmptyError&&) noexcept = default;
ConstJobSetEmptyError::~ConstJobSetEmptyError() = default;

void ConstJobSetEmptyError::ToProto(PbMessage* proto_jobsetemptyerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobSetEmptyError*>(proto_jobsetemptyerror));
}
  
::std::string ConstJobSetEmptyError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobSetEmptyError::__Empty__() const {
  return !data_;
}

int ConstJobSetEmptyError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobSetEmptyError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobSetEmptyError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobSetEmptyError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobSetEmptyError> ConstJobSetEmptyError::__SharedConst__() const {
  return ::std::make_shared<ConstJobSetEmptyError>(data_);
}
int64_t ConstJobSetEmptyError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobSetEmptyError::operator==(const ConstJobSetEmptyError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobSetEmptyError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobSetEmptyError::operator<(const ConstJobSetEmptyError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobSetEmptyError_>& ConstJobSetEmptyError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobSetEmptyError_> default_ptr = std::make_shared<_JobSetEmptyError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobSetEmptyError_>& ConstJobSetEmptyError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobSetEmptyError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobSetEmptyError
void ConstJobSetEmptyError::BuildFromProto(const PbMessage& proto_jobsetemptyerror) {
  data_ = ::std::make_shared<_JobSetEmptyError_>(dynamic_cast<const ::oneflow::JobSetEmptyError&>(proto_jobsetemptyerror));
}

JobSetEmptyError::JobSetEmptyError(const ::std::shared_ptr<_JobSetEmptyError_>& data)
  : ConstJobSetEmptyError(data) {}
JobSetEmptyError::JobSetEmptyError(const JobSetEmptyError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobSetEmptyError> resize
JobSetEmptyError::JobSetEmptyError(JobSetEmptyError&&) noexcept = default; 
JobSetEmptyError::JobSetEmptyError(const ::oneflow::JobSetEmptyError& proto_jobsetemptyerror) {
  InitFromProto(proto_jobsetemptyerror);
}
JobSetEmptyError::JobSetEmptyError() = default;

JobSetEmptyError::~JobSetEmptyError() = default;

void JobSetEmptyError::InitFromProto(const PbMessage& proto_jobsetemptyerror) {
  BuildFromProto(proto_jobsetemptyerror);
}
  
void* JobSetEmptyError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobSetEmptyError::operator==(const JobSetEmptyError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobSetEmptyError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobSetEmptyError::operator<(const JobSetEmptyError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobSetEmptyError::Clear() {
  if (data_) { data_.reset(); }
}
void JobSetEmptyError::CopyFrom(const JobSetEmptyError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobSetEmptyError& JobSetEmptyError::operator=(const JobSetEmptyError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobSetEmptyError> JobSetEmptyError::__SharedMutable__() {
  return ::std::make_shared<JobSetEmptyError>(__SharedPtr__());
}
ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::_DeviceTagNotFoundError_() { Clear(); }
ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::_DeviceTagNotFoundError_(const _DeviceTagNotFoundError_& other) { CopyFrom(other); }
ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::_DeviceTagNotFoundError_(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror) {
  InitFromProto(proto_devicetagnotfounderror);
}
ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::_DeviceTagNotFoundError_(_DeviceTagNotFoundError_&& other) = default;
ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::~_DeviceTagNotFoundError_() = default;

void ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::InitFromProto(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror) {
  Clear();
    
}

void ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::ToProto(::oneflow::DeviceTagNotFoundError* proto_devicetagnotfounderror) const {
  proto_devicetagnotfounderror->Clear();

}

::std::string ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::DebugString() const {
  ::oneflow::DeviceTagNotFoundError proto_devicetagnotfounderror;
  this->ToProto(&proto_devicetagnotfounderror);
  return proto_devicetagnotfounderror.DebugString();
}

void ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::Clear() {
}

void ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::CopyFrom(const _DeviceTagNotFoundError_& other) {
}



int ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::compare(const _DeviceTagNotFoundError_& other) {
  return 0;
}

bool ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::operator==(const _DeviceTagNotFoundError_& other) const {
  return true
  ;
}

std::size_t ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_::operator<(const _DeviceTagNotFoundError_& other) const {
  return false
  ;
}

using _DeviceTagNotFoundError_ =  ConstDeviceTagNotFoundError::_DeviceTagNotFoundError_;
ConstDeviceTagNotFoundError::ConstDeviceTagNotFoundError(const ::std::shared_ptr<_DeviceTagNotFoundError_>& data): data_(data) {}
ConstDeviceTagNotFoundError::ConstDeviceTagNotFoundError(): data_(::std::make_shared<_DeviceTagNotFoundError_>()) {}
ConstDeviceTagNotFoundError::ConstDeviceTagNotFoundError(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror) {
  BuildFromProto(proto_devicetagnotfounderror);
}
ConstDeviceTagNotFoundError::ConstDeviceTagNotFoundError(const ConstDeviceTagNotFoundError&) = default;
ConstDeviceTagNotFoundError::ConstDeviceTagNotFoundError(ConstDeviceTagNotFoundError&&) noexcept = default;
ConstDeviceTagNotFoundError::~ConstDeviceTagNotFoundError() = default;

void ConstDeviceTagNotFoundError::ToProto(PbMessage* proto_devicetagnotfounderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::DeviceTagNotFoundError*>(proto_devicetagnotfounderror));
}
  
::std::string ConstDeviceTagNotFoundError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstDeviceTagNotFoundError::__Empty__() const {
  return !data_;
}

int ConstDeviceTagNotFoundError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstDeviceTagNotFoundError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstDeviceTagNotFoundError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstDeviceTagNotFoundError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstDeviceTagNotFoundError> ConstDeviceTagNotFoundError::__SharedConst__() const {
  return ::std::make_shared<ConstDeviceTagNotFoundError>(data_);
}
int64_t ConstDeviceTagNotFoundError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstDeviceTagNotFoundError::operator==(const ConstDeviceTagNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstDeviceTagNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstDeviceTagNotFoundError::operator<(const ConstDeviceTagNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_DeviceTagNotFoundError_>& ConstDeviceTagNotFoundError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_DeviceTagNotFoundError_> default_ptr = std::make_shared<_DeviceTagNotFoundError_>();
  return default_ptr;
}
const ::std::shared_ptr<_DeviceTagNotFoundError_>& ConstDeviceTagNotFoundError::__SharedPtr__() {
  if (!data_) { data_.reset(new _DeviceTagNotFoundError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstDeviceTagNotFoundError
void ConstDeviceTagNotFoundError::BuildFromProto(const PbMessage& proto_devicetagnotfounderror) {
  data_ = ::std::make_shared<_DeviceTagNotFoundError_>(dynamic_cast<const ::oneflow::DeviceTagNotFoundError&>(proto_devicetagnotfounderror));
}

DeviceTagNotFoundError::DeviceTagNotFoundError(const ::std::shared_ptr<_DeviceTagNotFoundError_>& data)
  : ConstDeviceTagNotFoundError(data) {}
DeviceTagNotFoundError::DeviceTagNotFoundError(const DeviceTagNotFoundError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<DeviceTagNotFoundError> resize
DeviceTagNotFoundError::DeviceTagNotFoundError(DeviceTagNotFoundError&&) noexcept = default; 
DeviceTagNotFoundError::DeviceTagNotFoundError(const ::oneflow::DeviceTagNotFoundError& proto_devicetagnotfounderror) {
  InitFromProto(proto_devicetagnotfounderror);
}
DeviceTagNotFoundError::DeviceTagNotFoundError() = default;

DeviceTagNotFoundError::~DeviceTagNotFoundError() = default;

void DeviceTagNotFoundError::InitFromProto(const PbMessage& proto_devicetagnotfounderror) {
  BuildFromProto(proto_devicetagnotfounderror);
}
  
void* DeviceTagNotFoundError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool DeviceTagNotFoundError::operator==(const DeviceTagNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t DeviceTagNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool DeviceTagNotFoundError::operator<(const DeviceTagNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void DeviceTagNotFoundError::Clear() {
  if (data_) { data_.reset(); }
}
void DeviceTagNotFoundError::CopyFrom(const DeviceTagNotFoundError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
DeviceTagNotFoundError& DeviceTagNotFoundError::operator=(const DeviceTagNotFoundError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<DeviceTagNotFoundError> DeviceTagNotFoundError::__SharedMutable__() {
  return ::std::make_shared<DeviceTagNotFoundError>(__SharedPtr__());
}
ConstJobNameExistError::_JobNameExistError_::_JobNameExistError_() { Clear(); }
ConstJobNameExistError::_JobNameExistError_::_JobNameExistError_(const _JobNameExistError_& other) { CopyFrom(other); }
ConstJobNameExistError::_JobNameExistError_::_JobNameExistError_(const ::oneflow::JobNameExistError& proto_jobnameexisterror) {
  InitFromProto(proto_jobnameexisterror);
}
ConstJobNameExistError::_JobNameExistError_::_JobNameExistError_(_JobNameExistError_&& other) = default;
ConstJobNameExistError::_JobNameExistError_::~_JobNameExistError_() = default;

void ConstJobNameExistError::_JobNameExistError_::InitFromProto(const ::oneflow::JobNameExistError& proto_jobnameexisterror) {
  Clear();
    
}

void ConstJobNameExistError::_JobNameExistError_::ToProto(::oneflow::JobNameExistError* proto_jobnameexisterror) const {
  proto_jobnameexisterror->Clear();

}

::std::string ConstJobNameExistError::_JobNameExistError_::DebugString() const {
  ::oneflow::JobNameExistError proto_jobnameexisterror;
  this->ToProto(&proto_jobnameexisterror);
  return proto_jobnameexisterror.DebugString();
}

void ConstJobNameExistError::_JobNameExistError_::Clear() {
}

void ConstJobNameExistError::_JobNameExistError_::CopyFrom(const _JobNameExistError_& other) {
}



int ConstJobNameExistError::_JobNameExistError_::compare(const _JobNameExistError_& other) {
  return 0;
}

bool ConstJobNameExistError::_JobNameExistError_::operator==(const _JobNameExistError_& other) const {
  return true
  ;
}

std::size_t ConstJobNameExistError::_JobNameExistError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobNameExistError::_JobNameExistError_::operator<(const _JobNameExistError_& other) const {
  return false
  ;
}

using _JobNameExistError_ =  ConstJobNameExistError::_JobNameExistError_;
ConstJobNameExistError::ConstJobNameExistError(const ::std::shared_ptr<_JobNameExistError_>& data): data_(data) {}
ConstJobNameExistError::ConstJobNameExistError(): data_(::std::make_shared<_JobNameExistError_>()) {}
ConstJobNameExistError::ConstJobNameExistError(const ::oneflow::JobNameExistError& proto_jobnameexisterror) {
  BuildFromProto(proto_jobnameexisterror);
}
ConstJobNameExistError::ConstJobNameExistError(const ConstJobNameExistError&) = default;
ConstJobNameExistError::ConstJobNameExistError(ConstJobNameExistError&&) noexcept = default;
ConstJobNameExistError::~ConstJobNameExistError() = default;

void ConstJobNameExistError::ToProto(PbMessage* proto_jobnameexisterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobNameExistError*>(proto_jobnameexisterror));
}
  
::std::string ConstJobNameExistError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobNameExistError::__Empty__() const {
  return !data_;
}

int ConstJobNameExistError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobNameExistError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobNameExistError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobNameExistError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobNameExistError> ConstJobNameExistError::__SharedConst__() const {
  return ::std::make_shared<ConstJobNameExistError>(data_);
}
int64_t ConstJobNameExistError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobNameExistError::operator==(const ConstJobNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobNameExistError::operator<(const ConstJobNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobNameExistError_>& ConstJobNameExistError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobNameExistError_> default_ptr = std::make_shared<_JobNameExistError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobNameExistError_>& ConstJobNameExistError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobNameExistError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobNameExistError
void ConstJobNameExistError::BuildFromProto(const PbMessage& proto_jobnameexisterror) {
  data_ = ::std::make_shared<_JobNameExistError_>(dynamic_cast<const ::oneflow::JobNameExistError&>(proto_jobnameexisterror));
}

JobNameExistError::JobNameExistError(const ::std::shared_ptr<_JobNameExistError_>& data)
  : ConstJobNameExistError(data) {}
JobNameExistError::JobNameExistError(const JobNameExistError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobNameExistError> resize
JobNameExistError::JobNameExistError(JobNameExistError&&) noexcept = default; 
JobNameExistError::JobNameExistError(const ::oneflow::JobNameExistError& proto_jobnameexisterror) {
  InitFromProto(proto_jobnameexisterror);
}
JobNameExistError::JobNameExistError() = default;

JobNameExistError::~JobNameExistError() = default;

void JobNameExistError::InitFromProto(const PbMessage& proto_jobnameexisterror) {
  BuildFromProto(proto_jobnameexisterror);
}
  
void* JobNameExistError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobNameExistError::operator==(const JobNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobNameExistError::operator<(const JobNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobNameExistError::Clear() {
  if (data_) { data_.reset(); }
}
void JobNameExistError::CopyFrom(const JobNameExistError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobNameExistError& JobNameExistError::operator=(const JobNameExistError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobNameExistError> JobNameExistError::__SharedMutable__() {
  return ::std::make_shared<JobNameExistError>(__SharedPtr__());
}
ConstJobNameEmptyError::_JobNameEmptyError_::_JobNameEmptyError_() { Clear(); }
ConstJobNameEmptyError::_JobNameEmptyError_::_JobNameEmptyError_(const _JobNameEmptyError_& other) { CopyFrom(other); }
ConstJobNameEmptyError::_JobNameEmptyError_::_JobNameEmptyError_(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror) {
  InitFromProto(proto_jobnameemptyerror);
}
ConstJobNameEmptyError::_JobNameEmptyError_::_JobNameEmptyError_(_JobNameEmptyError_&& other) = default;
ConstJobNameEmptyError::_JobNameEmptyError_::~_JobNameEmptyError_() = default;

void ConstJobNameEmptyError::_JobNameEmptyError_::InitFromProto(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror) {
  Clear();
    
}

void ConstJobNameEmptyError::_JobNameEmptyError_::ToProto(::oneflow::JobNameEmptyError* proto_jobnameemptyerror) const {
  proto_jobnameemptyerror->Clear();

}

::std::string ConstJobNameEmptyError::_JobNameEmptyError_::DebugString() const {
  ::oneflow::JobNameEmptyError proto_jobnameemptyerror;
  this->ToProto(&proto_jobnameemptyerror);
  return proto_jobnameemptyerror.DebugString();
}

void ConstJobNameEmptyError::_JobNameEmptyError_::Clear() {
}

void ConstJobNameEmptyError::_JobNameEmptyError_::CopyFrom(const _JobNameEmptyError_& other) {
}



int ConstJobNameEmptyError::_JobNameEmptyError_::compare(const _JobNameEmptyError_& other) {
  return 0;
}

bool ConstJobNameEmptyError::_JobNameEmptyError_::operator==(const _JobNameEmptyError_& other) const {
  return true
  ;
}

std::size_t ConstJobNameEmptyError::_JobNameEmptyError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobNameEmptyError::_JobNameEmptyError_::operator<(const _JobNameEmptyError_& other) const {
  return false
  ;
}

using _JobNameEmptyError_ =  ConstJobNameEmptyError::_JobNameEmptyError_;
ConstJobNameEmptyError::ConstJobNameEmptyError(const ::std::shared_ptr<_JobNameEmptyError_>& data): data_(data) {}
ConstJobNameEmptyError::ConstJobNameEmptyError(): data_(::std::make_shared<_JobNameEmptyError_>()) {}
ConstJobNameEmptyError::ConstJobNameEmptyError(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror) {
  BuildFromProto(proto_jobnameemptyerror);
}
ConstJobNameEmptyError::ConstJobNameEmptyError(const ConstJobNameEmptyError&) = default;
ConstJobNameEmptyError::ConstJobNameEmptyError(ConstJobNameEmptyError&&) noexcept = default;
ConstJobNameEmptyError::~ConstJobNameEmptyError() = default;

void ConstJobNameEmptyError::ToProto(PbMessage* proto_jobnameemptyerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobNameEmptyError*>(proto_jobnameemptyerror));
}
  
::std::string ConstJobNameEmptyError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobNameEmptyError::__Empty__() const {
  return !data_;
}

int ConstJobNameEmptyError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobNameEmptyError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobNameEmptyError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobNameEmptyError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobNameEmptyError> ConstJobNameEmptyError::__SharedConst__() const {
  return ::std::make_shared<ConstJobNameEmptyError>(data_);
}
int64_t ConstJobNameEmptyError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobNameEmptyError::operator==(const ConstJobNameEmptyError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobNameEmptyError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobNameEmptyError::operator<(const ConstJobNameEmptyError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobNameEmptyError_>& ConstJobNameEmptyError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobNameEmptyError_> default_ptr = std::make_shared<_JobNameEmptyError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobNameEmptyError_>& ConstJobNameEmptyError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobNameEmptyError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobNameEmptyError
void ConstJobNameEmptyError::BuildFromProto(const PbMessage& proto_jobnameemptyerror) {
  data_ = ::std::make_shared<_JobNameEmptyError_>(dynamic_cast<const ::oneflow::JobNameEmptyError&>(proto_jobnameemptyerror));
}

JobNameEmptyError::JobNameEmptyError(const ::std::shared_ptr<_JobNameEmptyError_>& data)
  : ConstJobNameEmptyError(data) {}
JobNameEmptyError::JobNameEmptyError(const JobNameEmptyError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobNameEmptyError> resize
JobNameEmptyError::JobNameEmptyError(JobNameEmptyError&&) noexcept = default; 
JobNameEmptyError::JobNameEmptyError(const ::oneflow::JobNameEmptyError& proto_jobnameemptyerror) {
  InitFromProto(proto_jobnameemptyerror);
}
JobNameEmptyError::JobNameEmptyError() = default;

JobNameEmptyError::~JobNameEmptyError() = default;

void JobNameEmptyError::InitFromProto(const PbMessage& proto_jobnameemptyerror) {
  BuildFromProto(proto_jobnameemptyerror);
}
  
void* JobNameEmptyError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobNameEmptyError::operator==(const JobNameEmptyError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobNameEmptyError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobNameEmptyError::operator<(const JobNameEmptyError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobNameEmptyError::Clear() {
  if (data_) { data_.reset(); }
}
void JobNameEmptyError::CopyFrom(const JobNameEmptyError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobNameEmptyError& JobNameEmptyError::operator=(const JobNameEmptyError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobNameEmptyError> JobNameEmptyError::__SharedMutable__() {
  return ::std::make_shared<JobNameEmptyError>(__SharedPtr__());
}
ConstJobNameNotEqualError::_JobNameNotEqualError_::_JobNameNotEqualError_() { Clear(); }
ConstJobNameNotEqualError::_JobNameNotEqualError_::_JobNameNotEqualError_(const _JobNameNotEqualError_& other) { CopyFrom(other); }
ConstJobNameNotEqualError::_JobNameNotEqualError_::_JobNameNotEqualError_(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror) {
  InitFromProto(proto_jobnamenotequalerror);
}
ConstJobNameNotEqualError::_JobNameNotEqualError_::_JobNameNotEqualError_(_JobNameNotEqualError_&& other) = default;
ConstJobNameNotEqualError::_JobNameNotEqualError_::~_JobNameNotEqualError_() = default;

void ConstJobNameNotEqualError::_JobNameNotEqualError_::InitFromProto(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror) {
  Clear();
    
}

void ConstJobNameNotEqualError::_JobNameNotEqualError_::ToProto(::oneflow::JobNameNotEqualError* proto_jobnamenotequalerror) const {
  proto_jobnamenotequalerror->Clear();

}

::std::string ConstJobNameNotEqualError::_JobNameNotEqualError_::DebugString() const {
  ::oneflow::JobNameNotEqualError proto_jobnamenotequalerror;
  this->ToProto(&proto_jobnamenotequalerror);
  return proto_jobnamenotequalerror.DebugString();
}

void ConstJobNameNotEqualError::_JobNameNotEqualError_::Clear() {
}

void ConstJobNameNotEqualError::_JobNameNotEqualError_::CopyFrom(const _JobNameNotEqualError_& other) {
}



int ConstJobNameNotEqualError::_JobNameNotEqualError_::compare(const _JobNameNotEqualError_& other) {
  return 0;
}

bool ConstJobNameNotEqualError::_JobNameNotEqualError_::operator==(const _JobNameNotEqualError_& other) const {
  return true
  ;
}

std::size_t ConstJobNameNotEqualError::_JobNameNotEqualError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobNameNotEqualError::_JobNameNotEqualError_::operator<(const _JobNameNotEqualError_& other) const {
  return false
  ;
}

using _JobNameNotEqualError_ =  ConstJobNameNotEqualError::_JobNameNotEqualError_;
ConstJobNameNotEqualError::ConstJobNameNotEqualError(const ::std::shared_ptr<_JobNameNotEqualError_>& data): data_(data) {}
ConstJobNameNotEqualError::ConstJobNameNotEqualError(): data_(::std::make_shared<_JobNameNotEqualError_>()) {}
ConstJobNameNotEqualError::ConstJobNameNotEqualError(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror) {
  BuildFromProto(proto_jobnamenotequalerror);
}
ConstJobNameNotEqualError::ConstJobNameNotEqualError(const ConstJobNameNotEqualError&) = default;
ConstJobNameNotEqualError::ConstJobNameNotEqualError(ConstJobNameNotEqualError&&) noexcept = default;
ConstJobNameNotEqualError::~ConstJobNameNotEqualError() = default;

void ConstJobNameNotEqualError::ToProto(PbMessage* proto_jobnamenotequalerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobNameNotEqualError*>(proto_jobnamenotequalerror));
}
  
::std::string ConstJobNameNotEqualError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobNameNotEqualError::__Empty__() const {
  return !data_;
}

int ConstJobNameNotEqualError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobNameNotEqualError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobNameNotEqualError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobNameNotEqualError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobNameNotEqualError> ConstJobNameNotEqualError::__SharedConst__() const {
  return ::std::make_shared<ConstJobNameNotEqualError>(data_);
}
int64_t ConstJobNameNotEqualError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobNameNotEqualError::operator==(const ConstJobNameNotEqualError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobNameNotEqualError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobNameNotEqualError::operator<(const ConstJobNameNotEqualError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobNameNotEqualError_>& ConstJobNameNotEqualError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobNameNotEqualError_> default_ptr = std::make_shared<_JobNameNotEqualError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobNameNotEqualError_>& ConstJobNameNotEqualError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobNameNotEqualError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobNameNotEqualError
void ConstJobNameNotEqualError::BuildFromProto(const PbMessage& proto_jobnamenotequalerror) {
  data_ = ::std::make_shared<_JobNameNotEqualError_>(dynamic_cast<const ::oneflow::JobNameNotEqualError&>(proto_jobnamenotequalerror));
}

JobNameNotEqualError::JobNameNotEqualError(const ::std::shared_ptr<_JobNameNotEqualError_>& data)
  : ConstJobNameNotEqualError(data) {}
JobNameNotEqualError::JobNameNotEqualError(const JobNameNotEqualError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobNameNotEqualError> resize
JobNameNotEqualError::JobNameNotEqualError(JobNameNotEqualError&&) noexcept = default; 
JobNameNotEqualError::JobNameNotEqualError(const ::oneflow::JobNameNotEqualError& proto_jobnamenotequalerror) {
  InitFromProto(proto_jobnamenotequalerror);
}
JobNameNotEqualError::JobNameNotEqualError() = default;

JobNameNotEqualError::~JobNameNotEqualError() = default;

void JobNameNotEqualError::InitFromProto(const PbMessage& proto_jobnamenotequalerror) {
  BuildFromProto(proto_jobnamenotequalerror);
}
  
void* JobNameNotEqualError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobNameNotEqualError::operator==(const JobNameNotEqualError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobNameNotEqualError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobNameNotEqualError::operator<(const JobNameNotEqualError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobNameNotEqualError::Clear() {
  if (data_) { data_.reset(); }
}
void JobNameNotEqualError::CopyFrom(const JobNameNotEqualError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobNameNotEqualError& JobNameNotEqualError::operator=(const JobNameNotEqualError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobNameNotEqualError> JobNameNotEqualError::__SharedMutable__() {
  return ::std::make_shared<JobNameNotEqualError>(__SharedPtr__());
}
ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::_NoJobBuildAndInferCtxError_() { Clear(); }
ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::_NoJobBuildAndInferCtxError_(const _NoJobBuildAndInferCtxError_& other) { CopyFrom(other); }
ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::_NoJobBuildAndInferCtxError_(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror) {
  InitFromProto(proto_nojobbuildandinferctxerror);
}
ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::_NoJobBuildAndInferCtxError_(_NoJobBuildAndInferCtxError_&& other) = default;
ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::~_NoJobBuildAndInferCtxError_() = default;

void ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::InitFromProto(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror) {
  Clear();
    
}

void ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::ToProto(::oneflow::NoJobBuildAndInferCtxError* proto_nojobbuildandinferctxerror) const {
  proto_nojobbuildandinferctxerror->Clear();

}

::std::string ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::DebugString() const {
  ::oneflow::NoJobBuildAndInferCtxError proto_nojobbuildandinferctxerror;
  this->ToProto(&proto_nojobbuildandinferctxerror);
  return proto_nojobbuildandinferctxerror.DebugString();
}

void ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::Clear() {
}

void ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::CopyFrom(const _NoJobBuildAndInferCtxError_& other) {
}



int ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::compare(const _NoJobBuildAndInferCtxError_& other) {
  return 0;
}

bool ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::operator==(const _NoJobBuildAndInferCtxError_& other) const {
  return true
  ;
}

std::size_t ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_::operator<(const _NoJobBuildAndInferCtxError_& other) const {
  return false
  ;
}

using _NoJobBuildAndInferCtxError_ =  ConstNoJobBuildAndInferCtxError::_NoJobBuildAndInferCtxError_;
ConstNoJobBuildAndInferCtxError::ConstNoJobBuildAndInferCtxError(const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& data): data_(data) {}
ConstNoJobBuildAndInferCtxError::ConstNoJobBuildAndInferCtxError(): data_(::std::make_shared<_NoJobBuildAndInferCtxError_>()) {}
ConstNoJobBuildAndInferCtxError::ConstNoJobBuildAndInferCtxError(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror) {
  BuildFromProto(proto_nojobbuildandinferctxerror);
}
ConstNoJobBuildAndInferCtxError::ConstNoJobBuildAndInferCtxError(const ConstNoJobBuildAndInferCtxError&) = default;
ConstNoJobBuildAndInferCtxError::ConstNoJobBuildAndInferCtxError(ConstNoJobBuildAndInferCtxError&&) noexcept = default;
ConstNoJobBuildAndInferCtxError::~ConstNoJobBuildAndInferCtxError() = default;

void ConstNoJobBuildAndInferCtxError::ToProto(PbMessage* proto_nojobbuildandinferctxerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::NoJobBuildAndInferCtxError*>(proto_nojobbuildandinferctxerror));
}
  
::std::string ConstNoJobBuildAndInferCtxError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstNoJobBuildAndInferCtxError::__Empty__() const {
  return !data_;
}

int ConstNoJobBuildAndInferCtxError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstNoJobBuildAndInferCtxError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstNoJobBuildAndInferCtxError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstNoJobBuildAndInferCtxError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstNoJobBuildAndInferCtxError> ConstNoJobBuildAndInferCtxError::__SharedConst__() const {
  return ::std::make_shared<ConstNoJobBuildAndInferCtxError>(data_);
}
int64_t ConstNoJobBuildAndInferCtxError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstNoJobBuildAndInferCtxError::operator==(const ConstNoJobBuildAndInferCtxError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstNoJobBuildAndInferCtxError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstNoJobBuildAndInferCtxError::operator<(const ConstNoJobBuildAndInferCtxError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& ConstNoJobBuildAndInferCtxError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_NoJobBuildAndInferCtxError_> default_ptr = std::make_shared<_NoJobBuildAndInferCtxError_>();
  return default_ptr;
}
const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& ConstNoJobBuildAndInferCtxError::__SharedPtr__() {
  if (!data_) { data_.reset(new _NoJobBuildAndInferCtxError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstNoJobBuildAndInferCtxError
void ConstNoJobBuildAndInferCtxError::BuildFromProto(const PbMessage& proto_nojobbuildandinferctxerror) {
  data_ = ::std::make_shared<_NoJobBuildAndInferCtxError_>(dynamic_cast<const ::oneflow::NoJobBuildAndInferCtxError&>(proto_nojobbuildandinferctxerror));
}

NoJobBuildAndInferCtxError::NoJobBuildAndInferCtxError(const ::std::shared_ptr<_NoJobBuildAndInferCtxError_>& data)
  : ConstNoJobBuildAndInferCtxError(data) {}
NoJobBuildAndInferCtxError::NoJobBuildAndInferCtxError(const NoJobBuildAndInferCtxError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<NoJobBuildAndInferCtxError> resize
NoJobBuildAndInferCtxError::NoJobBuildAndInferCtxError(NoJobBuildAndInferCtxError&&) noexcept = default; 
NoJobBuildAndInferCtxError::NoJobBuildAndInferCtxError(const ::oneflow::NoJobBuildAndInferCtxError& proto_nojobbuildandinferctxerror) {
  InitFromProto(proto_nojobbuildandinferctxerror);
}
NoJobBuildAndInferCtxError::NoJobBuildAndInferCtxError() = default;

NoJobBuildAndInferCtxError::~NoJobBuildAndInferCtxError() = default;

void NoJobBuildAndInferCtxError::InitFromProto(const PbMessage& proto_nojobbuildandinferctxerror) {
  BuildFromProto(proto_nojobbuildandinferctxerror);
}
  
void* NoJobBuildAndInferCtxError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool NoJobBuildAndInferCtxError::operator==(const NoJobBuildAndInferCtxError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t NoJobBuildAndInferCtxError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool NoJobBuildAndInferCtxError::operator<(const NoJobBuildAndInferCtxError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void NoJobBuildAndInferCtxError::Clear() {
  if (data_) { data_.reset(); }
}
void NoJobBuildAndInferCtxError::CopyFrom(const NoJobBuildAndInferCtxError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
NoJobBuildAndInferCtxError& NoJobBuildAndInferCtxError::operator=(const NoJobBuildAndInferCtxError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<NoJobBuildAndInferCtxError> NoJobBuildAndInferCtxError::__SharedMutable__() {
  return ::std::make_shared<NoJobBuildAndInferCtxError>(__SharedPtr__());
}
ConstJobConfFrozenError::_JobConfFrozenError_::_JobConfFrozenError_() { Clear(); }
ConstJobConfFrozenError::_JobConfFrozenError_::_JobConfFrozenError_(const _JobConfFrozenError_& other) { CopyFrom(other); }
ConstJobConfFrozenError::_JobConfFrozenError_::_JobConfFrozenError_(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror) {
  InitFromProto(proto_jobconffrozenerror);
}
ConstJobConfFrozenError::_JobConfFrozenError_::_JobConfFrozenError_(_JobConfFrozenError_&& other) = default;
ConstJobConfFrozenError::_JobConfFrozenError_::~_JobConfFrozenError_() = default;

void ConstJobConfFrozenError::_JobConfFrozenError_::InitFromProto(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror) {
  Clear();
    
}

void ConstJobConfFrozenError::_JobConfFrozenError_::ToProto(::oneflow::JobConfFrozenError* proto_jobconffrozenerror) const {
  proto_jobconffrozenerror->Clear();

}

::std::string ConstJobConfFrozenError::_JobConfFrozenError_::DebugString() const {
  ::oneflow::JobConfFrozenError proto_jobconffrozenerror;
  this->ToProto(&proto_jobconffrozenerror);
  return proto_jobconffrozenerror.DebugString();
}

void ConstJobConfFrozenError::_JobConfFrozenError_::Clear() {
}

void ConstJobConfFrozenError::_JobConfFrozenError_::CopyFrom(const _JobConfFrozenError_& other) {
}



int ConstJobConfFrozenError::_JobConfFrozenError_::compare(const _JobConfFrozenError_& other) {
  return 0;
}

bool ConstJobConfFrozenError::_JobConfFrozenError_::operator==(const _JobConfFrozenError_& other) const {
  return true
  ;
}

std::size_t ConstJobConfFrozenError::_JobConfFrozenError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobConfFrozenError::_JobConfFrozenError_::operator<(const _JobConfFrozenError_& other) const {
  return false
  ;
}

using _JobConfFrozenError_ =  ConstJobConfFrozenError::_JobConfFrozenError_;
ConstJobConfFrozenError::ConstJobConfFrozenError(const ::std::shared_ptr<_JobConfFrozenError_>& data): data_(data) {}
ConstJobConfFrozenError::ConstJobConfFrozenError(): data_(::std::make_shared<_JobConfFrozenError_>()) {}
ConstJobConfFrozenError::ConstJobConfFrozenError(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror) {
  BuildFromProto(proto_jobconffrozenerror);
}
ConstJobConfFrozenError::ConstJobConfFrozenError(const ConstJobConfFrozenError&) = default;
ConstJobConfFrozenError::ConstJobConfFrozenError(ConstJobConfFrozenError&&) noexcept = default;
ConstJobConfFrozenError::~ConstJobConfFrozenError() = default;

void ConstJobConfFrozenError::ToProto(PbMessage* proto_jobconffrozenerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobConfFrozenError*>(proto_jobconffrozenerror));
}
  
::std::string ConstJobConfFrozenError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobConfFrozenError::__Empty__() const {
  return !data_;
}

int ConstJobConfFrozenError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobConfFrozenError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobConfFrozenError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobConfFrozenError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobConfFrozenError> ConstJobConfFrozenError::__SharedConst__() const {
  return ::std::make_shared<ConstJobConfFrozenError>(data_);
}
int64_t ConstJobConfFrozenError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobConfFrozenError::operator==(const ConstJobConfFrozenError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobConfFrozenError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobConfFrozenError::operator<(const ConstJobConfFrozenError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobConfFrozenError_>& ConstJobConfFrozenError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobConfFrozenError_> default_ptr = std::make_shared<_JobConfFrozenError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobConfFrozenError_>& ConstJobConfFrozenError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobConfFrozenError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobConfFrozenError
void ConstJobConfFrozenError::BuildFromProto(const PbMessage& proto_jobconffrozenerror) {
  data_ = ::std::make_shared<_JobConfFrozenError_>(dynamic_cast<const ::oneflow::JobConfFrozenError&>(proto_jobconffrozenerror));
}

JobConfFrozenError::JobConfFrozenError(const ::std::shared_ptr<_JobConfFrozenError_>& data)
  : ConstJobConfFrozenError(data) {}
JobConfFrozenError::JobConfFrozenError(const JobConfFrozenError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobConfFrozenError> resize
JobConfFrozenError::JobConfFrozenError(JobConfFrozenError&&) noexcept = default; 
JobConfFrozenError::JobConfFrozenError(const ::oneflow::JobConfFrozenError& proto_jobconffrozenerror) {
  InitFromProto(proto_jobconffrozenerror);
}
JobConfFrozenError::JobConfFrozenError() = default;

JobConfFrozenError::~JobConfFrozenError() = default;

void JobConfFrozenError::InitFromProto(const PbMessage& proto_jobconffrozenerror) {
  BuildFromProto(proto_jobconffrozenerror);
}
  
void* JobConfFrozenError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobConfFrozenError::operator==(const JobConfFrozenError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobConfFrozenError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobConfFrozenError::operator<(const JobConfFrozenError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobConfFrozenError::Clear() {
  if (data_) { data_.reset(); }
}
void JobConfFrozenError::CopyFrom(const JobConfFrozenError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobConfFrozenError& JobConfFrozenError::operator=(const JobConfFrozenError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobConfFrozenError> JobConfFrozenError::__SharedMutable__() {
  return ::std::make_shared<JobConfFrozenError>(__SharedPtr__());
}
ConstJobConfNotSetError::_JobConfNotSetError_::_JobConfNotSetError_() { Clear(); }
ConstJobConfNotSetError::_JobConfNotSetError_::_JobConfNotSetError_(const _JobConfNotSetError_& other) { CopyFrom(other); }
ConstJobConfNotSetError::_JobConfNotSetError_::_JobConfNotSetError_(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror) {
  InitFromProto(proto_jobconfnotseterror);
}
ConstJobConfNotSetError::_JobConfNotSetError_::_JobConfNotSetError_(_JobConfNotSetError_&& other) = default;
ConstJobConfNotSetError::_JobConfNotSetError_::~_JobConfNotSetError_() = default;

void ConstJobConfNotSetError::_JobConfNotSetError_::InitFromProto(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror) {
  Clear();
    
}

void ConstJobConfNotSetError::_JobConfNotSetError_::ToProto(::oneflow::JobConfNotSetError* proto_jobconfnotseterror) const {
  proto_jobconfnotseterror->Clear();

}

::std::string ConstJobConfNotSetError::_JobConfNotSetError_::DebugString() const {
  ::oneflow::JobConfNotSetError proto_jobconfnotseterror;
  this->ToProto(&proto_jobconfnotseterror);
  return proto_jobconfnotseterror.DebugString();
}

void ConstJobConfNotSetError::_JobConfNotSetError_::Clear() {
}

void ConstJobConfNotSetError::_JobConfNotSetError_::CopyFrom(const _JobConfNotSetError_& other) {
}



int ConstJobConfNotSetError::_JobConfNotSetError_::compare(const _JobConfNotSetError_& other) {
  return 0;
}

bool ConstJobConfNotSetError::_JobConfNotSetError_::operator==(const _JobConfNotSetError_& other) const {
  return true
  ;
}

std::size_t ConstJobConfNotSetError::_JobConfNotSetError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobConfNotSetError::_JobConfNotSetError_::operator<(const _JobConfNotSetError_& other) const {
  return false
  ;
}

using _JobConfNotSetError_ =  ConstJobConfNotSetError::_JobConfNotSetError_;
ConstJobConfNotSetError::ConstJobConfNotSetError(const ::std::shared_ptr<_JobConfNotSetError_>& data): data_(data) {}
ConstJobConfNotSetError::ConstJobConfNotSetError(): data_(::std::make_shared<_JobConfNotSetError_>()) {}
ConstJobConfNotSetError::ConstJobConfNotSetError(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror) {
  BuildFromProto(proto_jobconfnotseterror);
}
ConstJobConfNotSetError::ConstJobConfNotSetError(const ConstJobConfNotSetError&) = default;
ConstJobConfNotSetError::ConstJobConfNotSetError(ConstJobConfNotSetError&&) noexcept = default;
ConstJobConfNotSetError::~ConstJobConfNotSetError() = default;

void ConstJobConfNotSetError::ToProto(PbMessage* proto_jobconfnotseterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobConfNotSetError*>(proto_jobconfnotseterror));
}
  
::std::string ConstJobConfNotSetError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobConfNotSetError::__Empty__() const {
  return !data_;
}

int ConstJobConfNotSetError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobConfNotSetError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobConfNotSetError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobConfNotSetError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobConfNotSetError> ConstJobConfNotSetError::__SharedConst__() const {
  return ::std::make_shared<ConstJobConfNotSetError>(data_);
}
int64_t ConstJobConfNotSetError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobConfNotSetError::operator==(const ConstJobConfNotSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobConfNotSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobConfNotSetError::operator<(const ConstJobConfNotSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobConfNotSetError_>& ConstJobConfNotSetError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobConfNotSetError_> default_ptr = std::make_shared<_JobConfNotSetError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobConfNotSetError_>& ConstJobConfNotSetError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobConfNotSetError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobConfNotSetError
void ConstJobConfNotSetError::BuildFromProto(const PbMessage& proto_jobconfnotseterror) {
  data_ = ::std::make_shared<_JobConfNotSetError_>(dynamic_cast<const ::oneflow::JobConfNotSetError&>(proto_jobconfnotseterror));
}

JobConfNotSetError::JobConfNotSetError(const ::std::shared_ptr<_JobConfNotSetError_>& data)
  : ConstJobConfNotSetError(data) {}
JobConfNotSetError::JobConfNotSetError(const JobConfNotSetError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobConfNotSetError> resize
JobConfNotSetError::JobConfNotSetError(JobConfNotSetError&&) noexcept = default; 
JobConfNotSetError::JobConfNotSetError(const ::oneflow::JobConfNotSetError& proto_jobconfnotseterror) {
  InitFromProto(proto_jobconfnotseterror);
}
JobConfNotSetError::JobConfNotSetError() = default;

JobConfNotSetError::~JobConfNotSetError() = default;

void JobConfNotSetError::InitFromProto(const PbMessage& proto_jobconfnotseterror) {
  BuildFromProto(proto_jobconfnotseterror);
}
  
void* JobConfNotSetError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobConfNotSetError::operator==(const JobConfNotSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobConfNotSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobConfNotSetError::operator<(const JobConfNotSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobConfNotSetError::Clear() {
  if (data_) { data_.reset(); }
}
void JobConfNotSetError::CopyFrom(const JobConfNotSetError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobConfNotSetError& JobConfNotSetError::operator=(const JobConfNotSetError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobConfNotSetError> JobConfNotSetError::__SharedMutable__() {
  return ::std::make_shared<JobConfNotSetError>(__SharedPtr__());
}
ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::_JobConfRepeatedSetError_() { Clear(); }
ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::_JobConfRepeatedSetError_(const _JobConfRepeatedSetError_& other) { CopyFrom(other); }
ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::_JobConfRepeatedSetError_(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror) {
  InitFromProto(proto_jobconfrepeatedseterror);
}
ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::_JobConfRepeatedSetError_(_JobConfRepeatedSetError_&& other) = default;
ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::~_JobConfRepeatedSetError_() = default;

void ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::InitFromProto(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror) {
  Clear();
    
}

void ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::ToProto(::oneflow::JobConfRepeatedSetError* proto_jobconfrepeatedseterror) const {
  proto_jobconfrepeatedseterror->Clear();

}

::std::string ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::DebugString() const {
  ::oneflow::JobConfRepeatedSetError proto_jobconfrepeatedseterror;
  this->ToProto(&proto_jobconfrepeatedseterror);
  return proto_jobconfrepeatedseterror.DebugString();
}

void ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::Clear() {
}

void ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::CopyFrom(const _JobConfRepeatedSetError_& other) {
}



int ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::compare(const _JobConfRepeatedSetError_& other) {
  return 0;
}

bool ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::operator==(const _JobConfRepeatedSetError_& other) const {
  return true
  ;
}

std::size_t ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_::operator<(const _JobConfRepeatedSetError_& other) const {
  return false
  ;
}

using _JobConfRepeatedSetError_ =  ConstJobConfRepeatedSetError::_JobConfRepeatedSetError_;
ConstJobConfRepeatedSetError::ConstJobConfRepeatedSetError(const ::std::shared_ptr<_JobConfRepeatedSetError_>& data): data_(data) {}
ConstJobConfRepeatedSetError::ConstJobConfRepeatedSetError(): data_(::std::make_shared<_JobConfRepeatedSetError_>()) {}
ConstJobConfRepeatedSetError::ConstJobConfRepeatedSetError(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror) {
  BuildFromProto(proto_jobconfrepeatedseterror);
}
ConstJobConfRepeatedSetError::ConstJobConfRepeatedSetError(const ConstJobConfRepeatedSetError&) = default;
ConstJobConfRepeatedSetError::ConstJobConfRepeatedSetError(ConstJobConfRepeatedSetError&&) noexcept = default;
ConstJobConfRepeatedSetError::~ConstJobConfRepeatedSetError() = default;

void ConstJobConfRepeatedSetError::ToProto(PbMessage* proto_jobconfrepeatedseterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobConfRepeatedSetError*>(proto_jobconfrepeatedseterror));
}
  
::std::string ConstJobConfRepeatedSetError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobConfRepeatedSetError::__Empty__() const {
  return !data_;
}

int ConstJobConfRepeatedSetError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobConfRepeatedSetError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobConfRepeatedSetError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobConfRepeatedSetError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobConfRepeatedSetError> ConstJobConfRepeatedSetError::__SharedConst__() const {
  return ::std::make_shared<ConstJobConfRepeatedSetError>(data_);
}
int64_t ConstJobConfRepeatedSetError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobConfRepeatedSetError::operator==(const ConstJobConfRepeatedSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobConfRepeatedSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobConfRepeatedSetError::operator<(const ConstJobConfRepeatedSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobConfRepeatedSetError_>& ConstJobConfRepeatedSetError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobConfRepeatedSetError_> default_ptr = std::make_shared<_JobConfRepeatedSetError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobConfRepeatedSetError_>& ConstJobConfRepeatedSetError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobConfRepeatedSetError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobConfRepeatedSetError
void ConstJobConfRepeatedSetError::BuildFromProto(const PbMessage& proto_jobconfrepeatedseterror) {
  data_ = ::std::make_shared<_JobConfRepeatedSetError_>(dynamic_cast<const ::oneflow::JobConfRepeatedSetError&>(proto_jobconfrepeatedseterror));
}

JobConfRepeatedSetError::JobConfRepeatedSetError(const ::std::shared_ptr<_JobConfRepeatedSetError_>& data)
  : ConstJobConfRepeatedSetError(data) {}
JobConfRepeatedSetError::JobConfRepeatedSetError(const JobConfRepeatedSetError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobConfRepeatedSetError> resize
JobConfRepeatedSetError::JobConfRepeatedSetError(JobConfRepeatedSetError&&) noexcept = default; 
JobConfRepeatedSetError::JobConfRepeatedSetError(const ::oneflow::JobConfRepeatedSetError& proto_jobconfrepeatedseterror) {
  InitFromProto(proto_jobconfrepeatedseterror);
}
JobConfRepeatedSetError::JobConfRepeatedSetError() = default;

JobConfRepeatedSetError::~JobConfRepeatedSetError() = default;

void JobConfRepeatedSetError::InitFromProto(const PbMessage& proto_jobconfrepeatedseterror) {
  BuildFromProto(proto_jobconfrepeatedseterror);
}
  
void* JobConfRepeatedSetError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobConfRepeatedSetError::operator==(const JobConfRepeatedSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobConfRepeatedSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobConfRepeatedSetError::operator<(const JobConfRepeatedSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobConfRepeatedSetError::Clear() {
  if (data_) { data_.reset(); }
}
void JobConfRepeatedSetError::CopyFrom(const JobConfRepeatedSetError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobConfRepeatedSetError& JobConfRepeatedSetError::operator=(const JobConfRepeatedSetError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobConfRepeatedSetError> JobConfRepeatedSetError::__SharedMutable__() {
  return ::std::make_shared<JobConfRepeatedSetError>(__SharedPtr__());
}
ConstJobTypeNotSetError::_JobTypeNotSetError_::_JobTypeNotSetError_() { Clear(); }
ConstJobTypeNotSetError::_JobTypeNotSetError_::_JobTypeNotSetError_(const _JobTypeNotSetError_& other) { CopyFrom(other); }
ConstJobTypeNotSetError::_JobTypeNotSetError_::_JobTypeNotSetError_(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror) {
  InitFromProto(proto_jobtypenotseterror);
}
ConstJobTypeNotSetError::_JobTypeNotSetError_::_JobTypeNotSetError_(_JobTypeNotSetError_&& other) = default;
ConstJobTypeNotSetError::_JobTypeNotSetError_::~_JobTypeNotSetError_() = default;

void ConstJobTypeNotSetError::_JobTypeNotSetError_::InitFromProto(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror) {
  Clear();
    
}

void ConstJobTypeNotSetError::_JobTypeNotSetError_::ToProto(::oneflow::JobTypeNotSetError* proto_jobtypenotseterror) const {
  proto_jobtypenotseterror->Clear();

}

::std::string ConstJobTypeNotSetError::_JobTypeNotSetError_::DebugString() const {
  ::oneflow::JobTypeNotSetError proto_jobtypenotseterror;
  this->ToProto(&proto_jobtypenotseterror);
  return proto_jobtypenotseterror.DebugString();
}

void ConstJobTypeNotSetError::_JobTypeNotSetError_::Clear() {
}

void ConstJobTypeNotSetError::_JobTypeNotSetError_::CopyFrom(const _JobTypeNotSetError_& other) {
}



int ConstJobTypeNotSetError::_JobTypeNotSetError_::compare(const _JobTypeNotSetError_& other) {
  return 0;
}

bool ConstJobTypeNotSetError::_JobTypeNotSetError_::operator==(const _JobTypeNotSetError_& other) const {
  return true
  ;
}

std::size_t ConstJobTypeNotSetError::_JobTypeNotSetError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstJobTypeNotSetError::_JobTypeNotSetError_::operator<(const _JobTypeNotSetError_& other) const {
  return false
  ;
}

using _JobTypeNotSetError_ =  ConstJobTypeNotSetError::_JobTypeNotSetError_;
ConstJobTypeNotSetError::ConstJobTypeNotSetError(const ::std::shared_ptr<_JobTypeNotSetError_>& data): data_(data) {}
ConstJobTypeNotSetError::ConstJobTypeNotSetError(): data_(::std::make_shared<_JobTypeNotSetError_>()) {}
ConstJobTypeNotSetError::ConstJobTypeNotSetError(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror) {
  BuildFromProto(proto_jobtypenotseterror);
}
ConstJobTypeNotSetError::ConstJobTypeNotSetError(const ConstJobTypeNotSetError&) = default;
ConstJobTypeNotSetError::ConstJobTypeNotSetError(ConstJobTypeNotSetError&&) noexcept = default;
ConstJobTypeNotSetError::~ConstJobTypeNotSetError() = default;

void ConstJobTypeNotSetError::ToProto(PbMessage* proto_jobtypenotseterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobTypeNotSetError*>(proto_jobtypenotseterror));
}
  
::std::string ConstJobTypeNotSetError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobTypeNotSetError::__Empty__() const {
  return !data_;
}

int ConstJobTypeNotSetError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstJobTypeNotSetError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstJobTypeNotSetError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobTypeNotSetError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstJobTypeNotSetError> ConstJobTypeNotSetError::__SharedConst__() const {
  return ::std::make_shared<ConstJobTypeNotSetError>(data_);
}
int64_t ConstJobTypeNotSetError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobTypeNotSetError::operator==(const ConstJobTypeNotSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobTypeNotSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobTypeNotSetError::operator<(const ConstJobTypeNotSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobTypeNotSetError_>& ConstJobTypeNotSetError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobTypeNotSetError_> default_ptr = std::make_shared<_JobTypeNotSetError_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobTypeNotSetError_>& ConstJobTypeNotSetError::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobTypeNotSetError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobTypeNotSetError
void ConstJobTypeNotSetError::BuildFromProto(const PbMessage& proto_jobtypenotseterror) {
  data_ = ::std::make_shared<_JobTypeNotSetError_>(dynamic_cast<const ::oneflow::JobTypeNotSetError&>(proto_jobtypenotseterror));
}

JobTypeNotSetError::JobTypeNotSetError(const ::std::shared_ptr<_JobTypeNotSetError_>& data)
  : ConstJobTypeNotSetError(data) {}
JobTypeNotSetError::JobTypeNotSetError(const JobTypeNotSetError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobTypeNotSetError> resize
JobTypeNotSetError::JobTypeNotSetError(JobTypeNotSetError&&) noexcept = default; 
JobTypeNotSetError::JobTypeNotSetError(const ::oneflow::JobTypeNotSetError& proto_jobtypenotseterror) {
  InitFromProto(proto_jobtypenotseterror);
}
JobTypeNotSetError::JobTypeNotSetError() = default;

JobTypeNotSetError::~JobTypeNotSetError() = default;

void JobTypeNotSetError::InitFromProto(const PbMessage& proto_jobtypenotseterror) {
  BuildFromProto(proto_jobtypenotseterror);
}
  
void* JobTypeNotSetError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool JobTypeNotSetError::operator==(const JobTypeNotSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobTypeNotSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobTypeNotSetError::operator<(const JobTypeNotSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobTypeNotSetError::Clear() {
  if (data_) { data_.reset(); }
}
void JobTypeNotSetError::CopyFrom(const JobTypeNotSetError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobTypeNotSetError& JobTypeNotSetError::operator=(const JobTypeNotSetError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<JobTypeNotSetError> JobTypeNotSetError::__SharedMutable__() {
  return ::std::make_shared<JobTypeNotSetError>(__SharedPtr__());
}
ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::_LogicalBlobNameNotExistError_() { Clear(); }
ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::_LogicalBlobNameNotExistError_(const _LogicalBlobNameNotExistError_& other) { CopyFrom(other); }
ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::_LogicalBlobNameNotExistError_(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror) {
  InitFromProto(proto_logicalblobnamenotexisterror);
}
ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::_LogicalBlobNameNotExistError_(_LogicalBlobNameNotExistError_&& other) = default;
ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::~_LogicalBlobNameNotExistError_() = default;

void ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::InitFromProto(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror) {
  Clear();
    
}

void ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::ToProto(::oneflow::LogicalBlobNameNotExistError* proto_logicalblobnamenotexisterror) const {
  proto_logicalblobnamenotexisterror->Clear();

}

::std::string ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::DebugString() const {
  ::oneflow::LogicalBlobNameNotExistError proto_logicalblobnamenotexisterror;
  this->ToProto(&proto_logicalblobnamenotexisterror);
  return proto_logicalblobnamenotexisterror.DebugString();
}

void ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::Clear() {
}

void ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::CopyFrom(const _LogicalBlobNameNotExistError_& other) {
}



int ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::compare(const _LogicalBlobNameNotExistError_& other) {
  return 0;
}

bool ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::operator==(const _LogicalBlobNameNotExistError_& other) const {
  return true
  ;
}

std::size_t ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_::operator<(const _LogicalBlobNameNotExistError_& other) const {
  return false
  ;
}

using _LogicalBlobNameNotExistError_ =  ConstLogicalBlobNameNotExistError::_LogicalBlobNameNotExistError_;
ConstLogicalBlobNameNotExistError::ConstLogicalBlobNameNotExistError(const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& data): data_(data) {}
ConstLogicalBlobNameNotExistError::ConstLogicalBlobNameNotExistError(): data_(::std::make_shared<_LogicalBlobNameNotExistError_>()) {}
ConstLogicalBlobNameNotExistError::ConstLogicalBlobNameNotExistError(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror) {
  BuildFromProto(proto_logicalblobnamenotexisterror);
}
ConstLogicalBlobNameNotExistError::ConstLogicalBlobNameNotExistError(const ConstLogicalBlobNameNotExistError&) = default;
ConstLogicalBlobNameNotExistError::ConstLogicalBlobNameNotExistError(ConstLogicalBlobNameNotExistError&&) noexcept = default;
ConstLogicalBlobNameNotExistError::~ConstLogicalBlobNameNotExistError() = default;

void ConstLogicalBlobNameNotExistError::ToProto(PbMessage* proto_logicalblobnamenotexisterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobNameNotExistError*>(proto_logicalblobnamenotexisterror));
}
  
::std::string ConstLogicalBlobNameNotExistError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobNameNotExistError::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobNameNotExistError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstLogicalBlobNameNotExistError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstLogicalBlobNameNotExistError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobNameNotExistError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstLogicalBlobNameNotExistError> ConstLogicalBlobNameNotExistError::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobNameNotExistError>(data_);
}
int64_t ConstLogicalBlobNameNotExistError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobNameNotExistError::operator==(const ConstLogicalBlobNameNotExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobNameNotExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobNameNotExistError::operator<(const ConstLogicalBlobNameNotExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& ConstLogicalBlobNameNotExistError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobNameNotExistError_> default_ptr = std::make_shared<_LogicalBlobNameNotExistError_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& ConstLogicalBlobNameNotExistError::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobNameNotExistError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameNotExistError
void ConstLogicalBlobNameNotExistError::BuildFromProto(const PbMessage& proto_logicalblobnamenotexisterror) {
  data_ = ::std::make_shared<_LogicalBlobNameNotExistError_>(dynamic_cast<const ::oneflow::LogicalBlobNameNotExistError&>(proto_logicalblobnamenotexisterror));
}

LogicalBlobNameNotExistError::LogicalBlobNameNotExistError(const ::std::shared_ptr<_LogicalBlobNameNotExistError_>& data)
  : ConstLogicalBlobNameNotExistError(data) {}
LogicalBlobNameNotExistError::LogicalBlobNameNotExistError(const LogicalBlobNameNotExistError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobNameNotExistError> resize
LogicalBlobNameNotExistError::LogicalBlobNameNotExistError(LogicalBlobNameNotExistError&&) noexcept = default; 
LogicalBlobNameNotExistError::LogicalBlobNameNotExistError(const ::oneflow::LogicalBlobNameNotExistError& proto_logicalblobnamenotexisterror) {
  InitFromProto(proto_logicalblobnamenotexisterror);
}
LogicalBlobNameNotExistError::LogicalBlobNameNotExistError() = default;

LogicalBlobNameNotExistError::~LogicalBlobNameNotExistError() = default;

void LogicalBlobNameNotExistError::InitFromProto(const PbMessage& proto_logicalblobnamenotexisterror) {
  BuildFromProto(proto_logicalblobnamenotexisterror);
}
  
void* LogicalBlobNameNotExistError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool LogicalBlobNameNotExistError::operator==(const LogicalBlobNameNotExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobNameNotExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobNameNotExistError::operator<(const LogicalBlobNameNotExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobNameNotExistError::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobNameNotExistError::CopyFrom(const LogicalBlobNameNotExistError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobNameNotExistError& LogicalBlobNameNotExistError::operator=(const LogicalBlobNameNotExistError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<LogicalBlobNameNotExistError> LogicalBlobNameNotExistError::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobNameNotExistError>(__SharedPtr__());
}
ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::_LogicalBlobNameExistError_() { Clear(); }
ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::_LogicalBlobNameExistError_(const _LogicalBlobNameExistError_& other) { CopyFrom(other); }
ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::_LogicalBlobNameExistError_(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror) {
  InitFromProto(proto_logicalblobnameexisterror);
}
ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::_LogicalBlobNameExistError_(_LogicalBlobNameExistError_&& other) = default;
ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::~_LogicalBlobNameExistError_() = default;

void ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::InitFromProto(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror) {
  Clear();
    
}

void ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::ToProto(::oneflow::LogicalBlobNameExistError* proto_logicalblobnameexisterror) const {
  proto_logicalblobnameexisterror->Clear();

}

::std::string ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::DebugString() const {
  ::oneflow::LogicalBlobNameExistError proto_logicalblobnameexisterror;
  this->ToProto(&proto_logicalblobnameexisterror);
  return proto_logicalblobnameexisterror.DebugString();
}

void ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::Clear() {
}

void ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::CopyFrom(const _LogicalBlobNameExistError_& other) {
}



int ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::compare(const _LogicalBlobNameExistError_& other) {
  return 0;
}

bool ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::operator==(const _LogicalBlobNameExistError_& other) const {
  return true
  ;
}

std::size_t ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_::operator<(const _LogicalBlobNameExistError_& other) const {
  return false
  ;
}

using _LogicalBlobNameExistError_ =  ConstLogicalBlobNameExistError::_LogicalBlobNameExistError_;
ConstLogicalBlobNameExistError::ConstLogicalBlobNameExistError(const ::std::shared_ptr<_LogicalBlobNameExistError_>& data): data_(data) {}
ConstLogicalBlobNameExistError::ConstLogicalBlobNameExistError(): data_(::std::make_shared<_LogicalBlobNameExistError_>()) {}
ConstLogicalBlobNameExistError::ConstLogicalBlobNameExistError(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror) {
  BuildFromProto(proto_logicalblobnameexisterror);
}
ConstLogicalBlobNameExistError::ConstLogicalBlobNameExistError(const ConstLogicalBlobNameExistError&) = default;
ConstLogicalBlobNameExistError::ConstLogicalBlobNameExistError(ConstLogicalBlobNameExistError&&) noexcept = default;
ConstLogicalBlobNameExistError::~ConstLogicalBlobNameExistError() = default;

void ConstLogicalBlobNameExistError::ToProto(PbMessage* proto_logicalblobnameexisterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobNameExistError*>(proto_logicalblobnameexisterror));
}
  
::std::string ConstLogicalBlobNameExistError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobNameExistError::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobNameExistError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstLogicalBlobNameExistError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstLogicalBlobNameExistError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobNameExistError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstLogicalBlobNameExistError> ConstLogicalBlobNameExistError::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobNameExistError>(data_);
}
int64_t ConstLogicalBlobNameExistError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobNameExistError::operator==(const ConstLogicalBlobNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobNameExistError::operator<(const ConstLogicalBlobNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobNameExistError_>& ConstLogicalBlobNameExistError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobNameExistError_> default_ptr = std::make_shared<_LogicalBlobNameExistError_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobNameExistError_>& ConstLogicalBlobNameExistError::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobNameExistError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameExistError
void ConstLogicalBlobNameExistError::BuildFromProto(const PbMessage& proto_logicalblobnameexisterror) {
  data_ = ::std::make_shared<_LogicalBlobNameExistError_>(dynamic_cast<const ::oneflow::LogicalBlobNameExistError&>(proto_logicalblobnameexisterror));
}

LogicalBlobNameExistError::LogicalBlobNameExistError(const ::std::shared_ptr<_LogicalBlobNameExistError_>& data)
  : ConstLogicalBlobNameExistError(data) {}
LogicalBlobNameExistError::LogicalBlobNameExistError(const LogicalBlobNameExistError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobNameExistError> resize
LogicalBlobNameExistError::LogicalBlobNameExistError(LogicalBlobNameExistError&&) noexcept = default; 
LogicalBlobNameExistError::LogicalBlobNameExistError(const ::oneflow::LogicalBlobNameExistError& proto_logicalblobnameexisterror) {
  InitFromProto(proto_logicalblobnameexisterror);
}
LogicalBlobNameExistError::LogicalBlobNameExistError() = default;

LogicalBlobNameExistError::~LogicalBlobNameExistError() = default;

void LogicalBlobNameExistError::InitFromProto(const PbMessage& proto_logicalblobnameexisterror) {
  BuildFromProto(proto_logicalblobnameexisterror);
}
  
void* LogicalBlobNameExistError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool LogicalBlobNameExistError::operator==(const LogicalBlobNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobNameExistError::operator<(const LogicalBlobNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobNameExistError::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobNameExistError::CopyFrom(const LogicalBlobNameExistError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobNameExistError& LogicalBlobNameExistError::operator=(const LogicalBlobNameExistError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<LogicalBlobNameExistError> LogicalBlobNameExistError::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobNameExistError>(__SharedPtr__());
}
ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::_LogicalBlobNameInvalidError_() { Clear(); }
ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::_LogicalBlobNameInvalidError_(const _LogicalBlobNameInvalidError_& other) { CopyFrom(other); }
ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::_LogicalBlobNameInvalidError_(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror) {
  InitFromProto(proto_logicalblobnameinvaliderror);
}
ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::_LogicalBlobNameInvalidError_(_LogicalBlobNameInvalidError_&& other) = default;
ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::~_LogicalBlobNameInvalidError_() = default;

void ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::InitFromProto(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror) {
  Clear();
    
}

void ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::ToProto(::oneflow::LogicalBlobNameInvalidError* proto_logicalblobnameinvaliderror) const {
  proto_logicalblobnameinvaliderror->Clear();

}

::std::string ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::DebugString() const {
  ::oneflow::LogicalBlobNameInvalidError proto_logicalblobnameinvaliderror;
  this->ToProto(&proto_logicalblobnameinvaliderror);
  return proto_logicalblobnameinvaliderror.DebugString();
}

void ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::Clear() {
}

void ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::CopyFrom(const _LogicalBlobNameInvalidError_& other) {
}



int ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::compare(const _LogicalBlobNameInvalidError_& other) {
  return 0;
}

bool ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::operator==(const _LogicalBlobNameInvalidError_& other) const {
  return true
  ;
}

std::size_t ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_::operator<(const _LogicalBlobNameInvalidError_& other) const {
  return false
  ;
}

using _LogicalBlobNameInvalidError_ =  ConstLogicalBlobNameInvalidError::_LogicalBlobNameInvalidError_;
ConstLogicalBlobNameInvalidError::ConstLogicalBlobNameInvalidError(const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& data): data_(data) {}
ConstLogicalBlobNameInvalidError::ConstLogicalBlobNameInvalidError(): data_(::std::make_shared<_LogicalBlobNameInvalidError_>()) {}
ConstLogicalBlobNameInvalidError::ConstLogicalBlobNameInvalidError(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror) {
  BuildFromProto(proto_logicalblobnameinvaliderror);
}
ConstLogicalBlobNameInvalidError::ConstLogicalBlobNameInvalidError(const ConstLogicalBlobNameInvalidError&) = default;
ConstLogicalBlobNameInvalidError::ConstLogicalBlobNameInvalidError(ConstLogicalBlobNameInvalidError&&) noexcept = default;
ConstLogicalBlobNameInvalidError::~ConstLogicalBlobNameInvalidError() = default;

void ConstLogicalBlobNameInvalidError::ToProto(PbMessage* proto_logicalblobnameinvaliderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobNameInvalidError*>(proto_logicalblobnameinvaliderror));
}
  
::std::string ConstLogicalBlobNameInvalidError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobNameInvalidError::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobNameInvalidError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstLogicalBlobNameInvalidError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstLogicalBlobNameInvalidError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobNameInvalidError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstLogicalBlobNameInvalidError> ConstLogicalBlobNameInvalidError::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobNameInvalidError>(data_);
}
int64_t ConstLogicalBlobNameInvalidError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobNameInvalidError::operator==(const ConstLogicalBlobNameInvalidError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobNameInvalidError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobNameInvalidError::operator<(const ConstLogicalBlobNameInvalidError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& ConstLogicalBlobNameInvalidError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobNameInvalidError_> default_ptr = std::make_shared<_LogicalBlobNameInvalidError_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& ConstLogicalBlobNameInvalidError::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobNameInvalidError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobNameInvalidError
void ConstLogicalBlobNameInvalidError::BuildFromProto(const PbMessage& proto_logicalblobnameinvaliderror) {
  data_ = ::std::make_shared<_LogicalBlobNameInvalidError_>(dynamic_cast<const ::oneflow::LogicalBlobNameInvalidError&>(proto_logicalblobnameinvaliderror));
}

LogicalBlobNameInvalidError::LogicalBlobNameInvalidError(const ::std::shared_ptr<_LogicalBlobNameInvalidError_>& data)
  : ConstLogicalBlobNameInvalidError(data) {}
LogicalBlobNameInvalidError::LogicalBlobNameInvalidError(const LogicalBlobNameInvalidError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobNameInvalidError> resize
LogicalBlobNameInvalidError::LogicalBlobNameInvalidError(LogicalBlobNameInvalidError&&) noexcept = default; 
LogicalBlobNameInvalidError::LogicalBlobNameInvalidError(const ::oneflow::LogicalBlobNameInvalidError& proto_logicalblobnameinvaliderror) {
  InitFromProto(proto_logicalblobnameinvaliderror);
}
LogicalBlobNameInvalidError::LogicalBlobNameInvalidError() = default;

LogicalBlobNameInvalidError::~LogicalBlobNameInvalidError() = default;

void LogicalBlobNameInvalidError::InitFromProto(const PbMessage& proto_logicalblobnameinvaliderror) {
  BuildFromProto(proto_logicalblobnameinvaliderror);
}
  
void* LogicalBlobNameInvalidError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool LogicalBlobNameInvalidError::operator==(const LogicalBlobNameInvalidError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobNameInvalidError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobNameInvalidError::operator<(const LogicalBlobNameInvalidError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobNameInvalidError::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobNameInvalidError::CopyFrom(const LogicalBlobNameInvalidError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobNameInvalidError& LogicalBlobNameInvalidError::operator=(const LogicalBlobNameInvalidError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<LogicalBlobNameInvalidError> LogicalBlobNameInvalidError::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobNameInvalidError>(__SharedPtr__());
}
ConstOpNameExistError::_OpNameExistError_::_OpNameExistError_() { Clear(); }
ConstOpNameExistError::_OpNameExistError_::_OpNameExistError_(const _OpNameExistError_& other) { CopyFrom(other); }
ConstOpNameExistError::_OpNameExistError_::_OpNameExistError_(const ::oneflow::OpNameExistError& proto_opnameexisterror) {
  InitFromProto(proto_opnameexisterror);
}
ConstOpNameExistError::_OpNameExistError_::_OpNameExistError_(_OpNameExistError_&& other) = default;
ConstOpNameExistError::_OpNameExistError_::~_OpNameExistError_() = default;

void ConstOpNameExistError::_OpNameExistError_::InitFromProto(const ::oneflow::OpNameExistError& proto_opnameexisterror) {
  Clear();
    
}

void ConstOpNameExistError::_OpNameExistError_::ToProto(::oneflow::OpNameExistError* proto_opnameexisterror) const {
  proto_opnameexisterror->Clear();

}

::std::string ConstOpNameExistError::_OpNameExistError_::DebugString() const {
  ::oneflow::OpNameExistError proto_opnameexisterror;
  this->ToProto(&proto_opnameexisterror);
  return proto_opnameexisterror.DebugString();
}

void ConstOpNameExistError::_OpNameExistError_::Clear() {
}

void ConstOpNameExistError::_OpNameExistError_::CopyFrom(const _OpNameExistError_& other) {
}



int ConstOpNameExistError::_OpNameExistError_::compare(const _OpNameExistError_& other) {
  return 0;
}

bool ConstOpNameExistError::_OpNameExistError_::operator==(const _OpNameExistError_& other) const {
  return true
  ;
}

std::size_t ConstOpNameExistError::_OpNameExistError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstOpNameExistError::_OpNameExistError_::operator<(const _OpNameExistError_& other) const {
  return false
  ;
}

using _OpNameExistError_ =  ConstOpNameExistError::_OpNameExistError_;
ConstOpNameExistError::ConstOpNameExistError(const ::std::shared_ptr<_OpNameExistError_>& data): data_(data) {}
ConstOpNameExistError::ConstOpNameExistError(): data_(::std::make_shared<_OpNameExistError_>()) {}
ConstOpNameExistError::ConstOpNameExistError(const ::oneflow::OpNameExistError& proto_opnameexisterror) {
  BuildFromProto(proto_opnameexisterror);
}
ConstOpNameExistError::ConstOpNameExistError(const ConstOpNameExistError&) = default;
ConstOpNameExistError::ConstOpNameExistError(ConstOpNameExistError&&) noexcept = default;
ConstOpNameExistError::~ConstOpNameExistError() = default;

void ConstOpNameExistError::ToProto(PbMessage* proto_opnameexisterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpNameExistError*>(proto_opnameexisterror));
}
  
::std::string ConstOpNameExistError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpNameExistError::__Empty__() const {
  return !data_;
}

int ConstOpNameExistError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstOpNameExistError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstOpNameExistError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpNameExistError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstOpNameExistError> ConstOpNameExistError::__SharedConst__() const {
  return ::std::make_shared<ConstOpNameExistError>(data_);
}
int64_t ConstOpNameExistError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpNameExistError::operator==(const ConstOpNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpNameExistError::operator<(const ConstOpNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpNameExistError_>& ConstOpNameExistError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpNameExistError_> default_ptr = std::make_shared<_OpNameExistError_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpNameExistError_>& ConstOpNameExistError::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpNameExistError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpNameExistError
void ConstOpNameExistError::BuildFromProto(const PbMessage& proto_opnameexisterror) {
  data_ = ::std::make_shared<_OpNameExistError_>(dynamic_cast<const ::oneflow::OpNameExistError&>(proto_opnameexisterror));
}

OpNameExistError::OpNameExistError(const ::std::shared_ptr<_OpNameExistError_>& data)
  : ConstOpNameExistError(data) {}
OpNameExistError::OpNameExistError(const OpNameExistError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpNameExistError> resize
OpNameExistError::OpNameExistError(OpNameExistError&&) noexcept = default; 
OpNameExistError::OpNameExistError(const ::oneflow::OpNameExistError& proto_opnameexisterror) {
  InitFromProto(proto_opnameexisterror);
}
OpNameExistError::OpNameExistError() = default;

OpNameExistError::~OpNameExistError() = default;

void OpNameExistError::InitFromProto(const PbMessage& proto_opnameexisterror) {
  BuildFromProto(proto_opnameexisterror);
}
  
void* OpNameExistError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool OpNameExistError::operator==(const OpNameExistError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpNameExistError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpNameExistError::operator<(const OpNameExistError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpNameExistError::Clear() {
  if (data_) { data_.reset(); }
}
void OpNameExistError::CopyFrom(const OpNameExistError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpNameExistError& OpNameExistError::operator=(const OpNameExistError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<OpNameExistError> OpNameExistError::__SharedMutable__() {
  return ::std::make_shared<OpNameExistError>(__SharedPtr__());
}
ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::_OpConfDeviceTagNoSetError_() { Clear(); }
ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::_OpConfDeviceTagNoSetError_(const _OpConfDeviceTagNoSetError_& other) { CopyFrom(other); }
ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::_OpConfDeviceTagNoSetError_(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror) {
  InitFromProto(proto_opconfdevicetagnoseterror);
}
ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::_OpConfDeviceTagNoSetError_(_OpConfDeviceTagNoSetError_&& other) = default;
ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::~_OpConfDeviceTagNoSetError_() = default;

void ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::InitFromProto(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror) {
  Clear();
    
}

void ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::ToProto(::oneflow::OpConfDeviceTagNoSetError* proto_opconfdevicetagnoseterror) const {
  proto_opconfdevicetagnoseterror->Clear();

}

::std::string ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::DebugString() const {
  ::oneflow::OpConfDeviceTagNoSetError proto_opconfdevicetagnoseterror;
  this->ToProto(&proto_opconfdevicetagnoseterror);
  return proto_opconfdevicetagnoseterror.DebugString();
}

void ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::Clear() {
}

void ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::CopyFrom(const _OpConfDeviceTagNoSetError_& other) {
}



int ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::compare(const _OpConfDeviceTagNoSetError_& other) {
  return 0;
}

bool ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::operator==(const _OpConfDeviceTagNoSetError_& other) const {
  return true
  ;
}

std::size_t ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_::operator<(const _OpConfDeviceTagNoSetError_& other) const {
  return false
  ;
}

using _OpConfDeviceTagNoSetError_ =  ConstOpConfDeviceTagNoSetError::_OpConfDeviceTagNoSetError_;
ConstOpConfDeviceTagNoSetError::ConstOpConfDeviceTagNoSetError(const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& data): data_(data) {}
ConstOpConfDeviceTagNoSetError::ConstOpConfDeviceTagNoSetError(): data_(::std::make_shared<_OpConfDeviceTagNoSetError_>()) {}
ConstOpConfDeviceTagNoSetError::ConstOpConfDeviceTagNoSetError(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror) {
  BuildFromProto(proto_opconfdevicetagnoseterror);
}
ConstOpConfDeviceTagNoSetError::ConstOpConfDeviceTagNoSetError(const ConstOpConfDeviceTagNoSetError&) = default;
ConstOpConfDeviceTagNoSetError::ConstOpConfDeviceTagNoSetError(ConstOpConfDeviceTagNoSetError&&) noexcept = default;
ConstOpConfDeviceTagNoSetError::~ConstOpConfDeviceTagNoSetError() = default;

void ConstOpConfDeviceTagNoSetError::ToProto(PbMessage* proto_opconfdevicetagnoseterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpConfDeviceTagNoSetError*>(proto_opconfdevicetagnoseterror));
}
  
::std::string ConstOpConfDeviceTagNoSetError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpConfDeviceTagNoSetError::__Empty__() const {
  return !data_;
}

int ConstOpConfDeviceTagNoSetError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstOpConfDeviceTagNoSetError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstOpConfDeviceTagNoSetError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpConfDeviceTagNoSetError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstOpConfDeviceTagNoSetError> ConstOpConfDeviceTagNoSetError::__SharedConst__() const {
  return ::std::make_shared<ConstOpConfDeviceTagNoSetError>(data_);
}
int64_t ConstOpConfDeviceTagNoSetError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpConfDeviceTagNoSetError::operator==(const ConstOpConfDeviceTagNoSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpConfDeviceTagNoSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpConfDeviceTagNoSetError::operator<(const ConstOpConfDeviceTagNoSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& ConstOpConfDeviceTagNoSetError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpConfDeviceTagNoSetError_> default_ptr = std::make_shared<_OpConfDeviceTagNoSetError_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& ConstOpConfDeviceTagNoSetError::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpConfDeviceTagNoSetError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpConfDeviceTagNoSetError
void ConstOpConfDeviceTagNoSetError::BuildFromProto(const PbMessage& proto_opconfdevicetagnoseterror) {
  data_ = ::std::make_shared<_OpConfDeviceTagNoSetError_>(dynamic_cast<const ::oneflow::OpConfDeviceTagNoSetError&>(proto_opconfdevicetagnoseterror));
}

OpConfDeviceTagNoSetError::OpConfDeviceTagNoSetError(const ::std::shared_ptr<_OpConfDeviceTagNoSetError_>& data)
  : ConstOpConfDeviceTagNoSetError(data) {}
OpConfDeviceTagNoSetError::OpConfDeviceTagNoSetError(const OpConfDeviceTagNoSetError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpConfDeviceTagNoSetError> resize
OpConfDeviceTagNoSetError::OpConfDeviceTagNoSetError(OpConfDeviceTagNoSetError&&) noexcept = default; 
OpConfDeviceTagNoSetError::OpConfDeviceTagNoSetError(const ::oneflow::OpConfDeviceTagNoSetError& proto_opconfdevicetagnoseterror) {
  InitFromProto(proto_opconfdevicetagnoseterror);
}
OpConfDeviceTagNoSetError::OpConfDeviceTagNoSetError() = default;

OpConfDeviceTagNoSetError::~OpConfDeviceTagNoSetError() = default;

void OpConfDeviceTagNoSetError::InitFromProto(const PbMessage& proto_opconfdevicetagnoseterror) {
  BuildFromProto(proto_opconfdevicetagnoseterror);
}
  
void* OpConfDeviceTagNoSetError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool OpConfDeviceTagNoSetError::operator==(const OpConfDeviceTagNoSetError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpConfDeviceTagNoSetError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpConfDeviceTagNoSetError::operator<(const OpConfDeviceTagNoSetError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpConfDeviceTagNoSetError::Clear() {
  if (data_) { data_.reset(); }
}
void OpConfDeviceTagNoSetError::CopyFrom(const OpConfDeviceTagNoSetError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpConfDeviceTagNoSetError& OpConfDeviceTagNoSetError::operator=(const OpConfDeviceTagNoSetError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<OpConfDeviceTagNoSetError> OpConfDeviceTagNoSetError::__SharedMutable__() {
  return ::std::make_shared<OpConfDeviceTagNoSetError>(__SharedPtr__());
}
ConstPlacementError::_PlacementError_::_PlacementError_() { Clear(); }
ConstPlacementError::_PlacementError_::_PlacementError_(const _PlacementError_& other) { CopyFrom(other); }
ConstPlacementError::_PlacementError_::_PlacementError_(const ::oneflow::PlacementError& proto_placementerror) {
  InitFromProto(proto_placementerror);
}
ConstPlacementError::_PlacementError_::_PlacementError_(_PlacementError_&& other) = default;
ConstPlacementError::_PlacementError_::~_PlacementError_() = default;

void ConstPlacementError::_PlacementError_::InitFromProto(const ::oneflow::PlacementError& proto_placementerror) {
  Clear();
    
}

void ConstPlacementError::_PlacementError_::ToProto(::oneflow::PlacementError* proto_placementerror) const {
  proto_placementerror->Clear();

}

::std::string ConstPlacementError::_PlacementError_::DebugString() const {
  ::oneflow::PlacementError proto_placementerror;
  this->ToProto(&proto_placementerror);
  return proto_placementerror.DebugString();
}

void ConstPlacementError::_PlacementError_::Clear() {
}

void ConstPlacementError::_PlacementError_::CopyFrom(const _PlacementError_& other) {
}



int ConstPlacementError::_PlacementError_::compare(const _PlacementError_& other) {
  return 0;
}

bool ConstPlacementError::_PlacementError_::operator==(const _PlacementError_& other) const {
  return true
  ;
}

std::size_t ConstPlacementError::_PlacementError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstPlacementError::_PlacementError_::operator<(const _PlacementError_& other) const {
  return false
  ;
}

using _PlacementError_ =  ConstPlacementError::_PlacementError_;
ConstPlacementError::ConstPlacementError(const ::std::shared_ptr<_PlacementError_>& data): data_(data) {}
ConstPlacementError::ConstPlacementError(): data_(::std::make_shared<_PlacementError_>()) {}
ConstPlacementError::ConstPlacementError(const ::oneflow::PlacementError& proto_placementerror) {
  BuildFromProto(proto_placementerror);
}
ConstPlacementError::ConstPlacementError(const ConstPlacementError&) = default;
ConstPlacementError::ConstPlacementError(ConstPlacementError&&) noexcept = default;
ConstPlacementError::~ConstPlacementError() = default;

void ConstPlacementError::ToProto(PbMessage* proto_placementerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PlacementError*>(proto_placementerror));
}
  
::std::string ConstPlacementError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPlacementError::__Empty__() const {
  return !data_;
}

int ConstPlacementError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstPlacementError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstPlacementError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPlacementError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstPlacementError> ConstPlacementError::__SharedConst__() const {
  return ::std::make_shared<ConstPlacementError>(data_);
}
int64_t ConstPlacementError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPlacementError::operator==(const ConstPlacementError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPlacementError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPlacementError::operator<(const ConstPlacementError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PlacementError_>& ConstPlacementError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PlacementError_> default_ptr = std::make_shared<_PlacementError_>();
  return default_ptr;
}
const ::std::shared_ptr<_PlacementError_>& ConstPlacementError::__SharedPtr__() {
  if (!data_) { data_.reset(new _PlacementError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPlacementError
void ConstPlacementError::BuildFromProto(const PbMessage& proto_placementerror) {
  data_ = ::std::make_shared<_PlacementError_>(dynamic_cast<const ::oneflow::PlacementError&>(proto_placementerror));
}

PlacementError::PlacementError(const ::std::shared_ptr<_PlacementError_>& data)
  : ConstPlacementError(data) {}
PlacementError::PlacementError(const PlacementError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PlacementError> resize
PlacementError::PlacementError(PlacementError&&) noexcept = default; 
PlacementError::PlacementError(const ::oneflow::PlacementError& proto_placementerror) {
  InitFromProto(proto_placementerror);
}
PlacementError::PlacementError() = default;

PlacementError::~PlacementError() = default;

void PlacementError::InitFromProto(const PbMessage& proto_placementerror) {
  BuildFromProto(proto_placementerror);
}
  
void* PlacementError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool PlacementError::operator==(const PlacementError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PlacementError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PlacementError::operator<(const PlacementError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PlacementError::Clear() {
  if (data_) { data_.reset(); }
}
void PlacementError::CopyFrom(const PlacementError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PlacementError& PlacementError::operator=(const PlacementError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<PlacementError> PlacementError::__SharedMutable__() {
  return ::std::make_shared<PlacementError>(__SharedPtr__());
}
ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::_BlobSplitAxisInferError_() { Clear(); }
ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::_BlobSplitAxisInferError_(const _BlobSplitAxisInferError_& other) { CopyFrom(other); }
ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::_BlobSplitAxisInferError_(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror) {
  InitFromProto(proto_blobsplitaxisinfererror);
}
ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::_BlobSplitAxisInferError_(_BlobSplitAxisInferError_&& other) = default;
ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::~_BlobSplitAxisInferError_() = default;

void ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::InitFromProto(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror) {
  Clear();
    
}

void ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::ToProto(::oneflow::BlobSplitAxisInferError* proto_blobsplitaxisinfererror) const {
  proto_blobsplitaxisinfererror->Clear();

}

::std::string ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::DebugString() const {
  ::oneflow::BlobSplitAxisInferError proto_blobsplitaxisinfererror;
  this->ToProto(&proto_blobsplitaxisinfererror);
  return proto_blobsplitaxisinfererror.DebugString();
}

void ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::Clear() {
}

void ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::CopyFrom(const _BlobSplitAxisInferError_& other) {
}



int ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::compare(const _BlobSplitAxisInferError_& other) {
  return 0;
}

bool ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::operator==(const _BlobSplitAxisInferError_& other) const {
  return true
  ;
}

std::size_t ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_::operator<(const _BlobSplitAxisInferError_& other) const {
  return false
  ;
}

using _BlobSplitAxisInferError_ =  ConstBlobSplitAxisInferError::_BlobSplitAxisInferError_;
ConstBlobSplitAxisInferError::ConstBlobSplitAxisInferError(const ::std::shared_ptr<_BlobSplitAxisInferError_>& data): data_(data) {}
ConstBlobSplitAxisInferError::ConstBlobSplitAxisInferError(): data_(::std::make_shared<_BlobSplitAxisInferError_>()) {}
ConstBlobSplitAxisInferError::ConstBlobSplitAxisInferError(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror) {
  BuildFromProto(proto_blobsplitaxisinfererror);
}
ConstBlobSplitAxisInferError::ConstBlobSplitAxisInferError(const ConstBlobSplitAxisInferError&) = default;
ConstBlobSplitAxisInferError::ConstBlobSplitAxisInferError(ConstBlobSplitAxisInferError&&) noexcept = default;
ConstBlobSplitAxisInferError::~ConstBlobSplitAxisInferError() = default;

void ConstBlobSplitAxisInferError::ToProto(PbMessage* proto_blobsplitaxisinfererror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobSplitAxisInferError*>(proto_blobsplitaxisinfererror));
}
  
::std::string ConstBlobSplitAxisInferError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobSplitAxisInferError::__Empty__() const {
  return !data_;
}

int ConstBlobSplitAxisInferError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstBlobSplitAxisInferError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstBlobSplitAxisInferError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobSplitAxisInferError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstBlobSplitAxisInferError> ConstBlobSplitAxisInferError::__SharedConst__() const {
  return ::std::make_shared<ConstBlobSplitAxisInferError>(data_);
}
int64_t ConstBlobSplitAxisInferError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobSplitAxisInferError::operator==(const ConstBlobSplitAxisInferError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobSplitAxisInferError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobSplitAxisInferError::operator<(const ConstBlobSplitAxisInferError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobSplitAxisInferError_>& ConstBlobSplitAxisInferError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobSplitAxisInferError_> default_ptr = std::make_shared<_BlobSplitAxisInferError_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobSplitAxisInferError_>& ConstBlobSplitAxisInferError::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobSplitAxisInferError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobSplitAxisInferError
void ConstBlobSplitAxisInferError::BuildFromProto(const PbMessage& proto_blobsplitaxisinfererror) {
  data_ = ::std::make_shared<_BlobSplitAxisInferError_>(dynamic_cast<const ::oneflow::BlobSplitAxisInferError&>(proto_blobsplitaxisinfererror));
}

BlobSplitAxisInferError::BlobSplitAxisInferError(const ::std::shared_ptr<_BlobSplitAxisInferError_>& data)
  : ConstBlobSplitAxisInferError(data) {}
BlobSplitAxisInferError::BlobSplitAxisInferError(const BlobSplitAxisInferError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobSplitAxisInferError> resize
BlobSplitAxisInferError::BlobSplitAxisInferError(BlobSplitAxisInferError&&) noexcept = default; 
BlobSplitAxisInferError::BlobSplitAxisInferError(const ::oneflow::BlobSplitAxisInferError& proto_blobsplitaxisinfererror) {
  InitFromProto(proto_blobsplitaxisinfererror);
}
BlobSplitAxisInferError::BlobSplitAxisInferError() = default;

BlobSplitAxisInferError::~BlobSplitAxisInferError() = default;

void BlobSplitAxisInferError::InitFromProto(const PbMessage& proto_blobsplitaxisinfererror) {
  BuildFromProto(proto_blobsplitaxisinfererror);
}
  
void* BlobSplitAxisInferError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool BlobSplitAxisInferError::operator==(const BlobSplitAxisInferError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobSplitAxisInferError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobSplitAxisInferError::operator<(const BlobSplitAxisInferError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobSplitAxisInferError::Clear() {
  if (data_) { data_.reset(); }
}
void BlobSplitAxisInferError::CopyFrom(const BlobSplitAxisInferError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobSplitAxisInferError& BlobSplitAxisInferError::operator=(const BlobSplitAxisInferError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<BlobSplitAxisInferError> BlobSplitAxisInferError::__SharedMutable__() {
  return ::std::make_shared<BlobSplitAxisInferError>(__SharedPtr__());
}
ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::_UnknownJobBuildAndInferError_() { Clear(); }
ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::_UnknownJobBuildAndInferError_(const _UnknownJobBuildAndInferError_& other) { CopyFrom(other); }
ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::_UnknownJobBuildAndInferError_(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror) {
  InitFromProto(proto_unknownjobbuildandinfererror);
}
ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::_UnknownJobBuildAndInferError_(_UnknownJobBuildAndInferError_&& other) = default;
ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::~_UnknownJobBuildAndInferError_() = default;

void ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::InitFromProto(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror) {
  Clear();
    
}

void ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::ToProto(::oneflow::UnknownJobBuildAndInferError* proto_unknownjobbuildandinfererror) const {
  proto_unknownjobbuildandinfererror->Clear();

}

::std::string ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::DebugString() const {
  ::oneflow::UnknownJobBuildAndInferError proto_unknownjobbuildandinfererror;
  this->ToProto(&proto_unknownjobbuildandinfererror);
  return proto_unknownjobbuildandinfererror.DebugString();
}

void ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::Clear() {
}

void ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::CopyFrom(const _UnknownJobBuildAndInferError_& other) {
}



int ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::compare(const _UnknownJobBuildAndInferError_& other) {
  return 0;
}

bool ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::operator==(const _UnknownJobBuildAndInferError_& other) const {
  return true
  ;
}

std::size_t ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_::operator<(const _UnknownJobBuildAndInferError_& other) const {
  return false
  ;
}

using _UnknownJobBuildAndInferError_ =  ConstUnknownJobBuildAndInferError::_UnknownJobBuildAndInferError_;
ConstUnknownJobBuildAndInferError::ConstUnknownJobBuildAndInferError(const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& data): data_(data) {}
ConstUnknownJobBuildAndInferError::ConstUnknownJobBuildAndInferError(): data_(::std::make_shared<_UnknownJobBuildAndInferError_>()) {}
ConstUnknownJobBuildAndInferError::ConstUnknownJobBuildAndInferError(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror) {
  BuildFromProto(proto_unknownjobbuildandinfererror);
}
ConstUnknownJobBuildAndInferError::ConstUnknownJobBuildAndInferError(const ConstUnknownJobBuildAndInferError&) = default;
ConstUnknownJobBuildAndInferError::ConstUnknownJobBuildAndInferError(ConstUnknownJobBuildAndInferError&&) noexcept = default;
ConstUnknownJobBuildAndInferError::~ConstUnknownJobBuildAndInferError() = default;

void ConstUnknownJobBuildAndInferError::ToProto(PbMessage* proto_unknownjobbuildandinfererror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::UnknownJobBuildAndInferError*>(proto_unknownjobbuildandinfererror));
}
  
::std::string ConstUnknownJobBuildAndInferError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstUnknownJobBuildAndInferError::__Empty__() const {
  return !data_;
}

int ConstUnknownJobBuildAndInferError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstUnknownJobBuildAndInferError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstUnknownJobBuildAndInferError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstUnknownJobBuildAndInferError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstUnknownJobBuildAndInferError> ConstUnknownJobBuildAndInferError::__SharedConst__() const {
  return ::std::make_shared<ConstUnknownJobBuildAndInferError>(data_);
}
int64_t ConstUnknownJobBuildAndInferError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstUnknownJobBuildAndInferError::operator==(const ConstUnknownJobBuildAndInferError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstUnknownJobBuildAndInferError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstUnknownJobBuildAndInferError::operator<(const ConstUnknownJobBuildAndInferError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& ConstUnknownJobBuildAndInferError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_UnknownJobBuildAndInferError_> default_ptr = std::make_shared<_UnknownJobBuildAndInferError_>();
  return default_ptr;
}
const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& ConstUnknownJobBuildAndInferError::__SharedPtr__() {
  if (!data_) { data_.reset(new _UnknownJobBuildAndInferError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstUnknownJobBuildAndInferError
void ConstUnknownJobBuildAndInferError::BuildFromProto(const PbMessage& proto_unknownjobbuildandinfererror) {
  data_ = ::std::make_shared<_UnknownJobBuildAndInferError_>(dynamic_cast<const ::oneflow::UnknownJobBuildAndInferError&>(proto_unknownjobbuildandinfererror));
}

UnknownJobBuildAndInferError::UnknownJobBuildAndInferError(const ::std::shared_ptr<_UnknownJobBuildAndInferError_>& data)
  : ConstUnknownJobBuildAndInferError(data) {}
UnknownJobBuildAndInferError::UnknownJobBuildAndInferError(const UnknownJobBuildAndInferError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<UnknownJobBuildAndInferError> resize
UnknownJobBuildAndInferError::UnknownJobBuildAndInferError(UnknownJobBuildAndInferError&&) noexcept = default; 
UnknownJobBuildAndInferError::UnknownJobBuildAndInferError(const ::oneflow::UnknownJobBuildAndInferError& proto_unknownjobbuildandinfererror) {
  InitFromProto(proto_unknownjobbuildandinfererror);
}
UnknownJobBuildAndInferError::UnknownJobBuildAndInferError() = default;

UnknownJobBuildAndInferError::~UnknownJobBuildAndInferError() = default;

void UnknownJobBuildAndInferError::InitFromProto(const PbMessage& proto_unknownjobbuildandinfererror) {
  BuildFromProto(proto_unknownjobbuildandinfererror);
}
  
void* UnknownJobBuildAndInferError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool UnknownJobBuildAndInferError::operator==(const UnknownJobBuildAndInferError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t UnknownJobBuildAndInferError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool UnknownJobBuildAndInferError::operator<(const UnknownJobBuildAndInferError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void UnknownJobBuildAndInferError::Clear() {
  if (data_) { data_.reset(); }
}
void UnknownJobBuildAndInferError::CopyFrom(const UnknownJobBuildAndInferError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
UnknownJobBuildAndInferError& UnknownJobBuildAndInferError::operator=(const UnknownJobBuildAndInferError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<UnknownJobBuildAndInferError> UnknownJobBuildAndInferError::__SharedMutable__() {
  return ::std::make_shared<UnknownJobBuildAndInferError>(__SharedPtr__());
}
ConstProtoParseFailedError::_ProtoParseFailedError_::_ProtoParseFailedError_() { Clear(); }
ConstProtoParseFailedError::_ProtoParseFailedError_::_ProtoParseFailedError_(const _ProtoParseFailedError_& other) { CopyFrom(other); }
ConstProtoParseFailedError::_ProtoParseFailedError_::_ProtoParseFailedError_(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror) {
  InitFromProto(proto_protoparsefailederror);
}
ConstProtoParseFailedError::_ProtoParseFailedError_::_ProtoParseFailedError_(_ProtoParseFailedError_&& other) = default;
ConstProtoParseFailedError::_ProtoParseFailedError_::~_ProtoParseFailedError_() = default;

void ConstProtoParseFailedError::_ProtoParseFailedError_::InitFromProto(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror) {
  Clear();
    
}

void ConstProtoParseFailedError::_ProtoParseFailedError_::ToProto(::oneflow::ProtoParseFailedError* proto_protoparsefailederror) const {
  proto_protoparsefailederror->Clear();

}

::std::string ConstProtoParseFailedError::_ProtoParseFailedError_::DebugString() const {
  ::oneflow::ProtoParseFailedError proto_protoparsefailederror;
  this->ToProto(&proto_protoparsefailederror);
  return proto_protoparsefailederror.DebugString();
}

void ConstProtoParseFailedError::_ProtoParseFailedError_::Clear() {
}

void ConstProtoParseFailedError::_ProtoParseFailedError_::CopyFrom(const _ProtoParseFailedError_& other) {
}



int ConstProtoParseFailedError::_ProtoParseFailedError_::compare(const _ProtoParseFailedError_& other) {
  return 0;
}

bool ConstProtoParseFailedError::_ProtoParseFailedError_::operator==(const _ProtoParseFailedError_& other) const {
  return true
  ;
}

std::size_t ConstProtoParseFailedError::_ProtoParseFailedError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstProtoParseFailedError::_ProtoParseFailedError_::operator<(const _ProtoParseFailedError_& other) const {
  return false
  ;
}

using _ProtoParseFailedError_ =  ConstProtoParseFailedError::_ProtoParseFailedError_;
ConstProtoParseFailedError::ConstProtoParseFailedError(const ::std::shared_ptr<_ProtoParseFailedError_>& data): data_(data) {}
ConstProtoParseFailedError::ConstProtoParseFailedError(): data_(::std::make_shared<_ProtoParseFailedError_>()) {}
ConstProtoParseFailedError::ConstProtoParseFailedError(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror) {
  BuildFromProto(proto_protoparsefailederror);
}
ConstProtoParseFailedError::ConstProtoParseFailedError(const ConstProtoParseFailedError&) = default;
ConstProtoParseFailedError::ConstProtoParseFailedError(ConstProtoParseFailedError&&) noexcept = default;
ConstProtoParseFailedError::~ConstProtoParseFailedError() = default;

void ConstProtoParseFailedError::ToProto(PbMessage* proto_protoparsefailederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ProtoParseFailedError*>(proto_protoparsefailederror));
}
  
::std::string ConstProtoParseFailedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstProtoParseFailedError::__Empty__() const {
  return !data_;
}

int ConstProtoParseFailedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstProtoParseFailedError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstProtoParseFailedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstProtoParseFailedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstProtoParseFailedError> ConstProtoParseFailedError::__SharedConst__() const {
  return ::std::make_shared<ConstProtoParseFailedError>(data_);
}
int64_t ConstProtoParseFailedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstProtoParseFailedError::operator==(const ConstProtoParseFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstProtoParseFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstProtoParseFailedError::operator<(const ConstProtoParseFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ProtoParseFailedError_>& ConstProtoParseFailedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ProtoParseFailedError_> default_ptr = std::make_shared<_ProtoParseFailedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_ProtoParseFailedError_>& ConstProtoParseFailedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _ProtoParseFailedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstProtoParseFailedError
void ConstProtoParseFailedError::BuildFromProto(const PbMessage& proto_protoparsefailederror) {
  data_ = ::std::make_shared<_ProtoParseFailedError_>(dynamic_cast<const ::oneflow::ProtoParseFailedError&>(proto_protoparsefailederror));
}

ProtoParseFailedError::ProtoParseFailedError(const ::std::shared_ptr<_ProtoParseFailedError_>& data)
  : ConstProtoParseFailedError(data) {}
ProtoParseFailedError::ProtoParseFailedError(const ProtoParseFailedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ProtoParseFailedError> resize
ProtoParseFailedError::ProtoParseFailedError(ProtoParseFailedError&&) noexcept = default; 
ProtoParseFailedError::ProtoParseFailedError(const ::oneflow::ProtoParseFailedError& proto_protoparsefailederror) {
  InitFromProto(proto_protoparsefailederror);
}
ProtoParseFailedError::ProtoParseFailedError() = default;

ProtoParseFailedError::~ProtoParseFailedError() = default;

void ProtoParseFailedError::InitFromProto(const PbMessage& proto_protoparsefailederror) {
  BuildFromProto(proto_protoparsefailederror);
}
  
void* ProtoParseFailedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool ProtoParseFailedError::operator==(const ProtoParseFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ProtoParseFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ProtoParseFailedError::operator<(const ProtoParseFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ProtoParseFailedError::Clear() {
  if (data_) { data_.reset(); }
}
void ProtoParseFailedError::CopyFrom(const ProtoParseFailedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ProtoParseFailedError& ProtoParseFailedError::operator=(const ProtoParseFailedError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<ProtoParseFailedError> ProtoParseFailedError::__SharedMutable__() {
  return ::std::make_shared<ProtoParseFailedError>(__SharedPtr__());
}
ConstCheckFailedError::_CheckFailedError_::_CheckFailedError_() { Clear(); }
ConstCheckFailedError::_CheckFailedError_::_CheckFailedError_(const _CheckFailedError_& other) { CopyFrom(other); }
ConstCheckFailedError::_CheckFailedError_::_CheckFailedError_(const ::oneflow::CheckFailedError& proto_checkfailederror) {
  InitFromProto(proto_checkfailederror);
}
ConstCheckFailedError::_CheckFailedError_::_CheckFailedError_(_CheckFailedError_&& other) = default;
ConstCheckFailedError::_CheckFailedError_::~_CheckFailedError_() = default;

void ConstCheckFailedError::_CheckFailedError_::InitFromProto(const ::oneflow::CheckFailedError& proto_checkfailederror) {
  Clear();
    
}

void ConstCheckFailedError::_CheckFailedError_::ToProto(::oneflow::CheckFailedError* proto_checkfailederror) const {
  proto_checkfailederror->Clear();

}

::std::string ConstCheckFailedError::_CheckFailedError_::DebugString() const {
  ::oneflow::CheckFailedError proto_checkfailederror;
  this->ToProto(&proto_checkfailederror);
  return proto_checkfailederror.DebugString();
}

void ConstCheckFailedError::_CheckFailedError_::Clear() {
}

void ConstCheckFailedError::_CheckFailedError_::CopyFrom(const _CheckFailedError_& other) {
}



int ConstCheckFailedError::_CheckFailedError_::compare(const _CheckFailedError_& other) {
  return 0;
}

bool ConstCheckFailedError::_CheckFailedError_::operator==(const _CheckFailedError_& other) const {
  return true
  ;
}

std::size_t ConstCheckFailedError::_CheckFailedError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstCheckFailedError::_CheckFailedError_::operator<(const _CheckFailedError_& other) const {
  return false
  ;
}

using _CheckFailedError_ =  ConstCheckFailedError::_CheckFailedError_;
ConstCheckFailedError::ConstCheckFailedError(const ::std::shared_ptr<_CheckFailedError_>& data): data_(data) {}
ConstCheckFailedError::ConstCheckFailedError(): data_(::std::make_shared<_CheckFailedError_>()) {}
ConstCheckFailedError::ConstCheckFailedError(const ::oneflow::CheckFailedError& proto_checkfailederror) {
  BuildFromProto(proto_checkfailederror);
}
ConstCheckFailedError::ConstCheckFailedError(const ConstCheckFailedError&) = default;
ConstCheckFailedError::ConstCheckFailedError(ConstCheckFailedError&&) noexcept = default;
ConstCheckFailedError::~ConstCheckFailedError() = default;

void ConstCheckFailedError::ToProto(PbMessage* proto_checkfailederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::CheckFailedError*>(proto_checkfailederror));
}
  
::std::string ConstCheckFailedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCheckFailedError::__Empty__() const {
  return !data_;
}

int ConstCheckFailedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstCheckFailedError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstCheckFailedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCheckFailedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstCheckFailedError> ConstCheckFailedError::__SharedConst__() const {
  return ::std::make_shared<ConstCheckFailedError>(data_);
}
int64_t ConstCheckFailedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCheckFailedError::operator==(const ConstCheckFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCheckFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCheckFailedError::operator<(const ConstCheckFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CheckFailedError_>& ConstCheckFailedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CheckFailedError_> default_ptr = std::make_shared<_CheckFailedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_CheckFailedError_>& ConstCheckFailedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _CheckFailedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCheckFailedError
void ConstCheckFailedError::BuildFromProto(const PbMessage& proto_checkfailederror) {
  data_ = ::std::make_shared<_CheckFailedError_>(dynamic_cast<const ::oneflow::CheckFailedError&>(proto_checkfailederror));
}

CheckFailedError::CheckFailedError(const ::std::shared_ptr<_CheckFailedError_>& data)
  : ConstCheckFailedError(data) {}
CheckFailedError::CheckFailedError(const CheckFailedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CheckFailedError> resize
CheckFailedError::CheckFailedError(CheckFailedError&&) noexcept = default; 
CheckFailedError::CheckFailedError(const ::oneflow::CheckFailedError& proto_checkfailederror) {
  InitFromProto(proto_checkfailederror);
}
CheckFailedError::CheckFailedError() = default;

CheckFailedError::~CheckFailedError() = default;

void CheckFailedError::InitFromProto(const PbMessage& proto_checkfailederror) {
  BuildFromProto(proto_checkfailederror);
}
  
void* CheckFailedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool CheckFailedError::operator==(const CheckFailedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CheckFailedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CheckFailedError::operator<(const CheckFailedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CheckFailedError::Clear() {
  if (data_) { data_.reset(); }
}
void CheckFailedError::CopyFrom(const CheckFailedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CheckFailedError& CheckFailedError::operator=(const CheckFailedError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<CheckFailedError> CheckFailedError::__SharedMutable__() {
  return ::std::make_shared<CheckFailedError>(__SharedPtr__());
}
ConstTodoError::_TodoError_::_TodoError_() { Clear(); }
ConstTodoError::_TodoError_::_TodoError_(const _TodoError_& other) { CopyFrom(other); }
ConstTodoError::_TodoError_::_TodoError_(const ::oneflow::TodoError& proto_todoerror) {
  InitFromProto(proto_todoerror);
}
ConstTodoError::_TodoError_::_TodoError_(_TodoError_&& other) = default;
ConstTodoError::_TodoError_::~_TodoError_() = default;

void ConstTodoError::_TodoError_::InitFromProto(const ::oneflow::TodoError& proto_todoerror) {
  Clear();
    
}

void ConstTodoError::_TodoError_::ToProto(::oneflow::TodoError* proto_todoerror) const {
  proto_todoerror->Clear();

}

::std::string ConstTodoError::_TodoError_::DebugString() const {
  ::oneflow::TodoError proto_todoerror;
  this->ToProto(&proto_todoerror);
  return proto_todoerror.DebugString();
}

void ConstTodoError::_TodoError_::Clear() {
}

void ConstTodoError::_TodoError_::CopyFrom(const _TodoError_& other) {
}



int ConstTodoError::_TodoError_::compare(const _TodoError_& other) {
  return 0;
}

bool ConstTodoError::_TodoError_::operator==(const _TodoError_& other) const {
  return true
  ;
}

std::size_t ConstTodoError::_TodoError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstTodoError::_TodoError_::operator<(const _TodoError_& other) const {
  return false
  ;
}

using _TodoError_ =  ConstTodoError::_TodoError_;
ConstTodoError::ConstTodoError(const ::std::shared_ptr<_TodoError_>& data): data_(data) {}
ConstTodoError::ConstTodoError(): data_(::std::make_shared<_TodoError_>()) {}
ConstTodoError::ConstTodoError(const ::oneflow::TodoError& proto_todoerror) {
  BuildFromProto(proto_todoerror);
}
ConstTodoError::ConstTodoError(const ConstTodoError&) = default;
ConstTodoError::ConstTodoError(ConstTodoError&&) noexcept = default;
ConstTodoError::~ConstTodoError() = default;

void ConstTodoError::ToProto(PbMessage* proto_todoerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TodoError*>(proto_todoerror));
}
  
::std::string ConstTodoError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTodoError::__Empty__() const {
  return !data_;
}

int ConstTodoError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstTodoError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstTodoError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTodoError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstTodoError> ConstTodoError::__SharedConst__() const {
  return ::std::make_shared<ConstTodoError>(data_);
}
int64_t ConstTodoError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTodoError::operator==(const ConstTodoError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTodoError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTodoError::operator<(const ConstTodoError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TodoError_>& ConstTodoError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TodoError_> default_ptr = std::make_shared<_TodoError_>();
  return default_ptr;
}
const ::std::shared_ptr<_TodoError_>& ConstTodoError::__SharedPtr__() {
  if (!data_) { data_.reset(new _TodoError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTodoError
void ConstTodoError::BuildFromProto(const PbMessage& proto_todoerror) {
  data_ = ::std::make_shared<_TodoError_>(dynamic_cast<const ::oneflow::TodoError&>(proto_todoerror));
}

TodoError::TodoError(const ::std::shared_ptr<_TodoError_>& data)
  : ConstTodoError(data) {}
TodoError::TodoError(const TodoError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TodoError> resize
TodoError::TodoError(TodoError&&) noexcept = default; 
TodoError::TodoError(const ::oneflow::TodoError& proto_todoerror) {
  InitFromProto(proto_todoerror);
}
TodoError::TodoError() = default;

TodoError::~TodoError() = default;

void TodoError::InitFromProto(const PbMessage& proto_todoerror) {
  BuildFromProto(proto_todoerror);
}
  
void* TodoError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool TodoError::operator==(const TodoError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TodoError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TodoError::operator<(const TodoError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TodoError::Clear() {
  if (data_) { data_.reset(); }
}
void TodoError::CopyFrom(const TodoError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TodoError& TodoError::operator=(const TodoError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<TodoError> TodoError::__SharedMutable__() {
  return ::std::make_shared<TodoError>(__SharedPtr__());
}
ConstUnimplementedError::_UnimplementedError_::_UnimplementedError_() { Clear(); }
ConstUnimplementedError::_UnimplementedError_::_UnimplementedError_(const _UnimplementedError_& other) { CopyFrom(other); }
ConstUnimplementedError::_UnimplementedError_::_UnimplementedError_(const ::oneflow::UnimplementedError& proto_unimplementederror) {
  InitFromProto(proto_unimplementederror);
}
ConstUnimplementedError::_UnimplementedError_::_UnimplementedError_(_UnimplementedError_&& other) = default;
ConstUnimplementedError::_UnimplementedError_::~_UnimplementedError_() = default;

void ConstUnimplementedError::_UnimplementedError_::InitFromProto(const ::oneflow::UnimplementedError& proto_unimplementederror) {
  Clear();
    
}

void ConstUnimplementedError::_UnimplementedError_::ToProto(::oneflow::UnimplementedError* proto_unimplementederror) const {
  proto_unimplementederror->Clear();

}

::std::string ConstUnimplementedError::_UnimplementedError_::DebugString() const {
  ::oneflow::UnimplementedError proto_unimplementederror;
  this->ToProto(&proto_unimplementederror);
  return proto_unimplementederror.DebugString();
}

void ConstUnimplementedError::_UnimplementedError_::Clear() {
}

void ConstUnimplementedError::_UnimplementedError_::CopyFrom(const _UnimplementedError_& other) {
}



int ConstUnimplementedError::_UnimplementedError_::compare(const _UnimplementedError_& other) {
  return 0;
}

bool ConstUnimplementedError::_UnimplementedError_::operator==(const _UnimplementedError_& other) const {
  return true
  ;
}

std::size_t ConstUnimplementedError::_UnimplementedError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstUnimplementedError::_UnimplementedError_::operator<(const _UnimplementedError_& other) const {
  return false
  ;
}

using _UnimplementedError_ =  ConstUnimplementedError::_UnimplementedError_;
ConstUnimplementedError::ConstUnimplementedError(const ::std::shared_ptr<_UnimplementedError_>& data): data_(data) {}
ConstUnimplementedError::ConstUnimplementedError(): data_(::std::make_shared<_UnimplementedError_>()) {}
ConstUnimplementedError::ConstUnimplementedError(const ::oneflow::UnimplementedError& proto_unimplementederror) {
  BuildFromProto(proto_unimplementederror);
}
ConstUnimplementedError::ConstUnimplementedError(const ConstUnimplementedError&) = default;
ConstUnimplementedError::ConstUnimplementedError(ConstUnimplementedError&&) noexcept = default;
ConstUnimplementedError::~ConstUnimplementedError() = default;

void ConstUnimplementedError::ToProto(PbMessage* proto_unimplementederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::UnimplementedError*>(proto_unimplementederror));
}
  
::std::string ConstUnimplementedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstUnimplementedError::__Empty__() const {
  return !data_;
}

int ConstUnimplementedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstUnimplementedError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstUnimplementedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstUnimplementedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstUnimplementedError> ConstUnimplementedError::__SharedConst__() const {
  return ::std::make_shared<ConstUnimplementedError>(data_);
}
int64_t ConstUnimplementedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstUnimplementedError::operator==(const ConstUnimplementedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstUnimplementedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstUnimplementedError::operator<(const ConstUnimplementedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_UnimplementedError_>& ConstUnimplementedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_UnimplementedError_> default_ptr = std::make_shared<_UnimplementedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_UnimplementedError_>& ConstUnimplementedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _UnimplementedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstUnimplementedError
void ConstUnimplementedError::BuildFromProto(const PbMessage& proto_unimplementederror) {
  data_ = ::std::make_shared<_UnimplementedError_>(dynamic_cast<const ::oneflow::UnimplementedError&>(proto_unimplementederror));
}

UnimplementedError::UnimplementedError(const ::std::shared_ptr<_UnimplementedError_>& data)
  : ConstUnimplementedError(data) {}
UnimplementedError::UnimplementedError(const UnimplementedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<UnimplementedError> resize
UnimplementedError::UnimplementedError(UnimplementedError&&) noexcept = default; 
UnimplementedError::UnimplementedError(const ::oneflow::UnimplementedError& proto_unimplementederror) {
  InitFromProto(proto_unimplementederror);
}
UnimplementedError::UnimplementedError() = default;

UnimplementedError::~UnimplementedError() = default;

void UnimplementedError::InitFromProto(const PbMessage& proto_unimplementederror) {
  BuildFromProto(proto_unimplementederror);
}
  
void* UnimplementedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool UnimplementedError::operator==(const UnimplementedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t UnimplementedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool UnimplementedError::operator<(const UnimplementedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void UnimplementedError::Clear() {
  if (data_) { data_.reset(); }
}
void UnimplementedError::CopyFrom(const UnimplementedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
UnimplementedError& UnimplementedError::operator=(const UnimplementedError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<UnimplementedError> UnimplementedError::__SharedMutable__() {
  return ::std::make_shared<UnimplementedError>(__SharedPtr__());
}
ConstBoxingNotSupportedError::_BoxingNotSupportedError_::_BoxingNotSupportedError_() { Clear(); }
ConstBoxingNotSupportedError::_BoxingNotSupportedError_::_BoxingNotSupportedError_(const _BoxingNotSupportedError_& other) { CopyFrom(other); }
ConstBoxingNotSupportedError::_BoxingNotSupportedError_::_BoxingNotSupportedError_(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror) {
  InitFromProto(proto_boxingnotsupportederror);
}
ConstBoxingNotSupportedError::_BoxingNotSupportedError_::_BoxingNotSupportedError_(_BoxingNotSupportedError_&& other) = default;
ConstBoxingNotSupportedError::_BoxingNotSupportedError_::~_BoxingNotSupportedError_() = default;

void ConstBoxingNotSupportedError::_BoxingNotSupportedError_::InitFromProto(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror) {
  Clear();
    
}

void ConstBoxingNotSupportedError::_BoxingNotSupportedError_::ToProto(::oneflow::BoxingNotSupportedError* proto_boxingnotsupportederror) const {
  proto_boxingnotsupportederror->Clear();

}

::std::string ConstBoxingNotSupportedError::_BoxingNotSupportedError_::DebugString() const {
  ::oneflow::BoxingNotSupportedError proto_boxingnotsupportederror;
  this->ToProto(&proto_boxingnotsupportederror);
  return proto_boxingnotsupportederror.DebugString();
}

void ConstBoxingNotSupportedError::_BoxingNotSupportedError_::Clear() {
}

void ConstBoxingNotSupportedError::_BoxingNotSupportedError_::CopyFrom(const _BoxingNotSupportedError_& other) {
}



int ConstBoxingNotSupportedError::_BoxingNotSupportedError_::compare(const _BoxingNotSupportedError_& other) {
  return 0;
}

bool ConstBoxingNotSupportedError::_BoxingNotSupportedError_::operator==(const _BoxingNotSupportedError_& other) const {
  return true
  ;
}

std::size_t ConstBoxingNotSupportedError::_BoxingNotSupportedError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstBoxingNotSupportedError::_BoxingNotSupportedError_::operator<(const _BoxingNotSupportedError_& other) const {
  return false
  ;
}

using _BoxingNotSupportedError_ =  ConstBoxingNotSupportedError::_BoxingNotSupportedError_;
ConstBoxingNotSupportedError::ConstBoxingNotSupportedError(const ::std::shared_ptr<_BoxingNotSupportedError_>& data): data_(data) {}
ConstBoxingNotSupportedError::ConstBoxingNotSupportedError(): data_(::std::make_shared<_BoxingNotSupportedError_>()) {}
ConstBoxingNotSupportedError::ConstBoxingNotSupportedError(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror) {
  BuildFromProto(proto_boxingnotsupportederror);
}
ConstBoxingNotSupportedError::ConstBoxingNotSupportedError(const ConstBoxingNotSupportedError&) = default;
ConstBoxingNotSupportedError::ConstBoxingNotSupportedError(ConstBoxingNotSupportedError&&) noexcept = default;
ConstBoxingNotSupportedError::~ConstBoxingNotSupportedError() = default;

void ConstBoxingNotSupportedError::ToProto(PbMessage* proto_boxingnotsupportederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BoxingNotSupportedError*>(proto_boxingnotsupportederror));
}
  
::std::string ConstBoxingNotSupportedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBoxingNotSupportedError::__Empty__() const {
  return !data_;
}

int ConstBoxingNotSupportedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstBoxingNotSupportedError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstBoxingNotSupportedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBoxingNotSupportedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstBoxingNotSupportedError> ConstBoxingNotSupportedError::__SharedConst__() const {
  return ::std::make_shared<ConstBoxingNotSupportedError>(data_);
}
int64_t ConstBoxingNotSupportedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBoxingNotSupportedError::operator==(const ConstBoxingNotSupportedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBoxingNotSupportedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBoxingNotSupportedError::operator<(const ConstBoxingNotSupportedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BoxingNotSupportedError_>& ConstBoxingNotSupportedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BoxingNotSupportedError_> default_ptr = std::make_shared<_BoxingNotSupportedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_BoxingNotSupportedError_>& ConstBoxingNotSupportedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _BoxingNotSupportedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBoxingNotSupportedError
void ConstBoxingNotSupportedError::BuildFromProto(const PbMessage& proto_boxingnotsupportederror) {
  data_ = ::std::make_shared<_BoxingNotSupportedError_>(dynamic_cast<const ::oneflow::BoxingNotSupportedError&>(proto_boxingnotsupportederror));
}

BoxingNotSupportedError::BoxingNotSupportedError(const ::std::shared_ptr<_BoxingNotSupportedError_>& data)
  : ConstBoxingNotSupportedError(data) {}
BoxingNotSupportedError::BoxingNotSupportedError(const BoxingNotSupportedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BoxingNotSupportedError> resize
BoxingNotSupportedError::BoxingNotSupportedError(BoxingNotSupportedError&&) noexcept = default; 
BoxingNotSupportedError::BoxingNotSupportedError(const ::oneflow::BoxingNotSupportedError& proto_boxingnotsupportederror) {
  InitFromProto(proto_boxingnotsupportederror);
}
BoxingNotSupportedError::BoxingNotSupportedError() = default;

BoxingNotSupportedError::~BoxingNotSupportedError() = default;

void BoxingNotSupportedError::InitFromProto(const PbMessage& proto_boxingnotsupportederror) {
  BuildFromProto(proto_boxingnotsupportederror);
}
  
void* BoxingNotSupportedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool BoxingNotSupportedError::operator==(const BoxingNotSupportedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BoxingNotSupportedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BoxingNotSupportedError::operator<(const BoxingNotSupportedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BoxingNotSupportedError::Clear() {
  if (data_) { data_.reset(); }
}
void BoxingNotSupportedError::CopyFrom(const BoxingNotSupportedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BoxingNotSupportedError& BoxingNotSupportedError::operator=(const BoxingNotSupportedError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<BoxingNotSupportedError> BoxingNotSupportedError::__SharedMutable__() {
  return ::std::make_shared<BoxingNotSupportedError>(__SharedPtr__());
}
ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::_GradientFunctionNotFoundError_() { Clear(); }
ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::_GradientFunctionNotFoundError_(const _GradientFunctionNotFoundError_& other) { CopyFrom(other); }
ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::_GradientFunctionNotFoundError_(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror) {
  InitFromProto(proto_gradientfunctionnotfounderror);
}
ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::_GradientFunctionNotFoundError_(_GradientFunctionNotFoundError_&& other) = default;
ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::~_GradientFunctionNotFoundError_() = default;

void ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::InitFromProto(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror) {
  Clear();
    
}

void ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::ToProto(::oneflow::GradientFunctionNotFoundError* proto_gradientfunctionnotfounderror) const {
  proto_gradientfunctionnotfounderror->Clear();

}

::std::string ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::DebugString() const {
  ::oneflow::GradientFunctionNotFoundError proto_gradientfunctionnotfounderror;
  this->ToProto(&proto_gradientfunctionnotfounderror);
  return proto_gradientfunctionnotfounderror.DebugString();
}

void ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::Clear() {
}

void ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::CopyFrom(const _GradientFunctionNotFoundError_& other) {
}



int ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::compare(const _GradientFunctionNotFoundError_& other) {
  return 0;
}

bool ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::operator==(const _GradientFunctionNotFoundError_& other) const {
  return true
  ;
}

std::size_t ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_::operator<(const _GradientFunctionNotFoundError_& other) const {
  return false
  ;
}

using _GradientFunctionNotFoundError_ =  ConstGradientFunctionNotFoundError::_GradientFunctionNotFoundError_;
ConstGradientFunctionNotFoundError::ConstGradientFunctionNotFoundError(const ::std::shared_ptr<_GradientFunctionNotFoundError_>& data): data_(data) {}
ConstGradientFunctionNotFoundError::ConstGradientFunctionNotFoundError(): data_(::std::make_shared<_GradientFunctionNotFoundError_>()) {}
ConstGradientFunctionNotFoundError::ConstGradientFunctionNotFoundError(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror) {
  BuildFromProto(proto_gradientfunctionnotfounderror);
}
ConstGradientFunctionNotFoundError::ConstGradientFunctionNotFoundError(const ConstGradientFunctionNotFoundError&) = default;
ConstGradientFunctionNotFoundError::ConstGradientFunctionNotFoundError(ConstGradientFunctionNotFoundError&&) noexcept = default;
ConstGradientFunctionNotFoundError::~ConstGradientFunctionNotFoundError() = default;

void ConstGradientFunctionNotFoundError::ToProto(PbMessage* proto_gradientfunctionnotfounderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::GradientFunctionNotFoundError*>(proto_gradientfunctionnotfounderror));
}
  
::std::string ConstGradientFunctionNotFoundError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstGradientFunctionNotFoundError::__Empty__() const {
  return !data_;
}

int ConstGradientFunctionNotFoundError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstGradientFunctionNotFoundError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstGradientFunctionNotFoundError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstGradientFunctionNotFoundError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstGradientFunctionNotFoundError> ConstGradientFunctionNotFoundError::__SharedConst__() const {
  return ::std::make_shared<ConstGradientFunctionNotFoundError>(data_);
}
int64_t ConstGradientFunctionNotFoundError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstGradientFunctionNotFoundError::operator==(const ConstGradientFunctionNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstGradientFunctionNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstGradientFunctionNotFoundError::operator<(const ConstGradientFunctionNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_GradientFunctionNotFoundError_>& ConstGradientFunctionNotFoundError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_GradientFunctionNotFoundError_> default_ptr = std::make_shared<_GradientFunctionNotFoundError_>();
  return default_ptr;
}
const ::std::shared_ptr<_GradientFunctionNotFoundError_>& ConstGradientFunctionNotFoundError::__SharedPtr__() {
  if (!data_) { data_.reset(new _GradientFunctionNotFoundError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstGradientFunctionNotFoundError
void ConstGradientFunctionNotFoundError::BuildFromProto(const PbMessage& proto_gradientfunctionnotfounderror) {
  data_ = ::std::make_shared<_GradientFunctionNotFoundError_>(dynamic_cast<const ::oneflow::GradientFunctionNotFoundError&>(proto_gradientfunctionnotfounderror));
}

GradientFunctionNotFoundError::GradientFunctionNotFoundError(const ::std::shared_ptr<_GradientFunctionNotFoundError_>& data)
  : ConstGradientFunctionNotFoundError(data) {}
GradientFunctionNotFoundError::GradientFunctionNotFoundError(const GradientFunctionNotFoundError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<GradientFunctionNotFoundError> resize
GradientFunctionNotFoundError::GradientFunctionNotFoundError(GradientFunctionNotFoundError&&) noexcept = default; 
GradientFunctionNotFoundError::GradientFunctionNotFoundError(const ::oneflow::GradientFunctionNotFoundError& proto_gradientfunctionnotfounderror) {
  InitFromProto(proto_gradientfunctionnotfounderror);
}
GradientFunctionNotFoundError::GradientFunctionNotFoundError() = default;

GradientFunctionNotFoundError::~GradientFunctionNotFoundError() = default;

void GradientFunctionNotFoundError::InitFromProto(const PbMessage& proto_gradientfunctionnotfounderror) {
  BuildFromProto(proto_gradientfunctionnotfounderror);
}
  
void* GradientFunctionNotFoundError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool GradientFunctionNotFoundError::operator==(const GradientFunctionNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t GradientFunctionNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool GradientFunctionNotFoundError::operator<(const GradientFunctionNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void GradientFunctionNotFoundError::Clear() {
  if (data_) { data_.reset(); }
}
void GradientFunctionNotFoundError::CopyFrom(const GradientFunctionNotFoundError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
GradientFunctionNotFoundError& GradientFunctionNotFoundError::operator=(const GradientFunctionNotFoundError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<GradientFunctionNotFoundError> GradientFunctionNotFoundError::__SharedMutable__() {
  return ::std::make_shared<GradientFunctionNotFoundError>(__SharedPtr__());
}
ConstOpKernelNotFoundError::_OpKernelNotFoundError_::_OpKernelNotFoundError_() { Clear(); }
ConstOpKernelNotFoundError::_OpKernelNotFoundError_::_OpKernelNotFoundError_(const _OpKernelNotFoundError_& other) { CopyFrom(other); }
ConstOpKernelNotFoundError::_OpKernelNotFoundError_::_OpKernelNotFoundError_(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror) {
  InitFromProto(proto_opkernelnotfounderror);
}
ConstOpKernelNotFoundError::_OpKernelNotFoundError_::_OpKernelNotFoundError_(_OpKernelNotFoundError_&& other) = default;
ConstOpKernelNotFoundError::_OpKernelNotFoundError_::~_OpKernelNotFoundError_() = default;

void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::InitFromProto(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror) {
  Clear();
  // repeated field: op_kernels_not_found_debug_str
  if (!proto_opkernelnotfounderror.op_kernels_not_found_debug_str().empty()) {
    for (const ::std::string& elem : proto_opkernelnotfounderror.op_kernels_not_found_debug_str()) {
      add_op_kernels_not_found_debug_str(elem);
    }
  }
    
}

void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::ToProto(::oneflow::OpKernelNotFoundError* proto_opkernelnotfounderror) const {
  proto_opkernelnotfounderror->Clear();
  // repeated field: op_kernels_not_found_debug_str
  if (!op_kernels_not_found_debug_str().empty()) {
    for (const ::std::string& elem : op_kernels_not_found_debug_str()) {
      proto_opkernelnotfounderror->add_op_kernels_not_found_debug_str(elem);
    }
  }

}

::std::string ConstOpKernelNotFoundError::_OpKernelNotFoundError_::DebugString() const {
  ::oneflow::OpKernelNotFoundError proto_opkernelnotfounderror;
  this->ToProto(&proto_opkernelnotfounderror);
  return proto_opkernelnotfounderror.DebugString();
}

void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::Clear() {
  clear_op_kernels_not_found_debug_str();
}

void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::CopyFrom(const _OpKernelNotFoundError_& other) {
  mutable_op_kernels_not_found_debug_str()->CopyFrom(other.op_kernels_not_found_debug_str());
}


// repeated field op_kernels_not_found_debug_str
::std::size_t ConstOpKernelNotFoundError::_OpKernelNotFoundError_::op_kernels_not_found_debug_str_size() const {
  if (!op_kernels_not_found_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return op_kernels_not_found_debug_str_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstOpKernelNotFoundError::_OpKernelNotFoundError_::op_kernels_not_found_debug_str() const {
  if (!op_kernels_not_found_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(op_kernels_not_found_debug_str_.get());
}
const ::std::string& ConstOpKernelNotFoundError::_OpKernelNotFoundError_::op_kernels_not_found_debug_str(::std::size_t index) const {
  if (!op_kernels_not_found_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return op_kernels_not_found_debug_str_->Get(index);
}
void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::clear_op_kernels_not_found_debug_str() {
  if (!op_kernels_not_found_debug_str_) {
    op_kernels_not_found_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return op_kernels_not_found_debug_str_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstOpKernelNotFoundError::_OpKernelNotFoundError_::mutable_op_kernels_not_found_debug_str() {
  if (!op_kernels_not_found_debug_str_) {
    op_kernels_not_found_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  op_kernels_not_found_debug_str_.get();
}
::std::string* ConstOpKernelNotFoundError::_OpKernelNotFoundError_::mutable_op_kernels_not_found_debug_str(::std::size_t index) {
  if (!op_kernels_not_found_debug_str_) {
    op_kernels_not_found_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  op_kernels_not_found_debug_str_->Mutable(index);
}
void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::add_op_kernels_not_found_debug_str(const ::std::string& value) {
  if (!op_kernels_not_found_debug_str_) {
    op_kernels_not_found_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return op_kernels_not_found_debug_str_->Add(value);
}
void ConstOpKernelNotFoundError::_OpKernelNotFoundError_::set_op_kernels_not_found_debug_str(::std::size_t index, const ::std::string& value) {
  if (!op_kernels_not_found_debug_str_) {
    op_kernels_not_found_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return op_kernels_not_found_debug_str_->Set(index, value);
}


int ConstOpKernelNotFoundError::_OpKernelNotFoundError_::compare(const _OpKernelNotFoundError_& other) {
  if (!(op_kernels_not_found_debug_str() == other.op_kernels_not_found_debug_str())) {
    return op_kernels_not_found_debug_str() < other.op_kernels_not_found_debug_str() ? -1 : 1;
  }
  return 0;
}

bool ConstOpKernelNotFoundError::_OpKernelNotFoundError_::operator==(const _OpKernelNotFoundError_& other) const {
  return true
    && op_kernels_not_found_debug_str() == other.op_kernels_not_found_debug_str()
  ;
}

std::size_t ConstOpKernelNotFoundError::_OpKernelNotFoundError_::__CalcHash__() const {
  return 0
    ^ op_kernels_not_found_debug_str().__CalcHash__()
  ;
}

bool ConstOpKernelNotFoundError::_OpKernelNotFoundError_::operator<(const _OpKernelNotFoundError_& other) const {
  return false
    || !(op_kernels_not_found_debug_str() == other.op_kernels_not_found_debug_str()) ? 
      op_kernels_not_found_debug_str() < other.op_kernels_not_found_debug_str() : false
  ;
}

using _OpKernelNotFoundError_ =  ConstOpKernelNotFoundError::_OpKernelNotFoundError_;
ConstOpKernelNotFoundError::ConstOpKernelNotFoundError(const ::std::shared_ptr<_OpKernelNotFoundError_>& data): data_(data) {}
ConstOpKernelNotFoundError::ConstOpKernelNotFoundError(): data_(::std::make_shared<_OpKernelNotFoundError_>()) {}
ConstOpKernelNotFoundError::ConstOpKernelNotFoundError(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror) {
  BuildFromProto(proto_opkernelnotfounderror);
}
ConstOpKernelNotFoundError::ConstOpKernelNotFoundError(const ConstOpKernelNotFoundError&) = default;
ConstOpKernelNotFoundError::ConstOpKernelNotFoundError(ConstOpKernelNotFoundError&&) noexcept = default;
ConstOpKernelNotFoundError::~ConstOpKernelNotFoundError() = default;

void ConstOpKernelNotFoundError::ToProto(PbMessage* proto_opkernelnotfounderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpKernelNotFoundError*>(proto_opkernelnotfounderror));
}
  
::std::string ConstOpKernelNotFoundError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpKernelNotFoundError::__Empty__() const {
  return !data_;
}

int ConstOpKernelNotFoundError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_kernels_not_found_debug_str", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpKernelNotFoundError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpKernelNotFoundError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpKernelNotFoundError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_kernels_not_found_debug_str();
    default: return nullptr;
  }
}

// repeated field op_kernels_not_found_debug_str
::std::size_t ConstOpKernelNotFoundError::op_kernels_not_found_debug_str_size() const {
  return __SharedPtrOrDefault__()->op_kernels_not_found_debug_str_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstOpKernelNotFoundError::op_kernels_not_found_debug_str() const {
  return __SharedPtrOrDefault__()->op_kernels_not_found_debug_str();
}
const ::std::string& ConstOpKernelNotFoundError::op_kernels_not_found_debug_str(::std::size_t index) const {
  return __SharedPtrOrDefault__()->op_kernels_not_found_debug_str(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstOpKernelNotFoundError::shared_const_op_kernels_not_found_debug_str() const {
  return op_kernels_not_found_debug_str().__SharedConst__();
}

::std::shared_ptr<ConstOpKernelNotFoundError> ConstOpKernelNotFoundError::__SharedConst__() const {
  return ::std::make_shared<ConstOpKernelNotFoundError>(data_);
}
int64_t ConstOpKernelNotFoundError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpKernelNotFoundError::operator==(const ConstOpKernelNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpKernelNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpKernelNotFoundError::operator<(const ConstOpKernelNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpKernelNotFoundError_>& ConstOpKernelNotFoundError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpKernelNotFoundError_> default_ptr = std::make_shared<_OpKernelNotFoundError_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpKernelNotFoundError_>& ConstOpKernelNotFoundError::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpKernelNotFoundError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpKernelNotFoundError
void ConstOpKernelNotFoundError::BuildFromProto(const PbMessage& proto_opkernelnotfounderror) {
  data_ = ::std::make_shared<_OpKernelNotFoundError_>(dynamic_cast<const ::oneflow::OpKernelNotFoundError&>(proto_opkernelnotfounderror));
}

OpKernelNotFoundError::OpKernelNotFoundError(const ::std::shared_ptr<_OpKernelNotFoundError_>& data)
  : ConstOpKernelNotFoundError(data) {}
OpKernelNotFoundError::OpKernelNotFoundError(const OpKernelNotFoundError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpKernelNotFoundError> resize
OpKernelNotFoundError::OpKernelNotFoundError(OpKernelNotFoundError&&) noexcept = default; 
OpKernelNotFoundError::OpKernelNotFoundError(const ::oneflow::OpKernelNotFoundError& proto_opkernelnotfounderror) {
  InitFromProto(proto_opkernelnotfounderror);
}
OpKernelNotFoundError::OpKernelNotFoundError() = default;

OpKernelNotFoundError::~OpKernelNotFoundError() = default;

void OpKernelNotFoundError::InitFromProto(const PbMessage& proto_opkernelnotfounderror) {
  BuildFromProto(proto_opkernelnotfounderror);
}
  
void* OpKernelNotFoundError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_kernels_not_found_debug_str();
    default: return nullptr;
  }
}

bool OpKernelNotFoundError::operator==(const OpKernelNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpKernelNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpKernelNotFoundError::operator<(const OpKernelNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpKernelNotFoundError::Clear() {
  if (data_) { data_.reset(); }
}
void OpKernelNotFoundError::CopyFrom(const OpKernelNotFoundError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpKernelNotFoundError& OpKernelNotFoundError::operator=(const OpKernelNotFoundError& other) {
  CopyFrom(other);
  return *this;
}

// repeated field op_kernels_not_found_debug_str
void OpKernelNotFoundError::clear_op_kernels_not_found_debug_str() {
  return __SharedPtr__()->clear_op_kernels_not_found_debug_str();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* OpKernelNotFoundError::mutable_op_kernels_not_found_debug_str() {
  return __SharedPtr__()->mutable_op_kernels_not_found_debug_str();
}
::std::string* OpKernelNotFoundError::mutable_op_kernels_not_found_debug_str(::std::size_t index) {
  return __SharedPtr__()->mutable_op_kernels_not_found_debug_str(index);
}
void OpKernelNotFoundError::add_op_kernels_not_found_debug_str(const ::std::string& value) {
  return __SharedPtr__()->add_op_kernels_not_found_debug_str(value);
}
void OpKernelNotFoundError::set_op_kernels_not_found_debug_str(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_op_kernels_not_found_debug_str(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> OpKernelNotFoundError::shared_mutable_op_kernels_not_found_debug_str() {
  return mutable_op_kernels_not_found_debug_str()->__SharedMutable__();
}

::std::shared_ptr<OpKernelNotFoundError> OpKernelNotFoundError::__SharedMutable__() {
  return ::std::make_shared<OpKernelNotFoundError>(__SharedPtr__());
}
ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::_MultipleOpKernelsMatchedError_() { Clear(); }
ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::_MultipleOpKernelsMatchedError_(const _MultipleOpKernelsMatchedError_& other) { CopyFrom(other); }
ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::_MultipleOpKernelsMatchedError_(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror) {
  InitFromProto(proto_multipleopkernelsmatchederror);
}
ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::_MultipleOpKernelsMatchedError_(_MultipleOpKernelsMatchedError_&& other) = default;
ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::~_MultipleOpKernelsMatchedError_() = default;

void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::InitFromProto(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror) {
  Clear();
  // repeated field: matched_op_kernels_debug_str
  if (!proto_multipleopkernelsmatchederror.matched_op_kernels_debug_str().empty()) {
    for (const ::std::string& elem : proto_multipleopkernelsmatchederror.matched_op_kernels_debug_str()) {
      add_matched_op_kernels_debug_str(elem);
    }
  }
    
}

void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::ToProto(::oneflow::MultipleOpKernelsMatchedError* proto_multipleopkernelsmatchederror) const {
  proto_multipleopkernelsmatchederror->Clear();
  // repeated field: matched_op_kernels_debug_str
  if (!matched_op_kernels_debug_str().empty()) {
    for (const ::std::string& elem : matched_op_kernels_debug_str()) {
      proto_multipleopkernelsmatchederror->add_matched_op_kernels_debug_str(elem);
    }
  }

}

::std::string ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::DebugString() const {
  ::oneflow::MultipleOpKernelsMatchedError proto_multipleopkernelsmatchederror;
  this->ToProto(&proto_multipleopkernelsmatchederror);
  return proto_multipleopkernelsmatchederror.DebugString();
}

void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::Clear() {
  clear_matched_op_kernels_debug_str();
}

void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::CopyFrom(const _MultipleOpKernelsMatchedError_& other) {
  mutable_matched_op_kernels_debug_str()->CopyFrom(other.matched_op_kernels_debug_str());
}


// repeated field matched_op_kernels_debug_str
::std::size_t ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::matched_op_kernels_debug_str_size() const {
  if (!matched_op_kernels_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return matched_op_kernels_debug_str_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::matched_op_kernels_debug_str() const {
  if (!matched_op_kernels_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(matched_op_kernels_debug_str_.get());
}
const ::std::string& ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::matched_op_kernels_debug_str(::std::size_t index) const {
  if (!matched_op_kernels_debug_str_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return matched_op_kernels_debug_str_->Get(index);
}
void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::clear_matched_op_kernels_debug_str() {
  if (!matched_op_kernels_debug_str_) {
    matched_op_kernels_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return matched_op_kernels_debug_str_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::mutable_matched_op_kernels_debug_str() {
  if (!matched_op_kernels_debug_str_) {
    matched_op_kernels_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  matched_op_kernels_debug_str_.get();
}
::std::string* ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::mutable_matched_op_kernels_debug_str(::std::size_t index) {
  if (!matched_op_kernels_debug_str_) {
    matched_op_kernels_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  matched_op_kernels_debug_str_->Mutable(index);
}
void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::add_matched_op_kernels_debug_str(const ::std::string& value) {
  if (!matched_op_kernels_debug_str_) {
    matched_op_kernels_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return matched_op_kernels_debug_str_->Add(value);
}
void ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::set_matched_op_kernels_debug_str(::std::size_t index, const ::std::string& value) {
  if (!matched_op_kernels_debug_str_) {
    matched_op_kernels_debug_str_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return matched_op_kernels_debug_str_->Set(index, value);
}


int ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::compare(const _MultipleOpKernelsMatchedError_& other) {
  if (!(matched_op_kernels_debug_str() == other.matched_op_kernels_debug_str())) {
    return matched_op_kernels_debug_str() < other.matched_op_kernels_debug_str() ? -1 : 1;
  }
  return 0;
}

bool ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::operator==(const _MultipleOpKernelsMatchedError_& other) const {
  return true
    && matched_op_kernels_debug_str() == other.matched_op_kernels_debug_str()
  ;
}

std::size_t ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::__CalcHash__() const {
  return 0
    ^ matched_op_kernels_debug_str().__CalcHash__()
  ;
}

bool ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_::operator<(const _MultipleOpKernelsMatchedError_& other) const {
  return false
    || !(matched_op_kernels_debug_str() == other.matched_op_kernels_debug_str()) ? 
      matched_op_kernels_debug_str() < other.matched_op_kernels_debug_str() : false
  ;
}

using _MultipleOpKernelsMatchedError_ =  ConstMultipleOpKernelsMatchedError::_MultipleOpKernelsMatchedError_;
ConstMultipleOpKernelsMatchedError::ConstMultipleOpKernelsMatchedError(const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& data): data_(data) {}
ConstMultipleOpKernelsMatchedError::ConstMultipleOpKernelsMatchedError(): data_(::std::make_shared<_MultipleOpKernelsMatchedError_>()) {}
ConstMultipleOpKernelsMatchedError::ConstMultipleOpKernelsMatchedError(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror) {
  BuildFromProto(proto_multipleopkernelsmatchederror);
}
ConstMultipleOpKernelsMatchedError::ConstMultipleOpKernelsMatchedError(const ConstMultipleOpKernelsMatchedError&) = default;
ConstMultipleOpKernelsMatchedError::ConstMultipleOpKernelsMatchedError(ConstMultipleOpKernelsMatchedError&&) noexcept = default;
ConstMultipleOpKernelsMatchedError::~ConstMultipleOpKernelsMatchedError() = default;

void ConstMultipleOpKernelsMatchedError::ToProto(PbMessage* proto_multipleopkernelsmatchederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MultipleOpKernelsMatchedError*>(proto_multipleopkernelsmatchederror));
}
  
::std::string ConstMultipleOpKernelsMatchedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMultipleOpKernelsMatchedError::__Empty__() const {
  return !data_;
}

int ConstMultipleOpKernelsMatchedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"matched_op_kernels_debug_str", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMultipleOpKernelsMatchedError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMultipleOpKernelsMatchedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMultipleOpKernelsMatchedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &matched_op_kernels_debug_str();
    default: return nullptr;
  }
}

// repeated field matched_op_kernels_debug_str
::std::size_t ConstMultipleOpKernelsMatchedError::matched_op_kernels_debug_str_size() const {
  return __SharedPtrOrDefault__()->matched_op_kernels_debug_str_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMultipleOpKernelsMatchedError::matched_op_kernels_debug_str() const {
  return __SharedPtrOrDefault__()->matched_op_kernels_debug_str();
}
const ::std::string& ConstMultipleOpKernelsMatchedError::matched_op_kernels_debug_str(::std::size_t index) const {
  return __SharedPtrOrDefault__()->matched_op_kernels_debug_str(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMultipleOpKernelsMatchedError::shared_const_matched_op_kernels_debug_str() const {
  return matched_op_kernels_debug_str().__SharedConst__();
}

::std::shared_ptr<ConstMultipleOpKernelsMatchedError> ConstMultipleOpKernelsMatchedError::__SharedConst__() const {
  return ::std::make_shared<ConstMultipleOpKernelsMatchedError>(data_);
}
int64_t ConstMultipleOpKernelsMatchedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMultipleOpKernelsMatchedError::operator==(const ConstMultipleOpKernelsMatchedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMultipleOpKernelsMatchedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMultipleOpKernelsMatchedError::operator<(const ConstMultipleOpKernelsMatchedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& ConstMultipleOpKernelsMatchedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MultipleOpKernelsMatchedError_> default_ptr = std::make_shared<_MultipleOpKernelsMatchedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& ConstMultipleOpKernelsMatchedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _MultipleOpKernelsMatchedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMultipleOpKernelsMatchedError
void ConstMultipleOpKernelsMatchedError::BuildFromProto(const PbMessage& proto_multipleopkernelsmatchederror) {
  data_ = ::std::make_shared<_MultipleOpKernelsMatchedError_>(dynamic_cast<const ::oneflow::MultipleOpKernelsMatchedError&>(proto_multipleopkernelsmatchederror));
}

MultipleOpKernelsMatchedError::MultipleOpKernelsMatchedError(const ::std::shared_ptr<_MultipleOpKernelsMatchedError_>& data)
  : ConstMultipleOpKernelsMatchedError(data) {}
MultipleOpKernelsMatchedError::MultipleOpKernelsMatchedError(const MultipleOpKernelsMatchedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MultipleOpKernelsMatchedError> resize
MultipleOpKernelsMatchedError::MultipleOpKernelsMatchedError(MultipleOpKernelsMatchedError&&) noexcept = default; 
MultipleOpKernelsMatchedError::MultipleOpKernelsMatchedError(const ::oneflow::MultipleOpKernelsMatchedError& proto_multipleopkernelsmatchederror) {
  InitFromProto(proto_multipleopkernelsmatchederror);
}
MultipleOpKernelsMatchedError::MultipleOpKernelsMatchedError() = default;

MultipleOpKernelsMatchedError::~MultipleOpKernelsMatchedError() = default;

void MultipleOpKernelsMatchedError::InitFromProto(const PbMessage& proto_multipleopkernelsmatchederror) {
  BuildFromProto(proto_multipleopkernelsmatchederror);
}
  
void* MultipleOpKernelsMatchedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_matched_op_kernels_debug_str();
    default: return nullptr;
  }
}

bool MultipleOpKernelsMatchedError::operator==(const MultipleOpKernelsMatchedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MultipleOpKernelsMatchedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MultipleOpKernelsMatchedError::operator<(const MultipleOpKernelsMatchedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MultipleOpKernelsMatchedError::Clear() {
  if (data_) { data_.reset(); }
}
void MultipleOpKernelsMatchedError::CopyFrom(const MultipleOpKernelsMatchedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MultipleOpKernelsMatchedError& MultipleOpKernelsMatchedError::operator=(const MultipleOpKernelsMatchedError& other) {
  CopyFrom(other);
  return *this;
}

// repeated field matched_op_kernels_debug_str
void MultipleOpKernelsMatchedError::clear_matched_op_kernels_debug_str() {
  return __SharedPtr__()->clear_matched_op_kernels_debug_str();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MultipleOpKernelsMatchedError::mutable_matched_op_kernels_debug_str() {
  return __SharedPtr__()->mutable_matched_op_kernels_debug_str();
}
::std::string* MultipleOpKernelsMatchedError::mutable_matched_op_kernels_debug_str(::std::size_t index) {
  return __SharedPtr__()->mutable_matched_op_kernels_debug_str(index);
}
void MultipleOpKernelsMatchedError::add_matched_op_kernels_debug_str(const ::std::string& value) {
  return __SharedPtr__()->add_matched_op_kernels_debug_str(value);
}
void MultipleOpKernelsMatchedError::set_matched_op_kernels_debug_str(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_matched_op_kernels_debug_str(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MultipleOpKernelsMatchedError::shared_mutable_matched_op_kernels_debug_str() {
  return mutable_matched_op_kernels_debug_str()->__SharedMutable__();
}

::std::shared_ptr<MultipleOpKernelsMatchedError> MultipleOpKernelsMatchedError::__SharedMutable__() {
  return ::std::make_shared<MultipleOpKernelsMatchedError>(__SharedPtr__());
}
ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::_MemoryZoneOutOfMemoryError_() { Clear(); }
ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::_MemoryZoneOutOfMemoryError_(const _MemoryZoneOutOfMemoryError_& other) { CopyFrom(other); }
ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::_MemoryZoneOutOfMemoryError_(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror) {
  InitFromProto(proto_memoryzoneoutofmemoryerror);
}
ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::_MemoryZoneOutOfMemoryError_(_MemoryZoneOutOfMemoryError_&& other) = default;
ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::~_MemoryZoneOutOfMemoryError_() = default;

void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::InitFromProto(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror) {
  Clear();
  // repeated field: machine_id
  if (!proto_memoryzoneoutofmemoryerror.machine_id().empty()) {
    for (const ::std::string& elem : proto_memoryzoneoutofmemoryerror.machine_id()) {
      add_machine_id(elem);
    }
  }
  // repeated field: mem_zone_id
  if (!proto_memoryzoneoutofmemoryerror.mem_zone_id().empty()) {
    for (const ::std::string& elem : proto_memoryzoneoutofmemoryerror.mem_zone_id()) {
      add_mem_zone_id(elem);
    }
  }
  // repeated field: device_tag
  if (!proto_memoryzoneoutofmemoryerror.device_tag().empty()) {
    for (const ::std::string& elem : proto_memoryzoneoutofmemoryerror.device_tag()) {
      add_device_tag(elem);
    }
  }
  // repeated field: required
  if (!proto_memoryzoneoutofmemoryerror.required().empty()) {
    for (const ::std::string& elem : proto_memoryzoneoutofmemoryerror.required()) {
      add_required(elem);
    }
  }
  // repeated field: available
  if (!proto_memoryzoneoutofmemoryerror.available().empty()) {
    for (const ::std::string& elem : proto_memoryzoneoutofmemoryerror.available()) {
      add_available(elem);
    }
  }
    
}

void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::ToProto(::oneflow::MemoryZoneOutOfMemoryError* proto_memoryzoneoutofmemoryerror) const {
  proto_memoryzoneoutofmemoryerror->Clear();
  // repeated field: machine_id
  if (!machine_id().empty()) {
    for (const ::std::string& elem : machine_id()) {
      proto_memoryzoneoutofmemoryerror->add_machine_id(elem);
    }
  }
  // repeated field: mem_zone_id
  if (!mem_zone_id().empty()) {
    for (const ::std::string& elem : mem_zone_id()) {
      proto_memoryzoneoutofmemoryerror->add_mem_zone_id(elem);
    }
  }
  // repeated field: device_tag
  if (!device_tag().empty()) {
    for (const ::std::string& elem : device_tag()) {
      proto_memoryzoneoutofmemoryerror->add_device_tag(elem);
    }
  }
  // repeated field: required
  if (!required().empty()) {
    for (const ::std::string& elem : required()) {
      proto_memoryzoneoutofmemoryerror->add_required(elem);
    }
  }
  // repeated field: available
  if (!available().empty()) {
    for (const ::std::string& elem : available()) {
      proto_memoryzoneoutofmemoryerror->add_available(elem);
    }
  }

}

::std::string ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::DebugString() const {
  ::oneflow::MemoryZoneOutOfMemoryError proto_memoryzoneoutofmemoryerror;
  this->ToProto(&proto_memoryzoneoutofmemoryerror);
  return proto_memoryzoneoutofmemoryerror.DebugString();
}

void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::Clear() {
  clear_machine_id();
  clear_mem_zone_id();
  clear_device_tag();
  clear_required();
  clear_available();
}

void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::CopyFrom(const _MemoryZoneOutOfMemoryError_& other) {
  mutable_machine_id()->CopyFrom(other.machine_id());
  mutable_mem_zone_id()->CopyFrom(other.mem_zone_id());
  mutable_device_tag()->CopyFrom(other.device_tag());
  mutable_required()->CopyFrom(other.required());
  mutable_available()->CopyFrom(other.available());
}


// repeated field machine_id
::std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::machine_id_size() const {
  if (!machine_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return machine_id_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::machine_id() const {
  if (!machine_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(machine_id_.get());
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::machine_id(::std::size_t index) const {
  if (!machine_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return machine_id_->Get(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::clear_machine_id() {
  if (!machine_id_) {
    machine_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return machine_id_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_machine_id() {
  if (!machine_id_) {
    machine_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  machine_id_.get();
}
::std::string* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_machine_id(::std::size_t index) {
  if (!machine_id_) {
    machine_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  machine_id_->Mutable(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::add_machine_id(const ::std::string& value) {
  if (!machine_id_) {
    machine_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return machine_id_->Add(value);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::set_machine_id(::std::size_t index, const ::std::string& value) {
  if (!machine_id_) {
    machine_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return machine_id_->Set(index, value);
}

// repeated field mem_zone_id
::std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mem_zone_id_size() const {
  if (!mem_zone_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return mem_zone_id_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mem_zone_id() const {
  if (!mem_zone_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(mem_zone_id_.get());
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mem_zone_id(::std::size_t index) const {
  if (!mem_zone_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return mem_zone_id_->Get(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::clear_mem_zone_id() {
  if (!mem_zone_id_) {
    mem_zone_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return mem_zone_id_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_mem_zone_id() {
  if (!mem_zone_id_) {
    mem_zone_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  mem_zone_id_.get();
}
::std::string* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_mem_zone_id(::std::size_t index) {
  if (!mem_zone_id_) {
    mem_zone_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  mem_zone_id_->Mutable(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::add_mem_zone_id(const ::std::string& value) {
  if (!mem_zone_id_) {
    mem_zone_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return mem_zone_id_->Add(value);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::set_mem_zone_id(::std::size_t index, const ::std::string& value) {
  if (!mem_zone_id_) {
    mem_zone_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return mem_zone_id_->Set(index, value);
}

// repeated field device_tag
::std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::device_tag_size() const {
  if (!device_tag_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return device_tag_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::device_tag() const {
  if (!device_tag_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(device_tag_.get());
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::device_tag(::std::size_t index) const {
  if (!device_tag_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return device_tag_->Get(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::clear_device_tag() {
  if (!device_tag_) {
    device_tag_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return device_tag_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_device_tag() {
  if (!device_tag_) {
    device_tag_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  device_tag_.get();
}
::std::string* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_device_tag(::std::size_t index) {
  if (!device_tag_) {
    device_tag_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  device_tag_->Mutable(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::add_device_tag(const ::std::string& value) {
  if (!device_tag_) {
    device_tag_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return device_tag_->Add(value);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::set_device_tag(::std::size_t index, const ::std::string& value) {
  if (!device_tag_) {
    device_tag_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return device_tag_->Set(index, value);
}

// repeated field required
::std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::required_size() const {
  if (!required_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return required_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::required() const {
  if (!required_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(required_.get());
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::required(::std::size_t index) const {
  if (!required_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return required_->Get(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::clear_required() {
  if (!required_) {
    required_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return required_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_required() {
  if (!required_) {
    required_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  required_.get();
}
::std::string* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_required(::std::size_t index) {
  if (!required_) {
    required_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  required_->Mutable(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::add_required(const ::std::string& value) {
  if (!required_) {
    required_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return required_->Add(value);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::set_required(::std::size_t index, const ::std::string& value) {
  if (!required_) {
    required_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return required_->Set(index, value);
}

// repeated field available
::std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::available_size() const {
  if (!available_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return available_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::available() const {
  if (!available_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(available_.get());
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::available(::std::size_t index) const {
  if (!available_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return available_->Get(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::clear_available() {
  if (!available_) {
    available_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return available_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_available() {
  if (!available_) {
    available_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  available_.get();
}
::std::string* ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::mutable_available(::std::size_t index) {
  if (!available_) {
    available_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  available_->Mutable(index);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::add_available(const ::std::string& value) {
  if (!available_) {
    available_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return available_->Add(value);
}
void ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::set_available(::std::size_t index, const ::std::string& value) {
  if (!available_) {
    available_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return available_->Set(index, value);
}


int ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::compare(const _MemoryZoneOutOfMemoryError_& other) {
  if (!(machine_id() == other.machine_id())) {
    return machine_id() < other.machine_id() ? -1 : 1;
  }
  if (!(mem_zone_id() == other.mem_zone_id())) {
    return mem_zone_id() < other.mem_zone_id() ? -1 : 1;
  }
  if (!(device_tag() == other.device_tag())) {
    return device_tag() < other.device_tag() ? -1 : 1;
  }
  if (!(required() == other.required())) {
    return required() < other.required() ? -1 : 1;
  }
  if (!(available() == other.available())) {
    return available() < other.available() ? -1 : 1;
  }
  return 0;
}

bool ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::operator==(const _MemoryZoneOutOfMemoryError_& other) const {
  return true
    && machine_id() == other.machine_id()
    && mem_zone_id() == other.mem_zone_id()
    && device_tag() == other.device_tag()
    && required() == other.required()
    && available() == other.available()
  ;
}

std::size_t ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::__CalcHash__() const {
  return 0
    ^ machine_id().__CalcHash__()
    ^ mem_zone_id().__CalcHash__()
    ^ device_tag().__CalcHash__()
    ^ required().__CalcHash__()
    ^ available().__CalcHash__()
  ;
}

bool ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_::operator<(const _MemoryZoneOutOfMemoryError_& other) const {
  return false
    || !(machine_id() == other.machine_id()) ? 
      machine_id() < other.machine_id() : false
    || !(mem_zone_id() == other.mem_zone_id()) ? 
      mem_zone_id() < other.mem_zone_id() : false
    || !(device_tag() == other.device_tag()) ? 
      device_tag() < other.device_tag() : false
    || !(required() == other.required()) ? 
      required() < other.required() : false
    || !(available() == other.available()) ? 
      available() < other.available() : false
  ;
}

using _MemoryZoneOutOfMemoryError_ =  ConstMemoryZoneOutOfMemoryError::_MemoryZoneOutOfMemoryError_;
ConstMemoryZoneOutOfMemoryError::ConstMemoryZoneOutOfMemoryError(const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& data): data_(data) {}
ConstMemoryZoneOutOfMemoryError::ConstMemoryZoneOutOfMemoryError(): data_(::std::make_shared<_MemoryZoneOutOfMemoryError_>()) {}
ConstMemoryZoneOutOfMemoryError::ConstMemoryZoneOutOfMemoryError(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror) {
  BuildFromProto(proto_memoryzoneoutofmemoryerror);
}
ConstMemoryZoneOutOfMemoryError::ConstMemoryZoneOutOfMemoryError(const ConstMemoryZoneOutOfMemoryError&) = default;
ConstMemoryZoneOutOfMemoryError::ConstMemoryZoneOutOfMemoryError(ConstMemoryZoneOutOfMemoryError&&) noexcept = default;
ConstMemoryZoneOutOfMemoryError::~ConstMemoryZoneOutOfMemoryError() = default;

void ConstMemoryZoneOutOfMemoryError::ToProto(PbMessage* proto_memoryzoneoutofmemoryerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MemoryZoneOutOfMemoryError*>(proto_memoryzoneoutofmemoryerror));
}
  
::std::string ConstMemoryZoneOutOfMemoryError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMemoryZoneOutOfMemoryError::__Empty__() const {
  return !data_;
}

int ConstMemoryZoneOutOfMemoryError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"machine_id", 1},
    {"mem_zone_id", 2},
    {"device_tag", 3},
    {"required", 4},
    {"available", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMemoryZoneOutOfMemoryError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMemoryZoneOutOfMemoryError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMemoryZoneOutOfMemoryError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &machine_id();
    case 2: return &mem_zone_id();
    case 3: return &device_tag();
    case 4: return &required();
    case 5: return &available();
    default: return nullptr;
  }
}

// repeated field machine_id
::std::size_t ConstMemoryZoneOutOfMemoryError::machine_id_size() const {
  return __SharedPtrOrDefault__()->machine_id_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::machine_id() const {
  return __SharedPtrOrDefault__()->machine_id();
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::machine_id(::std::size_t index) const {
  return __SharedPtrOrDefault__()->machine_id(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMemoryZoneOutOfMemoryError::shared_const_machine_id() const {
  return machine_id().__SharedConst__();
}
// repeated field mem_zone_id
::std::size_t ConstMemoryZoneOutOfMemoryError::mem_zone_id_size() const {
  return __SharedPtrOrDefault__()->mem_zone_id_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::mem_zone_id() const {
  return __SharedPtrOrDefault__()->mem_zone_id();
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::mem_zone_id(::std::size_t index) const {
  return __SharedPtrOrDefault__()->mem_zone_id(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMemoryZoneOutOfMemoryError::shared_const_mem_zone_id() const {
  return mem_zone_id().__SharedConst__();
}
// repeated field device_tag
::std::size_t ConstMemoryZoneOutOfMemoryError::device_tag_size() const {
  return __SharedPtrOrDefault__()->device_tag_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::device_tag() const {
  return __SharedPtrOrDefault__()->device_tag();
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::device_tag(::std::size_t index) const {
  return __SharedPtrOrDefault__()->device_tag(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMemoryZoneOutOfMemoryError::shared_const_device_tag() const {
  return device_tag().__SharedConst__();
}
// repeated field required
::std::size_t ConstMemoryZoneOutOfMemoryError::required_size() const {
  return __SharedPtrOrDefault__()->required_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::required() const {
  return __SharedPtrOrDefault__()->required();
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::required(::std::size_t index) const {
  return __SharedPtrOrDefault__()->required(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMemoryZoneOutOfMemoryError::shared_const_required() const {
  return required().__SharedConst__();
}
// repeated field available
::std::size_t ConstMemoryZoneOutOfMemoryError::available_size() const {
  return __SharedPtrOrDefault__()->available_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstMemoryZoneOutOfMemoryError::available() const {
  return __SharedPtrOrDefault__()->available();
}
const ::std::string& ConstMemoryZoneOutOfMemoryError::available(::std::size_t index) const {
  return __SharedPtrOrDefault__()->available(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstMemoryZoneOutOfMemoryError::shared_const_available() const {
  return available().__SharedConst__();
}

::std::shared_ptr<ConstMemoryZoneOutOfMemoryError> ConstMemoryZoneOutOfMemoryError::__SharedConst__() const {
  return ::std::make_shared<ConstMemoryZoneOutOfMemoryError>(data_);
}
int64_t ConstMemoryZoneOutOfMemoryError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMemoryZoneOutOfMemoryError::operator==(const ConstMemoryZoneOutOfMemoryError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMemoryZoneOutOfMemoryError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMemoryZoneOutOfMemoryError::operator<(const ConstMemoryZoneOutOfMemoryError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& ConstMemoryZoneOutOfMemoryError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_> default_ptr = std::make_shared<_MemoryZoneOutOfMemoryError_>();
  return default_ptr;
}
const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& ConstMemoryZoneOutOfMemoryError::__SharedPtr__() {
  if (!data_) { data_.reset(new _MemoryZoneOutOfMemoryError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMemoryZoneOutOfMemoryError
void ConstMemoryZoneOutOfMemoryError::BuildFromProto(const PbMessage& proto_memoryzoneoutofmemoryerror) {
  data_ = ::std::make_shared<_MemoryZoneOutOfMemoryError_>(dynamic_cast<const ::oneflow::MemoryZoneOutOfMemoryError&>(proto_memoryzoneoutofmemoryerror));
}

MemoryZoneOutOfMemoryError::MemoryZoneOutOfMemoryError(const ::std::shared_ptr<_MemoryZoneOutOfMemoryError_>& data)
  : ConstMemoryZoneOutOfMemoryError(data) {}
MemoryZoneOutOfMemoryError::MemoryZoneOutOfMemoryError(const MemoryZoneOutOfMemoryError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MemoryZoneOutOfMemoryError> resize
MemoryZoneOutOfMemoryError::MemoryZoneOutOfMemoryError(MemoryZoneOutOfMemoryError&&) noexcept = default; 
MemoryZoneOutOfMemoryError::MemoryZoneOutOfMemoryError(const ::oneflow::MemoryZoneOutOfMemoryError& proto_memoryzoneoutofmemoryerror) {
  InitFromProto(proto_memoryzoneoutofmemoryerror);
}
MemoryZoneOutOfMemoryError::MemoryZoneOutOfMemoryError() = default;

MemoryZoneOutOfMemoryError::~MemoryZoneOutOfMemoryError() = default;

void MemoryZoneOutOfMemoryError::InitFromProto(const PbMessage& proto_memoryzoneoutofmemoryerror) {
  BuildFromProto(proto_memoryzoneoutofmemoryerror);
}
  
void* MemoryZoneOutOfMemoryError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_machine_id();
    case 2: return mutable_mem_zone_id();
    case 3: return mutable_device_tag();
    case 4: return mutable_required();
    case 5: return mutable_available();
    default: return nullptr;
  }
}

bool MemoryZoneOutOfMemoryError::operator==(const MemoryZoneOutOfMemoryError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MemoryZoneOutOfMemoryError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MemoryZoneOutOfMemoryError::operator<(const MemoryZoneOutOfMemoryError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MemoryZoneOutOfMemoryError::Clear() {
  if (data_) { data_.reset(); }
}
void MemoryZoneOutOfMemoryError::CopyFrom(const MemoryZoneOutOfMemoryError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MemoryZoneOutOfMemoryError& MemoryZoneOutOfMemoryError::operator=(const MemoryZoneOutOfMemoryError& other) {
  CopyFrom(other);
  return *this;
}

// repeated field machine_id
void MemoryZoneOutOfMemoryError::clear_machine_id() {
  return __SharedPtr__()->clear_machine_id();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MemoryZoneOutOfMemoryError::mutable_machine_id() {
  return __SharedPtr__()->mutable_machine_id();
}
::std::string* MemoryZoneOutOfMemoryError::mutable_machine_id(::std::size_t index) {
  return __SharedPtr__()->mutable_machine_id(index);
}
void MemoryZoneOutOfMemoryError::add_machine_id(const ::std::string& value) {
  return __SharedPtr__()->add_machine_id(value);
}
void MemoryZoneOutOfMemoryError::set_machine_id(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_machine_id(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MemoryZoneOutOfMemoryError::shared_mutable_machine_id() {
  return mutable_machine_id()->__SharedMutable__();
}
// repeated field mem_zone_id
void MemoryZoneOutOfMemoryError::clear_mem_zone_id() {
  return __SharedPtr__()->clear_mem_zone_id();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MemoryZoneOutOfMemoryError::mutable_mem_zone_id() {
  return __SharedPtr__()->mutable_mem_zone_id();
}
::std::string* MemoryZoneOutOfMemoryError::mutable_mem_zone_id(::std::size_t index) {
  return __SharedPtr__()->mutable_mem_zone_id(index);
}
void MemoryZoneOutOfMemoryError::add_mem_zone_id(const ::std::string& value) {
  return __SharedPtr__()->add_mem_zone_id(value);
}
void MemoryZoneOutOfMemoryError::set_mem_zone_id(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_mem_zone_id(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MemoryZoneOutOfMemoryError::shared_mutable_mem_zone_id() {
  return mutable_mem_zone_id()->__SharedMutable__();
}
// repeated field device_tag
void MemoryZoneOutOfMemoryError::clear_device_tag() {
  return __SharedPtr__()->clear_device_tag();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MemoryZoneOutOfMemoryError::mutable_device_tag() {
  return __SharedPtr__()->mutable_device_tag();
}
::std::string* MemoryZoneOutOfMemoryError::mutable_device_tag(::std::size_t index) {
  return __SharedPtr__()->mutable_device_tag(index);
}
void MemoryZoneOutOfMemoryError::add_device_tag(const ::std::string& value) {
  return __SharedPtr__()->add_device_tag(value);
}
void MemoryZoneOutOfMemoryError::set_device_tag(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_device_tag(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MemoryZoneOutOfMemoryError::shared_mutable_device_tag() {
  return mutable_device_tag()->__SharedMutable__();
}
// repeated field required
void MemoryZoneOutOfMemoryError::clear_required() {
  return __SharedPtr__()->clear_required();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MemoryZoneOutOfMemoryError::mutable_required() {
  return __SharedPtr__()->mutable_required();
}
::std::string* MemoryZoneOutOfMemoryError::mutable_required(::std::size_t index) {
  return __SharedPtr__()->mutable_required(index);
}
void MemoryZoneOutOfMemoryError::add_required(const ::std::string& value) {
  return __SharedPtr__()->add_required(value);
}
void MemoryZoneOutOfMemoryError::set_required(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_required(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MemoryZoneOutOfMemoryError::shared_mutable_required() {
  return mutable_required()->__SharedMutable__();
}
// repeated field available
void MemoryZoneOutOfMemoryError::clear_available() {
  return __SharedPtr__()->clear_available();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* MemoryZoneOutOfMemoryError::mutable_available() {
  return __SharedPtr__()->mutable_available();
}
::std::string* MemoryZoneOutOfMemoryError::mutable_available(::std::size_t index) {
  return __SharedPtr__()->mutable_available(index);
}
void MemoryZoneOutOfMemoryError::add_available(const ::std::string& value) {
  return __SharedPtr__()->add_available(value);
}
void MemoryZoneOutOfMemoryError::set_available(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_available(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> MemoryZoneOutOfMemoryError::shared_mutable_available() {
  return mutable_available()->__SharedMutable__();
}

::std::shared_ptr<MemoryZoneOutOfMemoryError> MemoryZoneOutOfMemoryError::__SharedMutable__() {
  return ::std::make_shared<MemoryZoneOutOfMemoryError>(__SharedPtr__());
}
ConstLossBlobNotFoundError::_LossBlobNotFoundError_::_LossBlobNotFoundError_() { Clear(); }
ConstLossBlobNotFoundError::_LossBlobNotFoundError_::_LossBlobNotFoundError_(const _LossBlobNotFoundError_& other) { CopyFrom(other); }
ConstLossBlobNotFoundError::_LossBlobNotFoundError_::_LossBlobNotFoundError_(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror) {
  InitFromProto(proto_lossblobnotfounderror);
}
ConstLossBlobNotFoundError::_LossBlobNotFoundError_::_LossBlobNotFoundError_(_LossBlobNotFoundError_&& other) = default;
ConstLossBlobNotFoundError::_LossBlobNotFoundError_::~_LossBlobNotFoundError_() = default;

void ConstLossBlobNotFoundError::_LossBlobNotFoundError_::InitFromProto(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror) {
  Clear();
    
}

void ConstLossBlobNotFoundError::_LossBlobNotFoundError_::ToProto(::oneflow::LossBlobNotFoundError* proto_lossblobnotfounderror) const {
  proto_lossblobnotfounderror->Clear();

}

::std::string ConstLossBlobNotFoundError::_LossBlobNotFoundError_::DebugString() const {
  ::oneflow::LossBlobNotFoundError proto_lossblobnotfounderror;
  this->ToProto(&proto_lossblobnotfounderror);
  return proto_lossblobnotfounderror.DebugString();
}

void ConstLossBlobNotFoundError::_LossBlobNotFoundError_::Clear() {
}

void ConstLossBlobNotFoundError::_LossBlobNotFoundError_::CopyFrom(const _LossBlobNotFoundError_& other) {
}



int ConstLossBlobNotFoundError::_LossBlobNotFoundError_::compare(const _LossBlobNotFoundError_& other) {
  return 0;
}

bool ConstLossBlobNotFoundError::_LossBlobNotFoundError_::operator==(const _LossBlobNotFoundError_& other) const {
  return true
  ;
}

std::size_t ConstLossBlobNotFoundError::_LossBlobNotFoundError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstLossBlobNotFoundError::_LossBlobNotFoundError_::operator<(const _LossBlobNotFoundError_& other) const {
  return false
  ;
}

using _LossBlobNotFoundError_ =  ConstLossBlobNotFoundError::_LossBlobNotFoundError_;
ConstLossBlobNotFoundError::ConstLossBlobNotFoundError(const ::std::shared_ptr<_LossBlobNotFoundError_>& data): data_(data) {}
ConstLossBlobNotFoundError::ConstLossBlobNotFoundError(): data_(::std::make_shared<_LossBlobNotFoundError_>()) {}
ConstLossBlobNotFoundError::ConstLossBlobNotFoundError(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror) {
  BuildFromProto(proto_lossblobnotfounderror);
}
ConstLossBlobNotFoundError::ConstLossBlobNotFoundError(const ConstLossBlobNotFoundError&) = default;
ConstLossBlobNotFoundError::ConstLossBlobNotFoundError(ConstLossBlobNotFoundError&&) noexcept = default;
ConstLossBlobNotFoundError::~ConstLossBlobNotFoundError() = default;

void ConstLossBlobNotFoundError::ToProto(PbMessage* proto_lossblobnotfounderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LossBlobNotFoundError*>(proto_lossblobnotfounderror));
}
  
::std::string ConstLossBlobNotFoundError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLossBlobNotFoundError::__Empty__() const {
  return !data_;
}

int ConstLossBlobNotFoundError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstLossBlobNotFoundError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstLossBlobNotFoundError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLossBlobNotFoundError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstLossBlobNotFoundError> ConstLossBlobNotFoundError::__SharedConst__() const {
  return ::std::make_shared<ConstLossBlobNotFoundError>(data_);
}
int64_t ConstLossBlobNotFoundError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLossBlobNotFoundError::operator==(const ConstLossBlobNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLossBlobNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLossBlobNotFoundError::operator<(const ConstLossBlobNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LossBlobNotFoundError_>& ConstLossBlobNotFoundError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LossBlobNotFoundError_> default_ptr = std::make_shared<_LossBlobNotFoundError_>();
  return default_ptr;
}
const ::std::shared_ptr<_LossBlobNotFoundError_>& ConstLossBlobNotFoundError::__SharedPtr__() {
  if (!data_) { data_.reset(new _LossBlobNotFoundError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLossBlobNotFoundError
void ConstLossBlobNotFoundError::BuildFromProto(const PbMessage& proto_lossblobnotfounderror) {
  data_ = ::std::make_shared<_LossBlobNotFoundError_>(dynamic_cast<const ::oneflow::LossBlobNotFoundError&>(proto_lossblobnotfounderror));
}

LossBlobNotFoundError::LossBlobNotFoundError(const ::std::shared_ptr<_LossBlobNotFoundError_>& data)
  : ConstLossBlobNotFoundError(data) {}
LossBlobNotFoundError::LossBlobNotFoundError(const LossBlobNotFoundError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LossBlobNotFoundError> resize
LossBlobNotFoundError::LossBlobNotFoundError(LossBlobNotFoundError&&) noexcept = default; 
LossBlobNotFoundError::LossBlobNotFoundError(const ::oneflow::LossBlobNotFoundError& proto_lossblobnotfounderror) {
  InitFromProto(proto_lossblobnotfounderror);
}
LossBlobNotFoundError::LossBlobNotFoundError() = default;

LossBlobNotFoundError::~LossBlobNotFoundError() = default;

void LossBlobNotFoundError::InitFromProto(const PbMessage& proto_lossblobnotfounderror) {
  BuildFromProto(proto_lossblobnotfounderror);
}
  
void* LossBlobNotFoundError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool LossBlobNotFoundError::operator==(const LossBlobNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LossBlobNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LossBlobNotFoundError::operator<(const LossBlobNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LossBlobNotFoundError::Clear() {
  if (data_) { data_.reset(); }
}
void LossBlobNotFoundError::CopyFrom(const LossBlobNotFoundError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LossBlobNotFoundError& LossBlobNotFoundError::operator=(const LossBlobNotFoundError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<LossBlobNotFoundError> LossBlobNotFoundError::__SharedMutable__() {
  return ::std::make_shared<LossBlobNotFoundError>(__SharedPtr__());
}
ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::_RwMutexedObjectNotFoundError_() { Clear(); }
ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::_RwMutexedObjectNotFoundError_(const _RwMutexedObjectNotFoundError_& other) { CopyFrom(other); }
ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::_RwMutexedObjectNotFoundError_(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror) {
  InitFromProto(proto_rwmutexedobjectnotfounderror);
}
ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::_RwMutexedObjectNotFoundError_(_RwMutexedObjectNotFoundError_&& other) = default;
ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::~_RwMutexedObjectNotFoundError_() = default;

void ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::InitFromProto(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror) {
  Clear();
    
}

void ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::ToProto(::oneflow::RwMutexedObjectNotFoundError* proto_rwmutexedobjectnotfounderror) const {
  proto_rwmutexedobjectnotfounderror->Clear();

}

::std::string ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::DebugString() const {
  ::oneflow::RwMutexedObjectNotFoundError proto_rwmutexedobjectnotfounderror;
  this->ToProto(&proto_rwmutexedobjectnotfounderror);
  return proto_rwmutexedobjectnotfounderror.DebugString();
}

void ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::Clear() {
}

void ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::CopyFrom(const _RwMutexedObjectNotFoundError_& other) {
}



int ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::compare(const _RwMutexedObjectNotFoundError_& other) {
  return 0;
}

bool ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::operator==(const _RwMutexedObjectNotFoundError_& other) const {
  return true
  ;
}

std::size_t ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_::operator<(const _RwMutexedObjectNotFoundError_& other) const {
  return false
  ;
}

using _RwMutexedObjectNotFoundError_ =  ConstRwMutexedObjectNotFoundError::_RwMutexedObjectNotFoundError_;
ConstRwMutexedObjectNotFoundError::ConstRwMutexedObjectNotFoundError(const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& data): data_(data) {}
ConstRwMutexedObjectNotFoundError::ConstRwMutexedObjectNotFoundError(): data_(::std::make_shared<_RwMutexedObjectNotFoundError_>()) {}
ConstRwMutexedObjectNotFoundError::ConstRwMutexedObjectNotFoundError(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror) {
  BuildFromProto(proto_rwmutexedobjectnotfounderror);
}
ConstRwMutexedObjectNotFoundError::ConstRwMutexedObjectNotFoundError(const ConstRwMutexedObjectNotFoundError&) = default;
ConstRwMutexedObjectNotFoundError::ConstRwMutexedObjectNotFoundError(ConstRwMutexedObjectNotFoundError&&) noexcept = default;
ConstRwMutexedObjectNotFoundError::~ConstRwMutexedObjectNotFoundError() = default;

void ConstRwMutexedObjectNotFoundError::ToProto(PbMessage* proto_rwmutexedobjectnotfounderror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RwMutexedObjectNotFoundError*>(proto_rwmutexedobjectnotfounderror));
}
  
::std::string ConstRwMutexedObjectNotFoundError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRwMutexedObjectNotFoundError::__Empty__() const {
  return !data_;
}

int ConstRwMutexedObjectNotFoundError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstRwMutexedObjectNotFoundError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstRwMutexedObjectNotFoundError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRwMutexedObjectNotFoundError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstRwMutexedObjectNotFoundError> ConstRwMutexedObjectNotFoundError::__SharedConst__() const {
  return ::std::make_shared<ConstRwMutexedObjectNotFoundError>(data_);
}
int64_t ConstRwMutexedObjectNotFoundError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRwMutexedObjectNotFoundError::operator==(const ConstRwMutexedObjectNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRwMutexedObjectNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRwMutexedObjectNotFoundError::operator<(const ConstRwMutexedObjectNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& ConstRwMutexedObjectNotFoundError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RwMutexedObjectNotFoundError_> default_ptr = std::make_shared<_RwMutexedObjectNotFoundError_>();
  return default_ptr;
}
const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& ConstRwMutexedObjectNotFoundError::__SharedPtr__() {
  if (!data_) { data_.reset(new _RwMutexedObjectNotFoundError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRwMutexedObjectNotFoundError
void ConstRwMutexedObjectNotFoundError::BuildFromProto(const PbMessage& proto_rwmutexedobjectnotfounderror) {
  data_ = ::std::make_shared<_RwMutexedObjectNotFoundError_>(dynamic_cast<const ::oneflow::RwMutexedObjectNotFoundError&>(proto_rwmutexedobjectnotfounderror));
}

RwMutexedObjectNotFoundError::RwMutexedObjectNotFoundError(const ::std::shared_ptr<_RwMutexedObjectNotFoundError_>& data)
  : ConstRwMutexedObjectNotFoundError(data) {}
RwMutexedObjectNotFoundError::RwMutexedObjectNotFoundError(const RwMutexedObjectNotFoundError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RwMutexedObjectNotFoundError> resize
RwMutexedObjectNotFoundError::RwMutexedObjectNotFoundError(RwMutexedObjectNotFoundError&&) noexcept = default; 
RwMutexedObjectNotFoundError::RwMutexedObjectNotFoundError(const ::oneflow::RwMutexedObjectNotFoundError& proto_rwmutexedobjectnotfounderror) {
  InitFromProto(proto_rwmutexedobjectnotfounderror);
}
RwMutexedObjectNotFoundError::RwMutexedObjectNotFoundError() = default;

RwMutexedObjectNotFoundError::~RwMutexedObjectNotFoundError() = default;

void RwMutexedObjectNotFoundError::InitFromProto(const PbMessage& proto_rwmutexedobjectnotfounderror) {
  BuildFromProto(proto_rwmutexedobjectnotfounderror);
}
  
void* RwMutexedObjectNotFoundError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool RwMutexedObjectNotFoundError::operator==(const RwMutexedObjectNotFoundError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RwMutexedObjectNotFoundError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RwMutexedObjectNotFoundError::operator<(const RwMutexedObjectNotFoundError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RwMutexedObjectNotFoundError::Clear() {
  if (data_) { data_.reset(); }
}
void RwMutexedObjectNotFoundError::CopyFrom(const RwMutexedObjectNotFoundError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RwMutexedObjectNotFoundError& RwMutexedObjectNotFoundError::operator=(const RwMutexedObjectNotFoundError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<RwMutexedObjectNotFoundError> RwMutexedObjectNotFoundError::__SharedMutable__() {
  return ::std::make_shared<RwMutexedObjectNotFoundError>(__SharedPtr__());
}
ConstUnknownError::_UnknownError_::_UnknownError_() { Clear(); }
ConstUnknownError::_UnknownError_::_UnknownError_(const _UnknownError_& other) { CopyFrom(other); }
ConstUnknownError::_UnknownError_::_UnknownError_(const ::oneflow::UnknownError& proto_unknownerror) {
  InitFromProto(proto_unknownerror);
}
ConstUnknownError::_UnknownError_::_UnknownError_(_UnknownError_&& other) = default;
ConstUnknownError::_UnknownError_::~_UnknownError_() = default;

void ConstUnknownError::_UnknownError_::InitFromProto(const ::oneflow::UnknownError& proto_unknownerror) {
  Clear();
    
}

void ConstUnknownError::_UnknownError_::ToProto(::oneflow::UnknownError* proto_unknownerror) const {
  proto_unknownerror->Clear();

}

::std::string ConstUnknownError::_UnknownError_::DebugString() const {
  ::oneflow::UnknownError proto_unknownerror;
  this->ToProto(&proto_unknownerror);
  return proto_unknownerror.DebugString();
}

void ConstUnknownError::_UnknownError_::Clear() {
}

void ConstUnknownError::_UnknownError_::CopyFrom(const _UnknownError_& other) {
}



int ConstUnknownError::_UnknownError_::compare(const _UnknownError_& other) {
  return 0;
}

bool ConstUnknownError::_UnknownError_::operator==(const _UnknownError_& other) const {
  return true
  ;
}

std::size_t ConstUnknownError::_UnknownError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstUnknownError::_UnknownError_::operator<(const _UnknownError_& other) const {
  return false
  ;
}

using _UnknownError_ =  ConstUnknownError::_UnknownError_;
ConstUnknownError::ConstUnknownError(const ::std::shared_ptr<_UnknownError_>& data): data_(data) {}
ConstUnknownError::ConstUnknownError(): data_(::std::make_shared<_UnknownError_>()) {}
ConstUnknownError::ConstUnknownError(const ::oneflow::UnknownError& proto_unknownerror) {
  BuildFromProto(proto_unknownerror);
}
ConstUnknownError::ConstUnknownError(const ConstUnknownError&) = default;
ConstUnknownError::ConstUnknownError(ConstUnknownError&&) noexcept = default;
ConstUnknownError::~ConstUnknownError() = default;

void ConstUnknownError::ToProto(PbMessage* proto_unknownerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::UnknownError*>(proto_unknownerror));
}
  
::std::string ConstUnknownError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstUnknownError::__Empty__() const {
  return !data_;
}

int ConstUnknownError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstUnknownError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstUnknownError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstUnknownError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstUnknownError> ConstUnknownError::__SharedConst__() const {
  return ::std::make_shared<ConstUnknownError>(data_);
}
int64_t ConstUnknownError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstUnknownError::operator==(const ConstUnknownError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstUnknownError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstUnknownError::operator<(const ConstUnknownError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_UnknownError_>& ConstUnknownError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_UnknownError_> default_ptr = std::make_shared<_UnknownError_>();
  return default_ptr;
}
const ::std::shared_ptr<_UnknownError_>& ConstUnknownError::__SharedPtr__() {
  if (!data_) { data_.reset(new _UnknownError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstUnknownError
void ConstUnknownError::BuildFromProto(const PbMessage& proto_unknownerror) {
  data_ = ::std::make_shared<_UnknownError_>(dynamic_cast<const ::oneflow::UnknownError&>(proto_unknownerror));
}

UnknownError::UnknownError(const ::std::shared_ptr<_UnknownError_>& data)
  : ConstUnknownError(data) {}
UnknownError::UnknownError(const UnknownError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<UnknownError> resize
UnknownError::UnknownError(UnknownError&&) noexcept = default; 
UnknownError::UnknownError(const ::oneflow::UnknownError& proto_unknownerror) {
  InitFromProto(proto_unknownerror);
}
UnknownError::UnknownError() = default;

UnknownError::~UnknownError() = default;

void UnknownError::InitFromProto(const PbMessage& proto_unknownerror) {
  BuildFromProto(proto_unknownerror);
}
  
void* UnknownError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool UnknownError::operator==(const UnknownError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t UnknownError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool UnknownError::operator<(const UnknownError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void UnknownError::Clear() {
  if (data_) { data_.reset(); }
}
void UnknownError::CopyFrom(const UnknownError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
UnknownError& UnknownError::operator=(const UnknownError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<UnknownError> UnknownError::__SharedMutable__() {
  return ::std::make_shared<UnknownError>(__SharedPtr__());
}
ConstCompileOptionWrongError::_CompileOptionWrongError_::_CompileOptionWrongError_() { Clear(); }
ConstCompileOptionWrongError::_CompileOptionWrongError_::_CompileOptionWrongError_(const _CompileOptionWrongError_& other) { CopyFrom(other); }
ConstCompileOptionWrongError::_CompileOptionWrongError_::_CompileOptionWrongError_(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror) {
  InitFromProto(proto_compileoptionwrongerror);
}
ConstCompileOptionWrongError::_CompileOptionWrongError_::_CompileOptionWrongError_(_CompileOptionWrongError_&& other) = default;
ConstCompileOptionWrongError::_CompileOptionWrongError_::~_CompileOptionWrongError_() = default;

void ConstCompileOptionWrongError::_CompileOptionWrongError_::InitFromProto(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror) {
  Clear();
    
}

void ConstCompileOptionWrongError::_CompileOptionWrongError_::ToProto(::oneflow::CompileOptionWrongError* proto_compileoptionwrongerror) const {
  proto_compileoptionwrongerror->Clear();

}

::std::string ConstCompileOptionWrongError::_CompileOptionWrongError_::DebugString() const {
  ::oneflow::CompileOptionWrongError proto_compileoptionwrongerror;
  this->ToProto(&proto_compileoptionwrongerror);
  return proto_compileoptionwrongerror.DebugString();
}

void ConstCompileOptionWrongError::_CompileOptionWrongError_::Clear() {
}

void ConstCompileOptionWrongError::_CompileOptionWrongError_::CopyFrom(const _CompileOptionWrongError_& other) {
}



int ConstCompileOptionWrongError::_CompileOptionWrongError_::compare(const _CompileOptionWrongError_& other) {
  return 0;
}

bool ConstCompileOptionWrongError::_CompileOptionWrongError_::operator==(const _CompileOptionWrongError_& other) const {
  return true
  ;
}

std::size_t ConstCompileOptionWrongError::_CompileOptionWrongError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstCompileOptionWrongError::_CompileOptionWrongError_::operator<(const _CompileOptionWrongError_& other) const {
  return false
  ;
}

using _CompileOptionWrongError_ =  ConstCompileOptionWrongError::_CompileOptionWrongError_;
ConstCompileOptionWrongError::ConstCompileOptionWrongError(const ::std::shared_ptr<_CompileOptionWrongError_>& data): data_(data) {}
ConstCompileOptionWrongError::ConstCompileOptionWrongError(): data_(::std::make_shared<_CompileOptionWrongError_>()) {}
ConstCompileOptionWrongError::ConstCompileOptionWrongError(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror) {
  BuildFromProto(proto_compileoptionwrongerror);
}
ConstCompileOptionWrongError::ConstCompileOptionWrongError(const ConstCompileOptionWrongError&) = default;
ConstCompileOptionWrongError::ConstCompileOptionWrongError(ConstCompileOptionWrongError&&) noexcept = default;
ConstCompileOptionWrongError::~ConstCompileOptionWrongError() = default;

void ConstCompileOptionWrongError::ToProto(PbMessage* proto_compileoptionwrongerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::CompileOptionWrongError*>(proto_compileoptionwrongerror));
}
  
::std::string ConstCompileOptionWrongError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCompileOptionWrongError::__Empty__() const {
  return !data_;
}

int ConstCompileOptionWrongError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstCompileOptionWrongError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstCompileOptionWrongError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCompileOptionWrongError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstCompileOptionWrongError> ConstCompileOptionWrongError::__SharedConst__() const {
  return ::std::make_shared<ConstCompileOptionWrongError>(data_);
}
int64_t ConstCompileOptionWrongError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCompileOptionWrongError::operator==(const ConstCompileOptionWrongError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCompileOptionWrongError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCompileOptionWrongError::operator<(const ConstCompileOptionWrongError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CompileOptionWrongError_>& ConstCompileOptionWrongError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CompileOptionWrongError_> default_ptr = std::make_shared<_CompileOptionWrongError_>();
  return default_ptr;
}
const ::std::shared_ptr<_CompileOptionWrongError_>& ConstCompileOptionWrongError::__SharedPtr__() {
  if (!data_) { data_.reset(new _CompileOptionWrongError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCompileOptionWrongError
void ConstCompileOptionWrongError::BuildFromProto(const PbMessage& proto_compileoptionwrongerror) {
  data_ = ::std::make_shared<_CompileOptionWrongError_>(dynamic_cast<const ::oneflow::CompileOptionWrongError&>(proto_compileoptionwrongerror));
}

CompileOptionWrongError::CompileOptionWrongError(const ::std::shared_ptr<_CompileOptionWrongError_>& data)
  : ConstCompileOptionWrongError(data) {}
CompileOptionWrongError::CompileOptionWrongError(const CompileOptionWrongError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CompileOptionWrongError> resize
CompileOptionWrongError::CompileOptionWrongError(CompileOptionWrongError&&) noexcept = default; 
CompileOptionWrongError::CompileOptionWrongError(const ::oneflow::CompileOptionWrongError& proto_compileoptionwrongerror) {
  InitFromProto(proto_compileoptionwrongerror);
}
CompileOptionWrongError::CompileOptionWrongError() = default;

CompileOptionWrongError::~CompileOptionWrongError() = default;

void CompileOptionWrongError::InitFromProto(const PbMessage& proto_compileoptionwrongerror) {
  BuildFromProto(proto_compileoptionwrongerror);
}
  
void* CompileOptionWrongError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool CompileOptionWrongError::operator==(const CompileOptionWrongError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CompileOptionWrongError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CompileOptionWrongError::operator<(const CompileOptionWrongError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CompileOptionWrongError::Clear() {
  if (data_) { data_.reset(); }
}
void CompileOptionWrongError::CopyFrom(const CompileOptionWrongError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CompileOptionWrongError& CompileOptionWrongError::operator=(const CompileOptionWrongError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<CompileOptionWrongError> CompileOptionWrongError::__SharedMutable__() {
  return ::std::make_shared<CompileOptionWrongError>(__SharedPtr__());
}
ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::_InputDeviceNotMatchError_() { Clear(); }
ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::_InputDeviceNotMatchError_(const _InputDeviceNotMatchError_& other) { CopyFrom(other); }
ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::_InputDeviceNotMatchError_(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror) {
  InitFromProto(proto_inputdevicenotmatcherror);
}
ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::_InputDeviceNotMatchError_(_InputDeviceNotMatchError_&& other) = default;
ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::~_InputDeviceNotMatchError_() = default;

void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::InitFromProto(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror) {
  Clear();
  // repeated field: info
  if (!proto_inputdevicenotmatcherror.info().empty()) {
    for (const ::std::string& elem : proto_inputdevicenotmatcherror.info()) {
      add_info(elem);
    }
  }
    
}

void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::ToProto(::oneflow::InputDeviceNotMatchError* proto_inputdevicenotmatcherror) const {
  proto_inputdevicenotmatcherror->Clear();
  // repeated field: info
  if (!info().empty()) {
    for (const ::std::string& elem : info()) {
      proto_inputdevicenotmatcherror->add_info(elem);
    }
  }

}

::std::string ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::DebugString() const {
  ::oneflow::InputDeviceNotMatchError proto_inputdevicenotmatcherror;
  this->ToProto(&proto_inputdevicenotmatcherror);
  return proto_inputdevicenotmatcherror.DebugString();
}

void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::Clear() {
  clear_info();
}

void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::CopyFrom(const _InputDeviceNotMatchError_& other) {
  mutable_info()->CopyFrom(other.info());
}


// repeated field info
::std::size_t ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::info_size() const {
  if (!info_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return info_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::info() const {
  if (!info_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(info_.get());
}
const ::std::string& ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::info(::std::size_t index) const {
  if (!info_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return info_->Get(index);
}
void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::clear_info() {
  if (!info_) {
    info_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return info_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::mutable_info() {
  if (!info_) {
    info_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  info_.get();
}
::std::string* ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::mutable_info(::std::size_t index) {
  if (!info_) {
    info_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return  info_->Mutable(index);
}
void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::add_info(const ::std::string& value) {
  if (!info_) {
    info_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return info_->Add(value);
}
void ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::set_info(::std::size_t index, const ::std::string& value) {
  if (!info_) {
    info_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>();
  }
  return info_->Set(index, value);
}


int ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::compare(const _InputDeviceNotMatchError_& other) {
  if (!(info() == other.info())) {
    return info() < other.info() ? -1 : 1;
  }
  return 0;
}

bool ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::operator==(const _InputDeviceNotMatchError_& other) const {
  return true
    && info() == other.info()
  ;
}

std::size_t ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::__CalcHash__() const {
  return 0
    ^ info().__CalcHash__()
  ;
}

bool ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_::operator<(const _InputDeviceNotMatchError_& other) const {
  return false
    || !(info() == other.info()) ? 
      info() < other.info() : false
  ;
}

using _InputDeviceNotMatchError_ =  ConstInputDeviceNotMatchError::_InputDeviceNotMatchError_;
ConstInputDeviceNotMatchError::ConstInputDeviceNotMatchError(const ::std::shared_ptr<_InputDeviceNotMatchError_>& data): data_(data) {}
ConstInputDeviceNotMatchError::ConstInputDeviceNotMatchError(): data_(::std::make_shared<_InputDeviceNotMatchError_>()) {}
ConstInputDeviceNotMatchError::ConstInputDeviceNotMatchError(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror) {
  BuildFromProto(proto_inputdevicenotmatcherror);
}
ConstInputDeviceNotMatchError::ConstInputDeviceNotMatchError(const ConstInputDeviceNotMatchError&) = default;
ConstInputDeviceNotMatchError::ConstInputDeviceNotMatchError(ConstInputDeviceNotMatchError&&) noexcept = default;
ConstInputDeviceNotMatchError::~ConstInputDeviceNotMatchError() = default;

void ConstInputDeviceNotMatchError::ToProto(PbMessage* proto_inputdevicenotmatcherror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InputDeviceNotMatchError*>(proto_inputdevicenotmatcherror));
}
  
::std::string ConstInputDeviceNotMatchError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInputDeviceNotMatchError::__Empty__() const {
  return !data_;
}

int ConstInputDeviceNotMatchError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"info", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInputDeviceNotMatchError::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInputDeviceNotMatchError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInputDeviceNotMatchError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &info();
    default: return nullptr;
  }
}

// repeated field info
::std::size_t ConstInputDeviceNotMatchError::info_size() const {
  return __SharedPtrOrDefault__()->info_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& ConstInputDeviceNotMatchError::info() const {
  return __SharedPtrOrDefault__()->info();
}
const ::std::string& ConstInputDeviceNotMatchError::info(::std::size_t index) const {
  return __SharedPtrOrDefault__()->info(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> ConstInputDeviceNotMatchError::shared_const_info() const {
  return info().__SharedConst__();
}

::std::shared_ptr<ConstInputDeviceNotMatchError> ConstInputDeviceNotMatchError::__SharedConst__() const {
  return ::std::make_shared<ConstInputDeviceNotMatchError>(data_);
}
int64_t ConstInputDeviceNotMatchError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInputDeviceNotMatchError::operator==(const ConstInputDeviceNotMatchError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInputDeviceNotMatchError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInputDeviceNotMatchError::operator<(const ConstInputDeviceNotMatchError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InputDeviceNotMatchError_>& ConstInputDeviceNotMatchError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InputDeviceNotMatchError_> default_ptr = std::make_shared<_InputDeviceNotMatchError_>();
  return default_ptr;
}
const ::std::shared_ptr<_InputDeviceNotMatchError_>& ConstInputDeviceNotMatchError::__SharedPtr__() {
  if (!data_) { data_.reset(new _InputDeviceNotMatchError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInputDeviceNotMatchError
void ConstInputDeviceNotMatchError::BuildFromProto(const PbMessage& proto_inputdevicenotmatcherror) {
  data_ = ::std::make_shared<_InputDeviceNotMatchError_>(dynamic_cast<const ::oneflow::InputDeviceNotMatchError&>(proto_inputdevicenotmatcherror));
}

InputDeviceNotMatchError::InputDeviceNotMatchError(const ::std::shared_ptr<_InputDeviceNotMatchError_>& data)
  : ConstInputDeviceNotMatchError(data) {}
InputDeviceNotMatchError::InputDeviceNotMatchError(const InputDeviceNotMatchError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InputDeviceNotMatchError> resize
InputDeviceNotMatchError::InputDeviceNotMatchError(InputDeviceNotMatchError&&) noexcept = default; 
InputDeviceNotMatchError::InputDeviceNotMatchError(const ::oneflow::InputDeviceNotMatchError& proto_inputdevicenotmatcherror) {
  InitFromProto(proto_inputdevicenotmatcherror);
}
InputDeviceNotMatchError::InputDeviceNotMatchError() = default;

InputDeviceNotMatchError::~InputDeviceNotMatchError() = default;

void InputDeviceNotMatchError::InitFromProto(const PbMessage& proto_inputdevicenotmatcherror) {
  BuildFromProto(proto_inputdevicenotmatcherror);
}
  
void* InputDeviceNotMatchError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_info();
    default: return nullptr;
  }
}

bool InputDeviceNotMatchError::operator==(const InputDeviceNotMatchError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InputDeviceNotMatchError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InputDeviceNotMatchError::operator<(const InputDeviceNotMatchError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InputDeviceNotMatchError::Clear() {
  if (data_) { data_.reset(); }
}
void InputDeviceNotMatchError::CopyFrom(const InputDeviceNotMatchError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InputDeviceNotMatchError& InputDeviceNotMatchError::operator=(const InputDeviceNotMatchError& other) {
  CopyFrom(other);
  return *this;
}

// repeated field info
void InputDeviceNotMatchError::clear_info() {
  return __SharedPtr__()->clear_info();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_* InputDeviceNotMatchError::mutable_info() {
  return __SharedPtr__()->mutable_info();
}
::std::string* InputDeviceNotMatchError::mutable_info(::std::size_t index) {
  return __SharedPtr__()->mutable_info(index);
}
void InputDeviceNotMatchError::add_info(const ::std::string& value) {
  return __SharedPtr__()->add_info(value);
}
void InputDeviceNotMatchError::set_info(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_info(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> InputDeviceNotMatchError::shared_mutable_info() {
  return mutable_info()->__SharedMutable__();
}

::std::shared_ptr<InputDeviceNotMatchError> InputDeviceNotMatchError::__SharedMutable__() {
  return ::std::make_shared<InputDeviceNotMatchError>(__SharedPtr__());
}
ConstErrorStackFrame::_ErrorStackFrame_::_ErrorStackFrame_() { Clear(); }
ConstErrorStackFrame::_ErrorStackFrame_::_ErrorStackFrame_(const _ErrorStackFrame_& other) { CopyFrom(other); }
ConstErrorStackFrame::_ErrorStackFrame_::_ErrorStackFrame_(const ::oneflow::ErrorStackFrame& proto_errorstackframe) {
  InitFromProto(proto_errorstackframe);
}
ConstErrorStackFrame::_ErrorStackFrame_::_ErrorStackFrame_(_ErrorStackFrame_&& other) = default;
ConstErrorStackFrame::_ErrorStackFrame_::~_ErrorStackFrame_() = default;

void ConstErrorStackFrame::_ErrorStackFrame_::InitFromProto(const ::oneflow::ErrorStackFrame& proto_errorstackframe) {
  Clear();
  // required_or_optional field: file
  if (proto_errorstackframe.has_file()) {
    set_file(proto_errorstackframe.file());
  }
  // required_or_optional field: line
  if (proto_errorstackframe.has_line()) {
    set_line(proto_errorstackframe.line());
  }
  // required_or_optional field: function
  if (proto_errorstackframe.has_function()) {
    set_function(proto_errorstackframe.function());
  }
  // required_or_optional field: error_msg
  if (proto_errorstackframe.has_error_msg()) {
    set_error_msg(proto_errorstackframe.error_msg());
  }
    
}

void ConstErrorStackFrame::_ErrorStackFrame_::ToProto(::oneflow::ErrorStackFrame* proto_errorstackframe) const {
  proto_errorstackframe->Clear();
  // required_or_optional field: file
  if (this->has_file()) {
    proto_errorstackframe->set_file(file());
    }
  // required_or_optional field: line
  if (this->has_line()) {
    proto_errorstackframe->set_line(line());
    }
  // required_or_optional field: function
  if (this->has_function()) {
    proto_errorstackframe->set_function(function());
    }
  // required_or_optional field: error_msg
  if (this->has_error_msg()) {
    proto_errorstackframe->set_error_msg(error_msg());
    }

}

::std::string ConstErrorStackFrame::_ErrorStackFrame_::DebugString() const {
  ::oneflow::ErrorStackFrame proto_errorstackframe;
  this->ToProto(&proto_errorstackframe);
  return proto_errorstackframe.DebugString();
}

void ConstErrorStackFrame::_ErrorStackFrame_::Clear() {
  clear_file();
  clear_line();
  clear_function();
  clear_error_msg();
}

void ConstErrorStackFrame::_ErrorStackFrame_::CopyFrom(const _ErrorStackFrame_& other) {
  if (other.has_file()) {
    set_file(other.file());
  } else {
    clear_file();
  }
  if (other.has_line()) {
    set_line(other.line());
  } else {
    clear_line();
  }
  if (other.has_function()) {
    set_function(other.function());
  } else {
    clear_function();
  }
  if (other.has_error_msg()) {
    set_error_msg(other.error_msg());
  } else {
    clear_error_msg();
  }
}


// optional field file
bool ConstErrorStackFrame::_ErrorStackFrame_::has_file() const {
  return has_file_;
}
const ::std::string& ConstErrorStackFrame::_ErrorStackFrame_::file() const {
  if (has_file_) { return file_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstErrorStackFrame::_ErrorStackFrame_::clear_file() {
  has_file_ = false;
}
void ConstErrorStackFrame::_ErrorStackFrame_::set_file(const ::std::string& value) {
  file_ = value;
  has_file_ = true;
}
::std::string* ConstErrorStackFrame::_ErrorStackFrame_::mutable_file() {
  has_file_ = true;
  return &file_;
}

// optional field line
bool ConstErrorStackFrame::_ErrorStackFrame_::has_line() const {
  return has_line_;
}
const int64_t& ConstErrorStackFrame::_ErrorStackFrame_::line() const {
  if (has_line_) { return line_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstErrorStackFrame::_ErrorStackFrame_::clear_line() {
  has_line_ = false;
}
void ConstErrorStackFrame::_ErrorStackFrame_::set_line(const int64_t& value) {
  line_ = value;
  has_line_ = true;
}
int64_t* ConstErrorStackFrame::_ErrorStackFrame_::mutable_line() {
  has_line_ = true;
  return &line_;
}

// optional field function
bool ConstErrorStackFrame::_ErrorStackFrame_::has_function() const {
  return has_function_;
}
const ::std::string& ConstErrorStackFrame::_ErrorStackFrame_::function() const {
  if (has_function_) { return function_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstErrorStackFrame::_ErrorStackFrame_::clear_function() {
  has_function_ = false;
}
void ConstErrorStackFrame::_ErrorStackFrame_::set_function(const ::std::string& value) {
  function_ = value;
  has_function_ = true;
}
::std::string* ConstErrorStackFrame::_ErrorStackFrame_::mutable_function() {
  has_function_ = true;
  return &function_;
}

// optional field error_msg
bool ConstErrorStackFrame::_ErrorStackFrame_::has_error_msg() const {
  return has_error_msg_;
}
const ::std::string& ConstErrorStackFrame::_ErrorStackFrame_::error_msg() const {
  if (has_error_msg_) { return error_msg_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstErrorStackFrame::_ErrorStackFrame_::clear_error_msg() {
  has_error_msg_ = false;
}
void ConstErrorStackFrame::_ErrorStackFrame_::set_error_msg(const ::std::string& value) {
  error_msg_ = value;
  has_error_msg_ = true;
}
::std::string* ConstErrorStackFrame::_ErrorStackFrame_::mutable_error_msg() {
  has_error_msg_ = true;
  return &error_msg_;
}


int ConstErrorStackFrame::_ErrorStackFrame_::compare(const _ErrorStackFrame_& other) {
  if (!(has_file() == other.has_file())) {
    return has_file() < other.has_file() ? -1 : 1;
  } else if (!(file() == other.file())) {
    return file() < other.file() ? -1 : 1;
  }
  if (!(has_line() == other.has_line())) {
    return has_line() < other.has_line() ? -1 : 1;
  } else if (!(line() == other.line())) {
    return line() < other.line() ? -1 : 1;
  }
  if (!(has_function() == other.has_function())) {
    return has_function() < other.has_function() ? -1 : 1;
  } else if (!(function() == other.function())) {
    return function() < other.function() ? -1 : 1;
  }
  if (!(has_error_msg() == other.has_error_msg())) {
    return has_error_msg() < other.has_error_msg() ? -1 : 1;
  } else if (!(error_msg() == other.error_msg())) {
    return error_msg() < other.error_msg() ? -1 : 1;
  }
  return 0;
}

bool ConstErrorStackFrame::_ErrorStackFrame_::operator==(const _ErrorStackFrame_& other) const {
  return true
    && has_file() == other.has_file() 
    && file() == other.file()
    && has_line() == other.has_line() 
    && line() == other.line()
    && has_function() == other.has_function() 
    && function() == other.function()
    && has_error_msg() == other.has_error_msg() 
    && error_msg() == other.error_msg()
  ;
}

std::size_t ConstErrorStackFrame::_ErrorStackFrame_::__CalcHash__() const {
  return 0
    ^ (has_file() ? std::hash<::std::string>()(file()) : 0)
    ^ (has_line() ? std::hash<int64_t>()(line()) : 0)
    ^ (has_function() ? std::hash<::std::string>()(function()) : 0)
    ^ (has_error_msg() ? std::hash<::std::string>()(error_msg()) : 0)
  ;
}

bool ConstErrorStackFrame::_ErrorStackFrame_::operator<(const _ErrorStackFrame_& other) const {
  return false
    || !(has_file() == other.has_file()) ? 
      has_file() < other.has_file() : false
    || !(file() == other.file()) ? 
      file() < other.file() : false
    || !(has_line() == other.has_line()) ? 
      has_line() < other.has_line() : false
    || !(line() == other.line()) ? 
      line() < other.line() : false
    || !(has_function() == other.has_function()) ? 
      has_function() < other.has_function() : false
    || !(function() == other.function()) ? 
      function() < other.function() : false
    || !(has_error_msg() == other.has_error_msg()) ? 
      has_error_msg() < other.has_error_msg() : false
    || !(error_msg() == other.error_msg()) ? 
      error_msg() < other.error_msg() : false
  ;
}

using _ErrorStackFrame_ =  ConstErrorStackFrame::_ErrorStackFrame_;
ConstErrorStackFrame::ConstErrorStackFrame(const ::std::shared_ptr<_ErrorStackFrame_>& data): data_(data) {}
ConstErrorStackFrame::ConstErrorStackFrame(): data_(::std::make_shared<_ErrorStackFrame_>()) {}
ConstErrorStackFrame::ConstErrorStackFrame(const ::oneflow::ErrorStackFrame& proto_errorstackframe) {
  BuildFromProto(proto_errorstackframe);
}
ConstErrorStackFrame::ConstErrorStackFrame(const ConstErrorStackFrame&) = default;
ConstErrorStackFrame::ConstErrorStackFrame(ConstErrorStackFrame&&) noexcept = default;
ConstErrorStackFrame::~ConstErrorStackFrame() = default;

void ConstErrorStackFrame::ToProto(PbMessage* proto_errorstackframe) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ErrorStackFrame*>(proto_errorstackframe));
}
  
::std::string ConstErrorStackFrame::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstErrorStackFrame::__Empty__() const {
  return !data_;
}

int ConstErrorStackFrame::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"file", 1},
    {"line", 2},
    {"function", 3},
    {"error_msg", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstErrorStackFrame::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstErrorStackFrame::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstErrorStackFrame::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &file();
    case 2: return &line();
    case 3: return &function();
    case 4: return &error_msg();
    default: return nullptr;
  }
}

// required or optional field file
bool ConstErrorStackFrame::has_file() const {
  return __SharedPtrOrDefault__()->has_file();
}
const ::std::string& ConstErrorStackFrame::file() const {
  return __SharedPtrOrDefault__()->file();
}
// used by pybind11 only
// required or optional field line
bool ConstErrorStackFrame::has_line() const {
  return __SharedPtrOrDefault__()->has_line();
}
const int64_t& ConstErrorStackFrame::line() const {
  return __SharedPtrOrDefault__()->line();
}
// used by pybind11 only
// required or optional field function
bool ConstErrorStackFrame::has_function() const {
  return __SharedPtrOrDefault__()->has_function();
}
const ::std::string& ConstErrorStackFrame::function() const {
  return __SharedPtrOrDefault__()->function();
}
// used by pybind11 only
// required or optional field error_msg
bool ConstErrorStackFrame::has_error_msg() const {
  return __SharedPtrOrDefault__()->has_error_msg();
}
const ::std::string& ConstErrorStackFrame::error_msg() const {
  return __SharedPtrOrDefault__()->error_msg();
}
// used by pybind11 only

::std::shared_ptr<ConstErrorStackFrame> ConstErrorStackFrame::__SharedConst__() const {
  return ::std::make_shared<ConstErrorStackFrame>(data_);
}
int64_t ConstErrorStackFrame::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstErrorStackFrame::operator==(const ConstErrorStackFrame& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstErrorStackFrame::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstErrorStackFrame::operator<(const ConstErrorStackFrame& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ErrorStackFrame_>& ConstErrorStackFrame::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ErrorStackFrame_> default_ptr = std::make_shared<_ErrorStackFrame_>();
  return default_ptr;
}
const ::std::shared_ptr<_ErrorStackFrame_>& ConstErrorStackFrame::__SharedPtr__() {
  if (!data_) { data_.reset(new _ErrorStackFrame_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstErrorStackFrame
void ConstErrorStackFrame::BuildFromProto(const PbMessage& proto_errorstackframe) {
  data_ = ::std::make_shared<_ErrorStackFrame_>(dynamic_cast<const ::oneflow::ErrorStackFrame&>(proto_errorstackframe));
}

ErrorStackFrame::ErrorStackFrame(const ::std::shared_ptr<_ErrorStackFrame_>& data)
  : ConstErrorStackFrame(data) {}
ErrorStackFrame::ErrorStackFrame(const ErrorStackFrame& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ErrorStackFrame> resize
ErrorStackFrame::ErrorStackFrame(ErrorStackFrame&&) noexcept = default; 
ErrorStackFrame::ErrorStackFrame(const ::oneflow::ErrorStackFrame& proto_errorstackframe) {
  InitFromProto(proto_errorstackframe);
}
ErrorStackFrame::ErrorStackFrame() = default;

ErrorStackFrame::~ErrorStackFrame() = default;

void ErrorStackFrame::InitFromProto(const PbMessage& proto_errorstackframe) {
  BuildFromProto(proto_errorstackframe);
}
  
void* ErrorStackFrame::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_file();
    case 2: return mutable_line();
    case 3: return mutable_function();
    case 4: return mutable_error_msg();
    default: return nullptr;
  }
}

bool ErrorStackFrame::operator==(const ErrorStackFrame& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ErrorStackFrame::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ErrorStackFrame::operator<(const ErrorStackFrame& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ErrorStackFrame::Clear() {
  if (data_) { data_.reset(); }
}
void ErrorStackFrame::CopyFrom(const ErrorStackFrame& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ErrorStackFrame& ErrorStackFrame::operator=(const ErrorStackFrame& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field file
void ErrorStackFrame::clear_file() {
  return __SharedPtr__()->clear_file();
}
void ErrorStackFrame::set_file(const ::std::string& value) {
  return __SharedPtr__()->set_file(value);
}
::std::string* ErrorStackFrame::mutable_file() {
  return  __SharedPtr__()->mutable_file();
}
// required or optional field line
void ErrorStackFrame::clear_line() {
  return __SharedPtr__()->clear_line();
}
void ErrorStackFrame::set_line(const int64_t& value) {
  return __SharedPtr__()->set_line(value);
}
int64_t* ErrorStackFrame::mutable_line() {
  return  __SharedPtr__()->mutable_line();
}
// required or optional field function
void ErrorStackFrame::clear_function() {
  return __SharedPtr__()->clear_function();
}
void ErrorStackFrame::set_function(const ::std::string& value) {
  return __SharedPtr__()->set_function(value);
}
::std::string* ErrorStackFrame::mutable_function() {
  return  __SharedPtr__()->mutable_function();
}
// required or optional field error_msg
void ErrorStackFrame::clear_error_msg() {
  return __SharedPtr__()->clear_error_msg();
}
void ErrorStackFrame::set_error_msg(const ::std::string& value) {
  return __SharedPtr__()->set_error_msg(value);
}
::std::string* ErrorStackFrame::mutable_error_msg() {
  return  __SharedPtr__()->mutable_error_msg();
}

::std::shared_ptr<ErrorStackFrame> ErrorStackFrame::__SharedMutable__() {
  return ::std::make_shared<ErrorStackFrame>(__SharedPtr__());
}
ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::_SymbolIdUninitializedError_() { Clear(); }
ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::_SymbolIdUninitializedError_(const _SymbolIdUninitializedError_& other) { CopyFrom(other); }
ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::_SymbolIdUninitializedError_(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror) {
  InitFromProto(proto_symboliduninitializederror);
}
ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::_SymbolIdUninitializedError_(_SymbolIdUninitializedError_&& other) = default;
ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::~_SymbolIdUninitializedError_() = default;

void ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::InitFromProto(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror) {
  Clear();
    
}

void ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::ToProto(::oneflow::SymbolIdUninitializedError* proto_symboliduninitializederror) const {
  proto_symboliduninitializederror->Clear();

}

::std::string ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::DebugString() const {
  ::oneflow::SymbolIdUninitializedError proto_symboliduninitializederror;
  this->ToProto(&proto_symboliduninitializederror);
  return proto_symboliduninitializederror.DebugString();
}

void ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::Clear() {
}

void ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::CopyFrom(const _SymbolIdUninitializedError_& other) {
}



int ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::compare(const _SymbolIdUninitializedError_& other) {
  return 0;
}

bool ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::operator==(const _SymbolIdUninitializedError_& other) const {
  return true
  ;
}

std::size_t ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_::operator<(const _SymbolIdUninitializedError_& other) const {
  return false
  ;
}

using _SymbolIdUninitializedError_ =  ConstSymbolIdUninitializedError::_SymbolIdUninitializedError_;
ConstSymbolIdUninitializedError::ConstSymbolIdUninitializedError(const ::std::shared_ptr<_SymbolIdUninitializedError_>& data): data_(data) {}
ConstSymbolIdUninitializedError::ConstSymbolIdUninitializedError(): data_(::std::make_shared<_SymbolIdUninitializedError_>()) {}
ConstSymbolIdUninitializedError::ConstSymbolIdUninitializedError(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror) {
  BuildFromProto(proto_symboliduninitializederror);
}
ConstSymbolIdUninitializedError::ConstSymbolIdUninitializedError(const ConstSymbolIdUninitializedError&) = default;
ConstSymbolIdUninitializedError::ConstSymbolIdUninitializedError(ConstSymbolIdUninitializedError&&) noexcept = default;
ConstSymbolIdUninitializedError::~ConstSymbolIdUninitializedError() = default;

void ConstSymbolIdUninitializedError::ToProto(PbMessage* proto_symboliduninitializederror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SymbolIdUninitializedError*>(proto_symboliduninitializederror));
}
  
::std::string ConstSymbolIdUninitializedError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSymbolIdUninitializedError::__Empty__() const {
  return !data_;
}

int ConstSymbolIdUninitializedError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstSymbolIdUninitializedError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstSymbolIdUninitializedError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSymbolIdUninitializedError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstSymbolIdUninitializedError> ConstSymbolIdUninitializedError::__SharedConst__() const {
  return ::std::make_shared<ConstSymbolIdUninitializedError>(data_);
}
int64_t ConstSymbolIdUninitializedError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSymbolIdUninitializedError::operator==(const ConstSymbolIdUninitializedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSymbolIdUninitializedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSymbolIdUninitializedError::operator<(const ConstSymbolIdUninitializedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SymbolIdUninitializedError_>& ConstSymbolIdUninitializedError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SymbolIdUninitializedError_> default_ptr = std::make_shared<_SymbolIdUninitializedError_>();
  return default_ptr;
}
const ::std::shared_ptr<_SymbolIdUninitializedError_>& ConstSymbolIdUninitializedError::__SharedPtr__() {
  if (!data_) { data_.reset(new _SymbolIdUninitializedError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSymbolIdUninitializedError
void ConstSymbolIdUninitializedError::BuildFromProto(const PbMessage& proto_symboliduninitializederror) {
  data_ = ::std::make_shared<_SymbolIdUninitializedError_>(dynamic_cast<const ::oneflow::SymbolIdUninitializedError&>(proto_symboliduninitializederror));
}

SymbolIdUninitializedError::SymbolIdUninitializedError(const ::std::shared_ptr<_SymbolIdUninitializedError_>& data)
  : ConstSymbolIdUninitializedError(data) {}
SymbolIdUninitializedError::SymbolIdUninitializedError(const SymbolIdUninitializedError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SymbolIdUninitializedError> resize
SymbolIdUninitializedError::SymbolIdUninitializedError(SymbolIdUninitializedError&&) noexcept = default; 
SymbolIdUninitializedError::SymbolIdUninitializedError(const ::oneflow::SymbolIdUninitializedError& proto_symboliduninitializederror) {
  InitFromProto(proto_symboliduninitializederror);
}
SymbolIdUninitializedError::SymbolIdUninitializedError() = default;

SymbolIdUninitializedError::~SymbolIdUninitializedError() = default;

void SymbolIdUninitializedError::InitFromProto(const PbMessage& proto_symboliduninitializederror) {
  BuildFromProto(proto_symboliduninitializederror);
}
  
void* SymbolIdUninitializedError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool SymbolIdUninitializedError::operator==(const SymbolIdUninitializedError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SymbolIdUninitializedError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SymbolIdUninitializedError::operator<(const SymbolIdUninitializedError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SymbolIdUninitializedError::Clear() {
  if (data_) { data_.reset(); }
}
void SymbolIdUninitializedError::CopyFrom(const SymbolIdUninitializedError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SymbolIdUninitializedError& SymbolIdUninitializedError::operator=(const SymbolIdUninitializedError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<SymbolIdUninitializedError> SymbolIdUninitializedError::__SharedMutable__() {
  return ::std::make_shared<SymbolIdUninitializedError>(__SharedPtr__());
}
ConstValueError::_ValueError_::_ValueError_() { Clear(); }
ConstValueError::_ValueError_::_ValueError_(const _ValueError_& other) { CopyFrom(other); }
ConstValueError::_ValueError_::_ValueError_(const ::oneflow::ValueError& proto_valueerror) {
  InitFromProto(proto_valueerror);
}
ConstValueError::_ValueError_::_ValueError_(_ValueError_&& other) = default;
ConstValueError::_ValueError_::~_ValueError_() = default;

void ConstValueError::_ValueError_::InitFromProto(const ::oneflow::ValueError& proto_valueerror) {
  Clear();
    
}

void ConstValueError::_ValueError_::ToProto(::oneflow::ValueError* proto_valueerror) const {
  proto_valueerror->Clear();

}

::std::string ConstValueError::_ValueError_::DebugString() const {
  ::oneflow::ValueError proto_valueerror;
  this->ToProto(&proto_valueerror);
  return proto_valueerror.DebugString();
}

void ConstValueError::_ValueError_::Clear() {
}

void ConstValueError::_ValueError_::CopyFrom(const _ValueError_& other) {
}



int ConstValueError::_ValueError_::compare(const _ValueError_& other) {
  return 0;
}

bool ConstValueError::_ValueError_::operator==(const _ValueError_& other) const {
  return true
  ;
}

std::size_t ConstValueError::_ValueError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstValueError::_ValueError_::operator<(const _ValueError_& other) const {
  return false
  ;
}

using _ValueError_ =  ConstValueError::_ValueError_;
ConstValueError::ConstValueError(const ::std::shared_ptr<_ValueError_>& data): data_(data) {}
ConstValueError::ConstValueError(): data_(::std::make_shared<_ValueError_>()) {}
ConstValueError::ConstValueError(const ::oneflow::ValueError& proto_valueerror) {
  BuildFromProto(proto_valueerror);
}
ConstValueError::ConstValueError(const ConstValueError&) = default;
ConstValueError::ConstValueError(ConstValueError&&) noexcept = default;
ConstValueError::~ConstValueError() = default;

void ConstValueError::ToProto(PbMessage* proto_valueerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ValueError*>(proto_valueerror));
}
  
::std::string ConstValueError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstValueError::__Empty__() const {
  return !data_;
}

int ConstValueError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstValueError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstValueError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstValueError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstValueError> ConstValueError::__SharedConst__() const {
  return ::std::make_shared<ConstValueError>(data_);
}
int64_t ConstValueError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstValueError::operator==(const ConstValueError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstValueError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstValueError::operator<(const ConstValueError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ValueError_>& ConstValueError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ValueError_> default_ptr = std::make_shared<_ValueError_>();
  return default_ptr;
}
const ::std::shared_ptr<_ValueError_>& ConstValueError::__SharedPtr__() {
  if (!data_) { data_.reset(new _ValueError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstValueError
void ConstValueError::BuildFromProto(const PbMessage& proto_valueerror) {
  data_ = ::std::make_shared<_ValueError_>(dynamic_cast<const ::oneflow::ValueError&>(proto_valueerror));
}

ValueError::ValueError(const ::std::shared_ptr<_ValueError_>& data)
  : ConstValueError(data) {}
ValueError::ValueError(const ValueError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ValueError> resize
ValueError::ValueError(ValueError&&) noexcept = default; 
ValueError::ValueError(const ::oneflow::ValueError& proto_valueerror) {
  InitFromProto(proto_valueerror);
}
ValueError::ValueError() = default;

ValueError::~ValueError() = default;

void ValueError::InitFromProto(const PbMessage& proto_valueerror) {
  BuildFromProto(proto_valueerror);
}
  
void* ValueError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool ValueError::operator==(const ValueError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ValueError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ValueError::operator<(const ValueError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ValueError::Clear() {
  if (data_) { data_.reset(); }
}
void ValueError::CopyFrom(const ValueError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ValueError& ValueError::operator=(const ValueError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<ValueError> ValueError::__SharedMutable__() {
  return ::std::make_shared<ValueError>(__SharedPtr__());
}
ConstIndexError::_IndexError_::_IndexError_() { Clear(); }
ConstIndexError::_IndexError_::_IndexError_(const _IndexError_& other) { CopyFrom(other); }
ConstIndexError::_IndexError_::_IndexError_(const ::oneflow::IndexError& proto_indexerror) {
  InitFromProto(proto_indexerror);
}
ConstIndexError::_IndexError_::_IndexError_(_IndexError_&& other) = default;
ConstIndexError::_IndexError_::~_IndexError_() = default;

void ConstIndexError::_IndexError_::InitFromProto(const ::oneflow::IndexError& proto_indexerror) {
  Clear();
    
}

void ConstIndexError::_IndexError_::ToProto(::oneflow::IndexError* proto_indexerror) const {
  proto_indexerror->Clear();

}

::std::string ConstIndexError::_IndexError_::DebugString() const {
  ::oneflow::IndexError proto_indexerror;
  this->ToProto(&proto_indexerror);
  return proto_indexerror.DebugString();
}

void ConstIndexError::_IndexError_::Clear() {
}

void ConstIndexError::_IndexError_::CopyFrom(const _IndexError_& other) {
}



int ConstIndexError::_IndexError_::compare(const _IndexError_& other) {
  return 0;
}

bool ConstIndexError::_IndexError_::operator==(const _IndexError_& other) const {
  return true
  ;
}

std::size_t ConstIndexError::_IndexError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstIndexError::_IndexError_::operator<(const _IndexError_& other) const {
  return false
  ;
}

using _IndexError_ =  ConstIndexError::_IndexError_;
ConstIndexError::ConstIndexError(const ::std::shared_ptr<_IndexError_>& data): data_(data) {}
ConstIndexError::ConstIndexError(): data_(::std::make_shared<_IndexError_>()) {}
ConstIndexError::ConstIndexError(const ::oneflow::IndexError& proto_indexerror) {
  BuildFromProto(proto_indexerror);
}
ConstIndexError::ConstIndexError(const ConstIndexError&) = default;
ConstIndexError::ConstIndexError(ConstIndexError&&) noexcept = default;
ConstIndexError::~ConstIndexError() = default;

void ConstIndexError::ToProto(PbMessage* proto_indexerror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::IndexError*>(proto_indexerror));
}
  
::std::string ConstIndexError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstIndexError::__Empty__() const {
  return !data_;
}

int ConstIndexError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstIndexError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstIndexError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstIndexError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstIndexError> ConstIndexError::__SharedConst__() const {
  return ::std::make_shared<ConstIndexError>(data_);
}
int64_t ConstIndexError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstIndexError::operator==(const ConstIndexError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstIndexError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstIndexError::operator<(const ConstIndexError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_IndexError_>& ConstIndexError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_IndexError_> default_ptr = std::make_shared<_IndexError_>();
  return default_ptr;
}
const ::std::shared_ptr<_IndexError_>& ConstIndexError::__SharedPtr__() {
  if (!data_) { data_.reset(new _IndexError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstIndexError
void ConstIndexError::BuildFromProto(const PbMessage& proto_indexerror) {
  data_ = ::std::make_shared<_IndexError_>(dynamic_cast<const ::oneflow::IndexError&>(proto_indexerror));
}

IndexError::IndexError(const ::std::shared_ptr<_IndexError_>& data)
  : ConstIndexError(data) {}
IndexError::IndexError(const IndexError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<IndexError> resize
IndexError::IndexError(IndexError&&) noexcept = default; 
IndexError::IndexError(const ::oneflow::IndexError& proto_indexerror) {
  InitFromProto(proto_indexerror);
}
IndexError::IndexError() = default;

IndexError::~IndexError() = default;

void IndexError::InitFromProto(const PbMessage& proto_indexerror) {
  BuildFromProto(proto_indexerror);
}
  
void* IndexError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool IndexError::operator==(const IndexError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t IndexError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool IndexError::operator<(const IndexError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void IndexError::Clear() {
  if (data_) { data_.reset(); }
}
void IndexError::CopyFrom(const IndexError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
IndexError& IndexError::operator=(const IndexError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<IndexError> IndexError::__SharedMutable__() {
  return ::std::make_shared<IndexError>(__SharedPtr__());
}
ConstTimeoutError::_TimeoutError_::_TimeoutError_() { Clear(); }
ConstTimeoutError::_TimeoutError_::_TimeoutError_(const _TimeoutError_& other) { CopyFrom(other); }
ConstTimeoutError::_TimeoutError_::_TimeoutError_(const ::oneflow::TimeoutError& proto_timeouterror) {
  InitFromProto(proto_timeouterror);
}
ConstTimeoutError::_TimeoutError_::_TimeoutError_(_TimeoutError_&& other) = default;
ConstTimeoutError::_TimeoutError_::~_TimeoutError_() = default;

void ConstTimeoutError::_TimeoutError_::InitFromProto(const ::oneflow::TimeoutError& proto_timeouterror) {
  Clear();
    
}

void ConstTimeoutError::_TimeoutError_::ToProto(::oneflow::TimeoutError* proto_timeouterror) const {
  proto_timeouterror->Clear();

}

::std::string ConstTimeoutError::_TimeoutError_::DebugString() const {
  ::oneflow::TimeoutError proto_timeouterror;
  this->ToProto(&proto_timeouterror);
  return proto_timeouterror.DebugString();
}

void ConstTimeoutError::_TimeoutError_::Clear() {
}

void ConstTimeoutError::_TimeoutError_::CopyFrom(const _TimeoutError_& other) {
}



int ConstTimeoutError::_TimeoutError_::compare(const _TimeoutError_& other) {
  return 0;
}

bool ConstTimeoutError::_TimeoutError_::operator==(const _TimeoutError_& other) const {
  return true
  ;
}

std::size_t ConstTimeoutError::_TimeoutError_::__CalcHash__() const {
  return 0
  ;
}

bool ConstTimeoutError::_TimeoutError_::operator<(const _TimeoutError_& other) const {
  return false
  ;
}

using _TimeoutError_ =  ConstTimeoutError::_TimeoutError_;
ConstTimeoutError::ConstTimeoutError(const ::std::shared_ptr<_TimeoutError_>& data): data_(data) {}
ConstTimeoutError::ConstTimeoutError(): data_(::std::make_shared<_TimeoutError_>()) {}
ConstTimeoutError::ConstTimeoutError(const ::oneflow::TimeoutError& proto_timeouterror) {
  BuildFromProto(proto_timeouterror);
}
ConstTimeoutError::ConstTimeoutError(const ConstTimeoutError&) = default;
ConstTimeoutError::ConstTimeoutError(ConstTimeoutError&&) noexcept = default;
ConstTimeoutError::~ConstTimeoutError() = default;

void ConstTimeoutError::ToProto(PbMessage* proto_timeouterror) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TimeoutError*>(proto_timeouterror));
}
  
::std::string ConstTimeoutError::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTimeoutError::__Empty__() const {
  return !data_;
}

int ConstTimeoutError::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstTimeoutError::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstTimeoutError::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTimeoutError::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstTimeoutError> ConstTimeoutError::__SharedConst__() const {
  return ::std::make_shared<ConstTimeoutError>(data_);
}
int64_t ConstTimeoutError::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTimeoutError::operator==(const ConstTimeoutError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTimeoutError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTimeoutError::operator<(const ConstTimeoutError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TimeoutError_>& ConstTimeoutError::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TimeoutError_> default_ptr = std::make_shared<_TimeoutError_>();
  return default_ptr;
}
const ::std::shared_ptr<_TimeoutError_>& ConstTimeoutError::__SharedPtr__() {
  if (!data_) { data_.reset(new _TimeoutError_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTimeoutError
void ConstTimeoutError::BuildFromProto(const PbMessage& proto_timeouterror) {
  data_ = ::std::make_shared<_TimeoutError_>(dynamic_cast<const ::oneflow::TimeoutError&>(proto_timeouterror));
}

TimeoutError::TimeoutError(const ::std::shared_ptr<_TimeoutError_>& data)
  : ConstTimeoutError(data) {}
TimeoutError::TimeoutError(const TimeoutError& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TimeoutError> resize
TimeoutError::TimeoutError(TimeoutError&&) noexcept = default; 
TimeoutError::TimeoutError(const ::oneflow::TimeoutError& proto_timeouterror) {
  InitFromProto(proto_timeouterror);
}
TimeoutError::TimeoutError() = default;

TimeoutError::~TimeoutError() = default;

void TimeoutError::InitFromProto(const PbMessage& proto_timeouterror) {
  BuildFromProto(proto_timeouterror);
}
  
void* TimeoutError::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool TimeoutError::operator==(const TimeoutError& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TimeoutError::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TimeoutError::operator<(const TimeoutError& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TimeoutError::Clear() {
  if (data_) { data_.reset(); }
}
void TimeoutError::CopyFrom(const TimeoutError& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TimeoutError& TimeoutError::operator=(const TimeoutError& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<TimeoutError> TimeoutError::__SharedMutable__() {
  return ::std::make_shared<TimeoutError>(__SharedPtr__());
}
ConstErrorProto::_ErrorProto_::_ErrorProto_() { Clear(); }
ConstErrorProto::_ErrorProto_::_ErrorProto_(const _ErrorProto_& other) { CopyFrom(other); }
ConstErrorProto::_ErrorProto_::_ErrorProto_(const ::oneflow::ErrorProto& proto_errorproto) {
  InitFromProto(proto_errorproto);
}
ConstErrorProto::_ErrorProto_::_ErrorProto_(_ErrorProto_&& other) = default;
ConstErrorProto::_ErrorProto_::~_ErrorProto_() = default;

void ConstErrorProto::_ErrorProto_::InitFromProto(const ::oneflow::ErrorProto& proto_errorproto) {
  Clear();
  // required_or_optional field: error_summary
  if (proto_errorproto.has_error_summary()) {
    set_error_summary(proto_errorproto.error_summary());
  }
  // required_or_optional field: msg
  if (proto_errorproto.has_msg()) {
    set_msg(proto_errorproto.msg());
  }
  // repeated field: stack_frame
  if (!proto_errorproto.stack_frame().empty()) {
    for (const ::oneflow::ErrorStackFrame& elem : proto_errorproto.stack_frame() ) {
      *mutable_stack_frame()->Add() = ::oneflow::cfg::ErrorStackFrame(elem);
    }
  }
  // oneof field: error_type
  ErrorTypeCase error_type_case = ErrorTypeCase(int(proto_errorproto.error_type_case()));
  switch (error_type_case) {
    case kConfigAssertFailedError: {
      *mutable_config_assert_failed_error() = ::oneflow::cfg::ConfigAssertFailedError(proto_errorproto.config_assert_failed_error());
      break;
  }
    case kConfigResourceUnavailableError: {
      *mutable_config_resource_unavailable_error() = ::oneflow::cfg::ConfigResourceUnavailableError(proto_errorproto.config_resource_unavailable_error());
      break;
  }
    case kProtoParseFailedError: {
      *mutable_proto_parse_failed_error() = ::oneflow::cfg::ProtoParseFailedError(proto_errorproto.proto_parse_failed_error());
      break;
  }
    case kCheckFailedError: {
      *mutable_check_failed_error() = ::oneflow::cfg::CheckFailedError(proto_errorproto.check_failed_error());
      break;
  }
    case kTodoError: {
      *mutable_todo_error() = ::oneflow::cfg::TodoError(proto_errorproto.todo_error());
      break;
  }
    case kUnimplementedError: {
      *mutable_unimplemented_error() = ::oneflow::cfg::UnimplementedError(proto_errorproto.unimplemented_error());
      break;
  }
    case kBoxingNotSupportedError: {
      *mutable_boxing_not_supported_error() = ::oneflow::cfg::BoxingNotSupportedError(proto_errorproto.boxing_not_supported_error());
      break;
  }
    case kGradientFunctionNotFoundError: {
      *mutable_gradient_function_not_found_error() = ::oneflow::cfg::GradientFunctionNotFoundError(proto_errorproto.gradient_function_not_found_error());
      break;
  }
    case kOpKernelNotFoundError: {
      *mutable_op_kernel_not_found_error() = ::oneflow::cfg::OpKernelNotFoundError(proto_errorproto.op_kernel_not_found_error());
      break;
  }
    case kMultipleOpKernelsMatchedError: {
      *mutable_multiple_op_kernels_matched_error() = ::oneflow::cfg::MultipleOpKernelsMatchedError(proto_errorproto.multiple_op_kernels_matched_error());
      break;
  }
    case kMemoryZoneOutOfMemoryError: {
      *mutable_memory_zone_out_of_memory_error() = ::oneflow::cfg::MemoryZoneOutOfMemoryError(proto_errorproto.memory_zone_out_of_memory_error());
      break;
  }
    case kLossBlobNotFoundError: {
      *mutable_loss_blob_not_found_error() = ::oneflow::cfg::LossBlobNotFoundError(proto_errorproto.loss_blob_not_found_error());
      break;
  }
    case kJobSetEmptyError: {
      *mutable_job_set_empty_error() = ::oneflow::cfg::JobSetEmptyError(proto_errorproto.job_set_empty_error());
      break;
  }
    case kDeviceTagNotFoundError: {
      *mutable_device_tag_not_found_error() = ::oneflow::cfg::DeviceTagNotFoundError(proto_errorproto.device_tag_not_found_error());
      break;
  }
    case kValueError: {
      *mutable_value_error() = ::oneflow::cfg::ValueError(proto_errorproto.value_error());
      break;
  }
    case kIndexError: {
      *mutable_index_error() = ::oneflow::cfg::IndexError(proto_errorproto.index_error());
      break;
  }
    case kTimeoutError: {
      *mutable_timeout_error() = ::oneflow::cfg::TimeoutError(proto_errorproto.timeout_error());
      break;
  }
    case kJobNameExistError: {
      *mutable_job_name_exist_error() = ::oneflow::cfg::JobNameExistError(proto_errorproto.job_name_exist_error());
      break;
  }
    case kJobNameEmptyError: {
      *mutable_job_name_empty_error() = ::oneflow::cfg::JobNameEmptyError(proto_errorproto.job_name_empty_error());
      break;
  }
    case kJobNameNotEqualError: {
      *mutable_job_name_not_equal_error() = ::oneflow::cfg::JobNameNotEqualError(proto_errorproto.job_name_not_equal_error());
      break;
  }
    case kNoJobBuildAndInferCtxError: {
      *mutable_no_job_build_and_infer_ctx_error() = ::oneflow::cfg::NoJobBuildAndInferCtxError(proto_errorproto.no_job_build_and_infer_ctx_error());
      break;
  }
    case kJobConfFrozenError: {
      *mutable_job_conf_frozen_error() = ::oneflow::cfg::JobConfFrozenError(proto_errorproto.job_conf_frozen_error());
      break;
  }
    case kJobConfNotSetError: {
      *mutable_job_conf_not_set_error() = ::oneflow::cfg::JobConfNotSetError(proto_errorproto.job_conf_not_set_error());
      break;
  }
    case kJobConfRepeatedSetError: {
      *mutable_job_conf_repeated_set_error() = ::oneflow::cfg::JobConfRepeatedSetError(proto_errorproto.job_conf_repeated_set_error());
      break;
  }
    case kJobTypeNotSetError: {
      *mutable_job_type_not_set_error() = ::oneflow::cfg::JobTypeNotSetError(proto_errorproto.job_type_not_set_error());
      break;
  }
    case kLogicalBlobNameNotExistError: {
      *mutable_logical_blob_name_not_exist_error() = ::oneflow::cfg::LogicalBlobNameNotExistError(proto_errorproto.logical_blob_name_not_exist_error());
      break;
  }
    case kLogicalBlobNameExistError: {
      *mutable_logical_blob_name_exist_error() = ::oneflow::cfg::LogicalBlobNameExistError(proto_errorproto.logical_blob_name_exist_error());
      break;
  }
    case kLogicalBlobNameInvalidError: {
      *mutable_logical_blob_name_invalid_error() = ::oneflow::cfg::LogicalBlobNameInvalidError(proto_errorproto.logical_blob_name_invalid_error());
      break;
  }
    case kOpNameExistError: {
      *mutable_op_name_exist_error() = ::oneflow::cfg::OpNameExistError(proto_errorproto.op_name_exist_error());
      break;
  }
    case kOpConfDeviceTagNoSetError: {
      *mutable_op_conf_device_tag_no_set_error() = ::oneflow::cfg::OpConfDeviceTagNoSetError(proto_errorproto.op_conf_device_tag_no_set_error());
      break;
  }
    case kPlacementError: {
      *mutable_placement_error() = ::oneflow::cfg::PlacementError(proto_errorproto.placement_error());
      break;
  }
    case kBlobSplitAxisInferError: {
      *mutable_blob_split_axis_infer_error() = ::oneflow::cfg::BlobSplitAxisInferError(proto_errorproto.blob_split_axis_infer_error());
      break;
  }
    case kUnknownJobBuildAndInferError: {
      *mutable_unknown_job_build_and_infer_error() = ::oneflow::cfg::UnknownJobBuildAndInferError(proto_errorproto.unknown_job_build_and_infer_error());
      break;
  }
    case kRwMutexedObjectNotFoundError: {
      *mutable_rw_mutexed_object_not_found_error() = ::oneflow::cfg::RwMutexedObjectNotFoundError(proto_errorproto.rw_mutexed_object_not_found_error());
      break;
  }
    case kSymbolIdUninitializedError: {
      *mutable_symbol_id_uninitialized_error() = ::oneflow::cfg::SymbolIdUninitializedError(proto_errorproto.symbol_id_uninitialized_error());
      break;
  }
    case kUnknownError: {
      *mutable_unknown_error() = ::oneflow::cfg::UnknownError(proto_errorproto.unknown_error());
      break;
  }
    case kCompileOptionWrongError: {
      *mutable_compile_option_wrong_error() = ::oneflow::cfg::CompileOptionWrongError(proto_errorproto.compile_option_wrong_error());
      break;
  }
    case kInputDeviceNotMatchError: {
      *mutable_input_device_not_match_error() = ::oneflow::cfg::InputDeviceNotMatchError(proto_errorproto.input_device_not_match_error());
      break;
  }
    case ERROR_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstErrorProto::_ErrorProto_::ToProto(::oneflow::ErrorProto* proto_errorproto) const {
  proto_errorproto->Clear();
  // required_or_optional field: error_summary
  if (this->has_error_summary()) {
    proto_errorproto->set_error_summary(error_summary());
    }
  // required_or_optional field: msg
  if (this->has_msg()) {
    proto_errorproto->set_msg(msg());
    }
  // repeated field: stack_frame
  if (!stack_frame().empty()) {
    for (const ::oneflow::cfg::ErrorStackFrame& elem : stack_frame() ) {
      ::oneflow::ErrorStackFrame proto_stack_frame_elem;
      elem.ToProto(&proto_stack_frame_elem);
      *proto_errorproto->mutable_stack_frame()->Add() = proto_stack_frame_elem;
    }
  }

  // oneof field: error_type
  ::oneflow::ErrorProto::ErrorTypeCase error_type_case = ::oneflow::ErrorProto::ErrorTypeCase(int(this->error_type_case()));
  switch (error_type_case) {
    case ::oneflow::ErrorProto::kConfigAssertFailedError: {
      ::oneflow::ConfigAssertFailedError of_proto_config_assert_failed_error;
      config_assert_failed_error().ToProto(&of_proto_config_assert_failed_error);
      proto_errorproto->mutable_config_assert_failed_error()->CopyFrom(of_proto_config_assert_failed_error);
      break;
    }
    case ::oneflow::ErrorProto::kConfigResourceUnavailableError: {
      ::oneflow::ConfigResourceUnavailableError of_proto_config_resource_unavailable_error;
      config_resource_unavailable_error().ToProto(&of_proto_config_resource_unavailable_error);
      proto_errorproto->mutable_config_resource_unavailable_error()->CopyFrom(of_proto_config_resource_unavailable_error);
      break;
    }
    case ::oneflow::ErrorProto::kProtoParseFailedError: {
      ::oneflow::ProtoParseFailedError of_proto_proto_parse_failed_error;
      proto_parse_failed_error().ToProto(&of_proto_proto_parse_failed_error);
      proto_errorproto->mutable_proto_parse_failed_error()->CopyFrom(of_proto_proto_parse_failed_error);
      break;
    }
    case ::oneflow::ErrorProto::kCheckFailedError: {
      ::oneflow::CheckFailedError of_proto_check_failed_error;
      check_failed_error().ToProto(&of_proto_check_failed_error);
      proto_errorproto->mutable_check_failed_error()->CopyFrom(of_proto_check_failed_error);
      break;
    }
    case ::oneflow::ErrorProto::kTodoError: {
      ::oneflow::TodoError of_proto_todo_error;
      todo_error().ToProto(&of_proto_todo_error);
      proto_errorproto->mutable_todo_error()->CopyFrom(of_proto_todo_error);
      break;
    }
    case ::oneflow::ErrorProto::kUnimplementedError: {
      ::oneflow::UnimplementedError of_proto_unimplemented_error;
      unimplemented_error().ToProto(&of_proto_unimplemented_error);
      proto_errorproto->mutable_unimplemented_error()->CopyFrom(of_proto_unimplemented_error);
      break;
    }
    case ::oneflow::ErrorProto::kBoxingNotSupportedError: {
      ::oneflow::BoxingNotSupportedError of_proto_boxing_not_supported_error;
      boxing_not_supported_error().ToProto(&of_proto_boxing_not_supported_error);
      proto_errorproto->mutable_boxing_not_supported_error()->CopyFrom(of_proto_boxing_not_supported_error);
      break;
    }
    case ::oneflow::ErrorProto::kGradientFunctionNotFoundError: {
      ::oneflow::GradientFunctionNotFoundError of_proto_gradient_function_not_found_error;
      gradient_function_not_found_error().ToProto(&of_proto_gradient_function_not_found_error);
      proto_errorproto->mutable_gradient_function_not_found_error()->CopyFrom(of_proto_gradient_function_not_found_error);
      break;
    }
    case ::oneflow::ErrorProto::kOpKernelNotFoundError: {
      ::oneflow::OpKernelNotFoundError of_proto_op_kernel_not_found_error;
      op_kernel_not_found_error().ToProto(&of_proto_op_kernel_not_found_error);
      proto_errorproto->mutable_op_kernel_not_found_error()->CopyFrom(of_proto_op_kernel_not_found_error);
      break;
    }
    case ::oneflow::ErrorProto::kMultipleOpKernelsMatchedError: {
      ::oneflow::MultipleOpKernelsMatchedError of_proto_multiple_op_kernels_matched_error;
      multiple_op_kernels_matched_error().ToProto(&of_proto_multiple_op_kernels_matched_error);
      proto_errorproto->mutable_multiple_op_kernels_matched_error()->CopyFrom(of_proto_multiple_op_kernels_matched_error);
      break;
    }
    case ::oneflow::ErrorProto::kMemoryZoneOutOfMemoryError: {
      ::oneflow::MemoryZoneOutOfMemoryError of_proto_memory_zone_out_of_memory_error;
      memory_zone_out_of_memory_error().ToProto(&of_proto_memory_zone_out_of_memory_error);
      proto_errorproto->mutable_memory_zone_out_of_memory_error()->CopyFrom(of_proto_memory_zone_out_of_memory_error);
      break;
    }
    case ::oneflow::ErrorProto::kLossBlobNotFoundError: {
      ::oneflow::LossBlobNotFoundError of_proto_loss_blob_not_found_error;
      loss_blob_not_found_error().ToProto(&of_proto_loss_blob_not_found_error);
      proto_errorproto->mutable_loss_blob_not_found_error()->CopyFrom(of_proto_loss_blob_not_found_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobSetEmptyError: {
      ::oneflow::JobSetEmptyError of_proto_job_set_empty_error;
      job_set_empty_error().ToProto(&of_proto_job_set_empty_error);
      proto_errorproto->mutable_job_set_empty_error()->CopyFrom(of_proto_job_set_empty_error);
      break;
    }
    case ::oneflow::ErrorProto::kDeviceTagNotFoundError: {
      ::oneflow::DeviceTagNotFoundError of_proto_device_tag_not_found_error;
      device_tag_not_found_error().ToProto(&of_proto_device_tag_not_found_error);
      proto_errorproto->mutable_device_tag_not_found_error()->CopyFrom(of_proto_device_tag_not_found_error);
      break;
    }
    case ::oneflow::ErrorProto::kValueError: {
      ::oneflow::ValueError of_proto_value_error;
      value_error().ToProto(&of_proto_value_error);
      proto_errorproto->mutable_value_error()->CopyFrom(of_proto_value_error);
      break;
    }
    case ::oneflow::ErrorProto::kIndexError: {
      ::oneflow::IndexError of_proto_index_error;
      index_error().ToProto(&of_proto_index_error);
      proto_errorproto->mutable_index_error()->CopyFrom(of_proto_index_error);
      break;
    }
    case ::oneflow::ErrorProto::kTimeoutError: {
      ::oneflow::TimeoutError of_proto_timeout_error;
      timeout_error().ToProto(&of_proto_timeout_error);
      proto_errorproto->mutable_timeout_error()->CopyFrom(of_proto_timeout_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobNameExistError: {
      ::oneflow::JobNameExistError of_proto_job_name_exist_error;
      job_name_exist_error().ToProto(&of_proto_job_name_exist_error);
      proto_errorproto->mutable_job_name_exist_error()->CopyFrom(of_proto_job_name_exist_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobNameEmptyError: {
      ::oneflow::JobNameEmptyError of_proto_job_name_empty_error;
      job_name_empty_error().ToProto(&of_proto_job_name_empty_error);
      proto_errorproto->mutable_job_name_empty_error()->CopyFrom(of_proto_job_name_empty_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobNameNotEqualError: {
      ::oneflow::JobNameNotEqualError of_proto_job_name_not_equal_error;
      job_name_not_equal_error().ToProto(&of_proto_job_name_not_equal_error);
      proto_errorproto->mutable_job_name_not_equal_error()->CopyFrom(of_proto_job_name_not_equal_error);
      break;
    }
    case ::oneflow::ErrorProto::kNoJobBuildAndInferCtxError: {
      ::oneflow::NoJobBuildAndInferCtxError of_proto_no_job_build_and_infer_ctx_error;
      no_job_build_and_infer_ctx_error().ToProto(&of_proto_no_job_build_and_infer_ctx_error);
      proto_errorproto->mutable_no_job_build_and_infer_ctx_error()->CopyFrom(of_proto_no_job_build_and_infer_ctx_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobConfFrozenError: {
      ::oneflow::JobConfFrozenError of_proto_job_conf_frozen_error;
      job_conf_frozen_error().ToProto(&of_proto_job_conf_frozen_error);
      proto_errorproto->mutable_job_conf_frozen_error()->CopyFrom(of_proto_job_conf_frozen_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobConfNotSetError: {
      ::oneflow::JobConfNotSetError of_proto_job_conf_not_set_error;
      job_conf_not_set_error().ToProto(&of_proto_job_conf_not_set_error);
      proto_errorproto->mutable_job_conf_not_set_error()->CopyFrom(of_proto_job_conf_not_set_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobConfRepeatedSetError: {
      ::oneflow::JobConfRepeatedSetError of_proto_job_conf_repeated_set_error;
      job_conf_repeated_set_error().ToProto(&of_proto_job_conf_repeated_set_error);
      proto_errorproto->mutable_job_conf_repeated_set_error()->CopyFrom(of_proto_job_conf_repeated_set_error);
      break;
    }
    case ::oneflow::ErrorProto::kJobTypeNotSetError: {
      ::oneflow::JobTypeNotSetError of_proto_job_type_not_set_error;
      job_type_not_set_error().ToProto(&of_proto_job_type_not_set_error);
      proto_errorproto->mutable_job_type_not_set_error()->CopyFrom(of_proto_job_type_not_set_error);
      break;
    }
    case ::oneflow::ErrorProto::kLogicalBlobNameNotExistError: {
      ::oneflow::LogicalBlobNameNotExistError of_proto_logical_blob_name_not_exist_error;
      logical_blob_name_not_exist_error().ToProto(&of_proto_logical_blob_name_not_exist_error);
      proto_errorproto->mutable_logical_blob_name_not_exist_error()->CopyFrom(of_proto_logical_blob_name_not_exist_error);
      break;
    }
    case ::oneflow::ErrorProto::kLogicalBlobNameExistError: {
      ::oneflow::LogicalBlobNameExistError of_proto_logical_blob_name_exist_error;
      logical_blob_name_exist_error().ToProto(&of_proto_logical_blob_name_exist_error);
      proto_errorproto->mutable_logical_blob_name_exist_error()->CopyFrom(of_proto_logical_blob_name_exist_error);
      break;
    }
    case ::oneflow::ErrorProto::kLogicalBlobNameInvalidError: {
      ::oneflow::LogicalBlobNameInvalidError of_proto_logical_blob_name_invalid_error;
      logical_blob_name_invalid_error().ToProto(&of_proto_logical_blob_name_invalid_error);
      proto_errorproto->mutable_logical_blob_name_invalid_error()->CopyFrom(of_proto_logical_blob_name_invalid_error);
      break;
    }
    case ::oneflow::ErrorProto::kOpNameExistError: {
      ::oneflow::OpNameExistError of_proto_op_name_exist_error;
      op_name_exist_error().ToProto(&of_proto_op_name_exist_error);
      proto_errorproto->mutable_op_name_exist_error()->CopyFrom(of_proto_op_name_exist_error);
      break;
    }
    case ::oneflow::ErrorProto::kOpConfDeviceTagNoSetError: {
      ::oneflow::OpConfDeviceTagNoSetError of_proto_op_conf_device_tag_no_set_error;
      op_conf_device_tag_no_set_error().ToProto(&of_proto_op_conf_device_tag_no_set_error);
      proto_errorproto->mutable_op_conf_device_tag_no_set_error()->CopyFrom(of_proto_op_conf_device_tag_no_set_error);
      break;
    }
    case ::oneflow::ErrorProto::kPlacementError: {
      ::oneflow::PlacementError of_proto_placement_error;
      placement_error().ToProto(&of_proto_placement_error);
      proto_errorproto->mutable_placement_error()->CopyFrom(of_proto_placement_error);
      break;
    }
    case ::oneflow::ErrorProto::kBlobSplitAxisInferError: {
      ::oneflow::BlobSplitAxisInferError of_proto_blob_split_axis_infer_error;
      blob_split_axis_infer_error().ToProto(&of_proto_blob_split_axis_infer_error);
      proto_errorproto->mutable_blob_split_axis_infer_error()->CopyFrom(of_proto_blob_split_axis_infer_error);
      break;
    }
    case ::oneflow::ErrorProto::kUnknownJobBuildAndInferError: {
      ::oneflow::UnknownJobBuildAndInferError of_proto_unknown_job_build_and_infer_error;
      unknown_job_build_and_infer_error().ToProto(&of_proto_unknown_job_build_and_infer_error);
      proto_errorproto->mutable_unknown_job_build_and_infer_error()->CopyFrom(of_proto_unknown_job_build_and_infer_error);
      break;
    }
    case ::oneflow::ErrorProto::kRwMutexedObjectNotFoundError: {
      ::oneflow::RwMutexedObjectNotFoundError of_proto_rw_mutexed_object_not_found_error;
      rw_mutexed_object_not_found_error().ToProto(&of_proto_rw_mutexed_object_not_found_error);
      proto_errorproto->mutable_rw_mutexed_object_not_found_error()->CopyFrom(of_proto_rw_mutexed_object_not_found_error);
      break;
    }
    case ::oneflow::ErrorProto::kSymbolIdUninitializedError: {
      ::oneflow::SymbolIdUninitializedError of_proto_symbol_id_uninitialized_error;
      symbol_id_uninitialized_error().ToProto(&of_proto_symbol_id_uninitialized_error);
      proto_errorproto->mutable_symbol_id_uninitialized_error()->CopyFrom(of_proto_symbol_id_uninitialized_error);
      break;
    }
    case ::oneflow::ErrorProto::kUnknownError: {
      ::oneflow::UnknownError of_proto_unknown_error;
      unknown_error().ToProto(&of_proto_unknown_error);
      proto_errorproto->mutable_unknown_error()->CopyFrom(of_proto_unknown_error);
      break;
    }
    case ::oneflow::ErrorProto::kCompileOptionWrongError: {
      ::oneflow::CompileOptionWrongError of_proto_compile_option_wrong_error;
      compile_option_wrong_error().ToProto(&of_proto_compile_option_wrong_error);
      proto_errorproto->mutable_compile_option_wrong_error()->CopyFrom(of_proto_compile_option_wrong_error);
      break;
    }
    case ::oneflow::ErrorProto::kInputDeviceNotMatchError: {
      ::oneflow::InputDeviceNotMatchError of_proto_input_device_not_match_error;
      input_device_not_match_error().ToProto(&of_proto_input_device_not_match_error);
      proto_errorproto->mutable_input_device_not_match_error()->CopyFrom(of_proto_input_device_not_match_error);
      break;
    }
    case ::oneflow::ErrorProto::ERROR_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstErrorProto::_ErrorProto_::DebugString() const {
  ::oneflow::ErrorProto proto_errorproto;
  this->ToProto(&proto_errorproto);
  return proto_errorproto.DebugString();
}

void ConstErrorProto::_ErrorProto_::Clear() {
  clear_error_summary();
  clear_msg();
  clear_stack_frame();
  clear_error_type();
}

void ConstErrorProto::_ErrorProto_::CopyFrom(const _ErrorProto_& other) {
  if (other.has_error_summary()) {
    set_error_summary(other.error_summary());
  } else {
    clear_error_summary();
  }
  if (other.has_msg()) {
    set_msg(other.msg());
  } else {
    clear_msg();
  }
  mutable_stack_frame()->CopyFrom(other.stack_frame());
  error_type_copy_from(other);
}


// optional field error_summary
bool ConstErrorProto::_ErrorProto_::has_error_summary() const {
  return has_error_summary_;
}
const ::std::string& ConstErrorProto::_ErrorProto_::error_summary() const {
  if (has_error_summary_) { return error_summary_; }
  static const ::std::string default_static_value =
    ::std::string("");
  return default_static_value;
}
void ConstErrorProto::_ErrorProto_::clear_error_summary() {
  has_error_summary_ = false;
}
void ConstErrorProto::_ErrorProto_::set_error_summary(const ::std::string& value) {
  error_summary_ = value;
  has_error_summary_ = true;
}
::std::string* ConstErrorProto::_ErrorProto_::mutable_error_summary() {
  has_error_summary_ = true;
  return &error_summary_;
}

// optional field msg
bool ConstErrorProto::_ErrorProto_::has_msg() const {
  return has_msg_;
}
const ::std::string& ConstErrorProto::_ErrorProto_::msg() const {
  if (has_msg_) { return msg_; }
  static const ::std::string default_static_value =
    ::std::string("");
  return default_static_value;
}
void ConstErrorProto::_ErrorProto_::clear_msg() {
  has_msg_ = false;
}
void ConstErrorProto::_ErrorProto_::set_msg(const ::std::string& value) {
  msg_ = value;
  has_msg_ = true;
}
::std::string* ConstErrorProto::_ErrorProto_::mutable_msg() {
  has_msg_ = true;
  return &msg_;
}

// repeated field stack_frame
::std::size_t ConstErrorProto::_ErrorProto_::stack_frame_size() const {
  if (!stack_frame_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
    return default_static_value->size();
  }
  return stack_frame_->size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& ConstErrorProto::_ErrorProto_::stack_frame() const {
  if (!stack_frame_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
    return *(default_static_value.get());
  }
  return *(stack_frame_.get());
}
const ::oneflow::cfg::ErrorStackFrame& ConstErrorProto::_ErrorProto_::stack_frame(::std::size_t index) const {
  if (!stack_frame_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
    return default_static_value->Get(index);
  }
  return stack_frame_->Get(index);
}
void ConstErrorProto::_ErrorProto_::clear_stack_frame() {
  if (!stack_frame_) {
    stack_frame_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
  }
  return stack_frame_->Clear();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_* ConstErrorProto::_ErrorProto_::mutable_stack_frame() {
  if (!stack_frame_) {
    stack_frame_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
  }
  return  stack_frame_.get();
}
::oneflow::cfg::ErrorStackFrame* ConstErrorProto::_ErrorProto_::mutable_stack_frame(::std::size_t index) {
  if (!stack_frame_) {
    stack_frame_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
  }
  return  stack_frame_->Mutable(index);
}
::oneflow::cfg::ErrorStackFrame* ConstErrorProto::_ErrorProto_::add_stack_frame() {
  if (!stack_frame_) {
    stack_frame_ = ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>();
  }
  return stack_frame_->Add();
}

// oneof field error_type: config_assert_failed_error
bool ConstErrorProto::_ErrorProto_::has_config_assert_failed_error() const {
  return error_type_case() == kConfigAssertFailedError;
}
void ConstErrorProto::_ErrorProto_::clear_config_assert_failed_error() {
  if (has_config_assert_failed_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.config_assert_failed_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ConfigAssertFailedError& ConstErrorProto::_ErrorProto_::config_assert_failed_error() const {
  if (has_config_assert_failed_error()) {
      const ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>*>(&(error_type_.config_assert_failed_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError> default_static_value = ::std::make_shared<::oneflow::cfg::ConfigAssertFailedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::ConfigAssertFailedError* ConstErrorProto::_ErrorProto_::mutable_config_assert_failed_error() {
  if(!has_config_assert_failed_error()) {
    clear_error_type();
    new (&(error_type_.config_assert_failed_error_)) ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>(new ::oneflow::cfg::ConfigAssertFailedError());
  }
  error_type_case_ = kConfigAssertFailedError;
  ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>*>(&(error_type_.config_assert_failed_error_));
  return  (*ptr).get();
}

// oneof field error_type: config_resource_unavailable_error
bool ConstErrorProto::_ErrorProto_::has_config_resource_unavailable_error() const {
  return error_type_case() == kConfigResourceUnavailableError;
}
void ConstErrorProto::_ErrorProto_::clear_config_resource_unavailable_error() {
  if (has_config_resource_unavailable_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.config_resource_unavailable_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ConfigResourceUnavailableError& ConstErrorProto::_ErrorProto_::config_resource_unavailable_error() const {
  if (has_config_resource_unavailable_error()) {
      const ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>*>(&(error_type_.config_resource_unavailable_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError> default_static_value = ::std::make_shared<::oneflow::cfg::ConfigResourceUnavailableError>();
    return *default_static_value;
    }
}
::oneflow::cfg::ConfigResourceUnavailableError* ConstErrorProto::_ErrorProto_::mutable_config_resource_unavailable_error() {
  if(!has_config_resource_unavailable_error()) {
    clear_error_type();
    new (&(error_type_.config_resource_unavailable_error_)) ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>(new ::oneflow::cfg::ConfigResourceUnavailableError());
  }
  error_type_case_ = kConfigResourceUnavailableError;
  ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>*>(&(error_type_.config_resource_unavailable_error_));
  return  (*ptr).get();
}

// oneof field error_type: proto_parse_failed_error
bool ConstErrorProto::_ErrorProto_::has_proto_parse_failed_error() const {
  return error_type_case() == kProtoParseFailedError;
}
void ConstErrorProto::_ErrorProto_::clear_proto_parse_failed_error() {
  if (has_proto_parse_failed_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.proto_parse_failed_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ProtoParseFailedError& ConstErrorProto::_ErrorProto_::proto_parse_failed_error() const {
  if (has_proto_parse_failed_error()) {
      const ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>*>(&(error_type_.proto_parse_failed_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError> default_static_value = ::std::make_shared<::oneflow::cfg::ProtoParseFailedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::ProtoParseFailedError* ConstErrorProto::_ErrorProto_::mutable_proto_parse_failed_error() {
  if(!has_proto_parse_failed_error()) {
    clear_error_type();
    new (&(error_type_.proto_parse_failed_error_)) ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>(new ::oneflow::cfg::ProtoParseFailedError());
  }
  error_type_case_ = kProtoParseFailedError;
  ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>*>(&(error_type_.proto_parse_failed_error_));
  return  (*ptr).get();
}

// oneof field error_type: check_failed_error
bool ConstErrorProto::_ErrorProto_::has_check_failed_error() const {
  return error_type_case() == kCheckFailedError;
}
void ConstErrorProto::_ErrorProto_::clear_check_failed_error() {
  if (has_check_failed_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CheckFailedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.check_failed_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::CheckFailedError& ConstErrorProto::_ErrorProto_::check_failed_error() const {
  if (has_check_failed_error()) {
      const ::std::shared_ptr<::oneflow::cfg::CheckFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::CheckFailedError>*>(&(error_type_.check_failed_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::CheckFailedError> default_static_value = ::std::make_shared<::oneflow::cfg::CheckFailedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::CheckFailedError* ConstErrorProto::_ErrorProto_::mutable_check_failed_error() {
  if(!has_check_failed_error()) {
    clear_error_type();
    new (&(error_type_.check_failed_error_)) ::std::shared_ptr<::oneflow::cfg::CheckFailedError>(new ::oneflow::cfg::CheckFailedError());
  }
  error_type_case_ = kCheckFailedError;
  ::std::shared_ptr<::oneflow::cfg::CheckFailedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::CheckFailedError>*>(&(error_type_.check_failed_error_));
  return  (*ptr).get();
}

// oneof field error_type: todo_error
bool ConstErrorProto::_ErrorProto_::has_todo_error() const {
  return error_type_case() == kTodoError;
}
void ConstErrorProto::_ErrorProto_::clear_todo_error() {
  if (has_todo_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TodoError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.todo_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TodoError& ConstErrorProto::_ErrorProto_::todo_error() const {
  if (has_todo_error()) {
      const ::std::shared_ptr<::oneflow::cfg::TodoError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TodoError>*>(&(error_type_.todo_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TodoError> default_static_value = ::std::make_shared<::oneflow::cfg::TodoError>();
    return *default_static_value;
    }
}
::oneflow::cfg::TodoError* ConstErrorProto::_ErrorProto_::mutable_todo_error() {
  if(!has_todo_error()) {
    clear_error_type();
    new (&(error_type_.todo_error_)) ::std::shared_ptr<::oneflow::cfg::TodoError>(new ::oneflow::cfg::TodoError());
  }
  error_type_case_ = kTodoError;
  ::std::shared_ptr<::oneflow::cfg::TodoError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TodoError>*>(&(error_type_.todo_error_));
  return  (*ptr).get();
}

// oneof field error_type: unimplemented_error
bool ConstErrorProto::_ErrorProto_::has_unimplemented_error() const {
  return error_type_case() == kUnimplementedError;
}
void ConstErrorProto::_ErrorProto_::clear_unimplemented_error() {
  if (has_unimplemented_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnimplementedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unimplemented_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::UnimplementedError& ConstErrorProto::_ErrorProto_::unimplemented_error() const {
  if (has_unimplemented_error()) {
      const ::std::shared_ptr<::oneflow::cfg::UnimplementedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::UnimplementedError>*>(&(error_type_.unimplemented_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::UnimplementedError> default_static_value = ::std::make_shared<::oneflow::cfg::UnimplementedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::UnimplementedError* ConstErrorProto::_ErrorProto_::mutable_unimplemented_error() {
  if(!has_unimplemented_error()) {
    clear_error_type();
    new (&(error_type_.unimplemented_error_)) ::std::shared_ptr<::oneflow::cfg::UnimplementedError>(new ::oneflow::cfg::UnimplementedError());
  }
  error_type_case_ = kUnimplementedError;
  ::std::shared_ptr<::oneflow::cfg::UnimplementedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::UnimplementedError>*>(&(error_type_.unimplemented_error_));
  return  (*ptr).get();
}

// oneof field error_type: boxing_not_supported_error
bool ConstErrorProto::_ErrorProto_::has_boxing_not_supported_error() const {
  return error_type_case() == kBoxingNotSupportedError;
}
void ConstErrorProto::_ErrorProto_::clear_boxing_not_supported_error() {
  if (has_boxing_not_supported_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.boxing_not_supported_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::BoxingNotSupportedError& ConstErrorProto::_ErrorProto_::boxing_not_supported_error() const {
  if (has_boxing_not_supported_error()) {
      const ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>*>(&(error_type_.boxing_not_supported_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError> default_static_value = ::std::make_shared<::oneflow::cfg::BoxingNotSupportedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::BoxingNotSupportedError* ConstErrorProto::_ErrorProto_::mutable_boxing_not_supported_error() {
  if(!has_boxing_not_supported_error()) {
    clear_error_type();
    new (&(error_type_.boxing_not_supported_error_)) ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>(new ::oneflow::cfg::BoxingNotSupportedError());
  }
  error_type_case_ = kBoxingNotSupportedError;
  ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>*>(&(error_type_.boxing_not_supported_error_));
  return  (*ptr).get();
}

// oneof field error_type: gradient_function_not_found_error
bool ConstErrorProto::_ErrorProto_::has_gradient_function_not_found_error() const {
  return error_type_case() == kGradientFunctionNotFoundError;
}
void ConstErrorProto::_ErrorProto_::clear_gradient_function_not_found_error() {
  if (has_gradient_function_not_found_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.gradient_function_not_found_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::GradientFunctionNotFoundError& ConstErrorProto::_ErrorProto_::gradient_function_not_found_error() const {
  if (has_gradient_function_not_found_error()) {
      const ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>*>(&(error_type_.gradient_function_not_found_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError> default_static_value = ::std::make_shared<::oneflow::cfg::GradientFunctionNotFoundError>();
    return *default_static_value;
    }
}
::oneflow::cfg::GradientFunctionNotFoundError* ConstErrorProto::_ErrorProto_::mutable_gradient_function_not_found_error() {
  if(!has_gradient_function_not_found_error()) {
    clear_error_type();
    new (&(error_type_.gradient_function_not_found_error_)) ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>(new ::oneflow::cfg::GradientFunctionNotFoundError());
  }
  error_type_case_ = kGradientFunctionNotFoundError;
  ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>*>(&(error_type_.gradient_function_not_found_error_));
  return  (*ptr).get();
}

// oneof field error_type: op_kernel_not_found_error
bool ConstErrorProto::_ErrorProto_::has_op_kernel_not_found_error() const {
  return error_type_case() == kOpKernelNotFoundError;
}
void ConstErrorProto::_ErrorProto_::clear_op_kernel_not_found_error() {
  if (has_op_kernel_not_found_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_kernel_not_found_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OpKernelNotFoundError& ConstErrorProto::_ErrorProto_::op_kernel_not_found_error() const {
  if (has_op_kernel_not_found_error()) {
      const ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>*>(&(error_type_.op_kernel_not_found_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError> default_static_value = ::std::make_shared<::oneflow::cfg::OpKernelNotFoundError>();
    return *default_static_value;
    }
}
::oneflow::cfg::OpKernelNotFoundError* ConstErrorProto::_ErrorProto_::mutable_op_kernel_not_found_error() {
  if(!has_op_kernel_not_found_error()) {
    clear_error_type();
    new (&(error_type_.op_kernel_not_found_error_)) ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>(new ::oneflow::cfg::OpKernelNotFoundError());
  }
  error_type_case_ = kOpKernelNotFoundError;
  ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>*>(&(error_type_.op_kernel_not_found_error_));
  return  (*ptr).get();
}

// oneof field error_type: multiple_op_kernels_matched_error
bool ConstErrorProto::_ErrorProto_::has_multiple_op_kernels_matched_error() const {
  return error_type_case() == kMultipleOpKernelsMatchedError;
}
void ConstErrorProto::_ErrorProto_::clear_multiple_op_kernels_matched_error() {
  if (has_multiple_op_kernels_matched_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.multiple_op_kernels_matched_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::MultipleOpKernelsMatchedError& ConstErrorProto::_ErrorProto_::multiple_op_kernels_matched_error() const {
  if (has_multiple_op_kernels_matched_error()) {
      const ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>*>(&(error_type_.multiple_op_kernels_matched_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError> default_static_value = ::std::make_shared<::oneflow::cfg::MultipleOpKernelsMatchedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::MultipleOpKernelsMatchedError* ConstErrorProto::_ErrorProto_::mutable_multiple_op_kernels_matched_error() {
  if(!has_multiple_op_kernels_matched_error()) {
    clear_error_type();
    new (&(error_type_.multiple_op_kernels_matched_error_)) ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>(new ::oneflow::cfg::MultipleOpKernelsMatchedError());
  }
  error_type_case_ = kMultipleOpKernelsMatchedError;
  ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>*>(&(error_type_.multiple_op_kernels_matched_error_));
  return  (*ptr).get();
}

// oneof field error_type: memory_zone_out_of_memory_error
bool ConstErrorProto::_ErrorProto_::has_memory_zone_out_of_memory_error() const {
  return error_type_case() == kMemoryZoneOutOfMemoryError;
}
void ConstErrorProto::_ErrorProto_::clear_memory_zone_out_of_memory_error() {
  if (has_memory_zone_out_of_memory_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.memory_zone_out_of_memory_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::MemoryZoneOutOfMemoryError& ConstErrorProto::_ErrorProto_::memory_zone_out_of_memory_error() const {
  if (has_memory_zone_out_of_memory_error()) {
      const ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>*>(&(error_type_.memory_zone_out_of_memory_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError> default_static_value = ::std::make_shared<::oneflow::cfg::MemoryZoneOutOfMemoryError>();
    return *default_static_value;
    }
}
::oneflow::cfg::MemoryZoneOutOfMemoryError* ConstErrorProto::_ErrorProto_::mutable_memory_zone_out_of_memory_error() {
  if(!has_memory_zone_out_of_memory_error()) {
    clear_error_type();
    new (&(error_type_.memory_zone_out_of_memory_error_)) ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>(new ::oneflow::cfg::MemoryZoneOutOfMemoryError());
  }
  error_type_case_ = kMemoryZoneOutOfMemoryError;
  ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>*>(&(error_type_.memory_zone_out_of_memory_error_));
  return  (*ptr).get();
}

// oneof field error_type: loss_blob_not_found_error
bool ConstErrorProto::_ErrorProto_::has_loss_blob_not_found_error() const {
  return error_type_case() == kLossBlobNotFoundError;
}
void ConstErrorProto::_ErrorProto_::clear_loss_blob_not_found_error() {
  if (has_loss_blob_not_found_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.loss_blob_not_found_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LossBlobNotFoundError& ConstErrorProto::_ErrorProto_::loss_blob_not_found_error() const {
  if (has_loss_blob_not_found_error()) {
      const ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>*>(&(error_type_.loss_blob_not_found_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError> default_static_value = ::std::make_shared<::oneflow::cfg::LossBlobNotFoundError>();
    return *default_static_value;
    }
}
::oneflow::cfg::LossBlobNotFoundError* ConstErrorProto::_ErrorProto_::mutable_loss_blob_not_found_error() {
  if(!has_loss_blob_not_found_error()) {
    clear_error_type();
    new (&(error_type_.loss_blob_not_found_error_)) ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>(new ::oneflow::cfg::LossBlobNotFoundError());
  }
  error_type_case_ = kLossBlobNotFoundError;
  ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>*>(&(error_type_.loss_blob_not_found_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_set_empty_error
bool ConstErrorProto::_ErrorProto_::has_job_set_empty_error() const {
  return error_type_case() == kJobSetEmptyError;
}
void ConstErrorProto::_ErrorProto_::clear_job_set_empty_error() {
  if (has_job_set_empty_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_set_empty_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobSetEmptyError& ConstErrorProto::_ErrorProto_::job_set_empty_error() const {
  if (has_job_set_empty_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>*>(&(error_type_.job_set_empty_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError> default_static_value = ::std::make_shared<::oneflow::cfg::JobSetEmptyError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobSetEmptyError* ConstErrorProto::_ErrorProto_::mutable_job_set_empty_error() {
  if(!has_job_set_empty_error()) {
    clear_error_type();
    new (&(error_type_.job_set_empty_error_)) ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>(new ::oneflow::cfg::JobSetEmptyError());
  }
  error_type_case_ = kJobSetEmptyError;
  ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>*>(&(error_type_.job_set_empty_error_));
  return  (*ptr).get();
}

// oneof field error_type: device_tag_not_found_error
bool ConstErrorProto::_ErrorProto_::has_device_tag_not_found_error() const {
  return error_type_case() == kDeviceTagNotFoundError;
}
void ConstErrorProto::_ErrorProto_::clear_device_tag_not_found_error() {
  if (has_device_tag_not_found_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.device_tag_not_found_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::DeviceTagNotFoundError& ConstErrorProto::_ErrorProto_::device_tag_not_found_error() const {
  if (has_device_tag_not_found_error()) {
      const ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>*>(&(error_type_.device_tag_not_found_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError> default_static_value = ::std::make_shared<::oneflow::cfg::DeviceTagNotFoundError>();
    return *default_static_value;
    }
}
::oneflow::cfg::DeviceTagNotFoundError* ConstErrorProto::_ErrorProto_::mutable_device_tag_not_found_error() {
  if(!has_device_tag_not_found_error()) {
    clear_error_type();
    new (&(error_type_.device_tag_not_found_error_)) ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>(new ::oneflow::cfg::DeviceTagNotFoundError());
  }
  error_type_case_ = kDeviceTagNotFoundError;
  ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>*>(&(error_type_.device_tag_not_found_error_));
  return  (*ptr).get();
}

// oneof field error_type: value_error
bool ConstErrorProto::_ErrorProto_::has_value_error() const {
  return error_type_case() == kValueError;
}
void ConstErrorProto::_ErrorProto_::clear_value_error() {
  if (has_value_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ValueError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.value_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ValueError& ConstErrorProto::_ErrorProto_::value_error() const {
  if (has_value_error()) {
      const ::std::shared_ptr<::oneflow::cfg::ValueError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ValueError>*>(&(error_type_.value_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ValueError> default_static_value = ::std::make_shared<::oneflow::cfg::ValueError>();
    return *default_static_value;
    }
}
::oneflow::cfg::ValueError* ConstErrorProto::_ErrorProto_::mutable_value_error() {
  if(!has_value_error()) {
    clear_error_type();
    new (&(error_type_.value_error_)) ::std::shared_ptr<::oneflow::cfg::ValueError>(new ::oneflow::cfg::ValueError());
  }
  error_type_case_ = kValueError;
  ::std::shared_ptr<::oneflow::cfg::ValueError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ValueError>*>(&(error_type_.value_error_));
  return  (*ptr).get();
}

// oneof field error_type: index_error
bool ConstErrorProto::_ErrorProto_::has_index_error() const {
  return error_type_case() == kIndexError;
}
void ConstErrorProto::_ErrorProto_::clear_index_error() {
  if (has_index_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::IndexError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.index_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::IndexError& ConstErrorProto::_ErrorProto_::index_error() const {
  if (has_index_error()) {
      const ::std::shared_ptr<::oneflow::cfg::IndexError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::IndexError>*>(&(error_type_.index_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::IndexError> default_static_value = ::std::make_shared<::oneflow::cfg::IndexError>();
    return *default_static_value;
    }
}
::oneflow::cfg::IndexError* ConstErrorProto::_ErrorProto_::mutable_index_error() {
  if(!has_index_error()) {
    clear_error_type();
    new (&(error_type_.index_error_)) ::std::shared_ptr<::oneflow::cfg::IndexError>(new ::oneflow::cfg::IndexError());
  }
  error_type_case_ = kIndexError;
  ::std::shared_ptr<::oneflow::cfg::IndexError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::IndexError>*>(&(error_type_.index_error_));
  return  (*ptr).get();
}

// oneof field error_type: timeout_error
bool ConstErrorProto::_ErrorProto_::has_timeout_error() const {
  return error_type_case() == kTimeoutError;
}
void ConstErrorProto::_ErrorProto_::clear_timeout_error() {
  if (has_timeout_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TimeoutError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.timeout_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TimeoutError& ConstErrorProto::_ErrorProto_::timeout_error() const {
  if (has_timeout_error()) {
      const ::std::shared_ptr<::oneflow::cfg::TimeoutError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TimeoutError>*>(&(error_type_.timeout_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TimeoutError> default_static_value = ::std::make_shared<::oneflow::cfg::TimeoutError>();
    return *default_static_value;
    }
}
::oneflow::cfg::TimeoutError* ConstErrorProto::_ErrorProto_::mutable_timeout_error() {
  if(!has_timeout_error()) {
    clear_error_type();
    new (&(error_type_.timeout_error_)) ::std::shared_ptr<::oneflow::cfg::TimeoutError>(new ::oneflow::cfg::TimeoutError());
  }
  error_type_case_ = kTimeoutError;
  ::std::shared_ptr<::oneflow::cfg::TimeoutError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TimeoutError>*>(&(error_type_.timeout_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_name_exist_error
bool ConstErrorProto::_ErrorProto_::has_job_name_exist_error() const {
  return error_type_case() == kJobNameExistError;
}
void ConstErrorProto::_ErrorProto_::clear_job_name_exist_error() {
  if (has_job_name_exist_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameExistError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_exist_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobNameExistError& ConstErrorProto::_ErrorProto_::job_name_exist_error() const {
  if (has_job_name_exist_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobNameExistError>*>(&(error_type_.job_name_exist_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobNameExistError> default_static_value = ::std::make_shared<::oneflow::cfg::JobNameExistError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobNameExistError* ConstErrorProto::_ErrorProto_::mutable_job_name_exist_error() {
  if(!has_job_name_exist_error()) {
    clear_error_type();
    new (&(error_type_.job_name_exist_error_)) ::std::shared_ptr<::oneflow::cfg::JobNameExistError>(new ::oneflow::cfg::JobNameExistError());
  }
  error_type_case_ = kJobNameExistError;
  ::std::shared_ptr<::oneflow::cfg::JobNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobNameExistError>*>(&(error_type_.job_name_exist_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_name_empty_error
bool ConstErrorProto::_ErrorProto_::has_job_name_empty_error() const {
  return error_type_case() == kJobNameEmptyError;
}
void ConstErrorProto::_ErrorProto_::clear_job_name_empty_error() {
  if (has_job_name_empty_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_empty_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobNameEmptyError& ConstErrorProto::_ErrorProto_::job_name_empty_error() const {
  if (has_job_name_empty_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>*>(&(error_type_.job_name_empty_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError> default_static_value = ::std::make_shared<::oneflow::cfg::JobNameEmptyError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobNameEmptyError* ConstErrorProto::_ErrorProto_::mutable_job_name_empty_error() {
  if(!has_job_name_empty_error()) {
    clear_error_type();
    new (&(error_type_.job_name_empty_error_)) ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>(new ::oneflow::cfg::JobNameEmptyError());
  }
  error_type_case_ = kJobNameEmptyError;
  ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>*>(&(error_type_.job_name_empty_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_name_not_equal_error
bool ConstErrorProto::_ErrorProto_::has_job_name_not_equal_error() const {
  return error_type_case() == kJobNameNotEqualError;
}
void ConstErrorProto::_ErrorProto_::clear_job_name_not_equal_error() {
  if (has_job_name_not_equal_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_not_equal_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobNameNotEqualError& ConstErrorProto::_ErrorProto_::job_name_not_equal_error() const {
  if (has_job_name_not_equal_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>*>(&(error_type_.job_name_not_equal_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError> default_static_value = ::std::make_shared<::oneflow::cfg::JobNameNotEqualError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobNameNotEqualError* ConstErrorProto::_ErrorProto_::mutable_job_name_not_equal_error() {
  if(!has_job_name_not_equal_error()) {
    clear_error_type();
    new (&(error_type_.job_name_not_equal_error_)) ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>(new ::oneflow::cfg::JobNameNotEqualError());
  }
  error_type_case_ = kJobNameNotEqualError;
  ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>*>(&(error_type_.job_name_not_equal_error_));
  return  (*ptr).get();
}

// oneof field error_type: no_job_build_and_infer_ctx_error
bool ConstErrorProto::_ErrorProto_::has_no_job_build_and_infer_ctx_error() const {
  return error_type_case() == kNoJobBuildAndInferCtxError;
}
void ConstErrorProto::_ErrorProto_::clear_no_job_build_and_infer_ctx_error() {
  if (has_no_job_build_and_infer_ctx_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.no_job_build_and_infer_ctx_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::NoJobBuildAndInferCtxError& ConstErrorProto::_ErrorProto_::no_job_build_and_infer_ctx_error() const {
  if (has_no_job_build_and_infer_ctx_error()) {
      const ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>*>(&(error_type_.no_job_build_and_infer_ctx_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError> default_static_value = ::std::make_shared<::oneflow::cfg::NoJobBuildAndInferCtxError>();
    return *default_static_value;
    }
}
::oneflow::cfg::NoJobBuildAndInferCtxError* ConstErrorProto::_ErrorProto_::mutable_no_job_build_and_infer_ctx_error() {
  if(!has_no_job_build_and_infer_ctx_error()) {
    clear_error_type();
    new (&(error_type_.no_job_build_and_infer_ctx_error_)) ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>(new ::oneflow::cfg::NoJobBuildAndInferCtxError());
  }
  error_type_case_ = kNoJobBuildAndInferCtxError;
  ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>*>(&(error_type_.no_job_build_and_infer_ctx_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_conf_frozen_error
bool ConstErrorProto::_ErrorProto_::has_job_conf_frozen_error() const {
  return error_type_case() == kJobConfFrozenError;
}
void ConstErrorProto::_ErrorProto_::clear_job_conf_frozen_error() {
  if (has_job_conf_frozen_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_frozen_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobConfFrozenError& ConstErrorProto::_ErrorProto_::job_conf_frozen_error() const {
  if (has_job_conf_frozen_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>*>(&(error_type_.job_conf_frozen_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError> default_static_value = ::std::make_shared<::oneflow::cfg::JobConfFrozenError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobConfFrozenError* ConstErrorProto::_ErrorProto_::mutable_job_conf_frozen_error() {
  if(!has_job_conf_frozen_error()) {
    clear_error_type();
    new (&(error_type_.job_conf_frozen_error_)) ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>(new ::oneflow::cfg::JobConfFrozenError());
  }
  error_type_case_ = kJobConfFrozenError;
  ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>*>(&(error_type_.job_conf_frozen_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_conf_not_set_error
bool ConstErrorProto::_ErrorProto_::has_job_conf_not_set_error() const {
  return error_type_case() == kJobConfNotSetError;
}
void ConstErrorProto::_ErrorProto_::clear_job_conf_not_set_error() {
  if (has_job_conf_not_set_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_not_set_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobConfNotSetError& ConstErrorProto::_ErrorProto_::job_conf_not_set_error() const {
  if (has_job_conf_not_set_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>*>(&(error_type_.job_conf_not_set_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError> default_static_value = ::std::make_shared<::oneflow::cfg::JobConfNotSetError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobConfNotSetError* ConstErrorProto::_ErrorProto_::mutable_job_conf_not_set_error() {
  if(!has_job_conf_not_set_error()) {
    clear_error_type();
    new (&(error_type_.job_conf_not_set_error_)) ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>(new ::oneflow::cfg::JobConfNotSetError());
  }
  error_type_case_ = kJobConfNotSetError;
  ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>*>(&(error_type_.job_conf_not_set_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_conf_repeated_set_error
bool ConstErrorProto::_ErrorProto_::has_job_conf_repeated_set_error() const {
  return error_type_case() == kJobConfRepeatedSetError;
}
void ConstErrorProto::_ErrorProto_::clear_job_conf_repeated_set_error() {
  if (has_job_conf_repeated_set_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_repeated_set_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobConfRepeatedSetError& ConstErrorProto::_ErrorProto_::job_conf_repeated_set_error() const {
  if (has_job_conf_repeated_set_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>*>(&(error_type_.job_conf_repeated_set_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError> default_static_value = ::std::make_shared<::oneflow::cfg::JobConfRepeatedSetError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobConfRepeatedSetError* ConstErrorProto::_ErrorProto_::mutable_job_conf_repeated_set_error() {
  if(!has_job_conf_repeated_set_error()) {
    clear_error_type();
    new (&(error_type_.job_conf_repeated_set_error_)) ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>(new ::oneflow::cfg::JobConfRepeatedSetError());
  }
  error_type_case_ = kJobConfRepeatedSetError;
  ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>*>(&(error_type_.job_conf_repeated_set_error_));
  return  (*ptr).get();
}

// oneof field error_type: job_type_not_set_error
bool ConstErrorProto::_ErrorProto_::has_job_type_not_set_error() const {
  return error_type_case() == kJobTypeNotSetError;
}
void ConstErrorProto::_ErrorProto_::clear_job_type_not_set_error() {
  if (has_job_type_not_set_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_type_not_set_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobTypeNotSetError& ConstErrorProto::_ErrorProto_::job_type_not_set_error() const {
  if (has_job_type_not_set_error()) {
      const ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>*>(&(error_type_.job_type_not_set_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError> default_static_value = ::std::make_shared<::oneflow::cfg::JobTypeNotSetError>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobTypeNotSetError* ConstErrorProto::_ErrorProto_::mutable_job_type_not_set_error() {
  if(!has_job_type_not_set_error()) {
    clear_error_type();
    new (&(error_type_.job_type_not_set_error_)) ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>(new ::oneflow::cfg::JobTypeNotSetError());
  }
  error_type_case_ = kJobTypeNotSetError;
  ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>*>(&(error_type_.job_type_not_set_error_));
  return  (*ptr).get();
}

// oneof field error_type: logical_blob_name_not_exist_error
bool ConstErrorProto::_ErrorProto_::has_logical_blob_name_not_exist_error() const {
  return error_type_case() == kLogicalBlobNameNotExistError;
}
void ConstErrorProto::_ErrorProto_::clear_logical_blob_name_not_exist_error() {
  if (has_logical_blob_name_not_exist_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_not_exist_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LogicalBlobNameNotExistError& ConstErrorProto::_ErrorProto_::logical_blob_name_not_exist_error() const {
  if (has_logical_blob_name_not_exist_error()) {
      const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>*>(&(error_type_.logical_blob_name_not_exist_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError> default_static_value = ::std::make_shared<::oneflow::cfg::LogicalBlobNameNotExistError>();
    return *default_static_value;
    }
}
::oneflow::cfg::LogicalBlobNameNotExistError* ConstErrorProto::_ErrorProto_::mutable_logical_blob_name_not_exist_error() {
  if(!has_logical_blob_name_not_exist_error()) {
    clear_error_type();
    new (&(error_type_.logical_blob_name_not_exist_error_)) ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>(new ::oneflow::cfg::LogicalBlobNameNotExistError());
  }
  error_type_case_ = kLogicalBlobNameNotExistError;
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>*>(&(error_type_.logical_blob_name_not_exist_error_));
  return  (*ptr).get();
}

// oneof field error_type: logical_blob_name_exist_error
bool ConstErrorProto::_ErrorProto_::has_logical_blob_name_exist_error() const {
  return error_type_case() == kLogicalBlobNameExistError;
}
void ConstErrorProto::_ErrorProto_::clear_logical_blob_name_exist_error() {
  if (has_logical_blob_name_exist_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_exist_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LogicalBlobNameExistError& ConstErrorProto::_ErrorProto_::logical_blob_name_exist_error() const {
  if (has_logical_blob_name_exist_error()) {
      const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>*>(&(error_type_.logical_blob_name_exist_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError> default_static_value = ::std::make_shared<::oneflow::cfg::LogicalBlobNameExistError>();
    return *default_static_value;
    }
}
::oneflow::cfg::LogicalBlobNameExistError* ConstErrorProto::_ErrorProto_::mutable_logical_blob_name_exist_error() {
  if(!has_logical_blob_name_exist_error()) {
    clear_error_type();
    new (&(error_type_.logical_blob_name_exist_error_)) ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>(new ::oneflow::cfg::LogicalBlobNameExistError());
  }
  error_type_case_ = kLogicalBlobNameExistError;
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>*>(&(error_type_.logical_blob_name_exist_error_));
  return  (*ptr).get();
}

// oneof field error_type: logical_blob_name_invalid_error
bool ConstErrorProto::_ErrorProto_::has_logical_blob_name_invalid_error() const {
  return error_type_case() == kLogicalBlobNameInvalidError;
}
void ConstErrorProto::_ErrorProto_::clear_logical_blob_name_invalid_error() {
  if (has_logical_blob_name_invalid_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_invalid_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LogicalBlobNameInvalidError& ConstErrorProto::_ErrorProto_::logical_blob_name_invalid_error() const {
  if (has_logical_blob_name_invalid_error()) {
      const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>*>(&(error_type_.logical_blob_name_invalid_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError> default_static_value = ::std::make_shared<::oneflow::cfg::LogicalBlobNameInvalidError>();
    return *default_static_value;
    }
}
::oneflow::cfg::LogicalBlobNameInvalidError* ConstErrorProto::_ErrorProto_::mutable_logical_blob_name_invalid_error() {
  if(!has_logical_blob_name_invalid_error()) {
    clear_error_type();
    new (&(error_type_.logical_blob_name_invalid_error_)) ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>(new ::oneflow::cfg::LogicalBlobNameInvalidError());
  }
  error_type_case_ = kLogicalBlobNameInvalidError;
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>*>(&(error_type_.logical_blob_name_invalid_error_));
  return  (*ptr).get();
}

// oneof field error_type: op_name_exist_error
bool ConstErrorProto::_ErrorProto_::has_op_name_exist_error() const {
  return error_type_case() == kOpNameExistError;
}
void ConstErrorProto::_ErrorProto_::clear_op_name_exist_error() {
  if (has_op_name_exist_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpNameExistError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_name_exist_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OpNameExistError& ConstErrorProto::_ErrorProto_::op_name_exist_error() const {
  if (has_op_name_exist_error()) {
      const ::std::shared_ptr<::oneflow::cfg::OpNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OpNameExistError>*>(&(error_type_.op_name_exist_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OpNameExistError> default_static_value = ::std::make_shared<::oneflow::cfg::OpNameExistError>();
    return *default_static_value;
    }
}
::oneflow::cfg::OpNameExistError* ConstErrorProto::_ErrorProto_::mutable_op_name_exist_error() {
  if(!has_op_name_exist_error()) {
    clear_error_type();
    new (&(error_type_.op_name_exist_error_)) ::std::shared_ptr<::oneflow::cfg::OpNameExistError>(new ::oneflow::cfg::OpNameExistError());
  }
  error_type_case_ = kOpNameExistError;
  ::std::shared_ptr<::oneflow::cfg::OpNameExistError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OpNameExistError>*>(&(error_type_.op_name_exist_error_));
  return  (*ptr).get();
}

// oneof field error_type: op_conf_device_tag_no_set_error
bool ConstErrorProto::_ErrorProto_::has_op_conf_device_tag_no_set_error() const {
  return error_type_case() == kOpConfDeviceTagNoSetError;
}
void ConstErrorProto::_ErrorProto_::clear_op_conf_device_tag_no_set_error() {
  if (has_op_conf_device_tag_no_set_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_conf_device_tag_no_set_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OpConfDeviceTagNoSetError& ConstErrorProto::_ErrorProto_::op_conf_device_tag_no_set_error() const {
  if (has_op_conf_device_tag_no_set_error()) {
      const ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>*>(&(error_type_.op_conf_device_tag_no_set_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError> default_static_value = ::std::make_shared<::oneflow::cfg::OpConfDeviceTagNoSetError>();
    return *default_static_value;
    }
}
::oneflow::cfg::OpConfDeviceTagNoSetError* ConstErrorProto::_ErrorProto_::mutable_op_conf_device_tag_no_set_error() {
  if(!has_op_conf_device_tag_no_set_error()) {
    clear_error_type();
    new (&(error_type_.op_conf_device_tag_no_set_error_)) ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>(new ::oneflow::cfg::OpConfDeviceTagNoSetError());
  }
  error_type_case_ = kOpConfDeviceTagNoSetError;
  ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>*>(&(error_type_.op_conf_device_tag_no_set_error_));
  return  (*ptr).get();
}

// oneof field error_type: placement_error
bool ConstErrorProto::_ErrorProto_::has_placement_error() const {
  return error_type_case() == kPlacementError;
}
void ConstErrorProto::_ErrorProto_::clear_placement_error() {
  if (has_placement_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PlacementError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.placement_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PlacementError& ConstErrorProto::_ErrorProto_::placement_error() const {
  if (has_placement_error()) {
      const ::std::shared_ptr<::oneflow::cfg::PlacementError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PlacementError>*>(&(error_type_.placement_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PlacementError> default_static_value = ::std::make_shared<::oneflow::cfg::PlacementError>();
    return *default_static_value;
    }
}
::oneflow::cfg::PlacementError* ConstErrorProto::_ErrorProto_::mutable_placement_error() {
  if(!has_placement_error()) {
    clear_error_type();
    new (&(error_type_.placement_error_)) ::std::shared_ptr<::oneflow::cfg::PlacementError>(new ::oneflow::cfg::PlacementError());
  }
  error_type_case_ = kPlacementError;
  ::std::shared_ptr<::oneflow::cfg::PlacementError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PlacementError>*>(&(error_type_.placement_error_));
  return  (*ptr).get();
}

// oneof field error_type: blob_split_axis_infer_error
bool ConstErrorProto::_ErrorProto_::has_blob_split_axis_infer_error() const {
  return error_type_case() == kBlobSplitAxisInferError;
}
void ConstErrorProto::_ErrorProto_::clear_blob_split_axis_infer_error() {
  if (has_blob_split_axis_infer_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.blob_split_axis_infer_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::BlobSplitAxisInferError& ConstErrorProto::_ErrorProto_::blob_split_axis_infer_error() const {
  if (has_blob_split_axis_infer_error()) {
      const ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>*>(&(error_type_.blob_split_axis_infer_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError> default_static_value = ::std::make_shared<::oneflow::cfg::BlobSplitAxisInferError>();
    return *default_static_value;
    }
}
::oneflow::cfg::BlobSplitAxisInferError* ConstErrorProto::_ErrorProto_::mutable_blob_split_axis_infer_error() {
  if(!has_blob_split_axis_infer_error()) {
    clear_error_type();
    new (&(error_type_.blob_split_axis_infer_error_)) ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>(new ::oneflow::cfg::BlobSplitAxisInferError());
  }
  error_type_case_ = kBlobSplitAxisInferError;
  ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>*>(&(error_type_.blob_split_axis_infer_error_));
  return  (*ptr).get();
}

// oneof field error_type: unknown_job_build_and_infer_error
bool ConstErrorProto::_ErrorProto_::has_unknown_job_build_and_infer_error() const {
  return error_type_case() == kUnknownJobBuildAndInferError;
}
void ConstErrorProto::_ErrorProto_::clear_unknown_job_build_and_infer_error() {
  if (has_unknown_job_build_and_infer_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unknown_job_build_and_infer_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::UnknownJobBuildAndInferError& ConstErrorProto::_ErrorProto_::unknown_job_build_and_infer_error() const {
  if (has_unknown_job_build_and_infer_error()) {
      const ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>*>(&(error_type_.unknown_job_build_and_infer_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError> default_static_value = ::std::make_shared<::oneflow::cfg::UnknownJobBuildAndInferError>();
    return *default_static_value;
    }
}
::oneflow::cfg::UnknownJobBuildAndInferError* ConstErrorProto::_ErrorProto_::mutable_unknown_job_build_and_infer_error() {
  if(!has_unknown_job_build_and_infer_error()) {
    clear_error_type();
    new (&(error_type_.unknown_job_build_and_infer_error_)) ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>(new ::oneflow::cfg::UnknownJobBuildAndInferError());
  }
  error_type_case_ = kUnknownJobBuildAndInferError;
  ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>*>(&(error_type_.unknown_job_build_and_infer_error_));
  return  (*ptr).get();
}

// oneof field error_type: rw_mutexed_object_not_found_error
bool ConstErrorProto::_ErrorProto_::has_rw_mutexed_object_not_found_error() const {
  return error_type_case() == kRwMutexedObjectNotFoundError;
}
void ConstErrorProto::_ErrorProto_::clear_rw_mutexed_object_not_found_error() {
  if (has_rw_mutexed_object_not_found_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.rw_mutexed_object_not_found_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::RwMutexedObjectNotFoundError& ConstErrorProto::_ErrorProto_::rw_mutexed_object_not_found_error() const {
  if (has_rw_mutexed_object_not_found_error()) {
      const ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>*>(&(error_type_.rw_mutexed_object_not_found_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError> default_static_value = ::std::make_shared<::oneflow::cfg::RwMutexedObjectNotFoundError>();
    return *default_static_value;
    }
}
::oneflow::cfg::RwMutexedObjectNotFoundError* ConstErrorProto::_ErrorProto_::mutable_rw_mutexed_object_not_found_error() {
  if(!has_rw_mutexed_object_not_found_error()) {
    clear_error_type();
    new (&(error_type_.rw_mutexed_object_not_found_error_)) ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>(new ::oneflow::cfg::RwMutexedObjectNotFoundError());
  }
  error_type_case_ = kRwMutexedObjectNotFoundError;
  ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>*>(&(error_type_.rw_mutexed_object_not_found_error_));
  return  (*ptr).get();
}

// oneof field error_type: symbol_id_uninitialized_error
bool ConstErrorProto::_ErrorProto_::has_symbol_id_uninitialized_error() const {
  return error_type_case() == kSymbolIdUninitializedError;
}
void ConstErrorProto::_ErrorProto_::clear_symbol_id_uninitialized_error() {
  if (has_symbol_id_uninitialized_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.symbol_id_uninitialized_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::SymbolIdUninitializedError& ConstErrorProto::_ErrorProto_::symbol_id_uninitialized_error() const {
  if (has_symbol_id_uninitialized_error()) {
      const ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>*>(&(error_type_.symbol_id_uninitialized_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError> default_static_value = ::std::make_shared<::oneflow::cfg::SymbolIdUninitializedError>();
    return *default_static_value;
    }
}
::oneflow::cfg::SymbolIdUninitializedError* ConstErrorProto::_ErrorProto_::mutable_symbol_id_uninitialized_error() {
  if(!has_symbol_id_uninitialized_error()) {
    clear_error_type();
    new (&(error_type_.symbol_id_uninitialized_error_)) ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>(new ::oneflow::cfg::SymbolIdUninitializedError());
  }
  error_type_case_ = kSymbolIdUninitializedError;
  ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>*>(&(error_type_.symbol_id_uninitialized_error_));
  return  (*ptr).get();
}

// oneof field error_type: unknown_error
bool ConstErrorProto::_ErrorProto_::has_unknown_error() const {
  return error_type_case() == kUnknownError;
}
void ConstErrorProto::_ErrorProto_::clear_unknown_error() {
  if (has_unknown_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnknownError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unknown_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::UnknownError& ConstErrorProto::_ErrorProto_::unknown_error() const {
  if (has_unknown_error()) {
      const ::std::shared_ptr<::oneflow::cfg::UnknownError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::UnknownError>*>(&(error_type_.unknown_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::UnknownError> default_static_value = ::std::make_shared<::oneflow::cfg::UnknownError>();
    return *default_static_value;
    }
}
::oneflow::cfg::UnknownError* ConstErrorProto::_ErrorProto_::mutable_unknown_error() {
  if(!has_unknown_error()) {
    clear_error_type();
    new (&(error_type_.unknown_error_)) ::std::shared_ptr<::oneflow::cfg::UnknownError>(new ::oneflow::cfg::UnknownError());
  }
  error_type_case_ = kUnknownError;
  ::std::shared_ptr<::oneflow::cfg::UnknownError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::UnknownError>*>(&(error_type_.unknown_error_));
  return  (*ptr).get();
}

// oneof field error_type: compile_option_wrong_error
bool ConstErrorProto::_ErrorProto_::has_compile_option_wrong_error() const {
  return error_type_case() == kCompileOptionWrongError;
}
void ConstErrorProto::_ErrorProto_::clear_compile_option_wrong_error() {
  if (has_compile_option_wrong_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.compile_option_wrong_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::CompileOptionWrongError& ConstErrorProto::_ErrorProto_::compile_option_wrong_error() const {
  if (has_compile_option_wrong_error()) {
      const ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>*>(&(error_type_.compile_option_wrong_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError> default_static_value = ::std::make_shared<::oneflow::cfg::CompileOptionWrongError>();
    return *default_static_value;
    }
}
::oneflow::cfg::CompileOptionWrongError* ConstErrorProto::_ErrorProto_::mutable_compile_option_wrong_error() {
  if(!has_compile_option_wrong_error()) {
    clear_error_type();
    new (&(error_type_.compile_option_wrong_error_)) ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>(new ::oneflow::cfg::CompileOptionWrongError());
  }
  error_type_case_ = kCompileOptionWrongError;
  ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>*>(&(error_type_.compile_option_wrong_error_));
  return  (*ptr).get();
}

// oneof field error_type: input_device_not_match_error
bool ConstErrorProto::_ErrorProto_::has_input_device_not_match_error() const {
  return error_type_case() == kInputDeviceNotMatchError;
}
void ConstErrorProto::_ErrorProto_::clear_input_device_not_match_error() {
  if (has_input_device_not_match_error()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.input_device_not_match_error_));
      ptr->~Shared_ptr();
    }
    error_type_case_ = ERROR_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::InputDeviceNotMatchError& ConstErrorProto::_ErrorProto_::input_device_not_match_error() const {
  if (has_input_device_not_match_error()) {
      const ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>*>(&(error_type_.input_device_not_match_error_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError> default_static_value = ::std::make_shared<::oneflow::cfg::InputDeviceNotMatchError>();
    return *default_static_value;
    }
}
::oneflow::cfg::InputDeviceNotMatchError* ConstErrorProto::_ErrorProto_::mutable_input_device_not_match_error() {
  if(!has_input_device_not_match_error()) {
    clear_error_type();
    new (&(error_type_.input_device_not_match_error_)) ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>(new ::oneflow::cfg::InputDeviceNotMatchError());
  }
  error_type_case_ = kInputDeviceNotMatchError;
  ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>*>(&(error_type_.input_device_not_match_error_));
  return  (*ptr).get();
}
ConstErrorProto::ErrorTypeCase ConstErrorProto::_ErrorProto_::error_type_case() const {
  return error_type_case_;
}
bool ConstErrorProto::_ErrorProto_::has_error_type() const {
  return error_type_case_ != ERROR_TYPE_NOT_SET;
}
void ConstErrorProto::_ErrorProto_::clear_error_type() {
  switch (error_type_case()) {
    case kConfigAssertFailedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.config_assert_failed_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kConfigResourceUnavailableError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.config_resource_unavailable_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kProtoParseFailedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.proto_parse_failed_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kCheckFailedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CheckFailedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.check_failed_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kTodoError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TodoError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.todo_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kUnimplementedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnimplementedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unimplemented_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kBoxingNotSupportedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.boxing_not_supported_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kGradientFunctionNotFoundError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.gradient_function_not_found_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kOpKernelNotFoundError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_kernel_not_found_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMultipleOpKernelsMatchedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.multiple_op_kernels_matched_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMemoryZoneOutOfMemoryError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.memory_zone_out_of_memory_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLossBlobNotFoundError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.loss_blob_not_found_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobSetEmptyError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobSetEmptyError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_set_empty_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kDeviceTagNotFoundError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.device_tag_not_found_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kValueError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ValueError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.value_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kIndexError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::IndexError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.index_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kTimeoutError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TimeoutError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.timeout_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobNameExistError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameExistError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_exist_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobNameEmptyError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameEmptyError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_empty_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobNameNotEqualError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_name_not_equal_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kNoJobBuildAndInferCtxError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.no_job_build_and_infer_ctx_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobConfFrozenError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfFrozenError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_frozen_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobConfNotSetError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfNotSetError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_not_set_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobConfRepeatedSetError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_conf_repeated_set_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobTypeNotSetError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.job_type_not_set_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLogicalBlobNameNotExistError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_not_exist_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLogicalBlobNameExistError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_exist_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLogicalBlobNameInvalidError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.logical_blob_name_invalid_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kOpNameExistError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpNameExistError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_name_exist_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kOpConfDeviceTagNoSetError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.op_conf_device_tag_no_set_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPlacementError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PlacementError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.placement_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kBlobSplitAxisInferError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.blob_split_axis_infer_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kUnknownJobBuildAndInferError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unknown_job_build_and_infer_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRwMutexedObjectNotFoundError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.rw_mutexed_object_not_found_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kSymbolIdUninitializedError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.symbol_id_uninitialized_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kUnknownError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::UnknownError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.unknown_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kCompileOptionWrongError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.compile_option_wrong_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kInputDeviceNotMatchError: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(error_type_.input_device_not_match_error_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case ERROR_TYPE_NOT_SET: {
      break;
    }
  }
  error_type_case_ = ERROR_TYPE_NOT_SET;
}
void ConstErrorProto::_ErrorProto_::error_type_copy_from(const _ErrorProto_& other) {
  switch (other.error_type_case()) {
    case kConfigAssertFailedError: {
      mutable_config_assert_failed_error()->CopyFrom(other.config_assert_failed_error());
      break;
    }
    case kConfigResourceUnavailableError: {
      mutable_config_resource_unavailable_error()->CopyFrom(other.config_resource_unavailable_error());
      break;
    }
    case kProtoParseFailedError: {
      mutable_proto_parse_failed_error()->CopyFrom(other.proto_parse_failed_error());
      break;
    }
    case kCheckFailedError: {
      mutable_check_failed_error()->CopyFrom(other.check_failed_error());
      break;
    }
    case kTodoError: {
      mutable_todo_error()->CopyFrom(other.todo_error());
      break;
    }
    case kUnimplementedError: {
      mutable_unimplemented_error()->CopyFrom(other.unimplemented_error());
      break;
    }
    case kBoxingNotSupportedError: {
      mutable_boxing_not_supported_error()->CopyFrom(other.boxing_not_supported_error());
      break;
    }
    case kGradientFunctionNotFoundError: {
      mutable_gradient_function_not_found_error()->CopyFrom(other.gradient_function_not_found_error());
      break;
    }
    case kOpKernelNotFoundError: {
      mutable_op_kernel_not_found_error()->CopyFrom(other.op_kernel_not_found_error());
      break;
    }
    case kMultipleOpKernelsMatchedError: {
      mutable_multiple_op_kernels_matched_error()->CopyFrom(other.multiple_op_kernels_matched_error());
      break;
    }
    case kMemoryZoneOutOfMemoryError: {
      mutable_memory_zone_out_of_memory_error()->CopyFrom(other.memory_zone_out_of_memory_error());
      break;
    }
    case kLossBlobNotFoundError: {
      mutable_loss_blob_not_found_error()->CopyFrom(other.loss_blob_not_found_error());
      break;
    }
    case kJobSetEmptyError: {
      mutable_job_set_empty_error()->CopyFrom(other.job_set_empty_error());
      break;
    }
    case kDeviceTagNotFoundError: {
      mutable_device_tag_not_found_error()->CopyFrom(other.device_tag_not_found_error());
      break;
    }
    case kValueError: {
      mutable_value_error()->CopyFrom(other.value_error());
      break;
    }
    case kIndexError: {
      mutable_index_error()->CopyFrom(other.index_error());
      break;
    }
    case kTimeoutError: {
      mutable_timeout_error()->CopyFrom(other.timeout_error());
      break;
    }
    case kJobNameExistError: {
      mutable_job_name_exist_error()->CopyFrom(other.job_name_exist_error());
      break;
    }
    case kJobNameEmptyError: {
      mutable_job_name_empty_error()->CopyFrom(other.job_name_empty_error());
      break;
    }
    case kJobNameNotEqualError: {
      mutable_job_name_not_equal_error()->CopyFrom(other.job_name_not_equal_error());
      break;
    }
    case kNoJobBuildAndInferCtxError: {
      mutable_no_job_build_and_infer_ctx_error()->CopyFrom(other.no_job_build_and_infer_ctx_error());
      break;
    }
    case kJobConfFrozenError: {
      mutable_job_conf_frozen_error()->CopyFrom(other.job_conf_frozen_error());
      break;
    }
    case kJobConfNotSetError: {
      mutable_job_conf_not_set_error()->CopyFrom(other.job_conf_not_set_error());
      break;
    }
    case kJobConfRepeatedSetError: {
      mutable_job_conf_repeated_set_error()->CopyFrom(other.job_conf_repeated_set_error());
      break;
    }
    case kJobTypeNotSetError: {
      mutable_job_type_not_set_error()->CopyFrom(other.job_type_not_set_error());
      break;
    }
    case kLogicalBlobNameNotExistError: {
      mutable_logical_blob_name_not_exist_error()->CopyFrom(other.logical_blob_name_not_exist_error());
      break;
    }
    case kLogicalBlobNameExistError: {
      mutable_logical_blob_name_exist_error()->CopyFrom(other.logical_blob_name_exist_error());
      break;
    }
    case kLogicalBlobNameInvalidError: {
      mutable_logical_blob_name_invalid_error()->CopyFrom(other.logical_blob_name_invalid_error());
      break;
    }
    case kOpNameExistError: {
      mutable_op_name_exist_error()->CopyFrom(other.op_name_exist_error());
      break;
    }
    case kOpConfDeviceTagNoSetError: {
      mutable_op_conf_device_tag_no_set_error()->CopyFrom(other.op_conf_device_tag_no_set_error());
      break;
    }
    case kPlacementError: {
      mutable_placement_error()->CopyFrom(other.placement_error());
      break;
    }
    case kBlobSplitAxisInferError: {
      mutable_blob_split_axis_infer_error()->CopyFrom(other.blob_split_axis_infer_error());
      break;
    }
    case kUnknownJobBuildAndInferError: {
      mutable_unknown_job_build_and_infer_error()->CopyFrom(other.unknown_job_build_and_infer_error());
      break;
    }
    case kRwMutexedObjectNotFoundError: {
      mutable_rw_mutexed_object_not_found_error()->CopyFrom(other.rw_mutexed_object_not_found_error());
      break;
    }
    case kSymbolIdUninitializedError: {
      mutable_symbol_id_uninitialized_error()->CopyFrom(other.symbol_id_uninitialized_error());
      break;
    }
    case kUnknownError: {
      mutable_unknown_error()->CopyFrom(other.unknown_error());
      break;
    }
    case kCompileOptionWrongError: {
      mutable_compile_option_wrong_error()->CopyFrom(other.compile_option_wrong_error());
      break;
    }
    case kInputDeviceNotMatchError: {
      mutable_input_device_not_match_error()->CopyFrom(other.input_device_not_match_error());
      break;
    }
    case ERROR_TYPE_NOT_SET: {
      clear_error_type();
    }
  }
}


int ConstErrorProto::_ErrorProto_::compare(const _ErrorProto_& other) {
  if (!(has_error_summary() == other.has_error_summary())) {
    return has_error_summary() < other.has_error_summary() ? -1 : 1;
  } else if (!(error_summary() == other.error_summary())) {
    return error_summary() < other.error_summary() ? -1 : 1;
  }
  if (!(has_msg() == other.has_msg())) {
    return has_msg() < other.has_msg() ? -1 : 1;
  } else if (!(msg() == other.msg())) {
    return msg() < other.msg() ? -1 : 1;
  }
  if (!(stack_frame() == other.stack_frame())) {
    return stack_frame() < other.stack_frame() ? -1 : 1;
  }
  if (!(error_type_case() == other.error_type_case())) {
    return error_type_case() < other.error_type_case() ? -1 : 1;
  }
  switch (error_type_case()) {
    case kConfigAssertFailedError: {
      if (!(config_assert_failed_error() == other.config_assert_failed_error())) {
        return config_assert_failed_error() < other.config_assert_failed_error() ? -1 : 1;
      }
      break;
    }
    case kConfigResourceUnavailableError: {
      if (!(config_resource_unavailable_error() == other.config_resource_unavailable_error())) {
        return config_resource_unavailable_error() < other.config_resource_unavailable_error() ? -1 : 1;
      }
      break;
    }
    case kProtoParseFailedError: {
      if (!(proto_parse_failed_error() == other.proto_parse_failed_error())) {
        return proto_parse_failed_error() < other.proto_parse_failed_error() ? -1 : 1;
      }
      break;
    }
    case kCheckFailedError: {
      if (!(check_failed_error() == other.check_failed_error())) {
        return check_failed_error() < other.check_failed_error() ? -1 : 1;
      }
      break;
    }
    case kTodoError: {
      if (!(todo_error() == other.todo_error())) {
        return todo_error() < other.todo_error() ? -1 : 1;
      }
      break;
    }
    case kUnimplementedError: {
      if (!(unimplemented_error() == other.unimplemented_error())) {
        return unimplemented_error() < other.unimplemented_error() ? -1 : 1;
      }
      break;
    }
    case kBoxingNotSupportedError: {
      if (!(boxing_not_supported_error() == other.boxing_not_supported_error())) {
        return boxing_not_supported_error() < other.boxing_not_supported_error() ? -1 : 1;
      }
      break;
    }
    case kGradientFunctionNotFoundError: {
      if (!(gradient_function_not_found_error() == other.gradient_function_not_found_error())) {
        return gradient_function_not_found_error() < other.gradient_function_not_found_error() ? -1 : 1;
      }
      break;
    }
    case kOpKernelNotFoundError: {
      if (!(op_kernel_not_found_error() == other.op_kernel_not_found_error())) {
        return op_kernel_not_found_error() < other.op_kernel_not_found_error() ? -1 : 1;
      }
      break;
    }
    case kMultipleOpKernelsMatchedError: {
      if (!(multiple_op_kernels_matched_error() == other.multiple_op_kernels_matched_error())) {
        return multiple_op_kernels_matched_error() < other.multiple_op_kernels_matched_error() ? -1 : 1;
      }
      break;
    }
    case kMemoryZoneOutOfMemoryError: {
      if (!(memory_zone_out_of_memory_error() == other.memory_zone_out_of_memory_error())) {
        return memory_zone_out_of_memory_error() < other.memory_zone_out_of_memory_error() ? -1 : 1;
      }
      break;
    }
    case kLossBlobNotFoundError: {
      if (!(loss_blob_not_found_error() == other.loss_blob_not_found_error())) {
        return loss_blob_not_found_error() < other.loss_blob_not_found_error() ? -1 : 1;
      }
      break;
    }
    case kJobSetEmptyError: {
      if (!(job_set_empty_error() == other.job_set_empty_error())) {
        return job_set_empty_error() < other.job_set_empty_error() ? -1 : 1;
      }
      break;
    }
    case kDeviceTagNotFoundError: {
      if (!(device_tag_not_found_error() == other.device_tag_not_found_error())) {
        return device_tag_not_found_error() < other.device_tag_not_found_error() ? -1 : 1;
      }
      break;
    }
    case kValueError: {
      if (!(value_error() == other.value_error())) {
        return value_error() < other.value_error() ? -1 : 1;
      }
      break;
    }
    case kIndexError: {
      if (!(index_error() == other.index_error())) {
        return index_error() < other.index_error() ? -1 : 1;
      }
      break;
    }
    case kTimeoutError: {
      if (!(timeout_error() == other.timeout_error())) {
        return timeout_error() < other.timeout_error() ? -1 : 1;
      }
      break;
    }
    case kJobNameExistError: {
      if (!(job_name_exist_error() == other.job_name_exist_error())) {
        return job_name_exist_error() < other.job_name_exist_error() ? -1 : 1;
      }
      break;
    }
    case kJobNameEmptyError: {
      if (!(job_name_empty_error() == other.job_name_empty_error())) {
        return job_name_empty_error() < other.job_name_empty_error() ? -1 : 1;
      }
      break;
    }
    case kJobNameNotEqualError: {
      if (!(job_name_not_equal_error() == other.job_name_not_equal_error())) {
        return job_name_not_equal_error() < other.job_name_not_equal_error() ? -1 : 1;
      }
      break;
    }
    case kNoJobBuildAndInferCtxError: {
      if (!(no_job_build_and_infer_ctx_error() == other.no_job_build_and_infer_ctx_error())) {
        return no_job_build_and_infer_ctx_error() < other.no_job_build_and_infer_ctx_error() ? -1 : 1;
      }
      break;
    }
    case kJobConfFrozenError: {
      if (!(job_conf_frozen_error() == other.job_conf_frozen_error())) {
        return job_conf_frozen_error() < other.job_conf_frozen_error() ? -1 : 1;
      }
      break;
    }
    case kJobConfNotSetError: {
      if (!(job_conf_not_set_error() == other.job_conf_not_set_error())) {
        return job_conf_not_set_error() < other.job_conf_not_set_error() ? -1 : 1;
      }
      break;
    }
    case kJobConfRepeatedSetError: {
      if (!(job_conf_repeated_set_error() == other.job_conf_repeated_set_error())) {
        return job_conf_repeated_set_error() < other.job_conf_repeated_set_error() ? -1 : 1;
      }
      break;
    }
    case kJobTypeNotSetError: {
      if (!(job_type_not_set_error() == other.job_type_not_set_error())) {
        return job_type_not_set_error() < other.job_type_not_set_error() ? -1 : 1;
      }
      break;
    }
    case kLogicalBlobNameNotExistError: {
      if (!(logical_blob_name_not_exist_error() == other.logical_blob_name_not_exist_error())) {
        return logical_blob_name_not_exist_error() < other.logical_blob_name_not_exist_error() ? -1 : 1;
      }
      break;
    }
    case kLogicalBlobNameExistError: {
      if (!(logical_blob_name_exist_error() == other.logical_blob_name_exist_error())) {
        return logical_blob_name_exist_error() < other.logical_blob_name_exist_error() ? -1 : 1;
      }
      break;
    }
    case kLogicalBlobNameInvalidError: {
      if (!(logical_blob_name_invalid_error() == other.logical_blob_name_invalid_error())) {
        return logical_blob_name_invalid_error() < other.logical_blob_name_invalid_error() ? -1 : 1;
      }
      break;
    }
    case kOpNameExistError: {
      if (!(op_name_exist_error() == other.op_name_exist_error())) {
        return op_name_exist_error() < other.op_name_exist_error() ? -1 : 1;
      }
      break;
    }
    case kOpConfDeviceTagNoSetError: {
      if (!(op_conf_device_tag_no_set_error() == other.op_conf_device_tag_no_set_error())) {
        return op_conf_device_tag_no_set_error() < other.op_conf_device_tag_no_set_error() ? -1 : 1;
      }
      break;
    }
    case kPlacementError: {
      if (!(placement_error() == other.placement_error())) {
        return placement_error() < other.placement_error() ? -1 : 1;
      }
      break;
    }
    case kBlobSplitAxisInferError: {
      if (!(blob_split_axis_infer_error() == other.blob_split_axis_infer_error())) {
        return blob_split_axis_infer_error() < other.blob_split_axis_infer_error() ? -1 : 1;
      }
      break;
    }
    case kUnknownJobBuildAndInferError: {
      if (!(unknown_job_build_and_infer_error() == other.unknown_job_build_and_infer_error())) {
        return unknown_job_build_and_infer_error() < other.unknown_job_build_and_infer_error() ? -1 : 1;
      }
      break;
    }
    case kRwMutexedObjectNotFoundError: {
      if (!(rw_mutexed_object_not_found_error() == other.rw_mutexed_object_not_found_error())) {
        return rw_mutexed_object_not_found_error() < other.rw_mutexed_object_not_found_error() ? -1 : 1;
      }
      break;
    }
    case kSymbolIdUninitializedError: {
      if (!(symbol_id_uninitialized_error() == other.symbol_id_uninitialized_error())) {
        return symbol_id_uninitialized_error() < other.symbol_id_uninitialized_error() ? -1 : 1;
      }
      break;
    }
    case kUnknownError: {
      if (!(unknown_error() == other.unknown_error())) {
        return unknown_error() < other.unknown_error() ? -1 : 1;
      }
      break;
    }
    case kCompileOptionWrongError: {
      if (!(compile_option_wrong_error() == other.compile_option_wrong_error())) {
        return compile_option_wrong_error() < other.compile_option_wrong_error() ? -1 : 1;
      }
      break;
    }
    case kInputDeviceNotMatchError: {
      if (!(input_device_not_match_error() == other.input_device_not_match_error())) {
        return input_device_not_match_error() < other.input_device_not_match_error() ? -1 : 1;
      }
      break;
    }
    case ERROR_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstErrorProto::_ErrorProto_::operator==(const _ErrorProto_& other) const {
  return true
    && has_error_summary() == other.has_error_summary() 
    && error_summary() == other.error_summary()
    && has_msg() == other.has_msg() 
    && msg() == other.msg()
    && stack_frame() == other.stack_frame()
    && error_type_case() == other.error_type_case()
    && (error_type_case() == kConfigAssertFailedError ? 
      config_assert_failed_error() == other.config_assert_failed_error() : true)
    && (error_type_case() == kConfigResourceUnavailableError ? 
      config_resource_unavailable_error() == other.config_resource_unavailable_error() : true)
    && (error_type_case() == kProtoParseFailedError ? 
      proto_parse_failed_error() == other.proto_parse_failed_error() : true)
    && (error_type_case() == kCheckFailedError ? 
      check_failed_error() == other.check_failed_error() : true)
    && (error_type_case() == kTodoError ? 
      todo_error() == other.todo_error() : true)
    && (error_type_case() == kUnimplementedError ? 
      unimplemented_error() == other.unimplemented_error() : true)
    && (error_type_case() == kBoxingNotSupportedError ? 
      boxing_not_supported_error() == other.boxing_not_supported_error() : true)
    && (error_type_case() == kGradientFunctionNotFoundError ? 
      gradient_function_not_found_error() == other.gradient_function_not_found_error() : true)
    && (error_type_case() == kOpKernelNotFoundError ? 
      op_kernel_not_found_error() == other.op_kernel_not_found_error() : true)
    && (error_type_case() == kMultipleOpKernelsMatchedError ? 
      multiple_op_kernels_matched_error() == other.multiple_op_kernels_matched_error() : true)
    && (error_type_case() == kMemoryZoneOutOfMemoryError ? 
      memory_zone_out_of_memory_error() == other.memory_zone_out_of_memory_error() : true)
    && (error_type_case() == kLossBlobNotFoundError ? 
      loss_blob_not_found_error() == other.loss_blob_not_found_error() : true)
    && (error_type_case() == kJobSetEmptyError ? 
      job_set_empty_error() == other.job_set_empty_error() : true)
    && (error_type_case() == kDeviceTagNotFoundError ? 
      device_tag_not_found_error() == other.device_tag_not_found_error() : true)
    && (error_type_case() == kValueError ? 
      value_error() == other.value_error() : true)
    && (error_type_case() == kIndexError ? 
      index_error() == other.index_error() : true)
    && (error_type_case() == kTimeoutError ? 
      timeout_error() == other.timeout_error() : true)
    && (error_type_case() == kJobNameExistError ? 
      job_name_exist_error() == other.job_name_exist_error() : true)
    && (error_type_case() == kJobNameEmptyError ? 
      job_name_empty_error() == other.job_name_empty_error() : true)
    && (error_type_case() == kJobNameNotEqualError ? 
      job_name_not_equal_error() == other.job_name_not_equal_error() : true)
    && (error_type_case() == kNoJobBuildAndInferCtxError ? 
      no_job_build_and_infer_ctx_error() == other.no_job_build_and_infer_ctx_error() : true)
    && (error_type_case() == kJobConfFrozenError ? 
      job_conf_frozen_error() == other.job_conf_frozen_error() : true)
    && (error_type_case() == kJobConfNotSetError ? 
      job_conf_not_set_error() == other.job_conf_not_set_error() : true)
    && (error_type_case() == kJobConfRepeatedSetError ? 
      job_conf_repeated_set_error() == other.job_conf_repeated_set_error() : true)
    && (error_type_case() == kJobTypeNotSetError ? 
      job_type_not_set_error() == other.job_type_not_set_error() : true)
    && (error_type_case() == kLogicalBlobNameNotExistError ? 
      logical_blob_name_not_exist_error() == other.logical_blob_name_not_exist_error() : true)
    && (error_type_case() == kLogicalBlobNameExistError ? 
      logical_blob_name_exist_error() == other.logical_blob_name_exist_error() : true)
    && (error_type_case() == kLogicalBlobNameInvalidError ? 
      logical_blob_name_invalid_error() == other.logical_blob_name_invalid_error() : true)
    && (error_type_case() == kOpNameExistError ? 
      op_name_exist_error() == other.op_name_exist_error() : true)
    && (error_type_case() == kOpConfDeviceTagNoSetError ? 
      op_conf_device_tag_no_set_error() == other.op_conf_device_tag_no_set_error() : true)
    && (error_type_case() == kPlacementError ? 
      placement_error() == other.placement_error() : true)
    && (error_type_case() == kBlobSplitAxisInferError ? 
      blob_split_axis_infer_error() == other.blob_split_axis_infer_error() : true)
    && (error_type_case() == kUnknownJobBuildAndInferError ? 
      unknown_job_build_and_infer_error() == other.unknown_job_build_and_infer_error() : true)
    && (error_type_case() == kRwMutexedObjectNotFoundError ? 
      rw_mutexed_object_not_found_error() == other.rw_mutexed_object_not_found_error() : true)
    && (error_type_case() == kSymbolIdUninitializedError ? 
      symbol_id_uninitialized_error() == other.symbol_id_uninitialized_error() : true)
    && (error_type_case() == kUnknownError ? 
      unknown_error() == other.unknown_error() : true)
    && (error_type_case() == kCompileOptionWrongError ? 
      compile_option_wrong_error() == other.compile_option_wrong_error() : true)
    && (error_type_case() == kInputDeviceNotMatchError ? 
      input_device_not_match_error() == other.input_device_not_match_error() : true)
  ;
}

std::size_t ConstErrorProto::_ErrorProto_::__CalcHash__() const {
  return 0
    ^ (has_error_summary() ? std::hash<::std::string>()(error_summary()) : 0)
    ^ (has_msg() ? std::hash<::std::string>()(msg()) : 0)
    ^ stack_frame().__CalcHash__()
    ^ static_cast<std::size_t>(error_type_case())
    ^ (has_config_assert_failed_error() ? std::hash<::oneflow::cfg::ConfigAssertFailedError>()(config_assert_failed_error()) : 0)
    ^ (has_config_resource_unavailable_error() ? std::hash<::oneflow::cfg::ConfigResourceUnavailableError>()(config_resource_unavailable_error()) : 0)
    ^ (has_proto_parse_failed_error() ? std::hash<::oneflow::cfg::ProtoParseFailedError>()(proto_parse_failed_error()) : 0)
    ^ (has_check_failed_error() ? std::hash<::oneflow::cfg::CheckFailedError>()(check_failed_error()) : 0)
    ^ (has_todo_error() ? std::hash<::oneflow::cfg::TodoError>()(todo_error()) : 0)
    ^ (has_unimplemented_error() ? std::hash<::oneflow::cfg::UnimplementedError>()(unimplemented_error()) : 0)
    ^ (has_boxing_not_supported_error() ? std::hash<::oneflow::cfg::BoxingNotSupportedError>()(boxing_not_supported_error()) : 0)
    ^ (has_gradient_function_not_found_error() ? std::hash<::oneflow::cfg::GradientFunctionNotFoundError>()(gradient_function_not_found_error()) : 0)
    ^ (has_op_kernel_not_found_error() ? std::hash<::oneflow::cfg::OpKernelNotFoundError>()(op_kernel_not_found_error()) : 0)
    ^ (has_multiple_op_kernels_matched_error() ? std::hash<::oneflow::cfg::MultipleOpKernelsMatchedError>()(multiple_op_kernels_matched_error()) : 0)
    ^ (has_memory_zone_out_of_memory_error() ? std::hash<::oneflow::cfg::MemoryZoneOutOfMemoryError>()(memory_zone_out_of_memory_error()) : 0)
    ^ (has_loss_blob_not_found_error() ? std::hash<::oneflow::cfg::LossBlobNotFoundError>()(loss_blob_not_found_error()) : 0)
    ^ (has_job_set_empty_error() ? std::hash<::oneflow::cfg::JobSetEmptyError>()(job_set_empty_error()) : 0)
    ^ (has_device_tag_not_found_error() ? std::hash<::oneflow::cfg::DeviceTagNotFoundError>()(device_tag_not_found_error()) : 0)
    ^ (has_value_error() ? std::hash<::oneflow::cfg::ValueError>()(value_error()) : 0)
    ^ (has_index_error() ? std::hash<::oneflow::cfg::IndexError>()(index_error()) : 0)
    ^ (has_timeout_error() ? std::hash<::oneflow::cfg::TimeoutError>()(timeout_error()) : 0)
    ^ (has_job_name_exist_error() ? std::hash<::oneflow::cfg::JobNameExistError>()(job_name_exist_error()) : 0)
    ^ (has_job_name_empty_error() ? std::hash<::oneflow::cfg::JobNameEmptyError>()(job_name_empty_error()) : 0)
    ^ (has_job_name_not_equal_error() ? std::hash<::oneflow::cfg::JobNameNotEqualError>()(job_name_not_equal_error()) : 0)
    ^ (has_no_job_build_and_infer_ctx_error() ? std::hash<::oneflow::cfg::NoJobBuildAndInferCtxError>()(no_job_build_and_infer_ctx_error()) : 0)
    ^ (has_job_conf_frozen_error() ? std::hash<::oneflow::cfg::JobConfFrozenError>()(job_conf_frozen_error()) : 0)
    ^ (has_job_conf_not_set_error() ? std::hash<::oneflow::cfg::JobConfNotSetError>()(job_conf_not_set_error()) : 0)
    ^ (has_job_conf_repeated_set_error() ? std::hash<::oneflow::cfg::JobConfRepeatedSetError>()(job_conf_repeated_set_error()) : 0)
    ^ (has_job_type_not_set_error() ? std::hash<::oneflow::cfg::JobTypeNotSetError>()(job_type_not_set_error()) : 0)
    ^ (has_logical_blob_name_not_exist_error() ? std::hash<::oneflow::cfg::LogicalBlobNameNotExistError>()(logical_blob_name_not_exist_error()) : 0)
    ^ (has_logical_blob_name_exist_error() ? std::hash<::oneflow::cfg::LogicalBlobNameExistError>()(logical_blob_name_exist_error()) : 0)
    ^ (has_logical_blob_name_invalid_error() ? std::hash<::oneflow::cfg::LogicalBlobNameInvalidError>()(logical_blob_name_invalid_error()) : 0)
    ^ (has_op_name_exist_error() ? std::hash<::oneflow::cfg::OpNameExistError>()(op_name_exist_error()) : 0)
    ^ (has_op_conf_device_tag_no_set_error() ? std::hash<::oneflow::cfg::OpConfDeviceTagNoSetError>()(op_conf_device_tag_no_set_error()) : 0)
    ^ (has_placement_error() ? std::hash<::oneflow::cfg::PlacementError>()(placement_error()) : 0)
    ^ (has_blob_split_axis_infer_error() ? std::hash<::oneflow::cfg::BlobSplitAxisInferError>()(blob_split_axis_infer_error()) : 0)
    ^ (has_unknown_job_build_and_infer_error() ? std::hash<::oneflow::cfg::UnknownJobBuildAndInferError>()(unknown_job_build_and_infer_error()) : 0)
    ^ (has_rw_mutexed_object_not_found_error() ? std::hash<::oneflow::cfg::RwMutexedObjectNotFoundError>()(rw_mutexed_object_not_found_error()) : 0)
    ^ (has_symbol_id_uninitialized_error() ? std::hash<::oneflow::cfg::SymbolIdUninitializedError>()(symbol_id_uninitialized_error()) : 0)
    ^ (has_unknown_error() ? std::hash<::oneflow::cfg::UnknownError>()(unknown_error()) : 0)
    ^ (has_compile_option_wrong_error() ? std::hash<::oneflow::cfg::CompileOptionWrongError>()(compile_option_wrong_error()) : 0)
    ^ (has_input_device_not_match_error() ? std::hash<::oneflow::cfg::InputDeviceNotMatchError>()(input_device_not_match_error()) : 0)
  ;
}

bool ConstErrorProto::_ErrorProto_::operator<(const _ErrorProto_& other) const {
  return false
    || !(has_error_summary() == other.has_error_summary()) ? 
      has_error_summary() < other.has_error_summary() : false
    || !(error_summary() == other.error_summary()) ? 
      error_summary() < other.error_summary() : false
    || !(has_msg() == other.has_msg()) ? 
      has_msg() < other.has_msg() : false
    || !(msg() == other.msg()) ? 
      msg() < other.msg() : false
    || !(stack_frame() == other.stack_frame()) ? 
      stack_frame() < other.stack_frame() : false
    || !(error_type_case() == other.error_type_case()) ? 
      error_type_case() < other.error_type_case() : false
    || ((error_type_case() == kConfigAssertFailedError) && 
      !(config_assert_failed_error() == other.config_assert_failed_error())) ? 
        config_assert_failed_error() < other.config_assert_failed_error() : false
    || ((error_type_case() == kConfigResourceUnavailableError) && 
      !(config_resource_unavailable_error() == other.config_resource_unavailable_error())) ? 
        config_resource_unavailable_error() < other.config_resource_unavailable_error() : false
    || ((error_type_case() == kProtoParseFailedError) && 
      !(proto_parse_failed_error() == other.proto_parse_failed_error())) ? 
        proto_parse_failed_error() < other.proto_parse_failed_error() : false
    || ((error_type_case() == kCheckFailedError) && 
      !(check_failed_error() == other.check_failed_error())) ? 
        check_failed_error() < other.check_failed_error() : false
    || ((error_type_case() == kTodoError) && 
      !(todo_error() == other.todo_error())) ? 
        todo_error() < other.todo_error() : false
    || ((error_type_case() == kUnimplementedError) && 
      !(unimplemented_error() == other.unimplemented_error())) ? 
        unimplemented_error() < other.unimplemented_error() : false
    || ((error_type_case() == kBoxingNotSupportedError) && 
      !(boxing_not_supported_error() == other.boxing_not_supported_error())) ? 
        boxing_not_supported_error() < other.boxing_not_supported_error() : false
    || ((error_type_case() == kGradientFunctionNotFoundError) && 
      !(gradient_function_not_found_error() == other.gradient_function_not_found_error())) ? 
        gradient_function_not_found_error() < other.gradient_function_not_found_error() : false
    || ((error_type_case() == kOpKernelNotFoundError) && 
      !(op_kernel_not_found_error() == other.op_kernel_not_found_error())) ? 
        op_kernel_not_found_error() < other.op_kernel_not_found_error() : false
    || ((error_type_case() == kMultipleOpKernelsMatchedError) && 
      !(multiple_op_kernels_matched_error() == other.multiple_op_kernels_matched_error())) ? 
        multiple_op_kernels_matched_error() < other.multiple_op_kernels_matched_error() : false
    || ((error_type_case() == kMemoryZoneOutOfMemoryError) && 
      !(memory_zone_out_of_memory_error() == other.memory_zone_out_of_memory_error())) ? 
        memory_zone_out_of_memory_error() < other.memory_zone_out_of_memory_error() : false
    || ((error_type_case() == kLossBlobNotFoundError) && 
      !(loss_blob_not_found_error() == other.loss_blob_not_found_error())) ? 
        loss_blob_not_found_error() < other.loss_blob_not_found_error() : false
    || ((error_type_case() == kJobSetEmptyError) && 
      !(job_set_empty_error() == other.job_set_empty_error())) ? 
        job_set_empty_error() < other.job_set_empty_error() : false
    || ((error_type_case() == kDeviceTagNotFoundError) && 
      !(device_tag_not_found_error() == other.device_tag_not_found_error())) ? 
        device_tag_not_found_error() < other.device_tag_not_found_error() : false
    || ((error_type_case() == kValueError) && 
      !(value_error() == other.value_error())) ? 
        value_error() < other.value_error() : false
    || ((error_type_case() == kIndexError) && 
      !(index_error() == other.index_error())) ? 
        index_error() < other.index_error() : false
    || ((error_type_case() == kTimeoutError) && 
      !(timeout_error() == other.timeout_error())) ? 
        timeout_error() < other.timeout_error() : false
    || ((error_type_case() == kJobNameExistError) && 
      !(job_name_exist_error() == other.job_name_exist_error())) ? 
        job_name_exist_error() < other.job_name_exist_error() : false
    || ((error_type_case() == kJobNameEmptyError) && 
      !(job_name_empty_error() == other.job_name_empty_error())) ? 
        job_name_empty_error() < other.job_name_empty_error() : false
    || ((error_type_case() == kJobNameNotEqualError) && 
      !(job_name_not_equal_error() == other.job_name_not_equal_error())) ? 
        job_name_not_equal_error() < other.job_name_not_equal_error() : false
    || ((error_type_case() == kNoJobBuildAndInferCtxError) && 
      !(no_job_build_and_infer_ctx_error() == other.no_job_build_and_infer_ctx_error())) ? 
        no_job_build_and_infer_ctx_error() < other.no_job_build_and_infer_ctx_error() : false
    || ((error_type_case() == kJobConfFrozenError) && 
      !(job_conf_frozen_error() == other.job_conf_frozen_error())) ? 
        job_conf_frozen_error() < other.job_conf_frozen_error() : false
    || ((error_type_case() == kJobConfNotSetError) && 
      !(job_conf_not_set_error() == other.job_conf_not_set_error())) ? 
        job_conf_not_set_error() < other.job_conf_not_set_error() : false
    || ((error_type_case() == kJobConfRepeatedSetError) && 
      !(job_conf_repeated_set_error() == other.job_conf_repeated_set_error())) ? 
        job_conf_repeated_set_error() < other.job_conf_repeated_set_error() : false
    || ((error_type_case() == kJobTypeNotSetError) && 
      !(job_type_not_set_error() == other.job_type_not_set_error())) ? 
        job_type_not_set_error() < other.job_type_not_set_error() : false
    || ((error_type_case() == kLogicalBlobNameNotExistError) && 
      !(logical_blob_name_not_exist_error() == other.logical_blob_name_not_exist_error())) ? 
        logical_blob_name_not_exist_error() < other.logical_blob_name_not_exist_error() : false
    || ((error_type_case() == kLogicalBlobNameExistError) && 
      !(logical_blob_name_exist_error() == other.logical_blob_name_exist_error())) ? 
        logical_blob_name_exist_error() < other.logical_blob_name_exist_error() : false
    || ((error_type_case() == kLogicalBlobNameInvalidError) && 
      !(logical_blob_name_invalid_error() == other.logical_blob_name_invalid_error())) ? 
        logical_blob_name_invalid_error() < other.logical_blob_name_invalid_error() : false
    || ((error_type_case() == kOpNameExistError) && 
      !(op_name_exist_error() == other.op_name_exist_error())) ? 
        op_name_exist_error() < other.op_name_exist_error() : false
    || ((error_type_case() == kOpConfDeviceTagNoSetError) && 
      !(op_conf_device_tag_no_set_error() == other.op_conf_device_tag_no_set_error())) ? 
        op_conf_device_tag_no_set_error() < other.op_conf_device_tag_no_set_error() : false
    || ((error_type_case() == kPlacementError) && 
      !(placement_error() == other.placement_error())) ? 
        placement_error() < other.placement_error() : false
    || ((error_type_case() == kBlobSplitAxisInferError) && 
      !(blob_split_axis_infer_error() == other.blob_split_axis_infer_error())) ? 
        blob_split_axis_infer_error() < other.blob_split_axis_infer_error() : false
    || ((error_type_case() == kUnknownJobBuildAndInferError) && 
      !(unknown_job_build_and_infer_error() == other.unknown_job_build_and_infer_error())) ? 
        unknown_job_build_and_infer_error() < other.unknown_job_build_and_infer_error() : false
    || ((error_type_case() == kRwMutexedObjectNotFoundError) && 
      !(rw_mutexed_object_not_found_error() == other.rw_mutexed_object_not_found_error())) ? 
        rw_mutexed_object_not_found_error() < other.rw_mutexed_object_not_found_error() : false
    || ((error_type_case() == kSymbolIdUninitializedError) && 
      !(symbol_id_uninitialized_error() == other.symbol_id_uninitialized_error())) ? 
        symbol_id_uninitialized_error() < other.symbol_id_uninitialized_error() : false
    || ((error_type_case() == kUnknownError) && 
      !(unknown_error() == other.unknown_error())) ? 
        unknown_error() < other.unknown_error() : false
    || ((error_type_case() == kCompileOptionWrongError) && 
      !(compile_option_wrong_error() == other.compile_option_wrong_error())) ? 
        compile_option_wrong_error() < other.compile_option_wrong_error() : false
    || ((error_type_case() == kInputDeviceNotMatchError) && 
      !(input_device_not_match_error() == other.input_device_not_match_error())) ? 
        input_device_not_match_error() < other.input_device_not_match_error() : false
  ;
}

using _ErrorProto_ =  ConstErrorProto::_ErrorProto_;
ConstErrorProto::ConstErrorProto(const ::std::shared_ptr<_ErrorProto_>& data): data_(data) {}
ConstErrorProto::ConstErrorProto(): data_(::std::make_shared<_ErrorProto_>()) {}
ConstErrorProto::ConstErrorProto(const ::oneflow::ErrorProto& proto_errorproto) {
  BuildFromProto(proto_errorproto);
}
ConstErrorProto::ConstErrorProto(const ConstErrorProto&) = default;
ConstErrorProto::ConstErrorProto(ConstErrorProto&&) noexcept = default;
ConstErrorProto::~ConstErrorProto() = default;

void ConstErrorProto::ToProto(PbMessage* proto_errorproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ErrorProto*>(proto_errorproto));
}
  
::std::string ConstErrorProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstErrorProto::__Empty__() const {
  return !data_;
}

int ConstErrorProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"error_summary", 1},
    {"msg", 2},
    {"stack_frame", 3},
    {"config_assert_failed_error", 12},
    {"config_resource_unavailable_error", 13},
    {"proto_parse_failed_error", 15},
    {"check_failed_error", 16},
    {"todo_error", 17},
    {"unimplemented_error", 18},
    {"boxing_not_supported_error", 19},
    {"gradient_function_not_found_error", 20},
    {"op_kernel_not_found_error", 21},
    {"multiple_op_kernels_matched_error", 22},
    {"memory_zone_out_of_memory_error", 23},
    {"loss_blob_not_found_error", 24},
    {"job_set_empty_error", 25},
    {"device_tag_not_found_error", 26},
    {"value_error", 27},
    {"index_error", 28},
    {"timeout_error", 29},
    {"job_name_exist_error", 100},
    {"job_name_empty_error", 101},
    {"job_name_not_equal_error", 102},
    {"no_job_build_and_infer_ctx_error", 200},
    {"job_conf_frozen_error", 300},
    {"job_conf_not_set_error", 301},
    {"job_conf_repeated_set_error", 302},
    {"job_type_not_set_error", 303},
    {"logical_blob_name_not_exist_error", 400},
    {"logical_blob_name_exist_error", 401},
    {"logical_blob_name_invalid_error", 402},
    {"op_name_exist_error", 450},
    {"op_conf_device_tag_no_set_error", 460},
    {"placement_error", 470},
    {"blob_split_axis_infer_error", 480},
    {"unknown_job_build_and_infer_error", 500},
    {"rw_mutexed_object_not_found_error", 600},
    {"symbol_id_uninitialized_error", 700},
    {"unknown_error", 900},
    {"compile_option_wrong_error", 950},
    {"input_device_not_match_error", 1000},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstErrorProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 12:
    case 13:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 100:
    case 101:
    case 102:
    case 200:
    case 300:
    case 301:
    case 302:
    case 303:
    case 400:
    case 401:
    case 402:
    case 450:
    case 460:
    case 470:
    case 480:
    case 500:
    case 600:
    case 700:
    case 900:
    case 950:
    case 1000:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstErrorProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ErrorStackFrame>)
      };
      return type_indices;
    }
    case 12: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ConfigAssertFailedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstConfigAssertFailedError),
      };
      return type_indices;
    }
    case 13: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ConfigResourceUnavailableError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstConfigResourceUnavailableError),
      };
      return type_indices;
    }
    case 15: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ProtoParseFailedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstProtoParseFailedError),
      };
      return type_indices;
    }
    case 16: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::CheckFailedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstCheckFailedError),
      };
      return type_indices;
    }
    case 17: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TodoError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTodoError),
      };
      return type_indices;
    }
    case 18: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::UnimplementedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstUnimplementedError),
      };
      return type_indices;
    }
    case 19: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BoxingNotSupportedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBoxingNotSupportedError),
      };
      return type_indices;
    }
    case 20: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::GradientFunctionNotFoundError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstGradientFunctionNotFoundError),
      };
      return type_indices;
    }
    case 21: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpKernelNotFoundError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpKernelNotFoundError),
      };
      return type_indices;
    }
    case 22: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MultipleOpKernelsMatchedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMultipleOpKernelsMatchedError),
      };
      return type_indices;
    }
    case 23: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MemoryZoneOutOfMemoryError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMemoryZoneOutOfMemoryError),
      };
      return type_indices;
    }
    case 24: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LossBlobNotFoundError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLossBlobNotFoundError),
      };
      return type_indices;
    }
    case 25: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobSetEmptyError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobSetEmptyError),
      };
      return type_indices;
    }
    case 26: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DeviceTagNotFoundError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstDeviceTagNotFoundError),
      };
      return type_indices;
    }
    case 27: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ValueError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstValueError),
      };
      return type_indices;
    }
    case 28: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::IndexError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstIndexError),
      };
      return type_indices;
    }
    case 29: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TimeoutError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTimeoutError),
      };
      return type_indices;
    }
    case 100: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobNameExistError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobNameExistError),
      };
      return type_indices;
    }
    case 101: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobNameEmptyError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobNameEmptyError),
      };
      return type_indices;
    }
    case 102: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobNameNotEqualError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobNameNotEqualError),
      };
      return type_indices;
    }
    case 200: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::NoJobBuildAndInferCtxError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstNoJobBuildAndInferCtxError),
      };
      return type_indices;
    }
    case 300: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobConfFrozenError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobConfFrozenError),
      };
      return type_indices;
    }
    case 301: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobConfNotSetError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobConfNotSetError),
      };
      return type_indices;
    }
    case 302: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobConfRepeatedSetError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobConfRepeatedSetError),
      };
      return type_indices;
    }
    case 303: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobTypeNotSetError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobTypeNotSetError),
      };
      return type_indices;
    }
    case 400: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobNameNotExistError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobNameNotExistError),
      };
      return type_indices;
    }
    case 401: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobNameExistError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobNameExistError),
      };
      return type_indices;
    }
    case 402: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobNameInvalidError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobNameInvalidError),
      };
      return type_indices;
    }
    case 450: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpNameExistError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpNameExistError),
      };
      return type_indices;
    }
    case 460: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpConfDeviceTagNoSetError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpConfDeviceTagNoSetError),
      };
      return type_indices;
    }
    case 470: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PlacementError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPlacementError),
      };
      return type_indices;
    }
    case 480: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobSplitAxisInferError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobSplitAxisInferError),
      };
      return type_indices;
    }
    case 500: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::UnknownJobBuildAndInferError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstUnknownJobBuildAndInferError),
      };
      return type_indices;
    }
    case 600: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RwMutexedObjectNotFoundError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRwMutexedObjectNotFoundError),
      };
      return type_indices;
    }
    case 700: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::SymbolIdUninitializedError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstSymbolIdUninitializedError),
      };
      return type_indices;
    }
    case 900: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::UnknownError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstUnknownError),
      };
      return type_indices;
    }
    case 950: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::CompileOptionWrongError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstCompileOptionWrongError),
      };
      return type_indices;
    }
    case 1000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::InputDeviceNotMatchError),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInputDeviceNotMatchError),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstErrorProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &error_summary();
    case 2: return &msg();
    case 3: return &stack_frame();
    case 12: return &config_assert_failed_error();
    case 13: return &config_resource_unavailable_error();
    case 15: return &proto_parse_failed_error();
    case 16: return &check_failed_error();
    case 17: return &todo_error();
    case 18: return &unimplemented_error();
    case 19: return &boxing_not_supported_error();
    case 20: return &gradient_function_not_found_error();
    case 21: return &op_kernel_not_found_error();
    case 22: return &multiple_op_kernels_matched_error();
    case 23: return &memory_zone_out_of_memory_error();
    case 24: return &loss_blob_not_found_error();
    case 25: return &job_set_empty_error();
    case 26: return &device_tag_not_found_error();
    case 27: return &value_error();
    case 28: return &index_error();
    case 29: return &timeout_error();
    case 100: return &job_name_exist_error();
    case 101: return &job_name_empty_error();
    case 102: return &job_name_not_equal_error();
    case 200: return &no_job_build_and_infer_ctx_error();
    case 300: return &job_conf_frozen_error();
    case 301: return &job_conf_not_set_error();
    case 302: return &job_conf_repeated_set_error();
    case 303: return &job_type_not_set_error();
    case 400: return &logical_blob_name_not_exist_error();
    case 401: return &logical_blob_name_exist_error();
    case 402: return &logical_blob_name_invalid_error();
    case 450: return &op_name_exist_error();
    case 460: return &op_conf_device_tag_no_set_error();
    case 470: return &placement_error();
    case 480: return &blob_split_axis_infer_error();
    case 500: return &unknown_job_build_and_infer_error();
    case 600: return &rw_mutexed_object_not_found_error();
    case 700: return &symbol_id_uninitialized_error();
    case 900: return &unknown_error();
    case 950: return &compile_option_wrong_error();
    case 1000: return &input_device_not_match_error();
    default: return nullptr;
  }
}

// required or optional field error_summary
bool ConstErrorProto::has_error_summary() const {
  return __SharedPtrOrDefault__()->has_error_summary();
}
const ::std::string& ConstErrorProto::error_summary() const {
  return __SharedPtrOrDefault__()->error_summary();
}
// used by pybind11 only
// required or optional field msg
bool ConstErrorProto::has_msg() const {
  return __SharedPtrOrDefault__()->has_msg();
}
const ::std::string& ConstErrorProto::msg() const {
  return __SharedPtrOrDefault__()->msg();
}
// used by pybind11 only
// repeated field stack_frame
::std::size_t ConstErrorProto::stack_frame_size() const {
  return __SharedPtrOrDefault__()->stack_frame_size();
}
const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& ConstErrorProto::stack_frame() const {
  return __SharedPtrOrDefault__()->stack_frame();
}
const ::oneflow::cfg::ErrorStackFrame& ConstErrorProto::stack_frame(::std::size_t index) const {
  return __SharedPtrOrDefault__()->stack_frame(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> ConstErrorProto::shared_const_stack_frame() const {
  return stack_frame().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstErrorStackFrame> ConstErrorProto::shared_const_stack_frame(::std::size_t index) const {
  return stack_frame(index).__SharedConst__();
}
 // oneof field error_type: config_assert_failed_error
bool ConstErrorProto::has_config_assert_failed_error() const {
  return __SharedPtrOrDefault__()->has_config_assert_failed_error();
}
const ::oneflow::cfg::ConfigAssertFailedError& ConstErrorProto::config_assert_failed_error() const {
  return __SharedPtrOrDefault__()->config_assert_failed_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstConfigAssertFailedError> ConstErrorProto::shared_const_config_assert_failed_error() const {
  return config_assert_failed_error().__SharedConst__();
}
 // oneof field error_type: config_resource_unavailable_error
bool ConstErrorProto::has_config_resource_unavailable_error() const {
  return __SharedPtrOrDefault__()->has_config_resource_unavailable_error();
}
const ::oneflow::cfg::ConfigResourceUnavailableError& ConstErrorProto::config_resource_unavailable_error() const {
  return __SharedPtrOrDefault__()->config_resource_unavailable_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstConfigResourceUnavailableError> ConstErrorProto::shared_const_config_resource_unavailable_error() const {
  return config_resource_unavailable_error().__SharedConst__();
}
 // oneof field error_type: proto_parse_failed_error
bool ConstErrorProto::has_proto_parse_failed_error() const {
  return __SharedPtrOrDefault__()->has_proto_parse_failed_error();
}
const ::oneflow::cfg::ProtoParseFailedError& ConstErrorProto::proto_parse_failed_error() const {
  return __SharedPtrOrDefault__()->proto_parse_failed_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstProtoParseFailedError> ConstErrorProto::shared_const_proto_parse_failed_error() const {
  return proto_parse_failed_error().__SharedConst__();
}
 // oneof field error_type: check_failed_error
bool ConstErrorProto::has_check_failed_error() const {
  return __SharedPtrOrDefault__()->has_check_failed_error();
}
const ::oneflow::cfg::CheckFailedError& ConstErrorProto::check_failed_error() const {
  return __SharedPtrOrDefault__()->check_failed_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstCheckFailedError> ConstErrorProto::shared_const_check_failed_error() const {
  return check_failed_error().__SharedConst__();
}
 // oneof field error_type: todo_error
bool ConstErrorProto::has_todo_error() const {
  return __SharedPtrOrDefault__()->has_todo_error();
}
const ::oneflow::cfg::TodoError& ConstErrorProto::todo_error() const {
  return __SharedPtrOrDefault__()->todo_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTodoError> ConstErrorProto::shared_const_todo_error() const {
  return todo_error().__SharedConst__();
}
 // oneof field error_type: unimplemented_error
bool ConstErrorProto::has_unimplemented_error() const {
  return __SharedPtrOrDefault__()->has_unimplemented_error();
}
const ::oneflow::cfg::UnimplementedError& ConstErrorProto::unimplemented_error() const {
  return __SharedPtrOrDefault__()->unimplemented_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstUnimplementedError> ConstErrorProto::shared_const_unimplemented_error() const {
  return unimplemented_error().__SharedConst__();
}
 // oneof field error_type: boxing_not_supported_error
bool ConstErrorProto::has_boxing_not_supported_error() const {
  return __SharedPtrOrDefault__()->has_boxing_not_supported_error();
}
const ::oneflow::cfg::BoxingNotSupportedError& ConstErrorProto::boxing_not_supported_error() const {
  return __SharedPtrOrDefault__()->boxing_not_supported_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBoxingNotSupportedError> ConstErrorProto::shared_const_boxing_not_supported_error() const {
  return boxing_not_supported_error().__SharedConst__();
}
 // oneof field error_type: gradient_function_not_found_error
bool ConstErrorProto::has_gradient_function_not_found_error() const {
  return __SharedPtrOrDefault__()->has_gradient_function_not_found_error();
}
const ::oneflow::cfg::GradientFunctionNotFoundError& ConstErrorProto::gradient_function_not_found_error() const {
  return __SharedPtrOrDefault__()->gradient_function_not_found_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstGradientFunctionNotFoundError> ConstErrorProto::shared_const_gradient_function_not_found_error() const {
  return gradient_function_not_found_error().__SharedConst__();
}
 // oneof field error_type: op_kernel_not_found_error
bool ConstErrorProto::has_op_kernel_not_found_error() const {
  return __SharedPtrOrDefault__()->has_op_kernel_not_found_error();
}
const ::oneflow::cfg::OpKernelNotFoundError& ConstErrorProto::op_kernel_not_found_error() const {
  return __SharedPtrOrDefault__()->op_kernel_not_found_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpKernelNotFoundError> ConstErrorProto::shared_const_op_kernel_not_found_error() const {
  return op_kernel_not_found_error().__SharedConst__();
}
 // oneof field error_type: multiple_op_kernels_matched_error
bool ConstErrorProto::has_multiple_op_kernels_matched_error() const {
  return __SharedPtrOrDefault__()->has_multiple_op_kernels_matched_error();
}
const ::oneflow::cfg::MultipleOpKernelsMatchedError& ConstErrorProto::multiple_op_kernels_matched_error() const {
  return __SharedPtrOrDefault__()->multiple_op_kernels_matched_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMultipleOpKernelsMatchedError> ConstErrorProto::shared_const_multiple_op_kernels_matched_error() const {
  return multiple_op_kernels_matched_error().__SharedConst__();
}
 // oneof field error_type: memory_zone_out_of_memory_error
bool ConstErrorProto::has_memory_zone_out_of_memory_error() const {
  return __SharedPtrOrDefault__()->has_memory_zone_out_of_memory_error();
}
const ::oneflow::cfg::MemoryZoneOutOfMemoryError& ConstErrorProto::memory_zone_out_of_memory_error() const {
  return __SharedPtrOrDefault__()->memory_zone_out_of_memory_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMemoryZoneOutOfMemoryError> ConstErrorProto::shared_const_memory_zone_out_of_memory_error() const {
  return memory_zone_out_of_memory_error().__SharedConst__();
}
 // oneof field error_type: loss_blob_not_found_error
bool ConstErrorProto::has_loss_blob_not_found_error() const {
  return __SharedPtrOrDefault__()->has_loss_blob_not_found_error();
}
const ::oneflow::cfg::LossBlobNotFoundError& ConstErrorProto::loss_blob_not_found_error() const {
  return __SharedPtrOrDefault__()->loss_blob_not_found_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLossBlobNotFoundError> ConstErrorProto::shared_const_loss_blob_not_found_error() const {
  return loss_blob_not_found_error().__SharedConst__();
}
 // oneof field error_type: job_set_empty_error
bool ConstErrorProto::has_job_set_empty_error() const {
  return __SharedPtrOrDefault__()->has_job_set_empty_error();
}
const ::oneflow::cfg::JobSetEmptyError& ConstErrorProto::job_set_empty_error() const {
  return __SharedPtrOrDefault__()->job_set_empty_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobSetEmptyError> ConstErrorProto::shared_const_job_set_empty_error() const {
  return job_set_empty_error().__SharedConst__();
}
 // oneof field error_type: device_tag_not_found_error
bool ConstErrorProto::has_device_tag_not_found_error() const {
  return __SharedPtrOrDefault__()->has_device_tag_not_found_error();
}
const ::oneflow::cfg::DeviceTagNotFoundError& ConstErrorProto::device_tag_not_found_error() const {
  return __SharedPtrOrDefault__()->device_tag_not_found_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstDeviceTagNotFoundError> ConstErrorProto::shared_const_device_tag_not_found_error() const {
  return device_tag_not_found_error().__SharedConst__();
}
 // oneof field error_type: value_error
bool ConstErrorProto::has_value_error() const {
  return __SharedPtrOrDefault__()->has_value_error();
}
const ::oneflow::cfg::ValueError& ConstErrorProto::value_error() const {
  return __SharedPtrOrDefault__()->value_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstValueError> ConstErrorProto::shared_const_value_error() const {
  return value_error().__SharedConst__();
}
 // oneof field error_type: index_error
bool ConstErrorProto::has_index_error() const {
  return __SharedPtrOrDefault__()->has_index_error();
}
const ::oneflow::cfg::IndexError& ConstErrorProto::index_error() const {
  return __SharedPtrOrDefault__()->index_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstIndexError> ConstErrorProto::shared_const_index_error() const {
  return index_error().__SharedConst__();
}
 // oneof field error_type: timeout_error
bool ConstErrorProto::has_timeout_error() const {
  return __SharedPtrOrDefault__()->has_timeout_error();
}
const ::oneflow::cfg::TimeoutError& ConstErrorProto::timeout_error() const {
  return __SharedPtrOrDefault__()->timeout_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTimeoutError> ConstErrorProto::shared_const_timeout_error() const {
  return timeout_error().__SharedConst__();
}
 // oneof field error_type: job_name_exist_error
bool ConstErrorProto::has_job_name_exist_error() const {
  return __SharedPtrOrDefault__()->has_job_name_exist_error();
}
const ::oneflow::cfg::JobNameExistError& ConstErrorProto::job_name_exist_error() const {
  return __SharedPtrOrDefault__()->job_name_exist_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobNameExistError> ConstErrorProto::shared_const_job_name_exist_error() const {
  return job_name_exist_error().__SharedConst__();
}
 // oneof field error_type: job_name_empty_error
bool ConstErrorProto::has_job_name_empty_error() const {
  return __SharedPtrOrDefault__()->has_job_name_empty_error();
}
const ::oneflow::cfg::JobNameEmptyError& ConstErrorProto::job_name_empty_error() const {
  return __SharedPtrOrDefault__()->job_name_empty_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobNameEmptyError> ConstErrorProto::shared_const_job_name_empty_error() const {
  return job_name_empty_error().__SharedConst__();
}
 // oneof field error_type: job_name_not_equal_error
bool ConstErrorProto::has_job_name_not_equal_error() const {
  return __SharedPtrOrDefault__()->has_job_name_not_equal_error();
}
const ::oneflow::cfg::JobNameNotEqualError& ConstErrorProto::job_name_not_equal_error() const {
  return __SharedPtrOrDefault__()->job_name_not_equal_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobNameNotEqualError> ConstErrorProto::shared_const_job_name_not_equal_error() const {
  return job_name_not_equal_error().__SharedConst__();
}
 // oneof field error_type: no_job_build_and_infer_ctx_error
bool ConstErrorProto::has_no_job_build_and_infer_ctx_error() const {
  return __SharedPtrOrDefault__()->has_no_job_build_and_infer_ctx_error();
}
const ::oneflow::cfg::NoJobBuildAndInferCtxError& ConstErrorProto::no_job_build_and_infer_ctx_error() const {
  return __SharedPtrOrDefault__()->no_job_build_and_infer_ctx_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstNoJobBuildAndInferCtxError> ConstErrorProto::shared_const_no_job_build_and_infer_ctx_error() const {
  return no_job_build_and_infer_ctx_error().__SharedConst__();
}
 // oneof field error_type: job_conf_frozen_error
bool ConstErrorProto::has_job_conf_frozen_error() const {
  return __SharedPtrOrDefault__()->has_job_conf_frozen_error();
}
const ::oneflow::cfg::JobConfFrozenError& ConstErrorProto::job_conf_frozen_error() const {
  return __SharedPtrOrDefault__()->job_conf_frozen_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobConfFrozenError> ConstErrorProto::shared_const_job_conf_frozen_error() const {
  return job_conf_frozen_error().__SharedConst__();
}
 // oneof field error_type: job_conf_not_set_error
bool ConstErrorProto::has_job_conf_not_set_error() const {
  return __SharedPtrOrDefault__()->has_job_conf_not_set_error();
}
const ::oneflow::cfg::JobConfNotSetError& ConstErrorProto::job_conf_not_set_error() const {
  return __SharedPtrOrDefault__()->job_conf_not_set_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobConfNotSetError> ConstErrorProto::shared_const_job_conf_not_set_error() const {
  return job_conf_not_set_error().__SharedConst__();
}
 // oneof field error_type: job_conf_repeated_set_error
bool ConstErrorProto::has_job_conf_repeated_set_error() const {
  return __SharedPtrOrDefault__()->has_job_conf_repeated_set_error();
}
const ::oneflow::cfg::JobConfRepeatedSetError& ConstErrorProto::job_conf_repeated_set_error() const {
  return __SharedPtrOrDefault__()->job_conf_repeated_set_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobConfRepeatedSetError> ConstErrorProto::shared_const_job_conf_repeated_set_error() const {
  return job_conf_repeated_set_error().__SharedConst__();
}
 // oneof field error_type: job_type_not_set_error
bool ConstErrorProto::has_job_type_not_set_error() const {
  return __SharedPtrOrDefault__()->has_job_type_not_set_error();
}
const ::oneflow::cfg::JobTypeNotSetError& ConstErrorProto::job_type_not_set_error() const {
  return __SharedPtrOrDefault__()->job_type_not_set_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobTypeNotSetError> ConstErrorProto::shared_const_job_type_not_set_error() const {
  return job_type_not_set_error().__SharedConst__();
}
 // oneof field error_type: logical_blob_name_not_exist_error
bool ConstErrorProto::has_logical_blob_name_not_exist_error() const {
  return __SharedPtrOrDefault__()->has_logical_blob_name_not_exist_error();
}
const ::oneflow::cfg::LogicalBlobNameNotExistError& ConstErrorProto::logical_blob_name_not_exist_error() const {
  return __SharedPtrOrDefault__()->logical_blob_name_not_exist_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameNotExistError> ConstErrorProto::shared_const_logical_blob_name_not_exist_error() const {
  return logical_blob_name_not_exist_error().__SharedConst__();
}
 // oneof field error_type: logical_blob_name_exist_error
bool ConstErrorProto::has_logical_blob_name_exist_error() const {
  return __SharedPtrOrDefault__()->has_logical_blob_name_exist_error();
}
const ::oneflow::cfg::LogicalBlobNameExistError& ConstErrorProto::logical_blob_name_exist_error() const {
  return __SharedPtrOrDefault__()->logical_blob_name_exist_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameExistError> ConstErrorProto::shared_const_logical_blob_name_exist_error() const {
  return logical_blob_name_exist_error().__SharedConst__();
}
 // oneof field error_type: logical_blob_name_invalid_error
bool ConstErrorProto::has_logical_blob_name_invalid_error() const {
  return __SharedPtrOrDefault__()->has_logical_blob_name_invalid_error();
}
const ::oneflow::cfg::LogicalBlobNameInvalidError& ConstErrorProto::logical_blob_name_invalid_error() const {
  return __SharedPtrOrDefault__()->logical_blob_name_invalid_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobNameInvalidError> ConstErrorProto::shared_const_logical_blob_name_invalid_error() const {
  return logical_blob_name_invalid_error().__SharedConst__();
}
 // oneof field error_type: op_name_exist_error
bool ConstErrorProto::has_op_name_exist_error() const {
  return __SharedPtrOrDefault__()->has_op_name_exist_error();
}
const ::oneflow::cfg::OpNameExistError& ConstErrorProto::op_name_exist_error() const {
  return __SharedPtrOrDefault__()->op_name_exist_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpNameExistError> ConstErrorProto::shared_const_op_name_exist_error() const {
  return op_name_exist_error().__SharedConst__();
}
 // oneof field error_type: op_conf_device_tag_no_set_error
bool ConstErrorProto::has_op_conf_device_tag_no_set_error() const {
  return __SharedPtrOrDefault__()->has_op_conf_device_tag_no_set_error();
}
const ::oneflow::cfg::OpConfDeviceTagNoSetError& ConstErrorProto::op_conf_device_tag_no_set_error() const {
  return __SharedPtrOrDefault__()->op_conf_device_tag_no_set_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpConfDeviceTagNoSetError> ConstErrorProto::shared_const_op_conf_device_tag_no_set_error() const {
  return op_conf_device_tag_no_set_error().__SharedConst__();
}
 // oneof field error_type: placement_error
bool ConstErrorProto::has_placement_error() const {
  return __SharedPtrOrDefault__()->has_placement_error();
}
const ::oneflow::cfg::PlacementError& ConstErrorProto::placement_error() const {
  return __SharedPtrOrDefault__()->placement_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPlacementError> ConstErrorProto::shared_const_placement_error() const {
  return placement_error().__SharedConst__();
}
 // oneof field error_type: blob_split_axis_infer_error
bool ConstErrorProto::has_blob_split_axis_infer_error() const {
  return __SharedPtrOrDefault__()->has_blob_split_axis_infer_error();
}
const ::oneflow::cfg::BlobSplitAxisInferError& ConstErrorProto::blob_split_axis_infer_error() const {
  return __SharedPtrOrDefault__()->blob_split_axis_infer_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobSplitAxisInferError> ConstErrorProto::shared_const_blob_split_axis_infer_error() const {
  return blob_split_axis_infer_error().__SharedConst__();
}
 // oneof field error_type: unknown_job_build_and_infer_error
bool ConstErrorProto::has_unknown_job_build_and_infer_error() const {
  return __SharedPtrOrDefault__()->has_unknown_job_build_and_infer_error();
}
const ::oneflow::cfg::UnknownJobBuildAndInferError& ConstErrorProto::unknown_job_build_and_infer_error() const {
  return __SharedPtrOrDefault__()->unknown_job_build_and_infer_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstUnknownJobBuildAndInferError> ConstErrorProto::shared_const_unknown_job_build_and_infer_error() const {
  return unknown_job_build_and_infer_error().__SharedConst__();
}
 // oneof field error_type: rw_mutexed_object_not_found_error
bool ConstErrorProto::has_rw_mutexed_object_not_found_error() const {
  return __SharedPtrOrDefault__()->has_rw_mutexed_object_not_found_error();
}
const ::oneflow::cfg::RwMutexedObjectNotFoundError& ConstErrorProto::rw_mutexed_object_not_found_error() const {
  return __SharedPtrOrDefault__()->rw_mutexed_object_not_found_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRwMutexedObjectNotFoundError> ConstErrorProto::shared_const_rw_mutexed_object_not_found_error() const {
  return rw_mutexed_object_not_found_error().__SharedConst__();
}
 // oneof field error_type: symbol_id_uninitialized_error
bool ConstErrorProto::has_symbol_id_uninitialized_error() const {
  return __SharedPtrOrDefault__()->has_symbol_id_uninitialized_error();
}
const ::oneflow::cfg::SymbolIdUninitializedError& ConstErrorProto::symbol_id_uninitialized_error() const {
  return __SharedPtrOrDefault__()->symbol_id_uninitialized_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstSymbolIdUninitializedError> ConstErrorProto::shared_const_symbol_id_uninitialized_error() const {
  return symbol_id_uninitialized_error().__SharedConst__();
}
 // oneof field error_type: unknown_error
bool ConstErrorProto::has_unknown_error() const {
  return __SharedPtrOrDefault__()->has_unknown_error();
}
const ::oneflow::cfg::UnknownError& ConstErrorProto::unknown_error() const {
  return __SharedPtrOrDefault__()->unknown_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstUnknownError> ConstErrorProto::shared_const_unknown_error() const {
  return unknown_error().__SharedConst__();
}
 // oneof field error_type: compile_option_wrong_error
bool ConstErrorProto::has_compile_option_wrong_error() const {
  return __SharedPtrOrDefault__()->has_compile_option_wrong_error();
}
const ::oneflow::cfg::CompileOptionWrongError& ConstErrorProto::compile_option_wrong_error() const {
  return __SharedPtrOrDefault__()->compile_option_wrong_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstCompileOptionWrongError> ConstErrorProto::shared_const_compile_option_wrong_error() const {
  return compile_option_wrong_error().__SharedConst__();
}
 // oneof field error_type: input_device_not_match_error
bool ConstErrorProto::has_input_device_not_match_error() const {
  return __SharedPtrOrDefault__()->has_input_device_not_match_error();
}
const ::oneflow::cfg::InputDeviceNotMatchError& ConstErrorProto::input_device_not_match_error() const {
  return __SharedPtrOrDefault__()->input_device_not_match_error();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInputDeviceNotMatchError> ConstErrorProto::shared_const_input_device_not_match_error() const {
  return input_device_not_match_error().__SharedConst__();
}
ConstErrorProto::ErrorTypeCase ConstErrorProto::error_type_case() const {
  return __SharedPtrOrDefault__()->error_type_case();
}

bool ConstErrorProto::has_error_type() const {
  return __SharedPtrOrDefault__()->has_error_type();
}

::std::shared_ptr<ConstErrorProto> ConstErrorProto::__SharedConst__() const {
  return ::std::make_shared<ConstErrorProto>(data_);
}
int64_t ConstErrorProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstErrorProto::operator==(const ConstErrorProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstErrorProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstErrorProto::operator<(const ConstErrorProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ErrorProto_>& ConstErrorProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ErrorProto_> default_ptr = std::make_shared<_ErrorProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_ErrorProto_>& ConstErrorProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _ErrorProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstErrorProto
void ConstErrorProto::BuildFromProto(const PbMessage& proto_errorproto) {
  data_ = ::std::make_shared<_ErrorProto_>(dynamic_cast<const ::oneflow::ErrorProto&>(proto_errorproto));
}

ErrorProto::ErrorProto(const ::std::shared_ptr<_ErrorProto_>& data)
  : ConstErrorProto(data) {}
ErrorProto::ErrorProto(const ErrorProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ErrorProto> resize
ErrorProto::ErrorProto(ErrorProto&&) noexcept = default; 
ErrorProto::ErrorProto(const ::oneflow::ErrorProto& proto_errorproto) {
  InitFromProto(proto_errorproto);
}
ErrorProto::ErrorProto() = default;

ErrorProto::~ErrorProto() = default;

void ErrorProto::InitFromProto(const PbMessage& proto_errorproto) {
  BuildFromProto(proto_errorproto);
}
  
void* ErrorProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_error_summary();
    case 2: return mutable_msg();
    case 3: return mutable_stack_frame();
    case 12: return mutable_config_assert_failed_error();
    case 13: return mutable_config_resource_unavailable_error();
    case 15: return mutable_proto_parse_failed_error();
    case 16: return mutable_check_failed_error();
    case 17: return mutable_todo_error();
    case 18: return mutable_unimplemented_error();
    case 19: return mutable_boxing_not_supported_error();
    case 20: return mutable_gradient_function_not_found_error();
    case 21: return mutable_op_kernel_not_found_error();
    case 22: return mutable_multiple_op_kernels_matched_error();
    case 23: return mutable_memory_zone_out_of_memory_error();
    case 24: return mutable_loss_blob_not_found_error();
    case 25: return mutable_job_set_empty_error();
    case 26: return mutable_device_tag_not_found_error();
    case 27: return mutable_value_error();
    case 28: return mutable_index_error();
    case 29: return mutable_timeout_error();
    case 100: return mutable_job_name_exist_error();
    case 101: return mutable_job_name_empty_error();
    case 102: return mutable_job_name_not_equal_error();
    case 200: return mutable_no_job_build_and_infer_ctx_error();
    case 300: return mutable_job_conf_frozen_error();
    case 301: return mutable_job_conf_not_set_error();
    case 302: return mutable_job_conf_repeated_set_error();
    case 303: return mutable_job_type_not_set_error();
    case 400: return mutable_logical_blob_name_not_exist_error();
    case 401: return mutable_logical_blob_name_exist_error();
    case 402: return mutable_logical_blob_name_invalid_error();
    case 450: return mutable_op_name_exist_error();
    case 460: return mutable_op_conf_device_tag_no_set_error();
    case 470: return mutable_placement_error();
    case 480: return mutable_blob_split_axis_infer_error();
    case 500: return mutable_unknown_job_build_and_infer_error();
    case 600: return mutable_rw_mutexed_object_not_found_error();
    case 700: return mutable_symbol_id_uninitialized_error();
    case 900: return mutable_unknown_error();
    case 950: return mutable_compile_option_wrong_error();
    case 1000: return mutable_input_device_not_match_error();
    default: return nullptr;
  }
}

bool ErrorProto::operator==(const ErrorProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ErrorProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ErrorProto::operator<(const ErrorProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ErrorProto::Clear() {
  if (data_) { data_.reset(); }
}
void ErrorProto::CopyFrom(const ErrorProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ErrorProto& ErrorProto::operator=(const ErrorProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field error_summary
void ErrorProto::clear_error_summary() {
  return __SharedPtr__()->clear_error_summary();
}
void ErrorProto::set_error_summary(const ::std::string& value) {
  return __SharedPtr__()->set_error_summary(value);
}
::std::string* ErrorProto::mutable_error_summary() {
  return  __SharedPtr__()->mutable_error_summary();
}
// required or optional field msg
void ErrorProto::clear_msg() {
  return __SharedPtr__()->clear_msg();
}
void ErrorProto::set_msg(const ::std::string& value) {
  return __SharedPtr__()->set_msg(value);
}
::std::string* ErrorProto::mutable_msg() {
  return  __SharedPtr__()->mutable_msg();
}
// repeated field stack_frame
void ErrorProto::clear_stack_frame() {
  return __SharedPtr__()->clear_stack_frame();
}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_* ErrorProto::mutable_stack_frame() {
  return __SharedPtr__()->mutable_stack_frame();
}
::oneflow::cfg::ErrorStackFrame* ErrorProto::mutable_stack_frame(::std::size_t index) {
  return __SharedPtr__()->mutable_stack_frame(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> ErrorProto::shared_mutable_stack_frame() {
  return mutable_stack_frame()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> ErrorProto::shared_mutable_stack_frame(::std::size_t index) {
  return mutable_stack_frame(index)->__SharedMutable__();
}
::oneflow::cfg::ErrorStackFrame* ErrorProto::add_stack_frame() {
  return __SharedPtr__()->add_stack_frame();
}
void ErrorProto::clear_config_assert_failed_error() {
  return __SharedPtr__()->clear_config_assert_failed_error();
}
::oneflow::cfg::ConfigAssertFailedError* ErrorProto::mutable_config_assert_failed_error() {
  return __SharedPtr__()->mutable_config_assert_failed_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConfigAssertFailedError> ErrorProto::shared_mutable_config_assert_failed_error() {
  return mutable_config_assert_failed_error()->__SharedMutable__();
}
void ErrorProto::clear_config_resource_unavailable_error() {
  return __SharedPtr__()->clear_config_resource_unavailable_error();
}
::oneflow::cfg::ConfigResourceUnavailableError* ErrorProto::mutable_config_resource_unavailable_error() {
  return __SharedPtr__()->mutable_config_resource_unavailable_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConfigResourceUnavailableError> ErrorProto::shared_mutable_config_resource_unavailable_error() {
  return mutable_config_resource_unavailable_error()->__SharedMutable__();
}
void ErrorProto::clear_proto_parse_failed_error() {
  return __SharedPtr__()->clear_proto_parse_failed_error();
}
::oneflow::cfg::ProtoParseFailedError* ErrorProto::mutable_proto_parse_failed_error() {
  return __SharedPtr__()->mutable_proto_parse_failed_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ProtoParseFailedError> ErrorProto::shared_mutable_proto_parse_failed_error() {
  return mutable_proto_parse_failed_error()->__SharedMutable__();
}
void ErrorProto::clear_check_failed_error() {
  return __SharedPtr__()->clear_check_failed_error();
}
::oneflow::cfg::CheckFailedError* ErrorProto::mutable_check_failed_error() {
  return __SharedPtr__()->mutable_check_failed_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::CheckFailedError> ErrorProto::shared_mutable_check_failed_error() {
  return mutable_check_failed_error()->__SharedMutable__();
}
void ErrorProto::clear_todo_error() {
  return __SharedPtr__()->clear_todo_error();
}
::oneflow::cfg::TodoError* ErrorProto::mutable_todo_error() {
  return __SharedPtr__()->mutable_todo_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TodoError> ErrorProto::shared_mutable_todo_error() {
  return mutable_todo_error()->__SharedMutable__();
}
void ErrorProto::clear_unimplemented_error() {
  return __SharedPtr__()->clear_unimplemented_error();
}
::oneflow::cfg::UnimplementedError* ErrorProto::mutable_unimplemented_error() {
  return __SharedPtr__()->mutable_unimplemented_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::UnimplementedError> ErrorProto::shared_mutable_unimplemented_error() {
  return mutable_unimplemented_error()->__SharedMutable__();
}
void ErrorProto::clear_boxing_not_supported_error() {
  return __SharedPtr__()->clear_boxing_not_supported_error();
}
::oneflow::cfg::BoxingNotSupportedError* ErrorProto::mutable_boxing_not_supported_error() {
  return __SharedPtr__()->mutable_boxing_not_supported_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BoxingNotSupportedError> ErrorProto::shared_mutable_boxing_not_supported_error() {
  return mutable_boxing_not_supported_error()->__SharedMutable__();
}
void ErrorProto::clear_gradient_function_not_found_error() {
  return __SharedPtr__()->clear_gradient_function_not_found_error();
}
::oneflow::cfg::GradientFunctionNotFoundError* ErrorProto::mutable_gradient_function_not_found_error() {
  return __SharedPtr__()->mutable_gradient_function_not_found_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::GradientFunctionNotFoundError> ErrorProto::shared_mutable_gradient_function_not_found_error() {
  return mutable_gradient_function_not_found_error()->__SharedMutable__();
}
void ErrorProto::clear_op_kernel_not_found_error() {
  return __SharedPtr__()->clear_op_kernel_not_found_error();
}
::oneflow::cfg::OpKernelNotFoundError* ErrorProto::mutable_op_kernel_not_found_error() {
  return __SharedPtr__()->mutable_op_kernel_not_found_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpKernelNotFoundError> ErrorProto::shared_mutable_op_kernel_not_found_error() {
  return mutable_op_kernel_not_found_error()->__SharedMutable__();
}
void ErrorProto::clear_multiple_op_kernels_matched_error() {
  return __SharedPtr__()->clear_multiple_op_kernels_matched_error();
}
::oneflow::cfg::MultipleOpKernelsMatchedError* ErrorProto::mutable_multiple_op_kernels_matched_error() {
  return __SharedPtr__()->mutable_multiple_op_kernels_matched_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MultipleOpKernelsMatchedError> ErrorProto::shared_mutable_multiple_op_kernels_matched_error() {
  return mutable_multiple_op_kernels_matched_error()->__SharedMutable__();
}
void ErrorProto::clear_memory_zone_out_of_memory_error() {
  return __SharedPtr__()->clear_memory_zone_out_of_memory_error();
}
::oneflow::cfg::MemoryZoneOutOfMemoryError* ErrorProto::mutable_memory_zone_out_of_memory_error() {
  return __SharedPtr__()->mutable_memory_zone_out_of_memory_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MemoryZoneOutOfMemoryError> ErrorProto::shared_mutable_memory_zone_out_of_memory_error() {
  return mutable_memory_zone_out_of_memory_error()->__SharedMutable__();
}
void ErrorProto::clear_loss_blob_not_found_error() {
  return __SharedPtr__()->clear_loss_blob_not_found_error();
}
::oneflow::cfg::LossBlobNotFoundError* ErrorProto::mutable_loss_blob_not_found_error() {
  return __SharedPtr__()->mutable_loss_blob_not_found_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LossBlobNotFoundError> ErrorProto::shared_mutable_loss_blob_not_found_error() {
  return mutable_loss_blob_not_found_error()->__SharedMutable__();
}
void ErrorProto::clear_job_set_empty_error() {
  return __SharedPtr__()->clear_job_set_empty_error();
}
::oneflow::cfg::JobSetEmptyError* ErrorProto::mutable_job_set_empty_error() {
  return __SharedPtr__()->mutable_job_set_empty_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobSetEmptyError> ErrorProto::shared_mutable_job_set_empty_error() {
  return mutable_job_set_empty_error()->__SharedMutable__();
}
void ErrorProto::clear_device_tag_not_found_error() {
  return __SharedPtr__()->clear_device_tag_not_found_error();
}
::oneflow::cfg::DeviceTagNotFoundError* ErrorProto::mutable_device_tag_not_found_error() {
  return __SharedPtr__()->mutable_device_tag_not_found_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::DeviceTagNotFoundError> ErrorProto::shared_mutable_device_tag_not_found_error() {
  return mutable_device_tag_not_found_error()->__SharedMutable__();
}
void ErrorProto::clear_value_error() {
  return __SharedPtr__()->clear_value_error();
}
::oneflow::cfg::ValueError* ErrorProto::mutable_value_error() {
  return __SharedPtr__()->mutable_value_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ValueError> ErrorProto::shared_mutable_value_error() {
  return mutable_value_error()->__SharedMutable__();
}
void ErrorProto::clear_index_error() {
  return __SharedPtr__()->clear_index_error();
}
::oneflow::cfg::IndexError* ErrorProto::mutable_index_error() {
  return __SharedPtr__()->mutable_index_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::IndexError> ErrorProto::shared_mutable_index_error() {
  return mutable_index_error()->__SharedMutable__();
}
void ErrorProto::clear_timeout_error() {
  return __SharedPtr__()->clear_timeout_error();
}
::oneflow::cfg::TimeoutError* ErrorProto::mutable_timeout_error() {
  return __SharedPtr__()->mutable_timeout_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TimeoutError> ErrorProto::shared_mutable_timeout_error() {
  return mutable_timeout_error()->__SharedMutable__();
}
void ErrorProto::clear_job_name_exist_error() {
  return __SharedPtr__()->clear_job_name_exist_error();
}
::oneflow::cfg::JobNameExistError* ErrorProto::mutable_job_name_exist_error() {
  return __SharedPtr__()->mutable_job_name_exist_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobNameExistError> ErrorProto::shared_mutable_job_name_exist_error() {
  return mutable_job_name_exist_error()->__SharedMutable__();
}
void ErrorProto::clear_job_name_empty_error() {
  return __SharedPtr__()->clear_job_name_empty_error();
}
::oneflow::cfg::JobNameEmptyError* ErrorProto::mutable_job_name_empty_error() {
  return __SharedPtr__()->mutable_job_name_empty_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobNameEmptyError> ErrorProto::shared_mutable_job_name_empty_error() {
  return mutable_job_name_empty_error()->__SharedMutable__();
}
void ErrorProto::clear_job_name_not_equal_error() {
  return __SharedPtr__()->clear_job_name_not_equal_error();
}
::oneflow::cfg::JobNameNotEqualError* ErrorProto::mutable_job_name_not_equal_error() {
  return __SharedPtr__()->mutable_job_name_not_equal_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobNameNotEqualError> ErrorProto::shared_mutable_job_name_not_equal_error() {
  return mutable_job_name_not_equal_error()->__SharedMutable__();
}
void ErrorProto::clear_no_job_build_and_infer_ctx_error() {
  return __SharedPtr__()->clear_no_job_build_and_infer_ctx_error();
}
::oneflow::cfg::NoJobBuildAndInferCtxError* ErrorProto::mutable_no_job_build_and_infer_ctx_error() {
  return __SharedPtr__()->mutable_no_job_build_and_infer_ctx_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::NoJobBuildAndInferCtxError> ErrorProto::shared_mutable_no_job_build_and_infer_ctx_error() {
  return mutable_no_job_build_and_infer_ctx_error()->__SharedMutable__();
}
void ErrorProto::clear_job_conf_frozen_error() {
  return __SharedPtr__()->clear_job_conf_frozen_error();
}
::oneflow::cfg::JobConfFrozenError* ErrorProto::mutable_job_conf_frozen_error() {
  return __SharedPtr__()->mutable_job_conf_frozen_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobConfFrozenError> ErrorProto::shared_mutable_job_conf_frozen_error() {
  return mutable_job_conf_frozen_error()->__SharedMutable__();
}
void ErrorProto::clear_job_conf_not_set_error() {
  return __SharedPtr__()->clear_job_conf_not_set_error();
}
::oneflow::cfg::JobConfNotSetError* ErrorProto::mutable_job_conf_not_set_error() {
  return __SharedPtr__()->mutable_job_conf_not_set_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobConfNotSetError> ErrorProto::shared_mutable_job_conf_not_set_error() {
  return mutable_job_conf_not_set_error()->__SharedMutable__();
}
void ErrorProto::clear_job_conf_repeated_set_error() {
  return __SharedPtr__()->clear_job_conf_repeated_set_error();
}
::oneflow::cfg::JobConfRepeatedSetError* ErrorProto::mutable_job_conf_repeated_set_error() {
  return __SharedPtr__()->mutable_job_conf_repeated_set_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobConfRepeatedSetError> ErrorProto::shared_mutable_job_conf_repeated_set_error() {
  return mutable_job_conf_repeated_set_error()->__SharedMutable__();
}
void ErrorProto::clear_job_type_not_set_error() {
  return __SharedPtr__()->clear_job_type_not_set_error();
}
::oneflow::cfg::JobTypeNotSetError* ErrorProto::mutable_job_type_not_set_error() {
  return __SharedPtr__()->mutable_job_type_not_set_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobTypeNotSetError> ErrorProto::shared_mutable_job_type_not_set_error() {
  return mutable_job_type_not_set_error()->__SharedMutable__();
}
void ErrorProto::clear_logical_blob_name_not_exist_error() {
  return __SharedPtr__()->clear_logical_blob_name_not_exist_error();
}
::oneflow::cfg::LogicalBlobNameNotExistError* ErrorProto::mutable_logical_blob_name_not_exist_error() {
  return __SharedPtr__()->mutable_logical_blob_name_not_exist_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobNameNotExistError> ErrorProto::shared_mutable_logical_blob_name_not_exist_error() {
  return mutable_logical_blob_name_not_exist_error()->__SharedMutable__();
}
void ErrorProto::clear_logical_blob_name_exist_error() {
  return __SharedPtr__()->clear_logical_blob_name_exist_error();
}
::oneflow::cfg::LogicalBlobNameExistError* ErrorProto::mutable_logical_blob_name_exist_error() {
  return __SharedPtr__()->mutable_logical_blob_name_exist_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobNameExistError> ErrorProto::shared_mutable_logical_blob_name_exist_error() {
  return mutable_logical_blob_name_exist_error()->__SharedMutable__();
}
void ErrorProto::clear_logical_blob_name_invalid_error() {
  return __SharedPtr__()->clear_logical_blob_name_invalid_error();
}
::oneflow::cfg::LogicalBlobNameInvalidError* ErrorProto::mutable_logical_blob_name_invalid_error() {
  return __SharedPtr__()->mutable_logical_blob_name_invalid_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobNameInvalidError> ErrorProto::shared_mutable_logical_blob_name_invalid_error() {
  return mutable_logical_blob_name_invalid_error()->__SharedMutable__();
}
void ErrorProto::clear_op_name_exist_error() {
  return __SharedPtr__()->clear_op_name_exist_error();
}
::oneflow::cfg::OpNameExistError* ErrorProto::mutable_op_name_exist_error() {
  return __SharedPtr__()->mutable_op_name_exist_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpNameExistError> ErrorProto::shared_mutable_op_name_exist_error() {
  return mutable_op_name_exist_error()->__SharedMutable__();
}
void ErrorProto::clear_op_conf_device_tag_no_set_error() {
  return __SharedPtr__()->clear_op_conf_device_tag_no_set_error();
}
::oneflow::cfg::OpConfDeviceTagNoSetError* ErrorProto::mutable_op_conf_device_tag_no_set_error() {
  return __SharedPtr__()->mutable_op_conf_device_tag_no_set_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpConfDeviceTagNoSetError> ErrorProto::shared_mutable_op_conf_device_tag_no_set_error() {
  return mutable_op_conf_device_tag_no_set_error()->__SharedMutable__();
}
void ErrorProto::clear_placement_error() {
  return __SharedPtr__()->clear_placement_error();
}
::oneflow::cfg::PlacementError* ErrorProto::mutable_placement_error() {
  return __SharedPtr__()->mutable_placement_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PlacementError> ErrorProto::shared_mutable_placement_error() {
  return mutable_placement_error()->__SharedMutable__();
}
void ErrorProto::clear_blob_split_axis_infer_error() {
  return __SharedPtr__()->clear_blob_split_axis_infer_error();
}
::oneflow::cfg::BlobSplitAxisInferError* ErrorProto::mutable_blob_split_axis_infer_error() {
  return __SharedPtr__()->mutable_blob_split_axis_infer_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobSplitAxisInferError> ErrorProto::shared_mutable_blob_split_axis_infer_error() {
  return mutable_blob_split_axis_infer_error()->__SharedMutable__();
}
void ErrorProto::clear_unknown_job_build_and_infer_error() {
  return __SharedPtr__()->clear_unknown_job_build_and_infer_error();
}
::oneflow::cfg::UnknownJobBuildAndInferError* ErrorProto::mutable_unknown_job_build_and_infer_error() {
  return __SharedPtr__()->mutable_unknown_job_build_and_infer_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::UnknownJobBuildAndInferError> ErrorProto::shared_mutable_unknown_job_build_and_infer_error() {
  return mutable_unknown_job_build_and_infer_error()->__SharedMutable__();
}
void ErrorProto::clear_rw_mutexed_object_not_found_error() {
  return __SharedPtr__()->clear_rw_mutexed_object_not_found_error();
}
::oneflow::cfg::RwMutexedObjectNotFoundError* ErrorProto::mutable_rw_mutexed_object_not_found_error() {
  return __SharedPtr__()->mutable_rw_mutexed_object_not_found_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RwMutexedObjectNotFoundError> ErrorProto::shared_mutable_rw_mutexed_object_not_found_error() {
  return mutable_rw_mutexed_object_not_found_error()->__SharedMutable__();
}
void ErrorProto::clear_symbol_id_uninitialized_error() {
  return __SharedPtr__()->clear_symbol_id_uninitialized_error();
}
::oneflow::cfg::SymbolIdUninitializedError* ErrorProto::mutable_symbol_id_uninitialized_error() {
  return __SharedPtr__()->mutable_symbol_id_uninitialized_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::SymbolIdUninitializedError> ErrorProto::shared_mutable_symbol_id_uninitialized_error() {
  return mutable_symbol_id_uninitialized_error()->__SharedMutable__();
}
void ErrorProto::clear_unknown_error() {
  return __SharedPtr__()->clear_unknown_error();
}
::oneflow::cfg::UnknownError* ErrorProto::mutable_unknown_error() {
  return __SharedPtr__()->mutable_unknown_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::UnknownError> ErrorProto::shared_mutable_unknown_error() {
  return mutable_unknown_error()->__SharedMutable__();
}
void ErrorProto::clear_compile_option_wrong_error() {
  return __SharedPtr__()->clear_compile_option_wrong_error();
}
::oneflow::cfg::CompileOptionWrongError* ErrorProto::mutable_compile_option_wrong_error() {
  return __SharedPtr__()->mutable_compile_option_wrong_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::CompileOptionWrongError> ErrorProto::shared_mutable_compile_option_wrong_error() {
  return mutable_compile_option_wrong_error()->__SharedMutable__();
}
void ErrorProto::clear_input_device_not_match_error() {
  return __SharedPtr__()->clear_input_device_not_match_error();
}
::oneflow::cfg::InputDeviceNotMatchError* ErrorProto::mutable_input_device_not_match_error() {
  return __SharedPtr__()->mutable_input_device_not_match_error();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::InputDeviceNotMatchError> ErrorProto::shared_mutable_input_device_not_match_error() {
  return mutable_input_device_not_match_error()->__SharedMutable__();
}

::std::shared_ptr<ErrorProto> ErrorProto::__SharedMutable__() {
  return ::std::make_shared<ErrorProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ErrorStackFrame>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ErrorStackFrame>(data) {}
Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_() = default;
Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::~Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_() = default;


bool Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::operator==(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ErrorStackFrame>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::operator<(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstErrorStackFrame> Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ErrorStackFrame>>& data): Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_(data) {}
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_() = default;
_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::~_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_() = default;

void _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::CopyFrom(const Const_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ErrorStackFrame>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::CopyFrom(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ErrorStackFrame>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::operator==(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ErrorStackFrame>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::operator<(const _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_> _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ErrorStackFrame> _CFG_ONEFLOW_CORE_COMMON_ERROR_CFG_H__RepeatedField_ErrorStackFrame_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
