#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.register.blob_desc", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstBlobDescProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstBlobDescProto>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstBlobDescProto> (Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_&))&_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::*)(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_&))&_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<BlobDescProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<BlobDescProto>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::BlobDescProto> (_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstBlobDescProto, ::oneflow::cfg::Message, std::shared_ptr<ConstBlobDescProto>> registry(m, "ConstBlobDescProto");
    registry.def("__id__", &::oneflow::cfg::BlobDescProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBlobDescProto::DebugString);
    registry.def("__repr__", &ConstBlobDescProto::DebugString);

    registry.def("has_shape", &ConstBlobDescProto::has_shape);
    registry.def("shape", &ConstBlobDescProto::shared_const_shape);

    registry.def("has_data_type", &ConstBlobDescProto::has_data_type);
    registry.def("data_type", &ConstBlobDescProto::data_type);

    registry.def("has_is_dynamic", &ConstBlobDescProto::has_is_dynamic);
    registry.def("is_dynamic", &ConstBlobDescProto::is_dynamic);
  }
  {
    pybind11::class_<::oneflow::cfg::BlobDescProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BlobDescProto>> registry(m, "BlobDescProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BlobDescProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobDescProto::*)(const ConstBlobDescProto&))&::oneflow::cfg::BlobDescProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobDescProto::*)(const ::oneflow::cfg::BlobDescProto&))&::oneflow::cfg::BlobDescProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BlobDescProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BlobDescProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BlobDescProto::DebugString);



    registry.def("has_shape", &::oneflow::cfg::BlobDescProto::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::BlobDescProto::clear_shape);
    registry.def("shape", &::oneflow::cfg::BlobDescProto::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::BlobDescProto::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::BlobDescProto::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::BlobDescProto::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::BlobDescProto::data_type);
    registry.def("set_data_type", &::oneflow::cfg::BlobDescProto::set_data_type);

    registry.def("has_is_dynamic", &::oneflow::cfg::BlobDescProto::has_is_dynamic);
    registry.def("clear_is_dynamic", &::oneflow::cfg::BlobDescProto::clear_is_dynamic);
    registry.def("is_dynamic", &::oneflow::cfg::BlobDescProto::is_dynamic);
    registry.def("set_is_dynamic", &::oneflow::cfg::BlobDescProto::set_is_dynamic);
  }
  {
    pybind11::class_<ConstBlobDescSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstBlobDescSignature>> registry(m, "ConstBlobDescSignature");
    registry.def("__id__", &::oneflow::cfg::BlobDescSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBlobDescSignature::DebugString);
    registry.def("__repr__", &ConstBlobDescSignature::DebugString);

    registry.def("bn_in_op2blob_desc_size", &ConstBlobDescSignature::bn_in_op2blob_desc_size);
    registry.def("bn_in_op2blob_desc", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> (ConstBlobDescSignature::*)() const)&ConstBlobDescSignature::shared_const_bn_in_op2blob_desc);

    registry.def("bn_in_op2blob_desc", (::std::shared_ptr<ConstBlobDescProto> (ConstBlobDescSignature::*)(const ::std::string&) const)&ConstBlobDescSignature::shared_const_bn_in_op2blob_desc);
  }
  {
    pybind11::class_<::oneflow::cfg::BlobDescSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BlobDescSignature>> registry(m, "BlobDescSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BlobDescSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobDescSignature::*)(const ConstBlobDescSignature&))&::oneflow::cfg::BlobDescSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobDescSignature::*)(const ::oneflow::cfg::BlobDescSignature&))&::oneflow::cfg::BlobDescSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BlobDescSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BlobDescSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BlobDescSignature::DebugString);



    registry.def("bn_in_op2blob_desc_size", &::oneflow::cfg::BlobDescSignature::bn_in_op2blob_desc_size);
    registry.def("bn_in_op2blob_desc", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> (::oneflow::cfg::BlobDescSignature::*)() const)&::oneflow::cfg::BlobDescSignature::shared_const_bn_in_op2blob_desc);
    registry.def("clear_bn_in_op2blob_desc", &::oneflow::cfg::BlobDescSignature::clear_bn_in_op2blob_desc);
    registry.def("mutable_bn_in_op2blob_desc", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> (::oneflow::cfg::BlobDescSignature::*)())&::oneflow::cfg::BlobDescSignature::shared_mutable_bn_in_op2blob_desc);
    registry.def("bn_in_op2blob_desc", (::std::shared_ptr<ConstBlobDescProto> (::oneflow::cfg::BlobDescSignature::*)(const ::std::string&) const)&::oneflow::cfg::BlobDescSignature::shared_const_bn_in_op2blob_desc);
  }
}