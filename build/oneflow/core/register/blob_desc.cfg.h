#ifndef CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H_
#define CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {

// forward declare proto class;
class BlobDescProto;
class BlobDescSignature_BnInOp2blobDescEntry;
class BlobDescSignature;

namespace cfg {


class BlobDescProto;
class ConstBlobDescProto;

class BlobDescSignature;
class ConstBlobDescSignature;



class ConstBlobDescProto : public ::oneflow::cfg::Message {
 public:

  class _BlobDescProto_ {
   public:
    _BlobDescProto_();
    explicit _BlobDescProto_(const _BlobDescProto_& other);
    explicit _BlobDescProto_(_BlobDescProto_&& other);
    _BlobDescProto_(const ::oneflow::BlobDescProto& proto_blobdescproto);
    ~_BlobDescProto_();

    void InitFromProto(const ::oneflow::BlobDescProto& proto_blobdescproto);

    void ToProto(::oneflow::BlobDescProto* proto_blobdescproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobDescProto_& other);
  
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
      // optional field is_dynamic
     public:
    bool has_is_dynamic() const;
    const bool& is_dynamic() const;
    void clear_is_dynamic();
    void set_is_dynamic(const bool& value);
    bool* mutable_is_dynamic();
   protected:
    bool has_is_dynamic_ = false;
    bool is_dynamic_;
           
   public:
    int compare(const _BlobDescProto_& other);

    bool operator==(const _BlobDescProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobDescProto_& other) const;
  };

  ConstBlobDescProto(const ::std::shared_ptr<_BlobDescProto_>& data);
  ConstBlobDescProto(const ConstBlobDescProto&);
  ConstBlobDescProto(ConstBlobDescProto&&) noexcept;
  ConstBlobDescProto();
  ConstBlobDescProto(const ::oneflow::BlobDescProto& proto_blobdescproto);
  virtual ~ConstBlobDescProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_blobdescproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
  // required or optional field is_dynamic
 public:
  bool has_is_dynamic() const;
  const bool& is_dynamic() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstBlobDescProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobDescProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobDescProto& other) const;
 protected:
  const ::std::shared_ptr<_BlobDescProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobDescProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobDescProto
  void BuildFromProto(const PbMessage& proto_blobdescproto);
  
  ::std::shared_ptr<_BlobDescProto_> data_;
};

class BlobDescProto final : public ConstBlobDescProto {
 public:
  BlobDescProto(const ::std::shared_ptr<_BlobDescProto_>& data);
  BlobDescProto(const BlobDescProto& other);
  // enable nothrow for ::std::vector<BlobDescProto> resize 
  BlobDescProto(BlobDescProto&&) noexcept;
  BlobDescProto();
  explicit BlobDescProto(const ::oneflow::BlobDescProto& proto_blobdescproto);

  ~BlobDescProto() override;

  void InitFromProto(const PbMessage& proto_blobdescproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobDescProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobDescProto& other) const;
  void Clear();
  void CopyFrom(const BlobDescProto& other);
  BlobDescProto& operator=(const BlobDescProto& other);

  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  // required or optional field is_dynamic
 public:
  void clear_is_dynamic();
  void set_is_dynamic(const bool& value);
  bool* mutable_is_dynamic();

  ::std::shared_ptr<BlobDescProto> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_; 
class Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_;

class ConstBlobDescSignature : public ::oneflow::cfg::Message {
 public:

  class _BlobDescSignature_ {
   public:
    _BlobDescSignature_();
    explicit _BlobDescSignature_(const _BlobDescSignature_& other);
    explicit _BlobDescSignature_(_BlobDescSignature_&& other);
    _BlobDescSignature_(const ::oneflow::BlobDescSignature& proto_blobdescsignature);
    ~_BlobDescSignature_();

    void InitFromProto(const ::oneflow::BlobDescSignature& proto_blobdescsignature);

    void ToProto(::oneflow::BlobDescSignature* proto_blobdescsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobDescSignature_& other);
  
     public:
    ::std::size_t bn_in_op2blob_desc_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& bn_in_op2blob_desc() const;

    _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ * mutable_bn_in_op2blob_desc();

    const ::oneflow::cfg::BlobDescProto& bn_in_op2blob_desc(::std::string key) const;

    void clear_bn_in_op2blob_desc();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> bn_in_op2blob_desc_;
         
   public:
    int compare(const _BlobDescSignature_& other);

    bool operator==(const _BlobDescSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobDescSignature_& other) const;
  };

  ConstBlobDescSignature(const ::std::shared_ptr<_BlobDescSignature_>& data);
  ConstBlobDescSignature(const ConstBlobDescSignature&);
  ConstBlobDescSignature(ConstBlobDescSignature&&) noexcept;
  ConstBlobDescSignature();
  ConstBlobDescSignature(const ::oneflow::BlobDescSignature& proto_blobdescsignature);
  virtual ~ConstBlobDescSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_blobdescsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2blob_desc
 public:
  ::std::size_t bn_in_op2blob_desc_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& bn_in_op2blob_desc() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> shared_const_bn_in_op2blob_desc() const;

 public:
  ::std::shared_ptr<ConstBlobDescSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobDescSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobDescSignature& other) const;
 protected:
  const ::std::shared_ptr<_BlobDescSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobDescSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobDescSignature
  void BuildFromProto(const PbMessage& proto_blobdescsignature);
  
  ::std::shared_ptr<_BlobDescSignature_> data_;
};

class BlobDescSignature final : public ConstBlobDescSignature {
 public:
  BlobDescSignature(const ::std::shared_ptr<_BlobDescSignature_>& data);
  BlobDescSignature(const BlobDescSignature& other);
  // enable nothrow for ::std::vector<BlobDescSignature> resize 
  BlobDescSignature(BlobDescSignature&&) noexcept;
  BlobDescSignature();
  explicit BlobDescSignature(const ::oneflow::BlobDescSignature& proto_blobdescsignature);

  ~BlobDescSignature() override;

  void InitFromProto(const PbMessage& proto_blobdescsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobDescSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobDescSignature& other) const;
  void Clear();
  void CopyFrom(const BlobDescSignature& other);
  BlobDescSignature& operator=(const BlobDescSignature& other);

  // repeated field bn_in_op2blob_desc
 public:
  void clear_bn_in_op2blob_desc();

  const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ & bn_in_op2blob_desc() const;

  _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_* mutable_bn_in_op2blob_desc();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> shared_mutable_bn_in_op2blob_desc();

  ::std::shared_ptr<BlobDescSignature> __SharedMutable__();
};







// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::BlobDescProto& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstBlobDescProto> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ConstBlobDescProto>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_();
  ~_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::BlobDescProto> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ::oneflow::cfg::BlobDescProto>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstBlobDescProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobDescProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobDescProto> {
  std::size_t operator()(const ::oneflow::cfg::BlobDescProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBlobDescSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobDescSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobDescSignature> {
  std::size_t operator()(const ::oneflow::cfg::BlobDescSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H_