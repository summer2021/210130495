#ifndef CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H_
#define CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstRangeProto;
class RangeProto;
}
}

namespace oneflow {

// forward declare proto class;
class TensorSliceViewProto;

namespace cfg {


class TensorSliceViewProto;
class ConstTensorSliceViewProto;


class _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_;
class Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_;

class ConstTensorSliceViewProto : public ::oneflow::cfg::Message {
 public:

  class _TensorSliceViewProto_ {
   public:
    _TensorSliceViewProto_();
    explicit _TensorSliceViewProto_(const _TensorSliceViewProto_& other);
    explicit _TensorSliceViewProto_(_TensorSliceViewProto_&& other);
    _TensorSliceViewProto_(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto);
    ~_TensorSliceViewProto_();

    void InitFromProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto);

    void ToProto(::oneflow::TensorSliceViewProto* proto_tensorsliceviewproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TensorSliceViewProto_& other);
  
      // repeated field dim
   public:
    ::std::size_t dim_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& dim() const;
    const ::oneflow::cfg::RangeProto& dim(::std::size_t index) const;
    void clear_dim();
    _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_* mutable_dim();
    ::oneflow::cfg::RangeProto* mutable_dim(::std::size_t index);
      ::oneflow::cfg::RangeProto* add_dim();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> dim_;
         
   public:
    int compare(const _TensorSliceViewProto_& other);

    bool operator==(const _TensorSliceViewProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TensorSliceViewProto_& other) const;
  };

  ConstTensorSliceViewProto(const ::std::shared_ptr<_TensorSliceViewProto_>& data);
  ConstTensorSliceViewProto(const ConstTensorSliceViewProto&);
  ConstTensorSliceViewProto(ConstTensorSliceViewProto&&) noexcept;
  ConstTensorSliceViewProto();
  ConstTensorSliceViewProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto);
  virtual ~ConstTensorSliceViewProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_tensorsliceviewproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field dim
 public:
  ::std::size_t dim_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& dim() const;
  const ::oneflow::cfg::RangeProto& dim(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> shared_const_dim() const;
  ::std::shared_ptr<::oneflow::cfg::ConstRangeProto> shared_const_dim(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstTensorSliceViewProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTensorSliceViewProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTensorSliceViewProto& other) const;
 protected:
  const ::std::shared_ptr<_TensorSliceViewProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TensorSliceViewProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTensorSliceViewProto
  void BuildFromProto(const PbMessage& proto_tensorsliceviewproto);
  
  ::std::shared_ptr<_TensorSliceViewProto_> data_;
};

class TensorSliceViewProto final : public ConstTensorSliceViewProto {
 public:
  TensorSliceViewProto(const ::std::shared_ptr<_TensorSliceViewProto_>& data);
  TensorSliceViewProto(const TensorSliceViewProto& other);
  // enable nothrow for ::std::vector<TensorSliceViewProto> resize 
  TensorSliceViewProto(TensorSliceViewProto&&) noexcept;
  TensorSliceViewProto();
  explicit TensorSliceViewProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto);

  ~TensorSliceViewProto() override;

  void InitFromProto(const PbMessage& proto_tensorsliceviewproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TensorSliceViewProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TensorSliceViewProto& other) const;
  void Clear();
  void CopyFrom(const TensorSliceViewProto& other);
  TensorSliceViewProto& operator=(const TensorSliceViewProto& other);

  // repeated field dim
 public:
  void clear_dim();
  _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_* mutable_dim();
  ::oneflow::cfg::RangeProto* mutable_dim(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> shared_mutable_dim();
  ::std::shared_ptr<::oneflow::cfg::RangeProto> shared_mutable_dim(::std::size_t index);
  ::oneflow::cfg::RangeProto* add_dim();

  ::std::shared_ptr<TensorSliceViewProto> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::RangeProto> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::RangeProto>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstRangeProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::RangeProto>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_();
  ~_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::RangeProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::RangeProto> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstTensorSliceViewProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstTensorSliceViewProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TensorSliceViewProto> {
  std::size_t operator()(const ::oneflow::cfg::TensorSliceViewProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H_