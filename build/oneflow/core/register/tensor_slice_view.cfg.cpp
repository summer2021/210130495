#include "oneflow/core/register/tensor_slice_view.cfg.h"
#include "oneflow/core/common/range.cfg.h"
#include "oneflow/core/register/tensor_slice_view.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstTensorSliceViewProto::_TensorSliceViewProto_::_TensorSliceViewProto_() { Clear(); }
ConstTensorSliceViewProto::_TensorSliceViewProto_::_TensorSliceViewProto_(const _TensorSliceViewProto_& other) { CopyFrom(other); }
ConstTensorSliceViewProto::_TensorSliceViewProto_::_TensorSliceViewProto_(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto) {
  InitFromProto(proto_tensorsliceviewproto);
}
ConstTensorSliceViewProto::_TensorSliceViewProto_::_TensorSliceViewProto_(_TensorSliceViewProto_&& other) = default;
ConstTensorSliceViewProto::_TensorSliceViewProto_::~_TensorSliceViewProto_() = default;

void ConstTensorSliceViewProto::_TensorSliceViewProto_::InitFromProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto) {
  Clear();
  // repeated field: dim
  if (!proto_tensorsliceviewproto.dim().empty()) {
    for (const ::oneflow::RangeProto& elem : proto_tensorsliceviewproto.dim() ) {
      *mutable_dim()->Add() = ::oneflow::cfg::RangeProto(elem);
    }
  }
    
}

void ConstTensorSliceViewProto::_TensorSliceViewProto_::ToProto(::oneflow::TensorSliceViewProto* proto_tensorsliceviewproto) const {
  proto_tensorsliceviewproto->Clear();
  // repeated field: dim
  if (!dim().empty()) {
    for (const ::oneflow::cfg::RangeProto& elem : dim() ) {
      ::oneflow::RangeProto proto_dim_elem;
      elem.ToProto(&proto_dim_elem);
      *proto_tensorsliceviewproto->mutable_dim()->Add() = proto_dim_elem;
    }
  }

}

::std::string ConstTensorSliceViewProto::_TensorSliceViewProto_::DebugString() const {
  ::oneflow::TensorSliceViewProto proto_tensorsliceviewproto;
  this->ToProto(&proto_tensorsliceviewproto);
  return proto_tensorsliceviewproto.DebugString();
}

void ConstTensorSliceViewProto::_TensorSliceViewProto_::Clear() {
  clear_dim();
}

void ConstTensorSliceViewProto::_TensorSliceViewProto_::CopyFrom(const _TensorSliceViewProto_& other) {
  mutable_dim()->CopyFrom(other.dim());
}


// repeated field dim
::std::size_t ConstTensorSliceViewProto::_TensorSliceViewProto_::dim_size() const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
    return default_static_value->size();
  }
  return dim_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& ConstTensorSliceViewProto::_TensorSliceViewProto_::dim() const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
    return *(default_static_value.get());
  }
  return *(dim_.get());
}
const ::oneflow::cfg::RangeProto& ConstTensorSliceViewProto::_TensorSliceViewProto_::dim(::std::size_t index) const {
  if (!dim_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
    return default_static_value->Get(index);
  }
  return dim_->Get(index);
}
void ConstTensorSliceViewProto::_TensorSliceViewProto_::clear_dim() {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
  }
  return dim_->Clear();
}
_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_* ConstTensorSliceViewProto::_TensorSliceViewProto_::mutable_dim() {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
  }
  return  dim_.get();
}
::oneflow::cfg::RangeProto* ConstTensorSliceViewProto::_TensorSliceViewProto_::mutable_dim(::std::size_t index) {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
  }
  return  dim_->Mutable(index);
}
::oneflow::cfg::RangeProto* ConstTensorSliceViewProto::_TensorSliceViewProto_::add_dim() {
  if (!dim_) {
    dim_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>();
  }
  return dim_->Add();
}


int ConstTensorSliceViewProto::_TensorSliceViewProto_::compare(const _TensorSliceViewProto_& other) {
  if (!(dim() == other.dim())) {
    return dim() < other.dim() ? -1 : 1;
  }
  return 0;
}

bool ConstTensorSliceViewProto::_TensorSliceViewProto_::operator==(const _TensorSliceViewProto_& other) const {
  return true
    && dim() == other.dim()
  ;
}

std::size_t ConstTensorSliceViewProto::_TensorSliceViewProto_::__CalcHash__() const {
  return 0
    ^ dim().__CalcHash__()
  ;
}

bool ConstTensorSliceViewProto::_TensorSliceViewProto_::operator<(const _TensorSliceViewProto_& other) const {
  return false
    || !(dim() == other.dim()) ? 
      dim() < other.dim() : false
  ;
}

using _TensorSliceViewProto_ =  ConstTensorSliceViewProto::_TensorSliceViewProto_;
ConstTensorSliceViewProto::ConstTensorSliceViewProto(const ::std::shared_ptr<_TensorSliceViewProto_>& data): data_(data) {}
ConstTensorSliceViewProto::ConstTensorSliceViewProto(): data_(::std::make_shared<_TensorSliceViewProto_>()) {}
ConstTensorSliceViewProto::ConstTensorSliceViewProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto) {
  BuildFromProto(proto_tensorsliceviewproto);
}
ConstTensorSliceViewProto::ConstTensorSliceViewProto(const ConstTensorSliceViewProto&) = default;
ConstTensorSliceViewProto::ConstTensorSliceViewProto(ConstTensorSliceViewProto&&) noexcept = default;
ConstTensorSliceViewProto::~ConstTensorSliceViewProto() = default;

void ConstTensorSliceViewProto::ToProto(PbMessage* proto_tensorsliceviewproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TensorSliceViewProto*>(proto_tensorsliceviewproto));
}
  
::std::string ConstTensorSliceViewProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTensorSliceViewProto::__Empty__() const {
  return !data_;
}

int ConstTensorSliceViewProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"dim", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstTensorSliceViewProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstTensorSliceViewProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::RangeProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTensorSliceViewProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &dim();
    default: return nullptr;
  }
}

// repeated field dim
::std::size_t ConstTensorSliceViewProto::dim_size() const {
  return __SharedPtrOrDefault__()->dim_size();
}
const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& ConstTensorSliceViewProto::dim() const {
  return __SharedPtrOrDefault__()->dim();
}
const ::oneflow::cfg::RangeProto& ConstTensorSliceViewProto::dim(::std::size_t index) const {
  return __SharedPtrOrDefault__()->dim(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> ConstTensorSliceViewProto::shared_const_dim() const {
  return dim().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstRangeProto> ConstTensorSliceViewProto::shared_const_dim(::std::size_t index) const {
  return dim(index).__SharedConst__();
}

::std::shared_ptr<ConstTensorSliceViewProto> ConstTensorSliceViewProto::__SharedConst__() const {
  return ::std::make_shared<ConstTensorSliceViewProto>(data_);
}
int64_t ConstTensorSliceViewProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTensorSliceViewProto::operator==(const ConstTensorSliceViewProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTensorSliceViewProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTensorSliceViewProto::operator<(const ConstTensorSliceViewProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TensorSliceViewProto_>& ConstTensorSliceViewProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TensorSliceViewProto_> default_ptr = std::make_shared<_TensorSliceViewProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_TensorSliceViewProto_>& ConstTensorSliceViewProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _TensorSliceViewProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTensorSliceViewProto
void ConstTensorSliceViewProto::BuildFromProto(const PbMessage& proto_tensorsliceviewproto) {
  data_ = ::std::make_shared<_TensorSliceViewProto_>(dynamic_cast<const ::oneflow::TensorSliceViewProto&>(proto_tensorsliceviewproto));
}

TensorSliceViewProto::TensorSliceViewProto(const ::std::shared_ptr<_TensorSliceViewProto_>& data)
  : ConstTensorSliceViewProto(data) {}
TensorSliceViewProto::TensorSliceViewProto(const TensorSliceViewProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TensorSliceViewProto> resize
TensorSliceViewProto::TensorSliceViewProto(TensorSliceViewProto&&) noexcept = default; 
TensorSliceViewProto::TensorSliceViewProto(const ::oneflow::TensorSliceViewProto& proto_tensorsliceviewproto) {
  InitFromProto(proto_tensorsliceviewproto);
}
TensorSliceViewProto::TensorSliceViewProto() = default;

TensorSliceViewProto::~TensorSliceViewProto() = default;

void TensorSliceViewProto::InitFromProto(const PbMessage& proto_tensorsliceviewproto) {
  BuildFromProto(proto_tensorsliceviewproto);
}
  
void* TensorSliceViewProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_dim();
    default: return nullptr;
  }
}

bool TensorSliceViewProto::operator==(const TensorSliceViewProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TensorSliceViewProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TensorSliceViewProto::operator<(const TensorSliceViewProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TensorSliceViewProto::Clear() {
  if (data_) { data_.reset(); }
}
void TensorSliceViewProto::CopyFrom(const TensorSliceViewProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TensorSliceViewProto& TensorSliceViewProto::operator=(const TensorSliceViewProto& other) {
  CopyFrom(other);
  return *this;
}

// repeated field dim
void TensorSliceViewProto::clear_dim() {
  return __SharedPtr__()->clear_dim();
}
_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_* TensorSliceViewProto::mutable_dim() {
  return __SharedPtr__()->mutable_dim();
}
::oneflow::cfg::RangeProto* TensorSliceViewProto::mutable_dim(::std::size_t index) {
  return __SharedPtr__()->mutable_dim(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> TensorSliceViewProto::shared_mutable_dim() {
  return mutable_dim()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::RangeProto> TensorSliceViewProto::shared_mutable_dim(::std::size_t index) {
  return mutable_dim(index)->__SharedMutable__();
}
::oneflow::cfg::RangeProto* TensorSliceViewProto::add_dim() {
  return __SharedPtr__()->add_dim();
}

::std::shared_ptr<TensorSliceViewProto> TensorSliceViewProto::__SharedMutable__() {
  return ::std::make_shared<TensorSliceViewProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::RangeProto>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::RangeProto>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::~Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_() = default;


bool Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::RangeProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstRangeProto> Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::RangeProto>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_() = default;
_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::~_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::RangeProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::RangeProto>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::RangeProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_> _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::RangeProto> _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::RangeProto> _CFG_ONEFLOW_CORE_REGISTER_TENSOR_SLICE_VIEW_CFG_H__RepeatedField_RangeProto_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
