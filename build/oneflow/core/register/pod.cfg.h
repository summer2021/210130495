#ifndef CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H_
#define CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}
namespace oneflow {
namespace cfg {
enum FieldKey : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstLogicalBlobId;
class LogicalBlobId;
}
}
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {

// forward declare proto class;
class TensorPodProto;
class StructPodProto;
class FieldId;
class FieldPodProto;
class PodProto;

namespace cfg {


class TensorPodProto;
class ConstTensorPodProto;

class StructPodProto;
class ConstStructPodProto;

class FieldId;
class ConstFieldId;

class FieldPodProto;
class ConstFieldPodProto;

class PodProto;
class ConstPodProto;

enum FieldKey : unsigned int {
  kInvalidFieldKey = 0,
  kTensorShape = 1,
  kFieldKeySize = 2,
};

inline ::std::string FieldKey_Name(FieldKey value) {
  switch (value) {
  case kInvalidFieldKey: { return "kInvalidFieldKey"; }
  case kTensorShape: { return "kTensorShape"; }
  case kFieldKeySize: { return "kFieldKeySize"; }
  default:
    return "";
  }
}



class ConstTensorPodProto : public ::oneflow::cfg::Message {
 public:

  class _TensorPodProto_ {
   public:
    _TensorPodProto_();
    explicit _TensorPodProto_(const _TensorPodProto_& other);
    explicit _TensorPodProto_(_TensorPodProto_&& other);
    _TensorPodProto_(const ::oneflow::TensorPodProto& proto_tensorpodproto);
    ~_TensorPodProto_();

    void InitFromProto(const ::oneflow::TensorPodProto& proto_tensorpodproto);

    void ToProto(::oneflow::TensorPodProto* proto_tensorpodproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TensorPodProto_& other);
  
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
           
   public:
    int compare(const _TensorPodProto_& other);

    bool operator==(const _TensorPodProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TensorPodProto_& other) const;
  };

  ConstTensorPodProto(const ::std::shared_ptr<_TensorPodProto_>& data);
  ConstTensorPodProto(const ConstTensorPodProto&);
  ConstTensorPodProto(ConstTensorPodProto&&) noexcept;
  ConstTensorPodProto();
  ConstTensorPodProto(const ::oneflow::TensorPodProto& proto_tensorpodproto);
  virtual ~ConstTensorPodProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_tensorpodproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstTensorPodProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTensorPodProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTensorPodProto& other) const;
 protected:
  const ::std::shared_ptr<_TensorPodProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TensorPodProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTensorPodProto
  void BuildFromProto(const PbMessage& proto_tensorpodproto);
  
  ::std::shared_ptr<_TensorPodProto_> data_;
};

class TensorPodProto final : public ConstTensorPodProto {
 public:
  TensorPodProto(const ::std::shared_ptr<_TensorPodProto_>& data);
  TensorPodProto(const TensorPodProto& other);
  // enable nothrow for ::std::vector<TensorPodProto> resize 
  TensorPodProto(TensorPodProto&&) noexcept;
  TensorPodProto();
  explicit TensorPodProto(const ::oneflow::TensorPodProto& proto_tensorpodproto);

  ~TensorPodProto() override;

  void InitFromProto(const PbMessage& proto_tensorpodproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TensorPodProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TensorPodProto& other) const;
  void Clear();
  void CopyFrom(const TensorPodProto& other);
  TensorPodProto& operator=(const TensorPodProto& other);

  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();

  ::std::shared_ptr<TensorPodProto> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_;
class Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_;

class ConstStructPodProto : public ::oneflow::cfg::Message {
 public:

  class _StructPodProto_ {
   public:
    _StructPodProto_();
    explicit _StructPodProto_(const _StructPodProto_& other);
    explicit _StructPodProto_(_StructPodProto_&& other);
    _StructPodProto_(const ::oneflow::StructPodProto& proto_structpodproto);
    ~_StructPodProto_();

    void InitFromProto(const ::oneflow::StructPodProto& proto_structpodproto);

    void ToProto(::oneflow::StructPodProto* proto_structpodproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _StructPodProto_& other);
  
      // repeated field field
   public:
    ::std::size_t field_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& field() const;
    const ::oneflow::cfg::FieldPodProto& field(::std::size_t index) const;
    void clear_field();
    _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_* mutable_field();
    ::oneflow::cfg::FieldPodProto* mutable_field(::std::size_t index);
      ::oneflow::cfg::FieldPodProto* add_field();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> field_;
         
   public:
    int compare(const _StructPodProto_& other);

    bool operator==(const _StructPodProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _StructPodProto_& other) const;
  };

  ConstStructPodProto(const ::std::shared_ptr<_StructPodProto_>& data);
  ConstStructPodProto(const ConstStructPodProto&);
  ConstStructPodProto(ConstStructPodProto&&) noexcept;
  ConstStructPodProto();
  ConstStructPodProto(const ::oneflow::StructPodProto& proto_structpodproto);
  virtual ~ConstStructPodProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_structpodproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field field
 public:
  ::std::size_t field_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& field() const;
  const ::oneflow::cfg::FieldPodProto& field(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> shared_const_field() const;
  ::std::shared_ptr<::oneflow::cfg::ConstFieldPodProto> shared_const_field(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstStructPodProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstStructPodProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstStructPodProto& other) const;
 protected:
  const ::std::shared_ptr<_StructPodProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_StructPodProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstStructPodProto
  void BuildFromProto(const PbMessage& proto_structpodproto);
  
  ::std::shared_ptr<_StructPodProto_> data_;
};

class StructPodProto final : public ConstStructPodProto {
 public:
  StructPodProto(const ::std::shared_ptr<_StructPodProto_>& data);
  StructPodProto(const StructPodProto& other);
  // enable nothrow for ::std::vector<StructPodProto> resize 
  StructPodProto(StructPodProto&&) noexcept;
  StructPodProto();
  explicit StructPodProto(const ::oneflow::StructPodProto& proto_structpodproto);

  ~StructPodProto() override;

  void InitFromProto(const PbMessage& proto_structpodproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const StructPodProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const StructPodProto& other) const;
  void Clear();
  void CopyFrom(const StructPodProto& other);
  StructPodProto& operator=(const StructPodProto& other);

  // repeated field field
 public:
  void clear_field();
  _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_* mutable_field();
  ::oneflow::cfg::FieldPodProto* mutable_field(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> shared_mutable_field();
  ::std::shared_ptr<::oneflow::cfg::FieldPodProto> shared_mutable_field(::std::size_t index);
  ::oneflow::cfg::FieldPodProto* add_field();

  ::std::shared_ptr<StructPodProto> __SharedMutable__();
};


class ConstFieldId : public ::oneflow::cfg::Message {
 public:

 // oneof enum field_id_type
 enum FieldIdTypeCase : unsigned int {
  FIELD_ID_TYPE_NOT_SET = 0,
    kKey = 1,
    kLbi = 2,
   };

  class _FieldId_ {
   public:
    _FieldId_();
    explicit _FieldId_(const _FieldId_& other);
    explicit _FieldId_(_FieldId_&& other);
    _FieldId_(const ::oneflow::FieldId& proto_fieldid);
    ~_FieldId_();

    void InitFromProto(const ::oneflow::FieldId& proto_fieldid);

    void ToProto(::oneflow::FieldId* proto_fieldid) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FieldId_& other);
  
     // oneof field field_id_type: key
   public:
    bool has_key() const;
    void clear_key();
    const ::oneflow::cfg::FieldKey& key() const;
      void set_key(const ::oneflow::cfg::FieldKey& value);
    ::oneflow::cfg::FieldKey* mutable_key();
      
     // oneof field field_id_type: lbi
   public:
    bool has_lbi() const;
    void clear_lbi();
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
      ::oneflow::cfg::LogicalBlobId* mutable_lbi();
           
   public:
    // oneof field_id_type
    FieldIdTypeCase field_id_type_case() const;
    bool has_field_id_type() const;
   protected:
    void clear_field_id_type();
    void field_id_type_copy_from(const _FieldId_& other);
    union FieldIdTypeUnion {
      // 64-bit aligned
      uint64_t __field_id_type_for_padding_64bit__;
          FieldKey key_;
            char lbi_[sizeof(::std::shared_ptr<::oneflow::cfg::LogicalBlobId>)];
        } field_id_type_;
    FieldIdTypeCase field_id_type_case_ = FIELD_ID_TYPE_NOT_SET;
     
   public:
    int compare(const _FieldId_& other);

    bool operator==(const _FieldId_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FieldId_& other) const;
  };

  ConstFieldId(const ::std::shared_ptr<_FieldId_>& data);
  ConstFieldId(const ConstFieldId&);
  ConstFieldId(ConstFieldId&&) noexcept;
  ConstFieldId();
  ConstFieldId(const ::oneflow::FieldId& proto_fieldid);
  virtual ~ConstFieldId() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_fieldid) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field field_id_type: key
 public:
  bool has_key() const;
  const ::oneflow::cfg::FieldKey& key() const;
  // used by pybind11 only
 // oneof field field_id_type: lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
 public:
  FieldIdTypeCase field_id_type_case() const;
 protected:
  bool has_field_id_type() const;

 public:
  ::std::shared_ptr<ConstFieldId> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFieldId& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFieldId& other) const;
 protected:
  const ::std::shared_ptr<_FieldId_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FieldId_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFieldId
  void BuildFromProto(const PbMessage& proto_fieldid);
  
  ::std::shared_ptr<_FieldId_> data_;
};

class FieldId final : public ConstFieldId {
 public:
  FieldId(const ::std::shared_ptr<_FieldId_>& data);
  FieldId(const FieldId& other);
  // enable nothrow for ::std::vector<FieldId> resize 
  FieldId(FieldId&&) noexcept;
  FieldId();
  explicit FieldId(const ::oneflow::FieldId& proto_fieldid);

  ~FieldId() override;

  void InitFromProto(const PbMessage& proto_fieldid) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FieldId& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FieldId& other) const;
  void Clear();
  void CopyFrom(const FieldId& other);
  FieldId& operator=(const FieldId& other);

  void clear_key();
  void set_key(const ::oneflow::cfg::FieldKey& value);
  ::oneflow::cfg::FieldKey* mutable_key();
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();

  ::std::shared_ptr<FieldId> __SharedMutable__();
};


class ConstFieldPodProto : public ::oneflow::cfg::Message {
 public:

  class _FieldPodProto_ {
   public:
    _FieldPodProto_();
    explicit _FieldPodProto_(const _FieldPodProto_& other);
    explicit _FieldPodProto_(_FieldPodProto_&& other);
    _FieldPodProto_(const ::oneflow::FieldPodProto& proto_fieldpodproto);
    ~_FieldPodProto_();

    void InitFromProto(const ::oneflow::FieldPodProto& proto_fieldpodproto);

    void ToProto(::oneflow::FieldPodProto* proto_fieldpodproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FieldPodProto_& other);
  
      // optional field field_id
     public:
    bool has_field_id() const;
    const ::oneflow::cfg::FieldId& field_id() const;
    void clear_field_id();
    ::oneflow::cfg::FieldId* mutable_field_id();
   protected:
    bool has_field_id_ = false;
    ::std::shared_ptr<::oneflow::cfg::FieldId> field_id_;
      
      // optional field alignment
     public:
    bool has_alignment() const;
    const int32_t& alignment() const;
    void clear_alignment();
    void set_alignment(const int32_t& value);
    int32_t* mutable_alignment();
   protected:
    bool has_alignment_ = false;
    int32_t alignment_;
      
      // optional field pod
     public:
    bool has_pod() const;
    const ::oneflow::cfg::PodProto& pod() const;
    void clear_pod();
    ::oneflow::cfg::PodProto* mutable_pod();
   protected:
    bool has_pod_ = false;
    ::std::shared_ptr<::oneflow::cfg::PodProto> pod_;
           
   public:
    int compare(const _FieldPodProto_& other);

    bool operator==(const _FieldPodProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FieldPodProto_& other) const;
  };

  ConstFieldPodProto(const ::std::shared_ptr<_FieldPodProto_>& data);
  ConstFieldPodProto(const ConstFieldPodProto&);
  ConstFieldPodProto(ConstFieldPodProto&&) noexcept;
  ConstFieldPodProto();
  ConstFieldPodProto(const ::oneflow::FieldPodProto& proto_fieldpodproto);
  virtual ~ConstFieldPodProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_fieldpodproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field field_id
 public:
  bool has_field_id() const;
  const ::oneflow::cfg::FieldId& field_id() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFieldId> shared_const_field_id() const;
  // required or optional field alignment
 public:
  bool has_alignment() const;
  const int32_t& alignment() const;
  // used by pybind11 only
  // required or optional field pod
 public:
  bool has_pod() const;
  const ::oneflow::cfg::PodProto& pod() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPodProto> shared_const_pod() const;

 public:
  ::std::shared_ptr<ConstFieldPodProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFieldPodProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFieldPodProto& other) const;
 protected:
  const ::std::shared_ptr<_FieldPodProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FieldPodProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFieldPodProto
  void BuildFromProto(const PbMessage& proto_fieldpodproto);
  
  ::std::shared_ptr<_FieldPodProto_> data_;
};

class FieldPodProto final : public ConstFieldPodProto {
 public:
  FieldPodProto(const ::std::shared_ptr<_FieldPodProto_>& data);
  FieldPodProto(const FieldPodProto& other);
  // enable nothrow for ::std::vector<FieldPodProto> resize 
  FieldPodProto(FieldPodProto&&) noexcept;
  FieldPodProto();
  explicit FieldPodProto(const ::oneflow::FieldPodProto& proto_fieldpodproto);

  ~FieldPodProto() override;

  void InitFromProto(const PbMessage& proto_fieldpodproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FieldPodProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FieldPodProto& other) const;
  void Clear();
  void CopyFrom(const FieldPodProto& other);
  FieldPodProto& operator=(const FieldPodProto& other);

  // required or optional field field_id
 public:
  void clear_field_id();
  ::oneflow::cfg::FieldId* mutable_field_id();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FieldId> shared_mutable_field_id();
  // required or optional field alignment
 public:
  void clear_alignment();
  void set_alignment(const int32_t& value);
  int32_t* mutable_alignment();
  // required or optional field pod
 public:
  void clear_pod();
  ::oneflow::cfg::PodProto* mutable_pod();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PodProto> shared_mutable_pod();

  ::std::shared_ptr<FieldPodProto> __SharedMutable__();
};


class ConstPodProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum pod_type
 enum PodTypeCase : unsigned int {
  POD_TYPE_NOT_SET = 0,
    kTensorPod = 1,
    kStructPod = 2,
   };

  class _PodProto_ {
   public:
    _PodProto_();
    explicit _PodProto_(const _PodProto_& other);
    explicit _PodProto_(_PodProto_&& other);
    _PodProto_(const ::oneflow::PodProto& proto_podproto);
    ~_PodProto_();

    void InitFromProto(const ::oneflow::PodProto& proto_podproto);

    void ToProto(::oneflow::PodProto* proto_podproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PodProto_& other);
  
     // oneof field pod_type: tensor_pod
   public:
    bool has_tensor_pod() const;
    void clear_tensor_pod();
    const ::oneflow::cfg::TensorPodProto& tensor_pod() const;
      ::oneflow::cfg::TensorPodProto* mutable_tensor_pod();
      
     // oneof field pod_type: struct_pod
   public:
    bool has_struct_pod() const;
    void clear_struct_pod();
    const ::oneflow::cfg::StructPodProto& struct_pod() const;
      ::oneflow::cfg::StructPodProto* mutable_struct_pod();
           
   public:
    // oneof pod_type
    PodTypeCase pod_type_case() const;
    bool has_pod_type() const;
   protected:
    void clear_pod_type();
    void pod_type_copy_from(const _PodProto_& other);
    union PodTypeUnion {
      // 64-bit aligned
      uint64_t __pod_type_for_padding_64bit__;
          char tensor_pod_[sizeof(::std::shared_ptr<::oneflow::cfg::TensorPodProto>)];
            char struct_pod_[sizeof(::std::shared_ptr<::oneflow::cfg::StructPodProto>)];
        } pod_type_;
    PodTypeCase pod_type_case_ = POD_TYPE_NOT_SET;
     
   public:
    int compare(const _PodProto_& other);

    bool operator==(const _PodProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PodProto_& other) const;
  };

  ConstPodProto(const ::std::shared_ptr<_PodProto_>& data);
  ConstPodProto(const ConstPodProto&);
  ConstPodProto(ConstPodProto&&) noexcept;
  ConstPodProto();
  ConstPodProto(const ::oneflow::PodProto& proto_podproto);
  virtual ~ConstPodProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_podproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field pod_type: tensor_pod
 public:
  bool has_tensor_pod() const;
  const ::oneflow::cfg::TensorPodProto& tensor_pod() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTensorPodProto> shared_const_tensor_pod() const;
 // oneof field pod_type: struct_pod
 public:
  bool has_struct_pod() const;
  const ::oneflow::cfg::StructPodProto& struct_pod() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstStructPodProto> shared_const_struct_pod() const;
 public:
  PodTypeCase pod_type_case() const;
 protected:
  bool has_pod_type() const;

 public:
  ::std::shared_ptr<ConstPodProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPodProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPodProto& other) const;
 protected:
  const ::std::shared_ptr<_PodProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PodProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPodProto
  void BuildFromProto(const PbMessage& proto_podproto);
  
  ::std::shared_ptr<_PodProto_> data_;
};

class PodProto final : public ConstPodProto {
 public:
  PodProto(const ::std::shared_ptr<_PodProto_>& data);
  PodProto(const PodProto& other);
  // enable nothrow for ::std::vector<PodProto> resize 
  PodProto(PodProto&&) noexcept;
  PodProto();
  explicit PodProto(const ::oneflow::PodProto& proto_podproto);

  ~PodProto() override;

  void InitFromProto(const PbMessage& proto_podproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PodProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PodProto& other) const;
  void Clear();
  void CopyFrom(const PodProto& other);
  PodProto& operator=(const PodProto& other);

  void clear_tensor_pod();
  ::oneflow::cfg::TensorPodProto* mutable_tensor_pod();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TensorPodProto> shared_mutable_tensor_pod();
  void clear_struct_pod();
  ::oneflow::cfg::StructPodProto* mutable_struct_pod();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::StructPodProto> shared_mutable_struct_pod();

  ::std::shared_ptr<PodProto> __SharedMutable__();
};







// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::FieldPodProto> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::FieldPodProto>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstFieldPodProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::FieldPodProto>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_();
  ~_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::FieldPodProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::FieldPodProto> __SharedMutable__(::std::size_t index);
};












} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::FieldKey> {
  std::size_t operator()(::oneflow::cfg::FieldKey enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstTensorPodProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstTensorPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TensorPodProto> {
  std::size_t operator()(const ::oneflow::cfg::TensorPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstStructPodProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstStructPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::StructPodProto> {
  std::size_t operator()(const ::oneflow::cfg::StructPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFieldId> {
  std::size_t operator()(const ::oneflow::cfg::ConstFieldId& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FieldId> {
  std::size_t operator()(const ::oneflow::cfg::FieldId& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFieldPodProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstFieldPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FieldPodProto> {
  std::size_t operator()(const ::oneflow::cfg::FieldPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPodProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstPodProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PodProto> {
  std::size_t operator()(const ::oneflow::cfg::PodProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H_