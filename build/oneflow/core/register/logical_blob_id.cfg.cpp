#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/register/logical_blob_id.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstLogicalBlobId::_LogicalBlobId_::_LogicalBlobId_() { Clear(); }
ConstLogicalBlobId::_LogicalBlobId_::_LogicalBlobId_(const _LogicalBlobId_& other) { CopyFrom(other); }
ConstLogicalBlobId::_LogicalBlobId_::_LogicalBlobId_(const ::oneflow::LogicalBlobId& proto_logicalblobid) {
  InitFromProto(proto_logicalblobid);
}
ConstLogicalBlobId::_LogicalBlobId_::_LogicalBlobId_(_LogicalBlobId_&& other) = default;
ConstLogicalBlobId::_LogicalBlobId_::~_LogicalBlobId_() = default;

void ConstLogicalBlobId::_LogicalBlobId_::InitFromProto(const ::oneflow::LogicalBlobId& proto_logicalblobid) {
  Clear();
  // required_or_optional field: op_name
  if (proto_logicalblobid.has_op_name()) {
    set_op_name(proto_logicalblobid.op_name());
  }
  // required_or_optional field: blob_name
  if (proto_logicalblobid.has_blob_name()) {
    set_blob_name(proto_logicalblobid.blob_name());
  }
    
}

void ConstLogicalBlobId::_LogicalBlobId_::ToProto(::oneflow::LogicalBlobId* proto_logicalblobid) const {
  proto_logicalblobid->Clear();
  // required_or_optional field: op_name
  if (this->has_op_name()) {
    proto_logicalblobid->set_op_name(op_name());
    }
  // required_or_optional field: blob_name
  if (this->has_blob_name()) {
    proto_logicalblobid->set_blob_name(blob_name());
    }

}

::std::string ConstLogicalBlobId::_LogicalBlobId_::DebugString() const {
  ::oneflow::LogicalBlobId proto_logicalblobid;
  this->ToProto(&proto_logicalblobid);
  return proto_logicalblobid.DebugString();
}

void ConstLogicalBlobId::_LogicalBlobId_::Clear() {
  clear_op_name();
  clear_blob_name();
}

void ConstLogicalBlobId::_LogicalBlobId_::CopyFrom(const _LogicalBlobId_& other) {
  if (other.has_op_name()) {
    set_op_name(other.op_name());
  } else {
    clear_op_name();
  }
  if (other.has_blob_name()) {
    set_blob_name(other.blob_name());
  } else {
    clear_blob_name();
  }
}


// optional field op_name
bool ConstLogicalBlobId::_LogicalBlobId_::has_op_name() const {
  return has_op_name_;
}
const ::std::string& ConstLogicalBlobId::_LogicalBlobId_::op_name() const {
  if (has_op_name_) { return op_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstLogicalBlobId::_LogicalBlobId_::clear_op_name() {
  has_op_name_ = false;
}
void ConstLogicalBlobId::_LogicalBlobId_::set_op_name(const ::std::string& value) {
  op_name_ = value;
  has_op_name_ = true;
}
::std::string* ConstLogicalBlobId::_LogicalBlobId_::mutable_op_name() {
  has_op_name_ = true;
  return &op_name_;
}

// optional field blob_name
bool ConstLogicalBlobId::_LogicalBlobId_::has_blob_name() const {
  return has_blob_name_;
}
const ::std::string& ConstLogicalBlobId::_LogicalBlobId_::blob_name() const {
  if (has_blob_name_) { return blob_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstLogicalBlobId::_LogicalBlobId_::clear_blob_name() {
  has_blob_name_ = false;
}
void ConstLogicalBlobId::_LogicalBlobId_::set_blob_name(const ::std::string& value) {
  blob_name_ = value;
  has_blob_name_ = true;
}
::std::string* ConstLogicalBlobId::_LogicalBlobId_::mutable_blob_name() {
  has_blob_name_ = true;
  return &blob_name_;
}


int ConstLogicalBlobId::_LogicalBlobId_::compare(const _LogicalBlobId_& other) {
  if (!(has_op_name() == other.has_op_name())) {
    return has_op_name() < other.has_op_name() ? -1 : 1;
  } else if (!(op_name() == other.op_name())) {
    return op_name() < other.op_name() ? -1 : 1;
  }
  if (!(has_blob_name() == other.has_blob_name())) {
    return has_blob_name() < other.has_blob_name() ? -1 : 1;
  } else if (!(blob_name() == other.blob_name())) {
    return blob_name() < other.blob_name() ? -1 : 1;
  }
  return 0;
}

bool ConstLogicalBlobId::_LogicalBlobId_::operator==(const _LogicalBlobId_& other) const {
  return true
    && has_op_name() == other.has_op_name() 
    && op_name() == other.op_name()
    && has_blob_name() == other.has_blob_name() 
    && blob_name() == other.blob_name()
  ;
}

std::size_t ConstLogicalBlobId::_LogicalBlobId_::__CalcHash__() const {
  return 0
    ^ (has_op_name() ? std::hash<::std::string>()(op_name()) : 0)
    ^ (has_blob_name() ? std::hash<::std::string>()(blob_name()) : 0)
  ;
}

bool ConstLogicalBlobId::_LogicalBlobId_::operator<(const _LogicalBlobId_& other) const {
  return false
    || !(has_op_name() == other.has_op_name()) ? 
      has_op_name() < other.has_op_name() : false
    || !(op_name() == other.op_name()) ? 
      op_name() < other.op_name() : false
    || !(has_blob_name() == other.has_blob_name()) ? 
      has_blob_name() < other.has_blob_name() : false
    || !(blob_name() == other.blob_name()) ? 
      blob_name() < other.blob_name() : false
  ;
}

using _LogicalBlobId_ =  ConstLogicalBlobId::_LogicalBlobId_;
ConstLogicalBlobId::ConstLogicalBlobId(const ::std::shared_ptr<_LogicalBlobId_>& data): data_(data) {}
ConstLogicalBlobId::ConstLogicalBlobId(): data_(::std::make_shared<_LogicalBlobId_>()) {}
ConstLogicalBlobId::ConstLogicalBlobId(const ::oneflow::LogicalBlobId& proto_logicalblobid) {
  BuildFromProto(proto_logicalblobid);
}
ConstLogicalBlobId::ConstLogicalBlobId(const ConstLogicalBlobId&) = default;
ConstLogicalBlobId::ConstLogicalBlobId(ConstLogicalBlobId&&) noexcept = default;
ConstLogicalBlobId::~ConstLogicalBlobId() = default;

void ConstLogicalBlobId::ToProto(PbMessage* proto_logicalblobid) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobId*>(proto_logicalblobid));
}
  
::std::string ConstLogicalBlobId::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobId::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobId::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_name", 1},
    {"blob_name", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLogicalBlobId::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLogicalBlobId::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobId::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_name();
    case 2: return &blob_name();
    default: return nullptr;
  }
}

// required or optional field op_name
bool ConstLogicalBlobId::has_op_name() const {
  return __SharedPtrOrDefault__()->has_op_name();
}
const ::std::string& ConstLogicalBlobId::op_name() const {
  return __SharedPtrOrDefault__()->op_name();
}
// used by pybind11 only
// required or optional field blob_name
bool ConstLogicalBlobId::has_blob_name() const {
  return __SharedPtrOrDefault__()->has_blob_name();
}
const ::std::string& ConstLogicalBlobId::blob_name() const {
  return __SharedPtrOrDefault__()->blob_name();
}
// used by pybind11 only

::std::shared_ptr<ConstLogicalBlobId> ConstLogicalBlobId::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobId>(data_);
}
int64_t ConstLogicalBlobId::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobId::operator==(const ConstLogicalBlobId& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobId::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobId::operator<(const ConstLogicalBlobId& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobId_>& ConstLogicalBlobId::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobId_> default_ptr = std::make_shared<_LogicalBlobId_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobId_>& ConstLogicalBlobId::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobId_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobId
void ConstLogicalBlobId::BuildFromProto(const PbMessage& proto_logicalblobid) {
  data_ = ::std::make_shared<_LogicalBlobId_>(dynamic_cast<const ::oneflow::LogicalBlobId&>(proto_logicalblobid));
}

LogicalBlobId::LogicalBlobId(const ::std::shared_ptr<_LogicalBlobId_>& data)
  : ConstLogicalBlobId(data) {}
LogicalBlobId::LogicalBlobId(const LogicalBlobId& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobId> resize
LogicalBlobId::LogicalBlobId(LogicalBlobId&&) noexcept = default; 
LogicalBlobId::LogicalBlobId(const ::oneflow::LogicalBlobId& proto_logicalblobid) {
  InitFromProto(proto_logicalblobid);
}
LogicalBlobId::LogicalBlobId() = default;

LogicalBlobId::~LogicalBlobId() = default;

void LogicalBlobId::InitFromProto(const PbMessage& proto_logicalblobid) {
  BuildFromProto(proto_logicalblobid);
}
  
void* LogicalBlobId::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_name();
    case 2: return mutable_blob_name();
    default: return nullptr;
  }
}

bool LogicalBlobId::operator==(const LogicalBlobId& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobId::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobId::operator<(const LogicalBlobId& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobId::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobId::CopyFrom(const LogicalBlobId& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobId& LogicalBlobId::operator=(const LogicalBlobId& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_name
void LogicalBlobId::clear_op_name() {
  return __SharedPtr__()->clear_op_name();
}
void LogicalBlobId::set_op_name(const ::std::string& value) {
  return __SharedPtr__()->set_op_name(value);
}
::std::string* LogicalBlobId::mutable_op_name() {
  return  __SharedPtr__()->mutable_op_name();
}
// required or optional field blob_name
void LogicalBlobId::clear_blob_name() {
  return __SharedPtr__()->clear_blob_name();
}
void LogicalBlobId::set_blob_name(const ::std::string& value) {
  return __SharedPtr__()->set_blob_name(value);
}
::std::string* LogicalBlobId::mutable_blob_name() {
  return  __SharedPtr__()->mutable_blob_name();
}

::std::shared_ptr<LogicalBlobId> LogicalBlobId::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobId>(__SharedPtr__());
}
ConstLogicalBlobIdPair::_LogicalBlobIdPair_::_LogicalBlobIdPair_() { Clear(); }
ConstLogicalBlobIdPair::_LogicalBlobIdPair_::_LogicalBlobIdPair_(const _LogicalBlobIdPair_& other) { CopyFrom(other); }
ConstLogicalBlobIdPair::_LogicalBlobIdPair_::_LogicalBlobIdPair_(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair) {
  InitFromProto(proto_logicalblobidpair);
}
ConstLogicalBlobIdPair::_LogicalBlobIdPair_::_LogicalBlobIdPair_(_LogicalBlobIdPair_&& other) = default;
ConstLogicalBlobIdPair::_LogicalBlobIdPair_::~_LogicalBlobIdPair_() = default;

void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::InitFromProto(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair) {
  Clear();
  // required_or_optional field: first
  if (proto_logicalblobidpair.has_first()) {
  *mutable_first() = ::oneflow::cfg::LogicalBlobId(proto_logicalblobidpair.first());      
  }
  // required_or_optional field: second
  if (proto_logicalblobidpair.has_second()) {
  *mutable_second() = ::oneflow::cfg::LogicalBlobId(proto_logicalblobidpair.second());      
  }
    
}

void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::ToProto(::oneflow::LogicalBlobIdPair* proto_logicalblobidpair) const {
  proto_logicalblobidpair->Clear();
  // required_or_optional field: first
  if (this->has_first()) {
    ::oneflow::LogicalBlobId proto_first;
    first().ToProto(&proto_first);
    proto_logicalblobidpair->mutable_first()->CopyFrom(proto_first);
    }
  // required_or_optional field: second
  if (this->has_second()) {
    ::oneflow::LogicalBlobId proto_second;
    second().ToProto(&proto_second);
    proto_logicalblobidpair->mutable_second()->CopyFrom(proto_second);
    }

}

::std::string ConstLogicalBlobIdPair::_LogicalBlobIdPair_::DebugString() const {
  ::oneflow::LogicalBlobIdPair proto_logicalblobidpair;
  this->ToProto(&proto_logicalblobidpair);
  return proto_logicalblobidpair.DebugString();
}

void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::Clear() {
  clear_first();
  clear_second();
}

void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::CopyFrom(const _LogicalBlobIdPair_& other) {
  if (other.has_first()) {
    mutable_first()->CopyFrom(other.first());
  } else {
    clear_first();
  }
  if (other.has_second()) {
    mutable_second()->CopyFrom(other.second());
  } else {
    clear_second();
  }
}


// optional field first
bool ConstLogicalBlobIdPair::_LogicalBlobIdPair_::has_first() const {
  return has_first_;
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdPair::_LogicalBlobIdPair_::first() const {
  if (!first_) {
    static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> default_static_value =
      ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
    return *default_static_value;
  }
  return *(first_.get());
}
void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::clear_first() {
  if (first_) {
    first_->Clear();
  }
  has_first_ = false;
}
::oneflow::cfg::LogicalBlobId* ConstLogicalBlobIdPair::_LogicalBlobIdPair_::mutable_first() {
  if (!first_) {
    first_ = ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
  }
  has_first_ = true;
  return first_.get();
}

// optional field second
bool ConstLogicalBlobIdPair::_LogicalBlobIdPair_::has_second() const {
  return has_second_;
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdPair::_LogicalBlobIdPair_::second() const {
  if (!second_) {
    static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> default_static_value =
      ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
    return *default_static_value;
  }
  return *(second_.get());
}
void ConstLogicalBlobIdPair::_LogicalBlobIdPair_::clear_second() {
  if (second_) {
    second_->Clear();
  }
  has_second_ = false;
}
::oneflow::cfg::LogicalBlobId* ConstLogicalBlobIdPair::_LogicalBlobIdPair_::mutable_second() {
  if (!second_) {
    second_ = ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
  }
  has_second_ = true;
  return second_.get();
}


int ConstLogicalBlobIdPair::_LogicalBlobIdPair_::compare(const _LogicalBlobIdPair_& other) {
  if (!(has_first() == other.has_first())) {
    return has_first() < other.has_first() ? -1 : 1;
  } else if (!(first() == other.first())) {
    return first() < other.first() ? -1 : 1;
  }
  if (!(has_second() == other.has_second())) {
    return has_second() < other.has_second() ? -1 : 1;
  } else if (!(second() == other.second())) {
    return second() < other.second() ? -1 : 1;
  }
  return 0;
}

bool ConstLogicalBlobIdPair::_LogicalBlobIdPair_::operator==(const _LogicalBlobIdPair_& other) const {
  return true
    && has_first() == other.has_first() 
    && first() == other.first()
    && has_second() == other.has_second() 
    && second() == other.second()
  ;
}

std::size_t ConstLogicalBlobIdPair::_LogicalBlobIdPair_::__CalcHash__() const {
  return 0
    ^ (has_first() ? std::hash<::oneflow::cfg::LogicalBlobId>()(first()) : 0)
    ^ (has_second() ? std::hash<::oneflow::cfg::LogicalBlobId>()(second()) : 0)
  ;
}

bool ConstLogicalBlobIdPair::_LogicalBlobIdPair_::operator<(const _LogicalBlobIdPair_& other) const {
  return false
    || !(has_first() == other.has_first()) ? 
      has_first() < other.has_first() : false
    || !(first() == other.first()) ? 
      first() < other.first() : false
    || !(has_second() == other.has_second()) ? 
      has_second() < other.has_second() : false
    || !(second() == other.second()) ? 
      second() < other.second() : false
  ;
}

using _LogicalBlobIdPair_ =  ConstLogicalBlobIdPair::_LogicalBlobIdPair_;
ConstLogicalBlobIdPair::ConstLogicalBlobIdPair(const ::std::shared_ptr<_LogicalBlobIdPair_>& data): data_(data) {}
ConstLogicalBlobIdPair::ConstLogicalBlobIdPair(): data_(::std::make_shared<_LogicalBlobIdPair_>()) {}
ConstLogicalBlobIdPair::ConstLogicalBlobIdPair(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair) {
  BuildFromProto(proto_logicalblobidpair);
}
ConstLogicalBlobIdPair::ConstLogicalBlobIdPair(const ConstLogicalBlobIdPair&) = default;
ConstLogicalBlobIdPair::ConstLogicalBlobIdPair(ConstLogicalBlobIdPair&&) noexcept = default;
ConstLogicalBlobIdPair::~ConstLogicalBlobIdPair() = default;

void ConstLogicalBlobIdPair::ToProto(PbMessage* proto_logicalblobidpair) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobIdPair*>(proto_logicalblobidpair));
}
  
::std::string ConstLogicalBlobIdPair::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobIdPair::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobIdPair::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"first", 1},
    {"second", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLogicalBlobIdPair::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLogicalBlobIdPair::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobId),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobId),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobIdPair::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &first();
    case 2: return &second();
    default: return nullptr;
  }
}

// required or optional field first
bool ConstLogicalBlobIdPair::has_first() const {
  return __SharedPtrOrDefault__()->has_first();
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdPair::first() const {
  return __SharedPtrOrDefault__()->first();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstLogicalBlobIdPair::shared_const_first() const {
  return first().__SharedConst__();
}
// required or optional field second
bool ConstLogicalBlobIdPair::has_second() const {
  return __SharedPtrOrDefault__()->has_second();
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdPair::second() const {
  return __SharedPtrOrDefault__()->second();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstLogicalBlobIdPair::shared_const_second() const {
  return second().__SharedConst__();
}

::std::shared_ptr<ConstLogicalBlobIdPair> ConstLogicalBlobIdPair::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobIdPair>(data_);
}
int64_t ConstLogicalBlobIdPair::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobIdPair::operator==(const ConstLogicalBlobIdPair& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobIdPair::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobIdPair::operator<(const ConstLogicalBlobIdPair& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobIdPair_>& ConstLogicalBlobIdPair::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobIdPair_> default_ptr = std::make_shared<_LogicalBlobIdPair_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobIdPair_>& ConstLogicalBlobIdPair::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobIdPair_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdPair
void ConstLogicalBlobIdPair::BuildFromProto(const PbMessage& proto_logicalblobidpair) {
  data_ = ::std::make_shared<_LogicalBlobIdPair_>(dynamic_cast<const ::oneflow::LogicalBlobIdPair&>(proto_logicalblobidpair));
}

LogicalBlobIdPair::LogicalBlobIdPair(const ::std::shared_ptr<_LogicalBlobIdPair_>& data)
  : ConstLogicalBlobIdPair(data) {}
LogicalBlobIdPair::LogicalBlobIdPair(const LogicalBlobIdPair& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobIdPair> resize
LogicalBlobIdPair::LogicalBlobIdPair(LogicalBlobIdPair&&) noexcept = default; 
LogicalBlobIdPair::LogicalBlobIdPair(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair) {
  InitFromProto(proto_logicalblobidpair);
}
LogicalBlobIdPair::LogicalBlobIdPair() = default;

LogicalBlobIdPair::~LogicalBlobIdPair() = default;

void LogicalBlobIdPair::InitFromProto(const PbMessage& proto_logicalblobidpair) {
  BuildFromProto(proto_logicalblobidpair);
}
  
void* LogicalBlobIdPair::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_first();
    case 2: return mutable_second();
    default: return nullptr;
  }
}

bool LogicalBlobIdPair::operator==(const LogicalBlobIdPair& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobIdPair::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobIdPair::operator<(const LogicalBlobIdPair& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobIdPair::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobIdPair::CopyFrom(const LogicalBlobIdPair& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobIdPair& LogicalBlobIdPair::operator=(const LogicalBlobIdPair& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field first
void LogicalBlobIdPair::clear_first() {
  return __SharedPtr__()->clear_first();
}
::oneflow::cfg::LogicalBlobId* LogicalBlobIdPair::mutable_first() {
  return __SharedPtr__()->mutable_first();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> LogicalBlobIdPair::shared_mutable_first() {
  return mutable_first()->__SharedMutable__();
}
// required or optional field second
void LogicalBlobIdPair::clear_second() {
  return __SharedPtr__()->clear_second();
}
::oneflow::cfg::LogicalBlobId* LogicalBlobIdPair::mutable_second() {
  return __SharedPtr__()->mutable_second();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> LogicalBlobIdPair::shared_mutable_second() {
  return mutable_second()->__SharedMutable__();
}

::std::shared_ptr<LogicalBlobIdPair> LogicalBlobIdPair::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobIdPair>(__SharedPtr__());
}
ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::_LogicalBlobIdPairs_() { Clear(); }
ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::_LogicalBlobIdPairs_(const _LogicalBlobIdPairs_& other) { CopyFrom(other); }
ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::_LogicalBlobIdPairs_(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs) {
  InitFromProto(proto_logicalblobidpairs);
}
ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::_LogicalBlobIdPairs_(_LogicalBlobIdPairs_&& other) = default;
ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::~_LogicalBlobIdPairs_() = default;

void ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::InitFromProto(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs) {
  Clear();
  // repeated field: pair
  if (!proto_logicalblobidpairs.pair().empty()) {
    for (const ::oneflow::LogicalBlobIdPair& elem : proto_logicalblobidpairs.pair() ) {
      *mutable_pair()->Add() = ::oneflow::cfg::LogicalBlobIdPair(elem);
    }
  }
    
}

void ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::ToProto(::oneflow::LogicalBlobIdPairs* proto_logicalblobidpairs) const {
  proto_logicalblobidpairs->Clear();
  // repeated field: pair
  if (!pair().empty()) {
    for (const ::oneflow::cfg::LogicalBlobIdPair& elem : pair() ) {
      ::oneflow::LogicalBlobIdPair proto_pair_elem;
      elem.ToProto(&proto_pair_elem);
      *proto_logicalblobidpairs->mutable_pair()->Add() = proto_pair_elem;
    }
  }

}

::std::string ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::DebugString() const {
  ::oneflow::LogicalBlobIdPairs proto_logicalblobidpairs;
  this->ToProto(&proto_logicalblobidpairs);
  return proto_logicalblobidpairs.DebugString();
}

void ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::Clear() {
  clear_pair();
}

void ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::CopyFrom(const _LogicalBlobIdPairs_& other) {
  mutable_pair()->CopyFrom(other.pair());
}


// repeated field pair
::std::size_t ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::pair_size() const {
  if (!pair_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
    return default_static_value->size();
  }
  return pair_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::pair() const {
  if (!pair_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
    return *(default_static_value.get());
  }
  return *(pair_.get());
}
const ::oneflow::cfg::LogicalBlobIdPair& ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::pair(::std::size_t index) const {
  if (!pair_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
    return default_static_value->Get(index);
  }
  return pair_->Get(index);
}
void ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::clear_pair() {
  if (!pair_) {
    pair_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
  }
  return pair_->Clear();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_* ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::mutable_pair() {
  if (!pair_) {
    pair_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
  }
  return  pair_.get();
}
::oneflow::cfg::LogicalBlobIdPair* ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::mutable_pair(::std::size_t index) {
  if (!pair_) {
    pair_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
  }
  return  pair_->Mutable(index);
}
::oneflow::cfg::LogicalBlobIdPair* ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::add_pair() {
  if (!pair_) {
    pair_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>();
  }
  return pair_->Add();
}


int ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::compare(const _LogicalBlobIdPairs_& other) {
  if (!(pair() == other.pair())) {
    return pair() < other.pair() ? -1 : 1;
  }
  return 0;
}

bool ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::operator==(const _LogicalBlobIdPairs_& other) const {
  return true
    && pair() == other.pair()
  ;
}

std::size_t ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::__CalcHash__() const {
  return 0
    ^ pair().__CalcHash__()
  ;
}

bool ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_::operator<(const _LogicalBlobIdPairs_& other) const {
  return false
    || !(pair() == other.pair()) ? 
      pair() < other.pair() : false
  ;
}

using _LogicalBlobIdPairs_ =  ConstLogicalBlobIdPairs::_LogicalBlobIdPairs_;
ConstLogicalBlobIdPairs::ConstLogicalBlobIdPairs(const ::std::shared_ptr<_LogicalBlobIdPairs_>& data): data_(data) {}
ConstLogicalBlobIdPairs::ConstLogicalBlobIdPairs(): data_(::std::make_shared<_LogicalBlobIdPairs_>()) {}
ConstLogicalBlobIdPairs::ConstLogicalBlobIdPairs(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs) {
  BuildFromProto(proto_logicalblobidpairs);
}
ConstLogicalBlobIdPairs::ConstLogicalBlobIdPairs(const ConstLogicalBlobIdPairs&) = default;
ConstLogicalBlobIdPairs::ConstLogicalBlobIdPairs(ConstLogicalBlobIdPairs&&) noexcept = default;
ConstLogicalBlobIdPairs::~ConstLogicalBlobIdPairs() = default;

void ConstLogicalBlobIdPairs::ToProto(PbMessage* proto_logicalblobidpairs) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobIdPairs*>(proto_logicalblobidpairs));
}
  
::std::string ConstLogicalBlobIdPairs::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobIdPairs::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobIdPairs::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"pair", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLogicalBlobIdPairs::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLogicalBlobIdPairs::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdPair>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobIdPairs::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &pair();
    default: return nullptr;
  }
}

// repeated field pair
::std::size_t ConstLogicalBlobIdPairs::pair_size() const {
  return __SharedPtrOrDefault__()->pair_size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& ConstLogicalBlobIdPairs::pair() const {
  return __SharedPtrOrDefault__()->pair();
}
const ::oneflow::cfg::LogicalBlobIdPair& ConstLogicalBlobIdPairs::pair(::std::size_t index) const {
  return __SharedPtrOrDefault__()->pair(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> ConstLogicalBlobIdPairs::shared_const_pair() const {
  return pair().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdPair> ConstLogicalBlobIdPairs::shared_const_pair(::std::size_t index) const {
  return pair(index).__SharedConst__();
}

::std::shared_ptr<ConstLogicalBlobIdPairs> ConstLogicalBlobIdPairs::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobIdPairs>(data_);
}
int64_t ConstLogicalBlobIdPairs::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobIdPairs::operator==(const ConstLogicalBlobIdPairs& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobIdPairs::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobIdPairs::operator<(const ConstLogicalBlobIdPairs& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobIdPairs_>& ConstLogicalBlobIdPairs::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobIdPairs_> default_ptr = std::make_shared<_LogicalBlobIdPairs_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobIdPairs_>& ConstLogicalBlobIdPairs::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobIdPairs_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdPairs
void ConstLogicalBlobIdPairs::BuildFromProto(const PbMessage& proto_logicalblobidpairs) {
  data_ = ::std::make_shared<_LogicalBlobIdPairs_>(dynamic_cast<const ::oneflow::LogicalBlobIdPairs&>(proto_logicalblobidpairs));
}

LogicalBlobIdPairs::LogicalBlobIdPairs(const ::std::shared_ptr<_LogicalBlobIdPairs_>& data)
  : ConstLogicalBlobIdPairs(data) {}
LogicalBlobIdPairs::LogicalBlobIdPairs(const LogicalBlobIdPairs& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobIdPairs> resize
LogicalBlobIdPairs::LogicalBlobIdPairs(LogicalBlobIdPairs&&) noexcept = default; 
LogicalBlobIdPairs::LogicalBlobIdPairs(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs) {
  InitFromProto(proto_logicalblobidpairs);
}
LogicalBlobIdPairs::LogicalBlobIdPairs() = default;

LogicalBlobIdPairs::~LogicalBlobIdPairs() = default;

void LogicalBlobIdPairs::InitFromProto(const PbMessage& proto_logicalblobidpairs) {
  BuildFromProto(proto_logicalblobidpairs);
}
  
void* LogicalBlobIdPairs::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_pair();
    default: return nullptr;
  }
}

bool LogicalBlobIdPairs::operator==(const LogicalBlobIdPairs& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobIdPairs::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobIdPairs::operator<(const LogicalBlobIdPairs& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobIdPairs::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobIdPairs::CopyFrom(const LogicalBlobIdPairs& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobIdPairs& LogicalBlobIdPairs::operator=(const LogicalBlobIdPairs& other) {
  CopyFrom(other);
  return *this;
}

// repeated field pair
void LogicalBlobIdPairs::clear_pair() {
  return __SharedPtr__()->clear_pair();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_* LogicalBlobIdPairs::mutable_pair() {
  return __SharedPtr__()->mutable_pair();
}
::oneflow::cfg::LogicalBlobIdPair* LogicalBlobIdPairs::mutable_pair(::std::size_t index) {
  return __SharedPtr__()->mutable_pair(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> LogicalBlobIdPairs::shared_mutable_pair() {
  return mutable_pair()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> LogicalBlobIdPairs::shared_mutable_pair(::std::size_t index) {
  return mutable_pair(index)->__SharedMutable__();
}
::oneflow::cfg::LogicalBlobIdPair* LogicalBlobIdPairs::add_pair() {
  return __SharedPtr__()->add_pair();
}

::std::shared_ptr<LogicalBlobIdPairs> LogicalBlobIdPairs::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobIdPairs>(__SharedPtr__());
}
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::_LogicalBlobIdGroups_LogicalBlobIdGroup_() { Clear(); }
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::_LogicalBlobIdGroups_LogicalBlobIdGroup_(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) { CopyFrom(other); }
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::_LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup) {
  InitFromProto(proto_logicalblobidgroups_logicalblobidgroup);
}
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::_LogicalBlobIdGroups_LogicalBlobIdGroup_(_LogicalBlobIdGroups_LogicalBlobIdGroup_&& other) = default;
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::~_LogicalBlobIdGroups_LogicalBlobIdGroup_() = default;

void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::InitFromProto(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup) {
  Clear();
  // repeated field: lbi
  if (!proto_logicalblobidgroups_logicalblobidgroup.lbi().empty()) {
    for (const ::oneflow::LogicalBlobId& elem : proto_logicalblobidgroups_logicalblobidgroup.lbi() ) {
      *mutable_lbi()->Add() = ::oneflow::cfg::LogicalBlobId(elem);
    }
  }
    
}

void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::ToProto(::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup* proto_logicalblobidgroups_logicalblobidgroup) const {
  proto_logicalblobidgroups_logicalblobidgroup->Clear();
  // repeated field: lbi
  if (!lbi().empty()) {
    for (const ::oneflow::cfg::LogicalBlobId& elem : lbi() ) {
      ::oneflow::LogicalBlobId proto_lbi_elem;
      elem.ToProto(&proto_lbi_elem);
      *proto_logicalblobidgroups_logicalblobidgroup->mutable_lbi()->Add() = proto_lbi_elem;
    }
  }

}

::std::string ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::DebugString() const {
  ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup proto_logicalblobidgroups_logicalblobidgroup;
  this->ToProto(&proto_logicalblobidgroups_logicalblobidgroup);
  return proto_logicalblobidgroups_logicalblobidgroup.DebugString();
}

void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::Clear() {
  clear_lbi();
}

void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::CopyFrom(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) {
  mutable_lbi()->CopyFrom(other.lbi());
}


// repeated field lbi
::std::size_t ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::lbi_size() const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
    return default_static_value->size();
  }
  return lbi_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::lbi() const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
    return *(default_static_value.get());
  }
  return *(lbi_.get());
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::lbi(::std::size_t index) const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
    return default_static_value->Get(index);
  }
  return lbi_->Get(index);
}
void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::clear_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return lbi_->Clear();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_* ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::mutable_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return  lbi_.get();
}
::oneflow::cfg::LogicalBlobId* ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::mutable_lbi(::std::size_t index) {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return  lbi_->Mutable(index);
}
::oneflow::cfg::LogicalBlobId* ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::add_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return lbi_->Add();
}


int ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::compare(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) {
  if (!(lbi() == other.lbi())) {
    return lbi() < other.lbi() ? -1 : 1;
  }
  return 0;
}

bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator==(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return true
    && lbi() == other.lbi()
  ;
}

std::size_t ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::__CalcHash__() const {
  return 0
    ^ lbi().__CalcHash__()
  ;
}

bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator<(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return false
    || !(lbi() == other.lbi()) ? 
      lbi() < other.lbi() : false
  ;
}

using _LogicalBlobIdGroups_LogicalBlobIdGroup_ =  ConstLogicalBlobIdGroups_LogicalBlobIdGroup::_LogicalBlobIdGroups_LogicalBlobIdGroup_;
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& data): data_(data) {}
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ConstLogicalBlobIdGroups_LogicalBlobIdGroup(): data_(::std::make_shared<_LogicalBlobIdGroups_LogicalBlobIdGroup_>()) {}
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup) {
  BuildFromProto(proto_logicalblobidgroups_logicalblobidgroup);
}
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup&) = default;
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ConstLogicalBlobIdGroups_LogicalBlobIdGroup(ConstLogicalBlobIdGroups_LogicalBlobIdGroup&&) noexcept = default;
ConstLogicalBlobIdGroups_LogicalBlobIdGroup::~ConstLogicalBlobIdGroups_LogicalBlobIdGroup() = default;

void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ToProto(PbMessage* proto_logicalblobidgroups_logicalblobidgroup) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup*>(proto_logicalblobidgroups_logicalblobidgroup));
}
  
::std::string ConstLogicalBlobIdGroups_LogicalBlobIdGroup::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobIdGroups_LogicalBlobIdGroup::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"lbi", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobIdGroups_LogicalBlobIdGroup::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &lbi();
    default: return nullptr;
  }
}

// repeated field lbi
::std::size_t ConstLogicalBlobIdGroups_LogicalBlobIdGroup::lbi_size() const {
  return __SharedPtrOrDefault__()->lbi_size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::lbi() const {
  return __SharedPtrOrDefault__()->lbi();
}
const ::oneflow::cfg::LogicalBlobId& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::lbi(::std::size_t index) const {
  return __SharedPtrOrDefault__()->lbi(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> ConstLogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi() const {
  return lbi().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstLogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi(::std::size_t index) const {
  return lbi(index).__SharedConst__();
}

::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobIdGroups_LogicalBlobIdGroup>(data_);
}
int64_t ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::operator==(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobIdGroups_LogicalBlobIdGroup::operator<(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_> default_ptr = std::make_shared<_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& ConstLogicalBlobIdGroups_LogicalBlobIdGroup::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobIdGroups_LogicalBlobIdGroup_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdGroups_LogicalBlobIdGroup
void ConstLogicalBlobIdGroups_LogicalBlobIdGroup::BuildFromProto(const PbMessage& proto_logicalblobidgroups_logicalblobidgroup) {
  data_ = ::std::make_shared<_LogicalBlobIdGroups_LogicalBlobIdGroup_>(dynamic_cast<const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup&>(proto_logicalblobidgroups_logicalblobidgroup));
}

LogicalBlobIdGroups_LogicalBlobIdGroup::LogicalBlobIdGroups_LogicalBlobIdGroup(const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& data)
  : ConstLogicalBlobIdGroups_LogicalBlobIdGroup(data) {}
LogicalBlobIdGroups_LogicalBlobIdGroup::LogicalBlobIdGroups_LogicalBlobIdGroup(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobIdGroups_LogicalBlobIdGroup> resize
LogicalBlobIdGroups_LogicalBlobIdGroup::LogicalBlobIdGroups_LogicalBlobIdGroup(LogicalBlobIdGroups_LogicalBlobIdGroup&&) noexcept = default; 
LogicalBlobIdGroups_LogicalBlobIdGroup::LogicalBlobIdGroups_LogicalBlobIdGroup(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup) {
  InitFromProto(proto_logicalblobidgroups_logicalblobidgroup);
}
LogicalBlobIdGroups_LogicalBlobIdGroup::LogicalBlobIdGroups_LogicalBlobIdGroup() = default;

LogicalBlobIdGroups_LogicalBlobIdGroup::~LogicalBlobIdGroups_LogicalBlobIdGroup() = default;

void LogicalBlobIdGroups_LogicalBlobIdGroup::InitFromProto(const PbMessage& proto_logicalblobidgroups_logicalblobidgroup) {
  BuildFromProto(proto_logicalblobidgroups_logicalblobidgroup);
}
  
void* LogicalBlobIdGroups_LogicalBlobIdGroup::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_lbi();
    default: return nullptr;
  }
}

bool LogicalBlobIdGroups_LogicalBlobIdGroup::operator==(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobIdGroups_LogicalBlobIdGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobIdGroups_LogicalBlobIdGroup::operator<(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobIdGroups_LogicalBlobIdGroup::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobIdGroups_LogicalBlobIdGroup::CopyFrom(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobIdGroups_LogicalBlobIdGroup& LogicalBlobIdGroups_LogicalBlobIdGroup::operator=(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) {
  CopyFrom(other);
  return *this;
}

// repeated field lbi
void LogicalBlobIdGroups_LogicalBlobIdGroup::clear_lbi() {
  return __SharedPtr__()->clear_lbi();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_* LogicalBlobIdGroups_LogicalBlobIdGroup::mutable_lbi() {
  return __SharedPtr__()->mutable_lbi();
}
::oneflow::cfg::LogicalBlobId* LogicalBlobIdGroups_LogicalBlobIdGroup::mutable_lbi(::std::size_t index) {
  return __SharedPtr__()->mutable_lbi(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> LogicalBlobIdGroups_LogicalBlobIdGroup::shared_mutable_lbi() {
  return mutable_lbi()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> LogicalBlobIdGroups_LogicalBlobIdGroup::shared_mutable_lbi(::std::size_t index) {
  return mutable_lbi(index)->__SharedMutable__();
}
::oneflow::cfg::LogicalBlobId* LogicalBlobIdGroups_LogicalBlobIdGroup::add_lbi() {
  return __SharedPtr__()->add_lbi();
}

::std::shared_ptr<LogicalBlobIdGroups_LogicalBlobIdGroup> LogicalBlobIdGroups_LogicalBlobIdGroup::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobIdGroups_LogicalBlobIdGroup>(__SharedPtr__());
}
ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::_LogicalBlobIdGroups_() { Clear(); }
ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::_LogicalBlobIdGroups_(const _LogicalBlobIdGroups_& other) { CopyFrom(other); }
ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::_LogicalBlobIdGroups_(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups) {
  InitFromProto(proto_logicalblobidgroups);
}
ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::_LogicalBlobIdGroups_(_LogicalBlobIdGroups_&& other) = default;
ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::~_LogicalBlobIdGroups_() = default;

void ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::InitFromProto(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups) {
  Clear();
  // repeated field: lbi_group
  if (!proto_logicalblobidgroups.lbi_group().empty()) {
    for (const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& elem : proto_logicalblobidgroups.lbi_group() ) {
      *mutable_lbi_group()->Add() = ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup(elem);
    }
  }
    
}

void ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::ToProto(::oneflow::LogicalBlobIdGroups* proto_logicalblobidgroups) const {
  proto_logicalblobidgroups->Clear();
  // repeated field: lbi_group
  if (!lbi_group().empty()) {
    for (const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& elem : lbi_group() ) {
      ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup proto_lbi_group_elem;
      elem.ToProto(&proto_lbi_group_elem);
      *proto_logicalblobidgroups->mutable_lbi_group()->Add() = proto_lbi_group_elem;
    }
  }

}

::std::string ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::DebugString() const {
  ::oneflow::LogicalBlobIdGroups proto_logicalblobidgroups;
  this->ToProto(&proto_logicalblobidgroups);
  return proto_logicalblobidgroups.DebugString();
}

void ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::Clear() {
  clear_lbi_group();
}

void ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::CopyFrom(const _LogicalBlobIdGroups_& other) {
  mutable_lbi_group()->CopyFrom(other.lbi_group());
}


// repeated field lbi_group
::std::size_t ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::lbi_group_size() const {
  if (!lbi_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
    return default_static_value->size();
  }
  return lbi_group_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::lbi_group() const {
  if (!lbi_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
    return *(default_static_value.get());
  }
  return *(lbi_group_.get());
}
const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::lbi_group(::std::size_t index) const {
  if (!lbi_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
    return default_static_value->Get(index);
  }
  return lbi_group_->Get(index);
}
void ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::clear_lbi_group() {
  if (!lbi_group_) {
    lbi_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
  }
  return lbi_group_->Clear();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_* ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::mutable_lbi_group() {
  if (!lbi_group_) {
    lbi_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
  }
  return  lbi_group_.get();
}
::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::mutable_lbi_group(::std::size_t index) {
  if (!lbi_group_) {
    lbi_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
  }
  return  lbi_group_->Mutable(index);
}
::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::add_lbi_group() {
  if (!lbi_group_) {
    lbi_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>();
  }
  return lbi_group_->Add();
}


int ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::compare(const _LogicalBlobIdGroups_& other) {
  if (!(lbi_group() == other.lbi_group())) {
    return lbi_group() < other.lbi_group() ? -1 : 1;
  }
  return 0;
}

bool ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::operator==(const _LogicalBlobIdGroups_& other) const {
  return true
    && lbi_group() == other.lbi_group()
  ;
}

std::size_t ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::__CalcHash__() const {
  return 0
    ^ lbi_group().__CalcHash__()
  ;
}

bool ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_::operator<(const _LogicalBlobIdGroups_& other) const {
  return false
    || !(lbi_group() == other.lbi_group()) ? 
      lbi_group() < other.lbi_group() : false
  ;
}

using _LogicalBlobIdGroups_ =  ConstLogicalBlobIdGroups::_LogicalBlobIdGroups_;
ConstLogicalBlobIdGroups::ConstLogicalBlobIdGroups(const ::std::shared_ptr<_LogicalBlobIdGroups_>& data): data_(data) {}
ConstLogicalBlobIdGroups::ConstLogicalBlobIdGroups(): data_(::std::make_shared<_LogicalBlobIdGroups_>()) {}
ConstLogicalBlobIdGroups::ConstLogicalBlobIdGroups(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups) {
  BuildFromProto(proto_logicalblobidgroups);
}
ConstLogicalBlobIdGroups::ConstLogicalBlobIdGroups(const ConstLogicalBlobIdGroups&) = default;
ConstLogicalBlobIdGroups::ConstLogicalBlobIdGroups(ConstLogicalBlobIdGroups&&) noexcept = default;
ConstLogicalBlobIdGroups::~ConstLogicalBlobIdGroups() = default;

void ConstLogicalBlobIdGroups::ToProto(PbMessage* proto_logicalblobidgroups) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LogicalBlobIdGroups*>(proto_logicalblobidgroups));
}
  
::std::string ConstLogicalBlobIdGroups::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLogicalBlobIdGroups::__Empty__() const {
  return !data_;
}

int ConstLogicalBlobIdGroups::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"lbi_group", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLogicalBlobIdGroups::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLogicalBlobIdGroups::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLogicalBlobIdGroups::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 2: return &lbi_group();
    default: return nullptr;
  }
}

// repeated field lbi_group
::std::size_t ConstLogicalBlobIdGroups::lbi_group_size() const {
  return __SharedPtrOrDefault__()->lbi_group_size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& ConstLogicalBlobIdGroups::lbi_group() const {
  return __SharedPtrOrDefault__()->lbi_group();
}
const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& ConstLogicalBlobIdGroups::lbi_group(::std::size_t index) const {
  return __SharedPtrOrDefault__()->lbi_group(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> ConstLogicalBlobIdGroups::shared_const_lbi_group() const {
  return lbi_group().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup> ConstLogicalBlobIdGroups::shared_const_lbi_group(::std::size_t index) const {
  return lbi_group(index).__SharedConst__();
}

::std::shared_ptr<ConstLogicalBlobIdGroups> ConstLogicalBlobIdGroups::__SharedConst__() const {
  return ::std::make_shared<ConstLogicalBlobIdGroups>(data_);
}
int64_t ConstLogicalBlobIdGroups::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLogicalBlobIdGroups::operator==(const ConstLogicalBlobIdGroups& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLogicalBlobIdGroups::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLogicalBlobIdGroups::operator<(const ConstLogicalBlobIdGroups& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LogicalBlobIdGroups_>& ConstLogicalBlobIdGroups::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LogicalBlobIdGroups_> default_ptr = std::make_shared<_LogicalBlobIdGroups_>();
  return default_ptr;
}
const ::std::shared_ptr<_LogicalBlobIdGroups_>& ConstLogicalBlobIdGroups::__SharedPtr__() {
  if (!data_) { data_.reset(new _LogicalBlobIdGroups_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdGroups
void ConstLogicalBlobIdGroups::BuildFromProto(const PbMessage& proto_logicalblobidgroups) {
  data_ = ::std::make_shared<_LogicalBlobIdGroups_>(dynamic_cast<const ::oneflow::LogicalBlobIdGroups&>(proto_logicalblobidgroups));
}

LogicalBlobIdGroups::LogicalBlobIdGroups(const ::std::shared_ptr<_LogicalBlobIdGroups_>& data)
  : ConstLogicalBlobIdGroups(data) {}
LogicalBlobIdGroups::LogicalBlobIdGroups(const LogicalBlobIdGroups& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LogicalBlobIdGroups> resize
LogicalBlobIdGroups::LogicalBlobIdGroups(LogicalBlobIdGroups&&) noexcept = default; 
LogicalBlobIdGroups::LogicalBlobIdGroups(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups) {
  InitFromProto(proto_logicalblobidgroups);
}
LogicalBlobIdGroups::LogicalBlobIdGroups() = default;

LogicalBlobIdGroups::~LogicalBlobIdGroups() = default;

void LogicalBlobIdGroups::InitFromProto(const PbMessage& proto_logicalblobidgroups) {
  BuildFromProto(proto_logicalblobidgroups);
}
  
void* LogicalBlobIdGroups::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 2: return mutable_lbi_group();
    default: return nullptr;
  }
}

bool LogicalBlobIdGroups::operator==(const LogicalBlobIdGroups& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LogicalBlobIdGroups::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LogicalBlobIdGroups::operator<(const LogicalBlobIdGroups& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LogicalBlobIdGroups::Clear() {
  if (data_) { data_.reset(); }
}
void LogicalBlobIdGroups::CopyFrom(const LogicalBlobIdGroups& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LogicalBlobIdGroups& LogicalBlobIdGroups::operator=(const LogicalBlobIdGroups& other) {
  CopyFrom(other);
  return *this;
}

// repeated field lbi_group
void LogicalBlobIdGroups::clear_lbi_group() {
  return __SharedPtr__()->clear_lbi_group();
}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_* LogicalBlobIdGroups::mutable_lbi_group() {
  return __SharedPtr__()->mutable_lbi_group();
}
::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* LogicalBlobIdGroups::mutable_lbi_group(::std::size_t index) {
  return __SharedPtr__()->mutable_lbi_group(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> LogicalBlobIdGroups::shared_mutable_lbi_group() {
  return mutable_lbi_group()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> LogicalBlobIdGroups::shared_mutable_lbi_group(::std::size_t index) {
  return mutable_lbi_group(index)->__SharedMutable__();
}
::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* LogicalBlobIdGroups::add_lbi_group() {
  return __SharedPtr__()->add_lbi_group();
}

::std::shared_ptr<LogicalBlobIdGroups> LogicalBlobIdGroups::__SharedMutable__() {
  return ::std::make_shared<LogicalBlobIdGroups>(__SharedPtr__());
}
ConstArgSignature::_ArgSignature_::_ArgSignature_() { Clear(); }
ConstArgSignature::_ArgSignature_::_ArgSignature_(const _ArgSignature_& other) { CopyFrom(other); }
ConstArgSignature::_ArgSignature_::_ArgSignature_(const ::oneflow::ArgSignature& proto_argsignature) {
  InitFromProto(proto_argsignature);
}
ConstArgSignature::_ArgSignature_::_ArgSignature_(_ArgSignature_&& other) = default;
ConstArgSignature::_ArgSignature_::~_ArgSignature_() = default;

void ConstArgSignature::_ArgSignature_::InitFromProto(const ::oneflow::ArgSignature& proto_argsignature) {
  Clear();
  // map field : bn_in_op2lbi
  if (!proto_argsignature.bn_in_op2lbi().empty()) {
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_&  mut_bn_in_op2lbi = *mutable_bn_in_op2lbi();
    for (const auto& pair : proto_argsignature.bn_in_op2lbi()) {
      mut_bn_in_op2lbi[pair.first] = ::oneflow::cfg::LogicalBlobId(pair.second);
    }
  }
    
}

void ConstArgSignature::_ArgSignature_::ToProto(::oneflow::ArgSignature* proto_argsignature) const {
  proto_argsignature->Clear();
  // map field : bn_in_op2lbi
  if (!bn_in_op2lbi().empty()) {
    auto& mut_bn_in_op2lbi = *(proto_argsignature->mutable_bn_in_op2lbi());
    for (const auto& pair : bn_in_op2lbi()) {
      ::oneflow::LogicalBlobId proto_bn_in_op2lbi_value;
      pair.second.ToProto(&proto_bn_in_op2lbi_value);
      mut_bn_in_op2lbi[pair.first] = proto_bn_in_op2lbi_value;
    }
  }

}

::std::string ConstArgSignature::_ArgSignature_::DebugString() const {
  ::oneflow::ArgSignature proto_argsignature;
  this->ToProto(&proto_argsignature);
  return proto_argsignature.DebugString();
}

void ConstArgSignature::_ArgSignature_::Clear() {
  clear_bn_in_op2lbi();
}

void ConstArgSignature::_ArgSignature_::CopyFrom(const _ArgSignature_& other) {
  mutable_bn_in_op2lbi()->CopyFrom(other.bn_in_op2lbi());
}


::std::size_t ConstArgSignature::_ArgSignature_::bn_in_op2lbi_size() const {
  if (!bn_in_op2lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>();
    return default_static_value->size();
  }
  return bn_in_op2lbi_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& ConstArgSignature::_ArgSignature_::bn_in_op2lbi() const {
  if (!bn_in_op2lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2lbi_.get());
}

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ * ConstArgSignature::_ArgSignature_::mutable_bn_in_op2lbi() {
  if (!bn_in_op2lbi_) {
    bn_in_op2lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>();
  }
  return bn_in_op2lbi_.get();
}

const ::oneflow::cfg::LogicalBlobId& ConstArgSignature::_ArgSignature_::bn_in_op2lbi(::std::string key) const {
  if (!bn_in_op2lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>();
    return default_static_value->at(key);
  }
  return bn_in_op2lbi_->at(key);
}

void ConstArgSignature::_ArgSignature_::clear_bn_in_op2lbi() {
  if (!bn_in_op2lbi_) {
    bn_in_op2lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>();
  }
  return bn_in_op2lbi_->Clear();
}



int ConstArgSignature::_ArgSignature_::compare(const _ArgSignature_& other) {
  if (!(bn_in_op2lbi() == other.bn_in_op2lbi())) {
    return bn_in_op2lbi() < other.bn_in_op2lbi() ? -1 : 1;
  }
  return 0;
}

bool ConstArgSignature::_ArgSignature_::operator==(const _ArgSignature_& other) const {
  return true
    && bn_in_op2lbi() == other.bn_in_op2lbi()
  ;
}

std::size_t ConstArgSignature::_ArgSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2lbi().__CalcHash__()
  ;
}

bool ConstArgSignature::_ArgSignature_::operator<(const _ArgSignature_& other) const {
  return false
    || !(bn_in_op2lbi() == other.bn_in_op2lbi()) ? 
      bn_in_op2lbi() < other.bn_in_op2lbi() : false
  ;
}

using _ArgSignature_ =  ConstArgSignature::_ArgSignature_;
ConstArgSignature::ConstArgSignature(const ::std::shared_ptr<_ArgSignature_>& data): data_(data) {}
ConstArgSignature::ConstArgSignature(): data_(::std::make_shared<_ArgSignature_>()) {}
ConstArgSignature::ConstArgSignature(const ::oneflow::ArgSignature& proto_argsignature) {
  BuildFromProto(proto_argsignature);
}
ConstArgSignature::ConstArgSignature(const ConstArgSignature&) = default;
ConstArgSignature::ConstArgSignature(ConstArgSignature&&) noexcept = default;
ConstArgSignature::~ConstArgSignature() = default;

void ConstArgSignature::ToProto(PbMessage* proto_argsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ArgSignature*>(proto_argsignature));
}
  
::std::string ConstArgSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstArgSignature::__Empty__() const {
  return !data_;
}

int ConstArgSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2lbi", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstArgSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstArgSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::LogicalBlobId>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstArgSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2lbi();
    default: return nullptr;
  }
}

// map field bn_in_op2lbi
::std::size_t ConstArgSignature::bn_in_op2lbi_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2lbi_size();
}

const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& ConstArgSignature::bn_in_op2lbi() const {
  return __SharedPtrOrDefault__()->bn_in_op2lbi();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> ConstArgSignature::shared_const_bn_in_op2lbi() const {
  return bn_in_op2lbi().__SharedConst__();
}

::std::shared_ptr<ConstArgSignature> ConstArgSignature::__SharedConst__() const {
  return ::std::make_shared<ConstArgSignature>(data_);
}
int64_t ConstArgSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstArgSignature::operator==(const ConstArgSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstArgSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstArgSignature::operator<(const ConstArgSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ArgSignature_>& ConstArgSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ArgSignature_> default_ptr = std::make_shared<_ArgSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ArgSignature_>& ConstArgSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ArgSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstArgSignature
void ConstArgSignature::BuildFromProto(const PbMessage& proto_argsignature) {
  data_ = ::std::make_shared<_ArgSignature_>(dynamic_cast<const ::oneflow::ArgSignature&>(proto_argsignature));
}

ArgSignature::ArgSignature(const ::std::shared_ptr<_ArgSignature_>& data)
  : ConstArgSignature(data) {}
ArgSignature::ArgSignature(const ArgSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ArgSignature> resize
ArgSignature::ArgSignature(ArgSignature&&) noexcept = default; 
ArgSignature::ArgSignature(const ::oneflow::ArgSignature& proto_argsignature) {
  InitFromProto(proto_argsignature);
}
ArgSignature::ArgSignature() = default;

ArgSignature::~ArgSignature() = default;

void ArgSignature::InitFromProto(const PbMessage& proto_argsignature) {
  BuildFromProto(proto_argsignature);
}
  
void* ArgSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2lbi();
    default: return nullptr;
  }
}

bool ArgSignature::operator==(const ArgSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ArgSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ArgSignature::operator<(const ArgSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ArgSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ArgSignature::CopyFrom(const ArgSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ArgSignature& ArgSignature::operator=(const ArgSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2lbi
void ArgSignature::clear_bn_in_op2lbi() {
  return __SharedPtr__()->clear_bn_in_op2lbi();
}

const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ & ArgSignature::bn_in_op2lbi() const {
  return __SharedConst__()->bn_in_op2lbi();
}

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_* ArgSignature::mutable_bn_in_op2lbi() {
  return __SharedPtr__()->mutable_bn_in_op2lbi();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> ArgSignature::shared_mutable_bn_in_op2lbi() {
  return mutable_bn_in_op2lbi()->__SharedMutable__();
}

::std::shared_ptr<ArgSignature> ArgSignature::__SharedMutable__() {
  return ::std::make_shared<ArgSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdPair>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdPair>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_() = default;


bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobIdPair>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdPair> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdPair>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_() = default;
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdPair>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdPair>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobIdPair>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_() = default;


bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_() = default;
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_() = default;


bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_() = default;
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::LogicalBlobId>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::LogicalBlobId>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_() = default;

bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::LogicalBlobId& Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstLogicalBlobId> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ConstLogicalBlobId> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ConstLogicalBlobId> Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::LogicalBlobId>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_() = default;
_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::shared_mut_end() { return end(); }

}
} // namespace oneflow
