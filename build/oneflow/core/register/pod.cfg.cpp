#include "oneflow/core/register/pod.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/register/pod.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstTensorPodProto::_TensorPodProto_::_TensorPodProto_() { Clear(); }
ConstTensorPodProto::_TensorPodProto_::_TensorPodProto_(const _TensorPodProto_& other) { CopyFrom(other); }
ConstTensorPodProto::_TensorPodProto_::_TensorPodProto_(const ::oneflow::TensorPodProto& proto_tensorpodproto) {
  InitFromProto(proto_tensorpodproto);
}
ConstTensorPodProto::_TensorPodProto_::_TensorPodProto_(_TensorPodProto_&& other) = default;
ConstTensorPodProto::_TensorPodProto_::~_TensorPodProto_() = default;

void ConstTensorPodProto::_TensorPodProto_::InitFromProto(const ::oneflow::TensorPodProto& proto_tensorpodproto) {
  Clear();
  // required_or_optional field: shape
  if (proto_tensorpodproto.has_shape()) {
  *mutable_shape() = ::oneflow::cfg::ShapeProto(proto_tensorpodproto.shape());      
  }
  // required_or_optional field: data_type
  if (proto_tensorpodproto.has_data_type()) {
  set_data_type(static_cast<std::remove_reference<std::remove_const<decltype(data_type())>::type>::type>(proto_tensorpodproto.data_type()));
  }
    
}

void ConstTensorPodProto::_TensorPodProto_::ToProto(::oneflow::TensorPodProto* proto_tensorpodproto) const {
  proto_tensorpodproto->Clear();
  // required_or_optional field: shape
  if (this->has_shape()) {
    ::oneflow::ShapeProto proto_shape;
    shape().ToProto(&proto_shape);
    proto_tensorpodproto->mutable_shape()->CopyFrom(proto_shape);
    }
  // required_or_optional field: data_type
  if (this->has_data_type()) {
    proto_tensorpodproto->set_data_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_tensorpodproto->data_type())>::type>::type>(data_type()));
    }

}

::std::string ConstTensorPodProto::_TensorPodProto_::DebugString() const {
  ::oneflow::TensorPodProto proto_tensorpodproto;
  this->ToProto(&proto_tensorpodproto);
  return proto_tensorpodproto.DebugString();
}

void ConstTensorPodProto::_TensorPodProto_::Clear() {
  clear_shape();
  clear_data_type();
}

void ConstTensorPodProto::_TensorPodProto_::CopyFrom(const _TensorPodProto_& other) {
  if (other.has_shape()) {
    mutable_shape()->CopyFrom(other.shape());
  } else {
    clear_shape();
  }
  if (other.has_data_type()) {
    set_data_type(other.data_type());
  } else {
    clear_data_type();
  }
}


// optional field shape
bool ConstTensorPodProto::_TensorPodProto_::has_shape() const {
  return has_shape_;
}
const ::oneflow::cfg::ShapeProto& ConstTensorPodProto::_TensorPodProto_::shape() const {
  if (!shape_) {
    static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
  }
  return *(shape_.get());
}
void ConstTensorPodProto::_TensorPodProto_::clear_shape() {
  if (shape_) {
    shape_->Clear();
  }
  has_shape_ = false;
}
::oneflow::cfg::ShapeProto* ConstTensorPodProto::_TensorPodProto_::mutable_shape() {
  if (!shape_) {
    shape_ = ::std::make_shared<::oneflow::cfg::ShapeProto>();
  }
  has_shape_ = true;
  return shape_.get();
}

// optional field data_type
bool ConstTensorPodProto::_TensorPodProto_::has_data_type() const {
  return has_data_type_;
}
const ::oneflow::cfg::DataType& ConstTensorPodProto::_TensorPodProto_::data_type() const {
  if (has_data_type_) { return data_type_; }
  static const ::oneflow::cfg::DataType default_static_value = ::oneflow::cfg::DataType();
  return default_static_value;
}
void ConstTensorPodProto::_TensorPodProto_::clear_data_type() {
  has_data_type_ = false;
}
void ConstTensorPodProto::_TensorPodProto_::set_data_type(const ::oneflow::cfg::DataType& value) {
  data_type_ = value;
  has_data_type_ = true;
}
::oneflow::cfg::DataType* ConstTensorPodProto::_TensorPodProto_::mutable_data_type() {
  has_data_type_ = true;
  return &data_type_;
}


int ConstTensorPodProto::_TensorPodProto_::compare(const _TensorPodProto_& other) {
  if (!(has_shape() == other.has_shape())) {
    return has_shape() < other.has_shape() ? -1 : 1;
  } else if (!(shape() == other.shape())) {
    return shape() < other.shape() ? -1 : 1;
  }
  if (!(has_data_type() == other.has_data_type())) {
    return has_data_type() < other.has_data_type() ? -1 : 1;
  } else if (!(data_type() == other.data_type())) {
    return data_type() < other.data_type() ? -1 : 1;
  }
  return 0;
}

bool ConstTensorPodProto::_TensorPodProto_::operator==(const _TensorPodProto_& other) const {
  return true
    && has_shape() == other.has_shape() 
    && shape() == other.shape()
    && has_data_type() == other.has_data_type() 
    && data_type() == other.data_type()
  ;
}

std::size_t ConstTensorPodProto::_TensorPodProto_::__CalcHash__() const {
  return 0
    ^ (has_shape() ? std::hash<::oneflow::cfg::ShapeProto>()(shape()) : 0)
    ^ (has_data_type() ? std::hash<::oneflow::cfg::DataType>()(data_type()) : 0)
  ;
}

bool ConstTensorPodProto::_TensorPodProto_::operator<(const _TensorPodProto_& other) const {
  return false
    || !(has_shape() == other.has_shape()) ? 
      has_shape() < other.has_shape() : false
    || !(shape() == other.shape()) ? 
      shape() < other.shape() : false
    || !(has_data_type() == other.has_data_type()) ? 
      has_data_type() < other.has_data_type() : false
    || !(data_type() == other.data_type()) ? 
      data_type() < other.data_type() : false
  ;
}

using _TensorPodProto_ =  ConstTensorPodProto::_TensorPodProto_;
ConstTensorPodProto::ConstTensorPodProto(const ::std::shared_ptr<_TensorPodProto_>& data): data_(data) {}
ConstTensorPodProto::ConstTensorPodProto(): data_(::std::make_shared<_TensorPodProto_>()) {}
ConstTensorPodProto::ConstTensorPodProto(const ::oneflow::TensorPodProto& proto_tensorpodproto) {
  BuildFromProto(proto_tensorpodproto);
}
ConstTensorPodProto::ConstTensorPodProto(const ConstTensorPodProto&) = default;
ConstTensorPodProto::ConstTensorPodProto(ConstTensorPodProto&&) noexcept = default;
ConstTensorPodProto::~ConstTensorPodProto() = default;

void ConstTensorPodProto::ToProto(PbMessage* proto_tensorpodproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TensorPodProto*>(proto_tensorpodproto));
}
  
::std::string ConstTensorPodProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTensorPodProto::__Empty__() const {
  return !data_;
}

int ConstTensorPodProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"shape", 1},
    {"data_type", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstTensorPodProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstTensorPodProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTensorPodProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &shape();
    case 2: return &data_type();
    default: return nullptr;
  }
}

// required or optional field shape
bool ConstTensorPodProto::has_shape() const {
  return __SharedPtrOrDefault__()->has_shape();
}
const ::oneflow::cfg::ShapeProto& ConstTensorPodProto::shape() const {
  return __SharedPtrOrDefault__()->shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstTensorPodProto::shared_const_shape() const {
  return shape().__SharedConst__();
}
// required or optional field data_type
bool ConstTensorPodProto::has_data_type() const {
  return __SharedPtrOrDefault__()->has_data_type();
}
const ::oneflow::cfg::DataType& ConstTensorPodProto::data_type() const {
  return __SharedPtrOrDefault__()->data_type();
}
// used by pybind11 only

::std::shared_ptr<ConstTensorPodProto> ConstTensorPodProto::__SharedConst__() const {
  return ::std::make_shared<ConstTensorPodProto>(data_);
}
int64_t ConstTensorPodProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTensorPodProto::operator==(const ConstTensorPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTensorPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTensorPodProto::operator<(const ConstTensorPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TensorPodProto_>& ConstTensorPodProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TensorPodProto_> default_ptr = std::make_shared<_TensorPodProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_TensorPodProto_>& ConstTensorPodProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _TensorPodProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTensorPodProto
void ConstTensorPodProto::BuildFromProto(const PbMessage& proto_tensorpodproto) {
  data_ = ::std::make_shared<_TensorPodProto_>(dynamic_cast<const ::oneflow::TensorPodProto&>(proto_tensorpodproto));
}

TensorPodProto::TensorPodProto(const ::std::shared_ptr<_TensorPodProto_>& data)
  : ConstTensorPodProto(data) {}
TensorPodProto::TensorPodProto(const TensorPodProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TensorPodProto> resize
TensorPodProto::TensorPodProto(TensorPodProto&&) noexcept = default; 
TensorPodProto::TensorPodProto(const ::oneflow::TensorPodProto& proto_tensorpodproto) {
  InitFromProto(proto_tensorpodproto);
}
TensorPodProto::TensorPodProto() = default;

TensorPodProto::~TensorPodProto() = default;

void TensorPodProto::InitFromProto(const PbMessage& proto_tensorpodproto) {
  BuildFromProto(proto_tensorpodproto);
}
  
void* TensorPodProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_shape();
    case 2: return mutable_data_type();
    default: return nullptr;
  }
}

bool TensorPodProto::operator==(const TensorPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TensorPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TensorPodProto::operator<(const TensorPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TensorPodProto::Clear() {
  if (data_) { data_.reset(); }
}
void TensorPodProto::CopyFrom(const TensorPodProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TensorPodProto& TensorPodProto::operator=(const TensorPodProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field shape
void TensorPodProto::clear_shape() {
  return __SharedPtr__()->clear_shape();
}
::oneflow::cfg::ShapeProto* TensorPodProto::mutable_shape() {
  return __SharedPtr__()->mutable_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> TensorPodProto::shared_mutable_shape() {
  return mutable_shape()->__SharedMutable__();
}
// required or optional field data_type
void TensorPodProto::clear_data_type() {
  return __SharedPtr__()->clear_data_type();
}
void TensorPodProto::set_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_data_type(value);
}
::oneflow::cfg::DataType* TensorPodProto::mutable_data_type() {
  return  __SharedPtr__()->mutable_data_type();
}

::std::shared_ptr<TensorPodProto> TensorPodProto::__SharedMutable__() {
  return ::std::make_shared<TensorPodProto>(__SharedPtr__());
}
ConstStructPodProto::_StructPodProto_::_StructPodProto_() { Clear(); }
ConstStructPodProto::_StructPodProto_::_StructPodProto_(const _StructPodProto_& other) { CopyFrom(other); }
ConstStructPodProto::_StructPodProto_::_StructPodProto_(const ::oneflow::StructPodProto& proto_structpodproto) {
  InitFromProto(proto_structpodproto);
}
ConstStructPodProto::_StructPodProto_::_StructPodProto_(_StructPodProto_&& other) = default;
ConstStructPodProto::_StructPodProto_::~_StructPodProto_() = default;

void ConstStructPodProto::_StructPodProto_::InitFromProto(const ::oneflow::StructPodProto& proto_structpodproto) {
  Clear();
  // repeated field: field
  if (!proto_structpodproto.field().empty()) {
    for (const ::oneflow::FieldPodProto& elem : proto_structpodproto.field() ) {
      *mutable_field()->Add() = ::oneflow::cfg::FieldPodProto(elem);
    }
  }
    
}

void ConstStructPodProto::_StructPodProto_::ToProto(::oneflow::StructPodProto* proto_structpodproto) const {
  proto_structpodproto->Clear();
  // repeated field: field
  if (!field().empty()) {
    for (const ::oneflow::cfg::FieldPodProto& elem : field() ) {
      ::oneflow::FieldPodProto proto_field_elem;
      elem.ToProto(&proto_field_elem);
      *proto_structpodproto->mutable_field()->Add() = proto_field_elem;
    }
  }

}

::std::string ConstStructPodProto::_StructPodProto_::DebugString() const {
  ::oneflow::StructPodProto proto_structpodproto;
  this->ToProto(&proto_structpodproto);
  return proto_structpodproto.DebugString();
}

void ConstStructPodProto::_StructPodProto_::Clear() {
  clear_field();
}

void ConstStructPodProto::_StructPodProto_::CopyFrom(const _StructPodProto_& other) {
  mutable_field()->CopyFrom(other.field());
}


// repeated field field
::std::size_t ConstStructPodProto::_StructPodProto_::field_size() const {
  if (!field_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
    return default_static_value->size();
  }
  return field_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& ConstStructPodProto::_StructPodProto_::field() const {
  if (!field_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
    return *(default_static_value.get());
  }
  return *(field_.get());
}
const ::oneflow::cfg::FieldPodProto& ConstStructPodProto::_StructPodProto_::field(::std::size_t index) const {
  if (!field_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
    return default_static_value->Get(index);
  }
  return field_->Get(index);
}
void ConstStructPodProto::_StructPodProto_::clear_field() {
  if (!field_) {
    field_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
  }
  return field_->Clear();
}
_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_* ConstStructPodProto::_StructPodProto_::mutable_field() {
  if (!field_) {
    field_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
  }
  return  field_.get();
}
::oneflow::cfg::FieldPodProto* ConstStructPodProto::_StructPodProto_::mutable_field(::std::size_t index) {
  if (!field_) {
    field_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
  }
  return  field_->Mutable(index);
}
::oneflow::cfg::FieldPodProto* ConstStructPodProto::_StructPodProto_::add_field() {
  if (!field_) {
    field_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>();
  }
  return field_->Add();
}


int ConstStructPodProto::_StructPodProto_::compare(const _StructPodProto_& other) {
  if (!(field() == other.field())) {
    return field() < other.field() ? -1 : 1;
  }
  return 0;
}

bool ConstStructPodProto::_StructPodProto_::operator==(const _StructPodProto_& other) const {
  return true
    && field() == other.field()
  ;
}

std::size_t ConstStructPodProto::_StructPodProto_::__CalcHash__() const {
  return 0
    ^ field().__CalcHash__()
  ;
}

bool ConstStructPodProto::_StructPodProto_::operator<(const _StructPodProto_& other) const {
  return false
    || !(field() == other.field()) ? 
      field() < other.field() : false
  ;
}

using _StructPodProto_ =  ConstStructPodProto::_StructPodProto_;
ConstStructPodProto::ConstStructPodProto(const ::std::shared_ptr<_StructPodProto_>& data): data_(data) {}
ConstStructPodProto::ConstStructPodProto(): data_(::std::make_shared<_StructPodProto_>()) {}
ConstStructPodProto::ConstStructPodProto(const ::oneflow::StructPodProto& proto_structpodproto) {
  BuildFromProto(proto_structpodproto);
}
ConstStructPodProto::ConstStructPodProto(const ConstStructPodProto&) = default;
ConstStructPodProto::ConstStructPodProto(ConstStructPodProto&&) noexcept = default;
ConstStructPodProto::~ConstStructPodProto() = default;

void ConstStructPodProto::ToProto(PbMessage* proto_structpodproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::StructPodProto*>(proto_structpodproto));
}
  
::std::string ConstStructPodProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstStructPodProto::__Empty__() const {
  return !data_;
}

int ConstStructPodProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"field", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstStructPodProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstStructPodProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::FieldPodProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstStructPodProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &field();
    default: return nullptr;
  }
}

// repeated field field
::std::size_t ConstStructPodProto::field_size() const {
  return __SharedPtrOrDefault__()->field_size();
}
const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& ConstStructPodProto::field() const {
  return __SharedPtrOrDefault__()->field();
}
const ::oneflow::cfg::FieldPodProto& ConstStructPodProto::field(::std::size_t index) const {
  return __SharedPtrOrDefault__()->field(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> ConstStructPodProto::shared_const_field() const {
  return field().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstFieldPodProto> ConstStructPodProto::shared_const_field(::std::size_t index) const {
  return field(index).__SharedConst__();
}

::std::shared_ptr<ConstStructPodProto> ConstStructPodProto::__SharedConst__() const {
  return ::std::make_shared<ConstStructPodProto>(data_);
}
int64_t ConstStructPodProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstStructPodProto::operator==(const ConstStructPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstStructPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstStructPodProto::operator<(const ConstStructPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_StructPodProto_>& ConstStructPodProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_StructPodProto_> default_ptr = std::make_shared<_StructPodProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_StructPodProto_>& ConstStructPodProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _StructPodProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstStructPodProto
void ConstStructPodProto::BuildFromProto(const PbMessage& proto_structpodproto) {
  data_ = ::std::make_shared<_StructPodProto_>(dynamic_cast<const ::oneflow::StructPodProto&>(proto_structpodproto));
}

StructPodProto::StructPodProto(const ::std::shared_ptr<_StructPodProto_>& data)
  : ConstStructPodProto(data) {}
StructPodProto::StructPodProto(const StructPodProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<StructPodProto> resize
StructPodProto::StructPodProto(StructPodProto&&) noexcept = default; 
StructPodProto::StructPodProto(const ::oneflow::StructPodProto& proto_structpodproto) {
  InitFromProto(proto_structpodproto);
}
StructPodProto::StructPodProto() = default;

StructPodProto::~StructPodProto() = default;

void StructPodProto::InitFromProto(const PbMessage& proto_structpodproto) {
  BuildFromProto(proto_structpodproto);
}
  
void* StructPodProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_field();
    default: return nullptr;
  }
}

bool StructPodProto::operator==(const StructPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t StructPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool StructPodProto::operator<(const StructPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void StructPodProto::Clear() {
  if (data_) { data_.reset(); }
}
void StructPodProto::CopyFrom(const StructPodProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
StructPodProto& StructPodProto::operator=(const StructPodProto& other) {
  CopyFrom(other);
  return *this;
}

// repeated field field
void StructPodProto::clear_field() {
  return __SharedPtr__()->clear_field();
}
_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_* StructPodProto::mutable_field() {
  return __SharedPtr__()->mutable_field();
}
::oneflow::cfg::FieldPodProto* StructPodProto::mutable_field(::std::size_t index) {
  return __SharedPtr__()->mutable_field(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> StructPodProto::shared_mutable_field() {
  return mutable_field()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::FieldPodProto> StructPodProto::shared_mutable_field(::std::size_t index) {
  return mutable_field(index)->__SharedMutable__();
}
::oneflow::cfg::FieldPodProto* StructPodProto::add_field() {
  return __SharedPtr__()->add_field();
}

::std::shared_ptr<StructPodProto> StructPodProto::__SharedMutable__() {
  return ::std::make_shared<StructPodProto>(__SharedPtr__());
}
ConstFieldId::_FieldId_::_FieldId_() { Clear(); }
ConstFieldId::_FieldId_::_FieldId_(const _FieldId_& other) { CopyFrom(other); }
ConstFieldId::_FieldId_::_FieldId_(const ::oneflow::FieldId& proto_fieldid) {
  InitFromProto(proto_fieldid);
}
ConstFieldId::_FieldId_::_FieldId_(_FieldId_&& other) = default;
ConstFieldId::_FieldId_::~_FieldId_() = default;

void ConstFieldId::_FieldId_::InitFromProto(const ::oneflow::FieldId& proto_fieldid) {
  Clear();
  // oneof field: field_id_type
  FieldIdTypeCase field_id_type_case = FieldIdTypeCase(int(proto_fieldid.field_id_type_case()));
  switch (field_id_type_case) {
    case kKey: {
      set_key(static_cast<std::remove_const<std::remove_reference<decltype(key())>::type>::type>(proto_fieldid.key()));
      break;
  }
    case kLbi: {
      *mutable_lbi() = ::oneflow::cfg::LogicalBlobId(proto_fieldid.lbi());
      break;
  }
    case FIELD_ID_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstFieldId::_FieldId_::ToProto(::oneflow::FieldId* proto_fieldid) const {
  proto_fieldid->Clear();

  // oneof field: field_id_type
  ::oneflow::FieldId::FieldIdTypeCase field_id_type_case = ::oneflow::FieldId::FieldIdTypeCase(int(this->field_id_type_case()));
  switch (field_id_type_case) {
    case ::oneflow::FieldId::kKey: {
      proto_fieldid->set_key(static_cast<decltype(proto_fieldid->key())>(key()));
      break;
    }
    case ::oneflow::FieldId::kLbi: {
      ::oneflow::LogicalBlobId of_proto_lbi;
      lbi().ToProto(&of_proto_lbi);
      proto_fieldid->mutable_lbi()->CopyFrom(of_proto_lbi);
      break;
    }
    case ::oneflow::FieldId::FIELD_ID_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstFieldId::_FieldId_::DebugString() const {
  ::oneflow::FieldId proto_fieldid;
  this->ToProto(&proto_fieldid);
  return proto_fieldid.DebugString();
}

void ConstFieldId::_FieldId_::Clear() {
  clear_field_id_type();
}

void ConstFieldId::_FieldId_::CopyFrom(const _FieldId_& other) {
  field_id_type_copy_from(other);
}


// oneof field field_id_type: key
bool ConstFieldId::_FieldId_::has_key() const {
  return field_id_type_case() == kKey;
}
void ConstFieldId::_FieldId_::clear_key() {
  if (has_key()) {
    field_id_type_.key_ = FieldKey();
    field_id_type_case_ = FIELD_ID_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::FieldKey& ConstFieldId::_FieldId_::key() const {
  if (has_key()) {
      return field_id_type_.key_;
    } else {
      static const ::oneflow::cfg::FieldKey default_static_value = ::oneflow::cfg::FieldKey();
    return default_static_value;
    }
}
void ConstFieldId::_FieldId_::set_key(const ::oneflow::cfg::FieldKey& value) {
  if(!has_key()) {
    clear_field_id_type();
    }
  field_id_type_case_ = kKey;
    field_id_type_.key_ = value;
  }
::oneflow::cfg::FieldKey* ConstFieldId::_FieldId_::mutable_key() {
  if(!has_key()) {
    clear_field_id_type();
    }
    field_id_type_case_ = kKey;
  return  &field_id_type_.key_;
  }

// oneof field field_id_type: lbi
bool ConstFieldId::_FieldId_::has_lbi() const {
  return field_id_type_case() == kLbi;
}
void ConstFieldId::_FieldId_::clear_lbi() {
  if (has_lbi()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(field_id_type_.lbi_));
      ptr->~Shared_ptr();
    }
    field_id_type_case_ = FIELD_ID_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LogicalBlobId& ConstFieldId::_FieldId_::lbi() const {
  if (has_lbi()) {
      const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>*>(&(field_id_type_.lbi_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> default_static_value = ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
    return *default_static_value;
    }
}
::oneflow::cfg::LogicalBlobId* ConstFieldId::_FieldId_::mutable_lbi() {
  if(!has_lbi()) {
    clear_field_id_type();
    new (&(field_id_type_.lbi_)) ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>(new ::oneflow::cfg::LogicalBlobId());
  }
  field_id_type_case_ = kLbi;
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LogicalBlobId>*>(&(field_id_type_.lbi_));
  return  (*ptr).get();
}
ConstFieldId::FieldIdTypeCase ConstFieldId::_FieldId_::field_id_type_case() const {
  return field_id_type_case_;
}
bool ConstFieldId::_FieldId_::has_field_id_type() const {
  return field_id_type_case_ != FIELD_ID_TYPE_NOT_SET;
}
void ConstFieldId::_FieldId_::clear_field_id_type() {
  switch (field_id_type_case()) {
    case kKey: {
      field_id_type_.key_ = FieldKey();
      break;
    }
    case kLbi: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LogicalBlobId>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(field_id_type_.lbi_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case FIELD_ID_TYPE_NOT_SET: {
      break;
    }
  }
  field_id_type_case_ = FIELD_ID_TYPE_NOT_SET;
}
void ConstFieldId::_FieldId_::field_id_type_copy_from(const _FieldId_& other) {
  switch (other.field_id_type_case()) {
    case kKey: {
      set_key(other.key());
      break;
    }
    case kLbi: {
      mutable_lbi()->CopyFrom(other.lbi());
      break;
    }
    case FIELD_ID_TYPE_NOT_SET: {
      clear_field_id_type();
    }
  }
}


int ConstFieldId::_FieldId_::compare(const _FieldId_& other) {
  if (!(field_id_type_case() == other.field_id_type_case())) {
    return field_id_type_case() < other.field_id_type_case() ? -1 : 1;
  }
  switch (field_id_type_case()) {
    case kKey: {
      if (!(key() == other.key())) {
        return key() < other.key() ? -1 : 1;
      }
      break;
    }
    case kLbi: {
      if (!(lbi() == other.lbi())) {
        return lbi() < other.lbi() ? -1 : 1;
      }
      break;
    }
    case FIELD_ID_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstFieldId::_FieldId_::operator==(const _FieldId_& other) const {
  return true
    && field_id_type_case() == other.field_id_type_case()
    && (field_id_type_case() == kKey ? 
      key() == other.key() : true)
    && (field_id_type_case() == kLbi ? 
      lbi() == other.lbi() : true)
  ;
}

std::size_t ConstFieldId::_FieldId_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(field_id_type_case())
    ^ (has_key() ? std::hash<::oneflow::cfg::FieldKey>()(key()) : 0)
    ^ (has_lbi() ? std::hash<::oneflow::cfg::LogicalBlobId>()(lbi()) : 0)
  ;
}

bool ConstFieldId::_FieldId_::operator<(const _FieldId_& other) const {
  return false
    || !(field_id_type_case() == other.field_id_type_case()) ? 
      field_id_type_case() < other.field_id_type_case() : false
    || ((field_id_type_case() == kKey) && 
      !(key() == other.key())) ? 
        key() < other.key() : false
    || ((field_id_type_case() == kLbi) && 
      !(lbi() == other.lbi())) ? 
        lbi() < other.lbi() : false
  ;
}

using _FieldId_ =  ConstFieldId::_FieldId_;
ConstFieldId::ConstFieldId(const ::std::shared_ptr<_FieldId_>& data): data_(data) {}
ConstFieldId::ConstFieldId(): data_(::std::make_shared<_FieldId_>()) {}
ConstFieldId::ConstFieldId(const ::oneflow::FieldId& proto_fieldid) {
  BuildFromProto(proto_fieldid);
}
ConstFieldId::ConstFieldId(const ConstFieldId&) = default;
ConstFieldId::ConstFieldId(ConstFieldId&&) noexcept = default;
ConstFieldId::~ConstFieldId() = default;

void ConstFieldId::ToProto(PbMessage* proto_fieldid) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::FieldId*>(proto_fieldid));
}
  
::std::string ConstFieldId::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstFieldId::__Empty__() const {
  return !data_;
}

int ConstFieldId::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"key", 1},
    {"lbi", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstFieldId::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstFieldId::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldKey),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobId),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstFieldId::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &key();
    case 2: return &lbi();
    default: return nullptr;
  }
}

 // oneof field field_id_type: key
bool ConstFieldId::has_key() const {
  return __SharedPtrOrDefault__()->has_key();
}
const ::oneflow::cfg::FieldKey& ConstFieldId::key() const {
  return __SharedPtrOrDefault__()->key();
}

// used by pybind11 only
 // oneof field field_id_type: lbi
bool ConstFieldId::has_lbi() const {
  return __SharedPtrOrDefault__()->has_lbi();
}
const ::oneflow::cfg::LogicalBlobId& ConstFieldId::lbi() const {
  return __SharedPtrOrDefault__()->lbi();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstFieldId::shared_const_lbi() const {
  return lbi().__SharedConst__();
}
ConstFieldId::FieldIdTypeCase ConstFieldId::field_id_type_case() const {
  return __SharedPtrOrDefault__()->field_id_type_case();
}

bool ConstFieldId::has_field_id_type() const {
  return __SharedPtrOrDefault__()->has_field_id_type();
}

::std::shared_ptr<ConstFieldId> ConstFieldId::__SharedConst__() const {
  return ::std::make_shared<ConstFieldId>(data_);
}
int64_t ConstFieldId::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstFieldId::operator==(const ConstFieldId& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstFieldId::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstFieldId::operator<(const ConstFieldId& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_FieldId_>& ConstFieldId::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_FieldId_> default_ptr = std::make_shared<_FieldId_>();
  return default_ptr;
}
const ::std::shared_ptr<_FieldId_>& ConstFieldId::__SharedPtr__() {
  if (!data_) { data_.reset(new _FieldId_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstFieldId
void ConstFieldId::BuildFromProto(const PbMessage& proto_fieldid) {
  data_ = ::std::make_shared<_FieldId_>(dynamic_cast<const ::oneflow::FieldId&>(proto_fieldid));
}

FieldId::FieldId(const ::std::shared_ptr<_FieldId_>& data)
  : ConstFieldId(data) {}
FieldId::FieldId(const FieldId& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<FieldId> resize
FieldId::FieldId(FieldId&&) noexcept = default; 
FieldId::FieldId(const ::oneflow::FieldId& proto_fieldid) {
  InitFromProto(proto_fieldid);
}
FieldId::FieldId() = default;

FieldId::~FieldId() = default;

void FieldId::InitFromProto(const PbMessage& proto_fieldid) {
  BuildFromProto(proto_fieldid);
}
  
void* FieldId::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_key();
    case 2: return mutable_lbi();
    default: return nullptr;
  }
}

bool FieldId::operator==(const FieldId& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t FieldId::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool FieldId::operator<(const FieldId& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void FieldId::Clear() {
  if (data_) { data_.reset(); }
}
void FieldId::CopyFrom(const FieldId& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
FieldId& FieldId::operator=(const FieldId& other) {
  CopyFrom(other);
  return *this;
}

void FieldId::clear_key() {
  return __SharedPtr__()->clear_key();
}
void FieldId::set_key(const ::oneflow::cfg::FieldKey& value) {
  return __SharedPtr__()->set_key(value);
}
::oneflow::cfg::FieldKey* FieldId::mutable_key() {
  return  __SharedPtr__()->mutable_key();
}
void FieldId::clear_lbi() {
  return __SharedPtr__()->clear_lbi();
}
::oneflow::cfg::LogicalBlobId* FieldId::mutable_lbi() {
  return __SharedPtr__()->mutable_lbi();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> FieldId::shared_mutable_lbi() {
  return mutable_lbi()->__SharedMutable__();
}

::std::shared_ptr<FieldId> FieldId::__SharedMutable__() {
  return ::std::make_shared<FieldId>(__SharedPtr__());
}
ConstFieldPodProto::_FieldPodProto_::_FieldPodProto_() { Clear(); }
ConstFieldPodProto::_FieldPodProto_::_FieldPodProto_(const _FieldPodProto_& other) { CopyFrom(other); }
ConstFieldPodProto::_FieldPodProto_::_FieldPodProto_(const ::oneflow::FieldPodProto& proto_fieldpodproto) {
  InitFromProto(proto_fieldpodproto);
}
ConstFieldPodProto::_FieldPodProto_::_FieldPodProto_(_FieldPodProto_&& other) = default;
ConstFieldPodProto::_FieldPodProto_::~_FieldPodProto_() = default;

void ConstFieldPodProto::_FieldPodProto_::InitFromProto(const ::oneflow::FieldPodProto& proto_fieldpodproto) {
  Clear();
  // required_or_optional field: field_id
  if (proto_fieldpodproto.has_field_id()) {
  *mutable_field_id() = ::oneflow::cfg::FieldId(proto_fieldpodproto.field_id());      
  }
  // required_or_optional field: alignment
  if (proto_fieldpodproto.has_alignment()) {
    set_alignment(proto_fieldpodproto.alignment());
  }
  // required_or_optional field: pod
  if (proto_fieldpodproto.has_pod()) {
  *mutable_pod() = ::oneflow::cfg::PodProto(proto_fieldpodproto.pod());      
  }
    
}

void ConstFieldPodProto::_FieldPodProto_::ToProto(::oneflow::FieldPodProto* proto_fieldpodproto) const {
  proto_fieldpodproto->Clear();
  // required_or_optional field: field_id
  if (this->has_field_id()) {
    ::oneflow::FieldId proto_field_id;
    field_id().ToProto(&proto_field_id);
    proto_fieldpodproto->mutable_field_id()->CopyFrom(proto_field_id);
    }
  // required_or_optional field: alignment
  if (this->has_alignment()) {
    proto_fieldpodproto->set_alignment(alignment());
    }
  // required_or_optional field: pod
  if (this->has_pod()) {
    ::oneflow::PodProto proto_pod;
    pod().ToProto(&proto_pod);
    proto_fieldpodproto->mutable_pod()->CopyFrom(proto_pod);
    }

}

::std::string ConstFieldPodProto::_FieldPodProto_::DebugString() const {
  ::oneflow::FieldPodProto proto_fieldpodproto;
  this->ToProto(&proto_fieldpodproto);
  return proto_fieldpodproto.DebugString();
}

void ConstFieldPodProto::_FieldPodProto_::Clear() {
  clear_field_id();
  clear_alignment();
  clear_pod();
}

void ConstFieldPodProto::_FieldPodProto_::CopyFrom(const _FieldPodProto_& other) {
  if (other.has_field_id()) {
    mutable_field_id()->CopyFrom(other.field_id());
  } else {
    clear_field_id();
  }
  if (other.has_alignment()) {
    set_alignment(other.alignment());
  } else {
    clear_alignment();
  }
  if (other.has_pod()) {
    mutable_pod()->CopyFrom(other.pod());
  } else {
    clear_pod();
  }
}


// optional field field_id
bool ConstFieldPodProto::_FieldPodProto_::has_field_id() const {
  return has_field_id_;
}
const ::oneflow::cfg::FieldId& ConstFieldPodProto::_FieldPodProto_::field_id() const {
  if (!field_id_) {
    static const ::std::shared_ptr<::oneflow::cfg::FieldId> default_static_value =
      ::std::make_shared<::oneflow::cfg::FieldId>();
    return *default_static_value;
  }
  return *(field_id_.get());
}
void ConstFieldPodProto::_FieldPodProto_::clear_field_id() {
  if (field_id_) {
    field_id_->Clear();
  }
  has_field_id_ = false;
}
::oneflow::cfg::FieldId* ConstFieldPodProto::_FieldPodProto_::mutable_field_id() {
  if (!field_id_) {
    field_id_ = ::std::make_shared<::oneflow::cfg::FieldId>();
  }
  has_field_id_ = true;
  return field_id_.get();
}

// optional field alignment
bool ConstFieldPodProto::_FieldPodProto_::has_alignment() const {
  return has_alignment_;
}
const int32_t& ConstFieldPodProto::_FieldPodProto_::alignment() const {
  if (has_alignment_) { return alignment_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstFieldPodProto::_FieldPodProto_::clear_alignment() {
  has_alignment_ = false;
}
void ConstFieldPodProto::_FieldPodProto_::set_alignment(const int32_t& value) {
  alignment_ = value;
  has_alignment_ = true;
}
int32_t* ConstFieldPodProto::_FieldPodProto_::mutable_alignment() {
  has_alignment_ = true;
  return &alignment_;
}

// optional field pod
bool ConstFieldPodProto::_FieldPodProto_::has_pod() const {
  return has_pod_;
}
const ::oneflow::cfg::PodProto& ConstFieldPodProto::_FieldPodProto_::pod() const {
  if (!pod_) {
    static const ::std::shared_ptr<::oneflow::cfg::PodProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::PodProto>();
    return *default_static_value;
  }
  return *(pod_.get());
}
void ConstFieldPodProto::_FieldPodProto_::clear_pod() {
  if (pod_) {
    pod_->Clear();
  }
  has_pod_ = false;
}
::oneflow::cfg::PodProto* ConstFieldPodProto::_FieldPodProto_::mutable_pod() {
  if (!pod_) {
    pod_ = ::std::make_shared<::oneflow::cfg::PodProto>();
  }
  has_pod_ = true;
  return pod_.get();
}


int ConstFieldPodProto::_FieldPodProto_::compare(const _FieldPodProto_& other) {
  if (!(has_field_id() == other.has_field_id())) {
    return has_field_id() < other.has_field_id() ? -1 : 1;
  } else if (!(field_id() == other.field_id())) {
    return field_id() < other.field_id() ? -1 : 1;
  }
  if (!(has_alignment() == other.has_alignment())) {
    return has_alignment() < other.has_alignment() ? -1 : 1;
  } else if (!(alignment() == other.alignment())) {
    return alignment() < other.alignment() ? -1 : 1;
  }
  if (!(has_pod() == other.has_pod())) {
    return has_pod() < other.has_pod() ? -1 : 1;
  } else if (!(pod() == other.pod())) {
    return pod() < other.pod() ? -1 : 1;
  }
  return 0;
}

bool ConstFieldPodProto::_FieldPodProto_::operator==(const _FieldPodProto_& other) const {
  return true
    && has_field_id() == other.has_field_id() 
    && field_id() == other.field_id()
    && has_alignment() == other.has_alignment() 
    && alignment() == other.alignment()
    && has_pod() == other.has_pod() 
    && pod() == other.pod()
  ;
}

std::size_t ConstFieldPodProto::_FieldPodProto_::__CalcHash__() const {
  return 0
    ^ (has_field_id() ? std::hash<::oneflow::cfg::FieldId>()(field_id()) : 0)
    ^ (has_alignment() ? std::hash<int32_t>()(alignment()) : 0)
    ^ (has_pod() ? std::hash<::oneflow::cfg::PodProto>()(pod()) : 0)
  ;
}

bool ConstFieldPodProto::_FieldPodProto_::operator<(const _FieldPodProto_& other) const {
  return false
    || !(has_field_id() == other.has_field_id()) ? 
      has_field_id() < other.has_field_id() : false
    || !(field_id() == other.field_id()) ? 
      field_id() < other.field_id() : false
    || !(has_alignment() == other.has_alignment()) ? 
      has_alignment() < other.has_alignment() : false
    || !(alignment() == other.alignment()) ? 
      alignment() < other.alignment() : false
    || !(has_pod() == other.has_pod()) ? 
      has_pod() < other.has_pod() : false
    || !(pod() == other.pod()) ? 
      pod() < other.pod() : false
  ;
}

using _FieldPodProto_ =  ConstFieldPodProto::_FieldPodProto_;
ConstFieldPodProto::ConstFieldPodProto(const ::std::shared_ptr<_FieldPodProto_>& data): data_(data) {}
ConstFieldPodProto::ConstFieldPodProto(): data_(::std::make_shared<_FieldPodProto_>()) {}
ConstFieldPodProto::ConstFieldPodProto(const ::oneflow::FieldPodProto& proto_fieldpodproto) {
  BuildFromProto(proto_fieldpodproto);
}
ConstFieldPodProto::ConstFieldPodProto(const ConstFieldPodProto&) = default;
ConstFieldPodProto::ConstFieldPodProto(ConstFieldPodProto&&) noexcept = default;
ConstFieldPodProto::~ConstFieldPodProto() = default;

void ConstFieldPodProto::ToProto(PbMessage* proto_fieldpodproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::FieldPodProto*>(proto_fieldpodproto));
}
  
::std::string ConstFieldPodProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstFieldPodProto::__Empty__() const {
  return !data_;
}

int ConstFieldPodProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"field_id", 1},
    {"alignment", 2},
    {"pod", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstFieldPodProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstFieldPodProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FieldId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFieldId),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PodProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPodProto),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstFieldPodProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &field_id();
    case 2: return &alignment();
    case 3: return &pod();
    default: return nullptr;
  }
}

// required or optional field field_id
bool ConstFieldPodProto::has_field_id() const {
  return __SharedPtrOrDefault__()->has_field_id();
}
const ::oneflow::cfg::FieldId& ConstFieldPodProto::field_id() const {
  return __SharedPtrOrDefault__()->field_id();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFieldId> ConstFieldPodProto::shared_const_field_id() const {
  return field_id().__SharedConst__();
}
// required or optional field alignment
bool ConstFieldPodProto::has_alignment() const {
  return __SharedPtrOrDefault__()->has_alignment();
}
const int32_t& ConstFieldPodProto::alignment() const {
  return __SharedPtrOrDefault__()->alignment();
}
// used by pybind11 only
// required or optional field pod
bool ConstFieldPodProto::has_pod() const {
  return __SharedPtrOrDefault__()->has_pod();
}
const ::oneflow::cfg::PodProto& ConstFieldPodProto::pod() const {
  return __SharedPtrOrDefault__()->pod();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPodProto> ConstFieldPodProto::shared_const_pod() const {
  return pod().__SharedConst__();
}

::std::shared_ptr<ConstFieldPodProto> ConstFieldPodProto::__SharedConst__() const {
  return ::std::make_shared<ConstFieldPodProto>(data_);
}
int64_t ConstFieldPodProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstFieldPodProto::operator==(const ConstFieldPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstFieldPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstFieldPodProto::operator<(const ConstFieldPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_FieldPodProto_>& ConstFieldPodProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_FieldPodProto_> default_ptr = std::make_shared<_FieldPodProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_FieldPodProto_>& ConstFieldPodProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _FieldPodProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstFieldPodProto
void ConstFieldPodProto::BuildFromProto(const PbMessage& proto_fieldpodproto) {
  data_ = ::std::make_shared<_FieldPodProto_>(dynamic_cast<const ::oneflow::FieldPodProto&>(proto_fieldpodproto));
}

FieldPodProto::FieldPodProto(const ::std::shared_ptr<_FieldPodProto_>& data)
  : ConstFieldPodProto(data) {}
FieldPodProto::FieldPodProto(const FieldPodProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<FieldPodProto> resize
FieldPodProto::FieldPodProto(FieldPodProto&&) noexcept = default; 
FieldPodProto::FieldPodProto(const ::oneflow::FieldPodProto& proto_fieldpodproto) {
  InitFromProto(proto_fieldpodproto);
}
FieldPodProto::FieldPodProto() = default;

FieldPodProto::~FieldPodProto() = default;

void FieldPodProto::InitFromProto(const PbMessage& proto_fieldpodproto) {
  BuildFromProto(proto_fieldpodproto);
}
  
void* FieldPodProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_field_id();
    case 2: return mutable_alignment();
    case 3: return mutable_pod();
    default: return nullptr;
  }
}

bool FieldPodProto::operator==(const FieldPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t FieldPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool FieldPodProto::operator<(const FieldPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void FieldPodProto::Clear() {
  if (data_) { data_.reset(); }
}
void FieldPodProto::CopyFrom(const FieldPodProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
FieldPodProto& FieldPodProto::operator=(const FieldPodProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field field_id
void FieldPodProto::clear_field_id() {
  return __SharedPtr__()->clear_field_id();
}
::oneflow::cfg::FieldId* FieldPodProto::mutable_field_id() {
  return __SharedPtr__()->mutable_field_id();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FieldId> FieldPodProto::shared_mutable_field_id() {
  return mutable_field_id()->__SharedMutable__();
}
// required or optional field alignment
void FieldPodProto::clear_alignment() {
  return __SharedPtr__()->clear_alignment();
}
void FieldPodProto::set_alignment(const int32_t& value) {
  return __SharedPtr__()->set_alignment(value);
}
int32_t* FieldPodProto::mutable_alignment() {
  return  __SharedPtr__()->mutable_alignment();
}
// required or optional field pod
void FieldPodProto::clear_pod() {
  return __SharedPtr__()->clear_pod();
}
::oneflow::cfg::PodProto* FieldPodProto::mutable_pod() {
  return __SharedPtr__()->mutable_pod();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PodProto> FieldPodProto::shared_mutable_pod() {
  return mutable_pod()->__SharedMutable__();
}

::std::shared_ptr<FieldPodProto> FieldPodProto::__SharedMutable__() {
  return ::std::make_shared<FieldPodProto>(__SharedPtr__());
}
ConstPodProto::_PodProto_::_PodProto_() { Clear(); }
ConstPodProto::_PodProto_::_PodProto_(const _PodProto_& other) { CopyFrom(other); }
ConstPodProto::_PodProto_::_PodProto_(const ::oneflow::PodProto& proto_podproto) {
  InitFromProto(proto_podproto);
}
ConstPodProto::_PodProto_::_PodProto_(_PodProto_&& other) = default;
ConstPodProto::_PodProto_::~_PodProto_() = default;

void ConstPodProto::_PodProto_::InitFromProto(const ::oneflow::PodProto& proto_podproto) {
  Clear();
  // oneof field: pod_type
  PodTypeCase pod_type_case = PodTypeCase(int(proto_podproto.pod_type_case()));
  switch (pod_type_case) {
    case kTensorPod: {
      *mutable_tensor_pod() = ::oneflow::cfg::TensorPodProto(proto_podproto.tensor_pod());
      break;
  }
    case kStructPod: {
      *mutable_struct_pod() = ::oneflow::cfg::StructPodProto(proto_podproto.struct_pod());
      break;
  }
    case POD_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstPodProto::_PodProto_::ToProto(::oneflow::PodProto* proto_podproto) const {
  proto_podproto->Clear();

  // oneof field: pod_type
  ::oneflow::PodProto::PodTypeCase pod_type_case = ::oneflow::PodProto::PodTypeCase(int(this->pod_type_case()));
  switch (pod_type_case) {
    case ::oneflow::PodProto::kTensorPod: {
      ::oneflow::TensorPodProto of_proto_tensor_pod;
      tensor_pod().ToProto(&of_proto_tensor_pod);
      proto_podproto->mutable_tensor_pod()->CopyFrom(of_proto_tensor_pod);
      break;
    }
    case ::oneflow::PodProto::kStructPod: {
      ::oneflow::StructPodProto of_proto_struct_pod;
      struct_pod().ToProto(&of_proto_struct_pod);
      proto_podproto->mutable_struct_pod()->CopyFrom(of_proto_struct_pod);
      break;
    }
    case ::oneflow::PodProto::POD_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstPodProto::_PodProto_::DebugString() const {
  ::oneflow::PodProto proto_podproto;
  this->ToProto(&proto_podproto);
  return proto_podproto.DebugString();
}

void ConstPodProto::_PodProto_::Clear() {
  clear_pod_type();
}

void ConstPodProto::_PodProto_::CopyFrom(const _PodProto_& other) {
  pod_type_copy_from(other);
}


// oneof field pod_type: tensor_pod
bool ConstPodProto::_PodProto_::has_tensor_pod() const {
  return pod_type_case() == kTensorPod;
}
void ConstPodProto::_PodProto_::clear_tensor_pod() {
  if (has_tensor_pod()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TensorPodProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(pod_type_.tensor_pod_));
      ptr->~Shared_ptr();
    }
    pod_type_case_ = POD_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TensorPodProto& ConstPodProto::_PodProto_::tensor_pod() const {
  if (has_tensor_pod()) {
      const ::std::shared_ptr<::oneflow::cfg::TensorPodProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TensorPodProto>*>(&(pod_type_.tensor_pod_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TensorPodProto> default_static_value = ::std::make_shared<::oneflow::cfg::TensorPodProto>();
    return *default_static_value;
    }
}
::oneflow::cfg::TensorPodProto* ConstPodProto::_PodProto_::mutable_tensor_pod() {
  if(!has_tensor_pod()) {
    clear_pod_type();
    new (&(pod_type_.tensor_pod_)) ::std::shared_ptr<::oneflow::cfg::TensorPodProto>(new ::oneflow::cfg::TensorPodProto());
  }
  pod_type_case_ = kTensorPod;
  ::std::shared_ptr<::oneflow::cfg::TensorPodProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TensorPodProto>*>(&(pod_type_.tensor_pod_));
  return  (*ptr).get();
}

// oneof field pod_type: struct_pod
bool ConstPodProto::_PodProto_::has_struct_pod() const {
  return pod_type_case() == kStructPod;
}
void ConstPodProto::_PodProto_::clear_struct_pod() {
  if (has_struct_pod()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::StructPodProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(pod_type_.struct_pod_));
      ptr->~Shared_ptr();
    }
    pod_type_case_ = POD_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::StructPodProto& ConstPodProto::_PodProto_::struct_pod() const {
  if (has_struct_pod()) {
      const ::std::shared_ptr<::oneflow::cfg::StructPodProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::StructPodProto>*>(&(pod_type_.struct_pod_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::StructPodProto> default_static_value = ::std::make_shared<::oneflow::cfg::StructPodProto>();
    return *default_static_value;
    }
}
::oneflow::cfg::StructPodProto* ConstPodProto::_PodProto_::mutable_struct_pod() {
  if(!has_struct_pod()) {
    clear_pod_type();
    new (&(pod_type_.struct_pod_)) ::std::shared_ptr<::oneflow::cfg::StructPodProto>(new ::oneflow::cfg::StructPodProto());
  }
  pod_type_case_ = kStructPod;
  ::std::shared_ptr<::oneflow::cfg::StructPodProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::StructPodProto>*>(&(pod_type_.struct_pod_));
  return  (*ptr).get();
}
ConstPodProto::PodTypeCase ConstPodProto::_PodProto_::pod_type_case() const {
  return pod_type_case_;
}
bool ConstPodProto::_PodProto_::has_pod_type() const {
  return pod_type_case_ != POD_TYPE_NOT_SET;
}
void ConstPodProto::_PodProto_::clear_pod_type() {
  switch (pod_type_case()) {
    case kTensorPod: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TensorPodProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(pod_type_.tensor_pod_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kStructPod: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::StructPodProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(pod_type_.struct_pod_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case POD_TYPE_NOT_SET: {
      break;
    }
  }
  pod_type_case_ = POD_TYPE_NOT_SET;
}
void ConstPodProto::_PodProto_::pod_type_copy_from(const _PodProto_& other) {
  switch (other.pod_type_case()) {
    case kTensorPod: {
      mutable_tensor_pod()->CopyFrom(other.tensor_pod());
      break;
    }
    case kStructPod: {
      mutable_struct_pod()->CopyFrom(other.struct_pod());
      break;
    }
    case POD_TYPE_NOT_SET: {
      clear_pod_type();
    }
  }
}


int ConstPodProto::_PodProto_::compare(const _PodProto_& other) {
  if (!(pod_type_case() == other.pod_type_case())) {
    return pod_type_case() < other.pod_type_case() ? -1 : 1;
  }
  switch (pod_type_case()) {
    case kTensorPod: {
      if (!(tensor_pod() == other.tensor_pod())) {
        return tensor_pod() < other.tensor_pod() ? -1 : 1;
      }
      break;
    }
    case kStructPod: {
      if (!(struct_pod() == other.struct_pod())) {
        return struct_pod() < other.struct_pod() ? -1 : 1;
      }
      break;
    }
    case POD_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstPodProto::_PodProto_::operator==(const _PodProto_& other) const {
  return true
    && pod_type_case() == other.pod_type_case()
    && (pod_type_case() == kTensorPod ? 
      tensor_pod() == other.tensor_pod() : true)
    && (pod_type_case() == kStructPod ? 
      struct_pod() == other.struct_pod() : true)
  ;
}

std::size_t ConstPodProto::_PodProto_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(pod_type_case())
    ^ (has_tensor_pod() ? std::hash<::oneflow::cfg::TensorPodProto>()(tensor_pod()) : 0)
    ^ (has_struct_pod() ? std::hash<::oneflow::cfg::StructPodProto>()(struct_pod()) : 0)
  ;
}

bool ConstPodProto::_PodProto_::operator<(const _PodProto_& other) const {
  return false
    || !(pod_type_case() == other.pod_type_case()) ? 
      pod_type_case() < other.pod_type_case() : false
    || ((pod_type_case() == kTensorPod) && 
      !(tensor_pod() == other.tensor_pod())) ? 
        tensor_pod() < other.tensor_pod() : false
    || ((pod_type_case() == kStructPod) && 
      !(struct_pod() == other.struct_pod())) ? 
        struct_pod() < other.struct_pod() : false
  ;
}

using _PodProto_ =  ConstPodProto::_PodProto_;
ConstPodProto::ConstPodProto(const ::std::shared_ptr<_PodProto_>& data): data_(data) {}
ConstPodProto::ConstPodProto(): data_(::std::make_shared<_PodProto_>()) {}
ConstPodProto::ConstPodProto(const ::oneflow::PodProto& proto_podproto) {
  BuildFromProto(proto_podproto);
}
ConstPodProto::ConstPodProto(const ConstPodProto&) = default;
ConstPodProto::ConstPodProto(ConstPodProto&&) noexcept = default;
ConstPodProto::~ConstPodProto() = default;

void ConstPodProto::ToProto(PbMessage* proto_podproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PodProto*>(proto_podproto));
}
  
::std::string ConstPodProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPodProto::__Empty__() const {
  return !data_;
}

int ConstPodProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"tensor_pod", 1},
    {"struct_pod", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPodProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPodProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TensorPodProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTensorPodProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::StructPodProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstStructPodProto),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPodProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &tensor_pod();
    case 2: return &struct_pod();
    default: return nullptr;
  }
}

 // oneof field pod_type: tensor_pod
bool ConstPodProto::has_tensor_pod() const {
  return __SharedPtrOrDefault__()->has_tensor_pod();
}
const ::oneflow::cfg::TensorPodProto& ConstPodProto::tensor_pod() const {
  return __SharedPtrOrDefault__()->tensor_pod();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTensorPodProto> ConstPodProto::shared_const_tensor_pod() const {
  return tensor_pod().__SharedConst__();
}
 // oneof field pod_type: struct_pod
bool ConstPodProto::has_struct_pod() const {
  return __SharedPtrOrDefault__()->has_struct_pod();
}
const ::oneflow::cfg::StructPodProto& ConstPodProto::struct_pod() const {
  return __SharedPtrOrDefault__()->struct_pod();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstStructPodProto> ConstPodProto::shared_const_struct_pod() const {
  return struct_pod().__SharedConst__();
}
ConstPodProto::PodTypeCase ConstPodProto::pod_type_case() const {
  return __SharedPtrOrDefault__()->pod_type_case();
}

bool ConstPodProto::has_pod_type() const {
  return __SharedPtrOrDefault__()->has_pod_type();
}

::std::shared_ptr<ConstPodProto> ConstPodProto::__SharedConst__() const {
  return ::std::make_shared<ConstPodProto>(data_);
}
int64_t ConstPodProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPodProto::operator==(const ConstPodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPodProto::operator<(const ConstPodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PodProto_>& ConstPodProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PodProto_> default_ptr = std::make_shared<_PodProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_PodProto_>& ConstPodProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _PodProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPodProto
void ConstPodProto::BuildFromProto(const PbMessage& proto_podproto) {
  data_ = ::std::make_shared<_PodProto_>(dynamic_cast<const ::oneflow::PodProto&>(proto_podproto));
}

PodProto::PodProto(const ::std::shared_ptr<_PodProto_>& data)
  : ConstPodProto(data) {}
PodProto::PodProto(const PodProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PodProto> resize
PodProto::PodProto(PodProto&&) noexcept = default; 
PodProto::PodProto(const ::oneflow::PodProto& proto_podproto) {
  InitFromProto(proto_podproto);
}
PodProto::PodProto() = default;

PodProto::~PodProto() = default;

void PodProto::InitFromProto(const PbMessage& proto_podproto) {
  BuildFromProto(proto_podproto);
}
  
void* PodProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_tensor_pod();
    case 2: return mutable_struct_pod();
    default: return nullptr;
  }
}

bool PodProto::operator==(const PodProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PodProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PodProto::operator<(const PodProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PodProto::Clear() {
  if (data_) { data_.reset(); }
}
void PodProto::CopyFrom(const PodProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PodProto& PodProto::operator=(const PodProto& other) {
  CopyFrom(other);
  return *this;
}

void PodProto::clear_tensor_pod() {
  return __SharedPtr__()->clear_tensor_pod();
}
::oneflow::cfg::TensorPodProto* PodProto::mutable_tensor_pod() {
  return __SharedPtr__()->mutable_tensor_pod();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TensorPodProto> PodProto::shared_mutable_tensor_pod() {
  return mutable_tensor_pod()->__SharedMutable__();
}
void PodProto::clear_struct_pod() {
  return __SharedPtr__()->clear_struct_pod();
}
::oneflow::cfg::StructPodProto* PodProto::mutable_struct_pod() {
  return __SharedPtr__()->mutable_struct_pod();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::StructPodProto> PodProto::shared_mutable_struct_pod() {
  return mutable_struct_pod()->__SharedMutable__();
}

::std::shared_ptr<PodProto> PodProto::__SharedMutable__() {
  return ::std::make_shared<PodProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::FieldPodProto>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::FieldPodProto>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::~Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_() = default;


bool Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::FieldPodProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstFieldPodProto> Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::FieldPodProto>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_() = default;
_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::~_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::FieldPodProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::FieldPodProto>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::FieldPodProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::FieldPodProto> _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::FieldPodProto> _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
