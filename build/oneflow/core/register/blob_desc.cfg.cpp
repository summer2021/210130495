#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/register/blob_desc.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstBlobDescProto::_BlobDescProto_::_BlobDescProto_() { Clear(); }
ConstBlobDescProto::_BlobDescProto_::_BlobDescProto_(const _BlobDescProto_& other) { CopyFrom(other); }
ConstBlobDescProto::_BlobDescProto_::_BlobDescProto_(const ::oneflow::BlobDescProto& proto_blobdescproto) {
  InitFromProto(proto_blobdescproto);
}
ConstBlobDescProto::_BlobDescProto_::_BlobDescProto_(_BlobDescProto_&& other) = default;
ConstBlobDescProto::_BlobDescProto_::~_BlobDescProto_() = default;

void ConstBlobDescProto::_BlobDescProto_::InitFromProto(const ::oneflow::BlobDescProto& proto_blobdescproto) {
  Clear();
  // required_or_optional field: shape
  if (proto_blobdescproto.has_shape()) {
  *mutable_shape() = ::oneflow::cfg::ShapeProto(proto_blobdescproto.shape());      
  }
  // required_or_optional field: data_type
  if (proto_blobdescproto.has_data_type()) {
  set_data_type(static_cast<std::remove_reference<std::remove_const<decltype(data_type())>::type>::type>(proto_blobdescproto.data_type()));
  }
  // required_or_optional field: is_dynamic
  if (proto_blobdescproto.has_is_dynamic()) {
    set_is_dynamic(proto_blobdescproto.is_dynamic());
  }
    
}

void ConstBlobDescProto::_BlobDescProto_::ToProto(::oneflow::BlobDescProto* proto_blobdescproto) const {
  proto_blobdescproto->Clear();
  // required_or_optional field: shape
  if (this->has_shape()) {
    ::oneflow::ShapeProto proto_shape;
    shape().ToProto(&proto_shape);
    proto_blobdescproto->mutable_shape()->CopyFrom(proto_shape);
    }
  // required_or_optional field: data_type
  if (this->has_data_type()) {
    proto_blobdescproto->set_data_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_blobdescproto->data_type())>::type>::type>(data_type()));
    }
  // required_or_optional field: is_dynamic
  if (this->has_is_dynamic()) {
    proto_blobdescproto->set_is_dynamic(is_dynamic());
    }

}

::std::string ConstBlobDescProto::_BlobDescProto_::DebugString() const {
  ::oneflow::BlobDescProto proto_blobdescproto;
  this->ToProto(&proto_blobdescproto);
  return proto_blobdescproto.DebugString();
}

void ConstBlobDescProto::_BlobDescProto_::Clear() {
  clear_shape();
  clear_data_type();
  clear_is_dynamic();
}

void ConstBlobDescProto::_BlobDescProto_::CopyFrom(const _BlobDescProto_& other) {
  if (other.has_shape()) {
    mutable_shape()->CopyFrom(other.shape());
  } else {
    clear_shape();
  }
  if (other.has_data_type()) {
    set_data_type(other.data_type());
  } else {
    clear_data_type();
  }
  if (other.has_is_dynamic()) {
    set_is_dynamic(other.is_dynamic());
  } else {
    clear_is_dynamic();
  }
}


// optional field shape
bool ConstBlobDescProto::_BlobDescProto_::has_shape() const {
  return has_shape_;
}
const ::oneflow::cfg::ShapeProto& ConstBlobDescProto::_BlobDescProto_::shape() const {
  if (!shape_) {
    static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
  }
  return *(shape_.get());
}
void ConstBlobDescProto::_BlobDescProto_::clear_shape() {
  if (shape_) {
    shape_->Clear();
  }
  has_shape_ = false;
}
::oneflow::cfg::ShapeProto* ConstBlobDescProto::_BlobDescProto_::mutable_shape() {
  if (!shape_) {
    shape_ = ::std::make_shared<::oneflow::cfg::ShapeProto>();
  }
  has_shape_ = true;
  return shape_.get();
}

// optional field data_type
bool ConstBlobDescProto::_BlobDescProto_::has_data_type() const {
  return has_data_type_;
}
const ::oneflow::cfg::DataType& ConstBlobDescProto::_BlobDescProto_::data_type() const {
  if (has_data_type_) { return data_type_; }
  static const ::oneflow::cfg::DataType default_static_value = ::oneflow::cfg::DataType();
  return default_static_value;
}
void ConstBlobDescProto::_BlobDescProto_::clear_data_type() {
  has_data_type_ = false;
}
void ConstBlobDescProto::_BlobDescProto_::set_data_type(const ::oneflow::cfg::DataType& value) {
  data_type_ = value;
  has_data_type_ = true;
}
::oneflow::cfg::DataType* ConstBlobDescProto::_BlobDescProto_::mutable_data_type() {
  has_data_type_ = true;
  return &data_type_;
}

// optional field is_dynamic
bool ConstBlobDescProto::_BlobDescProto_::has_is_dynamic() const {
  return has_is_dynamic_;
}
const bool& ConstBlobDescProto::_BlobDescProto_::is_dynamic() const {
  if (has_is_dynamic_) { return is_dynamic_; }
  static const bool default_static_value = bool();
  return default_static_value;
}
void ConstBlobDescProto::_BlobDescProto_::clear_is_dynamic() {
  has_is_dynamic_ = false;
}
void ConstBlobDescProto::_BlobDescProto_::set_is_dynamic(const bool& value) {
  is_dynamic_ = value;
  has_is_dynamic_ = true;
}
bool* ConstBlobDescProto::_BlobDescProto_::mutable_is_dynamic() {
  has_is_dynamic_ = true;
  return &is_dynamic_;
}


int ConstBlobDescProto::_BlobDescProto_::compare(const _BlobDescProto_& other) {
  if (!(has_shape() == other.has_shape())) {
    return has_shape() < other.has_shape() ? -1 : 1;
  } else if (!(shape() == other.shape())) {
    return shape() < other.shape() ? -1 : 1;
  }
  if (!(has_data_type() == other.has_data_type())) {
    return has_data_type() < other.has_data_type() ? -1 : 1;
  } else if (!(data_type() == other.data_type())) {
    return data_type() < other.data_type() ? -1 : 1;
  }
  if (!(has_is_dynamic() == other.has_is_dynamic())) {
    return has_is_dynamic() < other.has_is_dynamic() ? -1 : 1;
  } else if (!(is_dynamic() == other.is_dynamic())) {
    return is_dynamic() < other.is_dynamic() ? -1 : 1;
  }
  return 0;
}

bool ConstBlobDescProto::_BlobDescProto_::operator==(const _BlobDescProto_& other) const {
  return true
    && has_shape() == other.has_shape() 
    && shape() == other.shape()
    && has_data_type() == other.has_data_type() 
    && data_type() == other.data_type()
    && has_is_dynamic() == other.has_is_dynamic() 
    && is_dynamic() == other.is_dynamic()
  ;
}

std::size_t ConstBlobDescProto::_BlobDescProto_::__CalcHash__() const {
  return 0
    ^ (has_shape() ? std::hash<::oneflow::cfg::ShapeProto>()(shape()) : 0)
    ^ (has_data_type() ? std::hash<::oneflow::cfg::DataType>()(data_type()) : 0)
    ^ (has_is_dynamic() ? std::hash<bool>()(is_dynamic()) : 0)
  ;
}

bool ConstBlobDescProto::_BlobDescProto_::operator<(const _BlobDescProto_& other) const {
  return false
    || !(has_shape() == other.has_shape()) ? 
      has_shape() < other.has_shape() : false
    || !(shape() == other.shape()) ? 
      shape() < other.shape() : false
    || !(has_data_type() == other.has_data_type()) ? 
      has_data_type() < other.has_data_type() : false
    || !(data_type() == other.data_type()) ? 
      data_type() < other.data_type() : false
    || !(has_is_dynamic() == other.has_is_dynamic()) ? 
      has_is_dynamic() < other.has_is_dynamic() : false
    || !(is_dynamic() == other.is_dynamic()) ? 
      is_dynamic() < other.is_dynamic() : false
  ;
}

using _BlobDescProto_ =  ConstBlobDescProto::_BlobDescProto_;
ConstBlobDescProto::ConstBlobDescProto(const ::std::shared_ptr<_BlobDescProto_>& data): data_(data) {}
ConstBlobDescProto::ConstBlobDescProto(): data_(::std::make_shared<_BlobDescProto_>()) {}
ConstBlobDescProto::ConstBlobDescProto(const ::oneflow::BlobDescProto& proto_blobdescproto) {
  BuildFromProto(proto_blobdescproto);
}
ConstBlobDescProto::ConstBlobDescProto(const ConstBlobDescProto&) = default;
ConstBlobDescProto::ConstBlobDescProto(ConstBlobDescProto&&) noexcept = default;
ConstBlobDescProto::~ConstBlobDescProto() = default;

void ConstBlobDescProto::ToProto(PbMessage* proto_blobdescproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobDescProto*>(proto_blobdescproto));
}
  
::std::string ConstBlobDescProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobDescProto::__Empty__() const {
  return !data_;
}

int ConstBlobDescProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"shape", 1},
    {"data_type", 2},
    {"is_dynamic", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBlobDescProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBlobDescProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobDescProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &shape();
    case 2: return &data_type();
    case 3: return &is_dynamic();
    default: return nullptr;
  }
}

// required or optional field shape
bool ConstBlobDescProto::has_shape() const {
  return __SharedPtrOrDefault__()->has_shape();
}
const ::oneflow::cfg::ShapeProto& ConstBlobDescProto::shape() const {
  return __SharedPtrOrDefault__()->shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstBlobDescProto::shared_const_shape() const {
  return shape().__SharedConst__();
}
// required or optional field data_type
bool ConstBlobDescProto::has_data_type() const {
  return __SharedPtrOrDefault__()->has_data_type();
}
const ::oneflow::cfg::DataType& ConstBlobDescProto::data_type() const {
  return __SharedPtrOrDefault__()->data_type();
}
// used by pybind11 only
// required or optional field is_dynamic
bool ConstBlobDescProto::has_is_dynamic() const {
  return __SharedPtrOrDefault__()->has_is_dynamic();
}
const bool& ConstBlobDescProto::is_dynamic() const {
  return __SharedPtrOrDefault__()->is_dynamic();
}
// used by pybind11 only

::std::shared_ptr<ConstBlobDescProto> ConstBlobDescProto::__SharedConst__() const {
  return ::std::make_shared<ConstBlobDescProto>(data_);
}
int64_t ConstBlobDescProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobDescProto::operator==(const ConstBlobDescProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobDescProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobDescProto::operator<(const ConstBlobDescProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobDescProto_>& ConstBlobDescProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobDescProto_> default_ptr = std::make_shared<_BlobDescProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobDescProto_>& ConstBlobDescProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobDescProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobDescProto
void ConstBlobDescProto::BuildFromProto(const PbMessage& proto_blobdescproto) {
  data_ = ::std::make_shared<_BlobDescProto_>(dynamic_cast<const ::oneflow::BlobDescProto&>(proto_blobdescproto));
}

BlobDescProto::BlobDescProto(const ::std::shared_ptr<_BlobDescProto_>& data)
  : ConstBlobDescProto(data) {}
BlobDescProto::BlobDescProto(const BlobDescProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobDescProto> resize
BlobDescProto::BlobDescProto(BlobDescProto&&) noexcept = default; 
BlobDescProto::BlobDescProto(const ::oneflow::BlobDescProto& proto_blobdescproto) {
  InitFromProto(proto_blobdescproto);
}
BlobDescProto::BlobDescProto() = default;

BlobDescProto::~BlobDescProto() = default;

void BlobDescProto::InitFromProto(const PbMessage& proto_blobdescproto) {
  BuildFromProto(proto_blobdescproto);
}
  
void* BlobDescProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_shape();
    case 2: return mutable_data_type();
    case 3: return mutable_is_dynamic();
    default: return nullptr;
  }
}

bool BlobDescProto::operator==(const BlobDescProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobDescProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobDescProto::operator<(const BlobDescProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobDescProto::Clear() {
  if (data_) { data_.reset(); }
}
void BlobDescProto::CopyFrom(const BlobDescProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobDescProto& BlobDescProto::operator=(const BlobDescProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field shape
void BlobDescProto::clear_shape() {
  return __SharedPtr__()->clear_shape();
}
::oneflow::cfg::ShapeProto* BlobDescProto::mutable_shape() {
  return __SharedPtr__()->mutable_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> BlobDescProto::shared_mutable_shape() {
  return mutable_shape()->__SharedMutable__();
}
// required or optional field data_type
void BlobDescProto::clear_data_type() {
  return __SharedPtr__()->clear_data_type();
}
void BlobDescProto::set_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_data_type(value);
}
::oneflow::cfg::DataType* BlobDescProto::mutable_data_type() {
  return  __SharedPtr__()->mutable_data_type();
}
// required or optional field is_dynamic
void BlobDescProto::clear_is_dynamic() {
  return __SharedPtr__()->clear_is_dynamic();
}
void BlobDescProto::set_is_dynamic(const bool& value) {
  return __SharedPtr__()->set_is_dynamic(value);
}
bool* BlobDescProto::mutable_is_dynamic() {
  return  __SharedPtr__()->mutable_is_dynamic();
}

::std::shared_ptr<BlobDescProto> BlobDescProto::__SharedMutable__() {
  return ::std::make_shared<BlobDescProto>(__SharedPtr__());
}
ConstBlobDescSignature::_BlobDescSignature_::_BlobDescSignature_() { Clear(); }
ConstBlobDescSignature::_BlobDescSignature_::_BlobDescSignature_(const _BlobDescSignature_& other) { CopyFrom(other); }
ConstBlobDescSignature::_BlobDescSignature_::_BlobDescSignature_(const ::oneflow::BlobDescSignature& proto_blobdescsignature) {
  InitFromProto(proto_blobdescsignature);
}
ConstBlobDescSignature::_BlobDescSignature_::_BlobDescSignature_(_BlobDescSignature_&& other) = default;
ConstBlobDescSignature::_BlobDescSignature_::~_BlobDescSignature_() = default;

void ConstBlobDescSignature::_BlobDescSignature_::InitFromProto(const ::oneflow::BlobDescSignature& proto_blobdescsignature) {
  Clear();
  // map field : bn_in_op2blob_desc
  if (!proto_blobdescsignature.bn_in_op2blob_desc().empty()) {
_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_&  mut_bn_in_op2blob_desc = *mutable_bn_in_op2blob_desc();
    for (const auto& pair : proto_blobdescsignature.bn_in_op2blob_desc()) {
      mut_bn_in_op2blob_desc[pair.first] = ::oneflow::cfg::BlobDescProto(pair.second);
    }
  }
    
}

void ConstBlobDescSignature::_BlobDescSignature_::ToProto(::oneflow::BlobDescSignature* proto_blobdescsignature) const {
  proto_blobdescsignature->Clear();
  // map field : bn_in_op2blob_desc
  if (!bn_in_op2blob_desc().empty()) {
    auto& mut_bn_in_op2blob_desc = *(proto_blobdescsignature->mutable_bn_in_op2blob_desc());
    for (const auto& pair : bn_in_op2blob_desc()) {
      ::oneflow::BlobDescProto proto_bn_in_op2blob_desc_value;
      pair.second.ToProto(&proto_bn_in_op2blob_desc_value);
      mut_bn_in_op2blob_desc[pair.first] = proto_bn_in_op2blob_desc_value;
    }
  }

}

::std::string ConstBlobDescSignature::_BlobDescSignature_::DebugString() const {
  ::oneflow::BlobDescSignature proto_blobdescsignature;
  this->ToProto(&proto_blobdescsignature);
  return proto_blobdescsignature.DebugString();
}

void ConstBlobDescSignature::_BlobDescSignature_::Clear() {
  clear_bn_in_op2blob_desc();
}

void ConstBlobDescSignature::_BlobDescSignature_::CopyFrom(const _BlobDescSignature_& other) {
  mutable_bn_in_op2blob_desc()->CopyFrom(other.bn_in_op2blob_desc());
}


::std::size_t ConstBlobDescSignature::_BlobDescSignature_::bn_in_op2blob_desc_size() const {
  if (!bn_in_op2blob_desc_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>();
    return default_static_value->size();
  }
  return bn_in_op2blob_desc_->size();
}
const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& ConstBlobDescSignature::_BlobDescSignature_::bn_in_op2blob_desc() const {
  if (!bn_in_op2blob_desc_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2blob_desc_.get());
}

_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ * ConstBlobDescSignature::_BlobDescSignature_::mutable_bn_in_op2blob_desc() {
  if (!bn_in_op2blob_desc_) {
    bn_in_op2blob_desc_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>();
  }
  return bn_in_op2blob_desc_.get();
}

const ::oneflow::cfg::BlobDescProto& ConstBlobDescSignature::_BlobDescSignature_::bn_in_op2blob_desc(::std::string key) const {
  if (!bn_in_op2blob_desc_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>();
    return default_static_value->at(key);
  }
  return bn_in_op2blob_desc_->at(key);
}

void ConstBlobDescSignature::_BlobDescSignature_::clear_bn_in_op2blob_desc() {
  if (!bn_in_op2blob_desc_) {
    bn_in_op2blob_desc_ = ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>();
  }
  return bn_in_op2blob_desc_->Clear();
}



int ConstBlobDescSignature::_BlobDescSignature_::compare(const _BlobDescSignature_& other) {
  if (!(bn_in_op2blob_desc() == other.bn_in_op2blob_desc())) {
    return bn_in_op2blob_desc() < other.bn_in_op2blob_desc() ? -1 : 1;
  }
  return 0;
}

bool ConstBlobDescSignature::_BlobDescSignature_::operator==(const _BlobDescSignature_& other) const {
  return true
    && bn_in_op2blob_desc() == other.bn_in_op2blob_desc()
  ;
}

std::size_t ConstBlobDescSignature::_BlobDescSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2blob_desc().__CalcHash__()
  ;
}

bool ConstBlobDescSignature::_BlobDescSignature_::operator<(const _BlobDescSignature_& other) const {
  return false
    || !(bn_in_op2blob_desc() == other.bn_in_op2blob_desc()) ? 
      bn_in_op2blob_desc() < other.bn_in_op2blob_desc() : false
  ;
}

using _BlobDescSignature_ =  ConstBlobDescSignature::_BlobDescSignature_;
ConstBlobDescSignature::ConstBlobDescSignature(const ::std::shared_ptr<_BlobDescSignature_>& data): data_(data) {}
ConstBlobDescSignature::ConstBlobDescSignature(): data_(::std::make_shared<_BlobDescSignature_>()) {}
ConstBlobDescSignature::ConstBlobDescSignature(const ::oneflow::BlobDescSignature& proto_blobdescsignature) {
  BuildFromProto(proto_blobdescsignature);
}
ConstBlobDescSignature::ConstBlobDescSignature(const ConstBlobDescSignature&) = default;
ConstBlobDescSignature::ConstBlobDescSignature(ConstBlobDescSignature&&) noexcept = default;
ConstBlobDescSignature::~ConstBlobDescSignature() = default;

void ConstBlobDescSignature::ToProto(PbMessage* proto_blobdescsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobDescSignature*>(proto_blobdescsignature));
}
  
::std::string ConstBlobDescSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobDescSignature::__Empty__() const {
  return !data_;
}

int ConstBlobDescSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2blob_desc", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBlobDescSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBlobDescSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobDescSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2blob_desc();
    default: return nullptr;
  }
}

// map field bn_in_op2blob_desc
::std::size_t ConstBlobDescSignature::bn_in_op2blob_desc_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_desc_size();
}

const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& ConstBlobDescSignature::bn_in_op2blob_desc() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_desc();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> ConstBlobDescSignature::shared_const_bn_in_op2blob_desc() const {
  return bn_in_op2blob_desc().__SharedConst__();
}

::std::shared_ptr<ConstBlobDescSignature> ConstBlobDescSignature::__SharedConst__() const {
  return ::std::make_shared<ConstBlobDescSignature>(data_);
}
int64_t ConstBlobDescSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobDescSignature::operator==(const ConstBlobDescSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobDescSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobDescSignature::operator<(const ConstBlobDescSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobDescSignature_>& ConstBlobDescSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobDescSignature_> default_ptr = std::make_shared<_BlobDescSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobDescSignature_>& ConstBlobDescSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobDescSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobDescSignature
void ConstBlobDescSignature::BuildFromProto(const PbMessage& proto_blobdescsignature) {
  data_ = ::std::make_shared<_BlobDescSignature_>(dynamic_cast<const ::oneflow::BlobDescSignature&>(proto_blobdescsignature));
}

BlobDescSignature::BlobDescSignature(const ::std::shared_ptr<_BlobDescSignature_>& data)
  : ConstBlobDescSignature(data) {}
BlobDescSignature::BlobDescSignature(const BlobDescSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobDescSignature> resize
BlobDescSignature::BlobDescSignature(BlobDescSignature&&) noexcept = default; 
BlobDescSignature::BlobDescSignature(const ::oneflow::BlobDescSignature& proto_blobdescsignature) {
  InitFromProto(proto_blobdescsignature);
}
BlobDescSignature::BlobDescSignature() = default;

BlobDescSignature::~BlobDescSignature() = default;

void BlobDescSignature::InitFromProto(const PbMessage& proto_blobdescsignature) {
  BuildFromProto(proto_blobdescsignature);
}
  
void* BlobDescSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2blob_desc();
    default: return nullptr;
  }
}

bool BlobDescSignature::operator==(const BlobDescSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobDescSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobDescSignature::operator<(const BlobDescSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobDescSignature::Clear() {
  if (data_) { data_.reset(); }
}
void BlobDescSignature::CopyFrom(const BlobDescSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobDescSignature& BlobDescSignature::operator=(const BlobDescSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2blob_desc
void BlobDescSignature::clear_bn_in_op2blob_desc() {
  return __SharedPtr__()->clear_bn_in_op2blob_desc();
}

const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_ & BlobDescSignature::bn_in_op2blob_desc() const {
  return __SharedConst__()->bn_in_op2blob_desc();
}

_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_* BlobDescSignature::mutable_bn_in_op2blob_desc() {
  return __SharedPtr__()->mutable_bn_in_op2blob_desc();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> BlobDescSignature::shared_mutable_bn_in_op2blob_desc() {
  return mutable_bn_in_op2blob_desc()->__SharedMutable__();
}

::std::shared_ptr<BlobDescSignature> BlobDescSignature::__SharedMutable__() {
  return ::std::make_shared<BlobDescSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto>(data) {}
Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_() = default;
Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::~Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_() = default;

bool Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::BlobDescProto>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::BlobDescProto& Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstBlobDescProto> Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ConstBlobDescProto> Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ConstBlobDescProto> Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data): Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_(data) {}
_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_() = default;
_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::~_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_() = default;

void _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::operator==(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::BlobDescProto>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::operator<(const _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_> _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::BlobDescProto> _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ::oneflow::cfg::BlobDescProto> _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_, ::oneflow::cfg::BlobDescProto> _CFG_ONEFLOW_CORE_REGISTER_BLOB_DESC_CFG_H__MapField___std__string_BlobDescProto_::shared_mut_end() { return end(); }

}
} // namespace oneflow
