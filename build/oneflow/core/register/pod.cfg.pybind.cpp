#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/register/pod.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.register.pod", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::FieldKey> enm(m, "FieldKey");
    enm.value("kInvalidFieldKey", ::oneflow::cfg::kInvalidFieldKey);
    enm.value("kTensorShape", ::oneflow::cfg::kTensorShape);
    enm.value("kFieldKeySize", ::oneflow::cfg::kFieldKeySize);
    m.attr("kInvalidFieldKey") = enm.attr("kInvalidFieldKey");
    m.attr("kTensorShape") = enm.attr("kTensorShape");
    m.attr("kFieldKeySize") = enm.attr("kFieldKeySize");
  }


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstFieldPodProto> (Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstFieldPodProto> (Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_&))&_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(const _CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_&))&_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(const ::oneflow::cfg::FieldPodProto&))&_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::FieldPodProto> (_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::FieldPodProto> (_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstTensorPodProto, ::oneflow::cfg::Message, std::shared_ptr<ConstTensorPodProto>> registry(m, "ConstTensorPodProto");
    registry.def("__id__", &::oneflow::cfg::TensorPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstTensorPodProto::DebugString);
    registry.def("__repr__", &ConstTensorPodProto::DebugString);

    registry.def("has_shape", &ConstTensorPodProto::has_shape);
    registry.def("shape", &ConstTensorPodProto::shared_const_shape);

    registry.def("has_data_type", &ConstTensorPodProto::has_data_type);
    registry.def("data_type", &ConstTensorPodProto::data_type);
  }
  {
    pybind11::class_<::oneflow::cfg::TensorPodProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::TensorPodProto>> registry(m, "TensorPodProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::TensorPodProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::TensorPodProto::*)(const ConstTensorPodProto&))&::oneflow::cfg::TensorPodProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::TensorPodProto::*)(const ::oneflow::cfg::TensorPodProto&))&::oneflow::cfg::TensorPodProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::TensorPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::TensorPodProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::TensorPodProto::DebugString);



    registry.def("has_shape", &::oneflow::cfg::TensorPodProto::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::TensorPodProto::clear_shape);
    registry.def("shape", &::oneflow::cfg::TensorPodProto::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::TensorPodProto::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::TensorPodProto::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::TensorPodProto::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::TensorPodProto::data_type);
    registry.def("set_data_type", &::oneflow::cfg::TensorPodProto::set_data_type);
  }
  {
    pybind11::class_<ConstStructPodProto, ::oneflow::cfg::Message, std::shared_ptr<ConstStructPodProto>> registry(m, "ConstStructPodProto");
    registry.def("__id__", &::oneflow::cfg::StructPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstStructPodProto::DebugString);
    registry.def("__repr__", &ConstStructPodProto::DebugString);

    registry.def("field_size", &ConstStructPodProto::field_size);
    registry.def("field", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> (ConstStructPodProto::*)() const)&ConstStructPodProto::shared_const_field);
    registry.def("field", (::std::shared_ptr<ConstFieldPodProto> (ConstStructPodProto::*)(::std::size_t) const)&ConstStructPodProto::shared_const_field);
  }
  {
    pybind11::class_<::oneflow::cfg::StructPodProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::StructPodProto>> registry(m, "StructPodProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::StructPodProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::StructPodProto::*)(const ConstStructPodProto&))&::oneflow::cfg::StructPodProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::StructPodProto::*)(const ::oneflow::cfg::StructPodProto&))&::oneflow::cfg::StructPodProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::StructPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::StructPodProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::StructPodProto::DebugString);



    registry.def("field_size", &::oneflow::cfg::StructPodProto::field_size);
    registry.def("clear_field", &::oneflow::cfg::StructPodProto::clear_field);
    registry.def("mutable_field", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> (::oneflow::cfg::StructPodProto::*)())&::oneflow::cfg::StructPodProto::shared_mutable_field);
    registry.def("field", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_POD_CFG_H__RepeatedField_FieldPodProto_> (::oneflow::cfg::StructPodProto::*)() const)&::oneflow::cfg::StructPodProto::shared_const_field);
    registry.def("field", (::std::shared_ptr<ConstFieldPodProto> (::oneflow::cfg::StructPodProto::*)(::std::size_t) const)&::oneflow::cfg::StructPodProto::shared_const_field);
    registry.def("mutable_field", (::std::shared_ptr<::oneflow::cfg::FieldPodProto> (::oneflow::cfg::StructPodProto::*)(::std::size_t))&::oneflow::cfg::StructPodProto::shared_mutable_field);
  }
  {
    pybind11::class_<ConstFieldId, ::oneflow::cfg::Message, std::shared_ptr<ConstFieldId>> registry(m, "ConstFieldId");
    registry.def("__id__", &::oneflow::cfg::FieldId::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstFieldId::DebugString);
    registry.def("__repr__", &ConstFieldId::DebugString);

    registry.def("has_key", &ConstFieldId::has_key);
    registry.def("key", &ConstFieldId::key);

    registry.def("has_lbi", &ConstFieldId::has_lbi);
    registry.def("lbi", &ConstFieldId::shared_const_lbi);
    registry.def("field_id_type_case",  &ConstFieldId::field_id_type_case);
    registry.def_property_readonly_static("FIELD_ID_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::FieldId::FIELD_ID_TYPE_NOT_SET; })
        .def_property_readonly_static("kKey", [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kKey; })
        .def_property_readonly_static("kLbi", [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kLbi; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::FieldId, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::FieldId>> registry(m, "FieldId");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::FieldId::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::FieldId::*)(const ConstFieldId&))&::oneflow::cfg::FieldId::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::FieldId::*)(const ::oneflow::cfg::FieldId&))&::oneflow::cfg::FieldId::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::FieldId::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::FieldId::DebugString);
    registry.def("__repr__", &::oneflow::cfg::FieldId::DebugString);

    registry.def_property_readonly_static("FIELD_ID_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::FieldId::FIELD_ID_TYPE_NOT_SET; })
        .def_property_readonly_static("kKey", [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kKey; })
        .def_property_readonly_static("kLbi", [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kLbi; })
        ;


    registry.def("has_key", &::oneflow::cfg::FieldId::has_key);
    registry.def("clear_key", &::oneflow::cfg::FieldId::clear_key);
    registry.def_property_readonly_static("kKey",
        [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kKey; });
    registry.def("key", &::oneflow::cfg::FieldId::key);
    registry.def("set_key", &::oneflow::cfg::FieldId::set_key);

    registry.def("has_lbi", &::oneflow::cfg::FieldId::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::FieldId::clear_lbi);
    registry.def_property_readonly_static("kLbi",
        [](const pybind11::object&){ return ::oneflow::cfg::FieldId::kLbi; });
    registry.def("lbi", &::oneflow::cfg::FieldId::lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::FieldId::shared_mutable_lbi);
    pybind11::enum_<::oneflow::cfg::FieldId::FieldIdTypeCase>(registry, "FieldIdTypeCase")
        .value("FIELD_ID_TYPE_NOT_SET", ::oneflow::cfg::FieldId::FIELD_ID_TYPE_NOT_SET)
        .value("kKey", ::oneflow::cfg::FieldId::kKey)
        .value("kLbi", ::oneflow::cfg::FieldId::kLbi)
        ;
    registry.def("field_id_type_case",  &::oneflow::cfg::FieldId::field_id_type_case);
  }
  {
    pybind11::class_<ConstFieldPodProto, ::oneflow::cfg::Message, std::shared_ptr<ConstFieldPodProto>> registry(m, "ConstFieldPodProto");
    registry.def("__id__", &::oneflow::cfg::FieldPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstFieldPodProto::DebugString);
    registry.def("__repr__", &ConstFieldPodProto::DebugString);

    registry.def("has_field_id", &ConstFieldPodProto::has_field_id);
    registry.def("field_id", &ConstFieldPodProto::shared_const_field_id);

    registry.def("has_alignment", &ConstFieldPodProto::has_alignment);
    registry.def("alignment", &ConstFieldPodProto::alignment);

    registry.def("has_pod", &ConstFieldPodProto::has_pod);
    registry.def("pod", &ConstFieldPodProto::shared_const_pod);
  }
  {
    pybind11::class_<::oneflow::cfg::FieldPodProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::FieldPodProto>> registry(m, "FieldPodProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::FieldPodProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::FieldPodProto::*)(const ConstFieldPodProto&))&::oneflow::cfg::FieldPodProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::FieldPodProto::*)(const ::oneflow::cfg::FieldPodProto&))&::oneflow::cfg::FieldPodProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::FieldPodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::FieldPodProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::FieldPodProto::DebugString);



    registry.def("has_field_id", &::oneflow::cfg::FieldPodProto::has_field_id);
    registry.def("clear_field_id", &::oneflow::cfg::FieldPodProto::clear_field_id);
    registry.def("field_id", &::oneflow::cfg::FieldPodProto::shared_const_field_id);
    registry.def("mutable_field_id", &::oneflow::cfg::FieldPodProto::shared_mutable_field_id);

    registry.def("has_alignment", &::oneflow::cfg::FieldPodProto::has_alignment);
    registry.def("clear_alignment", &::oneflow::cfg::FieldPodProto::clear_alignment);
    registry.def("alignment", &::oneflow::cfg::FieldPodProto::alignment);
    registry.def("set_alignment", &::oneflow::cfg::FieldPodProto::set_alignment);

    registry.def("has_pod", &::oneflow::cfg::FieldPodProto::has_pod);
    registry.def("clear_pod", &::oneflow::cfg::FieldPodProto::clear_pod);
    registry.def("pod", &::oneflow::cfg::FieldPodProto::shared_const_pod);
    registry.def("mutable_pod", &::oneflow::cfg::FieldPodProto::shared_mutable_pod);
  }
  {
    pybind11::class_<ConstPodProto, ::oneflow::cfg::Message, std::shared_ptr<ConstPodProto>> registry(m, "ConstPodProto");
    registry.def("__id__", &::oneflow::cfg::PodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPodProto::DebugString);
    registry.def("__repr__", &ConstPodProto::DebugString);

    registry.def("has_tensor_pod", &ConstPodProto::has_tensor_pod);
    registry.def("tensor_pod", &ConstPodProto::shared_const_tensor_pod);

    registry.def("has_struct_pod", &ConstPodProto::has_struct_pod);
    registry.def("struct_pod", &ConstPodProto::shared_const_struct_pod);
    registry.def("pod_type_case",  &ConstPodProto::pod_type_case);
    registry.def_property_readonly_static("POD_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::PodProto::POD_TYPE_NOT_SET; })
        .def_property_readonly_static("kTensorPod", [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kTensorPod; })
        .def_property_readonly_static("kStructPod", [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kStructPod; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::PodProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PodProto>> registry(m, "PodProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PodProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PodProto::*)(const ConstPodProto&))&::oneflow::cfg::PodProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PodProto::*)(const ::oneflow::cfg::PodProto&))&::oneflow::cfg::PodProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PodProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PodProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PodProto::DebugString);

    registry.def_property_readonly_static("POD_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::PodProto::POD_TYPE_NOT_SET; })
        .def_property_readonly_static("kTensorPod", [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kTensorPod; })
        .def_property_readonly_static("kStructPod", [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kStructPod; })
        ;


    registry.def("has_tensor_pod", &::oneflow::cfg::PodProto::has_tensor_pod);
    registry.def("clear_tensor_pod", &::oneflow::cfg::PodProto::clear_tensor_pod);
    registry.def_property_readonly_static("kTensorPod",
        [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kTensorPod; });
    registry.def("tensor_pod", &::oneflow::cfg::PodProto::tensor_pod);
    registry.def("mutable_tensor_pod", &::oneflow::cfg::PodProto::shared_mutable_tensor_pod);

    registry.def("has_struct_pod", &::oneflow::cfg::PodProto::has_struct_pod);
    registry.def("clear_struct_pod", &::oneflow::cfg::PodProto::clear_struct_pod);
    registry.def_property_readonly_static("kStructPod",
        [](const pybind11::object&){ return ::oneflow::cfg::PodProto::kStructPod; });
    registry.def("struct_pod", &::oneflow::cfg::PodProto::struct_pod);
    registry.def("mutable_struct_pod", &::oneflow::cfg::PodProto::shared_mutable_struct_pod);
    pybind11::enum_<::oneflow::cfg::PodProto::PodTypeCase>(registry, "PodTypeCase")
        .value("POD_TYPE_NOT_SET", ::oneflow::cfg::PodProto::POD_TYPE_NOT_SET)
        .value("kTensorPod", ::oneflow::cfg::PodProto::kTensorPod)
        .value("kStructPod", ::oneflow::cfg::PodProto::kStructPod)
        ;
    registry.def("pod_type_case",  &::oneflow::cfg::PodProto::pod_type_case);
  }
}