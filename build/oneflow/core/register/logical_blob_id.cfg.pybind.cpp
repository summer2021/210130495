#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.register.logical_blob_id", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstLogicalBlobIdPair> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstLogicalBlobIdPair> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(const ::oneflow::cfg::LogicalBlobIdPair&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstLogicalBlobId> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstLogicalBlobId> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(const ::oneflow::cfg::LogicalBlobId&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>> registry(m, "Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstLogicalBlobId>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstLogicalBlobId>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstLogicalBlobId> (Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>> registry(m, "_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::*)(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::*)(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<LogicalBlobId>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<LogicalBlobId>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstLogicalBlobId, ::oneflow::cfg::Message, std::shared_ptr<ConstLogicalBlobId>> registry(m, "ConstLogicalBlobId");
    registry.def("__id__", &::oneflow::cfg::LogicalBlobId::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLogicalBlobId::DebugString);
    registry.def("__repr__", &ConstLogicalBlobId::DebugString);

    registry.def("has_op_name", &ConstLogicalBlobId::has_op_name);
    registry.def("op_name", &ConstLogicalBlobId::op_name);

    registry.def("has_blob_name", &ConstLogicalBlobId::has_blob_name);
    registry.def("blob_name", &ConstLogicalBlobId::blob_name);
  }
  {
    pybind11::class_<::oneflow::cfg::LogicalBlobId, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LogicalBlobId>> registry(m, "LogicalBlobId");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LogicalBlobId::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobId::*)(const ConstLogicalBlobId&))&::oneflow::cfg::LogicalBlobId::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobId::*)(const ::oneflow::cfg::LogicalBlobId&))&::oneflow::cfg::LogicalBlobId::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LogicalBlobId::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LogicalBlobId::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LogicalBlobId::DebugString);



    registry.def("has_op_name", &::oneflow::cfg::LogicalBlobId::has_op_name);
    registry.def("clear_op_name", &::oneflow::cfg::LogicalBlobId::clear_op_name);
    registry.def("op_name", &::oneflow::cfg::LogicalBlobId::op_name);
    registry.def("set_op_name", &::oneflow::cfg::LogicalBlobId::set_op_name);

    registry.def("has_blob_name", &::oneflow::cfg::LogicalBlobId::has_blob_name);
    registry.def("clear_blob_name", &::oneflow::cfg::LogicalBlobId::clear_blob_name);
    registry.def("blob_name", &::oneflow::cfg::LogicalBlobId::blob_name);
    registry.def("set_blob_name", &::oneflow::cfg::LogicalBlobId::set_blob_name);
  }
  {
    pybind11::class_<ConstLogicalBlobIdPair, ::oneflow::cfg::Message, std::shared_ptr<ConstLogicalBlobIdPair>> registry(m, "ConstLogicalBlobIdPair");
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdPair::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLogicalBlobIdPair::DebugString);
    registry.def("__repr__", &ConstLogicalBlobIdPair::DebugString);

    registry.def("has_first", &ConstLogicalBlobIdPair::has_first);
    registry.def("first", &ConstLogicalBlobIdPair::shared_const_first);

    registry.def("has_second", &ConstLogicalBlobIdPair::has_second);
    registry.def("second", &ConstLogicalBlobIdPair::shared_const_second);
  }
  {
    pybind11::class_<::oneflow::cfg::LogicalBlobIdPair, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair>> registry(m, "LogicalBlobIdPair");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LogicalBlobIdPair::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdPair::*)(const ConstLogicalBlobIdPair&))&::oneflow::cfg::LogicalBlobIdPair::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdPair::*)(const ::oneflow::cfg::LogicalBlobIdPair&))&::oneflow::cfg::LogicalBlobIdPair::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdPair::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LogicalBlobIdPair::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LogicalBlobIdPair::DebugString);



    registry.def("has_first", &::oneflow::cfg::LogicalBlobIdPair::has_first);
    registry.def("clear_first", &::oneflow::cfg::LogicalBlobIdPair::clear_first);
    registry.def("first", &::oneflow::cfg::LogicalBlobIdPair::shared_const_first);
    registry.def("mutable_first", &::oneflow::cfg::LogicalBlobIdPair::shared_mutable_first);

    registry.def("has_second", &::oneflow::cfg::LogicalBlobIdPair::has_second);
    registry.def("clear_second", &::oneflow::cfg::LogicalBlobIdPair::clear_second);
    registry.def("second", &::oneflow::cfg::LogicalBlobIdPair::shared_const_second);
    registry.def("mutable_second", &::oneflow::cfg::LogicalBlobIdPair::shared_mutable_second);
  }
  {
    pybind11::class_<ConstLogicalBlobIdPairs, ::oneflow::cfg::Message, std::shared_ptr<ConstLogicalBlobIdPairs>> registry(m, "ConstLogicalBlobIdPairs");
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdPairs::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLogicalBlobIdPairs::DebugString);
    registry.def("__repr__", &ConstLogicalBlobIdPairs::DebugString);

    registry.def("pair_size", &ConstLogicalBlobIdPairs::pair_size);
    registry.def("pair", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> (ConstLogicalBlobIdPairs::*)() const)&ConstLogicalBlobIdPairs::shared_const_pair);
    registry.def("pair", (::std::shared_ptr<ConstLogicalBlobIdPair> (ConstLogicalBlobIdPairs::*)(::std::size_t) const)&ConstLogicalBlobIdPairs::shared_const_pair);
  }
  {
    pybind11::class_<::oneflow::cfg::LogicalBlobIdPairs, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LogicalBlobIdPairs>> registry(m, "LogicalBlobIdPairs");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LogicalBlobIdPairs::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdPairs::*)(const ConstLogicalBlobIdPairs&))&::oneflow::cfg::LogicalBlobIdPairs::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdPairs::*)(const ::oneflow::cfg::LogicalBlobIdPairs&))&::oneflow::cfg::LogicalBlobIdPairs::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdPairs::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LogicalBlobIdPairs::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LogicalBlobIdPairs::DebugString);



    registry.def("pair_size", &::oneflow::cfg::LogicalBlobIdPairs::pair_size);
    registry.def("clear_pair", &::oneflow::cfg::LogicalBlobIdPairs::clear_pair);
    registry.def("mutable_pair", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> (::oneflow::cfg::LogicalBlobIdPairs::*)())&::oneflow::cfg::LogicalBlobIdPairs::shared_mutable_pair);
    registry.def("pair", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> (::oneflow::cfg::LogicalBlobIdPairs::*)() const)&::oneflow::cfg::LogicalBlobIdPairs::shared_const_pair);
    registry.def("pair", (::std::shared_ptr<ConstLogicalBlobIdPair> (::oneflow::cfg::LogicalBlobIdPairs::*)(::std::size_t) const)&::oneflow::cfg::LogicalBlobIdPairs::shared_const_pair);
    registry.def("mutable_pair", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> (::oneflow::cfg::LogicalBlobIdPairs::*)(::std::size_t))&::oneflow::cfg::LogicalBlobIdPairs::shared_mutable_pair);
  }
  {
    pybind11::class_<ConstLogicalBlobIdGroups_LogicalBlobIdGroup, ::oneflow::cfg::Message, std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup>> registry(m, "ConstLogicalBlobIdGroups_LogicalBlobIdGroup");
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLogicalBlobIdGroups_LogicalBlobIdGroup::DebugString);
    registry.def("__repr__", &ConstLogicalBlobIdGroups_LogicalBlobIdGroup::DebugString);

    registry.def("lbi_size", &ConstLogicalBlobIdGroups_LogicalBlobIdGroup::lbi_size);
    registry.def("lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> (ConstLogicalBlobIdGroups_LogicalBlobIdGroup::*)() const)&ConstLogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi);
    registry.def("lbi", (::std::shared_ptr<ConstLogicalBlobId> (ConstLogicalBlobIdGroups_LogicalBlobIdGroup::*)(::std::size_t) const)&ConstLogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>> registry(m, "LogicalBlobIdGroups_LogicalBlobIdGroup");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup&))&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)(const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup&))&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::DebugString);



    registry.def("lbi_size", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::lbi_size);
    registry.def("clear_lbi", &::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::clear_lbi);
    registry.def("mutable_lbi", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)())&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::shared_mutable_lbi);
    registry.def("lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)() const)&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi);
    registry.def("lbi", (::std::shared_ptr<ConstLogicalBlobId> (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)(::std::size_t) const)&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::shared_const_lbi);
    registry.def("mutable_lbi", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::*)(::std::size_t))&::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup::shared_mutable_lbi);
  }
  {
    pybind11::class_<ConstLogicalBlobIdGroups, ::oneflow::cfg::Message, std::shared_ptr<ConstLogicalBlobIdGroups>> registry(m, "ConstLogicalBlobIdGroups");
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdGroups::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLogicalBlobIdGroups::DebugString);
    registry.def("__repr__", &ConstLogicalBlobIdGroups::DebugString);

    registry.def("lbi_group_size", &ConstLogicalBlobIdGroups::lbi_group_size);
    registry.def("lbi_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> (ConstLogicalBlobIdGroups::*)() const)&ConstLogicalBlobIdGroups::shared_const_lbi_group);
    registry.def("lbi_group", (::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> (ConstLogicalBlobIdGroups::*)(::std::size_t) const)&ConstLogicalBlobIdGroups::shared_const_lbi_group);
  }
  {
    pybind11::class_<::oneflow::cfg::LogicalBlobIdGroups, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups>> registry(m, "LogicalBlobIdGroups");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LogicalBlobIdGroups::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdGroups::*)(const ConstLogicalBlobIdGroups&))&::oneflow::cfg::LogicalBlobIdGroups::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LogicalBlobIdGroups::*)(const ::oneflow::cfg::LogicalBlobIdGroups&))&::oneflow::cfg::LogicalBlobIdGroups::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LogicalBlobIdGroups::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LogicalBlobIdGroups::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LogicalBlobIdGroups::DebugString);



    registry.def("lbi_group_size", &::oneflow::cfg::LogicalBlobIdGroups::lbi_group_size);
    registry.def("clear_lbi_group", &::oneflow::cfg::LogicalBlobIdGroups::clear_lbi_group);
    registry.def("mutable_lbi_group", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> (::oneflow::cfg::LogicalBlobIdGroups::*)())&::oneflow::cfg::LogicalBlobIdGroups::shared_mutable_lbi_group);
    registry.def("lbi_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> (::oneflow::cfg::LogicalBlobIdGroups::*)() const)&::oneflow::cfg::LogicalBlobIdGroups::shared_const_lbi_group);
    registry.def("lbi_group", (::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> (::oneflow::cfg::LogicalBlobIdGroups::*)(::std::size_t) const)&::oneflow::cfg::LogicalBlobIdGroups::shared_const_lbi_group);
    registry.def("mutable_lbi_group", (::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> (::oneflow::cfg::LogicalBlobIdGroups::*)(::std::size_t))&::oneflow::cfg::LogicalBlobIdGroups::shared_mutable_lbi_group);
  }
  {
    pybind11::class_<ConstArgSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstArgSignature>> registry(m, "ConstArgSignature");
    registry.def("__id__", &::oneflow::cfg::ArgSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstArgSignature::DebugString);
    registry.def("__repr__", &ConstArgSignature::DebugString);

    registry.def("bn_in_op2lbi_size", &ConstArgSignature::bn_in_op2lbi_size);
    registry.def("bn_in_op2lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> (ConstArgSignature::*)() const)&ConstArgSignature::shared_const_bn_in_op2lbi);

    registry.def("bn_in_op2lbi", (::std::shared_ptr<ConstLogicalBlobId> (ConstArgSignature::*)(const ::std::string&) const)&ConstArgSignature::shared_const_bn_in_op2lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::ArgSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ArgSignature>> registry(m, "ArgSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ArgSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ArgSignature::*)(const ConstArgSignature&))&::oneflow::cfg::ArgSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ArgSignature::*)(const ::oneflow::cfg::ArgSignature&))&::oneflow::cfg::ArgSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ArgSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ArgSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ArgSignature::DebugString);



    registry.def("bn_in_op2lbi_size", &::oneflow::cfg::ArgSignature::bn_in_op2lbi_size);
    registry.def("bn_in_op2lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> (::oneflow::cfg::ArgSignature::*)() const)&::oneflow::cfg::ArgSignature::shared_const_bn_in_op2lbi);
    registry.def("clear_bn_in_op2lbi", &::oneflow::cfg::ArgSignature::clear_bn_in_op2lbi);
    registry.def("mutable_bn_in_op2lbi", (::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> (::oneflow::cfg::ArgSignature::*)())&::oneflow::cfg::ArgSignature::shared_mutable_bn_in_op2lbi);
    registry.def("bn_in_op2lbi", (::std::shared_ptr<ConstLogicalBlobId> (::oneflow::cfg::ArgSignature::*)(const ::std::string&) const)&::oneflow::cfg::ArgSignature::shared_const_bn_in_op2lbi);
  }
}