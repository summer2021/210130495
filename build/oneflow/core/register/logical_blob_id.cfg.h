#ifndef CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H_
#define CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class LogicalBlobId;
class LogicalBlobIdPair;
class LogicalBlobIdPairs;
class LogicalBlobIdGroups_LogicalBlobIdGroup;
class LogicalBlobIdGroups;
class ArgSignature_BnInOp2lbiEntry;
class ArgSignature;

namespace cfg {


class LogicalBlobId;
class ConstLogicalBlobId;

class LogicalBlobIdPair;
class ConstLogicalBlobIdPair;

class LogicalBlobIdPairs;
class ConstLogicalBlobIdPairs;

class LogicalBlobIdGroups_LogicalBlobIdGroup;
class ConstLogicalBlobIdGroups_LogicalBlobIdGroup;

class LogicalBlobIdGroups;
class ConstLogicalBlobIdGroups;

class ArgSignature;
class ConstArgSignature;



class ConstLogicalBlobId : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobId_ {
   public:
    _LogicalBlobId_();
    explicit _LogicalBlobId_(const _LogicalBlobId_& other);
    explicit _LogicalBlobId_(_LogicalBlobId_&& other);
    _LogicalBlobId_(const ::oneflow::LogicalBlobId& proto_logicalblobid);
    ~_LogicalBlobId_();

    void InitFromProto(const ::oneflow::LogicalBlobId& proto_logicalblobid);

    void ToProto(::oneflow::LogicalBlobId* proto_logicalblobid) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobId_& other);
  
      // optional field op_name
     public:
    bool has_op_name() const;
    const ::std::string& op_name() const;
    void clear_op_name();
    void set_op_name(const ::std::string& value);
    ::std::string* mutable_op_name();
   protected:
    bool has_op_name_ = false;
    ::std::string op_name_;
      
      // optional field blob_name
     public:
    bool has_blob_name() const;
    const ::std::string& blob_name() const;
    void clear_blob_name();
    void set_blob_name(const ::std::string& value);
    ::std::string* mutable_blob_name();
   protected:
    bool has_blob_name_ = false;
    ::std::string blob_name_;
           
   public:
    int compare(const _LogicalBlobId_& other);

    bool operator==(const _LogicalBlobId_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobId_& other) const;
  };

  ConstLogicalBlobId(const ::std::shared_ptr<_LogicalBlobId_>& data);
  ConstLogicalBlobId(const ConstLogicalBlobId&);
  ConstLogicalBlobId(ConstLogicalBlobId&&) noexcept;
  ConstLogicalBlobId();
  ConstLogicalBlobId(const ::oneflow::LogicalBlobId& proto_logicalblobid);
  virtual ~ConstLogicalBlobId() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobid) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_name
 public:
  bool has_op_name() const;
  const ::std::string& op_name() const;
  // used by pybind11 only
  // required or optional field blob_name
 public:
  bool has_blob_name() const;
  const ::std::string& blob_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLogicalBlobId> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobId& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobId& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobId_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobId_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobId
  void BuildFromProto(const PbMessage& proto_logicalblobid);
  
  ::std::shared_ptr<_LogicalBlobId_> data_;
};

class LogicalBlobId final : public ConstLogicalBlobId {
 public:
  LogicalBlobId(const ::std::shared_ptr<_LogicalBlobId_>& data);
  LogicalBlobId(const LogicalBlobId& other);
  // enable nothrow for ::std::vector<LogicalBlobId> resize 
  LogicalBlobId(LogicalBlobId&&) noexcept;
  LogicalBlobId();
  explicit LogicalBlobId(const ::oneflow::LogicalBlobId& proto_logicalblobid);

  ~LogicalBlobId() override;

  void InitFromProto(const PbMessage& proto_logicalblobid) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobId& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobId& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobId& other);
  LogicalBlobId& operator=(const LogicalBlobId& other);

  // required or optional field op_name
 public:
  void clear_op_name();
  void set_op_name(const ::std::string& value);
  ::std::string* mutable_op_name();
  // required or optional field blob_name
 public:
  void clear_blob_name();
  void set_blob_name(const ::std::string& value);
  ::std::string* mutable_blob_name();

  ::std::shared_ptr<LogicalBlobId> __SharedMutable__();
};


class ConstLogicalBlobIdPair : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobIdPair_ {
   public:
    _LogicalBlobIdPair_();
    explicit _LogicalBlobIdPair_(const _LogicalBlobIdPair_& other);
    explicit _LogicalBlobIdPair_(_LogicalBlobIdPair_&& other);
    _LogicalBlobIdPair_(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair);
    ~_LogicalBlobIdPair_();

    void InitFromProto(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair);

    void ToProto(::oneflow::LogicalBlobIdPair* proto_logicalblobidpair) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobIdPair_& other);
  
      // optional field first
     public:
    bool has_first() const;
    const ::oneflow::cfg::LogicalBlobId& first() const;
    void clear_first();
    ::oneflow::cfg::LogicalBlobId* mutable_first();
   protected:
    bool has_first_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> first_;
      
      // optional field second
     public:
    bool has_second() const;
    const ::oneflow::cfg::LogicalBlobId& second() const;
    void clear_second();
    ::oneflow::cfg::LogicalBlobId* mutable_second();
   protected:
    bool has_second_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> second_;
           
   public:
    int compare(const _LogicalBlobIdPair_& other);

    bool operator==(const _LogicalBlobIdPair_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobIdPair_& other) const;
  };

  ConstLogicalBlobIdPair(const ::std::shared_ptr<_LogicalBlobIdPair_>& data);
  ConstLogicalBlobIdPair(const ConstLogicalBlobIdPair&);
  ConstLogicalBlobIdPair(ConstLogicalBlobIdPair&&) noexcept;
  ConstLogicalBlobIdPair();
  ConstLogicalBlobIdPair(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair);
  virtual ~ConstLogicalBlobIdPair() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobidpair) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field first
 public:
  bool has_first() const;
  const ::oneflow::cfg::LogicalBlobId& first() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_first() const;
  // required or optional field second
 public:
  bool has_second() const;
  const ::oneflow::cfg::LogicalBlobId& second() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_second() const;

 public:
  ::std::shared_ptr<ConstLogicalBlobIdPair> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobIdPair& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobIdPair& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobIdPair_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobIdPair_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdPair
  void BuildFromProto(const PbMessage& proto_logicalblobidpair);
  
  ::std::shared_ptr<_LogicalBlobIdPair_> data_;
};

class LogicalBlobIdPair final : public ConstLogicalBlobIdPair {
 public:
  LogicalBlobIdPair(const ::std::shared_ptr<_LogicalBlobIdPair_>& data);
  LogicalBlobIdPair(const LogicalBlobIdPair& other);
  // enable nothrow for ::std::vector<LogicalBlobIdPair> resize 
  LogicalBlobIdPair(LogicalBlobIdPair&&) noexcept;
  LogicalBlobIdPair();
  explicit LogicalBlobIdPair(const ::oneflow::LogicalBlobIdPair& proto_logicalblobidpair);

  ~LogicalBlobIdPair() override;

  void InitFromProto(const PbMessage& proto_logicalblobidpair) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobIdPair& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobIdPair& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobIdPair& other);
  LogicalBlobIdPair& operator=(const LogicalBlobIdPair& other);

  // required or optional field first
 public:
  void clear_first();
  ::oneflow::cfg::LogicalBlobId* mutable_first();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_first();
  // required or optional field second
 public:
  void clear_second();
  ::oneflow::cfg::LogicalBlobId* mutable_second();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_second();

  ::std::shared_ptr<LogicalBlobIdPair> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_;
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_;

class ConstLogicalBlobIdPairs : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobIdPairs_ {
   public:
    _LogicalBlobIdPairs_();
    explicit _LogicalBlobIdPairs_(const _LogicalBlobIdPairs_& other);
    explicit _LogicalBlobIdPairs_(_LogicalBlobIdPairs_&& other);
    _LogicalBlobIdPairs_(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs);
    ~_LogicalBlobIdPairs_();

    void InitFromProto(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs);

    void ToProto(::oneflow::LogicalBlobIdPairs* proto_logicalblobidpairs) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobIdPairs_& other);
  
      // repeated field pair
   public:
    ::std::size_t pair_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& pair() const;
    const ::oneflow::cfg::LogicalBlobIdPair& pair(::std::size_t index) const;
    void clear_pair();
    _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_* mutable_pair();
    ::oneflow::cfg::LogicalBlobIdPair* mutable_pair(::std::size_t index);
      ::oneflow::cfg::LogicalBlobIdPair* add_pair();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> pair_;
         
   public:
    int compare(const _LogicalBlobIdPairs_& other);

    bool operator==(const _LogicalBlobIdPairs_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobIdPairs_& other) const;
  };

  ConstLogicalBlobIdPairs(const ::std::shared_ptr<_LogicalBlobIdPairs_>& data);
  ConstLogicalBlobIdPairs(const ConstLogicalBlobIdPairs&);
  ConstLogicalBlobIdPairs(ConstLogicalBlobIdPairs&&) noexcept;
  ConstLogicalBlobIdPairs();
  ConstLogicalBlobIdPairs(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs);
  virtual ~ConstLogicalBlobIdPairs() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobidpairs) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field pair
 public:
  ::std::size_t pair_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& pair() const;
  const ::oneflow::cfg::LogicalBlobIdPair& pair(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> shared_const_pair() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdPair> shared_const_pair(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstLogicalBlobIdPairs> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobIdPairs& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobIdPairs& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobIdPairs_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobIdPairs_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdPairs
  void BuildFromProto(const PbMessage& proto_logicalblobidpairs);
  
  ::std::shared_ptr<_LogicalBlobIdPairs_> data_;
};

class LogicalBlobIdPairs final : public ConstLogicalBlobIdPairs {
 public:
  LogicalBlobIdPairs(const ::std::shared_ptr<_LogicalBlobIdPairs_>& data);
  LogicalBlobIdPairs(const LogicalBlobIdPairs& other);
  // enable nothrow for ::std::vector<LogicalBlobIdPairs> resize 
  LogicalBlobIdPairs(LogicalBlobIdPairs&&) noexcept;
  LogicalBlobIdPairs();
  explicit LogicalBlobIdPairs(const ::oneflow::LogicalBlobIdPairs& proto_logicalblobidpairs);

  ~LogicalBlobIdPairs() override;

  void InitFromProto(const PbMessage& proto_logicalblobidpairs) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobIdPairs& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobIdPairs& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobIdPairs& other);
  LogicalBlobIdPairs& operator=(const LogicalBlobIdPairs& other);

  // repeated field pair
 public:
  void clear_pair();
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_* mutable_pair();
  ::oneflow::cfg::LogicalBlobIdPair* mutable_pair(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> shared_mutable_pair();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> shared_mutable_pair(::std::size_t index);
  ::oneflow::cfg::LogicalBlobIdPair* add_pair();

  ::std::shared_ptr<LogicalBlobIdPairs> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_;
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_;

class ConstLogicalBlobIdGroups_LogicalBlobIdGroup : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobIdGroups_LogicalBlobIdGroup_ {
   public:
    _LogicalBlobIdGroups_LogicalBlobIdGroup_();
    explicit _LogicalBlobIdGroups_LogicalBlobIdGroup_(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other);
    explicit _LogicalBlobIdGroups_LogicalBlobIdGroup_(_LogicalBlobIdGroups_LogicalBlobIdGroup_&& other);
    _LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup);
    ~_LogicalBlobIdGroups_LogicalBlobIdGroup_();

    void InitFromProto(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup);

    void ToProto(::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup* proto_logicalblobidgroups_logicalblobidgroup) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other);
  
      // repeated field lbi
   public:
    ::std::size_t lbi_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi(::std::size_t index) const;
    void clear_lbi();
    _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_* mutable_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi(::std::size_t index);
      ::oneflow::cfg::LogicalBlobId* add_lbi();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> lbi_;
         
   public:
    int compare(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other);

    bool operator==(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
  };

  ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& data);
  ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup&);
  ConstLogicalBlobIdGroups_LogicalBlobIdGroup(ConstLogicalBlobIdGroups_LogicalBlobIdGroup&&) noexcept;
  ConstLogicalBlobIdGroups_LogicalBlobIdGroup();
  ConstLogicalBlobIdGroups_LogicalBlobIdGroup(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup);
  virtual ~ConstLogicalBlobIdGroups_LogicalBlobIdGroup() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobidgroups_logicalblobidgroup) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field lbi
 public:
  ::std::size_t lbi_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> shared_const_lbi() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstLogicalBlobIdGroups_LogicalBlobIdGroup> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobIdGroups_LogicalBlobIdGroup& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdGroups_LogicalBlobIdGroup
  void BuildFromProto(const PbMessage& proto_logicalblobidgroups_logicalblobidgroup);
  
  ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_> data_;
};

class LogicalBlobIdGroups_LogicalBlobIdGroup final : public ConstLogicalBlobIdGroups_LogicalBlobIdGroup {
 public:
  LogicalBlobIdGroups_LogicalBlobIdGroup(const ::std::shared_ptr<_LogicalBlobIdGroups_LogicalBlobIdGroup_>& data);
  LogicalBlobIdGroups_LogicalBlobIdGroup(const LogicalBlobIdGroups_LogicalBlobIdGroup& other);
  // enable nothrow for ::std::vector<LogicalBlobIdGroups_LogicalBlobIdGroup> resize 
  LogicalBlobIdGroups_LogicalBlobIdGroup(LogicalBlobIdGroups_LogicalBlobIdGroup&&) noexcept;
  LogicalBlobIdGroups_LogicalBlobIdGroup();
  explicit LogicalBlobIdGroups_LogicalBlobIdGroup(const ::oneflow::LogicalBlobIdGroups_LogicalBlobIdGroup& proto_logicalblobidgroups_logicalblobidgroup);

  ~LogicalBlobIdGroups_LogicalBlobIdGroup() override;

  void InitFromProto(const PbMessage& proto_logicalblobidgroups_logicalblobidgroup) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobIdGroups_LogicalBlobIdGroup& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobIdGroups_LogicalBlobIdGroup& other);
  LogicalBlobIdGroups_LogicalBlobIdGroup& operator=(const LogicalBlobIdGroups_LogicalBlobIdGroup& other);

  // repeated field lbi
 public:
  void clear_lbi();
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_* mutable_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> shared_mutable_lbi();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi(::std::size_t index);
  ::oneflow::cfg::LogicalBlobId* add_lbi();

  ::std::shared_ptr<LogicalBlobIdGroups_LogicalBlobIdGroup> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_;
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_;

class ConstLogicalBlobIdGroups : public ::oneflow::cfg::Message {
 public:

  class _LogicalBlobIdGroups_ {
   public:
    _LogicalBlobIdGroups_();
    explicit _LogicalBlobIdGroups_(const _LogicalBlobIdGroups_& other);
    explicit _LogicalBlobIdGroups_(_LogicalBlobIdGroups_&& other);
    _LogicalBlobIdGroups_(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups);
    ~_LogicalBlobIdGroups_();

    void InitFromProto(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups);

    void ToProto(::oneflow::LogicalBlobIdGroups* proto_logicalblobidgroups) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LogicalBlobIdGroups_& other);
  
      // repeated field lbi_group
   public:
    ::std::size_t lbi_group_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& lbi_group() const;
    const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& lbi_group(::std::size_t index) const;
    void clear_lbi_group();
    _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_* mutable_lbi_group();
    ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* mutable_lbi_group(::std::size_t index);
      ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* add_lbi_group();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> lbi_group_;
         
   public:
    int compare(const _LogicalBlobIdGroups_& other);

    bool operator==(const _LogicalBlobIdGroups_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LogicalBlobIdGroups_& other) const;
  };

  ConstLogicalBlobIdGroups(const ::std::shared_ptr<_LogicalBlobIdGroups_>& data);
  ConstLogicalBlobIdGroups(const ConstLogicalBlobIdGroups&);
  ConstLogicalBlobIdGroups(ConstLogicalBlobIdGroups&&) noexcept;
  ConstLogicalBlobIdGroups();
  ConstLogicalBlobIdGroups(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups);
  virtual ~ConstLogicalBlobIdGroups() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_logicalblobidgroups) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field lbi_group
 public:
  ::std::size_t lbi_group_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& lbi_group() const;
  const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& lbi_group(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> shared_const_lbi_group() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup> shared_const_lbi_group(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstLogicalBlobIdGroups> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLogicalBlobIdGroups& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLogicalBlobIdGroups& other) const;
 protected:
  const ::std::shared_ptr<_LogicalBlobIdGroups_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LogicalBlobIdGroups_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLogicalBlobIdGroups
  void BuildFromProto(const PbMessage& proto_logicalblobidgroups);
  
  ::std::shared_ptr<_LogicalBlobIdGroups_> data_;
};

class LogicalBlobIdGroups final : public ConstLogicalBlobIdGroups {
 public:
  LogicalBlobIdGroups(const ::std::shared_ptr<_LogicalBlobIdGroups_>& data);
  LogicalBlobIdGroups(const LogicalBlobIdGroups& other);
  // enable nothrow for ::std::vector<LogicalBlobIdGroups> resize 
  LogicalBlobIdGroups(LogicalBlobIdGroups&&) noexcept;
  LogicalBlobIdGroups();
  explicit LogicalBlobIdGroups(const ::oneflow::LogicalBlobIdGroups& proto_logicalblobidgroups);

  ~LogicalBlobIdGroups() override;

  void InitFromProto(const PbMessage& proto_logicalblobidgroups) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LogicalBlobIdGroups& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LogicalBlobIdGroups& other) const;
  void Clear();
  void CopyFrom(const LogicalBlobIdGroups& other);
  LogicalBlobIdGroups& operator=(const LogicalBlobIdGroups& other);

  // repeated field lbi_group
 public:
  void clear_lbi_group();
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_* mutable_lbi_group();
  ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* mutable_lbi_group(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> shared_mutable_lbi_group();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> shared_mutable_lbi_group(::std::size_t index);
  ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup* add_lbi_group();

  ::std::shared_ptr<LogicalBlobIdGroups> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_; 
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_;

class ConstArgSignature : public ::oneflow::cfg::Message {
 public:

  class _ArgSignature_ {
   public:
    _ArgSignature_();
    explicit _ArgSignature_(const _ArgSignature_& other);
    explicit _ArgSignature_(_ArgSignature_&& other);
    _ArgSignature_(const ::oneflow::ArgSignature& proto_argsignature);
    ~_ArgSignature_();

    void InitFromProto(const ::oneflow::ArgSignature& proto_argsignature);

    void ToProto(::oneflow::ArgSignature* proto_argsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ArgSignature_& other);
  
     public:
    ::std::size_t bn_in_op2lbi_size() const;
    const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& bn_in_op2lbi() const;

    _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ * mutable_bn_in_op2lbi();

    const ::oneflow::cfg::LogicalBlobId& bn_in_op2lbi(::std::string key) const;

    void clear_bn_in_op2lbi();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> bn_in_op2lbi_;
         
   public:
    int compare(const _ArgSignature_& other);

    bool operator==(const _ArgSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ArgSignature_& other) const;
  };

  ConstArgSignature(const ::std::shared_ptr<_ArgSignature_>& data);
  ConstArgSignature(const ConstArgSignature&);
  ConstArgSignature(ConstArgSignature&&) noexcept;
  ConstArgSignature();
  ConstArgSignature(const ::oneflow::ArgSignature& proto_argsignature);
  virtual ~ConstArgSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_argsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2lbi
 public:
  ::std::size_t bn_in_op2lbi_size() const;
  const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& bn_in_op2lbi() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> shared_const_bn_in_op2lbi() const;

 public:
  ::std::shared_ptr<ConstArgSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstArgSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstArgSignature& other) const;
 protected:
  const ::std::shared_ptr<_ArgSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ArgSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstArgSignature
  void BuildFromProto(const PbMessage& proto_argsignature);
  
  ::std::shared_ptr<_ArgSignature_> data_;
};

class ArgSignature final : public ConstArgSignature {
 public:
  ArgSignature(const ::std::shared_ptr<_ArgSignature_>& data);
  ArgSignature(const ArgSignature& other);
  // enable nothrow for ::std::vector<ArgSignature> resize 
  ArgSignature(ArgSignature&&) noexcept;
  ArgSignature();
  explicit ArgSignature(const ::oneflow::ArgSignature& proto_argsignature);

  ~ArgSignature() override;

  void InitFromProto(const PbMessage& proto_argsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ArgSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ArgSignature& other) const;
  void Clear();
  void CopyFrom(const ArgSignature& other);
  ArgSignature& operator=(const ArgSignature& other);

  // repeated field bn_in_op2lbi
 public:
  void clear_bn_in_op2lbi();

  const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ & bn_in_op2lbi() const;

  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_* mutable_bn_in_op2lbi();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> shared_mutable_bn_in_op2lbi();

  ::std::shared_ptr<ArgSignature> __SharedMutable__();
};










// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdPair> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdPair>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdPair> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdPair>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_();
  ~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdPair_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdPair> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_();
  ~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobId_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_();
  ~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__RepeatedField_LogicalBlobIdGroups_LogicalBlobIdGroup_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::LogicalBlobId> {
 public:
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::LogicalBlobId>>& data);
  Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_();
  ~Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::LogicalBlobId& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstLogicalBlobId> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ConstLogicalBlobId>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ final : public Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_ {
 public:
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::LogicalBlobId>>& data);
  _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_();
  ~_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H__MapField___std__string_LogicalBlobId_, ::oneflow::cfg::LogicalBlobId>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstLogicalBlobId> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobId& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobId> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobId& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobIdPair> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobIdPair& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobIdPair> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobIdPair& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobIdPairs> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobIdPairs& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobIdPairs> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobIdPairs& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobIdGroups_LogicalBlobIdGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobIdGroups_LogicalBlobIdGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLogicalBlobIdGroups> {
  std::size_t operator()(const ::oneflow::cfg::ConstLogicalBlobIdGroups& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LogicalBlobIdGroups> {
  std::size_t operator()(const ::oneflow::cfg::LogicalBlobIdGroups& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstArgSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstArgSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ArgSignature> {
  std::size_t operator()(const ::oneflow::cfg::ArgSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_REGISTER_LOGICAL_BLOB_ID_CFG_H_