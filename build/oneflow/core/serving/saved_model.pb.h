// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/serving/saved_model.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/map.h>  // IWYU pragma: export
#include <google/protobuf/map_entry.h>
#include <google/protobuf/map_field_inl.h>
#include <google/protobuf/unknown_field_set.h>
#include "oneflow/core/operator/op_conf.pb.h"
#include "oneflow/core/job/job_conf.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[4]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto;
namespace oneflow {
class GraphDef;
class GraphDefDefaultTypeInternal;
extern GraphDefDefaultTypeInternal _GraphDef_default_instance_;
class GraphDef_SignaturesEntry_DoNotUse;
class GraphDef_SignaturesEntry_DoNotUseDefaultTypeInternal;
extern GraphDef_SignaturesEntry_DoNotUseDefaultTypeInternal _GraphDef_SignaturesEntry_DoNotUse_default_instance_;
class SavedModel;
class SavedModelDefaultTypeInternal;
extern SavedModelDefaultTypeInternal _SavedModel_default_instance_;
class SavedModel_GraphsEntry_DoNotUse;
class SavedModel_GraphsEntry_DoNotUseDefaultTypeInternal;
extern SavedModel_GraphsEntry_DoNotUseDefaultTypeInternal _SavedModel_GraphsEntry_DoNotUse_default_instance_;
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> ::oneflow::GraphDef* Arena::CreateMaybeMessage<::oneflow::GraphDef>(Arena*);
template<> ::oneflow::GraphDef_SignaturesEntry_DoNotUse* Arena::CreateMaybeMessage<::oneflow::GraphDef_SignaturesEntry_DoNotUse>(Arena*);
template<> ::oneflow::SavedModel* Arena::CreateMaybeMessage<::oneflow::SavedModel>(Arena*);
template<> ::oneflow::SavedModel_GraphsEntry_DoNotUse* Arena::CreateMaybeMessage<::oneflow::SavedModel_GraphsEntry_DoNotUse>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace oneflow {

// ===================================================================

class SavedModel_GraphsEntry_DoNotUse : public ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<SavedModel_GraphsEntry_DoNotUse, 
    std::string, ::oneflow::GraphDef,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > {
public:
  typedef ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<SavedModel_GraphsEntry_DoNotUse, 
    std::string, ::oneflow::GraphDef,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > SuperType;
  SavedModel_GraphsEntry_DoNotUse();
  SavedModel_GraphsEntry_DoNotUse(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  void MergeFrom(const SavedModel_GraphsEntry_DoNotUse& other);
  static const SavedModel_GraphsEntry_DoNotUse* internal_default_instance() { return reinterpret_cast<const SavedModel_GraphsEntry_DoNotUse*>(&_SavedModel_GraphsEntry_DoNotUse_default_instance_); }
  static bool ValidateKey(std::string* s) {
#ifndef NDEBUG
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
       s->data(), s->size(), ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::PARSE, "oneflow.SavedModel.GraphsEntry.key");
#endif
    return true;
 }
  static bool ValidateValue(void*) { return true; }
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& other) final;
  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto.file_level_metadata[0];
  }

  public:
};

// -------------------------------------------------------------------

class SavedModel :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.SavedModel) */ {
 public:
  SavedModel();
  virtual ~SavedModel();

  SavedModel(const SavedModel& from);
  SavedModel(SavedModel&& from) noexcept
    : SavedModel() {
    *this = ::std::move(from);
  }

  inline SavedModel& operator=(const SavedModel& from) {
    CopyFrom(from);
    return *this;
  }
  inline SavedModel& operator=(SavedModel&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const SavedModel& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const SavedModel* internal_default_instance() {
    return reinterpret_cast<const SavedModel*>(
               &_SavedModel_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    1;

  friend void swap(SavedModel& a, SavedModel& b) {
    a.Swap(&b);
  }
  inline void Swap(SavedModel* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline SavedModel* New() const final {
    return CreateMaybeMessage<SavedModel>(nullptr);
  }

  SavedModel* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<SavedModel>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const SavedModel& from);
  void MergeFrom(const SavedModel& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(SavedModel* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.SavedModel";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------


  // accessors -------------------------------------------------------

  enum : int {
    kGraphsFieldNumber = 4,
    kNameFieldNumber = 1,
    kCheckpointDirFieldNumber = 3,
    kDefaultGraphNameFieldNumber = 5,
    kVersionFieldNumber = 2,
  };
  // map<string, .oneflow.GraphDef> graphs = 4;
  int graphs_size() const;
  void clear_graphs();
  const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::GraphDef >&
      graphs() const;
  ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::GraphDef >*
      mutable_graphs();

  // required string name = 1;
  bool has_name() const;
  void clear_name();
  const std::string& name() const;
  void set_name(const std::string& value);
  void set_name(std::string&& value);
  void set_name(const char* value);
  void set_name(const char* value, size_t size);
  std::string* mutable_name();
  std::string* release_name();
  void set_allocated_name(std::string* name);

  // required string checkpoint_dir = 3;
  bool has_checkpoint_dir() const;
  void clear_checkpoint_dir();
  const std::string& checkpoint_dir() const;
  void set_checkpoint_dir(const std::string& value);
  void set_checkpoint_dir(std::string&& value);
  void set_checkpoint_dir(const char* value);
  void set_checkpoint_dir(const char* value, size_t size);
  std::string* mutable_checkpoint_dir();
  std::string* release_checkpoint_dir();
  void set_allocated_checkpoint_dir(std::string* checkpoint_dir);

  // optional string default_graph_name = 5;
  bool has_default_graph_name() const;
  void clear_default_graph_name();
  const std::string& default_graph_name() const;
  void set_default_graph_name(const std::string& value);
  void set_default_graph_name(std::string&& value);
  void set_default_graph_name(const char* value);
  void set_default_graph_name(const char* value, size_t size);
  std::string* mutable_default_graph_name();
  std::string* release_default_graph_name();
  void set_allocated_default_graph_name(std::string* default_graph_name);

  // required int64 version = 2;
  bool has_version() const;
  void clear_version();
  ::PROTOBUF_NAMESPACE_ID::int64 version() const;
  void set_version(::PROTOBUF_NAMESPACE_ID::int64 value);

  // @@protoc_insertion_point(class_scope:oneflow.SavedModel)
 private:
  class _Internal;

  // helper for ByteSizeLong()
  size_t RequiredFieldsByteSizeFallback() const;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::internal::MapField<
      SavedModel_GraphsEntry_DoNotUse,
      std::string, ::oneflow::GraphDef,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
      0 > graphs_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr name_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr checkpoint_dir_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr default_graph_name_;
  ::PROTOBUF_NAMESPACE_ID::int64 version_;
  friend struct ::TableStruct_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto;
};
// -------------------------------------------------------------------

class GraphDef_SignaturesEntry_DoNotUse : public ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<GraphDef_SignaturesEntry_DoNotUse, 
    std::string, ::oneflow::JobSignatureDef,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > {
public:
  typedef ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<GraphDef_SignaturesEntry_DoNotUse, 
    std::string, ::oneflow::JobSignatureDef,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > SuperType;
  GraphDef_SignaturesEntry_DoNotUse();
  GraphDef_SignaturesEntry_DoNotUse(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  void MergeFrom(const GraphDef_SignaturesEntry_DoNotUse& other);
  static const GraphDef_SignaturesEntry_DoNotUse* internal_default_instance() { return reinterpret_cast<const GraphDef_SignaturesEntry_DoNotUse*>(&_GraphDef_SignaturesEntry_DoNotUse_default_instance_); }
  static bool ValidateKey(std::string* s) {
#ifndef NDEBUG
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
       s->data(), s->size(), ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::PARSE, "oneflow.GraphDef.SignaturesEntry.key");
#endif
    return true;
 }
  static bool ValidateValue(void*) { return true; }
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& other) final;
  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto.file_level_metadata[2];
  }

  public:
};

// -------------------------------------------------------------------

class GraphDef :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.GraphDef) */ {
 public:
  GraphDef();
  virtual ~GraphDef();

  GraphDef(const GraphDef& from);
  GraphDef(GraphDef&& from) noexcept
    : GraphDef() {
    *this = ::std::move(from);
  }

  inline GraphDef& operator=(const GraphDef& from) {
    CopyFrom(from);
    return *this;
  }
  inline GraphDef& operator=(GraphDef&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const GraphDef& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const GraphDef* internal_default_instance() {
    return reinterpret_cast<const GraphDef*>(
               &_GraphDef_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    3;

  friend void swap(GraphDef& a, GraphDef& b) {
    a.Swap(&b);
  }
  inline void Swap(GraphDef* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline GraphDef* New() const final {
    return CreateMaybeMessage<GraphDef>(nullptr);
  }

  GraphDef* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<GraphDef>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const GraphDef& from);
  void MergeFrom(const GraphDef& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(GraphDef* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.GraphDef";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------


  // accessors -------------------------------------------------------

  enum : int {
    kOpListFieldNumber = 1,
    kSignaturesFieldNumber = 2,
    kDefaultSignatureNameFieldNumber = 3,
  };
  // repeated .oneflow.OperatorConf op_list = 1;
  int op_list_size() const;
  void clear_op_list();
  ::oneflow::OperatorConf* mutable_op_list(int index);
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::OperatorConf >*
      mutable_op_list();
  const ::oneflow::OperatorConf& op_list(int index) const;
  ::oneflow::OperatorConf* add_op_list();
  const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::OperatorConf >&
      op_list() const;

  // map<string, .oneflow.JobSignatureDef> signatures = 2;
  int signatures_size() const;
  void clear_signatures();
  const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::JobSignatureDef >&
      signatures() const;
  ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::JobSignatureDef >*
      mutable_signatures();

  // optional string default_signature_name = 3;
  bool has_default_signature_name() const;
  void clear_default_signature_name();
  const std::string& default_signature_name() const;
  void set_default_signature_name(const std::string& value);
  void set_default_signature_name(std::string&& value);
  void set_default_signature_name(const char* value);
  void set_default_signature_name(const char* value, size_t size);
  std::string* mutable_default_signature_name();
  std::string* release_default_signature_name();
  void set_allocated_default_signature_name(std::string* default_signature_name);

  // @@protoc_insertion_point(class_scope:oneflow.GraphDef)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::OperatorConf > op_list_;
  ::PROTOBUF_NAMESPACE_ID::internal::MapField<
      GraphDef_SignaturesEntry_DoNotUse,
      std::string, ::oneflow::JobSignatureDef,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
      0 > signatures_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr default_signature_name_;
  friend struct ::TableStruct_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// -------------------------------------------------------------------

// SavedModel

// required string name = 1;
inline bool SavedModel::has_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SavedModel::clear_name() {
  name_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _has_bits_[0] &= ~0x00000001u;
}
inline const std::string& SavedModel::name() const {
  // @@protoc_insertion_point(field_get:oneflow.SavedModel.name)
  return name_.GetNoArena();
}
inline void SavedModel::set_name(const std::string& value) {
  _has_bits_[0] |= 0x00000001u;
  name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:oneflow.SavedModel.name)
}
inline void SavedModel::set_name(std::string&& value) {
  _has_bits_[0] |= 0x00000001u;
  name_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.SavedModel.name)
}
inline void SavedModel::set_name(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000001u;
  name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.SavedModel.name)
}
inline void SavedModel::set_name(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000001u;
  name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.SavedModel.name)
}
inline std::string* SavedModel::mutable_name() {
  _has_bits_[0] |= 0x00000001u;
  // @@protoc_insertion_point(field_mutable:oneflow.SavedModel.name)
  return name_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* SavedModel::release_name() {
  // @@protoc_insertion_point(field_release:oneflow.SavedModel.name)
  if (!has_name()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000001u;
  return name_.ReleaseNonDefaultNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void SavedModel::set_allocated_name(std::string* name) {
  if (name != nullptr) {
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  name_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:oneflow.SavedModel.name)
}

// required int64 version = 2;
inline bool SavedModel::has_version() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void SavedModel::clear_version() {
  version_ = PROTOBUF_LONGLONG(0);
  _has_bits_[0] &= ~0x00000008u;
}
inline ::PROTOBUF_NAMESPACE_ID::int64 SavedModel::version() const {
  // @@protoc_insertion_point(field_get:oneflow.SavedModel.version)
  return version_;
}
inline void SavedModel::set_version(::PROTOBUF_NAMESPACE_ID::int64 value) {
  _has_bits_[0] |= 0x00000008u;
  version_ = value;
  // @@protoc_insertion_point(field_set:oneflow.SavedModel.version)
}

// required string checkpoint_dir = 3;
inline bool SavedModel::has_checkpoint_dir() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SavedModel::clear_checkpoint_dir() {
  checkpoint_dir_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _has_bits_[0] &= ~0x00000002u;
}
inline const std::string& SavedModel::checkpoint_dir() const {
  // @@protoc_insertion_point(field_get:oneflow.SavedModel.checkpoint_dir)
  return checkpoint_dir_.GetNoArena();
}
inline void SavedModel::set_checkpoint_dir(const std::string& value) {
  _has_bits_[0] |= 0x00000002u;
  checkpoint_dir_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:oneflow.SavedModel.checkpoint_dir)
}
inline void SavedModel::set_checkpoint_dir(std::string&& value) {
  _has_bits_[0] |= 0x00000002u;
  checkpoint_dir_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.SavedModel.checkpoint_dir)
}
inline void SavedModel::set_checkpoint_dir(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000002u;
  checkpoint_dir_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.SavedModel.checkpoint_dir)
}
inline void SavedModel::set_checkpoint_dir(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000002u;
  checkpoint_dir_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.SavedModel.checkpoint_dir)
}
inline std::string* SavedModel::mutable_checkpoint_dir() {
  _has_bits_[0] |= 0x00000002u;
  // @@protoc_insertion_point(field_mutable:oneflow.SavedModel.checkpoint_dir)
  return checkpoint_dir_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* SavedModel::release_checkpoint_dir() {
  // @@protoc_insertion_point(field_release:oneflow.SavedModel.checkpoint_dir)
  if (!has_checkpoint_dir()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000002u;
  return checkpoint_dir_.ReleaseNonDefaultNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void SavedModel::set_allocated_checkpoint_dir(std::string* checkpoint_dir) {
  if (checkpoint_dir != nullptr) {
    _has_bits_[0] |= 0x00000002u;
  } else {
    _has_bits_[0] &= ~0x00000002u;
  }
  checkpoint_dir_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), checkpoint_dir);
  // @@protoc_insertion_point(field_set_allocated:oneflow.SavedModel.checkpoint_dir)
}

// map<string, .oneflow.GraphDef> graphs = 4;
inline int SavedModel::graphs_size() const {
  return graphs_.size();
}
inline void SavedModel::clear_graphs() {
  graphs_.Clear();
}
inline const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::GraphDef >&
SavedModel::graphs() const {
  // @@protoc_insertion_point(field_map:oneflow.SavedModel.graphs)
  return graphs_.GetMap();
}
inline ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::GraphDef >*
SavedModel::mutable_graphs() {
  // @@protoc_insertion_point(field_mutable_map:oneflow.SavedModel.graphs)
  return graphs_.MutableMap();
}

// optional string default_graph_name = 5;
inline bool SavedModel::has_default_graph_name() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void SavedModel::clear_default_graph_name() {
  default_graph_name_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _has_bits_[0] &= ~0x00000004u;
}
inline const std::string& SavedModel::default_graph_name() const {
  // @@protoc_insertion_point(field_get:oneflow.SavedModel.default_graph_name)
  return default_graph_name_.GetNoArena();
}
inline void SavedModel::set_default_graph_name(const std::string& value) {
  _has_bits_[0] |= 0x00000004u;
  default_graph_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:oneflow.SavedModel.default_graph_name)
}
inline void SavedModel::set_default_graph_name(std::string&& value) {
  _has_bits_[0] |= 0x00000004u;
  default_graph_name_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.SavedModel.default_graph_name)
}
inline void SavedModel::set_default_graph_name(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000004u;
  default_graph_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.SavedModel.default_graph_name)
}
inline void SavedModel::set_default_graph_name(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000004u;
  default_graph_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.SavedModel.default_graph_name)
}
inline std::string* SavedModel::mutable_default_graph_name() {
  _has_bits_[0] |= 0x00000004u;
  // @@protoc_insertion_point(field_mutable:oneflow.SavedModel.default_graph_name)
  return default_graph_name_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* SavedModel::release_default_graph_name() {
  // @@protoc_insertion_point(field_release:oneflow.SavedModel.default_graph_name)
  if (!has_default_graph_name()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000004u;
  return default_graph_name_.ReleaseNonDefaultNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void SavedModel::set_allocated_default_graph_name(std::string* default_graph_name) {
  if (default_graph_name != nullptr) {
    _has_bits_[0] |= 0x00000004u;
  } else {
    _has_bits_[0] &= ~0x00000004u;
  }
  default_graph_name_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), default_graph_name);
  // @@protoc_insertion_point(field_set_allocated:oneflow.SavedModel.default_graph_name)
}

// -------------------------------------------------------------------

// -------------------------------------------------------------------

// GraphDef

// repeated .oneflow.OperatorConf op_list = 1;
inline int GraphDef::op_list_size() const {
  return op_list_.size();
}
inline ::oneflow::OperatorConf* GraphDef::mutable_op_list(int index) {
  // @@protoc_insertion_point(field_mutable:oneflow.GraphDef.op_list)
  return op_list_.Mutable(index);
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::OperatorConf >*
GraphDef::mutable_op_list() {
  // @@protoc_insertion_point(field_mutable_list:oneflow.GraphDef.op_list)
  return &op_list_;
}
inline const ::oneflow::OperatorConf& GraphDef::op_list(int index) const {
  // @@protoc_insertion_point(field_get:oneflow.GraphDef.op_list)
  return op_list_.Get(index);
}
inline ::oneflow::OperatorConf* GraphDef::add_op_list() {
  // @@protoc_insertion_point(field_add:oneflow.GraphDef.op_list)
  return op_list_.Add();
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::OperatorConf >&
GraphDef::op_list() const {
  // @@protoc_insertion_point(field_list:oneflow.GraphDef.op_list)
  return op_list_;
}

// map<string, .oneflow.JobSignatureDef> signatures = 2;
inline int GraphDef::signatures_size() const {
  return signatures_.size();
}
inline const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::JobSignatureDef >&
GraphDef::signatures() const {
  // @@protoc_insertion_point(field_map:oneflow.GraphDef.signatures)
  return signatures_.GetMap();
}
inline ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::JobSignatureDef >*
GraphDef::mutable_signatures() {
  // @@protoc_insertion_point(field_mutable_map:oneflow.GraphDef.signatures)
  return signatures_.MutableMap();
}

// optional string default_signature_name = 3;
inline bool GraphDef::has_default_signature_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void GraphDef::clear_default_signature_name() {
  default_signature_name_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _has_bits_[0] &= ~0x00000001u;
}
inline const std::string& GraphDef::default_signature_name() const {
  // @@protoc_insertion_point(field_get:oneflow.GraphDef.default_signature_name)
  return default_signature_name_.GetNoArena();
}
inline void GraphDef::set_default_signature_name(const std::string& value) {
  _has_bits_[0] |= 0x00000001u;
  default_signature_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:oneflow.GraphDef.default_signature_name)
}
inline void GraphDef::set_default_signature_name(std::string&& value) {
  _has_bits_[0] |= 0x00000001u;
  default_signature_name_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.GraphDef.default_signature_name)
}
inline void GraphDef::set_default_signature_name(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000001u;
  default_signature_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.GraphDef.default_signature_name)
}
inline void GraphDef::set_default_signature_name(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000001u;
  default_signature_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.GraphDef.default_signature_name)
}
inline std::string* GraphDef::mutable_default_signature_name() {
  _has_bits_[0] |= 0x00000001u;
  // @@protoc_insertion_point(field_mutable:oneflow.GraphDef.default_signature_name)
  return default_signature_name_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* GraphDef::release_default_signature_name() {
  // @@protoc_insertion_point(field_release:oneflow.GraphDef.default_signature_name)
  if (!has_default_signature_name()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000001u;
  return default_signature_name_.ReleaseNonDefaultNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void GraphDef::set_allocated_default_signature_name(std::string* default_signature_name) {
  if (default_signature_name != nullptr) {
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  default_signature_name_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), default_signature_name);
  // @@protoc_insertion_point(field_set_allocated:oneflow.GraphDef.default_signature_name)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------

// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace oneflow

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fserving_2fsaved_5fmodel_2eproto
