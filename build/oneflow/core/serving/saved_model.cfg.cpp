#include "oneflow/core/serving/saved_model.cfg.h"
#include "oneflow/core/operator/op_conf.cfg.h"
#include "oneflow/core/job/job_conf.cfg.h"
#include "oneflow/core/serving/saved_model.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstSavedModel::_SavedModel_::_SavedModel_() { Clear(); }
ConstSavedModel::_SavedModel_::_SavedModel_(const _SavedModel_& other) { CopyFrom(other); }
ConstSavedModel::_SavedModel_::_SavedModel_(const ::oneflow::SavedModel& proto_savedmodel) {
  InitFromProto(proto_savedmodel);
}
ConstSavedModel::_SavedModel_::_SavedModel_(_SavedModel_&& other) = default;
ConstSavedModel::_SavedModel_::~_SavedModel_() = default;

void ConstSavedModel::_SavedModel_::InitFromProto(const ::oneflow::SavedModel& proto_savedmodel) {
  Clear();
  // required_or_optional field: name
  if (proto_savedmodel.has_name()) {
    set_name(proto_savedmodel.name());
  }
  // required_or_optional field: version
  if (proto_savedmodel.has_version()) {
    set_version(proto_savedmodel.version());
  }
  // required_or_optional field: checkpoint_dir
  if (proto_savedmodel.has_checkpoint_dir()) {
    set_checkpoint_dir(proto_savedmodel.checkpoint_dir());
  }
  // map field : graphs
  if (!proto_savedmodel.graphs().empty()) {
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_&  mut_graphs = *mutable_graphs();
    for (const auto& pair : proto_savedmodel.graphs()) {
      mut_graphs[pair.first] = ::oneflow::cfg::GraphDef(pair.second);
    }
  }
  // required_or_optional field: default_graph_name
  if (proto_savedmodel.has_default_graph_name()) {
    set_default_graph_name(proto_savedmodel.default_graph_name());
  }
    
}

void ConstSavedModel::_SavedModel_::ToProto(::oneflow::SavedModel* proto_savedmodel) const {
  proto_savedmodel->Clear();
  // required_or_optional field: name
  if (this->has_name()) {
    proto_savedmodel->set_name(name());
    }
  // required_or_optional field: version
  if (this->has_version()) {
    proto_savedmodel->set_version(version());
    }
  // required_or_optional field: checkpoint_dir
  if (this->has_checkpoint_dir()) {
    proto_savedmodel->set_checkpoint_dir(checkpoint_dir());
    }
  // map field : graphs
  if (!graphs().empty()) {
    auto& mut_graphs = *(proto_savedmodel->mutable_graphs());
    for (const auto& pair : graphs()) {
      ::oneflow::GraphDef proto_graphs_value;
      pair.second.ToProto(&proto_graphs_value);
      mut_graphs[pair.first] = proto_graphs_value;
    }
  }
  // required_or_optional field: default_graph_name
  if (this->has_default_graph_name()) {
    proto_savedmodel->set_default_graph_name(default_graph_name());
    }

}

::std::string ConstSavedModel::_SavedModel_::DebugString() const {
  ::oneflow::SavedModel proto_savedmodel;
  this->ToProto(&proto_savedmodel);
  return proto_savedmodel.DebugString();
}

void ConstSavedModel::_SavedModel_::Clear() {
  clear_name();
  clear_version();
  clear_checkpoint_dir();
  clear_graphs();
  clear_default_graph_name();
}

void ConstSavedModel::_SavedModel_::CopyFrom(const _SavedModel_& other) {
  if (other.has_name()) {
    set_name(other.name());
  } else {
    clear_name();
  }
  if (other.has_version()) {
    set_version(other.version());
  } else {
    clear_version();
  }
  if (other.has_checkpoint_dir()) {
    set_checkpoint_dir(other.checkpoint_dir());
  } else {
    clear_checkpoint_dir();
  }
  mutable_graphs()->CopyFrom(other.graphs());
  if (other.has_default_graph_name()) {
    set_default_graph_name(other.default_graph_name());
  } else {
    clear_default_graph_name();
  }
}


// optional field name
bool ConstSavedModel::_SavedModel_::has_name() const {
  return has_name_;
}
const ::std::string& ConstSavedModel::_SavedModel_::name() const {
  if (has_name_) { return name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstSavedModel::_SavedModel_::clear_name() {
  has_name_ = false;
}
void ConstSavedModel::_SavedModel_::set_name(const ::std::string& value) {
  name_ = value;
  has_name_ = true;
}
::std::string* ConstSavedModel::_SavedModel_::mutable_name() {
  has_name_ = true;
  return &name_;
}

// optional field version
bool ConstSavedModel::_SavedModel_::has_version() const {
  return has_version_;
}
const int64_t& ConstSavedModel::_SavedModel_::version() const {
  if (has_version_) { return version_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstSavedModel::_SavedModel_::clear_version() {
  has_version_ = false;
}
void ConstSavedModel::_SavedModel_::set_version(const int64_t& value) {
  version_ = value;
  has_version_ = true;
}
int64_t* ConstSavedModel::_SavedModel_::mutable_version() {
  has_version_ = true;
  return &version_;
}

// optional field checkpoint_dir
bool ConstSavedModel::_SavedModel_::has_checkpoint_dir() const {
  return has_checkpoint_dir_;
}
const ::std::string& ConstSavedModel::_SavedModel_::checkpoint_dir() const {
  if (has_checkpoint_dir_) { return checkpoint_dir_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstSavedModel::_SavedModel_::clear_checkpoint_dir() {
  has_checkpoint_dir_ = false;
}
void ConstSavedModel::_SavedModel_::set_checkpoint_dir(const ::std::string& value) {
  checkpoint_dir_ = value;
  has_checkpoint_dir_ = true;
}
::std::string* ConstSavedModel::_SavedModel_::mutable_checkpoint_dir() {
  has_checkpoint_dir_ = true;
  return &checkpoint_dir_;
}

::std::size_t ConstSavedModel::_SavedModel_::graphs_size() const {
  if (!graphs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>();
    return default_static_value->size();
  }
  return graphs_->size();
}
const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& ConstSavedModel::_SavedModel_::graphs() const {
  if (!graphs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>();
    return *(default_static_value.get());
  }
  return *(graphs_.get());
}

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ * ConstSavedModel::_SavedModel_::mutable_graphs() {
  if (!graphs_) {
    graphs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>();
  }
  return graphs_.get();
}

const ::oneflow::cfg::GraphDef& ConstSavedModel::_SavedModel_::graphs(::std::string key) const {
  if (!graphs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>();
    return default_static_value->at(key);
  }
  return graphs_->at(key);
}

void ConstSavedModel::_SavedModel_::clear_graphs() {
  if (!graphs_) {
    graphs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>();
  }
  return graphs_->Clear();
}


// optional field default_graph_name
bool ConstSavedModel::_SavedModel_::has_default_graph_name() const {
  return has_default_graph_name_;
}
const ::std::string& ConstSavedModel::_SavedModel_::default_graph_name() const {
  if (has_default_graph_name_) { return default_graph_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstSavedModel::_SavedModel_::clear_default_graph_name() {
  has_default_graph_name_ = false;
}
void ConstSavedModel::_SavedModel_::set_default_graph_name(const ::std::string& value) {
  default_graph_name_ = value;
  has_default_graph_name_ = true;
}
::std::string* ConstSavedModel::_SavedModel_::mutable_default_graph_name() {
  has_default_graph_name_ = true;
  return &default_graph_name_;
}


int ConstSavedModel::_SavedModel_::compare(const _SavedModel_& other) {
  if (!(has_name() == other.has_name())) {
    return has_name() < other.has_name() ? -1 : 1;
  } else if (!(name() == other.name())) {
    return name() < other.name() ? -1 : 1;
  }
  if (!(has_version() == other.has_version())) {
    return has_version() < other.has_version() ? -1 : 1;
  } else if (!(version() == other.version())) {
    return version() < other.version() ? -1 : 1;
  }
  if (!(has_checkpoint_dir() == other.has_checkpoint_dir())) {
    return has_checkpoint_dir() < other.has_checkpoint_dir() ? -1 : 1;
  } else if (!(checkpoint_dir() == other.checkpoint_dir())) {
    return checkpoint_dir() < other.checkpoint_dir() ? -1 : 1;
  }
  if (!(graphs() == other.graphs())) {
    return graphs() < other.graphs() ? -1 : 1;
  }
  if (!(has_default_graph_name() == other.has_default_graph_name())) {
    return has_default_graph_name() < other.has_default_graph_name() ? -1 : 1;
  } else if (!(default_graph_name() == other.default_graph_name())) {
    return default_graph_name() < other.default_graph_name() ? -1 : 1;
  }
  return 0;
}

bool ConstSavedModel::_SavedModel_::operator==(const _SavedModel_& other) const {
  return true
    && has_name() == other.has_name() 
    && name() == other.name()
    && has_version() == other.has_version() 
    && version() == other.version()
    && has_checkpoint_dir() == other.has_checkpoint_dir() 
    && checkpoint_dir() == other.checkpoint_dir()
    && graphs() == other.graphs()
    && has_default_graph_name() == other.has_default_graph_name() 
    && default_graph_name() == other.default_graph_name()
  ;
}

std::size_t ConstSavedModel::_SavedModel_::__CalcHash__() const {
  return 0
    ^ (has_name() ? std::hash<::std::string>()(name()) : 0)
    ^ (has_version() ? std::hash<int64_t>()(version()) : 0)
    ^ (has_checkpoint_dir() ? std::hash<::std::string>()(checkpoint_dir()) : 0)
    ^ graphs().__CalcHash__()
    ^ (has_default_graph_name() ? std::hash<::std::string>()(default_graph_name()) : 0)
  ;
}

bool ConstSavedModel::_SavedModel_::operator<(const _SavedModel_& other) const {
  return false
    || !(has_name() == other.has_name()) ? 
      has_name() < other.has_name() : false
    || !(name() == other.name()) ? 
      name() < other.name() : false
    || !(has_version() == other.has_version()) ? 
      has_version() < other.has_version() : false
    || !(version() == other.version()) ? 
      version() < other.version() : false
    || !(has_checkpoint_dir() == other.has_checkpoint_dir()) ? 
      has_checkpoint_dir() < other.has_checkpoint_dir() : false
    || !(checkpoint_dir() == other.checkpoint_dir()) ? 
      checkpoint_dir() < other.checkpoint_dir() : false
    || !(graphs() == other.graphs()) ? 
      graphs() < other.graphs() : false
    || !(has_default_graph_name() == other.has_default_graph_name()) ? 
      has_default_graph_name() < other.has_default_graph_name() : false
    || !(default_graph_name() == other.default_graph_name()) ? 
      default_graph_name() < other.default_graph_name() : false
  ;
}

using _SavedModel_ =  ConstSavedModel::_SavedModel_;
ConstSavedModel::ConstSavedModel(const ::std::shared_ptr<_SavedModel_>& data): data_(data) {}
ConstSavedModel::ConstSavedModel(): data_(::std::make_shared<_SavedModel_>()) {}
ConstSavedModel::ConstSavedModel(const ::oneflow::SavedModel& proto_savedmodel) {
  BuildFromProto(proto_savedmodel);
}
ConstSavedModel::ConstSavedModel(const ConstSavedModel&) = default;
ConstSavedModel::ConstSavedModel(ConstSavedModel&&) noexcept = default;
ConstSavedModel::~ConstSavedModel() = default;

void ConstSavedModel::ToProto(PbMessage* proto_savedmodel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SavedModel*>(proto_savedmodel));
}
  
::std::string ConstSavedModel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSavedModel::__Empty__() const {
  return !data_;
}

int ConstSavedModel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"name", 1},
    {"version", 2},
    {"checkpoint_dir", 3},
    {"graphs", 4},
    {"default_graph_name", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstSavedModel::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstSavedModel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::GraphDef>)
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSavedModel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &name();
    case 2: return &version();
    case 3: return &checkpoint_dir();
    case 4: return &graphs();
    case 5: return &default_graph_name();
    default: return nullptr;
  }
}

// required or optional field name
bool ConstSavedModel::has_name() const {
  return __SharedPtrOrDefault__()->has_name();
}
const ::std::string& ConstSavedModel::name() const {
  return __SharedPtrOrDefault__()->name();
}
// used by pybind11 only
// required or optional field version
bool ConstSavedModel::has_version() const {
  return __SharedPtrOrDefault__()->has_version();
}
const int64_t& ConstSavedModel::version() const {
  return __SharedPtrOrDefault__()->version();
}
// used by pybind11 only
// required or optional field checkpoint_dir
bool ConstSavedModel::has_checkpoint_dir() const {
  return __SharedPtrOrDefault__()->has_checkpoint_dir();
}
const ::std::string& ConstSavedModel::checkpoint_dir() const {
  return __SharedPtrOrDefault__()->checkpoint_dir();
}
// used by pybind11 only
// map field graphs
::std::size_t ConstSavedModel::graphs_size() const {
  return __SharedPtrOrDefault__()->graphs_size();
}

const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& ConstSavedModel::graphs() const {
  return __SharedPtrOrDefault__()->graphs();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> ConstSavedModel::shared_const_graphs() const {
  return graphs().__SharedConst__();
}
// required or optional field default_graph_name
bool ConstSavedModel::has_default_graph_name() const {
  return __SharedPtrOrDefault__()->has_default_graph_name();
}
const ::std::string& ConstSavedModel::default_graph_name() const {
  return __SharedPtrOrDefault__()->default_graph_name();
}
// used by pybind11 only

::std::shared_ptr<ConstSavedModel> ConstSavedModel::__SharedConst__() const {
  return ::std::make_shared<ConstSavedModel>(data_);
}
int64_t ConstSavedModel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSavedModel::operator==(const ConstSavedModel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSavedModel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSavedModel::operator<(const ConstSavedModel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SavedModel_>& ConstSavedModel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SavedModel_> default_ptr = std::make_shared<_SavedModel_>();
  return default_ptr;
}
const ::std::shared_ptr<_SavedModel_>& ConstSavedModel::__SharedPtr__() {
  if (!data_) { data_.reset(new _SavedModel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSavedModel
void ConstSavedModel::BuildFromProto(const PbMessage& proto_savedmodel) {
  data_ = ::std::make_shared<_SavedModel_>(dynamic_cast<const ::oneflow::SavedModel&>(proto_savedmodel));
}

SavedModel::SavedModel(const ::std::shared_ptr<_SavedModel_>& data)
  : ConstSavedModel(data) {}
SavedModel::SavedModel(const SavedModel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SavedModel> resize
SavedModel::SavedModel(SavedModel&&) noexcept = default; 
SavedModel::SavedModel(const ::oneflow::SavedModel& proto_savedmodel) {
  InitFromProto(proto_savedmodel);
}
SavedModel::SavedModel() = default;

SavedModel::~SavedModel() = default;

void SavedModel::InitFromProto(const PbMessage& proto_savedmodel) {
  BuildFromProto(proto_savedmodel);
}
  
void* SavedModel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_name();
    case 2: return mutable_version();
    case 3: return mutable_checkpoint_dir();
    case 4: return mutable_graphs();
    case 5: return mutable_default_graph_name();
    default: return nullptr;
  }
}

bool SavedModel::operator==(const SavedModel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SavedModel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SavedModel::operator<(const SavedModel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SavedModel::Clear() {
  if (data_) { data_.reset(); }
}
void SavedModel::CopyFrom(const SavedModel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SavedModel& SavedModel::operator=(const SavedModel& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field name
void SavedModel::clear_name() {
  return __SharedPtr__()->clear_name();
}
void SavedModel::set_name(const ::std::string& value) {
  return __SharedPtr__()->set_name(value);
}
::std::string* SavedModel::mutable_name() {
  return  __SharedPtr__()->mutable_name();
}
// required or optional field version
void SavedModel::clear_version() {
  return __SharedPtr__()->clear_version();
}
void SavedModel::set_version(const int64_t& value) {
  return __SharedPtr__()->set_version(value);
}
int64_t* SavedModel::mutable_version() {
  return  __SharedPtr__()->mutable_version();
}
// required or optional field checkpoint_dir
void SavedModel::clear_checkpoint_dir() {
  return __SharedPtr__()->clear_checkpoint_dir();
}
void SavedModel::set_checkpoint_dir(const ::std::string& value) {
  return __SharedPtr__()->set_checkpoint_dir(value);
}
::std::string* SavedModel::mutable_checkpoint_dir() {
  return  __SharedPtr__()->mutable_checkpoint_dir();
}
// repeated field graphs
void SavedModel::clear_graphs() {
  return __SharedPtr__()->clear_graphs();
}

const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ & SavedModel::graphs() const {
  return __SharedConst__()->graphs();
}

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_* SavedModel::mutable_graphs() {
  return __SharedPtr__()->mutable_graphs();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> SavedModel::shared_mutable_graphs() {
  return mutable_graphs()->__SharedMutable__();
}
// required or optional field default_graph_name
void SavedModel::clear_default_graph_name() {
  return __SharedPtr__()->clear_default_graph_name();
}
void SavedModel::set_default_graph_name(const ::std::string& value) {
  return __SharedPtr__()->set_default_graph_name(value);
}
::std::string* SavedModel::mutable_default_graph_name() {
  return  __SharedPtr__()->mutable_default_graph_name();
}

::std::shared_ptr<SavedModel> SavedModel::__SharedMutable__() {
  return ::std::make_shared<SavedModel>(__SharedPtr__());
}
ConstGraphDef::_GraphDef_::_GraphDef_() { Clear(); }
ConstGraphDef::_GraphDef_::_GraphDef_(const _GraphDef_& other) { CopyFrom(other); }
ConstGraphDef::_GraphDef_::_GraphDef_(const ::oneflow::GraphDef& proto_graphdef) {
  InitFromProto(proto_graphdef);
}
ConstGraphDef::_GraphDef_::_GraphDef_(_GraphDef_&& other) = default;
ConstGraphDef::_GraphDef_::~_GraphDef_() = default;

void ConstGraphDef::_GraphDef_::InitFromProto(const ::oneflow::GraphDef& proto_graphdef) {
  Clear();
  // repeated field: op_list
  if (!proto_graphdef.op_list().empty()) {
    for (const ::oneflow::OperatorConf& elem : proto_graphdef.op_list() ) {
      *mutable_op_list()->Add() = ::oneflow::cfg::OperatorConf(elem);
    }
  }
  // map field : signatures
  if (!proto_graphdef.signatures().empty()) {
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_&  mut_signatures = *mutable_signatures();
    for (const auto& pair : proto_graphdef.signatures()) {
      mut_signatures[pair.first] = ::oneflow::cfg::JobSignatureDef(pair.second);
    }
  }
  // required_or_optional field: default_signature_name
  if (proto_graphdef.has_default_signature_name()) {
    set_default_signature_name(proto_graphdef.default_signature_name());
  }
    
}

void ConstGraphDef::_GraphDef_::ToProto(::oneflow::GraphDef* proto_graphdef) const {
  proto_graphdef->Clear();
  // repeated field: op_list
  if (!op_list().empty()) {
    for (const ::oneflow::cfg::OperatorConf& elem : op_list() ) {
      ::oneflow::OperatorConf proto_op_list_elem;
      elem.ToProto(&proto_op_list_elem);
      *proto_graphdef->mutable_op_list()->Add() = proto_op_list_elem;
    }
  }
  // map field : signatures
  if (!signatures().empty()) {
    auto& mut_signatures = *(proto_graphdef->mutable_signatures());
    for (const auto& pair : signatures()) {
      ::oneflow::JobSignatureDef proto_signatures_value;
      pair.second.ToProto(&proto_signatures_value);
      mut_signatures[pair.first] = proto_signatures_value;
    }
  }
  // required_or_optional field: default_signature_name
  if (this->has_default_signature_name()) {
    proto_graphdef->set_default_signature_name(default_signature_name());
    }

}

::std::string ConstGraphDef::_GraphDef_::DebugString() const {
  ::oneflow::GraphDef proto_graphdef;
  this->ToProto(&proto_graphdef);
  return proto_graphdef.DebugString();
}

void ConstGraphDef::_GraphDef_::Clear() {
  clear_op_list();
  clear_signatures();
  clear_default_signature_name();
}

void ConstGraphDef::_GraphDef_::CopyFrom(const _GraphDef_& other) {
  mutable_op_list()->CopyFrom(other.op_list());
  mutable_signatures()->CopyFrom(other.signatures());
  if (other.has_default_signature_name()) {
    set_default_signature_name(other.default_signature_name());
  } else {
    clear_default_signature_name();
  }
}


// repeated field op_list
::std::size_t ConstGraphDef::_GraphDef_::op_list_size() const {
  if (!op_list_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
    return default_static_value->size();
  }
  return op_list_->size();
}
const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& ConstGraphDef::_GraphDef_::op_list() const {
  if (!op_list_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
    return *(default_static_value.get());
  }
  return *(op_list_.get());
}
const ::oneflow::cfg::OperatorConf& ConstGraphDef::_GraphDef_::op_list(::std::size_t index) const {
  if (!op_list_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
    return default_static_value->Get(index);
  }
  return op_list_->Get(index);
}
void ConstGraphDef::_GraphDef_::clear_op_list() {
  if (!op_list_) {
    op_list_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
  }
  return op_list_->Clear();
}
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_* ConstGraphDef::_GraphDef_::mutable_op_list() {
  if (!op_list_) {
    op_list_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
  }
  return  op_list_.get();
}
::oneflow::cfg::OperatorConf* ConstGraphDef::_GraphDef_::mutable_op_list(::std::size_t index) {
  if (!op_list_) {
    op_list_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
  }
  return  op_list_->Mutable(index);
}
::oneflow::cfg::OperatorConf* ConstGraphDef::_GraphDef_::add_op_list() {
  if (!op_list_) {
    op_list_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>();
  }
  return op_list_->Add();
}

::std::size_t ConstGraphDef::_GraphDef_::signatures_size() const {
  if (!signatures_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>();
    return default_static_value->size();
  }
  return signatures_->size();
}
const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& ConstGraphDef::_GraphDef_::signatures() const {
  if (!signatures_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>();
    return *(default_static_value.get());
  }
  return *(signatures_.get());
}

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ * ConstGraphDef::_GraphDef_::mutable_signatures() {
  if (!signatures_) {
    signatures_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>();
  }
  return signatures_.get();
}

const ::oneflow::cfg::JobSignatureDef& ConstGraphDef::_GraphDef_::signatures(::std::string key) const {
  if (!signatures_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>();
    return default_static_value->at(key);
  }
  return signatures_->at(key);
}

void ConstGraphDef::_GraphDef_::clear_signatures() {
  if (!signatures_) {
    signatures_ = ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>();
  }
  return signatures_->Clear();
}


// optional field default_signature_name
bool ConstGraphDef::_GraphDef_::has_default_signature_name() const {
  return has_default_signature_name_;
}
const ::std::string& ConstGraphDef::_GraphDef_::default_signature_name() const {
  if (has_default_signature_name_) { return default_signature_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstGraphDef::_GraphDef_::clear_default_signature_name() {
  has_default_signature_name_ = false;
}
void ConstGraphDef::_GraphDef_::set_default_signature_name(const ::std::string& value) {
  default_signature_name_ = value;
  has_default_signature_name_ = true;
}
::std::string* ConstGraphDef::_GraphDef_::mutable_default_signature_name() {
  has_default_signature_name_ = true;
  return &default_signature_name_;
}


int ConstGraphDef::_GraphDef_::compare(const _GraphDef_& other) {
  if (!(op_list() == other.op_list())) {
    return op_list() < other.op_list() ? -1 : 1;
  }
  if (!(signatures() == other.signatures())) {
    return signatures() < other.signatures() ? -1 : 1;
  }
  if (!(has_default_signature_name() == other.has_default_signature_name())) {
    return has_default_signature_name() < other.has_default_signature_name() ? -1 : 1;
  } else if (!(default_signature_name() == other.default_signature_name())) {
    return default_signature_name() < other.default_signature_name() ? -1 : 1;
  }
  return 0;
}

bool ConstGraphDef::_GraphDef_::operator==(const _GraphDef_& other) const {
  return true
    && op_list() == other.op_list()
    && signatures() == other.signatures()
    && has_default_signature_name() == other.has_default_signature_name() 
    && default_signature_name() == other.default_signature_name()
  ;
}

std::size_t ConstGraphDef::_GraphDef_::__CalcHash__() const {
  return 0
    ^ op_list().__CalcHash__()
    ^ signatures().__CalcHash__()
    ^ (has_default_signature_name() ? std::hash<::std::string>()(default_signature_name()) : 0)
  ;
}

bool ConstGraphDef::_GraphDef_::operator<(const _GraphDef_& other) const {
  return false
    || !(op_list() == other.op_list()) ? 
      op_list() < other.op_list() : false
    || !(signatures() == other.signatures()) ? 
      signatures() < other.signatures() : false
    || !(has_default_signature_name() == other.has_default_signature_name()) ? 
      has_default_signature_name() < other.has_default_signature_name() : false
    || !(default_signature_name() == other.default_signature_name()) ? 
      default_signature_name() < other.default_signature_name() : false
  ;
}

using _GraphDef_ =  ConstGraphDef::_GraphDef_;
ConstGraphDef::ConstGraphDef(const ::std::shared_ptr<_GraphDef_>& data): data_(data) {}
ConstGraphDef::ConstGraphDef(): data_(::std::make_shared<_GraphDef_>()) {}
ConstGraphDef::ConstGraphDef(const ::oneflow::GraphDef& proto_graphdef) {
  BuildFromProto(proto_graphdef);
}
ConstGraphDef::ConstGraphDef(const ConstGraphDef&) = default;
ConstGraphDef::ConstGraphDef(ConstGraphDef&&) noexcept = default;
ConstGraphDef::~ConstGraphDef() = default;

void ConstGraphDef::ToProto(PbMessage* proto_graphdef) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::GraphDef*>(proto_graphdef));
}
  
::std::string ConstGraphDef::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstGraphDef::__Empty__() const {
  return !data_;
}

int ConstGraphDef::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_list", 1},
    {"signatures", 2},
    {"default_signature_name", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstGraphDef::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstGraphDef::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobSignatureDef>)
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstGraphDef::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_list();
    case 2: return &signatures();
    case 3: return &default_signature_name();
    default: return nullptr;
  }
}

// repeated field op_list
::std::size_t ConstGraphDef::op_list_size() const {
  return __SharedPtrOrDefault__()->op_list_size();
}
const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& ConstGraphDef::op_list() const {
  return __SharedPtrOrDefault__()->op_list();
}
const ::oneflow::cfg::OperatorConf& ConstGraphDef::op_list(::std::size_t index) const {
  return __SharedPtrOrDefault__()->op_list(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> ConstGraphDef::shared_const_op_list() const {
  return op_list().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> ConstGraphDef::shared_const_op_list(::std::size_t index) const {
  return op_list(index).__SharedConst__();
}
// map field signatures
::std::size_t ConstGraphDef::signatures_size() const {
  return __SharedPtrOrDefault__()->signatures_size();
}

const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& ConstGraphDef::signatures() const {
  return __SharedPtrOrDefault__()->signatures();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> ConstGraphDef::shared_const_signatures() const {
  return signatures().__SharedConst__();
}
// required or optional field default_signature_name
bool ConstGraphDef::has_default_signature_name() const {
  return __SharedPtrOrDefault__()->has_default_signature_name();
}
const ::std::string& ConstGraphDef::default_signature_name() const {
  return __SharedPtrOrDefault__()->default_signature_name();
}
// used by pybind11 only

::std::shared_ptr<ConstGraphDef> ConstGraphDef::__SharedConst__() const {
  return ::std::make_shared<ConstGraphDef>(data_);
}
int64_t ConstGraphDef::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstGraphDef::operator==(const ConstGraphDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstGraphDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstGraphDef::operator<(const ConstGraphDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_GraphDef_>& ConstGraphDef::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_GraphDef_> default_ptr = std::make_shared<_GraphDef_>();
  return default_ptr;
}
const ::std::shared_ptr<_GraphDef_>& ConstGraphDef::__SharedPtr__() {
  if (!data_) { data_.reset(new _GraphDef_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstGraphDef
void ConstGraphDef::BuildFromProto(const PbMessage& proto_graphdef) {
  data_ = ::std::make_shared<_GraphDef_>(dynamic_cast<const ::oneflow::GraphDef&>(proto_graphdef));
}

GraphDef::GraphDef(const ::std::shared_ptr<_GraphDef_>& data)
  : ConstGraphDef(data) {}
GraphDef::GraphDef(const GraphDef& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<GraphDef> resize
GraphDef::GraphDef(GraphDef&&) noexcept = default; 
GraphDef::GraphDef(const ::oneflow::GraphDef& proto_graphdef) {
  InitFromProto(proto_graphdef);
}
GraphDef::GraphDef() = default;

GraphDef::~GraphDef() = default;

void GraphDef::InitFromProto(const PbMessage& proto_graphdef) {
  BuildFromProto(proto_graphdef);
}
  
void* GraphDef::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_list();
    case 2: return mutable_signatures();
    case 3: return mutable_default_signature_name();
    default: return nullptr;
  }
}

bool GraphDef::operator==(const GraphDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t GraphDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool GraphDef::operator<(const GraphDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void GraphDef::Clear() {
  if (data_) { data_.reset(); }
}
void GraphDef::CopyFrom(const GraphDef& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
GraphDef& GraphDef::operator=(const GraphDef& other) {
  CopyFrom(other);
  return *this;
}

// repeated field op_list
void GraphDef::clear_op_list() {
  return __SharedPtr__()->clear_op_list();
}
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_* GraphDef::mutable_op_list() {
  return __SharedPtr__()->mutable_op_list();
}
::oneflow::cfg::OperatorConf* GraphDef::mutable_op_list(::std::size_t index) {
  return __SharedPtr__()->mutable_op_list(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> GraphDef::shared_mutable_op_list() {
  return mutable_op_list()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OperatorConf> GraphDef::shared_mutable_op_list(::std::size_t index) {
  return mutable_op_list(index)->__SharedMutable__();
}
::oneflow::cfg::OperatorConf* GraphDef::add_op_list() {
  return __SharedPtr__()->add_op_list();
}
// repeated field signatures
void GraphDef::clear_signatures() {
  return __SharedPtr__()->clear_signatures();
}

const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ & GraphDef::signatures() const {
  return __SharedConst__()->signatures();
}

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_* GraphDef::mutable_signatures() {
  return __SharedPtr__()->mutable_signatures();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> GraphDef::shared_mutable_signatures() {
  return mutable_signatures()->__SharedMutable__();
}
// required or optional field default_signature_name
void GraphDef::clear_default_signature_name() {
  return __SharedPtr__()->clear_default_signature_name();
}
void GraphDef::set_default_signature_name(const ::std::string& value) {
  return __SharedPtr__()->set_default_signature_name(value);
}
::std::string* GraphDef::mutable_default_signature_name() {
  return  __SharedPtr__()->mutable_default_signature_name();
}

::std::shared_ptr<GraphDef> GraphDef::__SharedMutable__() {
  return ::std::make_shared<GraphDef>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::GraphDef>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::GraphDef>(data) {}
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_() = default;
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_() = default;

bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::GraphDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::GraphDef& Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstGraphDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ConstGraphDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ConstGraphDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::GraphDef>>& data): Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_(data) {}
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_() = default;
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_() = default;

void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::GraphDef>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::GraphDef>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::GraphDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::GraphDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ::oneflow::cfg::GraphDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ::oneflow::cfg::GraphDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf>(data) {}
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_() = default;
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_() = default;


bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OperatorConf>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data): Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_(data) {}
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_() = default;
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_() = default;

void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OperatorConf>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::OperatorConf> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OperatorConf> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobSignatureDef>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobSignatureDef>(data) {}
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_() = default;
Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_() = default;

bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobSignatureDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::JobSignatureDef& Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstJobSignatureDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ConstJobSignatureDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ConstJobSignatureDef> Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobSignatureDef>>& data): Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_(data) {}
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_() = default;
_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_() = default;

void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobSignatureDef>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobSignatureDef>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobSignatureDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::JobSignatureDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ::oneflow::cfg::JobSignatureDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ::oneflow::cfg::JobSignatureDef> _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_::shared_mut_end() { return end(); }

}
} // namespace oneflow
