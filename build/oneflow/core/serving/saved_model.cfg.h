#ifndef CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H_
#define CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstJobSignatureDef;
class JobSignatureDef;
}
}
namespace oneflow {
namespace cfg {
class ConstOperatorConf;
class OperatorConf;
}
}

namespace oneflow {

// forward declare proto class;
class SavedModel_GraphsEntry;
class SavedModel;
class GraphDef_SignaturesEntry;
class GraphDef;

namespace cfg {


class SavedModel;
class ConstSavedModel;

class GraphDef;
class ConstGraphDef;


class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_; 
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_;

class ConstSavedModel : public ::oneflow::cfg::Message {
 public:

  class _SavedModel_ {
   public:
    _SavedModel_();
    explicit _SavedModel_(const _SavedModel_& other);
    explicit _SavedModel_(_SavedModel_&& other);
    _SavedModel_(const ::oneflow::SavedModel& proto_savedmodel);
    ~_SavedModel_();

    void InitFromProto(const ::oneflow::SavedModel& proto_savedmodel);

    void ToProto(::oneflow::SavedModel* proto_savedmodel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SavedModel_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
      // optional field version
     public:
    bool has_version() const;
    const int64_t& version() const;
    void clear_version();
    void set_version(const int64_t& value);
    int64_t* mutable_version();
   protected:
    bool has_version_ = false;
    int64_t version_;
      
      // optional field checkpoint_dir
     public:
    bool has_checkpoint_dir() const;
    const ::std::string& checkpoint_dir() const;
    void clear_checkpoint_dir();
    void set_checkpoint_dir(const ::std::string& value);
    ::std::string* mutable_checkpoint_dir();
   protected:
    bool has_checkpoint_dir_ = false;
    ::std::string checkpoint_dir_;
      
     public:
    ::std::size_t graphs_size() const;
    const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& graphs() const;

    _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ * mutable_graphs();

    const ::oneflow::cfg::GraphDef& graphs(::std::string key) const;

    void clear_graphs();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> graphs_;
    
      // optional field default_graph_name
     public:
    bool has_default_graph_name() const;
    const ::std::string& default_graph_name() const;
    void clear_default_graph_name();
    void set_default_graph_name(const ::std::string& value);
    ::std::string* mutable_default_graph_name();
   protected:
    bool has_default_graph_name_ = false;
    ::std::string default_graph_name_;
           
   public:
    int compare(const _SavedModel_& other);

    bool operator==(const _SavedModel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SavedModel_& other) const;
  };

  ConstSavedModel(const ::std::shared_ptr<_SavedModel_>& data);
  ConstSavedModel(const ConstSavedModel&);
  ConstSavedModel(ConstSavedModel&&) noexcept;
  ConstSavedModel();
  ConstSavedModel(const ::oneflow::SavedModel& proto_savedmodel);
  virtual ~ConstSavedModel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_savedmodel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // required or optional field version
 public:
  bool has_version() const;
  const int64_t& version() const;
  // used by pybind11 only
  // required or optional field checkpoint_dir
 public:
  bool has_checkpoint_dir() const;
  const ::std::string& checkpoint_dir() const;
  // used by pybind11 only
  // map field graphs
 public:
  ::std::size_t graphs_size() const;
  const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& graphs() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> shared_const_graphs() const;
  // required or optional field default_graph_name
 public:
  bool has_default_graph_name() const;
  const ::std::string& default_graph_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSavedModel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSavedModel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSavedModel& other) const;
 protected:
  const ::std::shared_ptr<_SavedModel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SavedModel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSavedModel
  void BuildFromProto(const PbMessage& proto_savedmodel);
  
  ::std::shared_ptr<_SavedModel_> data_;
};

class SavedModel final : public ConstSavedModel {
 public:
  SavedModel(const ::std::shared_ptr<_SavedModel_>& data);
  SavedModel(const SavedModel& other);
  // enable nothrow for ::std::vector<SavedModel> resize 
  SavedModel(SavedModel&&) noexcept;
  SavedModel();
  explicit SavedModel(const ::oneflow::SavedModel& proto_savedmodel);

  ~SavedModel() override;

  void InitFromProto(const PbMessage& proto_savedmodel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SavedModel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SavedModel& other) const;
  void Clear();
  void CopyFrom(const SavedModel& other);
  SavedModel& operator=(const SavedModel& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // required or optional field version
 public:
  void clear_version();
  void set_version(const int64_t& value);
  int64_t* mutable_version();
  // required or optional field checkpoint_dir
 public:
  void clear_checkpoint_dir();
  void set_checkpoint_dir(const ::std::string& value);
  ::std::string* mutable_checkpoint_dir();
  // repeated field graphs
 public:
  void clear_graphs();

  const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ & graphs() const;

  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_* mutable_graphs();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> shared_mutable_graphs();
  // required or optional field default_graph_name
 public:
  void clear_default_graph_name();
  void set_default_graph_name(const ::std::string& value);
  ::std::string* mutable_default_graph_name();

  ::std::shared_ptr<SavedModel> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_;
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_;
class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_; 
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_;

class ConstGraphDef : public ::oneflow::cfg::Message {
 public:

  class _GraphDef_ {
   public:
    _GraphDef_();
    explicit _GraphDef_(const _GraphDef_& other);
    explicit _GraphDef_(_GraphDef_&& other);
    _GraphDef_(const ::oneflow::GraphDef& proto_graphdef);
    ~_GraphDef_();

    void InitFromProto(const ::oneflow::GraphDef& proto_graphdef);

    void ToProto(::oneflow::GraphDef* proto_graphdef) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _GraphDef_& other);
  
      // repeated field op_list
   public:
    ::std::size_t op_list_size() const;
    const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& op_list() const;
    const ::oneflow::cfg::OperatorConf& op_list(::std::size_t index) const;
    void clear_op_list();
    _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_* mutable_op_list();
    ::oneflow::cfg::OperatorConf* mutable_op_list(::std::size_t index);
      ::oneflow::cfg::OperatorConf* add_op_list();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> op_list_;
    
     public:
    ::std::size_t signatures_size() const;
    const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& signatures() const;

    _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ * mutable_signatures();

    const ::oneflow::cfg::JobSignatureDef& signatures(::std::string key) const;

    void clear_signatures();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> signatures_;
    
      // optional field default_signature_name
     public:
    bool has_default_signature_name() const;
    const ::std::string& default_signature_name() const;
    void clear_default_signature_name();
    void set_default_signature_name(const ::std::string& value);
    ::std::string* mutable_default_signature_name();
   protected:
    bool has_default_signature_name_ = false;
    ::std::string default_signature_name_;
           
   public:
    int compare(const _GraphDef_& other);

    bool operator==(const _GraphDef_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _GraphDef_& other) const;
  };

  ConstGraphDef(const ::std::shared_ptr<_GraphDef_>& data);
  ConstGraphDef(const ConstGraphDef&);
  ConstGraphDef(ConstGraphDef&&) noexcept;
  ConstGraphDef();
  ConstGraphDef(const ::oneflow::GraphDef& proto_graphdef);
  virtual ~ConstGraphDef() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_graphdef) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_list
 public:
  ::std::size_t op_list_size() const;
  const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& op_list() const;
  const ::oneflow::cfg::OperatorConf& op_list(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> shared_const_op_list() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> shared_const_op_list(::std::size_t index) const;
  // map field signatures
 public:
  ::std::size_t signatures_size() const;
  const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& signatures() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> shared_const_signatures() const;
  // required or optional field default_signature_name
 public:
  bool has_default_signature_name() const;
  const ::std::string& default_signature_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstGraphDef> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstGraphDef& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstGraphDef& other) const;
 protected:
  const ::std::shared_ptr<_GraphDef_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_GraphDef_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstGraphDef
  void BuildFromProto(const PbMessage& proto_graphdef);
  
  ::std::shared_ptr<_GraphDef_> data_;
};

class GraphDef final : public ConstGraphDef {
 public:
  GraphDef(const ::std::shared_ptr<_GraphDef_>& data);
  GraphDef(const GraphDef& other);
  // enable nothrow for ::std::vector<GraphDef> resize 
  GraphDef(GraphDef&&) noexcept;
  GraphDef();
  explicit GraphDef(const ::oneflow::GraphDef& proto_graphdef);

  ~GraphDef() override;

  void InitFromProto(const PbMessage& proto_graphdef) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const GraphDef& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const GraphDef& other) const;
  void Clear();
  void CopyFrom(const GraphDef& other);
  GraphDef& operator=(const GraphDef& other);

  // repeated field op_list
 public:
  void clear_op_list();
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_* mutable_op_list();
  ::oneflow::cfg::OperatorConf* mutable_op_list(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> shared_mutable_op_list();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> shared_mutable_op_list(::std::size_t index);
  ::oneflow::cfg::OperatorConf* add_op_list();
  // repeated field signatures
 public:
  void clear_signatures();

  const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ & signatures() const;

  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_* mutable_signatures();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> shared_mutable_signatures();
  // required or optional field default_signature_name
 public:
  void clear_default_signature_name();
  void set_default_signature_name(const ::std::string& value);
  ::std::string* mutable_default_signature_name();

  ::std::shared_ptr<GraphDef> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::GraphDef> {
 public:
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::GraphDef>>& data);
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_();
  ~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::GraphDef& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstGraphDef> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ConstGraphDef>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ final : public Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_ {
 public:
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::GraphDef>>& data);
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_();
  ~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::GraphDef> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_GraphDef_, ::oneflow::cfg::GraphDef>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf> {
 public:
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data);
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_();
  ~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_ final : public Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_ {
 public:
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data);
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_();
  ~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__RepeatedField_OperatorConf_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> __SharedMutable__(::std::size_t index);
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobSignatureDef> {
 public:
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobSignatureDef>>& data);
  Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_();
  ~Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::JobSignatureDef& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstJobSignatureDef> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ConstJobSignatureDef>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ final : public Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_ {
 public:
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobSignatureDef>>& data);
  _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_();
  ~_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::JobSignatureDef> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H__MapField___std__string_JobSignatureDef_, ::oneflow::cfg::JobSignatureDef>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstSavedModel> {
  std::size_t operator()(const ::oneflow::cfg::ConstSavedModel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SavedModel> {
  std::size_t operator()(const ::oneflow::cfg::SavedModel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstGraphDef> {
  std::size_t operator()(const ::oneflow::cfg::ConstGraphDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::GraphDef> {
  std::size_t operator()(const ::oneflow::cfg::GraphDef& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_SERVING_SAVED_MODEL_CFG_H_