#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/job/placement.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstParallelContext::_ParallelContext_::_ParallelContext_() { Clear(); }
ConstParallelContext::_ParallelContext_::_ParallelContext_(const _ParallelContext_& other) { CopyFrom(other); }
ConstParallelContext::_ParallelContext_::_ParallelContext_(const ::oneflow::ParallelContext& proto_parallelcontext) {
  InitFromProto(proto_parallelcontext);
}
ConstParallelContext::_ParallelContext_::_ParallelContext_(_ParallelContext_&& other) = default;
ConstParallelContext::_ParallelContext_::~_ParallelContext_() = default;

void ConstParallelContext::_ParallelContext_::InitFromProto(const ::oneflow::ParallelContext& proto_parallelcontext) {
  Clear();
  // required_or_optional field: parallel_id
  if (proto_parallelcontext.has_parallel_id()) {
    set_parallel_id(proto_parallelcontext.parallel_id());
  }
  // required_or_optional field: parallel_num
  if (proto_parallelcontext.has_parallel_num()) {
    set_parallel_num(proto_parallelcontext.parallel_num());
  }
    
}

void ConstParallelContext::_ParallelContext_::ToProto(::oneflow::ParallelContext* proto_parallelcontext) const {
  proto_parallelcontext->Clear();
  // required_or_optional field: parallel_id
  if (this->has_parallel_id()) {
    proto_parallelcontext->set_parallel_id(parallel_id());
    }
  // required_or_optional field: parallel_num
  if (this->has_parallel_num()) {
    proto_parallelcontext->set_parallel_num(parallel_num());
    }

}

::std::string ConstParallelContext::_ParallelContext_::DebugString() const {
  ::oneflow::ParallelContext proto_parallelcontext;
  this->ToProto(&proto_parallelcontext);
  return proto_parallelcontext.DebugString();
}

void ConstParallelContext::_ParallelContext_::Clear() {
  clear_parallel_id();
  clear_parallel_num();
}

void ConstParallelContext::_ParallelContext_::CopyFrom(const _ParallelContext_& other) {
  if (other.has_parallel_id()) {
    set_parallel_id(other.parallel_id());
  } else {
    clear_parallel_id();
  }
  if (other.has_parallel_num()) {
    set_parallel_num(other.parallel_num());
  } else {
    clear_parallel_num();
  }
}


// optional field parallel_id
bool ConstParallelContext::_ParallelContext_::has_parallel_id() const {
  return has_parallel_id_;
}
const int64_t& ConstParallelContext::_ParallelContext_::parallel_id() const {
  if (has_parallel_id_) { return parallel_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstParallelContext::_ParallelContext_::clear_parallel_id() {
  has_parallel_id_ = false;
}
void ConstParallelContext::_ParallelContext_::set_parallel_id(const int64_t& value) {
  parallel_id_ = value;
  has_parallel_id_ = true;
}
int64_t* ConstParallelContext::_ParallelContext_::mutable_parallel_id() {
  has_parallel_id_ = true;
  return &parallel_id_;
}

// optional field parallel_num
bool ConstParallelContext::_ParallelContext_::has_parallel_num() const {
  return has_parallel_num_;
}
const int64_t& ConstParallelContext::_ParallelContext_::parallel_num() const {
  if (has_parallel_num_) { return parallel_num_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstParallelContext::_ParallelContext_::clear_parallel_num() {
  has_parallel_num_ = false;
}
void ConstParallelContext::_ParallelContext_::set_parallel_num(const int64_t& value) {
  parallel_num_ = value;
  has_parallel_num_ = true;
}
int64_t* ConstParallelContext::_ParallelContext_::mutable_parallel_num() {
  has_parallel_num_ = true;
  return &parallel_num_;
}


int ConstParallelContext::_ParallelContext_::compare(const _ParallelContext_& other) {
  if (!(has_parallel_id() == other.has_parallel_id())) {
    return has_parallel_id() < other.has_parallel_id() ? -1 : 1;
  } else if (!(parallel_id() == other.parallel_id())) {
    return parallel_id() < other.parallel_id() ? -1 : 1;
  }
  if (!(has_parallel_num() == other.has_parallel_num())) {
    return has_parallel_num() < other.has_parallel_num() ? -1 : 1;
  } else if (!(parallel_num() == other.parallel_num())) {
    return parallel_num() < other.parallel_num() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelContext::_ParallelContext_::operator==(const _ParallelContext_& other) const {
  return true
    && has_parallel_id() == other.has_parallel_id() 
    && parallel_id() == other.parallel_id()
    && has_parallel_num() == other.has_parallel_num() 
    && parallel_num() == other.parallel_num()
  ;
}

std::size_t ConstParallelContext::_ParallelContext_::__CalcHash__() const {
  return 0
    ^ (has_parallel_id() ? std::hash<int64_t>()(parallel_id()) : 0)
    ^ (has_parallel_num() ? std::hash<int64_t>()(parallel_num()) : 0)
  ;
}

bool ConstParallelContext::_ParallelContext_::operator<(const _ParallelContext_& other) const {
  return false
    || !(has_parallel_id() == other.has_parallel_id()) ? 
      has_parallel_id() < other.has_parallel_id() : false
    || !(parallel_id() == other.parallel_id()) ? 
      parallel_id() < other.parallel_id() : false
    || !(has_parallel_num() == other.has_parallel_num()) ? 
      has_parallel_num() < other.has_parallel_num() : false
    || !(parallel_num() == other.parallel_num()) ? 
      parallel_num() < other.parallel_num() : false
  ;
}

using _ParallelContext_ =  ConstParallelContext::_ParallelContext_;
ConstParallelContext::ConstParallelContext(const ::std::shared_ptr<_ParallelContext_>& data): data_(data) {}
ConstParallelContext::ConstParallelContext(): data_(::std::make_shared<_ParallelContext_>()) {}
ConstParallelContext::ConstParallelContext(const ::oneflow::ParallelContext& proto_parallelcontext) {
  BuildFromProto(proto_parallelcontext);
}
ConstParallelContext::ConstParallelContext(const ConstParallelContext&) = default;
ConstParallelContext::ConstParallelContext(ConstParallelContext&&) noexcept = default;
ConstParallelContext::~ConstParallelContext() = default;

void ConstParallelContext::ToProto(PbMessage* proto_parallelcontext) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelContext*>(proto_parallelcontext));
}
  
::std::string ConstParallelContext::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelContext::__Empty__() const {
  return !data_;
}

int ConstParallelContext::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"parallel_id", 1},
    {"parallel_num", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelContext::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelContext::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelContext::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &parallel_id();
    case 2: return &parallel_num();
    default: return nullptr;
  }
}

// required or optional field parallel_id
bool ConstParallelContext::has_parallel_id() const {
  return __SharedPtrOrDefault__()->has_parallel_id();
}
const int64_t& ConstParallelContext::parallel_id() const {
  return __SharedPtrOrDefault__()->parallel_id();
}
// used by pybind11 only
// required or optional field parallel_num
bool ConstParallelContext::has_parallel_num() const {
  return __SharedPtrOrDefault__()->has_parallel_num();
}
const int64_t& ConstParallelContext::parallel_num() const {
  return __SharedPtrOrDefault__()->parallel_num();
}
// used by pybind11 only

::std::shared_ptr<ConstParallelContext> ConstParallelContext::__SharedConst__() const {
  return ::std::make_shared<ConstParallelContext>(data_);
}
int64_t ConstParallelContext::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelContext::operator==(const ConstParallelContext& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelContext::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelContext::operator<(const ConstParallelContext& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelContext_>& ConstParallelContext::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelContext_> default_ptr = std::make_shared<_ParallelContext_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelContext_>& ConstParallelContext::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelContext_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelContext
void ConstParallelContext::BuildFromProto(const PbMessage& proto_parallelcontext) {
  data_ = ::std::make_shared<_ParallelContext_>(dynamic_cast<const ::oneflow::ParallelContext&>(proto_parallelcontext));
}

ParallelContext::ParallelContext(const ::std::shared_ptr<_ParallelContext_>& data)
  : ConstParallelContext(data) {}
ParallelContext::ParallelContext(const ParallelContext& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelContext> resize
ParallelContext::ParallelContext(ParallelContext&&) noexcept = default; 
ParallelContext::ParallelContext(const ::oneflow::ParallelContext& proto_parallelcontext) {
  InitFromProto(proto_parallelcontext);
}
ParallelContext::ParallelContext() = default;

ParallelContext::~ParallelContext() = default;

void ParallelContext::InitFromProto(const PbMessage& proto_parallelcontext) {
  BuildFromProto(proto_parallelcontext);
}
  
void* ParallelContext::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_parallel_id();
    case 2: return mutable_parallel_num();
    default: return nullptr;
  }
}

bool ParallelContext::operator==(const ParallelContext& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelContext::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelContext::operator<(const ParallelContext& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelContext::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelContext::CopyFrom(const ParallelContext& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelContext& ParallelContext::operator=(const ParallelContext& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field parallel_id
void ParallelContext::clear_parallel_id() {
  return __SharedPtr__()->clear_parallel_id();
}
void ParallelContext::set_parallel_id(const int64_t& value) {
  return __SharedPtr__()->set_parallel_id(value);
}
int64_t* ParallelContext::mutable_parallel_id() {
  return  __SharedPtr__()->mutable_parallel_id();
}
// required or optional field parallel_num
void ParallelContext::clear_parallel_num() {
  return __SharedPtr__()->clear_parallel_num();
}
void ParallelContext::set_parallel_num(const int64_t& value) {
  return __SharedPtr__()->set_parallel_num(value);
}
int64_t* ParallelContext::mutable_parallel_num() {
  return  __SharedPtr__()->mutable_parallel_num();
}

::std::shared_ptr<ParallelContext> ParallelContext::__SharedMutable__() {
  return ::std::make_shared<ParallelContext>(__SharedPtr__());
}
ConstParallelConf::_ParallelConf_::_ParallelConf_() { Clear(); }
ConstParallelConf::_ParallelConf_::_ParallelConf_(const _ParallelConf_& other) { CopyFrom(other); }
ConstParallelConf::_ParallelConf_::_ParallelConf_(const ::oneflow::ParallelConf& proto_parallelconf) {
  InitFromProto(proto_parallelconf);
}
ConstParallelConf::_ParallelConf_::_ParallelConf_(_ParallelConf_&& other) = default;
ConstParallelConf::_ParallelConf_::~_ParallelConf_() = default;

void ConstParallelConf::_ParallelConf_::InitFromProto(const ::oneflow::ParallelConf& proto_parallelconf) {
  Clear();
  // repeated field: device_name
  if (!proto_parallelconf.device_name().empty()) {
    for (const ::std::string& elem : proto_parallelconf.device_name()) {
      add_device_name(elem);
    }
  }
  // required_or_optional field: device_tag
  if (proto_parallelconf.has_device_tag()) {
    set_device_tag(proto_parallelconf.device_tag());
  }
  // required_or_optional field: hierarchy
  if (proto_parallelconf.has_hierarchy()) {
  *mutable_hierarchy() = ::oneflow::cfg::ShapeProto(proto_parallelconf.hierarchy());      
  }
    
}

void ConstParallelConf::_ParallelConf_::ToProto(::oneflow::ParallelConf* proto_parallelconf) const {
  proto_parallelconf->Clear();
  // repeated field: device_name
  if (!device_name().empty()) {
    for (const ::std::string& elem : device_name()) {
      proto_parallelconf->add_device_name(elem);
    }
  }
  // required_or_optional field: device_tag
  if (this->has_device_tag()) {
    proto_parallelconf->set_device_tag(device_tag());
    }
  // required_or_optional field: hierarchy
  if (this->has_hierarchy()) {
    ::oneflow::ShapeProto proto_hierarchy;
    hierarchy().ToProto(&proto_hierarchy);
    proto_parallelconf->mutable_hierarchy()->CopyFrom(proto_hierarchy);
    }

}

::std::string ConstParallelConf::_ParallelConf_::DebugString() const {
  ::oneflow::ParallelConf proto_parallelconf;
  this->ToProto(&proto_parallelconf);
  return proto_parallelconf.DebugString();
}

void ConstParallelConf::_ParallelConf_::Clear() {
  clear_device_name();
  clear_device_tag();
  clear_hierarchy();
}

void ConstParallelConf::_ParallelConf_::CopyFrom(const _ParallelConf_& other) {
  mutable_device_name()->CopyFrom(other.device_name());
  if (other.has_device_tag()) {
    set_device_tag(other.device_tag());
  } else {
    clear_device_tag();
  }
  if (other.has_hierarchy()) {
    mutable_hierarchy()->CopyFrom(other.hierarchy());
  } else {
    clear_hierarchy();
  }
}


// repeated field device_name
::std::size_t ConstParallelConf::_ParallelConf_::device_name_size() const {
  if (!device_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return device_name_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& ConstParallelConf::_ParallelConf_::device_name() const {
  if (!device_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(device_name_.get());
}
const ::std::string& ConstParallelConf::_ParallelConf_::device_name(::std::size_t index) const {
  if (!device_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return device_name_->Get(index);
}
void ConstParallelConf::_ParallelConf_::clear_device_name() {
  if (!device_name_) {
    device_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return device_name_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* ConstParallelConf::_ParallelConf_::mutable_device_name() {
  if (!device_name_) {
    device_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return  device_name_.get();
}
::std::string* ConstParallelConf::_ParallelConf_::mutable_device_name(::std::size_t index) {
  if (!device_name_) {
    device_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return  device_name_->Mutable(index);
}
void ConstParallelConf::_ParallelConf_::add_device_name(const ::std::string& value) {
  if (!device_name_) {
    device_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return device_name_->Add(value);
}
void ConstParallelConf::_ParallelConf_::set_device_name(::std::size_t index, const ::std::string& value) {
  if (!device_name_) {
    device_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return device_name_->Set(index, value);
}

// optional field device_tag
bool ConstParallelConf::_ParallelConf_::has_device_tag() const {
  return has_device_tag_;
}
const ::std::string& ConstParallelConf::_ParallelConf_::device_tag() const {
  if (has_device_tag_) { return device_tag_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstParallelConf::_ParallelConf_::clear_device_tag() {
  has_device_tag_ = false;
}
void ConstParallelConf::_ParallelConf_::set_device_tag(const ::std::string& value) {
  device_tag_ = value;
  has_device_tag_ = true;
}
::std::string* ConstParallelConf::_ParallelConf_::mutable_device_tag() {
  has_device_tag_ = true;
  return &device_tag_;
}

// optional field hierarchy
bool ConstParallelConf::_ParallelConf_::has_hierarchy() const {
  return has_hierarchy_;
}
const ::oneflow::cfg::ShapeProto& ConstParallelConf::_ParallelConf_::hierarchy() const {
  if (!hierarchy_) {
    static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
  }
  return *(hierarchy_.get());
}
void ConstParallelConf::_ParallelConf_::clear_hierarchy() {
  if (hierarchy_) {
    hierarchy_->Clear();
  }
  has_hierarchy_ = false;
}
::oneflow::cfg::ShapeProto* ConstParallelConf::_ParallelConf_::mutable_hierarchy() {
  if (!hierarchy_) {
    hierarchy_ = ::std::make_shared<::oneflow::cfg::ShapeProto>();
  }
  has_hierarchy_ = true;
  return hierarchy_.get();
}


int ConstParallelConf::_ParallelConf_::compare(const _ParallelConf_& other) {
  if (!(device_name() == other.device_name())) {
    return device_name() < other.device_name() ? -1 : 1;
  }
  if (!(has_device_tag() == other.has_device_tag())) {
    return has_device_tag() < other.has_device_tag() ? -1 : 1;
  } else if (!(device_tag() == other.device_tag())) {
    return device_tag() < other.device_tag() ? -1 : 1;
  }
  if (!(has_hierarchy() == other.has_hierarchy())) {
    return has_hierarchy() < other.has_hierarchy() ? -1 : 1;
  } else if (!(hierarchy() == other.hierarchy())) {
    return hierarchy() < other.hierarchy() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelConf::_ParallelConf_::operator==(const _ParallelConf_& other) const {
  return true
    && device_name() == other.device_name()
    && has_device_tag() == other.has_device_tag() 
    && device_tag() == other.device_tag()
    && has_hierarchy() == other.has_hierarchy() 
    && hierarchy() == other.hierarchy()
  ;
}

std::size_t ConstParallelConf::_ParallelConf_::__CalcHash__() const {
  return 0
    ^ device_name().__CalcHash__()
    ^ (has_device_tag() ? std::hash<::std::string>()(device_tag()) : 0)
    ^ (has_hierarchy() ? std::hash<::oneflow::cfg::ShapeProto>()(hierarchy()) : 0)
  ;
}

bool ConstParallelConf::_ParallelConf_::operator<(const _ParallelConf_& other) const {
  return false
    || !(device_name() == other.device_name()) ? 
      device_name() < other.device_name() : false
    || !(has_device_tag() == other.has_device_tag()) ? 
      has_device_tag() < other.has_device_tag() : false
    || !(device_tag() == other.device_tag()) ? 
      device_tag() < other.device_tag() : false
    || !(has_hierarchy() == other.has_hierarchy()) ? 
      has_hierarchy() < other.has_hierarchy() : false
    || !(hierarchy() == other.hierarchy()) ? 
      hierarchy() < other.hierarchy() : false
  ;
}

using _ParallelConf_ =  ConstParallelConf::_ParallelConf_;
ConstParallelConf::ConstParallelConf(const ::std::shared_ptr<_ParallelConf_>& data): data_(data) {}
ConstParallelConf::ConstParallelConf(): data_(::std::make_shared<_ParallelConf_>()) {}
ConstParallelConf::ConstParallelConf(const ::oneflow::ParallelConf& proto_parallelconf) {
  BuildFromProto(proto_parallelconf);
}
ConstParallelConf::ConstParallelConf(const ConstParallelConf&) = default;
ConstParallelConf::ConstParallelConf(ConstParallelConf&&) noexcept = default;
ConstParallelConf::~ConstParallelConf() = default;

void ConstParallelConf::ToProto(PbMessage* proto_parallelconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelConf*>(proto_parallelconf));
}
  
::std::string ConstParallelConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelConf::__Empty__() const {
  return !data_;
}

int ConstParallelConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"device_name", 1},
    {"device_tag", 2},
    {"hierarchy", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &device_name();
    case 2: return &device_tag();
    case 3: return &hierarchy();
    default: return nullptr;
  }
}

// repeated field device_name
::std::size_t ConstParallelConf::device_name_size() const {
  return __SharedPtrOrDefault__()->device_name_size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& ConstParallelConf::device_name() const {
  return __SharedPtrOrDefault__()->device_name();
}
const ::std::string& ConstParallelConf::device_name(::std::size_t index) const {
  return __SharedPtrOrDefault__()->device_name(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> ConstParallelConf::shared_const_device_name() const {
  return device_name().__SharedConst__();
}
// required or optional field device_tag
bool ConstParallelConf::has_device_tag() const {
  return __SharedPtrOrDefault__()->has_device_tag();
}
const ::std::string& ConstParallelConf::device_tag() const {
  return __SharedPtrOrDefault__()->device_tag();
}
// used by pybind11 only
// required or optional field hierarchy
bool ConstParallelConf::has_hierarchy() const {
  return __SharedPtrOrDefault__()->has_hierarchy();
}
const ::oneflow::cfg::ShapeProto& ConstParallelConf::hierarchy() const {
  return __SharedPtrOrDefault__()->hierarchy();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstParallelConf::shared_const_hierarchy() const {
  return hierarchy().__SharedConst__();
}

::std::shared_ptr<ConstParallelConf> ConstParallelConf::__SharedConst__() const {
  return ::std::make_shared<ConstParallelConf>(data_);
}
int64_t ConstParallelConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelConf::operator==(const ConstParallelConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelConf::operator<(const ConstParallelConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelConf_>& ConstParallelConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelConf_> default_ptr = std::make_shared<_ParallelConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelConf_>& ConstParallelConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelConf
void ConstParallelConf::BuildFromProto(const PbMessage& proto_parallelconf) {
  data_ = ::std::make_shared<_ParallelConf_>(dynamic_cast<const ::oneflow::ParallelConf&>(proto_parallelconf));
}

ParallelConf::ParallelConf(const ::std::shared_ptr<_ParallelConf_>& data)
  : ConstParallelConf(data) {}
ParallelConf::ParallelConf(const ParallelConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelConf> resize
ParallelConf::ParallelConf(ParallelConf&&) noexcept = default; 
ParallelConf::ParallelConf(const ::oneflow::ParallelConf& proto_parallelconf) {
  InitFromProto(proto_parallelconf);
}
ParallelConf::ParallelConf() = default;

ParallelConf::~ParallelConf() = default;

void ParallelConf::InitFromProto(const PbMessage& proto_parallelconf) {
  BuildFromProto(proto_parallelconf);
}
  
void* ParallelConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_device_name();
    case 2: return mutable_device_tag();
    case 3: return mutable_hierarchy();
    default: return nullptr;
  }
}

bool ParallelConf::operator==(const ParallelConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelConf::operator<(const ParallelConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelConf::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelConf::CopyFrom(const ParallelConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelConf& ParallelConf::operator=(const ParallelConf& other) {
  CopyFrom(other);
  return *this;
}

// repeated field device_name
void ParallelConf::clear_device_name() {
  return __SharedPtr__()->clear_device_name();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* ParallelConf::mutable_device_name() {
  return __SharedPtr__()->mutable_device_name();
}
::std::string* ParallelConf::mutable_device_name(::std::size_t index) {
  return __SharedPtr__()->mutable_device_name(index);
}
void ParallelConf::add_device_name(const ::std::string& value) {
  return __SharedPtr__()->add_device_name(value);
}
void ParallelConf::set_device_name(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_device_name(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> ParallelConf::shared_mutable_device_name() {
  return mutable_device_name()->__SharedMutable__();
}
// required or optional field device_tag
void ParallelConf::clear_device_tag() {
  return __SharedPtr__()->clear_device_tag();
}
void ParallelConf::set_device_tag(const ::std::string& value) {
  return __SharedPtr__()->set_device_tag(value);
}
::std::string* ParallelConf::mutable_device_tag() {
  return  __SharedPtr__()->mutable_device_tag();
}
// required or optional field hierarchy
void ParallelConf::clear_hierarchy() {
  return __SharedPtr__()->clear_hierarchy();
}
::oneflow::cfg::ShapeProto* ParallelConf::mutable_hierarchy() {
  return __SharedPtr__()->mutable_hierarchy();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> ParallelConf::shared_mutable_hierarchy() {
  return mutable_hierarchy()->__SharedMutable__();
}

::std::shared_ptr<ParallelConf> ParallelConf::__SharedMutable__() {
  return ::std::make_shared<ParallelConf>(__SharedPtr__());
}
ConstOpNameSet::_OpNameSet_::_OpNameSet_() { Clear(); }
ConstOpNameSet::_OpNameSet_::_OpNameSet_(const _OpNameSet_& other) { CopyFrom(other); }
ConstOpNameSet::_OpNameSet_::_OpNameSet_(const ::oneflow::OpNameSet& proto_opnameset) {
  InitFromProto(proto_opnameset);
}
ConstOpNameSet::_OpNameSet_::_OpNameSet_(_OpNameSet_&& other) = default;
ConstOpNameSet::_OpNameSet_::~_OpNameSet_() = default;

void ConstOpNameSet::_OpNameSet_::InitFromProto(const ::oneflow::OpNameSet& proto_opnameset) {
  Clear();
  // repeated field: op_name
  if (!proto_opnameset.op_name().empty()) {
    for (const ::std::string& elem : proto_opnameset.op_name()) {
      add_op_name(elem);
    }
  }
    
}

void ConstOpNameSet::_OpNameSet_::ToProto(::oneflow::OpNameSet* proto_opnameset) const {
  proto_opnameset->Clear();
  // repeated field: op_name
  if (!op_name().empty()) {
    for (const ::std::string& elem : op_name()) {
      proto_opnameset->add_op_name(elem);
    }
  }

}

::std::string ConstOpNameSet::_OpNameSet_::DebugString() const {
  ::oneflow::OpNameSet proto_opnameset;
  this->ToProto(&proto_opnameset);
  return proto_opnameset.DebugString();
}

void ConstOpNameSet::_OpNameSet_::Clear() {
  clear_op_name();
}

void ConstOpNameSet::_OpNameSet_::CopyFrom(const _OpNameSet_& other) {
  mutable_op_name()->CopyFrom(other.op_name());
}


// repeated field op_name
::std::size_t ConstOpNameSet::_OpNameSet_::op_name_size() const {
  if (!op_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return op_name_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& ConstOpNameSet::_OpNameSet_::op_name() const {
  if (!op_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(op_name_.get());
}
const ::std::string& ConstOpNameSet::_OpNameSet_::op_name(::std::size_t index) const {
  if (!op_name_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return op_name_->Get(index);
}
void ConstOpNameSet::_OpNameSet_::clear_op_name() {
  if (!op_name_) {
    op_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return op_name_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* ConstOpNameSet::_OpNameSet_::mutable_op_name() {
  if (!op_name_) {
    op_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return  op_name_.get();
}
::std::string* ConstOpNameSet::_OpNameSet_::mutable_op_name(::std::size_t index) {
  if (!op_name_) {
    op_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return  op_name_->Mutable(index);
}
void ConstOpNameSet::_OpNameSet_::add_op_name(const ::std::string& value) {
  if (!op_name_) {
    op_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return op_name_->Add(value);
}
void ConstOpNameSet::_OpNameSet_::set_op_name(::std::size_t index, const ::std::string& value) {
  if (!op_name_) {
    op_name_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>();
  }
  return op_name_->Set(index, value);
}


int ConstOpNameSet::_OpNameSet_::compare(const _OpNameSet_& other) {
  if (!(op_name() == other.op_name())) {
    return op_name() < other.op_name() ? -1 : 1;
  }
  return 0;
}

bool ConstOpNameSet::_OpNameSet_::operator==(const _OpNameSet_& other) const {
  return true
    && op_name() == other.op_name()
  ;
}

std::size_t ConstOpNameSet::_OpNameSet_::__CalcHash__() const {
  return 0
    ^ op_name().__CalcHash__()
  ;
}

bool ConstOpNameSet::_OpNameSet_::operator<(const _OpNameSet_& other) const {
  return false
    || !(op_name() == other.op_name()) ? 
      op_name() < other.op_name() : false
  ;
}

using _OpNameSet_ =  ConstOpNameSet::_OpNameSet_;
ConstOpNameSet::ConstOpNameSet(const ::std::shared_ptr<_OpNameSet_>& data): data_(data) {}
ConstOpNameSet::ConstOpNameSet(): data_(::std::make_shared<_OpNameSet_>()) {}
ConstOpNameSet::ConstOpNameSet(const ::oneflow::OpNameSet& proto_opnameset) {
  BuildFromProto(proto_opnameset);
}
ConstOpNameSet::ConstOpNameSet(const ConstOpNameSet&) = default;
ConstOpNameSet::ConstOpNameSet(ConstOpNameSet&&) noexcept = default;
ConstOpNameSet::~ConstOpNameSet() = default;

void ConstOpNameSet::ToProto(PbMessage* proto_opnameset) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpNameSet*>(proto_opnameset));
}
  
::std::string ConstOpNameSet::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpNameSet::__Empty__() const {
  return !data_;
}

int ConstOpNameSet::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_name", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpNameSet::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpNameSet::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpNameSet::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_name();
    default: return nullptr;
  }
}

// repeated field op_name
::std::size_t ConstOpNameSet::op_name_size() const {
  return __SharedPtrOrDefault__()->op_name_size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& ConstOpNameSet::op_name() const {
  return __SharedPtrOrDefault__()->op_name();
}
const ::std::string& ConstOpNameSet::op_name(::std::size_t index) const {
  return __SharedPtrOrDefault__()->op_name(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> ConstOpNameSet::shared_const_op_name() const {
  return op_name().__SharedConst__();
}

::std::shared_ptr<ConstOpNameSet> ConstOpNameSet::__SharedConst__() const {
  return ::std::make_shared<ConstOpNameSet>(data_);
}
int64_t ConstOpNameSet::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpNameSet::operator==(const ConstOpNameSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpNameSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpNameSet::operator<(const ConstOpNameSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpNameSet_>& ConstOpNameSet::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpNameSet_> default_ptr = std::make_shared<_OpNameSet_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpNameSet_>& ConstOpNameSet::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpNameSet_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpNameSet
void ConstOpNameSet::BuildFromProto(const PbMessage& proto_opnameset) {
  data_ = ::std::make_shared<_OpNameSet_>(dynamic_cast<const ::oneflow::OpNameSet&>(proto_opnameset));
}

OpNameSet::OpNameSet(const ::std::shared_ptr<_OpNameSet_>& data)
  : ConstOpNameSet(data) {}
OpNameSet::OpNameSet(const OpNameSet& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpNameSet> resize
OpNameSet::OpNameSet(OpNameSet&&) noexcept = default; 
OpNameSet::OpNameSet(const ::oneflow::OpNameSet& proto_opnameset) {
  InitFromProto(proto_opnameset);
}
OpNameSet::OpNameSet() = default;

OpNameSet::~OpNameSet() = default;

void OpNameSet::InitFromProto(const PbMessage& proto_opnameset) {
  BuildFromProto(proto_opnameset);
}
  
void* OpNameSet::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_name();
    default: return nullptr;
  }
}

bool OpNameSet::operator==(const OpNameSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpNameSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpNameSet::operator<(const OpNameSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpNameSet::Clear() {
  if (data_) { data_.reset(); }
}
void OpNameSet::CopyFrom(const OpNameSet& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpNameSet& OpNameSet::operator=(const OpNameSet& other) {
  CopyFrom(other);
  return *this;
}

// repeated field op_name
void OpNameSet::clear_op_name() {
  return __SharedPtr__()->clear_op_name();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* OpNameSet::mutable_op_name() {
  return __SharedPtr__()->mutable_op_name();
}
::std::string* OpNameSet::mutable_op_name(::std::size_t index) {
  return __SharedPtr__()->mutable_op_name(index);
}
void OpNameSet::add_op_name(const ::std::string& value) {
  return __SharedPtr__()->add_op_name(value);
}
void OpNameSet::set_op_name(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_op_name(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> OpNameSet::shared_mutable_op_name() {
  return mutable_op_name()->__SharedMutable__();
}

::std::shared_ptr<OpNameSet> OpNameSet::__SharedMutable__() {
  return ::std::make_shared<OpNameSet>(__SharedPtr__());
}
ConstPlacementGroup::_PlacementGroup_::_PlacementGroup_() { Clear(); }
ConstPlacementGroup::_PlacementGroup_::_PlacementGroup_(const _PlacementGroup_& other) { CopyFrom(other); }
ConstPlacementGroup::_PlacementGroup_::_PlacementGroup_(const ::oneflow::PlacementGroup& proto_placementgroup) {
  InitFromProto(proto_placementgroup);
}
ConstPlacementGroup::_PlacementGroup_::_PlacementGroup_(_PlacementGroup_&& other) = default;
ConstPlacementGroup::_PlacementGroup_::~_PlacementGroup_() = default;

void ConstPlacementGroup::_PlacementGroup_::InitFromProto(const ::oneflow::PlacementGroup& proto_placementgroup) {
  Clear();
  // required_or_optional field: op_set
  if (proto_placementgroup.has_op_set()) {
  *mutable_op_set() = ::oneflow::cfg::OpNameSet(proto_placementgroup.op_set());      
  }
  // required_or_optional field: parallel_conf
  if (proto_placementgroup.has_parallel_conf()) {
  *mutable_parallel_conf() = ::oneflow::cfg::ParallelConf(proto_placementgroup.parallel_conf());      
  }
    
}

void ConstPlacementGroup::_PlacementGroup_::ToProto(::oneflow::PlacementGroup* proto_placementgroup) const {
  proto_placementgroup->Clear();
  // required_or_optional field: op_set
  if (this->has_op_set()) {
    ::oneflow::OpNameSet proto_op_set;
    op_set().ToProto(&proto_op_set);
    proto_placementgroup->mutable_op_set()->CopyFrom(proto_op_set);
    }
  // required_or_optional field: parallel_conf
  if (this->has_parallel_conf()) {
    ::oneflow::ParallelConf proto_parallel_conf;
    parallel_conf().ToProto(&proto_parallel_conf);
    proto_placementgroup->mutable_parallel_conf()->CopyFrom(proto_parallel_conf);
    }

}

::std::string ConstPlacementGroup::_PlacementGroup_::DebugString() const {
  ::oneflow::PlacementGroup proto_placementgroup;
  this->ToProto(&proto_placementgroup);
  return proto_placementgroup.DebugString();
}

void ConstPlacementGroup::_PlacementGroup_::Clear() {
  clear_op_set();
  clear_parallel_conf();
}

void ConstPlacementGroup::_PlacementGroup_::CopyFrom(const _PlacementGroup_& other) {
  if (other.has_op_set()) {
    mutable_op_set()->CopyFrom(other.op_set());
  } else {
    clear_op_set();
  }
  if (other.has_parallel_conf()) {
    mutable_parallel_conf()->CopyFrom(other.parallel_conf());
  } else {
    clear_parallel_conf();
  }
}


// optional field op_set
bool ConstPlacementGroup::_PlacementGroup_::has_op_set() const {
  return has_op_set_;
}
const ::oneflow::cfg::OpNameSet& ConstPlacementGroup::_PlacementGroup_::op_set() const {
  if (!op_set_) {
    static const ::std::shared_ptr<::oneflow::cfg::OpNameSet> default_static_value =
      ::std::make_shared<::oneflow::cfg::OpNameSet>();
    return *default_static_value;
  }
  return *(op_set_.get());
}
void ConstPlacementGroup::_PlacementGroup_::clear_op_set() {
  if (op_set_) {
    op_set_->Clear();
  }
  has_op_set_ = false;
}
::oneflow::cfg::OpNameSet* ConstPlacementGroup::_PlacementGroup_::mutable_op_set() {
  if (!op_set_) {
    op_set_ = ::std::make_shared<::oneflow::cfg::OpNameSet>();
  }
  has_op_set_ = true;
  return op_set_.get();
}

// optional field parallel_conf
bool ConstPlacementGroup::_PlacementGroup_::has_parallel_conf() const {
  return has_parallel_conf_;
}
const ::oneflow::cfg::ParallelConf& ConstPlacementGroup::_PlacementGroup_::parallel_conf() const {
  if (!parallel_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelConf>();
    return *default_static_value;
  }
  return *(parallel_conf_.get());
}
void ConstPlacementGroup::_PlacementGroup_::clear_parallel_conf() {
  if (parallel_conf_) {
    parallel_conf_->Clear();
  }
  has_parallel_conf_ = false;
}
::oneflow::cfg::ParallelConf* ConstPlacementGroup::_PlacementGroup_::mutable_parallel_conf() {
  if (!parallel_conf_) {
    parallel_conf_ = ::std::make_shared<::oneflow::cfg::ParallelConf>();
  }
  has_parallel_conf_ = true;
  return parallel_conf_.get();
}


int ConstPlacementGroup::_PlacementGroup_::compare(const _PlacementGroup_& other) {
  if (!(has_op_set() == other.has_op_set())) {
    return has_op_set() < other.has_op_set() ? -1 : 1;
  } else if (!(op_set() == other.op_set())) {
    return op_set() < other.op_set() ? -1 : 1;
  }
  if (!(has_parallel_conf() == other.has_parallel_conf())) {
    return has_parallel_conf() < other.has_parallel_conf() ? -1 : 1;
  } else if (!(parallel_conf() == other.parallel_conf())) {
    return parallel_conf() < other.parallel_conf() ? -1 : 1;
  }
  return 0;
}

bool ConstPlacementGroup::_PlacementGroup_::operator==(const _PlacementGroup_& other) const {
  return true
    && has_op_set() == other.has_op_set() 
    && op_set() == other.op_set()
    && has_parallel_conf() == other.has_parallel_conf() 
    && parallel_conf() == other.parallel_conf()
  ;
}

std::size_t ConstPlacementGroup::_PlacementGroup_::__CalcHash__() const {
  return 0
    ^ (has_op_set() ? std::hash<::oneflow::cfg::OpNameSet>()(op_set()) : 0)
    ^ (has_parallel_conf() ? std::hash<::oneflow::cfg::ParallelConf>()(parallel_conf()) : 0)
  ;
}

bool ConstPlacementGroup::_PlacementGroup_::operator<(const _PlacementGroup_& other) const {
  return false
    || !(has_op_set() == other.has_op_set()) ? 
      has_op_set() < other.has_op_set() : false
    || !(op_set() == other.op_set()) ? 
      op_set() < other.op_set() : false
    || !(has_parallel_conf() == other.has_parallel_conf()) ? 
      has_parallel_conf() < other.has_parallel_conf() : false
    || !(parallel_conf() == other.parallel_conf()) ? 
      parallel_conf() < other.parallel_conf() : false
  ;
}

using _PlacementGroup_ =  ConstPlacementGroup::_PlacementGroup_;
ConstPlacementGroup::ConstPlacementGroup(const ::std::shared_ptr<_PlacementGroup_>& data): data_(data) {}
ConstPlacementGroup::ConstPlacementGroup(): data_(::std::make_shared<_PlacementGroup_>()) {}
ConstPlacementGroup::ConstPlacementGroup(const ::oneflow::PlacementGroup& proto_placementgroup) {
  BuildFromProto(proto_placementgroup);
}
ConstPlacementGroup::ConstPlacementGroup(const ConstPlacementGroup&) = default;
ConstPlacementGroup::ConstPlacementGroup(ConstPlacementGroup&&) noexcept = default;
ConstPlacementGroup::~ConstPlacementGroup() = default;

void ConstPlacementGroup::ToProto(PbMessage* proto_placementgroup) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PlacementGroup*>(proto_placementgroup));
}
  
::std::string ConstPlacementGroup::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPlacementGroup::__Empty__() const {
  return !data_;
}

int ConstPlacementGroup::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_set", 1},
    {"parallel_conf", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPlacementGroup::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPlacementGroup::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpNameSet),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpNameSet),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPlacementGroup::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_set();
    case 2: return &parallel_conf();
    default: return nullptr;
  }
}

// required or optional field op_set
bool ConstPlacementGroup::has_op_set() const {
  return __SharedPtrOrDefault__()->has_op_set();
}
const ::oneflow::cfg::OpNameSet& ConstPlacementGroup::op_set() const {
  return __SharedPtrOrDefault__()->op_set();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpNameSet> ConstPlacementGroup::shared_const_op_set() const {
  return op_set().__SharedConst__();
}
// required or optional field parallel_conf
bool ConstPlacementGroup::has_parallel_conf() const {
  return __SharedPtrOrDefault__()->has_parallel_conf();
}
const ::oneflow::cfg::ParallelConf& ConstPlacementGroup::parallel_conf() const {
  return __SharedPtrOrDefault__()->parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConf> ConstPlacementGroup::shared_const_parallel_conf() const {
  return parallel_conf().__SharedConst__();
}

::std::shared_ptr<ConstPlacementGroup> ConstPlacementGroup::__SharedConst__() const {
  return ::std::make_shared<ConstPlacementGroup>(data_);
}
int64_t ConstPlacementGroup::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPlacementGroup::operator==(const ConstPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPlacementGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPlacementGroup::operator<(const ConstPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PlacementGroup_>& ConstPlacementGroup::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PlacementGroup_> default_ptr = std::make_shared<_PlacementGroup_>();
  return default_ptr;
}
const ::std::shared_ptr<_PlacementGroup_>& ConstPlacementGroup::__SharedPtr__() {
  if (!data_) { data_.reset(new _PlacementGroup_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPlacementGroup
void ConstPlacementGroup::BuildFromProto(const PbMessage& proto_placementgroup) {
  data_ = ::std::make_shared<_PlacementGroup_>(dynamic_cast<const ::oneflow::PlacementGroup&>(proto_placementgroup));
}

PlacementGroup::PlacementGroup(const ::std::shared_ptr<_PlacementGroup_>& data)
  : ConstPlacementGroup(data) {}
PlacementGroup::PlacementGroup(const PlacementGroup& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PlacementGroup> resize
PlacementGroup::PlacementGroup(PlacementGroup&&) noexcept = default; 
PlacementGroup::PlacementGroup(const ::oneflow::PlacementGroup& proto_placementgroup) {
  InitFromProto(proto_placementgroup);
}
PlacementGroup::PlacementGroup() = default;

PlacementGroup::~PlacementGroup() = default;

void PlacementGroup::InitFromProto(const PbMessage& proto_placementgroup) {
  BuildFromProto(proto_placementgroup);
}
  
void* PlacementGroup::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_set();
    case 2: return mutable_parallel_conf();
    default: return nullptr;
  }
}

bool PlacementGroup::operator==(const PlacementGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PlacementGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PlacementGroup::operator<(const PlacementGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PlacementGroup::Clear() {
  if (data_) { data_.reset(); }
}
void PlacementGroup::CopyFrom(const PlacementGroup& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PlacementGroup& PlacementGroup::operator=(const PlacementGroup& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_set
void PlacementGroup::clear_op_set() {
  return __SharedPtr__()->clear_op_set();
}
::oneflow::cfg::OpNameSet* PlacementGroup::mutable_op_set() {
  return __SharedPtr__()->mutable_op_set();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpNameSet> PlacementGroup::shared_mutable_op_set() {
  return mutable_op_set()->__SharedMutable__();
}
// required or optional field parallel_conf
void PlacementGroup::clear_parallel_conf() {
  return __SharedPtr__()->clear_parallel_conf();
}
::oneflow::cfg::ParallelConf* PlacementGroup::mutable_parallel_conf() {
  return __SharedPtr__()->mutable_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConf> PlacementGroup::shared_mutable_parallel_conf() {
  return mutable_parallel_conf()->__SharedMutable__();
}

::std::shared_ptr<PlacementGroup> PlacementGroup::__SharedMutable__() {
  return ::std::make_shared<PlacementGroup>(__SharedPtr__());
}
ConstBlobPlacementGroup::_BlobPlacementGroup_::_BlobPlacementGroup_() { Clear(); }
ConstBlobPlacementGroup::_BlobPlacementGroup_::_BlobPlacementGroup_(const _BlobPlacementGroup_& other) { CopyFrom(other); }
ConstBlobPlacementGroup::_BlobPlacementGroup_::_BlobPlacementGroup_(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup) {
  InitFromProto(proto_blobplacementgroup);
}
ConstBlobPlacementGroup::_BlobPlacementGroup_::_BlobPlacementGroup_(_BlobPlacementGroup_&& other) = default;
ConstBlobPlacementGroup::_BlobPlacementGroup_::~_BlobPlacementGroup_() = default;

void ConstBlobPlacementGroup::_BlobPlacementGroup_::InitFromProto(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup) {
  Clear();
  // repeated field: lbi
  if (!proto_blobplacementgroup.lbi().empty()) {
    for (const ::oneflow::LogicalBlobId& elem : proto_blobplacementgroup.lbi() ) {
      *mutable_lbi()->Add() = ::oneflow::cfg::LogicalBlobId(elem);
    }
  }
  // required_or_optional field: parallel_conf
  if (proto_blobplacementgroup.has_parallel_conf()) {
  *mutable_parallel_conf() = ::oneflow::cfg::ParallelConf(proto_blobplacementgroup.parallel_conf());      
  }
    
}

void ConstBlobPlacementGroup::_BlobPlacementGroup_::ToProto(::oneflow::BlobPlacementGroup* proto_blobplacementgroup) const {
  proto_blobplacementgroup->Clear();
  // repeated field: lbi
  if (!lbi().empty()) {
    for (const ::oneflow::cfg::LogicalBlobId& elem : lbi() ) {
      ::oneflow::LogicalBlobId proto_lbi_elem;
      elem.ToProto(&proto_lbi_elem);
      *proto_blobplacementgroup->mutable_lbi()->Add() = proto_lbi_elem;
    }
  }
  // required_or_optional field: parallel_conf
  if (this->has_parallel_conf()) {
    ::oneflow::ParallelConf proto_parallel_conf;
    parallel_conf().ToProto(&proto_parallel_conf);
    proto_blobplacementgroup->mutable_parallel_conf()->CopyFrom(proto_parallel_conf);
    }

}

::std::string ConstBlobPlacementGroup::_BlobPlacementGroup_::DebugString() const {
  ::oneflow::BlobPlacementGroup proto_blobplacementgroup;
  this->ToProto(&proto_blobplacementgroup);
  return proto_blobplacementgroup.DebugString();
}

void ConstBlobPlacementGroup::_BlobPlacementGroup_::Clear() {
  clear_lbi();
  clear_parallel_conf();
}

void ConstBlobPlacementGroup::_BlobPlacementGroup_::CopyFrom(const _BlobPlacementGroup_& other) {
  mutable_lbi()->CopyFrom(other.lbi());
  if (other.has_parallel_conf()) {
    mutable_parallel_conf()->CopyFrom(other.parallel_conf());
  } else {
    clear_parallel_conf();
  }
}


// repeated field lbi
::std::size_t ConstBlobPlacementGroup::_BlobPlacementGroup_::lbi_size() const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
    return default_static_value->size();
  }
  return lbi_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& ConstBlobPlacementGroup::_BlobPlacementGroup_::lbi() const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
    return *(default_static_value.get());
  }
  return *(lbi_.get());
}
const ::oneflow::cfg::LogicalBlobId& ConstBlobPlacementGroup::_BlobPlacementGroup_::lbi(::std::size_t index) const {
  if (!lbi_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
    return default_static_value->Get(index);
  }
  return lbi_->Get(index);
}
void ConstBlobPlacementGroup::_BlobPlacementGroup_::clear_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return lbi_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_* ConstBlobPlacementGroup::_BlobPlacementGroup_::mutable_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return  lbi_.get();
}
::oneflow::cfg::LogicalBlobId* ConstBlobPlacementGroup::_BlobPlacementGroup_::mutable_lbi(::std::size_t index) {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return  lbi_->Mutable(index);
}
::oneflow::cfg::LogicalBlobId* ConstBlobPlacementGroup::_BlobPlacementGroup_::add_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>();
  }
  return lbi_->Add();
}

// optional field parallel_conf
bool ConstBlobPlacementGroup::_BlobPlacementGroup_::has_parallel_conf() const {
  return has_parallel_conf_;
}
const ::oneflow::cfg::ParallelConf& ConstBlobPlacementGroup::_BlobPlacementGroup_::parallel_conf() const {
  if (!parallel_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelConf>();
    return *default_static_value;
  }
  return *(parallel_conf_.get());
}
void ConstBlobPlacementGroup::_BlobPlacementGroup_::clear_parallel_conf() {
  if (parallel_conf_) {
    parallel_conf_->Clear();
  }
  has_parallel_conf_ = false;
}
::oneflow::cfg::ParallelConf* ConstBlobPlacementGroup::_BlobPlacementGroup_::mutable_parallel_conf() {
  if (!parallel_conf_) {
    parallel_conf_ = ::std::make_shared<::oneflow::cfg::ParallelConf>();
  }
  has_parallel_conf_ = true;
  return parallel_conf_.get();
}


int ConstBlobPlacementGroup::_BlobPlacementGroup_::compare(const _BlobPlacementGroup_& other) {
  if (!(lbi() == other.lbi())) {
    return lbi() < other.lbi() ? -1 : 1;
  }
  if (!(has_parallel_conf() == other.has_parallel_conf())) {
    return has_parallel_conf() < other.has_parallel_conf() ? -1 : 1;
  } else if (!(parallel_conf() == other.parallel_conf())) {
    return parallel_conf() < other.parallel_conf() ? -1 : 1;
  }
  return 0;
}

bool ConstBlobPlacementGroup::_BlobPlacementGroup_::operator==(const _BlobPlacementGroup_& other) const {
  return true
    && lbi() == other.lbi()
    && has_parallel_conf() == other.has_parallel_conf() 
    && parallel_conf() == other.parallel_conf()
  ;
}

std::size_t ConstBlobPlacementGroup::_BlobPlacementGroup_::__CalcHash__() const {
  return 0
    ^ lbi().__CalcHash__()
    ^ (has_parallel_conf() ? std::hash<::oneflow::cfg::ParallelConf>()(parallel_conf()) : 0)
  ;
}

bool ConstBlobPlacementGroup::_BlobPlacementGroup_::operator<(const _BlobPlacementGroup_& other) const {
  return false
    || !(lbi() == other.lbi()) ? 
      lbi() < other.lbi() : false
    || !(has_parallel_conf() == other.has_parallel_conf()) ? 
      has_parallel_conf() < other.has_parallel_conf() : false
    || !(parallel_conf() == other.parallel_conf()) ? 
      parallel_conf() < other.parallel_conf() : false
  ;
}

using _BlobPlacementGroup_ =  ConstBlobPlacementGroup::_BlobPlacementGroup_;
ConstBlobPlacementGroup::ConstBlobPlacementGroup(const ::std::shared_ptr<_BlobPlacementGroup_>& data): data_(data) {}
ConstBlobPlacementGroup::ConstBlobPlacementGroup(): data_(::std::make_shared<_BlobPlacementGroup_>()) {}
ConstBlobPlacementGroup::ConstBlobPlacementGroup(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup) {
  BuildFromProto(proto_blobplacementgroup);
}
ConstBlobPlacementGroup::ConstBlobPlacementGroup(const ConstBlobPlacementGroup&) = default;
ConstBlobPlacementGroup::ConstBlobPlacementGroup(ConstBlobPlacementGroup&&) noexcept = default;
ConstBlobPlacementGroup::~ConstBlobPlacementGroup() = default;

void ConstBlobPlacementGroup::ToProto(PbMessage* proto_blobplacementgroup) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobPlacementGroup*>(proto_blobplacementgroup));
}
  
::std::string ConstBlobPlacementGroup::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobPlacementGroup::__Empty__() const {
  return !data_;
}

int ConstBlobPlacementGroup::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"lbi", 1},
    {"parallel_conf", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBlobPlacementGroup::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBlobPlacementGroup::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobPlacementGroup::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &lbi();
    case 2: return &parallel_conf();
    default: return nullptr;
  }
}

// repeated field lbi
::std::size_t ConstBlobPlacementGroup::lbi_size() const {
  return __SharedPtrOrDefault__()->lbi_size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& ConstBlobPlacementGroup::lbi() const {
  return __SharedPtrOrDefault__()->lbi();
}
const ::oneflow::cfg::LogicalBlobId& ConstBlobPlacementGroup::lbi(::std::size_t index) const {
  return __SharedPtrOrDefault__()->lbi(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> ConstBlobPlacementGroup::shared_const_lbi() const {
  return lbi().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstBlobPlacementGroup::shared_const_lbi(::std::size_t index) const {
  return lbi(index).__SharedConst__();
}
// required or optional field parallel_conf
bool ConstBlobPlacementGroup::has_parallel_conf() const {
  return __SharedPtrOrDefault__()->has_parallel_conf();
}
const ::oneflow::cfg::ParallelConf& ConstBlobPlacementGroup::parallel_conf() const {
  return __SharedPtrOrDefault__()->parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConf> ConstBlobPlacementGroup::shared_const_parallel_conf() const {
  return parallel_conf().__SharedConst__();
}

::std::shared_ptr<ConstBlobPlacementGroup> ConstBlobPlacementGroup::__SharedConst__() const {
  return ::std::make_shared<ConstBlobPlacementGroup>(data_);
}
int64_t ConstBlobPlacementGroup::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobPlacementGroup::operator==(const ConstBlobPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobPlacementGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobPlacementGroup::operator<(const ConstBlobPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobPlacementGroup_>& ConstBlobPlacementGroup::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobPlacementGroup_> default_ptr = std::make_shared<_BlobPlacementGroup_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobPlacementGroup_>& ConstBlobPlacementGroup::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobPlacementGroup_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobPlacementGroup
void ConstBlobPlacementGroup::BuildFromProto(const PbMessage& proto_blobplacementgroup) {
  data_ = ::std::make_shared<_BlobPlacementGroup_>(dynamic_cast<const ::oneflow::BlobPlacementGroup&>(proto_blobplacementgroup));
}

BlobPlacementGroup::BlobPlacementGroup(const ::std::shared_ptr<_BlobPlacementGroup_>& data)
  : ConstBlobPlacementGroup(data) {}
BlobPlacementGroup::BlobPlacementGroup(const BlobPlacementGroup& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobPlacementGroup> resize
BlobPlacementGroup::BlobPlacementGroup(BlobPlacementGroup&&) noexcept = default; 
BlobPlacementGroup::BlobPlacementGroup(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup) {
  InitFromProto(proto_blobplacementgroup);
}
BlobPlacementGroup::BlobPlacementGroup() = default;

BlobPlacementGroup::~BlobPlacementGroup() = default;

void BlobPlacementGroup::InitFromProto(const PbMessage& proto_blobplacementgroup) {
  BuildFromProto(proto_blobplacementgroup);
}
  
void* BlobPlacementGroup::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_lbi();
    case 2: return mutable_parallel_conf();
    default: return nullptr;
  }
}

bool BlobPlacementGroup::operator==(const BlobPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobPlacementGroup::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobPlacementGroup::operator<(const BlobPlacementGroup& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobPlacementGroup::Clear() {
  if (data_) { data_.reset(); }
}
void BlobPlacementGroup::CopyFrom(const BlobPlacementGroup& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobPlacementGroup& BlobPlacementGroup::operator=(const BlobPlacementGroup& other) {
  CopyFrom(other);
  return *this;
}

// repeated field lbi
void BlobPlacementGroup::clear_lbi() {
  return __SharedPtr__()->clear_lbi();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_* BlobPlacementGroup::mutable_lbi() {
  return __SharedPtr__()->mutable_lbi();
}
::oneflow::cfg::LogicalBlobId* BlobPlacementGroup::mutable_lbi(::std::size_t index) {
  return __SharedPtr__()->mutable_lbi(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> BlobPlacementGroup::shared_mutable_lbi() {
  return mutable_lbi()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> BlobPlacementGroup::shared_mutable_lbi(::std::size_t index) {
  return mutable_lbi(index)->__SharedMutable__();
}
::oneflow::cfg::LogicalBlobId* BlobPlacementGroup::add_lbi() {
  return __SharedPtr__()->add_lbi();
}
// required or optional field parallel_conf
void BlobPlacementGroup::clear_parallel_conf() {
  return __SharedPtr__()->clear_parallel_conf();
}
::oneflow::cfg::ParallelConf* BlobPlacementGroup::mutable_parallel_conf() {
  return __SharedPtr__()->mutable_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConf> BlobPlacementGroup::shared_mutable_parallel_conf() {
  return mutable_parallel_conf()->__SharedMutable__();
}

::std::shared_ptr<BlobPlacementGroup> BlobPlacementGroup::__SharedMutable__() {
  return ::std::make_shared<BlobPlacementGroup>(__SharedPtr__());
}
ConstPlacement::_Placement_::_Placement_() { Clear(); }
ConstPlacement::_Placement_::_Placement_(const _Placement_& other) { CopyFrom(other); }
ConstPlacement::_Placement_::_Placement_(const ::oneflow::Placement& proto_placement) {
  InitFromProto(proto_placement);
}
ConstPlacement::_Placement_::_Placement_(_Placement_&& other) = default;
ConstPlacement::_Placement_::~_Placement_() = default;

void ConstPlacement::_Placement_::InitFromProto(const ::oneflow::Placement& proto_placement) {
  Clear();
  // repeated field: placement_group
  if (!proto_placement.placement_group().empty()) {
    for (const ::oneflow::PlacementGroup& elem : proto_placement.placement_group() ) {
      *mutable_placement_group()->Add() = ::oneflow::cfg::PlacementGroup(elem);
    }
  }
  // repeated field: blob_placement_group
  if (!proto_placement.blob_placement_group().empty()) {
    for (const ::oneflow::BlobPlacementGroup& elem : proto_placement.blob_placement_group() ) {
      *mutable_blob_placement_group()->Add() = ::oneflow::cfg::BlobPlacementGroup(elem);
    }
  }
    
}

void ConstPlacement::_Placement_::ToProto(::oneflow::Placement* proto_placement) const {
  proto_placement->Clear();
  // repeated field: placement_group
  if (!placement_group().empty()) {
    for (const ::oneflow::cfg::PlacementGroup& elem : placement_group() ) {
      ::oneflow::PlacementGroup proto_placement_group_elem;
      elem.ToProto(&proto_placement_group_elem);
      *proto_placement->mutable_placement_group()->Add() = proto_placement_group_elem;
    }
  }
  // repeated field: blob_placement_group
  if (!blob_placement_group().empty()) {
    for (const ::oneflow::cfg::BlobPlacementGroup& elem : blob_placement_group() ) {
      ::oneflow::BlobPlacementGroup proto_blob_placement_group_elem;
      elem.ToProto(&proto_blob_placement_group_elem);
      *proto_placement->mutable_blob_placement_group()->Add() = proto_blob_placement_group_elem;
    }
  }

}

::std::string ConstPlacement::_Placement_::DebugString() const {
  ::oneflow::Placement proto_placement;
  this->ToProto(&proto_placement);
  return proto_placement.DebugString();
}

void ConstPlacement::_Placement_::Clear() {
  clear_placement_group();
  clear_blob_placement_group();
}

void ConstPlacement::_Placement_::CopyFrom(const _Placement_& other) {
  mutable_placement_group()->CopyFrom(other.placement_group());
  mutable_blob_placement_group()->CopyFrom(other.blob_placement_group());
}


// repeated field placement_group
::std::size_t ConstPlacement::_Placement_::placement_group_size() const {
  if (!placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
    return default_static_value->size();
  }
  return placement_group_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& ConstPlacement::_Placement_::placement_group() const {
  if (!placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
    return *(default_static_value.get());
  }
  return *(placement_group_.get());
}
const ::oneflow::cfg::PlacementGroup& ConstPlacement::_Placement_::placement_group(::std::size_t index) const {
  if (!placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
    return default_static_value->Get(index);
  }
  return placement_group_->Get(index);
}
void ConstPlacement::_Placement_::clear_placement_group() {
  if (!placement_group_) {
    placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
  }
  return placement_group_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_* ConstPlacement::_Placement_::mutable_placement_group() {
  if (!placement_group_) {
    placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
  }
  return  placement_group_.get();
}
::oneflow::cfg::PlacementGroup* ConstPlacement::_Placement_::mutable_placement_group(::std::size_t index) {
  if (!placement_group_) {
    placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
  }
  return  placement_group_->Mutable(index);
}
::oneflow::cfg::PlacementGroup* ConstPlacement::_Placement_::add_placement_group() {
  if (!placement_group_) {
    placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>();
  }
  return placement_group_->Add();
}

// repeated field blob_placement_group
::std::size_t ConstPlacement::_Placement_::blob_placement_group_size() const {
  if (!blob_placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
    return default_static_value->size();
  }
  return blob_placement_group_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& ConstPlacement::_Placement_::blob_placement_group() const {
  if (!blob_placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
    return *(default_static_value.get());
  }
  return *(blob_placement_group_.get());
}
const ::oneflow::cfg::BlobPlacementGroup& ConstPlacement::_Placement_::blob_placement_group(::std::size_t index) const {
  if (!blob_placement_group_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
    return default_static_value->Get(index);
  }
  return blob_placement_group_->Get(index);
}
void ConstPlacement::_Placement_::clear_blob_placement_group() {
  if (!blob_placement_group_) {
    blob_placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
  }
  return blob_placement_group_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_* ConstPlacement::_Placement_::mutable_blob_placement_group() {
  if (!blob_placement_group_) {
    blob_placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
  }
  return  blob_placement_group_.get();
}
::oneflow::cfg::BlobPlacementGroup* ConstPlacement::_Placement_::mutable_blob_placement_group(::std::size_t index) {
  if (!blob_placement_group_) {
    blob_placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
  }
  return  blob_placement_group_->Mutable(index);
}
::oneflow::cfg::BlobPlacementGroup* ConstPlacement::_Placement_::add_blob_placement_group() {
  if (!blob_placement_group_) {
    blob_placement_group_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>();
  }
  return blob_placement_group_->Add();
}


int ConstPlacement::_Placement_::compare(const _Placement_& other) {
  if (!(placement_group() == other.placement_group())) {
    return placement_group() < other.placement_group() ? -1 : 1;
  }
  if (!(blob_placement_group() == other.blob_placement_group())) {
    return blob_placement_group() < other.blob_placement_group() ? -1 : 1;
  }
  return 0;
}

bool ConstPlacement::_Placement_::operator==(const _Placement_& other) const {
  return true
    && placement_group() == other.placement_group()
    && blob_placement_group() == other.blob_placement_group()
  ;
}

std::size_t ConstPlacement::_Placement_::__CalcHash__() const {
  return 0
    ^ placement_group().__CalcHash__()
    ^ blob_placement_group().__CalcHash__()
  ;
}

bool ConstPlacement::_Placement_::operator<(const _Placement_& other) const {
  return false
    || !(placement_group() == other.placement_group()) ? 
      placement_group() < other.placement_group() : false
    || !(blob_placement_group() == other.blob_placement_group()) ? 
      blob_placement_group() < other.blob_placement_group() : false
  ;
}

using _Placement_ =  ConstPlacement::_Placement_;
ConstPlacement::ConstPlacement(const ::std::shared_ptr<_Placement_>& data): data_(data) {}
ConstPlacement::ConstPlacement(): data_(::std::make_shared<_Placement_>()) {}
ConstPlacement::ConstPlacement(const ::oneflow::Placement& proto_placement) {
  BuildFromProto(proto_placement);
}
ConstPlacement::ConstPlacement(const ConstPlacement&) = default;
ConstPlacement::ConstPlacement(ConstPlacement&&) noexcept = default;
ConstPlacement::~ConstPlacement() = default;

void ConstPlacement::ToProto(PbMessage* proto_placement) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::Placement*>(proto_placement));
}
  
::std::string ConstPlacement::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPlacement::__Empty__() const {
  return !data_;
}

int ConstPlacement::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"placement_group", 1},
    {"blob_placement_group", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPlacement::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPlacement::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::PlacementGroup>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::BlobPlacementGroup>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPlacement::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &placement_group();
    case 2: return &blob_placement_group();
    default: return nullptr;
  }
}

// repeated field placement_group
::std::size_t ConstPlacement::placement_group_size() const {
  return __SharedPtrOrDefault__()->placement_group_size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& ConstPlacement::placement_group() const {
  return __SharedPtrOrDefault__()->placement_group();
}
const ::oneflow::cfg::PlacementGroup& ConstPlacement::placement_group(::std::size_t index) const {
  return __SharedPtrOrDefault__()->placement_group(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> ConstPlacement::shared_const_placement_group() const {
  return placement_group().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstPlacementGroup> ConstPlacement::shared_const_placement_group(::std::size_t index) const {
  return placement_group(index).__SharedConst__();
}
// repeated field blob_placement_group
::std::size_t ConstPlacement::blob_placement_group_size() const {
  return __SharedPtrOrDefault__()->blob_placement_group_size();
}
const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& ConstPlacement::blob_placement_group() const {
  return __SharedPtrOrDefault__()->blob_placement_group();
}
const ::oneflow::cfg::BlobPlacementGroup& ConstPlacement::blob_placement_group(::std::size_t index) const {
  return __SharedPtrOrDefault__()->blob_placement_group(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> ConstPlacement::shared_const_blob_placement_group() const {
  return blob_placement_group().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstBlobPlacementGroup> ConstPlacement::shared_const_blob_placement_group(::std::size_t index) const {
  return blob_placement_group(index).__SharedConst__();
}

::std::shared_ptr<ConstPlacement> ConstPlacement::__SharedConst__() const {
  return ::std::make_shared<ConstPlacement>(data_);
}
int64_t ConstPlacement::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPlacement::operator==(const ConstPlacement& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPlacement::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPlacement::operator<(const ConstPlacement& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_Placement_>& ConstPlacement::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_Placement_> default_ptr = std::make_shared<_Placement_>();
  return default_ptr;
}
const ::std::shared_ptr<_Placement_>& ConstPlacement::__SharedPtr__() {
  if (!data_) { data_.reset(new _Placement_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPlacement
void ConstPlacement::BuildFromProto(const PbMessage& proto_placement) {
  data_ = ::std::make_shared<_Placement_>(dynamic_cast<const ::oneflow::Placement&>(proto_placement));
}

Placement::Placement(const ::std::shared_ptr<_Placement_>& data)
  : ConstPlacement(data) {}
Placement::Placement(const Placement& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<Placement> resize
Placement::Placement(Placement&&) noexcept = default; 
Placement::Placement(const ::oneflow::Placement& proto_placement) {
  InitFromProto(proto_placement);
}
Placement::Placement() = default;

Placement::~Placement() = default;

void Placement::InitFromProto(const PbMessage& proto_placement) {
  BuildFromProto(proto_placement);
}
  
void* Placement::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_placement_group();
    case 2: return mutable_blob_placement_group();
    default: return nullptr;
  }
}

bool Placement::operator==(const Placement& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t Placement::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool Placement::operator<(const Placement& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void Placement::Clear() {
  if (data_) { data_.reset(); }
}
void Placement::CopyFrom(const Placement& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
Placement& Placement::operator=(const Placement& other) {
  CopyFrom(other);
  return *this;
}

// repeated field placement_group
void Placement::clear_placement_group() {
  return __SharedPtr__()->clear_placement_group();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_* Placement::mutable_placement_group() {
  return __SharedPtr__()->mutable_placement_group();
}
::oneflow::cfg::PlacementGroup* Placement::mutable_placement_group(::std::size_t index) {
  return __SharedPtr__()->mutable_placement_group(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> Placement::shared_mutable_placement_group() {
  return mutable_placement_group()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::PlacementGroup> Placement::shared_mutable_placement_group(::std::size_t index) {
  return mutable_placement_group(index)->__SharedMutable__();
}
::oneflow::cfg::PlacementGroup* Placement::add_placement_group() {
  return __SharedPtr__()->add_placement_group();
}
// repeated field blob_placement_group
void Placement::clear_blob_placement_group() {
  return __SharedPtr__()->clear_blob_placement_group();
}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_* Placement::mutable_blob_placement_group() {
  return __SharedPtr__()->mutable_blob_placement_group();
}
::oneflow::cfg::BlobPlacementGroup* Placement::mutable_blob_placement_group(::std::size_t index) {
  return __SharedPtr__()->mutable_blob_placement_group(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> Placement::shared_mutable_blob_placement_group() {
  return mutable_blob_placement_group()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> Placement::shared_mutable_blob_placement_group(::std::size_t index) {
  return mutable_blob_placement_group(index)->__SharedMutable__();
}
::oneflow::cfg::BlobPlacementGroup* Placement::add_blob_placement_group() {
  return __SharedPtr__()->add_blob_placement_group();
}

::std::shared_ptr<Placement> Placement::__SharedMutable__() {
  return ::std::make_shared<Placement>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data): Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_(data) {}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_() = default;
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_() = default;

void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::LogicalBlobId>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::PlacementGroup>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::PlacementGroup>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::PlacementGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstPlacementGroup> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::PlacementGroup>>& data): Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_(data) {}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_() = default;
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_() = default;

void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::PlacementGroup>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::PlacementGroup>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::PlacementGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::PlacementGroup> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::PlacementGroup> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::BlobPlacementGroup>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::BlobPlacementGroup>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::BlobPlacementGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstBlobPlacementGroup> Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::BlobPlacementGroup>>& data): Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_(data) {}
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_() = default;
_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_() = default;

void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::BlobPlacementGroup>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::BlobPlacementGroup>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::BlobPlacementGroup>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
