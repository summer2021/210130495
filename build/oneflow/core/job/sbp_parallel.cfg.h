#ifndef CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class SplitParallel;
class BroadcastParallel;
class PartialSumParallel;
class SbpParallel;
class SbpSignature_BnInOp2sbpParallelEntry;
class SbpSignature;
class ParallelDistribution;
class ParallelDistributionSignature_BnInOp2parallelDistributionEntry;
class ParallelDistributionSignature;
class SbpSignatureList;

namespace cfg {


class SplitParallel;
class ConstSplitParallel;

class BroadcastParallel;
class ConstBroadcastParallel;

class PartialSumParallel;
class ConstPartialSumParallel;

class SbpParallel;
class ConstSbpParallel;

class SbpSignature;
class ConstSbpSignature;

class ParallelDistribution;
class ConstParallelDistribution;

class ParallelDistributionSignature;
class ConstParallelDistributionSignature;

class SbpSignatureList;
class ConstSbpSignatureList;



class ConstSplitParallel : public ::oneflow::cfg::Message {
 public:

  class _SplitParallel_ {
   public:
    _SplitParallel_();
    explicit _SplitParallel_(const _SplitParallel_& other);
    explicit _SplitParallel_(_SplitParallel_&& other);
    _SplitParallel_(const ::oneflow::SplitParallel& proto_splitparallel);
    ~_SplitParallel_();

    void InitFromProto(const ::oneflow::SplitParallel& proto_splitparallel);

    void ToProto(::oneflow::SplitParallel* proto_splitparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SplitParallel_& other);
  
      // optional field axis
     public:
    bool has_axis() const;
    const int64_t& axis() const;
    void clear_axis();
    void set_axis(const int64_t& value);
    int64_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int64_t axis_;
           
   public:
    int compare(const _SplitParallel_& other);

    bool operator==(const _SplitParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SplitParallel_& other) const;
  };

  ConstSplitParallel(const ::std::shared_ptr<_SplitParallel_>& data);
  ConstSplitParallel(const ConstSplitParallel&);
  ConstSplitParallel(ConstSplitParallel&&) noexcept;
  ConstSplitParallel();
  ConstSplitParallel(const ::oneflow::SplitParallel& proto_splitparallel);
  virtual ~ConstSplitParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_splitparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field axis
 public:
  bool has_axis() const;
  const int64_t& axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSplitParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSplitParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSplitParallel& other) const;
 protected:
  const ::std::shared_ptr<_SplitParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SplitParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSplitParallel
  void BuildFromProto(const PbMessage& proto_splitparallel);
  
  ::std::shared_ptr<_SplitParallel_> data_;
};

class SplitParallel final : public ConstSplitParallel {
 public:
  SplitParallel(const ::std::shared_ptr<_SplitParallel_>& data);
  SplitParallel(const SplitParallel& other);
  // enable nothrow for ::std::vector<SplitParallel> resize 
  SplitParallel(SplitParallel&&) noexcept;
  SplitParallel();
  explicit SplitParallel(const ::oneflow::SplitParallel& proto_splitparallel);

  ~SplitParallel() override;

  void InitFromProto(const PbMessage& proto_splitparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SplitParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SplitParallel& other) const;
  void Clear();
  void CopyFrom(const SplitParallel& other);
  SplitParallel& operator=(const SplitParallel& other);

  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int64_t& value);
  int64_t* mutable_axis();

  ::std::shared_ptr<SplitParallel> __SharedMutable__();
};


class ConstBroadcastParallel : public ::oneflow::cfg::Message {
 public:

  class _BroadcastParallel_ {
   public:
    _BroadcastParallel_();
    explicit _BroadcastParallel_(const _BroadcastParallel_& other);
    explicit _BroadcastParallel_(_BroadcastParallel_&& other);
    _BroadcastParallel_(const ::oneflow::BroadcastParallel& proto_broadcastparallel);
    ~_BroadcastParallel_();

    void InitFromProto(const ::oneflow::BroadcastParallel& proto_broadcastparallel);

    void ToProto(::oneflow::BroadcastParallel* proto_broadcastparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BroadcastParallel_& other);
       
   public:
    int compare(const _BroadcastParallel_& other);

    bool operator==(const _BroadcastParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BroadcastParallel_& other) const;
  };

  ConstBroadcastParallel(const ::std::shared_ptr<_BroadcastParallel_>& data);
  ConstBroadcastParallel(const ConstBroadcastParallel&);
  ConstBroadcastParallel(ConstBroadcastParallel&&) noexcept;
  ConstBroadcastParallel();
  ConstBroadcastParallel(const ::oneflow::BroadcastParallel& proto_broadcastparallel);
  virtual ~ConstBroadcastParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_broadcastparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstBroadcastParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBroadcastParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBroadcastParallel& other) const;
 protected:
  const ::std::shared_ptr<_BroadcastParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BroadcastParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBroadcastParallel
  void BuildFromProto(const PbMessage& proto_broadcastparallel);
  
  ::std::shared_ptr<_BroadcastParallel_> data_;
};

class BroadcastParallel final : public ConstBroadcastParallel {
 public:
  BroadcastParallel(const ::std::shared_ptr<_BroadcastParallel_>& data);
  BroadcastParallel(const BroadcastParallel& other);
  // enable nothrow for ::std::vector<BroadcastParallel> resize 
  BroadcastParallel(BroadcastParallel&&) noexcept;
  BroadcastParallel();
  explicit BroadcastParallel(const ::oneflow::BroadcastParallel& proto_broadcastparallel);

  ~BroadcastParallel() override;

  void InitFromProto(const PbMessage& proto_broadcastparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BroadcastParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BroadcastParallel& other) const;
  void Clear();
  void CopyFrom(const BroadcastParallel& other);
  BroadcastParallel& operator=(const BroadcastParallel& other);


  ::std::shared_ptr<BroadcastParallel> __SharedMutable__();
};


class ConstPartialSumParallel : public ::oneflow::cfg::Message {
 public:

  class _PartialSumParallel_ {
   public:
    _PartialSumParallel_();
    explicit _PartialSumParallel_(const _PartialSumParallel_& other);
    explicit _PartialSumParallel_(_PartialSumParallel_&& other);
    _PartialSumParallel_(const ::oneflow::PartialSumParallel& proto_partialsumparallel);
    ~_PartialSumParallel_();

    void InitFromProto(const ::oneflow::PartialSumParallel& proto_partialsumparallel);

    void ToProto(::oneflow::PartialSumParallel* proto_partialsumparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PartialSumParallel_& other);
       
   public:
    int compare(const _PartialSumParallel_& other);

    bool operator==(const _PartialSumParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PartialSumParallel_& other) const;
  };

  ConstPartialSumParallel(const ::std::shared_ptr<_PartialSumParallel_>& data);
  ConstPartialSumParallel(const ConstPartialSumParallel&);
  ConstPartialSumParallel(ConstPartialSumParallel&&) noexcept;
  ConstPartialSumParallel();
  ConstPartialSumParallel(const ::oneflow::PartialSumParallel& proto_partialsumparallel);
  virtual ~ConstPartialSumParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_partialsumparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstPartialSumParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPartialSumParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPartialSumParallel& other) const;
 protected:
  const ::std::shared_ptr<_PartialSumParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PartialSumParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPartialSumParallel
  void BuildFromProto(const PbMessage& proto_partialsumparallel);
  
  ::std::shared_ptr<_PartialSumParallel_> data_;
};

class PartialSumParallel final : public ConstPartialSumParallel {
 public:
  PartialSumParallel(const ::std::shared_ptr<_PartialSumParallel_>& data);
  PartialSumParallel(const PartialSumParallel& other);
  // enable nothrow for ::std::vector<PartialSumParallel> resize 
  PartialSumParallel(PartialSumParallel&&) noexcept;
  PartialSumParallel();
  explicit PartialSumParallel(const ::oneflow::PartialSumParallel& proto_partialsumparallel);

  ~PartialSumParallel() override;

  void InitFromProto(const PbMessage& proto_partialsumparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PartialSumParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PartialSumParallel& other) const;
  void Clear();
  void CopyFrom(const PartialSumParallel& other);
  PartialSumParallel& operator=(const PartialSumParallel& other);


  ::std::shared_ptr<PartialSumParallel> __SharedMutable__();
};


class ConstSbpParallel : public ::oneflow::cfg::Message {
 public:

 // oneof enum parallel_type
 enum ParallelTypeCase : unsigned int {
  PARALLEL_TYPE_NOT_SET = 0,
    kSplitParallel = 1,
    kBroadcastParallel = 2,
    kPartialSumParallel = 3,
   };

  class _SbpParallel_ {
   public:
    _SbpParallel_();
    explicit _SbpParallel_(const _SbpParallel_& other);
    explicit _SbpParallel_(_SbpParallel_&& other);
    _SbpParallel_(const ::oneflow::SbpParallel& proto_sbpparallel);
    ~_SbpParallel_();

    void InitFromProto(const ::oneflow::SbpParallel& proto_sbpparallel);

    void ToProto(::oneflow::SbpParallel* proto_sbpparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SbpParallel_& other);
  
     // oneof field parallel_type: split_parallel
   public:
    bool has_split_parallel() const;
    void clear_split_parallel();
    const ::oneflow::cfg::SplitParallel& split_parallel() const;
      ::oneflow::cfg::SplitParallel* mutable_split_parallel();
      
     // oneof field parallel_type: broadcast_parallel
   public:
    bool has_broadcast_parallel() const;
    void clear_broadcast_parallel();
    const ::oneflow::cfg::BroadcastParallel& broadcast_parallel() const;
      ::oneflow::cfg::BroadcastParallel* mutable_broadcast_parallel();
      
     // oneof field parallel_type: partial_sum_parallel
   public:
    bool has_partial_sum_parallel() const;
    void clear_partial_sum_parallel();
    const ::oneflow::cfg::PartialSumParallel& partial_sum_parallel() const;
      ::oneflow::cfg::PartialSumParallel* mutable_partial_sum_parallel();
           
   public:
    // oneof parallel_type
    ParallelTypeCase parallel_type_case() const;
    bool has_parallel_type() const;
   protected:
    void clear_parallel_type();
    void parallel_type_copy_from(const _SbpParallel_& other);
    union ParallelTypeUnion {
      // 64-bit aligned
      uint64_t __parallel_type_for_padding_64bit__;
          char split_parallel_[sizeof(::std::shared_ptr<::oneflow::cfg::SplitParallel>)];
            char broadcast_parallel_[sizeof(::std::shared_ptr<::oneflow::cfg::BroadcastParallel>)];
            char partial_sum_parallel_[sizeof(::std::shared_ptr<::oneflow::cfg::PartialSumParallel>)];
        } parallel_type_;
    ParallelTypeCase parallel_type_case_ = PARALLEL_TYPE_NOT_SET;
     
   public:
    int compare(const _SbpParallel_& other);

    bool operator==(const _SbpParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SbpParallel_& other) const;
  };

  ConstSbpParallel(const ::std::shared_ptr<_SbpParallel_>& data);
  ConstSbpParallel(const ConstSbpParallel&);
  ConstSbpParallel(ConstSbpParallel&&) noexcept;
  ConstSbpParallel();
  ConstSbpParallel(const ::oneflow::SbpParallel& proto_sbpparallel);
  virtual ~ConstSbpParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sbpparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field parallel_type: split_parallel
 public:
  bool has_split_parallel() const;
  const ::oneflow::cfg::SplitParallel& split_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSplitParallel> shared_const_split_parallel() const;
 // oneof field parallel_type: broadcast_parallel
 public:
  bool has_broadcast_parallel() const;
  const ::oneflow::cfg::BroadcastParallel& broadcast_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBroadcastParallel> shared_const_broadcast_parallel() const;
 // oneof field parallel_type: partial_sum_parallel
 public:
  bool has_partial_sum_parallel() const;
  const ::oneflow::cfg::PartialSumParallel& partial_sum_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPartialSumParallel> shared_const_partial_sum_parallel() const;
 public:
  ParallelTypeCase parallel_type_case() const;
 protected:
  bool has_parallel_type() const;

 public:
  ::std::shared_ptr<ConstSbpParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSbpParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSbpParallel& other) const;
 protected:
  const ::std::shared_ptr<_SbpParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SbpParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSbpParallel
  void BuildFromProto(const PbMessage& proto_sbpparallel);
  
  ::std::shared_ptr<_SbpParallel_> data_;
};

class SbpParallel final : public ConstSbpParallel {
 public:
  SbpParallel(const ::std::shared_ptr<_SbpParallel_>& data);
  SbpParallel(const SbpParallel& other);
  // enable nothrow for ::std::vector<SbpParallel> resize 
  SbpParallel(SbpParallel&&) noexcept;
  SbpParallel();
  explicit SbpParallel(const ::oneflow::SbpParallel& proto_sbpparallel);

  ~SbpParallel() override;

  void InitFromProto(const PbMessage& proto_sbpparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SbpParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SbpParallel& other) const;
  void Clear();
  void CopyFrom(const SbpParallel& other);
  SbpParallel& operator=(const SbpParallel& other);

  void clear_split_parallel();
  ::oneflow::cfg::SplitParallel* mutable_split_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SplitParallel> shared_mutable_split_parallel();
  void clear_broadcast_parallel();
  ::oneflow::cfg::BroadcastParallel* mutable_broadcast_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BroadcastParallel> shared_mutable_broadcast_parallel();
  void clear_partial_sum_parallel();
  ::oneflow::cfg::PartialSumParallel* mutable_partial_sum_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PartialSumParallel> shared_mutable_partial_sum_parallel();

  ::std::shared_ptr<SbpParallel> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_; 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_;

class ConstSbpSignature : public ::oneflow::cfg::Message {
 public:

  class _SbpSignature_ {
   public:
    _SbpSignature_();
    explicit _SbpSignature_(const _SbpSignature_& other);
    explicit _SbpSignature_(_SbpSignature_&& other);
    _SbpSignature_(const ::oneflow::SbpSignature& proto_sbpsignature);
    ~_SbpSignature_();

    void InitFromProto(const ::oneflow::SbpSignature& proto_sbpsignature);

    void ToProto(::oneflow::SbpSignature* proto_sbpsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SbpSignature_& other);
  
     public:
    ::std::size_t bn_in_op2sbp_parallel_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& bn_in_op2sbp_parallel() const;

    _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ * mutable_bn_in_op2sbp_parallel();

    const ::oneflow::cfg::SbpParallel& bn_in_op2sbp_parallel(::std::string key) const;

    void clear_bn_in_op2sbp_parallel();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> bn_in_op2sbp_parallel_;
         
   public:
    int compare(const _SbpSignature_& other);

    bool operator==(const _SbpSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SbpSignature_& other) const;
  };

  ConstSbpSignature(const ::std::shared_ptr<_SbpSignature_>& data);
  ConstSbpSignature(const ConstSbpSignature&);
  ConstSbpSignature(ConstSbpSignature&&) noexcept;
  ConstSbpSignature();
  ConstSbpSignature(const ::oneflow::SbpSignature& proto_sbpsignature);
  virtual ~ConstSbpSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sbpsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2sbp_parallel
 public:
  ::std::size_t bn_in_op2sbp_parallel_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& bn_in_op2sbp_parallel() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> shared_const_bn_in_op2sbp_parallel() const;

 public:
  ::std::shared_ptr<ConstSbpSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSbpSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSbpSignature& other) const;
 protected:
  const ::std::shared_ptr<_SbpSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SbpSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSbpSignature
  void BuildFromProto(const PbMessage& proto_sbpsignature);
  
  ::std::shared_ptr<_SbpSignature_> data_;
};

class SbpSignature final : public ConstSbpSignature {
 public:
  SbpSignature(const ::std::shared_ptr<_SbpSignature_>& data);
  SbpSignature(const SbpSignature& other);
  // enable nothrow for ::std::vector<SbpSignature> resize 
  SbpSignature(SbpSignature&&) noexcept;
  SbpSignature();
  explicit SbpSignature(const ::oneflow::SbpSignature& proto_sbpsignature);

  ~SbpSignature() override;

  void InitFromProto(const PbMessage& proto_sbpsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SbpSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SbpSignature& other) const;
  void Clear();
  void CopyFrom(const SbpSignature& other);
  SbpSignature& operator=(const SbpSignature& other);

  // repeated field bn_in_op2sbp_parallel
 public:
  void clear_bn_in_op2sbp_parallel();

  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ & bn_in_op2sbp_parallel() const;

  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_* mutable_bn_in_op2sbp_parallel();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> shared_mutable_bn_in_op2sbp_parallel();

  ::std::shared_ptr<SbpSignature> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_;
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_;

class ConstParallelDistribution : public ::oneflow::cfg::Message {
 public:

  class _ParallelDistribution_ {
   public:
    _ParallelDistribution_();
    explicit _ParallelDistribution_(const _ParallelDistribution_& other);
    explicit _ParallelDistribution_(_ParallelDistribution_&& other);
    _ParallelDistribution_(const ::oneflow::ParallelDistribution& proto_paralleldistribution);
    ~_ParallelDistribution_();

    void InitFromProto(const ::oneflow::ParallelDistribution& proto_paralleldistribution);

    void ToProto(::oneflow::ParallelDistribution* proto_paralleldistribution) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelDistribution_& other);
  
      // repeated field sbp_parallel
   public:
    ::std::size_t sbp_parallel_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& sbp_parallel(::std::size_t index) const;
    void clear_sbp_parallel();
    _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_* mutable_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_sbp_parallel(::std::size_t index);
      ::oneflow::cfg::SbpParallel* add_sbp_parallel();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> sbp_parallel_;
         
   public:
    int compare(const _ParallelDistribution_& other);

    bool operator==(const _ParallelDistribution_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelDistribution_& other) const;
  };

  ConstParallelDistribution(const ::std::shared_ptr<_ParallelDistribution_>& data);
  ConstParallelDistribution(const ConstParallelDistribution&);
  ConstParallelDistribution(ConstParallelDistribution&&) noexcept;
  ConstParallelDistribution();
  ConstParallelDistribution(const ::oneflow::ParallelDistribution& proto_paralleldistribution);
  virtual ~ConstParallelDistribution() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_paralleldistribution) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field sbp_parallel
 public:
  ::std::size_t sbp_parallel_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& sbp_parallel(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> shared_const_sbp_parallel() const;
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_sbp_parallel(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstParallelDistribution> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelDistribution& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelDistribution& other) const;
 protected:
  const ::std::shared_ptr<_ParallelDistribution_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelDistribution_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelDistribution
  void BuildFromProto(const PbMessage& proto_paralleldistribution);
  
  ::std::shared_ptr<_ParallelDistribution_> data_;
};

class ParallelDistribution final : public ConstParallelDistribution {
 public:
  ParallelDistribution(const ::std::shared_ptr<_ParallelDistribution_>& data);
  ParallelDistribution(const ParallelDistribution& other);
  // enable nothrow for ::std::vector<ParallelDistribution> resize 
  ParallelDistribution(ParallelDistribution&&) noexcept;
  ParallelDistribution();
  explicit ParallelDistribution(const ::oneflow::ParallelDistribution& proto_paralleldistribution);

  ~ParallelDistribution() override;

  void InitFromProto(const PbMessage& proto_paralleldistribution) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelDistribution& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelDistribution& other) const;
  void Clear();
  void CopyFrom(const ParallelDistribution& other);
  ParallelDistribution& operator=(const ParallelDistribution& other);

  // repeated field sbp_parallel
 public:
  void clear_sbp_parallel();
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_* mutable_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_sbp_parallel(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> shared_mutable_sbp_parallel();
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_sbp_parallel(::std::size_t index);
  ::oneflow::cfg::SbpParallel* add_sbp_parallel();

  ::std::shared_ptr<ParallelDistribution> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_; 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_;

class ConstParallelDistributionSignature : public ::oneflow::cfg::Message {
 public:

  class _ParallelDistributionSignature_ {
   public:
    _ParallelDistributionSignature_();
    explicit _ParallelDistributionSignature_(const _ParallelDistributionSignature_& other);
    explicit _ParallelDistributionSignature_(_ParallelDistributionSignature_&& other);
    _ParallelDistributionSignature_(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature);
    ~_ParallelDistributionSignature_();

    void InitFromProto(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature);

    void ToProto(::oneflow::ParallelDistributionSignature* proto_paralleldistributionsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelDistributionSignature_& other);
  
     public:
    ::std::size_t bn_in_op2parallel_distribution_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& bn_in_op2parallel_distribution() const;

    _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ * mutable_bn_in_op2parallel_distribution();

    const ::oneflow::cfg::ParallelDistribution& bn_in_op2parallel_distribution(::std::string key) const;

    void clear_bn_in_op2parallel_distribution();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> bn_in_op2parallel_distribution_;
         
   public:
    int compare(const _ParallelDistributionSignature_& other);

    bool operator==(const _ParallelDistributionSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelDistributionSignature_& other) const;
  };

  ConstParallelDistributionSignature(const ::std::shared_ptr<_ParallelDistributionSignature_>& data);
  ConstParallelDistributionSignature(const ConstParallelDistributionSignature&);
  ConstParallelDistributionSignature(ConstParallelDistributionSignature&&) noexcept;
  ConstParallelDistributionSignature();
  ConstParallelDistributionSignature(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature);
  virtual ~ConstParallelDistributionSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_paralleldistributionsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2parallel_distribution
 public:
  ::std::size_t bn_in_op2parallel_distribution_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& bn_in_op2parallel_distribution() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> shared_const_bn_in_op2parallel_distribution() const;

 public:
  ::std::shared_ptr<ConstParallelDistributionSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelDistributionSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelDistributionSignature& other) const;
 protected:
  const ::std::shared_ptr<_ParallelDistributionSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelDistributionSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelDistributionSignature
  void BuildFromProto(const PbMessage& proto_paralleldistributionsignature);
  
  ::std::shared_ptr<_ParallelDistributionSignature_> data_;
};

class ParallelDistributionSignature final : public ConstParallelDistributionSignature {
 public:
  ParallelDistributionSignature(const ::std::shared_ptr<_ParallelDistributionSignature_>& data);
  ParallelDistributionSignature(const ParallelDistributionSignature& other);
  // enable nothrow for ::std::vector<ParallelDistributionSignature> resize 
  ParallelDistributionSignature(ParallelDistributionSignature&&) noexcept;
  ParallelDistributionSignature();
  explicit ParallelDistributionSignature(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature);

  ~ParallelDistributionSignature() override;

  void InitFromProto(const PbMessage& proto_paralleldistributionsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelDistributionSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelDistributionSignature& other) const;
  void Clear();
  void CopyFrom(const ParallelDistributionSignature& other);
  ParallelDistributionSignature& operator=(const ParallelDistributionSignature& other);

  // repeated field bn_in_op2parallel_distribution
 public:
  void clear_bn_in_op2parallel_distribution();

  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ & bn_in_op2parallel_distribution() const;

  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_* mutable_bn_in_op2parallel_distribution();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> shared_mutable_bn_in_op2parallel_distribution();

  ::std::shared_ptr<ParallelDistributionSignature> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_;
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_;

class ConstSbpSignatureList : public ::oneflow::cfg::Message {
 public:

  class _SbpSignatureList_ {
   public:
    _SbpSignatureList_();
    explicit _SbpSignatureList_(const _SbpSignatureList_& other);
    explicit _SbpSignatureList_(_SbpSignatureList_&& other);
    _SbpSignatureList_(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist);
    ~_SbpSignatureList_();

    void InitFromProto(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist);

    void ToProto(::oneflow::SbpSignatureList* proto_sbpsignaturelist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SbpSignatureList_& other);
  
      // repeated field sbp_signature
   public:
    ::std::size_t sbp_signature_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& sbp_signature() const;
    const ::oneflow::cfg::SbpSignature& sbp_signature(::std::size_t index) const;
    void clear_sbp_signature();
    _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_* mutable_sbp_signature();
    ::oneflow::cfg::SbpSignature* mutable_sbp_signature(::std::size_t index);
      ::oneflow::cfg::SbpSignature* add_sbp_signature();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> sbp_signature_;
         
   public:
    int compare(const _SbpSignatureList_& other);

    bool operator==(const _SbpSignatureList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SbpSignatureList_& other) const;
  };

  ConstSbpSignatureList(const ::std::shared_ptr<_SbpSignatureList_>& data);
  ConstSbpSignatureList(const ConstSbpSignatureList&);
  ConstSbpSignatureList(ConstSbpSignatureList&&) noexcept;
  ConstSbpSignatureList();
  ConstSbpSignatureList(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist);
  virtual ~ConstSbpSignatureList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sbpsignaturelist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field sbp_signature
 public:
  ::std::size_t sbp_signature_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& sbp_signature() const;
  const ::oneflow::cfg::SbpSignature& sbp_signature(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> shared_const_sbp_signature() const;
  ::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> shared_const_sbp_signature(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstSbpSignatureList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSbpSignatureList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSbpSignatureList& other) const;
 protected:
  const ::std::shared_ptr<_SbpSignatureList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SbpSignatureList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSbpSignatureList
  void BuildFromProto(const PbMessage& proto_sbpsignaturelist);
  
  ::std::shared_ptr<_SbpSignatureList_> data_;
};

class SbpSignatureList final : public ConstSbpSignatureList {
 public:
  SbpSignatureList(const ::std::shared_ptr<_SbpSignatureList_>& data);
  SbpSignatureList(const SbpSignatureList& other);
  // enable nothrow for ::std::vector<SbpSignatureList> resize 
  SbpSignatureList(SbpSignatureList&&) noexcept;
  SbpSignatureList();
  explicit SbpSignatureList(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist);

  ~SbpSignatureList() override;

  void InitFromProto(const PbMessage& proto_sbpsignaturelist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SbpSignatureList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SbpSignatureList& other) const;
  void Clear();
  void CopyFrom(const SbpSignatureList& other);
  SbpSignatureList& operator=(const SbpSignatureList& other);

  // repeated field sbp_signature
 public:
  void clear_sbp_signature();
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_* mutable_sbp_signature();
  ::oneflow::cfg::SbpSignature* mutable_sbp_signature(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> shared_mutable_sbp_signature();
  ::std::shared_ptr<::oneflow::cfg::SbpSignature> shared_mutable_sbp_signature(::std::size_t index);
  ::oneflow::cfg::SbpSignature* add_sbp_signature();

  ::std::shared_ptr<SbpSignatureList> __SharedMutable__();
};
















// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpParallel> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpParallel>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::SbpParallel& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstSbpParallel> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ConstSbpParallel>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ final : public Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpParallel>>& data);
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_();
  ~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::SbpParallel> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ::oneflow::cfg::SbpParallel>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpParallel> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpParallel>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_ final : public Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpParallel>>& data);
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_();
  ~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelDistribution> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelDistribution>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::ParallelDistribution& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstParallelDistribution> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ConstParallelDistribution>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ final : public Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelDistribution>>& data);
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_();
  ~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ::oneflow::cfg::ParallelDistribution>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpSignature> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpSignature>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_ final : public Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpSignature>>& data);
  _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_();
  ~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::SbpSignature> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::SbpSignature> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstSplitParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstSplitParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SplitParallel> {
  std::size_t operator()(const ::oneflow::cfg::SplitParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBroadcastParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstBroadcastParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BroadcastParallel> {
  std::size_t operator()(const ::oneflow::cfg::BroadcastParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPartialSumParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstPartialSumParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PartialSumParallel> {
  std::size_t operator()(const ::oneflow::cfg::PartialSumParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSbpParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstSbpParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SbpParallel> {
  std::size_t operator()(const ::oneflow::cfg::SbpParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSbpSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstSbpSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SbpSignature> {
  std::size_t operator()(const ::oneflow::cfg::SbpSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstParallelDistribution> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelDistribution& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelDistribution> {
  std::size_t operator()(const ::oneflow::cfg::ParallelDistribution& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstParallelDistributionSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelDistributionSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelDistributionSignature> {
  std::size_t operator()(const ::oneflow::cfg::ParallelDistributionSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSbpSignatureList> {
  std::size_t operator()(const ::oneflow::cfg::ConstSbpSignatureList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SbpSignatureList> {
  std::size_t operator()(const ::oneflow::cfg::SbpSignatureList& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H_