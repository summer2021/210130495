#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/scope.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.scope", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstAttrValue> (Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::*)(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::AttrValue> (_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstScopeProto, ::oneflow::cfg::Message, std::shared_ptr<ConstScopeProto>> registry(m, "ConstScopeProto");
    registry.def("__id__", &::oneflow::cfg::ScopeProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstScopeProto::DebugString);
    registry.def("__repr__", &ConstScopeProto::DebugString);

    registry.def("has_job_desc_symbol_id", &ConstScopeProto::has_job_desc_symbol_id);
    registry.def("job_desc_symbol_id", &ConstScopeProto::job_desc_symbol_id);

    registry.def("has_device_parallel_desc_symbol_id", &ConstScopeProto::has_device_parallel_desc_symbol_id);
    registry.def("device_parallel_desc_symbol_id", &ConstScopeProto::device_parallel_desc_symbol_id);

    registry.def("has_host_parallel_desc_symbol_id", &ConstScopeProto::has_host_parallel_desc_symbol_id);
    registry.def("host_parallel_desc_symbol_id", &ConstScopeProto::host_parallel_desc_symbol_id);

    registry.def("has_enable_cpu_alternative_op", &ConstScopeProto::has_enable_cpu_alternative_op);
    registry.def("enable_cpu_alternative_op", &ConstScopeProto::enable_cpu_alternative_op);

    registry.def("has_opt_mirrored_parallel_conf", &ConstScopeProto::has_opt_mirrored_parallel_conf);
    registry.def("opt_mirrored_parallel_conf", &ConstScopeProto::shared_const_opt_mirrored_parallel_conf);

    registry.def("scope_op_name_prefixes_size", &ConstScopeProto::scope_op_name_prefixes_size);
    registry.def("scope_op_name_prefixes", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> (ConstScopeProto::*)() const)&ConstScopeProto::shared_const_scope_op_name_prefixes);
    registry.def("scope_op_name_prefixes", (const ::std::string& (ConstScopeProto::*)(::std::size_t) const)&ConstScopeProto::scope_op_name_prefixes);

    registry.def("has_parent_scope_symbol_id", &ConstScopeProto::has_parent_scope_symbol_id);
    registry.def("parent_scope_symbol_id", &ConstScopeProto::parent_scope_symbol_id);

    registry.def("has_session_id", &ConstScopeProto::has_session_id);
    registry.def("session_id", &ConstScopeProto::session_id);

    registry.def("attr_name2attr_value_size", &ConstScopeProto::attr_name2attr_value_size);
    registry.def("attr_name2attr_value", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> (ConstScopeProto::*)() const)&ConstScopeProto::shared_const_attr_name2attr_value);

    registry.def("attr_name2attr_value", (::std::shared_ptr<ConstAttrValue> (ConstScopeProto::*)(const ::std::string&) const)&ConstScopeProto::shared_const_attr_name2attr_value);

    registry.def("has_calculation_pass_name", &ConstScopeProto::has_calculation_pass_name);
    registry.def("calculation_pass_name", &ConstScopeProto::calculation_pass_name);
  }
  {
    pybind11::class_<::oneflow::cfg::ScopeProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ScopeProto>> registry(m, "ScopeProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ScopeProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ScopeProto::*)(const ConstScopeProto&))&::oneflow::cfg::ScopeProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ScopeProto::*)(const ::oneflow::cfg::ScopeProto&))&::oneflow::cfg::ScopeProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ScopeProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ScopeProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ScopeProto::DebugString);



    registry.def("has_job_desc_symbol_id", &::oneflow::cfg::ScopeProto::has_job_desc_symbol_id);
    registry.def("clear_job_desc_symbol_id", &::oneflow::cfg::ScopeProto::clear_job_desc_symbol_id);
    registry.def("job_desc_symbol_id", &::oneflow::cfg::ScopeProto::job_desc_symbol_id);
    registry.def("set_job_desc_symbol_id", &::oneflow::cfg::ScopeProto::set_job_desc_symbol_id);

    registry.def("has_device_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::has_device_parallel_desc_symbol_id);
    registry.def("clear_device_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::clear_device_parallel_desc_symbol_id);
    registry.def("device_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::device_parallel_desc_symbol_id);
    registry.def("set_device_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::set_device_parallel_desc_symbol_id);

    registry.def("has_host_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::has_host_parallel_desc_symbol_id);
    registry.def("clear_host_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::clear_host_parallel_desc_symbol_id);
    registry.def("host_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::host_parallel_desc_symbol_id);
    registry.def("set_host_parallel_desc_symbol_id", &::oneflow::cfg::ScopeProto::set_host_parallel_desc_symbol_id);

    registry.def("has_enable_cpu_alternative_op", &::oneflow::cfg::ScopeProto::has_enable_cpu_alternative_op);
    registry.def("clear_enable_cpu_alternative_op", &::oneflow::cfg::ScopeProto::clear_enable_cpu_alternative_op);
    registry.def("enable_cpu_alternative_op", &::oneflow::cfg::ScopeProto::enable_cpu_alternative_op);
    registry.def("set_enable_cpu_alternative_op", &::oneflow::cfg::ScopeProto::set_enable_cpu_alternative_op);

    registry.def("has_opt_mirrored_parallel_conf", &::oneflow::cfg::ScopeProto::has_opt_mirrored_parallel_conf);
    registry.def("clear_opt_mirrored_parallel_conf", &::oneflow::cfg::ScopeProto::clear_opt_mirrored_parallel_conf);
    registry.def("opt_mirrored_parallel_conf", &::oneflow::cfg::ScopeProto::shared_const_opt_mirrored_parallel_conf);
    registry.def("mutable_opt_mirrored_parallel_conf", &::oneflow::cfg::ScopeProto::shared_mutable_opt_mirrored_parallel_conf);

    registry.def("scope_op_name_prefixes_size", &::oneflow::cfg::ScopeProto::scope_op_name_prefixes_size);
    registry.def("clear_scope_op_name_prefixes", &::oneflow::cfg::ScopeProto::clear_scope_op_name_prefixes);
    registry.def("mutable_scope_op_name_prefixes", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ScopeProto::*)())&::oneflow::cfg::ScopeProto::shared_mutable_scope_op_name_prefixes);
    registry.def("scope_op_name_prefixes", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ScopeProto::*)() const)&::oneflow::cfg::ScopeProto::shared_const_scope_op_name_prefixes);
    registry.def("scope_op_name_prefixes", (const ::std::string& (::oneflow::cfg::ScopeProto::*)(::std::size_t) const)&::oneflow::cfg::ScopeProto::scope_op_name_prefixes);
    registry.def("add_scope_op_name_prefixes", &::oneflow::cfg::ScopeProto::add_scope_op_name_prefixes);
    registry.def("set_scope_op_name_prefixes", &::oneflow::cfg::ScopeProto::set_scope_op_name_prefixes);

    registry.def("has_parent_scope_symbol_id", &::oneflow::cfg::ScopeProto::has_parent_scope_symbol_id);
    registry.def("clear_parent_scope_symbol_id", &::oneflow::cfg::ScopeProto::clear_parent_scope_symbol_id);
    registry.def("parent_scope_symbol_id", &::oneflow::cfg::ScopeProto::parent_scope_symbol_id);
    registry.def("set_parent_scope_symbol_id", &::oneflow::cfg::ScopeProto::set_parent_scope_symbol_id);

    registry.def("has_session_id", &::oneflow::cfg::ScopeProto::has_session_id);
    registry.def("clear_session_id", &::oneflow::cfg::ScopeProto::clear_session_id);
    registry.def("session_id", &::oneflow::cfg::ScopeProto::session_id);
    registry.def("set_session_id", &::oneflow::cfg::ScopeProto::set_session_id);

    registry.def("attr_name2attr_value_size", &::oneflow::cfg::ScopeProto::attr_name2attr_value_size);
    registry.def("attr_name2attr_value", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::ScopeProto::*)() const)&::oneflow::cfg::ScopeProto::shared_const_attr_name2attr_value);
    registry.def("clear_attr_name2attr_value", &::oneflow::cfg::ScopeProto::clear_attr_name2attr_value);
    registry.def("mutable_attr_name2attr_value", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::ScopeProto::*)())&::oneflow::cfg::ScopeProto::shared_mutable_attr_name2attr_value);
    registry.def("attr_name2attr_value", (::std::shared_ptr<ConstAttrValue> (::oneflow::cfg::ScopeProto::*)(const ::std::string&) const)&::oneflow::cfg::ScopeProto::shared_const_attr_name2attr_value);

    registry.def("has_calculation_pass_name", &::oneflow::cfg::ScopeProto::has_calculation_pass_name);
    registry.def("clear_calculation_pass_name", &::oneflow::cfg::ScopeProto::clear_calculation_pass_name);
    registry.def("calculation_pass_name", &::oneflow::cfg::ScopeProto::calculation_pass_name);
    registry.def("set_calculation_pass_name", &::oneflow::cfg::ScopeProto::set_calculation_pass_name);
  }
}