#ifndef CFG_ONEFLOW_CORE_JOB_CLUSTER_INSTRUCTION_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_CLUSTER_INSTRUCTION_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace vm {
namespace cfg {
class ConstEagerInstruction;
class EagerInstruction;
}
}
}

namespace oneflow {

// forward declare proto class;
class ClusterCtrlSessionStart;
class ClusterCtrlHalt;
class ClusterCtrlAbort;
class ClusterInstructionProto;

namespace cfg {


class ClusterCtrlSessionStart;
class ConstClusterCtrlSessionStart;

class ClusterCtrlHalt;
class ConstClusterCtrlHalt;

class ClusterCtrlAbort;
class ConstClusterCtrlAbort;

class ClusterInstructionProto;
class ConstClusterInstructionProto;



class ConstClusterCtrlSessionStart : public ::oneflow::cfg::Message {
 public:

  class _ClusterCtrlSessionStart_ {
   public:
    _ClusterCtrlSessionStart_();
    explicit _ClusterCtrlSessionStart_(const _ClusterCtrlSessionStart_& other);
    explicit _ClusterCtrlSessionStart_(_ClusterCtrlSessionStart_&& other);
    _ClusterCtrlSessionStart_(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart);
    ~_ClusterCtrlSessionStart_();

    void InitFromProto(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart);

    void ToProto(::oneflow::ClusterCtrlSessionStart* proto_clusterctrlsessionstart) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClusterCtrlSessionStart_& other);
       
   public:
    int compare(const _ClusterCtrlSessionStart_& other);

    bool operator==(const _ClusterCtrlSessionStart_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClusterCtrlSessionStart_& other) const;
  };

  ConstClusterCtrlSessionStart(const ::std::shared_ptr<_ClusterCtrlSessionStart_>& data);
  ConstClusterCtrlSessionStart(const ConstClusterCtrlSessionStart&);
  ConstClusterCtrlSessionStart(ConstClusterCtrlSessionStart&&) noexcept;
  ConstClusterCtrlSessionStart();
  ConstClusterCtrlSessionStart(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart);
  virtual ~ConstClusterCtrlSessionStart() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clusterctrlsessionstart) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstClusterCtrlSessionStart> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClusterCtrlSessionStart& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClusterCtrlSessionStart& other) const;
 protected:
  const ::std::shared_ptr<_ClusterCtrlSessionStart_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClusterCtrlSessionStart_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlSessionStart
  void BuildFromProto(const PbMessage& proto_clusterctrlsessionstart);
  
  ::std::shared_ptr<_ClusterCtrlSessionStart_> data_;
};

class ClusterCtrlSessionStart final : public ConstClusterCtrlSessionStart {
 public:
  ClusterCtrlSessionStart(const ::std::shared_ptr<_ClusterCtrlSessionStart_>& data);
  ClusterCtrlSessionStart(const ClusterCtrlSessionStart& other);
  // enable nothrow for ::std::vector<ClusterCtrlSessionStart> resize 
  ClusterCtrlSessionStart(ClusterCtrlSessionStart&&) noexcept;
  ClusterCtrlSessionStart();
  explicit ClusterCtrlSessionStart(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart);

  ~ClusterCtrlSessionStart() override;

  void InitFromProto(const PbMessage& proto_clusterctrlsessionstart) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClusterCtrlSessionStart& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClusterCtrlSessionStart& other) const;
  void Clear();
  void CopyFrom(const ClusterCtrlSessionStart& other);
  ClusterCtrlSessionStart& operator=(const ClusterCtrlSessionStart& other);


  ::std::shared_ptr<ClusterCtrlSessionStart> __SharedMutable__();
};


class ConstClusterCtrlHalt : public ::oneflow::cfg::Message {
 public:

  class _ClusterCtrlHalt_ {
   public:
    _ClusterCtrlHalt_();
    explicit _ClusterCtrlHalt_(const _ClusterCtrlHalt_& other);
    explicit _ClusterCtrlHalt_(_ClusterCtrlHalt_&& other);
    _ClusterCtrlHalt_(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt);
    ~_ClusterCtrlHalt_();

    void InitFromProto(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt);

    void ToProto(::oneflow::ClusterCtrlHalt* proto_clusterctrlhalt) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClusterCtrlHalt_& other);
       
   public:
    int compare(const _ClusterCtrlHalt_& other);

    bool operator==(const _ClusterCtrlHalt_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClusterCtrlHalt_& other) const;
  };

  ConstClusterCtrlHalt(const ::std::shared_ptr<_ClusterCtrlHalt_>& data);
  ConstClusterCtrlHalt(const ConstClusterCtrlHalt&);
  ConstClusterCtrlHalt(ConstClusterCtrlHalt&&) noexcept;
  ConstClusterCtrlHalt();
  ConstClusterCtrlHalt(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt);
  virtual ~ConstClusterCtrlHalt() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clusterctrlhalt) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstClusterCtrlHalt> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClusterCtrlHalt& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClusterCtrlHalt& other) const;
 protected:
  const ::std::shared_ptr<_ClusterCtrlHalt_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClusterCtrlHalt_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlHalt
  void BuildFromProto(const PbMessage& proto_clusterctrlhalt);
  
  ::std::shared_ptr<_ClusterCtrlHalt_> data_;
};

class ClusterCtrlHalt final : public ConstClusterCtrlHalt {
 public:
  ClusterCtrlHalt(const ::std::shared_ptr<_ClusterCtrlHalt_>& data);
  ClusterCtrlHalt(const ClusterCtrlHalt& other);
  // enable nothrow for ::std::vector<ClusterCtrlHalt> resize 
  ClusterCtrlHalt(ClusterCtrlHalt&&) noexcept;
  ClusterCtrlHalt();
  explicit ClusterCtrlHalt(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt);

  ~ClusterCtrlHalt() override;

  void InitFromProto(const PbMessage& proto_clusterctrlhalt) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClusterCtrlHalt& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClusterCtrlHalt& other) const;
  void Clear();
  void CopyFrom(const ClusterCtrlHalt& other);
  ClusterCtrlHalt& operator=(const ClusterCtrlHalt& other);


  ::std::shared_ptr<ClusterCtrlHalt> __SharedMutable__();
};


class ConstClusterCtrlAbort : public ::oneflow::cfg::Message {
 public:

  class _ClusterCtrlAbort_ {
   public:
    _ClusterCtrlAbort_();
    explicit _ClusterCtrlAbort_(const _ClusterCtrlAbort_& other);
    explicit _ClusterCtrlAbort_(_ClusterCtrlAbort_&& other);
    _ClusterCtrlAbort_(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort);
    ~_ClusterCtrlAbort_();

    void InitFromProto(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort);

    void ToProto(::oneflow::ClusterCtrlAbort* proto_clusterctrlabort) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClusterCtrlAbort_& other);
       
   public:
    int compare(const _ClusterCtrlAbort_& other);

    bool operator==(const _ClusterCtrlAbort_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClusterCtrlAbort_& other) const;
  };

  ConstClusterCtrlAbort(const ::std::shared_ptr<_ClusterCtrlAbort_>& data);
  ConstClusterCtrlAbort(const ConstClusterCtrlAbort&);
  ConstClusterCtrlAbort(ConstClusterCtrlAbort&&) noexcept;
  ConstClusterCtrlAbort();
  ConstClusterCtrlAbort(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort);
  virtual ~ConstClusterCtrlAbort() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clusterctrlabort) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstClusterCtrlAbort> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClusterCtrlAbort& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClusterCtrlAbort& other) const;
 protected:
  const ::std::shared_ptr<_ClusterCtrlAbort_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClusterCtrlAbort_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlAbort
  void BuildFromProto(const PbMessage& proto_clusterctrlabort);
  
  ::std::shared_ptr<_ClusterCtrlAbort_> data_;
};

class ClusterCtrlAbort final : public ConstClusterCtrlAbort {
 public:
  ClusterCtrlAbort(const ::std::shared_ptr<_ClusterCtrlAbort_>& data);
  ClusterCtrlAbort(const ClusterCtrlAbort& other);
  // enable nothrow for ::std::vector<ClusterCtrlAbort> resize 
  ClusterCtrlAbort(ClusterCtrlAbort&&) noexcept;
  ClusterCtrlAbort();
  explicit ClusterCtrlAbort(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort);

  ~ClusterCtrlAbort() override;

  void InitFromProto(const PbMessage& proto_clusterctrlabort) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClusterCtrlAbort& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClusterCtrlAbort& other) const;
  void Clear();
  void CopyFrom(const ClusterCtrlAbort& other);
  ClusterCtrlAbort& operator=(const ClusterCtrlAbort& other);


  ::std::shared_ptr<ClusterCtrlAbort> __SharedMutable__();
};


class ConstClusterInstructionProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum instruction_type
 enum InstructionTypeCase : unsigned int {
  INSTRUCTION_TYPE_NOT_SET = 0,
    kClusterCtrlSessionStart = 1,
    kClusterCtrlHalt = 2,
    kEagerInstruction = 3,
    kClusterCtrlAbort = 5,
   };

  class _ClusterInstructionProto_ {
   public:
    _ClusterInstructionProto_();
    explicit _ClusterInstructionProto_(const _ClusterInstructionProto_& other);
    explicit _ClusterInstructionProto_(_ClusterInstructionProto_&& other);
    _ClusterInstructionProto_(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto);
    ~_ClusterInstructionProto_();

    void InitFromProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto);

    void ToProto(::oneflow::ClusterInstructionProto* proto_clusterinstructionproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClusterInstructionProto_& other);
  
     // oneof field instruction_type: cluster_ctrl_session_start
   public:
    bool has_cluster_ctrl_session_start() const;
    void clear_cluster_ctrl_session_start();
    const ::oneflow::cfg::ClusterCtrlSessionStart& cluster_ctrl_session_start() const;
      ::oneflow::cfg::ClusterCtrlSessionStart* mutable_cluster_ctrl_session_start();
      
     // oneof field instruction_type: cluster_ctrl_halt
   public:
    bool has_cluster_ctrl_halt() const;
    void clear_cluster_ctrl_halt();
    const ::oneflow::cfg::ClusterCtrlHalt& cluster_ctrl_halt() const;
      ::oneflow::cfg::ClusterCtrlHalt* mutable_cluster_ctrl_halt();
      
     // oneof field instruction_type: eager_instruction
   public:
    bool has_eager_instruction() const;
    void clear_eager_instruction();
    const ::oneflow::vm::cfg::EagerInstruction& eager_instruction() const;
      ::oneflow::vm::cfg::EagerInstruction* mutable_eager_instruction();
      
     // oneof field instruction_type: cluster_ctrl_abort
   public:
    bool has_cluster_ctrl_abort() const;
    void clear_cluster_ctrl_abort();
    const ::oneflow::cfg::ClusterCtrlAbort& cluster_ctrl_abort() const;
      ::oneflow::cfg::ClusterCtrlAbort* mutable_cluster_ctrl_abort();
           
   public:
    // oneof instruction_type
    InstructionTypeCase instruction_type_case() const;
    bool has_instruction_type() const;
   protected:
    void clear_instruction_type();
    void instruction_type_copy_from(const _ClusterInstructionProto_& other);
    union InstructionTypeUnion {
      // 64-bit aligned
      uint64_t __instruction_type_for_padding_64bit__;
          char cluster_ctrl_session_start_[sizeof(::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>)];
            char cluster_ctrl_halt_[sizeof(::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>)];
            char eager_instruction_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>)];
            char cluster_ctrl_abort_[sizeof(::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>)];
        } instruction_type_;
    InstructionTypeCase instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
     
   public:
    int compare(const _ClusterInstructionProto_& other);

    bool operator==(const _ClusterInstructionProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClusterInstructionProto_& other) const;
  };

  ConstClusterInstructionProto(const ::std::shared_ptr<_ClusterInstructionProto_>& data);
  ConstClusterInstructionProto(const ConstClusterInstructionProto&);
  ConstClusterInstructionProto(ConstClusterInstructionProto&&) noexcept;
  ConstClusterInstructionProto();
  ConstClusterInstructionProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto);
  virtual ~ConstClusterInstructionProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clusterinstructionproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field instruction_type: cluster_ctrl_session_start
 public:
  bool has_cluster_ctrl_session_start() const;
  const ::oneflow::cfg::ClusterCtrlSessionStart& cluster_ctrl_session_start() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlSessionStart> shared_const_cluster_ctrl_session_start() const;
 // oneof field instruction_type: cluster_ctrl_halt
 public:
  bool has_cluster_ctrl_halt() const;
  const ::oneflow::cfg::ClusterCtrlHalt& cluster_ctrl_halt() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlHalt> shared_const_cluster_ctrl_halt() const;
 // oneof field instruction_type: eager_instruction
 public:
  bool has_eager_instruction() const;
  const ::oneflow::vm::cfg::EagerInstruction& eager_instruction() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstEagerInstruction> shared_const_eager_instruction() const;
 // oneof field instruction_type: cluster_ctrl_abort
 public:
  bool has_cluster_ctrl_abort() const;
  const ::oneflow::cfg::ClusterCtrlAbort& cluster_ctrl_abort() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlAbort> shared_const_cluster_ctrl_abort() const;
 public:
  InstructionTypeCase instruction_type_case() const;
 protected:
  bool has_instruction_type() const;

 public:
  ::std::shared_ptr<ConstClusterInstructionProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClusterInstructionProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClusterInstructionProto& other) const;
 protected:
  const ::std::shared_ptr<_ClusterInstructionProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClusterInstructionProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClusterInstructionProto
  void BuildFromProto(const PbMessage& proto_clusterinstructionproto);
  
  ::std::shared_ptr<_ClusterInstructionProto_> data_;
};

class ClusterInstructionProto final : public ConstClusterInstructionProto {
 public:
  ClusterInstructionProto(const ::std::shared_ptr<_ClusterInstructionProto_>& data);
  ClusterInstructionProto(const ClusterInstructionProto& other);
  // enable nothrow for ::std::vector<ClusterInstructionProto> resize 
  ClusterInstructionProto(ClusterInstructionProto&&) noexcept;
  ClusterInstructionProto();
  explicit ClusterInstructionProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto);

  ~ClusterInstructionProto() override;

  void InitFromProto(const PbMessage& proto_clusterinstructionproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClusterInstructionProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClusterInstructionProto& other) const;
  void Clear();
  void CopyFrom(const ClusterInstructionProto& other);
  ClusterInstructionProto& operator=(const ClusterInstructionProto& other);

  void clear_cluster_ctrl_session_start();
  ::oneflow::cfg::ClusterCtrlSessionStart* mutable_cluster_ctrl_session_start();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart> shared_mutable_cluster_ctrl_session_start();
  void clear_cluster_ctrl_halt();
  ::oneflow::cfg::ClusterCtrlHalt* mutable_cluster_ctrl_halt();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt> shared_mutable_cluster_ctrl_halt();
  void clear_eager_instruction();
  ::oneflow::vm::cfg::EagerInstruction* mutable_eager_instruction();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction> shared_mutable_eager_instruction();
  void clear_cluster_ctrl_abort();
  ::oneflow::cfg::ClusterCtrlAbort* mutable_cluster_ctrl_abort();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort> shared_mutable_cluster_ctrl_abort();

  ::std::shared_ptr<ClusterInstructionProto> __SharedMutable__();
};















} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstClusterCtrlSessionStart> {
  std::size_t operator()(const ::oneflow::cfg::ConstClusterCtrlSessionStart& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClusterCtrlSessionStart> {
  std::size_t operator()(const ::oneflow::cfg::ClusterCtrlSessionStart& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstClusterCtrlHalt> {
  std::size_t operator()(const ::oneflow::cfg::ConstClusterCtrlHalt& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClusterCtrlHalt> {
  std::size_t operator()(const ::oneflow::cfg::ClusterCtrlHalt& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstClusterCtrlAbort> {
  std::size_t operator()(const ::oneflow::cfg::ConstClusterCtrlAbort& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClusterCtrlAbort> {
  std::size_t operator()(const ::oneflow::cfg::ClusterCtrlAbort& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstClusterInstructionProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstClusterInstructionProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClusterInstructionProto> {
  std::size_t operator()(const ::oneflow::cfg::ClusterInstructionProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_CLUSTER_INSTRUCTION_CFG_H_