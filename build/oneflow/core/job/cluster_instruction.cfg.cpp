#include "oneflow/core/job/cluster_instruction.cfg.h"
#include "oneflow/core/eager/eager_instruction.cfg.h"
#include "oneflow/core/job/cluster_instruction.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::_ClusterCtrlSessionStart_() { Clear(); }
ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::_ClusterCtrlSessionStart_(const _ClusterCtrlSessionStart_& other) { CopyFrom(other); }
ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::_ClusterCtrlSessionStart_(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart) {
  InitFromProto(proto_clusterctrlsessionstart);
}
ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::_ClusterCtrlSessionStart_(_ClusterCtrlSessionStart_&& other) = default;
ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::~_ClusterCtrlSessionStart_() = default;

void ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::InitFromProto(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart) {
  Clear();
    
}

void ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::ToProto(::oneflow::ClusterCtrlSessionStart* proto_clusterctrlsessionstart) const {
  proto_clusterctrlsessionstart->Clear();

}

::std::string ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::DebugString() const {
  ::oneflow::ClusterCtrlSessionStart proto_clusterctrlsessionstart;
  this->ToProto(&proto_clusterctrlsessionstart);
  return proto_clusterctrlsessionstart.DebugString();
}

void ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::Clear() {
}

void ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::CopyFrom(const _ClusterCtrlSessionStart_& other) {
}



int ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::compare(const _ClusterCtrlSessionStart_& other) {
  return 0;
}

bool ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::operator==(const _ClusterCtrlSessionStart_& other) const {
  return true
  ;
}

std::size_t ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::__CalcHash__() const {
  return 0
  ;
}

bool ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_::operator<(const _ClusterCtrlSessionStart_& other) const {
  return false
  ;
}

using _ClusterCtrlSessionStart_ =  ConstClusterCtrlSessionStart::_ClusterCtrlSessionStart_;
ConstClusterCtrlSessionStart::ConstClusterCtrlSessionStart(const ::std::shared_ptr<_ClusterCtrlSessionStart_>& data): data_(data) {}
ConstClusterCtrlSessionStart::ConstClusterCtrlSessionStart(): data_(::std::make_shared<_ClusterCtrlSessionStart_>()) {}
ConstClusterCtrlSessionStart::ConstClusterCtrlSessionStart(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart) {
  BuildFromProto(proto_clusterctrlsessionstart);
}
ConstClusterCtrlSessionStart::ConstClusterCtrlSessionStart(const ConstClusterCtrlSessionStart&) = default;
ConstClusterCtrlSessionStart::ConstClusterCtrlSessionStart(ConstClusterCtrlSessionStart&&) noexcept = default;
ConstClusterCtrlSessionStart::~ConstClusterCtrlSessionStart() = default;

void ConstClusterCtrlSessionStart::ToProto(PbMessage* proto_clusterctrlsessionstart) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClusterCtrlSessionStart*>(proto_clusterctrlsessionstart));
}
  
::std::string ConstClusterCtrlSessionStart::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClusterCtrlSessionStart::__Empty__() const {
  return !data_;
}

int ConstClusterCtrlSessionStart::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstClusterCtrlSessionStart::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstClusterCtrlSessionStart::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClusterCtrlSessionStart::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstClusterCtrlSessionStart> ConstClusterCtrlSessionStart::__SharedConst__() const {
  return ::std::make_shared<ConstClusterCtrlSessionStart>(data_);
}
int64_t ConstClusterCtrlSessionStart::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClusterCtrlSessionStart::operator==(const ConstClusterCtrlSessionStart& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClusterCtrlSessionStart::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClusterCtrlSessionStart::operator<(const ConstClusterCtrlSessionStart& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClusterCtrlSessionStart_>& ConstClusterCtrlSessionStart::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClusterCtrlSessionStart_> default_ptr = std::make_shared<_ClusterCtrlSessionStart_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClusterCtrlSessionStart_>& ConstClusterCtrlSessionStart::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClusterCtrlSessionStart_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlSessionStart
void ConstClusterCtrlSessionStart::BuildFromProto(const PbMessage& proto_clusterctrlsessionstart) {
  data_ = ::std::make_shared<_ClusterCtrlSessionStart_>(dynamic_cast<const ::oneflow::ClusterCtrlSessionStart&>(proto_clusterctrlsessionstart));
}

ClusterCtrlSessionStart::ClusterCtrlSessionStart(const ::std::shared_ptr<_ClusterCtrlSessionStart_>& data)
  : ConstClusterCtrlSessionStart(data) {}
ClusterCtrlSessionStart::ClusterCtrlSessionStart(const ClusterCtrlSessionStart& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClusterCtrlSessionStart> resize
ClusterCtrlSessionStart::ClusterCtrlSessionStart(ClusterCtrlSessionStart&&) noexcept = default; 
ClusterCtrlSessionStart::ClusterCtrlSessionStart(const ::oneflow::ClusterCtrlSessionStart& proto_clusterctrlsessionstart) {
  InitFromProto(proto_clusterctrlsessionstart);
}
ClusterCtrlSessionStart::ClusterCtrlSessionStart() = default;

ClusterCtrlSessionStart::~ClusterCtrlSessionStart() = default;

void ClusterCtrlSessionStart::InitFromProto(const PbMessage& proto_clusterctrlsessionstart) {
  BuildFromProto(proto_clusterctrlsessionstart);
}
  
void* ClusterCtrlSessionStart::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool ClusterCtrlSessionStart::operator==(const ClusterCtrlSessionStart& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClusterCtrlSessionStart::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClusterCtrlSessionStart::operator<(const ClusterCtrlSessionStart& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClusterCtrlSessionStart::Clear() {
  if (data_) { data_.reset(); }
}
void ClusterCtrlSessionStart::CopyFrom(const ClusterCtrlSessionStart& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClusterCtrlSessionStart& ClusterCtrlSessionStart::operator=(const ClusterCtrlSessionStart& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<ClusterCtrlSessionStart> ClusterCtrlSessionStart::__SharedMutable__() {
  return ::std::make_shared<ClusterCtrlSessionStart>(__SharedPtr__());
}
ConstClusterCtrlHalt::_ClusterCtrlHalt_::_ClusterCtrlHalt_() { Clear(); }
ConstClusterCtrlHalt::_ClusterCtrlHalt_::_ClusterCtrlHalt_(const _ClusterCtrlHalt_& other) { CopyFrom(other); }
ConstClusterCtrlHalt::_ClusterCtrlHalt_::_ClusterCtrlHalt_(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt) {
  InitFromProto(proto_clusterctrlhalt);
}
ConstClusterCtrlHalt::_ClusterCtrlHalt_::_ClusterCtrlHalt_(_ClusterCtrlHalt_&& other) = default;
ConstClusterCtrlHalt::_ClusterCtrlHalt_::~_ClusterCtrlHalt_() = default;

void ConstClusterCtrlHalt::_ClusterCtrlHalt_::InitFromProto(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt) {
  Clear();
    
}

void ConstClusterCtrlHalt::_ClusterCtrlHalt_::ToProto(::oneflow::ClusterCtrlHalt* proto_clusterctrlhalt) const {
  proto_clusterctrlhalt->Clear();

}

::std::string ConstClusterCtrlHalt::_ClusterCtrlHalt_::DebugString() const {
  ::oneflow::ClusterCtrlHalt proto_clusterctrlhalt;
  this->ToProto(&proto_clusterctrlhalt);
  return proto_clusterctrlhalt.DebugString();
}

void ConstClusterCtrlHalt::_ClusterCtrlHalt_::Clear() {
}

void ConstClusterCtrlHalt::_ClusterCtrlHalt_::CopyFrom(const _ClusterCtrlHalt_& other) {
}



int ConstClusterCtrlHalt::_ClusterCtrlHalt_::compare(const _ClusterCtrlHalt_& other) {
  return 0;
}

bool ConstClusterCtrlHalt::_ClusterCtrlHalt_::operator==(const _ClusterCtrlHalt_& other) const {
  return true
  ;
}

std::size_t ConstClusterCtrlHalt::_ClusterCtrlHalt_::__CalcHash__() const {
  return 0
  ;
}

bool ConstClusterCtrlHalt::_ClusterCtrlHalt_::operator<(const _ClusterCtrlHalt_& other) const {
  return false
  ;
}

using _ClusterCtrlHalt_ =  ConstClusterCtrlHalt::_ClusterCtrlHalt_;
ConstClusterCtrlHalt::ConstClusterCtrlHalt(const ::std::shared_ptr<_ClusterCtrlHalt_>& data): data_(data) {}
ConstClusterCtrlHalt::ConstClusterCtrlHalt(): data_(::std::make_shared<_ClusterCtrlHalt_>()) {}
ConstClusterCtrlHalt::ConstClusterCtrlHalt(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt) {
  BuildFromProto(proto_clusterctrlhalt);
}
ConstClusterCtrlHalt::ConstClusterCtrlHalt(const ConstClusterCtrlHalt&) = default;
ConstClusterCtrlHalt::ConstClusterCtrlHalt(ConstClusterCtrlHalt&&) noexcept = default;
ConstClusterCtrlHalt::~ConstClusterCtrlHalt() = default;

void ConstClusterCtrlHalt::ToProto(PbMessage* proto_clusterctrlhalt) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClusterCtrlHalt*>(proto_clusterctrlhalt));
}
  
::std::string ConstClusterCtrlHalt::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClusterCtrlHalt::__Empty__() const {
  return !data_;
}

int ConstClusterCtrlHalt::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstClusterCtrlHalt::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstClusterCtrlHalt::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClusterCtrlHalt::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstClusterCtrlHalt> ConstClusterCtrlHalt::__SharedConst__() const {
  return ::std::make_shared<ConstClusterCtrlHalt>(data_);
}
int64_t ConstClusterCtrlHalt::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClusterCtrlHalt::operator==(const ConstClusterCtrlHalt& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClusterCtrlHalt::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClusterCtrlHalt::operator<(const ConstClusterCtrlHalt& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClusterCtrlHalt_>& ConstClusterCtrlHalt::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClusterCtrlHalt_> default_ptr = std::make_shared<_ClusterCtrlHalt_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClusterCtrlHalt_>& ConstClusterCtrlHalt::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClusterCtrlHalt_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlHalt
void ConstClusterCtrlHalt::BuildFromProto(const PbMessage& proto_clusterctrlhalt) {
  data_ = ::std::make_shared<_ClusterCtrlHalt_>(dynamic_cast<const ::oneflow::ClusterCtrlHalt&>(proto_clusterctrlhalt));
}

ClusterCtrlHalt::ClusterCtrlHalt(const ::std::shared_ptr<_ClusterCtrlHalt_>& data)
  : ConstClusterCtrlHalt(data) {}
ClusterCtrlHalt::ClusterCtrlHalt(const ClusterCtrlHalt& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClusterCtrlHalt> resize
ClusterCtrlHalt::ClusterCtrlHalt(ClusterCtrlHalt&&) noexcept = default; 
ClusterCtrlHalt::ClusterCtrlHalt(const ::oneflow::ClusterCtrlHalt& proto_clusterctrlhalt) {
  InitFromProto(proto_clusterctrlhalt);
}
ClusterCtrlHalt::ClusterCtrlHalt() = default;

ClusterCtrlHalt::~ClusterCtrlHalt() = default;

void ClusterCtrlHalt::InitFromProto(const PbMessage& proto_clusterctrlhalt) {
  BuildFromProto(proto_clusterctrlhalt);
}
  
void* ClusterCtrlHalt::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool ClusterCtrlHalt::operator==(const ClusterCtrlHalt& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClusterCtrlHalt::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClusterCtrlHalt::operator<(const ClusterCtrlHalt& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClusterCtrlHalt::Clear() {
  if (data_) { data_.reset(); }
}
void ClusterCtrlHalt::CopyFrom(const ClusterCtrlHalt& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClusterCtrlHalt& ClusterCtrlHalt::operator=(const ClusterCtrlHalt& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<ClusterCtrlHalt> ClusterCtrlHalt::__SharedMutable__() {
  return ::std::make_shared<ClusterCtrlHalt>(__SharedPtr__());
}
ConstClusterCtrlAbort::_ClusterCtrlAbort_::_ClusterCtrlAbort_() { Clear(); }
ConstClusterCtrlAbort::_ClusterCtrlAbort_::_ClusterCtrlAbort_(const _ClusterCtrlAbort_& other) { CopyFrom(other); }
ConstClusterCtrlAbort::_ClusterCtrlAbort_::_ClusterCtrlAbort_(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort) {
  InitFromProto(proto_clusterctrlabort);
}
ConstClusterCtrlAbort::_ClusterCtrlAbort_::_ClusterCtrlAbort_(_ClusterCtrlAbort_&& other) = default;
ConstClusterCtrlAbort::_ClusterCtrlAbort_::~_ClusterCtrlAbort_() = default;

void ConstClusterCtrlAbort::_ClusterCtrlAbort_::InitFromProto(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort) {
  Clear();
    
}

void ConstClusterCtrlAbort::_ClusterCtrlAbort_::ToProto(::oneflow::ClusterCtrlAbort* proto_clusterctrlabort) const {
  proto_clusterctrlabort->Clear();

}

::std::string ConstClusterCtrlAbort::_ClusterCtrlAbort_::DebugString() const {
  ::oneflow::ClusterCtrlAbort proto_clusterctrlabort;
  this->ToProto(&proto_clusterctrlabort);
  return proto_clusterctrlabort.DebugString();
}

void ConstClusterCtrlAbort::_ClusterCtrlAbort_::Clear() {
}

void ConstClusterCtrlAbort::_ClusterCtrlAbort_::CopyFrom(const _ClusterCtrlAbort_& other) {
}



int ConstClusterCtrlAbort::_ClusterCtrlAbort_::compare(const _ClusterCtrlAbort_& other) {
  return 0;
}

bool ConstClusterCtrlAbort::_ClusterCtrlAbort_::operator==(const _ClusterCtrlAbort_& other) const {
  return true
  ;
}

std::size_t ConstClusterCtrlAbort::_ClusterCtrlAbort_::__CalcHash__() const {
  return 0
  ;
}

bool ConstClusterCtrlAbort::_ClusterCtrlAbort_::operator<(const _ClusterCtrlAbort_& other) const {
  return false
  ;
}

using _ClusterCtrlAbort_ =  ConstClusterCtrlAbort::_ClusterCtrlAbort_;
ConstClusterCtrlAbort::ConstClusterCtrlAbort(const ::std::shared_ptr<_ClusterCtrlAbort_>& data): data_(data) {}
ConstClusterCtrlAbort::ConstClusterCtrlAbort(): data_(::std::make_shared<_ClusterCtrlAbort_>()) {}
ConstClusterCtrlAbort::ConstClusterCtrlAbort(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort) {
  BuildFromProto(proto_clusterctrlabort);
}
ConstClusterCtrlAbort::ConstClusterCtrlAbort(const ConstClusterCtrlAbort&) = default;
ConstClusterCtrlAbort::ConstClusterCtrlAbort(ConstClusterCtrlAbort&&) noexcept = default;
ConstClusterCtrlAbort::~ConstClusterCtrlAbort() = default;

void ConstClusterCtrlAbort::ToProto(PbMessage* proto_clusterctrlabort) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClusterCtrlAbort*>(proto_clusterctrlabort));
}
  
::std::string ConstClusterCtrlAbort::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClusterCtrlAbort::__Empty__() const {
  return !data_;
}

int ConstClusterCtrlAbort::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstClusterCtrlAbort::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstClusterCtrlAbort::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClusterCtrlAbort::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstClusterCtrlAbort> ConstClusterCtrlAbort::__SharedConst__() const {
  return ::std::make_shared<ConstClusterCtrlAbort>(data_);
}
int64_t ConstClusterCtrlAbort::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClusterCtrlAbort::operator==(const ConstClusterCtrlAbort& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClusterCtrlAbort::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClusterCtrlAbort::operator<(const ConstClusterCtrlAbort& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClusterCtrlAbort_>& ConstClusterCtrlAbort::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClusterCtrlAbort_> default_ptr = std::make_shared<_ClusterCtrlAbort_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClusterCtrlAbort_>& ConstClusterCtrlAbort::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClusterCtrlAbort_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClusterCtrlAbort
void ConstClusterCtrlAbort::BuildFromProto(const PbMessage& proto_clusterctrlabort) {
  data_ = ::std::make_shared<_ClusterCtrlAbort_>(dynamic_cast<const ::oneflow::ClusterCtrlAbort&>(proto_clusterctrlabort));
}

ClusterCtrlAbort::ClusterCtrlAbort(const ::std::shared_ptr<_ClusterCtrlAbort_>& data)
  : ConstClusterCtrlAbort(data) {}
ClusterCtrlAbort::ClusterCtrlAbort(const ClusterCtrlAbort& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClusterCtrlAbort> resize
ClusterCtrlAbort::ClusterCtrlAbort(ClusterCtrlAbort&&) noexcept = default; 
ClusterCtrlAbort::ClusterCtrlAbort(const ::oneflow::ClusterCtrlAbort& proto_clusterctrlabort) {
  InitFromProto(proto_clusterctrlabort);
}
ClusterCtrlAbort::ClusterCtrlAbort() = default;

ClusterCtrlAbort::~ClusterCtrlAbort() = default;

void ClusterCtrlAbort::InitFromProto(const PbMessage& proto_clusterctrlabort) {
  BuildFromProto(proto_clusterctrlabort);
}
  
void* ClusterCtrlAbort::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool ClusterCtrlAbort::operator==(const ClusterCtrlAbort& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClusterCtrlAbort::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClusterCtrlAbort::operator<(const ClusterCtrlAbort& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClusterCtrlAbort::Clear() {
  if (data_) { data_.reset(); }
}
void ClusterCtrlAbort::CopyFrom(const ClusterCtrlAbort& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClusterCtrlAbort& ClusterCtrlAbort::operator=(const ClusterCtrlAbort& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<ClusterCtrlAbort> ClusterCtrlAbort::__SharedMutable__() {
  return ::std::make_shared<ClusterCtrlAbort>(__SharedPtr__());
}
ConstClusterInstructionProto::_ClusterInstructionProto_::_ClusterInstructionProto_() { Clear(); }
ConstClusterInstructionProto::_ClusterInstructionProto_::_ClusterInstructionProto_(const _ClusterInstructionProto_& other) { CopyFrom(other); }
ConstClusterInstructionProto::_ClusterInstructionProto_::_ClusterInstructionProto_(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto) {
  InitFromProto(proto_clusterinstructionproto);
}
ConstClusterInstructionProto::_ClusterInstructionProto_::_ClusterInstructionProto_(_ClusterInstructionProto_&& other) = default;
ConstClusterInstructionProto::_ClusterInstructionProto_::~_ClusterInstructionProto_() = default;

void ConstClusterInstructionProto::_ClusterInstructionProto_::InitFromProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto) {
  Clear();
  // oneof field: instruction_type
  InstructionTypeCase instruction_type_case = InstructionTypeCase(int(proto_clusterinstructionproto.instruction_type_case()));
  switch (instruction_type_case) {
    case kClusterCtrlSessionStart: {
      *mutable_cluster_ctrl_session_start() = ::oneflow::cfg::ClusterCtrlSessionStart(proto_clusterinstructionproto.cluster_ctrl_session_start());
      break;
  }
    case kClusterCtrlHalt: {
      *mutable_cluster_ctrl_halt() = ::oneflow::cfg::ClusterCtrlHalt(proto_clusterinstructionproto.cluster_ctrl_halt());
      break;
  }
    case kEagerInstruction: {
      *mutable_eager_instruction() = ::oneflow::vm::cfg::EagerInstruction(proto_clusterinstructionproto.eager_instruction());
      break;
  }
    case kClusterCtrlAbort: {
      *mutable_cluster_ctrl_abort() = ::oneflow::cfg::ClusterCtrlAbort(proto_clusterinstructionproto.cluster_ctrl_abort());
      break;
  }
    case INSTRUCTION_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstClusterInstructionProto::_ClusterInstructionProto_::ToProto(::oneflow::ClusterInstructionProto* proto_clusterinstructionproto) const {
  proto_clusterinstructionproto->Clear();

  // oneof field: instruction_type
  ::oneflow::ClusterInstructionProto::InstructionTypeCase instruction_type_case = ::oneflow::ClusterInstructionProto::InstructionTypeCase(int(this->instruction_type_case()));
  switch (instruction_type_case) {
    case ::oneflow::ClusterInstructionProto::kClusterCtrlSessionStart: {
      ::oneflow::ClusterCtrlSessionStart of_proto_cluster_ctrl_session_start;
      cluster_ctrl_session_start().ToProto(&of_proto_cluster_ctrl_session_start);
      proto_clusterinstructionproto->mutable_cluster_ctrl_session_start()->CopyFrom(of_proto_cluster_ctrl_session_start);
      break;
    }
    case ::oneflow::ClusterInstructionProto::kClusterCtrlHalt: {
      ::oneflow::ClusterCtrlHalt of_proto_cluster_ctrl_halt;
      cluster_ctrl_halt().ToProto(&of_proto_cluster_ctrl_halt);
      proto_clusterinstructionproto->mutable_cluster_ctrl_halt()->CopyFrom(of_proto_cluster_ctrl_halt);
      break;
    }
    case ::oneflow::ClusterInstructionProto::kEagerInstruction: {
      ::oneflow::vm::EagerInstruction of_proto_eager_instruction;
      eager_instruction().ToProto(&of_proto_eager_instruction);
      proto_clusterinstructionproto->mutable_eager_instruction()->CopyFrom(of_proto_eager_instruction);
      break;
    }
    case ::oneflow::ClusterInstructionProto::kClusterCtrlAbort: {
      ::oneflow::ClusterCtrlAbort of_proto_cluster_ctrl_abort;
      cluster_ctrl_abort().ToProto(&of_proto_cluster_ctrl_abort);
      proto_clusterinstructionproto->mutable_cluster_ctrl_abort()->CopyFrom(of_proto_cluster_ctrl_abort);
      break;
    }
    case ::oneflow::ClusterInstructionProto::INSTRUCTION_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstClusterInstructionProto::_ClusterInstructionProto_::DebugString() const {
  ::oneflow::ClusterInstructionProto proto_clusterinstructionproto;
  this->ToProto(&proto_clusterinstructionproto);
  return proto_clusterinstructionproto.DebugString();
}

void ConstClusterInstructionProto::_ClusterInstructionProto_::Clear() {
  clear_instruction_type();
}

void ConstClusterInstructionProto::_ClusterInstructionProto_::CopyFrom(const _ClusterInstructionProto_& other) {
  instruction_type_copy_from(other);
}


// oneof field instruction_type: cluster_ctrl_session_start
bool ConstClusterInstructionProto::_ClusterInstructionProto_::has_cluster_ctrl_session_start() const {
  return instruction_type_case() == kClusterCtrlSessionStart;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::clear_cluster_ctrl_session_start() {
  if (has_cluster_ctrl_session_start()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_session_start_));
      ptr->~Shared_ptr();
    }
    instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ClusterCtrlSessionStart& ConstClusterInstructionProto::_ClusterInstructionProto_::cluster_ctrl_session_start() const {
  if (has_cluster_ctrl_session_start()) {
      const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>*>(&(instruction_type_.cluster_ctrl_session_start_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart> default_static_value = ::std::make_shared<::oneflow::cfg::ClusterCtrlSessionStart>();
    return *default_static_value;
    }
}
::oneflow::cfg::ClusterCtrlSessionStart* ConstClusterInstructionProto::_ClusterInstructionProto_::mutable_cluster_ctrl_session_start() {
  if(!has_cluster_ctrl_session_start()) {
    clear_instruction_type();
    new (&(instruction_type_.cluster_ctrl_session_start_)) ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>(new ::oneflow::cfg::ClusterCtrlSessionStart());
  }
  instruction_type_case_ = kClusterCtrlSessionStart;
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>*>(&(instruction_type_.cluster_ctrl_session_start_));
  return  (*ptr).get();
}

// oneof field instruction_type: cluster_ctrl_halt
bool ConstClusterInstructionProto::_ClusterInstructionProto_::has_cluster_ctrl_halt() const {
  return instruction_type_case() == kClusterCtrlHalt;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::clear_cluster_ctrl_halt() {
  if (has_cluster_ctrl_halt()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_halt_));
      ptr->~Shared_ptr();
    }
    instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ClusterCtrlHalt& ConstClusterInstructionProto::_ClusterInstructionProto_::cluster_ctrl_halt() const {
  if (has_cluster_ctrl_halt()) {
      const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>*>(&(instruction_type_.cluster_ctrl_halt_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt> default_static_value = ::std::make_shared<::oneflow::cfg::ClusterCtrlHalt>();
    return *default_static_value;
    }
}
::oneflow::cfg::ClusterCtrlHalt* ConstClusterInstructionProto::_ClusterInstructionProto_::mutable_cluster_ctrl_halt() {
  if(!has_cluster_ctrl_halt()) {
    clear_instruction_type();
    new (&(instruction_type_.cluster_ctrl_halt_)) ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>(new ::oneflow::cfg::ClusterCtrlHalt());
  }
  instruction_type_case_ = kClusterCtrlHalt;
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>*>(&(instruction_type_.cluster_ctrl_halt_));
  return  (*ptr).get();
}

// oneof field instruction_type: eager_instruction
bool ConstClusterInstructionProto::_ClusterInstructionProto_::has_eager_instruction() const {
  return instruction_type_case() == kEagerInstruction;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::clear_eager_instruction() {
  if (has_eager_instruction()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.eager_instruction_));
      ptr->~Shared_ptr();
    }
    instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::EagerInstruction& ConstClusterInstructionProto::_ClusterInstructionProto_::eager_instruction() const {
  if (has_eager_instruction()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>*>(&(instruction_type_.eager_instruction_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction> default_static_value = ::std::make_shared<::oneflow::vm::cfg::EagerInstruction>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::EagerInstruction* ConstClusterInstructionProto::_ClusterInstructionProto_::mutable_eager_instruction() {
  if(!has_eager_instruction()) {
    clear_instruction_type();
    new (&(instruction_type_.eager_instruction_)) ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>(new ::oneflow::vm::cfg::EagerInstruction());
  }
  instruction_type_case_ = kEagerInstruction;
  ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>*>(&(instruction_type_.eager_instruction_));
  return  (*ptr).get();
}

// oneof field instruction_type: cluster_ctrl_abort
bool ConstClusterInstructionProto::_ClusterInstructionProto_::has_cluster_ctrl_abort() const {
  return instruction_type_case() == kClusterCtrlAbort;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::clear_cluster_ctrl_abort() {
  if (has_cluster_ctrl_abort()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_abort_));
      ptr->~Shared_ptr();
    }
    instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ClusterCtrlAbort& ConstClusterInstructionProto::_ClusterInstructionProto_::cluster_ctrl_abort() const {
  if (has_cluster_ctrl_abort()) {
      const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>*>(&(instruction_type_.cluster_ctrl_abort_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort> default_static_value = ::std::make_shared<::oneflow::cfg::ClusterCtrlAbort>();
    return *default_static_value;
    }
}
::oneflow::cfg::ClusterCtrlAbort* ConstClusterInstructionProto::_ClusterInstructionProto_::mutable_cluster_ctrl_abort() {
  if(!has_cluster_ctrl_abort()) {
    clear_instruction_type();
    new (&(instruction_type_.cluster_ctrl_abort_)) ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>(new ::oneflow::cfg::ClusterCtrlAbort());
  }
  instruction_type_case_ = kClusterCtrlAbort;
  ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>*>(&(instruction_type_.cluster_ctrl_abort_));
  return  (*ptr).get();
}
ConstClusterInstructionProto::InstructionTypeCase ConstClusterInstructionProto::_ClusterInstructionProto_::instruction_type_case() const {
  return instruction_type_case_;
}
bool ConstClusterInstructionProto::_ClusterInstructionProto_::has_instruction_type() const {
  return instruction_type_case_ != INSTRUCTION_TYPE_NOT_SET;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::clear_instruction_type() {
  switch (instruction_type_case()) {
    case kClusterCtrlSessionStart: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_session_start_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kClusterCtrlHalt: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_halt_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kEagerInstruction: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.eager_instruction_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kClusterCtrlAbort: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(instruction_type_.cluster_ctrl_abort_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case INSTRUCTION_TYPE_NOT_SET: {
      break;
    }
  }
  instruction_type_case_ = INSTRUCTION_TYPE_NOT_SET;
}
void ConstClusterInstructionProto::_ClusterInstructionProto_::instruction_type_copy_from(const _ClusterInstructionProto_& other) {
  switch (other.instruction_type_case()) {
    case kClusterCtrlSessionStart: {
      mutable_cluster_ctrl_session_start()->CopyFrom(other.cluster_ctrl_session_start());
      break;
    }
    case kClusterCtrlHalt: {
      mutable_cluster_ctrl_halt()->CopyFrom(other.cluster_ctrl_halt());
      break;
    }
    case kEagerInstruction: {
      mutable_eager_instruction()->CopyFrom(other.eager_instruction());
      break;
    }
    case kClusterCtrlAbort: {
      mutable_cluster_ctrl_abort()->CopyFrom(other.cluster_ctrl_abort());
      break;
    }
    case INSTRUCTION_TYPE_NOT_SET: {
      clear_instruction_type();
    }
  }
}


int ConstClusterInstructionProto::_ClusterInstructionProto_::compare(const _ClusterInstructionProto_& other) {
  if (!(instruction_type_case() == other.instruction_type_case())) {
    return instruction_type_case() < other.instruction_type_case() ? -1 : 1;
  }
  switch (instruction_type_case()) {
    case kClusterCtrlSessionStart: {
      if (!(cluster_ctrl_session_start() == other.cluster_ctrl_session_start())) {
        return cluster_ctrl_session_start() < other.cluster_ctrl_session_start() ? -1 : 1;
      }
      break;
    }
    case kClusterCtrlHalt: {
      if (!(cluster_ctrl_halt() == other.cluster_ctrl_halt())) {
        return cluster_ctrl_halt() < other.cluster_ctrl_halt() ? -1 : 1;
      }
      break;
    }
    case kEagerInstruction: {
      if (!(eager_instruction() == other.eager_instruction())) {
        return eager_instruction() < other.eager_instruction() ? -1 : 1;
      }
      break;
    }
    case kClusterCtrlAbort: {
      if (!(cluster_ctrl_abort() == other.cluster_ctrl_abort())) {
        return cluster_ctrl_abort() < other.cluster_ctrl_abort() ? -1 : 1;
      }
      break;
    }
    case INSTRUCTION_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstClusterInstructionProto::_ClusterInstructionProto_::operator==(const _ClusterInstructionProto_& other) const {
  return true
    && instruction_type_case() == other.instruction_type_case()
    && (instruction_type_case() == kClusterCtrlSessionStart ? 
      cluster_ctrl_session_start() == other.cluster_ctrl_session_start() : true)
    && (instruction_type_case() == kClusterCtrlHalt ? 
      cluster_ctrl_halt() == other.cluster_ctrl_halt() : true)
    && (instruction_type_case() == kEagerInstruction ? 
      eager_instruction() == other.eager_instruction() : true)
    && (instruction_type_case() == kClusterCtrlAbort ? 
      cluster_ctrl_abort() == other.cluster_ctrl_abort() : true)
  ;
}

std::size_t ConstClusterInstructionProto::_ClusterInstructionProto_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(instruction_type_case())
    ^ (has_cluster_ctrl_session_start() ? std::hash<::oneflow::cfg::ClusterCtrlSessionStart>()(cluster_ctrl_session_start()) : 0)
    ^ (has_cluster_ctrl_halt() ? std::hash<::oneflow::cfg::ClusterCtrlHalt>()(cluster_ctrl_halt()) : 0)
    ^ (has_eager_instruction() ? std::hash<::oneflow::vm::cfg::EagerInstruction>()(eager_instruction()) : 0)
    ^ (has_cluster_ctrl_abort() ? std::hash<::oneflow::cfg::ClusterCtrlAbort>()(cluster_ctrl_abort()) : 0)
  ;
}

bool ConstClusterInstructionProto::_ClusterInstructionProto_::operator<(const _ClusterInstructionProto_& other) const {
  return false
    || !(instruction_type_case() == other.instruction_type_case()) ? 
      instruction_type_case() < other.instruction_type_case() : false
    || ((instruction_type_case() == kClusterCtrlSessionStart) && 
      !(cluster_ctrl_session_start() == other.cluster_ctrl_session_start())) ? 
        cluster_ctrl_session_start() < other.cluster_ctrl_session_start() : false
    || ((instruction_type_case() == kClusterCtrlHalt) && 
      !(cluster_ctrl_halt() == other.cluster_ctrl_halt())) ? 
        cluster_ctrl_halt() < other.cluster_ctrl_halt() : false
    || ((instruction_type_case() == kEagerInstruction) && 
      !(eager_instruction() == other.eager_instruction())) ? 
        eager_instruction() < other.eager_instruction() : false
    || ((instruction_type_case() == kClusterCtrlAbort) && 
      !(cluster_ctrl_abort() == other.cluster_ctrl_abort())) ? 
        cluster_ctrl_abort() < other.cluster_ctrl_abort() : false
  ;
}

using _ClusterInstructionProto_ =  ConstClusterInstructionProto::_ClusterInstructionProto_;
ConstClusterInstructionProto::ConstClusterInstructionProto(const ::std::shared_ptr<_ClusterInstructionProto_>& data): data_(data) {}
ConstClusterInstructionProto::ConstClusterInstructionProto(): data_(::std::make_shared<_ClusterInstructionProto_>()) {}
ConstClusterInstructionProto::ConstClusterInstructionProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto) {
  BuildFromProto(proto_clusterinstructionproto);
}
ConstClusterInstructionProto::ConstClusterInstructionProto(const ConstClusterInstructionProto&) = default;
ConstClusterInstructionProto::ConstClusterInstructionProto(ConstClusterInstructionProto&&) noexcept = default;
ConstClusterInstructionProto::~ConstClusterInstructionProto() = default;

void ConstClusterInstructionProto::ToProto(PbMessage* proto_clusterinstructionproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClusterInstructionProto*>(proto_clusterinstructionproto));
}
  
::std::string ConstClusterInstructionProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClusterInstructionProto::__Empty__() const {
  return !data_;
}

int ConstClusterInstructionProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"cluster_ctrl_session_start", 1},
    {"cluster_ctrl_halt", 2},
    {"eager_instruction", 3},
    {"cluster_ctrl_abort", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstClusterInstructionProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstClusterInstructionProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClusterCtrlSessionStart),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClusterCtrlSessionStart),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClusterCtrlHalt),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClusterCtrlHalt),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::EagerInstruction),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstEagerInstruction),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClusterCtrlAbort),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClusterCtrlAbort),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClusterInstructionProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &cluster_ctrl_session_start();
    case 2: return &cluster_ctrl_halt();
    case 3: return &eager_instruction();
    case 5: return &cluster_ctrl_abort();
    default: return nullptr;
  }
}

 // oneof field instruction_type: cluster_ctrl_session_start
bool ConstClusterInstructionProto::has_cluster_ctrl_session_start() const {
  return __SharedPtrOrDefault__()->has_cluster_ctrl_session_start();
}
const ::oneflow::cfg::ClusterCtrlSessionStart& ConstClusterInstructionProto::cluster_ctrl_session_start() const {
  return __SharedPtrOrDefault__()->cluster_ctrl_session_start();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlSessionStart> ConstClusterInstructionProto::shared_const_cluster_ctrl_session_start() const {
  return cluster_ctrl_session_start().__SharedConst__();
}
 // oneof field instruction_type: cluster_ctrl_halt
bool ConstClusterInstructionProto::has_cluster_ctrl_halt() const {
  return __SharedPtrOrDefault__()->has_cluster_ctrl_halt();
}
const ::oneflow::cfg::ClusterCtrlHalt& ConstClusterInstructionProto::cluster_ctrl_halt() const {
  return __SharedPtrOrDefault__()->cluster_ctrl_halt();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlHalt> ConstClusterInstructionProto::shared_const_cluster_ctrl_halt() const {
  return cluster_ctrl_halt().__SharedConst__();
}
 // oneof field instruction_type: eager_instruction
bool ConstClusterInstructionProto::has_eager_instruction() const {
  return __SharedPtrOrDefault__()->has_eager_instruction();
}
const ::oneflow::vm::cfg::EagerInstruction& ConstClusterInstructionProto::eager_instruction() const {
  return __SharedPtrOrDefault__()->eager_instruction();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstEagerInstruction> ConstClusterInstructionProto::shared_const_eager_instruction() const {
  return eager_instruction().__SharedConst__();
}
 // oneof field instruction_type: cluster_ctrl_abort
bool ConstClusterInstructionProto::has_cluster_ctrl_abort() const {
  return __SharedPtrOrDefault__()->has_cluster_ctrl_abort();
}
const ::oneflow::cfg::ClusterCtrlAbort& ConstClusterInstructionProto::cluster_ctrl_abort() const {
  return __SharedPtrOrDefault__()->cluster_ctrl_abort();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClusterCtrlAbort> ConstClusterInstructionProto::shared_const_cluster_ctrl_abort() const {
  return cluster_ctrl_abort().__SharedConst__();
}
ConstClusterInstructionProto::InstructionTypeCase ConstClusterInstructionProto::instruction_type_case() const {
  return __SharedPtrOrDefault__()->instruction_type_case();
}

bool ConstClusterInstructionProto::has_instruction_type() const {
  return __SharedPtrOrDefault__()->has_instruction_type();
}

::std::shared_ptr<ConstClusterInstructionProto> ConstClusterInstructionProto::__SharedConst__() const {
  return ::std::make_shared<ConstClusterInstructionProto>(data_);
}
int64_t ConstClusterInstructionProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClusterInstructionProto::operator==(const ConstClusterInstructionProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClusterInstructionProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClusterInstructionProto::operator<(const ConstClusterInstructionProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClusterInstructionProto_>& ConstClusterInstructionProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClusterInstructionProto_> default_ptr = std::make_shared<_ClusterInstructionProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClusterInstructionProto_>& ConstClusterInstructionProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClusterInstructionProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClusterInstructionProto
void ConstClusterInstructionProto::BuildFromProto(const PbMessage& proto_clusterinstructionproto) {
  data_ = ::std::make_shared<_ClusterInstructionProto_>(dynamic_cast<const ::oneflow::ClusterInstructionProto&>(proto_clusterinstructionproto));
}

ClusterInstructionProto::ClusterInstructionProto(const ::std::shared_ptr<_ClusterInstructionProto_>& data)
  : ConstClusterInstructionProto(data) {}
ClusterInstructionProto::ClusterInstructionProto(const ClusterInstructionProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClusterInstructionProto> resize
ClusterInstructionProto::ClusterInstructionProto(ClusterInstructionProto&&) noexcept = default; 
ClusterInstructionProto::ClusterInstructionProto(const ::oneflow::ClusterInstructionProto& proto_clusterinstructionproto) {
  InitFromProto(proto_clusterinstructionproto);
}
ClusterInstructionProto::ClusterInstructionProto() = default;

ClusterInstructionProto::~ClusterInstructionProto() = default;

void ClusterInstructionProto::InitFromProto(const PbMessage& proto_clusterinstructionproto) {
  BuildFromProto(proto_clusterinstructionproto);
}
  
void* ClusterInstructionProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_cluster_ctrl_session_start();
    case 2: return mutable_cluster_ctrl_halt();
    case 3: return mutable_eager_instruction();
    case 5: return mutable_cluster_ctrl_abort();
    default: return nullptr;
  }
}

bool ClusterInstructionProto::operator==(const ClusterInstructionProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClusterInstructionProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClusterInstructionProto::operator<(const ClusterInstructionProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClusterInstructionProto::Clear() {
  if (data_) { data_.reset(); }
}
void ClusterInstructionProto::CopyFrom(const ClusterInstructionProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClusterInstructionProto& ClusterInstructionProto::operator=(const ClusterInstructionProto& other) {
  CopyFrom(other);
  return *this;
}

void ClusterInstructionProto::clear_cluster_ctrl_session_start() {
  return __SharedPtr__()->clear_cluster_ctrl_session_start();
}
::oneflow::cfg::ClusterCtrlSessionStart* ClusterInstructionProto::mutable_cluster_ctrl_session_start() {
  return __SharedPtr__()->mutable_cluster_ctrl_session_start();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClusterCtrlSessionStart> ClusterInstructionProto::shared_mutable_cluster_ctrl_session_start() {
  return mutable_cluster_ctrl_session_start()->__SharedMutable__();
}
void ClusterInstructionProto::clear_cluster_ctrl_halt() {
  return __SharedPtr__()->clear_cluster_ctrl_halt();
}
::oneflow::cfg::ClusterCtrlHalt* ClusterInstructionProto::mutable_cluster_ctrl_halt() {
  return __SharedPtr__()->mutable_cluster_ctrl_halt();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClusterCtrlHalt> ClusterInstructionProto::shared_mutable_cluster_ctrl_halt() {
  return mutable_cluster_ctrl_halt()->__SharedMutable__();
}
void ClusterInstructionProto::clear_eager_instruction() {
  return __SharedPtr__()->clear_eager_instruction();
}
::oneflow::vm::cfg::EagerInstruction* ClusterInstructionProto::mutable_eager_instruction() {
  return __SharedPtr__()->mutable_eager_instruction();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::EagerInstruction> ClusterInstructionProto::shared_mutable_eager_instruction() {
  return mutable_eager_instruction()->__SharedMutable__();
}
void ClusterInstructionProto::clear_cluster_ctrl_abort() {
  return __SharedPtr__()->clear_cluster_ctrl_abort();
}
::oneflow::cfg::ClusterCtrlAbort* ClusterInstructionProto::mutable_cluster_ctrl_abort() {
  return __SharedPtr__()->mutable_cluster_ctrl_abort();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClusterCtrlAbort> ClusterInstructionProto::shared_mutable_cluster_ctrl_abort() {
  return mutable_cluster_ctrl_abort()->__SharedMutable__();
}

::std::shared_ptr<ClusterInstructionProto> ClusterInstructionProto::__SharedMutable__() {
  return ::std::make_shared<ClusterInstructionProto>(__SharedPtr__());
}


}
} // namespace oneflow
