#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/learning_rate_schedule_conf.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.learning_rate_schedule_conf", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::*)(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::*)(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::*)(const int64_t&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::*)(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::*)(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::*)(const double&))&_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Set);
  }

  {
    pybind11::class_<ConstExponentialDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstExponentialDecayConf>> registry(m, "ConstExponentialDecayConf");
    registry.def("__id__", &::oneflow::cfg::ExponentialDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstExponentialDecayConf::DebugString);
    registry.def("__repr__", &ConstExponentialDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstExponentialDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstExponentialDecayConf::decay_batches);

    registry.def("has_decay_rate", &ConstExponentialDecayConf::has_decay_rate);
    registry.def("decay_rate", &ConstExponentialDecayConf::decay_rate);

    registry.def("has_staircase", &ConstExponentialDecayConf::has_staircase);
    registry.def("staircase", &ConstExponentialDecayConf::staircase);
  }
  {
    pybind11::class_<::oneflow::cfg::ExponentialDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>> registry(m, "ExponentialDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ExponentialDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ExponentialDecayConf::*)(const ConstExponentialDecayConf&))&::oneflow::cfg::ExponentialDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ExponentialDecayConf::*)(const ::oneflow::cfg::ExponentialDecayConf&))&::oneflow::cfg::ExponentialDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ExponentialDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ExponentialDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ExponentialDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::ExponentialDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::ExponentialDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::ExponentialDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::ExponentialDecayConf::set_decay_batches);

    registry.def("has_decay_rate", &::oneflow::cfg::ExponentialDecayConf::has_decay_rate);
    registry.def("clear_decay_rate", &::oneflow::cfg::ExponentialDecayConf::clear_decay_rate);
    registry.def("decay_rate", &::oneflow::cfg::ExponentialDecayConf::decay_rate);
    registry.def("set_decay_rate", &::oneflow::cfg::ExponentialDecayConf::set_decay_rate);

    registry.def("has_staircase", &::oneflow::cfg::ExponentialDecayConf::has_staircase);
    registry.def("clear_staircase", &::oneflow::cfg::ExponentialDecayConf::clear_staircase);
    registry.def("staircase", &::oneflow::cfg::ExponentialDecayConf::staircase);
    registry.def("set_staircase", &::oneflow::cfg::ExponentialDecayConf::set_staircase);
  }
  {
    pybind11::class_<ConstInverseTimeDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstInverseTimeDecayConf>> registry(m, "ConstInverseTimeDecayConf");
    registry.def("__id__", &::oneflow::cfg::InverseTimeDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstInverseTimeDecayConf::DebugString);
    registry.def("__repr__", &ConstInverseTimeDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstInverseTimeDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstInverseTimeDecayConf::decay_batches);

    registry.def("has_decay_rate", &ConstInverseTimeDecayConf::has_decay_rate);
    registry.def("decay_rate", &ConstInverseTimeDecayConf::decay_rate);

    registry.def("has_staircase", &ConstInverseTimeDecayConf::has_staircase);
    registry.def("staircase", &ConstInverseTimeDecayConf::staircase);
  }
  {
    pybind11::class_<::oneflow::cfg::InverseTimeDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>> registry(m, "InverseTimeDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::InverseTimeDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::InverseTimeDecayConf::*)(const ConstInverseTimeDecayConf&))&::oneflow::cfg::InverseTimeDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::InverseTimeDecayConf::*)(const ::oneflow::cfg::InverseTimeDecayConf&))&::oneflow::cfg::InverseTimeDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::InverseTimeDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::InverseTimeDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::InverseTimeDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::InverseTimeDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::InverseTimeDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::InverseTimeDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::InverseTimeDecayConf::set_decay_batches);

    registry.def("has_decay_rate", &::oneflow::cfg::InverseTimeDecayConf::has_decay_rate);
    registry.def("clear_decay_rate", &::oneflow::cfg::InverseTimeDecayConf::clear_decay_rate);
    registry.def("decay_rate", &::oneflow::cfg::InverseTimeDecayConf::decay_rate);
    registry.def("set_decay_rate", &::oneflow::cfg::InverseTimeDecayConf::set_decay_rate);

    registry.def("has_staircase", &::oneflow::cfg::InverseTimeDecayConf::has_staircase);
    registry.def("clear_staircase", &::oneflow::cfg::InverseTimeDecayConf::clear_staircase);
    registry.def("staircase", &::oneflow::cfg::InverseTimeDecayConf::staircase);
    registry.def("set_staircase", &::oneflow::cfg::InverseTimeDecayConf::set_staircase);
  }
  {
    pybind11::class_<ConstNaturalExpDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstNaturalExpDecayConf>> registry(m, "ConstNaturalExpDecayConf");
    registry.def("__id__", &::oneflow::cfg::NaturalExpDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstNaturalExpDecayConf::DebugString);
    registry.def("__repr__", &ConstNaturalExpDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstNaturalExpDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstNaturalExpDecayConf::decay_batches);

    registry.def("has_decay_rate", &ConstNaturalExpDecayConf::has_decay_rate);
    registry.def("decay_rate", &ConstNaturalExpDecayConf::decay_rate);

    registry.def("has_staircase", &ConstNaturalExpDecayConf::has_staircase);
    registry.def("staircase", &ConstNaturalExpDecayConf::staircase);
  }
  {
    pybind11::class_<::oneflow::cfg::NaturalExpDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>> registry(m, "NaturalExpDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::NaturalExpDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::NaturalExpDecayConf::*)(const ConstNaturalExpDecayConf&))&::oneflow::cfg::NaturalExpDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::NaturalExpDecayConf::*)(const ::oneflow::cfg::NaturalExpDecayConf&))&::oneflow::cfg::NaturalExpDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::NaturalExpDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::NaturalExpDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::NaturalExpDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::NaturalExpDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::NaturalExpDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::NaturalExpDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::NaturalExpDecayConf::set_decay_batches);

    registry.def("has_decay_rate", &::oneflow::cfg::NaturalExpDecayConf::has_decay_rate);
    registry.def("clear_decay_rate", &::oneflow::cfg::NaturalExpDecayConf::clear_decay_rate);
    registry.def("decay_rate", &::oneflow::cfg::NaturalExpDecayConf::decay_rate);
    registry.def("set_decay_rate", &::oneflow::cfg::NaturalExpDecayConf::set_decay_rate);

    registry.def("has_staircase", &::oneflow::cfg::NaturalExpDecayConf::has_staircase);
    registry.def("clear_staircase", &::oneflow::cfg::NaturalExpDecayConf::clear_staircase);
    registry.def("staircase", &::oneflow::cfg::NaturalExpDecayConf::staircase);
    registry.def("set_staircase", &::oneflow::cfg::NaturalExpDecayConf::set_staircase);
  }
  {
    pybind11::class_<ConstPiecewiseConstantConf, ::oneflow::cfg::Message, std::shared_ptr<ConstPiecewiseConstantConf>> registry(m, "ConstPiecewiseConstantConf");
    registry.def("__id__", &::oneflow::cfg::PiecewiseConstantConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPiecewiseConstantConf::DebugString);
    registry.def("__repr__", &ConstPiecewiseConstantConf::DebugString);

    registry.def("boundaries_size", &ConstPiecewiseConstantConf::boundaries_size);
    registry.def("boundaries", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (ConstPiecewiseConstantConf::*)() const)&ConstPiecewiseConstantConf::shared_const_boundaries);
    registry.def("boundaries", (const int64_t& (ConstPiecewiseConstantConf::*)(::std::size_t) const)&ConstPiecewiseConstantConf::boundaries);

    registry.def("values_size", &ConstPiecewiseConstantConf::values_size);
    registry.def("values", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (ConstPiecewiseConstantConf::*)() const)&ConstPiecewiseConstantConf::shared_const_values);
    registry.def("values", (const double& (ConstPiecewiseConstantConf::*)(::std::size_t) const)&ConstPiecewiseConstantConf::values);
  }
  {
    pybind11::class_<::oneflow::cfg::PiecewiseConstantConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>> registry(m, "PiecewiseConstantConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PiecewiseConstantConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PiecewiseConstantConf::*)(const ConstPiecewiseConstantConf&))&::oneflow::cfg::PiecewiseConstantConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PiecewiseConstantConf::*)(const ::oneflow::cfg::PiecewiseConstantConf&))&::oneflow::cfg::PiecewiseConstantConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PiecewiseConstantConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PiecewiseConstantConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PiecewiseConstantConf::DebugString);



    registry.def("boundaries_size", &::oneflow::cfg::PiecewiseConstantConf::boundaries_size);
    registry.def("clear_boundaries", &::oneflow::cfg::PiecewiseConstantConf::clear_boundaries);
    registry.def("mutable_boundaries", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::PiecewiseConstantConf::*)())&::oneflow::cfg::PiecewiseConstantConf::shared_mutable_boundaries);
    registry.def("boundaries", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::PiecewiseConstantConf::*)() const)&::oneflow::cfg::PiecewiseConstantConf::shared_const_boundaries);
    registry.def("boundaries", (const int64_t& (::oneflow::cfg::PiecewiseConstantConf::*)(::std::size_t) const)&::oneflow::cfg::PiecewiseConstantConf::boundaries);
    registry.def("add_boundaries", &::oneflow::cfg::PiecewiseConstantConf::add_boundaries);
    registry.def("set_boundaries", &::oneflow::cfg::PiecewiseConstantConf::set_boundaries);

    registry.def("values_size", &::oneflow::cfg::PiecewiseConstantConf::values_size);
    registry.def("clear_values", &::oneflow::cfg::PiecewiseConstantConf::clear_values);
    registry.def("mutable_values", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (::oneflow::cfg::PiecewiseConstantConf::*)())&::oneflow::cfg::PiecewiseConstantConf::shared_mutable_values);
    registry.def("values", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (::oneflow::cfg::PiecewiseConstantConf::*)() const)&::oneflow::cfg::PiecewiseConstantConf::shared_const_values);
    registry.def("values", (const double& (::oneflow::cfg::PiecewiseConstantConf::*)(::std::size_t) const)&::oneflow::cfg::PiecewiseConstantConf::values);
    registry.def("add_values", &::oneflow::cfg::PiecewiseConstantConf::add_values);
    registry.def("set_values", &::oneflow::cfg::PiecewiseConstantConf::set_values);
  }
  {
    pybind11::class_<ConstPolynomialDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstPolynomialDecayConf>> registry(m, "ConstPolynomialDecayConf");
    registry.def("__id__", &::oneflow::cfg::PolynomialDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPolynomialDecayConf::DebugString);
    registry.def("__repr__", &ConstPolynomialDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstPolynomialDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstPolynomialDecayConf::decay_batches);

    registry.def("has_end_learning_rate", &ConstPolynomialDecayConf::has_end_learning_rate);
    registry.def("end_learning_rate", &ConstPolynomialDecayConf::end_learning_rate);

    registry.def("has_power", &ConstPolynomialDecayConf::has_power);
    registry.def("power", &ConstPolynomialDecayConf::power);

    registry.def("has_cycle", &ConstPolynomialDecayConf::has_cycle);
    registry.def("cycle", &ConstPolynomialDecayConf::cycle);
  }
  {
    pybind11::class_<::oneflow::cfg::PolynomialDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>> registry(m, "PolynomialDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PolynomialDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PolynomialDecayConf::*)(const ConstPolynomialDecayConf&))&::oneflow::cfg::PolynomialDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PolynomialDecayConf::*)(const ::oneflow::cfg::PolynomialDecayConf&))&::oneflow::cfg::PolynomialDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PolynomialDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PolynomialDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PolynomialDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::PolynomialDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::PolynomialDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::PolynomialDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::PolynomialDecayConf::set_decay_batches);

    registry.def("has_end_learning_rate", &::oneflow::cfg::PolynomialDecayConf::has_end_learning_rate);
    registry.def("clear_end_learning_rate", &::oneflow::cfg::PolynomialDecayConf::clear_end_learning_rate);
    registry.def("end_learning_rate", &::oneflow::cfg::PolynomialDecayConf::end_learning_rate);
    registry.def("set_end_learning_rate", &::oneflow::cfg::PolynomialDecayConf::set_end_learning_rate);

    registry.def("has_power", &::oneflow::cfg::PolynomialDecayConf::has_power);
    registry.def("clear_power", &::oneflow::cfg::PolynomialDecayConf::clear_power);
    registry.def("power", &::oneflow::cfg::PolynomialDecayConf::power);
    registry.def("set_power", &::oneflow::cfg::PolynomialDecayConf::set_power);

    registry.def("has_cycle", &::oneflow::cfg::PolynomialDecayConf::has_cycle);
    registry.def("clear_cycle", &::oneflow::cfg::PolynomialDecayConf::clear_cycle);
    registry.def("cycle", &::oneflow::cfg::PolynomialDecayConf::cycle);
    registry.def("set_cycle", &::oneflow::cfg::PolynomialDecayConf::set_cycle);
  }
  {
    pybind11::class_<ConstCosineDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCosineDecayConf>> registry(m, "ConstCosineDecayConf");
    registry.def("__id__", &::oneflow::cfg::CosineDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCosineDecayConf::DebugString);
    registry.def("__repr__", &ConstCosineDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstCosineDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstCosineDecayConf::decay_batches);

    registry.def("has_alpha", &ConstCosineDecayConf::has_alpha);
    registry.def("alpha", &ConstCosineDecayConf::alpha);
  }
  {
    pybind11::class_<::oneflow::cfg::CosineDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CosineDecayConf>> registry(m, "CosineDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CosineDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CosineDecayConf::*)(const ConstCosineDecayConf&))&::oneflow::cfg::CosineDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CosineDecayConf::*)(const ::oneflow::cfg::CosineDecayConf&))&::oneflow::cfg::CosineDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CosineDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CosineDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CosineDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::CosineDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::CosineDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::CosineDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::CosineDecayConf::set_decay_batches);

    registry.def("has_alpha", &::oneflow::cfg::CosineDecayConf::has_alpha);
    registry.def("clear_alpha", &::oneflow::cfg::CosineDecayConf::clear_alpha);
    registry.def("alpha", &::oneflow::cfg::CosineDecayConf::alpha);
    registry.def("set_alpha", &::oneflow::cfg::CosineDecayConf::set_alpha);
  }
  {
    pybind11::class_<ConstLinearCosineDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLinearCosineDecayConf>> registry(m, "ConstLinearCosineDecayConf");
    registry.def("__id__", &::oneflow::cfg::LinearCosineDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLinearCosineDecayConf::DebugString);
    registry.def("__repr__", &ConstLinearCosineDecayConf::DebugString);

    registry.def("has_decay_batches", &ConstLinearCosineDecayConf::has_decay_batches);
    registry.def("decay_batches", &ConstLinearCosineDecayConf::decay_batches);

    registry.def("has_num_periods", &ConstLinearCosineDecayConf::has_num_periods);
    registry.def("num_periods", &ConstLinearCosineDecayConf::num_periods);

    registry.def("has_alpha", &ConstLinearCosineDecayConf::has_alpha);
    registry.def("alpha", &ConstLinearCosineDecayConf::alpha);

    registry.def("has_beta", &ConstLinearCosineDecayConf::has_beta);
    registry.def("beta", &ConstLinearCosineDecayConf::beta);
  }
  {
    pybind11::class_<::oneflow::cfg::LinearCosineDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>> registry(m, "LinearCosineDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LinearCosineDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LinearCosineDecayConf::*)(const ConstLinearCosineDecayConf&))&::oneflow::cfg::LinearCosineDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LinearCosineDecayConf::*)(const ::oneflow::cfg::LinearCosineDecayConf&))&::oneflow::cfg::LinearCosineDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LinearCosineDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LinearCosineDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LinearCosineDecayConf::DebugString);



    registry.def("has_decay_batches", &::oneflow::cfg::LinearCosineDecayConf::has_decay_batches);
    registry.def("clear_decay_batches", &::oneflow::cfg::LinearCosineDecayConf::clear_decay_batches);
    registry.def("decay_batches", &::oneflow::cfg::LinearCosineDecayConf::decay_batches);
    registry.def("set_decay_batches", &::oneflow::cfg::LinearCosineDecayConf::set_decay_batches);

    registry.def("has_num_periods", &::oneflow::cfg::LinearCosineDecayConf::has_num_periods);
    registry.def("clear_num_periods", &::oneflow::cfg::LinearCosineDecayConf::clear_num_periods);
    registry.def("num_periods", &::oneflow::cfg::LinearCosineDecayConf::num_periods);
    registry.def("set_num_periods", &::oneflow::cfg::LinearCosineDecayConf::set_num_periods);

    registry.def("has_alpha", &::oneflow::cfg::LinearCosineDecayConf::has_alpha);
    registry.def("clear_alpha", &::oneflow::cfg::LinearCosineDecayConf::clear_alpha);
    registry.def("alpha", &::oneflow::cfg::LinearCosineDecayConf::alpha);
    registry.def("set_alpha", &::oneflow::cfg::LinearCosineDecayConf::set_alpha);

    registry.def("has_beta", &::oneflow::cfg::LinearCosineDecayConf::has_beta);
    registry.def("clear_beta", &::oneflow::cfg::LinearCosineDecayConf::clear_beta);
    registry.def("beta", &::oneflow::cfg::LinearCosineDecayConf::beta);
    registry.def("set_beta", &::oneflow::cfg::LinearCosineDecayConf::set_beta);
  }
  {
    pybind11::class_<ConstPiecewiseScalingConf, ::oneflow::cfg::Message, std::shared_ptr<ConstPiecewiseScalingConf>> registry(m, "ConstPiecewiseScalingConf");
    registry.def("__id__", &::oneflow::cfg::PiecewiseScalingConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPiecewiseScalingConf::DebugString);
    registry.def("__repr__", &ConstPiecewiseScalingConf::DebugString);

    registry.def("boundaries_size", &ConstPiecewiseScalingConf::boundaries_size);
    registry.def("boundaries", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (ConstPiecewiseScalingConf::*)() const)&ConstPiecewiseScalingConf::shared_const_boundaries);
    registry.def("boundaries", (const int64_t& (ConstPiecewiseScalingConf::*)(::std::size_t) const)&ConstPiecewiseScalingConf::boundaries);

    registry.def("scales_size", &ConstPiecewiseScalingConf::scales_size);
    registry.def("scales", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (ConstPiecewiseScalingConf::*)() const)&ConstPiecewiseScalingConf::shared_const_scales);
    registry.def("scales", (const double& (ConstPiecewiseScalingConf::*)(::std::size_t) const)&ConstPiecewiseScalingConf::scales);
  }
  {
    pybind11::class_<::oneflow::cfg::PiecewiseScalingConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>> registry(m, "PiecewiseScalingConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PiecewiseScalingConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PiecewiseScalingConf::*)(const ConstPiecewiseScalingConf&))&::oneflow::cfg::PiecewiseScalingConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PiecewiseScalingConf::*)(const ::oneflow::cfg::PiecewiseScalingConf&))&::oneflow::cfg::PiecewiseScalingConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PiecewiseScalingConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PiecewiseScalingConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PiecewiseScalingConf::DebugString);



    registry.def("boundaries_size", &::oneflow::cfg::PiecewiseScalingConf::boundaries_size);
    registry.def("clear_boundaries", &::oneflow::cfg::PiecewiseScalingConf::clear_boundaries);
    registry.def("mutable_boundaries", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::PiecewiseScalingConf::*)())&::oneflow::cfg::PiecewiseScalingConf::shared_mutable_boundaries);
    registry.def("boundaries", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::PiecewiseScalingConf::*)() const)&::oneflow::cfg::PiecewiseScalingConf::shared_const_boundaries);
    registry.def("boundaries", (const int64_t& (::oneflow::cfg::PiecewiseScalingConf::*)(::std::size_t) const)&::oneflow::cfg::PiecewiseScalingConf::boundaries);
    registry.def("add_boundaries", &::oneflow::cfg::PiecewiseScalingConf::add_boundaries);
    registry.def("set_boundaries", &::oneflow::cfg::PiecewiseScalingConf::set_boundaries);

    registry.def("scales_size", &::oneflow::cfg::PiecewiseScalingConf::scales_size);
    registry.def("clear_scales", &::oneflow::cfg::PiecewiseScalingConf::clear_scales);
    registry.def("mutable_scales", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (::oneflow::cfg::PiecewiseScalingConf::*)())&::oneflow::cfg::PiecewiseScalingConf::shared_mutable_scales);
    registry.def("scales", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> (::oneflow::cfg::PiecewiseScalingConf::*)() const)&::oneflow::cfg::PiecewiseScalingConf::shared_const_scales);
    registry.def("scales", (const double& (::oneflow::cfg::PiecewiseScalingConf::*)(::std::size_t) const)&::oneflow::cfg::PiecewiseScalingConf::scales);
    registry.def("add_scales", &::oneflow::cfg::PiecewiseScalingConf::add_scales);
    registry.def("set_scales", &::oneflow::cfg::PiecewiseScalingConf::set_scales);
  }
  {
    pybind11::class_<ConstLearningRateDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLearningRateDecayConf>> registry(m, "ConstLearningRateDecayConf");
    registry.def("__id__", &::oneflow::cfg::LearningRateDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLearningRateDecayConf::DebugString);
    registry.def("__repr__", &ConstLearningRateDecayConf::DebugString);

    registry.def("has_exponential_conf", &ConstLearningRateDecayConf::has_exponential_conf);
    registry.def("exponential_conf", &ConstLearningRateDecayConf::shared_const_exponential_conf);

    registry.def("has_inverse_time_conf", &ConstLearningRateDecayConf::has_inverse_time_conf);
    registry.def("inverse_time_conf", &ConstLearningRateDecayConf::shared_const_inverse_time_conf);

    registry.def("has_natural_exp_conf", &ConstLearningRateDecayConf::has_natural_exp_conf);
    registry.def("natural_exp_conf", &ConstLearningRateDecayConf::shared_const_natural_exp_conf);

    registry.def("has_piecewise_constant_conf", &ConstLearningRateDecayConf::has_piecewise_constant_conf);
    registry.def("piecewise_constant_conf", &ConstLearningRateDecayConf::shared_const_piecewise_constant_conf);

    registry.def("has_polynomial_conf", &ConstLearningRateDecayConf::has_polynomial_conf);
    registry.def("polynomial_conf", &ConstLearningRateDecayConf::shared_const_polynomial_conf);

    registry.def("has_cosine_conf", &ConstLearningRateDecayConf::has_cosine_conf);
    registry.def("cosine_conf", &ConstLearningRateDecayConf::shared_const_cosine_conf);

    registry.def("has_linear_cosine_conf", &ConstLearningRateDecayConf::has_linear_cosine_conf);
    registry.def("linear_cosine_conf", &ConstLearningRateDecayConf::shared_const_linear_cosine_conf);

    registry.def("has_piecewise_scaling_conf", &ConstLearningRateDecayConf::has_piecewise_scaling_conf);
    registry.def("piecewise_scaling_conf", &ConstLearningRateDecayConf::shared_const_piecewise_scaling_conf);
    registry.def("type_case",  &ConstLearningRateDecayConf::type_case);
    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kExponentialConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kExponentialConf; })
        .def_property_readonly_static("kInverseTimeConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kInverseTimeConf; })
        .def_property_readonly_static("kNaturalExpConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kNaturalExpConf; })
        .def_property_readonly_static("kPiecewiseConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseConstantConf; })
        .def_property_readonly_static("kPolynomialConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPolynomialConf; })
        .def_property_readonly_static("kCosineConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kCosineConf; })
        .def_property_readonly_static("kLinearCosineConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kLinearCosineConf; })
        .def_property_readonly_static("kPiecewiseScalingConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseScalingConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::LearningRateDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LearningRateDecayConf>> registry(m, "LearningRateDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LearningRateDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LearningRateDecayConf::*)(const ConstLearningRateDecayConf&))&::oneflow::cfg::LearningRateDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LearningRateDecayConf::*)(const ::oneflow::cfg::LearningRateDecayConf&))&::oneflow::cfg::LearningRateDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LearningRateDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LearningRateDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LearningRateDecayConf::DebugString);

    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kExponentialConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kExponentialConf; })
        .def_property_readonly_static("kInverseTimeConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kInverseTimeConf; })
        .def_property_readonly_static("kNaturalExpConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kNaturalExpConf; })
        .def_property_readonly_static("kPiecewiseConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseConstantConf; })
        .def_property_readonly_static("kPolynomialConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPolynomialConf; })
        .def_property_readonly_static("kCosineConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kCosineConf; })
        .def_property_readonly_static("kLinearCosineConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kLinearCosineConf; })
        .def_property_readonly_static("kPiecewiseScalingConf", [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseScalingConf; })
        ;


    registry.def("has_exponential_conf", &::oneflow::cfg::LearningRateDecayConf::has_exponential_conf);
    registry.def("clear_exponential_conf", &::oneflow::cfg::LearningRateDecayConf::clear_exponential_conf);
    registry.def_property_readonly_static("kExponentialConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kExponentialConf; });
    registry.def("exponential_conf", &::oneflow::cfg::LearningRateDecayConf::exponential_conf);
    registry.def("mutable_exponential_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_exponential_conf);

    registry.def("has_inverse_time_conf", &::oneflow::cfg::LearningRateDecayConf::has_inverse_time_conf);
    registry.def("clear_inverse_time_conf", &::oneflow::cfg::LearningRateDecayConf::clear_inverse_time_conf);
    registry.def_property_readonly_static("kInverseTimeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kInverseTimeConf; });
    registry.def("inverse_time_conf", &::oneflow::cfg::LearningRateDecayConf::inverse_time_conf);
    registry.def("mutable_inverse_time_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_inverse_time_conf);

    registry.def("has_natural_exp_conf", &::oneflow::cfg::LearningRateDecayConf::has_natural_exp_conf);
    registry.def("clear_natural_exp_conf", &::oneflow::cfg::LearningRateDecayConf::clear_natural_exp_conf);
    registry.def_property_readonly_static("kNaturalExpConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kNaturalExpConf; });
    registry.def("natural_exp_conf", &::oneflow::cfg::LearningRateDecayConf::natural_exp_conf);
    registry.def("mutable_natural_exp_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_natural_exp_conf);

    registry.def("has_piecewise_constant_conf", &::oneflow::cfg::LearningRateDecayConf::has_piecewise_constant_conf);
    registry.def("clear_piecewise_constant_conf", &::oneflow::cfg::LearningRateDecayConf::clear_piecewise_constant_conf);
    registry.def_property_readonly_static("kPiecewiseConstantConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseConstantConf; });
    registry.def("piecewise_constant_conf", &::oneflow::cfg::LearningRateDecayConf::piecewise_constant_conf);
    registry.def("mutable_piecewise_constant_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_piecewise_constant_conf);

    registry.def("has_polynomial_conf", &::oneflow::cfg::LearningRateDecayConf::has_polynomial_conf);
    registry.def("clear_polynomial_conf", &::oneflow::cfg::LearningRateDecayConf::clear_polynomial_conf);
    registry.def_property_readonly_static("kPolynomialConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPolynomialConf; });
    registry.def("polynomial_conf", &::oneflow::cfg::LearningRateDecayConf::polynomial_conf);
    registry.def("mutable_polynomial_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_polynomial_conf);

    registry.def("has_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::has_cosine_conf);
    registry.def("clear_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::clear_cosine_conf);
    registry.def_property_readonly_static("kCosineConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kCosineConf; });
    registry.def("cosine_conf", &::oneflow::cfg::LearningRateDecayConf::cosine_conf);
    registry.def("mutable_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_cosine_conf);

    registry.def("has_linear_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::has_linear_cosine_conf);
    registry.def("clear_linear_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::clear_linear_cosine_conf);
    registry.def_property_readonly_static("kLinearCosineConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kLinearCosineConf; });
    registry.def("linear_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::linear_cosine_conf);
    registry.def("mutable_linear_cosine_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_linear_cosine_conf);

    registry.def("has_piecewise_scaling_conf", &::oneflow::cfg::LearningRateDecayConf::has_piecewise_scaling_conf);
    registry.def("clear_piecewise_scaling_conf", &::oneflow::cfg::LearningRateDecayConf::clear_piecewise_scaling_conf);
    registry.def_property_readonly_static("kPiecewiseScalingConf",
        [](const pybind11::object&){ return ::oneflow::cfg::LearningRateDecayConf::kPiecewiseScalingConf; });
    registry.def("piecewise_scaling_conf", &::oneflow::cfg::LearningRateDecayConf::piecewise_scaling_conf);
    registry.def("mutable_piecewise_scaling_conf", &::oneflow::cfg::LearningRateDecayConf::shared_mutable_piecewise_scaling_conf);
    pybind11::enum_<::oneflow::cfg::LearningRateDecayConf::TypeCase>(registry, "TypeCase")
        .value("TYPE_NOT_SET", ::oneflow::cfg::LearningRateDecayConf::TYPE_NOT_SET)
        .value("kExponentialConf", ::oneflow::cfg::LearningRateDecayConf::kExponentialConf)
        .value("kInverseTimeConf", ::oneflow::cfg::LearningRateDecayConf::kInverseTimeConf)
        .value("kNaturalExpConf", ::oneflow::cfg::LearningRateDecayConf::kNaturalExpConf)
        .value("kPiecewiseConstantConf", ::oneflow::cfg::LearningRateDecayConf::kPiecewiseConstantConf)
        .value("kPolynomialConf", ::oneflow::cfg::LearningRateDecayConf::kPolynomialConf)
        .value("kCosineConf", ::oneflow::cfg::LearningRateDecayConf::kCosineConf)
        .value("kLinearCosineConf", ::oneflow::cfg::LearningRateDecayConf::kLinearCosineConf)
        .value("kPiecewiseScalingConf", ::oneflow::cfg::LearningRateDecayConf::kPiecewiseScalingConf)
        ;
    registry.def("type_case",  &::oneflow::cfg::LearningRateDecayConf::type_case);
  }
  {
    pybind11::class_<ConstConstantWarmupConf, ::oneflow::cfg::Message, std::shared_ptr<ConstConstantWarmupConf>> registry(m, "ConstConstantWarmupConf");
    registry.def("__id__", &::oneflow::cfg::ConstantWarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstConstantWarmupConf::DebugString);
    registry.def("__repr__", &ConstConstantWarmupConf::DebugString);

    registry.def("has_warmup_batches", &ConstConstantWarmupConf::has_warmup_batches);
    registry.def("warmup_batches", &ConstConstantWarmupConf::warmup_batches);

    registry.def("has_multiplier", &ConstConstantWarmupConf::has_multiplier);
    registry.def("multiplier", &ConstConstantWarmupConf::multiplier);
  }
  {
    pybind11::class_<::oneflow::cfg::ConstantWarmupConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>> registry(m, "ConstantWarmupConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ConstantWarmupConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantWarmupConf::*)(const ConstConstantWarmupConf&))&::oneflow::cfg::ConstantWarmupConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantWarmupConf::*)(const ::oneflow::cfg::ConstantWarmupConf&))&::oneflow::cfg::ConstantWarmupConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ConstantWarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ConstantWarmupConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ConstantWarmupConf::DebugString);



    registry.def("has_warmup_batches", &::oneflow::cfg::ConstantWarmupConf::has_warmup_batches);
    registry.def("clear_warmup_batches", &::oneflow::cfg::ConstantWarmupConf::clear_warmup_batches);
    registry.def("warmup_batches", &::oneflow::cfg::ConstantWarmupConf::warmup_batches);
    registry.def("set_warmup_batches", &::oneflow::cfg::ConstantWarmupConf::set_warmup_batches);

    registry.def("has_multiplier", &::oneflow::cfg::ConstantWarmupConf::has_multiplier);
    registry.def("clear_multiplier", &::oneflow::cfg::ConstantWarmupConf::clear_multiplier);
    registry.def("multiplier", &::oneflow::cfg::ConstantWarmupConf::multiplier);
    registry.def("set_multiplier", &::oneflow::cfg::ConstantWarmupConf::set_multiplier);
  }
  {
    pybind11::class_<ConstLinearWarmupConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLinearWarmupConf>> registry(m, "ConstLinearWarmupConf");
    registry.def("__id__", &::oneflow::cfg::LinearWarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLinearWarmupConf::DebugString);
    registry.def("__repr__", &ConstLinearWarmupConf::DebugString);

    registry.def("has_warmup_batches", &ConstLinearWarmupConf::has_warmup_batches);
    registry.def("warmup_batches", &ConstLinearWarmupConf::warmup_batches);

    registry.def("has_start_multiplier", &ConstLinearWarmupConf::has_start_multiplier);
    registry.def("start_multiplier", &ConstLinearWarmupConf::start_multiplier);
  }
  {
    pybind11::class_<::oneflow::cfg::LinearWarmupConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LinearWarmupConf>> registry(m, "LinearWarmupConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LinearWarmupConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LinearWarmupConf::*)(const ConstLinearWarmupConf&))&::oneflow::cfg::LinearWarmupConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LinearWarmupConf::*)(const ::oneflow::cfg::LinearWarmupConf&))&::oneflow::cfg::LinearWarmupConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LinearWarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LinearWarmupConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LinearWarmupConf::DebugString);



    registry.def("has_warmup_batches", &::oneflow::cfg::LinearWarmupConf::has_warmup_batches);
    registry.def("clear_warmup_batches", &::oneflow::cfg::LinearWarmupConf::clear_warmup_batches);
    registry.def("warmup_batches", &::oneflow::cfg::LinearWarmupConf::warmup_batches);
    registry.def("set_warmup_batches", &::oneflow::cfg::LinearWarmupConf::set_warmup_batches);

    registry.def("has_start_multiplier", &::oneflow::cfg::LinearWarmupConf::has_start_multiplier);
    registry.def("clear_start_multiplier", &::oneflow::cfg::LinearWarmupConf::clear_start_multiplier);
    registry.def("start_multiplier", &::oneflow::cfg::LinearWarmupConf::start_multiplier);
    registry.def("set_start_multiplier", &::oneflow::cfg::LinearWarmupConf::set_start_multiplier);
  }
  {
    pybind11::class_<ConstWarmupConf, ::oneflow::cfg::Message, std::shared_ptr<ConstWarmupConf>> registry(m, "ConstWarmupConf");
    registry.def("__id__", &::oneflow::cfg::WarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstWarmupConf::DebugString);
    registry.def("__repr__", &ConstWarmupConf::DebugString);

    registry.def("has_constant_conf", &ConstWarmupConf::has_constant_conf);
    registry.def("constant_conf", &ConstWarmupConf::shared_const_constant_conf);

    registry.def("has_linear_conf", &ConstWarmupConf::has_linear_conf);
    registry.def("linear_conf", &ConstWarmupConf::shared_const_linear_conf);
    registry.def("type_case",  &ConstWarmupConf::type_case);
    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kConstantConf; })
        .def_property_readonly_static("kLinearConf", [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kLinearConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::WarmupConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::WarmupConf>> registry(m, "WarmupConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::WarmupConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::WarmupConf::*)(const ConstWarmupConf&))&::oneflow::cfg::WarmupConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::WarmupConf::*)(const ::oneflow::cfg::WarmupConf&))&::oneflow::cfg::WarmupConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::WarmupConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::WarmupConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::WarmupConf::DebugString);

    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kConstantConf; })
        .def_property_readonly_static("kLinearConf", [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kLinearConf; })
        ;


    registry.def("has_constant_conf", &::oneflow::cfg::WarmupConf::has_constant_conf);
    registry.def("clear_constant_conf", &::oneflow::cfg::WarmupConf::clear_constant_conf);
    registry.def_property_readonly_static("kConstantConf",
        [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kConstantConf; });
    registry.def("constant_conf", &::oneflow::cfg::WarmupConf::constant_conf);
    registry.def("mutable_constant_conf", &::oneflow::cfg::WarmupConf::shared_mutable_constant_conf);

    registry.def("has_linear_conf", &::oneflow::cfg::WarmupConf::has_linear_conf);
    registry.def("clear_linear_conf", &::oneflow::cfg::WarmupConf::clear_linear_conf);
    registry.def_property_readonly_static("kLinearConf",
        [](const pybind11::object&){ return ::oneflow::cfg::WarmupConf::kLinearConf; });
    registry.def("linear_conf", &::oneflow::cfg::WarmupConf::linear_conf);
    registry.def("mutable_linear_conf", &::oneflow::cfg::WarmupConf::shared_mutable_linear_conf);
    pybind11::enum_<::oneflow::cfg::WarmupConf::TypeCase>(registry, "TypeCase")
        .value("TYPE_NOT_SET", ::oneflow::cfg::WarmupConf::TYPE_NOT_SET)
        .value("kConstantConf", ::oneflow::cfg::WarmupConf::kConstantConf)
        .value("kLinearConf", ::oneflow::cfg::WarmupConf::kLinearConf)
        ;
    registry.def("type_case",  &::oneflow::cfg::WarmupConf::type_case);
  }
}