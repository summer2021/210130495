#include "oneflow/core/job/parallel_signature.cfg.h"
#include "oneflow/core/job/parallel_signature.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstParallelSignature::_ParallelSignature_::_ParallelSignature_() { Clear(); }
ConstParallelSignature::_ParallelSignature_::_ParallelSignature_(const _ParallelSignature_& other) { CopyFrom(other); }
ConstParallelSignature::_ParallelSignature_::_ParallelSignature_(const ::oneflow::ParallelSignature& proto_parallelsignature) {
  InitFromProto(proto_parallelsignature);
}
ConstParallelSignature::_ParallelSignature_::_ParallelSignature_(_ParallelSignature_&& other) = default;
ConstParallelSignature::_ParallelSignature_::~_ParallelSignature_() = default;

void ConstParallelSignature::_ParallelSignature_::InitFromProto(const ::oneflow::ParallelSignature& proto_parallelsignature) {
  Clear();
  // required_or_optional field: op_parallel_desc_symbol_id
  if (proto_parallelsignature.has_op_parallel_desc_symbol_id()) {
    set_op_parallel_desc_symbol_id(proto_parallelsignature.op_parallel_desc_symbol_id());
  }
  // map field : bn_in_op2parallel_desc_symbol_id
  if (!proto_parallelsignature.bn_in_op2parallel_desc_symbol_id().empty()) {
_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_&  mut_bn_in_op2parallel_desc_symbol_id = *mutable_bn_in_op2parallel_desc_symbol_id();
    for (const auto& pair : proto_parallelsignature.bn_in_op2parallel_desc_symbol_id()) {
      mut_bn_in_op2parallel_desc_symbol_id[pair.first] = pair.second;
      }
  }
    
}

void ConstParallelSignature::_ParallelSignature_::ToProto(::oneflow::ParallelSignature* proto_parallelsignature) const {
  proto_parallelsignature->Clear();
  // required_or_optional field: op_parallel_desc_symbol_id
  if (this->has_op_parallel_desc_symbol_id()) {
    proto_parallelsignature->set_op_parallel_desc_symbol_id(op_parallel_desc_symbol_id());
    }
  // map field : bn_in_op2parallel_desc_symbol_id
  if (!bn_in_op2parallel_desc_symbol_id().empty()) {
    auto& mut_bn_in_op2parallel_desc_symbol_id = *(proto_parallelsignature->mutable_bn_in_op2parallel_desc_symbol_id());
    for (const auto& pair : bn_in_op2parallel_desc_symbol_id()) {
      mut_bn_in_op2parallel_desc_symbol_id[pair.first] = pair.second;
    }
  }

}

::std::string ConstParallelSignature::_ParallelSignature_::DebugString() const {
  ::oneflow::ParallelSignature proto_parallelsignature;
  this->ToProto(&proto_parallelsignature);
  return proto_parallelsignature.DebugString();
}

void ConstParallelSignature::_ParallelSignature_::Clear() {
  clear_op_parallel_desc_symbol_id();
  clear_bn_in_op2parallel_desc_symbol_id();
}

void ConstParallelSignature::_ParallelSignature_::CopyFrom(const _ParallelSignature_& other) {
  if (other.has_op_parallel_desc_symbol_id()) {
    set_op_parallel_desc_symbol_id(other.op_parallel_desc_symbol_id());
  } else {
    clear_op_parallel_desc_symbol_id();
  }
  mutable_bn_in_op2parallel_desc_symbol_id()->CopyFrom(other.bn_in_op2parallel_desc_symbol_id());
}


// optional field op_parallel_desc_symbol_id
bool ConstParallelSignature::_ParallelSignature_::has_op_parallel_desc_symbol_id() const {
  return has_op_parallel_desc_symbol_id_;
}
const int64_t& ConstParallelSignature::_ParallelSignature_::op_parallel_desc_symbol_id() const {
  if (has_op_parallel_desc_symbol_id_) { return op_parallel_desc_symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstParallelSignature::_ParallelSignature_::clear_op_parallel_desc_symbol_id() {
  has_op_parallel_desc_symbol_id_ = false;
}
void ConstParallelSignature::_ParallelSignature_::set_op_parallel_desc_symbol_id(const int64_t& value) {
  op_parallel_desc_symbol_id_ = value;
  has_op_parallel_desc_symbol_id_ = true;
}
int64_t* ConstParallelSignature::_ParallelSignature_::mutable_op_parallel_desc_symbol_id() {
  has_op_parallel_desc_symbol_id_ = true;
  return &op_parallel_desc_symbol_id_;
}

::std::size_t ConstParallelSignature::_ParallelSignature_::bn_in_op2parallel_desc_symbol_id_size() const {
  if (!bn_in_op2parallel_desc_symbol_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>();
    return default_static_value->size();
  }
  return bn_in_op2parallel_desc_symbol_id_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& ConstParallelSignature::_ParallelSignature_::bn_in_op2parallel_desc_symbol_id() const {
  if (!bn_in_op2parallel_desc_symbol_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2parallel_desc_symbol_id_.get());
}

_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ * ConstParallelSignature::_ParallelSignature_::mutable_bn_in_op2parallel_desc_symbol_id() {
  if (!bn_in_op2parallel_desc_symbol_id_) {
    bn_in_op2parallel_desc_symbol_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>();
  }
  return bn_in_op2parallel_desc_symbol_id_.get();
}

const int64_t& ConstParallelSignature::_ParallelSignature_::bn_in_op2parallel_desc_symbol_id(::std::string key) const {
  if (!bn_in_op2parallel_desc_symbol_id_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>();
    return default_static_value->at(key);
  }
  return bn_in_op2parallel_desc_symbol_id_->at(key);
}

void ConstParallelSignature::_ParallelSignature_::clear_bn_in_op2parallel_desc_symbol_id() {
  if (!bn_in_op2parallel_desc_symbol_id_) {
    bn_in_op2parallel_desc_symbol_id_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>();
  }
  return bn_in_op2parallel_desc_symbol_id_->Clear();
}



int ConstParallelSignature::_ParallelSignature_::compare(const _ParallelSignature_& other) {
  if (!(has_op_parallel_desc_symbol_id() == other.has_op_parallel_desc_symbol_id())) {
    return has_op_parallel_desc_symbol_id() < other.has_op_parallel_desc_symbol_id() ? -1 : 1;
  } else if (!(op_parallel_desc_symbol_id() == other.op_parallel_desc_symbol_id())) {
    return op_parallel_desc_symbol_id() < other.op_parallel_desc_symbol_id() ? -1 : 1;
  }
  if (!(bn_in_op2parallel_desc_symbol_id() == other.bn_in_op2parallel_desc_symbol_id())) {
    return bn_in_op2parallel_desc_symbol_id() < other.bn_in_op2parallel_desc_symbol_id() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelSignature::_ParallelSignature_::operator==(const _ParallelSignature_& other) const {
  return true
    && has_op_parallel_desc_symbol_id() == other.has_op_parallel_desc_symbol_id() 
    && op_parallel_desc_symbol_id() == other.op_parallel_desc_symbol_id()
    && bn_in_op2parallel_desc_symbol_id() == other.bn_in_op2parallel_desc_symbol_id()
  ;
}

std::size_t ConstParallelSignature::_ParallelSignature_::__CalcHash__() const {
  return 0
    ^ (has_op_parallel_desc_symbol_id() ? std::hash<int64_t>()(op_parallel_desc_symbol_id()) : 0)
    ^ bn_in_op2parallel_desc_symbol_id().__CalcHash__()
  ;
}

bool ConstParallelSignature::_ParallelSignature_::operator<(const _ParallelSignature_& other) const {
  return false
    || !(has_op_parallel_desc_symbol_id() == other.has_op_parallel_desc_symbol_id()) ? 
      has_op_parallel_desc_symbol_id() < other.has_op_parallel_desc_symbol_id() : false
    || !(op_parallel_desc_symbol_id() == other.op_parallel_desc_symbol_id()) ? 
      op_parallel_desc_symbol_id() < other.op_parallel_desc_symbol_id() : false
    || !(bn_in_op2parallel_desc_symbol_id() == other.bn_in_op2parallel_desc_symbol_id()) ? 
      bn_in_op2parallel_desc_symbol_id() < other.bn_in_op2parallel_desc_symbol_id() : false
  ;
}

using _ParallelSignature_ =  ConstParallelSignature::_ParallelSignature_;
ConstParallelSignature::ConstParallelSignature(const ::std::shared_ptr<_ParallelSignature_>& data): data_(data) {}
ConstParallelSignature::ConstParallelSignature(): data_(::std::make_shared<_ParallelSignature_>()) {}
ConstParallelSignature::ConstParallelSignature(const ::oneflow::ParallelSignature& proto_parallelsignature) {
  BuildFromProto(proto_parallelsignature);
}
ConstParallelSignature::ConstParallelSignature(const ConstParallelSignature&) = default;
ConstParallelSignature::ConstParallelSignature(ConstParallelSignature&&) noexcept = default;
ConstParallelSignature::~ConstParallelSignature() = default;

void ConstParallelSignature::ToProto(PbMessage* proto_parallelsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelSignature*>(proto_parallelsignature));
}
  
::std::string ConstParallelSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelSignature::__Empty__() const {
  return !data_;
}

int ConstParallelSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_parallel_desc_symbol_id", 1},
    {"bn_in_op2parallel_desc_symbol_id", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, int64_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_parallel_desc_symbol_id();
    case 2: return &bn_in_op2parallel_desc_symbol_id();
    default: return nullptr;
  }
}

// required or optional field op_parallel_desc_symbol_id
bool ConstParallelSignature::has_op_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->has_op_parallel_desc_symbol_id();
}
const int64_t& ConstParallelSignature::op_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->op_parallel_desc_symbol_id();
}
// used by pybind11 only
// map field bn_in_op2parallel_desc_symbol_id
::std::size_t ConstParallelSignature::bn_in_op2parallel_desc_symbol_id_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_desc_symbol_id_size();
}

const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& ConstParallelSignature::bn_in_op2parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_desc_symbol_id();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> ConstParallelSignature::shared_const_bn_in_op2parallel_desc_symbol_id() const {
  return bn_in_op2parallel_desc_symbol_id().__SharedConst__();
}

::std::shared_ptr<ConstParallelSignature> ConstParallelSignature::__SharedConst__() const {
  return ::std::make_shared<ConstParallelSignature>(data_);
}
int64_t ConstParallelSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelSignature::operator==(const ConstParallelSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelSignature::operator<(const ConstParallelSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelSignature_>& ConstParallelSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelSignature_> default_ptr = std::make_shared<_ParallelSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelSignature_>& ConstParallelSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelSignature
void ConstParallelSignature::BuildFromProto(const PbMessage& proto_parallelsignature) {
  data_ = ::std::make_shared<_ParallelSignature_>(dynamic_cast<const ::oneflow::ParallelSignature&>(proto_parallelsignature));
}

ParallelSignature::ParallelSignature(const ::std::shared_ptr<_ParallelSignature_>& data)
  : ConstParallelSignature(data) {}
ParallelSignature::ParallelSignature(const ParallelSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelSignature> resize
ParallelSignature::ParallelSignature(ParallelSignature&&) noexcept = default; 
ParallelSignature::ParallelSignature(const ::oneflow::ParallelSignature& proto_parallelsignature) {
  InitFromProto(proto_parallelsignature);
}
ParallelSignature::ParallelSignature() = default;

ParallelSignature::~ParallelSignature() = default;

void ParallelSignature::InitFromProto(const PbMessage& proto_parallelsignature) {
  BuildFromProto(proto_parallelsignature);
}
  
void* ParallelSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_parallel_desc_symbol_id();
    case 2: return mutable_bn_in_op2parallel_desc_symbol_id();
    default: return nullptr;
  }
}

bool ParallelSignature::operator==(const ParallelSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelSignature::operator<(const ParallelSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelSignature::CopyFrom(const ParallelSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelSignature& ParallelSignature::operator=(const ParallelSignature& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_parallel_desc_symbol_id
void ParallelSignature::clear_op_parallel_desc_symbol_id() {
  return __SharedPtr__()->clear_op_parallel_desc_symbol_id();
}
void ParallelSignature::set_op_parallel_desc_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_op_parallel_desc_symbol_id(value);
}
int64_t* ParallelSignature::mutable_op_parallel_desc_symbol_id() {
  return  __SharedPtr__()->mutable_op_parallel_desc_symbol_id();
}
// repeated field bn_in_op2parallel_desc_symbol_id
void ParallelSignature::clear_bn_in_op2parallel_desc_symbol_id() {
  return __SharedPtr__()->clear_bn_in_op2parallel_desc_symbol_id();
}

const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ & ParallelSignature::bn_in_op2parallel_desc_symbol_id() const {
  return __SharedConst__()->bn_in_op2parallel_desc_symbol_id();
}

_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_* ParallelSignature::mutable_bn_in_op2parallel_desc_symbol_id() {
  return __SharedPtr__()->mutable_bn_in_op2parallel_desc_symbol_id();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> ParallelSignature::shared_mutable_bn_in_op2parallel_desc_symbol_id() {
  return mutable_bn_in_op2parallel_desc_symbol_id()->__SharedMutable__();
}

::std::shared_ptr<ParallelSignature> ParallelSignature::__SharedMutable__() {
  return ::std::make_shared<ParallelSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_(const ::std::shared_ptr<::std::map<::std::string, int64_t>>& data): ::oneflow::cfg::_MapField_<::std::string, int64_t>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::~Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<int64_t>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const int64_t& Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>(__SharedPtr__());
}


_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_(const ::std::shared_ptr<::std::map<::std::string, int64_t>>& data): Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_(data) {}
_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_() = default;
_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::~_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_() = default;

void _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) {
  ::oneflow::cfg::_MapField_<::std::string, int64_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) {
  ::oneflow::cfg::_MapField_<::std::string, int64_t>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::operator==(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<int64_t>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::operator<(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_>(__SharedPtr__());
}

void _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_::Set(const ::std::string& key, const int64_t& value) {
  (*this)[key] = value;
}

}
} // namespace oneflow
