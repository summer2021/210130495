#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/job_conf.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/job/initializer_conf.cfg.h"
#include "oneflow/core/job/learning_rate_schedule_conf.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/operator/interface_blob_conf.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.job_conf", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstOptimizerConf> (Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstOptimizerConf> (Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(const ::oneflow::cfg::OptimizerConf&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::OptimizerConf> (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::OptimizerConf> (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstJobInputDef>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstJobInputDef>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstJobInputDef> (Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::*)(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::*)(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<JobInputDef>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<JobInputDef>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::JobInputDef> (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstJobOutputDef>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstJobOutputDef>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstJobOutputDef> (Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::*)(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::*)(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<JobOutputDef>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<JobOutputDef>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::JobOutputDef> (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstAttrValue> (Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::AttrValue> (_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstNaiveModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstNaiveModelUpdateConf>> registry(m, "ConstNaiveModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::NaiveModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstNaiveModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstNaiveModelUpdateConf::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::NaiveModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>> registry(m, "NaiveModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::NaiveModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::NaiveModelUpdateConf::*)(const ConstNaiveModelUpdateConf&))&::oneflow::cfg::NaiveModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::NaiveModelUpdateConf::*)(const ::oneflow::cfg::NaiveModelUpdateConf&))&::oneflow::cfg::NaiveModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::NaiveModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::NaiveModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::NaiveModelUpdateConf::DebugString);


  }
  {
    pybind11::class_<ConstMomentumModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstMomentumModelUpdateConf>> registry(m, "ConstMomentumModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::MomentumModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstMomentumModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstMomentumModelUpdateConf::DebugString);

    registry.def("has_beta", &ConstMomentumModelUpdateConf::has_beta);
    registry.def("beta", &ConstMomentumModelUpdateConf::beta);
  }
  {
    pybind11::class_<::oneflow::cfg::MomentumModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>> registry(m, "MomentumModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::MomentumModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::MomentumModelUpdateConf::*)(const ConstMomentumModelUpdateConf&))&::oneflow::cfg::MomentumModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::MomentumModelUpdateConf::*)(const ::oneflow::cfg::MomentumModelUpdateConf&))&::oneflow::cfg::MomentumModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::MomentumModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::MomentumModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::MomentumModelUpdateConf::DebugString);



    registry.def("has_beta", &::oneflow::cfg::MomentumModelUpdateConf::has_beta);
    registry.def("clear_beta", &::oneflow::cfg::MomentumModelUpdateConf::clear_beta);
    registry.def("beta", &::oneflow::cfg::MomentumModelUpdateConf::beta);
    registry.def("set_beta", &::oneflow::cfg::MomentumModelUpdateConf::set_beta);
  }
  {
    pybind11::class_<ConstRMSPropModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRMSPropModelUpdateConf>> registry(m, "ConstRMSPropModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::RMSPropModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRMSPropModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstRMSPropModelUpdateConf::DebugString);

    registry.def("has_decay_rate", &ConstRMSPropModelUpdateConf::has_decay_rate);
    registry.def("decay_rate", &ConstRMSPropModelUpdateConf::decay_rate);

    registry.def("has_epsilon", &ConstRMSPropModelUpdateConf::has_epsilon);
    registry.def("epsilon", &ConstRMSPropModelUpdateConf::epsilon);

    registry.def("has_centered", &ConstRMSPropModelUpdateConf::has_centered);
    registry.def("centered", &ConstRMSPropModelUpdateConf::centered);
  }
  {
    pybind11::class_<::oneflow::cfg::RMSPropModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>> registry(m, "RMSPropModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RMSPropModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RMSPropModelUpdateConf::*)(const ConstRMSPropModelUpdateConf&))&::oneflow::cfg::RMSPropModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RMSPropModelUpdateConf::*)(const ::oneflow::cfg::RMSPropModelUpdateConf&))&::oneflow::cfg::RMSPropModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RMSPropModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RMSPropModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RMSPropModelUpdateConf::DebugString);



    registry.def("has_decay_rate", &::oneflow::cfg::RMSPropModelUpdateConf::has_decay_rate);
    registry.def("clear_decay_rate", &::oneflow::cfg::RMSPropModelUpdateConf::clear_decay_rate);
    registry.def("decay_rate", &::oneflow::cfg::RMSPropModelUpdateConf::decay_rate);
    registry.def("set_decay_rate", &::oneflow::cfg::RMSPropModelUpdateConf::set_decay_rate);

    registry.def("has_epsilon", &::oneflow::cfg::RMSPropModelUpdateConf::has_epsilon);
    registry.def("clear_epsilon", &::oneflow::cfg::RMSPropModelUpdateConf::clear_epsilon);
    registry.def("epsilon", &::oneflow::cfg::RMSPropModelUpdateConf::epsilon);
    registry.def("set_epsilon", &::oneflow::cfg::RMSPropModelUpdateConf::set_epsilon);

    registry.def("has_centered", &::oneflow::cfg::RMSPropModelUpdateConf::has_centered);
    registry.def("clear_centered", &::oneflow::cfg::RMSPropModelUpdateConf::clear_centered);
    registry.def("centered", &::oneflow::cfg::RMSPropModelUpdateConf::centered);
    registry.def("set_centered", &::oneflow::cfg::RMSPropModelUpdateConf::set_centered);
  }
  {
    pybind11::class_<ConstLARSModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLARSModelUpdateConf>> registry(m, "ConstLARSModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::LARSModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLARSModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstLARSModelUpdateConf::DebugString);

    registry.def("has_momentum_beta", &ConstLARSModelUpdateConf::has_momentum_beta);
    registry.def("momentum_beta", &ConstLARSModelUpdateConf::momentum_beta);

    registry.def("has_epsilon", &ConstLARSModelUpdateConf::has_epsilon);
    registry.def("epsilon", &ConstLARSModelUpdateConf::epsilon);

    registry.def("has_lars_coefficient", &ConstLARSModelUpdateConf::has_lars_coefficient);
    registry.def("lars_coefficient", &ConstLARSModelUpdateConf::lars_coefficient);
  }
  {
    pybind11::class_<::oneflow::cfg::LARSModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>> registry(m, "LARSModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LARSModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LARSModelUpdateConf::*)(const ConstLARSModelUpdateConf&))&::oneflow::cfg::LARSModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LARSModelUpdateConf::*)(const ::oneflow::cfg::LARSModelUpdateConf&))&::oneflow::cfg::LARSModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LARSModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LARSModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LARSModelUpdateConf::DebugString);



    registry.def("has_momentum_beta", &::oneflow::cfg::LARSModelUpdateConf::has_momentum_beta);
    registry.def("clear_momentum_beta", &::oneflow::cfg::LARSModelUpdateConf::clear_momentum_beta);
    registry.def("momentum_beta", &::oneflow::cfg::LARSModelUpdateConf::momentum_beta);
    registry.def("set_momentum_beta", &::oneflow::cfg::LARSModelUpdateConf::set_momentum_beta);

    registry.def("has_epsilon", &::oneflow::cfg::LARSModelUpdateConf::has_epsilon);
    registry.def("clear_epsilon", &::oneflow::cfg::LARSModelUpdateConf::clear_epsilon);
    registry.def("epsilon", &::oneflow::cfg::LARSModelUpdateConf::epsilon);
    registry.def("set_epsilon", &::oneflow::cfg::LARSModelUpdateConf::set_epsilon);

    registry.def("has_lars_coefficient", &::oneflow::cfg::LARSModelUpdateConf::has_lars_coefficient);
    registry.def("clear_lars_coefficient", &::oneflow::cfg::LARSModelUpdateConf::clear_lars_coefficient);
    registry.def("lars_coefficient", &::oneflow::cfg::LARSModelUpdateConf::lars_coefficient);
    registry.def("set_lars_coefficient", &::oneflow::cfg::LARSModelUpdateConf::set_lars_coefficient);
  }
  {
    pybind11::class_<ConstAdamModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstAdamModelUpdateConf>> registry(m, "ConstAdamModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::AdamModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAdamModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstAdamModelUpdateConf::DebugString);

    registry.def("has_beta1", &ConstAdamModelUpdateConf::has_beta1);
    registry.def("beta1", &ConstAdamModelUpdateConf::beta1);

    registry.def("has_beta2", &ConstAdamModelUpdateConf::has_beta2);
    registry.def("beta2", &ConstAdamModelUpdateConf::beta2);

    registry.def("has_epsilon", &ConstAdamModelUpdateConf::has_epsilon);
    registry.def("epsilon", &ConstAdamModelUpdateConf::epsilon);

    registry.def("has_do_bias_correction", &ConstAdamModelUpdateConf::has_do_bias_correction);
    registry.def("do_bias_correction", &ConstAdamModelUpdateConf::do_bias_correction);
  }
  {
    pybind11::class_<::oneflow::cfg::AdamModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>> registry(m, "AdamModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AdamModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AdamModelUpdateConf::*)(const ConstAdamModelUpdateConf&))&::oneflow::cfg::AdamModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AdamModelUpdateConf::*)(const ::oneflow::cfg::AdamModelUpdateConf&))&::oneflow::cfg::AdamModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AdamModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AdamModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AdamModelUpdateConf::DebugString);



    registry.def("has_beta1", &::oneflow::cfg::AdamModelUpdateConf::has_beta1);
    registry.def("clear_beta1", &::oneflow::cfg::AdamModelUpdateConf::clear_beta1);
    registry.def("beta1", &::oneflow::cfg::AdamModelUpdateConf::beta1);
    registry.def("set_beta1", &::oneflow::cfg::AdamModelUpdateConf::set_beta1);

    registry.def("has_beta2", &::oneflow::cfg::AdamModelUpdateConf::has_beta2);
    registry.def("clear_beta2", &::oneflow::cfg::AdamModelUpdateConf::clear_beta2);
    registry.def("beta2", &::oneflow::cfg::AdamModelUpdateConf::beta2);
    registry.def("set_beta2", &::oneflow::cfg::AdamModelUpdateConf::set_beta2);

    registry.def("has_epsilon", &::oneflow::cfg::AdamModelUpdateConf::has_epsilon);
    registry.def("clear_epsilon", &::oneflow::cfg::AdamModelUpdateConf::clear_epsilon);
    registry.def("epsilon", &::oneflow::cfg::AdamModelUpdateConf::epsilon);
    registry.def("set_epsilon", &::oneflow::cfg::AdamModelUpdateConf::set_epsilon);

    registry.def("has_do_bias_correction", &::oneflow::cfg::AdamModelUpdateConf::has_do_bias_correction);
    registry.def("clear_do_bias_correction", &::oneflow::cfg::AdamModelUpdateConf::clear_do_bias_correction);
    registry.def("do_bias_correction", &::oneflow::cfg::AdamModelUpdateConf::do_bias_correction);
    registry.def("set_do_bias_correction", &::oneflow::cfg::AdamModelUpdateConf::set_do_bias_correction);
  }
  {
    pybind11::class_<ConstLazyAdamModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLazyAdamModelUpdateConf>> registry(m, "ConstLazyAdamModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::LazyAdamModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLazyAdamModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstLazyAdamModelUpdateConf::DebugString);

    registry.def("has_beta1", &ConstLazyAdamModelUpdateConf::has_beta1);
    registry.def("beta1", &ConstLazyAdamModelUpdateConf::beta1);

    registry.def("has_beta2", &ConstLazyAdamModelUpdateConf::has_beta2);
    registry.def("beta2", &ConstLazyAdamModelUpdateConf::beta2);

    registry.def("has_epsilon", &ConstLazyAdamModelUpdateConf::has_epsilon);
    registry.def("epsilon", &ConstLazyAdamModelUpdateConf::epsilon);
  }
  {
    pybind11::class_<::oneflow::cfg::LazyAdamModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>> registry(m, "LazyAdamModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LazyAdamModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LazyAdamModelUpdateConf::*)(const ConstLazyAdamModelUpdateConf&))&::oneflow::cfg::LazyAdamModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LazyAdamModelUpdateConf::*)(const ::oneflow::cfg::LazyAdamModelUpdateConf&))&::oneflow::cfg::LazyAdamModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LazyAdamModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LazyAdamModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LazyAdamModelUpdateConf::DebugString);



    registry.def("has_beta1", &::oneflow::cfg::LazyAdamModelUpdateConf::has_beta1);
    registry.def("clear_beta1", &::oneflow::cfg::LazyAdamModelUpdateConf::clear_beta1);
    registry.def("beta1", &::oneflow::cfg::LazyAdamModelUpdateConf::beta1);
    registry.def("set_beta1", &::oneflow::cfg::LazyAdamModelUpdateConf::set_beta1);

    registry.def("has_beta2", &::oneflow::cfg::LazyAdamModelUpdateConf::has_beta2);
    registry.def("clear_beta2", &::oneflow::cfg::LazyAdamModelUpdateConf::clear_beta2);
    registry.def("beta2", &::oneflow::cfg::LazyAdamModelUpdateConf::beta2);
    registry.def("set_beta2", &::oneflow::cfg::LazyAdamModelUpdateConf::set_beta2);

    registry.def("has_epsilon", &::oneflow::cfg::LazyAdamModelUpdateConf::has_epsilon);
    registry.def("clear_epsilon", &::oneflow::cfg::LazyAdamModelUpdateConf::clear_epsilon);
    registry.def("epsilon", &::oneflow::cfg::LazyAdamModelUpdateConf::epsilon);
    registry.def("set_epsilon", &::oneflow::cfg::LazyAdamModelUpdateConf::set_epsilon);
  }
  {
    pybind11::class_<ConstLambModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLambModelUpdateConf>> registry(m, "ConstLambModelUpdateConf");
    registry.def("__id__", &::oneflow::cfg::LambModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLambModelUpdateConf::DebugString);
    registry.def("__repr__", &ConstLambModelUpdateConf::DebugString);

    registry.def("has_beta1", &ConstLambModelUpdateConf::has_beta1);
    registry.def("beta1", &ConstLambModelUpdateConf::beta1);

    registry.def("has_beta2", &ConstLambModelUpdateConf::has_beta2);
    registry.def("beta2", &ConstLambModelUpdateConf::beta2);

    registry.def("has_epsilon", &ConstLambModelUpdateConf::has_epsilon);
    registry.def("epsilon", &ConstLambModelUpdateConf::epsilon);
  }
  {
    pybind11::class_<::oneflow::cfg::LambModelUpdateConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>> registry(m, "LambModelUpdateConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LambModelUpdateConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LambModelUpdateConf::*)(const ConstLambModelUpdateConf&))&::oneflow::cfg::LambModelUpdateConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LambModelUpdateConf::*)(const ::oneflow::cfg::LambModelUpdateConf&))&::oneflow::cfg::LambModelUpdateConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LambModelUpdateConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LambModelUpdateConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LambModelUpdateConf::DebugString);



    registry.def("has_beta1", &::oneflow::cfg::LambModelUpdateConf::has_beta1);
    registry.def("clear_beta1", &::oneflow::cfg::LambModelUpdateConf::clear_beta1);
    registry.def("beta1", &::oneflow::cfg::LambModelUpdateConf::beta1);
    registry.def("set_beta1", &::oneflow::cfg::LambModelUpdateConf::set_beta1);

    registry.def("has_beta2", &::oneflow::cfg::LambModelUpdateConf::has_beta2);
    registry.def("clear_beta2", &::oneflow::cfg::LambModelUpdateConf::clear_beta2);
    registry.def("beta2", &::oneflow::cfg::LambModelUpdateConf::beta2);
    registry.def("set_beta2", &::oneflow::cfg::LambModelUpdateConf::set_beta2);

    registry.def("has_epsilon", &::oneflow::cfg::LambModelUpdateConf::has_epsilon);
    registry.def("clear_epsilon", &::oneflow::cfg::LambModelUpdateConf::clear_epsilon);
    registry.def("epsilon", &::oneflow::cfg::LambModelUpdateConf::epsilon);
    registry.def("set_epsilon", &::oneflow::cfg::LambModelUpdateConf::set_epsilon);
  }
  {
    pybind11::class_<ConstClipByGlobalNormConf, ::oneflow::cfg::Message, std::shared_ptr<ConstClipByGlobalNormConf>> registry(m, "ConstClipByGlobalNormConf");
    registry.def("__id__", &::oneflow::cfg::ClipByGlobalNormConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstClipByGlobalNormConf::DebugString);
    registry.def("__repr__", &ConstClipByGlobalNormConf::DebugString);

    registry.def("has_clip_norm", &ConstClipByGlobalNormConf::has_clip_norm);
    registry.def("clip_norm", &ConstClipByGlobalNormConf::clip_norm);

    registry.def("has_global_norm", &ConstClipByGlobalNormConf::has_global_norm);
    registry.def("global_norm", &ConstClipByGlobalNormConf::global_norm);
  }
  {
    pybind11::class_<::oneflow::cfg::ClipByGlobalNormConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>> registry(m, "ClipByGlobalNormConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ClipByGlobalNormConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ClipByGlobalNormConf::*)(const ConstClipByGlobalNormConf&))&::oneflow::cfg::ClipByGlobalNormConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ClipByGlobalNormConf::*)(const ::oneflow::cfg::ClipByGlobalNormConf&))&::oneflow::cfg::ClipByGlobalNormConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ClipByGlobalNormConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ClipByGlobalNormConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ClipByGlobalNormConf::DebugString);



    registry.def("has_clip_norm", &::oneflow::cfg::ClipByGlobalNormConf::has_clip_norm);
    registry.def("clear_clip_norm", &::oneflow::cfg::ClipByGlobalNormConf::clear_clip_norm);
    registry.def("clip_norm", &::oneflow::cfg::ClipByGlobalNormConf::clip_norm);
    registry.def("set_clip_norm", &::oneflow::cfg::ClipByGlobalNormConf::set_clip_norm);

    registry.def("has_global_norm", &::oneflow::cfg::ClipByGlobalNormConf::has_global_norm);
    registry.def("clear_global_norm", &::oneflow::cfg::ClipByGlobalNormConf::clear_global_norm);
    registry.def("global_norm", &::oneflow::cfg::ClipByGlobalNormConf::global_norm);
    registry.def("set_global_norm", &::oneflow::cfg::ClipByGlobalNormConf::set_global_norm);
  }
  {
    pybind11::class_<ConstClipConf, ::oneflow::cfg::Message, std::shared_ptr<ConstClipConf>> registry(m, "ConstClipConf");
    registry.def("__id__", &::oneflow::cfg::ClipConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstClipConf::DebugString);
    registry.def("__repr__", &ConstClipConf::DebugString);

    registry.def("has_clip_by_global_norm", &ConstClipConf::has_clip_by_global_norm);
    registry.def("clip_by_global_norm", &ConstClipConf::shared_const_clip_by_global_norm);
    registry.def("type_case",  &ConstClipConf::type_case);
    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ClipConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kClipByGlobalNorm", [](const pybind11::object&){ return ::oneflow::cfg::ClipConf::kClipByGlobalNorm; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::ClipConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ClipConf>> registry(m, "ClipConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ClipConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ClipConf::*)(const ConstClipConf&))&::oneflow::cfg::ClipConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ClipConf::*)(const ::oneflow::cfg::ClipConf&))&::oneflow::cfg::ClipConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ClipConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ClipConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ClipConf::DebugString);

    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ClipConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kClipByGlobalNorm", [](const pybind11::object&){ return ::oneflow::cfg::ClipConf::kClipByGlobalNorm; })
        ;


    registry.def("has_clip_by_global_norm", &::oneflow::cfg::ClipConf::has_clip_by_global_norm);
    registry.def("clear_clip_by_global_norm", &::oneflow::cfg::ClipConf::clear_clip_by_global_norm);
    registry.def_property_readonly_static("kClipByGlobalNorm",
        [](const pybind11::object&){ return ::oneflow::cfg::ClipConf::kClipByGlobalNorm; });
    registry.def("clip_by_global_norm", &::oneflow::cfg::ClipConf::clip_by_global_norm);
    registry.def("mutable_clip_by_global_norm", &::oneflow::cfg::ClipConf::shared_mutable_clip_by_global_norm);
    pybind11::enum_<::oneflow::cfg::ClipConf::TypeCase>(registry, "TypeCase")
        .value("TYPE_NOT_SET", ::oneflow::cfg::ClipConf::TYPE_NOT_SET)
        .value("kClipByGlobalNorm", ::oneflow::cfg::ClipConf::kClipByGlobalNorm)
        ;
    registry.def("type_case",  &::oneflow::cfg::ClipConf::type_case);
  }
  {
    pybind11::class_<ConstWeightDecayFilterPatternSet, ::oneflow::cfg::Message, std::shared_ptr<ConstWeightDecayFilterPatternSet>> registry(m, "ConstWeightDecayFilterPatternSet");
    registry.def("__id__", &::oneflow::cfg::WeightDecayFilterPatternSet::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstWeightDecayFilterPatternSet::DebugString);
    registry.def("__repr__", &ConstWeightDecayFilterPatternSet::DebugString);

    registry.def("pattern_size", &ConstWeightDecayFilterPatternSet::pattern_size);
    registry.def("pattern", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (ConstWeightDecayFilterPatternSet::*)() const)&ConstWeightDecayFilterPatternSet::shared_const_pattern);
    registry.def("pattern", (const ::std::string& (ConstWeightDecayFilterPatternSet::*)(::std::size_t) const)&ConstWeightDecayFilterPatternSet::pattern);
  }
  {
    pybind11::class_<::oneflow::cfg::WeightDecayFilterPatternSet, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>> registry(m, "WeightDecayFilterPatternSet");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::WeightDecayFilterPatternSet::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::WeightDecayFilterPatternSet::*)(const ConstWeightDecayFilterPatternSet&))&::oneflow::cfg::WeightDecayFilterPatternSet::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::WeightDecayFilterPatternSet::*)(const ::oneflow::cfg::WeightDecayFilterPatternSet&))&::oneflow::cfg::WeightDecayFilterPatternSet::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::WeightDecayFilterPatternSet::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::WeightDecayFilterPatternSet::DebugString);
    registry.def("__repr__", &::oneflow::cfg::WeightDecayFilterPatternSet::DebugString);



    registry.def("pattern_size", &::oneflow::cfg::WeightDecayFilterPatternSet::pattern_size);
    registry.def("clear_pattern", &::oneflow::cfg::WeightDecayFilterPatternSet::clear_pattern);
    registry.def("mutable_pattern", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::WeightDecayFilterPatternSet::*)())&::oneflow::cfg::WeightDecayFilterPatternSet::shared_mutable_pattern);
    registry.def("pattern", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::WeightDecayFilterPatternSet::*)() const)&::oneflow::cfg::WeightDecayFilterPatternSet::shared_const_pattern);
    registry.def("pattern", (const ::std::string& (::oneflow::cfg::WeightDecayFilterPatternSet::*)(::std::size_t) const)&::oneflow::cfg::WeightDecayFilterPatternSet::pattern);
    registry.def("add_pattern", &::oneflow::cfg::WeightDecayFilterPatternSet::add_pattern);
    registry.def("set_pattern", &::oneflow::cfg::WeightDecayFilterPatternSet::set_pattern);
  }
  {
    pybind11::class_<ConstWeightDecayConf, ::oneflow::cfg::Message, std::shared_ptr<ConstWeightDecayConf>> registry(m, "ConstWeightDecayConf");
    registry.def("__id__", &::oneflow::cfg::WeightDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstWeightDecayConf::DebugString);
    registry.def("__repr__", &ConstWeightDecayConf::DebugString);

    registry.def("has_weight_decay_rate", &ConstWeightDecayConf::has_weight_decay_rate);
    registry.def("weight_decay_rate", &ConstWeightDecayConf::weight_decay_rate);

    registry.def("has_includes", &ConstWeightDecayConf::has_includes);
    registry.def("includes", &ConstWeightDecayConf::shared_const_includes);

    registry.def("has_excludes", &ConstWeightDecayConf::has_excludes);
    registry.def("excludes", &ConstWeightDecayConf::shared_const_excludes);
    registry.def("weight_decay_filter_type_case",  &ConstWeightDecayConf::weight_decay_filter_type_case);
    registry.def_property_readonly_static("WEIGHT_DECAY_FILTER_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::WEIGHT_DECAY_FILTER_TYPE_NOT_SET; })
        .def_property_readonly_static("kIncludes", [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kIncludes; })
        .def_property_readonly_static("kExcludes", [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kExcludes; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::WeightDecayConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::WeightDecayConf>> registry(m, "WeightDecayConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::WeightDecayConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::WeightDecayConf::*)(const ConstWeightDecayConf&))&::oneflow::cfg::WeightDecayConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::WeightDecayConf::*)(const ::oneflow::cfg::WeightDecayConf&))&::oneflow::cfg::WeightDecayConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::WeightDecayConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::WeightDecayConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::WeightDecayConf::DebugString);

    registry.def_property_readonly_static("WEIGHT_DECAY_FILTER_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::WEIGHT_DECAY_FILTER_TYPE_NOT_SET; })
        .def_property_readonly_static("kIncludes", [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kIncludes; })
        .def_property_readonly_static("kExcludes", [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kExcludes; })
        ;


    registry.def("has_weight_decay_rate", &::oneflow::cfg::WeightDecayConf::has_weight_decay_rate);
    registry.def("clear_weight_decay_rate", &::oneflow::cfg::WeightDecayConf::clear_weight_decay_rate);
    registry.def("weight_decay_rate", &::oneflow::cfg::WeightDecayConf::weight_decay_rate);
    registry.def("set_weight_decay_rate", &::oneflow::cfg::WeightDecayConf::set_weight_decay_rate);

    registry.def("has_includes", &::oneflow::cfg::WeightDecayConf::has_includes);
    registry.def("clear_includes", &::oneflow::cfg::WeightDecayConf::clear_includes);
    registry.def_property_readonly_static("kIncludes",
        [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kIncludes; });
    registry.def("includes", &::oneflow::cfg::WeightDecayConf::includes);
    registry.def("mutable_includes", &::oneflow::cfg::WeightDecayConf::shared_mutable_includes);

    registry.def("has_excludes", &::oneflow::cfg::WeightDecayConf::has_excludes);
    registry.def("clear_excludes", &::oneflow::cfg::WeightDecayConf::clear_excludes);
    registry.def_property_readonly_static("kExcludes",
        [](const pybind11::object&){ return ::oneflow::cfg::WeightDecayConf::kExcludes; });
    registry.def("excludes", &::oneflow::cfg::WeightDecayConf::excludes);
    registry.def("mutable_excludes", &::oneflow::cfg::WeightDecayConf::shared_mutable_excludes);
    pybind11::enum_<::oneflow::cfg::WeightDecayConf::WeightDecayFilterTypeCase>(registry, "WeightDecayFilterTypeCase")
        .value("WEIGHT_DECAY_FILTER_TYPE_NOT_SET", ::oneflow::cfg::WeightDecayConf::WEIGHT_DECAY_FILTER_TYPE_NOT_SET)
        .value("kIncludes", ::oneflow::cfg::WeightDecayConf::kIncludes)
        .value("kExcludes", ::oneflow::cfg::WeightDecayConf::kExcludes)
        ;
    registry.def("weight_decay_filter_type_case",  &::oneflow::cfg::WeightDecayConf::weight_decay_filter_type_case);
  }
  {
    pybind11::class_<ConstOptimizerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstOptimizerConf>> registry(m, "ConstOptimizerConf");
    registry.def("__id__", &::oneflow::cfg::OptimizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOptimizerConf::DebugString);
    registry.def("__repr__", &ConstOptimizerConf::DebugString);

    registry.def("variable_op_names_size", &ConstOptimizerConf::variable_op_names_size);
    registry.def("variable_op_names", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (ConstOptimizerConf::*)() const)&ConstOptimizerConf::shared_const_variable_op_names);
    registry.def("variable_op_names", (const ::std::string& (ConstOptimizerConf::*)(::std::size_t) const)&ConstOptimizerConf::variable_op_names);

    registry.def("has_base_learning_rate", &ConstOptimizerConf::has_base_learning_rate);
    registry.def("base_learning_rate", &ConstOptimizerConf::base_learning_rate);

    registry.def("has_warmup_conf", &ConstOptimizerConf::has_warmup_conf);
    registry.def("warmup_conf", &ConstOptimizerConf::shared_const_warmup_conf);

    registry.def("has_learning_rate_decay", &ConstOptimizerConf::has_learning_rate_decay);
    registry.def("learning_rate_decay", &ConstOptimizerConf::shared_const_learning_rate_decay);

    registry.def("has_learning_rate_lbn", &ConstOptimizerConf::has_learning_rate_lbn);
    registry.def("learning_rate_lbn", &ConstOptimizerConf::learning_rate_lbn);

    registry.def("has_clip_conf", &ConstOptimizerConf::has_clip_conf);
    registry.def("clip_conf", &ConstOptimizerConf::shared_const_clip_conf);

    registry.def("has_weight_decay_conf", &ConstOptimizerConf::has_weight_decay_conf);
    registry.def("weight_decay_conf", &ConstOptimizerConf::shared_const_weight_decay_conf);

    registry.def("has_naive_conf", &ConstOptimizerConf::has_naive_conf);
    registry.def("naive_conf", &ConstOptimizerConf::shared_const_naive_conf);

    registry.def("has_momentum_conf", &ConstOptimizerConf::has_momentum_conf);
    registry.def("momentum_conf", &ConstOptimizerConf::shared_const_momentum_conf);

    registry.def("has_rmsprop_conf", &ConstOptimizerConf::has_rmsprop_conf);
    registry.def("rmsprop_conf", &ConstOptimizerConf::shared_const_rmsprop_conf);

    registry.def("has_lars_conf", &ConstOptimizerConf::has_lars_conf);
    registry.def("lars_conf", &ConstOptimizerConf::shared_const_lars_conf);

    registry.def("has_adam_conf", &ConstOptimizerConf::has_adam_conf);
    registry.def("adam_conf", &ConstOptimizerConf::shared_const_adam_conf);

    registry.def("has_lazy_adam_conf", &ConstOptimizerConf::has_lazy_adam_conf);
    registry.def("lazy_adam_conf", &ConstOptimizerConf::shared_const_lazy_adam_conf);

    registry.def("has_lamb_conf", &ConstOptimizerConf::has_lamb_conf);
    registry.def("lamb_conf", &ConstOptimizerConf::shared_const_lamb_conf);
    registry.def("normal_mdupdt_case",  &ConstOptimizerConf::normal_mdupdt_case);
    registry.def_property_readonly_static("NORMAL_MDUPDT_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::NORMAL_MDUPDT_NOT_SET; })
        .def_property_readonly_static("kNaiveConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kNaiveConf; })
        .def_property_readonly_static("kMomentumConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kMomentumConf; })
        .def_property_readonly_static("kRmspropConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kRmspropConf; })
        .def_property_readonly_static("kLarsConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLarsConf; })
        .def_property_readonly_static("kAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kAdamConf; })
        .def_property_readonly_static("kLazyAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLazyAdamConf; })
        .def_property_readonly_static("kLambConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLambConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::OptimizerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OptimizerConf>> registry(m, "OptimizerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OptimizerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptimizerConf::*)(const ConstOptimizerConf&))&::oneflow::cfg::OptimizerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptimizerConf::*)(const ::oneflow::cfg::OptimizerConf&))&::oneflow::cfg::OptimizerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OptimizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OptimizerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OptimizerConf::DebugString);

    registry.def_property_readonly_static("NORMAL_MDUPDT_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::NORMAL_MDUPDT_NOT_SET; })
        .def_property_readonly_static("kNaiveConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kNaiveConf; })
        .def_property_readonly_static("kMomentumConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kMomentumConf; })
        .def_property_readonly_static("kRmspropConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kRmspropConf; })
        .def_property_readonly_static("kLarsConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLarsConf; })
        .def_property_readonly_static("kAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kAdamConf; })
        .def_property_readonly_static("kLazyAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLazyAdamConf; })
        .def_property_readonly_static("kLambConf", [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLambConf; })
        ;


    registry.def("variable_op_names_size", &::oneflow::cfg::OptimizerConf::variable_op_names_size);
    registry.def("clear_variable_op_names", &::oneflow::cfg::OptimizerConf::clear_variable_op_names);
    registry.def("mutable_variable_op_names", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OptimizerConf::*)())&::oneflow::cfg::OptimizerConf::shared_mutable_variable_op_names);
    registry.def("variable_op_names", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OptimizerConf::*)() const)&::oneflow::cfg::OptimizerConf::shared_const_variable_op_names);
    registry.def("variable_op_names", (const ::std::string& (::oneflow::cfg::OptimizerConf::*)(::std::size_t) const)&::oneflow::cfg::OptimizerConf::variable_op_names);
    registry.def("add_variable_op_names", &::oneflow::cfg::OptimizerConf::add_variable_op_names);
    registry.def("set_variable_op_names", &::oneflow::cfg::OptimizerConf::set_variable_op_names);

    registry.def("has_base_learning_rate", &::oneflow::cfg::OptimizerConf::has_base_learning_rate);
    registry.def("clear_base_learning_rate", &::oneflow::cfg::OptimizerConf::clear_base_learning_rate);
    registry.def("base_learning_rate", &::oneflow::cfg::OptimizerConf::base_learning_rate);
    registry.def("set_base_learning_rate", &::oneflow::cfg::OptimizerConf::set_base_learning_rate);

    registry.def("has_warmup_conf", &::oneflow::cfg::OptimizerConf::has_warmup_conf);
    registry.def("clear_warmup_conf", &::oneflow::cfg::OptimizerConf::clear_warmup_conf);
    registry.def("warmup_conf", &::oneflow::cfg::OptimizerConf::shared_const_warmup_conf);
    registry.def("mutable_warmup_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_warmup_conf);

    registry.def("has_learning_rate_decay", &::oneflow::cfg::OptimizerConf::has_learning_rate_decay);
    registry.def("clear_learning_rate_decay", &::oneflow::cfg::OptimizerConf::clear_learning_rate_decay);
    registry.def("learning_rate_decay", &::oneflow::cfg::OptimizerConf::shared_const_learning_rate_decay);
    registry.def("mutable_learning_rate_decay", &::oneflow::cfg::OptimizerConf::shared_mutable_learning_rate_decay);

    registry.def("has_learning_rate_lbn", &::oneflow::cfg::OptimizerConf::has_learning_rate_lbn);
    registry.def("clear_learning_rate_lbn", &::oneflow::cfg::OptimizerConf::clear_learning_rate_lbn);
    registry.def("learning_rate_lbn", &::oneflow::cfg::OptimizerConf::learning_rate_lbn);
    registry.def("set_learning_rate_lbn", &::oneflow::cfg::OptimizerConf::set_learning_rate_lbn);

    registry.def("has_clip_conf", &::oneflow::cfg::OptimizerConf::has_clip_conf);
    registry.def("clear_clip_conf", &::oneflow::cfg::OptimizerConf::clear_clip_conf);
    registry.def("clip_conf", &::oneflow::cfg::OptimizerConf::shared_const_clip_conf);
    registry.def("mutable_clip_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_clip_conf);

    registry.def("has_weight_decay_conf", &::oneflow::cfg::OptimizerConf::has_weight_decay_conf);
    registry.def("clear_weight_decay_conf", &::oneflow::cfg::OptimizerConf::clear_weight_decay_conf);
    registry.def("weight_decay_conf", &::oneflow::cfg::OptimizerConf::shared_const_weight_decay_conf);
    registry.def("mutable_weight_decay_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_weight_decay_conf);

    registry.def("has_naive_conf", &::oneflow::cfg::OptimizerConf::has_naive_conf);
    registry.def("clear_naive_conf", &::oneflow::cfg::OptimizerConf::clear_naive_conf);
    registry.def_property_readonly_static("kNaiveConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kNaiveConf; });
    registry.def("naive_conf", &::oneflow::cfg::OptimizerConf::naive_conf);
    registry.def("mutable_naive_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_naive_conf);

    registry.def("has_momentum_conf", &::oneflow::cfg::OptimizerConf::has_momentum_conf);
    registry.def("clear_momentum_conf", &::oneflow::cfg::OptimizerConf::clear_momentum_conf);
    registry.def_property_readonly_static("kMomentumConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kMomentumConf; });
    registry.def("momentum_conf", &::oneflow::cfg::OptimizerConf::momentum_conf);
    registry.def("mutable_momentum_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_momentum_conf);

    registry.def("has_rmsprop_conf", &::oneflow::cfg::OptimizerConf::has_rmsprop_conf);
    registry.def("clear_rmsprop_conf", &::oneflow::cfg::OptimizerConf::clear_rmsprop_conf);
    registry.def_property_readonly_static("kRmspropConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kRmspropConf; });
    registry.def("rmsprop_conf", &::oneflow::cfg::OptimizerConf::rmsprop_conf);
    registry.def("mutable_rmsprop_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_rmsprop_conf);

    registry.def("has_lars_conf", &::oneflow::cfg::OptimizerConf::has_lars_conf);
    registry.def("clear_lars_conf", &::oneflow::cfg::OptimizerConf::clear_lars_conf);
    registry.def_property_readonly_static("kLarsConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLarsConf; });
    registry.def("lars_conf", &::oneflow::cfg::OptimizerConf::lars_conf);
    registry.def("mutable_lars_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_lars_conf);

    registry.def("has_adam_conf", &::oneflow::cfg::OptimizerConf::has_adam_conf);
    registry.def("clear_adam_conf", &::oneflow::cfg::OptimizerConf::clear_adam_conf);
    registry.def_property_readonly_static("kAdamConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kAdamConf; });
    registry.def("adam_conf", &::oneflow::cfg::OptimizerConf::adam_conf);
    registry.def("mutable_adam_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_adam_conf);

    registry.def("has_lazy_adam_conf", &::oneflow::cfg::OptimizerConf::has_lazy_adam_conf);
    registry.def("clear_lazy_adam_conf", &::oneflow::cfg::OptimizerConf::clear_lazy_adam_conf);
    registry.def_property_readonly_static("kLazyAdamConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLazyAdamConf; });
    registry.def("lazy_adam_conf", &::oneflow::cfg::OptimizerConf::lazy_adam_conf);
    registry.def("mutable_lazy_adam_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_lazy_adam_conf);

    registry.def("has_lamb_conf", &::oneflow::cfg::OptimizerConf::has_lamb_conf);
    registry.def("clear_lamb_conf", &::oneflow::cfg::OptimizerConf::clear_lamb_conf);
    registry.def_property_readonly_static("kLambConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OptimizerConf::kLambConf; });
    registry.def("lamb_conf", &::oneflow::cfg::OptimizerConf::lamb_conf);
    registry.def("mutable_lamb_conf", &::oneflow::cfg::OptimizerConf::shared_mutable_lamb_conf);
    pybind11::enum_<::oneflow::cfg::OptimizerConf::NormalMdupdtCase>(registry, "NormalMdupdtCase")
        .value("NORMAL_MDUPDT_NOT_SET", ::oneflow::cfg::OptimizerConf::NORMAL_MDUPDT_NOT_SET)
        .value("kNaiveConf", ::oneflow::cfg::OptimizerConf::kNaiveConf)
        .value("kMomentumConf", ::oneflow::cfg::OptimizerConf::kMomentumConf)
        .value("kRmspropConf", ::oneflow::cfg::OptimizerConf::kRmspropConf)
        .value("kLarsConf", ::oneflow::cfg::OptimizerConf::kLarsConf)
        .value("kAdamConf", ::oneflow::cfg::OptimizerConf::kAdamConf)
        .value("kLazyAdamConf", ::oneflow::cfg::OptimizerConf::kLazyAdamConf)
        .value("kLambConf", ::oneflow::cfg::OptimizerConf::kLambConf)
        ;
    registry.def("normal_mdupdt_case",  &::oneflow::cfg::OptimizerConf::normal_mdupdt_case);
  }
  {
    pybind11::class_<ConstNormalModelUpdateOpUserConf, ::oneflow::cfg::Message, std::shared_ptr<ConstNormalModelUpdateOpUserConf>> registry(m, "ConstNormalModelUpdateOpUserConf");
    registry.def("__id__", &::oneflow::cfg::NormalModelUpdateOpUserConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstNormalModelUpdateOpUserConf::DebugString);
    registry.def("__repr__", &ConstNormalModelUpdateOpUserConf::DebugString);

    registry.def("has_learning_rate_decay", &ConstNormalModelUpdateOpUserConf::has_learning_rate_decay);
    registry.def("learning_rate_decay", &ConstNormalModelUpdateOpUserConf::shared_const_learning_rate_decay);

    registry.def("has_warmup_conf", &ConstNormalModelUpdateOpUserConf::has_warmup_conf);
    registry.def("warmup_conf", &ConstNormalModelUpdateOpUserConf::shared_const_warmup_conf);

    registry.def("has_clip_conf", &ConstNormalModelUpdateOpUserConf::has_clip_conf);
    registry.def("clip_conf", &ConstNormalModelUpdateOpUserConf::shared_const_clip_conf);

    registry.def("has_weight_decay_conf", &ConstNormalModelUpdateOpUserConf::has_weight_decay_conf);
    registry.def("weight_decay_conf", &ConstNormalModelUpdateOpUserConf::shared_const_weight_decay_conf);

    registry.def("has_naive_conf", &ConstNormalModelUpdateOpUserConf::has_naive_conf);
    registry.def("naive_conf", &ConstNormalModelUpdateOpUserConf::shared_const_naive_conf);

    registry.def("has_momentum_conf", &ConstNormalModelUpdateOpUserConf::has_momentum_conf);
    registry.def("momentum_conf", &ConstNormalModelUpdateOpUserConf::shared_const_momentum_conf);

    registry.def("has_rmsprop_conf", &ConstNormalModelUpdateOpUserConf::has_rmsprop_conf);
    registry.def("rmsprop_conf", &ConstNormalModelUpdateOpUserConf::shared_const_rmsprop_conf);

    registry.def("has_lars_conf", &ConstNormalModelUpdateOpUserConf::has_lars_conf);
    registry.def("lars_conf", &ConstNormalModelUpdateOpUserConf::shared_const_lars_conf);

    registry.def("has_adam_conf", &ConstNormalModelUpdateOpUserConf::has_adam_conf);
    registry.def("adam_conf", &ConstNormalModelUpdateOpUserConf::shared_const_adam_conf);

    registry.def("has_lazy_adam_conf", &ConstNormalModelUpdateOpUserConf::has_lazy_adam_conf);
    registry.def("lazy_adam_conf", &ConstNormalModelUpdateOpUserConf::shared_const_lazy_adam_conf);

    registry.def("has_lamb_conf", &ConstNormalModelUpdateOpUserConf::has_lamb_conf);
    registry.def("lamb_conf", &ConstNormalModelUpdateOpUserConf::shared_const_lamb_conf);
    registry.def("normal_mdupdt_case",  &ConstNormalModelUpdateOpUserConf::normal_mdupdt_case);
    registry.def_property_readonly_static("NORMAL_MDUPDT_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::NORMAL_MDUPDT_NOT_SET; })
        .def_property_readonly_static("kNaiveConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kNaiveConf; })
        .def_property_readonly_static("kMomentumConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kMomentumConf; })
        .def_property_readonly_static("kRmspropConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kRmspropConf; })
        .def_property_readonly_static("kLarsConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLarsConf; })
        .def_property_readonly_static("kAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kAdamConf; })
        .def_property_readonly_static("kLazyAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLazyAdamConf; })
        .def_property_readonly_static("kLambConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLambConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::NormalModelUpdateOpUserConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::NormalModelUpdateOpUserConf>> registry(m, "NormalModelUpdateOpUserConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::NormalModelUpdateOpUserConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::NormalModelUpdateOpUserConf::*)(const ConstNormalModelUpdateOpUserConf&))&::oneflow::cfg::NormalModelUpdateOpUserConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::NormalModelUpdateOpUserConf::*)(const ::oneflow::cfg::NormalModelUpdateOpUserConf&))&::oneflow::cfg::NormalModelUpdateOpUserConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::NormalModelUpdateOpUserConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::NormalModelUpdateOpUserConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::NormalModelUpdateOpUserConf::DebugString);

    registry.def_property_readonly_static("NORMAL_MDUPDT_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::NORMAL_MDUPDT_NOT_SET; })
        .def_property_readonly_static("kNaiveConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kNaiveConf; })
        .def_property_readonly_static("kMomentumConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kMomentumConf; })
        .def_property_readonly_static("kRmspropConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kRmspropConf; })
        .def_property_readonly_static("kLarsConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLarsConf; })
        .def_property_readonly_static("kAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kAdamConf; })
        .def_property_readonly_static("kLazyAdamConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLazyAdamConf; })
        .def_property_readonly_static("kLambConf", [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLambConf; })
        ;


    registry.def("has_learning_rate_decay", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_learning_rate_decay);
    registry.def("clear_learning_rate_decay", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_learning_rate_decay);
    registry.def("learning_rate_decay", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_const_learning_rate_decay);
    registry.def("mutable_learning_rate_decay", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_learning_rate_decay);

    registry.def("has_warmup_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_warmup_conf);
    registry.def("clear_warmup_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_warmup_conf);
    registry.def("warmup_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_const_warmup_conf);
    registry.def("mutable_warmup_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_warmup_conf);

    registry.def("has_clip_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_clip_conf);
    registry.def("clear_clip_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_clip_conf);
    registry.def("clip_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_const_clip_conf);
    registry.def("mutable_clip_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_clip_conf);

    registry.def("has_weight_decay_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_weight_decay_conf);
    registry.def("clear_weight_decay_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_weight_decay_conf);
    registry.def("weight_decay_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_const_weight_decay_conf);
    registry.def("mutable_weight_decay_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_weight_decay_conf);

    registry.def("has_naive_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_naive_conf);
    registry.def("clear_naive_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_naive_conf);
    registry.def_property_readonly_static("kNaiveConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kNaiveConf; });
    registry.def("naive_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::naive_conf);
    registry.def("mutable_naive_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_naive_conf);

    registry.def("has_momentum_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_momentum_conf);
    registry.def("clear_momentum_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_momentum_conf);
    registry.def_property_readonly_static("kMomentumConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kMomentumConf; });
    registry.def("momentum_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::momentum_conf);
    registry.def("mutable_momentum_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_momentum_conf);

    registry.def("has_rmsprop_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_rmsprop_conf);
    registry.def("clear_rmsprop_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_rmsprop_conf);
    registry.def_property_readonly_static("kRmspropConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kRmspropConf; });
    registry.def("rmsprop_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::rmsprop_conf);
    registry.def("mutable_rmsprop_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_rmsprop_conf);

    registry.def("has_lars_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_lars_conf);
    registry.def("clear_lars_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_lars_conf);
    registry.def_property_readonly_static("kLarsConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLarsConf; });
    registry.def("lars_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::lars_conf);
    registry.def("mutable_lars_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_lars_conf);

    registry.def("has_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_adam_conf);
    registry.def("clear_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_adam_conf);
    registry.def_property_readonly_static("kAdamConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kAdamConf; });
    registry.def("adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::adam_conf);
    registry.def("mutable_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_adam_conf);

    registry.def("has_lazy_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_lazy_adam_conf);
    registry.def("clear_lazy_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_lazy_adam_conf);
    registry.def_property_readonly_static("kLazyAdamConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLazyAdamConf; });
    registry.def("lazy_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::lazy_adam_conf);
    registry.def("mutable_lazy_adam_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_lazy_adam_conf);

    registry.def("has_lamb_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::has_lamb_conf);
    registry.def("clear_lamb_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::clear_lamb_conf);
    registry.def_property_readonly_static("kLambConf",
        [](const pybind11::object&){ return ::oneflow::cfg::NormalModelUpdateOpUserConf::kLambConf; });
    registry.def("lamb_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::lamb_conf);
    registry.def("mutable_lamb_conf", &::oneflow::cfg::NormalModelUpdateOpUserConf::shared_mutable_lamb_conf);
    pybind11::enum_<::oneflow::cfg::NormalModelUpdateOpUserConf::NormalMdupdtCase>(registry, "NormalMdupdtCase")
        .value("NORMAL_MDUPDT_NOT_SET", ::oneflow::cfg::NormalModelUpdateOpUserConf::NORMAL_MDUPDT_NOT_SET)
        .value("kNaiveConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kNaiveConf)
        .value("kMomentumConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kMomentumConf)
        .value("kRmspropConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kRmspropConf)
        .value("kLarsConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kLarsConf)
        .value("kAdamConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kAdamConf)
        .value("kLazyAdamConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kLazyAdamConf)
        .value("kLambConf", ::oneflow::cfg::NormalModelUpdateOpUserConf::kLambConf)
        ;
    registry.def("normal_mdupdt_case",  &::oneflow::cfg::NormalModelUpdateOpUserConf::normal_mdupdt_case);
  }
  {
    pybind11::class_<ConstDynamicLossScalePolicy, ::oneflow::cfg::Message, std::shared_ptr<ConstDynamicLossScalePolicy>> registry(m, "ConstDynamicLossScalePolicy");
    registry.def("__id__", &::oneflow::cfg::DynamicLossScalePolicy::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDynamicLossScalePolicy::DebugString);
    registry.def("__repr__", &ConstDynamicLossScalePolicy::DebugString);

    registry.def("has_initial_loss_scale", &ConstDynamicLossScalePolicy::has_initial_loss_scale);
    registry.def("initial_loss_scale", &ConstDynamicLossScalePolicy::initial_loss_scale);

    registry.def("has_increment_period", &ConstDynamicLossScalePolicy::has_increment_period);
    registry.def("increment_period", &ConstDynamicLossScalePolicy::increment_period);

    registry.def("has_multiplier", &ConstDynamicLossScalePolicy::has_multiplier);
    registry.def("multiplier", &ConstDynamicLossScalePolicy::multiplier);
  }
  {
    pybind11::class_<::oneflow::cfg::DynamicLossScalePolicy, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>> registry(m, "DynamicLossScalePolicy");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DynamicLossScalePolicy::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicLossScalePolicy::*)(const ConstDynamicLossScalePolicy&))&::oneflow::cfg::DynamicLossScalePolicy::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicLossScalePolicy::*)(const ::oneflow::cfg::DynamicLossScalePolicy&))&::oneflow::cfg::DynamicLossScalePolicy::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DynamicLossScalePolicy::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DynamicLossScalePolicy::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DynamicLossScalePolicy::DebugString);



    registry.def("has_initial_loss_scale", &::oneflow::cfg::DynamicLossScalePolicy::has_initial_loss_scale);
    registry.def("clear_initial_loss_scale", &::oneflow::cfg::DynamicLossScalePolicy::clear_initial_loss_scale);
    registry.def("initial_loss_scale", &::oneflow::cfg::DynamicLossScalePolicy::initial_loss_scale);
    registry.def("set_initial_loss_scale", &::oneflow::cfg::DynamicLossScalePolicy::set_initial_loss_scale);

    registry.def("has_increment_period", &::oneflow::cfg::DynamicLossScalePolicy::has_increment_period);
    registry.def("clear_increment_period", &::oneflow::cfg::DynamicLossScalePolicy::clear_increment_period);
    registry.def("increment_period", &::oneflow::cfg::DynamicLossScalePolicy::increment_period);
    registry.def("set_increment_period", &::oneflow::cfg::DynamicLossScalePolicy::set_increment_period);

    registry.def("has_multiplier", &::oneflow::cfg::DynamicLossScalePolicy::has_multiplier);
    registry.def("clear_multiplier", &::oneflow::cfg::DynamicLossScalePolicy::clear_multiplier);
    registry.def("multiplier", &::oneflow::cfg::DynamicLossScalePolicy::multiplier);
    registry.def("set_multiplier", &::oneflow::cfg::DynamicLossScalePolicy::set_multiplier);
  }
  {
    pybind11::class_<ConstTrainConf, ::oneflow::cfg::Message, std::shared_ptr<ConstTrainConf>> registry(m, "ConstTrainConf");
    registry.def("__id__", &::oneflow::cfg::TrainConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstTrainConf::DebugString);
    registry.def("__repr__", &ConstTrainConf::DebugString);

    registry.def("optimizer_conf_size", &ConstTrainConf::optimizer_conf_size);
    registry.def("optimizer_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> (ConstTrainConf::*)() const)&ConstTrainConf::shared_const_optimizer_conf);
    registry.def("optimizer_conf", (::std::shared_ptr<ConstOptimizerConf> (ConstTrainConf::*)(::std::size_t) const)&ConstTrainConf::shared_const_optimizer_conf);

    registry.def("loss_lbn_size", &ConstTrainConf::loss_lbn_size);
    registry.def("loss_lbn", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (ConstTrainConf::*)() const)&ConstTrainConf::shared_const_loss_lbn);
    registry.def("loss_lbn", (const ::std::string& (ConstTrainConf::*)(::std::size_t) const)&ConstTrainConf::loss_lbn);

    registry.def("has_train_step_lbn", &ConstTrainConf::has_train_step_lbn);
    registry.def("train_step_lbn", &ConstTrainConf::train_step_lbn);

    registry.def("has_loss_scale_factor", &ConstTrainConf::has_loss_scale_factor);
    registry.def("loss_scale_factor", &ConstTrainConf::loss_scale_factor);

    registry.def("has_dynamic_loss_scale_policy", &ConstTrainConf::has_dynamic_loss_scale_policy);
    registry.def("dynamic_loss_scale_policy", &ConstTrainConf::shared_const_dynamic_loss_scale_policy);

    registry.def("has_model_update_conf", &ConstTrainConf::has_model_update_conf);
    registry.def("model_update_conf", &ConstTrainConf::shared_const_model_update_conf);

    registry.def("has_primary_lr", &ConstTrainConf::has_primary_lr);
    registry.def("primary_lr", &ConstTrainConf::primary_lr);

    registry.def("has_secondary_lr", &ConstTrainConf::has_secondary_lr);
    registry.def("secondary_lr", &ConstTrainConf::secondary_lr);

    registry.def("has_primary_lr_lbn", &ConstTrainConf::has_primary_lr_lbn);
    registry.def("primary_lr_lbn", &ConstTrainConf::primary_lr_lbn);

    registry.def("has_secondary_lr_lbn", &ConstTrainConf::has_secondary_lr_lbn);
    registry.def("secondary_lr_lbn", &ConstTrainConf::secondary_lr_lbn);
    registry.def("loss_scale_policy_case",  &ConstTrainConf::loss_scale_policy_case);
    registry.def_property_readonly_static("LOSS_SCALE_POLICY_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::LOSS_SCALE_POLICY_NOT_SET; })
        .def_property_readonly_static("kLossScaleFactor", [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kLossScaleFactor; })
        .def_property_readonly_static("kDynamicLossScalePolicy", [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kDynamicLossScalePolicy; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::TrainConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::TrainConf>> registry(m, "TrainConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::TrainConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::TrainConf::*)(const ConstTrainConf&))&::oneflow::cfg::TrainConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::TrainConf::*)(const ::oneflow::cfg::TrainConf&))&::oneflow::cfg::TrainConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::TrainConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::TrainConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::TrainConf::DebugString);

    registry.def_property_readonly_static("LOSS_SCALE_POLICY_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::LOSS_SCALE_POLICY_NOT_SET; })
        .def_property_readonly_static("kLossScaleFactor", [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kLossScaleFactor; })
        .def_property_readonly_static("kDynamicLossScalePolicy", [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kDynamicLossScalePolicy; })
        ;


    registry.def("optimizer_conf_size", &::oneflow::cfg::TrainConf::optimizer_conf_size);
    registry.def("clear_optimizer_conf", &::oneflow::cfg::TrainConf::clear_optimizer_conf);
    registry.def("mutable_optimizer_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> (::oneflow::cfg::TrainConf::*)())&::oneflow::cfg::TrainConf::shared_mutable_optimizer_conf);
    registry.def("optimizer_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> (::oneflow::cfg::TrainConf::*)() const)&::oneflow::cfg::TrainConf::shared_const_optimizer_conf);
    registry.def("optimizer_conf", (::std::shared_ptr<ConstOptimizerConf> (::oneflow::cfg::TrainConf::*)(::std::size_t) const)&::oneflow::cfg::TrainConf::shared_const_optimizer_conf);
    registry.def("mutable_optimizer_conf", (::std::shared_ptr<::oneflow::cfg::OptimizerConf> (::oneflow::cfg::TrainConf::*)(::std::size_t))&::oneflow::cfg::TrainConf::shared_mutable_optimizer_conf);

    registry.def("loss_lbn_size", &::oneflow::cfg::TrainConf::loss_lbn_size);
    registry.def("clear_loss_lbn", &::oneflow::cfg::TrainConf::clear_loss_lbn);
    registry.def("mutable_loss_lbn", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TrainConf::*)())&::oneflow::cfg::TrainConf::shared_mutable_loss_lbn);
    registry.def("loss_lbn", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TrainConf::*)() const)&::oneflow::cfg::TrainConf::shared_const_loss_lbn);
    registry.def("loss_lbn", (const ::std::string& (::oneflow::cfg::TrainConf::*)(::std::size_t) const)&::oneflow::cfg::TrainConf::loss_lbn);
    registry.def("add_loss_lbn", &::oneflow::cfg::TrainConf::add_loss_lbn);
    registry.def("set_loss_lbn", &::oneflow::cfg::TrainConf::set_loss_lbn);

    registry.def("has_train_step_lbn", &::oneflow::cfg::TrainConf::has_train_step_lbn);
    registry.def("clear_train_step_lbn", &::oneflow::cfg::TrainConf::clear_train_step_lbn);
    registry.def("train_step_lbn", &::oneflow::cfg::TrainConf::train_step_lbn);
    registry.def("set_train_step_lbn", &::oneflow::cfg::TrainConf::set_train_step_lbn);

    registry.def("has_loss_scale_factor", &::oneflow::cfg::TrainConf::has_loss_scale_factor);
    registry.def("clear_loss_scale_factor", &::oneflow::cfg::TrainConf::clear_loss_scale_factor);
    registry.def_property_readonly_static("kLossScaleFactor",
        [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kLossScaleFactor; });
    registry.def("loss_scale_factor", &::oneflow::cfg::TrainConf::loss_scale_factor);
    registry.def("set_loss_scale_factor", &::oneflow::cfg::TrainConf::set_loss_scale_factor);

    registry.def("has_dynamic_loss_scale_policy", &::oneflow::cfg::TrainConf::has_dynamic_loss_scale_policy);
    registry.def("clear_dynamic_loss_scale_policy", &::oneflow::cfg::TrainConf::clear_dynamic_loss_scale_policy);
    registry.def_property_readonly_static("kDynamicLossScalePolicy",
        [](const pybind11::object&){ return ::oneflow::cfg::TrainConf::kDynamicLossScalePolicy; });
    registry.def("dynamic_loss_scale_policy", &::oneflow::cfg::TrainConf::dynamic_loss_scale_policy);
    registry.def("mutable_dynamic_loss_scale_policy", &::oneflow::cfg::TrainConf::shared_mutable_dynamic_loss_scale_policy);

    registry.def("has_model_update_conf", &::oneflow::cfg::TrainConf::has_model_update_conf);
    registry.def("clear_model_update_conf", &::oneflow::cfg::TrainConf::clear_model_update_conf);
    registry.def("model_update_conf", &::oneflow::cfg::TrainConf::shared_const_model_update_conf);
    registry.def("mutable_model_update_conf", &::oneflow::cfg::TrainConf::shared_mutable_model_update_conf);

    registry.def("has_primary_lr", &::oneflow::cfg::TrainConf::has_primary_lr);
    registry.def("clear_primary_lr", &::oneflow::cfg::TrainConf::clear_primary_lr);
    registry.def("primary_lr", &::oneflow::cfg::TrainConf::primary_lr);
    registry.def("set_primary_lr", &::oneflow::cfg::TrainConf::set_primary_lr);

    registry.def("has_secondary_lr", &::oneflow::cfg::TrainConf::has_secondary_lr);
    registry.def("clear_secondary_lr", &::oneflow::cfg::TrainConf::clear_secondary_lr);
    registry.def("secondary_lr", &::oneflow::cfg::TrainConf::secondary_lr);
    registry.def("set_secondary_lr", &::oneflow::cfg::TrainConf::set_secondary_lr);

    registry.def("has_primary_lr_lbn", &::oneflow::cfg::TrainConf::has_primary_lr_lbn);
    registry.def("clear_primary_lr_lbn", &::oneflow::cfg::TrainConf::clear_primary_lr_lbn);
    registry.def("primary_lr_lbn", &::oneflow::cfg::TrainConf::primary_lr_lbn);
    registry.def("set_primary_lr_lbn", &::oneflow::cfg::TrainConf::set_primary_lr_lbn);

    registry.def("has_secondary_lr_lbn", &::oneflow::cfg::TrainConf::has_secondary_lr_lbn);
    registry.def("clear_secondary_lr_lbn", &::oneflow::cfg::TrainConf::clear_secondary_lr_lbn);
    registry.def("secondary_lr_lbn", &::oneflow::cfg::TrainConf::secondary_lr_lbn);
    registry.def("set_secondary_lr_lbn", &::oneflow::cfg::TrainConf::set_secondary_lr_lbn);
    pybind11::enum_<::oneflow::cfg::TrainConf::LossScalePolicyCase>(registry, "LossScalePolicyCase")
        .value("LOSS_SCALE_POLICY_NOT_SET", ::oneflow::cfg::TrainConf::LOSS_SCALE_POLICY_NOT_SET)
        .value("kLossScaleFactor", ::oneflow::cfg::TrainConf::kLossScaleFactor)
        .value("kDynamicLossScalePolicy", ::oneflow::cfg::TrainConf::kDynamicLossScalePolicy)
        ;
    registry.def("loss_scale_policy_case",  &::oneflow::cfg::TrainConf::loss_scale_policy_case);
  }
  {
    pybind11::class_<ConstPredictConf, ::oneflow::cfg::Message, std::shared_ptr<ConstPredictConf>> registry(m, "ConstPredictConf");
    registry.def("__id__", &::oneflow::cfg::PredictConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPredictConf::DebugString);
    registry.def("__repr__", &ConstPredictConf::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::PredictConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PredictConf>> registry(m, "PredictConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PredictConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PredictConf::*)(const ConstPredictConf&))&::oneflow::cfg::PredictConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PredictConf::*)(const ::oneflow::cfg::PredictConf&))&::oneflow::cfg::PredictConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PredictConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PredictConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PredictConf::DebugString);


  }
  {
    pybind11::class_<ConstMemoryAllocationAlgorithmConf, ::oneflow::cfg::Message, std::shared_ptr<ConstMemoryAllocationAlgorithmConf>> registry(m, "ConstMemoryAllocationAlgorithmConf");
    registry.def("__id__", &::oneflow::cfg::MemoryAllocationAlgorithmConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstMemoryAllocationAlgorithmConf::DebugString);
    registry.def("__repr__", &ConstMemoryAllocationAlgorithmConf::DebugString);

    registry.def("has_use_mem_size_first_algo", &ConstMemoryAllocationAlgorithmConf::has_use_mem_size_first_algo);
    registry.def("use_mem_size_first_algo", &ConstMemoryAllocationAlgorithmConf::use_mem_size_first_algo);

    registry.def("has_use_mutual_exclusion_first_algo", &ConstMemoryAllocationAlgorithmConf::has_use_mutual_exclusion_first_algo);
    registry.def("use_mutual_exclusion_first_algo", &ConstMemoryAllocationAlgorithmConf::use_mutual_exclusion_first_algo);

    registry.def("has_use_time_line_algo", &ConstMemoryAllocationAlgorithmConf::has_use_time_line_algo);
    registry.def("use_time_line_algo", &ConstMemoryAllocationAlgorithmConf::use_time_line_algo);
  }
  {
    pybind11::class_<::oneflow::cfg::MemoryAllocationAlgorithmConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::MemoryAllocationAlgorithmConf>> registry(m, "MemoryAllocationAlgorithmConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::MemoryAllocationAlgorithmConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::MemoryAllocationAlgorithmConf::*)(const ConstMemoryAllocationAlgorithmConf&))&::oneflow::cfg::MemoryAllocationAlgorithmConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::MemoryAllocationAlgorithmConf::*)(const ::oneflow::cfg::MemoryAllocationAlgorithmConf&))&::oneflow::cfg::MemoryAllocationAlgorithmConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::MemoryAllocationAlgorithmConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::MemoryAllocationAlgorithmConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::MemoryAllocationAlgorithmConf::DebugString);



    registry.def("has_use_mem_size_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::has_use_mem_size_first_algo);
    registry.def("clear_use_mem_size_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::clear_use_mem_size_first_algo);
    registry.def("use_mem_size_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::use_mem_size_first_algo);
    registry.def("set_use_mem_size_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::set_use_mem_size_first_algo);

    registry.def("has_use_mutual_exclusion_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::has_use_mutual_exclusion_first_algo);
    registry.def("clear_use_mutual_exclusion_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::clear_use_mutual_exclusion_first_algo);
    registry.def("use_mutual_exclusion_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::use_mutual_exclusion_first_algo);
    registry.def("set_use_mutual_exclusion_first_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::set_use_mutual_exclusion_first_algo);

    registry.def("has_use_time_line_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::has_use_time_line_algo);
    registry.def("clear_use_time_line_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::clear_use_time_line_algo);
    registry.def("use_time_line_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::use_time_line_algo);
    registry.def("set_use_time_line_algo", &::oneflow::cfg::MemoryAllocationAlgorithmConf::set_use_time_line_algo);
  }
  {
    pybind11::class_<ConstXrtConfig_XlaConfig, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtConfig_XlaConfig>> registry(m, "ConstXrtConfig_XlaConfig");
    registry.def("__id__", &::oneflow::cfg::XrtConfig_XlaConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtConfig_XlaConfig::DebugString);
    registry.def("__repr__", &ConstXrtConfig_XlaConfig::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtConfig_XlaConfig, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtConfig_XlaConfig>> registry(m, "XrtConfig_XlaConfig");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtConfig_XlaConfig::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig_XlaConfig::*)(const ConstXrtConfig_XlaConfig&))&::oneflow::cfg::XrtConfig_XlaConfig::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig_XlaConfig::*)(const ::oneflow::cfg::XrtConfig_XlaConfig&))&::oneflow::cfg::XrtConfig_XlaConfig::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtConfig_XlaConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtConfig_XlaConfig::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtConfig_XlaConfig::DebugString);


  }
  {
    pybind11::class_<ConstXrtConfig_TensorRTConfig, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtConfig_TensorRTConfig>> registry(m, "ConstXrtConfig_TensorRTConfig");
    registry.def("__id__", &::oneflow::cfg::XrtConfig_TensorRTConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtConfig_TensorRTConfig::DebugString);
    registry.def("__repr__", &ConstXrtConfig_TensorRTConfig::DebugString);

    registry.def("has_use_fp16", &ConstXrtConfig_TensorRTConfig::has_use_fp16);
    registry.def("use_fp16", &ConstXrtConfig_TensorRTConfig::use_fp16);

    registry.def("has_use_int8", &ConstXrtConfig_TensorRTConfig::has_use_int8);
    registry.def("use_int8", &ConstXrtConfig_TensorRTConfig::use_int8);

    registry.def("has_int8_calibration", &ConstXrtConfig_TensorRTConfig::has_int8_calibration);
    registry.def("int8_calibration", &ConstXrtConfig_TensorRTConfig::int8_calibration);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtConfig_TensorRTConfig, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtConfig_TensorRTConfig>> registry(m, "XrtConfig_TensorRTConfig");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtConfig_TensorRTConfig::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig_TensorRTConfig::*)(const ConstXrtConfig_TensorRTConfig&))&::oneflow::cfg::XrtConfig_TensorRTConfig::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig_TensorRTConfig::*)(const ::oneflow::cfg::XrtConfig_TensorRTConfig&))&::oneflow::cfg::XrtConfig_TensorRTConfig::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtConfig_TensorRTConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtConfig_TensorRTConfig::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtConfig_TensorRTConfig::DebugString);



    registry.def("has_use_fp16", &::oneflow::cfg::XrtConfig_TensorRTConfig::has_use_fp16);
    registry.def("clear_use_fp16", &::oneflow::cfg::XrtConfig_TensorRTConfig::clear_use_fp16);
    registry.def("use_fp16", &::oneflow::cfg::XrtConfig_TensorRTConfig::use_fp16);
    registry.def("set_use_fp16", &::oneflow::cfg::XrtConfig_TensorRTConfig::set_use_fp16);

    registry.def("has_use_int8", &::oneflow::cfg::XrtConfig_TensorRTConfig::has_use_int8);
    registry.def("clear_use_int8", &::oneflow::cfg::XrtConfig_TensorRTConfig::clear_use_int8);
    registry.def("use_int8", &::oneflow::cfg::XrtConfig_TensorRTConfig::use_int8);
    registry.def("set_use_int8", &::oneflow::cfg::XrtConfig_TensorRTConfig::set_use_int8);

    registry.def("has_int8_calibration", &::oneflow::cfg::XrtConfig_TensorRTConfig::has_int8_calibration);
    registry.def("clear_int8_calibration", &::oneflow::cfg::XrtConfig_TensorRTConfig::clear_int8_calibration);
    registry.def("int8_calibration", &::oneflow::cfg::XrtConfig_TensorRTConfig::int8_calibration);
    registry.def("set_int8_calibration", &::oneflow::cfg::XrtConfig_TensorRTConfig::set_int8_calibration);
  }
  {
    pybind11::class_<ConstXrtConfig, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtConfig>> registry(m, "ConstXrtConfig");
    registry.def("__id__", &::oneflow::cfg::XrtConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtConfig::DebugString);
    registry.def("__repr__", &ConstXrtConfig::DebugString);

    registry.def("has_use_xla_jit", &ConstXrtConfig::has_use_xla_jit);
    registry.def("use_xla_jit", &ConstXrtConfig::use_xla_jit);

    registry.def("has_use_tensorrt", &ConstXrtConfig::has_use_tensorrt);
    registry.def("use_tensorrt", &ConstXrtConfig::use_tensorrt);

    registry.def("has_xla_config", &ConstXrtConfig::has_xla_config);
    registry.def("xla_config", &ConstXrtConfig::shared_const_xla_config);

    registry.def("has_tensorrt_config", &ConstXrtConfig::has_tensorrt_config);
    registry.def("tensorrt_config", &ConstXrtConfig::shared_const_tensorrt_config);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtConfig, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtConfig>> registry(m, "XrtConfig");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtConfig::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig::*)(const ConstXrtConfig&))&::oneflow::cfg::XrtConfig::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtConfig::*)(const ::oneflow::cfg::XrtConfig&))&::oneflow::cfg::XrtConfig::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtConfig::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtConfig::DebugString);



    registry.def("has_use_xla_jit", &::oneflow::cfg::XrtConfig::has_use_xla_jit);
    registry.def("clear_use_xla_jit", &::oneflow::cfg::XrtConfig::clear_use_xla_jit);
    registry.def("use_xla_jit", &::oneflow::cfg::XrtConfig::use_xla_jit);
    registry.def("set_use_xla_jit", &::oneflow::cfg::XrtConfig::set_use_xla_jit);

    registry.def("has_use_tensorrt", &::oneflow::cfg::XrtConfig::has_use_tensorrt);
    registry.def("clear_use_tensorrt", &::oneflow::cfg::XrtConfig::clear_use_tensorrt);
    registry.def("use_tensorrt", &::oneflow::cfg::XrtConfig::use_tensorrt);
    registry.def("set_use_tensorrt", &::oneflow::cfg::XrtConfig::set_use_tensorrt);

    registry.def("has_xla_config", &::oneflow::cfg::XrtConfig::has_xla_config);
    registry.def("clear_xla_config", &::oneflow::cfg::XrtConfig::clear_xla_config);
    registry.def("xla_config", &::oneflow::cfg::XrtConfig::shared_const_xla_config);
    registry.def("mutable_xla_config", &::oneflow::cfg::XrtConfig::shared_mutable_xla_config);

    registry.def("has_tensorrt_config", &::oneflow::cfg::XrtConfig::has_tensorrt_config);
    registry.def("clear_tensorrt_config", &::oneflow::cfg::XrtConfig::clear_tensorrt_config);
    registry.def("tensorrt_config", &::oneflow::cfg::XrtConfig::shared_const_tensorrt_config);
    registry.def("mutable_tensorrt_config", &::oneflow::cfg::XrtConfig::shared_mutable_tensorrt_config);
  }
  {
    pybind11::class_<ConstQatConfig, ::oneflow::cfg::Message, std::shared_ptr<ConstQatConfig>> registry(m, "ConstQatConfig");
    registry.def("__id__", &::oneflow::cfg::QatConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstQatConfig::DebugString);
    registry.def("__repr__", &ConstQatConfig::DebugString);

    registry.def("has_per_channel_weight_quantization", &ConstQatConfig::has_per_channel_weight_quantization);
    registry.def("per_channel_weight_quantization", &ConstQatConfig::per_channel_weight_quantization);

    registry.def("has_symmetric", &ConstQatConfig::has_symmetric);
    registry.def("symmetric", &ConstQatConfig::symmetric);

    registry.def("has_moving_min_max_momentum", &ConstQatConfig::has_moving_min_max_momentum);
    registry.def("moving_min_max_momentum", &ConstQatConfig::moving_min_max_momentum);

    registry.def("has_moving_min_max_stop_update_after_iters", &ConstQatConfig::has_moving_min_max_stop_update_after_iters);
    registry.def("moving_min_max_stop_update_after_iters", &ConstQatConfig::moving_min_max_stop_update_after_iters);

    registry.def("has_target_backend", &ConstQatConfig::has_target_backend);
    registry.def("target_backend", &ConstQatConfig::target_backend);
  }
  {
    pybind11::class_<::oneflow::cfg::QatConfig, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::QatConfig>> registry(m, "QatConfig");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::QatConfig::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::QatConfig::*)(const ConstQatConfig&))&::oneflow::cfg::QatConfig::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::QatConfig::*)(const ::oneflow::cfg::QatConfig&))&::oneflow::cfg::QatConfig::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::QatConfig::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::QatConfig::DebugString);
    registry.def("__repr__", &::oneflow::cfg::QatConfig::DebugString);



    registry.def("has_per_channel_weight_quantization", &::oneflow::cfg::QatConfig::has_per_channel_weight_quantization);
    registry.def("clear_per_channel_weight_quantization", &::oneflow::cfg::QatConfig::clear_per_channel_weight_quantization);
    registry.def("per_channel_weight_quantization", &::oneflow::cfg::QatConfig::per_channel_weight_quantization);
    registry.def("set_per_channel_weight_quantization", &::oneflow::cfg::QatConfig::set_per_channel_weight_quantization);

    registry.def("has_symmetric", &::oneflow::cfg::QatConfig::has_symmetric);
    registry.def("clear_symmetric", &::oneflow::cfg::QatConfig::clear_symmetric);
    registry.def("symmetric", &::oneflow::cfg::QatConfig::symmetric);
    registry.def("set_symmetric", &::oneflow::cfg::QatConfig::set_symmetric);

    registry.def("has_moving_min_max_momentum", &::oneflow::cfg::QatConfig::has_moving_min_max_momentum);
    registry.def("clear_moving_min_max_momentum", &::oneflow::cfg::QatConfig::clear_moving_min_max_momentum);
    registry.def("moving_min_max_momentum", &::oneflow::cfg::QatConfig::moving_min_max_momentum);
    registry.def("set_moving_min_max_momentum", &::oneflow::cfg::QatConfig::set_moving_min_max_momentum);

    registry.def("has_moving_min_max_stop_update_after_iters", &::oneflow::cfg::QatConfig::has_moving_min_max_stop_update_after_iters);
    registry.def("clear_moving_min_max_stop_update_after_iters", &::oneflow::cfg::QatConfig::clear_moving_min_max_stop_update_after_iters);
    registry.def("moving_min_max_stop_update_after_iters", &::oneflow::cfg::QatConfig::moving_min_max_stop_update_after_iters);
    registry.def("set_moving_min_max_stop_update_after_iters", &::oneflow::cfg::QatConfig::set_moving_min_max_stop_update_after_iters);

    registry.def("has_target_backend", &::oneflow::cfg::QatConfig::has_target_backend);
    registry.def("clear_target_backend", &::oneflow::cfg::QatConfig::clear_target_backend);
    registry.def("target_backend", &::oneflow::cfg::QatConfig::target_backend);
    registry.def("set_target_backend", &::oneflow::cfg::QatConfig::set_target_backend);
  }
  {
    pybind11::class_<ConstIndexedSlicesOptimizerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstIndexedSlicesOptimizerConf>> registry(m, "ConstIndexedSlicesOptimizerConf");
    registry.def("__id__", &::oneflow::cfg::IndexedSlicesOptimizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstIndexedSlicesOptimizerConf::DebugString);
    registry.def("__repr__", &ConstIndexedSlicesOptimizerConf::DebugString);

    registry.def("has_enable", &ConstIndexedSlicesOptimizerConf::has_enable);
    registry.def("enable", &ConstIndexedSlicesOptimizerConf::enable);

    registry.def("has_include_op_names", &ConstIndexedSlicesOptimizerConf::has_include_op_names);
    registry.def("include_op_names", &ConstIndexedSlicesOptimizerConf::shared_const_include_op_names);
  }
  {
    pybind11::class_<::oneflow::cfg::IndexedSlicesOptimizerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::IndexedSlicesOptimizerConf>> registry(m, "IndexedSlicesOptimizerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::IndexedSlicesOptimizerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::IndexedSlicesOptimizerConf::*)(const ConstIndexedSlicesOptimizerConf&))&::oneflow::cfg::IndexedSlicesOptimizerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::IndexedSlicesOptimizerConf::*)(const ::oneflow::cfg::IndexedSlicesOptimizerConf&))&::oneflow::cfg::IndexedSlicesOptimizerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::IndexedSlicesOptimizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::IndexedSlicesOptimizerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::IndexedSlicesOptimizerConf::DebugString);



    registry.def("has_enable", &::oneflow::cfg::IndexedSlicesOptimizerConf::has_enable);
    registry.def("clear_enable", &::oneflow::cfg::IndexedSlicesOptimizerConf::clear_enable);
    registry.def("enable", &::oneflow::cfg::IndexedSlicesOptimizerConf::enable);
    registry.def("set_enable", &::oneflow::cfg::IndexedSlicesOptimizerConf::set_enable);

    registry.def("has_include_op_names", &::oneflow::cfg::IndexedSlicesOptimizerConf::has_include_op_names);
    registry.def("clear_include_op_names", &::oneflow::cfg::IndexedSlicesOptimizerConf::clear_include_op_names);
    registry.def("include_op_names", &::oneflow::cfg::IndexedSlicesOptimizerConf::shared_const_include_op_names);
    registry.def("mutable_include_op_names", &::oneflow::cfg::IndexedSlicesOptimizerConf::shared_mutable_include_op_names);
  }
  {
    pybind11::class_<ConstParallelBlobConf, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelBlobConf>> registry(m, "ConstParallelBlobConf");
    registry.def("__id__", &::oneflow::cfg::ParallelBlobConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelBlobConf::DebugString);
    registry.def("__repr__", &ConstParallelBlobConf::DebugString);

    registry.def("has_logical_blob_desc_conf", &ConstParallelBlobConf::has_logical_blob_desc_conf);
    registry.def("logical_blob_desc_conf", &ConstParallelBlobConf::shared_const_logical_blob_desc_conf);

    registry.def("has_parallel_conf", &ConstParallelBlobConf::has_parallel_conf);
    registry.def("parallel_conf", &ConstParallelBlobConf::shared_const_parallel_conf);

    registry.def("has_parallel_distribution", &ConstParallelBlobConf::has_parallel_distribution);
    registry.def("parallel_distribution", &ConstParallelBlobConf::shared_const_parallel_distribution);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelBlobConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelBlobConf>> registry(m, "ParallelBlobConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelBlobConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelBlobConf::*)(const ConstParallelBlobConf&))&::oneflow::cfg::ParallelBlobConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelBlobConf::*)(const ::oneflow::cfg::ParallelBlobConf&))&::oneflow::cfg::ParallelBlobConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelBlobConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelBlobConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelBlobConf::DebugString);



    registry.def("has_logical_blob_desc_conf", &::oneflow::cfg::ParallelBlobConf::has_logical_blob_desc_conf);
    registry.def("clear_logical_blob_desc_conf", &::oneflow::cfg::ParallelBlobConf::clear_logical_blob_desc_conf);
    registry.def("logical_blob_desc_conf", &::oneflow::cfg::ParallelBlobConf::shared_const_logical_blob_desc_conf);
    registry.def("mutable_logical_blob_desc_conf", &::oneflow::cfg::ParallelBlobConf::shared_mutable_logical_blob_desc_conf);

    registry.def("has_parallel_conf", &::oneflow::cfg::ParallelBlobConf::has_parallel_conf);
    registry.def("clear_parallel_conf", &::oneflow::cfg::ParallelBlobConf::clear_parallel_conf);
    registry.def("parallel_conf", &::oneflow::cfg::ParallelBlobConf::shared_const_parallel_conf);
    registry.def("mutable_parallel_conf", &::oneflow::cfg::ParallelBlobConf::shared_mutable_parallel_conf);

    registry.def("has_parallel_distribution", &::oneflow::cfg::ParallelBlobConf::has_parallel_distribution);
    registry.def("clear_parallel_distribution", &::oneflow::cfg::ParallelBlobConf::clear_parallel_distribution);
    registry.def("parallel_distribution", &::oneflow::cfg::ParallelBlobConf::shared_const_parallel_distribution);
    registry.def("mutable_parallel_distribution", &::oneflow::cfg::ParallelBlobConf::shared_mutable_parallel_distribution);
  }
  {
    pybind11::class_<ConstJobInputDef, ::oneflow::cfg::Message, std::shared_ptr<ConstJobInputDef>> registry(m, "ConstJobInputDef");
    registry.def("__id__", &::oneflow::cfg::JobInputDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstJobInputDef::DebugString);
    registry.def("__repr__", &ConstJobInputDef::DebugString);

    registry.def("has_lbi", &ConstJobInputDef::has_lbi);
    registry.def("lbi", &ConstJobInputDef::shared_const_lbi);

    registry.def("has_blob_conf", &ConstJobInputDef::has_blob_conf);
    registry.def("blob_conf", &ConstJobInputDef::shared_const_blob_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::JobInputDef, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::JobInputDef>> registry(m, "JobInputDef");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::JobInputDef::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobInputDef::*)(const ConstJobInputDef&))&::oneflow::cfg::JobInputDef::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobInputDef::*)(const ::oneflow::cfg::JobInputDef&))&::oneflow::cfg::JobInputDef::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::JobInputDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::JobInputDef::DebugString);
    registry.def("__repr__", &::oneflow::cfg::JobInputDef::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::JobInputDef::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::JobInputDef::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::JobInputDef::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::JobInputDef::shared_mutable_lbi);

    registry.def("has_blob_conf", &::oneflow::cfg::JobInputDef::has_blob_conf);
    registry.def("clear_blob_conf", &::oneflow::cfg::JobInputDef::clear_blob_conf);
    registry.def("blob_conf", &::oneflow::cfg::JobInputDef::shared_const_blob_conf);
    registry.def("mutable_blob_conf", &::oneflow::cfg::JobInputDef::shared_mutable_blob_conf);
  }
  {
    pybind11::class_<ConstJobOutputDef, ::oneflow::cfg::Message, std::shared_ptr<ConstJobOutputDef>> registry(m, "ConstJobOutputDef");
    registry.def("__id__", &::oneflow::cfg::JobOutputDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstJobOutputDef::DebugString);
    registry.def("__repr__", &ConstJobOutputDef::DebugString);

    registry.def("has_lbi", &ConstJobOutputDef::has_lbi);
    registry.def("lbi", &ConstJobOutputDef::shared_const_lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::JobOutputDef, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::JobOutputDef>> registry(m, "JobOutputDef");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::JobOutputDef::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobOutputDef::*)(const ConstJobOutputDef&))&::oneflow::cfg::JobOutputDef::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobOutputDef::*)(const ::oneflow::cfg::JobOutputDef&))&::oneflow::cfg::JobOutputDef::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::JobOutputDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::JobOutputDef::DebugString);
    registry.def("__repr__", &::oneflow::cfg::JobOutputDef::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::JobOutputDef::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::JobOutputDef::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::JobOutputDef::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::JobOutputDef::shared_mutable_lbi);
  }
  {
    pybind11::class_<ConstJobSignatureDef, ::oneflow::cfg::Message, std::shared_ptr<ConstJobSignatureDef>> registry(m, "ConstJobSignatureDef");
    registry.def("__id__", &::oneflow::cfg::JobSignatureDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstJobSignatureDef::DebugString);
    registry.def("__repr__", &ConstJobSignatureDef::DebugString);

    registry.def("inputs_size", &ConstJobSignatureDef::inputs_size);
    registry.def("inputs", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> (ConstJobSignatureDef::*)() const)&ConstJobSignatureDef::shared_const_inputs);

    registry.def("inputs", (::std::shared_ptr<ConstJobInputDef> (ConstJobSignatureDef::*)(const ::std::string&) const)&ConstJobSignatureDef::shared_const_inputs);

    registry.def("outputs_size", &ConstJobSignatureDef::outputs_size);
    registry.def("outputs", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> (ConstJobSignatureDef::*)() const)&ConstJobSignatureDef::shared_const_outputs);

    registry.def("outputs", (::std::shared_ptr<ConstJobOutputDef> (ConstJobSignatureDef::*)(const ::std::string&) const)&ConstJobSignatureDef::shared_const_outputs);
  }
  {
    pybind11::class_<::oneflow::cfg::JobSignatureDef, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::JobSignatureDef>> registry(m, "JobSignatureDef");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::JobSignatureDef::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobSignatureDef::*)(const ConstJobSignatureDef&))&::oneflow::cfg::JobSignatureDef::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobSignatureDef::*)(const ::oneflow::cfg::JobSignatureDef&))&::oneflow::cfg::JobSignatureDef::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::JobSignatureDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::JobSignatureDef::DebugString);
    registry.def("__repr__", &::oneflow::cfg::JobSignatureDef::DebugString);



    registry.def("inputs_size", &::oneflow::cfg::JobSignatureDef::inputs_size);
    registry.def("inputs", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> (::oneflow::cfg::JobSignatureDef::*)() const)&::oneflow::cfg::JobSignatureDef::shared_const_inputs);
    registry.def("clear_inputs", &::oneflow::cfg::JobSignatureDef::clear_inputs);
    registry.def("mutable_inputs", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> (::oneflow::cfg::JobSignatureDef::*)())&::oneflow::cfg::JobSignatureDef::shared_mutable_inputs);
    registry.def("inputs", (::std::shared_ptr<ConstJobInputDef> (::oneflow::cfg::JobSignatureDef::*)(const ::std::string&) const)&::oneflow::cfg::JobSignatureDef::shared_const_inputs);

    registry.def("outputs_size", &::oneflow::cfg::JobSignatureDef::outputs_size);
    registry.def("outputs", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> (::oneflow::cfg::JobSignatureDef::*)() const)&::oneflow::cfg::JobSignatureDef::shared_const_outputs);
    registry.def("clear_outputs", &::oneflow::cfg::JobSignatureDef::clear_outputs);
    registry.def("mutable_outputs", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> (::oneflow::cfg::JobSignatureDef::*)())&::oneflow::cfg::JobSignatureDef::shared_mutable_outputs);
    registry.def("outputs", (::std::shared_ptr<ConstJobOutputDef> (::oneflow::cfg::JobSignatureDef::*)(const ::std::string&) const)&::oneflow::cfg::JobSignatureDef::shared_const_outputs);
  }
  {
    pybind11::class_<ConstJobConfigProto, ::oneflow::cfg::Message, std::shared_ptr<ConstJobConfigProto>> registry(m, "ConstJobConfigProto");
    registry.def("__id__", &::oneflow::cfg::JobConfigProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstJobConfigProto::DebugString);
    registry.def("__repr__", &ConstJobConfigProto::DebugString);

    registry.def("has_job_name", &ConstJobConfigProto::has_job_name);
    registry.def("job_name", &ConstJobConfigProto::job_name);

    registry.def("has_train_conf", &ConstJobConfigProto::has_train_conf);
    registry.def("train_conf", &ConstJobConfigProto::shared_const_train_conf);

    registry.def("has_predict_conf", &ConstJobConfigProto::has_predict_conf);
    registry.def("predict_conf", &ConstJobConfigProto::shared_const_predict_conf);

    registry.def("has_default_data_type", &ConstJobConfigProto::has_default_data_type);
    registry.def("default_data_type", &ConstJobConfigProto::default_data_type);

    registry.def("has_default_initializer_conf", &ConstJobConfigProto::has_default_initializer_conf);
    registry.def("default_initializer_conf", &ConstJobConfigProto::shared_const_default_initializer_conf);

    registry.def("has_default_initialize_with_snapshot_path", &ConstJobConfigProto::has_default_initialize_with_snapshot_path);
    registry.def("default_initialize_with_snapshot_path", &ConstJobConfigProto::default_initialize_with_snapshot_path);

    registry.def("has_memory_allocation_algorithm_conf", &ConstJobConfigProto::has_memory_allocation_algorithm_conf);
    registry.def("memory_allocation_algorithm_conf", &ConstJobConfigProto::shared_const_memory_allocation_algorithm_conf);

    registry.def("has_xrt_config", &ConstJobConfigProto::has_xrt_config);
    registry.def("xrt_config", &ConstJobConfigProto::shared_const_xrt_config);

    registry.def("has_indexed_slices_optimizer_conf", &ConstJobConfigProto::has_indexed_slices_optimizer_conf);
    registry.def("indexed_slices_optimizer_conf", &ConstJobConfigProto::shared_const_indexed_slices_optimizer_conf);

    registry.def("has_enable_fuse_model_update_ops", &ConstJobConfigProto::has_enable_fuse_model_update_ops);
    registry.def("enable_fuse_model_update_ops", &ConstJobConfigProto::enable_fuse_model_update_ops);

    registry.def("has_enable_gradients_stats_aggregation", &ConstJobConfigProto::has_enable_gradients_stats_aggregation);
    registry.def("enable_gradients_stats_aggregation", &ConstJobConfigProto::enable_gradients_stats_aggregation);

    registry.def("has_optimizer_placement_optimization_mode", &ConstJobConfigProto::has_optimizer_placement_optimization_mode);
    registry.def("optimizer_placement_optimization_mode", &ConstJobConfigProto::optimizer_placement_optimization_mode);

    registry.def("has_optimizer_placement_optimization_threshold", &ConstJobConfigProto::has_optimizer_placement_optimization_threshold);
    registry.def("optimizer_placement_optimization_threshold", &ConstJobConfigProto::optimizer_placement_optimization_threshold);

    registry.def("has_qat_config", &ConstJobConfigProto::has_qat_config);
    registry.def("qat_config", &ConstJobConfigProto::shared_const_qat_config);

    registry.def("has_enable_cudnn", &ConstJobConfigProto::has_enable_cudnn);
    registry.def("enable_cudnn", &ConstJobConfigProto::enable_cudnn);

    registry.def("has_cudnn_buf_limit_mbyte", &ConstJobConfigProto::has_cudnn_buf_limit_mbyte);
    registry.def("cudnn_buf_limit_mbyte", &ConstJobConfigProto::cudnn_buf_limit_mbyte);

    registry.def("has_cudnn_conv_force_fwd_algo", &ConstJobConfigProto::has_cudnn_conv_force_fwd_algo);
    registry.def("cudnn_conv_force_fwd_algo", &ConstJobConfigProto::cudnn_conv_force_fwd_algo);

    registry.def("has_cudnn_conv_force_bwd_data_algo", &ConstJobConfigProto::has_cudnn_conv_force_bwd_data_algo);
    registry.def("cudnn_conv_force_bwd_data_algo", &ConstJobConfigProto::cudnn_conv_force_bwd_data_algo);

    registry.def("has_cudnn_conv_force_bwd_filter_algo", &ConstJobConfigProto::has_cudnn_conv_force_bwd_filter_algo);
    registry.def("cudnn_conv_force_bwd_filter_algo", &ConstJobConfigProto::cudnn_conv_force_bwd_filter_algo);

    registry.def("has_cudnn_conv_heuristic_search_algo", &ConstJobConfigProto::has_cudnn_conv_heuristic_search_algo);
    registry.def("cudnn_conv_heuristic_search_algo", &ConstJobConfigProto::cudnn_conv_heuristic_search_algo);

    registry.def("has_cudnn_conv_use_deterministic_algo_only", &ConstJobConfigProto::has_cudnn_conv_use_deterministic_algo_only);
    registry.def("cudnn_conv_use_deterministic_algo_only", &ConstJobConfigProto::cudnn_conv_use_deterministic_algo_only);

    registry.def("has_enable_cudnn_fused_normalization_add_relu", &ConstJobConfigProto::has_enable_cudnn_fused_normalization_add_relu);
    registry.def("enable_cudnn_fused_normalization_add_relu", &ConstJobConfigProto::enable_cudnn_fused_normalization_add_relu);

    registry.def("has_enable_fuse_add_to_output", &ConstJobConfigProto::has_enable_fuse_add_to_output);
    registry.def("enable_fuse_add_to_output", &ConstJobConfigProto::enable_fuse_add_to_output);

    registry.def("has_enable_fuse_cast_scale", &ConstJobConfigProto::has_enable_fuse_cast_scale);
    registry.def("enable_fuse_cast_scale", &ConstJobConfigProto::enable_fuse_cast_scale);

    registry.def("has_num_gradient_accumulation_steps", &ConstJobConfigProto::has_num_gradient_accumulation_steps);
    registry.def("num_gradient_accumulation_steps", &ConstJobConfigProto::num_gradient_accumulation_steps);

    registry.def("has_enable_reuse_mem", &ConstJobConfigProto::has_enable_reuse_mem);
    registry.def("enable_reuse_mem", &ConstJobConfigProto::enable_reuse_mem);

    registry.def("has_enable_inplace", &ConstJobConfigProto::has_enable_inplace);
    registry.def("enable_inplace", &ConstJobConfigProto::enable_inplace);

    registry.def("has_enable_inplace_in_reduce_struct", &ConstJobConfigProto::has_enable_inplace_in_reduce_struct);
    registry.def("enable_inplace_in_reduce_struct", &ConstJobConfigProto::enable_inplace_in_reduce_struct);

    registry.def("has_do_parallel_cast_before_widening_type_cast", &ConstJobConfigProto::has_do_parallel_cast_before_widening_type_cast);
    registry.def("do_parallel_cast_before_widening_type_cast", &ConstJobConfigProto::do_parallel_cast_before_widening_type_cast);

    registry.def("has_prune_parallel_cast_ops", &ConstJobConfigProto::has_prune_parallel_cast_ops);
    registry.def("prune_parallel_cast_ops", &ConstJobConfigProto::prune_parallel_cast_ops);

    registry.def("has_prune_cast_to_static_shape_ops", &ConstJobConfigProto::has_prune_cast_to_static_shape_ops);
    registry.def("prune_cast_to_static_shape_ops", &ConstJobConfigProto::prune_cast_to_static_shape_ops);

    registry.def("has_prune_amp_white_identity_ops", &ConstJobConfigProto::has_prune_amp_white_identity_ops);
    registry.def("prune_amp_white_identity_ops", &ConstJobConfigProto::prune_amp_white_identity_ops);

    registry.def("has_cudnn_conv_enable_pseudo_half", &ConstJobConfigProto::has_cudnn_conv_enable_pseudo_half);
    registry.def("cudnn_conv_enable_pseudo_half", &ConstJobConfigProto::cudnn_conv_enable_pseudo_half);

    registry.def("has_enable_auto_mixed_precision", &ConstJobConfigProto::has_enable_auto_mixed_precision);
    registry.def("enable_auto_mixed_precision", &ConstJobConfigProto::enable_auto_mixed_precision);

    registry.def("has_enable_quantization_aware_training", &ConstJobConfigProto::has_enable_quantization_aware_training);
    registry.def("enable_quantization_aware_training", &ConstJobConfigProto::enable_quantization_aware_training);

    registry.def("has_concurrency_width", &ConstJobConfigProto::has_concurrency_width);
    registry.def("concurrency_width", &ConstJobConfigProto::concurrency_width);

    registry.def("flag_name2flag_value_size", &ConstJobConfigProto::flag_name2flag_value_size);
    registry.def("flag_name2flag_value", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> (ConstJobConfigProto::*)() const)&ConstJobConfigProto::shared_const_flag_name2flag_value);

    registry.def("flag_name2flag_value", (::std::shared_ptr<ConstAttrValue> (ConstJobConfigProto::*)(const ::std::string&) const)&ConstJobConfigProto::shared_const_flag_name2flag_value);

    registry.def("has_logical_object_id", &ConstJobConfigProto::has_logical_object_id);
    registry.def("logical_object_id", &ConstJobConfigProto::logical_object_id);

    registry.def("has_signature", &ConstJobConfigProto::has_signature);
    registry.def("signature", &ConstJobConfigProto::shared_const_signature);
    registry.def("job_type_case",  &ConstJobConfigProto::job_type_case);
    registry.def_property_readonly_static("JOB_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::JOB_TYPE_NOT_SET; })
        .def_property_readonly_static("kTrainConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kTrainConf; })
        .def_property_readonly_static("kPredictConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kPredictConf; })
        ;
    registry.def("default_initialize_conf_case",  &ConstJobConfigProto::default_initialize_conf_case);
    registry.def_property_readonly_static("DEFAULT_INITIALIZE_CONF_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::DEFAULT_INITIALIZE_CONF_NOT_SET; })
        .def_property_readonly_static("kDefaultInitializerConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializerConf; })
        .def_property_readonly_static("kDefaultInitializeWithSnapshotPath", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializeWithSnapshotPath; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::JobConfigProto, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::JobConfigProto>> registry(m, "JobConfigProto");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::JobConfigProto::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobConfigProto::*)(const ConstJobConfigProto&))&::oneflow::cfg::JobConfigProto::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::JobConfigProto::*)(const ::oneflow::cfg::JobConfigProto&))&::oneflow::cfg::JobConfigProto::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::JobConfigProto::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::JobConfigProto::DebugString);
    registry.def("__repr__", &::oneflow::cfg::JobConfigProto::DebugString);

    registry.def_property_readonly_static("JOB_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::JOB_TYPE_NOT_SET; })
        .def_property_readonly_static("kTrainConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kTrainConf; })
        .def_property_readonly_static("kPredictConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kPredictConf; })
        ;
    registry.def_property_readonly_static("DEFAULT_INITIALIZE_CONF_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::DEFAULT_INITIALIZE_CONF_NOT_SET; })
        .def_property_readonly_static("kDefaultInitializerConf", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializerConf; })
        .def_property_readonly_static("kDefaultInitializeWithSnapshotPath", [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializeWithSnapshotPath; })
        ;


    registry.def("has_job_name", &::oneflow::cfg::JobConfigProto::has_job_name);
    registry.def("clear_job_name", &::oneflow::cfg::JobConfigProto::clear_job_name);
    registry.def("job_name", &::oneflow::cfg::JobConfigProto::job_name);
    registry.def("set_job_name", &::oneflow::cfg::JobConfigProto::set_job_name);

    registry.def("has_train_conf", &::oneflow::cfg::JobConfigProto::has_train_conf);
    registry.def("clear_train_conf", &::oneflow::cfg::JobConfigProto::clear_train_conf);
    registry.def_property_readonly_static("kTrainConf",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kTrainConf; });
    registry.def("train_conf", &::oneflow::cfg::JobConfigProto::train_conf);
    registry.def("mutable_train_conf", &::oneflow::cfg::JobConfigProto::shared_mutable_train_conf);

    registry.def("has_predict_conf", &::oneflow::cfg::JobConfigProto::has_predict_conf);
    registry.def("clear_predict_conf", &::oneflow::cfg::JobConfigProto::clear_predict_conf);
    registry.def_property_readonly_static("kPredictConf",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kPredictConf; });
    registry.def("predict_conf", &::oneflow::cfg::JobConfigProto::predict_conf);
    registry.def("mutable_predict_conf", &::oneflow::cfg::JobConfigProto::shared_mutable_predict_conf);

    registry.def("has_default_data_type", &::oneflow::cfg::JobConfigProto::has_default_data_type);
    registry.def("clear_default_data_type", &::oneflow::cfg::JobConfigProto::clear_default_data_type);
    registry.def("default_data_type", &::oneflow::cfg::JobConfigProto::default_data_type);
    registry.def("set_default_data_type", &::oneflow::cfg::JobConfigProto::set_default_data_type);

    registry.def("has_default_initializer_conf", &::oneflow::cfg::JobConfigProto::has_default_initializer_conf);
    registry.def("clear_default_initializer_conf", &::oneflow::cfg::JobConfigProto::clear_default_initializer_conf);
    registry.def_property_readonly_static("kDefaultInitializerConf",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializerConf; });
    registry.def("default_initializer_conf", &::oneflow::cfg::JobConfigProto::default_initializer_conf);
    registry.def("mutable_default_initializer_conf", &::oneflow::cfg::JobConfigProto::shared_mutable_default_initializer_conf);

    registry.def("has_default_initialize_with_snapshot_path", &::oneflow::cfg::JobConfigProto::has_default_initialize_with_snapshot_path);
    registry.def("clear_default_initialize_with_snapshot_path", &::oneflow::cfg::JobConfigProto::clear_default_initialize_with_snapshot_path);
    registry.def_property_readonly_static("kDefaultInitializeWithSnapshotPath",
        [](const pybind11::object&){ return ::oneflow::cfg::JobConfigProto::kDefaultInitializeWithSnapshotPath; });
    registry.def("default_initialize_with_snapshot_path", &::oneflow::cfg::JobConfigProto::default_initialize_with_snapshot_path);
    registry.def("set_default_initialize_with_snapshot_path", &::oneflow::cfg::JobConfigProto::set_default_initialize_with_snapshot_path);

    registry.def("has_memory_allocation_algorithm_conf", &::oneflow::cfg::JobConfigProto::has_memory_allocation_algorithm_conf);
    registry.def("clear_memory_allocation_algorithm_conf", &::oneflow::cfg::JobConfigProto::clear_memory_allocation_algorithm_conf);
    registry.def("memory_allocation_algorithm_conf", &::oneflow::cfg::JobConfigProto::shared_const_memory_allocation_algorithm_conf);
    registry.def("mutable_memory_allocation_algorithm_conf", &::oneflow::cfg::JobConfigProto::shared_mutable_memory_allocation_algorithm_conf);

    registry.def("has_xrt_config", &::oneflow::cfg::JobConfigProto::has_xrt_config);
    registry.def("clear_xrt_config", &::oneflow::cfg::JobConfigProto::clear_xrt_config);
    registry.def("xrt_config", &::oneflow::cfg::JobConfigProto::shared_const_xrt_config);
    registry.def("mutable_xrt_config", &::oneflow::cfg::JobConfigProto::shared_mutable_xrt_config);

    registry.def("has_indexed_slices_optimizer_conf", &::oneflow::cfg::JobConfigProto::has_indexed_slices_optimizer_conf);
    registry.def("clear_indexed_slices_optimizer_conf", &::oneflow::cfg::JobConfigProto::clear_indexed_slices_optimizer_conf);
    registry.def("indexed_slices_optimizer_conf", &::oneflow::cfg::JobConfigProto::shared_const_indexed_slices_optimizer_conf);
    registry.def("mutable_indexed_slices_optimizer_conf", &::oneflow::cfg::JobConfigProto::shared_mutable_indexed_slices_optimizer_conf);

    registry.def("has_enable_fuse_model_update_ops", &::oneflow::cfg::JobConfigProto::has_enable_fuse_model_update_ops);
    registry.def("clear_enable_fuse_model_update_ops", &::oneflow::cfg::JobConfigProto::clear_enable_fuse_model_update_ops);
    registry.def("enable_fuse_model_update_ops", &::oneflow::cfg::JobConfigProto::enable_fuse_model_update_ops);
    registry.def("set_enable_fuse_model_update_ops", &::oneflow::cfg::JobConfigProto::set_enable_fuse_model_update_ops);

    registry.def("has_enable_gradients_stats_aggregation", &::oneflow::cfg::JobConfigProto::has_enable_gradients_stats_aggregation);
    registry.def("clear_enable_gradients_stats_aggregation", &::oneflow::cfg::JobConfigProto::clear_enable_gradients_stats_aggregation);
    registry.def("enable_gradients_stats_aggregation", &::oneflow::cfg::JobConfigProto::enable_gradients_stats_aggregation);
    registry.def("set_enable_gradients_stats_aggregation", &::oneflow::cfg::JobConfigProto::set_enable_gradients_stats_aggregation);

    registry.def("has_optimizer_placement_optimization_mode", &::oneflow::cfg::JobConfigProto::has_optimizer_placement_optimization_mode);
    registry.def("clear_optimizer_placement_optimization_mode", &::oneflow::cfg::JobConfigProto::clear_optimizer_placement_optimization_mode);
    registry.def("optimizer_placement_optimization_mode", &::oneflow::cfg::JobConfigProto::optimizer_placement_optimization_mode);
    registry.def("set_optimizer_placement_optimization_mode", &::oneflow::cfg::JobConfigProto::set_optimizer_placement_optimization_mode);

    registry.def("has_optimizer_placement_optimization_threshold", &::oneflow::cfg::JobConfigProto::has_optimizer_placement_optimization_threshold);
    registry.def("clear_optimizer_placement_optimization_threshold", &::oneflow::cfg::JobConfigProto::clear_optimizer_placement_optimization_threshold);
    registry.def("optimizer_placement_optimization_threshold", &::oneflow::cfg::JobConfigProto::optimizer_placement_optimization_threshold);
    registry.def("set_optimizer_placement_optimization_threshold", &::oneflow::cfg::JobConfigProto::set_optimizer_placement_optimization_threshold);

    registry.def("has_qat_config", &::oneflow::cfg::JobConfigProto::has_qat_config);
    registry.def("clear_qat_config", &::oneflow::cfg::JobConfigProto::clear_qat_config);
    registry.def("qat_config", &::oneflow::cfg::JobConfigProto::shared_const_qat_config);
    registry.def("mutable_qat_config", &::oneflow::cfg::JobConfigProto::shared_mutable_qat_config);

    registry.def("has_enable_cudnn", &::oneflow::cfg::JobConfigProto::has_enable_cudnn);
    registry.def("clear_enable_cudnn", &::oneflow::cfg::JobConfigProto::clear_enable_cudnn);
    registry.def("enable_cudnn", &::oneflow::cfg::JobConfigProto::enable_cudnn);
    registry.def("set_enable_cudnn", &::oneflow::cfg::JobConfigProto::set_enable_cudnn);

    registry.def("has_cudnn_buf_limit_mbyte", &::oneflow::cfg::JobConfigProto::has_cudnn_buf_limit_mbyte);
    registry.def("clear_cudnn_buf_limit_mbyte", &::oneflow::cfg::JobConfigProto::clear_cudnn_buf_limit_mbyte);
    registry.def("cudnn_buf_limit_mbyte", &::oneflow::cfg::JobConfigProto::cudnn_buf_limit_mbyte);
    registry.def("set_cudnn_buf_limit_mbyte", &::oneflow::cfg::JobConfigProto::set_cudnn_buf_limit_mbyte);

    registry.def("has_cudnn_conv_force_fwd_algo", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_force_fwd_algo);
    registry.def("clear_cudnn_conv_force_fwd_algo", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_force_fwd_algo);
    registry.def("cudnn_conv_force_fwd_algo", &::oneflow::cfg::JobConfigProto::cudnn_conv_force_fwd_algo);
    registry.def("set_cudnn_conv_force_fwd_algo", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_force_fwd_algo);

    registry.def("has_cudnn_conv_force_bwd_data_algo", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_force_bwd_data_algo);
    registry.def("clear_cudnn_conv_force_bwd_data_algo", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_force_bwd_data_algo);
    registry.def("cudnn_conv_force_bwd_data_algo", &::oneflow::cfg::JobConfigProto::cudnn_conv_force_bwd_data_algo);
    registry.def("set_cudnn_conv_force_bwd_data_algo", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_force_bwd_data_algo);

    registry.def("has_cudnn_conv_force_bwd_filter_algo", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_force_bwd_filter_algo);
    registry.def("clear_cudnn_conv_force_bwd_filter_algo", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_force_bwd_filter_algo);
    registry.def("cudnn_conv_force_bwd_filter_algo", &::oneflow::cfg::JobConfigProto::cudnn_conv_force_bwd_filter_algo);
    registry.def("set_cudnn_conv_force_bwd_filter_algo", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_force_bwd_filter_algo);

    registry.def("has_cudnn_conv_heuristic_search_algo", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_heuristic_search_algo);
    registry.def("clear_cudnn_conv_heuristic_search_algo", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_heuristic_search_algo);
    registry.def("cudnn_conv_heuristic_search_algo", &::oneflow::cfg::JobConfigProto::cudnn_conv_heuristic_search_algo);
    registry.def("set_cudnn_conv_heuristic_search_algo", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_heuristic_search_algo);

    registry.def("has_cudnn_conv_use_deterministic_algo_only", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_use_deterministic_algo_only);
    registry.def("clear_cudnn_conv_use_deterministic_algo_only", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_use_deterministic_algo_only);
    registry.def("cudnn_conv_use_deterministic_algo_only", &::oneflow::cfg::JobConfigProto::cudnn_conv_use_deterministic_algo_only);
    registry.def("set_cudnn_conv_use_deterministic_algo_only", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_use_deterministic_algo_only);

    registry.def("has_enable_cudnn_fused_normalization_add_relu", &::oneflow::cfg::JobConfigProto::has_enable_cudnn_fused_normalization_add_relu);
    registry.def("clear_enable_cudnn_fused_normalization_add_relu", &::oneflow::cfg::JobConfigProto::clear_enable_cudnn_fused_normalization_add_relu);
    registry.def("enable_cudnn_fused_normalization_add_relu", &::oneflow::cfg::JobConfigProto::enable_cudnn_fused_normalization_add_relu);
    registry.def("set_enable_cudnn_fused_normalization_add_relu", &::oneflow::cfg::JobConfigProto::set_enable_cudnn_fused_normalization_add_relu);

    registry.def("has_enable_fuse_add_to_output", &::oneflow::cfg::JobConfigProto::has_enable_fuse_add_to_output);
    registry.def("clear_enable_fuse_add_to_output", &::oneflow::cfg::JobConfigProto::clear_enable_fuse_add_to_output);
    registry.def("enable_fuse_add_to_output", &::oneflow::cfg::JobConfigProto::enable_fuse_add_to_output);
    registry.def("set_enable_fuse_add_to_output", &::oneflow::cfg::JobConfigProto::set_enable_fuse_add_to_output);

    registry.def("has_enable_fuse_cast_scale", &::oneflow::cfg::JobConfigProto::has_enable_fuse_cast_scale);
    registry.def("clear_enable_fuse_cast_scale", &::oneflow::cfg::JobConfigProto::clear_enable_fuse_cast_scale);
    registry.def("enable_fuse_cast_scale", &::oneflow::cfg::JobConfigProto::enable_fuse_cast_scale);
    registry.def("set_enable_fuse_cast_scale", &::oneflow::cfg::JobConfigProto::set_enable_fuse_cast_scale);

    registry.def("has_num_gradient_accumulation_steps", &::oneflow::cfg::JobConfigProto::has_num_gradient_accumulation_steps);
    registry.def("clear_num_gradient_accumulation_steps", &::oneflow::cfg::JobConfigProto::clear_num_gradient_accumulation_steps);
    registry.def("num_gradient_accumulation_steps", &::oneflow::cfg::JobConfigProto::num_gradient_accumulation_steps);
    registry.def("set_num_gradient_accumulation_steps", &::oneflow::cfg::JobConfigProto::set_num_gradient_accumulation_steps);

    registry.def("has_enable_reuse_mem", &::oneflow::cfg::JobConfigProto::has_enable_reuse_mem);
    registry.def("clear_enable_reuse_mem", &::oneflow::cfg::JobConfigProto::clear_enable_reuse_mem);
    registry.def("enable_reuse_mem", &::oneflow::cfg::JobConfigProto::enable_reuse_mem);
    registry.def("set_enable_reuse_mem", &::oneflow::cfg::JobConfigProto::set_enable_reuse_mem);

    registry.def("has_enable_inplace", &::oneflow::cfg::JobConfigProto::has_enable_inplace);
    registry.def("clear_enable_inplace", &::oneflow::cfg::JobConfigProto::clear_enable_inplace);
    registry.def("enable_inplace", &::oneflow::cfg::JobConfigProto::enable_inplace);
    registry.def("set_enable_inplace", &::oneflow::cfg::JobConfigProto::set_enable_inplace);

    registry.def("has_enable_inplace_in_reduce_struct", &::oneflow::cfg::JobConfigProto::has_enable_inplace_in_reduce_struct);
    registry.def("clear_enable_inplace_in_reduce_struct", &::oneflow::cfg::JobConfigProto::clear_enable_inplace_in_reduce_struct);
    registry.def("enable_inplace_in_reduce_struct", &::oneflow::cfg::JobConfigProto::enable_inplace_in_reduce_struct);
    registry.def("set_enable_inplace_in_reduce_struct", &::oneflow::cfg::JobConfigProto::set_enable_inplace_in_reduce_struct);

    registry.def("has_do_parallel_cast_before_widening_type_cast", &::oneflow::cfg::JobConfigProto::has_do_parallel_cast_before_widening_type_cast);
    registry.def("clear_do_parallel_cast_before_widening_type_cast", &::oneflow::cfg::JobConfigProto::clear_do_parallel_cast_before_widening_type_cast);
    registry.def("do_parallel_cast_before_widening_type_cast", &::oneflow::cfg::JobConfigProto::do_parallel_cast_before_widening_type_cast);
    registry.def("set_do_parallel_cast_before_widening_type_cast", &::oneflow::cfg::JobConfigProto::set_do_parallel_cast_before_widening_type_cast);

    registry.def("has_prune_parallel_cast_ops", &::oneflow::cfg::JobConfigProto::has_prune_parallel_cast_ops);
    registry.def("clear_prune_parallel_cast_ops", &::oneflow::cfg::JobConfigProto::clear_prune_parallel_cast_ops);
    registry.def("prune_parallel_cast_ops", &::oneflow::cfg::JobConfigProto::prune_parallel_cast_ops);
    registry.def("set_prune_parallel_cast_ops", &::oneflow::cfg::JobConfigProto::set_prune_parallel_cast_ops);

    registry.def("has_prune_cast_to_static_shape_ops", &::oneflow::cfg::JobConfigProto::has_prune_cast_to_static_shape_ops);
    registry.def("clear_prune_cast_to_static_shape_ops", &::oneflow::cfg::JobConfigProto::clear_prune_cast_to_static_shape_ops);
    registry.def("prune_cast_to_static_shape_ops", &::oneflow::cfg::JobConfigProto::prune_cast_to_static_shape_ops);
    registry.def("set_prune_cast_to_static_shape_ops", &::oneflow::cfg::JobConfigProto::set_prune_cast_to_static_shape_ops);

    registry.def("has_prune_amp_white_identity_ops", &::oneflow::cfg::JobConfigProto::has_prune_amp_white_identity_ops);
    registry.def("clear_prune_amp_white_identity_ops", &::oneflow::cfg::JobConfigProto::clear_prune_amp_white_identity_ops);
    registry.def("prune_amp_white_identity_ops", &::oneflow::cfg::JobConfigProto::prune_amp_white_identity_ops);
    registry.def("set_prune_amp_white_identity_ops", &::oneflow::cfg::JobConfigProto::set_prune_amp_white_identity_ops);

    registry.def("has_cudnn_conv_enable_pseudo_half", &::oneflow::cfg::JobConfigProto::has_cudnn_conv_enable_pseudo_half);
    registry.def("clear_cudnn_conv_enable_pseudo_half", &::oneflow::cfg::JobConfigProto::clear_cudnn_conv_enable_pseudo_half);
    registry.def("cudnn_conv_enable_pseudo_half", &::oneflow::cfg::JobConfigProto::cudnn_conv_enable_pseudo_half);
    registry.def("set_cudnn_conv_enable_pseudo_half", &::oneflow::cfg::JobConfigProto::set_cudnn_conv_enable_pseudo_half);

    registry.def("has_enable_auto_mixed_precision", &::oneflow::cfg::JobConfigProto::has_enable_auto_mixed_precision);
    registry.def("clear_enable_auto_mixed_precision", &::oneflow::cfg::JobConfigProto::clear_enable_auto_mixed_precision);
    registry.def("enable_auto_mixed_precision", &::oneflow::cfg::JobConfigProto::enable_auto_mixed_precision);
    registry.def("set_enable_auto_mixed_precision", &::oneflow::cfg::JobConfigProto::set_enable_auto_mixed_precision);

    registry.def("has_enable_quantization_aware_training", &::oneflow::cfg::JobConfigProto::has_enable_quantization_aware_training);
    registry.def("clear_enable_quantization_aware_training", &::oneflow::cfg::JobConfigProto::clear_enable_quantization_aware_training);
    registry.def("enable_quantization_aware_training", &::oneflow::cfg::JobConfigProto::enable_quantization_aware_training);
    registry.def("set_enable_quantization_aware_training", &::oneflow::cfg::JobConfigProto::set_enable_quantization_aware_training);

    registry.def("has_concurrency_width", &::oneflow::cfg::JobConfigProto::has_concurrency_width);
    registry.def("clear_concurrency_width", &::oneflow::cfg::JobConfigProto::clear_concurrency_width);
    registry.def("concurrency_width", &::oneflow::cfg::JobConfigProto::concurrency_width);
    registry.def("set_concurrency_width", &::oneflow::cfg::JobConfigProto::set_concurrency_width);

    registry.def("flag_name2flag_value_size", &::oneflow::cfg::JobConfigProto::flag_name2flag_value_size);
    registry.def("flag_name2flag_value", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::JobConfigProto::*)() const)&::oneflow::cfg::JobConfigProto::shared_const_flag_name2flag_value);
    registry.def("clear_flag_name2flag_value", &::oneflow::cfg::JobConfigProto::clear_flag_name2flag_value);
    registry.def("mutable_flag_name2flag_value", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::JobConfigProto::*)())&::oneflow::cfg::JobConfigProto::shared_mutable_flag_name2flag_value);
    registry.def("flag_name2flag_value", (::std::shared_ptr<ConstAttrValue> (::oneflow::cfg::JobConfigProto::*)(const ::std::string&) const)&::oneflow::cfg::JobConfigProto::shared_const_flag_name2flag_value);

    registry.def("has_logical_object_id", &::oneflow::cfg::JobConfigProto::has_logical_object_id);
    registry.def("clear_logical_object_id", &::oneflow::cfg::JobConfigProto::clear_logical_object_id);
    registry.def("logical_object_id", &::oneflow::cfg::JobConfigProto::logical_object_id);
    registry.def("set_logical_object_id", &::oneflow::cfg::JobConfigProto::set_logical_object_id);

    registry.def("has_signature", &::oneflow::cfg::JobConfigProto::has_signature);
    registry.def("clear_signature", &::oneflow::cfg::JobConfigProto::clear_signature);
    registry.def("signature", &::oneflow::cfg::JobConfigProto::shared_const_signature);
    registry.def("mutable_signature", &::oneflow::cfg::JobConfigProto::shared_mutable_signature);
    pybind11::enum_<::oneflow::cfg::JobConfigProto::JobTypeCase>(registry, "JobTypeCase")
        .value("JOB_TYPE_NOT_SET", ::oneflow::cfg::JobConfigProto::JOB_TYPE_NOT_SET)
        .value("kTrainConf", ::oneflow::cfg::JobConfigProto::kTrainConf)
        .value("kPredictConf", ::oneflow::cfg::JobConfigProto::kPredictConf)
        ;
    registry.def("job_type_case",  &::oneflow::cfg::JobConfigProto::job_type_case);
    pybind11::enum_<::oneflow::cfg::JobConfigProto::DefaultInitializeConfCase>(registry, "DefaultInitializeConfCase")
        .value("DEFAULT_INITIALIZE_CONF_NOT_SET", ::oneflow::cfg::JobConfigProto::DEFAULT_INITIALIZE_CONF_NOT_SET)
        .value("kDefaultInitializerConf", ::oneflow::cfg::JobConfigProto::kDefaultInitializerConf)
        .value("kDefaultInitializeWithSnapshotPath", ::oneflow::cfg::JobConfigProto::kDefaultInitializeWithSnapshotPath)
        ;
    registry.def("default_initialize_conf_case",  &::oneflow::cfg::JobConfigProto::default_initialize_conf_case);
  }
}