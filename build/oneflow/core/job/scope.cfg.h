#ifndef CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstAttrValue;
class AttrValue;
}
}
namespace oneflow {
namespace cfg {
class ConstOptMirroredParallel;
class OptMirroredParallel;
}
}

namespace oneflow {

// forward declare proto class;
class ScopeProto_AttrName2attrValueEntry;
class ScopeProto;

namespace cfg {


class ScopeProto;
class ConstScopeProto;


class _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_;
class _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_; 
class Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_;

class ConstScopeProto : public ::oneflow::cfg::Message {
 public:

  class _ScopeProto_ {
   public:
    _ScopeProto_();
    explicit _ScopeProto_(const _ScopeProto_& other);
    explicit _ScopeProto_(_ScopeProto_&& other);
    _ScopeProto_(const ::oneflow::ScopeProto& proto_scopeproto);
    ~_ScopeProto_();

    void InitFromProto(const ::oneflow::ScopeProto& proto_scopeproto);

    void ToProto(::oneflow::ScopeProto* proto_scopeproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ScopeProto_& other);
  
      // optional field job_desc_symbol_id
     public:
    bool has_job_desc_symbol_id() const;
    const int64_t& job_desc_symbol_id() const;
    void clear_job_desc_symbol_id();
    void set_job_desc_symbol_id(const int64_t& value);
    int64_t* mutable_job_desc_symbol_id();
   protected:
    bool has_job_desc_symbol_id_ = false;
    int64_t job_desc_symbol_id_;
      
      // optional field device_parallel_desc_symbol_id
     public:
    bool has_device_parallel_desc_symbol_id() const;
    const int64_t& device_parallel_desc_symbol_id() const;
    void clear_device_parallel_desc_symbol_id();
    void set_device_parallel_desc_symbol_id(const int64_t& value);
    int64_t* mutable_device_parallel_desc_symbol_id();
   protected:
    bool has_device_parallel_desc_symbol_id_ = false;
    int64_t device_parallel_desc_symbol_id_;
      
      // optional field host_parallel_desc_symbol_id
     public:
    bool has_host_parallel_desc_symbol_id() const;
    const int64_t& host_parallel_desc_symbol_id() const;
    void clear_host_parallel_desc_symbol_id();
    void set_host_parallel_desc_symbol_id(const int64_t& value);
    int64_t* mutable_host_parallel_desc_symbol_id();
   protected:
    bool has_host_parallel_desc_symbol_id_ = false;
    int64_t host_parallel_desc_symbol_id_;
      
      // optional field enable_cpu_alternative_op
     public:
    bool has_enable_cpu_alternative_op() const;
    const bool& enable_cpu_alternative_op() const;
    void clear_enable_cpu_alternative_op();
    void set_enable_cpu_alternative_op(const bool& value);
    bool* mutable_enable_cpu_alternative_op();
   protected:
    bool has_enable_cpu_alternative_op_ = false;
    bool enable_cpu_alternative_op_;
      
      // optional field opt_mirrored_parallel_conf
     public:
    bool has_opt_mirrored_parallel_conf() const;
    const ::oneflow::cfg::OptMirroredParallel& opt_mirrored_parallel_conf() const;
    void clear_opt_mirrored_parallel_conf();
    ::oneflow::cfg::OptMirroredParallel* mutable_opt_mirrored_parallel_conf();
   protected:
    bool has_opt_mirrored_parallel_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> opt_mirrored_parallel_conf_;
      
      // repeated field scope_op_name_prefixes
   public:
    ::std::size_t scope_op_name_prefixes_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& scope_op_name_prefixes() const;
    const ::std::string& scope_op_name_prefixes(::std::size_t index) const;
    void clear_scope_op_name_prefixes();
    _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_* mutable_scope_op_name_prefixes();
    ::std::string* mutable_scope_op_name_prefixes(::std::size_t index);
      void add_scope_op_name_prefixes(const ::std::string& value);
    void set_scope_op_name_prefixes(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> scope_op_name_prefixes_;
    
      // optional field parent_scope_symbol_id
     public:
    bool has_parent_scope_symbol_id() const;
    const int64_t& parent_scope_symbol_id() const;
    void clear_parent_scope_symbol_id();
    void set_parent_scope_symbol_id(const int64_t& value);
    int64_t* mutable_parent_scope_symbol_id();
   protected:
    bool has_parent_scope_symbol_id_ = false;
    int64_t parent_scope_symbol_id_;
      
      // optional field session_id
     public:
    bool has_session_id() const;
    const int64_t& session_id() const;
    void clear_session_id();
    void set_session_id(const int64_t& value);
    int64_t* mutable_session_id();
   protected:
    bool has_session_id_ = false;
    int64_t session_id_;
      
     public:
    ::std::size_t attr_name2attr_value_size() const;
    const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& attr_name2attr_value() const;

    _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ * mutable_attr_name2attr_value();

    const ::oneflow::cfg::AttrValue& attr_name2attr_value(::std::string key) const;

    void clear_attr_name2attr_value();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> attr_name2attr_value_;
    
      // optional field calculation_pass_name
     public:
    bool has_calculation_pass_name() const;
    const ::std::string& calculation_pass_name() const;
    void clear_calculation_pass_name();
    void set_calculation_pass_name(const ::std::string& value);
    ::std::string* mutable_calculation_pass_name();
   protected:
    bool has_calculation_pass_name_ = false;
    ::std::string calculation_pass_name_;
           
   public:
    int compare(const _ScopeProto_& other);

    bool operator==(const _ScopeProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ScopeProto_& other) const;
  };

  ConstScopeProto(const ::std::shared_ptr<_ScopeProto_>& data);
  ConstScopeProto(const ConstScopeProto&);
  ConstScopeProto(ConstScopeProto&&) noexcept;
  ConstScopeProto();
  ConstScopeProto(const ::oneflow::ScopeProto& proto_scopeproto);
  virtual ~ConstScopeProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_scopeproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field job_desc_symbol_id
 public:
  bool has_job_desc_symbol_id() const;
  const int64_t& job_desc_symbol_id() const;
  // used by pybind11 only
  // required or optional field device_parallel_desc_symbol_id
 public:
  bool has_device_parallel_desc_symbol_id() const;
  const int64_t& device_parallel_desc_symbol_id() const;
  // used by pybind11 only
  // required or optional field host_parallel_desc_symbol_id
 public:
  bool has_host_parallel_desc_symbol_id() const;
  const int64_t& host_parallel_desc_symbol_id() const;
  // used by pybind11 only
  // required or optional field enable_cpu_alternative_op
 public:
  bool has_enable_cpu_alternative_op() const;
  const bool& enable_cpu_alternative_op() const;
  // used by pybind11 only
  // required or optional field opt_mirrored_parallel_conf
 public:
  bool has_opt_mirrored_parallel_conf() const;
  const ::oneflow::cfg::OptMirroredParallel& opt_mirrored_parallel_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOptMirroredParallel> shared_const_opt_mirrored_parallel_conf() const;
  // repeated field scope_op_name_prefixes
 public:
  ::std::size_t scope_op_name_prefixes_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& scope_op_name_prefixes() const;
  const ::std::string& scope_op_name_prefixes(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> shared_const_scope_op_name_prefixes() const;
  // required or optional field parent_scope_symbol_id
 public:
  bool has_parent_scope_symbol_id() const;
  const int64_t& parent_scope_symbol_id() const;
  // used by pybind11 only
  // required or optional field session_id
 public:
  bool has_session_id() const;
  const int64_t& session_id() const;
  // used by pybind11 only
  // map field attr_name2attr_value
 public:
  ::std::size_t attr_name2attr_value_size() const;
  const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& attr_name2attr_value() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> shared_const_attr_name2attr_value() const;
  // required or optional field calculation_pass_name
 public:
  bool has_calculation_pass_name() const;
  const ::std::string& calculation_pass_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstScopeProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstScopeProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstScopeProto& other) const;
 protected:
  const ::std::shared_ptr<_ScopeProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ScopeProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstScopeProto
  void BuildFromProto(const PbMessage& proto_scopeproto);
  
  ::std::shared_ptr<_ScopeProto_> data_;
};

class ScopeProto final : public ConstScopeProto {
 public:
  ScopeProto(const ::std::shared_ptr<_ScopeProto_>& data);
  ScopeProto(const ScopeProto& other);
  // enable nothrow for ::std::vector<ScopeProto> resize 
  ScopeProto(ScopeProto&&) noexcept;
  ScopeProto();
  explicit ScopeProto(const ::oneflow::ScopeProto& proto_scopeproto);

  ~ScopeProto() override;

  void InitFromProto(const PbMessage& proto_scopeproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ScopeProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ScopeProto& other) const;
  void Clear();
  void CopyFrom(const ScopeProto& other);
  ScopeProto& operator=(const ScopeProto& other);

  // required or optional field job_desc_symbol_id
 public:
  void clear_job_desc_symbol_id();
  void set_job_desc_symbol_id(const int64_t& value);
  int64_t* mutable_job_desc_symbol_id();
  // required or optional field device_parallel_desc_symbol_id
 public:
  void clear_device_parallel_desc_symbol_id();
  void set_device_parallel_desc_symbol_id(const int64_t& value);
  int64_t* mutable_device_parallel_desc_symbol_id();
  // required or optional field host_parallel_desc_symbol_id
 public:
  void clear_host_parallel_desc_symbol_id();
  void set_host_parallel_desc_symbol_id(const int64_t& value);
  int64_t* mutable_host_parallel_desc_symbol_id();
  // required or optional field enable_cpu_alternative_op
 public:
  void clear_enable_cpu_alternative_op();
  void set_enable_cpu_alternative_op(const bool& value);
  bool* mutable_enable_cpu_alternative_op();
  // required or optional field opt_mirrored_parallel_conf
 public:
  void clear_opt_mirrored_parallel_conf();
  ::oneflow::cfg::OptMirroredParallel* mutable_opt_mirrored_parallel_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> shared_mutable_opt_mirrored_parallel_conf();
  // repeated field scope_op_name_prefixes
 public:
  void clear_scope_op_name_prefixes();
  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_* mutable_scope_op_name_prefixes();
  ::std::string* mutable_scope_op_name_prefixes(::std::size_t index);
  void add_scope_op_name_prefixes(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> shared_mutable_scope_op_name_prefixes();
  void set_scope_op_name_prefixes(::std::size_t index, const ::std::string& value);
  // required or optional field parent_scope_symbol_id
 public:
  void clear_parent_scope_symbol_id();
  void set_parent_scope_symbol_id(const int64_t& value);
  int64_t* mutable_parent_scope_symbol_id();
  // required or optional field session_id
 public:
  void clear_session_id();
  void set_session_id(const int64_t& value);
  int64_t* mutable_session_id();
  // repeated field attr_name2attr_value
 public:
  void clear_attr_name2attr_value();

  const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ & attr_name2attr_value() const;

  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_* mutable_attr_name2attr_value();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> shared_mutable_attr_name2attr_value();
  // required or optional field calculation_pass_name
 public:
  void clear_calculation_pass_name();
  void set_calculation_pass_name(const ::std::string& value);
  ::std::string* mutable_calculation_pass_name();

  ::std::shared_ptr<ScopeProto> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_();
  ~Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::AttrValue& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstAttrValue> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ final : public Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_();
  ~_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::AttrValue> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstScopeProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstScopeProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ScopeProto> {
  std::size_t operator()(const ::oneflow::cfg::ScopeProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H_