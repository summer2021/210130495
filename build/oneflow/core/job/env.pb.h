// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/job/env.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fenv_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fenv_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include "oneflow/core/control/ctrl_bootstrap.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fjob_2fenv_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_oneflow_2fcore_2fjob_2fenv_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[3]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto;
namespace oneflow {
class CppLoggingConf;
class CppLoggingConfDefaultTypeInternal;
extern CppLoggingConfDefaultTypeInternal _CppLoggingConf_default_instance_;
class EnvProto;
class EnvProtoDefaultTypeInternal;
extern EnvProtoDefaultTypeInternal _EnvProto_default_instance_;
class Machine;
class MachineDefaultTypeInternal;
extern MachineDefaultTypeInternal _Machine_default_instance_;
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> ::oneflow::CppLoggingConf* Arena::CreateMaybeMessage<::oneflow::CppLoggingConf>(Arena*);
template<> ::oneflow::EnvProto* Arena::CreateMaybeMessage<::oneflow::EnvProto>(Arena*);
template<> ::oneflow::Machine* Arena::CreateMaybeMessage<::oneflow::Machine>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace oneflow {

// ===================================================================

class Machine :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.Machine) */ {
 public:
  Machine();
  virtual ~Machine();

  Machine(const Machine& from);
  Machine(Machine&& from) noexcept
    : Machine() {
    *this = ::std::move(from);
  }

  inline Machine& operator=(const Machine& from) {
    CopyFrom(from);
    return *this;
  }
  inline Machine& operator=(Machine&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const Machine& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Machine* internal_default_instance() {
    return reinterpret_cast<const Machine*>(
               &_Machine_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(Machine& a, Machine& b) {
    a.Swap(&b);
  }
  inline void Swap(Machine* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline Machine* New() const final {
    return CreateMaybeMessage<Machine>(nullptr);
  }

  Machine* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<Machine>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const Machine& from);
  void MergeFrom(const Machine& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(Machine* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.Machine";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kAddrFieldNumber = 2,
    kIdFieldNumber = 1,
    kCtrlPortAgentFieldNumber = 3,
    kDataPortAgentFieldNumber = 4,
  };
  // required string addr = 2;
  bool has_addr() const;
  void clear_addr();
  const std::string& addr() const;
  void set_addr(const std::string& value);
  void set_addr(std::string&& value);
  void set_addr(const char* value);
  void set_addr(const char* value, size_t size);
  std::string* mutable_addr();
  std::string* release_addr();
  void set_allocated_addr(std::string* addr);

  // required int64 id = 1;
  bool has_id() const;
  void clear_id();
  ::PROTOBUF_NAMESPACE_ID::int64 id() const;
  void set_id(::PROTOBUF_NAMESPACE_ID::int64 value);

  // optional int32 ctrl_port_agent = 3 [default = -1];
  bool has_ctrl_port_agent() const;
  void clear_ctrl_port_agent();
  ::PROTOBUF_NAMESPACE_ID::int32 ctrl_port_agent() const;
  void set_ctrl_port_agent(::PROTOBUF_NAMESPACE_ID::int32 value);

  // optional int32 data_port_agent = 4 [default = -1];
  bool has_data_port_agent() const;
  void clear_data_port_agent();
  ::PROTOBUF_NAMESPACE_ID::int32 data_port_agent() const;
  void set_data_port_agent(::PROTOBUF_NAMESPACE_ID::int32 value);

  // @@protoc_insertion_point(class_scope:oneflow.Machine)
 private:
  class _Internal;

  // helper for ByteSizeLong()
  size_t RequiredFieldsByteSizeFallback() const;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr addr_;
  ::PROTOBUF_NAMESPACE_ID::int64 id_;
  ::PROTOBUF_NAMESPACE_ID::int32 ctrl_port_agent_;
  ::PROTOBUF_NAMESPACE_ID::int32 data_port_agent_;
  friend struct ::TableStruct_oneflow_2fcore_2fjob_2fenv_2eproto;
};
// -------------------------------------------------------------------

class CppLoggingConf :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.CppLoggingConf) */ {
 public:
  CppLoggingConf();
  virtual ~CppLoggingConf();

  CppLoggingConf(const CppLoggingConf& from);
  CppLoggingConf(CppLoggingConf&& from) noexcept
    : CppLoggingConf() {
    *this = ::std::move(from);
  }

  inline CppLoggingConf& operator=(const CppLoggingConf& from) {
    CopyFrom(from);
    return *this;
  }
  inline CppLoggingConf& operator=(CppLoggingConf&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const CppLoggingConf& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const CppLoggingConf* internal_default_instance() {
    return reinterpret_cast<const CppLoggingConf*>(
               &_CppLoggingConf_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    1;

  friend void swap(CppLoggingConf& a, CppLoggingConf& b) {
    a.Swap(&b);
  }
  inline void Swap(CppLoggingConf* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline CppLoggingConf* New() const final {
    return CreateMaybeMessage<CppLoggingConf>(nullptr);
  }

  CppLoggingConf* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<CppLoggingConf>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const CppLoggingConf& from);
  void MergeFrom(const CppLoggingConf& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(CppLoggingConf* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.CppLoggingConf";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kLogDirFieldNumber = 1,
    kLogtostderrFieldNumber = 2,
    kLogbuflevelFieldNumber = 3,
  };
  // optional string log_dir = 1 [default = "./log"];
  bool has_log_dir() const;
  void clear_log_dir();
  const std::string& log_dir() const;
  void set_log_dir(const std::string& value);
  void set_log_dir(std::string&& value);
  void set_log_dir(const char* value);
  void set_log_dir(const char* value, size_t size);
  std::string* mutable_log_dir();
  std::string* release_log_dir();
  void set_allocated_log_dir(std::string* log_dir);

  // optional int32 logtostderr = 2 [default = 0];
  bool has_logtostderr() const;
  void clear_logtostderr();
  ::PROTOBUF_NAMESPACE_ID::int32 logtostderr() const;
  void set_logtostderr(::PROTOBUF_NAMESPACE_ID::int32 value);

  // optional int32 logbuflevel = 3 [default = -1];
  bool has_logbuflevel() const;
  void clear_logbuflevel();
  ::PROTOBUF_NAMESPACE_ID::int32 logbuflevel() const;
  void set_logbuflevel(::PROTOBUF_NAMESPACE_ID::int32 value);

  // @@protoc_insertion_point(class_scope:oneflow.CppLoggingConf)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  public:
  static ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<std::string> _i_give_permission_to_break_this_code_default_log_dir_;
  private:
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr log_dir_;
  ::PROTOBUF_NAMESPACE_ID::int32 logtostderr_;
  ::PROTOBUF_NAMESPACE_ID::int32 logbuflevel_;
  friend struct ::TableStruct_oneflow_2fcore_2fjob_2fenv_2eproto;
};
// -------------------------------------------------------------------

class EnvProto :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.EnvProto) */ {
 public:
  EnvProto();
  virtual ~EnvProto();

  EnvProto(const EnvProto& from);
  EnvProto(EnvProto&& from) noexcept
    : EnvProto() {
    *this = ::std::move(from);
  }

  inline EnvProto& operator=(const EnvProto& from) {
    CopyFrom(from);
    return *this;
  }
  inline EnvProto& operator=(EnvProto&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const EnvProto& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const EnvProto* internal_default_instance() {
    return reinterpret_cast<const EnvProto*>(
               &_EnvProto_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    2;

  friend void swap(EnvProto& a, EnvProto& b) {
    a.Swap(&b);
  }
  inline void Swap(EnvProto* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline EnvProto* New() const final {
    return CreateMaybeMessage<EnvProto>(nullptr);
  }

  EnvProto* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<EnvProto>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const EnvProto& from);
  void MergeFrom(const EnvProto& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(EnvProto* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.EnvProto";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fjob_2fenv_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kMachineFieldNumber = 1,
    kCppLoggingConfFieldNumber = 4,
    kCtrlBootstrapConfFieldNumber = 5,
    kCtrlPortFieldNumber = 2,
    kDataPortFieldNumber = 3,
  };
  // repeated .oneflow.Machine machine = 1;
  int machine_size() const;
  void clear_machine();
  ::oneflow::Machine* mutable_machine(int index);
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::Machine >*
      mutable_machine();
  const ::oneflow::Machine& machine(int index) const;
  ::oneflow::Machine* add_machine();
  const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::Machine >&
      machine() const;

  // optional .oneflow.CppLoggingConf cpp_logging_conf = 4;
  bool has_cpp_logging_conf() const;
  void clear_cpp_logging_conf();
  const ::oneflow::CppLoggingConf& cpp_logging_conf() const;
  ::oneflow::CppLoggingConf* release_cpp_logging_conf();
  ::oneflow::CppLoggingConf* mutable_cpp_logging_conf();
  void set_allocated_cpp_logging_conf(::oneflow::CppLoggingConf* cpp_logging_conf);

  // optional .oneflow.BootstrapConf ctrl_bootstrap_conf = 5;
  bool has_ctrl_bootstrap_conf() const;
  void clear_ctrl_bootstrap_conf();
  const ::oneflow::BootstrapConf& ctrl_bootstrap_conf() const;
  ::oneflow::BootstrapConf* release_ctrl_bootstrap_conf();
  ::oneflow::BootstrapConf* mutable_ctrl_bootstrap_conf();
  void set_allocated_ctrl_bootstrap_conf(::oneflow::BootstrapConf* ctrl_bootstrap_conf);

  // required int32 ctrl_port = 2;
  bool has_ctrl_port() const;
  void clear_ctrl_port();
  ::PROTOBUF_NAMESPACE_ID::int32 ctrl_port() const;
  void set_ctrl_port(::PROTOBUF_NAMESPACE_ID::int32 value);

  // optional int32 data_port = 3 [default = -1];
  bool has_data_port() const;
  void clear_data_port();
  ::PROTOBUF_NAMESPACE_ID::int32 data_port() const;
  void set_data_port(::PROTOBUF_NAMESPACE_ID::int32 value);

  // @@protoc_insertion_point(class_scope:oneflow.EnvProto)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::Machine > machine_;
  ::oneflow::CppLoggingConf* cpp_logging_conf_;
  ::oneflow::BootstrapConf* ctrl_bootstrap_conf_;
  ::PROTOBUF_NAMESPACE_ID::int32 ctrl_port_;
  ::PROTOBUF_NAMESPACE_ID::int32 data_port_;
  friend struct ::TableStruct_oneflow_2fcore_2fjob_2fenv_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Machine

// required int64 id = 1;
inline bool Machine::has_id() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void Machine::clear_id() {
  id_ = PROTOBUF_LONGLONG(0);
  _has_bits_[0] &= ~0x00000002u;
}
inline ::PROTOBUF_NAMESPACE_ID::int64 Machine::id() const {
  // @@protoc_insertion_point(field_get:oneflow.Machine.id)
  return id_;
}
inline void Machine::set_id(::PROTOBUF_NAMESPACE_ID::int64 value) {
  _has_bits_[0] |= 0x00000002u;
  id_ = value;
  // @@protoc_insertion_point(field_set:oneflow.Machine.id)
}

// required string addr = 2;
inline bool Machine::has_addr() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Machine::clear_addr() {
  addr_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _has_bits_[0] &= ~0x00000001u;
}
inline const std::string& Machine::addr() const {
  // @@protoc_insertion_point(field_get:oneflow.Machine.addr)
  return addr_.GetNoArena();
}
inline void Machine::set_addr(const std::string& value) {
  _has_bits_[0] |= 0x00000001u;
  addr_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:oneflow.Machine.addr)
}
inline void Machine::set_addr(std::string&& value) {
  _has_bits_[0] |= 0x00000001u;
  addr_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.Machine.addr)
}
inline void Machine::set_addr(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000001u;
  addr_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.Machine.addr)
}
inline void Machine::set_addr(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000001u;
  addr_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.Machine.addr)
}
inline std::string* Machine::mutable_addr() {
  _has_bits_[0] |= 0x00000001u;
  // @@protoc_insertion_point(field_mutable:oneflow.Machine.addr)
  return addr_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* Machine::release_addr() {
  // @@protoc_insertion_point(field_release:oneflow.Machine.addr)
  if (!has_addr()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000001u;
  return addr_.ReleaseNonDefaultNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void Machine::set_allocated_addr(std::string* addr) {
  if (addr != nullptr) {
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  addr_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), addr);
  // @@protoc_insertion_point(field_set_allocated:oneflow.Machine.addr)
}

// optional int32 ctrl_port_agent = 3 [default = -1];
inline bool Machine::has_ctrl_port_agent() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void Machine::clear_ctrl_port_agent() {
  ctrl_port_agent_ = -1;
  _has_bits_[0] &= ~0x00000004u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 Machine::ctrl_port_agent() const {
  // @@protoc_insertion_point(field_get:oneflow.Machine.ctrl_port_agent)
  return ctrl_port_agent_;
}
inline void Machine::set_ctrl_port_agent(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000004u;
  ctrl_port_agent_ = value;
  // @@protoc_insertion_point(field_set:oneflow.Machine.ctrl_port_agent)
}

// optional int32 data_port_agent = 4 [default = -1];
inline bool Machine::has_data_port_agent() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void Machine::clear_data_port_agent() {
  data_port_agent_ = -1;
  _has_bits_[0] &= ~0x00000008u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 Machine::data_port_agent() const {
  // @@protoc_insertion_point(field_get:oneflow.Machine.data_port_agent)
  return data_port_agent_;
}
inline void Machine::set_data_port_agent(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000008u;
  data_port_agent_ = value;
  // @@protoc_insertion_point(field_set:oneflow.Machine.data_port_agent)
}

// -------------------------------------------------------------------

// CppLoggingConf

// optional string log_dir = 1 [default = "./log"];
inline bool CppLoggingConf::has_log_dir() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void CppLoggingConf::clear_log_dir() {
  log_dir_.ClearToDefaultNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get());
  _has_bits_[0] &= ~0x00000001u;
}
inline const std::string& CppLoggingConf::log_dir() const {
  // @@protoc_insertion_point(field_get:oneflow.CppLoggingConf.log_dir)
  return log_dir_.GetNoArena();
}
inline void CppLoggingConf::set_log_dir(const std::string& value) {
  _has_bits_[0] |= 0x00000001u;
  log_dir_.SetNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get(), value);
  // @@protoc_insertion_point(field_set:oneflow.CppLoggingConf.log_dir)
}
inline void CppLoggingConf::set_log_dir(std::string&& value) {
  _has_bits_[0] |= 0x00000001u;
  log_dir_.SetNoArena(
    &::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:oneflow.CppLoggingConf.log_dir)
}
inline void CppLoggingConf::set_log_dir(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  _has_bits_[0] |= 0x00000001u;
  log_dir_.SetNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:oneflow.CppLoggingConf.log_dir)
}
inline void CppLoggingConf::set_log_dir(const char* value, size_t size) {
  _has_bits_[0] |= 0x00000001u;
  log_dir_.SetNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:oneflow.CppLoggingConf.log_dir)
}
inline std::string* CppLoggingConf::mutable_log_dir() {
  _has_bits_[0] |= 0x00000001u;
  // @@protoc_insertion_point(field_mutable:oneflow.CppLoggingConf.log_dir)
  return log_dir_.MutableNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get());
}
inline std::string* CppLoggingConf::release_log_dir() {
  // @@protoc_insertion_point(field_release:oneflow.CppLoggingConf.log_dir)
  if (!has_log_dir()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000001u;
  return log_dir_.ReleaseNonDefaultNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get());
}
inline void CppLoggingConf::set_allocated_log_dir(std::string* log_dir) {
  if (log_dir != nullptr) {
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  log_dir_.SetAllocatedNoArena(&::oneflow::CppLoggingConf::_i_give_permission_to_break_this_code_default_log_dir_.get(), log_dir);
  // @@protoc_insertion_point(field_set_allocated:oneflow.CppLoggingConf.log_dir)
}

// optional int32 logtostderr = 2 [default = 0];
inline bool CppLoggingConf::has_logtostderr() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void CppLoggingConf::clear_logtostderr() {
  logtostderr_ = 0;
  _has_bits_[0] &= ~0x00000002u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 CppLoggingConf::logtostderr() const {
  // @@protoc_insertion_point(field_get:oneflow.CppLoggingConf.logtostderr)
  return logtostderr_;
}
inline void CppLoggingConf::set_logtostderr(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000002u;
  logtostderr_ = value;
  // @@protoc_insertion_point(field_set:oneflow.CppLoggingConf.logtostderr)
}

// optional int32 logbuflevel = 3 [default = -1];
inline bool CppLoggingConf::has_logbuflevel() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void CppLoggingConf::clear_logbuflevel() {
  logbuflevel_ = -1;
  _has_bits_[0] &= ~0x00000004u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 CppLoggingConf::logbuflevel() const {
  // @@protoc_insertion_point(field_get:oneflow.CppLoggingConf.logbuflevel)
  return logbuflevel_;
}
inline void CppLoggingConf::set_logbuflevel(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000004u;
  logbuflevel_ = value;
  // @@protoc_insertion_point(field_set:oneflow.CppLoggingConf.logbuflevel)
}

// -------------------------------------------------------------------

// EnvProto

// repeated .oneflow.Machine machine = 1;
inline int EnvProto::machine_size() const {
  return machine_.size();
}
inline void EnvProto::clear_machine() {
  machine_.Clear();
}
inline ::oneflow::Machine* EnvProto::mutable_machine(int index) {
  // @@protoc_insertion_point(field_mutable:oneflow.EnvProto.machine)
  return machine_.Mutable(index);
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::Machine >*
EnvProto::mutable_machine() {
  // @@protoc_insertion_point(field_mutable_list:oneflow.EnvProto.machine)
  return &machine_;
}
inline const ::oneflow::Machine& EnvProto::machine(int index) const {
  // @@protoc_insertion_point(field_get:oneflow.EnvProto.machine)
  return machine_.Get(index);
}
inline ::oneflow::Machine* EnvProto::add_machine() {
  // @@protoc_insertion_point(field_add:oneflow.EnvProto.machine)
  return machine_.Add();
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::Machine >&
EnvProto::machine() const {
  // @@protoc_insertion_point(field_list:oneflow.EnvProto.machine)
  return machine_;
}

// required int32 ctrl_port = 2;
inline bool EnvProto::has_ctrl_port() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void EnvProto::clear_ctrl_port() {
  ctrl_port_ = 0;
  _has_bits_[0] &= ~0x00000004u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 EnvProto::ctrl_port() const {
  // @@protoc_insertion_point(field_get:oneflow.EnvProto.ctrl_port)
  return ctrl_port_;
}
inline void EnvProto::set_ctrl_port(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000004u;
  ctrl_port_ = value;
  // @@protoc_insertion_point(field_set:oneflow.EnvProto.ctrl_port)
}

// optional int32 data_port = 3 [default = -1];
inline bool EnvProto::has_data_port() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void EnvProto::clear_data_port() {
  data_port_ = -1;
  _has_bits_[0] &= ~0x00000008u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 EnvProto::data_port() const {
  // @@protoc_insertion_point(field_get:oneflow.EnvProto.data_port)
  return data_port_;
}
inline void EnvProto::set_data_port(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000008u;
  data_port_ = value;
  // @@protoc_insertion_point(field_set:oneflow.EnvProto.data_port)
}

// optional .oneflow.CppLoggingConf cpp_logging_conf = 4;
inline bool EnvProto::has_cpp_logging_conf() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void EnvProto::clear_cpp_logging_conf() {
  if (cpp_logging_conf_ != nullptr) cpp_logging_conf_->Clear();
  _has_bits_[0] &= ~0x00000001u;
}
inline const ::oneflow::CppLoggingConf& EnvProto::cpp_logging_conf() const {
  const ::oneflow::CppLoggingConf* p = cpp_logging_conf_;
  // @@protoc_insertion_point(field_get:oneflow.EnvProto.cpp_logging_conf)
  return p != nullptr ? *p : *reinterpret_cast<const ::oneflow::CppLoggingConf*>(
      &::oneflow::_CppLoggingConf_default_instance_);
}
inline ::oneflow::CppLoggingConf* EnvProto::release_cpp_logging_conf() {
  // @@protoc_insertion_point(field_release:oneflow.EnvProto.cpp_logging_conf)
  _has_bits_[0] &= ~0x00000001u;
  ::oneflow::CppLoggingConf* temp = cpp_logging_conf_;
  cpp_logging_conf_ = nullptr;
  return temp;
}
inline ::oneflow::CppLoggingConf* EnvProto::mutable_cpp_logging_conf() {
  _has_bits_[0] |= 0x00000001u;
  if (cpp_logging_conf_ == nullptr) {
    auto* p = CreateMaybeMessage<::oneflow::CppLoggingConf>(GetArenaNoVirtual());
    cpp_logging_conf_ = p;
  }
  // @@protoc_insertion_point(field_mutable:oneflow.EnvProto.cpp_logging_conf)
  return cpp_logging_conf_;
}
inline void EnvProto::set_allocated_cpp_logging_conf(::oneflow::CppLoggingConf* cpp_logging_conf) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == nullptr) {
    delete cpp_logging_conf_;
  }
  if (cpp_logging_conf) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      cpp_logging_conf = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, cpp_logging_conf, submessage_arena);
    }
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  cpp_logging_conf_ = cpp_logging_conf;
  // @@protoc_insertion_point(field_set_allocated:oneflow.EnvProto.cpp_logging_conf)
}

// optional .oneflow.BootstrapConf ctrl_bootstrap_conf = 5;
inline bool EnvProto::has_ctrl_bootstrap_conf() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline const ::oneflow::BootstrapConf& EnvProto::ctrl_bootstrap_conf() const {
  const ::oneflow::BootstrapConf* p = ctrl_bootstrap_conf_;
  // @@protoc_insertion_point(field_get:oneflow.EnvProto.ctrl_bootstrap_conf)
  return p != nullptr ? *p : *reinterpret_cast<const ::oneflow::BootstrapConf*>(
      &::oneflow::_BootstrapConf_default_instance_);
}
inline ::oneflow::BootstrapConf* EnvProto::release_ctrl_bootstrap_conf() {
  // @@protoc_insertion_point(field_release:oneflow.EnvProto.ctrl_bootstrap_conf)
  _has_bits_[0] &= ~0x00000002u;
  ::oneflow::BootstrapConf* temp = ctrl_bootstrap_conf_;
  ctrl_bootstrap_conf_ = nullptr;
  return temp;
}
inline ::oneflow::BootstrapConf* EnvProto::mutable_ctrl_bootstrap_conf() {
  _has_bits_[0] |= 0x00000002u;
  if (ctrl_bootstrap_conf_ == nullptr) {
    auto* p = CreateMaybeMessage<::oneflow::BootstrapConf>(GetArenaNoVirtual());
    ctrl_bootstrap_conf_ = p;
  }
  // @@protoc_insertion_point(field_mutable:oneflow.EnvProto.ctrl_bootstrap_conf)
  return ctrl_bootstrap_conf_;
}
inline void EnvProto::set_allocated_ctrl_bootstrap_conf(::oneflow::BootstrapConf* ctrl_bootstrap_conf) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(ctrl_bootstrap_conf_);
  }
  if (ctrl_bootstrap_conf) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      ctrl_bootstrap_conf = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, ctrl_bootstrap_conf, submessage_arena);
    }
    _has_bits_[0] |= 0x00000002u;
  } else {
    _has_bits_[0] &= ~0x00000002u;
  }
  ctrl_bootstrap_conf_ = ctrl_bootstrap_conf;
  // @@protoc_insertion_point(field_set_allocated:oneflow.EnvProto.ctrl_bootstrap_conf)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace oneflow

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fenv_2eproto
