#ifndef CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class ExponentialDecayConf;
class InverseTimeDecayConf;
class NaturalExpDecayConf;
class PiecewiseConstantConf;
class PolynomialDecayConf;
class CosineDecayConf;
class LinearCosineDecayConf;
class PiecewiseScalingConf;
class LearningRateDecayConf;
class ConstantWarmupConf;
class LinearWarmupConf;
class WarmupConf;

namespace cfg {


class ExponentialDecayConf;
class ConstExponentialDecayConf;

class InverseTimeDecayConf;
class ConstInverseTimeDecayConf;

class NaturalExpDecayConf;
class ConstNaturalExpDecayConf;

class PiecewiseConstantConf;
class ConstPiecewiseConstantConf;

class PolynomialDecayConf;
class ConstPolynomialDecayConf;

class CosineDecayConf;
class ConstCosineDecayConf;

class LinearCosineDecayConf;
class ConstLinearCosineDecayConf;

class PiecewiseScalingConf;
class ConstPiecewiseScalingConf;

class LearningRateDecayConf;
class ConstLearningRateDecayConf;

class ConstantWarmupConf;
class ConstConstantWarmupConf;

class LinearWarmupConf;
class ConstLinearWarmupConf;

class WarmupConf;
class ConstWarmupConf;



class ConstExponentialDecayConf : public ::oneflow::cfg::Message {
 public:

  class _ExponentialDecayConf_ {
   public:
    _ExponentialDecayConf_();
    explicit _ExponentialDecayConf_(const _ExponentialDecayConf_& other);
    explicit _ExponentialDecayConf_(_ExponentialDecayConf_&& other);
    _ExponentialDecayConf_(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf);
    ~_ExponentialDecayConf_();

    void InitFromProto(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf);

    void ToProto(::oneflow::ExponentialDecayConf* proto_exponentialdecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ExponentialDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field decay_rate
     public:
    bool has_decay_rate() const;
    const double& decay_rate() const;
    void clear_decay_rate();
    void set_decay_rate(const double& value);
    double* mutable_decay_rate();
   protected:
    bool has_decay_rate_ = false;
    double decay_rate_;
      
      // optional field staircase
     public:
    bool has_staircase() const;
    const bool& staircase() const;
    void clear_staircase();
    void set_staircase(const bool& value);
    bool* mutable_staircase();
   protected:
    bool has_staircase_ = false;
    bool staircase_;
           
   public:
    int compare(const _ExponentialDecayConf_& other);

    bool operator==(const _ExponentialDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ExponentialDecayConf_& other) const;
  };

  ConstExponentialDecayConf(const ::std::shared_ptr<_ExponentialDecayConf_>& data);
  ConstExponentialDecayConf(const ConstExponentialDecayConf&);
  ConstExponentialDecayConf(ConstExponentialDecayConf&&) noexcept;
  ConstExponentialDecayConf();
  ConstExponentialDecayConf(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf);
  virtual ~ConstExponentialDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_exponentialdecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field decay_rate
 public:
  bool has_decay_rate() const;
  const double& decay_rate() const;
  // used by pybind11 only
  // required or optional field staircase
 public:
  bool has_staircase() const;
  const bool& staircase() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstExponentialDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstExponentialDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstExponentialDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_ExponentialDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ExponentialDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstExponentialDecayConf
  void BuildFromProto(const PbMessage& proto_exponentialdecayconf);
  
  ::std::shared_ptr<_ExponentialDecayConf_> data_;
};

class ExponentialDecayConf final : public ConstExponentialDecayConf {
 public:
  ExponentialDecayConf(const ::std::shared_ptr<_ExponentialDecayConf_>& data);
  ExponentialDecayConf(const ExponentialDecayConf& other);
  // enable nothrow for ::std::vector<ExponentialDecayConf> resize 
  ExponentialDecayConf(ExponentialDecayConf&&) noexcept;
  ExponentialDecayConf();
  explicit ExponentialDecayConf(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf);

  ~ExponentialDecayConf() override;

  void InitFromProto(const PbMessage& proto_exponentialdecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ExponentialDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ExponentialDecayConf& other) const;
  void Clear();
  void CopyFrom(const ExponentialDecayConf& other);
  ExponentialDecayConf& operator=(const ExponentialDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field decay_rate
 public:
  void clear_decay_rate();
  void set_decay_rate(const double& value);
  double* mutable_decay_rate();
  // required or optional field staircase
 public:
  void clear_staircase();
  void set_staircase(const bool& value);
  bool* mutable_staircase();

  ::std::shared_ptr<ExponentialDecayConf> __SharedMutable__();
};


class ConstInverseTimeDecayConf : public ::oneflow::cfg::Message {
 public:

  class _InverseTimeDecayConf_ {
   public:
    _InverseTimeDecayConf_();
    explicit _InverseTimeDecayConf_(const _InverseTimeDecayConf_& other);
    explicit _InverseTimeDecayConf_(_InverseTimeDecayConf_&& other);
    _InverseTimeDecayConf_(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf);
    ~_InverseTimeDecayConf_();

    void InitFromProto(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf);

    void ToProto(::oneflow::InverseTimeDecayConf* proto_inversetimedecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InverseTimeDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field decay_rate
     public:
    bool has_decay_rate() const;
    const double& decay_rate() const;
    void clear_decay_rate();
    void set_decay_rate(const double& value);
    double* mutable_decay_rate();
   protected:
    bool has_decay_rate_ = false;
    double decay_rate_;
      
      // optional field staircase
     public:
    bool has_staircase() const;
    const bool& staircase() const;
    void clear_staircase();
    void set_staircase(const bool& value);
    bool* mutable_staircase();
   protected:
    bool has_staircase_ = false;
    bool staircase_;
           
   public:
    int compare(const _InverseTimeDecayConf_& other);

    bool operator==(const _InverseTimeDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InverseTimeDecayConf_& other) const;
  };

  ConstInverseTimeDecayConf(const ::std::shared_ptr<_InverseTimeDecayConf_>& data);
  ConstInverseTimeDecayConf(const ConstInverseTimeDecayConf&);
  ConstInverseTimeDecayConf(ConstInverseTimeDecayConf&&) noexcept;
  ConstInverseTimeDecayConf();
  ConstInverseTimeDecayConf(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf);
  virtual ~ConstInverseTimeDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_inversetimedecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field decay_rate
 public:
  bool has_decay_rate() const;
  const double& decay_rate() const;
  // used by pybind11 only
  // required or optional field staircase
 public:
  bool has_staircase() const;
  const bool& staircase() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstInverseTimeDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInverseTimeDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInverseTimeDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_InverseTimeDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InverseTimeDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInverseTimeDecayConf
  void BuildFromProto(const PbMessage& proto_inversetimedecayconf);
  
  ::std::shared_ptr<_InverseTimeDecayConf_> data_;
};

class InverseTimeDecayConf final : public ConstInverseTimeDecayConf {
 public:
  InverseTimeDecayConf(const ::std::shared_ptr<_InverseTimeDecayConf_>& data);
  InverseTimeDecayConf(const InverseTimeDecayConf& other);
  // enable nothrow for ::std::vector<InverseTimeDecayConf> resize 
  InverseTimeDecayConf(InverseTimeDecayConf&&) noexcept;
  InverseTimeDecayConf();
  explicit InverseTimeDecayConf(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf);

  ~InverseTimeDecayConf() override;

  void InitFromProto(const PbMessage& proto_inversetimedecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InverseTimeDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InverseTimeDecayConf& other) const;
  void Clear();
  void CopyFrom(const InverseTimeDecayConf& other);
  InverseTimeDecayConf& operator=(const InverseTimeDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field decay_rate
 public:
  void clear_decay_rate();
  void set_decay_rate(const double& value);
  double* mutable_decay_rate();
  // required or optional field staircase
 public:
  void clear_staircase();
  void set_staircase(const bool& value);
  bool* mutable_staircase();

  ::std::shared_ptr<InverseTimeDecayConf> __SharedMutable__();
};


class ConstNaturalExpDecayConf : public ::oneflow::cfg::Message {
 public:

  class _NaturalExpDecayConf_ {
   public:
    _NaturalExpDecayConf_();
    explicit _NaturalExpDecayConf_(const _NaturalExpDecayConf_& other);
    explicit _NaturalExpDecayConf_(_NaturalExpDecayConf_&& other);
    _NaturalExpDecayConf_(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf);
    ~_NaturalExpDecayConf_();

    void InitFromProto(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf);

    void ToProto(::oneflow::NaturalExpDecayConf* proto_naturalexpdecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _NaturalExpDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field decay_rate
     public:
    bool has_decay_rate() const;
    const double& decay_rate() const;
    void clear_decay_rate();
    void set_decay_rate(const double& value);
    double* mutable_decay_rate();
   protected:
    bool has_decay_rate_ = false;
    double decay_rate_;
      
      // optional field staircase
     public:
    bool has_staircase() const;
    const bool& staircase() const;
    void clear_staircase();
    void set_staircase(const bool& value);
    bool* mutable_staircase();
   protected:
    bool has_staircase_ = false;
    bool staircase_;
           
   public:
    int compare(const _NaturalExpDecayConf_& other);

    bool operator==(const _NaturalExpDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _NaturalExpDecayConf_& other) const;
  };

  ConstNaturalExpDecayConf(const ::std::shared_ptr<_NaturalExpDecayConf_>& data);
  ConstNaturalExpDecayConf(const ConstNaturalExpDecayConf&);
  ConstNaturalExpDecayConf(ConstNaturalExpDecayConf&&) noexcept;
  ConstNaturalExpDecayConf();
  ConstNaturalExpDecayConf(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf);
  virtual ~ConstNaturalExpDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_naturalexpdecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field decay_rate
 public:
  bool has_decay_rate() const;
  const double& decay_rate() const;
  // used by pybind11 only
  // required or optional field staircase
 public:
  bool has_staircase() const;
  const bool& staircase() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstNaturalExpDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstNaturalExpDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstNaturalExpDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_NaturalExpDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_NaturalExpDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstNaturalExpDecayConf
  void BuildFromProto(const PbMessage& proto_naturalexpdecayconf);
  
  ::std::shared_ptr<_NaturalExpDecayConf_> data_;
};

class NaturalExpDecayConf final : public ConstNaturalExpDecayConf {
 public:
  NaturalExpDecayConf(const ::std::shared_ptr<_NaturalExpDecayConf_>& data);
  NaturalExpDecayConf(const NaturalExpDecayConf& other);
  // enable nothrow for ::std::vector<NaturalExpDecayConf> resize 
  NaturalExpDecayConf(NaturalExpDecayConf&&) noexcept;
  NaturalExpDecayConf();
  explicit NaturalExpDecayConf(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf);

  ~NaturalExpDecayConf() override;

  void InitFromProto(const PbMessage& proto_naturalexpdecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const NaturalExpDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const NaturalExpDecayConf& other) const;
  void Clear();
  void CopyFrom(const NaturalExpDecayConf& other);
  NaturalExpDecayConf& operator=(const NaturalExpDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field decay_rate
 public:
  void clear_decay_rate();
  void set_decay_rate(const double& value);
  double* mutable_decay_rate();
  // required or optional field staircase
 public:
  void clear_staircase();
  void set_staircase(const bool& value);
  bool* mutable_staircase();

  ::std::shared_ptr<NaturalExpDecayConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_;
class Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_;
class _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_;
class Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_;

class ConstPiecewiseConstantConf : public ::oneflow::cfg::Message {
 public:

  class _PiecewiseConstantConf_ {
   public:
    _PiecewiseConstantConf_();
    explicit _PiecewiseConstantConf_(const _PiecewiseConstantConf_& other);
    explicit _PiecewiseConstantConf_(_PiecewiseConstantConf_&& other);
    _PiecewiseConstantConf_(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf);
    ~_PiecewiseConstantConf_();

    void InitFromProto(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf);

    void ToProto(::oneflow::PiecewiseConstantConf* proto_piecewiseconstantconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PiecewiseConstantConf_& other);
  
      // repeated field boundaries
   public:
    ::std::size_t boundaries_size() const;
    const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& boundaries() const;
    const int64_t& boundaries(::std::size_t index) const;
    void clear_boundaries();
    _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* mutable_boundaries();
    int64_t* mutable_boundaries(::std::size_t index);
      void add_boundaries(const int64_t& value);
    void set_boundaries(::std::size_t index, const int64_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> boundaries_;
    
      // repeated field values
   public:
    ::std::size_t values_size() const;
    const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& values() const;
    const double& values(::std::size_t index) const;
    void clear_values();
    _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* mutable_values();
    double* mutable_values(::std::size_t index);
      void add_values(const double& value);
    void set_values(::std::size_t index, const double& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> values_;
         
   public:
    int compare(const _PiecewiseConstantConf_& other);

    bool operator==(const _PiecewiseConstantConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PiecewiseConstantConf_& other) const;
  };

  ConstPiecewiseConstantConf(const ::std::shared_ptr<_PiecewiseConstantConf_>& data);
  ConstPiecewiseConstantConf(const ConstPiecewiseConstantConf&);
  ConstPiecewiseConstantConf(ConstPiecewiseConstantConf&&) noexcept;
  ConstPiecewiseConstantConf();
  ConstPiecewiseConstantConf(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf);
  virtual ~ConstPiecewiseConstantConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_piecewiseconstantconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field boundaries
 public:
  ::std::size_t boundaries_size() const;
  const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& boundaries() const;
  const int64_t& boundaries(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> shared_const_boundaries() const;
  // repeated field values
 public:
  ::std::size_t values_size() const;
  const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& values() const;
  const double& values(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> shared_const_values() const;

 public:
  ::std::shared_ptr<ConstPiecewiseConstantConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPiecewiseConstantConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPiecewiseConstantConf& other) const;
 protected:
  const ::std::shared_ptr<_PiecewiseConstantConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PiecewiseConstantConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPiecewiseConstantConf
  void BuildFromProto(const PbMessage& proto_piecewiseconstantconf);
  
  ::std::shared_ptr<_PiecewiseConstantConf_> data_;
};

class PiecewiseConstantConf final : public ConstPiecewiseConstantConf {
 public:
  PiecewiseConstantConf(const ::std::shared_ptr<_PiecewiseConstantConf_>& data);
  PiecewiseConstantConf(const PiecewiseConstantConf& other);
  // enable nothrow for ::std::vector<PiecewiseConstantConf> resize 
  PiecewiseConstantConf(PiecewiseConstantConf&&) noexcept;
  PiecewiseConstantConf();
  explicit PiecewiseConstantConf(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf);

  ~PiecewiseConstantConf() override;

  void InitFromProto(const PbMessage& proto_piecewiseconstantconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PiecewiseConstantConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PiecewiseConstantConf& other) const;
  void Clear();
  void CopyFrom(const PiecewiseConstantConf& other);
  PiecewiseConstantConf& operator=(const PiecewiseConstantConf& other);

  // repeated field boundaries
 public:
  void clear_boundaries();
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* mutable_boundaries();
  int64_t* mutable_boundaries(::std::size_t index);
  void add_boundaries(const int64_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> shared_mutable_boundaries();
  void set_boundaries(::std::size_t index, const int64_t& value);
  // repeated field values
 public:
  void clear_values();
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* mutable_values();
  double* mutable_values(::std::size_t index);
  void add_values(const double& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> shared_mutable_values();
  void set_values(::std::size_t index, const double& value);

  ::std::shared_ptr<PiecewiseConstantConf> __SharedMutable__();
};


class ConstPolynomialDecayConf : public ::oneflow::cfg::Message {
 public:

  class _PolynomialDecayConf_ {
   public:
    _PolynomialDecayConf_();
    explicit _PolynomialDecayConf_(const _PolynomialDecayConf_& other);
    explicit _PolynomialDecayConf_(_PolynomialDecayConf_&& other);
    _PolynomialDecayConf_(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf);
    ~_PolynomialDecayConf_();

    void InitFromProto(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf);

    void ToProto(::oneflow::PolynomialDecayConf* proto_polynomialdecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PolynomialDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field end_learning_rate
     public:
    bool has_end_learning_rate() const;
    const double& end_learning_rate() const;
    void clear_end_learning_rate();
    void set_end_learning_rate(const double& value);
    double* mutable_end_learning_rate();
   protected:
    bool has_end_learning_rate_ = false;
    double end_learning_rate_;
      
      // optional field power
     public:
    bool has_power() const;
    const double& power() const;
    void clear_power();
    void set_power(const double& value);
    double* mutable_power();
   protected:
    bool has_power_ = false;
    double power_;
      
      // optional field cycle
     public:
    bool has_cycle() const;
    const bool& cycle() const;
    void clear_cycle();
    void set_cycle(const bool& value);
    bool* mutable_cycle();
   protected:
    bool has_cycle_ = false;
    bool cycle_;
           
   public:
    int compare(const _PolynomialDecayConf_& other);

    bool operator==(const _PolynomialDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PolynomialDecayConf_& other) const;
  };

  ConstPolynomialDecayConf(const ::std::shared_ptr<_PolynomialDecayConf_>& data);
  ConstPolynomialDecayConf(const ConstPolynomialDecayConf&);
  ConstPolynomialDecayConf(ConstPolynomialDecayConf&&) noexcept;
  ConstPolynomialDecayConf();
  ConstPolynomialDecayConf(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf);
  virtual ~ConstPolynomialDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_polynomialdecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field end_learning_rate
 public:
  bool has_end_learning_rate() const;
  const double& end_learning_rate() const;
  // used by pybind11 only
  // required or optional field power
 public:
  bool has_power() const;
  const double& power() const;
  // used by pybind11 only
  // required or optional field cycle
 public:
  bool has_cycle() const;
  const bool& cycle() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstPolynomialDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPolynomialDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPolynomialDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_PolynomialDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PolynomialDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPolynomialDecayConf
  void BuildFromProto(const PbMessage& proto_polynomialdecayconf);
  
  ::std::shared_ptr<_PolynomialDecayConf_> data_;
};

class PolynomialDecayConf final : public ConstPolynomialDecayConf {
 public:
  PolynomialDecayConf(const ::std::shared_ptr<_PolynomialDecayConf_>& data);
  PolynomialDecayConf(const PolynomialDecayConf& other);
  // enable nothrow for ::std::vector<PolynomialDecayConf> resize 
  PolynomialDecayConf(PolynomialDecayConf&&) noexcept;
  PolynomialDecayConf();
  explicit PolynomialDecayConf(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf);

  ~PolynomialDecayConf() override;

  void InitFromProto(const PbMessage& proto_polynomialdecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PolynomialDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PolynomialDecayConf& other) const;
  void Clear();
  void CopyFrom(const PolynomialDecayConf& other);
  PolynomialDecayConf& operator=(const PolynomialDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field end_learning_rate
 public:
  void clear_end_learning_rate();
  void set_end_learning_rate(const double& value);
  double* mutable_end_learning_rate();
  // required or optional field power
 public:
  void clear_power();
  void set_power(const double& value);
  double* mutable_power();
  // required or optional field cycle
 public:
  void clear_cycle();
  void set_cycle(const bool& value);
  bool* mutable_cycle();

  ::std::shared_ptr<PolynomialDecayConf> __SharedMutable__();
};


class ConstCosineDecayConf : public ::oneflow::cfg::Message {
 public:

  class _CosineDecayConf_ {
   public:
    _CosineDecayConf_();
    explicit _CosineDecayConf_(const _CosineDecayConf_& other);
    explicit _CosineDecayConf_(_CosineDecayConf_&& other);
    _CosineDecayConf_(const ::oneflow::CosineDecayConf& proto_cosinedecayconf);
    ~_CosineDecayConf_();

    void InitFromProto(const ::oneflow::CosineDecayConf& proto_cosinedecayconf);

    void ToProto(::oneflow::CosineDecayConf* proto_cosinedecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CosineDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field alpha
     public:
    bool has_alpha() const;
    const double& alpha() const;
    void clear_alpha();
    void set_alpha(const double& value);
    double* mutable_alpha();
   protected:
    bool has_alpha_ = false;
    double alpha_;
           
   public:
    int compare(const _CosineDecayConf_& other);

    bool operator==(const _CosineDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CosineDecayConf_& other) const;
  };

  ConstCosineDecayConf(const ::std::shared_ptr<_CosineDecayConf_>& data);
  ConstCosineDecayConf(const ConstCosineDecayConf&);
  ConstCosineDecayConf(ConstCosineDecayConf&&) noexcept;
  ConstCosineDecayConf();
  ConstCosineDecayConf(const ::oneflow::CosineDecayConf& proto_cosinedecayconf);
  virtual ~ConstCosineDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_cosinedecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field alpha
 public:
  bool has_alpha() const;
  const double& alpha() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstCosineDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCosineDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCosineDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_CosineDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CosineDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCosineDecayConf
  void BuildFromProto(const PbMessage& proto_cosinedecayconf);
  
  ::std::shared_ptr<_CosineDecayConf_> data_;
};

class CosineDecayConf final : public ConstCosineDecayConf {
 public:
  CosineDecayConf(const ::std::shared_ptr<_CosineDecayConf_>& data);
  CosineDecayConf(const CosineDecayConf& other);
  // enable nothrow for ::std::vector<CosineDecayConf> resize 
  CosineDecayConf(CosineDecayConf&&) noexcept;
  CosineDecayConf();
  explicit CosineDecayConf(const ::oneflow::CosineDecayConf& proto_cosinedecayconf);

  ~CosineDecayConf() override;

  void InitFromProto(const PbMessage& proto_cosinedecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CosineDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CosineDecayConf& other) const;
  void Clear();
  void CopyFrom(const CosineDecayConf& other);
  CosineDecayConf& operator=(const CosineDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field alpha
 public:
  void clear_alpha();
  void set_alpha(const double& value);
  double* mutable_alpha();

  ::std::shared_ptr<CosineDecayConf> __SharedMutable__();
};


class ConstLinearCosineDecayConf : public ::oneflow::cfg::Message {
 public:

  class _LinearCosineDecayConf_ {
   public:
    _LinearCosineDecayConf_();
    explicit _LinearCosineDecayConf_(const _LinearCosineDecayConf_& other);
    explicit _LinearCosineDecayConf_(_LinearCosineDecayConf_&& other);
    _LinearCosineDecayConf_(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf);
    ~_LinearCosineDecayConf_();

    void InitFromProto(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf);

    void ToProto(::oneflow::LinearCosineDecayConf* proto_linearcosinedecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LinearCosineDecayConf_& other);
  
      // optional field decay_batches
     public:
    bool has_decay_batches() const;
    const int64_t& decay_batches() const;
    void clear_decay_batches();
    void set_decay_batches(const int64_t& value);
    int64_t* mutable_decay_batches();
   protected:
    bool has_decay_batches_ = false;
    int64_t decay_batches_;
      
      // optional field num_periods
     public:
    bool has_num_periods() const;
    const double& num_periods() const;
    void clear_num_periods();
    void set_num_periods(const double& value);
    double* mutable_num_periods();
   protected:
    bool has_num_periods_ = false;
    double num_periods_;
      
      // optional field alpha
     public:
    bool has_alpha() const;
    const double& alpha() const;
    void clear_alpha();
    void set_alpha(const double& value);
    double* mutable_alpha();
   protected:
    bool has_alpha_ = false;
    double alpha_;
      
      // optional field beta
     public:
    bool has_beta() const;
    const double& beta() const;
    void clear_beta();
    void set_beta(const double& value);
    double* mutable_beta();
   protected:
    bool has_beta_ = false;
    double beta_;
           
   public:
    int compare(const _LinearCosineDecayConf_& other);

    bool operator==(const _LinearCosineDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LinearCosineDecayConf_& other) const;
  };

  ConstLinearCosineDecayConf(const ::std::shared_ptr<_LinearCosineDecayConf_>& data);
  ConstLinearCosineDecayConf(const ConstLinearCosineDecayConf&);
  ConstLinearCosineDecayConf(ConstLinearCosineDecayConf&&) noexcept;
  ConstLinearCosineDecayConf();
  ConstLinearCosineDecayConf(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf);
  virtual ~ConstLinearCosineDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_linearcosinedecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_batches
 public:
  bool has_decay_batches() const;
  const int64_t& decay_batches() const;
  // used by pybind11 only
  // required or optional field num_periods
 public:
  bool has_num_periods() const;
  const double& num_periods() const;
  // used by pybind11 only
  // required or optional field alpha
 public:
  bool has_alpha() const;
  const double& alpha() const;
  // used by pybind11 only
  // required or optional field beta
 public:
  bool has_beta() const;
  const double& beta() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLinearCosineDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLinearCosineDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLinearCosineDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_LinearCosineDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LinearCosineDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLinearCosineDecayConf
  void BuildFromProto(const PbMessage& proto_linearcosinedecayconf);
  
  ::std::shared_ptr<_LinearCosineDecayConf_> data_;
};

class LinearCosineDecayConf final : public ConstLinearCosineDecayConf {
 public:
  LinearCosineDecayConf(const ::std::shared_ptr<_LinearCosineDecayConf_>& data);
  LinearCosineDecayConf(const LinearCosineDecayConf& other);
  // enable nothrow for ::std::vector<LinearCosineDecayConf> resize 
  LinearCosineDecayConf(LinearCosineDecayConf&&) noexcept;
  LinearCosineDecayConf();
  explicit LinearCosineDecayConf(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf);

  ~LinearCosineDecayConf() override;

  void InitFromProto(const PbMessage& proto_linearcosinedecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LinearCosineDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LinearCosineDecayConf& other) const;
  void Clear();
  void CopyFrom(const LinearCosineDecayConf& other);
  LinearCosineDecayConf& operator=(const LinearCosineDecayConf& other);

  // required or optional field decay_batches
 public:
  void clear_decay_batches();
  void set_decay_batches(const int64_t& value);
  int64_t* mutable_decay_batches();
  // required or optional field num_periods
 public:
  void clear_num_periods();
  void set_num_periods(const double& value);
  double* mutable_num_periods();
  // required or optional field alpha
 public:
  void clear_alpha();
  void set_alpha(const double& value);
  double* mutable_alpha();
  // required or optional field beta
 public:
  void clear_beta();
  void set_beta(const double& value);
  double* mutable_beta();

  ::std::shared_ptr<LinearCosineDecayConf> __SharedMutable__();
};


class ConstPiecewiseScalingConf : public ::oneflow::cfg::Message {
 public:

  class _PiecewiseScalingConf_ {
   public:
    _PiecewiseScalingConf_();
    explicit _PiecewiseScalingConf_(const _PiecewiseScalingConf_& other);
    explicit _PiecewiseScalingConf_(_PiecewiseScalingConf_&& other);
    _PiecewiseScalingConf_(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf);
    ~_PiecewiseScalingConf_();

    void InitFromProto(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf);

    void ToProto(::oneflow::PiecewiseScalingConf* proto_piecewisescalingconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PiecewiseScalingConf_& other);
  
      // repeated field boundaries
   public:
    ::std::size_t boundaries_size() const;
    const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& boundaries() const;
    const int64_t& boundaries(::std::size_t index) const;
    void clear_boundaries();
    _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* mutable_boundaries();
    int64_t* mutable_boundaries(::std::size_t index);
      void add_boundaries(const int64_t& value);
    void set_boundaries(::std::size_t index, const int64_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> boundaries_;
    
      // repeated field scales
   public:
    ::std::size_t scales_size() const;
    const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& scales() const;
    const double& scales(::std::size_t index) const;
    void clear_scales();
    _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* mutable_scales();
    double* mutable_scales(::std::size_t index);
      void add_scales(const double& value);
    void set_scales(::std::size_t index, const double& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> scales_;
         
   public:
    int compare(const _PiecewiseScalingConf_& other);

    bool operator==(const _PiecewiseScalingConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PiecewiseScalingConf_& other) const;
  };

  ConstPiecewiseScalingConf(const ::std::shared_ptr<_PiecewiseScalingConf_>& data);
  ConstPiecewiseScalingConf(const ConstPiecewiseScalingConf&);
  ConstPiecewiseScalingConf(ConstPiecewiseScalingConf&&) noexcept;
  ConstPiecewiseScalingConf();
  ConstPiecewiseScalingConf(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf);
  virtual ~ConstPiecewiseScalingConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_piecewisescalingconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field boundaries
 public:
  ::std::size_t boundaries_size() const;
  const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& boundaries() const;
  const int64_t& boundaries(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> shared_const_boundaries() const;
  // repeated field scales
 public:
  ::std::size_t scales_size() const;
  const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& scales() const;
  const double& scales(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> shared_const_scales() const;

 public:
  ::std::shared_ptr<ConstPiecewiseScalingConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPiecewiseScalingConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPiecewiseScalingConf& other) const;
 protected:
  const ::std::shared_ptr<_PiecewiseScalingConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PiecewiseScalingConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPiecewiseScalingConf
  void BuildFromProto(const PbMessage& proto_piecewisescalingconf);
  
  ::std::shared_ptr<_PiecewiseScalingConf_> data_;
};

class PiecewiseScalingConf final : public ConstPiecewiseScalingConf {
 public:
  PiecewiseScalingConf(const ::std::shared_ptr<_PiecewiseScalingConf_>& data);
  PiecewiseScalingConf(const PiecewiseScalingConf& other);
  // enable nothrow for ::std::vector<PiecewiseScalingConf> resize 
  PiecewiseScalingConf(PiecewiseScalingConf&&) noexcept;
  PiecewiseScalingConf();
  explicit PiecewiseScalingConf(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf);

  ~PiecewiseScalingConf() override;

  void InitFromProto(const PbMessage& proto_piecewisescalingconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PiecewiseScalingConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PiecewiseScalingConf& other) const;
  void Clear();
  void CopyFrom(const PiecewiseScalingConf& other);
  PiecewiseScalingConf& operator=(const PiecewiseScalingConf& other);

  // repeated field boundaries
 public:
  void clear_boundaries();
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* mutable_boundaries();
  int64_t* mutable_boundaries(::std::size_t index);
  void add_boundaries(const int64_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> shared_mutable_boundaries();
  void set_boundaries(::std::size_t index, const int64_t& value);
  // repeated field scales
 public:
  void clear_scales();
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* mutable_scales();
  double* mutable_scales(::std::size_t index);
  void add_scales(const double& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> shared_mutable_scales();
  void set_scales(::std::size_t index, const double& value);

  ::std::shared_ptr<PiecewiseScalingConf> __SharedMutable__();
};


class ConstLearningRateDecayConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kExponentialConf = 2000,
    kInverseTimeConf = 2001,
    kNaturalExpConf = 2002,
    kPiecewiseConstantConf = 2003,
    kPolynomialConf = 2004,
    kCosineConf = 2005,
    kLinearCosineConf = 2006,
    kPiecewiseScalingConf = 2007,
   };

  class _LearningRateDecayConf_ {
   public:
    _LearningRateDecayConf_();
    explicit _LearningRateDecayConf_(const _LearningRateDecayConf_& other);
    explicit _LearningRateDecayConf_(_LearningRateDecayConf_&& other);
    _LearningRateDecayConf_(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf);
    ~_LearningRateDecayConf_();

    void InitFromProto(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf);

    void ToProto(::oneflow::LearningRateDecayConf* proto_learningratedecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LearningRateDecayConf_& other);
  
     // oneof field type: exponential_conf
   public:
    bool has_exponential_conf() const;
    void clear_exponential_conf();
    const ::oneflow::cfg::ExponentialDecayConf& exponential_conf() const;
      ::oneflow::cfg::ExponentialDecayConf* mutable_exponential_conf();
      
     // oneof field type: inverse_time_conf
   public:
    bool has_inverse_time_conf() const;
    void clear_inverse_time_conf();
    const ::oneflow::cfg::InverseTimeDecayConf& inverse_time_conf() const;
      ::oneflow::cfg::InverseTimeDecayConf* mutable_inverse_time_conf();
      
     // oneof field type: natural_exp_conf
   public:
    bool has_natural_exp_conf() const;
    void clear_natural_exp_conf();
    const ::oneflow::cfg::NaturalExpDecayConf& natural_exp_conf() const;
      ::oneflow::cfg::NaturalExpDecayConf* mutable_natural_exp_conf();
      
     // oneof field type: piecewise_constant_conf
   public:
    bool has_piecewise_constant_conf() const;
    void clear_piecewise_constant_conf();
    const ::oneflow::cfg::PiecewiseConstantConf& piecewise_constant_conf() const;
      ::oneflow::cfg::PiecewiseConstantConf* mutable_piecewise_constant_conf();
      
     // oneof field type: polynomial_conf
   public:
    bool has_polynomial_conf() const;
    void clear_polynomial_conf();
    const ::oneflow::cfg::PolynomialDecayConf& polynomial_conf() const;
      ::oneflow::cfg::PolynomialDecayConf* mutable_polynomial_conf();
      
     // oneof field type: cosine_conf
   public:
    bool has_cosine_conf() const;
    void clear_cosine_conf();
    const ::oneflow::cfg::CosineDecayConf& cosine_conf() const;
      ::oneflow::cfg::CosineDecayConf* mutable_cosine_conf();
      
     // oneof field type: linear_cosine_conf
   public:
    bool has_linear_cosine_conf() const;
    void clear_linear_cosine_conf();
    const ::oneflow::cfg::LinearCosineDecayConf& linear_cosine_conf() const;
      ::oneflow::cfg::LinearCosineDecayConf* mutable_linear_cosine_conf();
      
     // oneof field type: piecewise_scaling_conf
   public:
    bool has_piecewise_scaling_conf() const;
    void clear_piecewise_scaling_conf();
    const ::oneflow::cfg::PiecewiseScalingConf& piecewise_scaling_conf() const;
      ::oneflow::cfg::PiecewiseScalingConf* mutable_piecewise_scaling_conf();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _LearningRateDecayConf_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char exponential_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>)];
            char inverse_time_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>)];
            char natural_exp_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>)];
            char piecewise_constant_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>)];
            char polynomial_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>)];
            char cosine_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CosineDecayConf>)];
            char linear_cosine_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>)];
            char piecewise_scaling_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _LearningRateDecayConf_& other);

    bool operator==(const _LearningRateDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LearningRateDecayConf_& other) const;
  };

  ConstLearningRateDecayConf(const ::std::shared_ptr<_LearningRateDecayConf_>& data);
  ConstLearningRateDecayConf(const ConstLearningRateDecayConf&);
  ConstLearningRateDecayConf(ConstLearningRateDecayConf&&) noexcept;
  ConstLearningRateDecayConf();
  ConstLearningRateDecayConf(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf);
  virtual ~ConstLearningRateDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_learningratedecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: exponential_conf
 public:
  bool has_exponential_conf() const;
  const ::oneflow::cfg::ExponentialDecayConf& exponential_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstExponentialDecayConf> shared_const_exponential_conf() const;
 // oneof field type: inverse_time_conf
 public:
  bool has_inverse_time_conf() const;
  const ::oneflow::cfg::InverseTimeDecayConf& inverse_time_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInverseTimeDecayConf> shared_const_inverse_time_conf() const;
 // oneof field type: natural_exp_conf
 public:
  bool has_natural_exp_conf() const;
  const ::oneflow::cfg::NaturalExpDecayConf& natural_exp_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstNaturalExpDecayConf> shared_const_natural_exp_conf() const;
 // oneof field type: piecewise_constant_conf
 public:
  bool has_piecewise_constant_conf() const;
  const ::oneflow::cfg::PiecewiseConstantConf& piecewise_constant_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPiecewiseConstantConf> shared_const_piecewise_constant_conf() const;
 // oneof field type: polynomial_conf
 public:
  bool has_polynomial_conf() const;
  const ::oneflow::cfg::PolynomialDecayConf& polynomial_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPolynomialDecayConf> shared_const_polynomial_conf() const;
 // oneof field type: cosine_conf
 public:
  bool has_cosine_conf() const;
  const ::oneflow::cfg::CosineDecayConf& cosine_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCosineDecayConf> shared_const_cosine_conf() const;
 // oneof field type: linear_cosine_conf
 public:
  bool has_linear_cosine_conf() const;
  const ::oneflow::cfg::LinearCosineDecayConf& linear_cosine_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLinearCosineDecayConf> shared_const_linear_cosine_conf() const;
 // oneof field type: piecewise_scaling_conf
 public:
  bool has_piecewise_scaling_conf() const;
  const ::oneflow::cfg::PiecewiseScalingConf& piecewise_scaling_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPiecewiseScalingConf> shared_const_piecewise_scaling_conf() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstLearningRateDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLearningRateDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLearningRateDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_LearningRateDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LearningRateDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLearningRateDecayConf
  void BuildFromProto(const PbMessage& proto_learningratedecayconf);
  
  ::std::shared_ptr<_LearningRateDecayConf_> data_;
};

class LearningRateDecayConf final : public ConstLearningRateDecayConf {
 public:
  LearningRateDecayConf(const ::std::shared_ptr<_LearningRateDecayConf_>& data);
  LearningRateDecayConf(const LearningRateDecayConf& other);
  // enable nothrow for ::std::vector<LearningRateDecayConf> resize 
  LearningRateDecayConf(LearningRateDecayConf&&) noexcept;
  LearningRateDecayConf();
  explicit LearningRateDecayConf(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf);

  ~LearningRateDecayConf() override;

  void InitFromProto(const PbMessage& proto_learningratedecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LearningRateDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LearningRateDecayConf& other) const;
  void Clear();
  void CopyFrom(const LearningRateDecayConf& other);
  LearningRateDecayConf& operator=(const LearningRateDecayConf& other);

  void clear_exponential_conf();
  ::oneflow::cfg::ExponentialDecayConf* mutable_exponential_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf> shared_mutable_exponential_conf();
  void clear_inverse_time_conf();
  ::oneflow::cfg::InverseTimeDecayConf* mutable_inverse_time_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf> shared_mutable_inverse_time_conf();
  void clear_natural_exp_conf();
  ::oneflow::cfg::NaturalExpDecayConf* mutable_natural_exp_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf> shared_mutable_natural_exp_conf();
  void clear_piecewise_constant_conf();
  ::oneflow::cfg::PiecewiseConstantConf* mutable_piecewise_constant_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf> shared_mutable_piecewise_constant_conf();
  void clear_polynomial_conf();
  ::oneflow::cfg::PolynomialDecayConf* mutable_polynomial_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf> shared_mutable_polynomial_conf();
  void clear_cosine_conf();
  ::oneflow::cfg::CosineDecayConf* mutable_cosine_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CosineDecayConf> shared_mutable_cosine_conf();
  void clear_linear_cosine_conf();
  ::oneflow::cfg::LinearCosineDecayConf* mutable_linear_cosine_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf> shared_mutable_linear_cosine_conf();
  void clear_piecewise_scaling_conf();
  ::oneflow::cfg::PiecewiseScalingConf* mutable_piecewise_scaling_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf> shared_mutable_piecewise_scaling_conf();

  ::std::shared_ptr<LearningRateDecayConf> __SharedMutable__();
};


class ConstConstantWarmupConf : public ::oneflow::cfg::Message {
 public:

  class _ConstantWarmupConf_ {
   public:
    _ConstantWarmupConf_();
    explicit _ConstantWarmupConf_(const _ConstantWarmupConf_& other);
    explicit _ConstantWarmupConf_(_ConstantWarmupConf_&& other);
    _ConstantWarmupConf_(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf);
    ~_ConstantWarmupConf_();

    void InitFromProto(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf);

    void ToProto(::oneflow::ConstantWarmupConf* proto_constantwarmupconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConstantWarmupConf_& other);
  
      // optional field warmup_batches
     public:
    bool has_warmup_batches() const;
    const int64_t& warmup_batches() const;
    void clear_warmup_batches();
    void set_warmup_batches(const int64_t& value);
    int64_t* mutable_warmup_batches();
   protected:
    bool has_warmup_batches_ = false;
    int64_t warmup_batches_;
      
      // optional field multiplier
     public:
    bool has_multiplier() const;
    const double& multiplier() const;
    void clear_multiplier();
    void set_multiplier(const double& value);
    double* mutable_multiplier();
   protected:
    bool has_multiplier_ = false;
    double multiplier_;
           
   public:
    int compare(const _ConstantWarmupConf_& other);

    bool operator==(const _ConstantWarmupConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConstantWarmupConf_& other) const;
  };

  ConstConstantWarmupConf(const ::std::shared_ptr<_ConstantWarmupConf_>& data);
  ConstConstantWarmupConf(const ConstConstantWarmupConf&);
  ConstConstantWarmupConf(ConstConstantWarmupConf&&) noexcept;
  ConstConstantWarmupConf();
  ConstConstantWarmupConf(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf);
  virtual ~ConstConstantWarmupConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_constantwarmupconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field warmup_batches
 public:
  bool has_warmup_batches() const;
  const int64_t& warmup_batches() const;
  // used by pybind11 only
  // required or optional field multiplier
 public:
  bool has_multiplier() const;
  const double& multiplier() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstConstantWarmupConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConstantWarmupConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConstantWarmupConf& other) const;
 protected:
  const ::std::shared_ptr<_ConstantWarmupConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConstantWarmupConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConstantWarmupConf
  void BuildFromProto(const PbMessage& proto_constantwarmupconf);
  
  ::std::shared_ptr<_ConstantWarmupConf_> data_;
};

class ConstantWarmupConf final : public ConstConstantWarmupConf {
 public:
  ConstantWarmupConf(const ::std::shared_ptr<_ConstantWarmupConf_>& data);
  ConstantWarmupConf(const ConstantWarmupConf& other);
  // enable nothrow for ::std::vector<ConstantWarmupConf> resize 
  ConstantWarmupConf(ConstantWarmupConf&&) noexcept;
  ConstantWarmupConf();
  explicit ConstantWarmupConf(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf);

  ~ConstantWarmupConf() override;

  void InitFromProto(const PbMessage& proto_constantwarmupconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConstantWarmupConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConstantWarmupConf& other) const;
  void Clear();
  void CopyFrom(const ConstantWarmupConf& other);
  ConstantWarmupConf& operator=(const ConstantWarmupConf& other);

  // required or optional field warmup_batches
 public:
  void clear_warmup_batches();
  void set_warmup_batches(const int64_t& value);
  int64_t* mutable_warmup_batches();
  // required or optional field multiplier
 public:
  void clear_multiplier();
  void set_multiplier(const double& value);
  double* mutable_multiplier();

  ::std::shared_ptr<ConstantWarmupConf> __SharedMutable__();
};


class ConstLinearWarmupConf : public ::oneflow::cfg::Message {
 public:

  class _LinearWarmupConf_ {
   public:
    _LinearWarmupConf_();
    explicit _LinearWarmupConf_(const _LinearWarmupConf_& other);
    explicit _LinearWarmupConf_(_LinearWarmupConf_&& other);
    _LinearWarmupConf_(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf);
    ~_LinearWarmupConf_();

    void InitFromProto(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf);

    void ToProto(::oneflow::LinearWarmupConf* proto_linearwarmupconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LinearWarmupConf_& other);
  
      // optional field warmup_batches
     public:
    bool has_warmup_batches() const;
    const int64_t& warmup_batches() const;
    void clear_warmup_batches();
    void set_warmup_batches(const int64_t& value);
    int64_t* mutable_warmup_batches();
   protected:
    bool has_warmup_batches_ = false;
    int64_t warmup_batches_;
      
      // optional field start_multiplier
     public:
    bool has_start_multiplier() const;
    const double& start_multiplier() const;
    void clear_start_multiplier();
    void set_start_multiplier(const double& value);
    double* mutable_start_multiplier();
   protected:
    bool has_start_multiplier_ = false;
    double start_multiplier_;
           
   public:
    int compare(const _LinearWarmupConf_& other);

    bool operator==(const _LinearWarmupConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LinearWarmupConf_& other) const;
  };

  ConstLinearWarmupConf(const ::std::shared_ptr<_LinearWarmupConf_>& data);
  ConstLinearWarmupConf(const ConstLinearWarmupConf&);
  ConstLinearWarmupConf(ConstLinearWarmupConf&&) noexcept;
  ConstLinearWarmupConf();
  ConstLinearWarmupConf(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf);
  virtual ~ConstLinearWarmupConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_linearwarmupconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field warmup_batches
 public:
  bool has_warmup_batches() const;
  const int64_t& warmup_batches() const;
  // used by pybind11 only
  // required or optional field start_multiplier
 public:
  bool has_start_multiplier() const;
  const double& start_multiplier() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLinearWarmupConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLinearWarmupConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLinearWarmupConf& other) const;
 protected:
  const ::std::shared_ptr<_LinearWarmupConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LinearWarmupConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLinearWarmupConf
  void BuildFromProto(const PbMessage& proto_linearwarmupconf);
  
  ::std::shared_ptr<_LinearWarmupConf_> data_;
};

class LinearWarmupConf final : public ConstLinearWarmupConf {
 public:
  LinearWarmupConf(const ::std::shared_ptr<_LinearWarmupConf_>& data);
  LinearWarmupConf(const LinearWarmupConf& other);
  // enable nothrow for ::std::vector<LinearWarmupConf> resize 
  LinearWarmupConf(LinearWarmupConf&&) noexcept;
  LinearWarmupConf();
  explicit LinearWarmupConf(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf);

  ~LinearWarmupConf() override;

  void InitFromProto(const PbMessage& proto_linearwarmupconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LinearWarmupConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LinearWarmupConf& other) const;
  void Clear();
  void CopyFrom(const LinearWarmupConf& other);
  LinearWarmupConf& operator=(const LinearWarmupConf& other);

  // required or optional field warmup_batches
 public:
  void clear_warmup_batches();
  void set_warmup_batches(const int64_t& value);
  int64_t* mutable_warmup_batches();
  // required or optional field start_multiplier
 public:
  void clear_start_multiplier();
  void set_start_multiplier(const double& value);
  double* mutable_start_multiplier();

  ::std::shared_ptr<LinearWarmupConf> __SharedMutable__();
};


class ConstWarmupConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kConstantConf = 3000,
    kLinearConf = 3001,
   };

  class _WarmupConf_ {
   public:
    _WarmupConf_();
    explicit _WarmupConf_(const _WarmupConf_& other);
    explicit _WarmupConf_(_WarmupConf_&& other);
    _WarmupConf_(const ::oneflow::WarmupConf& proto_warmupconf);
    ~_WarmupConf_();

    void InitFromProto(const ::oneflow::WarmupConf& proto_warmupconf);

    void ToProto(::oneflow::WarmupConf* proto_warmupconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _WarmupConf_& other);
  
     // oneof field type: constant_conf
   public:
    bool has_constant_conf() const;
    void clear_constant_conf();
    const ::oneflow::cfg::ConstantWarmupConf& constant_conf() const;
      ::oneflow::cfg::ConstantWarmupConf* mutable_constant_conf();
      
     // oneof field type: linear_conf
   public:
    bool has_linear_conf() const;
    void clear_linear_conf();
    const ::oneflow::cfg::LinearWarmupConf& linear_conf() const;
      ::oneflow::cfg::LinearWarmupConf* mutable_linear_conf();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _WarmupConf_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char constant_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>)];
            char linear_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _WarmupConf_& other);

    bool operator==(const _WarmupConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _WarmupConf_& other) const;
  };

  ConstWarmupConf(const ::std::shared_ptr<_WarmupConf_>& data);
  ConstWarmupConf(const ConstWarmupConf&);
  ConstWarmupConf(ConstWarmupConf&&) noexcept;
  ConstWarmupConf();
  ConstWarmupConf(const ::oneflow::WarmupConf& proto_warmupconf);
  virtual ~ConstWarmupConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_warmupconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: constant_conf
 public:
  bool has_constant_conf() const;
  const ::oneflow::cfg::ConstantWarmupConf& constant_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConstantWarmupConf> shared_const_constant_conf() const;
 // oneof field type: linear_conf
 public:
  bool has_linear_conf() const;
  const ::oneflow::cfg::LinearWarmupConf& linear_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLinearWarmupConf> shared_const_linear_conf() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstWarmupConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstWarmupConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstWarmupConf& other) const;
 protected:
  const ::std::shared_ptr<_WarmupConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_WarmupConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstWarmupConf
  void BuildFromProto(const PbMessage& proto_warmupconf);
  
  ::std::shared_ptr<_WarmupConf_> data_;
};

class WarmupConf final : public ConstWarmupConf {
 public:
  WarmupConf(const ::std::shared_ptr<_WarmupConf_>& data);
  WarmupConf(const WarmupConf& other);
  // enable nothrow for ::std::vector<WarmupConf> resize 
  WarmupConf(WarmupConf&&) noexcept;
  WarmupConf();
  explicit WarmupConf(const ::oneflow::WarmupConf& proto_warmupconf);

  ~WarmupConf() override;

  void InitFromProto(const PbMessage& proto_warmupconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const WarmupConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const WarmupConf& other) const;
  void Clear();
  void CopyFrom(const WarmupConf& other);
  WarmupConf& operator=(const WarmupConf& other);

  void clear_constant_conf();
  ::oneflow::cfg::ConstantWarmupConf* mutable_constant_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf> shared_mutable_constant_conf();
  void clear_linear_conf();
  ::oneflow::cfg::LinearWarmupConf* mutable_linear_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf> shared_mutable_linear_conf();

  ::std::shared_ptr<WarmupConf> __SharedMutable__();
};













// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_ : public ::oneflow::cfg::_RepeatedField_<int64_t> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_();
  ~Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_ final : public Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_();
  ~_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> __SharedMutable__();
};

// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_ : public ::oneflow::cfg::_RepeatedField_<double> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_();
  ~Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_ final : public Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data);
  _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_();
  ~_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> __SharedMutable__();
};



























} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstExponentialDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstExponentialDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ExponentialDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ExponentialDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInverseTimeDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstInverseTimeDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InverseTimeDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::InverseTimeDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstNaturalExpDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstNaturalExpDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::NaturalExpDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::NaturalExpDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPiecewiseConstantConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstPiecewiseConstantConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PiecewiseConstantConf> {
  std::size_t operator()(const ::oneflow::cfg::PiecewiseConstantConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPolynomialDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstPolynomialDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PolynomialDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::PolynomialDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCosineDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCosineDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CosineDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::CosineDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLinearCosineDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLinearCosineDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LinearCosineDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::LinearCosineDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPiecewiseScalingConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstPiecewiseScalingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PiecewiseScalingConf> {
  std::size_t operator()(const ::oneflow::cfg::PiecewiseScalingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLearningRateDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLearningRateDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LearningRateDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::LearningRateDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstConstantWarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstConstantWarmupConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstantWarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstantWarmupConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLinearWarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLinearWarmupConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LinearWarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::LinearWarmupConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstWarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstWarmupConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::WarmupConf> {
  std::size_t operator()(const ::oneflow::cfg::WarmupConf& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H_