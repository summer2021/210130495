#ifndef CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstParallelConf;
class ParallelConf;
}
}

namespace oneflow {

// forward declare proto class;
class ParallelConfSignature_BnInOp2parallelConfEntry;
class ParallelConfSignature;

namespace cfg {


class ParallelConfSignature;
class ConstParallelConfSignature;


class _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_; 
class Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_;

class ConstParallelConfSignature : public ::oneflow::cfg::Message {
 public:

  class _ParallelConfSignature_ {
   public:
    _ParallelConfSignature_();
    explicit _ParallelConfSignature_(const _ParallelConfSignature_& other);
    explicit _ParallelConfSignature_(_ParallelConfSignature_&& other);
    _ParallelConfSignature_(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature);
    ~_ParallelConfSignature_();

    void InitFromProto(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature);

    void ToProto(::oneflow::ParallelConfSignature* proto_parallelconfsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelConfSignature_& other);
  
      // optional field op_parallel_conf
     public:
    bool has_op_parallel_conf() const;
    const ::oneflow::cfg::ParallelConf& op_parallel_conf() const;
    void clear_op_parallel_conf();
    ::oneflow::cfg::ParallelConf* mutable_op_parallel_conf();
   protected:
    bool has_op_parallel_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelConf> op_parallel_conf_;
      
     public:
    ::std::size_t bn_in_op2parallel_conf_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& bn_in_op2parallel_conf() const;

    _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ * mutable_bn_in_op2parallel_conf();

    const ::oneflow::cfg::ParallelConf& bn_in_op2parallel_conf(::std::string key) const;

    void clear_bn_in_op2parallel_conf();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> bn_in_op2parallel_conf_;
         
   public:
    int compare(const _ParallelConfSignature_& other);

    bool operator==(const _ParallelConfSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelConfSignature_& other) const;
  };

  ConstParallelConfSignature(const ::std::shared_ptr<_ParallelConfSignature_>& data);
  ConstParallelConfSignature(const ConstParallelConfSignature&);
  ConstParallelConfSignature(ConstParallelConfSignature&&) noexcept;
  ConstParallelConfSignature();
  ConstParallelConfSignature(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature);
  virtual ~ConstParallelConfSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_parallelconfsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_parallel_conf
 public:
  bool has_op_parallel_conf() const;
  const ::oneflow::cfg::ParallelConf& op_parallel_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConf> shared_const_op_parallel_conf() const;
  // map field bn_in_op2parallel_conf
 public:
  ::std::size_t bn_in_op2parallel_conf_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& bn_in_op2parallel_conf() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> shared_const_bn_in_op2parallel_conf() const;

 public:
  ::std::shared_ptr<ConstParallelConfSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelConfSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelConfSignature& other) const;
 protected:
  const ::std::shared_ptr<_ParallelConfSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelConfSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelConfSignature
  void BuildFromProto(const PbMessage& proto_parallelconfsignature);
  
  ::std::shared_ptr<_ParallelConfSignature_> data_;
};

class ParallelConfSignature final : public ConstParallelConfSignature {
 public:
  ParallelConfSignature(const ::std::shared_ptr<_ParallelConfSignature_>& data);
  ParallelConfSignature(const ParallelConfSignature& other);
  // enable nothrow for ::std::vector<ParallelConfSignature> resize 
  ParallelConfSignature(ParallelConfSignature&&) noexcept;
  ParallelConfSignature();
  explicit ParallelConfSignature(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature);

  ~ParallelConfSignature() override;

  void InitFromProto(const PbMessage& proto_parallelconfsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelConfSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelConfSignature& other) const;
  void Clear();
  void CopyFrom(const ParallelConfSignature& other);
  ParallelConfSignature& operator=(const ParallelConfSignature& other);

  // required or optional field op_parallel_conf
 public:
  void clear_op_parallel_conf();
  ::oneflow::cfg::ParallelConf* mutable_op_parallel_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConf> shared_mutable_op_parallel_conf();
  // repeated field bn_in_op2parallel_conf
 public:
  void clear_bn_in_op2parallel_conf();

  const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ & bn_in_op2parallel_conf() const;

  _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_* mutable_bn_in_op2parallel_conf();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> shared_mutable_bn_in_op2parallel_conf();

  ::std::shared_ptr<ParallelConfSignature> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelConf> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelConf>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::ParallelConf& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstParallelConf> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ConstParallelConf>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ final : public Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelConf>>& data);
  _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_();
  ~_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::ParallelConf> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ::oneflow::cfg::ParallelConf>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstParallelConfSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelConfSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelConfSignature> {
  std::size_t operator()(const ::oneflow::cfg::ParallelConfSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H_