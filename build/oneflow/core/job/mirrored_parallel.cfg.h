#ifndef CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class MirroredParallel;
class OptMirroredParallel;
class MirroredSignature_BnInOp2optMirroredParallelEntry;
class MirroredSignature;

namespace cfg {


class MirroredParallel;
class ConstMirroredParallel;

class OptMirroredParallel;
class ConstOptMirroredParallel;

class MirroredSignature;
class ConstMirroredSignature;



class ConstMirroredParallel : public ::oneflow::cfg::Message {
 public:

  class _MirroredParallel_ {
   public:
    _MirroredParallel_();
    explicit _MirroredParallel_(const _MirroredParallel_& other);
    explicit _MirroredParallel_(_MirroredParallel_&& other);
    _MirroredParallel_(const ::oneflow::MirroredParallel& proto_mirroredparallel);
    ~_MirroredParallel_();

    void InitFromProto(const ::oneflow::MirroredParallel& proto_mirroredparallel);

    void ToProto(::oneflow::MirroredParallel* proto_mirroredparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MirroredParallel_& other);
       
   public:
    int compare(const _MirroredParallel_& other);

    bool operator==(const _MirroredParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MirroredParallel_& other) const;
  };

  ConstMirroredParallel(const ::std::shared_ptr<_MirroredParallel_>& data);
  ConstMirroredParallel(const ConstMirroredParallel&);
  ConstMirroredParallel(ConstMirroredParallel&&) noexcept;
  ConstMirroredParallel();
  ConstMirroredParallel(const ::oneflow::MirroredParallel& proto_mirroredparallel);
  virtual ~ConstMirroredParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_mirroredparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstMirroredParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMirroredParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMirroredParallel& other) const;
 protected:
  const ::std::shared_ptr<_MirroredParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MirroredParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMirroredParallel
  void BuildFromProto(const PbMessage& proto_mirroredparallel);
  
  ::std::shared_ptr<_MirroredParallel_> data_;
};

class MirroredParallel final : public ConstMirroredParallel {
 public:
  MirroredParallel(const ::std::shared_ptr<_MirroredParallel_>& data);
  MirroredParallel(const MirroredParallel& other);
  // enable nothrow for ::std::vector<MirroredParallel> resize 
  MirroredParallel(MirroredParallel&&) noexcept;
  MirroredParallel();
  explicit MirroredParallel(const ::oneflow::MirroredParallel& proto_mirroredparallel);

  ~MirroredParallel() override;

  void InitFromProto(const PbMessage& proto_mirroredparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MirroredParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MirroredParallel& other) const;
  void Clear();
  void CopyFrom(const MirroredParallel& other);
  MirroredParallel& operator=(const MirroredParallel& other);


  ::std::shared_ptr<MirroredParallel> __SharedMutable__();
};


class ConstOptMirroredParallel : public ::oneflow::cfg::Message {
 public:

  class _OptMirroredParallel_ {
   public:
    _OptMirroredParallel_();
    explicit _OptMirroredParallel_(const _OptMirroredParallel_& other);
    explicit _OptMirroredParallel_(_OptMirroredParallel_&& other);
    _OptMirroredParallel_(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel);
    ~_OptMirroredParallel_();

    void InitFromProto(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel);

    void ToProto(::oneflow::OptMirroredParallel* proto_optmirroredparallel) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OptMirroredParallel_& other);
  
      // optional field mirrored_parallel
     public:
    bool has_mirrored_parallel() const;
    const ::oneflow::cfg::MirroredParallel& mirrored_parallel() const;
    void clear_mirrored_parallel();
    ::oneflow::cfg::MirroredParallel* mutable_mirrored_parallel();
   protected:
    bool has_mirrored_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::MirroredParallel> mirrored_parallel_;
           
   public:
    int compare(const _OptMirroredParallel_& other);

    bool operator==(const _OptMirroredParallel_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OptMirroredParallel_& other) const;
  };

  ConstOptMirroredParallel(const ::std::shared_ptr<_OptMirroredParallel_>& data);
  ConstOptMirroredParallel(const ConstOptMirroredParallel&);
  ConstOptMirroredParallel(ConstOptMirroredParallel&&) noexcept;
  ConstOptMirroredParallel();
  ConstOptMirroredParallel(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel);
  virtual ~ConstOptMirroredParallel() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_optmirroredparallel) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field mirrored_parallel
 public:
  bool has_mirrored_parallel() const;
  const ::oneflow::cfg::MirroredParallel& mirrored_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMirroredParallel> shared_const_mirrored_parallel() const;

 public:
  ::std::shared_ptr<ConstOptMirroredParallel> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOptMirroredParallel& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOptMirroredParallel& other) const;
 protected:
  const ::std::shared_ptr<_OptMirroredParallel_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OptMirroredParallel_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOptMirroredParallel
  void BuildFromProto(const PbMessage& proto_optmirroredparallel);
  
  ::std::shared_ptr<_OptMirroredParallel_> data_;
};

class OptMirroredParallel final : public ConstOptMirroredParallel {
 public:
  OptMirroredParallel(const ::std::shared_ptr<_OptMirroredParallel_>& data);
  OptMirroredParallel(const OptMirroredParallel& other);
  // enable nothrow for ::std::vector<OptMirroredParallel> resize 
  OptMirroredParallel(OptMirroredParallel&&) noexcept;
  OptMirroredParallel();
  explicit OptMirroredParallel(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel);

  ~OptMirroredParallel() override;

  void InitFromProto(const PbMessage& proto_optmirroredparallel) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OptMirroredParallel& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OptMirroredParallel& other) const;
  void Clear();
  void CopyFrom(const OptMirroredParallel& other);
  OptMirroredParallel& operator=(const OptMirroredParallel& other);

  // required or optional field mirrored_parallel
 public:
  void clear_mirrored_parallel();
  ::oneflow::cfg::MirroredParallel* mutable_mirrored_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MirroredParallel> shared_mutable_mirrored_parallel();

  ::std::shared_ptr<OptMirroredParallel> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_; 
class Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_;

class ConstMirroredSignature : public ::oneflow::cfg::Message {
 public:

  class _MirroredSignature_ {
   public:
    _MirroredSignature_();
    explicit _MirroredSignature_(const _MirroredSignature_& other);
    explicit _MirroredSignature_(_MirroredSignature_&& other);
    _MirroredSignature_(const ::oneflow::MirroredSignature& proto_mirroredsignature);
    ~_MirroredSignature_();

    void InitFromProto(const ::oneflow::MirroredSignature& proto_mirroredsignature);

    void ToProto(::oneflow::MirroredSignature* proto_mirroredsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MirroredSignature_& other);
  
     public:
    ::std::size_t bn_in_op2opt_mirrored_parallel_size() const;
    const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& bn_in_op2opt_mirrored_parallel() const;

    _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ * mutable_bn_in_op2opt_mirrored_parallel();

    const ::oneflow::cfg::OptMirroredParallel& bn_in_op2opt_mirrored_parallel(::std::string key) const;

    void clear_bn_in_op2opt_mirrored_parallel();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> bn_in_op2opt_mirrored_parallel_;
         
   public:
    int compare(const _MirroredSignature_& other);

    bool operator==(const _MirroredSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MirroredSignature_& other) const;
  };

  ConstMirroredSignature(const ::std::shared_ptr<_MirroredSignature_>& data);
  ConstMirroredSignature(const ConstMirroredSignature&);
  ConstMirroredSignature(ConstMirroredSignature&&) noexcept;
  ConstMirroredSignature();
  ConstMirroredSignature(const ::oneflow::MirroredSignature& proto_mirroredsignature);
  virtual ~ConstMirroredSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_mirroredsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2opt_mirrored_parallel
 public:
  ::std::size_t bn_in_op2opt_mirrored_parallel_size() const;
  const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& bn_in_op2opt_mirrored_parallel() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> shared_const_bn_in_op2opt_mirrored_parallel() const;

 public:
  ::std::shared_ptr<ConstMirroredSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMirroredSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMirroredSignature& other) const;
 protected:
  const ::std::shared_ptr<_MirroredSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MirroredSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMirroredSignature
  void BuildFromProto(const PbMessage& proto_mirroredsignature);
  
  ::std::shared_ptr<_MirroredSignature_> data_;
};

class MirroredSignature final : public ConstMirroredSignature {
 public:
  MirroredSignature(const ::std::shared_ptr<_MirroredSignature_>& data);
  MirroredSignature(const MirroredSignature& other);
  // enable nothrow for ::std::vector<MirroredSignature> resize 
  MirroredSignature(MirroredSignature&&) noexcept;
  MirroredSignature();
  explicit MirroredSignature(const ::oneflow::MirroredSignature& proto_mirroredsignature);

  ~MirroredSignature() override;

  void InitFromProto(const PbMessage& proto_mirroredsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MirroredSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MirroredSignature& other) const;
  void Clear();
  void CopyFrom(const MirroredSignature& other);
  MirroredSignature& operator=(const MirroredSignature& other);

  // repeated field bn_in_op2opt_mirrored_parallel
 public:
  void clear_bn_in_op2opt_mirrored_parallel();

  const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ & bn_in_op2opt_mirrored_parallel() const;

  _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_* mutable_bn_in_op2opt_mirrored_parallel();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> shared_mutable_bn_in_op2opt_mirrored_parallel();

  ::std::shared_ptr<MirroredSignature> __SharedMutable__();
};










// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OptMirroredParallel> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OptMirroredParallel>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_();
  ~Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::OptMirroredParallel& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstOptMirroredParallel> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ConstOptMirroredParallel>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ final : public Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OptMirroredParallel>>& data);
  _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_();
  ~_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ::oneflow::cfg::OptMirroredParallel>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstMirroredParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstMirroredParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MirroredParallel> {
  std::size_t operator()(const ::oneflow::cfg::MirroredParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOptMirroredParallel> {
  std::size_t operator()(const ::oneflow::cfg::ConstOptMirroredParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OptMirroredParallel> {
  std::size_t operator()(const ::oneflow::cfg::OptMirroredParallel& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMirroredSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstMirroredSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MirroredSignature> {
  std::size_t operator()(const ::oneflow::cfg::MirroredSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H_