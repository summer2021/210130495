#include "oneflow/core/job/scope.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/job/scope.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstScopeProto::_ScopeProto_::_ScopeProto_() { Clear(); }
ConstScopeProto::_ScopeProto_::_ScopeProto_(const _ScopeProto_& other) { CopyFrom(other); }
ConstScopeProto::_ScopeProto_::_ScopeProto_(const ::oneflow::ScopeProto& proto_scopeproto) {
  InitFromProto(proto_scopeproto);
}
ConstScopeProto::_ScopeProto_::_ScopeProto_(_ScopeProto_&& other) = default;
ConstScopeProto::_ScopeProto_::~_ScopeProto_() = default;

void ConstScopeProto::_ScopeProto_::InitFromProto(const ::oneflow::ScopeProto& proto_scopeproto) {
  Clear();
  // required_or_optional field: job_desc_symbol_id
  if (proto_scopeproto.has_job_desc_symbol_id()) {
    set_job_desc_symbol_id(proto_scopeproto.job_desc_symbol_id());
  }
  // required_or_optional field: device_parallel_desc_symbol_id
  if (proto_scopeproto.has_device_parallel_desc_symbol_id()) {
    set_device_parallel_desc_symbol_id(proto_scopeproto.device_parallel_desc_symbol_id());
  }
  // required_or_optional field: host_parallel_desc_symbol_id
  if (proto_scopeproto.has_host_parallel_desc_symbol_id()) {
    set_host_parallel_desc_symbol_id(proto_scopeproto.host_parallel_desc_symbol_id());
  }
  // required_or_optional field: enable_cpu_alternative_op
  if (proto_scopeproto.has_enable_cpu_alternative_op()) {
    set_enable_cpu_alternative_op(proto_scopeproto.enable_cpu_alternative_op());
  }
  // required_or_optional field: opt_mirrored_parallel_conf
  if (proto_scopeproto.has_opt_mirrored_parallel_conf()) {
  *mutable_opt_mirrored_parallel_conf() = ::oneflow::cfg::OptMirroredParallel(proto_scopeproto.opt_mirrored_parallel_conf());      
  }
  // repeated field: scope_op_name_prefixes
  if (!proto_scopeproto.scope_op_name_prefixes().empty()) {
    for (const ::std::string& elem : proto_scopeproto.scope_op_name_prefixes()) {
      add_scope_op_name_prefixes(elem);
    }
  }
  // required_or_optional field: parent_scope_symbol_id
  if (proto_scopeproto.has_parent_scope_symbol_id()) {
    set_parent_scope_symbol_id(proto_scopeproto.parent_scope_symbol_id());
  }
  // required_or_optional field: session_id
  if (proto_scopeproto.has_session_id()) {
    set_session_id(proto_scopeproto.session_id());
  }
  // map field : attr_name2attr_value
  if (!proto_scopeproto.attr_name2attr_value().empty()) {
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_&  mut_attr_name2attr_value = *mutable_attr_name2attr_value();
    for (const auto& pair : proto_scopeproto.attr_name2attr_value()) {
      mut_attr_name2attr_value[pair.first] = ::oneflow::cfg::AttrValue(pair.second);
    }
  }
  // required_or_optional field: calculation_pass_name
  if (proto_scopeproto.has_calculation_pass_name()) {
    set_calculation_pass_name(proto_scopeproto.calculation_pass_name());
  }
    
}

void ConstScopeProto::_ScopeProto_::ToProto(::oneflow::ScopeProto* proto_scopeproto) const {
  proto_scopeproto->Clear();
  // required_or_optional field: job_desc_symbol_id
  if (this->has_job_desc_symbol_id()) {
    proto_scopeproto->set_job_desc_symbol_id(job_desc_symbol_id());
    }
  // required_or_optional field: device_parallel_desc_symbol_id
  if (this->has_device_parallel_desc_symbol_id()) {
    proto_scopeproto->set_device_parallel_desc_symbol_id(device_parallel_desc_symbol_id());
    }
  // required_or_optional field: host_parallel_desc_symbol_id
  if (this->has_host_parallel_desc_symbol_id()) {
    proto_scopeproto->set_host_parallel_desc_symbol_id(host_parallel_desc_symbol_id());
    }
  // required_or_optional field: enable_cpu_alternative_op
  if (this->has_enable_cpu_alternative_op()) {
    proto_scopeproto->set_enable_cpu_alternative_op(enable_cpu_alternative_op());
    }
  // required_or_optional field: opt_mirrored_parallel_conf
  if (this->has_opt_mirrored_parallel_conf()) {
    ::oneflow::OptMirroredParallel proto_opt_mirrored_parallel_conf;
    opt_mirrored_parallel_conf().ToProto(&proto_opt_mirrored_parallel_conf);
    proto_scopeproto->mutable_opt_mirrored_parallel_conf()->CopyFrom(proto_opt_mirrored_parallel_conf);
    }
  // repeated field: scope_op_name_prefixes
  if (!scope_op_name_prefixes().empty()) {
    for (const ::std::string& elem : scope_op_name_prefixes()) {
      proto_scopeproto->add_scope_op_name_prefixes(elem);
    }
  }
  // required_or_optional field: parent_scope_symbol_id
  if (this->has_parent_scope_symbol_id()) {
    proto_scopeproto->set_parent_scope_symbol_id(parent_scope_symbol_id());
    }
  // required_or_optional field: session_id
  if (this->has_session_id()) {
    proto_scopeproto->set_session_id(session_id());
    }
  // map field : attr_name2attr_value
  if (!attr_name2attr_value().empty()) {
    auto& mut_attr_name2attr_value = *(proto_scopeproto->mutable_attr_name2attr_value());
    for (const auto& pair : attr_name2attr_value()) {
      ::oneflow::AttrValue proto_attr_name2attr_value_value;
      pair.second.ToProto(&proto_attr_name2attr_value_value);
      mut_attr_name2attr_value[pair.first] = proto_attr_name2attr_value_value;
    }
  }
  // required_or_optional field: calculation_pass_name
  if (this->has_calculation_pass_name()) {
    proto_scopeproto->set_calculation_pass_name(calculation_pass_name());
    }

}

::std::string ConstScopeProto::_ScopeProto_::DebugString() const {
  ::oneflow::ScopeProto proto_scopeproto;
  this->ToProto(&proto_scopeproto);
  return proto_scopeproto.DebugString();
}

void ConstScopeProto::_ScopeProto_::Clear() {
  clear_job_desc_symbol_id();
  clear_device_parallel_desc_symbol_id();
  clear_host_parallel_desc_symbol_id();
  clear_enable_cpu_alternative_op();
  clear_opt_mirrored_parallel_conf();
  clear_scope_op_name_prefixes();
  clear_parent_scope_symbol_id();
  clear_session_id();
  clear_attr_name2attr_value();
  clear_calculation_pass_name();
}

void ConstScopeProto::_ScopeProto_::CopyFrom(const _ScopeProto_& other) {
  if (other.has_job_desc_symbol_id()) {
    set_job_desc_symbol_id(other.job_desc_symbol_id());
  } else {
    clear_job_desc_symbol_id();
  }
  if (other.has_device_parallel_desc_symbol_id()) {
    set_device_parallel_desc_symbol_id(other.device_parallel_desc_symbol_id());
  } else {
    clear_device_parallel_desc_symbol_id();
  }
  if (other.has_host_parallel_desc_symbol_id()) {
    set_host_parallel_desc_symbol_id(other.host_parallel_desc_symbol_id());
  } else {
    clear_host_parallel_desc_symbol_id();
  }
  if (other.has_enable_cpu_alternative_op()) {
    set_enable_cpu_alternative_op(other.enable_cpu_alternative_op());
  } else {
    clear_enable_cpu_alternative_op();
  }
  if (other.has_opt_mirrored_parallel_conf()) {
    mutable_opt_mirrored_parallel_conf()->CopyFrom(other.opt_mirrored_parallel_conf());
  } else {
    clear_opt_mirrored_parallel_conf();
  }
  mutable_scope_op_name_prefixes()->CopyFrom(other.scope_op_name_prefixes());
  if (other.has_parent_scope_symbol_id()) {
    set_parent_scope_symbol_id(other.parent_scope_symbol_id());
  } else {
    clear_parent_scope_symbol_id();
  }
  if (other.has_session_id()) {
    set_session_id(other.session_id());
  } else {
    clear_session_id();
  }
  mutable_attr_name2attr_value()->CopyFrom(other.attr_name2attr_value());
  if (other.has_calculation_pass_name()) {
    set_calculation_pass_name(other.calculation_pass_name());
  } else {
    clear_calculation_pass_name();
  }
}


// optional field job_desc_symbol_id
bool ConstScopeProto::_ScopeProto_::has_job_desc_symbol_id() const {
  return has_job_desc_symbol_id_;
}
const int64_t& ConstScopeProto::_ScopeProto_::job_desc_symbol_id() const {
  if (has_job_desc_symbol_id_) { return job_desc_symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_job_desc_symbol_id() {
  has_job_desc_symbol_id_ = false;
}
void ConstScopeProto::_ScopeProto_::set_job_desc_symbol_id(const int64_t& value) {
  job_desc_symbol_id_ = value;
  has_job_desc_symbol_id_ = true;
}
int64_t* ConstScopeProto::_ScopeProto_::mutable_job_desc_symbol_id() {
  has_job_desc_symbol_id_ = true;
  return &job_desc_symbol_id_;
}

// optional field device_parallel_desc_symbol_id
bool ConstScopeProto::_ScopeProto_::has_device_parallel_desc_symbol_id() const {
  return has_device_parallel_desc_symbol_id_;
}
const int64_t& ConstScopeProto::_ScopeProto_::device_parallel_desc_symbol_id() const {
  if (has_device_parallel_desc_symbol_id_) { return device_parallel_desc_symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_device_parallel_desc_symbol_id() {
  has_device_parallel_desc_symbol_id_ = false;
}
void ConstScopeProto::_ScopeProto_::set_device_parallel_desc_symbol_id(const int64_t& value) {
  device_parallel_desc_symbol_id_ = value;
  has_device_parallel_desc_symbol_id_ = true;
}
int64_t* ConstScopeProto::_ScopeProto_::mutable_device_parallel_desc_symbol_id() {
  has_device_parallel_desc_symbol_id_ = true;
  return &device_parallel_desc_symbol_id_;
}

// optional field host_parallel_desc_symbol_id
bool ConstScopeProto::_ScopeProto_::has_host_parallel_desc_symbol_id() const {
  return has_host_parallel_desc_symbol_id_;
}
const int64_t& ConstScopeProto::_ScopeProto_::host_parallel_desc_symbol_id() const {
  if (has_host_parallel_desc_symbol_id_) { return host_parallel_desc_symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_host_parallel_desc_symbol_id() {
  has_host_parallel_desc_symbol_id_ = false;
}
void ConstScopeProto::_ScopeProto_::set_host_parallel_desc_symbol_id(const int64_t& value) {
  host_parallel_desc_symbol_id_ = value;
  has_host_parallel_desc_symbol_id_ = true;
}
int64_t* ConstScopeProto::_ScopeProto_::mutable_host_parallel_desc_symbol_id() {
  has_host_parallel_desc_symbol_id_ = true;
  return &host_parallel_desc_symbol_id_;
}

// optional field enable_cpu_alternative_op
bool ConstScopeProto::_ScopeProto_::has_enable_cpu_alternative_op() const {
  return has_enable_cpu_alternative_op_;
}
const bool& ConstScopeProto::_ScopeProto_::enable_cpu_alternative_op() const {
  if (has_enable_cpu_alternative_op_) { return enable_cpu_alternative_op_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_enable_cpu_alternative_op() {
  has_enable_cpu_alternative_op_ = false;
}
void ConstScopeProto::_ScopeProto_::set_enable_cpu_alternative_op(const bool& value) {
  enable_cpu_alternative_op_ = value;
  has_enable_cpu_alternative_op_ = true;
}
bool* ConstScopeProto::_ScopeProto_::mutable_enable_cpu_alternative_op() {
  has_enable_cpu_alternative_op_ = true;
  return &enable_cpu_alternative_op_;
}

// optional field opt_mirrored_parallel_conf
bool ConstScopeProto::_ScopeProto_::has_opt_mirrored_parallel_conf() const {
  return has_opt_mirrored_parallel_conf_;
}
const ::oneflow::cfg::OptMirroredParallel& ConstScopeProto::_ScopeProto_::opt_mirrored_parallel_conf() const {
  if (!opt_mirrored_parallel_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> default_static_value =
      ::std::make_shared<::oneflow::cfg::OptMirroredParallel>();
    return *default_static_value;
  }
  return *(opt_mirrored_parallel_conf_.get());
}
void ConstScopeProto::_ScopeProto_::clear_opt_mirrored_parallel_conf() {
  if (opt_mirrored_parallel_conf_) {
    opt_mirrored_parallel_conf_->Clear();
  }
  has_opt_mirrored_parallel_conf_ = false;
}
::oneflow::cfg::OptMirroredParallel* ConstScopeProto::_ScopeProto_::mutable_opt_mirrored_parallel_conf() {
  if (!opt_mirrored_parallel_conf_) {
    opt_mirrored_parallel_conf_ = ::std::make_shared<::oneflow::cfg::OptMirroredParallel>();
  }
  has_opt_mirrored_parallel_conf_ = true;
  return opt_mirrored_parallel_conf_.get();
}

// repeated field scope_op_name_prefixes
::std::size_t ConstScopeProto::_ScopeProto_::scope_op_name_prefixes_size() const {
  if (!scope_op_name_prefixes_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return scope_op_name_prefixes_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& ConstScopeProto::_ScopeProto_::scope_op_name_prefixes() const {
  if (!scope_op_name_prefixes_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(scope_op_name_prefixes_.get());
}
const ::std::string& ConstScopeProto::_ScopeProto_::scope_op_name_prefixes(::std::size_t index) const {
  if (!scope_op_name_prefixes_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return scope_op_name_prefixes_->Get(index);
}
void ConstScopeProto::_ScopeProto_::clear_scope_op_name_prefixes() {
  if (!scope_op_name_prefixes_) {
    scope_op_name_prefixes_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
  }
  return scope_op_name_prefixes_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_* ConstScopeProto::_ScopeProto_::mutable_scope_op_name_prefixes() {
  if (!scope_op_name_prefixes_) {
    scope_op_name_prefixes_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
  }
  return  scope_op_name_prefixes_.get();
}
::std::string* ConstScopeProto::_ScopeProto_::mutable_scope_op_name_prefixes(::std::size_t index) {
  if (!scope_op_name_prefixes_) {
    scope_op_name_prefixes_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
  }
  return  scope_op_name_prefixes_->Mutable(index);
}
void ConstScopeProto::_ScopeProto_::add_scope_op_name_prefixes(const ::std::string& value) {
  if (!scope_op_name_prefixes_) {
    scope_op_name_prefixes_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
  }
  return scope_op_name_prefixes_->Add(value);
}
void ConstScopeProto::_ScopeProto_::set_scope_op_name_prefixes(::std::size_t index, const ::std::string& value) {
  if (!scope_op_name_prefixes_) {
    scope_op_name_prefixes_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>();
  }
  return scope_op_name_prefixes_->Set(index, value);
}

// optional field parent_scope_symbol_id
bool ConstScopeProto::_ScopeProto_::has_parent_scope_symbol_id() const {
  return has_parent_scope_symbol_id_;
}
const int64_t& ConstScopeProto::_ScopeProto_::parent_scope_symbol_id() const {
  if (has_parent_scope_symbol_id_) { return parent_scope_symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_parent_scope_symbol_id() {
  has_parent_scope_symbol_id_ = false;
}
void ConstScopeProto::_ScopeProto_::set_parent_scope_symbol_id(const int64_t& value) {
  parent_scope_symbol_id_ = value;
  has_parent_scope_symbol_id_ = true;
}
int64_t* ConstScopeProto::_ScopeProto_::mutable_parent_scope_symbol_id() {
  has_parent_scope_symbol_id_ = true;
  return &parent_scope_symbol_id_;
}

// optional field session_id
bool ConstScopeProto::_ScopeProto_::has_session_id() const {
  return has_session_id_;
}
const int64_t& ConstScopeProto::_ScopeProto_::session_id() const {
  if (has_session_id_) { return session_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_session_id() {
  has_session_id_ = false;
}
void ConstScopeProto::_ScopeProto_::set_session_id(const int64_t& value) {
  session_id_ = value;
  has_session_id_ = true;
}
int64_t* ConstScopeProto::_ScopeProto_::mutable_session_id() {
  has_session_id_ = true;
  return &session_id_;
}

::std::size_t ConstScopeProto::_ScopeProto_::attr_name2attr_value_size() const {
  if (!attr_name2attr_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->size();
  }
  return attr_name2attr_value_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& ConstScopeProto::_ScopeProto_::attr_name2attr_value() const {
  if (!attr_name2attr_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>();
    return *(default_static_value.get());
  }
  return *(attr_name2attr_value_.get());
}

_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ * ConstScopeProto::_ScopeProto_::mutable_attr_name2attr_value() {
  if (!attr_name2attr_value_) {
    attr_name2attr_value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>();
  }
  return attr_name2attr_value_.get();
}

const ::oneflow::cfg::AttrValue& ConstScopeProto::_ScopeProto_::attr_name2attr_value(::std::string key) const {
  if (!attr_name2attr_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->at(key);
  }
  return attr_name2attr_value_->at(key);
}

void ConstScopeProto::_ScopeProto_::clear_attr_name2attr_value() {
  if (!attr_name2attr_value_) {
    attr_name2attr_value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>();
  }
  return attr_name2attr_value_->Clear();
}


// optional field calculation_pass_name
bool ConstScopeProto::_ScopeProto_::has_calculation_pass_name() const {
  return has_calculation_pass_name_;
}
const ::std::string& ConstScopeProto::_ScopeProto_::calculation_pass_name() const {
  if (has_calculation_pass_name_) { return calculation_pass_name_; }
  static const ::std::string default_static_value =
    ::std::string("forward_pass");
  return default_static_value;
}
void ConstScopeProto::_ScopeProto_::clear_calculation_pass_name() {
  has_calculation_pass_name_ = false;
}
void ConstScopeProto::_ScopeProto_::set_calculation_pass_name(const ::std::string& value) {
  calculation_pass_name_ = value;
  has_calculation_pass_name_ = true;
}
::std::string* ConstScopeProto::_ScopeProto_::mutable_calculation_pass_name() {
  has_calculation_pass_name_ = true;
  return &calculation_pass_name_;
}


int ConstScopeProto::_ScopeProto_::compare(const _ScopeProto_& other) {
  if (!(has_job_desc_symbol_id() == other.has_job_desc_symbol_id())) {
    return has_job_desc_symbol_id() < other.has_job_desc_symbol_id() ? -1 : 1;
  } else if (!(job_desc_symbol_id() == other.job_desc_symbol_id())) {
    return job_desc_symbol_id() < other.job_desc_symbol_id() ? -1 : 1;
  }
  if (!(has_device_parallel_desc_symbol_id() == other.has_device_parallel_desc_symbol_id())) {
    return has_device_parallel_desc_symbol_id() < other.has_device_parallel_desc_symbol_id() ? -1 : 1;
  } else if (!(device_parallel_desc_symbol_id() == other.device_parallel_desc_symbol_id())) {
    return device_parallel_desc_symbol_id() < other.device_parallel_desc_symbol_id() ? -1 : 1;
  }
  if (!(has_host_parallel_desc_symbol_id() == other.has_host_parallel_desc_symbol_id())) {
    return has_host_parallel_desc_symbol_id() < other.has_host_parallel_desc_symbol_id() ? -1 : 1;
  } else if (!(host_parallel_desc_symbol_id() == other.host_parallel_desc_symbol_id())) {
    return host_parallel_desc_symbol_id() < other.host_parallel_desc_symbol_id() ? -1 : 1;
  }
  if (!(has_enable_cpu_alternative_op() == other.has_enable_cpu_alternative_op())) {
    return has_enable_cpu_alternative_op() < other.has_enable_cpu_alternative_op() ? -1 : 1;
  } else if (!(enable_cpu_alternative_op() == other.enable_cpu_alternative_op())) {
    return enable_cpu_alternative_op() < other.enable_cpu_alternative_op() ? -1 : 1;
  }
  if (!(has_opt_mirrored_parallel_conf() == other.has_opt_mirrored_parallel_conf())) {
    return has_opt_mirrored_parallel_conf() < other.has_opt_mirrored_parallel_conf() ? -1 : 1;
  } else if (!(opt_mirrored_parallel_conf() == other.opt_mirrored_parallel_conf())) {
    return opt_mirrored_parallel_conf() < other.opt_mirrored_parallel_conf() ? -1 : 1;
  }
  if (!(scope_op_name_prefixes() == other.scope_op_name_prefixes())) {
    return scope_op_name_prefixes() < other.scope_op_name_prefixes() ? -1 : 1;
  }
  if (!(has_parent_scope_symbol_id() == other.has_parent_scope_symbol_id())) {
    return has_parent_scope_symbol_id() < other.has_parent_scope_symbol_id() ? -1 : 1;
  } else if (!(parent_scope_symbol_id() == other.parent_scope_symbol_id())) {
    return parent_scope_symbol_id() < other.parent_scope_symbol_id() ? -1 : 1;
  }
  if (!(has_session_id() == other.has_session_id())) {
    return has_session_id() < other.has_session_id() ? -1 : 1;
  } else if (!(session_id() == other.session_id())) {
    return session_id() < other.session_id() ? -1 : 1;
  }
  if (!(attr_name2attr_value() == other.attr_name2attr_value())) {
    return attr_name2attr_value() < other.attr_name2attr_value() ? -1 : 1;
  }
  if (!(has_calculation_pass_name() == other.has_calculation_pass_name())) {
    return has_calculation_pass_name() < other.has_calculation_pass_name() ? -1 : 1;
  } else if (!(calculation_pass_name() == other.calculation_pass_name())) {
    return calculation_pass_name() < other.calculation_pass_name() ? -1 : 1;
  }
  return 0;
}

bool ConstScopeProto::_ScopeProto_::operator==(const _ScopeProto_& other) const {
  return true
    && has_job_desc_symbol_id() == other.has_job_desc_symbol_id() 
    && job_desc_symbol_id() == other.job_desc_symbol_id()
    && has_device_parallel_desc_symbol_id() == other.has_device_parallel_desc_symbol_id() 
    && device_parallel_desc_symbol_id() == other.device_parallel_desc_symbol_id()
    && has_host_parallel_desc_symbol_id() == other.has_host_parallel_desc_symbol_id() 
    && host_parallel_desc_symbol_id() == other.host_parallel_desc_symbol_id()
    && has_enable_cpu_alternative_op() == other.has_enable_cpu_alternative_op() 
    && enable_cpu_alternative_op() == other.enable_cpu_alternative_op()
    && has_opt_mirrored_parallel_conf() == other.has_opt_mirrored_parallel_conf() 
    && opt_mirrored_parallel_conf() == other.opt_mirrored_parallel_conf()
    && scope_op_name_prefixes() == other.scope_op_name_prefixes()
    && has_parent_scope_symbol_id() == other.has_parent_scope_symbol_id() 
    && parent_scope_symbol_id() == other.parent_scope_symbol_id()
    && has_session_id() == other.has_session_id() 
    && session_id() == other.session_id()
    && attr_name2attr_value() == other.attr_name2attr_value()
    && has_calculation_pass_name() == other.has_calculation_pass_name() 
    && calculation_pass_name() == other.calculation_pass_name()
  ;
}

std::size_t ConstScopeProto::_ScopeProto_::__CalcHash__() const {
  return 0
    ^ (has_job_desc_symbol_id() ? std::hash<int64_t>()(job_desc_symbol_id()) : 0)
    ^ (has_device_parallel_desc_symbol_id() ? std::hash<int64_t>()(device_parallel_desc_symbol_id()) : 0)
    ^ (has_host_parallel_desc_symbol_id() ? std::hash<int64_t>()(host_parallel_desc_symbol_id()) : 0)
    ^ (has_enable_cpu_alternative_op() ? std::hash<bool>()(enable_cpu_alternative_op()) : 0)
    ^ (has_opt_mirrored_parallel_conf() ? std::hash<::oneflow::cfg::OptMirroredParallel>()(opt_mirrored_parallel_conf()) : 0)
    ^ scope_op_name_prefixes().__CalcHash__()
    ^ (has_parent_scope_symbol_id() ? std::hash<int64_t>()(parent_scope_symbol_id()) : 0)
    ^ (has_session_id() ? std::hash<int64_t>()(session_id()) : 0)
    ^ attr_name2attr_value().__CalcHash__()
    ^ (has_calculation_pass_name() ? std::hash<::std::string>()(calculation_pass_name()) : 0)
  ;
}

bool ConstScopeProto::_ScopeProto_::operator<(const _ScopeProto_& other) const {
  return false
    || !(has_job_desc_symbol_id() == other.has_job_desc_symbol_id()) ? 
      has_job_desc_symbol_id() < other.has_job_desc_symbol_id() : false
    || !(job_desc_symbol_id() == other.job_desc_symbol_id()) ? 
      job_desc_symbol_id() < other.job_desc_symbol_id() : false
    || !(has_device_parallel_desc_symbol_id() == other.has_device_parallel_desc_symbol_id()) ? 
      has_device_parallel_desc_symbol_id() < other.has_device_parallel_desc_symbol_id() : false
    || !(device_parallel_desc_symbol_id() == other.device_parallel_desc_symbol_id()) ? 
      device_parallel_desc_symbol_id() < other.device_parallel_desc_symbol_id() : false
    || !(has_host_parallel_desc_symbol_id() == other.has_host_parallel_desc_symbol_id()) ? 
      has_host_parallel_desc_symbol_id() < other.has_host_parallel_desc_symbol_id() : false
    || !(host_parallel_desc_symbol_id() == other.host_parallel_desc_symbol_id()) ? 
      host_parallel_desc_symbol_id() < other.host_parallel_desc_symbol_id() : false
    || !(has_enable_cpu_alternative_op() == other.has_enable_cpu_alternative_op()) ? 
      has_enable_cpu_alternative_op() < other.has_enable_cpu_alternative_op() : false
    || !(enable_cpu_alternative_op() == other.enable_cpu_alternative_op()) ? 
      enable_cpu_alternative_op() < other.enable_cpu_alternative_op() : false
    || !(has_opt_mirrored_parallel_conf() == other.has_opt_mirrored_parallel_conf()) ? 
      has_opt_mirrored_parallel_conf() < other.has_opt_mirrored_parallel_conf() : false
    || !(opt_mirrored_parallel_conf() == other.opt_mirrored_parallel_conf()) ? 
      opt_mirrored_parallel_conf() < other.opt_mirrored_parallel_conf() : false
    || !(scope_op_name_prefixes() == other.scope_op_name_prefixes()) ? 
      scope_op_name_prefixes() < other.scope_op_name_prefixes() : false
    || !(has_parent_scope_symbol_id() == other.has_parent_scope_symbol_id()) ? 
      has_parent_scope_symbol_id() < other.has_parent_scope_symbol_id() : false
    || !(parent_scope_symbol_id() == other.parent_scope_symbol_id()) ? 
      parent_scope_symbol_id() < other.parent_scope_symbol_id() : false
    || !(has_session_id() == other.has_session_id()) ? 
      has_session_id() < other.has_session_id() : false
    || !(session_id() == other.session_id()) ? 
      session_id() < other.session_id() : false
    || !(attr_name2attr_value() == other.attr_name2attr_value()) ? 
      attr_name2attr_value() < other.attr_name2attr_value() : false
    || !(has_calculation_pass_name() == other.has_calculation_pass_name()) ? 
      has_calculation_pass_name() < other.has_calculation_pass_name() : false
    || !(calculation_pass_name() == other.calculation_pass_name()) ? 
      calculation_pass_name() < other.calculation_pass_name() : false
  ;
}

using _ScopeProto_ =  ConstScopeProto::_ScopeProto_;
ConstScopeProto::ConstScopeProto(const ::std::shared_ptr<_ScopeProto_>& data): data_(data) {}
ConstScopeProto::ConstScopeProto(): data_(::std::make_shared<_ScopeProto_>()) {}
ConstScopeProto::ConstScopeProto(const ::oneflow::ScopeProto& proto_scopeproto) {
  BuildFromProto(proto_scopeproto);
}
ConstScopeProto::ConstScopeProto(const ConstScopeProto&) = default;
ConstScopeProto::ConstScopeProto(ConstScopeProto&&) noexcept = default;
ConstScopeProto::~ConstScopeProto() = default;

void ConstScopeProto::ToProto(PbMessage* proto_scopeproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ScopeProto*>(proto_scopeproto));
}
  
::std::string ConstScopeProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstScopeProto::__Empty__() const {
  return !data_;
}

int ConstScopeProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"job_desc_symbol_id", 20},
    {"device_parallel_desc_symbol_id", 30},
    {"host_parallel_desc_symbol_id", 40},
    {"enable_cpu_alternative_op", 41},
    {"opt_mirrored_parallel_conf", 50},
    {"scope_op_name_prefixes", 60},
    {"parent_scope_symbol_id", 70},
    {"session_id", 80},
    {"attr_name2attr_value", 90},
    {"calculation_pass_name", 100},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstScopeProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 20:
    case 30:
    case 40:
    case 41:
    case 50:
    case 60:
    case 70:
    case 80:
    case 90:
    case 100:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstScopeProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 20: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 30: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 40: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 41: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 50: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OptMirroredParallel),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOptMirroredParallel),
      };
      return type_indices;
    }
    case 60: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 70: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 80: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 90: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>)
      };
      return type_indices;
    }
    case 100: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstScopeProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 20: return &job_desc_symbol_id();
    case 30: return &device_parallel_desc_symbol_id();
    case 40: return &host_parallel_desc_symbol_id();
    case 41: return &enable_cpu_alternative_op();
    case 50: return &opt_mirrored_parallel_conf();
    case 60: return &scope_op_name_prefixes();
    case 70: return &parent_scope_symbol_id();
    case 80: return &session_id();
    case 90: return &attr_name2attr_value();
    case 100: return &calculation_pass_name();
    default: return nullptr;
  }
}

// required or optional field job_desc_symbol_id
bool ConstScopeProto::has_job_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->has_job_desc_symbol_id();
}
const int64_t& ConstScopeProto::job_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->job_desc_symbol_id();
}
// used by pybind11 only
// required or optional field device_parallel_desc_symbol_id
bool ConstScopeProto::has_device_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->has_device_parallel_desc_symbol_id();
}
const int64_t& ConstScopeProto::device_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->device_parallel_desc_symbol_id();
}
// used by pybind11 only
// required or optional field host_parallel_desc_symbol_id
bool ConstScopeProto::has_host_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->has_host_parallel_desc_symbol_id();
}
const int64_t& ConstScopeProto::host_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->host_parallel_desc_symbol_id();
}
// used by pybind11 only
// required or optional field enable_cpu_alternative_op
bool ConstScopeProto::has_enable_cpu_alternative_op() const {
  return __SharedPtrOrDefault__()->has_enable_cpu_alternative_op();
}
const bool& ConstScopeProto::enable_cpu_alternative_op() const {
  return __SharedPtrOrDefault__()->enable_cpu_alternative_op();
}
// used by pybind11 only
// required or optional field opt_mirrored_parallel_conf
bool ConstScopeProto::has_opt_mirrored_parallel_conf() const {
  return __SharedPtrOrDefault__()->has_opt_mirrored_parallel_conf();
}
const ::oneflow::cfg::OptMirroredParallel& ConstScopeProto::opt_mirrored_parallel_conf() const {
  return __SharedPtrOrDefault__()->opt_mirrored_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOptMirroredParallel> ConstScopeProto::shared_const_opt_mirrored_parallel_conf() const {
  return opt_mirrored_parallel_conf().__SharedConst__();
}
// repeated field scope_op_name_prefixes
::std::size_t ConstScopeProto::scope_op_name_prefixes_size() const {
  return __SharedPtrOrDefault__()->scope_op_name_prefixes_size();
}
const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& ConstScopeProto::scope_op_name_prefixes() const {
  return __SharedPtrOrDefault__()->scope_op_name_prefixes();
}
const ::std::string& ConstScopeProto::scope_op_name_prefixes(::std::size_t index) const {
  return __SharedPtrOrDefault__()->scope_op_name_prefixes(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> ConstScopeProto::shared_const_scope_op_name_prefixes() const {
  return scope_op_name_prefixes().__SharedConst__();
}
// required or optional field parent_scope_symbol_id
bool ConstScopeProto::has_parent_scope_symbol_id() const {
  return __SharedPtrOrDefault__()->has_parent_scope_symbol_id();
}
const int64_t& ConstScopeProto::parent_scope_symbol_id() const {
  return __SharedPtrOrDefault__()->parent_scope_symbol_id();
}
// used by pybind11 only
// required or optional field session_id
bool ConstScopeProto::has_session_id() const {
  return __SharedPtrOrDefault__()->has_session_id();
}
const int64_t& ConstScopeProto::session_id() const {
  return __SharedPtrOrDefault__()->session_id();
}
// used by pybind11 only
// map field attr_name2attr_value
::std::size_t ConstScopeProto::attr_name2attr_value_size() const {
  return __SharedPtrOrDefault__()->attr_name2attr_value_size();
}

const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& ConstScopeProto::attr_name2attr_value() const {
  return __SharedPtrOrDefault__()->attr_name2attr_value();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> ConstScopeProto::shared_const_attr_name2attr_value() const {
  return attr_name2attr_value().__SharedConst__();
}
// required or optional field calculation_pass_name
bool ConstScopeProto::has_calculation_pass_name() const {
  return __SharedPtrOrDefault__()->has_calculation_pass_name();
}
const ::std::string& ConstScopeProto::calculation_pass_name() const {
  return __SharedPtrOrDefault__()->calculation_pass_name();
}
// used by pybind11 only

::std::shared_ptr<ConstScopeProto> ConstScopeProto::__SharedConst__() const {
  return ::std::make_shared<ConstScopeProto>(data_);
}
int64_t ConstScopeProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstScopeProto::operator==(const ConstScopeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstScopeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstScopeProto::operator<(const ConstScopeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ScopeProto_>& ConstScopeProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ScopeProto_> default_ptr = std::make_shared<_ScopeProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_ScopeProto_>& ConstScopeProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _ScopeProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstScopeProto
void ConstScopeProto::BuildFromProto(const PbMessage& proto_scopeproto) {
  data_ = ::std::make_shared<_ScopeProto_>(dynamic_cast<const ::oneflow::ScopeProto&>(proto_scopeproto));
}

ScopeProto::ScopeProto(const ::std::shared_ptr<_ScopeProto_>& data)
  : ConstScopeProto(data) {}
ScopeProto::ScopeProto(const ScopeProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ScopeProto> resize
ScopeProto::ScopeProto(ScopeProto&&) noexcept = default; 
ScopeProto::ScopeProto(const ::oneflow::ScopeProto& proto_scopeproto) {
  InitFromProto(proto_scopeproto);
}
ScopeProto::ScopeProto() = default;

ScopeProto::~ScopeProto() = default;

void ScopeProto::InitFromProto(const PbMessage& proto_scopeproto) {
  BuildFromProto(proto_scopeproto);
}
  
void* ScopeProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 20: return mutable_job_desc_symbol_id();
    case 30: return mutable_device_parallel_desc_symbol_id();
    case 40: return mutable_host_parallel_desc_symbol_id();
    case 41: return mutable_enable_cpu_alternative_op();
    case 50: return mutable_opt_mirrored_parallel_conf();
    case 60: return mutable_scope_op_name_prefixes();
    case 70: return mutable_parent_scope_symbol_id();
    case 80: return mutable_session_id();
    case 90: return mutable_attr_name2attr_value();
    case 100: return mutable_calculation_pass_name();
    default: return nullptr;
  }
}

bool ScopeProto::operator==(const ScopeProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ScopeProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ScopeProto::operator<(const ScopeProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ScopeProto::Clear() {
  if (data_) { data_.reset(); }
}
void ScopeProto::CopyFrom(const ScopeProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ScopeProto& ScopeProto::operator=(const ScopeProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field job_desc_symbol_id
void ScopeProto::clear_job_desc_symbol_id() {
  return __SharedPtr__()->clear_job_desc_symbol_id();
}
void ScopeProto::set_job_desc_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_job_desc_symbol_id(value);
}
int64_t* ScopeProto::mutable_job_desc_symbol_id() {
  return  __SharedPtr__()->mutable_job_desc_symbol_id();
}
// required or optional field device_parallel_desc_symbol_id
void ScopeProto::clear_device_parallel_desc_symbol_id() {
  return __SharedPtr__()->clear_device_parallel_desc_symbol_id();
}
void ScopeProto::set_device_parallel_desc_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_device_parallel_desc_symbol_id(value);
}
int64_t* ScopeProto::mutable_device_parallel_desc_symbol_id() {
  return  __SharedPtr__()->mutable_device_parallel_desc_symbol_id();
}
// required or optional field host_parallel_desc_symbol_id
void ScopeProto::clear_host_parallel_desc_symbol_id() {
  return __SharedPtr__()->clear_host_parallel_desc_symbol_id();
}
void ScopeProto::set_host_parallel_desc_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_host_parallel_desc_symbol_id(value);
}
int64_t* ScopeProto::mutable_host_parallel_desc_symbol_id() {
  return  __SharedPtr__()->mutable_host_parallel_desc_symbol_id();
}
// required or optional field enable_cpu_alternative_op
void ScopeProto::clear_enable_cpu_alternative_op() {
  return __SharedPtr__()->clear_enable_cpu_alternative_op();
}
void ScopeProto::set_enable_cpu_alternative_op(const bool& value) {
  return __SharedPtr__()->set_enable_cpu_alternative_op(value);
}
bool* ScopeProto::mutable_enable_cpu_alternative_op() {
  return  __SharedPtr__()->mutable_enable_cpu_alternative_op();
}
// required or optional field opt_mirrored_parallel_conf
void ScopeProto::clear_opt_mirrored_parallel_conf() {
  return __SharedPtr__()->clear_opt_mirrored_parallel_conf();
}
::oneflow::cfg::OptMirroredParallel* ScopeProto::mutable_opt_mirrored_parallel_conf() {
  return __SharedPtr__()->mutable_opt_mirrored_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> ScopeProto::shared_mutable_opt_mirrored_parallel_conf() {
  return mutable_opt_mirrored_parallel_conf()->__SharedMutable__();
}
// repeated field scope_op_name_prefixes
void ScopeProto::clear_scope_op_name_prefixes() {
  return __SharedPtr__()->clear_scope_op_name_prefixes();
}
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_* ScopeProto::mutable_scope_op_name_prefixes() {
  return __SharedPtr__()->mutable_scope_op_name_prefixes();
}
::std::string* ScopeProto::mutable_scope_op_name_prefixes(::std::size_t index) {
  return __SharedPtr__()->mutable_scope_op_name_prefixes(index);
}
void ScopeProto::add_scope_op_name_prefixes(const ::std::string& value) {
  return __SharedPtr__()->add_scope_op_name_prefixes(value);
}
void ScopeProto::set_scope_op_name_prefixes(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_scope_op_name_prefixes(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> ScopeProto::shared_mutable_scope_op_name_prefixes() {
  return mutable_scope_op_name_prefixes()->__SharedMutable__();
}
// required or optional field parent_scope_symbol_id
void ScopeProto::clear_parent_scope_symbol_id() {
  return __SharedPtr__()->clear_parent_scope_symbol_id();
}
void ScopeProto::set_parent_scope_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_parent_scope_symbol_id(value);
}
int64_t* ScopeProto::mutable_parent_scope_symbol_id() {
  return  __SharedPtr__()->mutable_parent_scope_symbol_id();
}
// required or optional field session_id
void ScopeProto::clear_session_id() {
  return __SharedPtr__()->clear_session_id();
}
void ScopeProto::set_session_id(const int64_t& value) {
  return __SharedPtr__()->set_session_id(value);
}
int64_t* ScopeProto::mutable_session_id() {
  return  __SharedPtr__()->mutable_session_id();
}
// repeated field attr_name2attr_value
void ScopeProto::clear_attr_name2attr_value() {
  return __SharedPtr__()->clear_attr_name2attr_value();
}

const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_ & ScopeProto::attr_name2attr_value() const {
  return __SharedConst__()->attr_name2attr_value();
}

_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_* ScopeProto::mutable_attr_name2attr_value() {
  return __SharedPtr__()->mutable_attr_name2attr_value();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> ScopeProto::shared_mutable_attr_name2attr_value() {
  return mutable_attr_name2attr_value()->__SharedMutable__();
}
// required or optional field calculation_pass_name
void ScopeProto::clear_calculation_pass_name() {
  return __SharedPtr__()->clear_calculation_pass_name();
}
void ScopeProto::set_calculation_pass_name(const ::std::string& value) {
  return __SharedPtr__()->set_calculation_pass_name(value);
}
::std::string* ScopeProto::mutable_calculation_pass_name() {
  return  __SharedPtr__()->mutable_calculation_pass_name();
}

::std::shared_ptr<ScopeProto> ScopeProto::__SharedMutable__() {
  return ::std::make_shared<ScopeProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::~Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::AttrValue& Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_(data) {}
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_() = default;
_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::~_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_() = default;

void _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::operator==(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::operator<(const _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_> _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_SCOPE_CFG_H__MapField___std__string_AttrValue_::shared_mut_end() { return end(); }

}
} // namespace oneflow
