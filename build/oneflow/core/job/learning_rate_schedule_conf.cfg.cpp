#include "oneflow/core/job/learning_rate_schedule_conf.cfg.h"
#include "oneflow/core/job/learning_rate_schedule_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstExponentialDecayConf::_ExponentialDecayConf_::_ExponentialDecayConf_() { Clear(); }
ConstExponentialDecayConf::_ExponentialDecayConf_::_ExponentialDecayConf_(const _ExponentialDecayConf_& other) { CopyFrom(other); }
ConstExponentialDecayConf::_ExponentialDecayConf_::_ExponentialDecayConf_(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf) {
  InitFromProto(proto_exponentialdecayconf);
}
ConstExponentialDecayConf::_ExponentialDecayConf_::_ExponentialDecayConf_(_ExponentialDecayConf_&& other) = default;
ConstExponentialDecayConf::_ExponentialDecayConf_::~_ExponentialDecayConf_() = default;

void ConstExponentialDecayConf::_ExponentialDecayConf_::InitFromProto(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_exponentialdecayconf.has_decay_batches()) {
    set_decay_batches(proto_exponentialdecayconf.decay_batches());
  }
  // required_or_optional field: decay_rate
  if (proto_exponentialdecayconf.has_decay_rate()) {
    set_decay_rate(proto_exponentialdecayconf.decay_rate());
  }
  // required_or_optional field: staircase
  if (proto_exponentialdecayconf.has_staircase()) {
    set_staircase(proto_exponentialdecayconf.staircase());
  }
    
}

void ConstExponentialDecayConf::_ExponentialDecayConf_::ToProto(::oneflow::ExponentialDecayConf* proto_exponentialdecayconf) const {
  proto_exponentialdecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_exponentialdecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: decay_rate
  if (this->has_decay_rate()) {
    proto_exponentialdecayconf->set_decay_rate(decay_rate());
    }
  // required_or_optional field: staircase
  if (this->has_staircase()) {
    proto_exponentialdecayconf->set_staircase(staircase());
    }

}

::std::string ConstExponentialDecayConf::_ExponentialDecayConf_::DebugString() const {
  ::oneflow::ExponentialDecayConf proto_exponentialdecayconf;
  this->ToProto(&proto_exponentialdecayconf);
  return proto_exponentialdecayconf.DebugString();
}

void ConstExponentialDecayConf::_ExponentialDecayConf_::Clear() {
  clear_decay_batches();
  clear_decay_rate();
  clear_staircase();
}

void ConstExponentialDecayConf::_ExponentialDecayConf_::CopyFrom(const _ExponentialDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_decay_rate()) {
    set_decay_rate(other.decay_rate());
  } else {
    clear_decay_rate();
  }
  if (other.has_staircase()) {
    set_staircase(other.staircase());
  } else {
    clear_staircase();
  }
}


// optional field decay_batches
bool ConstExponentialDecayConf::_ExponentialDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstExponentialDecayConf::_ExponentialDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstExponentialDecayConf::_ExponentialDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field decay_rate
bool ConstExponentialDecayConf::_ExponentialDecayConf_::has_decay_rate() const {
  return has_decay_rate_;
}
const double& ConstExponentialDecayConf::_ExponentialDecayConf_::decay_rate() const {
  if (has_decay_rate_) { return decay_rate_; }
  static const double default_static_value = double();
  return default_static_value;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::clear_decay_rate() {
  has_decay_rate_ = false;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::set_decay_rate(const double& value) {
  decay_rate_ = value;
  has_decay_rate_ = true;
}
double* ConstExponentialDecayConf::_ExponentialDecayConf_::mutable_decay_rate() {
  has_decay_rate_ = true;
  return &decay_rate_;
}

// optional field staircase
bool ConstExponentialDecayConf::_ExponentialDecayConf_::has_staircase() const {
  return has_staircase_;
}
const bool& ConstExponentialDecayConf::_ExponentialDecayConf_::staircase() const {
  if (has_staircase_) { return staircase_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::clear_staircase() {
  has_staircase_ = false;
}
void ConstExponentialDecayConf::_ExponentialDecayConf_::set_staircase(const bool& value) {
  staircase_ = value;
  has_staircase_ = true;
}
bool* ConstExponentialDecayConf::_ExponentialDecayConf_::mutable_staircase() {
  has_staircase_ = true;
  return &staircase_;
}


int ConstExponentialDecayConf::_ExponentialDecayConf_::compare(const _ExponentialDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_decay_rate() == other.has_decay_rate())) {
    return has_decay_rate() < other.has_decay_rate() ? -1 : 1;
  } else if (!(decay_rate() == other.decay_rate())) {
    return decay_rate() < other.decay_rate() ? -1 : 1;
  }
  if (!(has_staircase() == other.has_staircase())) {
    return has_staircase() < other.has_staircase() ? -1 : 1;
  } else if (!(staircase() == other.staircase())) {
    return staircase() < other.staircase() ? -1 : 1;
  }
  return 0;
}

bool ConstExponentialDecayConf::_ExponentialDecayConf_::operator==(const _ExponentialDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_decay_rate() == other.has_decay_rate() 
    && decay_rate() == other.decay_rate()
    && has_staircase() == other.has_staircase() 
    && staircase() == other.staircase()
  ;
}

std::size_t ConstExponentialDecayConf::_ExponentialDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_decay_rate() ? std::hash<double>()(decay_rate()) : 0)
    ^ (has_staircase() ? std::hash<bool>()(staircase()) : 0)
  ;
}

bool ConstExponentialDecayConf::_ExponentialDecayConf_::operator<(const _ExponentialDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_decay_rate() == other.has_decay_rate()) ? 
      has_decay_rate() < other.has_decay_rate() : false
    || !(decay_rate() == other.decay_rate()) ? 
      decay_rate() < other.decay_rate() : false
    || !(has_staircase() == other.has_staircase()) ? 
      has_staircase() < other.has_staircase() : false
    || !(staircase() == other.staircase()) ? 
      staircase() < other.staircase() : false
  ;
}

using _ExponentialDecayConf_ =  ConstExponentialDecayConf::_ExponentialDecayConf_;
ConstExponentialDecayConf::ConstExponentialDecayConf(const ::std::shared_ptr<_ExponentialDecayConf_>& data): data_(data) {}
ConstExponentialDecayConf::ConstExponentialDecayConf(): data_(::std::make_shared<_ExponentialDecayConf_>()) {}
ConstExponentialDecayConf::ConstExponentialDecayConf(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf) {
  BuildFromProto(proto_exponentialdecayconf);
}
ConstExponentialDecayConf::ConstExponentialDecayConf(const ConstExponentialDecayConf&) = default;
ConstExponentialDecayConf::ConstExponentialDecayConf(ConstExponentialDecayConf&&) noexcept = default;
ConstExponentialDecayConf::~ConstExponentialDecayConf() = default;

void ConstExponentialDecayConf::ToProto(PbMessage* proto_exponentialdecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ExponentialDecayConf*>(proto_exponentialdecayconf));
}
  
::std::string ConstExponentialDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstExponentialDecayConf::__Empty__() const {
  return !data_;
}

int ConstExponentialDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"decay_rate", 2},
    {"staircase", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstExponentialDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstExponentialDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstExponentialDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &decay_rate();
    case 3: return &staircase();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstExponentialDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstExponentialDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field decay_rate
bool ConstExponentialDecayConf::has_decay_rate() const {
  return __SharedPtrOrDefault__()->has_decay_rate();
}
const double& ConstExponentialDecayConf::decay_rate() const {
  return __SharedPtrOrDefault__()->decay_rate();
}
// used by pybind11 only
// required or optional field staircase
bool ConstExponentialDecayConf::has_staircase() const {
  return __SharedPtrOrDefault__()->has_staircase();
}
const bool& ConstExponentialDecayConf::staircase() const {
  return __SharedPtrOrDefault__()->staircase();
}
// used by pybind11 only

::std::shared_ptr<ConstExponentialDecayConf> ConstExponentialDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstExponentialDecayConf>(data_);
}
int64_t ConstExponentialDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstExponentialDecayConf::operator==(const ConstExponentialDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstExponentialDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstExponentialDecayConf::operator<(const ConstExponentialDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ExponentialDecayConf_>& ConstExponentialDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ExponentialDecayConf_> default_ptr = std::make_shared<_ExponentialDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ExponentialDecayConf_>& ConstExponentialDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ExponentialDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstExponentialDecayConf
void ConstExponentialDecayConf::BuildFromProto(const PbMessage& proto_exponentialdecayconf) {
  data_ = ::std::make_shared<_ExponentialDecayConf_>(dynamic_cast<const ::oneflow::ExponentialDecayConf&>(proto_exponentialdecayconf));
}

ExponentialDecayConf::ExponentialDecayConf(const ::std::shared_ptr<_ExponentialDecayConf_>& data)
  : ConstExponentialDecayConf(data) {}
ExponentialDecayConf::ExponentialDecayConf(const ExponentialDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ExponentialDecayConf> resize
ExponentialDecayConf::ExponentialDecayConf(ExponentialDecayConf&&) noexcept = default; 
ExponentialDecayConf::ExponentialDecayConf(const ::oneflow::ExponentialDecayConf& proto_exponentialdecayconf) {
  InitFromProto(proto_exponentialdecayconf);
}
ExponentialDecayConf::ExponentialDecayConf() = default;

ExponentialDecayConf::~ExponentialDecayConf() = default;

void ExponentialDecayConf::InitFromProto(const PbMessage& proto_exponentialdecayconf) {
  BuildFromProto(proto_exponentialdecayconf);
}
  
void* ExponentialDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_decay_rate();
    case 3: return mutable_staircase();
    default: return nullptr;
  }
}

bool ExponentialDecayConf::operator==(const ExponentialDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ExponentialDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ExponentialDecayConf::operator<(const ExponentialDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ExponentialDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void ExponentialDecayConf::CopyFrom(const ExponentialDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ExponentialDecayConf& ExponentialDecayConf::operator=(const ExponentialDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void ExponentialDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void ExponentialDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* ExponentialDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field decay_rate
void ExponentialDecayConf::clear_decay_rate() {
  return __SharedPtr__()->clear_decay_rate();
}
void ExponentialDecayConf::set_decay_rate(const double& value) {
  return __SharedPtr__()->set_decay_rate(value);
}
double* ExponentialDecayConf::mutable_decay_rate() {
  return  __SharedPtr__()->mutable_decay_rate();
}
// required or optional field staircase
void ExponentialDecayConf::clear_staircase() {
  return __SharedPtr__()->clear_staircase();
}
void ExponentialDecayConf::set_staircase(const bool& value) {
  return __SharedPtr__()->set_staircase(value);
}
bool* ExponentialDecayConf::mutable_staircase() {
  return  __SharedPtr__()->mutable_staircase();
}

::std::shared_ptr<ExponentialDecayConf> ExponentialDecayConf::__SharedMutable__() {
  return ::std::make_shared<ExponentialDecayConf>(__SharedPtr__());
}
ConstInverseTimeDecayConf::_InverseTimeDecayConf_::_InverseTimeDecayConf_() { Clear(); }
ConstInverseTimeDecayConf::_InverseTimeDecayConf_::_InverseTimeDecayConf_(const _InverseTimeDecayConf_& other) { CopyFrom(other); }
ConstInverseTimeDecayConf::_InverseTimeDecayConf_::_InverseTimeDecayConf_(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf) {
  InitFromProto(proto_inversetimedecayconf);
}
ConstInverseTimeDecayConf::_InverseTimeDecayConf_::_InverseTimeDecayConf_(_InverseTimeDecayConf_&& other) = default;
ConstInverseTimeDecayConf::_InverseTimeDecayConf_::~_InverseTimeDecayConf_() = default;

void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::InitFromProto(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_inversetimedecayconf.has_decay_batches()) {
    set_decay_batches(proto_inversetimedecayconf.decay_batches());
  }
  // required_or_optional field: decay_rate
  if (proto_inversetimedecayconf.has_decay_rate()) {
    set_decay_rate(proto_inversetimedecayconf.decay_rate());
  }
  // required_or_optional field: staircase
  if (proto_inversetimedecayconf.has_staircase()) {
    set_staircase(proto_inversetimedecayconf.staircase());
  }
    
}

void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::ToProto(::oneflow::InverseTimeDecayConf* proto_inversetimedecayconf) const {
  proto_inversetimedecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_inversetimedecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: decay_rate
  if (this->has_decay_rate()) {
    proto_inversetimedecayconf->set_decay_rate(decay_rate());
    }
  // required_or_optional field: staircase
  if (this->has_staircase()) {
    proto_inversetimedecayconf->set_staircase(staircase());
    }

}

::std::string ConstInverseTimeDecayConf::_InverseTimeDecayConf_::DebugString() const {
  ::oneflow::InverseTimeDecayConf proto_inversetimedecayconf;
  this->ToProto(&proto_inversetimedecayconf);
  return proto_inversetimedecayconf.DebugString();
}

void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::Clear() {
  clear_decay_batches();
  clear_decay_rate();
  clear_staircase();
}

void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::CopyFrom(const _InverseTimeDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_decay_rate()) {
    set_decay_rate(other.decay_rate());
  } else {
    clear_decay_rate();
  }
  if (other.has_staircase()) {
    set_staircase(other.staircase());
  } else {
    clear_staircase();
  }
}


// optional field decay_batches
bool ConstInverseTimeDecayConf::_InverseTimeDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstInverseTimeDecayConf::_InverseTimeDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstInverseTimeDecayConf::_InverseTimeDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field decay_rate
bool ConstInverseTimeDecayConf::_InverseTimeDecayConf_::has_decay_rate() const {
  return has_decay_rate_;
}
const double& ConstInverseTimeDecayConf::_InverseTimeDecayConf_::decay_rate() const {
  if (has_decay_rate_) { return decay_rate_; }
  static const double default_static_value = double();
  return default_static_value;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::clear_decay_rate() {
  has_decay_rate_ = false;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::set_decay_rate(const double& value) {
  decay_rate_ = value;
  has_decay_rate_ = true;
}
double* ConstInverseTimeDecayConf::_InverseTimeDecayConf_::mutable_decay_rate() {
  has_decay_rate_ = true;
  return &decay_rate_;
}

// optional field staircase
bool ConstInverseTimeDecayConf::_InverseTimeDecayConf_::has_staircase() const {
  return has_staircase_;
}
const bool& ConstInverseTimeDecayConf::_InverseTimeDecayConf_::staircase() const {
  if (has_staircase_) { return staircase_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::clear_staircase() {
  has_staircase_ = false;
}
void ConstInverseTimeDecayConf::_InverseTimeDecayConf_::set_staircase(const bool& value) {
  staircase_ = value;
  has_staircase_ = true;
}
bool* ConstInverseTimeDecayConf::_InverseTimeDecayConf_::mutable_staircase() {
  has_staircase_ = true;
  return &staircase_;
}


int ConstInverseTimeDecayConf::_InverseTimeDecayConf_::compare(const _InverseTimeDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_decay_rate() == other.has_decay_rate())) {
    return has_decay_rate() < other.has_decay_rate() ? -1 : 1;
  } else if (!(decay_rate() == other.decay_rate())) {
    return decay_rate() < other.decay_rate() ? -1 : 1;
  }
  if (!(has_staircase() == other.has_staircase())) {
    return has_staircase() < other.has_staircase() ? -1 : 1;
  } else if (!(staircase() == other.staircase())) {
    return staircase() < other.staircase() ? -1 : 1;
  }
  return 0;
}

bool ConstInverseTimeDecayConf::_InverseTimeDecayConf_::operator==(const _InverseTimeDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_decay_rate() == other.has_decay_rate() 
    && decay_rate() == other.decay_rate()
    && has_staircase() == other.has_staircase() 
    && staircase() == other.staircase()
  ;
}

std::size_t ConstInverseTimeDecayConf::_InverseTimeDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_decay_rate() ? std::hash<double>()(decay_rate()) : 0)
    ^ (has_staircase() ? std::hash<bool>()(staircase()) : 0)
  ;
}

bool ConstInverseTimeDecayConf::_InverseTimeDecayConf_::operator<(const _InverseTimeDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_decay_rate() == other.has_decay_rate()) ? 
      has_decay_rate() < other.has_decay_rate() : false
    || !(decay_rate() == other.decay_rate()) ? 
      decay_rate() < other.decay_rate() : false
    || !(has_staircase() == other.has_staircase()) ? 
      has_staircase() < other.has_staircase() : false
    || !(staircase() == other.staircase()) ? 
      staircase() < other.staircase() : false
  ;
}

using _InverseTimeDecayConf_ =  ConstInverseTimeDecayConf::_InverseTimeDecayConf_;
ConstInverseTimeDecayConf::ConstInverseTimeDecayConf(const ::std::shared_ptr<_InverseTimeDecayConf_>& data): data_(data) {}
ConstInverseTimeDecayConf::ConstInverseTimeDecayConf(): data_(::std::make_shared<_InverseTimeDecayConf_>()) {}
ConstInverseTimeDecayConf::ConstInverseTimeDecayConf(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf) {
  BuildFromProto(proto_inversetimedecayconf);
}
ConstInverseTimeDecayConf::ConstInverseTimeDecayConf(const ConstInverseTimeDecayConf&) = default;
ConstInverseTimeDecayConf::ConstInverseTimeDecayConf(ConstInverseTimeDecayConf&&) noexcept = default;
ConstInverseTimeDecayConf::~ConstInverseTimeDecayConf() = default;

void ConstInverseTimeDecayConf::ToProto(PbMessage* proto_inversetimedecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InverseTimeDecayConf*>(proto_inversetimedecayconf));
}
  
::std::string ConstInverseTimeDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInverseTimeDecayConf::__Empty__() const {
  return !data_;
}

int ConstInverseTimeDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"decay_rate", 2},
    {"staircase", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInverseTimeDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInverseTimeDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInverseTimeDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &decay_rate();
    case 3: return &staircase();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstInverseTimeDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstInverseTimeDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field decay_rate
bool ConstInverseTimeDecayConf::has_decay_rate() const {
  return __SharedPtrOrDefault__()->has_decay_rate();
}
const double& ConstInverseTimeDecayConf::decay_rate() const {
  return __SharedPtrOrDefault__()->decay_rate();
}
// used by pybind11 only
// required or optional field staircase
bool ConstInverseTimeDecayConf::has_staircase() const {
  return __SharedPtrOrDefault__()->has_staircase();
}
const bool& ConstInverseTimeDecayConf::staircase() const {
  return __SharedPtrOrDefault__()->staircase();
}
// used by pybind11 only

::std::shared_ptr<ConstInverseTimeDecayConf> ConstInverseTimeDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstInverseTimeDecayConf>(data_);
}
int64_t ConstInverseTimeDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInverseTimeDecayConf::operator==(const ConstInverseTimeDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInverseTimeDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInverseTimeDecayConf::operator<(const ConstInverseTimeDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InverseTimeDecayConf_>& ConstInverseTimeDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InverseTimeDecayConf_> default_ptr = std::make_shared<_InverseTimeDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_InverseTimeDecayConf_>& ConstInverseTimeDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _InverseTimeDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInverseTimeDecayConf
void ConstInverseTimeDecayConf::BuildFromProto(const PbMessage& proto_inversetimedecayconf) {
  data_ = ::std::make_shared<_InverseTimeDecayConf_>(dynamic_cast<const ::oneflow::InverseTimeDecayConf&>(proto_inversetimedecayconf));
}

InverseTimeDecayConf::InverseTimeDecayConf(const ::std::shared_ptr<_InverseTimeDecayConf_>& data)
  : ConstInverseTimeDecayConf(data) {}
InverseTimeDecayConf::InverseTimeDecayConf(const InverseTimeDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InverseTimeDecayConf> resize
InverseTimeDecayConf::InverseTimeDecayConf(InverseTimeDecayConf&&) noexcept = default; 
InverseTimeDecayConf::InverseTimeDecayConf(const ::oneflow::InverseTimeDecayConf& proto_inversetimedecayconf) {
  InitFromProto(proto_inversetimedecayconf);
}
InverseTimeDecayConf::InverseTimeDecayConf() = default;

InverseTimeDecayConf::~InverseTimeDecayConf() = default;

void InverseTimeDecayConf::InitFromProto(const PbMessage& proto_inversetimedecayconf) {
  BuildFromProto(proto_inversetimedecayconf);
}
  
void* InverseTimeDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_decay_rate();
    case 3: return mutable_staircase();
    default: return nullptr;
  }
}

bool InverseTimeDecayConf::operator==(const InverseTimeDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InverseTimeDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InverseTimeDecayConf::operator<(const InverseTimeDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InverseTimeDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void InverseTimeDecayConf::CopyFrom(const InverseTimeDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InverseTimeDecayConf& InverseTimeDecayConf::operator=(const InverseTimeDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void InverseTimeDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void InverseTimeDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* InverseTimeDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field decay_rate
void InverseTimeDecayConf::clear_decay_rate() {
  return __SharedPtr__()->clear_decay_rate();
}
void InverseTimeDecayConf::set_decay_rate(const double& value) {
  return __SharedPtr__()->set_decay_rate(value);
}
double* InverseTimeDecayConf::mutable_decay_rate() {
  return  __SharedPtr__()->mutable_decay_rate();
}
// required or optional field staircase
void InverseTimeDecayConf::clear_staircase() {
  return __SharedPtr__()->clear_staircase();
}
void InverseTimeDecayConf::set_staircase(const bool& value) {
  return __SharedPtr__()->set_staircase(value);
}
bool* InverseTimeDecayConf::mutable_staircase() {
  return  __SharedPtr__()->mutable_staircase();
}

::std::shared_ptr<InverseTimeDecayConf> InverseTimeDecayConf::__SharedMutable__() {
  return ::std::make_shared<InverseTimeDecayConf>(__SharedPtr__());
}
ConstNaturalExpDecayConf::_NaturalExpDecayConf_::_NaturalExpDecayConf_() { Clear(); }
ConstNaturalExpDecayConf::_NaturalExpDecayConf_::_NaturalExpDecayConf_(const _NaturalExpDecayConf_& other) { CopyFrom(other); }
ConstNaturalExpDecayConf::_NaturalExpDecayConf_::_NaturalExpDecayConf_(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf) {
  InitFromProto(proto_naturalexpdecayconf);
}
ConstNaturalExpDecayConf::_NaturalExpDecayConf_::_NaturalExpDecayConf_(_NaturalExpDecayConf_&& other) = default;
ConstNaturalExpDecayConf::_NaturalExpDecayConf_::~_NaturalExpDecayConf_() = default;

void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::InitFromProto(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_naturalexpdecayconf.has_decay_batches()) {
    set_decay_batches(proto_naturalexpdecayconf.decay_batches());
  }
  // required_or_optional field: decay_rate
  if (proto_naturalexpdecayconf.has_decay_rate()) {
    set_decay_rate(proto_naturalexpdecayconf.decay_rate());
  }
  // required_or_optional field: staircase
  if (proto_naturalexpdecayconf.has_staircase()) {
    set_staircase(proto_naturalexpdecayconf.staircase());
  }
    
}

void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::ToProto(::oneflow::NaturalExpDecayConf* proto_naturalexpdecayconf) const {
  proto_naturalexpdecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_naturalexpdecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: decay_rate
  if (this->has_decay_rate()) {
    proto_naturalexpdecayconf->set_decay_rate(decay_rate());
    }
  // required_or_optional field: staircase
  if (this->has_staircase()) {
    proto_naturalexpdecayconf->set_staircase(staircase());
    }

}

::std::string ConstNaturalExpDecayConf::_NaturalExpDecayConf_::DebugString() const {
  ::oneflow::NaturalExpDecayConf proto_naturalexpdecayconf;
  this->ToProto(&proto_naturalexpdecayconf);
  return proto_naturalexpdecayconf.DebugString();
}

void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::Clear() {
  clear_decay_batches();
  clear_decay_rate();
  clear_staircase();
}

void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::CopyFrom(const _NaturalExpDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_decay_rate()) {
    set_decay_rate(other.decay_rate());
  } else {
    clear_decay_rate();
  }
  if (other.has_staircase()) {
    set_staircase(other.staircase());
  } else {
    clear_staircase();
  }
}


// optional field decay_batches
bool ConstNaturalExpDecayConf::_NaturalExpDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstNaturalExpDecayConf::_NaturalExpDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstNaturalExpDecayConf::_NaturalExpDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field decay_rate
bool ConstNaturalExpDecayConf::_NaturalExpDecayConf_::has_decay_rate() const {
  return has_decay_rate_;
}
const double& ConstNaturalExpDecayConf::_NaturalExpDecayConf_::decay_rate() const {
  if (has_decay_rate_) { return decay_rate_; }
  static const double default_static_value = double();
  return default_static_value;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::clear_decay_rate() {
  has_decay_rate_ = false;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::set_decay_rate(const double& value) {
  decay_rate_ = value;
  has_decay_rate_ = true;
}
double* ConstNaturalExpDecayConf::_NaturalExpDecayConf_::mutable_decay_rate() {
  has_decay_rate_ = true;
  return &decay_rate_;
}

// optional field staircase
bool ConstNaturalExpDecayConf::_NaturalExpDecayConf_::has_staircase() const {
  return has_staircase_;
}
const bool& ConstNaturalExpDecayConf::_NaturalExpDecayConf_::staircase() const {
  if (has_staircase_) { return staircase_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::clear_staircase() {
  has_staircase_ = false;
}
void ConstNaturalExpDecayConf::_NaturalExpDecayConf_::set_staircase(const bool& value) {
  staircase_ = value;
  has_staircase_ = true;
}
bool* ConstNaturalExpDecayConf::_NaturalExpDecayConf_::mutable_staircase() {
  has_staircase_ = true;
  return &staircase_;
}


int ConstNaturalExpDecayConf::_NaturalExpDecayConf_::compare(const _NaturalExpDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_decay_rate() == other.has_decay_rate())) {
    return has_decay_rate() < other.has_decay_rate() ? -1 : 1;
  } else if (!(decay_rate() == other.decay_rate())) {
    return decay_rate() < other.decay_rate() ? -1 : 1;
  }
  if (!(has_staircase() == other.has_staircase())) {
    return has_staircase() < other.has_staircase() ? -1 : 1;
  } else if (!(staircase() == other.staircase())) {
    return staircase() < other.staircase() ? -1 : 1;
  }
  return 0;
}

bool ConstNaturalExpDecayConf::_NaturalExpDecayConf_::operator==(const _NaturalExpDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_decay_rate() == other.has_decay_rate() 
    && decay_rate() == other.decay_rate()
    && has_staircase() == other.has_staircase() 
    && staircase() == other.staircase()
  ;
}

std::size_t ConstNaturalExpDecayConf::_NaturalExpDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_decay_rate() ? std::hash<double>()(decay_rate()) : 0)
    ^ (has_staircase() ? std::hash<bool>()(staircase()) : 0)
  ;
}

bool ConstNaturalExpDecayConf::_NaturalExpDecayConf_::operator<(const _NaturalExpDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_decay_rate() == other.has_decay_rate()) ? 
      has_decay_rate() < other.has_decay_rate() : false
    || !(decay_rate() == other.decay_rate()) ? 
      decay_rate() < other.decay_rate() : false
    || !(has_staircase() == other.has_staircase()) ? 
      has_staircase() < other.has_staircase() : false
    || !(staircase() == other.staircase()) ? 
      staircase() < other.staircase() : false
  ;
}

using _NaturalExpDecayConf_ =  ConstNaturalExpDecayConf::_NaturalExpDecayConf_;
ConstNaturalExpDecayConf::ConstNaturalExpDecayConf(const ::std::shared_ptr<_NaturalExpDecayConf_>& data): data_(data) {}
ConstNaturalExpDecayConf::ConstNaturalExpDecayConf(): data_(::std::make_shared<_NaturalExpDecayConf_>()) {}
ConstNaturalExpDecayConf::ConstNaturalExpDecayConf(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf) {
  BuildFromProto(proto_naturalexpdecayconf);
}
ConstNaturalExpDecayConf::ConstNaturalExpDecayConf(const ConstNaturalExpDecayConf&) = default;
ConstNaturalExpDecayConf::ConstNaturalExpDecayConf(ConstNaturalExpDecayConf&&) noexcept = default;
ConstNaturalExpDecayConf::~ConstNaturalExpDecayConf() = default;

void ConstNaturalExpDecayConf::ToProto(PbMessage* proto_naturalexpdecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::NaturalExpDecayConf*>(proto_naturalexpdecayconf));
}
  
::std::string ConstNaturalExpDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstNaturalExpDecayConf::__Empty__() const {
  return !data_;
}

int ConstNaturalExpDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"decay_rate", 2},
    {"staircase", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstNaturalExpDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstNaturalExpDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstNaturalExpDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &decay_rate();
    case 3: return &staircase();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstNaturalExpDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstNaturalExpDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field decay_rate
bool ConstNaturalExpDecayConf::has_decay_rate() const {
  return __SharedPtrOrDefault__()->has_decay_rate();
}
const double& ConstNaturalExpDecayConf::decay_rate() const {
  return __SharedPtrOrDefault__()->decay_rate();
}
// used by pybind11 only
// required or optional field staircase
bool ConstNaturalExpDecayConf::has_staircase() const {
  return __SharedPtrOrDefault__()->has_staircase();
}
const bool& ConstNaturalExpDecayConf::staircase() const {
  return __SharedPtrOrDefault__()->staircase();
}
// used by pybind11 only

::std::shared_ptr<ConstNaturalExpDecayConf> ConstNaturalExpDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstNaturalExpDecayConf>(data_);
}
int64_t ConstNaturalExpDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstNaturalExpDecayConf::operator==(const ConstNaturalExpDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstNaturalExpDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstNaturalExpDecayConf::operator<(const ConstNaturalExpDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_NaturalExpDecayConf_>& ConstNaturalExpDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_NaturalExpDecayConf_> default_ptr = std::make_shared<_NaturalExpDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_NaturalExpDecayConf_>& ConstNaturalExpDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _NaturalExpDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstNaturalExpDecayConf
void ConstNaturalExpDecayConf::BuildFromProto(const PbMessage& proto_naturalexpdecayconf) {
  data_ = ::std::make_shared<_NaturalExpDecayConf_>(dynamic_cast<const ::oneflow::NaturalExpDecayConf&>(proto_naturalexpdecayconf));
}

NaturalExpDecayConf::NaturalExpDecayConf(const ::std::shared_ptr<_NaturalExpDecayConf_>& data)
  : ConstNaturalExpDecayConf(data) {}
NaturalExpDecayConf::NaturalExpDecayConf(const NaturalExpDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<NaturalExpDecayConf> resize
NaturalExpDecayConf::NaturalExpDecayConf(NaturalExpDecayConf&&) noexcept = default; 
NaturalExpDecayConf::NaturalExpDecayConf(const ::oneflow::NaturalExpDecayConf& proto_naturalexpdecayconf) {
  InitFromProto(proto_naturalexpdecayconf);
}
NaturalExpDecayConf::NaturalExpDecayConf() = default;

NaturalExpDecayConf::~NaturalExpDecayConf() = default;

void NaturalExpDecayConf::InitFromProto(const PbMessage& proto_naturalexpdecayconf) {
  BuildFromProto(proto_naturalexpdecayconf);
}
  
void* NaturalExpDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_decay_rate();
    case 3: return mutable_staircase();
    default: return nullptr;
  }
}

bool NaturalExpDecayConf::operator==(const NaturalExpDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t NaturalExpDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool NaturalExpDecayConf::operator<(const NaturalExpDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void NaturalExpDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void NaturalExpDecayConf::CopyFrom(const NaturalExpDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
NaturalExpDecayConf& NaturalExpDecayConf::operator=(const NaturalExpDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void NaturalExpDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void NaturalExpDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* NaturalExpDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field decay_rate
void NaturalExpDecayConf::clear_decay_rate() {
  return __SharedPtr__()->clear_decay_rate();
}
void NaturalExpDecayConf::set_decay_rate(const double& value) {
  return __SharedPtr__()->set_decay_rate(value);
}
double* NaturalExpDecayConf::mutable_decay_rate() {
  return  __SharedPtr__()->mutable_decay_rate();
}
// required or optional field staircase
void NaturalExpDecayConf::clear_staircase() {
  return __SharedPtr__()->clear_staircase();
}
void NaturalExpDecayConf::set_staircase(const bool& value) {
  return __SharedPtr__()->set_staircase(value);
}
bool* NaturalExpDecayConf::mutable_staircase() {
  return  __SharedPtr__()->mutable_staircase();
}

::std::shared_ptr<NaturalExpDecayConf> NaturalExpDecayConf::__SharedMutable__() {
  return ::std::make_shared<NaturalExpDecayConf>(__SharedPtr__());
}
ConstPiecewiseConstantConf::_PiecewiseConstantConf_::_PiecewiseConstantConf_() { Clear(); }
ConstPiecewiseConstantConf::_PiecewiseConstantConf_::_PiecewiseConstantConf_(const _PiecewiseConstantConf_& other) { CopyFrom(other); }
ConstPiecewiseConstantConf::_PiecewiseConstantConf_::_PiecewiseConstantConf_(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf) {
  InitFromProto(proto_piecewiseconstantconf);
}
ConstPiecewiseConstantConf::_PiecewiseConstantConf_::_PiecewiseConstantConf_(_PiecewiseConstantConf_&& other) = default;
ConstPiecewiseConstantConf::_PiecewiseConstantConf_::~_PiecewiseConstantConf_() = default;

void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::InitFromProto(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf) {
  Clear();
  // repeated field: boundaries
  if (!proto_piecewiseconstantconf.boundaries().empty()) {
    for (const int64_t& elem : proto_piecewiseconstantconf.boundaries()) {
      add_boundaries(elem);
    }
  }
  // repeated field: values
  if (!proto_piecewiseconstantconf.values().empty()) {
    for (const double& elem : proto_piecewiseconstantconf.values()) {
      add_values(elem);
    }
  }
    
}

void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::ToProto(::oneflow::PiecewiseConstantConf* proto_piecewiseconstantconf) const {
  proto_piecewiseconstantconf->Clear();
  // repeated field: boundaries
  if (!boundaries().empty()) {
    for (const int64_t& elem : boundaries()) {
      proto_piecewiseconstantconf->add_boundaries(elem);
    }
  }
  // repeated field: values
  if (!values().empty()) {
    for (const double& elem : values()) {
      proto_piecewiseconstantconf->add_values(elem);
    }
  }

}

::std::string ConstPiecewiseConstantConf::_PiecewiseConstantConf_::DebugString() const {
  ::oneflow::PiecewiseConstantConf proto_piecewiseconstantconf;
  this->ToProto(&proto_piecewiseconstantconf);
  return proto_piecewiseconstantconf.DebugString();
}

void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::Clear() {
  clear_boundaries();
  clear_values();
}

void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::CopyFrom(const _PiecewiseConstantConf_& other) {
  mutable_boundaries()->CopyFrom(other.boundaries());
  mutable_values()->CopyFrom(other.values());
}


// repeated field boundaries
::std::size_t ConstPiecewiseConstantConf::_PiecewiseConstantConf_::boundaries_size() const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->size();
  }
  return boundaries_->size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& ConstPiecewiseConstantConf::_PiecewiseConstantConf_::boundaries() const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return *(default_static_value.get());
  }
  return *(boundaries_.get());
}
const int64_t& ConstPiecewiseConstantConf::_PiecewiseConstantConf_::boundaries(::std::size_t index) const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->Get(index);
  }
  return boundaries_->Get(index);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::clear_boundaries() {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* ConstPiecewiseConstantConf::_PiecewiseConstantConf_::mutable_boundaries() {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return  boundaries_.get();
}
int64_t* ConstPiecewiseConstantConf::_PiecewiseConstantConf_::mutable_boundaries(::std::size_t index) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return  boundaries_->Mutable(index);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::add_boundaries(const int64_t& value) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Add(value);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::set_boundaries(::std::size_t index, const int64_t& value) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Set(index, value);
}

// repeated field values
::std::size_t ConstPiecewiseConstantConf::_PiecewiseConstantConf_::values_size() const {
  if (!values_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return default_static_value->size();
  }
  return values_->size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& ConstPiecewiseConstantConf::_PiecewiseConstantConf_::values() const {
  if (!values_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return *(default_static_value.get());
  }
  return *(values_.get());
}
const double& ConstPiecewiseConstantConf::_PiecewiseConstantConf_::values(::std::size_t index) const {
  if (!values_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return default_static_value->Get(index);
  }
  return values_->Get(index);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::clear_values() {
  if (!values_) {
    values_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return values_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* ConstPiecewiseConstantConf::_PiecewiseConstantConf_::mutable_values() {
  if (!values_) {
    values_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return  values_.get();
}
double* ConstPiecewiseConstantConf::_PiecewiseConstantConf_::mutable_values(::std::size_t index) {
  if (!values_) {
    values_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return  values_->Mutable(index);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::add_values(const double& value) {
  if (!values_) {
    values_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return values_->Add(value);
}
void ConstPiecewiseConstantConf::_PiecewiseConstantConf_::set_values(::std::size_t index, const double& value) {
  if (!values_) {
    values_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return values_->Set(index, value);
}


int ConstPiecewiseConstantConf::_PiecewiseConstantConf_::compare(const _PiecewiseConstantConf_& other) {
  if (!(boundaries() == other.boundaries())) {
    return boundaries() < other.boundaries() ? -1 : 1;
  }
  if (!(values() == other.values())) {
    return values() < other.values() ? -1 : 1;
  }
  return 0;
}

bool ConstPiecewiseConstantConf::_PiecewiseConstantConf_::operator==(const _PiecewiseConstantConf_& other) const {
  return true
    && boundaries() == other.boundaries()
    && values() == other.values()
  ;
}

std::size_t ConstPiecewiseConstantConf::_PiecewiseConstantConf_::__CalcHash__() const {
  return 0
    ^ boundaries().__CalcHash__()
    ^ values().__CalcHash__()
  ;
}

bool ConstPiecewiseConstantConf::_PiecewiseConstantConf_::operator<(const _PiecewiseConstantConf_& other) const {
  return false
    || !(boundaries() == other.boundaries()) ? 
      boundaries() < other.boundaries() : false
    || !(values() == other.values()) ? 
      values() < other.values() : false
  ;
}

using _PiecewiseConstantConf_ =  ConstPiecewiseConstantConf::_PiecewiseConstantConf_;
ConstPiecewiseConstantConf::ConstPiecewiseConstantConf(const ::std::shared_ptr<_PiecewiseConstantConf_>& data): data_(data) {}
ConstPiecewiseConstantConf::ConstPiecewiseConstantConf(): data_(::std::make_shared<_PiecewiseConstantConf_>()) {}
ConstPiecewiseConstantConf::ConstPiecewiseConstantConf(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf) {
  BuildFromProto(proto_piecewiseconstantconf);
}
ConstPiecewiseConstantConf::ConstPiecewiseConstantConf(const ConstPiecewiseConstantConf&) = default;
ConstPiecewiseConstantConf::ConstPiecewiseConstantConf(ConstPiecewiseConstantConf&&) noexcept = default;
ConstPiecewiseConstantConf::~ConstPiecewiseConstantConf() = default;

void ConstPiecewiseConstantConf::ToProto(PbMessage* proto_piecewiseconstantconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PiecewiseConstantConf*>(proto_piecewiseconstantconf));
}
  
::std::string ConstPiecewiseConstantConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPiecewiseConstantConf::__Empty__() const {
  return !data_;
}

int ConstPiecewiseConstantConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"boundaries", 1},
    {"values", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPiecewiseConstantConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPiecewiseConstantConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int64_t>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<double>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPiecewiseConstantConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &boundaries();
    case 2: return &values();
    default: return nullptr;
  }
}

// repeated field boundaries
::std::size_t ConstPiecewiseConstantConf::boundaries_size() const {
  return __SharedPtrOrDefault__()->boundaries_size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& ConstPiecewiseConstantConf::boundaries() const {
  return __SharedPtrOrDefault__()->boundaries();
}
const int64_t& ConstPiecewiseConstantConf::boundaries(::std::size_t index) const {
  return __SharedPtrOrDefault__()->boundaries(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> ConstPiecewiseConstantConf::shared_const_boundaries() const {
  return boundaries().__SharedConst__();
}
// repeated field values
::std::size_t ConstPiecewiseConstantConf::values_size() const {
  return __SharedPtrOrDefault__()->values_size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& ConstPiecewiseConstantConf::values() const {
  return __SharedPtrOrDefault__()->values();
}
const double& ConstPiecewiseConstantConf::values(::std::size_t index) const {
  return __SharedPtrOrDefault__()->values(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> ConstPiecewiseConstantConf::shared_const_values() const {
  return values().__SharedConst__();
}

::std::shared_ptr<ConstPiecewiseConstantConf> ConstPiecewiseConstantConf::__SharedConst__() const {
  return ::std::make_shared<ConstPiecewiseConstantConf>(data_);
}
int64_t ConstPiecewiseConstantConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPiecewiseConstantConf::operator==(const ConstPiecewiseConstantConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPiecewiseConstantConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPiecewiseConstantConf::operator<(const ConstPiecewiseConstantConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PiecewiseConstantConf_>& ConstPiecewiseConstantConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PiecewiseConstantConf_> default_ptr = std::make_shared<_PiecewiseConstantConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_PiecewiseConstantConf_>& ConstPiecewiseConstantConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _PiecewiseConstantConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPiecewiseConstantConf
void ConstPiecewiseConstantConf::BuildFromProto(const PbMessage& proto_piecewiseconstantconf) {
  data_ = ::std::make_shared<_PiecewiseConstantConf_>(dynamic_cast<const ::oneflow::PiecewiseConstantConf&>(proto_piecewiseconstantconf));
}

PiecewiseConstantConf::PiecewiseConstantConf(const ::std::shared_ptr<_PiecewiseConstantConf_>& data)
  : ConstPiecewiseConstantConf(data) {}
PiecewiseConstantConf::PiecewiseConstantConf(const PiecewiseConstantConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PiecewiseConstantConf> resize
PiecewiseConstantConf::PiecewiseConstantConf(PiecewiseConstantConf&&) noexcept = default; 
PiecewiseConstantConf::PiecewiseConstantConf(const ::oneflow::PiecewiseConstantConf& proto_piecewiseconstantconf) {
  InitFromProto(proto_piecewiseconstantconf);
}
PiecewiseConstantConf::PiecewiseConstantConf() = default;

PiecewiseConstantConf::~PiecewiseConstantConf() = default;

void PiecewiseConstantConf::InitFromProto(const PbMessage& proto_piecewiseconstantconf) {
  BuildFromProto(proto_piecewiseconstantconf);
}
  
void* PiecewiseConstantConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_boundaries();
    case 2: return mutable_values();
    default: return nullptr;
  }
}

bool PiecewiseConstantConf::operator==(const PiecewiseConstantConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PiecewiseConstantConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PiecewiseConstantConf::operator<(const PiecewiseConstantConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PiecewiseConstantConf::Clear() {
  if (data_) { data_.reset(); }
}
void PiecewiseConstantConf::CopyFrom(const PiecewiseConstantConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PiecewiseConstantConf& PiecewiseConstantConf::operator=(const PiecewiseConstantConf& other) {
  CopyFrom(other);
  return *this;
}

// repeated field boundaries
void PiecewiseConstantConf::clear_boundaries() {
  return __SharedPtr__()->clear_boundaries();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* PiecewiseConstantConf::mutable_boundaries() {
  return __SharedPtr__()->mutable_boundaries();
}
int64_t* PiecewiseConstantConf::mutable_boundaries(::std::size_t index) {
  return __SharedPtr__()->mutable_boundaries(index);
}
void PiecewiseConstantConf::add_boundaries(const int64_t& value) {
  return __SharedPtr__()->add_boundaries(value);
}
void PiecewiseConstantConf::set_boundaries(::std::size_t index, const int64_t& value) {
  return __SharedPtr__()->set_boundaries(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> PiecewiseConstantConf::shared_mutable_boundaries() {
  return mutable_boundaries()->__SharedMutable__();
}
// repeated field values
void PiecewiseConstantConf::clear_values() {
  return __SharedPtr__()->clear_values();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* PiecewiseConstantConf::mutable_values() {
  return __SharedPtr__()->mutable_values();
}
double* PiecewiseConstantConf::mutable_values(::std::size_t index) {
  return __SharedPtr__()->mutable_values(index);
}
void PiecewiseConstantConf::add_values(const double& value) {
  return __SharedPtr__()->add_values(value);
}
void PiecewiseConstantConf::set_values(::std::size_t index, const double& value) {
  return __SharedPtr__()->set_values(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> PiecewiseConstantConf::shared_mutable_values() {
  return mutable_values()->__SharedMutable__();
}

::std::shared_ptr<PiecewiseConstantConf> PiecewiseConstantConf::__SharedMutable__() {
  return ::std::make_shared<PiecewiseConstantConf>(__SharedPtr__());
}
ConstPolynomialDecayConf::_PolynomialDecayConf_::_PolynomialDecayConf_() { Clear(); }
ConstPolynomialDecayConf::_PolynomialDecayConf_::_PolynomialDecayConf_(const _PolynomialDecayConf_& other) { CopyFrom(other); }
ConstPolynomialDecayConf::_PolynomialDecayConf_::_PolynomialDecayConf_(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf) {
  InitFromProto(proto_polynomialdecayconf);
}
ConstPolynomialDecayConf::_PolynomialDecayConf_::_PolynomialDecayConf_(_PolynomialDecayConf_&& other) = default;
ConstPolynomialDecayConf::_PolynomialDecayConf_::~_PolynomialDecayConf_() = default;

void ConstPolynomialDecayConf::_PolynomialDecayConf_::InitFromProto(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_polynomialdecayconf.has_decay_batches()) {
    set_decay_batches(proto_polynomialdecayconf.decay_batches());
  }
  // required_or_optional field: end_learning_rate
  if (proto_polynomialdecayconf.has_end_learning_rate()) {
    set_end_learning_rate(proto_polynomialdecayconf.end_learning_rate());
  }
  // required_or_optional field: power
  if (proto_polynomialdecayconf.has_power()) {
    set_power(proto_polynomialdecayconf.power());
  }
  // required_or_optional field: cycle
  if (proto_polynomialdecayconf.has_cycle()) {
    set_cycle(proto_polynomialdecayconf.cycle());
  }
    
}

void ConstPolynomialDecayConf::_PolynomialDecayConf_::ToProto(::oneflow::PolynomialDecayConf* proto_polynomialdecayconf) const {
  proto_polynomialdecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_polynomialdecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: end_learning_rate
  if (this->has_end_learning_rate()) {
    proto_polynomialdecayconf->set_end_learning_rate(end_learning_rate());
    }
  // required_or_optional field: power
  if (this->has_power()) {
    proto_polynomialdecayconf->set_power(power());
    }
  // required_or_optional field: cycle
  if (this->has_cycle()) {
    proto_polynomialdecayconf->set_cycle(cycle());
    }

}

::std::string ConstPolynomialDecayConf::_PolynomialDecayConf_::DebugString() const {
  ::oneflow::PolynomialDecayConf proto_polynomialdecayconf;
  this->ToProto(&proto_polynomialdecayconf);
  return proto_polynomialdecayconf.DebugString();
}

void ConstPolynomialDecayConf::_PolynomialDecayConf_::Clear() {
  clear_decay_batches();
  clear_end_learning_rate();
  clear_power();
  clear_cycle();
}

void ConstPolynomialDecayConf::_PolynomialDecayConf_::CopyFrom(const _PolynomialDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_end_learning_rate()) {
    set_end_learning_rate(other.end_learning_rate());
  } else {
    clear_end_learning_rate();
  }
  if (other.has_power()) {
    set_power(other.power());
  } else {
    clear_power();
  }
  if (other.has_cycle()) {
    set_cycle(other.cycle());
  } else {
    clear_cycle();
  }
}


// optional field decay_batches
bool ConstPolynomialDecayConf::_PolynomialDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstPolynomialDecayConf::_PolynomialDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstPolynomialDecayConf::_PolynomialDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field end_learning_rate
bool ConstPolynomialDecayConf::_PolynomialDecayConf_::has_end_learning_rate() const {
  return has_end_learning_rate_;
}
const double& ConstPolynomialDecayConf::_PolynomialDecayConf_::end_learning_rate() const {
  if (has_end_learning_rate_) { return end_learning_rate_; }
  static const double default_static_value =
    double(0.0001);
  return default_static_value;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::clear_end_learning_rate() {
  has_end_learning_rate_ = false;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::set_end_learning_rate(const double& value) {
  end_learning_rate_ = value;
  has_end_learning_rate_ = true;
}
double* ConstPolynomialDecayConf::_PolynomialDecayConf_::mutable_end_learning_rate() {
  has_end_learning_rate_ = true;
  return &end_learning_rate_;
}

// optional field power
bool ConstPolynomialDecayConf::_PolynomialDecayConf_::has_power() const {
  return has_power_;
}
const double& ConstPolynomialDecayConf::_PolynomialDecayConf_::power() const {
  if (has_power_) { return power_; }
  static const double default_static_value =
    double(1.0);
  return default_static_value;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::clear_power() {
  has_power_ = false;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::set_power(const double& value) {
  power_ = value;
  has_power_ = true;
}
double* ConstPolynomialDecayConf::_PolynomialDecayConf_::mutable_power() {
  has_power_ = true;
  return &power_;
}

// optional field cycle
bool ConstPolynomialDecayConf::_PolynomialDecayConf_::has_cycle() const {
  return has_cycle_;
}
const bool& ConstPolynomialDecayConf::_PolynomialDecayConf_::cycle() const {
  if (has_cycle_) { return cycle_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::clear_cycle() {
  has_cycle_ = false;
}
void ConstPolynomialDecayConf::_PolynomialDecayConf_::set_cycle(const bool& value) {
  cycle_ = value;
  has_cycle_ = true;
}
bool* ConstPolynomialDecayConf::_PolynomialDecayConf_::mutable_cycle() {
  has_cycle_ = true;
  return &cycle_;
}


int ConstPolynomialDecayConf::_PolynomialDecayConf_::compare(const _PolynomialDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_end_learning_rate() == other.has_end_learning_rate())) {
    return has_end_learning_rate() < other.has_end_learning_rate() ? -1 : 1;
  } else if (!(end_learning_rate() == other.end_learning_rate())) {
    return end_learning_rate() < other.end_learning_rate() ? -1 : 1;
  }
  if (!(has_power() == other.has_power())) {
    return has_power() < other.has_power() ? -1 : 1;
  } else if (!(power() == other.power())) {
    return power() < other.power() ? -1 : 1;
  }
  if (!(has_cycle() == other.has_cycle())) {
    return has_cycle() < other.has_cycle() ? -1 : 1;
  } else if (!(cycle() == other.cycle())) {
    return cycle() < other.cycle() ? -1 : 1;
  }
  return 0;
}

bool ConstPolynomialDecayConf::_PolynomialDecayConf_::operator==(const _PolynomialDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_end_learning_rate() == other.has_end_learning_rate() 
    && end_learning_rate() == other.end_learning_rate()
    && has_power() == other.has_power() 
    && power() == other.power()
    && has_cycle() == other.has_cycle() 
    && cycle() == other.cycle()
  ;
}

std::size_t ConstPolynomialDecayConf::_PolynomialDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_end_learning_rate() ? std::hash<double>()(end_learning_rate()) : 0)
    ^ (has_power() ? std::hash<double>()(power()) : 0)
    ^ (has_cycle() ? std::hash<bool>()(cycle()) : 0)
  ;
}

bool ConstPolynomialDecayConf::_PolynomialDecayConf_::operator<(const _PolynomialDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_end_learning_rate() == other.has_end_learning_rate()) ? 
      has_end_learning_rate() < other.has_end_learning_rate() : false
    || !(end_learning_rate() == other.end_learning_rate()) ? 
      end_learning_rate() < other.end_learning_rate() : false
    || !(has_power() == other.has_power()) ? 
      has_power() < other.has_power() : false
    || !(power() == other.power()) ? 
      power() < other.power() : false
    || !(has_cycle() == other.has_cycle()) ? 
      has_cycle() < other.has_cycle() : false
    || !(cycle() == other.cycle()) ? 
      cycle() < other.cycle() : false
  ;
}

using _PolynomialDecayConf_ =  ConstPolynomialDecayConf::_PolynomialDecayConf_;
ConstPolynomialDecayConf::ConstPolynomialDecayConf(const ::std::shared_ptr<_PolynomialDecayConf_>& data): data_(data) {}
ConstPolynomialDecayConf::ConstPolynomialDecayConf(): data_(::std::make_shared<_PolynomialDecayConf_>()) {}
ConstPolynomialDecayConf::ConstPolynomialDecayConf(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf) {
  BuildFromProto(proto_polynomialdecayconf);
}
ConstPolynomialDecayConf::ConstPolynomialDecayConf(const ConstPolynomialDecayConf&) = default;
ConstPolynomialDecayConf::ConstPolynomialDecayConf(ConstPolynomialDecayConf&&) noexcept = default;
ConstPolynomialDecayConf::~ConstPolynomialDecayConf() = default;

void ConstPolynomialDecayConf::ToProto(PbMessage* proto_polynomialdecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PolynomialDecayConf*>(proto_polynomialdecayconf));
}
  
::std::string ConstPolynomialDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPolynomialDecayConf::__Empty__() const {
  return !data_;
}

int ConstPolynomialDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"end_learning_rate", 2},
    {"power", 3},
    {"cycle", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPolynomialDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPolynomialDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPolynomialDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &end_learning_rate();
    case 3: return &power();
    case 4: return &cycle();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstPolynomialDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstPolynomialDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field end_learning_rate
bool ConstPolynomialDecayConf::has_end_learning_rate() const {
  return __SharedPtrOrDefault__()->has_end_learning_rate();
}
const double& ConstPolynomialDecayConf::end_learning_rate() const {
  return __SharedPtrOrDefault__()->end_learning_rate();
}
// used by pybind11 only
// required or optional field power
bool ConstPolynomialDecayConf::has_power() const {
  return __SharedPtrOrDefault__()->has_power();
}
const double& ConstPolynomialDecayConf::power() const {
  return __SharedPtrOrDefault__()->power();
}
// used by pybind11 only
// required or optional field cycle
bool ConstPolynomialDecayConf::has_cycle() const {
  return __SharedPtrOrDefault__()->has_cycle();
}
const bool& ConstPolynomialDecayConf::cycle() const {
  return __SharedPtrOrDefault__()->cycle();
}
// used by pybind11 only

::std::shared_ptr<ConstPolynomialDecayConf> ConstPolynomialDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstPolynomialDecayConf>(data_);
}
int64_t ConstPolynomialDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPolynomialDecayConf::operator==(const ConstPolynomialDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPolynomialDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPolynomialDecayConf::operator<(const ConstPolynomialDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PolynomialDecayConf_>& ConstPolynomialDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PolynomialDecayConf_> default_ptr = std::make_shared<_PolynomialDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_PolynomialDecayConf_>& ConstPolynomialDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _PolynomialDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPolynomialDecayConf
void ConstPolynomialDecayConf::BuildFromProto(const PbMessage& proto_polynomialdecayconf) {
  data_ = ::std::make_shared<_PolynomialDecayConf_>(dynamic_cast<const ::oneflow::PolynomialDecayConf&>(proto_polynomialdecayconf));
}

PolynomialDecayConf::PolynomialDecayConf(const ::std::shared_ptr<_PolynomialDecayConf_>& data)
  : ConstPolynomialDecayConf(data) {}
PolynomialDecayConf::PolynomialDecayConf(const PolynomialDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PolynomialDecayConf> resize
PolynomialDecayConf::PolynomialDecayConf(PolynomialDecayConf&&) noexcept = default; 
PolynomialDecayConf::PolynomialDecayConf(const ::oneflow::PolynomialDecayConf& proto_polynomialdecayconf) {
  InitFromProto(proto_polynomialdecayconf);
}
PolynomialDecayConf::PolynomialDecayConf() = default;

PolynomialDecayConf::~PolynomialDecayConf() = default;

void PolynomialDecayConf::InitFromProto(const PbMessage& proto_polynomialdecayconf) {
  BuildFromProto(proto_polynomialdecayconf);
}
  
void* PolynomialDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_end_learning_rate();
    case 3: return mutable_power();
    case 4: return mutable_cycle();
    default: return nullptr;
  }
}

bool PolynomialDecayConf::operator==(const PolynomialDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PolynomialDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PolynomialDecayConf::operator<(const PolynomialDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PolynomialDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void PolynomialDecayConf::CopyFrom(const PolynomialDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PolynomialDecayConf& PolynomialDecayConf::operator=(const PolynomialDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void PolynomialDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void PolynomialDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* PolynomialDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field end_learning_rate
void PolynomialDecayConf::clear_end_learning_rate() {
  return __SharedPtr__()->clear_end_learning_rate();
}
void PolynomialDecayConf::set_end_learning_rate(const double& value) {
  return __SharedPtr__()->set_end_learning_rate(value);
}
double* PolynomialDecayConf::mutable_end_learning_rate() {
  return  __SharedPtr__()->mutable_end_learning_rate();
}
// required or optional field power
void PolynomialDecayConf::clear_power() {
  return __SharedPtr__()->clear_power();
}
void PolynomialDecayConf::set_power(const double& value) {
  return __SharedPtr__()->set_power(value);
}
double* PolynomialDecayConf::mutable_power() {
  return  __SharedPtr__()->mutable_power();
}
// required or optional field cycle
void PolynomialDecayConf::clear_cycle() {
  return __SharedPtr__()->clear_cycle();
}
void PolynomialDecayConf::set_cycle(const bool& value) {
  return __SharedPtr__()->set_cycle(value);
}
bool* PolynomialDecayConf::mutable_cycle() {
  return  __SharedPtr__()->mutable_cycle();
}

::std::shared_ptr<PolynomialDecayConf> PolynomialDecayConf::__SharedMutable__() {
  return ::std::make_shared<PolynomialDecayConf>(__SharedPtr__());
}
ConstCosineDecayConf::_CosineDecayConf_::_CosineDecayConf_() { Clear(); }
ConstCosineDecayConf::_CosineDecayConf_::_CosineDecayConf_(const _CosineDecayConf_& other) { CopyFrom(other); }
ConstCosineDecayConf::_CosineDecayConf_::_CosineDecayConf_(const ::oneflow::CosineDecayConf& proto_cosinedecayconf) {
  InitFromProto(proto_cosinedecayconf);
}
ConstCosineDecayConf::_CosineDecayConf_::_CosineDecayConf_(_CosineDecayConf_&& other) = default;
ConstCosineDecayConf::_CosineDecayConf_::~_CosineDecayConf_() = default;

void ConstCosineDecayConf::_CosineDecayConf_::InitFromProto(const ::oneflow::CosineDecayConf& proto_cosinedecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_cosinedecayconf.has_decay_batches()) {
    set_decay_batches(proto_cosinedecayconf.decay_batches());
  }
  // required_or_optional field: alpha
  if (proto_cosinedecayconf.has_alpha()) {
    set_alpha(proto_cosinedecayconf.alpha());
  }
    
}

void ConstCosineDecayConf::_CosineDecayConf_::ToProto(::oneflow::CosineDecayConf* proto_cosinedecayconf) const {
  proto_cosinedecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_cosinedecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: alpha
  if (this->has_alpha()) {
    proto_cosinedecayconf->set_alpha(alpha());
    }

}

::std::string ConstCosineDecayConf::_CosineDecayConf_::DebugString() const {
  ::oneflow::CosineDecayConf proto_cosinedecayconf;
  this->ToProto(&proto_cosinedecayconf);
  return proto_cosinedecayconf.DebugString();
}

void ConstCosineDecayConf::_CosineDecayConf_::Clear() {
  clear_decay_batches();
  clear_alpha();
}

void ConstCosineDecayConf::_CosineDecayConf_::CopyFrom(const _CosineDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_alpha()) {
    set_alpha(other.alpha());
  } else {
    clear_alpha();
  }
}


// optional field decay_batches
bool ConstCosineDecayConf::_CosineDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstCosineDecayConf::_CosineDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstCosineDecayConf::_CosineDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstCosineDecayConf::_CosineDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstCosineDecayConf::_CosineDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field alpha
bool ConstCosineDecayConf::_CosineDecayConf_::has_alpha() const {
  return has_alpha_;
}
const double& ConstCosineDecayConf::_CosineDecayConf_::alpha() const {
  if (has_alpha_) { return alpha_; }
  static const double default_static_value =
    double(0.0);
  return default_static_value;
}
void ConstCosineDecayConf::_CosineDecayConf_::clear_alpha() {
  has_alpha_ = false;
}
void ConstCosineDecayConf::_CosineDecayConf_::set_alpha(const double& value) {
  alpha_ = value;
  has_alpha_ = true;
}
double* ConstCosineDecayConf::_CosineDecayConf_::mutable_alpha() {
  has_alpha_ = true;
  return &alpha_;
}


int ConstCosineDecayConf::_CosineDecayConf_::compare(const _CosineDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_alpha() == other.has_alpha())) {
    return has_alpha() < other.has_alpha() ? -1 : 1;
  } else if (!(alpha() == other.alpha())) {
    return alpha() < other.alpha() ? -1 : 1;
  }
  return 0;
}

bool ConstCosineDecayConf::_CosineDecayConf_::operator==(const _CosineDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_alpha() == other.has_alpha() 
    && alpha() == other.alpha()
  ;
}

std::size_t ConstCosineDecayConf::_CosineDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_alpha() ? std::hash<double>()(alpha()) : 0)
  ;
}

bool ConstCosineDecayConf::_CosineDecayConf_::operator<(const _CosineDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_alpha() == other.has_alpha()) ? 
      has_alpha() < other.has_alpha() : false
    || !(alpha() == other.alpha()) ? 
      alpha() < other.alpha() : false
  ;
}

using _CosineDecayConf_ =  ConstCosineDecayConf::_CosineDecayConf_;
ConstCosineDecayConf::ConstCosineDecayConf(const ::std::shared_ptr<_CosineDecayConf_>& data): data_(data) {}
ConstCosineDecayConf::ConstCosineDecayConf(): data_(::std::make_shared<_CosineDecayConf_>()) {}
ConstCosineDecayConf::ConstCosineDecayConf(const ::oneflow::CosineDecayConf& proto_cosinedecayconf) {
  BuildFromProto(proto_cosinedecayconf);
}
ConstCosineDecayConf::ConstCosineDecayConf(const ConstCosineDecayConf&) = default;
ConstCosineDecayConf::ConstCosineDecayConf(ConstCosineDecayConf&&) noexcept = default;
ConstCosineDecayConf::~ConstCosineDecayConf() = default;

void ConstCosineDecayConf::ToProto(PbMessage* proto_cosinedecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::CosineDecayConf*>(proto_cosinedecayconf));
}
  
::std::string ConstCosineDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCosineDecayConf::__Empty__() const {
  return !data_;
}

int ConstCosineDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"alpha", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstCosineDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstCosineDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCosineDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &alpha();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstCosineDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstCosineDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field alpha
bool ConstCosineDecayConf::has_alpha() const {
  return __SharedPtrOrDefault__()->has_alpha();
}
const double& ConstCosineDecayConf::alpha() const {
  return __SharedPtrOrDefault__()->alpha();
}
// used by pybind11 only

::std::shared_ptr<ConstCosineDecayConf> ConstCosineDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstCosineDecayConf>(data_);
}
int64_t ConstCosineDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCosineDecayConf::operator==(const ConstCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCosineDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCosineDecayConf::operator<(const ConstCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CosineDecayConf_>& ConstCosineDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CosineDecayConf_> default_ptr = std::make_shared<_CosineDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_CosineDecayConf_>& ConstCosineDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _CosineDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCosineDecayConf
void ConstCosineDecayConf::BuildFromProto(const PbMessage& proto_cosinedecayconf) {
  data_ = ::std::make_shared<_CosineDecayConf_>(dynamic_cast<const ::oneflow::CosineDecayConf&>(proto_cosinedecayconf));
}

CosineDecayConf::CosineDecayConf(const ::std::shared_ptr<_CosineDecayConf_>& data)
  : ConstCosineDecayConf(data) {}
CosineDecayConf::CosineDecayConf(const CosineDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CosineDecayConf> resize
CosineDecayConf::CosineDecayConf(CosineDecayConf&&) noexcept = default; 
CosineDecayConf::CosineDecayConf(const ::oneflow::CosineDecayConf& proto_cosinedecayconf) {
  InitFromProto(proto_cosinedecayconf);
}
CosineDecayConf::CosineDecayConf() = default;

CosineDecayConf::~CosineDecayConf() = default;

void CosineDecayConf::InitFromProto(const PbMessage& proto_cosinedecayconf) {
  BuildFromProto(proto_cosinedecayconf);
}
  
void* CosineDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_alpha();
    default: return nullptr;
  }
}

bool CosineDecayConf::operator==(const CosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CosineDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CosineDecayConf::operator<(const CosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CosineDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void CosineDecayConf::CopyFrom(const CosineDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CosineDecayConf& CosineDecayConf::operator=(const CosineDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void CosineDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void CosineDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* CosineDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field alpha
void CosineDecayConf::clear_alpha() {
  return __SharedPtr__()->clear_alpha();
}
void CosineDecayConf::set_alpha(const double& value) {
  return __SharedPtr__()->set_alpha(value);
}
double* CosineDecayConf::mutable_alpha() {
  return  __SharedPtr__()->mutable_alpha();
}

::std::shared_ptr<CosineDecayConf> CosineDecayConf::__SharedMutable__() {
  return ::std::make_shared<CosineDecayConf>(__SharedPtr__());
}
ConstLinearCosineDecayConf::_LinearCosineDecayConf_::_LinearCosineDecayConf_() { Clear(); }
ConstLinearCosineDecayConf::_LinearCosineDecayConf_::_LinearCosineDecayConf_(const _LinearCosineDecayConf_& other) { CopyFrom(other); }
ConstLinearCosineDecayConf::_LinearCosineDecayConf_::_LinearCosineDecayConf_(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf) {
  InitFromProto(proto_linearcosinedecayconf);
}
ConstLinearCosineDecayConf::_LinearCosineDecayConf_::_LinearCosineDecayConf_(_LinearCosineDecayConf_&& other) = default;
ConstLinearCosineDecayConf::_LinearCosineDecayConf_::~_LinearCosineDecayConf_() = default;

void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::InitFromProto(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf) {
  Clear();
  // required_or_optional field: decay_batches
  if (proto_linearcosinedecayconf.has_decay_batches()) {
    set_decay_batches(proto_linearcosinedecayconf.decay_batches());
  }
  // required_or_optional field: num_periods
  if (proto_linearcosinedecayconf.has_num_periods()) {
    set_num_periods(proto_linearcosinedecayconf.num_periods());
  }
  // required_or_optional field: alpha
  if (proto_linearcosinedecayconf.has_alpha()) {
    set_alpha(proto_linearcosinedecayconf.alpha());
  }
  // required_or_optional field: beta
  if (proto_linearcosinedecayconf.has_beta()) {
    set_beta(proto_linearcosinedecayconf.beta());
  }
    
}

void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::ToProto(::oneflow::LinearCosineDecayConf* proto_linearcosinedecayconf) const {
  proto_linearcosinedecayconf->Clear();
  // required_or_optional field: decay_batches
  if (this->has_decay_batches()) {
    proto_linearcosinedecayconf->set_decay_batches(decay_batches());
    }
  // required_or_optional field: num_periods
  if (this->has_num_periods()) {
    proto_linearcosinedecayconf->set_num_periods(num_periods());
    }
  // required_or_optional field: alpha
  if (this->has_alpha()) {
    proto_linearcosinedecayconf->set_alpha(alpha());
    }
  // required_or_optional field: beta
  if (this->has_beta()) {
    proto_linearcosinedecayconf->set_beta(beta());
    }

}

::std::string ConstLinearCosineDecayConf::_LinearCosineDecayConf_::DebugString() const {
  ::oneflow::LinearCosineDecayConf proto_linearcosinedecayconf;
  this->ToProto(&proto_linearcosinedecayconf);
  return proto_linearcosinedecayconf.DebugString();
}

void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::Clear() {
  clear_decay_batches();
  clear_num_periods();
  clear_alpha();
  clear_beta();
}

void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::CopyFrom(const _LinearCosineDecayConf_& other) {
  if (other.has_decay_batches()) {
    set_decay_batches(other.decay_batches());
  } else {
    clear_decay_batches();
  }
  if (other.has_num_periods()) {
    set_num_periods(other.num_periods());
  } else {
    clear_num_periods();
  }
  if (other.has_alpha()) {
    set_alpha(other.alpha());
  } else {
    clear_alpha();
  }
  if (other.has_beta()) {
    set_beta(other.beta());
  } else {
    clear_beta();
  }
}


// optional field decay_batches
bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::has_decay_batches() const {
  return has_decay_batches_;
}
const int64_t& ConstLinearCosineDecayConf::_LinearCosineDecayConf_::decay_batches() const {
  if (has_decay_batches_) { return decay_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::clear_decay_batches() {
  has_decay_batches_ = false;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::set_decay_batches(const int64_t& value) {
  decay_batches_ = value;
  has_decay_batches_ = true;
}
int64_t* ConstLinearCosineDecayConf::_LinearCosineDecayConf_::mutable_decay_batches() {
  has_decay_batches_ = true;
  return &decay_batches_;
}

// optional field num_periods
bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::has_num_periods() const {
  return has_num_periods_;
}
const double& ConstLinearCosineDecayConf::_LinearCosineDecayConf_::num_periods() const {
  if (has_num_periods_) { return num_periods_; }
  static const double default_static_value =
    double(0.5);
  return default_static_value;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::clear_num_periods() {
  has_num_periods_ = false;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::set_num_periods(const double& value) {
  num_periods_ = value;
  has_num_periods_ = true;
}
double* ConstLinearCosineDecayConf::_LinearCosineDecayConf_::mutable_num_periods() {
  has_num_periods_ = true;
  return &num_periods_;
}

// optional field alpha
bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::has_alpha() const {
  return has_alpha_;
}
const double& ConstLinearCosineDecayConf::_LinearCosineDecayConf_::alpha() const {
  if (has_alpha_) { return alpha_; }
  static const double default_static_value =
    double(0.0);
  return default_static_value;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::clear_alpha() {
  has_alpha_ = false;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::set_alpha(const double& value) {
  alpha_ = value;
  has_alpha_ = true;
}
double* ConstLinearCosineDecayConf::_LinearCosineDecayConf_::mutable_alpha() {
  has_alpha_ = true;
  return &alpha_;
}

// optional field beta
bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::has_beta() const {
  return has_beta_;
}
const double& ConstLinearCosineDecayConf::_LinearCosineDecayConf_::beta() const {
  if (has_beta_) { return beta_; }
  static const double default_static_value =
    double(0.001);
  return default_static_value;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::clear_beta() {
  has_beta_ = false;
}
void ConstLinearCosineDecayConf::_LinearCosineDecayConf_::set_beta(const double& value) {
  beta_ = value;
  has_beta_ = true;
}
double* ConstLinearCosineDecayConf::_LinearCosineDecayConf_::mutable_beta() {
  has_beta_ = true;
  return &beta_;
}


int ConstLinearCosineDecayConf::_LinearCosineDecayConf_::compare(const _LinearCosineDecayConf_& other) {
  if (!(has_decay_batches() == other.has_decay_batches())) {
    return has_decay_batches() < other.has_decay_batches() ? -1 : 1;
  } else if (!(decay_batches() == other.decay_batches())) {
    return decay_batches() < other.decay_batches() ? -1 : 1;
  }
  if (!(has_num_periods() == other.has_num_periods())) {
    return has_num_periods() < other.has_num_periods() ? -1 : 1;
  } else if (!(num_periods() == other.num_periods())) {
    return num_periods() < other.num_periods() ? -1 : 1;
  }
  if (!(has_alpha() == other.has_alpha())) {
    return has_alpha() < other.has_alpha() ? -1 : 1;
  } else if (!(alpha() == other.alpha())) {
    return alpha() < other.alpha() ? -1 : 1;
  }
  if (!(has_beta() == other.has_beta())) {
    return has_beta() < other.has_beta() ? -1 : 1;
  } else if (!(beta() == other.beta())) {
    return beta() < other.beta() ? -1 : 1;
  }
  return 0;
}

bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::operator==(const _LinearCosineDecayConf_& other) const {
  return true
    && has_decay_batches() == other.has_decay_batches() 
    && decay_batches() == other.decay_batches()
    && has_num_periods() == other.has_num_periods() 
    && num_periods() == other.num_periods()
    && has_alpha() == other.has_alpha() 
    && alpha() == other.alpha()
    && has_beta() == other.has_beta() 
    && beta() == other.beta()
  ;
}

std::size_t ConstLinearCosineDecayConf::_LinearCosineDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_batches() ? std::hash<int64_t>()(decay_batches()) : 0)
    ^ (has_num_periods() ? std::hash<double>()(num_periods()) : 0)
    ^ (has_alpha() ? std::hash<double>()(alpha()) : 0)
    ^ (has_beta() ? std::hash<double>()(beta()) : 0)
  ;
}

bool ConstLinearCosineDecayConf::_LinearCosineDecayConf_::operator<(const _LinearCosineDecayConf_& other) const {
  return false
    || !(has_decay_batches() == other.has_decay_batches()) ? 
      has_decay_batches() < other.has_decay_batches() : false
    || !(decay_batches() == other.decay_batches()) ? 
      decay_batches() < other.decay_batches() : false
    || !(has_num_periods() == other.has_num_periods()) ? 
      has_num_periods() < other.has_num_periods() : false
    || !(num_periods() == other.num_periods()) ? 
      num_periods() < other.num_periods() : false
    || !(has_alpha() == other.has_alpha()) ? 
      has_alpha() < other.has_alpha() : false
    || !(alpha() == other.alpha()) ? 
      alpha() < other.alpha() : false
    || !(has_beta() == other.has_beta()) ? 
      has_beta() < other.has_beta() : false
    || !(beta() == other.beta()) ? 
      beta() < other.beta() : false
  ;
}

using _LinearCosineDecayConf_ =  ConstLinearCosineDecayConf::_LinearCosineDecayConf_;
ConstLinearCosineDecayConf::ConstLinearCosineDecayConf(const ::std::shared_ptr<_LinearCosineDecayConf_>& data): data_(data) {}
ConstLinearCosineDecayConf::ConstLinearCosineDecayConf(): data_(::std::make_shared<_LinearCosineDecayConf_>()) {}
ConstLinearCosineDecayConf::ConstLinearCosineDecayConf(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf) {
  BuildFromProto(proto_linearcosinedecayconf);
}
ConstLinearCosineDecayConf::ConstLinearCosineDecayConf(const ConstLinearCosineDecayConf&) = default;
ConstLinearCosineDecayConf::ConstLinearCosineDecayConf(ConstLinearCosineDecayConf&&) noexcept = default;
ConstLinearCosineDecayConf::~ConstLinearCosineDecayConf() = default;

void ConstLinearCosineDecayConf::ToProto(PbMessage* proto_linearcosinedecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LinearCosineDecayConf*>(proto_linearcosinedecayconf));
}
  
::std::string ConstLinearCosineDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLinearCosineDecayConf::__Empty__() const {
  return !data_;
}

int ConstLinearCosineDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_batches", 1},
    {"num_periods", 2},
    {"alpha", 3},
    {"beta", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLinearCosineDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLinearCosineDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLinearCosineDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_batches();
    case 2: return &num_periods();
    case 3: return &alpha();
    case 4: return &beta();
    default: return nullptr;
  }
}

// required or optional field decay_batches
bool ConstLinearCosineDecayConf::has_decay_batches() const {
  return __SharedPtrOrDefault__()->has_decay_batches();
}
const int64_t& ConstLinearCosineDecayConf::decay_batches() const {
  return __SharedPtrOrDefault__()->decay_batches();
}
// used by pybind11 only
// required or optional field num_periods
bool ConstLinearCosineDecayConf::has_num_periods() const {
  return __SharedPtrOrDefault__()->has_num_periods();
}
const double& ConstLinearCosineDecayConf::num_periods() const {
  return __SharedPtrOrDefault__()->num_periods();
}
// used by pybind11 only
// required or optional field alpha
bool ConstLinearCosineDecayConf::has_alpha() const {
  return __SharedPtrOrDefault__()->has_alpha();
}
const double& ConstLinearCosineDecayConf::alpha() const {
  return __SharedPtrOrDefault__()->alpha();
}
// used by pybind11 only
// required or optional field beta
bool ConstLinearCosineDecayConf::has_beta() const {
  return __SharedPtrOrDefault__()->has_beta();
}
const double& ConstLinearCosineDecayConf::beta() const {
  return __SharedPtrOrDefault__()->beta();
}
// used by pybind11 only

::std::shared_ptr<ConstLinearCosineDecayConf> ConstLinearCosineDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstLinearCosineDecayConf>(data_);
}
int64_t ConstLinearCosineDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLinearCosineDecayConf::operator==(const ConstLinearCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLinearCosineDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLinearCosineDecayConf::operator<(const ConstLinearCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LinearCosineDecayConf_>& ConstLinearCosineDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LinearCosineDecayConf_> default_ptr = std::make_shared<_LinearCosineDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LinearCosineDecayConf_>& ConstLinearCosineDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LinearCosineDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLinearCosineDecayConf
void ConstLinearCosineDecayConf::BuildFromProto(const PbMessage& proto_linearcosinedecayconf) {
  data_ = ::std::make_shared<_LinearCosineDecayConf_>(dynamic_cast<const ::oneflow::LinearCosineDecayConf&>(proto_linearcosinedecayconf));
}

LinearCosineDecayConf::LinearCosineDecayConf(const ::std::shared_ptr<_LinearCosineDecayConf_>& data)
  : ConstLinearCosineDecayConf(data) {}
LinearCosineDecayConf::LinearCosineDecayConf(const LinearCosineDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LinearCosineDecayConf> resize
LinearCosineDecayConf::LinearCosineDecayConf(LinearCosineDecayConf&&) noexcept = default; 
LinearCosineDecayConf::LinearCosineDecayConf(const ::oneflow::LinearCosineDecayConf& proto_linearcosinedecayconf) {
  InitFromProto(proto_linearcosinedecayconf);
}
LinearCosineDecayConf::LinearCosineDecayConf() = default;

LinearCosineDecayConf::~LinearCosineDecayConf() = default;

void LinearCosineDecayConf::InitFromProto(const PbMessage& proto_linearcosinedecayconf) {
  BuildFromProto(proto_linearcosinedecayconf);
}
  
void* LinearCosineDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_batches();
    case 2: return mutable_num_periods();
    case 3: return mutable_alpha();
    case 4: return mutable_beta();
    default: return nullptr;
  }
}

bool LinearCosineDecayConf::operator==(const LinearCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LinearCosineDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LinearCosineDecayConf::operator<(const LinearCosineDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LinearCosineDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void LinearCosineDecayConf::CopyFrom(const LinearCosineDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LinearCosineDecayConf& LinearCosineDecayConf::operator=(const LinearCosineDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_batches
void LinearCosineDecayConf::clear_decay_batches() {
  return __SharedPtr__()->clear_decay_batches();
}
void LinearCosineDecayConf::set_decay_batches(const int64_t& value) {
  return __SharedPtr__()->set_decay_batches(value);
}
int64_t* LinearCosineDecayConf::mutable_decay_batches() {
  return  __SharedPtr__()->mutable_decay_batches();
}
// required or optional field num_periods
void LinearCosineDecayConf::clear_num_periods() {
  return __SharedPtr__()->clear_num_periods();
}
void LinearCosineDecayConf::set_num_periods(const double& value) {
  return __SharedPtr__()->set_num_periods(value);
}
double* LinearCosineDecayConf::mutable_num_periods() {
  return  __SharedPtr__()->mutable_num_periods();
}
// required or optional field alpha
void LinearCosineDecayConf::clear_alpha() {
  return __SharedPtr__()->clear_alpha();
}
void LinearCosineDecayConf::set_alpha(const double& value) {
  return __SharedPtr__()->set_alpha(value);
}
double* LinearCosineDecayConf::mutable_alpha() {
  return  __SharedPtr__()->mutable_alpha();
}
// required or optional field beta
void LinearCosineDecayConf::clear_beta() {
  return __SharedPtr__()->clear_beta();
}
void LinearCosineDecayConf::set_beta(const double& value) {
  return __SharedPtr__()->set_beta(value);
}
double* LinearCosineDecayConf::mutable_beta() {
  return  __SharedPtr__()->mutable_beta();
}

::std::shared_ptr<LinearCosineDecayConf> LinearCosineDecayConf::__SharedMutable__() {
  return ::std::make_shared<LinearCosineDecayConf>(__SharedPtr__());
}
ConstPiecewiseScalingConf::_PiecewiseScalingConf_::_PiecewiseScalingConf_() { Clear(); }
ConstPiecewiseScalingConf::_PiecewiseScalingConf_::_PiecewiseScalingConf_(const _PiecewiseScalingConf_& other) { CopyFrom(other); }
ConstPiecewiseScalingConf::_PiecewiseScalingConf_::_PiecewiseScalingConf_(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf) {
  InitFromProto(proto_piecewisescalingconf);
}
ConstPiecewiseScalingConf::_PiecewiseScalingConf_::_PiecewiseScalingConf_(_PiecewiseScalingConf_&& other) = default;
ConstPiecewiseScalingConf::_PiecewiseScalingConf_::~_PiecewiseScalingConf_() = default;

void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::InitFromProto(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf) {
  Clear();
  // repeated field: boundaries
  if (!proto_piecewisescalingconf.boundaries().empty()) {
    for (const int64_t& elem : proto_piecewisescalingconf.boundaries()) {
      add_boundaries(elem);
    }
  }
  // repeated field: scales
  if (!proto_piecewisescalingconf.scales().empty()) {
    for (const double& elem : proto_piecewisescalingconf.scales()) {
      add_scales(elem);
    }
  }
    
}

void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::ToProto(::oneflow::PiecewiseScalingConf* proto_piecewisescalingconf) const {
  proto_piecewisescalingconf->Clear();
  // repeated field: boundaries
  if (!boundaries().empty()) {
    for (const int64_t& elem : boundaries()) {
      proto_piecewisescalingconf->add_boundaries(elem);
    }
  }
  // repeated field: scales
  if (!scales().empty()) {
    for (const double& elem : scales()) {
      proto_piecewisescalingconf->add_scales(elem);
    }
  }

}

::std::string ConstPiecewiseScalingConf::_PiecewiseScalingConf_::DebugString() const {
  ::oneflow::PiecewiseScalingConf proto_piecewisescalingconf;
  this->ToProto(&proto_piecewisescalingconf);
  return proto_piecewisescalingconf.DebugString();
}

void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::Clear() {
  clear_boundaries();
  clear_scales();
}

void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::CopyFrom(const _PiecewiseScalingConf_& other) {
  mutable_boundaries()->CopyFrom(other.boundaries());
  mutable_scales()->CopyFrom(other.scales());
}


// repeated field boundaries
::std::size_t ConstPiecewiseScalingConf::_PiecewiseScalingConf_::boundaries_size() const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->size();
  }
  return boundaries_->size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& ConstPiecewiseScalingConf::_PiecewiseScalingConf_::boundaries() const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return *(default_static_value.get());
  }
  return *(boundaries_.get());
}
const int64_t& ConstPiecewiseScalingConf::_PiecewiseScalingConf_::boundaries(::std::size_t index) const {
  if (!boundaries_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->Get(index);
  }
  return boundaries_->Get(index);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::clear_boundaries() {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* ConstPiecewiseScalingConf::_PiecewiseScalingConf_::mutable_boundaries() {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return  boundaries_.get();
}
int64_t* ConstPiecewiseScalingConf::_PiecewiseScalingConf_::mutable_boundaries(::std::size_t index) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return  boundaries_->Mutable(index);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::add_boundaries(const int64_t& value) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Add(value);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::set_boundaries(::std::size_t index, const int64_t& value) {
  if (!boundaries_) {
    boundaries_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>();
  }
  return boundaries_->Set(index, value);
}

// repeated field scales
::std::size_t ConstPiecewiseScalingConf::_PiecewiseScalingConf_::scales_size() const {
  if (!scales_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return default_static_value->size();
  }
  return scales_->size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& ConstPiecewiseScalingConf::_PiecewiseScalingConf_::scales() const {
  if (!scales_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return *(default_static_value.get());
  }
  return *(scales_.get());
}
const double& ConstPiecewiseScalingConf::_PiecewiseScalingConf_::scales(::std::size_t index) const {
  if (!scales_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
    return default_static_value->Get(index);
  }
  return scales_->Get(index);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::clear_scales() {
  if (!scales_) {
    scales_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return scales_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* ConstPiecewiseScalingConf::_PiecewiseScalingConf_::mutable_scales() {
  if (!scales_) {
    scales_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return  scales_.get();
}
double* ConstPiecewiseScalingConf::_PiecewiseScalingConf_::mutable_scales(::std::size_t index) {
  if (!scales_) {
    scales_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return  scales_->Mutable(index);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::add_scales(const double& value) {
  if (!scales_) {
    scales_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return scales_->Add(value);
}
void ConstPiecewiseScalingConf::_PiecewiseScalingConf_::set_scales(::std::size_t index, const double& value) {
  if (!scales_) {
    scales_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>();
  }
  return scales_->Set(index, value);
}


int ConstPiecewiseScalingConf::_PiecewiseScalingConf_::compare(const _PiecewiseScalingConf_& other) {
  if (!(boundaries() == other.boundaries())) {
    return boundaries() < other.boundaries() ? -1 : 1;
  }
  if (!(scales() == other.scales())) {
    return scales() < other.scales() ? -1 : 1;
  }
  return 0;
}

bool ConstPiecewiseScalingConf::_PiecewiseScalingConf_::operator==(const _PiecewiseScalingConf_& other) const {
  return true
    && boundaries() == other.boundaries()
    && scales() == other.scales()
  ;
}

std::size_t ConstPiecewiseScalingConf::_PiecewiseScalingConf_::__CalcHash__() const {
  return 0
    ^ boundaries().__CalcHash__()
    ^ scales().__CalcHash__()
  ;
}

bool ConstPiecewiseScalingConf::_PiecewiseScalingConf_::operator<(const _PiecewiseScalingConf_& other) const {
  return false
    || !(boundaries() == other.boundaries()) ? 
      boundaries() < other.boundaries() : false
    || !(scales() == other.scales()) ? 
      scales() < other.scales() : false
  ;
}

using _PiecewiseScalingConf_ =  ConstPiecewiseScalingConf::_PiecewiseScalingConf_;
ConstPiecewiseScalingConf::ConstPiecewiseScalingConf(const ::std::shared_ptr<_PiecewiseScalingConf_>& data): data_(data) {}
ConstPiecewiseScalingConf::ConstPiecewiseScalingConf(): data_(::std::make_shared<_PiecewiseScalingConf_>()) {}
ConstPiecewiseScalingConf::ConstPiecewiseScalingConf(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf) {
  BuildFromProto(proto_piecewisescalingconf);
}
ConstPiecewiseScalingConf::ConstPiecewiseScalingConf(const ConstPiecewiseScalingConf&) = default;
ConstPiecewiseScalingConf::ConstPiecewiseScalingConf(ConstPiecewiseScalingConf&&) noexcept = default;
ConstPiecewiseScalingConf::~ConstPiecewiseScalingConf() = default;

void ConstPiecewiseScalingConf::ToProto(PbMessage* proto_piecewisescalingconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PiecewiseScalingConf*>(proto_piecewisescalingconf));
}
  
::std::string ConstPiecewiseScalingConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPiecewiseScalingConf::__Empty__() const {
  return !data_;
}

int ConstPiecewiseScalingConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"boundaries", 1},
    {"scales", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstPiecewiseScalingConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstPiecewiseScalingConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int64_t>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<double>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPiecewiseScalingConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &boundaries();
    case 2: return &scales();
    default: return nullptr;
  }
}

// repeated field boundaries
::std::size_t ConstPiecewiseScalingConf::boundaries_size() const {
  return __SharedPtrOrDefault__()->boundaries_size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& ConstPiecewiseScalingConf::boundaries() const {
  return __SharedPtrOrDefault__()->boundaries();
}
const int64_t& ConstPiecewiseScalingConf::boundaries(::std::size_t index) const {
  return __SharedPtrOrDefault__()->boundaries(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> ConstPiecewiseScalingConf::shared_const_boundaries() const {
  return boundaries().__SharedConst__();
}
// repeated field scales
::std::size_t ConstPiecewiseScalingConf::scales_size() const {
  return __SharedPtrOrDefault__()->scales_size();
}
const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& ConstPiecewiseScalingConf::scales() const {
  return __SharedPtrOrDefault__()->scales();
}
const double& ConstPiecewiseScalingConf::scales(::std::size_t index) const {
  return __SharedPtrOrDefault__()->scales(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> ConstPiecewiseScalingConf::shared_const_scales() const {
  return scales().__SharedConst__();
}

::std::shared_ptr<ConstPiecewiseScalingConf> ConstPiecewiseScalingConf::__SharedConst__() const {
  return ::std::make_shared<ConstPiecewiseScalingConf>(data_);
}
int64_t ConstPiecewiseScalingConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPiecewiseScalingConf::operator==(const ConstPiecewiseScalingConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPiecewiseScalingConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPiecewiseScalingConf::operator<(const ConstPiecewiseScalingConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PiecewiseScalingConf_>& ConstPiecewiseScalingConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PiecewiseScalingConf_> default_ptr = std::make_shared<_PiecewiseScalingConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_PiecewiseScalingConf_>& ConstPiecewiseScalingConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _PiecewiseScalingConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPiecewiseScalingConf
void ConstPiecewiseScalingConf::BuildFromProto(const PbMessage& proto_piecewisescalingconf) {
  data_ = ::std::make_shared<_PiecewiseScalingConf_>(dynamic_cast<const ::oneflow::PiecewiseScalingConf&>(proto_piecewisescalingconf));
}

PiecewiseScalingConf::PiecewiseScalingConf(const ::std::shared_ptr<_PiecewiseScalingConf_>& data)
  : ConstPiecewiseScalingConf(data) {}
PiecewiseScalingConf::PiecewiseScalingConf(const PiecewiseScalingConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PiecewiseScalingConf> resize
PiecewiseScalingConf::PiecewiseScalingConf(PiecewiseScalingConf&&) noexcept = default; 
PiecewiseScalingConf::PiecewiseScalingConf(const ::oneflow::PiecewiseScalingConf& proto_piecewisescalingconf) {
  InitFromProto(proto_piecewisescalingconf);
}
PiecewiseScalingConf::PiecewiseScalingConf() = default;

PiecewiseScalingConf::~PiecewiseScalingConf() = default;

void PiecewiseScalingConf::InitFromProto(const PbMessage& proto_piecewisescalingconf) {
  BuildFromProto(proto_piecewisescalingconf);
}
  
void* PiecewiseScalingConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_boundaries();
    case 2: return mutable_scales();
    default: return nullptr;
  }
}

bool PiecewiseScalingConf::operator==(const PiecewiseScalingConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PiecewiseScalingConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PiecewiseScalingConf::operator<(const PiecewiseScalingConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PiecewiseScalingConf::Clear() {
  if (data_) { data_.reset(); }
}
void PiecewiseScalingConf::CopyFrom(const PiecewiseScalingConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PiecewiseScalingConf& PiecewiseScalingConf::operator=(const PiecewiseScalingConf& other) {
  CopyFrom(other);
  return *this;
}

// repeated field boundaries
void PiecewiseScalingConf::clear_boundaries() {
  return __SharedPtr__()->clear_boundaries();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_* PiecewiseScalingConf::mutable_boundaries() {
  return __SharedPtr__()->mutable_boundaries();
}
int64_t* PiecewiseScalingConf::mutable_boundaries(::std::size_t index) {
  return __SharedPtr__()->mutable_boundaries(index);
}
void PiecewiseScalingConf::add_boundaries(const int64_t& value) {
  return __SharedPtr__()->add_boundaries(value);
}
void PiecewiseScalingConf::set_boundaries(::std::size_t index, const int64_t& value) {
  return __SharedPtr__()->set_boundaries(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> PiecewiseScalingConf::shared_mutable_boundaries() {
  return mutable_boundaries()->__SharedMutable__();
}
// repeated field scales
void PiecewiseScalingConf::clear_scales() {
  return __SharedPtr__()->clear_scales();
}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_* PiecewiseScalingConf::mutable_scales() {
  return __SharedPtr__()->mutable_scales();
}
double* PiecewiseScalingConf::mutable_scales(::std::size_t index) {
  return __SharedPtr__()->mutable_scales(index);
}
void PiecewiseScalingConf::add_scales(const double& value) {
  return __SharedPtr__()->add_scales(value);
}
void PiecewiseScalingConf::set_scales(::std::size_t index, const double& value) {
  return __SharedPtr__()->set_scales(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> PiecewiseScalingConf::shared_mutable_scales() {
  return mutable_scales()->__SharedMutable__();
}

::std::shared_ptr<PiecewiseScalingConf> PiecewiseScalingConf::__SharedMutable__() {
  return ::std::make_shared<PiecewiseScalingConf>(__SharedPtr__());
}
ConstLearningRateDecayConf::_LearningRateDecayConf_::_LearningRateDecayConf_() { Clear(); }
ConstLearningRateDecayConf::_LearningRateDecayConf_::_LearningRateDecayConf_(const _LearningRateDecayConf_& other) { CopyFrom(other); }
ConstLearningRateDecayConf::_LearningRateDecayConf_::_LearningRateDecayConf_(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf) {
  InitFromProto(proto_learningratedecayconf);
}
ConstLearningRateDecayConf::_LearningRateDecayConf_::_LearningRateDecayConf_(_LearningRateDecayConf_&& other) = default;
ConstLearningRateDecayConf::_LearningRateDecayConf_::~_LearningRateDecayConf_() = default;

void ConstLearningRateDecayConf::_LearningRateDecayConf_::InitFromProto(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_learningratedecayconf.type_case()));
  switch (type_case) {
    case kExponentialConf: {
      *mutable_exponential_conf() = ::oneflow::cfg::ExponentialDecayConf(proto_learningratedecayconf.exponential_conf());
      break;
  }
    case kInverseTimeConf: {
      *mutable_inverse_time_conf() = ::oneflow::cfg::InverseTimeDecayConf(proto_learningratedecayconf.inverse_time_conf());
      break;
  }
    case kNaturalExpConf: {
      *mutable_natural_exp_conf() = ::oneflow::cfg::NaturalExpDecayConf(proto_learningratedecayconf.natural_exp_conf());
      break;
  }
    case kPiecewiseConstantConf: {
      *mutable_piecewise_constant_conf() = ::oneflow::cfg::PiecewiseConstantConf(proto_learningratedecayconf.piecewise_constant_conf());
      break;
  }
    case kPolynomialConf: {
      *mutable_polynomial_conf() = ::oneflow::cfg::PolynomialDecayConf(proto_learningratedecayconf.polynomial_conf());
      break;
  }
    case kCosineConf: {
      *mutable_cosine_conf() = ::oneflow::cfg::CosineDecayConf(proto_learningratedecayconf.cosine_conf());
      break;
  }
    case kLinearCosineConf: {
      *mutable_linear_cosine_conf() = ::oneflow::cfg::LinearCosineDecayConf(proto_learningratedecayconf.linear_cosine_conf());
      break;
  }
    case kPiecewiseScalingConf: {
      *mutable_piecewise_scaling_conf() = ::oneflow::cfg::PiecewiseScalingConf(proto_learningratedecayconf.piecewise_scaling_conf());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstLearningRateDecayConf::_LearningRateDecayConf_::ToProto(::oneflow::LearningRateDecayConf* proto_learningratedecayconf) const {
  proto_learningratedecayconf->Clear();

  // oneof field: type
  ::oneflow::LearningRateDecayConf::TypeCase type_case = ::oneflow::LearningRateDecayConf::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::LearningRateDecayConf::kExponentialConf: {
      ::oneflow::ExponentialDecayConf of_proto_exponential_conf;
      exponential_conf().ToProto(&of_proto_exponential_conf);
      proto_learningratedecayconf->mutable_exponential_conf()->CopyFrom(of_proto_exponential_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kInverseTimeConf: {
      ::oneflow::InverseTimeDecayConf of_proto_inverse_time_conf;
      inverse_time_conf().ToProto(&of_proto_inverse_time_conf);
      proto_learningratedecayconf->mutable_inverse_time_conf()->CopyFrom(of_proto_inverse_time_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kNaturalExpConf: {
      ::oneflow::NaturalExpDecayConf of_proto_natural_exp_conf;
      natural_exp_conf().ToProto(&of_proto_natural_exp_conf);
      proto_learningratedecayconf->mutable_natural_exp_conf()->CopyFrom(of_proto_natural_exp_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kPiecewiseConstantConf: {
      ::oneflow::PiecewiseConstantConf of_proto_piecewise_constant_conf;
      piecewise_constant_conf().ToProto(&of_proto_piecewise_constant_conf);
      proto_learningratedecayconf->mutable_piecewise_constant_conf()->CopyFrom(of_proto_piecewise_constant_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kPolynomialConf: {
      ::oneflow::PolynomialDecayConf of_proto_polynomial_conf;
      polynomial_conf().ToProto(&of_proto_polynomial_conf);
      proto_learningratedecayconf->mutable_polynomial_conf()->CopyFrom(of_proto_polynomial_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kCosineConf: {
      ::oneflow::CosineDecayConf of_proto_cosine_conf;
      cosine_conf().ToProto(&of_proto_cosine_conf);
      proto_learningratedecayconf->mutable_cosine_conf()->CopyFrom(of_proto_cosine_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kLinearCosineConf: {
      ::oneflow::LinearCosineDecayConf of_proto_linear_cosine_conf;
      linear_cosine_conf().ToProto(&of_proto_linear_cosine_conf);
      proto_learningratedecayconf->mutable_linear_cosine_conf()->CopyFrom(of_proto_linear_cosine_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::kPiecewiseScalingConf: {
      ::oneflow::PiecewiseScalingConf of_proto_piecewise_scaling_conf;
      piecewise_scaling_conf().ToProto(&of_proto_piecewise_scaling_conf);
      proto_learningratedecayconf->mutable_piecewise_scaling_conf()->CopyFrom(of_proto_piecewise_scaling_conf);
      break;
    }
    case ::oneflow::LearningRateDecayConf::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstLearningRateDecayConf::_LearningRateDecayConf_::DebugString() const {
  ::oneflow::LearningRateDecayConf proto_learningratedecayconf;
  this->ToProto(&proto_learningratedecayconf);
  return proto_learningratedecayconf.DebugString();
}

void ConstLearningRateDecayConf::_LearningRateDecayConf_::Clear() {
  clear_type();
}

void ConstLearningRateDecayConf::_LearningRateDecayConf_::CopyFrom(const _LearningRateDecayConf_& other) {
  type_copy_from(other);
}


// oneof field type: exponential_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_exponential_conf() const {
  return type_case() == kExponentialConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_exponential_conf() {
  if (has_exponential_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.exponential_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ExponentialDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::exponential_conf() const {
  if (has_exponential_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>*>(&(type_.exponential_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::ExponentialDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ExponentialDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_exponential_conf() {
  if(!has_exponential_conf()) {
    clear_type();
    new (&(type_.exponential_conf_)) ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>(new ::oneflow::cfg::ExponentialDecayConf());
  }
  type_case_ = kExponentialConf;
  ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>*>(&(type_.exponential_conf_));
  return  (*ptr).get();
}

// oneof field type: inverse_time_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_inverse_time_conf() const {
  return type_case() == kInverseTimeConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_inverse_time_conf() {
  if (has_inverse_time_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.inverse_time_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::InverseTimeDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::inverse_time_conf() const {
  if (has_inverse_time_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>*>(&(type_.inverse_time_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::InverseTimeDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::InverseTimeDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_inverse_time_conf() {
  if(!has_inverse_time_conf()) {
    clear_type();
    new (&(type_.inverse_time_conf_)) ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>(new ::oneflow::cfg::InverseTimeDecayConf());
  }
  type_case_ = kInverseTimeConf;
  ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>*>(&(type_.inverse_time_conf_));
  return  (*ptr).get();
}

// oneof field type: natural_exp_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_natural_exp_conf() const {
  return type_case() == kNaturalExpConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_natural_exp_conf() {
  if (has_natural_exp_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.natural_exp_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::NaturalExpDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::natural_exp_conf() const {
  if (has_natural_exp_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>*>(&(type_.natural_exp_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::NaturalExpDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::NaturalExpDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_natural_exp_conf() {
  if(!has_natural_exp_conf()) {
    clear_type();
    new (&(type_.natural_exp_conf_)) ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>(new ::oneflow::cfg::NaturalExpDecayConf());
  }
  type_case_ = kNaturalExpConf;
  ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>*>(&(type_.natural_exp_conf_));
  return  (*ptr).get();
}

// oneof field type: piecewise_constant_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_piecewise_constant_conf() const {
  return type_case() == kPiecewiseConstantConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_piecewise_constant_conf() {
  if (has_piecewise_constant_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.piecewise_constant_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PiecewiseConstantConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::piecewise_constant_conf() const {
  if (has_piecewise_constant_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>*>(&(type_.piecewise_constant_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf> default_static_value = ::std::make_shared<::oneflow::cfg::PiecewiseConstantConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::PiecewiseConstantConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_piecewise_constant_conf() {
  if(!has_piecewise_constant_conf()) {
    clear_type();
    new (&(type_.piecewise_constant_conf_)) ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>(new ::oneflow::cfg::PiecewiseConstantConf());
  }
  type_case_ = kPiecewiseConstantConf;
  ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>*>(&(type_.piecewise_constant_conf_));
  return  (*ptr).get();
}

// oneof field type: polynomial_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_polynomial_conf() const {
  return type_case() == kPolynomialConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_polynomial_conf() {
  if (has_polynomial_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.polynomial_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PolynomialDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::polynomial_conf() const {
  if (has_polynomial_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>*>(&(type_.polynomial_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::PolynomialDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::PolynomialDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_polynomial_conf() {
  if(!has_polynomial_conf()) {
    clear_type();
    new (&(type_.polynomial_conf_)) ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>(new ::oneflow::cfg::PolynomialDecayConf());
  }
  type_case_ = kPolynomialConf;
  ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>*>(&(type_.polynomial_conf_));
  return  (*ptr).get();
}

// oneof field type: cosine_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_cosine_conf() const {
  return type_case() == kCosineConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_cosine_conf() {
  if (has_cosine_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.cosine_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::CosineDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::cosine_conf() const {
  if (has_cosine_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>*>(&(type_.cosine_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::CosineDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::CosineDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::CosineDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_cosine_conf() {
  if(!has_cosine_conf()) {
    clear_type();
    new (&(type_.cosine_conf_)) ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>(new ::oneflow::cfg::CosineDecayConf());
  }
  type_case_ = kCosineConf;
  ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::CosineDecayConf>*>(&(type_.cosine_conf_));
  return  (*ptr).get();
}

// oneof field type: linear_cosine_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_linear_cosine_conf() const {
  return type_case() == kLinearCosineConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_linear_cosine_conf() {
  if (has_linear_cosine_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.linear_cosine_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LinearCosineDecayConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::linear_cosine_conf() const {
  if (has_linear_cosine_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>*>(&(type_.linear_cosine_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf> default_static_value = ::std::make_shared<::oneflow::cfg::LinearCosineDecayConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LinearCosineDecayConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_linear_cosine_conf() {
  if(!has_linear_cosine_conf()) {
    clear_type();
    new (&(type_.linear_cosine_conf_)) ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>(new ::oneflow::cfg::LinearCosineDecayConf());
  }
  type_case_ = kLinearCosineConf;
  ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>*>(&(type_.linear_cosine_conf_));
  return  (*ptr).get();
}

// oneof field type: piecewise_scaling_conf
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_piecewise_scaling_conf() const {
  return type_case() == kPiecewiseScalingConf;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_piecewise_scaling_conf() {
  if (has_piecewise_scaling_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.piecewise_scaling_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PiecewiseScalingConf& ConstLearningRateDecayConf::_LearningRateDecayConf_::piecewise_scaling_conf() const {
  if (has_piecewise_scaling_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>*>(&(type_.piecewise_scaling_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf> default_static_value = ::std::make_shared<::oneflow::cfg::PiecewiseScalingConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::PiecewiseScalingConf* ConstLearningRateDecayConf::_LearningRateDecayConf_::mutable_piecewise_scaling_conf() {
  if(!has_piecewise_scaling_conf()) {
    clear_type();
    new (&(type_.piecewise_scaling_conf_)) ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>(new ::oneflow::cfg::PiecewiseScalingConf());
  }
  type_case_ = kPiecewiseScalingConf;
  ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>*>(&(type_.piecewise_scaling_conf_));
  return  (*ptr).get();
}
ConstLearningRateDecayConf::TypeCase ConstLearningRateDecayConf::_LearningRateDecayConf_::type_case() const {
  return type_case_;
}
bool ConstLearningRateDecayConf::_LearningRateDecayConf_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::clear_type() {
  switch (type_case()) {
    case kExponentialConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.exponential_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kInverseTimeConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.inverse_time_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kNaturalExpConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.natural_exp_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPiecewiseConstantConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.piecewise_constant_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPolynomialConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.polynomial_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kCosineConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::CosineDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.cosine_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLinearCosineConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.linear_cosine_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPiecewiseScalingConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.piecewise_scaling_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstLearningRateDecayConf::_LearningRateDecayConf_::type_copy_from(const _LearningRateDecayConf_& other) {
  switch (other.type_case()) {
    case kExponentialConf: {
      mutable_exponential_conf()->CopyFrom(other.exponential_conf());
      break;
    }
    case kInverseTimeConf: {
      mutable_inverse_time_conf()->CopyFrom(other.inverse_time_conf());
      break;
    }
    case kNaturalExpConf: {
      mutable_natural_exp_conf()->CopyFrom(other.natural_exp_conf());
      break;
    }
    case kPiecewiseConstantConf: {
      mutable_piecewise_constant_conf()->CopyFrom(other.piecewise_constant_conf());
      break;
    }
    case kPolynomialConf: {
      mutable_polynomial_conf()->CopyFrom(other.polynomial_conf());
      break;
    }
    case kCosineConf: {
      mutable_cosine_conf()->CopyFrom(other.cosine_conf());
      break;
    }
    case kLinearCosineConf: {
      mutable_linear_cosine_conf()->CopyFrom(other.linear_cosine_conf());
      break;
    }
    case kPiecewiseScalingConf: {
      mutable_piecewise_scaling_conf()->CopyFrom(other.piecewise_scaling_conf());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstLearningRateDecayConf::_LearningRateDecayConf_::compare(const _LearningRateDecayConf_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kExponentialConf: {
      if (!(exponential_conf() == other.exponential_conf())) {
        return exponential_conf() < other.exponential_conf() ? -1 : 1;
      }
      break;
    }
    case kInverseTimeConf: {
      if (!(inverse_time_conf() == other.inverse_time_conf())) {
        return inverse_time_conf() < other.inverse_time_conf() ? -1 : 1;
      }
      break;
    }
    case kNaturalExpConf: {
      if (!(natural_exp_conf() == other.natural_exp_conf())) {
        return natural_exp_conf() < other.natural_exp_conf() ? -1 : 1;
      }
      break;
    }
    case kPiecewiseConstantConf: {
      if (!(piecewise_constant_conf() == other.piecewise_constant_conf())) {
        return piecewise_constant_conf() < other.piecewise_constant_conf() ? -1 : 1;
      }
      break;
    }
    case kPolynomialConf: {
      if (!(polynomial_conf() == other.polynomial_conf())) {
        return polynomial_conf() < other.polynomial_conf() ? -1 : 1;
      }
      break;
    }
    case kCosineConf: {
      if (!(cosine_conf() == other.cosine_conf())) {
        return cosine_conf() < other.cosine_conf() ? -1 : 1;
      }
      break;
    }
    case kLinearCosineConf: {
      if (!(linear_cosine_conf() == other.linear_cosine_conf())) {
        return linear_cosine_conf() < other.linear_cosine_conf() ? -1 : 1;
      }
      break;
    }
    case kPiecewiseScalingConf: {
      if (!(piecewise_scaling_conf() == other.piecewise_scaling_conf())) {
        return piecewise_scaling_conf() < other.piecewise_scaling_conf() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstLearningRateDecayConf::_LearningRateDecayConf_::operator==(const _LearningRateDecayConf_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kExponentialConf ? 
      exponential_conf() == other.exponential_conf() : true)
    && (type_case() == kInverseTimeConf ? 
      inverse_time_conf() == other.inverse_time_conf() : true)
    && (type_case() == kNaturalExpConf ? 
      natural_exp_conf() == other.natural_exp_conf() : true)
    && (type_case() == kPiecewiseConstantConf ? 
      piecewise_constant_conf() == other.piecewise_constant_conf() : true)
    && (type_case() == kPolynomialConf ? 
      polynomial_conf() == other.polynomial_conf() : true)
    && (type_case() == kCosineConf ? 
      cosine_conf() == other.cosine_conf() : true)
    && (type_case() == kLinearCosineConf ? 
      linear_cosine_conf() == other.linear_cosine_conf() : true)
    && (type_case() == kPiecewiseScalingConf ? 
      piecewise_scaling_conf() == other.piecewise_scaling_conf() : true)
  ;
}

std::size_t ConstLearningRateDecayConf::_LearningRateDecayConf_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_exponential_conf() ? std::hash<::oneflow::cfg::ExponentialDecayConf>()(exponential_conf()) : 0)
    ^ (has_inverse_time_conf() ? std::hash<::oneflow::cfg::InverseTimeDecayConf>()(inverse_time_conf()) : 0)
    ^ (has_natural_exp_conf() ? std::hash<::oneflow::cfg::NaturalExpDecayConf>()(natural_exp_conf()) : 0)
    ^ (has_piecewise_constant_conf() ? std::hash<::oneflow::cfg::PiecewiseConstantConf>()(piecewise_constant_conf()) : 0)
    ^ (has_polynomial_conf() ? std::hash<::oneflow::cfg::PolynomialDecayConf>()(polynomial_conf()) : 0)
    ^ (has_cosine_conf() ? std::hash<::oneflow::cfg::CosineDecayConf>()(cosine_conf()) : 0)
    ^ (has_linear_cosine_conf() ? std::hash<::oneflow::cfg::LinearCosineDecayConf>()(linear_cosine_conf()) : 0)
    ^ (has_piecewise_scaling_conf() ? std::hash<::oneflow::cfg::PiecewiseScalingConf>()(piecewise_scaling_conf()) : 0)
  ;
}

bool ConstLearningRateDecayConf::_LearningRateDecayConf_::operator<(const _LearningRateDecayConf_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kExponentialConf) && 
      !(exponential_conf() == other.exponential_conf())) ? 
        exponential_conf() < other.exponential_conf() : false
    || ((type_case() == kInverseTimeConf) && 
      !(inverse_time_conf() == other.inverse_time_conf())) ? 
        inverse_time_conf() < other.inverse_time_conf() : false
    || ((type_case() == kNaturalExpConf) && 
      !(natural_exp_conf() == other.natural_exp_conf())) ? 
        natural_exp_conf() < other.natural_exp_conf() : false
    || ((type_case() == kPiecewiseConstantConf) && 
      !(piecewise_constant_conf() == other.piecewise_constant_conf())) ? 
        piecewise_constant_conf() < other.piecewise_constant_conf() : false
    || ((type_case() == kPolynomialConf) && 
      !(polynomial_conf() == other.polynomial_conf())) ? 
        polynomial_conf() < other.polynomial_conf() : false
    || ((type_case() == kCosineConf) && 
      !(cosine_conf() == other.cosine_conf())) ? 
        cosine_conf() < other.cosine_conf() : false
    || ((type_case() == kLinearCosineConf) && 
      !(linear_cosine_conf() == other.linear_cosine_conf())) ? 
        linear_cosine_conf() < other.linear_cosine_conf() : false
    || ((type_case() == kPiecewiseScalingConf) && 
      !(piecewise_scaling_conf() == other.piecewise_scaling_conf())) ? 
        piecewise_scaling_conf() < other.piecewise_scaling_conf() : false
  ;
}

using _LearningRateDecayConf_ =  ConstLearningRateDecayConf::_LearningRateDecayConf_;
ConstLearningRateDecayConf::ConstLearningRateDecayConf(const ::std::shared_ptr<_LearningRateDecayConf_>& data): data_(data) {}
ConstLearningRateDecayConf::ConstLearningRateDecayConf(): data_(::std::make_shared<_LearningRateDecayConf_>()) {}
ConstLearningRateDecayConf::ConstLearningRateDecayConf(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf) {
  BuildFromProto(proto_learningratedecayconf);
}
ConstLearningRateDecayConf::ConstLearningRateDecayConf(const ConstLearningRateDecayConf&) = default;
ConstLearningRateDecayConf::ConstLearningRateDecayConf(ConstLearningRateDecayConf&&) noexcept = default;
ConstLearningRateDecayConf::~ConstLearningRateDecayConf() = default;

void ConstLearningRateDecayConf::ToProto(PbMessage* proto_learningratedecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LearningRateDecayConf*>(proto_learningratedecayconf));
}
  
::std::string ConstLearningRateDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLearningRateDecayConf::__Empty__() const {
  return !data_;
}

int ConstLearningRateDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"exponential_conf", 2000},
    {"inverse_time_conf", 2001},
    {"natural_exp_conf", 2002},
    {"piecewise_constant_conf", 2003},
    {"polynomial_conf", 2004},
    {"cosine_conf", 2005},
    {"linear_cosine_conf", 2006},
    {"piecewise_scaling_conf", 2007},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLearningRateDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 2000:
    case 2001:
    case 2002:
    case 2003:
    case 2004:
    case 2005:
    case 2006:
    case 2007:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLearningRateDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 2000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ExponentialDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstExponentialDecayConf),
      };
      return type_indices;
    }
    case 2001: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::InverseTimeDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInverseTimeDecayConf),
      };
      return type_indices;
    }
    case 2002: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::NaturalExpDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstNaturalExpDecayConf),
      };
      return type_indices;
    }
    case 2003: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PiecewiseConstantConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPiecewiseConstantConf),
      };
      return type_indices;
    }
    case 2004: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PolynomialDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPolynomialDecayConf),
      };
      return type_indices;
    }
    case 2005: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::CosineDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstCosineDecayConf),
      };
      return type_indices;
    }
    case 2006: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LinearCosineDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLinearCosineDecayConf),
      };
      return type_indices;
    }
    case 2007: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PiecewiseScalingConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPiecewiseScalingConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLearningRateDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 2000: return &exponential_conf();
    case 2001: return &inverse_time_conf();
    case 2002: return &natural_exp_conf();
    case 2003: return &piecewise_constant_conf();
    case 2004: return &polynomial_conf();
    case 2005: return &cosine_conf();
    case 2006: return &linear_cosine_conf();
    case 2007: return &piecewise_scaling_conf();
    default: return nullptr;
  }
}

 // oneof field type: exponential_conf
bool ConstLearningRateDecayConf::has_exponential_conf() const {
  return __SharedPtrOrDefault__()->has_exponential_conf();
}
const ::oneflow::cfg::ExponentialDecayConf& ConstLearningRateDecayConf::exponential_conf() const {
  return __SharedPtrOrDefault__()->exponential_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstExponentialDecayConf> ConstLearningRateDecayConf::shared_const_exponential_conf() const {
  return exponential_conf().__SharedConst__();
}
 // oneof field type: inverse_time_conf
bool ConstLearningRateDecayConf::has_inverse_time_conf() const {
  return __SharedPtrOrDefault__()->has_inverse_time_conf();
}
const ::oneflow::cfg::InverseTimeDecayConf& ConstLearningRateDecayConf::inverse_time_conf() const {
  return __SharedPtrOrDefault__()->inverse_time_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInverseTimeDecayConf> ConstLearningRateDecayConf::shared_const_inverse_time_conf() const {
  return inverse_time_conf().__SharedConst__();
}
 // oneof field type: natural_exp_conf
bool ConstLearningRateDecayConf::has_natural_exp_conf() const {
  return __SharedPtrOrDefault__()->has_natural_exp_conf();
}
const ::oneflow::cfg::NaturalExpDecayConf& ConstLearningRateDecayConf::natural_exp_conf() const {
  return __SharedPtrOrDefault__()->natural_exp_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstNaturalExpDecayConf> ConstLearningRateDecayConf::shared_const_natural_exp_conf() const {
  return natural_exp_conf().__SharedConst__();
}
 // oneof field type: piecewise_constant_conf
bool ConstLearningRateDecayConf::has_piecewise_constant_conf() const {
  return __SharedPtrOrDefault__()->has_piecewise_constant_conf();
}
const ::oneflow::cfg::PiecewiseConstantConf& ConstLearningRateDecayConf::piecewise_constant_conf() const {
  return __SharedPtrOrDefault__()->piecewise_constant_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPiecewiseConstantConf> ConstLearningRateDecayConf::shared_const_piecewise_constant_conf() const {
  return piecewise_constant_conf().__SharedConst__();
}
 // oneof field type: polynomial_conf
bool ConstLearningRateDecayConf::has_polynomial_conf() const {
  return __SharedPtrOrDefault__()->has_polynomial_conf();
}
const ::oneflow::cfg::PolynomialDecayConf& ConstLearningRateDecayConf::polynomial_conf() const {
  return __SharedPtrOrDefault__()->polynomial_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPolynomialDecayConf> ConstLearningRateDecayConf::shared_const_polynomial_conf() const {
  return polynomial_conf().__SharedConst__();
}
 // oneof field type: cosine_conf
bool ConstLearningRateDecayConf::has_cosine_conf() const {
  return __SharedPtrOrDefault__()->has_cosine_conf();
}
const ::oneflow::cfg::CosineDecayConf& ConstLearningRateDecayConf::cosine_conf() const {
  return __SharedPtrOrDefault__()->cosine_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstCosineDecayConf> ConstLearningRateDecayConf::shared_const_cosine_conf() const {
  return cosine_conf().__SharedConst__();
}
 // oneof field type: linear_cosine_conf
bool ConstLearningRateDecayConf::has_linear_cosine_conf() const {
  return __SharedPtrOrDefault__()->has_linear_cosine_conf();
}
const ::oneflow::cfg::LinearCosineDecayConf& ConstLearningRateDecayConf::linear_cosine_conf() const {
  return __SharedPtrOrDefault__()->linear_cosine_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLinearCosineDecayConf> ConstLearningRateDecayConf::shared_const_linear_cosine_conf() const {
  return linear_cosine_conf().__SharedConst__();
}
 // oneof field type: piecewise_scaling_conf
bool ConstLearningRateDecayConf::has_piecewise_scaling_conf() const {
  return __SharedPtrOrDefault__()->has_piecewise_scaling_conf();
}
const ::oneflow::cfg::PiecewiseScalingConf& ConstLearningRateDecayConf::piecewise_scaling_conf() const {
  return __SharedPtrOrDefault__()->piecewise_scaling_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPiecewiseScalingConf> ConstLearningRateDecayConf::shared_const_piecewise_scaling_conf() const {
  return piecewise_scaling_conf().__SharedConst__();
}
ConstLearningRateDecayConf::TypeCase ConstLearningRateDecayConf::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstLearningRateDecayConf::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstLearningRateDecayConf> ConstLearningRateDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstLearningRateDecayConf>(data_);
}
int64_t ConstLearningRateDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLearningRateDecayConf::operator==(const ConstLearningRateDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLearningRateDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLearningRateDecayConf::operator<(const ConstLearningRateDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LearningRateDecayConf_>& ConstLearningRateDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LearningRateDecayConf_> default_ptr = std::make_shared<_LearningRateDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LearningRateDecayConf_>& ConstLearningRateDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LearningRateDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLearningRateDecayConf
void ConstLearningRateDecayConf::BuildFromProto(const PbMessage& proto_learningratedecayconf) {
  data_ = ::std::make_shared<_LearningRateDecayConf_>(dynamic_cast<const ::oneflow::LearningRateDecayConf&>(proto_learningratedecayconf));
}

LearningRateDecayConf::LearningRateDecayConf(const ::std::shared_ptr<_LearningRateDecayConf_>& data)
  : ConstLearningRateDecayConf(data) {}
LearningRateDecayConf::LearningRateDecayConf(const LearningRateDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LearningRateDecayConf> resize
LearningRateDecayConf::LearningRateDecayConf(LearningRateDecayConf&&) noexcept = default; 
LearningRateDecayConf::LearningRateDecayConf(const ::oneflow::LearningRateDecayConf& proto_learningratedecayconf) {
  InitFromProto(proto_learningratedecayconf);
}
LearningRateDecayConf::LearningRateDecayConf() = default;

LearningRateDecayConf::~LearningRateDecayConf() = default;

void LearningRateDecayConf::InitFromProto(const PbMessage& proto_learningratedecayconf) {
  BuildFromProto(proto_learningratedecayconf);
}
  
void* LearningRateDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 2000: return mutable_exponential_conf();
    case 2001: return mutable_inverse_time_conf();
    case 2002: return mutable_natural_exp_conf();
    case 2003: return mutable_piecewise_constant_conf();
    case 2004: return mutable_polynomial_conf();
    case 2005: return mutable_cosine_conf();
    case 2006: return mutable_linear_cosine_conf();
    case 2007: return mutable_piecewise_scaling_conf();
    default: return nullptr;
  }
}

bool LearningRateDecayConf::operator==(const LearningRateDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LearningRateDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LearningRateDecayConf::operator<(const LearningRateDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LearningRateDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void LearningRateDecayConf::CopyFrom(const LearningRateDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LearningRateDecayConf& LearningRateDecayConf::operator=(const LearningRateDecayConf& other) {
  CopyFrom(other);
  return *this;
}

void LearningRateDecayConf::clear_exponential_conf() {
  return __SharedPtr__()->clear_exponential_conf();
}
::oneflow::cfg::ExponentialDecayConf* LearningRateDecayConf::mutable_exponential_conf() {
  return __SharedPtr__()->mutable_exponential_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ExponentialDecayConf> LearningRateDecayConf::shared_mutable_exponential_conf() {
  return mutable_exponential_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_inverse_time_conf() {
  return __SharedPtr__()->clear_inverse_time_conf();
}
::oneflow::cfg::InverseTimeDecayConf* LearningRateDecayConf::mutable_inverse_time_conf() {
  return __SharedPtr__()->mutable_inverse_time_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::InverseTimeDecayConf> LearningRateDecayConf::shared_mutable_inverse_time_conf() {
  return mutable_inverse_time_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_natural_exp_conf() {
  return __SharedPtr__()->clear_natural_exp_conf();
}
::oneflow::cfg::NaturalExpDecayConf* LearningRateDecayConf::mutable_natural_exp_conf() {
  return __SharedPtr__()->mutable_natural_exp_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::NaturalExpDecayConf> LearningRateDecayConf::shared_mutable_natural_exp_conf() {
  return mutable_natural_exp_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_piecewise_constant_conf() {
  return __SharedPtr__()->clear_piecewise_constant_conf();
}
::oneflow::cfg::PiecewiseConstantConf* LearningRateDecayConf::mutable_piecewise_constant_conf() {
  return __SharedPtr__()->mutable_piecewise_constant_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PiecewiseConstantConf> LearningRateDecayConf::shared_mutable_piecewise_constant_conf() {
  return mutable_piecewise_constant_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_polynomial_conf() {
  return __SharedPtr__()->clear_polynomial_conf();
}
::oneflow::cfg::PolynomialDecayConf* LearningRateDecayConf::mutable_polynomial_conf() {
  return __SharedPtr__()->mutable_polynomial_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PolynomialDecayConf> LearningRateDecayConf::shared_mutable_polynomial_conf() {
  return mutable_polynomial_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_cosine_conf() {
  return __SharedPtr__()->clear_cosine_conf();
}
::oneflow::cfg::CosineDecayConf* LearningRateDecayConf::mutable_cosine_conf() {
  return __SharedPtr__()->mutable_cosine_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::CosineDecayConf> LearningRateDecayConf::shared_mutable_cosine_conf() {
  return mutable_cosine_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_linear_cosine_conf() {
  return __SharedPtr__()->clear_linear_cosine_conf();
}
::oneflow::cfg::LinearCosineDecayConf* LearningRateDecayConf::mutable_linear_cosine_conf() {
  return __SharedPtr__()->mutable_linear_cosine_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LinearCosineDecayConf> LearningRateDecayConf::shared_mutable_linear_cosine_conf() {
  return mutable_linear_cosine_conf()->__SharedMutable__();
}
void LearningRateDecayConf::clear_piecewise_scaling_conf() {
  return __SharedPtr__()->clear_piecewise_scaling_conf();
}
::oneflow::cfg::PiecewiseScalingConf* LearningRateDecayConf::mutable_piecewise_scaling_conf() {
  return __SharedPtr__()->mutable_piecewise_scaling_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PiecewiseScalingConf> LearningRateDecayConf::shared_mutable_piecewise_scaling_conf() {
  return mutable_piecewise_scaling_conf()->__SharedMutable__();
}

::std::shared_ptr<LearningRateDecayConf> LearningRateDecayConf::__SharedMutable__() {
  return ::std::make_shared<LearningRateDecayConf>(__SharedPtr__());
}
ConstConstantWarmupConf::_ConstantWarmupConf_::_ConstantWarmupConf_() { Clear(); }
ConstConstantWarmupConf::_ConstantWarmupConf_::_ConstantWarmupConf_(const _ConstantWarmupConf_& other) { CopyFrom(other); }
ConstConstantWarmupConf::_ConstantWarmupConf_::_ConstantWarmupConf_(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf) {
  InitFromProto(proto_constantwarmupconf);
}
ConstConstantWarmupConf::_ConstantWarmupConf_::_ConstantWarmupConf_(_ConstantWarmupConf_&& other) = default;
ConstConstantWarmupConf::_ConstantWarmupConf_::~_ConstantWarmupConf_() = default;

void ConstConstantWarmupConf::_ConstantWarmupConf_::InitFromProto(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf) {
  Clear();
  // required_or_optional field: warmup_batches
  if (proto_constantwarmupconf.has_warmup_batches()) {
    set_warmup_batches(proto_constantwarmupconf.warmup_batches());
  }
  // required_or_optional field: multiplier
  if (proto_constantwarmupconf.has_multiplier()) {
    set_multiplier(proto_constantwarmupconf.multiplier());
  }
    
}

void ConstConstantWarmupConf::_ConstantWarmupConf_::ToProto(::oneflow::ConstantWarmupConf* proto_constantwarmupconf) const {
  proto_constantwarmupconf->Clear();
  // required_or_optional field: warmup_batches
  if (this->has_warmup_batches()) {
    proto_constantwarmupconf->set_warmup_batches(warmup_batches());
    }
  // required_or_optional field: multiplier
  if (this->has_multiplier()) {
    proto_constantwarmupconf->set_multiplier(multiplier());
    }

}

::std::string ConstConstantWarmupConf::_ConstantWarmupConf_::DebugString() const {
  ::oneflow::ConstantWarmupConf proto_constantwarmupconf;
  this->ToProto(&proto_constantwarmupconf);
  return proto_constantwarmupconf.DebugString();
}

void ConstConstantWarmupConf::_ConstantWarmupConf_::Clear() {
  clear_warmup_batches();
  clear_multiplier();
}

void ConstConstantWarmupConf::_ConstantWarmupConf_::CopyFrom(const _ConstantWarmupConf_& other) {
  if (other.has_warmup_batches()) {
    set_warmup_batches(other.warmup_batches());
  } else {
    clear_warmup_batches();
  }
  if (other.has_multiplier()) {
    set_multiplier(other.multiplier());
  } else {
    clear_multiplier();
  }
}


// optional field warmup_batches
bool ConstConstantWarmupConf::_ConstantWarmupConf_::has_warmup_batches() const {
  return has_warmup_batches_;
}
const int64_t& ConstConstantWarmupConf::_ConstantWarmupConf_::warmup_batches() const {
  if (has_warmup_batches_) { return warmup_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstConstantWarmupConf::_ConstantWarmupConf_::clear_warmup_batches() {
  has_warmup_batches_ = false;
}
void ConstConstantWarmupConf::_ConstantWarmupConf_::set_warmup_batches(const int64_t& value) {
  warmup_batches_ = value;
  has_warmup_batches_ = true;
}
int64_t* ConstConstantWarmupConf::_ConstantWarmupConf_::mutable_warmup_batches() {
  has_warmup_batches_ = true;
  return &warmup_batches_;
}

// optional field multiplier
bool ConstConstantWarmupConf::_ConstantWarmupConf_::has_multiplier() const {
  return has_multiplier_;
}
const double& ConstConstantWarmupConf::_ConstantWarmupConf_::multiplier() const {
  if (has_multiplier_) { return multiplier_; }
  static const double default_static_value = double();
  return default_static_value;
}
void ConstConstantWarmupConf::_ConstantWarmupConf_::clear_multiplier() {
  has_multiplier_ = false;
}
void ConstConstantWarmupConf::_ConstantWarmupConf_::set_multiplier(const double& value) {
  multiplier_ = value;
  has_multiplier_ = true;
}
double* ConstConstantWarmupConf::_ConstantWarmupConf_::mutable_multiplier() {
  has_multiplier_ = true;
  return &multiplier_;
}


int ConstConstantWarmupConf::_ConstantWarmupConf_::compare(const _ConstantWarmupConf_& other) {
  if (!(has_warmup_batches() == other.has_warmup_batches())) {
    return has_warmup_batches() < other.has_warmup_batches() ? -1 : 1;
  } else if (!(warmup_batches() == other.warmup_batches())) {
    return warmup_batches() < other.warmup_batches() ? -1 : 1;
  }
  if (!(has_multiplier() == other.has_multiplier())) {
    return has_multiplier() < other.has_multiplier() ? -1 : 1;
  } else if (!(multiplier() == other.multiplier())) {
    return multiplier() < other.multiplier() ? -1 : 1;
  }
  return 0;
}

bool ConstConstantWarmupConf::_ConstantWarmupConf_::operator==(const _ConstantWarmupConf_& other) const {
  return true
    && has_warmup_batches() == other.has_warmup_batches() 
    && warmup_batches() == other.warmup_batches()
    && has_multiplier() == other.has_multiplier() 
    && multiplier() == other.multiplier()
  ;
}

std::size_t ConstConstantWarmupConf::_ConstantWarmupConf_::__CalcHash__() const {
  return 0
    ^ (has_warmup_batches() ? std::hash<int64_t>()(warmup_batches()) : 0)
    ^ (has_multiplier() ? std::hash<double>()(multiplier()) : 0)
  ;
}

bool ConstConstantWarmupConf::_ConstantWarmupConf_::operator<(const _ConstantWarmupConf_& other) const {
  return false
    || !(has_warmup_batches() == other.has_warmup_batches()) ? 
      has_warmup_batches() < other.has_warmup_batches() : false
    || !(warmup_batches() == other.warmup_batches()) ? 
      warmup_batches() < other.warmup_batches() : false
    || !(has_multiplier() == other.has_multiplier()) ? 
      has_multiplier() < other.has_multiplier() : false
    || !(multiplier() == other.multiplier()) ? 
      multiplier() < other.multiplier() : false
  ;
}

using _ConstantWarmupConf_ =  ConstConstantWarmupConf::_ConstantWarmupConf_;
ConstConstantWarmupConf::ConstConstantWarmupConf(const ::std::shared_ptr<_ConstantWarmupConf_>& data): data_(data) {}
ConstConstantWarmupConf::ConstConstantWarmupConf(): data_(::std::make_shared<_ConstantWarmupConf_>()) {}
ConstConstantWarmupConf::ConstConstantWarmupConf(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf) {
  BuildFromProto(proto_constantwarmupconf);
}
ConstConstantWarmupConf::ConstConstantWarmupConf(const ConstConstantWarmupConf&) = default;
ConstConstantWarmupConf::ConstConstantWarmupConf(ConstConstantWarmupConf&&) noexcept = default;
ConstConstantWarmupConf::~ConstConstantWarmupConf() = default;

void ConstConstantWarmupConf::ToProto(PbMessage* proto_constantwarmupconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ConstantWarmupConf*>(proto_constantwarmupconf));
}
  
::std::string ConstConstantWarmupConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstConstantWarmupConf::__Empty__() const {
  return !data_;
}

int ConstConstantWarmupConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"warmup_batches", 1},
    {"multiplier", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstConstantWarmupConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstConstantWarmupConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstConstantWarmupConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &warmup_batches();
    case 2: return &multiplier();
    default: return nullptr;
  }
}

// required or optional field warmup_batches
bool ConstConstantWarmupConf::has_warmup_batches() const {
  return __SharedPtrOrDefault__()->has_warmup_batches();
}
const int64_t& ConstConstantWarmupConf::warmup_batches() const {
  return __SharedPtrOrDefault__()->warmup_batches();
}
// used by pybind11 only
// required or optional field multiplier
bool ConstConstantWarmupConf::has_multiplier() const {
  return __SharedPtrOrDefault__()->has_multiplier();
}
const double& ConstConstantWarmupConf::multiplier() const {
  return __SharedPtrOrDefault__()->multiplier();
}
// used by pybind11 only

::std::shared_ptr<ConstConstantWarmupConf> ConstConstantWarmupConf::__SharedConst__() const {
  return ::std::make_shared<ConstConstantWarmupConf>(data_);
}
int64_t ConstConstantWarmupConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstConstantWarmupConf::operator==(const ConstConstantWarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstConstantWarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstConstantWarmupConf::operator<(const ConstConstantWarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ConstantWarmupConf_>& ConstConstantWarmupConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ConstantWarmupConf_> default_ptr = std::make_shared<_ConstantWarmupConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ConstantWarmupConf_>& ConstConstantWarmupConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ConstantWarmupConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstConstantWarmupConf
void ConstConstantWarmupConf::BuildFromProto(const PbMessage& proto_constantwarmupconf) {
  data_ = ::std::make_shared<_ConstantWarmupConf_>(dynamic_cast<const ::oneflow::ConstantWarmupConf&>(proto_constantwarmupconf));
}

ConstantWarmupConf::ConstantWarmupConf(const ::std::shared_ptr<_ConstantWarmupConf_>& data)
  : ConstConstantWarmupConf(data) {}
ConstantWarmupConf::ConstantWarmupConf(const ConstantWarmupConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ConstantWarmupConf> resize
ConstantWarmupConf::ConstantWarmupConf(ConstantWarmupConf&&) noexcept = default; 
ConstantWarmupConf::ConstantWarmupConf(const ::oneflow::ConstantWarmupConf& proto_constantwarmupconf) {
  InitFromProto(proto_constantwarmupconf);
}
ConstantWarmupConf::ConstantWarmupConf() = default;

ConstantWarmupConf::~ConstantWarmupConf() = default;

void ConstantWarmupConf::InitFromProto(const PbMessage& proto_constantwarmupconf) {
  BuildFromProto(proto_constantwarmupconf);
}
  
void* ConstantWarmupConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_warmup_batches();
    case 2: return mutable_multiplier();
    default: return nullptr;
  }
}

bool ConstantWarmupConf::operator==(const ConstantWarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstantWarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstantWarmupConf::operator<(const ConstantWarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ConstantWarmupConf::Clear() {
  if (data_) { data_.reset(); }
}
void ConstantWarmupConf::CopyFrom(const ConstantWarmupConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ConstantWarmupConf& ConstantWarmupConf::operator=(const ConstantWarmupConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field warmup_batches
void ConstantWarmupConf::clear_warmup_batches() {
  return __SharedPtr__()->clear_warmup_batches();
}
void ConstantWarmupConf::set_warmup_batches(const int64_t& value) {
  return __SharedPtr__()->set_warmup_batches(value);
}
int64_t* ConstantWarmupConf::mutable_warmup_batches() {
  return  __SharedPtr__()->mutable_warmup_batches();
}
// required or optional field multiplier
void ConstantWarmupConf::clear_multiplier() {
  return __SharedPtr__()->clear_multiplier();
}
void ConstantWarmupConf::set_multiplier(const double& value) {
  return __SharedPtr__()->set_multiplier(value);
}
double* ConstantWarmupConf::mutable_multiplier() {
  return  __SharedPtr__()->mutable_multiplier();
}

::std::shared_ptr<ConstantWarmupConf> ConstantWarmupConf::__SharedMutable__() {
  return ::std::make_shared<ConstantWarmupConf>(__SharedPtr__());
}
ConstLinearWarmupConf::_LinearWarmupConf_::_LinearWarmupConf_() { Clear(); }
ConstLinearWarmupConf::_LinearWarmupConf_::_LinearWarmupConf_(const _LinearWarmupConf_& other) { CopyFrom(other); }
ConstLinearWarmupConf::_LinearWarmupConf_::_LinearWarmupConf_(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf) {
  InitFromProto(proto_linearwarmupconf);
}
ConstLinearWarmupConf::_LinearWarmupConf_::_LinearWarmupConf_(_LinearWarmupConf_&& other) = default;
ConstLinearWarmupConf::_LinearWarmupConf_::~_LinearWarmupConf_() = default;

void ConstLinearWarmupConf::_LinearWarmupConf_::InitFromProto(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf) {
  Clear();
  // required_or_optional field: warmup_batches
  if (proto_linearwarmupconf.has_warmup_batches()) {
    set_warmup_batches(proto_linearwarmupconf.warmup_batches());
  }
  // required_or_optional field: start_multiplier
  if (proto_linearwarmupconf.has_start_multiplier()) {
    set_start_multiplier(proto_linearwarmupconf.start_multiplier());
  }
    
}

void ConstLinearWarmupConf::_LinearWarmupConf_::ToProto(::oneflow::LinearWarmupConf* proto_linearwarmupconf) const {
  proto_linearwarmupconf->Clear();
  // required_or_optional field: warmup_batches
  if (this->has_warmup_batches()) {
    proto_linearwarmupconf->set_warmup_batches(warmup_batches());
    }
  // required_or_optional field: start_multiplier
  if (this->has_start_multiplier()) {
    proto_linearwarmupconf->set_start_multiplier(start_multiplier());
    }

}

::std::string ConstLinearWarmupConf::_LinearWarmupConf_::DebugString() const {
  ::oneflow::LinearWarmupConf proto_linearwarmupconf;
  this->ToProto(&proto_linearwarmupconf);
  return proto_linearwarmupconf.DebugString();
}

void ConstLinearWarmupConf::_LinearWarmupConf_::Clear() {
  clear_warmup_batches();
  clear_start_multiplier();
}

void ConstLinearWarmupConf::_LinearWarmupConf_::CopyFrom(const _LinearWarmupConf_& other) {
  if (other.has_warmup_batches()) {
    set_warmup_batches(other.warmup_batches());
  } else {
    clear_warmup_batches();
  }
  if (other.has_start_multiplier()) {
    set_start_multiplier(other.start_multiplier());
  } else {
    clear_start_multiplier();
  }
}


// optional field warmup_batches
bool ConstLinearWarmupConf::_LinearWarmupConf_::has_warmup_batches() const {
  return has_warmup_batches_;
}
const int64_t& ConstLinearWarmupConf::_LinearWarmupConf_::warmup_batches() const {
  if (has_warmup_batches_) { return warmup_batches_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstLinearWarmupConf::_LinearWarmupConf_::clear_warmup_batches() {
  has_warmup_batches_ = false;
}
void ConstLinearWarmupConf::_LinearWarmupConf_::set_warmup_batches(const int64_t& value) {
  warmup_batches_ = value;
  has_warmup_batches_ = true;
}
int64_t* ConstLinearWarmupConf::_LinearWarmupConf_::mutable_warmup_batches() {
  has_warmup_batches_ = true;
  return &warmup_batches_;
}

// optional field start_multiplier
bool ConstLinearWarmupConf::_LinearWarmupConf_::has_start_multiplier() const {
  return has_start_multiplier_;
}
const double& ConstLinearWarmupConf::_LinearWarmupConf_::start_multiplier() const {
  if (has_start_multiplier_) { return start_multiplier_; }
  static const double default_static_value = double();
  return default_static_value;
}
void ConstLinearWarmupConf::_LinearWarmupConf_::clear_start_multiplier() {
  has_start_multiplier_ = false;
}
void ConstLinearWarmupConf::_LinearWarmupConf_::set_start_multiplier(const double& value) {
  start_multiplier_ = value;
  has_start_multiplier_ = true;
}
double* ConstLinearWarmupConf::_LinearWarmupConf_::mutable_start_multiplier() {
  has_start_multiplier_ = true;
  return &start_multiplier_;
}


int ConstLinearWarmupConf::_LinearWarmupConf_::compare(const _LinearWarmupConf_& other) {
  if (!(has_warmup_batches() == other.has_warmup_batches())) {
    return has_warmup_batches() < other.has_warmup_batches() ? -1 : 1;
  } else if (!(warmup_batches() == other.warmup_batches())) {
    return warmup_batches() < other.warmup_batches() ? -1 : 1;
  }
  if (!(has_start_multiplier() == other.has_start_multiplier())) {
    return has_start_multiplier() < other.has_start_multiplier() ? -1 : 1;
  } else if (!(start_multiplier() == other.start_multiplier())) {
    return start_multiplier() < other.start_multiplier() ? -1 : 1;
  }
  return 0;
}

bool ConstLinearWarmupConf::_LinearWarmupConf_::operator==(const _LinearWarmupConf_& other) const {
  return true
    && has_warmup_batches() == other.has_warmup_batches() 
    && warmup_batches() == other.warmup_batches()
    && has_start_multiplier() == other.has_start_multiplier() 
    && start_multiplier() == other.start_multiplier()
  ;
}

std::size_t ConstLinearWarmupConf::_LinearWarmupConf_::__CalcHash__() const {
  return 0
    ^ (has_warmup_batches() ? std::hash<int64_t>()(warmup_batches()) : 0)
    ^ (has_start_multiplier() ? std::hash<double>()(start_multiplier()) : 0)
  ;
}

bool ConstLinearWarmupConf::_LinearWarmupConf_::operator<(const _LinearWarmupConf_& other) const {
  return false
    || !(has_warmup_batches() == other.has_warmup_batches()) ? 
      has_warmup_batches() < other.has_warmup_batches() : false
    || !(warmup_batches() == other.warmup_batches()) ? 
      warmup_batches() < other.warmup_batches() : false
    || !(has_start_multiplier() == other.has_start_multiplier()) ? 
      has_start_multiplier() < other.has_start_multiplier() : false
    || !(start_multiplier() == other.start_multiplier()) ? 
      start_multiplier() < other.start_multiplier() : false
  ;
}

using _LinearWarmupConf_ =  ConstLinearWarmupConf::_LinearWarmupConf_;
ConstLinearWarmupConf::ConstLinearWarmupConf(const ::std::shared_ptr<_LinearWarmupConf_>& data): data_(data) {}
ConstLinearWarmupConf::ConstLinearWarmupConf(): data_(::std::make_shared<_LinearWarmupConf_>()) {}
ConstLinearWarmupConf::ConstLinearWarmupConf(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf) {
  BuildFromProto(proto_linearwarmupconf);
}
ConstLinearWarmupConf::ConstLinearWarmupConf(const ConstLinearWarmupConf&) = default;
ConstLinearWarmupConf::ConstLinearWarmupConf(ConstLinearWarmupConf&&) noexcept = default;
ConstLinearWarmupConf::~ConstLinearWarmupConf() = default;

void ConstLinearWarmupConf::ToProto(PbMessage* proto_linearwarmupconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LinearWarmupConf*>(proto_linearwarmupconf));
}
  
::std::string ConstLinearWarmupConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLinearWarmupConf::__Empty__() const {
  return !data_;
}

int ConstLinearWarmupConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"warmup_batches", 1},
    {"start_multiplier", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLinearWarmupConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLinearWarmupConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLinearWarmupConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &warmup_batches();
    case 2: return &start_multiplier();
    default: return nullptr;
  }
}

// required or optional field warmup_batches
bool ConstLinearWarmupConf::has_warmup_batches() const {
  return __SharedPtrOrDefault__()->has_warmup_batches();
}
const int64_t& ConstLinearWarmupConf::warmup_batches() const {
  return __SharedPtrOrDefault__()->warmup_batches();
}
// used by pybind11 only
// required or optional field start_multiplier
bool ConstLinearWarmupConf::has_start_multiplier() const {
  return __SharedPtrOrDefault__()->has_start_multiplier();
}
const double& ConstLinearWarmupConf::start_multiplier() const {
  return __SharedPtrOrDefault__()->start_multiplier();
}
// used by pybind11 only

::std::shared_ptr<ConstLinearWarmupConf> ConstLinearWarmupConf::__SharedConst__() const {
  return ::std::make_shared<ConstLinearWarmupConf>(data_);
}
int64_t ConstLinearWarmupConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLinearWarmupConf::operator==(const ConstLinearWarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLinearWarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLinearWarmupConf::operator<(const ConstLinearWarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LinearWarmupConf_>& ConstLinearWarmupConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LinearWarmupConf_> default_ptr = std::make_shared<_LinearWarmupConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LinearWarmupConf_>& ConstLinearWarmupConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LinearWarmupConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLinearWarmupConf
void ConstLinearWarmupConf::BuildFromProto(const PbMessage& proto_linearwarmupconf) {
  data_ = ::std::make_shared<_LinearWarmupConf_>(dynamic_cast<const ::oneflow::LinearWarmupConf&>(proto_linearwarmupconf));
}

LinearWarmupConf::LinearWarmupConf(const ::std::shared_ptr<_LinearWarmupConf_>& data)
  : ConstLinearWarmupConf(data) {}
LinearWarmupConf::LinearWarmupConf(const LinearWarmupConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LinearWarmupConf> resize
LinearWarmupConf::LinearWarmupConf(LinearWarmupConf&&) noexcept = default; 
LinearWarmupConf::LinearWarmupConf(const ::oneflow::LinearWarmupConf& proto_linearwarmupconf) {
  InitFromProto(proto_linearwarmupconf);
}
LinearWarmupConf::LinearWarmupConf() = default;

LinearWarmupConf::~LinearWarmupConf() = default;

void LinearWarmupConf::InitFromProto(const PbMessage& proto_linearwarmupconf) {
  BuildFromProto(proto_linearwarmupconf);
}
  
void* LinearWarmupConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_warmup_batches();
    case 2: return mutable_start_multiplier();
    default: return nullptr;
  }
}

bool LinearWarmupConf::operator==(const LinearWarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LinearWarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LinearWarmupConf::operator<(const LinearWarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LinearWarmupConf::Clear() {
  if (data_) { data_.reset(); }
}
void LinearWarmupConf::CopyFrom(const LinearWarmupConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LinearWarmupConf& LinearWarmupConf::operator=(const LinearWarmupConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field warmup_batches
void LinearWarmupConf::clear_warmup_batches() {
  return __SharedPtr__()->clear_warmup_batches();
}
void LinearWarmupConf::set_warmup_batches(const int64_t& value) {
  return __SharedPtr__()->set_warmup_batches(value);
}
int64_t* LinearWarmupConf::mutable_warmup_batches() {
  return  __SharedPtr__()->mutable_warmup_batches();
}
// required or optional field start_multiplier
void LinearWarmupConf::clear_start_multiplier() {
  return __SharedPtr__()->clear_start_multiplier();
}
void LinearWarmupConf::set_start_multiplier(const double& value) {
  return __SharedPtr__()->set_start_multiplier(value);
}
double* LinearWarmupConf::mutable_start_multiplier() {
  return  __SharedPtr__()->mutable_start_multiplier();
}

::std::shared_ptr<LinearWarmupConf> LinearWarmupConf::__SharedMutable__() {
  return ::std::make_shared<LinearWarmupConf>(__SharedPtr__());
}
ConstWarmupConf::_WarmupConf_::_WarmupConf_() { Clear(); }
ConstWarmupConf::_WarmupConf_::_WarmupConf_(const _WarmupConf_& other) { CopyFrom(other); }
ConstWarmupConf::_WarmupConf_::_WarmupConf_(const ::oneflow::WarmupConf& proto_warmupconf) {
  InitFromProto(proto_warmupconf);
}
ConstWarmupConf::_WarmupConf_::_WarmupConf_(_WarmupConf_&& other) = default;
ConstWarmupConf::_WarmupConf_::~_WarmupConf_() = default;

void ConstWarmupConf::_WarmupConf_::InitFromProto(const ::oneflow::WarmupConf& proto_warmupconf) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_warmupconf.type_case()));
  switch (type_case) {
    case kConstantConf: {
      *mutable_constant_conf() = ::oneflow::cfg::ConstantWarmupConf(proto_warmupconf.constant_conf());
      break;
  }
    case kLinearConf: {
      *mutable_linear_conf() = ::oneflow::cfg::LinearWarmupConf(proto_warmupconf.linear_conf());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstWarmupConf::_WarmupConf_::ToProto(::oneflow::WarmupConf* proto_warmupconf) const {
  proto_warmupconf->Clear();

  // oneof field: type
  ::oneflow::WarmupConf::TypeCase type_case = ::oneflow::WarmupConf::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::WarmupConf::kConstantConf: {
      ::oneflow::ConstantWarmupConf of_proto_constant_conf;
      constant_conf().ToProto(&of_proto_constant_conf);
      proto_warmupconf->mutable_constant_conf()->CopyFrom(of_proto_constant_conf);
      break;
    }
    case ::oneflow::WarmupConf::kLinearConf: {
      ::oneflow::LinearWarmupConf of_proto_linear_conf;
      linear_conf().ToProto(&of_proto_linear_conf);
      proto_warmupconf->mutable_linear_conf()->CopyFrom(of_proto_linear_conf);
      break;
    }
    case ::oneflow::WarmupConf::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstWarmupConf::_WarmupConf_::DebugString() const {
  ::oneflow::WarmupConf proto_warmupconf;
  this->ToProto(&proto_warmupconf);
  return proto_warmupconf.DebugString();
}

void ConstWarmupConf::_WarmupConf_::Clear() {
  clear_type();
}

void ConstWarmupConf::_WarmupConf_::CopyFrom(const _WarmupConf_& other) {
  type_copy_from(other);
}


// oneof field type: constant_conf
bool ConstWarmupConf::_WarmupConf_::has_constant_conf() const {
  return type_case() == kConstantConf;
}
void ConstWarmupConf::_WarmupConf_::clear_constant_conf() {
  if (has_constant_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ConstantWarmupConf& ConstWarmupConf::_WarmupConf_::constant_conf() const {
  if (has_constant_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>*>(&(type_.constant_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf> default_static_value = ::std::make_shared<::oneflow::cfg::ConstantWarmupConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ConstantWarmupConf* ConstWarmupConf::_WarmupConf_::mutable_constant_conf() {
  if(!has_constant_conf()) {
    clear_type();
    new (&(type_.constant_conf_)) ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>(new ::oneflow::cfg::ConstantWarmupConf());
  }
  type_case_ = kConstantConf;
  ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>*>(&(type_.constant_conf_));
  return  (*ptr).get();
}

// oneof field type: linear_conf
bool ConstWarmupConf::_WarmupConf_::has_linear_conf() const {
  return type_case() == kLinearConf;
}
void ConstWarmupConf::_WarmupConf_::clear_linear_conf() {
  if (has_linear_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.linear_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::LinearWarmupConf& ConstWarmupConf::_WarmupConf_::linear_conf() const {
  if (has_linear_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>*>(&(type_.linear_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf> default_static_value = ::std::make_shared<::oneflow::cfg::LinearWarmupConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LinearWarmupConf* ConstWarmupConf::_WarmupConf_::mutable_linear_conf() {
  if(!has_linear_conf()) {
    clear_type();
    new (&(type_.linear_conf_)) ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>(new ::oneflow::cfg::LinearWarmupConf());
  }
  type_case_ = kLinearConf;
  ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>*>(&(type_.linear_conf_));
  return  (*ptr).get();
}
ConstWarmupConf::TypeCase ConstWarmupConf::_WarmupConf_::type_case() const {
  return type_case_;
}
bool ConstWarmupConf::_WarmupConf_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstWarmupConf::_WarmupConf_::clear_type() {
  switch (type_case()) {
    case kConstantConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLinearConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LinearWarmupConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.linear_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstWarmupConf::_WarmupConf_::type_copy_from(const _WarmupConf_& other) {
  switch (other.type_case()) {
    case kConstantConf: {
      mutable_constant_conf()->CopyFrom(other.constant_conf());
      break;
    }
    case kLinearConf: {
      mutable_linear_conf()->CopyFrom(other.linear_conf());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstWarmupConf::_WarmupConf_::compare(const _WarmupConf_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kConstantConf: {
      if (!(constant_conf() == other.constant_conf())) {
        return constant_conf() < other.constant_conf() ? -1 : 1;
      }
      break;
    }
    case kLinearConf: {
      if (!(linear_conf() == other.linear_conf())) {
        return linear_conf() < other.linear_conf() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstWarmupConf::_WarmupConf_::operator==(const _WarmupConf_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kConstantConf ? 
      constant_conf() == other.constant_conf() : true)
    && (type_case() == kLinearConf ? 
      linear_conf() == other.linear_conf() : true)
  ;
}

std::size_t ConstWarmupConf::_WarmupConf_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_constant_conf() ? std::hash<::oneflow::cfg::ConstantWarmupConf>()(constant_conf()) : 0)
    ^ (has_linear_conf() ? std::hash<::oneflow::cfg::LinearWarmupConf>()(linear_conf()) : 0)
  ;
}

bool ConstWarmupConf::_WarmupConf_::operator<(const _WarmupConf_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kConstantConf) && 
      !(constant_conf() == other.constant_conf())) ? 
        constant_conf() < other.constant_conf() : false
    || ((type_case() == kLinearConf) && 
      !(linear_conf() == other.linear_conf())) ? 
        linear_conf() < other.linear_conf() : false
  ;
}

using _WarmupConf_ =  ConstWarmupConf::_WarmupConf_;
ConstWarmupConf::ConstWarmupConf(const ::std::shared_ptr<_WarmupConf_>& data): data_(data) {}
ConstWarmupConf::ConstWarmupConf(): data_(::std::make_shared<_WarmupConf_>()) {}
ConstWarmupConf::ConstWarmupConf(const ::oneflow::WarmupConf& proto_warmupconf) {
  BuildFromProto(proto_warmupconf);
}
ConstWarmupConf::ConstWarmupConf(const ConstWarmupConf&) = default;
ConstWarmupConf::ConstWarmupConf(ConstWarmupConf&&) noexcept = default;
ConstWarmupConf::~ConstWarmupConf() = default;

void ConstWarmupConf::ToProto(PbMessage* proto_warmupconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::WarmupConf*>(proto_warmupconf));
}
  
::std::string ConstWarmupConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstWarmupConf::__Empty__() const {
  return !data_;
}

int ConstWarmupConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"constant_conf", 3000},
    {"linear_conf", 3001},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstWarmupConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 3000:
    case 3001:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstWarmupConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 3000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ConstantWarmupConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstConstantWarmupConf),
      };
      return type_indices;
    }
    case 3001: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LinearWarmupConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLinearWarmupConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstWarmupConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 3000: return &constant_conf();
    case 3001: return &linear_conf();
    default: return nullptr;
  }
}

 // oneof field type: constant_conf
bool ConstWarmupConf::has_constant_conf() const {
  return __SharedPtrOrDefault__()->has_constant_conf();
}
const ::oneflow::cfg::ConstantWarmupConf& ConstWarmupConf::constant_conf() const {
  return __SharedPtrOrDefault__()->constant_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstConstantWarmupConf> ConstWarmupConf::shared_const_constant_conf() const {
  return constant_conf().__SharedConst__();
}
 // oneof field type: linear_conf
bool ConstWarmupConf::has_linear_conf() const {
  return __SharedPtrOrDefault__()->has_linear_conf();
}
const ::oneflow::cfg::LinearWarmupConf& ConstWarmupConf::linear_conf() const {
  return __SharedPtrOrDefault__()->linear_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLinearWarmupConf> ConstWarmupConf::shared_const_linear_conf() const {
  return linear_conf().__SharedConst__();
}
ConstWarmupConf::TypeCase ConstWarmupConf::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstWarmupConf::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstWarmupConf> ConstWarmupConf::__SharedConst__() const {
  return ::std::make_shared<ConstWarmupConf>(data_);
}
int64_t ConstWarmupConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstWarmupConf::operator==(const ConstWarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstWarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstWarmupConf::operator<(const ConstWarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_WarmupConf_>& ConstWarmupConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_WarmupConf_> default_ptr = std::make_shared<_WarmupConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_WarmupConf_>& ConstWarmupConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _WarmupConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstWarmupConf
void ConstWarmupConf::BuildFromProto(const PbMessage& proto_warmupconf) {
  data_ = ::std::make_shared<_WarmupConf_>(dynamic_cast<const ::oneflow::WarmupConf&>(proto_warmupconf));
}

WarmupConf::WarmupConf(const ::std::shared_ptr<_WarmupConf_>& data)
  : ConstWarmupConf(data) {}
WarmupConf::WarmupConf(const WarmupConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<WarmupConf> resize
WarmupConf::WarmupConf(WarmupConf&&) noexcept = default; 
WarmupConf::WarmupConf(const ::oneflow::WarmupConf& proto_warmupconf) {
  InitFromProto(proto_warmupconf);
}
WarmupConf::WarmupConf() = default;

WarmupConf::~WarmupConf() = default;

void WarmupConf::InitFromProto(const PbMessage& proto_warmupconf) {
  BuildFromProto(proto_warmupconf);
}
  
void* WarmupConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 3000: return mutable_constant_conf();
    case 3001: return mutable_linear_conf();
    default: return nullptr;
  }
}

bool WarmupConf::operator==(const WarmupConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t WarmupConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool WarmupConf::operator<(const WarmupConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void WarmupConf::Clear() {
  if (data_) { data_.reset(); }
}
void WarmupConf::CopyFrom(const WarmupConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
WarmupConf& WarmupConf::operator=(const WarmupConf& other) {
  CopyFrom(other);
  return *this;
}

void WarmupConf::clear_constant_conf() {
  return __SharedPtr__()->clear_constant_conf();
}
::oneflow::cfg::ConstantWarmupConf* WarmupConf::mutable_constant_conf() {
  return __SharedPtr__()->mutable_constant_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstantWarmupConf> WarmupConf::shared_mutable_constant_conf() {
  return mutable_constant_conf()->__SharedMutable__();
}
void WarmupConf::clear_linear_conf() {
  return __SharedPtr__()->clear_linear_conf();
}
::oneflow::cfg::LinearWarmupConf* WarmupConf::mutable_linear_conf() {
  return __SharedPtr__()->mutable_linear_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LinearWarmupConf> WarmupConf::shared_mutable_linear_conf() {
  return mutable_linear_conf()->__SharedMutable__();
}

::std::shared_ptr<WarmupConf> WarmupConf::__SharedMutable__() {
  return ::std::make_shared<WarmupConf>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): ::oneflow::cfg::_RepeatedField_<int64_t>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_() = default;
Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::~Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_(data) {}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_() = default;
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::~_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_() = default;

void _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::operator==(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::operator<(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_> _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data): ::oneflow::cfg::_RepeatedField_<double>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_() = default;
Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::~Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<double>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data): Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_(data) {}
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_() = default;
_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::~_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_() = default;

void _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) {
  ::oneflow::cfg::_RepeatedField_<double>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) {
  ::oneflow::cfg::_RepeatedField_<double>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::operator==(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<double>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::operator<(const _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_> _CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_LEARNING_RATE_SCHEDULE_CONF_CFG_H__RepeatedField_double_>(__SharedPtr__());
}

}
} // namespace oneflow
