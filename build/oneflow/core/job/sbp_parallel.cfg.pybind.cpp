#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.sbp_parallel", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstSbpParallel>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstSbpParallel>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstSbpParallel> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::*)(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<SbpParallel>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<SbpParallel>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::SbpParallel> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstSbpParallel> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstSbpParallel> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(const ::oneflow::cfg::SbpParallel&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::SbpParallel> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::SbpParallel> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstParallelDistribution>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstParallelDistribution>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstParallelDistribution> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::*)(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ParallelDistribution>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ParallelDistribution>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::ParallelDistribution> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstSbpSignature> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstSbpSignature> (Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(const ::oneflow::cfg::SbpSignature&))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::SbpSignature> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::SbpSignature> (_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstSplitParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstSplitParallel>> registry(m, "ConstSplitParallel");
    registry.def("__id__", &::oneflow::cfg::SplitParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSplitParallel::DebugString);
    registry.def("__repr__", &ConstSplitParallel::DebugString);

    registry.def("has_axis", &ConstSplitParallel::has_axis);
    registry.def("axis", &ConstSplitParallel::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::SplitParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SplitParallel>> registry(m, "SplitParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SplitParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SplitParallel::*)(const ConstSplitParallel&))&::oneflow::cfg::SplitParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SplitParallel::*)(const ::oneflow::cfg::SplitParallel&))&::oneflow::cfg::SplitParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SplitParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SplitParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SplitParallel::DebugString);



    registry.def("has_axis", &::oneflow::cfg::SplitParallel::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::SplitParallel::clear_axis);
    registry.def("axis", &::oneflow::cfg::SplitParallel::axis);
    registry.def("set_axis", &::oneflow::cfg::SplitParallel::set_axis);
  }
  {
    pybind11::class_<ConstBroadcastParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstBroadcastParallel>> registry(m, "ConstBroadcastParallel");
    registry.def("__id__", &::oneflow::cfg::BroadcastParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBroadcastParallel::DebugString);
    registry.def("__repr__", &ConstBroadcastParallel::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::BroadcastParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BroadcastParallel>> registry(m, "BroadcastParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BroadcastParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BroadcastParallel::*)(const ConstBroadcastParallel&))&::oneflow::cfg::BroadcastParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BroadcastParallel::*)(const ::oneflow::cfg::BroadcastParallel&))&::oneflow::cfg::BroadcastParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BroadcastParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BroadcastParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BroadcastParallel::DebugString);


  }
  {
    pybind11::class_<ConstPartialSumParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstPartialSumParallel>> registry(m, "ConstPartialSumParallel");
    registry.def("__id__", &::oneflow::cfg::PartialSumParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPartialSumParallel::DebugString);
    registry.def("__repr__", &ConstPartialSumParallel::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::PartialSumParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PartialSumParallel>> registry(m, "PartialSumParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PartialSumParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PartialSumParallel::*)(const ConstPartialSumParallel&))&::oneflow::cfg::PartialSumParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PartialSumParallel::*)(const ::oneflow::cfg::PartialSumParallel&))&::oneflow::cfg::PartialSumParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PartialSumParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PartialSumParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PartialSumParallel::DebugString);


  }
  {
    pybind11::class_<ConstSbpParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstSbpParallel>> registry(m, "ConstSbpParallel");
    registry.def("__id__", &::oneflow::cfg::SbpParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSbpParallel::DebugString);
    registry.def("__repr__", &ConstSbpParallel::DebugString);

    registry.def("has_split_parallel", &ConstSbpParallel::has_split_parallel);
    registry.def("split_parallel", &ConstSbpParallel::shared_const_split_parallel);

    registry.def("has_broadcast_parallel", &ConstSbpParallel::has_broadcast_parallel);
    registry.def("broadcast_parallel", &ConstSbpParallel::shared_const_broadcast_parallel);

    registry.def("has_partial_sum_parallel", &ConstSbpParallel::has_partial_sum_parallel);
    registry.def("partial_sum_parallel", &ConstSbpParallel::shared_const_partial_sum_parallel);
    registry.def("parallel_type_case",  &ConstSbpParallel::parallel_type_case);
    registry.def_property_readonly_static("PARALLEL_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::PARALLEL_TYPE_NOT_SET; })
        .def_property_readonly_static("kSplitParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kSplitParallel; })
        .def_property_readonly_static("kBroadcastParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kBroadcastParallel; })
        .def_property_readonly_static("kPartialSumParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kPartialSumParallel; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::SbpParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SbpParallel>> registry(m, "SbpParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SbpParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpParallel::*)(const ConstSbpParallel&))&::oneflow::cfg::SbpParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpParallel::*)(const ::oneflow::cfg::SbpParallel&))&::oneflow::cfg::SbpParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SbpParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SbpParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SbpParallel::DebugString);

    registry.def_property_readonly_static("PARALLEL_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::PARALLEL_TYPE_NOT_SET; })
        .def_property_readonly_static("kSplitParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kSplitParallel; })
        .def_property_readonly_static("kBroadcastParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kBroadcastParallel; })
        .def_property_readonly_static("kPartialSumParallel", [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kPartialSumParallel; })
        ;


    registry.def("has_split_parallel", &::oneflow::cfg::SbpParallel::has_split_parallel);
    registry.def("clear_split_parallel", &::oneflow::cfg::SbpParallel::clear_split_parallel);
    registry.def_property_readonly_static("kSplitParallel",
        [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kSplitParallel; });
    registry.def("split_parallel", &::oneflow::cfg::SbpParallel::split_parallel);
    registry.def("mutable_split_parallel", &::oneflow::cfg::SbpParallel::shared_mutable_split_parallel);

    registry.def("has_broadcast_parallel", &::oneflow::cfg::SbpParallel::has_broadcast_parallel);
    registry.def("clear_broadcast_parallel", &::oneflow::cfg::SbpParallel::clear_broadcast_parallel);
    registry.def_property_readonly_static("kBroadcastParallel",
        [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kBroadcastParallel; });
    registry.def("broadcast_parallel", &::oneflow::cfg::SbpParallel::broadcast_parallel);
    registry.def("mutable_broadcast_parallel", &::oneflow::cfg::SbpParallel::shared_mutable_broadcast_parallel);

    registry.def("has_partial_sum_parallel", &::oneflow::cfg::SbpParallel::has_partial_sum_parallel);
    registry.def("clear_partial_sum_parallel", &::oneflow::cfg::SbpParallel::clear_partial_sum_parallel);
    registry.def_property_readonly_static("kPartialSumParallel",
        [](const pybind11::object&){ return ::oneflow::cfg::SbpParallel::kPartialSumParallel; });
    registry.def("partial_sum_parallel", &::oneflow::cfg::SbpParallel::partial_sum_parallel);
    registry.def("mutable_partial_sum_parallel", &::oneflow::cfg::SbpParallel::shared_mutable_partial_sum_parallel);
    pybind11::enum_<::oneflow::cfg::SbpParallel::ParallelTypeCase>(registry, "ParallelTypeCase")
        .value("PARALLEL_TYPE_NOT_SET", ::oneflow::cfg::SbpParallel::PARALLEL_TYPE_NOT_SET)
        .value("kSplitParallel", ::oneflow::cfg::SbpParallel::kSplitParallel)
        .value("kBroadcastParallel", ::oneflow::cfg::SbpParallel::kBroadcastParallel)
        .value("kPartialSumParallel", ::oneflow::cfg::SbpParallel::kPartialSumParallel)
        ;
    registry.def("parallel_type_case",  &::oneflow::cfg::SbpParallel::parallel_type_case);
  }
  {
    pybind11::class_<ConstSbpSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstSbpSignature>> registry(m, "ConstSbpSignature");
    registry.def("__id__", &::oneflow::cfg::SbpSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSbpSignature::DebugString);
    registry.def("__repr__", &ConstSbpSignature::DebugString);

    registry.def("bn_in_op2sbp_parallel_size", &ConstSbpSignature::bn_in_op2sbp_parallel_size);
    registry.def("bn_in_op2sbp_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> (ConstSbpSignature::*)() const)&ConstSbpSignature::shared_const_bn_in_op2sbp_parallel);

    registry.def("bn_in_op2sbp_parallel", (::std::shared_ptr<ConstSbpParallel> (ConstSbpSignature::*)(const ::std::string&) const)&ConstSbpSignature::shared_const_bn_in_op2sbp_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::SbpSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SbpSignature>> registry(m, "SbpSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SbpSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpSignature::*)(const ConstSbpSignature&))&::oneflow::cfg::SbpSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpSignature::*)(const ::oneflow::cfg::SbpSignature&))&::oneflow::cfg::SbpSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SbpSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SbpSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SbpSignature::DebugString);



    registry.def("bn_in_op2sbp_parallel_size", &::oneflow::cfg::SbpSignature::bn_in_op2sbp_parallel_size);
    registry.def("bn_in_op2sbp_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> (::oneflow::cfg::SbpSignature::*)() const)&::oneflow::cfg::SbpSignature::shared_const_bn_in_op2sbp_parallel);
    registry.def("clear_bn_in_op2sbp_parallel", &::oneflow::cfg::SbpSignature::clear_bn_in_op2sbp_parallel);
    registry.def("mutable_bn_in_op2sbp_parallel", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> (::oneflow::cfg::SbpSignature::*)())&::oneflow::cfg::SbpSignature::shared_mutable_bn_in_op2sbp_parallel);
    registry.def("bn_in_op2sbp_parallel", (::std::shared_ptr<ConstSbpParallel> (::oneflow::cfg::SbpSignature::*)(const ::std::string&) const)&::oneflow::cfg::SbpSignature::shared_const_bn_in_op2sbp_parallel);
  }
  {
    pybind11::class_<ConstParallelDistribution, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelDistribution>> registry(m, "ConstParallelDistribution");
    registry.def("__id__", &::oneflow::cfg::ParallelDistribution::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelDistribution::DebugString);
    registry.def("__repr__", &ConstParallelDistribution::DebugString);

    registry.def("sbp_parallel_size", &ConstParallelDistribution::sbp_parallel_size);
    registry.def("sbp_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> (ConstParallelDistribution::*)() const)&ConstParallelDistribution::shared_const_sbp_parallel);
    registry.def("sbp_parallel", (::std::shared_ptr<ConstSbpParallel> (ConstParallelDistribution::*)(::std::size_t) const)&ConstParallelDistribution::shared_const_sbp_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelDistribution, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelDistribution>> registry(m, "ParallelDistribution");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelDistribution::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelDistribution::*)(const ConstParallelDistribution&))&::oneflow::cfg::ParallelDistribution::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelDistribution::*)(const ::oneflow::cfg::ParallelDistribution&))&::oneflow::cfg::ParallelDistribution::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelDistribution::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelDistribution::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelDistribution::DebugString);



    registry.def("sbp_parallel_size", &::oneflow::cfg::ParallelDistribution::sbp_parallel_size);
    registry.def("clear_sbp_parallel", &::oneflow::cfg::ParallelDistribution::clear_sbp_parallel);
    registry.def("mutable_sbp_parallel", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> (::oneflow::cfg::ParallelDistribution::*)())&::oneflow::cfg::ParallelDistribution::shared_mutable_sbp_parallel);
    registry.def("sbp_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> (::oneflow::cfg::ParallelDistribution::*)() const)&::oneflow::cfg::ParallelDistribution::shared_const_sbp_parallel);
    registry.def("sbp_parallel", (::std::shared_ptr<ConstSbpParallel> (::oneflow::cfg::ParallelDistribution::*)(::std::size_t) const)&::oneflow::cfg::ParallelDistribution::shared_const_sbp_parallel);
    registry.def("mutable_sbp_parallel", (::std::shared_ptr<::oneflow::cfg::SbpParallel> (::oneflow::cfg::ParallelDistribution::*)(::std::size_t))&::oneflow::cfg::ParallelDistribution::shared_mutable_sbp_parallel);
  }
  {
    pybind11::class_<ConstParallelDistributionSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelDistributionSignature>> registry(m, "ConstParallelDistributionSignature");
    registry.def("__id__", &::oneflow::cfg::ParallelDistributionSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelDistributionSignature::DebugString);
    registry.def("__repr__", &ConstParallelDistributionSignature::DebugString);

    registry.def("bn_in_op2parallel_distribution_size", &ConstParallelDistributionSignature::bn_in_op2parallel_distribution_size);
    registry.def("bn_in_op2parallel_distribution", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> (ConstParallelDistributionSignature::*)() const)&ConstParallelDistributionSignature::shared_const_bn_in_op2parallel_distribution);

    registry.def("bn_in_op2parallel_distribution", (::std::shared_ptr<ConstParallelDistribution> (ConstParallelDistributionSignature::*)(const ::std::string&) const)&ConstParallelDistributionSignature::shared_const_bn_in_op2parallel_distribution);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelDistributionSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelDistributionSignature>> registry(m, "ParallelDistributionSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelDistributionSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelDistributionSignature::*)(const ConstParallelDistributionSignature&))&::oneflow::cfg::ParallelDistributionSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelDistributionSignature::*)(const ::oneflow::cfg::ParallelDistributionSignature&))&::oneflow::cfg::ParallelDistributionSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelDistributionSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelDistributionSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelDistributionSignature::DebugString);



    registry.def("bn_in_op2parallel_distribution_size", &::oneflow::cfg::ParallelDistributionSignature::bn_in_op2parallel_distribution_size);
    registry.def("bn_in_op2parallel_distribution", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> (::oneflow::cfg::ParallelDistributionSignature::*)() const)&::oneflow::cfg::ParallelDistributionSignature::shared_const_bn_in_op2parallel_distribution);
    registry.def("clear_bn_in_op2parallel_distribution", &::oneflow::cfg::ParallelDistributionSignature::clear_bn_in_op2parallel_distribution);
    registry.def("mutable_bn_in_op2parallel_distribution", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> (::oneflow::cfg::ParallelDistributionSignature::*)())&::oneflow::cfg::ParallelDistributionSignature::shared_mutable_bn_in_op2parallel_distribution);
    registry.def("bn_in_op2parallel_distribution", (::std::shared_ptr<ConstParallelDistribution> (::oneflow::cfg::ParallelDistributionSignature::*)(const ::std::string&) const)&::oneflow::cfg::ParallelDistributionSignature::shared_const_bn_in_op2parallel_distribution);
  }
  {
    pybind11::class_<ConstSbpSignatureList, ::oneflow::cfg::Message, std::shared_ptr<ConstSbpSignatureList>> registry(m, "ConstSbpSignatureList");
    registry.def("__id__", &::oneflow::cfg::SbpSignatureList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSbpSignatureList::DebugString);
    registry.def("__repr__", &ConstSbpSignatureList::DebugString);

    registry.def("sbp_signature_size", &ConstSbpSignatureList::sbp_signature_size);
    registry.def("sbp_signature", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> (ConstSbpSignatureList::*)() const)&ConstSbpSignatureList::shared_const_sbp_signature);
    registry.def("sbp_signature", (::std::shared_ptr<ConstSbpSignature> (ConstSbpSignatureList::*)(::std::size_t) const)&ConstSbpSignatureList::shared_const_sbp_signature);
  }
  {
    pybind11::class_<::oneflow::cfg::SbpSignatureList, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SbpSignatureList>> registry(m, "SbpSignatureList");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SbpSignatureList::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpSignatureList::*)(const ConstSbpSignatureList&))&::oneflow::cfg::SbpSignatureList::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SbpSignatureList::*)(const ::oneflow::cfg::SbpSignatureList&))&::oneflow::cfg::SbpSignatureList::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SbpSignatureList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SbpSignatureList::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SbpSignatureList::DebugString);



    registry.def("sbp_signature_size", &::oneflow::cfg::SbpSignatureList::sbp_signature_size);
    registry.def("clear_sbp_signature", &::oneflow::cfg::SbpSignatureList::clear_sbp_signature);
    registry.def("mutable_sbp_signature", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> (::oneflow::cfg::SbpSignatureList::*)())&::oneflow::cfg::SbpSignatureList::shared_mutable_sbp_signature);
    registry.def("sbp_signature", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> (::oneflow::cfg::SbpSignatureList::*)() const)&::oneflow::cfg::SbpSignatureList::shared_const_sbp_signature);
    registry.def("sbp_signature", (::std::shared_ptr<ConstSbpSignature> (::oneflow::cfg::SbpSignatureList::*)(::std::size_t) const)&::oneflow::cfg::SbpSignatureList::shared_const_sbp_signature);
    registry.def("mutable_sbp_signature", (::std::shared_ptr<::oneflow::cfg::SbpSignature> (::oneflow::cfg::SbpSignatureList::*)(::std::size_t))&::oneflow::cfg::SbpSignatureList::shared_mutable_sbp_signature);
  }
}