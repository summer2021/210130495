#ifndef CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstAttrValue;
class AttrValue;
}
}
namespace oneflow {
namespace cfg {
class ConstBlobDescProto;
class BlobDescProto;
}
}
namespace oneflow {
namespace cfg {
class ConstInitializerConf;
class InitializerConf;
}
}
namespace oneflow {
namespace cfg {
class ConstInterfaceBlobConf;
class InterfaceBlobConf;
}
}
namespace oneflow {
namespace cfg {
class ConstLearningRateDecayConf;
class LearningRateDecayConf;
}
}
namespace oneflow {
namespace cfg {
class ConstLogicalBlobId;
class LogicalBlobId;
}
}
namespace oneflow {
namespace cfg {
class ConstOpNameSet;
class OpNameSet;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelConf;
class ParallelConf;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelDistribution;
class ParallelDistribution;
}
}
namespace oneflow {
namespace cfg {
class ConstWarmupConf;
class WarmupConf;
}
}

namespace oneflow {

// forward declare proto class;
class NaiveModelUpdateConf;
class MomentumModelUpdateConf;
class RMSPropModelUpdateConf;
class LARSModelUpdateConf;
class AdamModelUpdateConf;
class LazyAdamModelUpdateConf;
class LambModelUpdateConf;
class ClipByGlobalNormConf;
class ClipConf;
class WeightDecayFilterPatternSet;
class WeightDecayConf;
class OptimizerConf;
class NormalModelUpdateOpUserConf;
class DynamicLossScalePolicy;
class TrainConf;
class PredictConf;
class MemoryAllocationAlgorithmConf;
class XrtConfig_XlaConfig;
class XrtConfig_TensorRTConfig;
class XrtConfig;
class QatConfig;
class IndexedSlicesOptimizerConf;
class ParallelBlobConf;
class JobInputDef;
class JobOutputDef;
class JobSignatureDef_InputsEntry;
class JobSignatureDef_OutputsEntry;
class JobSignatureDef;
class JobConfigProto_FlagName2flagValueEntry;
class JobConfigProto;

namespace cfg {


class NaiveModelUpdateConf;
class ConstNaiveModelUpdateConf;

class MomentumModelUpdateConf;
class ConstMomentumModelUpdateConf;

class RMSPropModelUpdateConf;
class ConstRMSPropModelUpdateConf;

class LARSModelUpdateConf;
class ConstLARSModelUpdateConf;

class AdamModelUpdateConf;
class ConstAdamModelUpdateConf;

class LazyAdamModelUpdateConf;
class ConstLazyAdamModelUpdateConf;

class LambModelUpdateConf;
class ConstLambModelUpdateConf;

class ClipByGlobalNormConf;
class ConstClipByGlobalNormConf;

class ClipConf;
class ConstClipConf;

class WeightDecayFilterPatternSet;
class ConstWeightDecayFilterPatternSet;

class WeightDecayConf;
class ConstWeightDecayConf;

class OptimizerConf;
class ConstOptimizerConf;

class NormalModelUpdateOpUserConf;
class ConstNormalModelUpdateOpUserConf;

class DynamicLossScalePolicy;
class ConstDynamicLossScalePolicy;

class TrainConf;
class ConstTrainConf;

class PredictConf;
class ConstPredictConf;

class MemoryAllocationAlgorithmConf;
class ConstMemoryAllocationAlgorithmConf;

class XrtConfig_XlaConfig;
class ConstXrtConfig_XlaConfig;

class XrtConfig_TensorRTConfig;
class ConstXrtConfig_TensorRTConfig;

class XrtConfig;
class ConstXrtConfig;

class QatConfig;
class ConstQatConfig;

class IndexedSlicesOptimizerConf;
class ConstIndexedSlicesOptimizerConf;

class ParallelBlobConf;
class ConstParallelBlobConf;

class JobInputDef;
class ConstJobInputDef;

class JobOutputDef;
class ConstJobOutputDef;

class JobSignatureDef;
class ConstJobSignatureDef;

class JobConfigProto;
class ConstJobConfigProto;



class ConstNaiveModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _NaiveModelUpdateConf_ {
   public:
    _NaiveModelUpdateConf_();
    explicit _NaiveModelUpdateConf_(const _NaiveModelUpdateConf_& other);
    explicit _NaiveModelUpdateConf_(_NaiveModelUpdateConf_&& other);
    _NaiveModelUpdateConf_(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf);
    ~_NaiveModelUpdateConf_();

    void InitFromProto(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf);

    void ToProto(::oneflow::NaiveModelUpdateConf* proto_naivemodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _NaiveModelUpdateConf_& other);
       
   public:
    int compare(const _NaiveModelUpdateConf_& other);

    bool operator==(const _NaiveModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _NaiveModelUpdateConf_& other) const;
  };

  ConstNaiveModelUpdateConf(const ::std::shared_ptr<_NaiveModelUpdateConf_>& data);
  ConstNaiveModelUpdateConf(const ConstNaiveModelUpdateConf&);
  ConstNaiveModelUpdateConf(ConstNaiveModelUpdateConf&&) noexcept;
  ConstNaiveModelUpdateConf();
  ConstNaiveModelUpdateConf(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf);
  virtual ~ConstNaiveModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_naivemodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstNaiveModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstNaiveModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstNaiveModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_NaiveModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_NaiveModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstNaiveModelUpdateConf
  void BuildFromProto(const PbMessage& proto_naivemodelupdateconf);
  
  ::std::shared_ptr<_NaiveModelUpdateConf_> data_;
};

class NaiveModelUpdateConf final : public ConstNaiveModelUpdateConf {
 public:
  NaiveModelUpdateConf(const ::std::shared_ptr<_NaiveModelUpdateConf_>& data);
  NaiveModelUpdateConf(const NaiveModelUpdateConf& other);
  // enable nothrow for ::std::vector<NaiveModelUpdateConf> resize 
  NaiveModelUpdateConf(NaiveModelUpdateConf&&) noexcept;
  NaiveModelUpdateConf();
  explicit NaiveModelUpdateConf(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf);

  ~NaiveModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_naivemodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const NaiveModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const NaiveModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const NaiveModelUpdateConf& other);
  NaiveModelUpdateConf& operator=(const NaiveModelUpdateConf& other);


  ::std::shared_ptr<NaiveModelUpdateConf> __SharedMutable__();
};


class ConstMomentumModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _MomentumModelUpdateConf_ {
   public:
    _MomentumModelUpdateConf_();
    explicit _MomentumModelUpdateConf_(const _MomentumModelUpdateConf_& other);
    explicit _MomentumModelUpdateConf_(_MomentumModelUpdateConf_&& other);
    _MomentumModelUpdateConf_(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf);
    ~_MomentumModelUpdateConf_();

    void InitFromProto(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf);

    void ToProto(::oneflow::MomentumModelUpdateConf* proto_momentummodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MomentumModelUpdateConf_& other);
  
      // optional field beta
     public:
    bool has_beta() const;
    const float& beta() const;
    void clear_beta();
    void set_beta(const float& value);
    float* mutable_beta();
   protected:
    bool has_beta_ = false;
    float beta_;
           
   public:
    int compare(const _MomentumModelUpdateConf_& other);

    bool operator==(const _MomentumModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MomentumModelUpdateConf_& other) const;
  };

  ConstMomentumModelUpdateConf(const ::std::shared_ptr<_MomentumModelUpdateConf_>& data);
  ConstMomentumModelUpdateConf(const ConstMomentumModelUpdateConf&);
  ConstMomentumModelUpdateConf(ConstMomentumModelUpdateConf&&) noexcept;
  ConstMomentumModelUpdateConf();
  ConstMomentumModelUpdateConf(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf);
  virtual ~ConstMomentumModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_momentummodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field beta
 public:
  bool has_beta() const;
  const float& beta() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstMomentumModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMomentumModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMomentumModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_MomentumModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MomentumModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMomentumModelUpdateConf
  void BuildFromProto(const PbMessage& proto_momentummodelupdateconf);
  
  ::std::shared_ptr<_MomentumModelUpdateConf_> data_;
};

class MomentumModelUpdateConf final : public ConstMomentumModelUpdateConf {
 public:
  MomentumModelUpdateConf(const ::std::shared_ptr<_MomentumModelUpdateConf_>& data);
  MomentumModelUpdateConf(const MomentumModelUpdateConf& other);
  // enable nothrow for ::std::vector<MomentumModelUpdateConf> resize 
  MomentumModelUpdateConf(MomentumModelUpdateConf&&) noexcept;
  MomentumModelUpdateConf();
  explicit MomentumModelUpdateConf(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf);

  ~MomentumModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_momentummodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MomentumModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MomentumModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const MomentumModelUpdateConf& other);
  MomentumModelUpdateConf& operator=(const MomentumModelUpdateConf& other);

  // required or optional field beta
 public:
  void clear_beta();
  void set_beta(const float& value);
  float* mutable_beta();

  ::std::shared_ptr<MomentumModelUpdateConf> __SharedMutable__();
};


class ConstRMSPropModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _RMSPropModelUpdateConf_ {
   public:
    _RMSPropModelUpdateConf_();
    explicit _RMSPropModelUpdateConf_(const _RMSPropModelUpdateConf_& other);
    explicit _RMSPropModelUpdateConf_(_RMSPropModelUpdateConf_&& other);
    _RMSPropModelUpdateConf_(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf);
    ~_RMSPropModelUpdateConf_();

    void InitFromProto(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf);

    void ToProto(::oneflow::RMSPropModelUpdateConf* proto_rmspropmodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RMSPropModelUpdateConf_& other);
  
      // optional field decay_rate
     public:
    bool has_decay_rate() const;
    const float& decay_rate() const;
    void clear_decay_rate();
    void set_decay_rate(const float& value);
    float* mutable_decay_rate();
   protected:
    bool has_decay_rate_ = false;
    float decay_rate_;
      
      // optional field epsilon
     public:
    bool has_epsilon() const;
    const float& epsilon() const;
    void clear_epsilon();
    void set_epsilon(const float& value);
    float* mutable_epsilon();
   protected:
    bool has_epsilon_ = false;
    float epsilon_;
      
      // optional field centered
     public:
    bool has_centered() const;
    const bool& centered() const;
    void clear_centered();
    void set_centered(const bool& value);
    bool* mutable_centered();
   protected:
    bool has_centered_ = false;
    bool centered_;
           
   public:
    int compare(const _RMSPropModelUpdateConf_& other);

    bool operator==(const _RMSPropModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RMSPropModelUpdateConf_& other) const;
  };

  ConstRMSPropModelUpdateConf(const ::std::shared_ptr<_RMSPropModelUpdateConf_>& data);
  ConstRMSPropModelUpdateConf(const ConstRMSPropModelUpdateConf&);
  ConstRMSPropModelUpdateConf(ConstRMSPropModelUpdateConf&&) noexcept;
  ConstRMSPropModelUpdateConf();
  ConstRMSPropModelUpdateConf(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf);
  virtual ~ConstRMSPropModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_rmspropmodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field decay_rate
 public:
  bool has_decay_rate() const;
  const float& decay_rate() const;
  // used by pybind11 only
  // required or optional field epsilon
 public:
  bool has_epsilon() const;
  const float& epsilon() const;
  // used by pybind11 only
  // required or optional field centered
 public:
  bool has_centered() const;
  const bool& centered() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRMSPropModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRMSPropModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRMSPropModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_RMSPropModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RMSPropModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRMSPropModelUpdateConf
  void BuildFromProto(const PbMessage& proto_rmspropmodelupdateconf);
  
  ::std::shared_ptr<_RMSPropModelUpdateConf_> data_;
};

class RMSPropModelUpdateConf final : public ConstRMSPropModelUpdateConf {
 public:
  RMSPropModelUpdateConf(const ::std::shared_ptr<_RMSPropModelUpdateConf_>& data);
  RMSPropModelUpdateConf(const RMSPropModelUpdateConf& other);
  // enable nothrow for ::std::vector<RMSPropModelUpdateConf> resize 
  RMSPropModelUpdateConf(RMSPropModelUpdateConf&&) noexcept;
  RMSPropModelUpdateConf();
  explicit RMSPropModelUpdateConf(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf);

  ~RMSPropModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_rmspropmodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RMSPropModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RMSPropModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const RMSPropModelUpdateConf& other);
  RMSPropModelUpdateConf& operator=(const RMSPropModelUpdateConf& other);

  // required or optional field decay_rate
 public:
  void clear_decay_rate();
  void set_decay_rate(const float& value);
  float* mutable_decay_rate();
  // required or optional field epsilon
 public:
  void clear_epsilon();
  void set_epsilon(const float& value);
  float* mutable_epsilon();
  // required or optional field centered
 public:
  void clear_centered();
  void set_centered(const bool& value);
  bool* mutable_centered();

  ::std::shared_ptr<RMSPropModelUpdateConf> __SharedMutable__();
};


class ConstLARSModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _LARSModelUpdateConf_ {
   public:
    _LARSModelUpdateConf_();
    explicit _LARSModelUpdateConf_(const _LARSModelUpdateConf_& other);
    explicit _LARSModelUpdateConf_(_LARSModelUpdateConf_&& other);
    _LARSModelUpdateConf_(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf);
    ~_LARSModelUpdateConf_();

    void InitFromProto(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf);

    void ToProto(::oneflow::LARSModelUpdateConf* proto_larsmodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LARSModelUpdateConf_& other);
  
      // optional field momentum_beta
     public:
    bool has_momentum_beta() const;
    const float& momentum_beta() const;
    void clear_momentum_beta();
    void set_momentum_beta(const float& value);
    float* mutable_momentum_beta();
   protected:
    bool has_momentum_beta_ = false;
    float momentum_beta_;
      
      // optional field epsilon
     public:
    bool has_epsilon() const;
    const float& epsilon() const;
    void clear_epsilon();
    void set_epsilon(const float& value);
    float* mutable_epsilon();
   protected:
    bool has_epsilon_ = false;
    float epsilon_;
      
      // optional field lars_coefficient
     public:
    bool has_lars_coefficient() const;
    const float& lars_coefficient() const;
    void clear_lars_coefficient();
    void set_lars_coefficient(const float& value);
    float* mutable_lars_coefficient();
   protected:
    bool has_lars_coefficient_ = false;
    float lars_coefficient_;
           
   public:
    int compare(const _LARSModelUpdateConf_& other);

    bool operator==(const _LARSModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LARSModelUpdateConf_& other) const;
  };

  ConstLARSModelUpdateConf(const ::std::shared_ptr<_LARSModelUpdateConf_>& data);
  ConstLARSModelUpdateConf(const ConstLARSModelUpdateConf&);
  ConstLARSModelUpdateConf(ConstLARSModelUpdateConf&&) noexcept;
  ConstLARSModelUpdateConf();
  ConstLARSModelUpdateConf(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf);
  virtual ~ConstLARSModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_larsmodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field momentum_beta
 public:
  bool has_momentum_beta() const;
  const float& momentum_beta() const;
  // used by pybind11 only
  // required or optional field epsilon
 public:
  bool has_epsilon() const;
  const float& epsilon() const;
  // used by pybind11 only
  // required or optional field lars_coefficient
 public:
  bool has_lars_coefficient() const;
  const float& lars_coefficient() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLARSModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLARSModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLARSModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_LARSModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LARSModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLARSModelUpdateConf
  void BuildFromProto(const PbMessage& proto_larsmodelupdateconf);
  
  ::std::shared_ptr<_LARSModelUpdateConf_> data_;
};

class LARSModelUpdateConf final : public ConstLARSModelUpdateConf {
 public:
  LARSModelUpdateConf(const ::std::shared_ptr<_LARSModelUpdateConf_>& data);
  LARSModelUpdateConf(const LARSModelUpdateConf& other);
  // enable nothrow for ::std::vector<LARSModelUpdateConf> resize 
  LARSModelUpdateConf(LARSModelUpdateConf&&) noexcept;
  LARSModelUpdateConf();
  explicit LARSModelUpdateConf(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf);

  ~LARSModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_larsmodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LARSModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LARSModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const LARSModelUpdateConf& other);
  LARSModelUpdateConf& operator=(const LARSModelUpdateConf& other);

  // required or optional field momentum_beta
 public:
  void clear_momentum_beta();
  void set_momentum_beta(const float& value);
  float* mutable_momentum_beta();
  // required or optional field epsilon
 public:
  void clear_epsilon();
  void set_epsilon(const float& value);
  float* mutable_epsilon();
  // required or optional field lars_coefficient
 public:
  void clear_lars_coefficient();
  void set_lars_coefficient(const float& value);
  float* mutable_lars_coefficient();

  ::std::shared_ptr<LARSModelUpdateConf> __SharedMutable__();
};


class ConstAdamModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _AdamModelUpdateConf_ {
   public:
    _AdamModelUpdateConf_();
    explicit _AdamModelUpdateConf_(const _AdamModelUpdateConf_& other);
    explicit _AdamModelUpdateConf_(_AdamModelUpdateConf_&& other);
    _AdamModelUpdateConf_(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf);
    ~_AdamModelUpdateConf_();

    void InitFromProto(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf);

    void ToProto(::oneflow::AdamModelUpdateConf* proto_adammodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AdamModelUpdateConf_& other);
  
      // optional field beta1
     public:
    bool has_beta1() const;
    const float& beta1() const;
    void clear_beta1();
    void set_beta1(const float& value);
    float* mutable_beta1();
   protected:
    bool has_beta1_ = false;
    float beta1_;
      
      // optional field beta2
     public:
    bool has_beta2() const;
    const float& beta2() const;
    void clear_beta2();
    void set_beta2(const float& value);
    float* mutable_beta2();
   protected:
    bool has_beta2_ = false;
    float beta2_;
      
      // optional field epsilon
     public:
    bool has_epsilon() const;
    const float& epsilon() const;
    void clear_epsilon();
    void set_epsilon(const float& value);
    float* mutable_epsilon();
   protected:
    bool has_epsilon_ = false;
    float epsilon_;
      
      // optional field do_bias_correction
     public:
    bool has_do_bias_correction() const;
    const bool& do_bias_correction() const;
    void clear_do_bias_correction();
    void set_do_bias_correction(const bool& value);
    bool* mutable_do_bias_correction();
   protected:
    bool has_do_bias_correction_ = false;
    bool do_bias_correction_;
           
   public:
    int compare(const _AdamModelUpdateConf_& other);

    bool operator==(const _AdamModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AdamModelUpdateConf_& other) const;
  };

  ConstAdamModelUpdateConf(const ::std::shared_ptr<_AdamModelUpdateConf_>& data);
  ConstAdamModelUpdateConf(const ConstAdamModelUpdateConf&);
  ConstAdamModelUpdateConf(ConstAdamModelUpdateConf&&) noexcept;
  ConstAdamModelUpdateConf();
  ConstAdamModelUpdateConf(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf);
  virtual ~ConstAdamModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_adammodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field beta1
 public:
  bool has_beta1() const;
  const float& beta1() const;
  // used by pybind11 only
  // required or optional field beta2
 public:
  bool has_beta2() const;
  const float& beta2() const;
  // used by pybind11 only
  // required or optional field epsilon
 public:
  bool has_epsilon() const;
  const float& epsilon() const;
  // used by pybind11 only
  // required or optional field do_bias_correction
 public:
  bool has_do_bias_correction() const;
  const bool& do_bias_correction() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstAdamModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAdamModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAdamModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_AdamModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AdamModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAdamModelUpdateConf
  void BuildFromProto(const PbMessage& proto_adammodelupdateconf);
  
  ::std::shared_ptr<_AdamModelUpdateConf_> data_;
};

class AdamModelUpdateConf final : public ConstAdamModelUpdateConf {
 public:
  AdamModelUpdateConf(const ::std::shared_ptr<_AdamModelUpdateConf_>& data);
  AdamModelUpdateConf(const AdamModelUpdateConf& other);
  // enable nothrow for ::std::vector<AdamModelUpdateConf> resize 
  AdamModelUpdateConf(AdamModelUpdateConf&&) noexcept;
  AdamModelUpdateConf();
  explicit AdamModelUpdateConf(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf);

  ~AdamModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_adammodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AdamModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AdamModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const AdamModelUpdateConf& other);
  AdamModelUpdateConf& operator=(const AdamModelUpdateConf& other);

  // required or optional field beta1
 public:
  void clear_beta1();
  void set_beta1(const float& value);
  float* mutable_beta1();
  // required or optional field beta2
 public:
  void clear_beta2();
  void set_beta2(const float& value);
  float* mutable_beta2();
  // required or optional field epsilon
 public:
  void clear_epsilon();
  void set_epsilon(const float& value);
  float* mutable_epsilon();
  // required or optional field do_bias_correction
 public:
  void clear_do_bias_correction();
  void set_do_bias_correction(const bool& value);
  bool* mutable_do_bias_correction();

  ::std::shared_ptr<AdamModelUpdateConf> __SharedMutable__();
};


class ConstLazyAdamModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _LazyAdamModelUpdateConf_ {
   public:
    _LazyAdamModelUpdateConf_();
    explicit _LazyAdamModelUpdateConf_(const _LazyAdamModelUpdateConf_& other);
    explicit _LazyAdamModelUpdateConf_(_LazyAdamModelUpdateConf_&& other);
    _LazyAdamModelUpdateConf_(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf);
    ~_LazyAdamModelUpdateConf_();

    void InitFromProto(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf);

    void ToProto(::oneflow::LazyAdamModelUpdateConf* proto_lazyadammodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LazyAdamModelUpdateConf_& other);
  
      // optional field beta1
     public:
    bool has_beta1() const;
    const float& beta1() const;
    void clear_beta1();
    void set_beta1(const float& value);
    float* mutable_beta1();
   protected:
    bool has_beta1_ = false;
    float beta1_;
      
      // optional field beta2
     public:
    bool has_beta2() const;
    const float& beta2() const;
    void clear_beta2();
    void set_beta2(const float& value);
    float* mutable_beta2();
   protected:
    bool has_beta2_ = false;
    float beta2_;
      
      // optional field epsilon
     public:
    bool has_epsilon() const;
    const float& epsilon() const;
    void clear_epsilon();
    void set_epsilon(const float& value);
    float* mutable_epsilon();
   protected:
    bool has_epsilon_ = false;
    float epsilon_;
           
   public:
    int compare(const _LazyAdamModelUpdateConf_& other);

    bool operator==(const _LazyAdamModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LazyAdamModelUpdateConf_& other) const;
  };

  ConstLazyAdamModelUpdateConf(const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& data);
  ConstLazyAdamModelUpdateConf(const ConstLazyAdamModelUpdateConf&);
  ConstLazyAdamModelUpdateConf(ConstLazyAdamModelUpdateConf&&) noexcept;
  ConstLazyAdamModelUpdateConf();
  ConstLazyAdamModelUpdateConf(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf);
  virtual ~ConstLazyAdamModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_lazyadammodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field beta1
 public:
  bool has_beta1() const;
  const float& beta1() const;
  // used by pybind11 only
  // required or optional field beta2
 public:
  bool has_beta2() const;
  const float& beta2() const;
  // used by pybind11 only
  // required or optional field epsilon
 public:
  bool has_epsilon() const;
  const float& epsilon() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLazyAdamModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLazyAdamModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLazyAdamModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLazyAdamModelUpdateConf
  void BuildFromProto(const PbMessage& proto_lazyadammodelupdateconf);
  
  ::std::shared_ptr<_LazyAdamModelUpdateConf_> data_;
};

class LazyAdamModelUpdateConf final : public ConstLazyAdamModelUpdateConf {
 public:
  LazyAdamModelUpdateConf(const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& data);
  LazyAdamModelUpdateConf(const LazyAdamModelUpdateConf& other);
  // enable nothrow for ::std::vector<LazyAdamModelUpdateConf> resize 
  LazyAdamModelUpdateConf(LazyAdamModelUpdateConf&&) noexcept;
  LazyAdamModelUpdateConf();
  explicit LazyAdamModelUpdateConf(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf);

  ~LazyAdamModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_lazyadammodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LazyAdamModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LazyAdamModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const LazyAdamModelUpdateConf& other);
  LazyAdamModelUpdateConf& operator=(const LazyAdamModelUpdateConf& other);

  // required or optional field beta1
 public:
  void clear_beta1();
  void set_beta1(const float& value);
  float* mutable_beta1();
  // required or optional field beta2
 public:
  void clear_beta2();
  void set_beta2(const float& value);
  float* mutable_beta2();
  // required or optional field epsilon
 public:
  void clear_epsilon();
  void set_epsilon(const float& value);
  float* mutable_epsilon();

  ::std::shared_ptr<LazyAdamModelUpdateConf> __SharedMutable__();
};


class ConstLambModelUpdateConf : public ::oneflow::cfg::Message {
 public:

  class _LambModelUpdateConf_ {
   public:
    _LambModelUpdateConf_();
    explicit _LambModelUpdateConf_(const _LambModelUpdateConf_& other);
    explicit _LambModelUpdateConf_(_LambModelUpdateConf_&& other);
    _LambModelUpdateConf_(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf);
    ~_LambModelUpdateConf_();

    void InitFromProto(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf);

    void ToProto(::oneflow::LambModelUpdateConf* proto_lambmodelupdateconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LambModelUpdateConf_& other);
  
      // optional field beta1
     public:
    bool has_beta1() const;
    const float& beta1() const;
    void clear_beta1();
    void set_beta1(const float& value);
    float* mutable_beta1();
   protected:
    bool has_beta1_ = false;
    float beta1_;
      
      // optional field beta2
     public:
    bool has_beta2() const;
    const float& beta2() const;
    void clear_beta2();
    void set_beta2(const float& value);
    float* mutable_beta2();
   protected:
    bool has_beta2_ = false;
    float beta2_;
      
      // optional field epsilon
     public:
    bool has_epsilon() const;
    const float& epsilon() const;
    void clear_epsilon();
    void set_epsilon(const float& value);
    float* mutable_epsilon();
   protected:
    bool has_epsilon_ = false;
    float epsilon_;
           
   public:
    int compare(const _LambModelUpdateConf_& other);

    bool operator==(const _LambModelUpdateConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LambModelUpdateConf_& other) const;
  };

  ConstLambModelUpdateConf(const ::std::shared_ptr<_LambModelUpdateConf_>& data);
  ConstLambModelUpdateConf(const ConstLambModelUpdateConf&);
  ConstLambModelUpdateConf(ConstLambModelUpdateConf&&) noexcept;
  ConstLambModelUpdateConf();
  ConstLambModelUpdateConf(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf);
  virtual ~ConstLambModelUpdateConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_lambmodelupdateconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field beta1
 public:
  bool has_beta1() const;
  const float& beta1() const;
  // used by pybind11 only
  // required or optional field beta2
 public:
  bool has_beta2() const;
  const float& beta2() const;
  // used by pybind11 only
  // required or optional field epsilon
 public:
  bool has_epsilon() const;
  const float& epsilon() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstLambModelUpdateConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLambModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLambModelUpdateConf& other) const;
 protected:
  const ::std::shared_ptr<_LambModelUpdateConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LambModelUpdateConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLambModelUpdateConf
  void BuildFromProto(const PbMessage& proto_lambmodelupdateconf);
  
  ::std::shared_ptr<_LambModelUpdateConf_> data_;
};

class LambModelUpdateConf final : public ConstLambModelUpdateConf {
 public:
  LambModelUpdateConf(const ::std::shared_ptr<_LambModelUpdateConf_>& data);
  LambModelUpdateConf(const LambModelUpdateConf& other);
  // enable nothrow for ::std::vector<LambModelUpdateConf> resize 
  LambModelUpdateConf(LambModelUpdateConf&&) noexcept;
  LambModelUpdateConf();
  explicit LambModelUpdateConf(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf);

  ~LambModelUpdateConf() override;

  void InitFromProto(const PbMessage& proto_lambmodelupdateconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LambModelUpdateConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LambModelUpdateConf& other) const;
  void Clear();
  void CopyFrom(const LambModelUpdateConf& other);
  LambModelUpdateConf& operator=(const LambModelUpdateConf& other);

  // required or optional field beta1
 public:
  void clear_beta1();
  void set_beta1(const float& value);
  float* mutable_beta1();
  // required or optional field beta2
 public:
  void clear_beta2();
  void set_beta2(const float& value);
  float* mutable_beta2();
  // required or optional field epsilon
 public:
  void clear_epsilon();
  void set_epsilon(const float& value);
  float* mutable_epsilon();

  ::std::shared_ptr<LambModelUpdateConf> __SharedMutable__();
};


class ConstClipByGlobalNormConf : public ::oneflow::cfg::Message {
 public:

  class _ClipByGlobalNormConf_ {
   public:
    _ClipByGlobalNormConf_();
    explicit _ClipByGlobalNormConf_(const _ClipByGlobalNormConf_& other);
    explicit _ClipByGlobalNormConf_(_ClipByGlobalNormConf_&& other);
    _ClipByGlobalNormConf_(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf);
    ~_ClipByGlobalNormConf_();

    void InitFromProto(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf);

    void ToProto(::oneflow::ClipByGlobalNormConf* proto_clipbyglobalnormconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClipByGlobalNormConf_& other);
  
      // optional field clip_norm
     public:
    bool has_clip_norm() const;
    const float& clip_norm() const;
    void clear_clip_norm();
    void set_clip_norm(const float& value);
    float* mutable_clip_norm();
   protected:
    bool has_clip_norm_ = false;
    float clip_norm_;
      
      // optional field global_norm
     public:
    bool has_global_norm() const;
    const float& global_norm() const;
    void clear_global_norm();
    void set_global_norm(const float& value);
    float* mutable_global_norm();
   protected:
    bool has_global_norm_ = false;
    float global_norm_;
           
   public:
    int compare(const _ClipByGlobalNormConf_& other);

    bool operator==(const _ClipByGlobalNormConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClipByGlobalNormConf_& other) const;
  };

  ConstClipByGlobalNormConf(const ::std::shared_ptr<_ClipByGlobalNormConf_>& data);
  ConstClipByGlobalNormConf(const ConstClipByGlobalNormConf&);
  ConstClipByGlobalNormConf(ConstClipByGlobalNormConf&&) noexcept;
  ConstClipByGlobalNormConf();
  ConstClipByGlobalNormConf(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf);
  virtual ~ConstClipByGlobalNormConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clipbyglobalnormconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field clip_norm
 public:
  bool has_clip_norm() const;
  const float& clip_norm() const;
  // used by pybind11 only
  // required or optional field global_norm
 public:
  bool has_global_norm() const;
  const float& global_norm() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstClipByGlobalNormConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClipByGlobalNormConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClipByGlobalNormConf& other) const;
 protected:
  const ::std::shared_ptr<_ClipByGlobalNormConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClipByGlobalNormConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClipByGlobalNormConf
  void BuildFromProto(const PbMessage& proto_clipbyglobalnormconf);
  
  ::std::shared_ptr<_ClipByGlobalNormConf_> data_;
};

class ClipByGlobalNormConf final : public ConstClipByGlobalNormConf {
 public:
  ClipByGlobalNormConf(const ::std::shared_ptr<_ClipByGlobalNormConf_>& data);
  ClipByGlobalNormConf(const ClipByGlobalNormConf& other);
  // enable nothrow for ::std::vector<ClipByGlobalNormConf> resize 
  ClipByGlobalNormConf(ClipByGlobalNormConf&&) noexcept;
  ClipByGlobalNormConf();
  explicit ClipByGlobalNormConf(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf);

  ~ClipByGlobalNormConf() override;

  void InitFromProto(const PbMessage& proto_clipbyglobalnormconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClipByGlobalNormConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClipByGlobalNormConf& other) const;
  void Clear();
  void CopyFrom(const ClipByGlobalNormConf& other);
  ClipByGlobalNormConf& operator=(const ClipByGlobalNormConf& other);

  // required or optional field clip_norm
 public:
  void clear_clip_norm();
  void set_clip_norm(const float& value);
  float* mutable_clip_norm();
  // required or optional field global_norm
 public:
  void clear_global_norm();
  void set_global_norm(const float& value);
  float* mutable_global_norm();

  ::std::shared_ptr<ClipByGlobalNormConf> __SharedMutable__();
};


class ConstClipConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kClipByGlobalNorm = 1,
   };

  class _ClipConf_ {
   public:
    _ClipConf_();
    explicit _ClipConf_(const _ClipConf_& other);
    explicit _ClipConf_(_ClipConf_&& other);
    _ClipConf_(const ::oneflow::ClipConf& proto_clipconf);
    ~_ClipConf_();

    void InitFromProto(const ::oneflow::ClipConf& proto_clipconf);

    void ToProto(::oneflow::ClipConf* proto_clipconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ClipConf_& other);
  
     // oneof field type: clip_by_global_norm
   public:
    bool has_clip_by_global_norm() const;
    void clear_clip_by_global_norm();
    const ::oneflow::cfg::ClipByGlobalNormConf& clip_by_global_norm() const;
      ::oneflow::cfg::ClipByGlobalNormConf* mutable_clip_by_global_norm();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _ClipConf_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char clip_by_global_norm_[sizeof(::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _ClipConf_& other);

    bool operator==(const _ClipConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ClipConf_& other) const;
  };

  ConstClipConf(const ::std::shared_ptr<_ClipConf_>& data);
  ConstClipConf(const ConstClipConf&);
  ConstClipConf(ConstClipConf&&) noexcept;
  ConstClipConf();
  ConstClipConf(const ::oneflow::ClipConf& proto_clipconf);
  virtual ~ConstClipConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_clipconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: clip_by_global_norm
 public:
  bool has_clip_by_global_norm() const;
  const ::oneflow::cfg::ClipByGlobalNormConf& clip_by_global_norm() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClipByGlobalNormConf> shared_const_clip_by_global_norm() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstClipConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstClipConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstClipConf& other) const;
 protected:
  const ::std::shared_ptr<_ClipConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ClipConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstClipConf
  void BuildFromProto(const PbMessage& proto_clipconf);
  
  ::std::shared_ptr<_ClipConf_> data_;
};

class ClipConf final : public ConstClipConf {
 public:
  ClipConf(const ::std::shared_ptr<_ClipConf_>& data);
  ClipConf(const ClipConf& other);
  // enable nothrow for ::std::vector<ClipConf> resize 
  ClipConf(ClipConf&&) noexcept;
  ClipConf();
  explicit ClipConf(const ::oneflow::ClipConf& proto_clipconf);

  ~ClipConf() override;

  void InitFromProto(const PbMessage& proto_clipconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ClipConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ClipConf& other) const;
  void Clear();
  void CopyFrom(const ClipConf& other);
  ClipConf& operator=(const ClipConf& other);

  void clear_clip_by_global_norm();
  ::oneflow::cfg::ClipByGlobalNormConf* mutable_clip_by_global_norm();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf> shared_mutable_clip_by_global_norm();

  ::std::shared_ptr<ClipConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_;

class ConstWeightDecayFilterPatternSet : public ::oneflow::cfg::Message {
 public:

  class _WeightDecayFilterPatternSet_ {
   public:
    _WeightDecayFilterPatternSet_();
    explicit _WeightDecayFilterPatternSet_(const _WeightDecayFilterPatternSet_& other);
    explicit _WeightDecayFilterPatternSet_(_WeightDecayFilterPatternSet_&& other);
    _WeightDecayFilterPatternSet_(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset);
    ~_WeightDecayFilterPatternSet_();

    void InitFromProto(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset);

    void ToProto(::oneflow::WeightDecayFilterPatternSet* proto_weightdecayfilterpatternset) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _WeightDecayFilterPatternSet_& other);
  
      // repeated field pattern
   public:
    ::std::size_t pattern_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& pattern() const;
    const ::std::string& pattern(::std::size_t index) const;
    void clear_pattern();
    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_pattern();
    ::std::string* mutable_pattern(::std::size_t index);
      void add_pattern(const ::std::string& value);
    void set_pattern(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> pattern_;
         
   public:
    int compare(const _WeightDecayFilterPatternSet_& other);

    bool operator==(const _WeightDecayFilterPatternSet_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _WeightDecayFilterPatternSet_& other) const;
  };

  ConstWeightDecayFilterPatternSet(const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& data);
  ConstWeightDecayFilterPatternSet(const ConstWeightDecayFilterPatternSet&);
  ConstWeightDecayFilterPatternSet(ConstWeightDecayFilterPatternSet&&) noexcept;
  ConstWeightDecayFilterPatternSet();
  ConstWeightDecayFilterPatternSet(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset);
  virtual ~ConstWeightDecayFilterPatternSet() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_weightdecayfilterpatternset) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field pattern
 public:
  ::std::size_t pattern_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& pattern() const;
  const ::std::string& pattern(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_const_pattern() const;

 public:
  ::std::shared_ptr<ConstWeightDecayFilterPatternSet> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstWeightDecayFilterPatternSet& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstWeightDecayFilterPatternSet& other) const;
 protected:
  const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstWeightDecayFilterPatternSet
  void BuildFromProto(const PbMessage& proto_weightdecayfilterpatternset);
  
  ::std::shared_ptr<_WeightDecayFilterPatternSet_> data_;
};

class WeightDecayFilterPatternSet final : public ConstWeightDecayFilterPatternSet {
 public:
  WeightDecayFilterPatternSet(const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& data);
  WeightDecayFilterPatternSet(const WeightDecayFilterPatternSet& other);
  // enable nothrow for ::std::vector<WeightDecayFilterPatternSet> resize 
  WeightDecayFilterPatternSet(WeightDecayFilterPatternSet&&) noexcept;
  WeightDecayFilterPatternSet();
  explicit WeightDecayFilterPatternSet(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset);

  ~WeightDecayFilterPatternSet() override;

  void InitFromProto(const PbMessage& proto_weightdecayfilterpatternset) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const WeightDecayFilterPatternSet& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const WeightDecayFilterPatternSet& other) const;
  void Clear();
  void CopyFrom(const WeightDecayFilterPatternSet& other);
  WeightDecayFilterPatternSet& operator=(const WeightDecayFilterPatternSet& other);

  // repeated field pattern
 public:
  void clear_pattern();
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_pattern();
  ::std::string* mutable_pattern(::std::size_t index);
  void add_pattern(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_pattern();
  void set_pattern(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<WeightDecayFilterPatternSet> __SharedMutable__();
};


class ConstWeightDecayConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum weight_decay_filter_type
 enum WeightDecayFilterTypeCase : unsigned int {
  WEIGHT_DECAY_FILTER_TYPE_NOT_SET = 0,
    kIncludes = 2,
    kExcludes = 3,
   };

  class _WeightDecayConf_ {
   public:
    _WeightDecayConf_();
    explicit _WeightDecayConf_(const _WeightDecayConf_& other);
    explicit _WeightDecayConf_(_WeightDecayConf_&& other);
    _WeightDecayConf_(const ::oneflow::WeightDecayConf& proto_weightdecayconf);
    ~_WeightDecayConf_();

    void InitFromProto(const ::oneflow::WeightDecayConf& proto_weightdecayconf);

    void ToProto(::oneflow::WeightDecayConf* proto_weightdecayconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _WeightDecayConf_& other);
  
      // optional field weight_decay_rate
     public:
    bool has_weight_decay_rate() const;
    const float& weight_decay_rate() const;
    void clear_weight_decay_rate();
    void set_weight_decay_rate(const float& value);
    float* mutable_weight_decay_rate();
   protected:
    bool has_weight_decay_rate_ = false;
    float weight_decay_rate_;
      
     // oneof field weight_decay_filter_type: includes
   public:
    bool has_includes() const;
    void clear_includes();
    const ::oneflow::cfg::WeightDecayFilterPatternSet& includes() const;
      ::oneflow::cfg::WeightDecayFilterPatternSet* mutable_includes();
      
     // oneof field weight_decay_filter_type: excludes
   public:
    bool has_excludes() const;
    void clear_excludes();
    const ::oneflow::cfg::WeightDecayFilterPatternSet& excludes() const;
      ::oneflow::cfg::WeightDecayFilterPatternSet* mutable_excludes();
           
   public:
    // oneof weight_decay_filter_type
    WeightDecayFilterTypeCase weight_decay_filter_type_case() const;
    bool has_weight_decay_filter_type() const;
   protected:
    void clear_weight_decay_filter_type();
    void weight_decay_filter_type_copy_from(const _WeightDecayConf_& other);
    union WeightDecayFilterTypeUnion {
      // 64-bit aligned
      uint64_t __weight_decay_filter_type_for_padding_64bit__;
          char includes_[sizeof(::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>)];
            char excludes_[sizeof(::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>)];
        } weight_decay_filter_type_;
    WeightDecayFilterTypeCase weight_decay_filter_type_case_ = WEIGHT_DECAY_FILTER_TYPE_NOT_SET;
     
   public:
    int compare(const _WeightDecayConf_& other);

    bool operator==(const _WeightDecayConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _WeightDecayConf_& other) const;
  };

  ConstWeightDecayConf(const ::std::shared_ptr<_WeightDecayConf_>& data);
  ConstWeightDecayConf(const ConstWeightDecayConf&);
  ConstWeightDecayConf(ConstWeightDecayConf&&) noexcept;
  ConstWeightDecayConf();
  ConstWeightDecayConf(const ::oneflow::WeightDecayConf& proto_weightdecayconf);
  virtual ~ConstWeightDecayConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_weightdecayconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field weight_decay_rate
 public:
  bool has_weight_decay_rate() const;
  const float& weight_decay_rate() const;
  // used by pybind11 only
 // oneof field weight_decay_filter_type: includes
 public:
  bool has_includes() const;
  const ::oneflow::cfg::WeightDecayFilterPatternSet& includes() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWeightDecayFilterPatternSet> shared_const_includes() const;
 // oneof field weight_decay_filter_type: excludes
 public:
  bool has_excludes() const;
  const ::oneflow::cfg::WeightDecayFilterPatternSet& excludes() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWeightDecayFilterPatternSet> shared_const_excludes() const;
 public:
  WeightDecayFilterTypeCase weight_decay_filter_type_case() const;
 protected:
  bool has_weight_decay_filter_type() const;

 public:
  ::std::shared_ptr<ConstWeightDecayConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstWeightDecayConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstWeightDecayConf& other) const;
 protected:
  const ::std::shared_ptr<_WeightDecayConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_WeightDecayConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstWeightDecayConf
  void BuildFromProto(const PbMessage& proto_weightdecayconf);
  
  ::std::shared_ptr<_WeightDecayConf_> data_;
};

class WeightDecayConf final : public ConstWeightDecayConf {
 public:
  WeightDecayConf(const ::std::shared_ptr<_WeightDecayConf_>& data);
  WeightDecayConf(const WeightDecayConf& other);
  // enable nothrow for ::std::vector<WeightDecayConf> resize 
  WeightDecayConf(WeightDecayConf&&) noexcept;
  WeightDecayConf();
  explicit WeightDecayConf(const ::oneflow::WeightDecayConf& proto_weightdecayconf);

  ~WeightDecayConf() override;

  void InitFromProto(const PbMessage& proto_weightdecayconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const WeightDecayConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const WeightDecayConf& other) const;
  void Clear();
  void CopyFrom(const WeightDecayConf& other);
  WeightDecayConf& operator=(const WeightDecayConf& other);

  // required or optional field weight_decay_rate
 public:
  void clear_weight_decay_rate();
  void set_weight_decay_rate(const float& value);
  float* mutable_weight_decay_rate();
  void clear_includes();
  ::oneflow::cfg::WeightDecayFilterPatternSet* mutable_includes();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> shared_mutable_includes();
  void clear_excludes();
  ::oneflow::cfg::WeightDecayFilterPatternSet* mutable_excludes();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> shared_mutable_excludes();

  ::std::shared_ptr<WeightDecayConf> __SharedMutable__();
};


class ConstOptimizerConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum normal_mdupdt
 enum NormalMdupdtCase : unsigned int {
  NORMAL_MDUPDT_NOT_SET = 0,
    kNaiveConf = 1000,
    kMomentumConf = 1001,
    kRmspropConf = 1002,
    kLarsConf = 1003,
    kAdamConf = 1004,
    kLazyAdamConf = 1005,
    kLambConf = 1006,
   };

  class _OptimizerConf_ {
   public:
    _OptimizerConf_();
    explicit _OptimizerConf_(const _OptimizerConf_& other);
    explicit _OptimizerConf_(_OptimizerConf_&& other);
    _OptimizerConf_(const ::oneflow::OptimizerConf& proto_optimizerconf);
    ~_OptimizerConf_();

    void InitFromProto(const ::oneflow::OptimizerConf& proto_optimizerconf);

    void ToProto(::oneflow::OptimizerConf* proto_optimizerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OptimizerConf_& other);
  
      // repeated field variable_op_names
   public:
    ::std::size_t variable_op_names_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& variable_op_names() const;
    const ::std::string& variable_op_names(::std::size_t index) const;
    void clear_variable_op_names();
    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_names();
    ::std::string* mutable_variable_op_names(::std::size_t index);
      void add_variable_op_names(const ::std::string& value);
    void set_variable_op_names(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> variable_op_names_;
    
      // optional field base_learning_rate
     public:
    bool has_base_learning_rate() const;
    const float& base_learning_rate() const;
    void clear_base_learning_rate();
    void set_base_learning_rate(const float& value);
    float* mutable_base_learning_rate();
   protected:
    bool has_base_learning_rate_ = false;
    float base_learning_rate_;
      
      // optional field warmup_conf
     public:
    bool has_warmup_conf() const;
    const ::oneflow::cfg::WarmupConf& warmup_conf() const;
    void clear_warmup_conf();
    ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
   protected:
    bool has_warmup_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::WarmupConf> warmup_conf_;
      
      // optional field learning_rate_decay
     public:
    bool has_learning_rate_decay() const;
    const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
    void clear_learning_rate_decay();
    ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
   protected:
    bool has_learning_rate_decay_ = false;
    ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> learning_rate_decay_;
      
      // optional field learning_rate_lbn
     public:
    bool has_learning_rate_lbn() const;
    const ::std::string& learning_rate_lbn() const;
    void clear_learning_rate_lbn();
    void set_learning_rate_lbn(const ::std::string& value);
    ::std::string* mutable_learning_rate_lbn();
   protected:
    bool has_learning_rate_lbn_ = false;
    ::std::string learning_rate_lbn_;
      
      // optional field clip_conf
     public:
    bool has_clip_conf() const;
    const ::oneflow::cfg::ClipConf& clip_conf() const;
    void clear_clip_conf();
    ::oneflow::cfg::ClipConf* mutable_clip_conf();
   protected:
    bool has_clip_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ClipConf> clip_conf_;
      
      // optional field weight_decay_conf
     public:
    bool has_weight_decay_conf() const;
    const ::oneflow::cfg::WeightDecayConf& weight_decay_conf() const;
    void clear_weight_decay_conf();
    ::oneflow::cfg::WeightDecayConf* mutable_weight_decay_conf();
   protected:
    bool has_weight_decay_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> weight_decay_conf_;
      
     // oneof field normal_mdupdt: naive_conf
   public:
    bool has_naive_conf() const;
    void clear_naive_conf();
    const ::oneflow::cfg::NaiveModelUpdateConf& naive_conf() const;
      ::oneflow::cfg::NaiveModelUpdateConf* mutable_naive_conf();
      
     // oneof field normal_mdupdt: momentum_conf
   public:
    bool has_momentum_conf() const;
    void clear_momentum_conf();
    const ::oneflow::cfg::MomentumModelUpdateConf& momentum_conf() const;
      ::oneflow::cfg::MomentumModelUpdateConf* mutable_momentum_conf();
      
     // oneof field normal_mdupdt: rmsprop_conf
   public:
    bool has_rmsprop_conf() const;
    void clear_rmsprop_conf();
    const ::oneflow::cfg::RMSPropModelUpdateConf& rmsprop_conf() const;
      ::oneflow::cfg::RMSPropModelUpdateConf* mutable_rmsprop_conf();
      
     // oneof field normal_mdupdt: lars_conf
   public:
    bool has_lars_conf() const;
    void clear_lars_conf();
    const ::oneflow::cfg::LARSModelUpdateConf& lars_conf() const;
      ::oneflow::cfg::LARSModelUpdateConf* mutable_lars_conf();
      
     // oneof field normal_mdupdt: adam_conf
   public:
    bool has_adam_conf() const;
    void clear_adam_conf();
    const ::oneflow::cfg::AdamModelUpdateConf& adam_conf() const;
      ::oneflow::cfg::AdamModelUpdateConf* mutable_adam_conf();
      
     // oneof field normal_mdupdt: lazy_adam_conf
   public:
    bool has_lazy_adam_conf() const;
    void clear_lazy_adam_conf();
    const ::oneflow::cfg::LazyAdamModelUpdateConf& lazy_adam_conf() const;
      ::oneflow::cfg::LazyAdamModelUpdateConf* mutable_lazy_adam_conf();
      
     // oneof field normal_mdupdt: lamb_conf
   public:
    bool has_lamb_conf() const;
    void clear_lamb_conf();
    const ::oneflow::cfg::LambModelUpdateConf& lamb_conf() const;
      ::oneflow::cfg::LambModelUpdateConf* mutable_lamb_conf();
           
   public:
    // oneof normal_mdupdt
    NormalMdupdtCase normal_mdupdt_case() const;
    bool has_normal_mdupdt() const;
   protected:
    void clear_normal_mdupdt();
    void normal_mdupdt_copy_from(const _OptimizerConf_& other);
    union NormalMdupdtUnion {
      // 64-bit aligned
      uint64_t __normal_mdupdt_for_padding_64bit__;
          char naive_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>)];
            char momentum_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>)];
            char rmsprop_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>)];
            char lars_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>)];
            char adam_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>)];
            char lazy_adam_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>)];
            char lamb_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>)];
        } normal_mdupdt_;
    NormalMdupdtCase normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
     
   public:
    int compare(const _OptimizerConf_& other);

    bool operator==(const _OptimizerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OptimizerConf_& other) const;
  };

  ConstOptimizerConf(const ::std::shared_ptr<_OptimizerConf_>& data);
  ConstOptimizerConf(const ConstOptimizerConf&);
  ConstOptimizerConf(ConstOptimizerConf&&) noexcept;
  ConstOptimizerConf();
  ConstOptimizerConf(const ::oneflow::OptimizerConf& proto_optimizerconf);
  virtual ~ConstOptimizerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_optimizerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field variable_op_names
 public:
  ::std::size_t variable_op_names_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& variable_op_names() const;
  const ::std::string& variable_op_names(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_names() const;
  // required or optional field base_learning_rate
 public:
  bool has_base_learning_rate() const;
  const float& base_learning_rate() const;
  // used by pybind11 only
  // required or optional field warmup_conf
 public:
  bool has_warmup_conf() const;
  const ::oneflow::cfg::WarmupConf& warmup_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWarmupConf> shared_const_warmup_conf() const;
  // required or optional field learning_rate_decay
 public:
  bool has_learning_rate_decay() const;
  const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLearningRateDecayConf> shared_const_learning_rate_decay() const;
  // required or optional field learning_rate_lbn
 public:
  bool has_learning_rate_lbn() const;
  const ::std::string& learning_rate_lbn() const;
  // used by pybind11 only
  // required or optional field clip_conf
 public:
  bool has_clip_conf() const;
  const ::oneflow::cfg::ClipConf& clip_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClipConf> shared_const_clip_conf() const;
  // required or optional field weight_decay_conf
 public:
  bool has_weight_decay_conf() const;
  const ::oneflow::cfg::WeightDecayConf& weight_decay_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWeightDecayConf> shared_const_weight_decay_conf() const;
 // oneof field normal_mdupdt: naive_conf
 public:
  bool has_naive_conf() const;
  const ::oneflow::cfg::NaiveModelUpdateConf& naive_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstNaiveModelUpdateConf> shared_const_naive_conf() const;
 // oneof field normal_mdupdt: momentum_conf
 public:
  bool has_momentum_conf() const;
  const ::oneflow::cfg::MomentumModelUpdateConf& momentum_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMomentumModelUpdateConf> shared_const_momentum_conf() const;
 // oneof field normal_mdupdt: rmsprop_conf
 public:
  bool has_rmsprop_conf() const;
  const ::oneflow::cfg::RMSPropModelUpdateConf& rmsprop_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRMSPropModelUpdateConf> shared_const_rmsprop_conf() const;
 // oneof field normal_mdupdt: lars_conf
 public:
  bool has_lars_conf() const;
  const ::oneflow::cfg::LARSModelUpdateConf& lars_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLARSModelUpdateConf> shared_const_lars_conf() const;
 // oneof field normal_mdupdt: adam_conf
 public:
  bool has_adam_conf() const;
  const ::oneflow::cfg::AdamModelUpdateConf& adam_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAdamModelUpdateConf> shared_const_adam_conf() const;
 // oneof field normal_mdupdt: lazy_adam_conf
 public:
  bool has_lazy_adam_conf() const;
  const ::oneflow::cfg::LazyAdamModelUpdateConf& lazy_adam_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLazyAdamModelUpdateConf> shared_const_lazy_adam_conf() const;
 // oneof field normal_mdupdt: lamb_conf
 public:
  bool has_lamb_conf() const;
  const ::oneflow::cfg::LambModelUpdateConf& lamb_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLambModelUpdateConf> shared_const_lamb_conf() const;
 public:
  NormalMdupdtCase normal_mdupdt_case() const;
 protected:
  bool has_normal_mdupdt() const;

 public:
  ::std::shared_ptr<ConstOptimizerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOptimizerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOptimizerConf& other) const;
 protected:
  const ::std::shared_ptr<_OptimizerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OptimizerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOptimizerConf
  void BuildFromProto(const PbMessage& proto_optimizerconf);
  
  ::std::shared_ptr<_OptimizerConf_> data_;
};

class OptimizerConf final : public ConstOptimizerConf {
 public:
  OptimizerConf(const ::std::shared_ptr<_OptimizerConf_>& data);
  OptimizerConf(const OptimizerConf& other);
  // enable nothrow for ::std::vector<OptimizerConf> resize 
  OptimizerConf(OptimizerConf&&) noexcept;
  OptimizerConf();
  explicit OptimizerConf(const ::oneflow::OptimizerConf& proto_optimizerconf);

  ~OptimizerConf() override;

  void InitFromProto(const PbMessage& proto_optimizerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OptimizerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OptimizerConf& other) const;
  void Clear();
  void CopyFrom(const OptimizerConf& other);
  OptimizerConf& operator=(const OptimizerConf& other);

  // repeated field variable_op_names
 public:
  void clear_variable_op_names();
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_names();
  ::std::string* mutable_variable_op_names(::std::size_t index);
  void add_variable_op_names(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_names();
  void set_variable_op_names(::std::size_t index, const ::std::string& value);
  // required or optional field base_learning_rate
 public:
  void clear_base_learning_rate();
  void set_base_learning_rate(const float& value);
  float* mutable_base_learning_rate();
  // required or optional field warmup_conf
 public:
  void clear_warmup_conf();
  ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WarmupConf> shared_mutable_warmup_conf();
  // required or optional field learning_rate_decay
 public:
  void clear_learning_rate_decay();
  ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> shared_mutable_learning_rate_decay();
  // required or optional field learning_rate_lbn
 public:
  void clear_learning_rate_lbn();
  void set_learning_rate_lbn(const ::std::string& value);
  ::std::string* mutable_learning_rate_lbn();
  // required or optional field clip_conf
 public:
  void clear_clip_conf();
  ::oneflow::cfg::ClipConf* mutable_clip_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClipConf> shared_mutable_clip_conf();
  // required or optional field weight_decay_conf
 public:
  void clear_weight_decay_conf();
  ::oneflow::cfg::WeightDecayConf* mutable_weight_decay_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> shared_mutable_weight_decay_conf();
  void clear_naive_conf();
  ::oneflow::cfg::NaiveModelUpdateConf* mutable_naive_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> shared_mutable_naive_conf();
  void clear_momentum_conf();
  ::oneflow::cfg::MomentumModelUpdateConf* mutable_momentum_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> shared_mutable_momentum_conf();
  void clear_rmsprop_conf();
  ::oneflow::cfg::RMSPropModelUpdateConf* mutable_rmsprop_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> shared_mutable_rmsprop_conf();
  void clear_lars_conf();
  ::oneflow::cfg::LARSModelUpdateConf* mutable_lars_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> shared_mutable_lars_conf();
  void clear_adam_conf();
  ::oneflow::cfg::AdamModelUpdateConf* mutable_adam_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> shared_mutable_adam_conf();
  void clear_lazy_adam_conf();
  ::oneflow::cfg::LazyAdamModelUpdateConf* mutable_lazy_adam_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> shared_mutable_lazy_adam_conf();
  void clear_lamb_conf();
  ::oneflow::cfg::LambModelUpdateConf* mutable_lamb_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> shared_mutable_lamb_conf();

  ::std::shared_ptr<OptimizerConf> __SharedMutable__();
};


class ConstNormalModelUpdateOpUserConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum normal_mdupdt
 enum NormalMdupdtCase : unsigned int {
  NORMAL_MDUPDT_NOT_SET = 0,
    kNaiveConf = 1000,
    kMomentumConf = 1001,
    kRmspropConf = 1002,
    kLarsConf = 1003,
    kAdamConf = 1004,
    kLazyAdamConf = 1005,
    kLambConf = 1006,
   };

  class _NormalModelUpdateOpUserConf_ {
   public:
    _NormalModelUpdateOpUserConf_();
    explicit _NormalModelUpdateOpUserConf_(const _NormalModelUpdateOpUserConf_& other);
    explicit _NormalModelUpdateOpUserConf_(_NormalModelUpdateOpUserConf_&& other);
    _NormalModelUpdateOpUserConf_(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf);
    ~_NormalModelUpdateOpUserConf_();

    void InitFromProto(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf);

    void ToProto(::oneflow::NormalModelUpdateOpUserConf* proto_normalmodelupdateopuserconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _NormalModelUpdateOpUserConf_& other);
  
      // optional field learning_rate_decay
     public:
    bool has_learning_rate_decay() const;
    const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
    void clear_learning_rate_decay();
    ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
   protected:
    bool has_learning_rate_decay_ = false;
    ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> learning_rate_decay_;
      
      // optional field warmup_conf
     public:
    bool has_warmup_conf() const;
    const ::oneflow::cfg::WarmupConf& warmup_conf() const;
    void clear_warmup_conf();
    ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
   protected:
    bool has_warmup_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::WarmupConf> warmup_conf_;
      
      // optional field clip_conf
     public:
    bool has_clip_conf() const;
    const ::oneflow::cfg::ClipConf& clip_conf() const;
    void clear_clip_conf();
    ::oneflow::cfg::ClipConf* mutable_clip_conf();
   protected:
    bool has_clip_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ClipConf> clip_conf_;
      
      // optional field weight_decay_conf
     public:
    bool has_weight_decay_conf() const;
    const ::oneflow::cfg::WeightDecayConf& weight_decay_conf() const;
    void clear_weight_decay_conf();
    ::oneflow::cfg::WeightDecayConf* mutable_weight_decay_conf();
   protected:
    bool has_weight_decay_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> weight_decay_conf_;
      
     // oneof field normal_mdupdt: naive_conf
   public:
    bool has_naive_conf() const;
    void clear_naive_conf();
    const ::oneflow::cfg::NaiveModelUpdateConf& naive_conf() const;
      ::oneflow::cfg::NaiveModelUpdateConf* mutable_naive_conf();
      
     // oneof field normal_mdupdt: momentum_conf
   public:
    bool has_momentum_conf() const;
    void clear_momentum_conf();
    const ::oneflow::cfg::MomentumModelUpdateConf& momentum_conf() const;
      ::oneflow::cfg::MomentumModelUpdateConf* mutable_momentum_conf();
      
     // oneof field normal_mdupdt: rmsprop_conf
   public:
    bool has_rmsprop_conf() const;
    void clear_rmsprop_conf();
    const ::oneflow::cfg::RMSPropModelUpdateConf& rmsprop_conf() const;
      ::oneflow::cfg::RMSPropModelUpdateConf* mutable_rmsprop_conf();
      
     // oneof field normal_mdupdt: lars_conf
   public:
    bool has_lars_conf() const;
    void clear_lars_conf();
    const ::oneflow::cfg::LARSModelUpdateConf& lars_conf() const;
      ::oneflow::cfg::LARSModelUpdateConf* mutable_lars_conf();
      
     // oneof field normal_mdupdt: adam_conf
   public:
    bool has_adam_conf() const;
    void clear_adam_conf();
    const ::oneflow::cfg::AdamModelUpdateConf& adam_conf() const;
      ::oneflow::cfg::AdamModelUpdateConf* mutable_adam_conf();
      
     // oneof field normal_mdupdt: lazy_adam_conf
   public:
    bool has_lazy_adam_conf() const;
    void clear_lazy_adam_conf();
    const ::oneflow::cfg::LazyAdamModelUpdateConf& lazy_adam_conf() const;
      ::oneflow::cfg::LazyAdamModelUpdateConf* mutable_lazy_adam_conf();
      
     // oneof field normal_mdupdt: lamb_conf
   public:
    bool has_lamb_conf() const;
    void clear_lamb_conf();
    const ::oneflow::cfg::LambModelUpdateConf& lamb_conf() const;
      ::oneflow::cfg::LambModelUpdateConf* mutable_lamb_conf();
           
   public:
    // oneof normal_mdupdt
    NormalMdupdtCase normal_mdupdt_case() const;
    bool has_normal_mdupdt() const;
   protected:
    void clear_normal_mdupdt();
    void normal_mdupdt_copy_from(const _NormalModelUpdateOpUserConf_& other);
    union NormalMdupdtUnion {
      // 64-bit aligned
      uint64_t __normal_mdupdt_for_padding_64bit__;
          char naive_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>)];
            char momentum_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>)];
            char rmsprop_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>)];
            char lars_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>)];
            char adam_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>)];
            char lazy_adam_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>)];
            char lamb_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>)];
        } normal_mdupdt_;
    NormalMdupdtCase normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
     
   public:
    int compare(const _NormalModelUpdateOpUserConf_& other);

    bool operator==(const _NormalModelUpdateOpUserConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _NormalModelUpdateOpUserConf_& other) const;
  };

  ConstNormalModelUpdateOpUserConf(const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& data);
  ConstNormalModelUpdateOpUserConf(const ConstNormalModelUpdateOpUserConf&);
  ConstNormalModelUpdateOpUserConf(ConstNormalModelUpdateOpUserConf&&) noexcept;
  ConstNormalModelUpdateOpUserConf();
  ConstNormalModelUpdateOpUserConf(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf);
  virtual ~ConstNormalModelUpdateOpUserConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_normalmodelupdateopuserconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field learning_rate_decay
 public:
  bool has_learning_rate_decay() const;
  const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLearningRateDecayConf> shared_const_learning_rate_decay() const;
  // required or optional field warmup_conf
 public:
  bool has_warmup_conf() const;
  const ::oneflow::cfg::WarmupConf& warmup_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWarmupConf> shared_const_warmup_conf() const;
  // required or optional field clip_conf
 public:
  bool has_clip_conf() const;
  const ::oneflow::cfg::ClipConf& clip_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstClipConf> shared_const_clip_conf() const;
  // required or optional field weight_decay_conf
 public:
  bool has_weight_decay_conf() const;
  const ::oneflow::cfg::WeightDecayConf& weight_decay_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWeightDecayConf> shared_const_weight_decay_conf() const;
 // oneof field normal_mdupdt: naive_conf
 public:
  bool has_naive_conf() const;
  const ::oneflow::cfg::NaiveModelUpdateConf& naive_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstNaiveModelUpdateConf> shared_const_naive_conf() const;
 // oneof field normal_mdupdt: momentum_conf
 public:
  bool has_momentum_conf() const;
  const ::oneflow::cfg::MomentumModelUpdateConf& momentum_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMomentumModelUpdateConf> shared_const_momentum_conf() const;
 // oneof field normal_mdupdt: rmsprop_conf
 public:
  bool has_rmsprop_conf() const;
  const ::oneflow::cfg::RMSPropModelUpdateConf& rmsprop_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRMSPropModelUpdateConf> shared_const_rmsprop_conf() const;
 // oneof field normal_mdupdt: lars_conf
 public:
  bool has_lars_conf() const;
  const ::oneflow::cfg::LARSModelUpdateConf& lars_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLARSModelUpdateConf> shared_const_lars_conf() const;
 // oneof field normal_mdupdt: adam_conf
 public:
  bool has_adam_conf() const;
  const ::oneflow::cfg::AdamModelUpdateConf& adam_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAdamModelUpdateConf> shared_const_adam_conf() const;
 // oneof field normal_mdupdt: lazy_adam_conf
 public:
  bool has_lazy_adam_conf() const;
  const ::oneflow::cfg::LazyAdamModelUpdateConf& lazy_adam_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLazyAdamModelUpdateConf> shared_const_lazy_adam_conf() const;
 // oneof field normal_mdupdt: lamb_conf
 public:
  bool has_lamb_conf() const;
  const ::oneflow::cfg::LambModelUpdateConf& lamb_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLambModelUpdateConf> shared_const_lamb_conf() const;
 public:
  NormalMdupdtCase normal_mdupdt_case() const;
 protected:
  bool has_normal_mdupdt() const;

 public:
  ::std::shared_ptr<ConstNormalModelUpdateOpUserConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstNormalModelUpdateOpUserConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstNormalModelUpdateOpUserConf& other) const;
 protected:
  const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstNormalModelUpdateOpUserConf
  void BuildFromProto(const PbMessage& proto_normalmodelupdateopuserconf);
  
  ::std::shared_ptr<_NormalModelUpdateOpUserConf_> data_;
};

class NormalModelUpdateOpUserConf final : public ConstNormalModelUpdateOpUserConf {
 public:
  NormalModelUpdateOpUserConf(const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& data);
  NormalModelUpdateOpUserConf(const NormalModelUpdateOpUserConf& other);
  // enable nothrow for ::std::vector<NormalModelUpdateOpUserConf> resize 
  NormalModelUpdateOpUserConf(NormalModelUpdateOpUserConf&&) noexcept;
  NormalModelUpdateOpUserConf();
  explicit NormalModelUpdateOpUserConf(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf);

  ~NormalModelUpdateOpUserConf() override;

  void InitFromProto(const PbMessage& proto_normalmodelupdateopuserconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const NormalModelUpdateOpUserConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const NormalModelUpdateOpUserConf& other) const;
  void Clear();
  void CopyFrom(const NormalModelUpdateOpUserConf& other);
  NormalModelUpdateOpUserConf& operator=(const NormalModelUpdateOpUserConf& other);

  // required or optional field learning_rate_decay
 public:
  void clear_learning_rate_decay();
  ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> shared_mutable_learning_rate_decay();
  // required or optional field warmup_conf
 public:
  void clear_warmup_conf();
  ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WarmupConf> shared_mutable_warmup_conf();
  // required or optional field clip_conf
 public:
  void clear_clip_conf();
  ::oneflow::cfg::ClipConf* mutable_clip_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ClipConf> shared_mutable_clip_conf();
  // required or optional field weight_decay_conf
 public:
  void clear_weight_decay_conf();
  ::oneflow::cfg::WeightDecayConf* mutable_weight_decay_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> shared_mutable_weight_decay_conf();
  void clear_naive_conf();
  ::oneflow::cfg::NaiveModelUpdateConf* mutable_naive_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> shared_mutable_naive_conf();
  void clear_momentum_conf();
  ::oneflow::cfg::MomentumModelUpdateConf* mutable_momentum_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> shared_mutable_momentum_conf();
  void clear_rmsprop_conf();
  ::oneflow::cfg::RMSPropModelUpdateConf* mutable_rmsprop_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> shared_mutable_rmsprop_conf();
  void clear_lars_conf();
  ::oneflow::cfg::LARSModelUpdateConf* mutable_lars_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> shared_mutable_lars_conf();
  void clear_adam_conf();
  ::oneflow::cfg::AdamModelUpdateConf* mutable_adam_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> shared_mutable_adam_conf();
  void clear_lazy_adam_conf();
  ::oneflow::cfg::LazyAdamModelUpdateConf* mutable_lazy_adam_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> shared_mutable_lazy_adam_conf();
  void clear_lamb_conf();
  ::oneflow::cfg::LambModelUpdateConf* mutable_lamb_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> shared_mutable_lamb_conf();

  ::std::shared_ptr<NormalModelUpdateOpUserConf> __SharedMutable__();
};


class ConstDynamicLossScalePolicy : public ::oneflow::cfg::Message {
 public:

  class _DynamicLossScalePolicy_ {
   public:
    _DynamicLossScalePolicy_();
    explicit _DynamicLossScalePolicy_(const _DynamicLossScalePolicy_& other);
    explicit _DynamicLossScalePolicy_(_DynamicLossScalePolicy_&& other);
    _DynamicLossScalePolicy_(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy);
    ~_DynamicLossScalePolicy_();

    void InitFromProto(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy);

    void ToProto(::oneflow::DynamicLossScalePolicy* proto_dynamiclossscalepolicy) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DynamicLossScalePolicy_& other);
  
      // optional field initial_loss_scale
     public:
    bool has_initial_loss_scale() const;
    const float& initial_loss_scale() const;
    void clear_initial_loss_scale();
    void set_initial_loss_scale(const float& value);
    float* mutable_initial_loss_scale();
   protected:
    bool has_initial_loss_scale_ = false;
    float initial_loss_scale_;
      
      // optional field increment_period
     public:
    bool has_increment_period() const;
    const float& increment_period() const;
    void clear_increment_period();
    void set_increment_period(const float& value);
    float* mutable_increment_period();
   protected:
    bool has_increment_period_ = false;
    float increment_period_;
      
      // optional field multiplier
     public:
    bool has_multiplier() const;
    const float& multiplier() const;
    void clear_multiplier();
    void set_multiplier(const float& value);
    float* mutable_multiplier();
   protected:
    bool has_multiplier_ = false;
    float multiplier_;
           
   public:
    int compare(const _DynamicLossScalePolicy_& other);

    bool operator==(const _DynamicLossScalePolicy_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DynamicLossScalePolicy_& other) const;
  };

  ConstDynamicLossScalePolicy(const ::std::shared_ptr<_DynamicLossScalePolicy_>& data);
  ConstDynamicLossScalePolicy(const ConstDynamicLossScalePolicy&);
  ConstDynamicLossScalePolicy(ConstDynamicLossScalePolicy&&) noexcept;
  ConstDynamicLossScalePolicy();
  ConstDynamicLossScalePolicy(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy);
  virtual ~ConstDynamicLossScalePolicy() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_dynamiclossscalepolicy) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field initial_loss_scale
 public:
  bool has_initial_loss_scale() const;
  const float& initial_loss_scale() const;
  // used by pybind11 only
  // required or optional field increment_period
 public:
  bool has_increment_period() const;
  const float& increment_period() const;
  // used by pybind11 only
  // required or optional field multiplier
 public:
  bool has_multiplier() const;
  const float& multiplier() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDynamicLossScalePolicy> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDynamicLossScalePolicy& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDynamicLossScalePolicy& other) const;
 protected:
  const ::std::shared_ptr<_DynamicLossScalePolicy_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DynamicLossScalePolicy_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDynamicLossScalePolicy
  void BuildFromProto(const PbMessage& proto_dynamiclossscalepolicy);
  
  ::std::shared_ptr<_DynamicLossScalePolicy_> data_;
};

class DynamicLossScalePolicy final : public ConstDynamicLossScalePolicy {
 public:
  DynamicLossScalePolicy(const ::std::shared_ptr<_DynamicLossScalePolicy_>& data);
  DynamicLossScalePolicy(const DynamicLossScalePolicy& other);
  // enable nothrow for ::std::vector<DynamicLossScalePolicy> resize 
  DynamicLossScalePolicy(DynamicLossScalePolicy&&) noexcept;
  DynamicLossScalePolicy();
  explicit DynamicLossScalePolicy(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy);

  ~DynamicLossScalePolicy() override;

  void InitFromProto(const PbMessage& proto_dynamiclossscalepolicy) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DynamicLossScalePolicy& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DynamicLossScalePolicy& other) const;
  void Clear();
  void CopyFrom(const DynamicLossScalePolicy& other);
  DynamicLossScalePolicy& operator=(const DynamicLossScalePolicy& other);

  // required or optional field initial_loss_scale
 public:
  void clear_initial_loss_scale();
  void set_initial_loss_scale(const float& value);
  float* mutable_initial_loss_scale();
  // required or optional field increment_period
 public:
  void clear_increment_period();
  void set_increment_period(const float& value);
  float* mutable_increment_period();
  // required or optional field multiplier
 public:
  void clear_multiplier();
  void set_multiplier(const float& value);
  float* mutable_multiplier();

  ::std::shared_ptr<DynamicLossScalePolicy> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_;
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_;

class ConstTrainConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum loss_scale_policy
 enum LossScalePolicyCase : unsigned int {
  LOSS_SCALE_POLICY_NOT_SET = 0,
    kLossScaleFactor = 4,
    kDynamicLossScalePolicy = 5,
   };

  class _TrainConf_ {
   public:
    _TrainConf_();
    explicit _TrainConf_(const _TrainConf_& other);
    explicit _TrainConf_(_TrainConf_&& other);
    _TrainConf_(const ::oneflow::TrainConf& proto_trainconf);
    ~_TrainConf_();

    void InitFromProto(const ::oneflow::TrainConf& proto_trainconf);

    void ToProto(::oneflow::TrainConf* proto_trainconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TrainConf_& other);
  
      // repeated field optimizer_conf
   public:
    ::std::size_t optimizer_conf_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& optimizer_conf() const;
    const ::oneflow::cfg::OptimizerConf& optimizer_conf(::std::size_t index) const;
    void clear_optimizer_conf();
    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_* mutable_optimizer_conf();
    ::oneflow::cfg::OptimizerConf* mutable_optimizer_conf(::std::size_t index);
      ::oneflow::cfg::OptimizerConf* add_optimizer_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> optimizer_conf_;
    
      // repeated field loss_lbn
   public:
    ::std::size_t loss_lbn_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& loss_lbn() const;
    const ::std::string& loss_lbn(::std::size_t index) const;
    void clear_loss_lbn();
    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_loss_lbn();
    ::std::string* mutable_loss_lbn(::std::size_t index);
      void add_loss_lbn(const ::std::string& value);
    void set_loss_lbn(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> loss_lbn_;
    
      // optional field train_step_lbn
     public:
    bool has_train_step_lbn() const;
    const ::std::string& train_step_lbn() const;
    void clear_train_step_lbn();
    void set_train_step_lbn(const ::std::string& value);
    ::std::string* mutable_train_step_lbn();
   protected:
    bool has_train_step_lbn_ = false;
    ::std::string train_step_lbn_;
      
     // oneof field loss_scale_policy: loss_scale_factor
   public:
    bool has_loss_scale_factor() const;
    void clear_loss_scale_factor();
    const float& loss_scale_factor() const;
      void set_loss_scale_factor(const float& value);
    float* mutable_loss_scale_factor();
      
     // oneof field loss_scale_policy: dynamic_loss_scale_policy
   public:
    bool has_dynamic_loss_scale_policy() const;
    void clear_dynamic_loss_scale_policy();
    const ::oneflow::cfg::DynamicLossScalePolicy& dynamic_loss_scale_policy() const;
      ::oneflow::cfg::DynamicLossScalePolicy* mutable_dynamic_loss_scale_policy();
      
      // optional field model_update_conf
     public:
    bool has_model_update_conf() const;
    const ::oneflow::cfg::NormalModelUpdateOpUserConf& model_update_conf() const;
    void clear_model_update_conf();
    ::oneflow::cfg::NormalModelUpdateOpUserConf* mutable_model_update_conf();
   protected:
    bool has_model_update_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::NormalModelUpdateOpUserConf> model_update_conf_;
      
      // optional field primary_lr
     public:
    bool has_primary_lr() const;
    const float& primary_lr() const;
    void clear_primary_lr();
    void set_primary_lr(const float& value);
    float* mutable_primary_lr();
   protected:
    bool has_primary_lr_ = false;
    float primary_lr_;
      
      // optional field secondary_lr
     public:
    bool has_secondary_lr() const;
    const float& secondary_lr() const;
    void clear_secondary_lr();
    void set_secondary_lr(const float& value);
    float* mutable_secondary_lr();
   protected:
    bool has_secondary_lr_ = false;
    float secondary_lr_;
      
      // optional field primary_lr_lbn
     public:
    bool has_primary_lr_lbn() const;
    const ::std::string& primary_lr_lbn() const;
    void clear_primary_lr_lbn();
    void set_primary_lr_lbn(const ::std::string& value);
    ::std::string* mutable_primary_lr_lbn();
   protected:
    bool has_primary_lr_lbn_ = false;
    ::std::string primary_lr_lbn_;
      
      // optional field secondary_lr_lbn
     public:
    bool has_secondary_lr_lbn() const;
    const ::std::string& secondary_lr_lbn() const;
    void clear_secondary_lr_lbn();
    void set_secondary_lr_lbn(const ::std::string& value);
    ::std::string* mutable_secondary_lr_lbn();
   protected:
    bool has_secondary_lr_lbn_ = false;
    ::std::string secondary_lr_lbn_;
           
   public:
    // oneof loss_scale_policy
    LossScalePolicyCase loss_scale_policy_case() const;
    bool has_loss_scale_policy() const;
   protected:
    void clear_loss_scale_policy();
    void loss_scale_policy_copy_from(const _TrainConf_& other);
    union LossScalePolicyUnion {
      // 64-bit aligned
      uint64_t __loss_scale_policy_for_padding_64bit__;
          float loss_scale_factor_;
            char dynamic_loss_scale_policy_[sizeof(::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>)];
        } loss_scale_policy_;
    LossScalePolicyCase loss_scale_policy_case_ = LOSS_SCALE_POLICY_NOT_SET;
     
   public:
    int compare(const _TrainConf_& other);

    bool operator==(const _TrainConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TrainConf_& other) const;
  };

  ConstTrainConf(const ::std::shared_ptr<_TrainConf_>& data);
  ConstTrainConf(const ConstTrainConf&);
  ConstTrainConf(ConstTrainConf&&) noexcept;
  ConstTrainConf();
  ConstTrainConf(const ::oneflow::TrainConf& proto_trainconf);
  virtual ~ConstTrainConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_trainconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field optimizer_conf
 public:
  ::std::size_t optimizer_conf_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& optimizer_conf() const;
  const ::oneflow::cfg::OptimizerConf& optimizer_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> shared_const_optimizer_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOptimizerConf> shared_const_optimizer_conf(::std::size_t index) const;
  // repeated field loss_lbn
 public:
  ::std::size_t loss_lbn_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& loss_lbn() const;
  const ::std::string& loss_lbn(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_const_loss_lbn() const;
  // required or optional field train_step_lbn
 public:
  bool has_train_step_lbn() const;
  const ::std::string& train_step_lbn() const;
  // used by pybind11 only
 // oneof field loss_scale_policy: loss_scale_factor
 public:
  bool has_loss_scale_factor() const;
  const float& loss_scale_factor() const;
  // used by pybind11 only
 // oneof field loss_scale_policy: dynamic_loss_scale_policy
 public:
  bool has_dynamic_loss_scale_policy() const;
  const ::oneflow::cfg::DynamicLossScalePolicy& dynamic_loss_scale_policy() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDynamicLossScalePolicy> shared_const_dynamic_loss_scale_policy() const;
  // required or optional field model_update_conf
 public:
  bool has_model_update_conf() const;
  const ::oneflow::cfg::NormalModelUpdateOpUserConf& model_update_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstNormalModelUpdateOpUserConf> shared_const_model_update_conf() const;
  // required or optional field primary_lr
 public:
  bool has_primary_lr() const;
  const float& primary_lr() const;
  // used by pybind11 only
  // required or optional field secondary_lr
 public:
  bool has_secondary_lr() const;
  const float& secondary_lr() const;
  // used by pybind11 only
  // required or optional field primary_lr_lbn
 public:
  bool has_primary_lr_lbn() const;
  const ::std::string& primary_lr_lbn() const;
  // used by pybind11 only
  // required or optional field secondary_lr_lbn
 public:
  bool has_secondary_lr_lbn() const;
  const ::std::string& secondary_lr_lbn() const;
  // used by pybind11 only
 public:
  LossScalePolicyCase loss_scale_policy_case() const;
 protected:
  bool has_loss_scale_policy() const;

 public:
  ::std::shared_ptr<ConstTrainConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTrainConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTrainConf& other) const;
 protected:
  const ::std::shared_ptr<_TrainConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TrainConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTrainConf
  void BuildFromProto(const PbMessage& proto_trainconf);
  
  ::std::shared_ptr<_TrainConf_> data_;
};

class TrainConf final : public ConstTrainConf {
 public:
  TrainConf(const ::std::shared_ptr<_TrainConf_>& data);
  TrainConf(const TrainConf& other);
  // enable nothrow for ::std::vector<TrainConf> resize 
  TrainConf(TrainConf&&) noexcept;
  TrainConf();
  explicit TrainConf(const ::oneflow::TrainConf& proto_trainconf);

  ~TrainConf() override;

  void InitFromProto(const PbMessage& proto_trainconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TrainConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TrainConf& other) const;
  void Clear();
  void CopyFrom(const TrainConf& other);
  TrainConf& operator=(const TrainConf& other);

  // repeated field optimizer_conf
 public:
  void clear_optimizer_conf();
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_* mutable_optimizer_conf();
  ::oneflow::cfg::OptimizerConf* mutable_optimizer_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> shared_mutable_optimizer_conf();
  ::std::shared_ptr<::oneflow::cfg::OptimizerConf> shared_mutable_optimizer_conf(::std::size_t index);
  ::oneflow::cfg::OptimizerConf* add_optimizer_conf();
  // repeated field loss_lbn
 public:
  void clear_loss_lbn();
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* mutable_loss_lbn();
  ::std::string* mutable_loss_lbn(::std::size_t index);
  void add_loss_lbn(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_loss_lbn();
  void set_loss_lbn(::std::size_t index, const ::std::string& value);
  // required or optional field train_step_lbn
 public:
  void clear_train_step_lbn();
  void set_train_step_lbn(const ::std::string& value);
  ::std::string* mutable_train_step_lbn();
  void clear_loss_scale_factor();
  void set_loss_scale_factor(const float& value);
  float* mutable_loss_scale_factor();
  void clear_dynamic_loss_scale_policy();
  ::oneflow::cfg::DynamicLossScalePolicy* mutable_dynamic_loss_scale_policy();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy> shared_mutable_dynamic_loss_scale_policy();
  // required or optional field model_update_conf
 public:
  void clear_model_update_conf();
  ::oneflow::cfg::NormalModelUpdateOpUserConf* mutable_model_update_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::NormalModelUpdateOpUserConf> shared_mutable_model_update_conf();
  // required or optional field primary_lr
 public:
  void clear_primary_lr();
  void set_primary_lr(const float& value);
  float* mutable_primary_lr();
  // required or optional field secondary_lr
 public:
  void clear_secondary_lr();
  void set_secondary_lr(const float& value);
  float* mutable_secondary_lr();
  // required or optional field primary_lr_lbn
 public:
  void clear_primary_lr_lbn();
  void set_primary_lr_lbn(const ::std::string& value);
  ::std::string* mutable_primary_lr_lbn();
  // required or optional field secondary_lr_lbn
 public:
  void clear_secondary_lr_lbn();
  void set_secondary_lr_lbn(const ::std::string& value);
  ::std::string* mutable_secondary_lr_lbn();

  ::std::shared_ptr<TrainConf> __SharedMutable__();
};


class ConstPredictConf : public ::oneflow::cfg::Message {
 public:

  class _PredictConf_ {
   public:
    _PredictConf_();
    explicit _PredictConf_(const _PredictConf_& other);
    explicit _PredictConf_(_PredictConf_&& other);
    _PredictConf_(const ::oneflow::PredictConf& proto_predictconf);
    ~_PredictConf_();

    void InitFromProto(const ::oneflow::PredictConf& proto_predictconf);

    void ToProto(::oneflow::PredictConf* proto_predictconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PredictConf_& other);
       
   public:
    int compare(const _PredictConf_& other);

    bool operator==(const _PredictConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PredictConf_& other) const;
  };

  ConstPredictConf(const ::std::shared_ptr<_PredictConf_>& data);
  ConstPredictConf(const ConstPredictConf&);
  ConstPredictConf(ConstPredictConf&&) noexcept;
  ConstPredictConf();
  ConstPredictConf(const ::oneflow::PredictConf& proto_predictconf);
  virtual ~ConstPredictConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_predictconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstPredictConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPredictConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPredictConf& other) const;
 protected:
  const ::std::shared_ptr<_PredictConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PredictConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPredictConf
  void BuildFromProto(const PbMessage& proto_predictconf);
  
  ::std::shared_ptr<_PredictConf_> data_;
};

class PredictConf final : public ConstPredictConf {
 public:
  PredictConf(const ::std::shared_ptr<_PredictConf_>& data);
  PredictConf(const PredictConf& other);
  // enable nothrow for ::std::vector<PredictConf> resize 
  PredictConf(PredictConf&&) noexcept;
  PredictConf();
  explicit PredictConf(const ::oneflow::PredictConf& proto_predictconf);

  ~PredictConf() override;

  void InitFromProto(const PbMessage& proto_predictconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PredictConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PredictConf& other) const;
  void Clear();
  void CopyFrom(const PredictConf& other);
  PredictConf& operator=(const PredictConf& other);


  ::std::shared_ptr<PredictConf> __SharedMutable__();
};


class ConstMemoryAllocationAlgorithmConf : public ::oneflow::cfg::Message {
 public:

  class _MemoryAllocationAlgorithmConf_ {
   public:
    _MemoryAllocationAlgorithmConf_();
    explicit _MemoryAllocationAlgorithmConf_(const _MemoryAllocationAlgorithmConf_& other);
    explicit _MemoryAllocationAlgorithmConf_(_MemoryAllocationAlgorithmConf_&& other);
    _MemoryAllocationAlgorithmConf_(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf);
    ~_MemoryAllocationAlgorithmConf_();

    void InitFromProto(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf);

    void ToProto(::oneflow::MemoryAllocationAlgorithmConf* proto_memoryallocationalgorithmconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MemoryAllocationAlgorithmConf_& other);
  
      // optional field use_mem_size_first_algo
     public:
    bool has_use_mem_size_first_algo() const;
    const bool& use_mem_size_first_algo() const;
    void clear_use_mem_size_first_algo();
    void set_use_mem_size_first_algo(const bool& value);
    bool* mutable_use_mem_size_first_algo();
   protected:
    bool has_use_mem_size_first_algo_ = false;
    bool use_mem_size_first_algo_;
      
      // optional field use_mutual_exclusion_first_algo
     public:
    bool has_use_mutual_exclusion_first_algo() const;
    const bool& use_mutual_exclusion_first_algo() const;
    void clear_use_mutual_exclusion_first_algo();
    void set_use_mutual_exclusion_first_algo(const bool& value);
    bool* mutable_use_mutual_exclusion_first_algo();
   protected:
    bool has_use_mutual_exclusion_first_algo_ = false;
    bool use_mutual_exclusion_first_algo_;
      
      // optional field use_time_line_algo
     public:
    bool has_use_time_line_algo() const;
    const bool& use_time_line_algo() const;
    void clear_use_time_line_algo();
    void set_use_time_line_algo(const bool& value);
    bool* mutable_use_time_line_algo();
   protected:
    bool has_use_time_line_algo_ = false;
    bool use_time_line_algo_;
           
   public:
    int compare(const _MemoryAllocationAlgorithmConf_& other);

    bool operator==(const _MemoryAllocationAlgorithmConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MemoryAllocationAlgorithmConf_& other) const;
  };

  ConstMemoryAllocationAlgorithmConf(const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& data);
  ConstMemoryAllocationAlgorithmConf(const ConstMemoryAllocationAlgorithmConf&);
  ConstMemoryAllocationAlgorithmConf(ConstMemoryAllocationAlgorithmConf&&) noexcept;
  ConstMemoryAllocationAlgorithmConf();
  ConstMemoryAllocationAlgorithmConf(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf);
  virtual ~ConstMemoryAllocationAlgorithmConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_memoryallocationalgorithmconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field use_mem_size_first_algo
 public:
  bool has_use_mem_size_first_algo() const;
  const bool& use_mem_size_first_algo() const;
  // used by pybind11 only
  // required or optional field use_mutual_exclusion_first_algo
 public:
  bool has_use_mutual_exclusion_first_algo() const;
  const bool& use_mutual_exclusion_first_algo() const;
  // used by pybind11 only
  // required or optional field use_time_line_algo
 public:
  bool has_use_time_line_algo() const;
  const bool& use_time_line_algo() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstMemoryAllocationAlgorithmConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMemoryAllocationAlgorithmConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMemoryAllocationAlgorithmConf& other) const;
 protected:
  const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMemoryAllocationAlgorithmConf
  void BuildFromProto(const PbMessage& proto_memoryallocationalgorithmconf);
  
  ::std::shared_ptr<_MemoryAllocationAlgorithmConf_> data_;
};

class MemoryAllocationAlgorithmConf final : public ConstMemoryAllocationAlgorithmConf {
 public:
  MemoryAllocationAlgorithmConf(const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& data);
  MemoryAllocationAlgorithmConf(const MemoryAllocationAlgorithmConf& other);
  // enable nothrow for ::std::vector<MemoryAllocationAlgorithmConf> resize 
  MemoryAllocationAlgorithmConf(MemoryAllocationAlgorithmConf&&) noexcept;
  MemoryAllocationAlgorithmConf();
  explicit MemoryAllocationAlgorithmConf(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf);

  ~MemoryAllocationAlgorithmConf() override;

  void InitFromProto(const PbMessage& proto_memoryallocationalgorithmconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MemoryAllocationAlgorithmConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MemoryAllocationAlgorithmConf& other) const;
  void Clear();
  void CopyFrom(const MemoryAllocationAlgorithmConf& other);
  MemoryAllocationAlgorithmConf& operator=(const MemoryAllocationAlgorithmConf& other);

  // required or optional field use_mem_size_first_algo
 public:
  void clear_use_mem_size_first_algo();
  void set_use_mem_size_first_algo(const bool& value);
  bool* mutable_use_mem_size_first_algo();
  // required or optional field use_mutual_exclusion_first_algo
 public:
  void clear_use_mutual_exclusion_first_algo();
  void set_use_mutual_exclusion_first_algo(const bool& value);
  bool* mutable_use_mutual_exclusion_first_algo();
  // required or optional field use_time_line_algo
 public:
  void clear_use_time_line_algo();
  void set_use_time_line_algo(const bool& value);
  bool* mutable_use_time_line_algo();

  ::std::shared_ptr<MemoryAllocationAlgorithmConf> __SharedMutable__();
};


class ConstXrtConfig_XlaConfig : public ::oneflow::cfg::Message {
 public:

  class _XrtConfig_XlaConfig_ {
   public:
    _XrtConfig_XlaConfig_();
    explicit _XrtConfig_XlaConfig_(const _XrtConfig_XlaConfig_& other);
    explicit _XrtConfig_XlaConfig_(_XrtConfig_XlaConfig_&& other);
    _XrtConfig_XlaConfig_(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig);
    ~_XrtConfig_XlaConfig_();

    void InitFromProto(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig);

    void ToProto(::oneflow::XrtConfig_XlaConfig* proto_xrtconfig_xlaconfig) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtConfig_XlaConfig_& other);
       
   public:
    int compare(const _XrtConfig_XlaConfig_& other);

    bool operator==(const _XrtConfig_XlaConfig_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtConfig_XlaConfig_& other) const;
  };

  ConstXrtConfig_XlaConfig(const ::std::shared_ptr<_XrtConfig_XlaConfig_>& data);
  ConstXrtConfig_XlaConfig(const ConstXrtConfig_XlaConfig&);
  ConstXrtConfig_XlaConfig(ConstXrtConfig_XlaConfig&&) noexcept;
  ConstXrtConfig_XlaConfig();
  ConstXrtConfig_XlaConfig(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig);
  virtual ~ConstXrtConfig_XlaConfig() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtconfig_xlaconfig) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstXrtConfig_XlaConfig> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtConfig_XlaConfig& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtConfig_XlaConfig& other) const;
 protected:
  const ::std::shared_ptr<_XrtConfig_XlaConfig_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtConfig_XlaConfig_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig_XlaConfig
  void BuildFromProto(const PbMessage& proto_xrtconfig_xlaconfig);
  
  ::std::shared_ptr<_XrtConfig_XlaConfig_> data_;
};

class XrtConfig_XlaConfig final : public ConstXrtConfig_XlaConfig {
 public:
  XrtConfig_XlaConfig(const ::std::shared_ptr<_XrtConfig_XlaConfig_>& data);
  XrtConfig_XlaConfig(const XrtConfig_XlaConfig& other);
  // enable nothrow for ::std::vector<XrtConfig_XlaConfig> resize 
  XrtConfig_XlaConfig(XrtConfig_XlaConfig&&) noexcept;
  XrtConfig_XlaConfig();
  explicit XrtConfig_XlaConfig(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig);

  ~XrtConfig_XlaConfig() override;

  void InitFromProto(const PbMessage& proto_xrtconfig_xlaconfig) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtConfig_XlaConfig& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtConfig_XlaConfig& other) const;
  void Clear();
  void CopyFrom(const XrtConfig_XlaConfig& other);
  XrtConfig_XlaConfig& operator=(const XrtConfig_XlaConfig& other);


  ::std::shared_ptr<XrtConfig_XlaConfig> __SharedMutable__();
};


class ConstXrtConfig_TensorRTConfig : public ::oneflow::cfg::Message {
 public:

  class _XrtConfig_TensorRTConfig_ {
   public:
    _XrtConfig_TensorRTConfig_();
    explicit _XrtConfig_TensorRTConfig_(const _XrtConfig_TensorRTConfig_& other);
    explicit _XrtConfig_TensorRTConfig_(_XrtConfig_TensorRTConfig_&& other);
    _XrtConfig_TensorRTConfig_(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig);
    ~_XrtConfig_TensorRTConfig_();

    void InitFromProto(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig);

    void ToProto(::oneflow::XrtConfig_TensorRTConfig* proto_xrtconfig_tensorrtconfig) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtConfig_TensorRTConfig_& other);
  
      // optional field use_fp16
     public:
    bool has_use_fp16() const;
    const bool& use_fp16() const;
    void clear_use_fp16();
    void set_use_fp16(const bool& value);
    bool* mutable_use_fp16();
   protected:
    bool has_use_fp16_ = false;
    bool use_fp16_;
      
      // optional field use_int8
     public:
    bool has_use_int8() const;
    const bool& use_int8() const;
    void clear_use_int8();
    void set_use_int8(const bool& value);
    bool* mutable_use_int8();
   protected:
    bool has_use_int8_ = false;
    bool use_int8_;
      
      // optional field int8_calibration
     public:
    bool has_int8_calibration() const;
    const ::std::string& int8_calibration() const;
    void clear_int8_calibration();
    void set_int8_calibration(const ::std::string& value);
    ::std::string* mutable_int8_calibration();
   protected:
    bool has_int8_calibration_ = false;
    ::std::string int8_calibration_;
           
   public:
    int compare(const _XrtConfig_TensorRTConfig_& other);

    bool operator==(const _XrtConfig_TensorRTConfig_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtConfig_TensorRTConfig_& other) const;
  };

  ConstXrtConfig_TensorRTConfig(const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& data);
  ConstXrtConfig_TensorRTConfig(const ConstXrtConfig_TensorRTConfig&);
  ConstXrtConfig_TensorRTConfig(ConstXrtConfig_TensorRTConfig&&) noexcept;
  ConstXrtConfig_TensorRTConfig();
  ConstXrtConfig_TensorRTConfig(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig);
  virtual ~ConstXrtConfig_TensorRTConfig() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtconfig_tensorrtconfig) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field use_fp16
 public:
  bool has_use_fp16() const;
  const bool& use_fp16() const;
  // used by pybind11 only
  // required or optional field use_int8
 public:
  bool has_use_int8() const;
  const bool& use_int8() const;
  // used by pybind11 only
  // required or optional field int8_calibration
 public:
  bool has_int8_calibration() const;
  const ::std::string& int8_calibration() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstXrtConfig_TensorRTConfig> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtConfig_TensorRTConfig& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtConfig_TensorRTConfig& other) const;
 protected:
  const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig_TensorRTConfig
  void BuildFromProto(const PbMessage& proto_xrtconfig_tensorrtconfig);
  
  ::std::shared_ptr<_XrtConfig_TensorRTConfig_> data_;
};

class XrtConfig_TensorRTConfig final : public ConstXrtConfig_TensorRTConfig {
 public:
  XrtConfig_TensorRTConfig(const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& data);
  XrtConfig_TensorRTConfig(const XrtConfig_TensorRTConfig& other);
  // enable nothrow for ::std::vector<XrtConfig_TensorRTConfig> resize 
  XrtConfig_TensorRTConfig(XrtConfig_TensorRTConfig&&) noexcept;
  XrtConfig_TensorRTConfig();
  explicit XrtConfig_TensorRTConfig(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig);

  ~XrtConfig_TensorRTConfig() override;

  void InitFromProto(const PbMessage& proto_xrtconfig_tensorrtconfig) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtConfig_TensorRTConfig& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtConfig_TensorRTConfig& other) const;
  void Clear();
  void CopyFrom(const XrtConfig_TensorRTConfig& other);
  XrtConfig_TensorRTConfig& operator=(const XrtConfig_TensorRTConfig& other);

  // required or optional field use_fp16
 public:
  void clear_use_fp16();
  void set_use_fp16(const bool& value);
  bool* mutable_use_fp16();
  // required or optional field use_int8
 public:
  void clear_use_int8();
  void set_use_int8(const bool& value);
  bool* mutable_use_int8();
  // required or optional field int8_calibration
 public:
  void clear_int8_calibration();
  void set_int8_calibration(const ::std::string& value);
  ::std::string* mutable_int8_calibration();

  ::std::shared_ptr<XrtConfig_TensorRTConfig> __SharedMutable__();
};


class ConstXrtConfig : public ::oneflow::cfg::Message {
 public:

  class _XrtConfig_ {
   public:
    _XrtConfig_();
    explicit _XrtConfig_(const _XrtConfig_& other);
    explicit _XrtConfig_(_XrtConfig_&& other);
    _XrtConfig_(const ::oneflow::XrtConfig& proto_xrtconfig);
    ~_XrtConfig_();

    void InitFromProto(const ::oneflow::XrtConfig& proto_xrtconfig);

    void ToProto(::oneflow::XrtConfig* proto_xrtconfig) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtConfig_& other);
  
      // optional field use_xla_jit
     public:
    bool has_use_xla_jit() const;
    const bool& use_xla_jit() const;
    void clear_use_xla_jit();
    void set_use_xla_jit(const bool& value);
    bool* mutable_use_xla_jit();
   protected:
    bool has_use_xla_jit_ = false;
    bool use_xla_jit_;
      
      // optional field use_tensorrt
     public:
    bool has_use_tensorrt() const;
    const bool& use_tensorrt() const;
    void clear_use_tensorrt();
    void set_use_tensorrt(const bool& value);
    bool* mutable_use_tensorrt();
   protected:
    bool has_use_tensorrt_ = false;
    bool use_tensorrt_;
      
      // optional field xla_config
     public:
    bool has_xla_config() const;
    const ::oneflow::cfg::XrtConfig_XlaConfig& xla_config() const;
    void clear_xla_config();
    ::oneflow::cfg::XrtConfig_XlaConfig* mutable_xla_config();
   protected:
    bool has_xla_config_ = false;
    ::std::shared_ptr<::oneflow::cfg::XrtConfig_XlaConfig> xla_config_;
      
      // optional field tensorrt_config
     public:
    bool has_tensorrt_config() const;
    const ::oneflow::cfg::XrtConfig_TensorRTConfig& tensorrt_config() const;
    void clear_tensorrt_config();
    ::oneflow::cfg::XrtConfig_TensorRTConfig* mutable_tensorrt_config();
   protected:
    bool has_tensorrt_config_ = false;
    ::std::shared_ptr<::oneflow::cfg::XrtConfig_TensorRTConfig> tensorrt_config_;
           
   public:
    int compare(const _XrtConfig_& other);

    bool operator==(const _XrtConfig_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtConfig_& other) const;
  };

  ConstXrtConfig(const ::std::shared_ptr<_XrtConfig_>& data);
  ConstXrtConfig(const ConstXrtConfig&);
  ConstXrtConfig(ConstXrtConfig&&) noexcept;
  ConstXrtConfig();
  ConstXrtConfig(const ::oneflow::XrtConfig& proto_xrtconfig);
  virtual ~ConstXrtConfig() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtconfig) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field use_xla_jit
 public:
  bool has_use_xla_jit() const;
  const bool& use_xla_jit() const;
  // used by pybind11 only
  // required or optional field use_tensorrt
 public:
  bool has_use_tensorrt() const;
  const bool& use_tensorrt() const;
  // used by pybind11 only
  // required or optional field xla_config
 public:
  bool has_xla_config() const;
  const ::oneflow::cfg::XrtConfig_XlaConfig& xla_config() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXrtConfig_XlaConfig> shared_const_xla_config() const;
  // required or optional field tensorrt_config
 public:
  bool has_tensorrt_config() const;
  const ::oneflow::cfg::XrtConfig_TensorRTConfig& tensorrt_config() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXrtConfig_TensorRTConfig> shared_const_tensorrt_config() const;

 public:
  ::std::shared_ptr<ConstXrtConfig> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtConfig& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtConfig& other) const;
 protected:
  const ::std::shared_ptr<_XrtConfig_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtConfig_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig
  void BuildFromProto(const PbMessage& proto_xrtconfig);
  
  ::std::shared_ptr<_XrtConfig_> data_;
};

class XrtConfig final : public ConstXrtConfig {
 public:
  XrtConfig(const ::std::shared_ptr<_XrtConfig_>& data);
  XrtConfig(const XrtConfig& other);
  // enable nothrow for ::std::vector<XrtConfig> resize 
  XrtConfig(XrtConfig&&) noexcept;
  XrtConfig();
  explicit XrtConfig(const ::oneflow::XrtConfig& proto_xrtconfig);

  ~XrtConfig() override;

  void InitFromProto(const PbMessage& proto_xrtconfig) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtConfig& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtConfig& other) const;
  void Clear();
  void CopyFrom(const XrtConfig& other);
  XrtConfig& operator=(const XrtConfig& other);

  // required or optional field use_xla_jit
 public:
  void clear_use_xla_jit();
  void set_use_xla_jit(const bool& value);
  bool* mutable_use_xla_jit();
  // required or optional field use_tensorrt
 public:
  void clear_use_tensorrt();
  void set_use_tensorrt(const bool& value);
  bool* mutable_use_tensorrt();
  // required or optional field xla_config
 public:
  void clear_xla_config();
  ::oneflow::cfg::XrtConfig_XlaConfig* mutable_xla_config();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XrtConfig_XlaConfig> shared_mutable_xla_config();
  // required or optional field tensorrt_config
 public:
  void clear_tensorrt_config();
  ::oneflow::cfg::XrtConfig_TensorRTConfig* mutable_tensorrt_config();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XrtConfig_TensorRTConfig> shared_mutable_tensorrt_config();

  ::std::shared_ptr<XrtConfig> __SharedMutable__();
};


class ConstQatConfig : public ::oneflow::cfg::Message {
 public:

  class _QatConfig_ {
   public:
    _QatConfig_();
    explicit _QatConfig_(const _QatConfig_& other);
    explicit _QatConfig_(_QatConfig_&& other);
    _QatConfig_(const ::oneflow::QatConfig& proto_qatconfig);
    ~_QatConfig_();

    void InitFromProto(const ::oneflow::QatConfig& proto_qatconfig);

    void ToProto(::oneflow::QatConfig* proto_qatconfig) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _QatConfig_& other);
  
      // optional field per_channel_weight_quantization
     public:
    bool has_per_channel_weight_quantization() const;
    const bool& per_channel_weight_quantization() const;
    void clear_per_channel_weight_quantization();
    void set_per_channel_weight_quantization(const bool& value);
    bool* mutable_per_channel_weight_quantization();
   protected:
    bool has_per_channel_weight_quantization_ = false;
    bool per_channel_weight_quantization_;
      
      // optional field symmetric
     public:
    bool has_symmetric() const;
    const bool& symmetric() const;
    void clear_symmetric();
    void set_symmetric(const bool& value);
    bool* mutable_symmetric();
   protected:
    bool has_symmetric_ = false;
    bool symmetric_;
      
      // optional field moving_min_max_momentum
     public:
    bool has_moving_min_max_momentum() const;
    const float& moving_min_max_momentum() const;
    void clear_moving_min_max_momentum();
    void set_moving_min_max_momentum(const float& value);
    float* mutable_moving_min_max_momentum();
   protected:
    bool has_moving_min_max_momentum_ = false;
    float moving_min_max_momentum_;
      
      // optional field moving_min_max_stop_update_after_iters
     public:
    bool has_moving_min_max_stop_update_after_iters() const;
    const int64_t& moving_min_max_stop_update_after_iters() const;
    void clear_moving_min_max_stop_update_after_iters();
    void set_moving_min_max_stop_update_after_iters(const int64_t& value);
    int64_t* mutable_moving_min_max_stop_update_after_iters();
   protected:
    bool has_moving_min_max_stop_update_after_iters_ = false;
    int64_t moving_min_max_stop_update_after_iters_;
      
      // optional field target_backend
     public:
    bool has_target_backend() const;
    const ::std::string& target_backend() const;
    void clear_target_backend();
    void set_target_backend(const ::std::string& value);
    ::std::string* mutable_target_backend();
   protected:
    bool has_target_backend_ = false;
    ::std::string target_backend_;
           
   public:
    int compare(const _QatConfig_& other);

    bool operator==(const _QatConfig_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _QatConfig_& other) const;
  };

  ConstQatConfig(const ::std::shared_ptr<_QatConfig_>& data);
  ConstQatConfig(const ConstQatConfig&);
  ConstQatConfig(ConstQatConfig&&) noexcept;
  ConstQatConfig();
  ConstQatConfig(const ::oneflow::QatConfig& proto_qatconfig);
  virtual ~ConstQatConfig() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_qatconfig) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field per_channel_weight_quantization
 public:
  bool has_per_channel_weight_quantization() const;
  const bool& per_channel_weight_quantization() const;
  // used by pybind11 only
  // required or optional field symmetric
 public:
  bool has_symmetric() const;
  const bool& symmetric() const;
  // used by pybind11 only
  // required or optional field moving_min_max_momentum
 public:
  bool has_moving_min_max_momentum() const;
  const float& moving_min_max_momentum() const;
  // used by pybind11 only
  // required or optional field moving_min_max_stop_update_after_iters
 public:
  bool has_moving_min_max_stop_update_after_iters() const;
  const int64_t& moving_min_max_stop_update_after_iters() const;
  // used by pybind11 only
  // required or optional field target_backend
 public:
  bool has_target_backend() const;
  const ::std::string& target_backend() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstQatConfig> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstQatConfig& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstQatConfig& other) const;
 protected:
  const ::std::shared_ptr<_QatConfig_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_QatConfig_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstQatConfig
  void BuildFromProto(const PbMessage& proto_qatconfig);
  
  ::std::shared_ptr<_QatConfig_> data_;
};

class QatConfig final : public ConstQatConfig {
 public:
  QatConfig(const ::std::shared_ptr<_QatConfig_>& data);
  QatConfig(const QatConfig& other);
  // enable nothrow for ::std::vector<QatConfig> resize 
  QatConfig(QatConfig&&) noexcept;
  QatConfig();
  explicit QatConfig(const ::oneflow::QatConfig& proto_qatconfig);

  ~QatConfig() override;

  void InitFromProto(const PbMessage& proto_qatconfig) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const QatConfig& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const QatConfig& other) const;
  void Clear();
  void CopyFrom(const QatConfig& other);
  QatConfig& operator=(const QatConfig& other);

  // required or optional field per_channel_weight_quantization
 public:
  void clear_per_channel_weight_quantization();
  void set_per_channel_weight_quantization(const bool& value);
  bool* mutable_per_channel_weight_quantization();
  // required or optional field symmetric
 public:
  void clear_symmetric();
  void set_symmetric(const bool& value);
  bool* mutable_symmetric();
  // required or optional field moving_min_max_momentum
 public:
  void clear_moving_min_max_momentum();
  void set_moving_min_max_momentum(const float& value);
  float* mutable_moving_min_max_momentum();
  // required or optional field moving_min_max_stop_update_after_iters
 public:
  void clear_moving_min_max_stop_update_after_iters();
  void set_moving_min_max_stop_update_after_iters(const int64_t& value);
  int64_t* mutable_moving_min_max_stop_update_after_iters();
  // required or optional field target_backend
 public:
  void clear_target_backend();
  void set_target_backend(const ::std::string& value);
  ::std::string* mutable_target_backend();

  ::std::shared_ptr<QatConfig> __SharedMutable__();
};


class ConstIndexedSlicesOptimizerConf : public ::oneflow::cfg::Message {
 public:

  class _IndexedSlicesOptimizerConf_ {
   public:
    _IndexedSlicesOptimizerConf_();
    explicit _IndexedSlicesOptimizerConf_(const _IndexedSlicesOptimizerConf_& other);
    explicit _IndexedSlicesOptimizerConf_(_IndexedSlicesOptimizerConf_&& other);
    _IndexedSlicesOptimizerConf_(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf);
    ~_IndexedSlicesOptimizerConf_();

    void InitFromProto(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf);

    void ToProto(::oneflow::IndexedSlicesOptimizerConf* proto_indexedslicesoptimizerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _IndexedSlicesOptimizerConf_& other);
  
      // optional field enable
     public:
    bool has_enable() const;
    const bool& enable() const;
    void clear_enable();
    void set_enable(const bool& value);
    bool* mutable_enable();
   protected:
    bool has_enable_ = false;
    bool enable_;
      
      // optional field include_op_names
     public:
    bool has_include_op_names() const;
    const ::oneflow::cfg::OpNameSet& include_op_names() const;
    void clear_include_op_names();
    ::oneflow::cfg::OpNameSet* mutable_include_op_names();
   protected:
    bool has_include_op_names_ = false;
    ::std::shared_ptr<::oneflow::cfg::OpNameSet> include_op_names_;
           
   public:
    int compare(const _IndexedSlicesOptimizerConf_& other);

    bool operator==(const _IndexedSlicesOptimizerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _IndexedSlicesOptimizerConf_& other) const;
  };

  ConstIndexedSlicesOptimizerConf(const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& data);
  ConstIndexedSlicesOptimizerConf(const ConstIndexedSlicesOptimizerConf&);
  ConstIndexedSlicesOptimizerConf(ConstIndexedSlicesOptimizerConf&&) noexcept;
  ConstIndexedSlicesOptimizerConf();
  ConstIndexedSlicesOptimizerConf(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf);
  virtual ~ConstIndexedSlicesOptimizerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_indexedslicesoptimizerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field enable
 public:
  bool has_enable() const;
  const bool& enable() const;
  // used by pybind11 only
  // required or optional field include_op_names
 public:
  bool has_include_op_names() const;
  const ::oneflow::cfg::OpNameSet& include_op_names() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpNameSet> shared_const_include_op_names() const;

 public:
  ::std::shared_ptr<ConstIndexedSlicesOptimizerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstIndexedSlicesOptimizerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstIndexedSlicesOptimizerConf& other) const;
 protected:
  const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstIndexedSlicesOptimizerConf
  void BuildFromProto(const PbMessage& proto_indexedslicesoptimizerconf);
  
  ::std::shared_ptr<_IndexedSlicesOptimizerConf_> data_;
};

class IndexedSlicesOptimizerConf final : public ConstIndexedSlicesOptimizerConf {
 public:
  IndexedSlicesOptimizerConf(const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& data);
  IndexedSlicesOptimizerConf(const IndexedSlicesOptimizerConf& other);
  // enable nothrow for ::std::vector<IndexedSlicesOptimizerConf> resize 
  IndexedSlicesOptimizerConf(IndexedSlicesOptimizerConf&&) noexcept;
  IndexedSlicesOptimizerConf();
  explicit IndexedSlicesOptimizerConf(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf);

  ~IndexedSlicesOptimizerConf() override;

  void InitFromProto(const PbMessage& proto_indexedslicesoptimizerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const IndexedSlicesOptimizerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const IndexedSlicesOptimizerConf& other) const;
  void Clear();
  void CopyFrom(const IndexedSlicesOptimizerConf& other);
  IndexedSlicesOptimizerConf& operator=(const IndexedSlicesOptimizerConf& other);

  // required or optional field enable
 public:
  void clear_enable();
  void set_enable(const bool& value);
  bool* mutable_enable();
  // required or optional field include_op_names
 public:
  void clear_include_op_names();
  ::oneflow::cfg::OpNameSet* mutable_include_op_names();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpNameSet> shared_mutable_include_op_names();

  ::std::shared_ptr<IndexedSlicesOptimizerConf> __SharedMutable__();
};


class ConstParallelBlobConf : public ::oneflow::cfg::Message {
 public:

  class _ParallelBlobConf_ {
   public:
    _ParallelBlobConf_();
    explicit _ParallelBlobConf_(const _ParallelBlobConf_& other);
    explicit _ParallelBlobConf_(_ParallelBlobConf_&& other);
    _ParallelBlobConf_(const ::oneflow::ParallelBlobConf& proto_parallelblobconf);
    ~_ParallelBlobConf_();

    void InitFromProto(const ::oneflow::ParallelBlobConf& proto_parallelblobconf);

    void ToProto(::oneflow::ParallelBlobConf* proto_parallelblobconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelBlobConf_& other);
  
      // optional field logical_blob_desc_conf
     public:
    bool has_logical_blob_desc_conf() const;
    const ::oneflow::cfg::BlobDescProto& logical_blob_desc_conf() const;
    void clear_logical_blob_desc_conf();
    ::oneflow::cfg::BlobDescProto* mutable_logical_blob_desc_conf();
   protected:
    bool has_logical_blob_desc_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::BlobDescProto> logical_blob_desc_conf_;
      
      // optional field parallel_conf
     public:
    bool has_parallel_conf() const;
    const ::oneflow::cfg::ParallelConf& parallel_conf() const;
    void clear_parallel_conf();
    ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
   protected:
    bool has_parallel_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelConf> parallel_conf_;
      
      // optional field parallel_distribution
     public:
    bool has_parallel_distribution() const;
    const ::oneflow::cfg::ParallelDistribution& parallel_distribution() const;
    void clear_parallel_distribution();
    ::oneflow::cfg::ParallelDistribution* mutable_parallel_distribution();
   protected:
    bool has_parallel_distribution_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> parallel_distribution_;
           
   public:
    int compare(const _ParallelBlobConf_& other);

    bool operator==(const _ParallelBlobConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelBlobConf_& other) const;
  };

  ConstParallelBlobConf(const ::std::shared_ptr<_ParallelBlobConf_>& data);
  ConstParallelBlobConf(const ConstParallelBlobConf&);
  ConstParallelBlobConf(ConstParallelBlobConf&&) noexcept;
  ConstParallelBlobConf();
  ConstParallelBlobConf(const ::oneflow::ParallelBlobConf& proto_parallelblobconf);
  virtual ~ConstParallelBlobConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_parallelblobconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field logical_blob_desc_conf
 public:
  bool has_logical_blob_desc_conf() const;
  const ::oneflow::cfg::BlobDescProto& logical_blob_desc_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobDescProto> shared_const_logical_blob_desc_conf() const;
  // required or optional field parallel_conf
 public:
  bool has_parallel_conf() const;
  const ::oneflow::cfg::ParallelConf& parallel_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConf> shared_const_parallel_conf() const;
  // required or optional field parallel_distribution
 public:
  bool has_parallel_distribution() const;
  const ::oneflow::cfg::ParallelDistribution& parallel_distribution() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelDistribution> shared_const_parallel_distribution() const;

 public:
  ::std::shared_ptr<ConstParallelBlobConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelBlobConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelBlobConf& other) const;
 protected:
  const ::std::shared_ptr<_ParallelBlobConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelBlobConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelBlobConf
  void BuildFromProto(const PbMessage& proto_parallelblobconf);
  
  ::std::shared_ptr<_ParallelBlobConf_> data_;
};

class ParallelBlobConf final : public ConstParallelBlobConf {
 public:
  ParallelBlobConf(const ::std::shared_ptr<_ParallelBlobConf_>& data);
  ParallelBlobConf(const ParallelBlobConf& other);
  // enable nothrow for ::std::vector<ParallelBlobConf> resize 
  ParallelBlobConf(ParallelBlobConf&&) noexcept;
  ParallelBlobConf();
  explicit ParallelBlobConf(const ::oneflow::ParallelBlobConf& proto_parallelblobconf);

  ~ParallelBlobConf() override;

  void InitFromProto(const PbMessage& proto_parallelblobconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelBlobConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelBlobConf& other) const;
  void Clear();
  void CopyFrom(const ParallelBlobConf& other);
  ParallelBlobConf& operator=(const ParallelBlobConf& other);

  // required or optional field logical_blob_desc_conf
 public:
  void clear_logical_blob_desc_conf();
  ::oneflow::cfg::BlobDescProto* mutable_logical_blob_desc_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobDescProto> shared_mutable_logical_blob_desc_conf();
  // required or optional field parallel_conf
 public:
  void clear_parallel_conf();
  ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConf> shared_mutable_parallel_conf();
  // required or optional field parallel_distribution
 public:
  void clear_parallel_distribution();
  ::oneflow::cfg::ParallelDistribution* mutable_parallel_distribution();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> shared_mutable_parallel_distribution();

  ::std::shared_ptr<ParallelBlobConf> __SharedMutable__();
};


class ConstJobInputDef : public ::oneflow::cfg::Message {
 public:

  class _JobInputDef_ {
   public:
    _JobInputDef_();
    explicit _JobInputDef_(const _JobInputDef_& other);
    explicit _JobInputDef_(_JobInputDef_&& other);
    _JobInputDef_(const ::oneflow::JobInputDef& proto_jobinputdef);
    ~_JobInputDef_();

    void InitFromProto(const ::oneflow::JobInputDef& proto_jobinputdef);

    void ToProto(::oneflow::JobInputDef* proto_jobinputdef) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobInputDef_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field blob_conf
     public:
    bool has_blob_conf() const;
    const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
    void clear_blob_conf();
    ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
   protected:
    bool has_blob_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> blob_conf_;
           
   public:
    int compare(const _JobInputDef_& other);

    bool operator==(const _JobInputDef_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobInputDef_& other) const;
  };

  ConstJobInputDef(const ::std::shared_ptr<_JobInputDef_>& data);
  ConstJobInputDef(const ConstJobInputDef&);
  ConstJobInputDef(ConstJobInputDef&&) noexcept;
  ConstJobInputDef();
  ConstJobInputDef(const ::oneflow::JobInputDef& proto_jobinputdef);
  virtual ~ConstJobInputDef() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobinputdef) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field blob_conf
 public:
  bool has_blob_conf() const;
  const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInterfaceBlobConf> shared_const_blob_conf() const;

 public:
  ::std::shared_ptr<ConstJobInputDef> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobInputDef& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobInputDef& other) const;
 protected:
  const ::std::shared_ptr<_JobInputDef_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobInputDef_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobInputDef
  void BuildFromProto(const PbMessage& proto_jobinputdef);
  
  ::std::shared_ptr<_JobInputDef_> data_;
};

class JobInputDef final : public ConstJobInputDef {
 public:
  JobInputDef(const ::std::shared_ptr<_JobInputDef_>& data);
  JobInputDef(const JobInputDef& other);
  // enable nothrow for ::std::vector<JobInputDef> resize 
  JobInputDef(JobInputDef&&) noexcept;
  JobInputDef();
  explicit JobInputDef(const ::oneflow::JobInputDef& proto_jobinputdef);

  ~JobInputDef() override;

  void InitFromProto(const PbMessage& proto_jobinputdef) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobInputDef& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobInputDef& other) const;
  void Clear();
  void CopyFrom(const JobInputDef& other);
  JobInputDef& operator=(const JobInputDef& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field blob_conf
 public:
  void clear_blob_conf();
  ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> shared_mutable_blob_conf();

  ::std::shared_ptr<JobInputDef> __SharedMutable__();
};


class ConstJobOutputDef : public ::oneflow::cfg::Message {
 public:

  class _JobOutputDef_ {
   public:
    _JobOutputDef_();
    explicit _JobOutputDef_(const _JobOutputDef_& other);
    explicit _JobOutputDef_(_JobOutputDef_&& other);
    _JobOutputDef_(const ::oneflow::JobOutputDef& proto_joboutputdef);
    ~_JobOutputDef_();

    void InitFromProto(const ::oneflow::JobOutputDef& proto_joboutputdef);

    void ToProto(::oneflow::JobOutputDef* proto_joboutputdef) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobOutputDef_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
           
   public:
    int compare(const _JobOutputDef_& other);

    bool operator==(const _JobOutputDef_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobOutputDef_& other) const;
  };

  ConstJobOutputDef(const ::std::shared_ptr<_JobOutputDef_>& data);
  ConstJobOutputDef(const ConstJobOutputDef&);
  ConstJobOutputDef(ConstJobOutputDef&&) noexcept;
  ConstJobOutputDef();
  ConstJobOutputDef(const ::oneflow::JobOutputDef& proto_joboutputdef);
  virtual ~ConstJobOutputDef() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_joboutputdef) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;

 public:
  ::std::shared_ptr<ConstJobOutputDef> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobOutputDef& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobOutputDef& other) const;
 protected:
  const ::std::shared_ptr<_JobOutputDef_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobOutputDef_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobOutputDef
  void BuildFromProto(const PbMessage& proto_joboutputdef);
  
  ::std::shared_ptr<_JobOutputDef_> data_;
};

class JobOutputDef final : public ConstJobOutputDef {
 public:
  JobOutputDef(const ::std::shared_ptr<_JobOutputDef_>& data);
  JobOutputDef(const JobOutputDef& other);
  // enable nothrow for ::std::vector<JobOutputDef> resize 
  JobOutputDef(JobOutputDef&&) noexcept;
  JobOutputDef();
  explicit JobOutputDef(const ::oneflow::JobOutputDef& proto_joboutputdef);

  ~JobOutputDef() override;

  void InitFromProto(const PbMessage& proto_joboutputdef) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobOutputDef& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobOutputDef& other) const;
  void Clear();
  void CopyFrom(const JobOutputDef& other);
  JobOutputDef& operator=(const JobOutputDef& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();

  ::std::shared_ptr<JobOutputDef> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_; 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_;
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_; 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_;

class ConstJobSignatureDef : public ::oneflow::cfg::Message {
 public:

  class _JobSignatureDef_ {
   public:
    _JobSignatureDef_();
    explicit _JobSignatureDef_(const _JobSignatureDef_& other);
    explicit _JobSignatureDef_(_JobSignatureDef_&& other);
    _JobSignatureDef_(const ::oneflow::JobSignatureDef& proto_jobsignaturedef);
    ~_JobSignatureDef_();

    void InitFromProto(const ::oneflow::JobSignatureDef& proto_jobsignaturedef);

    void ToProto(::oneflow::JobSignatureDef* proto_jobsignaturedef) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobSignatureDef_& other);
  
     public:
    ::std::size_t inputs_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& inputs() const;

    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ * mutable_inputs();

    const ::oneflow::cfg::JobInputDef& inputs(::std::string key) const;

    void clear_inputs();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> inputs_;
    
     public:
    ::std::size_t outputs_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& outputs() const;

    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ * mutable_outputs();

    const ::oneflow::cfg::JobOutputDef& outputs(::std::string key) const;

    void clear_outputs();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> outputs_;
         
   public:
    int compare(const _JobSignatureDef_& other);

    bool operator==(const _JobSignatureDef_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobSignatureDef_& other) const;
  };

  ConstJobSignatureDef(const ::std::shared_ptr<_JobSignatureDef_>& data);
  ConstJobSignatureDef(const ConstJobSignatureDef&);
  ConstJobSignatureDef(ConstJobSignatureDef&&) noexcept;
  ConstJobSignatureDef();
  ConstJobSignatureDef(const ::oneflow::JobSignatureDef& proto_jobsignaturedef);
  virtual ~ConstJobSignatureDef() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobsignaturedef) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field inputs
 public:
  ::std::size_t inputs_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& inputs() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> shared_const_inputs() const;
  // map field outputs
 public:
  ::std::size_t outputs_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& outputs() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> shared_const_outputs() const;

 public:
  ::std::shared_ptr<ConstJobSignatureDef> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobSignatureDef& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobSignatureDef& other) const;
 protected:
  const ::std::shared_ptr<_JobSignatureDef_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobSignatureDef_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobSignatureDef
  void BuildFromProto(const PbMessage& proto_jobsignaturedef);
  
  ::std::shared_ptr<_JobSignatureDef_> data_;
};

class JobSignatureDef final : public ConstJobSignatureDef {
 public:
  JobSignatureDef(const ::std::shared_ptr<_JobSignatureDef_>& data);
  JobSignatureDef(const JobSignatureDef& other);
  // enable nothrow for ::std::vector<JobSignatureDef> resize 
  JobSignatureDef(JobSignatureDef&&) noexcept;
  JobSignatureDef();
  explicit JobSignatureDef(const ::oneflow::JobSignatureDef& proto_jobsignaturedef);

  ~JobSignatureDef() override;

  void InitFromProto(const PbMessage& proto_jobsignaturedef) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobSignatureDef& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobSignatureDef& other) const;
  void Clear();
  void CopyFrom(const JobSignatureDef& other);
  JobSignatureDef& operator=(const JobSignatureDef& other);

  // repeated field inputs
 public:
  void clear_inputs();

  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ & inputs() const;

  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_* mutable_inputs();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> shared_mutable_inputs();
  // repeated field outputs
 public:
  void clear_outputs();

  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ & outputs() const;

  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_* mutable_outputs();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> shared_mutable_outputs();

  ::std::shared_ptr<JobSignatureDef> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_; 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_;

class ConstJobConfigProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum job_type
 enum JobTypeCase : unsigned int {
  JOB_TYPE_NOT_SET = 0,
    kTrainConf = 3,
    kPredictConf = 4,
   };

 // oneof enum default_initialize_conf
 enum DefaultInitializeConfCase : unsigned int {
  DEFAULT_INITIALIZE_CONF_NOT_SET = 0,
    kDefaultInitializerConf = 10,
    kDefaultInitializeWithSnapshotPath = 11,
   };

  class _JobConfigProto_ {
   public:
    _JobConfigProto_();
    explicit _JobConfigProto_(const _JobConfigProto_& other);
    explicit _JobConfigProto_(_JobConfigProto_&& other);
    _JobConfigProto_(const ::oneflow::JobConfigProto& proto_jobconfigproto);
    ~_JobConfigProto_();

    void InitFromProto(const ::oneflow::JobConfigProto& proto_jobconfigproto);

    void ToProto(::oneflow::JobConfigProto* proto_jobconfigproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _JobConfigProto_& other);
  
      // optional field job_name
     public:
    bool has_job_name() const;
    const ::std::string& job_name() const;
    void clear_job_name();
    void set_job_name(const ::std::string& value);
    ::std::string* mutable_job_name();
   protected:
    bool has_job_name_ = false;
    ::std::string job_name_;
      
     // oneof field job_type: train_conf
   public:
    bool has_train_conf() const;
    void clear_train_conf();
    const ::oneflow::cfg::TrainConf& train_conf() const;
      ::oneflow::cfg::TrainConf* mutable_train_conf();
      
     // oneof field job_type: predict_conf
   public:
    bool has_predict_conf() const;
    void clear_predict_conf();
    const ::oneflow::cfg::PredictConf& predict_conf() const;
      ::oneflow::cfg::PredictConf* mutable_predict_conf();
      
      // optional field default_data_type
     public:
    bool has_default_data_type() const;
    const ::oneflow::cfg::DataType& default_data_type() const;
    void clear_default_data_type();
    void set_default_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_default_data_type();
   protected:
    bool has_default_data_type_ = false;
    ::oneflow::cfg::DataType default_data_type_;
      
     // oneof field default_initialize_conf: default_initializer_conf
   public:
    bool has_default_initializer_conf() const;
    void clear_default_initializer_conf();
    const ::oneflow::cfg::InitializerConf& default_initializer_conf() const;
      ::oneflow::cfg::InitializerConf* mutable_default_initializer_conf();
      
     // oneof field default_initialize_conf: default_initialize_with_snapshot_path
   public:
    bool has_default_initialize_with_snapshot_path() const;
    void clear_default_initialize_with_snapshot_path();
    const ::std::string& default_initialize_with_snapshot_path() const;
      void set_default_initialize_with_snapshot_path(const ::std::string& value);
    ::std::string* mutable_default_initialize_with_snapshot_path();
      
      // optional field memory_allocation_algorithm_conf
     public:
    bool has_memory_allocation_algorithm_conf() const;
    const ::oneflow::cfg::MemoryAllocationAlgorithmConf& memory_allocation_algorithm_conf() const;
    void clear_memory_allocation_algorithm_conf();
    ::oneflow::cfg::MemoryAllocationAlgorithmConf* mutable_memory_allocation_algorithm_conf();
   protected:
    bool has_memory_allocation_algorithm_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::MemoryAllocationAlgorithmConf> memory_allocation_algorithm_conf_;
      
      // optional field xrt_config
     public:
    bool has_xrt_config() const;
    const ::oneflow::cfg::XrtConfig& xrt_config() const;
    void clear_xrt_config();
    ::oneflow::cfg::XrtConfig* mutable_xrt_config();
   protected:
    bool has_xrt_config_ = false;
    ::std::shared_ptr<::oneflow::cfg::XrtConfig> xrt_config_;
      
      // optional field indexed_slices_optimizer_conf
     public:
    bool has_indexed_slices_optimizer_conf() const;
    const ::oneflow::cfg::IndexedSlicesOptimizerConf& indexed_slices_optimizer_conf() const;
    void clear_indexed_slices_optimizer_conf();
    ::oneflow::cfg::IndexedSlicesOptimizerConf* mutable_indexed_slices_optimizer_conf();
   protected:
    bool has_indexed_slices_optimizer_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::IndexedSlicesOptimizerConf> indexed_slices_optimizer_conf_;
      
      // optional field enable_fuse_model_update_ops
     public:
    bool has_enable_fuse_model_update_ops() const;
    const bool& enable_fuse_model_update_ops() const;
    void clear_enable_fuse_model_update_ops();
    void set_enable_fuse_model_update_ops(const bool& value);
    bool* mutable_enable_fuse_model_update_ops();
   protected:
    bool has_enable_fuse_model_update_ops_ = false;
    bool enable_fuse_model_update_ops_;
      
      // optional field enable_gradients_stats_aggregation
     public:
    bool has_enable_gradients_stats_aggregation() const;
    const bool& enable_gradients_stats_aggregation() const;
    void clear_enable_gradients_stats_aggregation();
    void set_enable_gradients_stats_aggregation(const bool& value);
    bool* mutable_enable_gradients_stats_aggregation();
   protected:
    bool has_enable_gradients_stats_aggregation_ = false;
    bool enable_gradients_stats_aggregation_;
      
      // optional field optimizer_placement_optimization_mode
     public:
    bool has_optimizer_placement_optimization_mode() const;
    const ::std::string& optimizer_placement_optimization_mode() const;
    void clear_optimizer_placement_optimization_mode();
    void set_optimizer_placement_optimization_mode(const ::std::string& value);
    ::std::string* mutable_optimizer_placement_optimization_mode();
   protected:
    bool has_optimizer_placement_optimization_mode_ = false;
    ::std::string optimizer_placement_optimization_mode_;
      
      // optional field optimizer_placement_optimization_threshold
     public:
    bool has_optimizer_placement_optimization_threshold() const;
    const int64_t& optimizer_placement_optimization_threshold() const;
    void clear_optimizer_placement_optimization_threshold();
    void set_optimizer_placement_optimization_threshold(const int64_t& value);
    int64_t* mutable_optimizer_placement_optimization_threshold();
   protected:
    bool has_optimizer_placement_optimization_threshold_ = false;
    int64_t optimizer_placement_optimization_threshold_;
      
      // optional field qat_config
     public:
    bool has_qat_config() const;
    const ::oneflow::cfg::QatConfig& qat_config() const;
    void clear_qat_config();
    ::oneflow::cfg::QatConfig* mutable_qat_config();
   protected:
    bool has_qat_config_ = false;
    ::std::shared_ptr<::oneflow::cfg::QatConfig> qat_config_;
      
      // optional field enable_cudnn
     public:
    bool has_enable_cudnn() const;
    const bool& enable_cudnn() const;
    void clear_enable_cudnn();
    void set_enable_cudnn(const bool& value);
    bool* mutable_enable_cudnn();
   protected:
    bool has_enable_cudnn_ = false;
    bool enable_cudnn_;
      
      // optional field cudnn_buf_limit_mbyte
     public:
    bool has_cudnn_buf_limit_mbyte() const;
    const int64_t& cudnn_buf_limit_mbyte() const;
    void clear_cudnn_buf_limit_mbyte();
    void set_cudnn_buf_limit_mbyte(const int64_t& value);
    int64_t* mutable_cudnn_buf_limit_mbyte();
   protected:
    bool has_cudnn_buf_limit_mbyte_ = false;
    int64_t cudnn_buf_limit_mbyte_;
      
      // optional field cudnn_conv_force_fwd_algo
     public:
    bool has_cudnn_conv_force_fwd_algo() const;
    const int32_t& cudnn_conv_force_fwd_algo() const;
    void clear_cudnn_conv_force_fwd_algo();
    void set_cudnn_conv_force_fwd_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_fwd_algo();
   protected:
    bool has_cudnn_conv_force_fwd_algo_ = false;
    int32_t cudnn_conv_force_fwd_algo_;
      
      // optional field cudnn_conv_force_bwd_data_algo
     public:
    bool has_cudnn_conv_force_bwd_data_algo() const;
    const int32_t& cudnn_conv_force_bwd_data_algo() const;
    void clear_cudnn_conv_force_bwd_data_algo();
    void set_cudnn_conv_force_bwd_data_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_bwd_data_algo();
   protected:
    bool has_cudnn_conv_force_bwd_data_algo_ = false;
    int32_t cudnn_conv_force_bwd_data_algo_;
      
      // optional field cudnn_conv_force_bwd_filter_algo
     public:
    bool has_cudnn_conv_force_bwd_filter_algo() const;
    const int32_t& cudnn_conv_force_bwd_filter_algo() const;
    void clear_cudnn_conv_force_bwd_filter_algo();
    void set_cudnn_conv_force_bwd_filter_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_bwd_filter_algo();
   protected:
    bool has_cudnn_conv_force_bwd_filter_algo_ = false;
    int32_t cudnn_conv_force_bwd_filter_algo_;
      
      // optional field cudnn_conv_heuristic_search_algo
     public:
    bool has_cudnn_conv_heuristic_search_algo() const;
    const bool& cudnn_conv_heuristic_search_algo() const;
    void clear_cudnn_conv_heuristic_search_algo();
    void set_cudnn_conv_heuristic_search_algo(const bool& value);
    bool* mutable_cudnn_conv_heuristic_search_algo();
   protected:
    bool has_cudnn_conv_heuristic_search_algo_ = false;
    bool cudnn_conv_heuristic_search_algo_;
      
      // optional field cudnn_conv_use_deterministic_algo_only
     public:
    bool has_cudnn_conv_use_deterministic_algo_only() const;
    const bool& cudnn_conv_use_deterministic_algo_only() const;
    void clear_cudnn_conv_use_deterministic_algo_only();
    void set_cudnn_conv_use_deterministic_algo_only(const bool& value);
    bool* mutable_cudnn_conv_use_deterministic_algo_only();
   protected:
    bool has_cudnn_conv_use_deterministic_algo_only_ = false;
    bool cudnn_conv_use_deterministic_algo_only_;
      
      // optional field enable_cudnn_fused_normalization_add_relu
     public:
    bool has_enable_cudnn_fused_normalization_add_relu() const;
    const bool& enable_cudnn_fused_normalization_add_relu() const;
    void clear_enable_cudnn_fused_normalization_add_relu();
    void set_enable_cudnn_fused_normalization_add_relu(const bool& value);
    bool* mutable_enable_cudnn_fused_normalization_add_relu();
   protected:
    bool has_enable_cudnn_fused_normalization_add_relu_ = false;
    bool enable_cudnn_fused_normalization_add_relu_;
      
      // optional field enable_fuse_add_to_output
     public:
    bool has_enable_fuse_add_to_output() const;
    const bool& enable_fuse_add_to_output() const;
    void clear_enable_fuse_add_to_output();
    void set_enable_fuse_add_to_output(const bool& value);
    bool* mutable_enable_fuse_add_to_output();
   protected:
    bool has_enable_fuse_add_to_output_ = false;
    bool enable_fuse_add_to_output_;
      
      // optional field enable_fuse_cast_scale
     public:
    bool has_enable_fuse_cast_scale() const;
    const bool& enable_fuse_cast_scale() const;
    void clear_enable_fuse_cast_scale();
    void set_enable_fuse_cast_scale(const bool& value);
    bool* mutable_enable_fuse_cast_scale();
   protected:
    bool has_enable_fuse_cast_scale_ = false;
    bool enable_fuse_cast_scale_;
      
      // optional field num_gradient_accumulation_steps
     public:
    bool has_num_gradient_accumulation_steps() const;
    const int64_t& num_gradient_accumulation_steps() const;
    void clear_num_gradient_accumulation_steps();
    void set_num_gradient_accumulation_steps(const int64_t& value);
    int64_t* mutable_num_gradient_accumulation_steps();
   protected:
    bool has_num_gradient_accumulation_steps_ = false;
    int64_t num_gradient_accumulation_steps_;
      
      // optional field enable_reuse_mem
     public:
    bool has_enable_reuse_mem() const;
    const bool& enable_reuse_mem() const;
    void clear_enable_reuse_mem();
    void set_enable_reuse_mem(const bool& value);
    bool* mutable_enable_reuse_mem();
   protected:
    bool has_enable_reuse_mem_ = false;
    bool enable_reuse_mem_;
      
      // optional field enable_inplace
     public:
    bool has_enable_inplace() const;
    const bool& enable_inplace() const;
    void clear_enable_inplace();
    void set_enable_inplace(const bool& value);
    bool* mutable_enable_inplace();
   protected:
    bool has_enable_inplace_ = false;
    bool enable_inplace_;
      
      // optional field enable_inplace_in_reduce_struct
     public:
    bool has_enable_inplace_in_reduce_struct() const;
    const bool& enable_inplace_in_reduce_struct() const;
    void clear_enable_inplace_in_reduce_struct();
    void set_enable_inplace_in_reduce_struct(const bool& value);
    bool* mutable_enable_inplace_in_reduce_struct();
   protected:
    bool has_enable_inplace_in_reduce_struct_ = false;
    bool enable_inplace_in_reduce_struct_;
      
      // optional field do_parallel_cast_before_widening_type_cast
     public:
    bool has_do_parallel_cast_before_widening_type_cast() const;
    const bool& do_parallel_cast_before_widening_type_cast() const;
    void clear_do_parallel_cast_before_widening_type_cast();
    void set_do_parallel_cast_before_widening_type_cast(const bool& value);
    bool* mutable_do_parallel_cast_before_widening_type_cast();
   protected:
    bool has_do_parallel_cast_before_widening_type_cast_ = false;
    bool do_parallel_cast_before_widening_type_cast_;
      
      // optional field prune_parallel_cast_ops
     public:
    bool has_prune_parallel_cast_ops() const;
    const bool& prune_parallel_cast_ops() const;
    void clear_prune_parallel_cast_ops();
    void set_prune_parallel_cast_ops(const bool& value);
    bool* mutable_prune_parallel_cast_ops();
   protected:
    bool has_prune_parallel_cast_ops_ = false;
    bool prune_parallel_cast_ops_;
      
      // optional field prune_cast_to_static_shape_ops
     public:
    bool has_prune_cast_to_static_shape_ops() const;
    const bool& prune_cast_to_static_shape_ops() const;
    void clear_prune_cast_to_static_shape_ops();
    void set_prune_cast_to_static_shape_ops(const bool& value);
    bool* mutable_prune_cast_to_static_shape_ops();
   protected:
    bool has_prune_cast_to_static_shape_ops_ = false;
    bool prune_cast_to_static_shape_ops_;
      
      // optional field prune_amp_white_identity_ops
     public:
    bool has_prune_amp_white_identity_ops() const;
    const bool& prune_amp_white_identity_ops() const;
    void clear_prune_amp_white_identity_ops();
    void set_prune_amp_white_identity_ops(const bool& value);
    bool* mutable_prune_amp_white_identity_ops();
   protected:
    bool has_prune_amp_white_identity_ops_ = false;
    bool prune_amp_white_identity_ops_;
      
      // optional field cudnn_conv_enable_pseudo_half
     public:
    bool has_cudnn_conv_enable_pseudo_half() const;
    const bool& cudnn_conv_enable_pseudo_half() const;
    void clear_cudnn_conv_enable_pseudo_half();
    void set_cudnn_conv_enable_pseudo_half(const bool& value);
    bool* mutable_cudnn_conv_enable_pseudo_half();
   protected:
    bool has_cudnn_conv_enable_pseudo_half_ = false;
    bool cudnn_conv_enable_pseudo_half_;
      
      // optional field enable_auto_mixed_precision
     public:
    bool has_enable_auto_mixed_precision() const;
    const bool& enable_auto_mixed_precision() const;
    void clear_enable_auto_mixed_precision();
    void set_enable_auto_mixed_precision(const bool& value);
    bool* mutable_enable_auto_mixed_precision();
   protected:
    bool has_enable_auto_mixed_precision_ = false;
    bool enable_auto_mixed_precision_;
      
      // optional field enable_quantization_aware_training
     public:
    bool has_enable_quantization_aware_training() const;
    const bool& enable_quantization_aware_training() const;
    void clear_enable_quantization_aware_training();
    void set_enable_quantization_aware_training(const bool& value);
    bool* mutable_enable_quantization_aware_training();
   protected:
    bool has_enable_quantization_aware_training_ = false;
    bool enable_quantization_aware_training_;
      
      // optional field concurrency_width
     public:
    bool has_concurrency_width() const;
    const int64_t& concurrency_width() const;
    void clear_concurrency_width();
    void set_concurrency_width(const int64_t& value);
    int64_t* mutable_concurrency_width();
   protected:
    bool has_concurrency_width_ = false;
    int64_t concurrency_width_;
      
     public:
    ::std::size_t flag_name2flag_value_size() const;
    const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& flag_name2flag_value() const;

    _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ * mutable_flag_name2flag_value();

    const ::oneflow::cfg::AttrValue& flag_name2flag_value(::std::string key) const;

    void clear_flag_name2flag_value();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> flag_name2flag_value_;
    
      // optional field logical_object_id
     public:
    bool has_logical_object_id() const;
    const int64_t& logical_object_id() const;
    void clear_logical_object_id();
    void set_logical_object_id(const int64_t& value);
    int64_t* mutable_logical_object_id();
   protected:
    bool has_logical_object_id_ = false;
    int64_t logical_object_id_;
      
      // optional field signature
     public:
    bool has_signature() const;
    const ::oneflow::cfg::JobSignatureDef& signature() const;
    void clear_signature();
    ::oneflow::cfg::JobSignatureDef* mutable_signature();
   protected:
    bool has_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::JobSignatureDef> signature_;
           
   public:
    // oneof job_type
    JobTypeCase job_type_case() const;
    bool has_job_type() const;
   protected:
    void clear_job_type();
    void job_type_copy_from(const _JobConfigProto_& other);
    union JobTypeUnion {
      // 64-bit aligned
      uint64_t __job_type_for_padding_64bit__;
          char train_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::TrainConf>)];
            char predict_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::PredictConf>)];
        } job_type_;
    JobTypeCase job_type_case_ = JOB_TYPE_NOT_SET;
     
   public:
    // oneof default_initialize_conf
    DefaultInitializeConfCase default_initialize_conf_case() const;
    bool has_default_initialize_conf() const;
   protected:
    void clear_default_initialize_conf();
    void default_initialize_conf_copy_from(const _JobConfigProto_& other);
    union DefaultInitializeConfUnion {
      // 64-bit aligned
      uint64_t __default_initialize_conf_for_padding_64bit__;
          char default_initializer_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::InitializerConf>)];
            char default_initialize_with_snapshot_path_[sizeof(::std::string)];
        } default_initialize_conf_;
    DefaultInitializeConfCase default_initialize_conf_case_ = DEFAULT_INITIALIZE_CONF_NOT_SET;
     
   public:
    int compare(const _JobConfigProto_& other);

    bool operator==(const _JobConfigProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _JobConfigProto_& other) const;
  };

  ConstJobConfigProto(const ::std::shared_ptr<_JobConfigProto_>& data);
  ConstJobConfigProto(const ConstJobConfigProto&);
  ConstJobConfigProto(ConstJobConfigProto&&) noexcept;
  ConstJobConfigProto();
  ConstJobConfigProto(const ::oneflow::JobConfigProto& proto_jobconfigproto);
  virtual ~ConstJobConfigProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_jobconfigproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field job_name
 public:
  bool has_job_name() const;
  const ::std::string& job_name() const;
  // used by pybind11 only
 // oneof field job_type: train_conf
 public:
  bool has_train_conf() const;
  const ::oneflow::cfg::TrainConf& train_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTrainConf> shared_const_train_conf() const;
 // oneof field job_type: predict_conf
 public:
  bool has_predict_conf() const;
  const ::oneflow::cfg::PredictConf& predict_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstPredictConf> shared_const_predict_conf() const;
  // required or optional field default_data_type
 public:
  bool has_default_data_type() const;
  const ::oneflow::cfg::DataType& default_data_type() const;
  // used by pybind11 only
 // oneof field default_initialize_conf: default_initializer_conf
 public:
  bool has_default_initializer_conf() const;
  const ::oneflow::cfg::InitializerConf& default_initializer_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInitializerConf> shared_const_default_initializer_conf() const;
 // oneof field default_initialize_conf: default_initialize_with_snapshot_path
 public:
  bool has_default_initialize_with_snapshot_path() const;
  const ::std::string& default_initialize_with_snapshot_path() const;
  // used by pybind11 only
  // required or optional field memory_allocation_algorithm_conf
 public:
  bool has_memory_allocation_algorithm_conf() const;
  const ::oneflow::cfg::MemoryAllocationAlgorithmConf& memory_allocation_algorithm_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMemoryAllocationAlgorithmConf> shared_const_memory_allocation_algorithm_conf() const;
  // required or optional field xrt_config
 public:
  bool has_xrt_config() const;
  const ::oneflow::cfg::XrtConfig& xrt_config() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXrtConfig> shared_const_xrt_config() const;
  // required or optional field indexed_slices_optimizer_conf
 public:
  bool has_indexed_slices_optimizer_conf() const;
  const ::oneflow::cfg::IndexedSlicesOptimizerConf& indexed_slices_optimizer_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstIndexedSlicesOptimizerConf> shared_const_indexed_slices_optimizer_conf() const;
  // required or optional field enable_fuse_model_update_ops
 public:
  bool has_enable_fuse_model_update_ops() const;
  const bool& enable_fuse_model_update_ops() const;
  // used by pybind11 only
  // required or optional field enable_gradients_stats_aggregation
 public:
  bool has_enable_gradients_stats_aggregation() const;
  const bool& enable_gradients_stats_aggregation() const;
  // used by pybind11 only
  // required or optional field optimizer_placement_optimization_mode
 public:
  bool has_optimizer_placement_optimization_mode() const;
  const ::std::string& optimizer_placement_optimization_mode() const;
  // used by pybind11 only
  // required or optional field optimizer_placement_optimization_threshold
 public:
  bool has_optimizer_placement_optimization_threshold() const;
  const int64_t& optimizer_placement_optimization_threshold() const;
  // used by pybind11 only
  // required or optional field qat_config
 public:
  bool has_qat_config() const;
  const ::oneflow::cfg::QatConfig& qat_config() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstQatConfig> shared_const_qat_config() const;
  // required or optional field enable_cudnn
 public:
  bool has_enable_cudnn() const;
  const bool& enable_cudnn() const;
  // used by pybind11 only
  // required or optional field cudnn_buf_limit_mbyte
 public:
  bool has_cudnn_buf_limit_mbyte() const;
  const int64_t& cudnn_buf_limit_mbyte() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_fwd_algo
 public:
  bool has_cudnn_conv_force_fwd_algo() const;
  const int32_t& cudnn_conv_force_fwd_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_bwd_data_algo
 public:
  bool has_cudnn_conv_force_bwd_data_algo() const;
  const int32_t& cudnn_conv_force_bwd_data_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_bwd_filter_algo
 public:
  bool has_cudnn_conv_force_bwd_filter_algo() const;
  const int32_t& cudnn_conv_force_bwd_filter_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_heuristic_search_algo
 public:
  bool has_cudnn_conv_heuristic_search_algo() const;
  const bool& cudnn_conv_heuristic_search_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_use_deterministic_algo_only
 public:
  bool has_cudnn_conv_use_deterministic_algo_only() const;
  const bool& cudnn_conv_use_deterministic_algo_only() const;
  // used by pybind11 only
  // required or optional field enable_cudnn_fused_normalization_add_relu
 public:
  bool has_enable_cudnn_fused_normalization_add_relu() const;
  const bool& enable_cudnn_fused_normalization_add_relu() const;
  // used by pybind11 only
  // required or optional field enable_fuse_add_to_output
 public:
  bool has_enable_fuse_add_to_output() const;
  const bool& enable_fuse_add_to_output() const;
  // used by pybind11 only
  // required or optional field enable_fuse_cast_scale
 public:
  bool has_enable_fuse_cast_scale() const;
  const bool& enable_fuse_cast_scale() const;
  // used by pybind11 only
  // required or optional field num_gradient_accumulation_steps
 public:
  bool has_num_gradient_accumulation_steps() const;
  const int64_t& num_gradient_accumulation_steps() const;
  // used by pybind11 only
  // required or optional field enable_reuse_mem
 public:
  bool has_enable_reuse_mem() const;
  const bool& enable_reuse_mem() const;
  // used by pybind11 only
  // required or optional field enable_inplace
 public:
  bool has_enable_inplace() const;
  const bool& enable_inplace() const;
  // used by pybind11 only
  // required or optional field enable_inplace_in_reduce_struct
 public:
  bool has_enable_inplace_in_reduce_struct() const;
  const bool& enable_inplace_in_reduce_struct() const;
  // used by pybind11 only
  // required or optional field do_parallel_cast_before_widening_type_cast
 public:
  bool has_do_parallel_cast_before_widening_type_cast() const;
  const bool& do_parallel_cast_before_widening_type_cast() const;
  // used by pybind11 only
  // required or optional field prune_parallel_cast_ops
 public:
  bool has_prune_parallel_cast_ops() const;
  const bool& prune_parallel_cast_ops() const;
  // used by pybind11 only
  // required or optional field prune_cast_to_static_shape_ops
 public:
  bool has_prune_cast_to_static_shape_ops() const;
  const bool& prune_cast_to_static_shape_ops() const;
  // used by pybind11 only
  // required or optional field prune_amp_white_identity_ops
 public:
  bool has_prune_amp_white_identity_ops() const;
  const bool& prune_amp_white_identity_ops() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_enable_pseudo_half
 public:
  bool has_cudnn_conv_enable_pseudo_half() const;
  const bool& cudnn_conv_enable_pseudo_half() const;
  // used by pybind11 only
  // required or optional field enable_auto_mixed_precision
 public:
  bool has_enable_auto_mixed_precision() const;
  const bool& enable_auto_mixed_precision() const;
  // used by pybind11 only
  // required or optional field enable_quantization_aware_training
 public:
  bool has_enable_quantization_aware_training() const;
  const bool& enable_quantization_aware_training() const;
  // used by pybind11 only
  // required or optional field concurrency_width
 public:
  bool has_concurrency_width() const;
  const int64_t& concurrency_width() const;
  // used by pybind11 only
  // map field flag_name2flag_value
 public:
  ::std::size_t flag_name2flag_value_size() const;
  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& flag_name2flag_value() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> shared_const_flag_name2flag_value() const;
  // required or optional field logical_object_id
 public:
  bool has_logical_object_id() const;
  const int64_t& logical_object_id() const;
  // used by pybind11 only
  // required or optional field signature
 public:
  bool has_signature() const;
  const ::oneflow::cfg::JobSignatureDef& signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobSignatureDef> shared_const_signature() const;
 public:
  JobTypeCase job_type_case() const;
 protected:
  bool has_job_type() const;
 public:
  DefaultInitializeConfCase default_initialize_conf_case() const;
 protected:
  bool has_default_initialize_conf() const;

 public:
  ::std::shared_ptr<ConstJobConfigProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstJobConfigProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstJobConfigProto& other) const;
 protected:
  const ::std::shared_ptr<_JobConfigProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_JobConfigProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstJobConfigProto
  void BuildFromProto(const PbMessage& proto_jobconfigproto);
  
  ::std::shared_ptr<_JobConfigProto_> data_;
};

class JobConfigProto final : public ConstJobConfigProto {
 public:
  JobConfigProto(const ::std::shared_ptr<_JobConfigProto_>& data);
  JobConfigProto(const JobConfigProto& other);
  // enable nothrow for ::std::vector<JobConfigProto> resize 
  JobConfigProto(JobConfigProto&&) noexcept;
  JobConfigProto();
  explicit JobConfigProto(const ::oneflow::JobConfigProto& proto_jobconfigproto);

  ~JobConfigProto() override;

  void InitFromProto(const PbMessage& proto_jobconfigproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const JobConfigProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const JobConfigProto& other) const;
  void Clear();
  void CopyFrom(const JobConfigProto& other);
  JobConfigProto& operator=(const JobConfigProto& other);

  // required or optional field job_name
 public:
  void clear_job_name();
  void set_job_name(const ::std::string& value);
  ::std::string* mutable_job_name();
  void clear_train_conf();
  ::oneflow::cfg::TrainConf* mutable_train_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TrainConf> shared_mutable_train_conf();
  void clear_predict_conf();
  ::oneflow::cfg::PredictConf* mutable_predict_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::PredictConf> shared_mutable_predict_conf();
  // required or optional field default_data_type
 public:
  void clear_default_data_type();
  void set_default_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_default_data_type();
  void clear_default_initializer_conf();
  ::oneflow::cfg::InitializerConf* mutable_default_initializer_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InitializerConf> shared_mutable_default_initializer_conf();
  void clear_default_initialize_with_snapshot_path();
  void set_default_initialize_with_snapshot_path(const ::std::string& value);
  ::std::string* mutable_default_initialize_with_snapshot_path();
  // required or optional field memory_allocation_algorithm_conf
 public:
  void clear_memory_allocation_algorithm_conf();
  ::oneflow::cfg::MemoryAllocationAlgorithmConf* mutable_memory_allocation_algorithm_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MemoryAllocationAlgorithmConf> shared_mutable_memory_allocation_algorithm_conf();
  // required or optional field xrt_config
 public:
  void clear_xrt_config();
  ::oneflow::cfg::XrtConfig* mutable_xrt_config();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XrtConfig> shared_mutable_xrt_config();
  // required or optional field indexed_slices_optimizer_conf
 public:
  void clear_indexed_slices_optimizer_conf();
  ::oneflow::cfg::IndexedSlicesOptimizerConf* mutable_indexed_slices_optimizer_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::IndexedSlicesOptimizerConf> shared_mutable_indexed_slices_optimizer_conf();
  // required or optional field enable_fuse_model_update_ops
 public:
  void clear_enable_fuse_model_update_ops();
  void set_enable_fuse_model_update_ops(const bool& value);
  bool* mutable_enable_fuse_model_update_ops();
  // required or optional field enable_gradients_stats_aggregation
 public:
  void clear_enable_gradients_stats_aggregation();
  void set_enable_gradients_stats_aggregation(const bool& value);
  bool* mutable_enable_gradients_stats_aggregation();
  // required or optional field optimizer_placement_optimization_mode
 public:
  void clear_optimizer_placement_optimization_mode();
  void set_optimizer_placement_optimization_mode(const ::std::string& value);
  ::std::string* mutable_optimizer_placement_optimization_mode();
  // required or optional field optimizer_placement_optimization_threshold
 public:
  void clear_optimizer_placement_optimization_threshold();
  void set_optimizer_placement_optimization_threshold(const int64_t& value);
  int64_t* mutable_optimizer_placement_optimization_threshold();
  // required or optional field qat_config
 public:
  void clear_qat_config();
  ::oneflow::cfg::QatConfig* mutable_qat_config();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::QatConfig> shared_mutable_qat_config();
  // required or optional field enable_cudnn
 public:
  void clear_enable_cudnn();
  void set_enable_cudnn(const bool& value);
  bool* mutable_enable_cudnn();
  // required or optional field cudnn_buf_limit_mbyte
 public:
  void clear_cudnn_buf_limit_mbyte();
  void set_cudnn_buf_limit_mbyte(const int64_t& value);
  int64_t* mutable_cudnn_buf_limit_mbyte();
  // required or optional field cudnn_conv_force_fwd_algo
 public:
  void clear_cudnn_conv_force_fwd_algo();
  void set_cudnn_conv_force_fwd_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_fwd_algo();
  // required or optional field cudnn_conv_force_bwd_data_algo
 public:
  void clear_cudnn_conv_force_bwd_data_algo();
  void set_cudnn_conv_force_bwd_data_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_bwd_data_algo();
  // required or optional field cudnn_conv_force_bwd_filter_algo
 public:
  void clear_cudnn_conv_force_bwd_filter_algo();
  void set_cudnn_conv_force_bwd_filter_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_bwd_filter_algo();
  // required or optional field cudnn_conv_heuristic_search_algo
 public:
  void clear_cudnn_conv_heuristic_search_algo();
  void set_cudnn_conv_heuristic_search_algo(const bool& value);
  bool* mutable_cudnn_conv_heuristic_search_algo();
  // required or optional field cudnn_conv_use_deterministic_algo_only
 public:
  void clear_cudnn_conv_use_deterministic_algo_only();
  void set_cudnn_conv_use_deterministic_algo_only(const bool& value);
  bool* mutable_cudnn_conv_use_deterministic_algo_only();
  // required or optional field enable_cudnn_fused_normalization_add_relu
 public:
  void clear_enable_cudnn_fused_normalization_add_relu();
  void set_enable_cudnn_fused_normalization_add_relu(const bool& value);
  bool* mutable_enable_cudnn_fused_normalization_add_relu();
  // required or optional field enable_fuse_add_to_output
 public:
  void clear_enable_fuse_add_to_output();
  void set_enable_fuse_add_to_output(const bool& value);
  bool* mutable_enable_fuse_add_to_output();
  // required or optional field enable_fuse_cast_scale
 public:
  void clear_enable_fuse_cast_scale();
  void set_enable_fuse_cast_scale(const bool& value);
  bool* mutable_enable_fuse_cast_scale();
  // required or optional field num_gradient_accumulation_steps
 public:
  void clear_num_gradient_accumulation_steps();
  void set_num_gradient_accumulation_steps(const int64_t& value);
  int64_t* mutable_num_gradient_accumulation_steps();
  // required or optional field enable_reuse_mem
 public:
  void clear_enable_reuse_mem();
  void set_enable_reuse_mem(const bool& value);
  bool* mutable_enable_reuse_mem();
  // required or optional field enable_inplace
 public:
  void clear_enable_inplace();
  void set_enable_inplace(const bool& value);
  bool* mutable_enable_inplace();
  // required or optional field enable_inplace_in_reduce_struct
 public:
  void clear_enable_inplace_in_reduce_struct();
  void set_enable_inplace_in_reduce_struct(const bool& value);
  bool* mutable_enable_inplace_in_reduce_struct();
  // required or optional field do_parallel_cast_before_widening_type_cast
 public:
  void clear_do_parallel_cast_before_widening_type_cast();
  void set_do_parallel_cast_before_widening_type_cast(const bool& value);
  bool* mutable_do_parallel_cast_before_widening_type_cast();
  // required or optional field prune_parallel_cast_ops
 public:
  void clear_prune_parallel_cast_ops();
  void set_prune_parallel_cast_ops(const bool& value);
  bool* mutable_prune_parallel_cast_ops();
  // required or optional field prune_cast_to_static_shape_ops
 public:
  void clear_prune_cast_to_static_shape_ops();
  void set_prune_cast_to_static_shape_ops(const bool& value);
  bool* mutable_prune_cast_to_static_shape_ops();
  // required or optional field prune_amp_white_identity_ops
 public:
  void clear_prune_amp_white_identity_ops();
  void set_prune_amp_white_identity_ops(const bool& value);
  bool* mutable_prune_amp_white_identity_ops();
  // required or optional field cudnn_conv_enable_pseudo_half
 public:
  void clear_cudnn_conv_enable_pseudo_half();
  void set_cudnn_conv_enable_pseudo_half(const bool& value);
  bool* mutable_cudnn_conv_enable_pseudo_half();
  // required or optional field enable_auto_mixed_precision
 public:
  void clear_enable_auto_mixed_precision();
  void set_enable_auto_mixed_precision(const bool& value);
  bool* mutable_enable_auto_mixed_precision();
  // required or optional field enable_quantization_aware_training
 public:
  void clear_enable_quantization_aware_training();
  void set_enable_quantization_aware_training(const bool& value);
  bool* mutable_enable_quantization_aware_training();
  // required or optional field concurrency_width
 public:
  void clear_concurrency_width();
  void set_concurrency_width(const int64_t& value);
  int64_t* mutable_concurrency_width();
  // repeated field flag_name2flag_value
 public:
  void clear_flag_name2flag_value();

  const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ & flag_name2flag_value() const;

  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_* mutable_flag_name2flag_value();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> shared_mutable_flag_name2flag_value();
  // required or optional field logical_object_id
 public:
  void clear_logical_object_id();
  void set_logical_object_id(const int64_t& value);
  int64_t* mutable_logical_object_id();
  // required or optional field signature
 public:
  void clear_signature();
  ::oneflow::cfg::JobSignatureDef* mutable_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobSignatureDef> shared_mutable_signature();

  ::std::shared_ptr<JobConfigProto> __SharedMutable__();
};































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};
















// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OptimizerConf> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OptimizerConf>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_();
  ~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOptimizerConf> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_ final : public Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OptimizerConf>>& data);
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_();
  ~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::OptimizerConf> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::OptimizerConf> __SharedMutable__(::std::size_t index);
};


































// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobInputDef> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobInputDef>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_();
  ~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::JobInputDef& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstJobInputDef> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ConstJobInputDef>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ final : public Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobInputDef>>& data);
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_();
  ~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::JobInputDef> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ::oneflow::cfg::JobInputDef>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobOutputDef> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobOutputDef>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_();
  ~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::JobOutputDef& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstJobOutputDef> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ConstJobOutputDef>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ final : public Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobOutputDef>>& data);
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_();
  ~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::JobOutputDef> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ::oneflow::cfg::JobOutputDef>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_();
  ~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::AttrValue& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstAttrValue> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ final : public Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_();
  ~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::AttrValue> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstNaiveModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstNaiveModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::NaiveModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::NaiveModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMomentumModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstMomentumModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MomentumModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::MomentumModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRMSPropModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRMSPropModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RMSPropModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::RMSPropModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLARSModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLARSModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LARSModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::LARSModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAdamModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstAdamModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AdamModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::AdamModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLazyAdamModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLazyAdamModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LazyAdamModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::LazyAdamModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLambModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLambModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LambModelUpdateConf> {
  std::size_t operator()(const ::oneflow::cfg::LambModelUpdateConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstClipByGlobalNormConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstClipByGlobalNormConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClipByGlobalNormConf> {
  std::size_t operator()(const ::oneflow::cfg::ClipByGlobalNormConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstClipConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstClipConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ClipConf> {
  std::size_t operator()(const ::oneflow::cfg::ClipConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstWeightDecayFilterPatternSet> {
  std::size_t operator()(const ::oneflow::cfg::ConstWeightDecayFilterPatternSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::WeightDecayFilterPatternSet> {
  std::size_t operator()(const ::oneflow::cfg::WeightDecayFilterPatternSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstWeightDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstWeightDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::WeightDecayConf> {
  std::size_t operator()(const ::oneflow::cfg::WeightDecayConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOptimizerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstOptimizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OptimizerConf> {
  std::size_t operator()(const ::oneflow::cfg::OptimizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstNormalModelUpdateOpUserConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstNormalModelUpdateOpUserConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::NormalModelUpdateOpUserConf> {
  std::size_t operator()(const ::oneflow::cfg::NormalModelUpdateOpUserConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDynamicLossScalePolicy> {
  std::size_t operator()(const ::oneflow::cfg::ConstDynamicLossScalePolicy& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DynamicLossScalePolicy> {
  std::size_t operator()(const ::oneflow::cfg::DynamicLossScalePolicy& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTrainConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstTrainConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TrainConf> {
  std::size_t operator()(const ::oneflow::cfg::TrainConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPredictConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstPredictConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PredictConf> {
  std::size_t operator()(const ::oneflow::cfg::PredictConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMemoryAllocationAlgorithmConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstMemoryAllocationAlgorithmConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MemoryAllocationAlgorithmConf> {
  std::size_t operator()(const ::oneflow::cfg::MemoryAllocationAlgorithmConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtConfig_XlaConfig> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtConfig_XlaConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtConfig_XlaConfig> {
  std::size_t operator()(const ::oneflow::cfg::XrtConfig_XlaConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtConfig_TensorRTConfig> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtConfig_TensorRTConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtConfig_TensorRTConfig> {
  std::size_t operator()(const ::oneflow::cfg::XrtConfig_TensorRTConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtConfig> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtConfig> {
  std::size_t operator()(const ::oneflow::cfg::XrtConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstQatConfig> {
  std::size_t operator()(const ::oneflow::cfg::ConstQatConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::QatConfig> {
  std::size_t operator()(const ::oneflow::cfg::QatConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstIndexedSlicesOptimizerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstIndexedSlicesOptimizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::IndexedSlicesOptimizerConf> {
  std::size_t operator()(const ::oneflow::cfg::IndexedSlicesOptimizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstParallelBlobConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelBlobConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelBlobConf> {
  std::size_t operator()(const ::oneflow::cfg::ParallelBlobConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobInputDef> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobInputDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobInputDef> {
  std::size_t operator()(const ::oneflow::cfg::JobInputDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobOutputDef> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobOutputDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobOutputDef> {
  std::size_t operator()(const ::oneflow::cfg::JobOutputDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobSignatureDef> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobSignatureDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobSignatureDef> {
  std::size_t operator()(const ::oneflow::cfg::JobSignatureDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstJobConfigProto> {
  std::size_t operator()(const ::oneflow::cfg::ConstJobConfigProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::JobConfigProto> {
  std::size_t operator()(const ::oneflow::cfg::JobConfigProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H_