#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/regularizer_conf.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.regularizer_conf", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<ConstL1L2RegularizerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstL1L2RegularizerConf>> registry(m, "ConstL1L2RegularizerConf");
    registry.def("__id__", &::oneflow::cfg::L1L2RegularizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstL1L2RegularizerConf::DebugString);
    registry.def("__repr__", &ConstL1L2RegularizerConf::DebugString);

    registry.def("has_l1", &ConstL1L2RegularizerConf::has_l1);
    registry.def("l1", &ConstL1L2RegularizerConf::l1);

    registry.def("has_l2", &ConstL1L2RegularizerConf::has_l2);
    registry.def("l2", &ConstL1L2RegularizerConf::l2);
  }
  {
    pybind11::class_<::oneflow::cfg::L1L2RegularizerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>> registry(m, "L1L2RegularizerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::L1L2RegularizerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::L1L2RegularizerConf::*)(const ConstL1L2RegularizerConf&))&::oneflow::cfg::L1L2RegularizerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::L1L2RegularizerConf::*)(const ::oneflow::cfg::L1L2RegularizerConf&))&::oneflow::cfg::L1L2RegularizerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::L1L2RegularizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::L1L2RegularizerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::L1L2RegularizerConf::DebugString);



    registry.def("has_l1", &::oneflow::cfg::L1L2RegularizerConf::has_l1);
    registry.def("clear_l1", &::oneflow::cfg::L1L2RegularizerConf::clear_l1);
    registry.def("l1", &::oneflow::cfg::L1L2RegularizerConf::l1);
    registry.def("set_l1", &::oneflow::cfg::L1L2RegularizerConf::set_l1);

    registry.def("has_l2", &::oneflow::cfg::L1L2RegularizerConf::has_l2);
    registry.def("clear_l2", &::oneflow::cfg::L1L2RegularizerConf::clear_l2);
    registry.def("l2", &::oneflow::cfg::L1L2RegularizerConf::l2);
    registry.def("set_l2", &::oneflow::cfg::L1L2RegularizerConf::set_l2);
  }
  {
    pybind11::class_<ConstRegularizerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRegularizerConf>> registry(m, "ConstRegularizerConf");
    registry.def("__id__", &::oneflow::cfg::RegularizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRegularizerConf::DebugString);
    registry.def("__repr__", &ConstRegularizerConf::DebugString);

    registry.def("has_l1_l2_conf", &ConstRegularizerConf::has_l1_l2_conf);
    registry.def("l1_l2_conf", &ConstRegularizerConf::shared_const_l1_l2_conf);
    registry.def("type_case",  &ConstRegularizerConf::type_case);
    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::RegularizerConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kL1L2Conf", [](const pybind11::object&){ return ::oneflow::cfg::RegularizerConf::kL1L2Conf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::RegularizerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RegularizerConf>> registry(m, "RegularizerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RegularizerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RegularizerConf::*)(const ConstRegularizerConf&))&::oneflow::cfg::RegularizerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RegularizerConf::*)(const ::oneflow::cfg::RegularizerConf&))&::oneflow::cfg::RegularizerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RegularizerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RegularizerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RegularizerConf::DebugString);

    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::RegularizerConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kL1L2Conf", [](const pybind11::object&){ return ::oneflow::cfg::RegularizerConf::kL1L2Conf; })
        ;


    registry.def("has_l1_l2_conf", &::oneflow::cfg::RegularizerConf::has_l1_l2_conf);
    registry.def("clear_l1_l2_conf", &::oneflow::cfg::RegularizerConf::clear_l1_l2_conf);
    registry.def_property_readonly_static("kL1L2Conf",
        [](const pybind11::object&){ return ::oneflow::cfg::RegularizerConf::kL1L2Conf; });
    registry.def("l1_l2_conf", &::oneflow::cfg::RegularizerConf::l1_l2_conf);
    registry.def("mutable_l1_l2_conf", &::oneflow::cfg::RegularizerConf::shared_mutable_l1_l2_conf);
    pybind11::enum_<::oneflow::cfg::RegularizerConf::TypeCase>(registry, "TypeCase")
        .value("TYPE_NOT_SET", ::oneflow::cfg::RegularizerConf::TYPE_NOT_SET)
        .value("kL1L2Conf", ::oneflow::cfg::RegularizerConf::kL1L2Conf)
        ;
    registry.def("type_case",  &::oneflow::cfg::RegularizerConf::type_case);
  }
}