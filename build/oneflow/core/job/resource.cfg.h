#ifndef CFG_ONEFLOW_CORE_JOB_RESOURCE_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_RESOURCE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class CollectiveBoxingConf;
class CudnnConfig;
class Resource;

namespace cfg {


class CollectiveBoxingConf;
class ConstCollectiveBoxingConf;

class CudnnConfig;
class ConstCudnnConfig;

class Resource;
class ConstResource;



class ConstCollectiveBoxingConf : public ::oneflow::cfg::Message {
 public:

  class _CollectiveBoxingConf_ {
   public:
    _CollectiveBoxingConf_();
    explicit _CollectiveBoxingConf_(const _CollectiveBoxingConf_& other);
    explicit _CollectiveBoxingConf_(_CollectiveBoxingConf_&& other);
    _CollectiveBoxingConf_(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf);
    ~_CollectiveBoxingConf_();

    void InitFromProto(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf);

    void ToProto(::oneflow::CollectiveBoxingConf* proto_collectiveboxingconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CollectiveBoxingConf_& other);
  
      // optional field enable_fusion
     public:
    bool has_enable_fusion() const;
    const bool& enable_fusion() const;
    void clear_enable_fusion();
    void set_enable_fusion(const bool& value);
    bool* mutable_enable_fusion();
   protected:
    bool has_enable_fusion_ = false;
    bool enable_fusion_;
      
      // optional field num_callback_threads
     public:
    bool has_num_callback_threads() const;
    const int64_t& num_callback_threads() const;
    void clear_num_callback_threads();
    void set_num_callback_threads(const int64_t& value);
    int64_t* mutable_num_callback_threads();
   protected:
    bool has_num_callback_threads_ = false;
    int64_t num_callback_threads_;
      
      // optional field nccl_num_streams
     public:
    bool has_nccl_num_streams() const;
    const int64_t& nccl_num_streams() const;
    void clear_nccl_num_streams();
    void set_nccl_num_streams(const int64_t& value);
    int64_t* mutable_nccl_num_streams();
   protected:
    bool has_nccl_num_streams_ = false;
    int64_t nccl_num_streams_;
      
      // optional field nccl_fusion_threshold_mb
     public:
    bool has_nccl_fusion_threshold_mb() const;
    const int64_t& nccl_fusion_threshold_mb() const;
    void clear_nccl_fusion_threshold_mb();
    void set_nccl_fusion_threshold_mb(const int64_t& value);
    int64_t* mutable_nccl_fusion_threshold_mb();
   protected:
    bool has_nccl_fusion_threshold_mb_ = false;
    int64_t nccl_fusion_threshold_mb_;
      
      // optional field nccl_fusion_all_reduce
     public:
    bool has_nccl_fusion_all_reduce() const;
    const bool& nccl_fusion_all_reduce() const;
    void clear_nccl_fusion_all_reduce();
    void set_nccl_fusion_all_reduce(const bool& value);
    bool* mutable_nccl_fusion_all_reduce();
   protected:
    bool has_nccl_fusion_all_reduce_ = false;
    bool nccl_fusion_all_reduce_;
      
      // optional field nccl_fusion_reduce_scatter
     public:
    bool has_nccl_fusion_reduce_scatter() const;
    const bool& nccl_fusion_reduce_scatter() const;
    void clear_nccl_fusion_reduce_scatter();
    void set_nccl_fusion_reduce_scatter(const bool& value);
    bool* mutable_nccl_fusion_reduce_scatter();
   protected:
    bool has_nccl_fusion_reduce_scatter_ = false;
    bool nccl_fusion_reduce_scatter_;
      
      // optional field nccl_fusion_all_gather
     public:
    bool has_nccl_fusion_all_gather() const;
    const bool& nccl_fusion_all_gather() const;
    void clear_nccl_fusion_all_gather();
    void set_nccl_fusion_all_gather(const bool& value);
    bool* mutable_nccl_fusion_all_gather();
   protected:
    bool has_nccl_fusion_all_gather_ = false;
    bool nccl_fusion_all_gather_;
      
      // optional field nccl_fusion_reduce
     public:
    bool has_nccl_fusion_reduce() const;
    const bool& nccl_fusion_reduce() const;
    void clear_nccl_fusion_reduce();
    void set_nccl_fusion_reduce(const bool& value);
    bool* mutable_nccl_fusion_reduce();
   protected:
    bool has_nccl_fusion_reduce_ = false;
    bool nccl_fusion_reduce_;
      
      // optional field nccl_fusion_broadcast
     public:
    bool has_nccl_fusion_broadcast() const;
    const bool& nccl_fusion_broadcast() const;
    void clear_nccl_fusion_broadcast();
    void set_nccl_fusion_broadcast(const bool& value);
    bool* mutable_nccl_fusion_broadcast();
   protected:
    bool has_nccl_fusion_broadcast_ = false;
    bool nccl_fusion_broadcast_;
      
      // optional field nccl_fusion_all_reduce_use_buffer
     public:
    bool has_nccl_fusion_all_reduce_use_buffer() const;
    const bool& nccl_fusion_all_reduce_use_buffer() const;
    void clear_nccl_fusion_all_reduce_use_buffer();
    void set_nccl_fusion_all_reduce_use_buffer(const bool& value);
    bool* mutable_nccl_fusion_all_reduce_use_buffer();
   protected:
    bool has_nccl_fusion_all_reduce_use_buffer_ = false;
    bool nccl_fusion_all_reduce_use_buffer_;
      
      // optional field nccl_fusion_max_ops
     public:
    bool has_nccl_fusion_max_ops() const;
    const int64_t& nccl_fusion_max_ops() const;
    void clear_nccl_fusion_max_ops();
    void set_nccl_fusion_max_ops(const int64_t& value);
    int64_t* mutable_nccl_fusion_max_ops();
   protected:
    bool has_nccl_fusion_max_ops_ = false;
    int64_t nccl_fusion_max_ops_;
      
      // optional field nccl_enable_all_to_all
     public:
    bool has_nccl_enable_all_to_all() const;
    const bool& nccl_enable_all_to_all() const;
    void clear_nccl_enable_all_to_all();
    void set_nccl_enable_all_to_all(const bool& value);
    bool* mutable_nccl_enable_all_to_all();
   protected:
    bool has_nccl_enable_all_to_all_ = false;
    bool nccl_enable_all_to_all_;
      
      // optional field nccl_enable_mixed_fusion
     public:
    bool has_nccl_enable_mixed_fusion() const;
    const bool& nccl_enable_mixed_fusion() const;
    void clear_nccl_enable_mixed_fusion();
    void set_nccl_enable_mixed_fusion(const bool& value);
    bool* mutable_nccl_enable_mixed_fusion();
   protected:
    bool has_nccl_enable_mixed_fusion_ = false;
    bool nccl_enable_mixed_fusion_;
           
   public:
    int compare(const _CollectiveBoxingConf_& other);

    bool operator==(const _CollectiveBoxingConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CollectiveBoxingConf_& other) const;
  };

  ConstCollectiveBoxingConf(const ::std::shared_ptr<_CollectiveBoxingConf_>& data);
  ConstCollectiveBoxingConf(const ConstCollectiveBoxingConf&);
  ConstCollectiveBoxingConf(ConstCollectiveBoxingConf&&) noexcept;
  ConstCollectiveBoxingConf();
  ConstCollectiveBoxingConf(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf);
  virtual ~ConstCollectiveBoxingConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_collectiveboxingconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field enable_fusion
 public:
  bool has_enable_fusion() const;
  const bool& enable_fusion() const;
  // used by pybind11 only
  // required or optional field num_callback_threads
 public:
  bool has_num_callback_threads() const;
  const int64_t& num_callback_threads() const;
  // used by pybind11 only
  // required or optional field nccl_num_streams
 public:
  bool has_nccl_num_streams() const;
  const int64_t& nccl_num_streams() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_threshold_mb
 public:
  bool has_nccl_fusion_threshold_mb() const;
  const int64_t& nccl_fusion_threshold_mb() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_all_reduce
 public:
  bool has_nccl_fusion_all_reduce() const;
  const bool& nccl_fusion_all_reduce() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_reduce_scatter
 public:
  bool has_nccl_fusion_reduce_scatter() const;
  const bool& nccl_fusion_reduce_scatter() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_all_gather
 public:
  bool has_nccl_fusion_all_gather() const;
  const bool& nccl_fusion_all_gather() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_reduce
 public:
  bool has_nccl_fusion_reduce() const;
  const bool& nccl_fusion_reduce() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_broadcast
 public:
  bool has_nccl_fusion_broadcast() const;
  const bool& nccl_fusion_broadcast() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_all_reduce_use_buffer
 public:
  bool has_nccl_fusion_all_reduce_use_buffer() const;
  const bool& nccl_fusion_all_reduce_use_buffer() const;
  // used by pybind11 only
  // required or optional field nccl_fusion_max_ops
 public:
  bool has_nccl_fusion_max_ops() const;
  const int64_t& nccl_fusion_max_ops() const;
  // used by pybind11 only
  // required or optional field nccl_enable_all_to_all
 public:
  bool has_nccl_enable_all_to_all() const;
  const bool& nccl_enable_all_to_all() const;
  // used by pybind11 only
  // required or optional field nccl_enable_mixed_fusion
 public:
  bool has_nccl_enable_mixed_fusion() const;
  const bool& nccl_enable_mixed_fusion() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstCollectiveBoxingConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCollectiveBoxingConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCollectiveBoxingConf& other) const;
 protected:
  const ::std::shared_ptr<_CollectiveBoxingConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CollectiveBoxingConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCollectiveBoxingConf
  void BuildFromProto(const PbMessage& proto_collectiveboxingconf);
  
  ::std::shared_ptr<_CollectiveBoxingConf_> data_;
};

class CollectiveBoxingConf final : public ConstCollectiveBoxingConf {
 public:
  CollectiveBoxingConf(const ::std::shared_ptr<_CollectiveBoxingConf_>& data);
  CollectiveBoxingConf(const CollectiveBoxingConf& other);
  // enable nothrow for ::std::vector<CollectiveBoxingConf> resize 
  CollectiveBoxingConf(CollectiveBoxingConf&&) noexcept;
  CollectiveBoxingConf();
  explicit CollectiveBoxingConf(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf);

  ~CollectiveBoxingConf() override;

  void InitFromProto(const PbMessage& proto_collectiveboxingconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CollectiveBoxingConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CollectiveBoxingConf& other) const;
  void Clear();
  void CopyFrom(const CollectiveBoxingConf& other);
  CollectiveBoxingConf& operator=(const CollectiveBoxingConf& other);

  // required or optional field enable_fusion
 public:
  void clear_enable_fusion();
  void set_enable_fusion(const bool& value);
  bool* mutable_enable_fusion();
  // required or optional field num_callback_threads
 public:
  void clear_num_callback_threads();
  void set_num_callback_threads(const int64_t& value);
  int64_t* mutable_num_callback_threads();
  // required or optional field nccl_num_streams
 public:
  void clear_nccl_num_streams();
  void set_nccl_num_streams(const int64_t& value);
  int64_t* mutable_nccl_num_streams();
  // required or optional field nccl_fusion_threshold_mb
 public:
  void clear_nccl_fusion_threshold_mb();
  void set_nccl_fusion_threshold_mb(const int64_t& value);
  int64_t* mutable_nccl_fusion_threshold_mb();
  // required or optional field nccl_fusion_all_reduce
 public:
  void clear_nccl_fusion_all_reduce();
  void set_nccl_fusion_all_reduce(const bool& value);
  bool* mutable_nccl_fusion_all_reduce();
  // required or optional field nccl_fusion_reduce_scatter
 public:
  void clear_nccl_fusion_reduce_scatter();
  void set_nccl_fusion_reduce_scatter(const bool& value);
  bool* mutable_nccl_fusion_reduce_scatter();
  // required or optional field nccl_fusion_all_gather
 public:
  void clear_nccl_fusion_all_gather();
  void set_nccl_fusion_all_gather(const bool& value);
  bool* mutable_nccl_fusion_all_gather();
  // required or optional field nccl_fusion_reduce
 public:
  void clear_nccl_fusion_reduce();
  void set_nccl_fusion_reduce(const bool& value);
  bool* mutable_nccl_fusion_reduce();
  // required or optional field nccl_fusion_broadcast
 public:
  void clear_nccl_fusion_broadcast();
  void set_nccl_fusion_broadcast(const bool& value);
  bool* mutable_nccl_fusion_broadcast();
  // required or optional field nccl_fusion_all_reduce_use_buffer
 public:
  void clear_nccl_fusion_all_reduce_use_buffer();
  void set_nccl_fusion_all_reduce_use_buffer(const bool& value);
  bool* mutable_nccl_fusion_all_reduce_use_buffer();
  // required or optional field nccl_fusion_max_ops
 public:
  void clear_nccl_fusion_max_ops();
  void set_nccl_fusion_max_ops(const int64_t& value);
  int64_t* mutable_nccl_fusion_max_ops();
  // required or optional field nccl_enable_all_to_all
 public:
  void clear_nccl_enable_all_to_all();
  void set_nccl_enable_all_to_all(const bool& value);
  bool* mutable_nccl_enable_all_to_all();
  // required or optional field nccl_enable_mixed_fusion
 public:
  void clear_nccl_enable_mixed_fusion();
  void set_nccl_enable_mixed_fusion(const bool& value);
  bool* mutable_nccl_enable_mixed_fusion();

  ::std::shared_ptr<CollectiveBoxingConf> __SharedMutable__();
};


class ConstCudnnConfig : public ::oneflow::cfg::Message {
 public:

  class _CudnnConfig_ {
   public:
    _CudnnConfig_();
    explicit _CudnnConfig_(const _CudnnConfig_& other);
    explicit _CudnnConfig_(_CudnnConfig_&& other);
    _CudnnConfig_(const ::oneflow::CudnnConfig& proto_cudnnconfig);
    ~_CudnnConfig_();

    void InitFromProto(const ::oneflow::CudnnConfig& proto_cudnnconfig);

    void ToProto(::oneflow::CudnnConfig* proto_cudnnconfig) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CudnnConfig_& other);
  
      // optional field enable_cudnn
     public:
    bool has_enable_cudnn() const;
    const bool& enable_cudnn() const;
    void clear_enable_cudnn();
    void set_enable_cudnn(const bool& value);
    bool* mutable_enable_cudnn();
   protected:
    bool has_enable_cudnn_ = false;
    bool enable_cudnn_;
      
      // optional field cudnn_buf_limit_mbyte
     public:
    bool has_cudnn_buf_limit_mbyte() const;
    const int64_t& cudnn_buf_limit_mbyte() const;
    void clear_cudnn_buf_limit_mbyte();
    void set_cudnn_buf_limit_mbyte(const int64_t& value);
    int64_t* mutable_cudnn_buf_limit_mbyte();
   protected:
    bool has_cudnn_buf_limit_mbyte_ = false;
    int64_t cudnn_buf_limit_mbyte_;
      
      // optional field cudnn_conv_force_fwd_algo
     public:
    bool has_cudnn_conv_force_fwd_algo() const;
    const int32_t& cudnn_conv_force_fwd_algo() const;
    void clear_cudnn_conv_force_fwd_algo();
    void set_cudnn_conv_force_fwd_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_fwd_algo();
   protected:
    bool has_cudnn_conv_force_fwd_algo_ = false;
    int32_t cudnn_conv_force_fwd_algo_;
      
      // optional field cudnn_conv_force_bwd_data_algo
     public:
    bool has_cudnn_conv_force_bwd_data_algo() const;
    const int32_t& cudnn_conv_force_bwd_data_algo() const;
    void clear_cudnn_conv_force_bwd_data_algo();
    void set_cudnn_conv_force_bwd_data_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_bwd_data_algo();
   protected:
    bool has_cudnn_conv_force_bwd_data_algo_ = false;
    int32_t cudnn_conv_force_bwd_data_algo_;
      
      // optional field cudnn_conv_force_bwd_filter_algo
     public:
    bool has_cudnn_conv_force_bwd_filter_algo() const;
    const int32_t& cudnn_conv_force_bwd_filter_algo() const;
    void clear_cudnn_conv_force_bwd_filter_algo();
    void set_cudnn_conv_force_bwd_filter_algo(const int32_t& value);
    int32_t* mutable_cudnn_conv_force_bwd_filter_algo();
   protected:
    bool has_cudnn_conv_force_bwd_filter_algo_ = false;
    int32_t cudnn_conv_force_bwd_filter_algo_;
      
      // optional field cudnn_conv_heuristic_search_algo
     public:
    bool has_cudnn_conv_heuristic_search_algo() const;
    const bool& cudnn_conv_heuristic_search_algo() const;
    void clear_cudnn_conv_heuristic_search_algo();
    void set_cudnn_conv_heuristic_search_algo(const bool& value);
    bool* mutable_cudnn_conv_heuristic_search_algo();
   protected:
    bool has_cudnn_conv_heuristic_search_algo_ = false;
    bool cudnn_conv_heuristic_search_algo_;
      
      // optional field cudnn_conv_use_deterministic_algo_only
     public:
    bool has_cudnn_conv_use_deterministic_algo_only() const;
    const bool& cudnn_conv_use_deterministic_algo_only() const;
    void clear_cudnn_conv_use_deterministic_algo_only();
    void set_cudnn_conv_use_deterministic_algo_only(const bool& value);
    bool* mutable_cudnn_conv_use_deterministic_algo_only();
   protected:
    bool has_cudnn_conv_use_deterministic_algo_only_ = false;
    bool cudnn_conv_use_deterministic_algo_only_;
      
      // optional field enable_cudnn_fused_normalization_add_relu
     public:
    bool has_enable_cudnn_fused_normalization_add_relu() const;
    const bool& enable_cudnn_fused_normalization_add_relu() const;
    void clear_enable_cudnn_fused_normalization_add_relu();
    void set_enable_cudnn_fused_normalization_add_relu(const bool& value);
    bool* mutable_enable_cudnn_fused_normalization_add_relu();
   protected:
    bool has_enable_cudnn_fused_normalization_add_relu_ = false;
    bool enable_cudnn_fused_normalization_add_relu_;
      
      // optional field cudnn_conv_enable_pseudo_half
     public:
    bool has_cudnn_conv_enable_pseudo_half() const;
    const bool& cudnn_conv_enable_pseudo_half() const;
    void clear_cudnn_conv_enable_pseudo_half();
    void set_cudnn_conv_enable_pseudo_half(const bool& value);
    bool* mutable_cudnn_conv_enable_pseudo_half();
   protected:
    bool has_cudnn_conv_enable_pseudo_half_ = false;
    bool cudnn_conv_enable_pseudo_half_;
           
   public:
    int compare(const _CudnnConfig_& other);

    bool operator==(const _CudnnConfig_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CudnnConfig_& other) const;
  };

  ConstCudnnConfig(const ::std::shared_ptr<_CudnnConfig_>& data);
  ConstCudnnConfig(const ConstCudnnConfig&);
  ConstCudnnConfig(ConstCudnnConfig&&) noexcept;
  ConstCudnnConfig();
  ConstCudnnConfig(const ::oneflow::CudnnConfig& proto_cudnnconfig);
  virtual ~ConstCudnnConfig() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_cudnnconfig) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field enable_cudnn
 public:
  bool has_enable_cudnn() const;
  const bool& enable_cudnn() const;
  // used by pybind11 only
  // required or optional field cudnn_buf_limit_mbyte
 public:
  bool has_cudnn_buf_limit_mbyte() const;
  const int64_t& cudnn_buf_limit_mbyte() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_fwd_algo
 public:
  bool has_cudnn_conv_force_fwd_algo() const;
  const int32_t& cudnn_conv_force_fwd_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_bwd_data_algo
 public:
  bool has_cudnn_conv_force_bwd_data_algo() const;
  const int32_t& cudnn_conv_force_bwd_data_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_force_bwd_filter_algo
 public:
  bool has_cudnn_conv_force_bwd_filter_algo() const;
  const int32_t& cudnn_conv_force_bwd_filter_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_heuristic_search_algo
 public:
  bool has_cudnn_conv_heuristic_search_algo() const;
  const bool& cudnn_conv_heuristic_search_algo() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_use_deterministic_algo_only
 public:
  bool has_cudnn_conv_use_deterministic_algo_only() const;
  const bool& cudnn_conv_use_deterministic_algo_only() const;
  // used by pybind11 only
  // required or optional field enable_cudnn_fused_normalization_add_relu
 public:
  bool has_enable_cudnn_fused_normalization_add_relu() const;
  const bool& enable_cudnn_fused_normalization_add_relu() const;
  // used by pybind11 only
  // required or optional field cudnn_conv_enable_pseudo_half
 public:
  bool has_cudnn_conv_enable_pseudo_half() const;
  const bool& cudnn_conv_enable_pseudo_half() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstCudnnConfig> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCudnnConfig& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCudnnConfig& other) const;
 protected:
  const ::std::shared_ptr<_CudnnConfig_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CudnnConfig_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCudnnConfig
  void BuildFromProto(const PbMessage& proto_cudnnconfig);
  
  ::std::shared_ptr<_CudnnConfig_> data_;
};

class CudnnConfig final : public ConstCudnnConfig {
 public:
  CudnnConfig(const ::std::shared_ptr<_CudnnConfig_>& data);
  CudnnConfig(const CudnnConfig& other);
  // enable nothrow for ::std::vector<CudnnConfig> resize 
  CudnnConfig(CudnnConfig&&) noexcept;
  CudnnConfig();
  explicit CudnnConfig(const ::oneflow::CudnnConfig& proto_cudnnconfig);

  ~CudnnConfig() override;

  void InitFromProto(const PbMessage& proto_cudnnconfig) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CudnnConfig& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CudnnConfig& other) const;
  void Clear();
  void CopyFrom(const CudnnConfig& other);
  CudnnConfig& operator=(const CudnnConfig& other);

  // required or optional field enable_cudnn
 public:
  void clear_enable_cudnn();
  void set_enable_cudnn(const bool& value);
  bool* mutable_enable_cudnn();
  // required or optional field cudnn_buf_limit_mbyte
 public:
  void clear_cudnn_buf_limit_mbyte();
  void set_cudnn_buf_limit_mbyte(const int64_t& value);
  int64_t* mutable_cudnn_buf_limit_mbyte();
  // required or optional field cudnn_conv_force_fwd_algo
 public:
  void clear_cudnn_conv_force_fwd_algo();
  void set_cudnn_conv_force_fwd_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_fwd_algo();
  // required or optional field cudnn_conv_force_bwd_data_algo
 public:
  void clear_cudnn_conv_force_bwd_data_algo();
  void set_cudnn_conv_force_bwd_data_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_bwd_data_algo();
  // required or optional field cudnn_conv_force_bwd_filter_algo
 public:
  void clear_cudnn_conv_force_bwd_filter_algo();
  void set_cudnn_conv_force_bwd_filter_algo(const int32_t& value);
  int32_t* mutable_cudnn_conv_force_bwd_filter_algo();
  // required or optional field cudnn_conv_heuristic_search_algo
 public:
  void clear_cudnn_conv_heuristic_search_algo();
  void set_cudnn_conv_heuristic_search_algo(const bool& value);
  bool* mutable_cudnn_conv_heuristic_search_algo();
  // required or optional field cudnn_conv_use_deterministic_algo_only
 public:
  void clear_cudnn_conv_use_deterministic_algo_only();
  void set_cudnn_conv_use_deterministic_algo_only(const bool& value);
  bool* mutable_cudnn_conv_use_deterministic_algo_only();
  // required or optional field enable_cudnn_fused_normalization_add_relu
 public:
  void clear_enable_cudnn_fused_normalization_add_relu();
  void set_enable_cudnn_fused_normalization_add_relu(const bool& value);
  bool* mutable_enable_cudnn_fused_normalization_add_relu();
  // required or optional field cudnn_conv_enable_pseudo_half
 public:
  void clear_cudnn_conv_enable_pseudo_half();
  void set_cudnn_conv_enable_pseudo_half(const bool& value);
  bool* mutable_cudnn_conv_enable_pseudo_half();

  ::std::shared_ptr<CudnnConfig> __SharedMutable__();
};


class ConstResource : public ::oneflow::cfg::Message {
 public:

  class _Resource_ {
   public:
    _Resource_();
    explicit _Resource_(const _Resource_& other);
    explicit _Resource_(_Resource_&& other);
    _Resource_(const ::oneflow::Resource& proto_resource);
    ~_Resource_();

    void InitFromProto(const ::oneflow::Resource& proto_resource);

    void ToProto(::oneflow::Resource* proto_resource) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _Resource_& other);
  
      // optional field machine_num
     public:
    bool has_machine_num() const;
    const int32_t& machine_num() const;
    void clear_machine_num();
    void set_machine_num(const int32_t& value);
    int32_t* mutable_machine_num();
   protected:
    bool has_machine_num_ = false;
    int32_t machine_num_;
      
      // optional field gpu_device_num
     public:
    bool has_gpu_device_num() const;
    const int32_t& gpu_device_num() const;
    void clear_gpu_device_num();
    void set_gpu_device_num(const int32_t& value);
    int32_t* mutable_gpu_device_num();
   protected:
    bool has_gpu_device_num_ = false;
    int32_t gpu_device_num_;
      
      // optional field cpu_device_num
     public:
    bool has_cpu_device_num() const;
    const int32_t& cpu_device_num() const;
    void clear_cpu_device_num();
    void set_cpu_device_num(const int32_t& value);
    int32_t* mutable_cpu_device_num();
   protected:
    bool has_cpu_device_num_ = false;
    int32_t cpu_device_num_;
      
      // optional field comm_net_worker_num
     public:
    bool has_comm_net_worker_num() const;
    const int32_t& comm_net_worker_num() const;
    void clear_comm_net_worker_num();
    void set_comm_net_worker_num(const int32_t& value);
    int32_t* mutable_comm_net_worker_num();
   protected:
    bool has_comm_net_worker_num_ = false;
    int32_t comm_net_worker_num_;
      
      // optional field max_mdsave_worker_num
     public:
    bool has_max_mdsave_worker_num() const;
    const int32_t& max_mdsave_worker_num() const;
    void clear_max_mdsave_worker_num();
    void set_max_mdsave_worker_num(const int32_t& value);
    int32_t* mutable_max_mdsave_worker_num();
   protected:
    bool has_max_mdsave_worker_num_ = false;
    int32_t max_mdsave_worker_num_;
      
      // optional field reserved_host_mem_mbyte
     public:
    bool has_reserved_host_mem_mbyte() const;
    const uint64_t& reserved_host_mem_mbyte() const;
    void clear_reserved_host_mem_mbyte();
    void set_reserved_host_mem_mbyte(const uint64_t& value);
    uint64_t* mutable_reserved_host_mem_mbyte();
   protected:
    bool has_reserved_host_mem_mbyte_ = false;
    uint64_t reserved_host_mem_mbyte_;
      
      // optional field reserved_device_mem_mbyte
     public:
    bool has_reserved_device_mem_mbyte() const;
    const uint64_t& reserved_device_mem_mbyte() const;
    void clear_reserved_device_mem_mbyte();
    void set_reserved_device_mem_mbyte(const uint64_t& value);
    uint64_t* mutable_reserved_device_mem_mbyte();
   protected:
    bool has_reserved_device_mem_mbyte_ = false;
    uint64_t reserved_device_mem_mbyte_;
      
      // optional field compute_thread_pool_size
     public:
    bool has_compute_thread_pool_size() const;
    const int32_t& compute_thread_pool_size() const;
    void clear_compute_thread_pool_size();
    void set_compute_thread_pool_size(const int32_t& value);
    int32_t* mutable_compute_thread_pool_size();
   protected:
    bool has_compute_thread_pool_size_ = false;
    int32_t compute_thread_pool_size_;
      
      // optional field enable_thread_local_cache
     public:
    bool has_enable_thread_local_cache() const;
    const bool& enable_thread_local_cache() const;
    void clear_enable_thread_local_cache();
    void set_enable_thread_local_cache(const bool& value);
    bool* mutable_enable_thread_local_cache();
   protected:
    bool has_enable_thread_local_cache_ = false;
    bool enable_thread_local_cache_;
      
      // optional field thread_local_cache_max_size
     public:
    bool has_thread_local_cache_max_size() const;
    const int64_t& thread_local_cache_max_size() const;
    void clear_thread_local_cache_max_size();
    void set_thread_local_cache_max_size(const int64_t& value);
    int64_t* mutable_thread_local_cache_max_size();
   protected:
    bool has_thread_local_cache_max_size_ = false;
    int64_t thread_local_cache_max_size_;
      
      // optional field enable_debug_mode
     public:
    bool has_enable_debug_mode() const;
    const bool& enable_debug_mode() const;
    void clear_enable_debug_mode();
    void set_enable_debug_mode(const bool& value);
    bool* mutable_enable_debug_mode();
   protected:
    bool has_enable_debug_mode_ = false;
    bool enable_debug_mode_;
      
      // optional field enable_tensor_float_32_compute
     public:
    bool has_enable_tensor_float_32_compute() const;
    const bool& enable_tensor_float_32_compute() const;
    void clear_enable_tensor_float_32_compute();
    void set_enable_tensor_float_32_compute(const bool& value);
    bool* mutable_enable_tensor_float_32_compute();
   protected:
    bool has_enable_tensor_float_32_compute_ = false;
    bool enable_tensor_float_32_compute_;
      
      // optional field enable_mem_chain_merge
     public:
    bool has_enable_mem_chain_merge() const;
    const bool& enable_mem_chain_merge() const;
    void clear_enable_mem_chain_merge();
    void set_enable_mem_chain_merge(const bool& value);
    bool* mutable_enable_mem_chain_merge();
   protected:
    bool has_enable_mem_chain_merge_ = false;
    bool enable_mem_chain_merge_;
      
      // optional field collective_boxing_conf
     public:
    bool has_collective_boxing_conf() const;
    const ::oneflow::cfg::CollectiveBoxingConf& collective_boxing_conf() const;
    void clear_collective_boxing_conf();
    ::oneflow::cfg::CollectiveBoxingConf* mutable_collective_boxing_conf();
   protected:
    bool has_collective_boxing_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingConf> collective_boxing_conf_;
      
      // optional field nccl_use_compute_stream
     public:
    bool has_nccl_use_compute_stream() const;
    const bool& nccl_use_compute_stream() const;
    void clear_nccl_use_compute_stream();
    void set_nccl_use_compute_stream(const bool& value);
    bool* mutable_nccl_use_compute_stream();
   protected:
    bool has_nccl_use_compute_stream_ = false;
    bool nccl_use_compute_stream_;
      
      // optional field disable_group_boxing_by_dst_parallel
     public:
    bool has_disable_group_boxing_by_dst_parallel() const;
    const bool& disable_group_boxing_by_dst_parallel() const;
    void clear_disable_group_boxing_by_dst_parallel();
    void set_disable_group_boxing_by_dst_parallel(const bool& value);
    bool* mutable_disable_group_boxing_by_dst_parallel();
   protected:
    bool has_disable_group_boxing_by_dst_parallel_ = false;
    bool disable_group_boxing_by_dst_parallel_;
      
      // optional field cudnn_conf
     public:
    bool has_cudnn_conf() const;
    const ::oneflow::cfg::CudnnConfig& cudnn_conf() const;
    void clear_cudnn_conf();
    ::oneflow::cfg::CudnnConfig* mutable_cudnn_conf();
   protected:
    bool has_cudnn_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::CudnnConfig> cudnn_conf_;
      
      // optional field enable_model_io_v2
     public:
    bool has_enable_model_io_v2() const;
    const bool& enable_model_io_v2() const;
    void clear_enable_model_io_v2();
    void set_enable_model_io_v2(const bool& value);
    bool* mutable_enable_model_io_v2();
   protected:
    bool has_enable_model_io_v2_ = false;
    bool enable_model_io_v2_;
      
      // optional field enable_legacy_model_io
     public:
    bool has_enable_legacy_model_io() const;
    const bool& enable_legacy_model_io() const;
    void clear_enable_legacy_model_io();
    void set_enable_legacy_model_io(const bool& value);
    bool* mutable_enable_legacy_model_io();
   protected:
    bool has_enable_legacy_model_io_ = false;
    bool enable_legacy_model_io_;
           
   public:
    int compare(const _Resource_& other);

    bool operator==(const _Resource_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _Resource_& other) const;
  };

  ConstResource(const ::std::shared_ptr<_Resource_>& data);
  ConstResource(const ConstResource&);
  ConstResource(ConstResource&&) noexcept;
  ConstResource();
  ConstResource(const ::oneflow::Resource& proto_resource);
  virtual ~ConstResource() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_resource) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field machine_num
 public:
  bool has_machine_num() const;
  const int32_t& machine_num() const;
  // used by pybind11 only
  // required or optional field gpu_device_num
 public:
  bool has_gpu_device_num() const;
  const int32_t& gpu_device_num() const;
  // used by pybind11 only
  // required or optional field cpu_device_num
 public:
  bool has_cpu_device_num() const;
  const int32_t& cpu_device_num() const;
  // used by pybind11 only
  // required or optional field comm_net_worker_num
 public:
  bool has_comm_net_worker_num() const;
  const int32_t& comm_net_worker_num() const;
  // used by pybind11 only
  // required or optional field max_mdsave_worker_num
 public:
  bool has_max_mdsave_worker_num() const;
  const int32_t& max_mdsave_worker_num() const;
  // used by pybind11 only
  // required or optional field reserved_host_mem_mbyte
 public:
  bool has_reserved_host_mem_mbyte() const;
  const uint64_t& reserved_host_mem_mbyte() const;
  // used by pybind11 only
  // required or optional field reserved_device_mem_mbyte
 public:
  bool has_reserved_device_mem_mbyte() const;
  const uint64_t& reserved_device_mem_mbyte() const;
  // used by pybind11 only
  // required or optional field compute_thread_pool_size
 public:
  bool has_compute_thread_pool_size() const;
  const int32_t& compute_thread_pool_size() const;
  // used by pybind11 only
  // required or optional field enable_thread_local_cache
 public:
  bool has_enable_thread_local_cache() const;
  const bool& enable_thread_local_cache() const;
  // used by pybind11 only
  // required or optional field thread_local_cache_max_size
 public:
  bool has_thread_local_cache_max_size() const;
  const int64_t& thread_local_cache_max_size() const;
  // used by pybind11 only
  // required or optional field enable_debug_mode
 public:
  bool has_enable_debug_mode() const;
  const bool& enable_debug_mode() const;
  // used by pybind11 only
  // required or optional field enable_tensor_float_32_compute
 public:
  bool has_enable_tensor_float_32_compute() const;
  const bool& enable_tensor_float_32_compute() const;
  // used by pybind11 only
  // required or optional field enable_mem_chain_merge
 public:
  bool has_enable_mem_chain_merge() const;
  const bool& enable_mem_chain_merge() const;
  // used by pybind11 only
  // required or optional field collective_boxing_conf
 public:
  bool has_collective_boxing_conf() const;
  const ::oneflow::cfg::CollectiveBoxingConf& collective_boxing_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCollectiveBoxingConf> shared_const_collective_boxing_conf() const;
  // required or optional field nccl_use_compute_stream
 public:
  bool has_nccl_use_compute_stream() const;
  const bool& nccl_use_compute_stream() const;
  // used by pybind11 only
  // required or optional field disable_group_boxing_by_dst_parallel
 public:
  bool has_disable_group_boxing_by_dst_parallel() const;
  const bool& disable_group_boxing_by_dst_parallel() const;
  // used by pybind11 only
  // required or optional field cudnn_conf
 public:
  bool has_cudnn_conf() const;
  const ::oneflow::cfg::CudnnConfig& cudnn_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCudnnConfig> shared_const_cudnn_conf() const;
  // required or optional field enable_model_io_v2
 public:
  bool has_enable_model_io_v2() const;
  const bool& enable_model_io_v2() const;
  // used by pybind11 only
  // required or optional field enable_legacy_model_io
 public:
  bool has_enable_legacy_model_io() const;
  const bool& enable_legacy_model_io() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstResource> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstResource& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstResource& other) const;
 protected:
  const ::std::shared_ptr<_Resource_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_Resource_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstResource
  void BuildFromProto(const PbMessage& proto_resource);
  
  ::std::shared_ptr<_Resource_> data_;
};

class Resource final : public ConstResource {
 public:
  Resource(const ::std::shared_ptr<_Resource_>& data);
  Resource(const Resource& other);
  // enable nothrow for ::std::vector<Resource> resize 
  Resource(Resource&&) noexcept;
  Resource();
  explicit Resource(const ::oneflow::Resource& proto_resource);

  ~Resource() override;

  void InitFromProto(const PbMessage& proto_resource) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const Resource& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Resource& other) const;
  void Clear();
  void CopyFrom(const Resource& other);
  Resource& operator=(const Resource& other);

  // required or optional field machine_num
 public:
  void clear_machine_num();
  void set_machine_num(const int32_t& value);
  int32_t* mutable_machine_num();
  // required or optional field gpu_device_num
 public:
  void clear_gpu_device_num();
  void set_gpu_device_num(const int32_t& value);
  int32_t* mutable_gpu_device_num();
  // required or optional field cpu_device_num
 public:
  void clear_cpu_device_num();
  void set_cpu_device_num(const int32_t& value);
  int32_t* mutable_cpu_device_num();
  // required or optional field comm_net_worker_num
 public:
  void clear_comm_net_worker_num();
  void set_comm_net_worker_num(const int32_t& value);
  int32_t* mutable_comm_net_worker_num();
  // required or optional field max_mdsave_worker_num
 public:
  void clear_max_mdsave_worker_num();
  void set_max_mdsave_worker_num(const int32_t& value);
  int32_t* mutable_max_mdsave_worker_num();
  // required or optional field reserved_host_mem_mbyte
 public:
  void clear_reserved_host_mem_mbyte();
  void set_reserved_host_mem_mbyte(const uint64_t& value);
  uint64_t* mutable_reserved_host_mem_mbyte();
  // required or optional field reserved_device_mem_mbyte
 public:
  void clear_reserved_device_mem_mbyte();
  void set_reserved_device_mem_mbyte(const uint64_t& value);
  uint64_t* mutable_reserved_device_mem_mbyte();
  // required or optional field compute_thread_pool_size
 public:
  void clear_compute_thread_pool_size();
  void set_compute_thread_pool_size(const int32_t& value);
  int32_t* mutable_compute_thread_pool_size();
  // required or optional field enable_thread_local_cache
 public:
  void clear_enable_thread_local_cache();
  void set_enable_thread_local_cache(const bool& value);
  bool* mutable_enable_thread_local_cache();
  // required or optional field thread_local_cache_max_size
 public:
  void clear_thread_local_cache_max_size();
  void set_thread_local_cache_max_size(const int64_t& value);
  int64_t* mutable_thread_local_cache_max_size();
  // required or optional field enable_debug_mode
 public:
  void clear_enable_debug_mode();
  void set_enable_debug_mode(const bool& value);
  bool* mutable_enable_debug_mode();
  // required or optional field enable_tensor_float_32_compute
 public:
  void clear_enable_tensor_float_32_compute();
  void set_enable_tensor_float_32_compute(const bool& value);
  bool* mutable_enable_tensor_float_32_compute();
  // required or optional field enable_mem_chain_merge
 public:
  void clear_enable_mem_chain_merge();
  void set_enable_mem_chain_merge(const bool& value);
  bool* mutable_enable_mem_chain_merge();
  // required or optional field collective_boxing_conf
 public:
  void clear_collective_boxing_conf();
  ::oneflow::cfg::CollectiveBoxingConf* mutable_collective_boxing_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingConf> shared_mutable_collective_boxing_conf();
  // required or optional field nccl_use_compute_stream
 public:
  void clear_nccl_use_compute_stream();
  void set_nccl_use_compute_stream(const bool& value);
  bool* mutable_nccl_use_compute_stream();
  // required or optional field disable_group_boxing_by_dst_parallel
 public:
  void clear_disable_group_boxing_by_dst_parallel();
  void set_disable_group_boxing_by_dst_parallel(const bool& value);
  bool* mutable_disable_group_boxing_by_dst_parallel();
  // required or optional field cudnn_conf
 public:
  void clear_cudnn_conf();
  ::oneflow::cfg::CudnnConfig* mutable_cudnn_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CudnnConfig> shared_mutable_cudnn_conf();
  // required or optional field enable_model_io_v2
 public:
  void clear_enable_model_io_v2();
  void set_enable_model_io_v2(const bool& value);
  bool* mutable_enable_model_io_v2();
  // required or optional field enable_legacy_model_io
 public:
  void clear_enable_legacy_model_io();
  void set_enable_legacy_model_io(const bool& value);
  bool* mutable_enable_legacy_model_io();

  ::std::shared_ptr<Resource> __SharedMutable__();
};












} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstCollectiveBoxingConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCollectiveBoxingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CollectiveBoxingConf> {
  std::size_t operator()(const ::oneflow::cfg::CollectiveBoxingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCudnnConfig> {
  std::size_t operator()(const ::oneflow::cfg::ConstCudnnConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CudnnConfig> {
  std::size_t operator()(const ::oneflow::cfg::CudnnConfig& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstResource> {
  std::size_t operator()(const ::oneflow::cfg::ConstResource& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::Resource> {
  std::size_t operator()(const ::oneflow::cfg::Resource& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_RESOURCE_CFG_H_