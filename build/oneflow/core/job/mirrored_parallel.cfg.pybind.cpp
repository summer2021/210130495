#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.mirrored_parallel", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstOptMirroredParallel>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstOptMirroredParallel>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstOptMirroredParallel> (Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::*)(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_&))&_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::*)(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_&))&_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<OptMirroredParallel>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<OptMirroredParallel>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> (_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstMirroredParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstMirroredParallel>> registry(m, "ConstMirroredParallel");
    registry.def("__id__", &::oneflow::cfg::MirroredParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstMirroredParallel::DebugString);
    registry.def("__repr__", &ConstMirroredParallel::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::MirroredParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::MirroredParallel>> registry(m, "MirroredParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::MirroredParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::MirroredParallel::*)(const ConstMirroredParallel&))&::oneflow::cfg::MirroredParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::MirroredParallel::*)(const ::oneflow::cfg::MirroredParallel&))&::oneflow::cfg::MirroredParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::MirroredParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::MirroredParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::MirroredParallel::DebugString);


  }
  {
    pybind11::class_<ConstOptMirroredParallel, ::oneflow::cfg::Message, std::shared_ptr<ConstOptMirroredParallel>> registry(m, "ConstOptMirroredParallel");
    registry.def("__id__", &::oneflow::cfg::OptMirroredParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOptMirroredParallel::DebugString);
    registry.def("__repr__", &ConstOptMirroredParallel::DebugString);

    registry.def("has_mirrored_parallel", &ConstOptMirroredParallel::has_mirrored_parallel);
    registry.def("mirrored_parallel", &ConstOptMirroredParallel::shared_const_mirrored_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::OptMirroredParallel, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OptMirroredParallel>> registry(m, "OptMirroredParallel");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OptMirroredParallel::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptMirroredParallel::*)(const ConstOptMirroredParallel&))&::oneflow::cfg::OptMirroredParallel::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OptMirroredParallel::*)(const ::oneflow::cfg::OptMirroredParallel&))&::oneflow::cfg::OptMirroredParallel::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OptMirroredParallel::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OptMirroredParallel::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OptMirroredParallel::DebugString);



    registry.def("has_mirrored_parallel", &::oneflow::cfg::OptMirroredParallel::has_mirrored_parallel);
    registry.def("clear_mirrored_parallel", &::oneflow::cfg::OptMirroredParallel::clear_mirrored_parallel);
    registry.def("mirrored_parallel", &::oneflow::cfg::OptMirroredParallel::shared_const_mirrored_parallel);
    registry.def("mutable_mirrored_parallel", &::oneflow::cfg::OptMirroredParallel::shared_mutable_mirrored_parallel);
  }
  {
    pybind11::class_<ConstMirroredSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstMirroredSignature>> registry(m, "ConstMirroredSignature");
    registry.def("__id__", &::oneflow::cfg::MirroredSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstMirroredSignature::DebugString);
    registry.def("__repr__", &ConstMirroredSignature::DebugString);

    registry.def("bn_in_op2opt_mirrored_parallel_size", &ConstMirroredSignature::bn_in_op2opt_mirrored_parallel_size);
    registry.def("bn_in_op2opt_mirrored_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> (ConstMirroredSignature::*)() const)&ConstMirroredSignature::shared_const_bn_in_op2opt_mirrored_parallel);

    registry.def("bn_in_op2opt_mirrored_parallel", (::std::shared_ptr<ConstOptMirroredParallel> (ConstMirroredSignature::*)(const ::std::string&) const)&ConstMirroredSignature::shared_const_bn_in_op2opt_mirrored_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::MirroredSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::MirroredSignature>> registry(m, "MirroredSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::MirroredSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::MirroredSignature::*)(const ConstMirroredSignature&))&::oneflow::cfg::MirroredSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::MirroredSignature::*)(const ::oneflow::cfg::MirroredSignature&))&::oneflow::cfg::MirroredSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::MirroredSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::MirroredSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::MirroredSignature::DebugString);



    registry.def("bn_in_op2opt_mirrored_parallel_size", &::oneflow::cfg::MirroredSignature::bn_in_op2opt_mirrored_parallel_size);
    registry.def("bn_in_op2opt_mirrored_parallel", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> (::oneflow::cfg::MirroredSignature::*)() const)&::oneflow::cfg::MirroredSignature::shared_const_bn_in_op2opt_mirrored_parallel);
    registry.def("clear_bn_in_op2opt_mirrored_parallel", &::oneflow::cfg::MirroredSignature::clear_bn_in_op2opt_mirrored_parallel);
    registry.def("mutable_bn_in_op2opt_mirrored_parallel", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> (::oneflow::cfg::MirroredSignature::*)())&::oneflow::cfg::MirroredSignature::shared_mutable_bn_in_op2opt_mirrored_parallel);
    registry.def("bn_in_op2opt_mirrored_parallel", (::std::shared_ptr<ConstOptMirroredParallel> (::oneflow::cfg::MirroredSignature::*)(const ::std::string&) const)&::oneflow::cfg::MirroredSignature::shared_const_bn_in_op2opt_mirrored_parallel);
  }
}