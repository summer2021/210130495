#ifndef CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class BlobLastUsedSignature_BnInOp2blobLastUsedEntry;
class BlobLastUsedSignature;
class BlobBackwardUsedSignature_BnInOp2blobBackwardUsedEntry;
class BlobBackwardUsedSignature;

namespace cfg {


class BlobLastUsedSignature;
class ConstBlobLastUsedSignature;

class BlobBackwardUsedSignature;
class ConstBlobBackwardUsedSignature;


class _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_; 
class Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_;

class ConstBlobLastUsedSignature : public ::oneflow::cfg::Message {
 public:

  class _BlobLastUsedSignature_ {
   public:
    _BlobLastUsedSignature_();
    explicit _BlobLastUsedSignature_(const _BlobLastUsedSignature_& other);
    explicit _BlobLastUsedSignature_(_BlobLastUsedSignature_&& other);
    _BlobLastUsedSignature_(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature);
    ~_BlobLastUsedSignature_();

    void InitFromProto(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature);

    void ToProto(::oneflow::BlobLastUsedSignature* proto_bloblastusedsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobLastUsedSignature_& other);
  
     public:
    ::std::size_t bn_in_op2blob_last_used_size() const;
    const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& bn_in_op2blob_last_used() const;

    _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ * mutable_bn_in_op2blob_last_used();

    const bool& bn_in_op2blob_last_used(::std::string key) const;

    void clear_bn_in_op2blob_last_used();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> bn_in_op2blob_last_used_;
         
   public:
    int compare(const _BlobLastUsedSignature_& other);

    bool operator==(const _BlobLastUsedSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobLastUsedSignature_& other) const;
  };

  ConstBlobLastUsedSignature(const ::std::shared_ptr<_BlobLastUsedSignature_>& data);
  ConstBlobLastUsedSignature(const ConstBlobLastUsedSignature&);
  ConstBlobLastUsedSignature(ConstBlobLastUsedSignature&&) noexcept;
  ConstBlobLastUsedSignature();
  ConstBlobLastUsedSignature(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature);
  virtual ~ConstBlobLastUsedSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_bloblastusedsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2blob_last_used
 public:
  ::std::size_t bn_in_op2blob_last_used_size() const;
  const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& bn_in_op2blob_last_used() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> shared_const_bn_in_op2blob_last_used() const;

 public:
  ::std::shared_ptr<ConstBlobLastUsedSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobLastUsedSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobLastUsedSignature& other) const;
 protected:
  const ::std::shared_ptr<_BlobLastUsedSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobLastUsedSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobLastUsedSignature
  void BuildFromProto(const PbMessage& proto_bloblastusedsignature);
  
  ::std::shared_ptr<_BlobLastUsedSignature_> data_;
};

class BlobLastUsedSignature final : public ConstBlobLastUsedSignature {
 public:
  BlobLastUsedSignature(const ::std::shared_ptr<_BlobLastUsedSignature_>& data);
  BlobLastUsedSignature(const BlobLastUsedSignature& other);
  // enable nothrow for ::std::vector<BlobLastUsedSignature> resize 
  BlobLastUsedSignature(BlobLastUsedSignature&&) noexcept;
  BlobLastUsedSignature();
  explicit BlobLastUsedSignature(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature);

  ~BlobLastUsedSignature() override;

  void InitFromProto(const PbMessage& proto_bloblastusedsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobLastUsedSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobLastUsedSignature& other) const;
  void Clear();
  void CopyFrom(const BlobLastUsedSignature& other);
  BlobLastUsedSignature& operator=(const BlobLastUsedSignature& other);

  // repeated field bn_in_op2blob_last_used
 public:
  void clear_bn_in_op2blob_last_used();

  const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ & bn_in_op2blob_last_used() const;

  _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_* mutable_bn_in_op2blob_last_used();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> shared_mutable_bn_in_op2blob_last_used();

  ::std::shared_ptr<BlobLastUsedSignature> __SharedMutable__();
};


class ConstBlobBackwardUsedSignature : public ::oneflow::cfg::Message {
 public:

  class _BlobBackwardUsedSignature_ {
   public:
    _BlobBackwardUsedSignature_();
    explicit _BlobBackwardUsedSignature_(const _BlobBackwardUsedSignature_& other);
    explicit _BlobBackwardUsedSignature_(_BlobBackwardUsedSignature_&& other);
    _BlobBackwardUsedSignature_(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature);
    ~_BlobBackwardUsedSignature_();

    void InitFromProto(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature);

    void ToProto(::oneflow::BlobBackwardUsedSignature* proto_blobbackwardusedsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobBackwardUsedSignature_& other);
  
     public:
    ::std::size_t bn_in_op2blob_backward_used_size() const;
    const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& bn_in_op2blob_backward_used() const;

    _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ * mutable_bn_in_op2blob_backward_used();

    const bool& bn_in_op2blob_backward_used(::std::string key) const;

    void clear_bn_in_op2blob_backward_used();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> bn_in_op2blob_backward_used_;
         
   public:
    int compare(const _BlobBackwardUsedSignature_& other);

    bool operator==(const _BlobBackwardUsedSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobBackwardUsedSignature_& other) const;
  };

  ConstBlobBackwardUsedSignature(const ::std::shared_ptr<_BlobBackwardUsedSignature_>& data);
  ConstBlobBackwardUsedSignature(const ConstBlobBackwardUsedSignature&);
  ConstBlobBackwardUsedSignature(ConstBlobBackwardUsedSignature&&) noexcept;
  ConstBlobBackwardUsedSignature();
  ConstBlobBackwardUsedSignature(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature);
  virtual ~ConstBlobBackwardUsedSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_blobbackwardusedsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field bn_in_op2blob_backward_used
 public:
  ::std::size_t bn_in_op2blob_backward_used_size() const;
  const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& bn_in_op2blob_backward_used() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> shared_const_bn_in_op2blob_backward_used() const;

 public:
  ::std::shared_ptr<ConstBlobBackwardUsedSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobBackwardUsedSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobBackwardUsedSignature& other) const;
 protected:
  const ::std::shared_ptr<_BlobBackwardUsedSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobBackwardUsedSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobBackwardUsedSignature
  void BuildFromProto(const PbMessage& proto_blobbackwardusedsignature);
  
  ::std::shared_ptr<_BlobBackwardUsedSignature_> data_;
};

class BlobBackwardUsedSignature final : public ConstBlobBackwardUsedSignature {
 public:
  BlobBackwardUsedSignature(const ::std::shared_ptr<_BlobBackwardUsedSignature_>& data);
  BlobBackwardUsedSignature(const BlobBackwardUsedSignature& other);
  // enable nothrow for ::std::vector<BlobBackwardUsedSignature> resize 
  BlobBackwardUsedSignature(BlobBackwardUsedSignature&&) noexcept;
  BlobBackwardUsedSignature();
  explicit BlobBackwardUsedSignature(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature);

  ~BlobBackwardUsedSignature() override;

  void InitFromProto(const PbMessage& proto_blobbackwardusedsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobBackwardUsedSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobBackwardUsedSignature& other) const;
  void Clear();
  void CopyFrom(const BlobBackwardUsedSignature& other);
  BlobBackwardUsedSignature& operator=(const BlobBackwardUsedSignature& other);

  // repeated field bn_in_op2blob_backward_used
 public:
  void clear_bn_in_op2blob_backward_used();

  const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ & bn_in_op2blob_backward_used() const;

  _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_* mutable_bn_in_op2blob_backward_used();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> shared_mutable_bn_in_op2blob_backward_used();

  ::std::shared_ptr<BlobBackwardUsedSignature> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ : public ::oneflow::cfg::_MapField_<::std::string, bool> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_();
  ~Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const;
  // used by pybind11 only
  const bool& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ final : public Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data);
  _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_();
  ~_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> __SharedMutable__();

  void Set(const ::std::string& key, const bool& value);
};






} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstBlobLastUsedSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobLastUsedSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobLastUsedSignature> {
  std::size_t operator()(const ::oneflow::cfg::BlobLastUsedSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBlobBackwardUsedSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobBackwardUsedSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobBackwardUsedSignature> {
  std::size_t operator()(const ::oneflow::cfg::BlobBackwardUsedSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H_