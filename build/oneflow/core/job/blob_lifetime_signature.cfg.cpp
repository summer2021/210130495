#include "oneflow/core/job/blob_lifetime_signature.cfg.h"
#include "oneflow/core/job/blob_lifetime_signature.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstBlobLastUsedSignature::_BlobLastUsedSignature_::_BlobLastUsedSignature_() { Clear(); }
ConstBlobLastUsedSignature::_BlobLastUsedSignature_::_BlobLastUsedSignature_(const _BlobLastUsedSignature_& other) { CopyFrom(other); }
ConstBlobLastUsedSignature::_BlobLastUsedSignature_::_BlobLastUsedSignature_(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature) {
  InitFromProto(proto_bloblastusedsignature);
}
ConstBlobLastUsedSignature::_BlobLastUsedSignature_::_BlobLastUsedSignature_(_BlobLastUsedSignature_&& other) = default;
ConstBlobLastUsedSignature::_BlobLastUsedSignature_::~_BlobLastUsedSignature_() = default;

void ConstBlobLastUsedSignature::_BlobLastUsedSignature_::InitFromProto(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature) {
  Clear();
  // map field : bn_in_op2blob_last_used
  if (!proto_bloblastusedsignature.bn_in_op2blob_last_used().empty()) {
_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_&  mut_bn_in_op2blob_last_used = *mutable_bn_in_op2blob_last_used();
    for (const auto& pair : proto_bloblastusedsignature.bn_in_op2blob_last_used()) {
      mut_bn_in_op2blob_last_used[pair.first] = pair.second;
      }
  }
    
}

void ConstBlobLastUsedSignature::_BlobLastUsedSignature_::ToProto(::oneflow::BlobLastUsedSignature* proto_bloblastusedsignature) const {
  proto_bloblastusedsignature->Clear();
  // map field : bn_in_op2blob_last_used
  if (!bn_in_op2blob_last_used().empty()) {
    auto& mut_bn_in_op2blob_last_used = *(proto_bloblastusedsignature->mutable_bn_in_op2blob_last_used());
    for (const auto& pair : bn_in_op2blob_last_used()) {
      mut_bn_in_op2blob_last_used[pair.first] = pair.second;
    }
  }

}

::std::string ConstBlobLastUsedSignature::_BlobLastUsedSignature_::DebugString() const {
  ::oneflow::BlobLastUsedSignature proto_bloblastusedsignature;
  this->ToProto(&proto_bloblastusedsignature);
  return proto_bloblastusedsignature.DebugString();
}

void ConstBlobLastUsedSignature::_BlobLastUsedSignature_::Clear() {
  clear_bn_in_op2blob_last_used();
}

void ConstBlobLastUsedSignature::_BlobLastUsedSignature_::CopyFrom(const _BlobLastUsedSignature_& other) {
  mutable_bn_in_op2blob_last_used()->CopyFrom(other.bn_in_op2blob_last_used());
}


::std::size_t ConstBlobLastUsedSignature::_BlobLastUsedSignature_::bn_in_op2blob_last_used_size() const {
  if (!bn_in_op2blob_last_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return default_static_value->size();
  }
  return bn_in_op2blob_last_used_->size();
}
const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& ConstBlobLastUsedSignature::_BlobLastUsedSignature_::bn_in_op2blob_last_used() const {
  if (!bn_in_op2blob_last_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2blob_last_used_.get());
}

_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ * ConstBlobLastUsedSignature::_BlobLastUsedSignature_::mutable_bn_in_op2blob_last_used() {
  if (!bn_in_op2blob_last_used_) {
    bn_in_op2blob_last_used_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
  }
  return bn_in_op2blob_last_used_.get();
}

const bool& ConstBlobLastUsedSignature::_BlobLastUsedSignature_::bn_in_op2blob_last_used(::std::string key) const {
  if (!bn_in_op2blob_last_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return default_static_value->at(key);
  }
  return bn_in_op2blob_last_used_->at(key);
}

void ConstBlobLastUsedSignature::_BlobLastUsedSignature_::clear_bn_in_op2blob_last_used() {
  if (!bn_in_op2blob_last_used_) {
    bn_in_op2blob_last_used_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
  }
  return bn_in_op2blob_last_used_->Clear();
}



int ConstBlobLastUsedSignature::_BlobLastUsedSignature_::compare(const _BlobLastUsedSignature_& other) {
  if (!(bn_in_op2blob_last_used() == other.bn_in_op2blob_last_used())) {
    return bn_in_op2blob_last_used() < other.bn_in_op2blob_last_used() ? -1 : 1;
  }
  return 0;
}

bool ConstBlobLastUsedSignature::_BlobLastUsedSignature_::operator==(const _BlobLastUsedSignature_& other) const {
  return true
    && bn_in_op2blob_last_used() == other.bn_in_op2blob_last_used()
  ;
}

std::size_t ConstBlobLastUsedSignature::_BlobLastUsedSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2blob_last_used().__CalcHash__()
  ;
}

bool ConstBlobLastUsedSignature::_BlobLastUsedSignature_::operator<(const _BlobLastUsedSignature_& other) const {
  return false
    || !(bn_in_op2blob_last_used() == other.bn_in_op2blob_last_used()) ? 
      bn_in_op2blob_last_used() < other.bn_in_op2blob_last_used() : false
  ;
}

using _BlobLastUsedSignature_ =  ConstBlobLastUsedSignature::_BlobLastUsedSignature_;
ConstBlobLastUsedSignature::ConstBlobLastUsedSignature(const ::std::shared_ptr<_BlobLastUsedSignature_>& data): data_(data) {}
ConstBlobLastUsedSignature::ConstBlobLastUsedSignature(): data_(::std::make_shared<_BlobLastUsedSignature_>()) {}
ConstBlobLastUsedSignature::ConstBlobLastUsedSignature(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature) {
  BuildFromProto(proto_bloblastusedsignature);
}
ConstBlobLastUsedSignature::ConstBlobLastUsedSignature(const ConstBlobLastUsedSignature&) = default;
ConstBlobLastUsedSignature::ConstBlobLastUsedSignature(ConstBlobLastUsedSignature&&) noexcept = default;
ConstBlobLastUsedSignature::~ConstBlobLastUsedSignature() = default;

void ConstBlobLastUsedSignature::ToProto(PbMessage* proto_bloblastusedsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobLastUsedSignature*>(proto_bloblastusedsignature));
}
  
::std::string ConstBlobLastUsedSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobLastUsedSignature::__Empty__() const {
  return !data_;
}

int ConstBlobLastUsedSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2blob_last_used", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBlobLastUsedSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBlobLastUsedSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, bool>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobLastUsedSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2blob_last_used();
    default: return nullptr;
  }
}

// map field bn_in_op2blob_last_used
::std::size_t ConstBlobLastUsedSignature::bn_in_op2blob_last_used_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_last_used_size();
}

const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& ConstBlobLastUsedSignature::bn_in_op2blob_last_used() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_last_used();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> ConstBlobLastUsedSignature::shared_const_bn_in_op2blob_last_used() const {
  return bn_in_op2blob_last_used().__SharedConst__();
}

::std::shared_ptr<ConstBlobLastUsedSignature> ConstBlobLastUsedSignature::__SharedConst__() const {
  return ::std::make_shared<ConstBlobLastUsedSignature>(data_);
}
int64_t ConstBlobLastUsedSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobLastUsedSignature::operator==(const ConstBlobLastUsedSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobLastUsedSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobLastUsedSignature::operator<(const ConstBlobLastUsedSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobLastUsedSignature_>& ConstBlobLastUsedSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobLastUsedSignature_> default_ptr = std::make_shared<_BlobLastUsedSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobLastUsedSignature_>& ConstBlobLastUsedSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobLastUsedSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobLastUsedSignature
void ConstBlobLastUsedSignature::BuildFromProto(const PbMessage& proto_bloblastusedsignature) {
  data_ = ::std::make_shared<_BlobLastUsedSignature_>(dynamic_cast<const ::oneflow::BlobLastUsedSignature&>(proto_bloblastusedsignature));
}

BlobLastUsedSignature::BlobLastUsedSignature(const ::std::shared_ptr<_BlobLastUsedSignature_>& data)
  : ConstBlobLastUsedSignature(data) {}
BlobLastUsedSignature::BlobLastUsedSignature(const BlobLastUsedSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobLastUsedSignature> resize
BlobLastUsedSignature::BlobLastUsedSignature(BlobLastUsedSignature&&) noexcept = default; 
BlobLastUsedSignature::BlobLastUsedSignature(const ::oneflow::BlobLastUsedSignature& proto_bloblastusedsignature) {
  InitFromProto(proto_bloblastusedsignature);
}
BlobLastUsedSignature::BlobLastUsedSignature() = default;

BlobLastUsedSignature::~BlobLastUsedSignature() = default;

void BlobLastUsedSignature::InitFromProto(const PbMessage& proto_bloblastusedsignature) {
  BuildFromProto(proto_bloblastusedsignature);
}
  
void* BlobLastUsedSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2blob_last_used();
    default: return nullptr;
  }
}

bool BlobLastUsedSignature::operator==(const BlobLastUsedSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobLastUsedSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobLastUsedSignature::operator<(const BlobLastUsedSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobLastUsedSignature::Clear() {
  if (data_) { data_.reset(); }
}
void BlobLastUsedSignature::CopyFrom(const BlobLastUsedSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobLastUsedSignature& BlobLastUsedSignature::operator=(const BlobLastUsedSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2blob_last_used
void BlobLastUsedSignature::clear_bn_in_op2blob_last_used() {
  return __SharedPtr__()->clear_bn_in_op2blob_last_used();
}

const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ & BlobLastUsedSignature::bn_in_op2blob_last_used() const {
  return __SharedConst__()->bn_in_op2blob_last_used();
}

_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_* BlobLastUsedSignature::mutable_bn_in_op2blob_last_used() {
  return __SharedPtr__()->mutable_bn_in_op2blob_last_used();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> BlobLastUsedSignature::shared_mutable_bn_in_op2blob_last_used() {
  return mutable_bn_in_op2blob_last_used()->__SharedMutable__();
}

::std::shared_ptr<BlobLastUsedSignature> BlobLastUsedSignature::__SharedMutable__() {
  return ::std::make_shared<BlobLastUsedSignature>(__SharedPtr__());
}
ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::_BlobBackwardUsedSignature_() { Clear(); }
ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::_BlobBackwardUsedSignature_(const _BlobBackwardUsedSignature_& other) { CopyFrom(other); }
ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::_BlobBackwardUsedSignature_(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature) {
  InitFromProto(proto_blobbackwardusedsignature);
}
ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::_BlobBackwardUsedSignature_(_BlobBackwardUsedSignature_&& other) = default;
ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::~_BlobBackwardUsedSignature_() = default;

void ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::InitFromProto(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature) {
  Clear();
  // map field : bn_in_op2blob_backward_used
  if (!proto_blobbackwardusedsignature.bn_in_op2blob_backward_used().empty()) {
_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_&  mut_bn_in_op2blob_backward_used = *mutable_bn_in_op2blob_backward_used();
    for (const auto& pair : proto_blobbackwardusedsignature.bn_in_op2blob_backward_used()) {
      mut_bn_in_op2blob_backward_used[pair.first] = pair.second;
      }
  }
    
}

void ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::ToProto(::oneflow::BlobBackwardUsedSignature* proto_blobbackwardusedsignature) const {
  proto_blobbackwardusedsignature->Clear();
  // map field : bn_in_op2blob_backward_used
  if (!bn_in_op2blob_backward_used().empty()) {
    auto& mut_bn_in_op2blob_backward_used = *(proto_blobbackwardusedsignature->mutable_bn_in_op2blob_backward_used());
    for (const auto& pair : bn_in_op2blob_backward_used()) {
      mut_bn_in_op2blob_backward_used[pair.first] = pair.second;
    }
  }

}

::std::string ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::DebugString() const {
  ::oneflow::BlobBackwardUsedSignature proto_blobbackwardusedsignature;
  this->ToProto(&proto_blobbackwardusedsignature);
  return proto_blobbackwardusedsignature.DebugString();
}

void ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::Clear() {
  clear_bn_in_op2blob_backward_used();
}

void ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::CopyFrom(const _BlobBackwardUsedSignature_& other) {
  mutable_bn_in_op2blob_backward_used()->CopyFrom(other.bn_in_op2blob_backward_used());
}


::std::size_t ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::bn_in_op2blob_backward_used_size() const {
  if (!bn_in_op2blob_backward_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return default_static_value->size();
  }
  return bn_in_op2blob_backward_used_->size();
}
const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::bn_in_op2blob_backward_used() const {
  if (!bn_in_op2blob_backward_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2blob_backward_used_.get());
}

_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ * ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::mutable_bn_in_op2blob_backward_used() {
  if (!bn_in_op2blob_backward_used_) {
    bn_in_op2blob_backward_used_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
  }
  return bn_in_op2blob_backward_used_.get();
}

const bool& ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::bn_in_op2blob_backward_used(::std::string key) const {
  if (!bn_in_op2blob_backward_used_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
    return default_static_value->at(key);
  }
  return bn_in_op2blob_backward_used_->at(key);
}

void ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::clear_bn_in_op2blob_backward_used() {
  if (!bn_in_op2blob_backward_used_) {
    bn_in_op2blob_backward_used_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>();
  }
  return bn_in_op2blob_backward_used_->Clear();
}



int ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::compare(const _BlobBackwardUsedSignature_& other) {
  if (!(bn_in_op2blob_backward_used() == other.bn_in_op2blob_backward_used())) {
    return bn_in_op2blob_backward_used() < other.bn_in_op2blob_backward_used() ? -1 : 1;
  }
  return 0;
}

bool ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::operator==(const _BlobBackwardUsedSignature_& other) const {
  return true
    && bn_in_op2blob_backward_used() == other.bn_in_op2blob_backward_used()
  ;
}

std::size_t ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2blob_backward_used().__CalcHash__()
  ;
}

bool ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_::operator<(const _BlobBackwardUsedSignature_& other) const {
  return false
    || !(bn_in_op2blob_backward_used() == other.bn_in_op2blob_backward_used()) ? 
      bn_in_op2blob_backward_used() < other.bn_in_op2blob_backward_used() : false
  ;
}

using _BlobBackwardUsedSignature_ =  ConstBlobBackwardUsedSignature::_BlobBackwardUsedSignature_;
ConstBlobBackwardUsedSignature::ConstBlobBackwardUsedSignature(const ::std::shared_ptr<_BlobBackwardUsedSignature_>& data): data_(data) {}
ConstBlobBackwardUsedSignature::ConstBlobBackwardUsedSignature(): data_(::std::make_shared<_BlobBackwardUsedSignature_>()) {}
ConstBlobBackwardUsedSignature::ConstBlobBackwardUsedSignature(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature) {
  BuildFromProto(proto_blobbackwardusedsignature);
}
ConstBlobBackwardUsedSignature::ConstBlobBackwardUsedSignature(const ConstBlobBackwardUsedSignature&) = default;
ConstBlobBackwardUsedSignature::ConstBlobBackwardUsedSignature(ConstBlobBackwardUsedSignature&&) noexcept = default;
ConstBlobBackwardUsedSignature::~ConstBlobBackwardUsedSignature() = default;

void ConstBlobBackwardUsedSignature::ToProto(PbMessage* proto_blobbackwardusedsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BlobBackwardUsedSignature*>(proto_blobbackwardusedsignature));
}
  
::std::string ConstBlobBackwardUsedSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBlobBackwardUsedSignature::__Empty__() const {
  return !data_;
}

int ConstBlobBackwardUsedSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2blob_backward_used", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBlobBackwardUsedSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBlobBackwardUsedSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, bool>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBlobBackwardUsedSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2blob_backward_used();
    default: return nullptr;
  }
}

// map field bn_in_op2blob_backward_used
::std::size_t ConstBlobBackwardUsedSignature::bn_in_op2blob_backward_used_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_backward_used_size();
}

const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& ConstBlobBackwardUsedSignature::bn_in_op2blob_backward_used() const {
  return __SharedPtrOrDefault__()->bn_in_op2blob_backward_used();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> ConstBlobBackwardUsedSignature::shared_const_bn_in_op2blob_backward_used() const {
  return bn_in_op2blob_backward_used().__SharedConst__();
}

::std::shared_ptr<ConstBlobBackwardUsedSignature> ConstBlobBackwardUsedSignature::__SharedConst__() const {
  return ::std::make_shared<ConstBlobBackwardUsedSignature>(data_);
}
int64_t ConstBlobBackwardUsedSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBlobBackwardUsedSignature::operator==(const ConstBlobBackwardUsedSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBlobBackwardUsedSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBlobBackwardUsedSignature::operator<(const ConstBlobBackwardUsedSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BlobBackwardUsedSignature_>& ConstBlobBackwardUsedSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BlobBackwardUsedSignature_> default_ptr = std::make_shared<_BlobBackwardUsedSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_BlobBackwardUsedSignature_>& ConstBlobBackwardUsedSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _BlobBackwardUsedSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBlobBackwardUsedSignature
void ConstBlobBackwardUsedSignature::BuildFromProto(const PbMessage& proto_blobbackwardusedsignature) {
  data_ = ::std::make_shared<_BlobBackwardUsedSignature_>(dynamic_cast<const ::oneflow::BlobBackwardUsedSignature&>(proto_blobbackwardusedsignature));
}

BlobBackwardUsedSignature::BlobBackwardUsedSignature(const ::std::shared_ptr<_BlobBackwardUsedSignature_>& data)
  : ConstBlobBackwardUsedSignature(data) {}
BlobBackwardUsedSignature::BlobBackwardUsedSignature(const BlobBackwardUsedSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BlobBackwardUsedSignature> resize
BlobBackwardUsedSignature::BlobBackwardUsedSignature(BlobBackwardUsedSignature&&) noexcept = default; 
BlobBackwardUsedSignature::BlobBackwardUsedSignature(const ::oneflow::BlobBackwardUsedSignature& proto_blobbackwardusedsignature) {
  InitFromProto(proto_blobbackwardusedsignature);
}
BlobBackwardUsedSignature::BlobBackwardUsedSignature() = default;

BlobBackwardUsedSignature::~BlobBackwardUsedSignature() = default;

void BlobBackwardUsedSignature::InitFromProto(const PbMessage& proto_blobbackwardusedsignature) {
  BuildFromProto(proto_blobbackwardusedsignature);
}
  
void* BlobBackwardUsedSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2blob_backward_used();
    default: return nullptr;
  }
}

bool BlobBackwardUsedSignature::operator==(const BlobBackwardUsedSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BlobBackwardUsedSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BlobBackwardUsedSignature::operator<(const BlobBackwardUsedSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BlobBackwardUsedSignature::Clear() {
  if (data_) { data_.reset(); }
}
void BlobBackwardUsedSignature::CopyFrom(const BlobBackwardUsedSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BlobBackwardUsedSignature& BlobBackwardUsedSignature::operator=(const BlobBackwardUsedSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2blob_backward_used
void BlobBackwardUsedSignature::clear_bn_in_op2blob_backward_used() {
  return __SharedPtr__()->clear_bn_in_op2blob_backward_used();
}

const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_ & BlobBackwardUsedSignature::bn_in_op2blob_backward_used() const {
  return __SharedConst__()->bn_in_op2blob_backward_used();
}

_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_* BlobBackwardUsedSignature::mutable_bn_in_op2blob_backward_used() {
  return __SharedPtr__()->mutable_bn_in_op2blob_backward_used();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> BlobBackwardUsedSignature::shared_mutable_bn_in_op2blob_backward_used() {
  return mutable_bn_in_op2blob_backward_used()->__SharedMutable__();
}

::std::shared_ptr<BlobBackwardUsedSignature> BlobBackwardUsedSignature::__SharedMutable__() {
  return ::std::make_shared<BlobBackwardUsedSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data): ::oneflow::cfg::_MapField_<::std::string, bool>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_() = default;
Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::~Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<bool>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const bool& Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>(__SharedPtr__());
}


_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data): Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_(data) {}
_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_() = default;
_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::~_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_() = default;

void _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) {
  ::oneflow::cfg::_MapField_<::std::string, bool>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) {
  ::oneflow::cfg::_MapField_<::std::string, bool>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::operator==(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<bool>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::operator<(const _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_> _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_>(__SharedPtr__());
}

void _CFG_ONEFLOW_CORE_JOB_BLOB_LIFETIME_SIGNATURE_CFG_H__MapField___std__string_bool_::Set(const ::std::string& key, const bool& value) {
  (*this)[key] = value;
}

}
} // namespace oneflow
