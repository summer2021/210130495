// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/job/parallel_conf_signature.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/map.h>  // IWYU pragma: export
#include <google/protobuf/map_entry.h>
#include <google/protobuf/map_field_inl.h>
#include <google/protobuf/unknown_field_set.h>
#include "oneflow/core/job/placement.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[2]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto;
namespace oneflow {
class ParallelConfSignature;
class ParallelConfSignatureDefaultTypeInternal;
extern ParallelConfSignatureDefaultTypeInternal _ParallelConfSignature_default_instance_;
class ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse;
class ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUseDefaultTypeInternal;
extern ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUseDefaultTypeInternal _ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse_default_instance_;
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> ::oneflow::ParallelConfSignature* Arena::CreateMaybeMessage<::oneflow::ParallelConfSignature>(Arena*);
template<> ::oneflow::ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse* Arena::CreateMaybeMessage<::oneflow::ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace oneflow {

// ===================================================================

class ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse : public ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse, 
    std::string, ::oneflow::ParallelConf,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > {
public:
  typedef ::PROTOBUF_NAMESPACE_ID::internal::MapEntry<ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse, 
    std::string, ::oneflow::ParallelConf,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
    0 > SuperType;
  ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse();
  ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  void MergeFrom(const ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse& other);
  static const ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse* internal_default_instance() { return reinterpret_cast<const ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse*>(&_ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse_default_instance_); }
  static bool ValidateKey(std::string* s) {
#ifndef NDEBUG
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
       s->data(), s->size(), ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::PARSE, "oneflow.ParallelConfSignature.BnInOp2parallelConfEntry.key");
#endif
    return true;
 }
  static bool ValidateValue(void*) { return true; }
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& other) final;
  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto.file_level_metadata[0];
  }

  public:
};

// -------------------------------------------------------------------

class ParallelConfSignature :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.ParallelConfSignature) */ {
 public:
  ParallelConfSignature();
  virtual ~ParallelConfSignature();

  ParallelConfSignature(const ParallelConfSignature& from);
  ParallelConfSignature(ParallelConfSignature&& from) noexcept
    : ParallelConfSignature() {
    *this = ::std::move(from);
  }

  inline ParallelConfSignature& operator=(const ParallelConfSignature& from) {
    CopyFrom(from);
    return *this;
  }
  inline ParallelConfSignature& operator=(ParallelConfSignature&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const ParallelConfSignature& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const ParallelConfSignature* internal_default_instance() {
    return reinterpret_cast<const ParallelConfSignature*>(
               &_ParallelConfSignature_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    1;

  friend void swap(ParallelConfSignature& a, ParallelConfSignature& b) {
    a.Swap(&b);
  }
  inline void Swap(ParallelConfSignature* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline ParallelConfSignature* New() const final {
    return CreateMaybeMessage<ParallelConfSignature>(nullptr);
  }

  ParallelConfSignature* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<ParallelConfSignature>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const ParallelConfSignature& from);
  void MergeFrom(const ParallelConfSignature& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(ParallelConfSignature* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.ParallelConfSignature";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto);
    return ::descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------


  // accessors -------------------------------------------------------

  enum : int {
    kBnInOp2ParallelConfFieldNumber = 2,
    kOpParallelConfFieldNumber = 1,
  };
  // map<string, .oneflow.ParallelConf> bn_in_op2parallel_conf = 2;
  int bn_in_op2parallel_conf_size() const;
  void clear_bn_in_op2parallel_conf();
  const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::ParallelConf >&
      bn_in_op2parallel_conf() const;
  ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::ParallelConf >*
      mutable_bn_in_op2parallel_conf();

  // optional .oneflow.ParallelConf op_parallel_conf = 1;
  bool has_op_parallel_conf() const;
  void clear_op_parallel_conf();
  const ::oneflow::ParallelConf& op_parallel_conf() const;
  ::oneflow::ParallelConf* release_op_parallel_conf();
  ::oneflow::ParallelConf* mutable_op_parallel_conf();
  void set_allocated_op_parallel_conf(::oneflow::ParallelConf* op_parallel_conf);

  // @@protoc_insertion_point(class_scope:oneflow.ParallelConfSignature)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::internal::MapField<
      ParallelConfSignature_BnInOp2parallelConfEntry_DoNotUse,
      std::string, ::oneflow::ParallelConf,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_STRING,
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::TYPE_MESSAGE,
      0 > bn_in_op2parallel_conf_;
  ::oneflow::ParallelConf* op_parallel_conf_;
  friend struct ::TableStruct_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// -------------------------------------------------------------------

// ParallelConfSignature

// optional .oneflow.ParallelConf op_parallel_conf = 1;
inline bool ParallelConfSignature::has_op_parallel_conf() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline const ::oneflow::ParallelConf& ParallelConfSignature::op_parallel_conf() const {
  const ::oneflow::ParallelConf* p = op_parallel_conf_;
  // @@protoc_insertion_point(field_get:oneflow.ParallelConfSignature.op_parallel_conf)
  return p != nullptr ? *p : *reinterpret_cast<const ::oneflow::ParallelConf*>(
      &::oneflow::_ParallelConf_default_instance_);
}
inline ::oneflow::ParallelConf* ParallelConfSignature::release_op_parallel_conf() {
  // @@protoc_insertion_point(field_release:oneflow.ParallelConfSignature.op_parallel_conf)
  _has_bits_[0] &= ~0x00000001u;
  ::oneflow::ParallelConf* temp = op_parallel_conf_;
  op_parallel_conf_ = nullptr;
  return temp;
}
inline ::oneflow::ParallelConf* ParallelConfSignature::mutable_op_parallel_conf() {
  _has_bits_[0] |= 0x00000001u;
  if (op_parallel_conf_ == nullptr) {
    auto* p = CreateMaybeMessage<::oneflow::ParallelConf>(GetArenaNoVirtual());
    op_parallel_conf_ = p;
  }
  // @@protoc_insertion_point(field_mutable:oneflow.ParallelConfSignature.op_parallel_conf)
  return op_parallel_conf_;
}
inline void ParallelConfSignature::set_allocated_op_parallel_conf(::oneflow::ParallelConf* op_parallel_conf) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(op_parallel_conf_);
  }
  if (op_parallel_conf) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      op_parallel_conf = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, op_parallel_conf, submessage_arena);
    }
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  op_parallel_conf_ = op_parallel_conf;
  // @@protoc_insertion_point(field_set_allocated:oneflow.ParallelConfSignature.op_parallel_conf)
}

// map<string, .oneflow.ParallelConf> bn_in_op2parallel_conf = 2;
inline int ParallelConfSignature::bn_in_op2parallel_conf_size() const {
  return bn_in_op2parallel_conf_.size();
}
inline const ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::ParallelConf >&
ParallelConfSignature::bn_in_op2parallel_conf() const {
  // @@protoc_insertion_point(field_map:oneflow.ParallelConfSignature.bn_in_op2parallel_conf)
  return bn_in_op2parallel_conf_.GetMap();
}
inline ::PROTOBUF_NAMESPACE_ID::Map< std::string, ::oneflow::ParallelConf >*
ParallelConfSignature::mutable_bn_in_op2parallel_conf() {
  // @@protoc_insertion_point(field_mutable_map:oneflow.ParallelConfSignature.bn_in_op2parallel_conf)
  return bn_in_op2parallel_conf_.MutableMap();
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace oneflow

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2fjob_2fparallel_5fconf_5fsignature_2eproto
