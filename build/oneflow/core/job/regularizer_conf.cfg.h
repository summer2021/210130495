#ifndef CFG_ONEFLOW_CORE_JOB_REGULARIZER_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_REGULARIZER_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class L1L2RegularizerConf;
class RegularizerConf;

namespace cfg {


class L1L2RegularizerConf;
class ConstL1L2RegularizerConf;

class RegularizerConf;
class ConstRegularizerConf;



class ConstL1L2RegularizerConf : public ::oneflow::cfg::Message {
 public:

  class _L1L2RegularizerConf_ {
   public:
    _L1L2RegularizerConf_();
    explicit _L1L2RegularizerConf_(const _L1L2RegularizerConf_& other);
    explicit _L1L2RegularizerConf_(_L1L2RegularizerConf_&& other);
    _L1L2RegularizerConf_(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf);
    ~_L1L2RegularizerConf_();

    void InitFromProto(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf);

    void ToProto(::oneflow::L1L2RegularizerConf* proto_l1l2regularizerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _L1L2RegularizerConf_& other);
  
      // optional field l1
     public:
    bool has_l1() const;
    const float& l1() const;
    void clear_l1();
    void set_l1(const float& value);
    float* mutable_l1();
   protected:
    bool has_l1_ = false;
    float l1_;
      
      // optional field l2
     public:
    bool has_l2() const;
    const float& l2() const;
    void clear_l2();
    void set_l2(const float& value);
    float* mutable_l2();
   protected:
    bool has_l2_ = false;
    float l2_;
           
   public:
    int compare(const _L1L2RegularizerConf_& other);

    bool operator==(const _L1L2RegularizerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _L1L2RegularizerConf_& other) const;
  };

  ConstL1L2RegularizerConf(const ::std::shared_ptr<_L1L2RegularizerConf_>& data);
  ConstL1L2RegularizerConf(const ConstL1L2RegularizerConf&);
  ConstL1L2RegularizerConf(ConstL1L2RegularizerConf&&) noexcept;
  ConstL1L2RegularizerConf();
  ConstL1L2RegularizerConf(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf);
  virtual ~ConstL1L2RegularizerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_l1l2regularizerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field l1
 public:
  bool has_l1() const;
  const float& l1() const;
  // used by pybind11 only
  // required or optional field l2
 public:
  bool has_l2() const;
  const float& l2() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstL1L2RegularizerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstL1L2RegularizerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstL1L2RegularizerConf& other) const;
 protected:
  const ::std::shared_ptr<_L1L2RegularizerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_L1L2RegularizerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstL1L2RegularizerConf
  void BuildFromProto(const PbMessage& proto_l1l2regularizerconf);
  
  ::std::shared_ptr<_L1L2RegularizerConf_> data_;
};

class L1L2RegularizerConf final : public ConstL1L2RegularizerConf {
 public:
  L1L2RegularizerConf(const ::std::shared_ptr<_L1L2RegularizerConf_>& data);
  L1L2RegularizerConf(const L1L2RegularizerConf& other);
  // enable nothrow for ::std::vector<L1L2RegularizerConf> resize 
  L1L2RegularizerConf(L1L2RegularizerConf&&) noexcept;
  L1L2RegularizerConf();
  explicit L1L2RegularizerConf(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf);

  ~L1L2RegularizerConf() override;

  void InitFromProto(const PbMessage& proto_l1l2regularizerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const L1L2RegularizerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const L1L2RegularizerConf& other) const;
  void Clear();
  void CopyFrom(const L1L2RegularizerConf& other);
  L1L2RegularizerConf& operator=(const L1L2RegularizerConf& other);

  // required or optional field l1
 public:
  void clear_l1();
  void set_l1(const float& value);
  float* mutable_l1();
  // required or optional field l2
 public:
  void clear_l2();
  void set_l2(const float& value);
  float* mutable_l2();

  ::std::shared_ptr<L1L2RegularizerConf> __SharedMutable__();
};


class ConstRegularizerConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kL1L2Conf = 1,
   };

  class _RegularizerConf_ {
   public:
    _RegularizerConf_();
    explicit _RegularizerConf_(const _RegularizerConf_& other);
    explicit _RegularizerConf_(_RegularizerConf_&& other);
    _RegularizerConf_(const ::oneflow::RegularizerConf& proto_regularizerconf);
    ~_RegularizerConf_();

    void InitFromProto(const ::oneflow::RegularizerConf& proto_regularizerconf);

    void ToProto(::oneflow::RegularizerConf* proto_regularizerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RegularizerConf_& other);
  
     // oneof field type: l1_l2_conf
   public:
    bool has_l1_l2_conf() const;
    void clear_l1_l2_conf();
    const ::oneflow::cfg::L1L2RegularizerConf& l1_l2_conf() const;
      ::oneflow::cfg::L1L2RegularizerConf* mutable_l1_l2_conf();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _RegularizerConf_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char l1_l2_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _RegularizerConf_& other);

    bool operator==(const _RegularizerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RegularizerConf_& other) const;
  };

  ConstRegularizerConf(const ::std::shared_ptr<_RegularizerConf_>& data);
  ConstRegularizerConf(const ConstRegularizerConf&);
  ConstRegularizerConf(ConstRegularizerConf&&) noexcept;
  ConstRegularizerConf();
  ConstRegularizerConf(const ::oneflow::RegularizerConf& proto_regularizerconf);
  virtual ~ConstRegularizerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_regularizerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: l1_l2_conf
 public:
  bool has_l1_l2_conf() const;
  const ::oneflow::cfg::L1L2RegularizerConf& l1_l2_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstL1L2RegularizerConf> shared_const_l1_l2_conf() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstRegularizerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRegularizerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRegularizerConf& other) const;
 protected:
  const ::std::shared_ptr<_RegularizerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RegularizerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRegularizerConf
  void BuildFromProto(const PbMessage& proto_regularizerconf);
  
  ::std::shared_ptr<_RegularizerConf_> data_;
};

class RegularizerConf final : public ConstRegularizerConf {
 public:
  RegularizerConf(const ::std::shared_ptr<_RegularizerConf_>& data);
  RegularizerConf(const RegularizerConf& other);
  // enable nothrow for ::std::vector<RegularizerConf> resize 
  RegularizerConf(RegularizerConf&&) noexcept;
  RegularizerConf();
  explicit RegularizerConf(const ::oneflow::RegularizerConf& proto_regularizerconf);

  ~RegularizerConf() override;

  void InitFromProto(const PbMessage& proto_regularizerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RegularizerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RegularizerConf& other) const;
  void Clear();
  void CopyFrom(const RegularizerConf& other);
  RegularizerConf& operator=(const RegularizerConf& other);

  void clear_l1_l2_conf();
  ::oneflow::cfg::L1L2RegularizerConf* mutable_l1_l2_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf> shared_mutable_l1_l2_conf();

  ::std::shared_ptr<RegularizerConf> __SharedMutable__();
};









} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstL1L2RegularizerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstL1L2RegularizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::L1L2RegularizerConf> {
  std::size_t operator()(const ::oneflow::cfg::L1L2RegularizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRegularizerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRegularizerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RegularizerConf> {
  std::size_t operator()(const ::oneflow::cfg::RegularizerConf& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_REGULARIZER_CONF_CFG_H_