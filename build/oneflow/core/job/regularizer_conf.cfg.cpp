#include "oneflow/core/job/regularizer_conf.cfg.h"
#include "oneflow/core/job/regularizer_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstL1L2RegularizerConf::_L1L2RegularizerConf_::_L1L2RegularizerConf_() { Clear(); }
ConstL1L2RegularizerConf::_L1L2RegularizerConf_::_L1L2RegularizerConf_(const _L1L2RegularizerConf_& other) { CopyFrom(other); }
ConstL1L2RegularizerConf::_L1L2RegularizerConf_::_L1L2RegularizerConf_(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf) {
  InitFromProto(proto_l1l2regularizerconf);
}
ConstL1L2RegularizerConf::_L1L2RegularizerConf_::_L1L2RegularizerConf_(_L1L2RegularizerConf_&& other) = default;
ConstL1L2RegularizerConf::_L1L2RegularizerConf_::~_L1L2RegularizerConf_() = default;

void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::InitFromProto(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf) {
  Clear();
  // required_or_optional field: l1
  if (proto_l1l2regularizerconf.has_l1()) {
    set_l1(proto_l1l2regularizerconf.l1());
  }
  // required_or_optional field: l2
  if (proto_l1l2regularizerconf.has_l2()) {
    set_l2(proto_l1l2regularizerconf.l2());
  }
    
}

void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::ToProto(::oneflow::L1L2RegularizerConf* proto_l1l2regularizerconf) const {
  proto_l1l2regularizerconf->Clear();
  // required_or_optional field: l1
  if (this->has_l1()) {
    proto_l1l2regularizerconf->set_l1(l1());
    }
  // required_or_optional field: l2
  if (this->has_l2()) {
    proto_l1l2regularizerconf->set_l2(l2());
    }

}

::std::string ConstL1L2RegularizerConf::_L1L2RegularizerConf_::DebugString() const {
  ::oneflow::L1L2RegularizerConf proto_l1l2regularizerconf;
  this->ToProto(&proto_l1l2regularizerconf);
  return proto_l1l2regularizerconf.DebugString();
}

void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::Clear() {
  clear_l1();
  clear_l2();
}

void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::CopyFrom(const _L1L2RegularizerConf_& other) {
  if (other.has_l1()) {
    set_l1(other.l1());
  } else {
    clear_l1();
  }
  if (other.has_l2()) {
    set_l2(other.l2());
  } else {
    clear_l2();
  }
}


// optional field l1
bool ConstL1L2RegularizerConf::_L1L2RegularizerConf_::has_l1() const {
  return has_l1_;
}
const float& ConstL1L2RegularizerConf::_L1L2RegularizerConf_::l1() const {
  if (has_l1_) { return l1_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::clear_l1() {
  has_l1_ = false;
}
void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::set_l1(const float& value) {
  l1_ = value;
  has_l1_ = true;
}
float* ConstL1L2RegularizerConf::_L1L2RegularizerConf_::mutable_l1() {
  has_l1_ = true;
  return &l1_;
}

// optional field l2
bool ConstL1L2RegularizerConf::_L1L2RegularizerConf_::has_l2() const {
  return has_l2_;
}
const float& ConstL1L2RegularizerConf::_L1L2RegularizerConf_::l2() const {
  if (has_l2_) { return l2_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::clear_l2() {
  has_l2_ = false;
}
void ConstL1L2RegularizerConf::_L1L2RegularizerConf_::set_l2(const float& value) {
  l2_ = value;
  has_l2_ = true;
}
float* ConstL1L2RegularizerConf::_L1L2RegularizerConf_::mutable_l2() {
  has_l2_ = true;
  return &l2_;
}


int ConstL1L2RegularizerConf::_L1L2RegularizerConf_::compare(const _L1L2RegularizerConf_& other) {
  if (!(has_l1() == other.has_l1())) {
    return has_l1() < other.has_l1() ? -1 : 1;
  } else if (!(l1() == other.l1())) {
    return l1() < other.l1() ? -1 : 1;
  }
  if (!(has_l2() == other.has_l2())) {
    return has_l2() < other.has_l2() ? -1 : 1;
  } else if (!(l2() == other.l2())) {
    return l2() < other.l2() ? -1 : 1;
  }
  return 0;
}

bool ConstL1L2RegularizerConf::_L1L2RegularizerConf_::operator==(const _L1L2RegularizerConf_& other) const {
  return true
    && has_l1() == other.has_l1() 
    && l1() == other.l1()
    && has_l2() == other.has_l2() 
    && l2() == other.l2()
  ;
}

std::size_t ConstL1L2RegularizerConf::_L1L2RegularizerConf_::__CalcHash__() const {
  return 0
    ^ (has_l1() ? std::hash<float>()(l1()) : 0)
    ^ (has_l2() ? std::hash<float>()(l2()) : 0)
  ;
}

bool ConstL1L2RegularizerConf::_L1L2RegularizerConf_::operator<(const _L1L2RegularizerConf_& other) const {
  return false
    || !(has_l1() == other.has_l1()) ? 
      has_l1() < other.has_l1() : false
    || !(l1() == other.l1()) ? 
      l1() < other.l1() : false
    || !(has_l2() == other.has_l2()) ? 
      has_l2() < other.has_l2() : false
    || !(l2() == other.l2()) ? 
      l2() < other.l2() : false
  ;
}

using _L1L2RegularizerConf_ =  ConstL1L2RegularizerConf::_L1L2RegularizerConf_;
ConstL1L2RegularizerConf::ConstL1L2RegularizerConf(const ::std::shared_ptr<_L1L2RegularizerConf_>& data): data_(data) {}
ConstL1L2RegularizerConf::ConstL1L2RegularizerConf(): data_(::std::make_shared<_L1L2RegularizerConf_>()) {}
ConstL1L2RegularizerConf::ConstL1L2RegularizerConf(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf) {
  BuildFromProto(proto_l1l2regularizerconf);
}
ConstL1L2RegularizerConf::ConstL1L2RegularizerConf(const ConstL1L2RegularizerConf&) = default;
ConstL1L2RegularizerConf::ConstL1L2RegularizerConf(ConstL1L2RegularizerConf&&) noexcept = default;
ConstL1L2RegularizerConf::~ConstL1L2RegularizerConf() = default;

void ConstL1L2RegularizerConf::ToProto(PbMessage* proto_l1l2regularizerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::L1L2RegularizerConf*>(proto_l1l2regularizerconf));
}
  
::std::string ConstL1L2RegularizerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstL1L2RegularizerConf::__Empty__() const {
  return !data_;
}

int ConstL1L2RegularizerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"l1", 1},
    {"l2", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstL1L2RegularizerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstL1L2RegularizerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstL1L2RegularizerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &l1();
    case 2: return &l2();
    default: return nullptr;
  }
}

// required or optional field l1
bool ConstL1L2RegularizerConf::has_l1() const {
  return __SharedPtrOrDefault__()->has_l1();
}
const float& ConstL1L2RegularizerConf::l1() const {
  return __SharedPtrOrDefault__()->l1();
}
// used by pybind11 only
// required or optional field l2
bool ConstL1L2RegularizerConf::has_l2() const {
  return __SharedPtrOrDefault__()->has_l2();
}
const float& ConstL1L2RegularizerConf::l2() const {
  return __SharedPtrOrDefault__()->l2();
}
// used by pybind11 only

::std::shared_ptr<ConstL1L2RegularizerConf> ConstL1L2RegularizerConf::__SharedConst__() const {
  return ::std::make_shared<ConstL1L2RegularizerConf>(data_);
}
int64_t ConstL1L2RegularizerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstL1L2RegularizerConf::operator==(const ConstL1L2RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstL1L2RegularizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstL1L2RegularizerConf::operator<(const ConstL1L2RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_L1L2RegularizerConf_>& ConstL1L2RegularizerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_L1L2RegularizerConf_> default_ptr = std::make_shared<_L1L2RegularizerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_L1L2RegularizerConf_>& ConstL1L2RegularizerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _L1L2RegularizerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstL1L2RegularizerConf
void ConstL1L2RegularizerConf::BuildFromProto(const PbMessage& proto_l1l2regularizerconf) {
  data_ = ::std::make_shared<_L1L2RegularizerConf_>(dynamic_cast<const ::oneflow::L1L2RegularizerConf&>(proto_l1l2regularizerconf));
}

L1L2RegularizerConf::L1L2RegularizerConf(const ::std::shared_ptr<_L1L2RegularizerConf_>& data)
  : ConstL1L2RegularizerConf(data) {}
L1L2RegularizerConf::L1L2RegularizerConf(const L1L2RegularizerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<L1L2RegularizerConf> resize
L1L2RegularizerConf::L1L2RegularizerConf(L1L2RegularizerConf&&) noexcept = default; 
L1L2RegularizerConf::L1L2RegularizerConf(const ::oneflow::L1L2RegularizerConf& proto_l1l2regularizerconf) {
  InitFromProto(proto_l1l2regularizerconf);
}
L1L2RegularizerConf::L1L2RegularizerConf() = default;

L1L2RegularizerConf::~L1L2RegularizerConf() = default;

void L1L2RegularizerConf::InitFromProto(const PbMessage& proto_l1l2regularizerconf) {
  BuildFromProto(proto_l1l2regularizerconf);
}
  
void* L1L2RegularizerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_l1();
    case 2: return mutable_l2();
    default: return nullptr;
  }
}

bool L1L2RegularizerConf::operator==(const L1L2RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t L1L2RegularizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool L1L2RegularizerConf::operator<(const L1L2RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void L1L2RegularizerConf::Clear() {
  if (data_) { data_.reset(); }
}
void L1L2RegularizerConf::CopyFrom(const L1L2RegularizerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
L1L2RegularizerConf& L1L2RegularizerConf::operator=(const L1L2RegularizerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field l1
void L1L2RegularizerConf::clear_l1() {
  return __SharedPtr__()->clear_l1();
}
void L1L2RegularizerConf::set_l1(const float& value) {
  return __SharedPtr__()->set_l1(value);
}
float* L1L2RegularizerConf::mutable_l1() {
  return  __SharedPtr__()->mutable_l1();
}
// required or optional field l2
void L1L2RegularizerConf::clear_l2() {
  return __SharedPtr__()->clear_l2();
}
void L1L2RegularizerConf::set_l2(const float& value) {
  return __SharedPtr__()->set_l2(value);
}
float* L1L2RegularizerConf::mutable_l2() {
  return  __SharedPtr__()->mutable_l2();
}

::std::shared_ptr<L1L2RegularizerConf> L1L2RegularizerConf::__SharedMutable__() {
  return ::std::make_shared<L1L2RegularizerConf>(__SharedPtr__());
}
ConstRegularizerConf::_RegularizerConf_::_RegularizerConf_() { Clear(); }
ConstRegularizerConf::_RegularizerConf_::_RegularizerConf_(const _RegularizerConf_& other) { CopyFrom(other); }
ConstRegularizerConf::_RegularizerConf_::_RegularizerConf_(const ::oneflow::RegularizerConf& proto_regularizerconf) {
  InitFromProto(proto_regularizerconf);
}
ConstRegularizerConf::_RegularizerConf_::_RegularizerConf_(_RegularizerConf_&& other) = default;
ConstRegularizerConf::_RegularizerConf_::~_RegularizerConf_() = default;

void ConstRegularizerConf::_RegularizerConf_::InitFromProto(const ::oneflow::RegularizerConf& proto_regularizerconf) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_regularizerconf.type_case()));
  switch (type_case) {
    case kL1L2Conf: {
      *mutable_l1_l2_conf() = ::oneflow::cfg::L1L2RegularizerConf(proto_regularizerconf.l1_l2_conf());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstRegularizerConf::_RegularizerConf_::ToProto(::oneflow::RegularizerConf* proto_regularizerconf) const {
  proto_regularizerconf->Clear();

  // oneof field: type
  ::oneflow::RegularizerConf::TypeCase type_case = ::oneflow::RegularizerConf::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::RegularizerConf::kL1L2Conf: {
      ::oneflow::L1L2RegularizerConf of_proto_l1_l2_conf;
      l1_l2_conf().ToProto(&of_proto_l1_l2_conf);
      proto_regularizerconf->mutable_l1_l2_conf()->CopyFrom(of_proto_l1_l2_conf);
      break;
    }
    case ::oneflow::RegularizerConf::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstRegularizerConf::_RegularizerConf_::DebugString() const {
  ::oneflow::RegularizerConf proto_regularizerconf;
  this->ToProto(&proto_regularizerconf);
  return proto_regularizerconf.DebugString();
}

void ConstRegularizerConf::_RegularizerConf_::Clear() {
  clear_type();
}

void ConstRegularizerConf::_RegularizerConf_::CopyFrom(const _RegularizerConf_& other) {
  type_copy_from(other);
}


// oneof field type: l1_l2_conf
bool ConstRegularizerConf::_RegularizerConf_::has_l1_l2_conf() const {
  return type_case() == kL1L2Conf;
}
void ConstRegularizerConf::_RegularizerConf_::clear_l1_l2_conf() {
  if (has_l1_l2_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.l1_l2_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::L1L2RegularizerConf& ConstRegularizerConf::_RegularizerConf_::l1_l2_conf() const {
  if (has_l1_l2_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>*>(&(type_.l1_l2_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf> default_static_value = ::std::make_shared<::oneflow::cfg::L1L2RegularizerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::L1L2RegularizerConf* ConstRegularizerConf::_RegularizerConf_::mutable_l1_l2_conf() {
  if(!has_l1_l2_conf()) {
    clear_type();
    new (&(type_.l1_l2_conf_)) ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>(new ::oneflow::cfg::L1L2RegularizerConf());
  }
  type_case_ = kL1L2Conf;
  ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>*>(&(type_.l1_l2_conf_));
  return  (*ptr).get();
}
ConstRegularizerConf::TypeCase ConstRegularizerConf::_RegularizerConf_::type_case() const {
  return type_case_;
}
bool ConstRegularizerConf::_RegularizerConf_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstRegularizerConf::_RegularizerConf_::clear_type() {
  switch (type_case()) {
    case kL1L2Conf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.l1_l2_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstRegularizerConf::_RegularizerConf_::type_copy_from(const _RegularizerConf_& other) {
  switch (other.type_case()) {
    case kL1L2Conf: {
      mutable_l1_l2_conf()->CopyFrom(other.l1_l2_conf());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstRegularizerConf::_RegularizerConf_::compare(const _RegularizerConf_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kL1L2Conf: {
      if (!(l1_l2_conf() == other.l1_l2_conf())) {
        return l1_l2_conf() < other.l1_l2_conf() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstRegularizerConf::_RegularizerConf_::operator==(const _RegularizerConf_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kL1L2Conf ? 
      l1_l2_conf() == other.l1_l2_conf() : true)
  ;
}

std::size_t ConstRegularizerConf::_RegularizerConf_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_l1_l2_conf() ? std::hash<::oneflow::cfg::L1L2RegularizerConf>()(l1_l2_conf()) : 0)
  ;
}

bool ConstRegularizerConf::_RegularizerConf_::operator<(const _RegularizerConf_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kL1L2Conf) && 
      !(l1_l2_conf() == other.l1_l2_conf())) ? 
        l1_l2_conf() < other.l1_l2_conf() : false
  ;
}

using _RegularizerConf_ =  ConstRegularizerConf::_RegularizerConf_;
ConstRegularizerConf::ConstRegularizerConf(const ::std::shared_ptr<_RegularizerConf_>& data): data_(data) {}
ConstRegularizerConf::ConstRegularizerConf(): data_(::std::make_shared<_RegularizerConf_>()) {}
ConstRegularizerConf::ConstRegularizerConf(const ::oneflow::RegularizerConf& proto_regularizerconf) {
  BuildFromProto(proto_regularizerconf);
}
ConstRegularizerConf::ConstRegularizerConf(const ConstRegularizerConf&) = default;
ConstRegularizerConf::ConstRegularizerConf(ConstRegularizerConf&&) noexcept = default;
ConstRegularizerConf::~ConstRegularizerConf() = default;

void ConstRegularizerConf::ToProto(PbMessage* proto_regularizerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RegularizerConf*>(proto_regularizerconf));
}
  
::std::string ConstRegularizerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRegularizerConf::__Empty__() const {
  return !data_;
}

int ConstRegularizerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"l1_l2_conf", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRegularizerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRegularizerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::L1L2RegularizerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstL1L2RegularizerConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRegularizerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &l1_l2_conf();
    default: return nullptr;
  }
}

 // oneof field type: l1_l2_conf
bool ConstRegularizerConf::has_l1_l2_conf() const {
  return __SharedPtrOrDefault__()->has_l1_l2_conf();
}
const ::oneflow::cfg::L1L2RegularizerConf& ConstRegularizerConf::l1_l2_conf() const {
  return __SharedPtrOrDefault__()->l1_l2_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstL1L2RegularizerConf> ConstRegularizerConf::shared_const_l1_l2_conf() const {
  return l1_l2_conf().__SharedConst__();
}
ConstRegularizerConf::TypeCase ConstRegularizerConf::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstRegularizerConf::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstRegularizerConf> ConstRegularizerConf::__SharedConst__() const {
  return ::std::make_shared<ConstRegularizerConf>(data_);
}
int64_t ConstRegularizerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRegularizerConf::operator==(const ConstRegularizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRegularizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRegularizerConf::operator<(const ConstRegularizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RegularizerConf_>& ConstRegularizerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RegularizerConf_> default_ptr = std::make_shared<_RegularizerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RegularizerConf_>& ConstRegularizerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RegularizerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRegularizerConf
void ConstRegularizerConf::BuildFromProto(const PbMessage& proto_regularizerconf) {
  data_ = ::std::make_shared<_RegularizerConf_>(dynamic_cast<const ::oneflow::RegularizerConf&>(proto_regularizerconf));
}

RegularizerConf::RegularizerConf(const ::std::shared_ptr<_RegularizerConf_>& data)
  : ConstRegularizerConf(data) {}
RegularizerConf::RegularizerConf(const RegularizerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RegularizerConf> resize
RegularizerConf::RegularizerConf(RegularizerConf&&) noexcept = default; 
RegularizerConf::RegularizerConf(const ::oneflow::RegularizerConf& proto_regularizerconf) {
  InitFromProto(proto_regularizerconf);
}
RegularizerConf::RegularizerConf() = default;

RegularizerConf::~RegularizerConf() = default;

void RegularizerConf::InitFromProto(const PbMessage& proto_regularizerconf) {
  BuildFromProto(proto_regularizerconf);
}
  
void* RegularizerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_l1_l2_conf();
    default: return nullptr;
  }
}

bool RegularizerConf::operator==(const RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RegularizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RegularizerConf::operator<(const RegularizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RegularizerConf::Clear() {
  if (data_) { data_.reset(); }
}
void RegularizerConf::CopyFrom(const RegularizerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RegularizerConf& RegularizerConf::operator=(const RegularizerConf& other) {
  CopyFrom(other);
  return *this;
}

void RegularizerConf::clear_l1_l2_conf() {
  return __SharedPtr__()->clear_l1_l2_conf();
}
::oneflow::cfg::L1L2RegularizerConf* RegularizerConf::mutable_l1_l2_conf() {
  return __SharedPtr__()->mutable_l1_l2_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::L1L2RegularizerConf> RegularizerConf::shared_mutable_l1_l2_conf() {
  return mutable_l1_l2_conf()->__SharedMutable__();
}

::std::shared_ptr<RegularizerConf> RegularizerConf::__SharedMutable__() {
  return ::std::make_shared<RegularizerConf>(__SharedPtr__());
}


}
} // namespace oneflow
