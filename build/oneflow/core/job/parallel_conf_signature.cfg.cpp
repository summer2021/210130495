#include "oneflow/core/job/parallel_conf_signature.cfg.h"
#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/job/parallel_conf_signature.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstParallelConfSignature::_ParallelConfSignature_::_ParallelConfSignature_() { Clear(); }
ConstParallelConfSignature::_ParallelConfSignature_::_ParallelConfSignature_(const _ParallelConfSignature_& other) { CopyFrom(other); }
ConstParallelConfSignature::_ParallelConfSignature_::_ParallelConfSignature_(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature) {
  InitFromProto(proto_parallelconfsignature);
}
ConstParallelConfSignature::_ParallelConfSignature_::_ParallelConfSignature_(_ParallelConfSignature_&& other) = default;
ConstParallelConfSignature::_ParallelConfSignature_::~_ParallelConfSignature_() = default;

void ConstParallelConfSignature::_ParallelConfSignature_::InitFromProto(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature) {
  Clear();
  // required_or_optional field: op_parallel_conf
  if (proto_parallelconfsignature.has_op_parallel_conf()) {
  *mutable_op_parallel_conf() = ::oneflow::cfg::ParallelConf(proto_parallelconfsignature.op_parallel_conf());      
  }
  // map field : bn_in_op2parallel_conf
  if (!proto_parallelconfsignature.bn_in_op2parallel_conf().empty()) {
_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_&  mut_bn_in_op2parallel_conf = *mutable_bn_in_op2parallel_conf();
    for (const auto& pair : proto_parallelconfsignature.bn_in_op2parallel_conf()) {
      mut_bn_in_op2parallel_conf[pair.first] = ::oneflow::cfg::ParallelConf(pair.second);
    }
  }
    
}

void ConstParallelConfSignature::_ParallelConfSignature_::ToProto(::oneflow::ParallelConfSignature* proto_parallelconfsignature) const {
  proto_parallelconfsignature->Clear();
  // required_or_optional field: op_parallel_conf
  if (this->has_op_parallel_conf()) {
    ::oneflow::ParallelConf proto_op_parallel_conf;
    op_parallel_conf().ToProto(&proto_op_parallel_conf);
    proto_parallelconfsignature->mutable_op_parallel_conf()->CopyFrom(proto_op_parallel_conf);
    }
  // map field : bn_in_op2parallel_conf
  if (!bn_in_op2parallel_conf().empty()) {
    auto& mut_bn_in_op2parallel_conf = *(proto_parallelconfsignature->mutable_bn_in_op2parallel_conf());
    for (const auto& pair : bn_in_op2parallel_conf()) {
      ::oneflow::ParallelConf proto_bn_in_op2parallel_conf_value;
      pair.second.ToProto(&proto_bn_in_op2parallel_conf_value);
      mut_bn_in_op2parallel_conf[pair.first] = proto_bn_in_op2parallel_conf_value;
    }
  }

}

::std::string ConstParallelConfSignature::_ParallelConfSignature_::DebugString() const {
  ::oneflow::ParallelConfSignature proto_parallelconfsignature;
  this->ToProto(&proto_parallelconfsignature);
  return proto_parallelconfsignature.DebugString();
}

void ConstParallelConfSignature::_ParallelConfSignature_::Clear() {
  clear_op_parallel_conf();
  clear_bn_in_op2parallel_conf();
}

void ConstParallelConfSignature::_ParallelConfSignature_::CopyFrom(const _ParallelConfSignature_& other) {
  if (other.has_op_parallel_conf()) {
    mutable_op_parallel_conf()->CopyFrom(other.op_parallel_conf());
  } else {
    clear_op_parallel_conf();
  }
  mutable_bn_in_op2parallel_conf()->CopyFrom(other.bn_in_op2parallel_conf());
}


// optional field op_parallel_conf
bool ConstParallelConfSignature::_ParallelConfSignature_::has_op_parallel_conf() const {
  return has_op_parallel_conf_;
}
const ::oneflow::cfg::ParallelConf& ConstParallelConfSignature::_ParallelConfSignature_::op_parallel_conf() const {
  if (!op_parallel_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelConf>();
    return *default_static_value;
  }
  return *(op_parallel_conf_.get());
}
void ConstParallelConfSignature::_ParallelConfSignature_::clear_op_parallel_conf() {
  if (op_parallel_conf_) {
    op_parallel_conf_->Clear();
  }
  has_op_parallel_conf_ = false;
}
::oneflow::cfg::ParallelConf* ConstParallelConfSignature::_ParallelConfSignature_::mutable_op_parallel_conf() {
  if (!op_parallel_conf_) {
    op_parallel_conf_ = ::std::make_shared<::oneflow::cfg::ParallelConf>();
  }
  has_op_parallel_conf_ = true;
  return op_parallel_conf_.get();
}

::std::size_t ConstParallelConfSignature::_ParallelConfSignature_::bn_in_op2parallel_conf_size() const {
  if (!bn_in_op2parallel_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>();
    return default_static_value->size();
  }
  return bn_in_op2parallel_conf_->size();
}
const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& ConstParallelConfSignature::_ParallelConfSignature_::bn_in_op2parallel_conf() const {
  if (!bn_in_op2parallel_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2parallel_conf_.get());
}

_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ * ConstParallelConfSignature::_ParallelConfSignature_::mutable_bn_in_op2parallel_conf() {
  if (!bn_in_op2parallel_conf_) {
    bn_in_op2parallel_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>();
  }
  return bn_in_op2parallel_conf_.get();
}

const ::oneflow::cfg::ParallelConf& ConstParallelConfSignature::_ParallelConfSignature_::bn_in_op2parallel_conf(::std::string key) const {
  if (!bn_in_op2parallel_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>();
    return default_static_value->at(key);
  }
  return bn_in_op2parallel_conf_->at(key);
}

void ConstParallelConfSignature::_ParallelConfSignature_::clear_bn_in_op2parallel_conf() {
  if (!bn_in_op2parallel_conf_) {
    bn_in_op2parallel_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>();
  }
  return bn_in_op2parallel_conf_->Clear();
}



int ConstParallelConfSignature::_ParallelConfSignature_::compare(const _ParallelConfSignature_& other) {
  if (!(has_op_parallel_conf() == other.has_op_parallel_conf())) {
    return has_op_parallel_conf() < other.has_op_parallel_conf() ? -1 : 1;
  } else if (!(op_parallel_conf() == other.op_parallel_conf())) {
    return op_parallel_conf() < other.op_parallel_conf() ? -1 : 1;
  }
  if (!(bn_in_op2parallel_conf() == other.bn_in_op2parallel_conf())) {
    return bn_in_op2parallel_conf() < other.bn_in_op2parallel_conf() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelConfSignature::_ParallelConfSignature_::operator==(const _ParallelConfSignature_& other) const {
  return true
    && has_op_parallel_conf() == other.has_op_parallel_conf() 
    && op_parallel_conf() == other.op_parallel_conf()
    && bn_in_op2parallel_conf() == other.bn_in_op2parallel_conf()
  ;
}

std::size_t ConstParallelConfSignature::_ParallelConfSignature_::__CalcHash__() const {
  return 0
    ^ (has_op_parallel_conf() ? std::hash<::oneflow::cfg::ParallelConf>()(op_parallel_conf()) : 0)
    ^ bn_in_op2parallel_conf().__CalcHash__()
  ;
}

bool ConstParallelConfSignature::_ParallelConfSignature_::operator<(const _ParallelConfSignature_& other) const {
  return false
    || !(has_op_parallel_conf() == other.has_op_parallel_conf()) ? 
      has_op_parallel_conf() < other.has_op_parallel_conf() : false
    || !(op_parallel_conf() == other.op_parallel_conf()) ? 
      op_parallel_conf() < other.op_parallel_conf() : false
    || !(bn_in_op2parallel_conf() == other.bn_in_op2parallel_conf()) ? 
      bn_in_op2parallel_conf() < other.bn_in_op2parallel_conf() : false
  ;
}

using _ParallelConfSignature_ =  ConstParallelConfSignature::_ParallelConfSignature_;
ConstParallelConfSignature::ConstParallelConfSignature(const ::std::shared_ptr<_ParallelConfSignature_>& data): data_(data) {}
ConstParallelConfSignature::ConstParallelConfSignature(): data_(::std::make_shared<_ParallelConfSignature_>()) {}
ConstParallelConfSignature::ConstParallelConfSignature(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature) {
  BuildFromProto(proto_parallelconfsignature);
}
ConstParallelConfSignature::ConstParallelConfSignature(const ConstParallelConfSignature&) = default;
ConstParallelConfSignature::ConstParallelConfSignature(ConstParallelConfSignature&&) noexcept = default;
ConstParallelConfSignature::~ConstParallelConfSignature() = default;

void ConstParallelConfSignature::ToProto(PbMessage* proto_parallelconfsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelConfSignature*>(proto_parallelconfsignature));
}
  
::std::string ConstParallelConfSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelConfSignature::__Empty__() const {
  return !data_;
}

int ConstParallelConfSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_parallel_conf", 1},
    {"bn_in_op2parallel_conf", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelConfSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelConfSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConf),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelConf>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelConfSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_parallel_conf();
    case 2: return &bn_in_op2parallel_conf();
    default: return nullptr;
  }
}

// required or optional field op_parallel_conf
bool ConstParallelConfSignature::has_op_parallel_conf() const {
  return __SharedPtrOrDefault__()->has_op_parallel_conf();
}
const ::oneflow::cfg::ParallelConf& ConstParallelConfSignature::op_parallel_conf() const {
  return __SharedPtrOrDefault__()->op_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConf> ConstParallelConfSignature::shared_const_op_parallel_conf() const {
  return op_parallel_conf().__SharedConst__();
}
// map field bn_in_op2parallel_conf
::std::size_t ConstParallelConfSignature::bn_in_op2parallel_conf_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_conf_size();
}

const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& ConstParallelConfSignature::bn_in_op2parallel_conf() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_conf();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> ConstParallelConfSignature::shared_const_bn_in_op2parallel_conf() const {
  return bn_in_op2parallel_conf().__SharedConst__();
}

::std::shared_ptr<ConstParallelConfSignature> ConstParallelConfSignature::__SharedConst__() const {
  return ::std::make_shared<ConstParallelConfSignature>(data_);
}
int64_t ConstParallelConfSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelConfSignature::operator==(const ConstParallelConfSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelConfSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelConfSignature::operator<(const ConstParallelConfSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelConfSignature_>& ConstParallelConfSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelConfSignature_> default_ptr = std::make_shared<_ParallelConfSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelConfSignature_>& ConstParallelConfSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelConfSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelConfSignature
void ConstParallelConfSignature::BuildFromProto(const PbMessage& proto_parallelconfsignature) {
  data_ = ::std::make_shared<_ParallelConfSignature_>(dynamic_cast<const ::oneflow::ParallelConfSignature&>(proto_parallelconfsignature));
}

ParallelConfSignature::ParallelConfSignature(const ::std::shared_ptr<_ParallelConfSignature_>& data)
  : ConstParallelConfSignature(data) {}
ParallelConfSignature::ParallelConfSignature(const ParallelConfSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelConfSignature> resize
ParallelConfSignature::ParallelConfSignature(ParallelConfSignature&&) noexcept = default; 
ParallelConfSignature::ParallelConfSignature(const ::oneflow::ParallelConfSignature& proto_parallelconfsignature) {
  InitFromProto(proto_parallelconfsignature);
}
ParallelConfSignature::ParallelConfSignature() = default;

ParallelConfSignature::~ParallelConfSignature() = default;

void ParallelConfSignature::InitFromProto(const PbMessage& proto_parallelconfsignature) {
  BuildFromProto(proto_parallelconfsignature);
}
  
void* ParallelConfSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_parallel_conf();
    case 2: return mutable_bn_in_op2parallel_conf();
    default: return nullptr;
  }
}

bool ParallelConfSignature::operator==(const ParallelConfSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelConfSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelConfSignature::operator<(const ParallelConfSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelConfSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelConfSignature::CopyFrom(const ParallelConfSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelConfSignature& ParallelConfSignature::operator=(const ParallelConfSignature& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_parallel_conf
void ParallelConfSignature::clear_op_parallel_conf() {
  return __SharedPtr__()->clear_op_parallel_conf();
}
::oneflow::cfg::ParallelConf* ParallelConfSignature::mutable_op_parallel_conf() {
  return __SharedPtr__()->mutable_op_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConf> ParallelConfSignature::shared_mutable_op_parallel_conf() {
  return mutable_op_parallel_conf()->__SharedMutable__();
}
// repeated field bn_in_op2parallel_conf
void ParallelConfSignature::clear_bn_in_op2parallel_conf() {
  return __SharedPtr__()->clear_bn_in_op2parallel_conf();
}

const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_ & ParallelConfSignature::bn_in_op2parallel_conf() const {
  return __SharedConst__()->bn_in_op2parallel_conf();
}

_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_* ParallelConfSignature::mutable_bn_in_op2parallel_conf() {
  return __SharedPtr__()->mutable_bn_in_op2parallel_conf();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> ParallelConfSignature::shared_mutable_bn_in_op2parallel_conf() {
  return mutable_bn_in_op2parallel_conf()->__SharedMutable__();
}

::std::shared_ptr<ParallelConfSignature> ParallelConfSignature::__SharedMutable__() {
  return ::std::make_shared<ParallelConfSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelConf>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelConf>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_() = default;
Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::~Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ParallelConf>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::ParallelConf& Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstParallelConf> Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ConstParallelConf> Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ConstParallelConf> Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelConf>>& data): Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_(data) {}
_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_() = default;
_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::~_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_() = default;

void _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelConf>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelConf>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::operator==(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ParallelConf>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::operator<(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::ParallelConf> _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ::oneflow::cfg::ParallelConf> _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, ::oneflow::cfg::ParallelConf> _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::shared_mut_end() { return end(); }

}
} // namespace oneflow
