#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/parallel_conf_signature.cfg.h"
#include "oneflow/core/job/placement.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.parallel_conf_signature", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstParallelConf>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstParallelConf>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstParallelConf> (Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::*)(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_&))&_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::*)(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_&))&_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ParallelConf>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ParallelConf>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::ParallelConf> (_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstParallelConfSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelConfSignature>> registry(m, "ConstParallelConfSignature");
    registry.def("__id__", &::oneflow::cfg::ParallelConfSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelConfSignature::DebugString);
    registry.def("__repr__", &ConstParallelConfSignature::DebugString);

    registry.def("has_op_parallel_conf", &ConstParallelConfSignature::has_op_parallel_conf);
    registry.def("op_parallel_conf", &ConstParallelConfSignature::shared_const_op_parallel_conf);

    registry.def("bn_in_op2parallel_conf_size", &ConstParallelConfSignature::bn_in_op2parallel_conf_size);
    registry.def("bn_in_op2parallel_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> (ConstParallelConfSignature::*)() const)&ConstParallelConfSignature::shared_const_bn_in_op2parallel_conf);

    registry.def("bn_in_op2parallel_conf", (::std::shared_ptr<ConstParallelConf> (ConstParallelConfSignature::*)(const ::std::string&) const)&ConstParallelConfSignature::shared_const_bn_in_op2parallel_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelConfSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelConfSignature>> registry(m, "ParallelConfSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelConfSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelConfSignature::*)(const ConstParallelConfSignature&))&::oneflow::cfg::ParallelConfSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelConfSignature::*)(const ::oneflow::cfg::ParallelConfSignature&))&::oneflow::cfg::ParallelConfSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelConfSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelConfSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelConfSignature::DebugString);



    registry.def("has_op_parallel_conf", &::oneflow::cfg::ParallelConfSignature::has_op_parallel_conf);
    registry.def("clear_op_parallel_conf", &::oneflow::cfg::ParallelConfSignature::clear_op_parallel_conf);
    registry.def("op_parallel_conf", &::oneflow::cfg::ParallelConfSignature::shared_const_op_parallel_conf);
    registry.def("mutable_op_parallel_conf", &::oneflow::cfg::ParallelConfSignature::shared_mutable_op_parallel_conf);

    registry.def("bn_in_op2parallel_conf_size", &::oneflow::cfg::ParallelConfSignature::bn_in_op2parallel_conf_size);
    registry.def("bn_in_op2parallel_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> (::oneflow::cfg::ParallelConfSignature::*)() const)&::oneflow::cfg::ParallelConfSignature::shared_const_bn_in_op2parallel_conf);
    registry.def("clear_bn_in_op2parallel_conf", &::oneflow::cfg::ParallelConfSignature::clear_bn_in_op2parallel_conf);
    registry.def("mutable_bn_in_op2parallel_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_CONF_SIGNATURE_CFG_H__MapField___std__string_ParallelConf_> (::oneflow::cfg::ParallelConfSignature::*)())&::oneflow::cfg::ParallelConfSignature::shared_mutable_bn_in_op2parallel_conf);
    registry.def("bn_in_op2parallel_conf", (::std::shared_ptr<ConstParallelConf> (::oneflow::cfg::ParallelConfSignature::*)(const ::std::string&) const)&::oneflow::cfg::ParallelConfSignature::shared_const_bn_in_op2parallel_conf);
  }
}