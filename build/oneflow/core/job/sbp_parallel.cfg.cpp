#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/job/sbp_parallel.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstSplitParallel::_SplitParallel_::_SplitParallel_() { Clear(); }
ConstSplitParallel::_SplitParallel_::_SplitParallel_(const _SplitParallel_& other) { CopyFrom(other); }
ConstSplitParallel::_SplitParallel_::_SplitParallel_(const ::oneflow::SplitParallel& proto_splitparallel) {
  InitFromProto(proto_splitparallel);
}
ConstSplitParallel::_SplitParallel_::_SplitParallel_(_SplitParallel_&& other) = default;
ConstSplitParallel::_SplitParallel_::~_SplitParallel_() = default;

void ConstSplitParallel::_SplitParallel_::InitFromProto(const ::oneflow::SplitParallel& proto_splitparallel) {
  Clear();
  // required_or_optional field: axis
  if (proto_splitparallel.has_axis()) {
    set_axis(proto_splitparallel.axis());
  }
    
}

void ConstSplitParallel::_SplitParallel_::ToProto(::oneflow::SplitParallel* proto_splitparallel) const {
  proto_splitparallel->Clear();
  // required_or_optional field: axis
  if (this->has_axis()) {
    proto_splitparallel->set_axis(axis());
    }

}

::std::string ConstSplitParallel::_SplitParallel_::DebugString() const {
  ::oneflow::SplitParallel proto_splitparallel;
  this->ToProto(&proto_splitparallel);
  return proto_splitparallel.DebugString();
}

void ConstSplitParallel::_SplitParallel_::Clear() {
  clear_axis();
}

void ConstSplitParallel::_SplitParallel_::CopyFrom(const _SplitParallel_& other) {
  if (other.has_axis()) {
    set_axis(other.axis());
  } else {
    clear_axis();
  }
}


// optional field axis
bool ConstSplitParallel::_SplitParallel_::has_axis() const {
  return has_axis_;
}
const int64_t& ConstSplitParallel::_SplitParallel_::axis() const {
  if (has_axis_) { return axis_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstSplitParallel::_SplitParallel_::clear_axis() {
  has_axis_ = false;
}
void ConstSplitParallel::_SplitParallel_::set_axis(const int64_t& value) {
  axis_ = value;
  has_axis_ = true;
}
int64_t* ConstSplitParallel::_SplitParallel_::mutable_axis() {
  has_axis_ = true;
  return &axis_;
}


int ConstSplitParallel::_SplitParallel_::compare(const _SplitParallel_& other) {
  if (!(has_axis() == other.has_axis())) {
    return has_axis() < other.has_axis() ? -1 : 1;
  } else if (!(axis() == other.axis())) {
    return axis() < other.axis() ? -1 : 1;
  }
  return 0;
}

bool ConstSplitParallel::_SplitParallel_::operator==(const _SplitParallel_& other) const {
  return true
    && has_axis() == other.has_axis() 
    && axis() == other.axis()
  ;
}

std::size_t ConstSplitParallel::_SplitParallel_::__CalcHash__() const {
  return 0
    ^ (has_axis() ? std::hash<int64_t>()(axis()) : 0)
  ;
}

bool ConstSplitParallel::_SplitParallel_::operator<(const _SplitParallel_& other) const {
  return false
    || !(has_axis() == other.has_axis()) ? 
      has_axis() < other.has_axis() : false
    || !(axis() == other.axis()) ? 
      axis() < other.axis() : false
  ;
}

using _SplitParallel_ =  ConstSplitParallel::_SplitParallel_;
ConstSplitParallel::ConstSplitParallel(const ::std::shared_ptr<_SplitParallel_>& data): data_(data) {}
ConstSplitParallel::ConstSplitParallel(): data_(::std::make_shared<_SplitParallel_>()) {}
ConstSplitParallel::ConstSplitParallel(const ::oneflow::SplitParallel& proto_splitparallel) {
  BuildFromProto(proto_splitparallel);
}
ConstSplitParallel::ConstSplitParallel(const ConstSplitParallel&) = default;
ConstSplitParallel::ConstSplitParallel(ConstSplitParallel&&) noexcept = default;
ConstSplitParallel::~ConstSplitParallel() = default;

void ConstSplitParallel::ToProto(PbMessage* proto_splitparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SplitParallel*>(proto_splitparallel));
}
  
::std::string ConstSplitParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSplitParallel::__Empty__() const {
  return !data_;
}

int ConstSplitParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"axis", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstSplitParallel::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstSplitParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSplitParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &axis();
    default: return nullptr;
  }
}

// required or optional field axis
bool ConstSplitParallel::has_axis() const {
  return __SharedPtrOrDefault__()->has_axis();
}
const int64_t& ConstSplitParallel::axis() const {
  return __SharedPtrOrDefault__()->axis();
}
// used by pybind11 only

::std::shared_ptr<ConstSplitParallel> ConstSplitParallel::__SharedConst__() const {
  return ::std::make_shared<ConstSplitParallel>(data_);
}
int64_t ConstSplitParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSplitParallel::operator==(const ConstSplitParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSplitParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSplitParallel::operator<(const ConstSplitParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SplitParallel_>& ConstSplitParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SplitParallel_> default_ptr = std::make_shared<_SplitParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_SplitParallel_>& ConstSplitParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _SplitParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSplitParallel
void ConstSplitParallel::BuildFromProto(const PbMessage& proto_splitparallel) {
  data_ = ::std::make_shared<_SplitParallel_>(dynamic_cast<const ::oneflow::SplitParallel&>(proto_splitparallel));
}

SplitParallel::SplitParallel(const ::std::shared_ptr<_SplitParallel_>& data)
  : ConstSplitParallel(data) {}
SplitParallel::SplitParallel(const SplitParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SplitParallel> resize
SplitParallel::SplitParallel(SplitParallel&&) noexcept = default; 
SplitParallel::SplitParallel(const ::oneflow::SplitParallel& proto_splitparallel) {
  InitFromProto(proto_splitparallel);
}
SplitParallel::SplitParallel() = default;

SplitParallel::~SplitParallel() = default;

void SplitParallel::InitFromProto(const PbMessage& proto_splitparallel) {
  BuildFromProto(proto_splitparallel);
}
  
void* SplitParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_axis();
    default: return nullptr;
  }
}

bool SplitParallel::operator==(const SplitParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SplitParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SplitParallel::operator<(const SplitParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SplitParallel::Clear() {
  if (data_) { data_.reset(); }
}
void SplitParallel::CopyFrom(const SplitParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SplitParallel& SplitParallel::operator=(const SplitParallel& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field axis
void SplitParallel::clear_axis() {
  return __SharedPtr__()->clear_axis();
}
void SplitParallel::set_axis(const int64_t& value) {
  return __SharedPtr__()->set_axis(value);
}
int64_t* SplitParallel::mutable_axis() {
  return  __SharedPtr__()->mutable_axis();
}

::std::shared_ptr<SplitParallel> SplitParallel::__SharedMutable__() {
  return ::std::make_shared<SplitParallel>(__SharedPtr__());
}
ConstBroadcastParallel::_BroadcastParallel_::_BroadcastParallel_() { Clear(); }
ConstBroadcastParallel::_BroadcastParallel_::_BroadcastParallel_(const _BroadcastParallel_& other) { CopyFrom(other); }
ConstBroadcastParallel::_BroadcastParallel_::_BroadcastParallel_(const ::oneflow::BroadcastParallel& proto_broadcastparallel) {
  InitFromProto(proto_broadcastparallel);
}
ConstBroadcastParallel::_BroadcastParallel_::_BroadcastParallel_(_BroadcastParallel_&& other) = default;
ConstBroadcastParallel::_BroadcastParallel_::~_BroadcastParallel_() = default;

void ConstBroadcastParallel::_BroadcastParallel_::InitFromProto(const ::oneflow::BroadcastParallel& proto_broadcastparallel) {
  Clear();
    
}

void ConstBroadcastParallel::_BroadcastParallel_::ToProto(::oneflow::BroadcastParallel* proto_broadcastparallel) const {
  proto_broadcastparallel->Clear();

}

::std::string ConstBroadcastParallel::_BroadcastParallel_::DebugString() const {
  ::oneflow::BroadcastParallel proto_broadcastparallel;
  this->ToProto(&proto_broadcastparallel);
  return proto_broadcastparallel.DebugString();
}

void ConstBroadcastParallel::_BroadcastParallel_::Clear() {
}

void ConstBroadcastParallel::_BroadcastParallel_::CopyFrom(const _BroadcastParallel_& other) {
}



int ConstBroadcastParallel::_BroadcastParallel_::compare(const _BroadcastParallel_& other) {
  return 0;
}

bool ConstBroadcastParallel::_BroadcastParallel_::operator==(const _BroadcastParallel_& other) const {
  return true
  ;
}

std::size_t ConstBroadcastParallel::_BroadcastParallel_::__CalcHash__() const {
  return 0
  ;
}

bool ConstBroadcastParallel::_BroadcastParallel_::operator<(const _BroadcastParallel_& other) const {
  return false
  ;
}

using _BroadcastParallel_ =  ConstBroadcastParallel::_BroadcastParallel_;
ConstBroadcastParallel::ConstBroadcastParallel(const ::std::shared_ptr<_BroadcastParallel_>& data): data_(data) {}
ConstBroadcastParallel::ConstBroadcastParallel(): data_(::std::make_shared<_BroadcastParallel_>()) {}
ConstBroadcastParallel::ConstBroadcastParallel(const ::oneflow::BroadcastParallel& proto_broadcastparallel) {
  BuildFromProto(proto_broadcastparallel);
}
ConstBroadcastParallel::ConstBroadcastParallel(const ConstBroadcastParallel&) = default;
ConstBroadcastParallel::ConstBroadcastParallel(ConstBroadcastParallel&&) noexcept = default;
ConstBroadcastParallel::~ConstBroadcastParallel() = default;

void ConstBroadcastParallel::ToProto(PbMessage* proto_broadcastparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BroadcastParallel*>(proto_broadcastparallel));
}
  
::std::string ConstBroadcastParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBroadcastParallel::__Empty__() const {
  return !data_;
}

int ConstBroadcastParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstBroadcastParallel::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstBroadcastParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBroadcastParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstBroadcastParallel> ConstBroadcastParallel::__SharedConst__() const {
  return ::std::make_shared<ConstBroadcastParallel>(data_);
}
int64_t ConstBroadcastParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBroadcastParallel::operator==(const ConstBroadcastParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBroadcastParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBroadcastParallel::operator<(const ConstBroadcastParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BroadcastParallel_>& ConstBroadcastParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BroadcastParallel_> default_ptr = std::make_shared<_BroadcastParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_BroadcastParallel_>& ConstBroadcastParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _BroadcastParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBroadcastParallel
void ConstBroadcastParallel::BuildFromProto(const PbMessage& proto_broadcastparallel) {
  data_ = ::std::make_shared<_BroadcastParallel_>(dynamic_cast<const ::oneflow::BroadcastParallel&>(proto_broadcastparallel));
}

BroadcastParallel::BroadcastParallel(const ::std::shared_ptr<_BroadcastParallel_>& data)
  : ConstBroadcastParallel(data) {}
BroadcastParallel::BroadcastParallel(const BroadcastParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BroadcastParallel> resize
BroadcastParallel::BroadcastParallel(BroadcastParallel&&) noexcept = default; 
BroadcastParallel::BroadcastParallel(const ::oneflow::BroadcastParallel& proto_broadcastparallel) {
  InitFromProto(proto_broadcastparallel);
}
BroadcastParallel::BroadcastParallel() = default;

BroadcastParallel::~BroadcastParallel() = default;

void BroadcastParallel::InitFromProto(const PbMessage& proto_broadcastparallel) {
  BuildFromProto(proto_broadcastparallel);
}
  
void* BroadcastParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool BroadcastParallel::operator==(const BroadcastParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BroadcastParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BroadcastParallel::operator<(const BroadcastParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BroadcastParallel::Clear() {
  if (data_) { data_.reset(); }
}
void BroadcastParallel::CopyFrom(const BroadcastParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BroadcastParallel& BroadcastParallel::operator=(const BroadcastParallel& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<BroadcastParallel> BroadcastParallel::__SharedMutable__() {
  return ::std::make_shared<BroadcastParallel>(__SharedPtr__());
}
ConstPartialSumParallel::_PartialSumParallel_::_PartialSumParallel_() { Clear(); }
ConstPartialSumParallel::_PartialSumParallel_::_PartialSumParallel_(const _PartialSumParallel_& other) { CopyFrom(other); }
ConstPartialSumParallel::_PartialSumParallel_::_PartialSumParallel_(const ::oneflow::PartialSumParallel& proto_partialsumparallel) {
  InitFromProto(proto_partialsumparallel);
}
ConstPartialSumParallel::_PartialSumParallel_::_PartialSumParallel_(_PartialSumParallel_&& other) = default;
ConstPartialSumParallel::_PartialSumParallel_::~_PartialSumParallel_() = default;

void ConstPartialSumParallel::_PartialSumParallel_::InitFromProto(const ::oneflow::PartialSumParallel& proto_partialsumparallel) {
  Clear();
    
}

void ConstPartialSumParallel::_PartialSumParallel_::ToProto(::oneflow::PartialSumParallel* proto_partialsumparallel) const {
  proto_partialsumparallel->Clear();

}

::std::string ConstPartialSumParallel::_PartialSumParallel_::DebugString() const {
  ::oneflow::PartialSumParallel proto_partialsumparallel;
  this->ToProto(&proto_partialsumparallel);
  return proto_partialsumparallel.DebugString();
}

void ConstPartialSumParallel::_PartialSumParallel_::Clear() {
}

void ConstPartialSumParallel::_PartialSumParallel_::CopyFrom(const _PartialSumParallel_& other) {
}



int ConstPartialSumParallel::_PartialSumParallel_::compare(const _PartialSumParallel_& other) {
  return 0;
}

bool ConstPartialSumParallel::_PartialSumParallel_::operator==(const _PartialSumParallel_& other) const {
  return true
  ;
}

std::size_t ConstPartialSumParallel::_PartialSumParallel_::__CalcHash__() const {
  return 0
  ;
}

bool ConstPartialSumParallel::_PartialSumParallel_::operator<(const _PartialSumParallel_& other) const {
  return false
  ;
}

using _PartialSumParallel_ =  ConstPartialSumParallel::_PartialSumParallel_;
ConstPartialSumParallel::ConstPartialSumParallel(const ::std::shared_ptr<_PartialSumParallel_>& data): data_(data) {}
ConstPartialSumParallel::ConstPartialSumParallel(): data_(::std::make_shared<_PartialSumParallel_>()) {}
ConstPartialSumParallel::ConstPartialSumParallel(const ::oneflow::PartialSumParallel& proto_partialsumparallel) {
  BuildFromProto(proto_partialsumparallel);
}
ConstPartialSumParallel::ConstPartialSumParallel(const ConstPartialSumParallel&) = default;
ConstPartialSumParallel::ConstPartialSumParallel(ConstPartialSumParallel&&) noexcept = default;
ConstPartialSumParallel::~ConstPartialSumParallel() = default;

void ConstPartialSumParallel::ToProto(PbMessage* proto_partialsumparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PartialSumParallel*>(proto_partialsumparallel));
}
  
::std::string ConstPartialSumParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPartialSumParallel::__Empty__() const {
  return !data_;
}

int ConstPartialSumParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstPartialSumParallel::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstPartialSumParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPartialSumParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstPartialSumParallel> ConstPartialSumParallel::__SharedConst__() const {
  return ::std::make_shared<ConstPartialSumParallel>(data_);
}
int64_t ConstPartialSumParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPartialSumParallel::operator==(const ConstPartialSumParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPartialSumParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPartialSumParallel::operator<(const ConstPartialSumParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PartialSumParallel_>& ConstPartialSumParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PartialSumParallel_> default_ptr = std::make_shared<_PartialSumParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_PartialSumParallel_>& ConstPartialSumParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _PartialSumParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPartialSumParallel
void ConstPartialSumParallel::BuildFromProto(const PbMessage& proto_partialsumparallel) {
  data_ = ::std::make_shared<_PartialSumParallel_>(dynamic_cast<const ::oneflow::PartialSumParallel&>(proto_partialsumparallel));
}

PartialSumParallel::PartialSumParallel(const ::std::shared_ptr<_PartialSumParallel_>& data)
  : ConstPartialSumParallel(data) {}
PartialSumParallel::PartialSumParallel(const PartialSumParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PartialSumParallel> resize
PartialSumParallel::PartialSumParallel(PartialSumParallel&&) noexcept = default; 
PartialSumParallel::PartialSumParallel(const ::oneflow::PartialSumParallel& proto_partialsumparallel) {
  InitFromProto(proto_partialsumparallel);
}
PartialSumParallel::PartialSumParallel() = default;

PartialSumParallel::~PartialSumParallel() = default;

void PartialSumParallel::InitFromProto(const PbMessage& proto_partialsumparallel) {
  BuildFromProto(proto_partialsumparallel);
}
  
void* PartialSumParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool PartialSumParallel::operator==(const PartialSumParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PartialSumParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PartialSumParallel::operator<(const PartialSumParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PartialSumParallel::Clear() {
  if (data_) { data_.reset(); }
}
void PartialSumParallel::CopyFrom(const PartialSumParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PartialSumParallel& PartialSumParallel::operator=(const PartialSumParallel& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<PartialSumParallel> PartialSumParallel::__SharedMutable__() {
  return ::std::make_shared<PartialSumParallel>(__SharedPtr__());
}
ConstSbpParallel::_SbpParallel_::_SbpParallel_() { Clear(); }
ConstSbpParallel::_SbpParallel_::_SbpParallel_(const _SbpParallel_& other) { CopyFrom(other); }
ConstSbpParallel::_SbpParallel_::_SbpParallel_(const ::oneflow::SbpParallel& proto_sbpparallel) {
  InitFromProto(proto_sbpparallel);
}
ConstSbpParallel::_SbpParallel_::_SbpParallel_(_SbpParallel_&& other) = default;
ConstSbpParallel::_SbpParallel_::~_SbpParallel_() = default;

void ConstSbpParallel::_SbpParallel_::InitFromProto(const ::oneflow::SbpParallel& proto_sbpparallel) {
  Clear();
  // oneof field: parallel_type
  ParallelTypeCase parallel_type_case = ParallelTypeCase(int(proto_sbpparallel.parallel_type_case()));
  switch (parallel_type_case) {
    case kSplitParallel: {
      *mutable_split_parallel() = ::oneflow::cfg::SplitParallel(proto_sbpparallel.split_parallel());
      break;
  }
    case kBroadcastParallel: {
      *mutable_broadcast_parallel() = ::oneflow::cfg::BroadcastParallel(proto_sbpparallel.broadcast_parallel());
      break;
  }
    case kPartialSumParallel: {
      *mutable_partial_sum_parallel() = ::oneflow::cfg::PartialSumParallel(proto_sbpparallel.partial_sum_parallel());
      break;
  }
    case PARALLEL_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstSbpParallel::_SbpParallel_::ToProto(::oneflow::SbpParallel* proto_sbpparallel) const {
  proto_sbpparallel->Clear();

  // oneof field: parallel_type
  ::oneflow::SbpParallel::ParallelTypeCase parallel_type_case = ::oneflow::SbpParallel::ParallelTypeCase(int(this->parallel_type_case()));
  switch (parallel_type_case) {
    case ::oneflow::SbpParallel::kSplitParallel: {
      ::oneflow::SplitParallel of_proto_split_parallel;
      split_parallel().ToProto(&of_proto_split_parallel);
      proto_sbpparallel->mutable_split_parallel()->CopyFrom(of_proto_split_parallel);
      break;
    }
    case ::oneflow::SbpParallel::kBroadcastParallel: {
      ::oneflow::BroadcastParallel of_proto_broadcast_parallel;
      broadcast_parallel().ToProto(&of_proto_broadcast_parallel);
      proto_sbpparallel->mutable_broadcast_parallel()->CopyFrom(of_proto_broadcast_parallel);
      break;
    }
    case ::oneflow::SbpParallel::kPartialSumParallel: {
      ::oneflow::PartialSumParallel of_proto_partial_sum_parallel;
      partial_sum_parallel().ToProto(&of_proto_partial_sum_parallel);
      proto_sbpparallel->mutable_partial_sum_parallel()->CopyFrom(of_proto_partial_sum_parallel);
      break;
    }
    case ::oneflow::SbpParallel::PARALLEL_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstSbpParallel::_SbpParallel_::DebugString() const {
  ::oneflow::SbpParallel proto_sbpparallel;
  this->ToProto(&proto_sbpparallel);
  return proto_sbpparallel.DebugString();
}

void ConstSbpParallel::_SbpParallel_::Clear() {
  clear_parallel_type();
}

void ConstSbpParallel::_SbpParallel_::CopyFrom(const _SbpParallel_& other) {
  parallel_type_copy_from(other);
}


// oneof field parallel_type: split_parallel
bool ConstSbpParallel::_SbpParallel_::has_split_parallel() const {
  return parallel_type_case() == kSplitParallel;
}
void ConstSbpParallel::_SbpParallel_::clear_split_parallel() {
  if (has_split_parallel()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::SplitParallel>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.split_parallel_));
      ptr->~Shared_ptr();
    }
    parallel_type_case_ = PARALLEL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::SplitParallel& ConstSbpParallel::_SbpParallel_::split_parallel() const {
  if (has_split_parallel()) {
      const ::std::shared_ptr<::oneflow::cfg::SplitParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::SplitParallel>*>(&(parallel_type_.split_parallel_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::SplitParallel> default_static_value = ::std::make_shared<::oneflow::cfg::SplitParallel>();
    return *default_static_value;
    }
}
::oneflow::cfg::SplitParallel* ConstSbpParallel::_SbpParallel_::mutable_split_parallel() {
  if(!has_split_parallel()) {
    clear_parallel_type();
    new (&(parallel_type_.split_parallel_)) ::std::shared_ptr<::oneflow::cfg::SplitParallel>(new ::oneflow::cfg::SplitParallel());
  }
  parallel_type_case_ = kSplitParallel;
  ::std::shared_ptr<::oneflow::cfg::SplitParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::SplitParallel>*>(&(parallel_type_.split_parallel_));
  return  (*ptr).get();
}

// oneof field parallel_type: broadcast_parallel
bool ConstSbpParallel::_SbpParallel_::has_broadcast_parallel() const {
  return parallel_type_case() == kBroadcastParallel;
}
void ConstSbpParallel::_SbpParallel_::clear_broadcast_parallel() {
  if (has_broadcast_parallel()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.broadcast_parallel_));
      ptr->~Shared_ptr();
    }
    parallel_type_case_ = PARALLEL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::BroadcastParallel& ConstSbpParallel::_SbpParallel_::broadcast_parallel() const {
  if (has_broadcast_parallel()) {
      const ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>*>(&(parallel_type_.broadcast_parallel_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::BroadcastParallel> default_static_value = ::std::make_shared<::oneflow::cfg::BroadcastParallel>();
    return *default_static_value;
    }
}
::oneflow::cfg::BroadcastParallel* ConstSbpParallel::_SbpParallel_::mutable_broadcast_parallel() {
  if(!has_broadcast_parallel()) {
    clear_parallel_type();
    new (&(parallel_type_.broadcast_parallel_)) ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>(new ::oneflow::cfg::BroadcastParallel());
  }
  parallel_type_case_ = kBroadcastParallel;
  ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::BroadcastParallel>*>(&(parallel_type_.broadcast_parallel_));
  return  (*ptr).get();
}

// oneof field parallel_type: partial_sum_parallel
bool ConstSbpParallel::_SbpParallel_::has_partial_sum_parallel() const {
  return parallel_type_case() == kPartialSumParallel;
}
void ConstSbpParallel::_SbpParallel_::clear_partial_sum_parallel() {
  if (has_partial_sum_parallel()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.partial_sum_parallel_));
      ptr->~Shared_ptr();
    }
    parallel_type_case_ = PARALLEL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PartialSumParallel& ConstSbpParallel::_SbpParallel_::partial_sum_parallel() const {
  if (has_partial_sum_parallel()) {
      const ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>*>(&(parallel_type_.partial_sum_parallel_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PartialSumParallel> default_static_value = ::std::make_shared<::oneflow::cfg::PartialSumParallel>();
    return *default_static_value;
    }
}
::oneflow::cfg::PartialSumParallel* ConstSbpParallel::_SbpParallel_::mutable_partial_sum_parallel() {
  if(!has_partial_sum_parallel()) {
    clear_parallel_type();
    new (&(parallel_type_.partial_sum_parallel_)) ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>(new ::oneflow::cfg::PartialSumParallel());
  }
  parallel_type_case_ = kPartialSumParallel;
  ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PartialSumParallel>*>(&(parallel_type_.partial_sum_parallel_));
  return  (*ptr).get();
}
ConstSbpParallel::ParallelTypeCase ConstSbpParallel::_SbpParallel_::parallel_type_case() const {
  return parallel_type_case_;
}
bool ConstSbpParallel::_SbpParallel_::has_parallel_type() const {
  return parallel_type_case_ != PARALLEL_TYPE_NOT_SET;
}
void ConstSbpParallel::_SbpParallel_::clear_parallel_type() {
  switch (parallel_type_case()) {
    case kSplitParallel: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::SplitParallel>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.split_parallel_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kBroadcastParallel: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BroadcastParallel>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.broadcast_parallel_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPartialSumParallel: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PartialSumParallel>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(parallel_type_.partial_sum_parallel_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case PARALLEL_TYPE_NOT_SET: {
      break;
    }
  }
  parallel_type_case_ = PARALLEL_TYPE_NOT_SET;
}
void ConstSbpParallel::_SbpParallel_::parallel_type_copy_from(const _SbpParallel_& other) {
  switch (other.parallel_type_case()) {
    case kSplitParallel: {
      mutable_split_parallel()->CopyFrom(other.split_parallel());
      break;
    }
    case kBroadcastParallel: {
      mutable_broadcast_parallel()->CopyFrom(other.broadcast_parallel());
      break;
    }
    case kPartialSumParallel: {
      mutable_partial_sum_parallel()->CopyFrom(other.partial_sum_parallel());
      break;
    }
    case PARALLEL_TYPE_NOT_SET: {
      clear_parallel_type();
    }
  }
}


int ConstSbpParallel::_SbpParallel_::compare(const _SbpParallel_& other) {
  if (!(parallel_type_case() == other.parallel_type_case())) {
    return parallel_type_case() < other.parallel_type_case() ? -1 : 1;
  }
  switch (parallel_type_case()) {
    case kSplitParallel: {
      if (!(split_parallel() == other.split_parallel())) {
        return split_parallel() < other.split_parallel() ? -1 : 1;
      }
      break;
    }
    case kBroadcastParallel: {
      if (!(broadcast_parallel() == other.broadcast_parallel())) {
        return broadcast_parallel() < other.broadcast_parallel() ? -1 : 1;
      }
      break;
    }
    case kPartialSumParallel: {
      if (!(partial_sum_parallel() == other.partial_sum_parallel())) {
        return partial_sum_parallel() < other.partial_sum_parallel() ? -1 : 1;
      }
      break;
    }
    case PARALLEL_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstSbpParallel::_SbpParallel_::operator==(const _SbpParallel_& other) const {
  return true
    && parallel_type_case() == other.parallel_type_case()
    && (parallel_type_case() == kSplitParallel ? 
      split_parallel() == other.split_parallel() : true)
    && (parallel_type_case() == kBroadcastParallel ? 
      broadcast_parallel() == other.broadcast_parallel() : true)
    && (parallel_type_case() == kPartialSumParallel ? 
      partial_sum_parallel() == other.partial_sum_parallel() : true)
  ;
}

std::size_t ConstSbpParallel::_SbpParallel_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(parallel_type_case())
    ^ (has_split_parallel() ? std::hash<::oneflow::cfg::SplitParallel>()(split_parallel()) : 0)
    ^ (has_broadcast_parallel() ? std::hash<::oneflow::cfg::BroadcastParallel>()(broadcast_parallel()) : 0)
    ^ (has_partial_sum_parallel() ? std::hash<::oneflow::cfg::PartialSumParallel>()(partial_sum_parallel()) : 0)
  ;
}

bool ConstSbpParallel::_SbpParallel_::operator<(const _SbpParallel_& other) const {
  return false
    || !(parallel_type_case() == other.parallel_type_case()) ? 
      parallel_type_case() < other.parallel_type_case() : false
    || ((parallel_type_case() == kSplitParallel) && 
      !(split_parallel() == other.split_parallel())) ? 
        split_parallel() < other.split_parallel() : false
    || ((parallel_type_case() == kBroadcastParallel) && 
      !(broadcast_parallel() == other.broadcast_parallel())) ? 
        broadcast_parallel() < other.broadcast_parallel() : false
    || ((parallel_type_case() == kPartialSumParallel) && 
      !(partial_sum_parallel() == other.partial_sum_parallel())) ? 
        partial_sum_parallel() < other.partial_sum_parallel() : false
  ;
}

using _SbpParallel_ =  ConstSbpParallel::_SbpParallel_;
ConstSbpParallel::ConstSbpParallel(const ::std::shared_ptr<_SbpParallel_>& data): data_(data) {}
ConstSbpParallel::ConstSbpParallel(): data_(::std::make_shared<_SbpParallel_>()) {}
ConstSbpParallel::ConstSbpParallel(const ::oneflow::SbpParallel& proto_sbpparallel) {
  BuildFromProto(proto_sbpparallel);
}
ConstSbpParallel::ConstSbpParallel(const ConstSbpParallel&) = default;
ConstSbpParallel::ConstSbpParallel(ConstSbpParallel&&) noexcept = default;
ConstSbpParallel::~ConstSbpParallel() = default;

void ConstSbpParallel::ToProto(PbMessage* proto_sbpparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SbpParallel*>(proto_sbpparallel));
}
  
::std::string ConstSbpParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSbpParallel::__Empty__() const {
  return !data_;
}

int ConstSbpParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"split_parallel", 1},
    {"broadcast_parallel", 2},
    {"partial_sum_parallel", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstSbpParallel::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstSbpParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::SplitParallel),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstSplitParallel),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BroadcastParallel),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBroadcastParallel),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PartialSumParallel),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPartialSumParallel),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSbpParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &split_parallel();
    case 2: return &broadcast_parallel();
    case 3: return &partial_sum_parallel();
    default: return nullptr;
  }
}

 // oneof field parallel_type: split_parallel
bool ConstSbpParallel::has_split_parallel() const {
  return __SharedPtrOrDefault__()->has_split_parallel();
}
const ::oneflow::cfg::SplitParallel& ConstSbpParallel::split_parallel() const {
  return __SharedPtrOrDefault__()->split_parallel();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstSplitParallel> ConstSbpParallel::shared_const_split_parallel() const {
  return split_parallel().__SharedConst__();
}
 // oneof field parallel_type: broadcast_parallel
bool ConstSbpParallel::has_broadcast_parallel() const {
  return __SharedPtrOrDefault__()->has_broadcast_parallel();
}
const ::oneflow::cfg::BroadcastParallel& ConstSbpParallel::broadcast_parallel() const {
  return __SharedPtrOrDefault__()->broadcast_parallel();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBroadcastParallel> ConstSbpParallel::shared_const_broadcast_parallel() const {
  return broadcast_parallel().__SharedConst__();
}
 // oneof field parallel_type: partial_sum_parallel
bool ConstSbpParallel::has_partial_sum_parallel() const {
  return __SharedPtrOrDefault__()->has_partial_sum_parallel();
}
const ::oneflow::cfg::PartialSumParallel& ConstSbpParallel::partial_sum_parallel() const {
  return __SharedPtrOrDefault__()->partial_sum_parallel();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPartialSumParallel> ConstSbpParallel::shared_const_partial_sum_parallel() const {
  return partial_sum_parallel().__SharedConst__();
}
ConstSbpParallel::ParallelTypeCase ConstSbpParallel::parallel_type_case() const {
  return __SharedPtrOrDefault__()->parallel_type_case();
}

bool ConstSbpParallel::has_parallel_type() const {
  return __SharedPtrOrDefault__()->has_parallel_type();
}

::std::shared_ptr<ConstSbpParallel> ConstSbpParallel::__SharedConst__() const {
  return ::std::make_shared<ConstSbpParallel>(data_);
}
int64_t ConstSbpParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSbpParallel::operator==(const ConstSbpParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSbpParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSbpParallel::operator<(const ConstSbpParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SbpParallel_>& ConstSbpParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SbpParallel_> default_ptr = std::make_shared<_SbpParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_SbpParallel_>& ConstSbpParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _SbpParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSbpParallel
void ConstSbpParallel::BuildFromProto(const PbMessage& proto_sbpparallel) {
  data_ = ::std::make_shared<_SbpParallel_>(dynamic_cast<const ::oneflow::SbpParallel&>(proto_sbpparallel));
}

SbpParallel::SbpParallel(const ::std::shared_ptr<_SbpParallel_>& data)
  : ConstSbpParallel(data) {}
SbpParallel::SbpParallel(const SbpParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SbpParallel> resize
SbpParallel::SbpParallel(SbpParallel&&) noexcept = default; 
SbpParallel::SbpParallel(const ::oneflow::SbpParallel& proto_sbpparallel) {
  InitFromProto(proto_sbpparallel);
}
SbpParallel::SbpParallel() = default;

SbpParallel::~SbpParallel() = default;

void SbpParallel::InitFromProto(const PbMessage& proto_sbpparallel) {
  BuildFromProto(proto_sbpparallel);
}
  
void* SbpParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_split_parallel();
    case 2: return mutable_broadcast_parallel();
    case 3: return mutable_partial_sum_parallel();
    default: return nullptr;
  }
}

bool SbpParallel::operator==(const SbpParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SbpParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SbpParallel::operator<(const SbpParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SbpParallel::Clear() {
  if (data_) { data_.reset(); }
}
void SbpParallel::CopyFrom(const SbpParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SbpParallel& SbpParallel::operator=(const SbpParallel& other) {
  CopyFrom(other);
  return *this;
}

void SbpParallel::clear_split_parallel() {
  return __SharedPtr__()->clear_split_parallel();
}
::oneflow::cfg::SplitParallel* SbpParallel::mutable_split_parallel() {
  return __SharedPtr__()->mutable_split_parallel();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::SplitParallel> SbpParallel::shared_mutable_split_parallel() {
  return mutable_split_parallel()->__SharedMutable__();
}
void SbpParallel::clear_broadcast_parallel() {
  return __SharedPtr__()->clear_broadcast_parallel();
}
::oneflow::cfg::BroadcastParallel* SbpParallel::mutable_broadcast_parallel() {
  return __SharedPtr__()->mutable_broadcast_parallel();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BroadcastParallel> SbpParallel::shared_mutable_broadcast_parallel() {
  return mutable_broadcast_parallel()->__SharedMutable__();
}
void SbpParallel::clear_partial_sum_parallel() {
  return __SharedPtr__()->clear_partial_sum_parallel();
}
::oneflow::cfg::PartialSumParallel* SbpParallel::mutable_partial_sum_parallel() {
  return __SharedPtr__()->mutable_partial_sum_parallel();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PartialSumParallel> SbpParallel::shared_mutable_partial_sum_parallel() {
  return mutable_partial_sum_parallel()->__SharedMutable__();
}

::std::shared_ptr<SbpParallel> SbpParallel::__SharedMutable__() {
  return ::std::make_shared<SbpParallel>(__SharedPtr__());
}
ConstSbpSignature::_SbpSignature_::_SbpSignature_() { Clear(); }
ConstSbpSignature::_SbpSignature_::_SbpSignature_(const _SbpSignature_& other) { CopyFrom(other); }
ConstSbpSignature::_SbpSignature_::_SbpSignature_(const ::oneflow::SbpSignature& proto_sbpsignature) {
  InitFromProto(proto_sbpsignature);
}
ConstSbpSignature::_SbpSignature_::_SbpSignature_(_SbpSignature_&& other) = default;
ConstSbpSignature::_SbpSignature_::~_SbpSignature_() = default;

void ConstSbpSignature::_SbpSignature_::InitFromProto(const ::oneflow::SbpSignature& proto_sbpsignature) {
  Clear();
  // map field : bn_in_op2sbp_parallel
  if (!proto_sbpsignature.bn_in_op2sbp_parallel().empty()) {
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_&  mut_bn_in_op2sbp_parallel = *mutable_bn_in_op2sbp_parallel();
    for (const auto& pair : proto_sbpsignature.bn_in_op2sbp_parallel()) {
      mut_bn_in_op2sbp_parallel[pair.first] = ::oneflow::cfg::SbpParallel(pair.second);
    }
  }
    
}

void ConstSbpSignature::_SbpSignature_::ToProto(::oneflow::SbpSignature* proto_sbpsignature) const {
  proto_sbpsignature->Clear();
  // map field : bn_in_op2sbp_parallel
  if (!bn_in_op2sbp_parallel().empty()) {
    auto& mut_bn_in_op2sbp_parallel = *(proto_sbpsignature->mutable_bn_in_op2sbp_parallel());
    for (const auto& pair : bn_in_op2sbp_parallel()) {
      ::oneflow::SbpParallel proto_bn_in_op2sbp_parallel_value;
      pair.second.ToProto(&proto_bn_in_op2sbp_parallel_value);
      mut_bn_in_op2sbp_parallel[pair.first] = proto_bn_in_op2sbp_parallel_value;
    }
  }

}

::std::string ConstSbpSignature::_SbpSignature_::DebugString() const {
  ::oneflow::SbpSignature proto_sbpsignature;
  this->ToProto(&proto_sbpsignature);
  return proto_sbpsignature.DebugString();
}

void ConstSbpSignature::_SbpSignature_::Clear() {
  clear_bn_in_op2sbp_parallel();
}

void ConstSbpSignature::_SbpSignature_::CopyFrom(const _SbpSignature_& other) {
  mutable_bn_in_op2sbp_parallel()->CopyFrom(other.bn_in_op2sbp_parallel());
}


::std::size_t ConstSbpSignature::_SbpSignature_::bn_in_op2sbp_parallel_size() const {
  if (!bn_in_op2sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>();
    return default_static_value->size();
  }
  return bn_in_op2sbp_parallel_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& ConstSbpSignature::_SbpSignature_::bn_in_op2sbp_parallel() const {
  if (!bn_in_op2sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2sbp_parallel_.get());
}

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ * ConstSbpSignature::_SbpSignature_::mutable_bn_in_op2sbp_parallel() {
  if (!bn_in_op2sbp_parallel_) {
    bn_in_op2sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>();
  }
  return bn_in_op2sbp_parallel_.get();
}

const ::oneflow::cfg::SbpParallel& ConstSbpSignature::_SbpSignature_::bn_in_op2sbp_parallel(::std::string key) const {
  if (!bn_in_op2sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>();
    return default_static_value->at(key);
  }
  return bn_in_op2sbp_parallel_->at(key);
}

void ConstSbpSignature::_SbpSignature_::clear_bn_in_op2sbp_parallel() {
  if (!bn_in_op2sbp_parallel_) {
    bn_in_op2sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>();
  }
  return bn_in_op2sbp_parallel_->Clear();
}



int ConstSbpSignature::_SbpSignature_::compare(const _SbpSignature_& other) {
  if (!(bn_in_op2sbp_parallel() == other.bn_in_op2sbp_parallel())) {
    return bn_in_op2sbp_parallel() < other.bn_in_op2sbp_parallel() ? -1 : 1;
  }
  return 0;
}

bool ConstSbpSignature::_SbpSignature_::operator==(const _SbpSignature_& other) const {
  return true
    && bn_in_op2sbp_parallel() == other.bn_in_op2sbp_parallel()
  ;
}

std::size_t ConstSbpSignature::_SbpSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2sbp_parallel().__CalcHash__()
  ;
}

bool ConstSbpSignature::_SbpSignature_::operator<(const _SbpSignature_& other) const {
  return false
    || !(bn_in_op2sbp_parallel() == other.bn_in_op2sbp_parallel()) ? 
      bn_in_op2sbp_parallel() < other.bn_in_op2sbp_parallel() : false
  ;
}

using _SbpSignature_ =  ConstSbpSignature::_SbpSignature_;
ConstSbpSignature::ConstSbpSignature(const ::std::shared_ptr<_SbpSignature_>& data): data_(data) {}
ConstSbpSignature::ConstSbpSignature(): data_(::std::make_shared<_SbpSignature_>()) {}
ConstSbpSignature::ConstSbpSignature(const ::oneflow::SbpSignature& proto_sbpsignature) {
  BuildFromProto(proto_sbpsignature);
}
ConstSbpSignature::ConstSbpSignature(const ConstSbpSignature&) = default;
ConstSbpSignature::ConstSbpSignature(ConstSbpSignature&&) noexcept = default;
ConstSbpSignature::~ConstSbpSignature() = default;

void ConstSbpSignature::ToProto(PbMessage* proto_sbpsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SbpSignature*>(proto_sbpsignature));
}
  
::std::string ConstSbpSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSbpSignature::__Empty__() const {
  return !data_;
}

int ConstSbpSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2sbp_parallel", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstSbpSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstSbpSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpParallel>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSbpSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2sbp_parallel();
    default: return nullptr;
  }
}

// map field bn_in_op2sbp_parallel
::std::size_t ConstSbpSignature::bn_in_op2sbp_parallel_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2sbp_parallel_size();
}

const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& ConstSbpSignature::bn_in_op2sbp_parallel() const {
  return __SharedPtrOrDefault__()->bn_in_op2sbp_parallel();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> ConstSbpSignature::shared_const_bn_in_op2sbp_parallel() const {
  return bn_in_op2sbp_parallel().__SharedConst__();
}

::std::shared_ptr<ConstSbpSignature> ConstSbpSignature::__SharedConst__() const {
  return ::std::make_shared<ConstSbpSignature>(data_);
}
int64_t ConstSbpSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSbpSignature::operator==(const ConstSbpSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSbpSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSbpSignature::operator<(const ConstSbpSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SbpSignature_>& ConstSbpSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SbpSignature_> default_ptr = std::make_shared<_SbpSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_SbpSignature_>& ConstSbpSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _SbpSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSbpSignature
void ConstSbpSignature::BuildFromProto(const PbMessage& proto_sbpsignature) {
  data_ = ::std::make_shared<_SbpSignature_>(dynamic_cast<const ::oneflow::SbpSignature&>(proto_sbpsignature));
}

SbpSignature::SbpSignature(const ::std::shared_ptr<_SbpSignature_>& data)
  : ConstSbpSignature(data) {}
SbpSignature::SbpSignature(const SbpSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SbpSignature> resize
SbpSignature::SbpSignature(SbpSignature&&) noexcept = default; 
SbpSignature::SbpSignature(const ::oneflow::SbpSignature& proto_sbpsignature) {
  InitFromProto(proto_sbpsignature);
}
SbpSignature::SbpSignature() = default;

SbpSignature::~SbpSignature() = default;

void SbpSignature::InitFromProto(const PbMessage& proto_sbpsignature) {
  BuildFromProto(proto_sbpsignature);
}
  
void* SbpSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2sbp_parallel();
    default: return nullptr;
  }
}

bool SbpSignature::operator==(const SbpSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SbpSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SbpSignature::operator<(const SbpSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SbpSignature::Clear() {
  if (data_) { data_.reset(); }
}
void SbpSignature::CopyFrom(const SbpSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SbpSignature& SbpSignature::operator=(const SbpSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2sbp_parallel
void SbpSignature::clear_bn_in_op2sbp_parallel() {
  return __SharedPtr__()->clear_bn_in_op2sbp_parallel();
}

const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_ & SbpSignature::bn_in_op2sbp_parallel() const {
  return __SharedConst__()->bn_in_op2sbp_parallel();
}

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_* SbpSignature::mutable_bn_in_op2sbp_parallel() {
  return __SharedPtr__()->mutable_bn_in_op2sbp_parallel();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> SbpSignature::shared_mutable_bn_in_op2sbp_parallel() {
  return mutable_bn_in_op2sbp_parallel()->__SharedMutable__();
}

::std::shared_ptr<SbpSignature> SbpSignature::__SharedMutable__() {
  return ::std::make_shared<SbpSignature>(__SharedPtr__());
}
ConstParallelDistribution::_ParallelDistribution_::_ParallelDistribution_() { Clear(); }
ConstParallelDistribution::_ParallelDistribution_::_ParallelDistribution_(const _ParallelDistribution_& other) { CopyFrom(other); }
ConstParallelDistribution::_ParallelDistribution_::_ParallelDistribution_(const ::oneflow::ParallelDistribution& proto_paralleldistribution) {
  InitFromProto(proto_paralleldistribution);
}
ConstParallelDistribution::_ParallelDistribution_::_ParallelDistribution_(_ParallelDistribution_&& other) = default;
ConstParallelDistribution::_ParallelDistribution_::~_ParallelDistribution_() = default;

void ConstParallelDistribution::_ParallelDistribution_::InitFromProto(const ::oneflow::ParallelDistribution& proto_paralleldistribution) {
  Clear();
  // repeated field: sbp_parallel
  if (!proto_paralleldistribution.sbp_parallel().empty()) {
    for (const ::oneflow::SbpParallel& elem : proto_paralleldistribution.sbp_parallel() ) {
      *mutable_sbp_parallel()->Add() = ::oneflow::cfg::SbpParallel(elem);
    }
  }
    
}

void ConstParallelDistribution::_ParallelDistribution_::ToProto(::oneflow::ParallelDistribution* proto_paralleldistribution) const {
  proto_paralleldistribution->Clear();
  // repeated field: sbp_parallel
  if (!sbp_parallel().empty()) {
    for (const ::oneflow::cfg::SbpParallel& elem : sbp_parallel() ) {
      ::oneflow::SbpParallel proto_sbp_parallel_elem;
      elem.ToProto(&proto_sbp_parallel_elem);
      *proto_paralleldistribution->mutable_sbp_parallel()->Add() = proto_sbp_parallel_elem;
    }
  }

}

::std::string ConstParallelDistribution::_ParallelDistribution_::DebugString() const {
  ::oneflow::ParallelDistribution proto_paralleldistribution;
  this->ToProto(&proto_paralleldistribution);
  return proto_paralleldistribution.DebugString();
}

void ConstParallelDistribution::_ParallelDistribution_::Clear() {
  clear_sbp_parallel();
}

void ConstParallelDistribution::_ParallelDistribution_::CopyFrom(const _ParallelDistribution_& other) {
  mutable_sbp_parallel()->CopyFrom(other.sbp_parallel());
}


// repeated field sbp_parallel
::std::size_t ConstParallelDistribution::_ParallelDistribution_::sbp_parallel_size() const {
  if (!sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
    return default_static_value->size();
  }
  return sbp_parallel_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& ConstParallelDistribution::_ParallelDistribution_::sbp_parallel() const {
  if (!sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
    return *(default_static_value.get());
  }
  return *(sbp_parallel_.get());
}
const ::oneflow::cfg::SbpParallel& ConstParallelDistribution::_ParallelDistribution_::sbp_parallel(::std::size_t index) const {
  if (!sbp_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
    return default_static_value->Get(index);
  }
  return sbp_parallel_->Get(index);
}
void ConstParallelDistribution::_ParallelDistribution_::clear_sbp_parallel() {
  if (!sbp_parallel_) {
    sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
  }
  return sbp_parallel_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_* ConstParallelDistribution::_ParallelDistribution_::mutable_sbp_parallel() {
  if (!sbp_parallel_) {
    sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
  }
  return  sbp_parallel_.get();
}
::oneflow::cfg::SbpParallel* ConstParallelDistribution::_ParallelDistribution_::mutable_sbp_parallel(::std::size_t index) {
  if (!sbp_parallel_) {
    sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
  }
  return  sbp_parallel_->Mutable(index);
}
::oneflow::cfg::SbpParallel* ConstParallelDistribution::_ParallelDistribution_::add_sbp_parallel() {
  if (!sbp_parallel_) {
    sbp_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>();
  }
  return sbp_parallel_->Add();
}


int ConstParallelDistribution::_ParallelDistribution_::compare(const _ParallelDistribution_& other) {
  if (!(sbp_parallel() == other.sbp_parallel())) {
    return sbp_parallel() < other.sbp_parallel() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelDistribution::_ParallelDistribution_::operator==(const _ParallelDistribution_& other) const {
  return true
    && sbp_parallel() == other.sbp_parallel()
  ;
}

std::size_t ConstParallelDistribution::_ParallelDistribution_::__CalcHash__() const {
  return 0
    ^ sbp_parallel().__CalcHash__()
  ;
}

bool ConstParallelDistribution::_ParallelDistribution_::operator<(const _ParallelDistribution_& other) const {
  return false
    || !(sbp_parallel() == other.sbp_parallel()) ? 
      sbp_parallel() < other.sbp_parallel() : false
  ;
}

using _ParallelDistribution_ =  ConstParallelDistribution::_ParallelDistribution_;
ConstParallelDistribution::ConstParallelDistribution(const ::std::shared_ptr<_ParallelDistribution_>& data): data_(data) {}
ConstParallelDistribution::ConstParallelDistribution(): data_(::std::make_shared<_ParallelDistribution_>()) {}
ConstParallelDistribution::ConstParallelDistribution(const ::oneflow::ParallelDistribution& proto_paralleldistribution) {
  BuildFromProto(proto_paralleldistribution);
}
ConstParallelDistribution::ConstParallelDistribution(const ConstParallelDistribution&) = default;
ConstParallelDistribution::ConstParallelDistribution(ConstParallelDistribution&&) noexcept = default;
ConstParallelDistribution::~ConstParallelDistribution() = default;

void ConstParallelDistribution::ToProto(PbMessage* proto_paralleldistribution) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelDistribution*>(proto_paralleldistribution));
}
  
::std::string ConstParallelDistribution::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelDistribution::__Empty__() const {
  return !data_;
}

int ConstParallelDistribution::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"sbp_parallel", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelDistribution::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelDistribution::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpParallel>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelDistribution::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &sbp_parallel();
    default: return nullptr;
  }
}

// repeated field sbp_parallel
::std::size_t ConstParallelDistribution::sbp_parallel_size() const {
  return __SharedPtrOrDefault__()->sbp_parallel_size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& ConstParallelDistribution::sbp_parallel() const {
  return __SharedPtrOrDefault__()->sbp_parallel();
}
const ::oneflow::cfg::SbpParallel& ConstParallelDistribution::sbp_parallel(::std::size_t index) const {
  return __SharedPtrOrDefault__()->sbp_parallel(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> ConstParallelDistribution::shared_const_sbp_parallel() const {
  return sbp_parallel().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> ConstParallelDistribution::shared_const_sbp_parallel(::std::size_t index) const {
  return sbp_parallel(index).__SharedConst__();
}

::std::shared_ptr<ConstParallelDistribution> ConstParallelDistribution::__SharedConst__() const {
  return ::std::make_shared<ConstParallelDistribution>(data_);
}
int64_t ConstParallelDistribution::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelDistribution::operator==(const ConstParallelDistribution& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelDistribution::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelDistribution::operator<(const ConstParallelDistribution& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelDistribution_>& ConstParallelDistribution::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelDistribution_> default_ptr = std::make_shared<_ParallelDistribution_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelDistribution_>& ConstParallelDistribution::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelDistribution_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelDistribution
void ConstParallelDistribution::BuildFromProto(const PbMessage& proto_paralleldistribution) {
  data_ = ::std::make_shared<_ParallelDistribution_>(dynamic_cast<const ::oneflow::ParallelDistribution&>(proto_paralleldistribution));
}

ParallelDistribution::ParallelDistribution(const ::std::shared_ptr<_ParallelDistribution_>& data)
  : ConstParallelDistribution(data) {}
ParallelDistribution::ParallelDistribution(const ParallelDistribution& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelDistribution> resize
ParallelDistribution::ParallelDistribution(ParallelDistribution&&) noexcept = default; 
ParallelDistribution::ParallelDistribution(const ::oneflow::ParallelDistribution& proto_paralleldistribution) {
  InitFromProto(proto_paralleldistribution);
}
ParallelDistribution::ParallelDistribution() = default;

ParallelDistribution::~ParallelDistribution() = default;

void ParallelDistribution::InitFromProto(const PbMessage& proto_paralleldistribution) {
  BuildFromProto(proto_paralleldistribution);
}
  
void* ParallelDistribution::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_sbp_parallel();
    default: return nullptr;
  }
}

bool ParallelDistribution::operator==(const ParallelDistribution& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelDistribution::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelDistribution::operator<(const ParallelDistribution& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelDistribution::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelDistribution::CopyFrom(const ParallelDistribution& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelDistribution& ParallelDistribution::operator=(const ParallelDistribution& other) {
  CopyFrom(other);
  return *this;
}

// repeated field sbp_parallel
void ParallelDistribution::clear_sbp_parallel() {
  return __SharedPtr__()->clear_sbp_parallel();
}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_* ParallelDistribution::mutable_sbp_parallel() {
  return __SharedPtr__()->mutable_sbp_parallel();
}
::oneflow::cfg::SbpParallel* ParallelDistribution::mutable_sbp_parallel(::std::size_t index) {
  return __SharedPtr__()->mutable_sbp_parallel(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> ParallelDistribution::shared_mutable_sbp_parallel() {
  return mutable_sbp_parallel()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::SbpParallel> ParallelDistribution::shared_mutable_sbp_parallel(::std::size_t index) {
  return mutable_sbp_parallel(index)->__SharedMutable__();
}
::oneflow::cfg::SbpParallel* ParallelDistribution::add_sbp_parallel() {
  return __SharedPtr__()->add_sbp_parallel();
}

::std::shared_ptr<ParallelDistribution> ParallelDistribution::__SharedMutable__() {
  return ::std::make_shared<ParallelDistribution>(__SharedPtr__());
}
ConstParallelDistributionSignature::_ParallelDistributionSignature_::_ParallelDistributionSignature_() { Clear(); }
ConstParallelDistributionSignature::_ParallelDistributionSignature_::_ParallelDistributionSignature_(const _ParallelDistributionSignature_& other) { CopyFrom(other); }
ConstParallelDistributionSignature::_ParallelDistributionSignature_::_ParallelDistributionSignature_(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature) {
  InitFromProto(proto_paralleldistributionsignature);
}
ConstParallelDistributionSignature::_ParallelDistributionSignature_::_ParallelDistributionSignature_(_ParallelDistributionSignature_&& other) = default;
ConstParallelDistributionSignature::_ParallelDistributionSignature_::~_ParallelDistributionSignature_() = default;

void ConstParallelDistributionSignature::_ParallelDistributionSignature_::InitFromProto(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature) {
  Clear();
  // map field : bn_in_op2parallel_distribution
  if (!proto_paralleldistributionsignature.bn_in_op2parallel_distribution().empty()) {
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_&  mut_bn_in_op2parallel_distribution = *mutable_bn_in_op2parallel_distribution();
    for (const auto& pair : proto_paralleldistributionsignature.bn_in_op2parallel_distribution()) {
      mut_bn_in_op2parallel_distribution[pair.first] = ::oneflow::cfg::ParallelDistribution(pair.second);
    }
  }
    
}

void ConstParallelDistributionSignature::_ParallelDistributionSignature_::ToProto(::oneflow::ParallelDistributionSignature* proto_paralleldistributionsignature) const {
  proto_paralleldistributionsignature->Clear();
  // map field : bn_in_op2parallel_distribution
  if (!bn_in_op2parallel_distribution().empty()) {
    auto& mut_bn_in_op2parallel_distribution = *(proto_paralleldistributionsignature->mutable_bn_in_op2parallel_distribution());
    for (const auto& pair : bn_in_op2parallel_distribution()) {
      ::oneflow::ParallelDistribution proto_bn_in_op2parallel_distribution_value;
      pair.second.ToProto(&proto_bn_in_op2parallel_distribution_value);
      mut_bn_in_op2parallel_distribution[pair.first] = proto_bn_in_op2parallel_distribution_value;
    }
  }

}

::std::string ConstParallelDistributionSignature::_ParallelDistributionSignature_::DebugString() const {
  ::oneflow::ParallelDistributionSignature proto_paralleldistributionsignature;
  this->ToProto(&proto_paralleldistributionsignature);
  return proto_paralleldistributionsignature.DebugString();
}

void ConstParallelDistributionSignature::_ParallelDistributionSignature_::Clear() {
  clear_bn_in_op2parallel_distribution();
}

void ConstParallelDistributionSignature::_ParallelDistributionSignature_::CopyFrom(const _ParallelDistributionSignature_& other) {
  mutable_bn_in_op2parallel_distribution()->CopyFrom(other.bn_in_op2parallel_distribution());
}


::std::size_t ConstParallelDistributionSignature::_ParallelDistributionSignature_::bn_in_op2parallel_distribution_size() const {
  if (!bn_in_op2parallel_distribution_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>();
    return default_static_value->size();
  }
  return bn_in_op2parallel_distribution_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& ConstParallelDistributionSignature::_ParallelDistributionSignature_::bn_in_op2parallel_distribution() const {
  if (!bn_in_op2parallel_distribution_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2parallel_distribution_.get());
}

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ * ConstParallelDistributionSignature::_ParallelDistributionSignature_::mutable_bn_in_op2parallel_distribution() {
  if (!bn_in_op2parallel_distribution_) {
    bn_in_op2parallel_distribution_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>();
  }
  return bn_in_op2parallel_distribution_.get();
}

const ::oneflow::cfg::ParallelDistribution& ConstParallelDistributionSignature::_ParallelDistributionSignature_::bn_in_op2parallel_distribution(::std::string key) const {
  if (!bn_in_op2parallel_distribution_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>();
    return default_static_value->at(key);
  }
  return bn_in_op2parallel_distribution_->at(key);
}

void ConstParallelDistributionSignature::_ParallelDistributionSignature_::clear_bn_in_op2parallel_distribution() {
  if (!bn_in_op2parallel_distribution_) {
    bn_in_op2parallel_distribution_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>();
  }
  return bn_in_op2parallel_distribution_->Clear();
}



int ConstParallelDistributionSignature::_ParallelDistributionSignature_::compare(const _ParallelDistributionSignature_& other) {
  if (!(bn_in_op2parallel_distribution() == other.bn_in_op2parallel_distribution())) {
    return bn_in_op2parallel_distribution() < other.bn_in_op2parallel_distribution() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelDistributionSignature::_ParallelDistributionSignature_::operator==(const _ParallelDistributionSignature_& other) const {
  return true
    && bn_in_op2parallel_distribution() == other.bn_in_op2parallel_distribution()
  ;
}

std::size_t ConstParallelDistributionSignature::_ParallelDistributionSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2parallel_distribution().__CalcHash__()
  ;
}

bool ConstParallelDistributionSignature::_ParallelDistributionSignature_::operator<(const _ParallelDistributionSignature_& other) const {
  return false
    || !(bn_in_op2parallel_distribution() == other.bn_in_op2parallel_distribution()) ? 
      bn_in_op2parallel_distribution() < other.bn_in_op2parallel_distribution() : false
  ;
}

using _ParallelDistributionSignature_ =  ConstParallelDistributionSignature::_ParallelDistributionSignature_;
ConstParallelDistributionSignature::ConstParallelDistributionSignature(const ::std::shared_ptr<_ParallelDistributionSignature_>& data): data_(data) {}
ConstParallelDistributionSignature::ConstParallelDistributionSignature(): data_(::std::make_shared<_ParallelDistributionSignature_>()) {}
ConstParallelDistributionSignature::ConstParallelDistributionSignature(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature) {
  BuildFromProto(proto_paralleldistributionsignature);
}
ConstParallelDistributionSignature::ConstParallelDistributionSignature(const ConstParallelDistributionSignature&) = default;
ConstParallelDistributionSignature::ConstParallelDistributionSignature(ConstParallelDistributionSignature&&) noexcept = default;
ConstParallelDistributionSignature::~ConstParallelDistributionSignature() = default;

void ConstParallelDistributionSignature::ToProto(PbMessage* proto_paralleldistributionsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelDistributionSignature*>(proto_paralleldistributionsignature));
}
  
::std::string ConstParallelDistributionSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelDistributionSignature::__Empty__() const {
  return !data_;
}

int ConstParallelDistributionSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2parallel_distribution", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelDistributionSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelDistributionSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelDistribution>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelDistributionSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2parallel_distribution();
    default: return nullptr;
  }
}

// map field bn_in_op2parallel_distribution
::std::size_t ConstParallelDistributionSignature::bn_in_op2parallel_distribution_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_distribution_size();
}

const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& ConstParallelDistributionSignature::bn_in_op2parallel_distribution() const {
  return __SharedPtrOrDefault__()->bn_in_op2parallel_distribution();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> ConstParallelDistributionSignature::shared_const_bn_in_op2parallel_distribution() const {
  return bn_in_op2parallel_distribution().__SharedConst__();
}

::std::shared_ptr<ConstParallelDistributionSignature> ConstParallelDistributionSignature::__SharedConst__() const {
  return ::std::make_shared<ConstParallelDistributionSignature>(data_);
}
int64_t ConstParallelDistributionSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelDistributionSignature::operator==(const ConstParallelDistributionSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelDistributionSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelDistributionSignature::operator<(const ConstParallelDistributionSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelDistributionSignature_>& ConstParallelDistributionSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelDistributionSignature_> default_ptr = std::make_shared<_ParallelDistributionSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelDistributionSignature_>& ConstParallelDistributionSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelDistributionSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelDistributionSignature
void ConstParallelDistributionSignature::BuildFromProto(const PbMessage& proto_paralleldistributionsignature) {
  data_ = ::std::make_shared<_ParallelDistributionSignature_>(dynamic_cast<const ::oneflow::ParallelDistributionSignature&>(proto_paralleldistributionsignature));
}

ParallelDistributionSignature::ParallelDistributionSignature(const ::std::shared_ptr<_ParallelDistributionSignature_>& data)
  : ConstParallelDistributionSignature(data) {}
ParallelDistributionSignature::ParallelDistributionSignature(const ParallelDistributionSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelDistributionSignature> resize
ParallelDistributionSignature::ParallelDistributionSignature(ParallelDistributionSignature&&) noexcept = default; 
ParallelDistributionSignature::ParallelDistributionSignature(const ::oneflow::ParallelDistributionSignature& proto_paralleldistributionsignature) {
  InitFromProto(proto_paralleldistributionsignature);
}
ParallelDistributionSignature::ParallelDistributionSignature() = default;

ParallelDistributionSignature::~ParallelDistributionSignature() = default;

void ParallelDistributionSignature::InitFromProto(const PbMessage& proto_paralleldistributionsignature) {
  BuildFromProto(proto_paralleldistributionsignature);
}
  
void* ParallelDistributionSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2parallel_distribution();
    default: return nullptr;
  }
}

bool ParallelDistributionSignature::operator==(const ParallelDistributionSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelDistributionSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelDistributionSignature::operator<(const ParallelDistributionSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelDistributionSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelDistributionSignature::CopyFrom(const ParallelDistributionSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelDistributionSignature& ParallelDistributionSignature::operator=(const ParallelDistributionSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2parallel_distribution
void ParallelDistributionSignature::clear_bn_in_op2parallel_distribution() {
  return __SharedPtr__()->clear_bn_in_op2parallel_distribution();
}

const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_ & ParallelDistributionSignature::bn_in_op2parallel_distribution() const {
  return __SharedConst__()->bn_in_op2parallel_distribution();
}

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_* ParallelDistributionSignature::mutable_bn_in_op2parallel_distribution() {
  return __SharedPtr__()->mutable_bn_in_op2parallel_distribution();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> ParallelDistributionSignature::shared_mutable_bn_in_op2parallel_distribution() {
  return mutable_bn_in_op2parallel_distribution()->__SharedMutable__();
}

::std::shared_ptr<ParallelDistributionSignature> ParallelDistributionSignature::__SharedMutable__() {
  return ::std::make_shared<ParallelDistributionSignature>(__SharedPtr__());
}
ConstSbpSignatureList::_SbpSignatureList_::_SbpSignatureList_() { Clear(); }
ConstSbpSignatureList::_SbpSignatureList_::_SbpSignatureList_(const _SbpSignatureList_& other) { CopyFrom(other); }
ConstSbpSignatureList::_SbpSignatureList_::_SbpSignatureList_(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist) {
  InitFromProto(proto_sbpsignaturelist);
}
ConstSbpSignatureList::_SbpSignatureList_::_SbpSignatureList_(_SbpSignatureList_&& other) = default;
ConstSbpSignatureList::_SbpSignatureList_::~_SbpSignatureList_() = default;

void ConstSbpSignatureList::_SbpSignatureList_::InitFromProto(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist) {
  Clear();
  // repeated field: sbp_signature
  if (!proto_sbpsignaturelist.sbp_signature().empty()) {
    for (const ::oneflow::SbpSignature& elem : proto_sbpsignaturelist.sbp_signature() ) {
      *mutable_sbp_signature()->Add() = ::oneflow::cfg::SbpSignature(elem);
    }
  }
    
}

void ConstSbpSignatureList::_SbpSignatureList_::ToProto(::oneflow::SbpSignatureList* proto_sbpsignaturelist) const {
  proto_sbpsignaturelist->Clear();
  // repeated field: sbp_signature
  if (!sbp_signature().empty()) {
    for (const ::oneflow::cfg::SbpSignature& elem : sbp_signature() ) {
      ::oneflow::SbpSignature proto_sbp_signature_elem;
      elem.ToProto(&proto_sbp_signature_elem);
      *proto_sbpsignaturelist->mutable_sbp_signature()->Add() = proto_sbp_signature_elem;
    }
  }

}

::std::string ConstSbpSignatureList::_SbpSignatureList_::DebugString() const {
  ::oneflow::SbpSignatureList proto_sbpsignaturelist;
  this->ToProto(&proto_sbpsignaturelist);
  return proto_sbpsignaturelist.DebugString();
}

void ConstSbpSignatureList::_SbpSignatureList_::Clear() {
  clear_sbp_signature();
}

void ConstSbpSignatureList::_SbpSignatureList_::CopyFrom(const _SbpSignatureList_& other) {
  mutable_sbp_signature()->CopyFrom(other.sbp_signature());
}


// repeated field sbp_signature
::std::size_t ConstSbpSignatureList::_SbpSignatureList_::sbp_signature_size() const {
  if (!sbp_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
    return default_static_value->size();
  }
  return sbp_signature_->size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& ConstSbpSignatureList::_SbpSignatureList_::sbp_signature() const {
  if (!sbp_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
    return *(default_static_value.get());
  }
  return *(sbp_signature_.get());
}
const ::oneflow::cfg::SbpSignature& ConstSbpSignatureList::_SbpSignatureList_::sbp_signature(::std::size_t index) const {
  if (!sbp_signature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
    return default_static_value->Get(index);
  }
  return sbp_signature_->Get(index);
}
void ConstSbpSignatureList::_SbpSignatureList_::clear_sbp_signature() {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
  }
  return sbp_signature_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_* ConstSbpSignatureList::_SbpSignatureList_::mutable_sbp_signature() {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
  }
  return  sbp_signature_.get();
}
::oneflow::cfg::SbpSignature* ConstSbpSignatureList::_SbpSignatureList_::mutable_sbp_signature(::std::size_t index) {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
  }
  return  sbp_signature_->Mutable(index);
}
::oneflow::cfg::SbpSignature* ConstSbpSignatureList::_SbpSignatureList_::add_sbp_signature() {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>();
  }
  return sbp_signature_->Add();
}


int ConstSbpSignatureList::_SbpSignatureList_::compare(const _SbpSignatureList_& other) {
  if (!(sbp_signature() == other.sbp_signature())) {
    return sbp_signature() < other.sbp_signature() ? -1 : 1;
  }
  return 0;
}

bool ConstSbpSignatureList::_SbpSignatureList_::operator==(const _SbpSignatureList_& other) const {
  return true
    && sbp_signature() == other.sbp_signature()
  ;
}

std::size_t ConstSbpSignatureList::_SbpSignatureList_::__CalcHash__() const {
  return 0
    ^ sbp_signature().__CalcHash__()
  ;
}

bool ConstSbpSignatureList::_SbpSignatureList_::operator<(const _SbpSignatureList_& other) const {
  return false
    || !(sbp_signature() == other.sbp_signature()) ? 
      sbp_signature() < other.sbp_signature() : false
  ;
}

using _SbpSignatureList_ =  ConstSbpSignatureList::_SbpSignatureList_;
ConstSbpSignatureList::ConstSbpSignatureList(const ::std::shared_ptr<_SbpSignatureList_>& data): data_(data) {}
ConstSbpSignatureList::ConstSbpSignatureList(): data_(::std::make_shared<_SbpSignatureList_>()) {}
ConstSbpSignatureList::ConstSbpSignatureList(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist) {
  BuildFromProto(proto_sbpsignaturelist);
}
ConstSbpSignatureList::ConstSbpSignatureList(const ConstSbpSignatureList&) = default;
ConstSbpSignatureList::ConstSbpSignatureList(ConstSbpSignatureList&&) noexcept = default;
ConstSbpSignatureList::~ConstSbpSignatureList() = default;

void ConstSbpSignatureList::ToProto(PbMessage* proto_sbpsignaturelist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::SbpSignatureList*>(proto_sbpsignaturelist));
}
  
::std::string ConstSbpSignatureList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSbpSignatureList::__Empty__() const {
  return !data_;
}

int ConstSbpSignatureList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"sbp_signature", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstSbpSignatureList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstSbpSignatureList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpSignature>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSbpSignatureList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &sbp_signature();
    default: return nullptr;
  }
}

// repeated field sbp_signature
::std::size_t ConstSbpSignatureList::sbp_signature_size() const {
  return __SharedPtrOrDefault__()->sbp_signature_size();
}
const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& ConstSbpSignatureList::sbp_signature() const {
  return __SharedPtrOrDefault__()->sbp_signature();
}
const ::oneflow::cfg::SbpSignature& ConstSbpSignatureList::sbp_signature(::std::size_t index) const {
  return __SharedPtrOrDefault__()->sbp_signature(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> ConstSbpSignatureList::shared_const_sbp_signature() const {
  return sbp_signature().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> ConstSbpSignatureList::shared_const_sbp_signature(::std::size_t index) const {
  return sbp_signature(index).__SharedConst__();
}

::std::shared_ptr<ConstSbpSignatureList> ConstSbpSignatureList::__SharedConst__() const {
  return ::std::make_shared<ConstSbpSignatureList>(data_);
}
int64_t ConstSbpSignatureList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSbpSignatureList::operator==(const ConstSbpSignatureList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSbpSignatureList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSbpSignatureList::operator<(const ConstSbpSignatureList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SbpSignatureList_>& ConstSbpSignatureList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SbpSignatureList_> default_ptr = std::make_shared<_SbpSignatureList_>();
  return default_ptr;
}
const ::std::shared_ptr<_SbpSignatureList_>& ConstSbpSignatureList::__SharedPtr__() {
  if (!data_) { data_.reset(new _SbpSignatureList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSbpSignatureList
void ConstSbpSignatureList::BuildFromProto(const PbMessage& proto_sbpsignaturelist) {
  data_ = ::std::make_shared<_SbpSignatureList_>(dynamic_cast<const ::oneflow::SbpSignatureList&>(proto_sbpsignaturelist));
}

SbpSignatureList::SbpSignatureList(const ::std::shared_ptr<_SbpSignatureList_>& data)
  : ConstSbpSignatureList(data) {}
SbpSignatureList::SbpSignatureList(const SbpSignatureList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SbpSignatureList> resize
SbpSignatureList::SbpSignatureList(SbpSignatureList&&) noexcept = default; 
SbpSignatureList::SbpSignatureList(const ::oneflow::SbpSignatureList& proto_sbpsignaturelist) {
  InitFromProto(proto_sbpsignaturelist);
}
SbpSignatureList::SbpSignatureList() = default;

SbpSignatureList::~SbpSignatureList() = default;

void SbpSignatureList::InitFromProto(const PbMessage& proto_sbpsignaturelist) {
  BuildFromProto(proto_sbpsignaturelist);
}
  
void* SbpSignatureList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_sbp_signature();
    default: return nullptr;
  }
}

bool SbpSignatureList::operator==(const SbpSignatureList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SbpSignatureList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SbpSignatureList::operator<(const SbpSignatureList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SbpSignatureList::Clear() {
  if (data_) { data_.reset(); }
}
void SbpSignatureList::CopyFrom(const SbpSignatureList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SbpSignatureList& SbpSignatureList::operator=(const SbpSignatureList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field sbp_signature
void SbpSignatureList::clear_sbp_signature() {
  return __SharedPtr__()->clear_sbp_signature();
}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_* SbpSignatureList::mutable_sbp_signature() {
  return __SharedPtr__()->mutable_sbp_signature();
}
::oneflow::cfg::SbpSignature* SbpSignatureList::mutable_sbp_signature(::std::size_t index) {
  return __SharedPtr__()->mutable_sbp_signature(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> SbpSignatureList::shared_mutable_sbp_signature() {
  return mutable_sbp_signature()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::SbpSignature> SbpSignatureList::shared_mutable_sbp_signature(::std::size_t index) {
  return mutable_sbp_signature(index)->__SharedMutable__();
}
::oneflow::cfg::SbpSignature* SbpSignatureList::add_sbp_signature() {
  return __SharedPtr__()->add_sbp_signature();
}

::std::shared_ptr<SbpSignatureList> SbpSignatureList::__SharedMutable__() {
  return ::std::make_shared<SbpSignatureList>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpParallel>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpParallel>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::SbpParallel>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::SbpParallel& Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstSbpParallel> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ConstSbpParallel> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ConstSbpParallel> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpParallel>>& data): Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_(data) {}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_() = default;
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_() = default;

void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpParallel>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpParallel>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::SbpParallel>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::SbpParallel> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ::oneflow::cfg::SbpParallel> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_, ::oneflow::cfg::SbpParallel> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_SbpParallel_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpParallel>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpParallel>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::SbpParallel>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpParallel>>& data): Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_(data) {}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_() = default;
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_() = default;

void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpParallel>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpParallel>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::SbpParallel>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::SbpParallel> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::SbpParallel> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpParallel_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelDistribution>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelDistribution>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ParallelDistribution>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::ParallelDistribution& Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstParallelDistribution> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ConstParallelDistribution> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ConstParallelDistribution> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::ParallelDistribution>>& data): Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_(data) {}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_() = default;
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_() = default;

void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelDistribution>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::ParallelDistribution>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::ParallelDistribution>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::ParallelDistribution> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ::oneflow::cfg::ParallelDistribution> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_, ::oneflow::cfg::ParallelDistribution> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__MapField___std__string_ParallelDistribution_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpSignature>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpSignature>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_() = default;
Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::~Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::SbpSignature>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::SbpSignature>>& data): Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_(data) {}
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_() = default;
_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::~_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_() = default;

void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpSignature>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::SbpSignature>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::operator==(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::SbpSignature>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::operator<(const _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::SbpSignature> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::SbpSignature> _CFG_ONEFLOW_CORE_JOB_SBP_PARALLEL_CFG_H__RepeatedField_SbpSignature_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
