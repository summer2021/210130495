#include "oneflow/core/job/initializer_conf.cfg.h"
#include "oneflow/core/job/initializer_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstConstantInitializerConf::_ConstantInitializerConf_::_ConstantInitializerConf_() { Clear(); }
ConstConstantInitializerConf::_ConstantInitializerConf_::_ConstantInitializerConf_(const _ConstantInitializerConf_& other) { CopyFrom(other); }
ConstConstantInitializerConf::_ConstantInitializerConf_::_ConstantInitializerConf_(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf) {
  InitFromProto(proto_constantinitializerconf);
}
ConstConstantInitializerConf::_ConstantInitializerConf_::_ConstantInitializerConf_(_ConstantInitializerConf_&& other) = default;
ConstConstantInitializerConf::_ConstantInitializerConf_::~_ConstantInitializerConf_() = default;

void ConstConstantInitializerConf::_ConstantInitializerConf_::InitFromProto(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf) {
  Clear();
  // required_or_optional field: value
  if (proto_constantinitializerconf.has_value()) {
    set_value(proto_constantinitializerconf.value());
  }
    
}

void ConstConstantInitializerConf::_ConstantInitializerConf_::ToProto(::oneflow::ConstantInitializerConf* proto_constantinitializerconf) const {
  proto_constantinitializerconf->Clear();
  // required_or_optional field: value
  if (this->has_value()) {
    proto_constantinitializerconf->set_value(value());
    }

}

::std::string ConstConstantInitializerConf::_ConstantInitializerConf_::DebugString() const {
  ::oneflow::ConstantInitializerConf proto_constantinitializerconf;
  this->ToProto(&proto_constantinitializerconf);
  return proto_constantinitializerconf.DebugString();
}

void ConstConstantInitializerConf::_ConstantInitializerConf_::Clear() {
  clear_value();
}

void ConstConstantInitializerConf::_ConstantInitializerConf_::CopyFrom(const _ConstantInitializerConf_& other) {
  if (other.has_value()) {
    set_value(other.value());
  } else {
    clear_value();
  }
}


// optional field value
bool ConstConstantInitializerConf::_ConstantInitializerConf_::has_value() const {
  return has_value_;
}
const float& ConstConstantInitializerConf::_ConstantInitializerConf_::value() const {
  if (has_value_) { return value_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstConstantInitializerConf::_ConstantInitializerConf_::clear_value() {
  has_value_ = false;
}
void ConstConstantInitializerConf::_ConstantInitializerConf_::set_value(const float& value) {
  value_ = value;
  has_value_ = true;
}
float* ConstConstantInitializerConf::_ConstantInitializerConf_::mutable_value() {
  has_value_ = true;
  return &value_;
}


int ConstConstantInitializerConf::_ConstantInitializerConf_::compare(const _ConstantInitializerConf_& other) {
  if (!(has_value() == other.has_value())) {
    return has_value() < other.has_value() ? -1 : 1;
  } else if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstConstantInitializerConf::_ConstantInitializerConf_::operator==(const _ConstantInitializerConf_& other) const {
  return true
    && has_value() == other.has_value() 
    && value() == other.value()
  ;
}

std::size_t ConstConstantInitializerConf::_ConstantInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_value() ? std::hash<float>()(value()) : 0)
  ;
}

bool ConstConstantInitializerConf::_ConstantInitializerConf_::operator<(const _ConstantInitializerConf_& other) const {
  return false
    || !(has_value() == other.has_value()) ? 
      has_value() < other.has_value() : false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _ConstantInitializerConf_ =  ConstConstantInitializerConf::_ConstantInitializerConf_;
ConstConstantInitializerConf::ConstConstantInitializerConf(const ::std::shared_ptr<_ConstantInitializerConf_>& data): data_(data) {}
ConstConstantInitializerConf::ConstConstantInitializerConf(): data_(::std::make_shared<_ConstantInitializerConf_>()) {}
ConstConstantInitializerConf::ConstConstantInitializerConf(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf) {
  BuildFromProto(proto_constantinitializerconf);
}
ConstConstantInitializerConf::ConstConstantInitializerConf(const ConstConstantInitializerConf&) = default;
ConstConstantInitializerConf::ConstConstantInitializerConf(ConstConstantInitializerConf&&) noexcept = default;
ConstConstantInitializerConf::~ConstConstantInitializerConf() = default;

void ConstConstantInitializerConf::ToProto(PbMessage* proto_constantinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ConstantInitializerConf*>(proto_constantinitializerconf));
}
  
::std::string ConstConstantInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstConstantInitializerConf::__Empty__() const {
  return !data_;
}

int ConstConstantInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstConstantInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstConstantInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstConstantInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// required or optional field value
bool ConstConstantInitializerConf::has_value() const {
  return __SharedPtrOrDefault__()->has_value();
}
const float& ConstConstantInitializerConf::value() const {
  return __SharedPtrOrDefault__()->value();
}
// used by pybind11 only

::std::shared_ptr<ConstConstantInitializerConf> ConstConstantInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstConstantInitializerConf>(data_);
}
int64_t ConstConstantInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstConstantInitializerConf::operator==(const ConstConstantInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstConstantInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstConstantInitializerConf::operator<(const ConstConstantInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ConstantInitializerConf_>& ConstConstantInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ConstantInitializerConf_> default_ptr = std::make_shared<_ConstantInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ConstantInitializerConf_>& ConstConstantInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ConstantInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstConstantInitializerConf
void ConstConstantInitializerConf::BuildFromProto(const PbMessage& proto_constantinitializerconf) {
  data_ = ::std::make_shared<_ConstantInitializerConf_>(dynamic_cast<const ::oneflow::ConstantInitializerConf&>(proto_constantinitializerconf));
}

ConstantInitializerConf::ConstantInitializerConf(const ::std::shared_ptr<_ConstantInitializerConf_>& data)
  : ConstConstantInitializerConf(data) {}
ConstantInitializerConf::ConstantInitializerConf(const ConstantInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ConstantInitializerConf> resize
ConstantInitializerConf::ConstantInitializerConf(ConstantInitializerConf&&) noexcept = default; 
ConstantInitializerConf::ConstantInitializerConf(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf) {
  InitFromProto(proto_constantinitializerconf);
}
ConstantInitializerConf::ConstantInitializerConf() = default;

ConstantInitializerConf::~ConstantInitializerConf() = default;

void ConstantInitializerConf::InitFromProto(const PbMessage& proto_constantinitializerconf) {
  BuildFromProto(proto_constantinitializerconf);
}
  
void* ConstantInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool ConstantInitializerConf::operator==(const ConstantInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstantInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstantInitializerConf::operator<(const ConstantInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ConstantInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void ConstantInitializerConf::CopyFrom(const ConstantInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ConstantInitializerConf& ConstantInitializerConf::operator=(const ConstantInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field value
void ConstantInitializerConf::clear_value() {
  return __SharedPtr__()->clear_value();
}
void ConstantInitializerConf::set_value(const float& value) {
  return __SharedPtr__()->set_value(value);
}
float* ConstantInitializerConf::mutable_value() {
  return  __SharedPtr__()->mutable_value();
}

::std::shared_ptr<ConstantInitializerConf> ConstantInitializerConf::__SharedMutable__() {
  return ::std::make_shared<ConstantInitializerConf>(__SharedPtr__());
}
ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::_ConstantIntInitializerConf_() { Clear(); }
ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::_ConstantIntInitializerConf_(const _ConstantIntInitializerConf_& other) { CopyFrom(other); }
ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::_ConstantIntInitializerConf_(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf) {
  InitFromProto(proto_constantintinitializerconf);
}
ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::_ConstantIntInitializerConf_(_ConstantIntInitializerConf_&& other) = default;
ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::~_ConstantIntInitializerConf_() = default;

void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::InitFromProto(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf) {
  Clear();
  // required_or_optional field: value
  if (proto_constantintinitializerconf.has_value()) {
    set_value(proto_constantintinitializerconf.value());
  }
    
}

void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::ToProto(::oneflow::ConstantIntInitializerConf* proto_constantintinitializerconf) const {
  proto_constantintinitializerconf->Clear();
  // required_or_optional field: value
  if (this->has_value()) {
    proto_constantintinitializerconf->set_value(value());
    }

}

::std::string ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::DebugString() const {
  ::oneflow::ConstantIntInitializerConf proto_constantintinitializerconf;
  this->ToProto(&proto_constantintinitializerconf);
  return proto_constantintinitializerconf.DebugString();
}

void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::Clear() {
  clear_value();
}

void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::CopyFrom(const _ConstantIntInitializerConf_& other) {
  if (other.has_value()) {
    set_value(other.value());
  } else {
    clear_value();
  }
}


// optional field value
bool ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::has_value() const {
  return has_value_;
}
const int64_t& ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::value() const {
  if (has_value_) { return value_; }
  static const int64_t default_static_value =
    int64_t(0);
  return default_static_value;
}
void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::clear_value() {
  has_value_ = false;
}
void ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::set_value(const int64_t& value) {
  value_ = value;
  has_value_ = true;
}
int64_t* ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::mutable_value() {
  has_value_ = true;
  return &value_;
}


int ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::compare(const _ConstantIntInitializerConf_& other) {
  if (!(has_value() == other.has_value())) {
    return has_value() < other.has_value() ? -1 : 1;
  } else if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::operator==(const _ConstantIntInitializerConf_& other) const {
  return true
    && has_value() == other.has_value() 
    && value() == other.value()
  ;
}

std::size_t ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_value() ? std::hash<int64_t>()(value()) : 0)
  ;
}

bool ConstConstantIntInitializerConf::_ConstantIntInitializerConf_::operator<(const _ConstantIntInitializerConf_& other) const {
  return false
    || !(has_value() == other.has_value()) ? 
      has_value() < other.has_value() : false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _ConstantIntInitializerConf_ =  ConstConstantIntInitializerConf::_ConstantIntInitializerConf_;
ConstConstantIntInitializerConf::ConstConstantIntInitializerConf(const ::std::shared_ptr<_ConstantIntInitializerConf_>& data): data_(data) {}
ConstConstantIntInitializerConf::ConstConstantIntInitializerConf(): data_(::std::make_shared<_ConstantIntInitializerConf_>()) {}
ConstConstantIntInitializerConf::ConstConstantIntInitializerConf(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf) {
  BuildFromProto(proto_constantintinitializerconf);
}
ConstConstantIntInitializerConf::ConstConstantIntInitializerConf(const ConstConstantIntInitializerConf&) = default;
ConstConstantIntInitializerConf::ConstConstantIntInitializerConf(ConstConstantIntInitializerConf&&) noexcept = default;
ConstConstantIntInitializerConf::~ConstConstantIntInitializerConf() = default;

void ConstConstantIntInitializerConf::ToProto(PbMessage* proto_constantintinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ConstantIntInitializerConf*>(proto_constantintinitializerconf));
}
  
::std::string ConstConstantIntInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstConstantIntInitializerConf::__Empty__() const {
  return !data_;
}

int ConstConstantIntInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstConstantIntInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstConstantIntInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstConstantIntInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// required or optional field value
bool ConstConstantIntInitializerConf::has_value() const {
  return __SharedPtrOrDefault__()->has_value();
}
const int64_t& ConstConstantIntInitializerConf::value() const {
  return __SharedPtrOrDefault__()->value();
}
// used by pybind11 only

::std::shared_ptr<ConstConstantIntInitializerConf> ConstConstantIntInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstConstantIntInitializerConf>(data_);
}
int64_t ConstConstantIntInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstConstantIntInitializerConf::operator==(const ConstConstantIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstConstantIntInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstConstantIntInitializerConf::operator<(const ConstConstantIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ConstantIntInitializerConf_>& ConstConstantIntInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ConstantIntInitializerConf_> default_ptr = std::make_shared<_ConstantIntInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ConstantIntInitializerConf_>& ConstConstantIntInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ConstantIntInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstConstantIntInitializerConf
void ConstConstantIntInitializerConf::BuildFromProto(const PbMessage& proto_constantintinitializerconf) {
  data_ = ::std::make_shared<_ConstantIntInitializerConf_>(dynamic_cast<const ::oneflow::ConstantIntInitializerConf&>(proto_constantintinitializerconf));
}

ConstantIntInitializerConf::ConstantIntInitializerConf(const ::std::shared_ptr<_ConstantIntInitializerConf_>& data)
  : ConstConstantIntInitializerConf(data) {}
ConstantIntInitializerConf::ConstantIntInitializerConf(const ConstantIntInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ConstantIntInitializerConf> resize
ConstantIntInitializerConf::ConstantIntInitializerConf(ConstantIntInitializerConf&&) noexcept = default; 
ConstantIntInitializerConf::ConstantIntInitializerConf(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf) {
  InitFromProto(proto_constantintinitializerconf);
}
ConstantIntInitializerConf::ConstantIntInitializerConf() = default;

ConstantIntInitializerConf::~ConstantIntInitializerConf() = default;

void ConstantIntInitializerConf::InitFromProto(const PbMessage& proto_constantintinitializerconf) {
  BuildFromProto(proto_constantintinitializerconf);
}
  
void* ConstantIntInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool ConstantIntInitializerConf::operator==(const ConstantIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstantIntInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstantIntInitializerConf::operator<(const ConstantIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ConstantIntInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void ConstantIntInitializerConf::CopyFrom(const ConstantIntInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ConstantIntInitializerConf& ConstantIntInitializerConf::operator=(const ConstantIntInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field value
void ConstantIntInitializerConf::clear_value() {
  return __SharedPtr__()->clear_value();
}
void ConstantIntInitializerConf::set_value(const int64_t& value) {
  return __SharedPtr__()->set_value(value);
}
int64_t* ConstantIntInitializerConf::mutable_value() {
  return  __SharedPtr__()->mutable_value();
}

::std::shared_ptr<ConstantIntInitializerConf> ConstantIntInitializerConf::__SharedMutable__() {
  return ::std::make_shared<ConstantIntInitializerConf>(__SharedPtr__());
}
ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::_RandomUniformInitializerConf_() { Clear(); }
ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::_RandomUniformInitializerConf_(const _RandomUniformInitializerConf_& other) { CopyFrom(other); }
ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::_RandomUniformInitializerConf_(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf) {
  InitFromProto(proto_randomuniforminitializerconf);
}
ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::_RandomUniformInitializerConf_(_RandomUniformInitializerConf_&& other) = default;
ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::~_RandomUniformInitializerConf_() = default;

void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::InitFromProto(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf) {
  Clear();
  // required_or_optional field: min
  if (proto_randomuniforminitializerconf.has_min()) {
    set_min(proto_randomuniforminitializerconf.min());
  }
  // required_or_optional field: max
  if (proto_randomuniforminitializerconf.has_max()) {
    set_max(proto_randomuniforminitializerconf.max());
  }
    
}

void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::ToProto(::oneflow::RandomUniformInitializerConf* proto_randomuniforminitializerconf) const {
  proto_randomuniforminitializerconf->Clear();
  // required_or_optional field: min
  if (this->has_min()) {
    proto_randomuniforminitializerconf->set_min(min());
    }
  // required_or_optional field: max
  if (this->has_max()) {
    proto_randomuniforminitializerconf->set_max(max());
    }

}

::std::string ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::DebugString() const {
  ::oneflow::RandomUniformInitializerConf proto_randomuniforminitializerconf;
  this->ToProto(&proto_randomuniforminitializerconf);
  return proto_randomuniforminitializerconf.DebugString();
}

void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::Clear() {
  clear_min();
  clear_max();
}

void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::CopyFrom(const _RandomUniformInitializerConf_& other) {
  if (other.has_min()) {
    set_min(other.min());
  } else {
    clear_min();
  }
  if (other.has_max()) {
    set_max(other.max());
  } else {
    clear_max();
  }
}


// optional field min
bool ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::has_min() const {
  return has_min_;
}
const float& ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::min() const {
  if (has_min_) { return min_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::clear_min() {
  has_min_ = false;
}
void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::set_min(const float& value) {
  min_ = value;
  has_min_ = true;
}
float* ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::mutable_min() {
  has_min_ = true;
  return &min_;
}

// optional field max
bool ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::has_max() const {
  return has_max_;
}
const float& ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::max() const {
  if (has_max_) { return max_; }
  static const float default_static_value =
    float(1.0);
  return default_static_value;
}
void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::clear_max() {
  has_max_ = false;
}
void ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::set_max(const float& value) {
  max_ = value;
  has_max_ = true;
}
float* ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::mutable_max() {
  has_max_ = true;
  return &max_;
}


int ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::compare(const _RandomUniformInitializerConf_& other) {
  if (!(has_min() == other.has_min())) {
    return has_min() < other.has_min() ? -1 : 1;
  } else if (!(min() == other.min())) {
    return min() < other.min() ? -1 : 1;
  }
  if (!(has_max() == other.has_max())) {
    return has_max() < other.has_max() ? -1 : 1;
  } else if (!(max() == other.max())) {
    return max() < other.max() ? -1 : 1;
  }
  return 0;
}

bool ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::operator==(const _RandomUniformInitializerConf_& other) const {
  return true
    && has_min() == other.has_min() 
    && min() == other.min()
    && has_max() == other.has_max() 
    && max() == other.max()
  ;
}

std::size_t ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_min() ? std::hash<float>()(min()) : 0)
    ^ (has_max() ? std::hash<float>()(max()) : 0)
  ;
}

bool ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_::operator<(const _RandomUniformInitializerConf_& other) const {
  return false
    || !(has_min() == other.has_min()) ? 
      has_min() < other.has_min() : false
    || !(min() == other.min()) ? 
      min() < other.min() : false
    || !(has_max() == other.has_max()) ? 
      has_max() < other.has_max() : false
    || !(max() == other.max()) ? 
      max() < other.max() : false
  ;
}

using _RandomUniformInitializerConf_ =  ConstRandomUniformInitializerConf::_RandomUniformInitializerConf_;
ConstRandomUniformInitializerConf::ConstRandomUniformInitializerConf(const ::std::shared_ptr<_RandomUniformInitializerConf_>& data): data_(data) {}
ConstRandomUniformInitializerConf::ConstRandomUniformInitializerConf(): data_(::std::make_shared<_RandomUniformInitializerConf_>()) {}
ConstRandomUniformInitializerConf::ConstRandomUniformInitializerConf(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf) {
  BuildFromProto(proto_randomuniforminitializerconf);
}
ConstRandomUniformInitializerConf::ConstRandomUniformInitializerConf(const ConstRandomUniformInitializerConf&) = default;
ConstRandomUniformInitializerConf::ConstRandomUniformInitializerConf(ConstRandomUniformInitializerConf&&) noexcept = default;
ConstRandomUniformInitializerConf::~ConstRandomUniformInitializerConf() = default;

void ConstRandomUniformInitializerConf::ToProto(PbMessage* proto_randomuniforminitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RandomUniformInitializerConf*>(proto_randomuniforminitializerconf));
}
  
::std::string ConstRandomUniformInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRandomUniformInitializerConf::__Empty__() const {
  return !data_;
}

int ConstRandomUniformInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"min", 1},
    {"max", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRandomUniformInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRandomUniformInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRandomUniformInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &min();
    case 2: return &max();
    default: return nullptr;
  }
}

// required or optional field min
bool ConstRandomUniformInitializerConf::has_min() const {
  return __SharedPtrOrDefault__()->has_min();
}
const float& ConstRandomUniformInitializerConf::min() const {
  return __SharedPtrOrDefault__()->min();
}
// used by pybind11 only
// required or optional field max
bool ConstRandomUniformInitializerConf::has_max() const {
  return __SharedPtrOrDefault__()->has_max();
}
const float& ConstRandomUniformInitializerConf::max() const {
  return __SharedPtrOrDefault__()->max();
}
// used by pybind11 only

::std::shared_ptr<ConstRandomUniformInitializerConf> ConstRandomUniformInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstRandomUniformInitializerConf>(data_);
}
int64_t ConstRandomUniformInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRandomUniformInitializerConf::operator==(const ConstRandomUniformInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRandomUniformInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRandomUniformInitializerConf::operator<(const ConstRandomUniformInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RandomUniformInitializerConf_>& ConstRandomUniformInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RandomUniformInitializerConf_> default_ptr = std::make_shared<_RandomUniformInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RandomUniformInitializerConf_>& ConstRandomUniformInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RandomUniformInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRandomUniformInitializerConf
void ConstRandomUniformInitializerConf::BuildFromProto(const PbMessage& proto_randomuniforminitializerconf) {
  data_ = ::std::make_shared<_RandomUniformInitializerConf_>(dynamic_cast<const ::oneflow::RandomUniformInitializerConf&>(proto_randomuniforminitializerconf));
}

RandomUniformInitializerConf::RandomUniformInitializerConf(const ::std::shared_ptr<_RandomUniformInitializerConf_>& data)
  : ConstRandomUniformInitializerConf(data) {}
RandomUniformInitializerConf::RandomUniformInitializerConf(const RandomUniformInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RandomUniformInitializerConf> resize
RandomUniformInitializerConf::RandomUniformInitializerConf(RandomUniformInitializerConf&&) noexcept = default; 
RandomUniformInitializerConf::RandomUniformInitializerConf(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf) {
  InitFromProto(proto_randomuniforminitializerconf);
}
RandomUniformInitializerConf::RandomUniformInitializerConf() = default;

RandomUniformInitializerConf::~RandomUniformInitializerConf() = default;

void RandomUniformInitializerConf::InitFromProto(const PbMessage& proto_randomuniforminitializerconf) {
  BuildFromProto(proto_randomuniforminitializerconf);
}
  
void* RandomUniformInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_min();
    case 2: return mutable_max();
    default: return nullptr;
  }
}

bool RandomUniformInitializerConf::operator==(const RandomUniformInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RandomUniformInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RandomUniformInitializerConf::operator<(const RandomUniformInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RandomUniformInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void RandomUniformInitializerConf::CopyFrom(const RandomUniformInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RandomUniformInitializerConf& RandomUniformInitializerConf::operator=(const RandomUniformInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field min
void RandomUniformInitializerConf::clear_min() {
  return __SharedPtr__()->clear_min();
}
void RandomUniformInitializerConf::set_min(const float& value) {
  return __SharedPtr__()->set_min(value);
}
float* RandomUniformInitializerConf::mutable_min() {
  return  __SharedPtr__()->mutable_min();
}
// required or optional field max
void RandomUniformInitializerConf::clear_max() {
  return __SharedPtr__()->clear_max();
}
void RandomUniformInitializerConf::set_max(const float& value) {
  return __SharedPtr__()->set_max(value);
}
float* RandomUniformInitializerConf::mutable_max() {
  return  __SharedPtr__()->mutable_max();
}

::std::shared_ptr<RandomUniformInitializerConf> RandomUniformInitializerConf::__SharedMutable__() {
  return ::std::make_shared<RandomUniformInitializerConf>(__SharedPtr__());
}
ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::_RandomUniformIntInitializerConf_() { Clear(); }
ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::_RandomUniformIntInitializerConf_(const _RandomUniformIntInitializerConf_& other) { CopyFrom(other); }
ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::_RandomUniformIntInitializerConf_(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf) {
  InitFromProto(proto_randomuniformintinitializerconf);
}
ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::_RandomUniformIntInitializerConf_(_RandomUniformIntInitializerConf_&& other) = default;
ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::~_RandomUniformIntInitializerConf_() = default;

void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::InitFromProto(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf) {
  Clear();
  // required_or_optional field: min
  if (proto_randomuniformintinitializerconf.has_min()) {
    set_min(proto_randomuniformintinitializerconf.min());
  }
  // required_or_optional field: max
  if (proto_randomuniformintinitializerconf.has_max()) {
    set_max(proto_randomuniformintinitializerconf.max());
  }
    
}

void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::ToProto(::oneflow::RandomUniformIntInitializerConf* proto_randomuniformintinitializerconf) const {
  proto_randomuniformintinitializerconf->Clear();
  // required_or_optional field: min
  if (this->has_min()) {
    proto_randomuniformintinitializerconf->set_min(min());
    }
  // required_or_optional field: max
  if (this->has_max()) {
    proto_randomuniformintinitializerconf->set_max(max());
    }

}

::std::string ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::DebugString() const {
  ::oneflow::RandomUniformIntInitializerConf proto_randomuniformintinitializerconf;
  this->ToProto(&proto_randomuniformintinitializerconf);
  return proto_randomuniformintinitializerconf.DebugString();
}

void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::Clear() {
  clear_min();
  clear_max();
}

void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::CopyFrom(const _RandomUniformIntInitializerConf_& other) {
  if (other.has_min()) {
    set_min(other.min());
  } else {
    clear_min();
  }
  if (other.has_max()) {
    set_max(other.max());
  } else {
    clear_max();
  }
}


// optional field min
bool ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::has_min() const {
  return has_min_;
}
const int32_t& ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::min() const {
  if (has_min_) { return min_; }
  static const int32_t default_static_value =
    int32_t(0);
  return default_static_value;
}
void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::clear_min() {
  has_min_ = false;
}
void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::set_min(const int32_t& value) {
  min_ = value;
  has_min_ = true;
}
int32_t* ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::mutable_min() {
  has_min_ = true;
  return &min_;
}

// optional field max
bool ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::has_max() const {
  return has_max_;
}
const int32_t& ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::max() const {
  if (has_max_) { return max_; }
  static const int32_t default_static_value =
    int32_t(1);
  return default_static_value;
}
void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::clear_max() {
  has_max_ = false;
}
void ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::set_max(const int32_t& value) {
  max_ = value;
  has_max_ = true;
}
int32_t* ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::mutable_max() {
  has_max_ = true;
  return &max_;
}


int ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::compare(const _RandomUniformIntInitializerConf_& other) {
  if (!(has_min() == other.has_min())) {
    return has_min() < other.has_min() ? -1 : 1;
  } else if (!(min() == other.min())) {
    return min() < other.min() ? -1 : 1;
  }
  if (!(has_max() == other.has_max())) {
    return has_max() < other.has_max() ? -1 : 1;
  } else if (!(max() == other.max())) {
    return max() < other.max() ? -1 : 1;
  }
  return 0;
}

bool ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::operator==(const _RandomUniformIntInitializerConf_& other) const {
  return true
    && has_min() == other.has_min() 
    && min() == other.min()
    && has_max() == other.has_max() 
    && max() == other.max()
  ;
}

std::size_t ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_min() ? std::hash<int32_t>()(min()) : 0)
    ^ (has_max() ? std::hash<int32_t>()(max()) : 0)
  ;
}

bool ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_::operator<(const _RandomUniformIntInitializerConf_& other) const {
  return false
    || !(has_min() == other.has_min()) ? 
      has_min() < other.has_min() : false
    || !(min() == other.min()) ? 
      min() < other.min() : false
    || !(has_max() == other.has_max()) ? 
      has_max() < other.has_max() : false
    || !(max() == other.max()) ? 
      max() < other.max() : false
  ;
}

using _RandomUniformIntInitializerConf_ =  ConstRandomUniformIntInitializerConf::_RandomUniformIntInitializerConf_;
ConstRandomUniformIntInitializerConf::ConstRandomUniformIntInitializerConf(const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& data): data_(data) {}
ConstRandomUniformIntInitializerConf::ConstRandomUniformIntInitializerConf(): data_(::std::make_shared<_RandomUniformIntInitializerConf_>()) {}
ConstRandomUniformIntInitializerConf::ConstRandomUniformIntInitializerConf(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf) {
  BuildFromProto(proto_randomuniformintinitializerconf);
}
ConstRandomUniformIntInitializerConf::ConstRandomUniformIntInitializerConf(const ConstRandomUniformIntInitializerConf&) = default;
ConstRandomUniformIntInitializerConf::ConstRandomUniformIntInitializerConf(ConstRandomUniformIntInitializerConf&&) noexcept = default;
ConstRandomUniformIntInitializerConf::~ConstRandomUniformIntInitializerConf() = default;

void ConstRandomUniformIntInitializerConf::ToProto(PbMessage* proto_randomuniformintinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RandomUniformIntInitializerConf*>(proto_randomuniformintinitializerconf));
}
  
::std::string ConstRandomUniformIntInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRandomUniformIntInitializerConf::__Empty__() const {
  return !data_;
}

int ConstRandomUniformIntInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"min", 1},
    {"max", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRandomUniformIntInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRandomUniformIntInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRandomUniformIntInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &min();
    case 2: return &max();
    default: return nullptr;
  }
}

// required or optional field min
bool ConstRandomUniformIntInitializerConf::has_min() const {
  return __SharedPtrOrDefault__()->has_min();
}
const int32_t& ConstRandomUniformIntInitializerConf::min() const {
  return __SharedPtrOrDefault__()->min();
}
// used by pybind11 only
// required or optional field max
bool ConstRandomUniformIntInitializerConf::has_max() const {
  return __SharedPtrOrDefault__()->has_max();
}
const int32_t& ConstRandomUniformIntInitializerConf::max() const {
  return __SharedPtrOrDefault__()->max();
}
// used by pybind11 only

::std::shared_ptr<ConstRandomUniformIntInitializerConf> ConstRandomUniformIntInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstRandomUniformIntInitializerConf>(data_);
}
int64_t ConstRandomUniformIntInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRandomUniformIntInitializerConf::operator==(const ConstRandomUniformIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRandomUniformIntInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRandomUniformIntInitializerConf::operator<(const ConstRandomUniformIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& ConstRandomUniformIntInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RandomUniformIntInitializerConf_> default_ptr = std::make_shared<_RandomUniformIntInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& ConstRandomUniformIntInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RandomUniformIntInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRandomUniformIntInitializerConf
void ConstRandomUniformIntInitializerConf::BuildFromProto(const PbMessage& proto_randomuniformintinitializerconf) {
  data_ = ::std::make_shared<_RandomUniformIntInitializerConf_>(dynamic_cast<const ::oneflow::RandomUniformIntInitializerConf&>(proto_randomuniformintinitializerconf));
}

RandomUniformIntInitializerConf::RandomUniformIntInitializerConf(const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& data)
  : ConstRandomUniformIntInitializerConf(data) {}
RandomUniformIntInitializerConf::RandomUniformIntInitializerConf(const RandomUniformIntInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RandomUniformIntInitializerConf> resize
RandomUniformIntInitializerConf::RandomUniformIntInitializerConf(RandomUniformIntInitializerConf&&) noexcept = default; 
RandomUniformIntInitializerConf::RandomUniformIntInitializerConf(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf) {
  InitFromProto(proto_randomuniformintinitializerconf);
}
RandomUniformIntInitializerConf::RandomUniformIntInitializerConf() = default;

RandomUniformIntInitializerConf::~RandomUniformIntInitializerConf() = default;

void RandomUniformIntInitializerConf::InitFromProto(const PbMessage& proto_randomuniformintinitializerconf) {
  BuildFromProto(proto_randomuniformintinitializerconf);
}
  
void* RandomUniformIntInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_min();
    case 2: return mutable_max();
    default: return nullptr;
  }
}

bool RandomUniformIntInitializerConf::operator==(const RandomUniformIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RandomUniformIntInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RandomUniformIntInitializerConf::operator<(const RandomUniformIntInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RandomUniformIntInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void RandomUniformIntInitializerConf::CopyFrom(const RandomUniformIntInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RandomUniformIntInitializerConf& RandomUniformIntInitializerConf::operator=(const RandomUniformIntInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field min
void RandomUniformIntInitializerConf::clear_min() {
  return __SharedPtr__()->clear_min();
}
void RandomUniformIntInitializerConf::set_min(const int32_t& value) {
  return __SharedPtr__()->set_min(value);
}
int32_t* RandomUniformIntInitializerConf::mutable_min() {
  return  __SharedPtr__()->mutable_min();
}
// required or optional field max
void RandomUniformIntInitializerConf::clear_max() {
  return __SharedPtr__()->clear_max();
}
void RandomUniformIntInitializerConf::set_max(const int32_t& value) {
  return __SharedPtr__()->set_max(value);
}
int32_t* RandomUniformIntInitializerConf::mutable_max() {
  return  __SharedPtr__()->mutable_max();
}

::std::shared_ptr<RandomUniformIntInitializerConf> RandomUniformIntInitializerConf::__SharedMutable__() {
  return ::std::make_shared<RandomUniformIntInitializerConf>(__SharedPtr__());
}
ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::_RandomNormalInitializerConf_() { Clear(); }
ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::_RandomNormalInitializerConf_(const _RandomNormalInitializerConf_& other) { CopyFrom(other); }
ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::_RandomNormalInitializerConf_(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf) {
  InitFromProto(proto_randomnormalinitializerconf);
}
ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::_RandomNormalInitializerConf_(_RandomNormalInitializerConf_&& other) = default;
ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::~_RandomNormalInitializerConf_() = default;

void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::InitFromProto(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf) {
  Clear();
  // required_or_optional field: mean
  if (proto_randomnormalinitializerconf.has_mean()) {
    set_mean(proto_randomnormalinitializerconf.mean());
  }
  // required_or_optional field: std
  if (proto_randomnormalinitializerconf.has_std()) {
    set_std(proto_randomnormalinitializerconf.std());
  }
    
}

void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::ToProto(::oneflow::RandomNormalInitializerConf* proto_randomnormalinitializerconf) const {
  proto_randomnormalinitializerconf->Clear();
  // required_or_optional field: mean
  if (this->has_mean()) {
    proto_randomnormalinitializerconf->set_mean(mean());
    }
  // required_or_optional field: std
  if (this->has_std()) {
    proto_randomnormalinitializerconf->set_std(std());
    }

}

::std::string ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::DebugString() const {
  ::oneflow::RandomNormalInitializerConf proto_randomnormalinitializerconf;
  this->ToProto(&proto_randomnormalinitializerconf);
  return proto_randomnormalinitializerconf.DebugString();
}

void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::Clear() {
  clear_mean();
  clear_std();
}

void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::CopyFrom(const _RandomNormalInitializerConf_& other) {
  if (other.has_mean()) {
    set_mean(other.mean());
  } else {
    clear_mean();
  }
  if (other.has_std()) {
    set_std(other.std());
  } else {
    clear_std();
  }
}


// optional field mean
bool ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::has_mean() const {
  return has_mean_;
}
const float& ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::mean() const {
  if (has_mean_) { return mean_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::clear_mean() {
  has_mean_ = false;
}
void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::set_mean(const float& value) {
  mean_ = value;
  has_mean_ = true;
}
float* ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::mutable_mean() {
  has_mean_ = true;
  return &mean_;
}

// optional field std
bool ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::has_std() const {
  return has_std_;
}
const float& ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::std() const {
  if (has_std_) { return std_; }
  static const float default_static_value =
    float(1.0);
  return default_static_value;
}
void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::clear_std() {
  has_std_ = false;
}
void ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::set_std(const float& value) {
  std_ = value;
  has_std_ = true;
}
float* ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::mutable_std() {
  has_std_ = true;
  return &std_;
}


int ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::compare(const _RandomNormalInitializerConf_& other) {
  if (!(has_mean() == other.has_mean())) {
    return has_mean() < other.has_mean() ? -1 : 1;
  } else if (!(mean() == other.mean())) {
    return mean() < other.mean() ? -1 : 1;
  }
  if (!(has_std() == other.has_std())) {
    return has_std() < other.has_std() ? -1 : 1;
  } else if (!(std() == other.std())) {
    return std() < other.std() ? -1 : 1;
  }
  return 0;
}

bool ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::operator==(const _RandomNormalInitializerConf_& other) const {
  return true
    && has_mean() == other.has_mean() 
    && mean() == other.mean()
    && has_std() == other.has_std() 
    && std() == other.std()
  ;
}

std::size_t ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_mean() ? std::hash<float>()(mean()) : 0)
    ^ (has_std() ? std::hash<float>()(std()) : 0)
  ;
}

bool ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_::operator<(const _RandomNormalInitializerConf_& other) const {
  return false
    || !(has_mean() == other.has_mean()) ? 
      has_mean() < other.has_mean() : false
    || !(mean() == other.mean()) ? 
      mean() < other.mean() : false
    || !(has_std() == other.has_std()) ? 
      has_std() < other.has_std() : false
    || !(std() == other.std()) ? 
      std() < other.std() : false
  ;
}

using _RandomNormalInitializerConf_ =  ConstRandomNormalInitializerConf::_RandomNormalInitializerConf_;
ConstRandomNormalInitializerConf::ConstRandomNormalInitializerConf(const ::std::shared_ptr<_RandomNormalInitializerConf_>& data): data_(data) {}
ConstRandomNormalInitializerConf::ConstRandomNormalInitializerConf(): data_(::std::make_shared<_RandomNormalInitializerConf_>()) {}
ConstRandomNormalInitializerConf::ConstRandomNormalInitializerConf(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf) {
  BuildFromProto(proto_randomnormalinitializerconf);
}
ConstRandomNormalInitializerConf::ConstRandomNormalInitializerConf(const ConstRandomNormalInitializerConf&) = default;
ConstRandomNormalInitializerConf::ConstRandomNormalInitializerConf(ConstRandomNormalInitializerConf&&) noexcept = default;
ConstRandomNormalInitializerConf::~ConstRandomNormalInitializerConf() = default;

void ConstRandomNormalInitializerConf::ToProto(PbMessage* proto_randomnormalinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RandomNormalInitializerConf*>(proto_randomnormalinitializerconf));
}
  
::std::string ConstRandomNormalInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRandomNormalInitializerConf::__Empty__() const {
  return !data_;
}

int ConstRandomNormalInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"mean", 1},
    {"std", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRandomNormalInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRandomNormalInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRandomNormalInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &mean();
    case 2: return &std();
    default: return nullptr;
  }
}

// required or optional field mean
bool ConstRandomNormalInitializerConf::has_mean() const {
  return __SharedPtrOrDefault__()->has_mean();
}
const float& ConstRandomNormalInitializerConf::mean() const {
  return __SharedPtrOrDefault__()->mean();
}
// used by pybind11 only
// required or optional field std
bool ConstRandomNormalInitializerConf::has_std() const {
  return __SharedPtrOrDefault__()->has_std();
}
const float& ConstRandomNormalInitializerConf::std() const {
  return __SharedPtrOrDefault__()->std();
}
// used by pybind11 only

::std::shared_ptr<ConstRandomNormalInitializerConf> ConstRandomNormalInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstRandomNormalInitializerConf>(data_);
}
int64_t ConstRandomNormalInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRandomNormalInitializerConf::operator==(const ConstRandomNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRandomNormalInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRandomNormalInitializerConf::operator<(const ConstRandomNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RandomNormalInitializerConf_>& ConstRandomNormalInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RandomNormalInitializerConf_> default_ptr = std::make_shared<_RandomNormalInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RandomNormalInitializerConf_>& ConstRandomNormalInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RandomNormalInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRandomNormalInitializerConf
void ConstRandomNormalInitializerConf::BuildFromProto(const PbMessage& proto_randomnormalinitializerconf) {
  data_ = ::std::make_shared<_RandomNormalInitializerConf_>(dynamic_cast<const ::oneflow::RandomNormalInitializerConf&>(proto_randomnormalinitializerconf));
}

RandomNormalInitializerConf::RandomNormalInitializerConf(const ::std::shared_ptr<_RandomNormalInitializerConf_>& data)
  : ConstRandomNormalInitializerConf(data) {}
RandomNormalInitializerConf::RandomNormalInitializerConf(const RandomNormalInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RandomNormalInitializerConf> resize
RandomNormalInitializerConf::RandomNormalInitializerConf(RandomNormalInitializerConf&&) noexcept = default; 
RandomNormalInitializerConf::RandomNormalInitializerConf(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf) {
  InitFromProto(proto_randomnormalinitializerconf);
}
RandomNormalInitializerConf::RandomNormalInitializerConf() = default;

RandomNormalInitializerConf::~RandomNormalInitializerConf() = default;

void RandomNormalInitializerConf::InitFromProto(const PbMessage& proto_randomnormalinitializerconf) {
  BuildFromProto(proto_randomnormalinitializerconf);
}
  
void* RandomNormalInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_mean();
    case 2: return mutable_std();
    default: return nullptr;
  }
}

bool RandomNormalInitializerConf::operator==(const RandomNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RandomNormalInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RandomNormalInitializerConf::operator<(const RandomNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RandomNormalInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void RandomNormalInitializerConf::CopyFrom(const RandomNormalInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RandomNormalInitializerConf& RandomNormalInitializerConf::operator=(const RandomNormalInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field mean
void RandomNormalInitializerConf::clear_mean() {
  return __SharedPtr__()->clear_mean();
}
void RandomNormalInitializerConf::set_mean(const float& value) {
  return __SharedPtr__()->set_mean(value);
}
float* RandomNormalInitializerConf::mutable_mean() {
  return  __SharedPtr__()->mutable_mean();
}
// required or optional field std
void RandomNormalInitializerConf::clear_std() {
  return __SharedPtr__()->clear_std();
}
void RandomNormalInitializerConf::set_std(const float& value) {
  return __SharedPtr__()->set_std(value);
}
float* RandomNormalInitializerConf::mutable_std() {
  return  __SharedPtr__()->mutable_std();
}

::std::shared_ptr<RandomNormalInitializerConf> RandomNormalInitializerConf::__SharedMutable__() {
  return ::std::make_shared<RandomNormalInitializerConf>(__SharedPtr__());
}
ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::_TruncatedNormalInitializerConf_() { Clear(); }
ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::_TruncatedNormalInitializerConf_(const _TruncatedNormalInitializerConf_& other) { CopyFrom(other); }
ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::_TruncatedNormalInitializerConf_(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf) {
  InitFromProto(proto_truncatednormalinitializerconf);
}
ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::_TruncatedNormalInitializerConf_(_TruncatedNormalInitializerConf_&& other) = default;
ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::~_TruncatedNormalInitializerConf_() = default;

void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::InitFromProto(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf) {
  Clear();
  // required_or_optional field: mean
  if (proto_truncatednormalinitializerconf.has_mean()) {
    set_mean(proto_truncatednormalinitializerconf.mean());
  }
  // required_or_optional field: std
  if (proto_truncatednormalinitializerconf.has_std()) {
    set_std(proto_truncatednormalinitializerconf.std());
  }
    
}

void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::ToProto(::oneflow::TruncatedNormalInitializerConf* proto_truncatednormalinitializerconf) const {
  proto_truncatednormalinitializerconf->Clear();
  // required_or_optional field: mean
  if (this->has_mean()) {
    proto_truncatednormalinitializerconf->set_mean(mean());
    }
  // required_or_optional field: std
  if (this->has_std()) {
    proto_truncatednormalinitializerconf->set_std(std());
    }

}

::std::string ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::DebugString() const {
  ::oneflow::TruncatedNormalInitializerConf proto_truncatednormalinitializerconf;
  this->ToProto(&proto_truncatednormalinitializerconf);
  return proto_truncatednormalinitializerconf.DebugString();
}

void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::Clear() {
  clear_mean();
  clear_std();
}

void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::CopyFrom(const _TruncatedNormalInitializerConf_& other) {
  if (other.has_mean()) {
    set_mean(other.mean());
  } else {
    clear_mean();
  }
  if (other.has_std()) {
    set_std(other.std());
  } else {
    clear_std();
  }
}


// optional field mean
bool ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::has_mean() const {
  return has_mean_;
}
const float& ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::mean() const {
  if (has_mean_) { return mean_; }
  static const float default_static_value =
    float(0.0);
  return default_static_value;
}
void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::clear_mean() {
  has_mean_ = false;
}
void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::set_mean(const float& value) {
  mean_ = value;
  has_mean_ = true;
}
float* ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::mutable_mean() {
  has_mean_ = true;
  return &mean_;
}

// optional field std
bool ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::has_std() const {
  return has_std_;
}
const float& ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::std() const {
  if (has_std_) { return std_; }
  static const float default_static_value =
    float(0.05000000074505806);
  return default_static_value;
}
void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::clear_std() {
  has_std_ = false;
}
void ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::set_std(const float& value) {
  std_ = value;
  has_std_ = true;
}
float* ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::mutable_std() {
  has_std_ = true;
  return &std_;
}


int ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::compare(const _TruncatedNormalInitializerConf_& other) {
  if (!(has_mean() == other.has_mean())) {
    return has_mean() < other.has_mean() ? -1 : 1;
  } else if (!(mean() == other.mean())) {
    return mean() < other.mean() ? -1 : 1;
  }
  if (!(has_std() == other.has_std())) {
    return has_std() < other.has_std() ? -1 : 1;
  } else if (!(std() == other.std())) {
    return std() < other.std() ? -1 : 1;
  }
  return 0;
}

bool ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::operator==(const _TruncatedNormalInitializerConf_& other) const {
  return true
    && has_mean() == other.has_mean() 
    && mean() == other.mean()
    && has_std() == other.has_std() 
    && std() == other.std()
  ;
}

std::size_t ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_mean() ? std::hash<float>()(mean()) : 0)
    ^ (has_std() ? std::hash<float>()(std()) : 0)
  ;
}

bool ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_::operator<(const _TruncatedNormalInitializerConf_& other) const {
  return false
    || !(has_mean() == other.has_mean()) ? 
      has_mean() < other.has_mean() : false
    || !(mean() == other.mean()) ? 
      mean() < other.mean() : false
    || !(has_std() == other.has_std()) ? 
      has_std() < other.has_std() : false
    || !(std() == other.std()) ? 
      std() < other.std() : false
  ;
}

using _TruncatedNormalInitializerConf_ =  ConstTruncatedNormalInitializerConf::_TruncatedNormalInitializerConf_;
ConstTruncatedNormalInitializerConf::ConstTruncatedNormalInitializerConf(const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& data): data_(data) {}
ConstTruncatedNormalInitializerConf::ConstTruncatedNormalInitializerConf(): data_(::std::make_shared<_TruncatedNormalInitializerConf_>()) {}
ConstTruncatedNormalInitializerConf::ConstTruncatedNormalInitializerConf(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf) {
  BuildFromProto(proto_truncatednormalinitializerconf);
}
ConstTruncatedNormalInitializerConf::ConstTruncatedNormalInitializerConf(const ConstTruncatedNormalInitializerConf&) = default;
ConstTruncatedNormalInitializerConf::ConstTruncatedNormalInitializerConf(ConstTruncatedNormalInitializerConf&&) noexcept = default;
ConstTruncatedNormalInitializerConf::~ConstTruncatedNormalInitializerConf() = default;

void ConstTruncatedNormalInitializerConf::ToProto(PbMessage* proto_truncatednormalinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TruncatedNormalInitializerConf*>(proto_truncatednormalinitializerconf));
}
  
::std::string ConstTruncatedNormalInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTruncatedNormalInitializerConf::__Empty__() const {
  return !data_;
}

int ConstTruncatedNormalInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"mean", 1},
    {"std", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstTruncatedNormalInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstTruncatedNormalInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTruncatedNormalInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &mean();
    case 2: return &std();
    default: return nullptr;
  }
}

// required or optional field mean
bool ConstTruncatedNormalInitializerConf::has_mean() const {
  return __SharedPtrOrDefault__()->has_mean();
}
const float& ConstTruncatedNormalInitializerConf::mean() const {
  return __SharedPtrOrDefault__()->mean();
}
// used by pybind11 only
// required or optional field std
bool ConstTruncatedNormalInitializerConf::has_std() const {
  return __SharedPtrOrDefault__()->has_std();
}
const float& ConstTruncatedNormalInitializerConf::std() const {
  return __SharedPtrOrDefault__()->std();
}
// used by pybind11 only

::std::shared_ptr<ConstTruncatedNormalInitializerConf> ConstTruncatedNormalInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstTruncatedNormalInitializerConf>(data_);
}
int64_t ConstTruncatedNormalInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTruncatedNormalInitializerConf::operator==(const ConstTruncatedNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTruncatedNormalInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTruncatedNormalInitializerConf::operator<(const ConstTruncatedNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& ConstTruncatedNormalInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TruncatedNormalInitializerConf_> default_ptr = std::make_shared<_TruncatedNormalInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& ConstTruncatedNormalInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _TruncatedNormalInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTruncatedNormalInitializerConf
void ConstTruncatedNormalInitializerConf::BuildFromProto(const PbMessage& proto_truncatednormalinitializerconf) {
  data_ = ::std::make_shared<_TruncatedNormalInitializerConf_>(dynamic_cast<const ::oneflow::TruncatedNormalInitializerConf&>(proto_truncatednormalinitializerconf));
}

TruncatedNormalInitializerConf::TruncatedNormalInitializerConf(const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& data)
  : ConstTruncatedNormalInitializerConf(data) {}
TruncatedNormalInitializerConf::TruncatedNormalInitializerConf(const TruncatedNormalInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TruncatedNormalInitializerConf> resize
TruncatedNormalInitializerConf::TruncatedNormalInitializerConf(TruncatedNormalInitializerConf&&) noexcept = default; 
TruncatedNormalInitializerConf::TruncatedNormalInitializerConf(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf) {
  InitFromProto(proto_truncatednormalinitializerconf);
}
TruncatedNormalInitializerConf::TruncatedNormalInitializerConf() = default;

TruncatedNormalInitializerConf::~TruncatedNormalInitializerConf() = default;

void TruncatedNormalInitializerConf::InitFromProto(const PbMessage& proto_truncatednormalinitializerconf) {
  BuildFromProto(proto_truncatednormalinitializerconf);
}
  
void* TruncatedNormalInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_mean();
    case 2: return mutable_std();
    default: return nullptr;
  }
}

bool TruncatedNormalInitializerConf::operator==(const TruncatedNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TruncatedNormalInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TruncatedNormalInitializerConf::operator<(const TruncatedNormalInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TruncatedNormalInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void TruncatedNormalInitializerConf::CopyFrom(const TruncatedNormalInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TruncatedNormalInitializerConf& TruncatedNormalInitializerConf::operator=(const TruncatedNormalInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field mean
void TruncatedNormalInitializerConf::clear_mean() {
  return __SharedPtr__()->clear_mean();
}
void TruncatedNormalInitializerConf::set_mean(const float& value) {
  return __SharedPtr__()->set_mean(value);
}
float* TruncatedNormalInitializerConf::mutable_mean() {
  return  __SharedPtr__()->mutable_mean();
}
// required or optional field std
void TruncatedNormalInitializerConf::clear_std() {
  return __SharedPtr__()->clear_std();
}
void TruncatedNormalInitializerConf::set_std(const float& value) {
  return __SharedPtr__()->set_std(value);
}
float* TruncatedNormalInitializerConf::mutable_std() {
  return  __SharedPtr__()->mutable_std();
}

::std::shared_ptr<TruncatedNormalInitializerConf> TruncatedNormalInitializerConf::__SharedMutable__() {
  return ::std::make_shared<TruncatedNormalInitializerConf>(__SharedPtr__());
}
ConstXavierInitializerConf::_XavierInitializerConf_::_XavierInitializerConf_() { Clear(); }
ConstXavierInitializerConf::_XavierInitializerConf_::_XavierInitializerConf_(const _XavierInitializerConf_& other) { CopyFrom(other); }
ConstXavierInitializerConf::_XavierInitializerConf_::_XavierInitializerConf_(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf) {
  InitFromProto(proto_xavierinitializerconf);
}
ConstXavierInitializerConf::_XavierInitializerConf_::_XavierInitializerConf_(_XavierInitializerConf_&& other) = default;
ConstXavierInitializerConf::_XavierInitializerConf_::~_XavierInitializerConf_() = default;

void ConstXavierInitializerConf::_XavierInitializerConf_::InitFromProto(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf) {
  Clear();
  // required_or_optional field: variance_norm
  if (proto_xavierinitializerconf.has_variance_norm()) {
  set_variance_norm(static_cast<std::remove_reference<std::remove_const<decltype(variance_norm())>::type>::type>(proto_xavierinitializerconf.variance_norm()));
  }
  // required_or_optional field: data_format
  if (proto_xavierinitializerconf.has_data_format()) {
    set_data_format(proto_xavierinitializerconf.data_format());
  }
    
}

void ConstXavierInitializerConf::_XavierInitializerConf_::ToProto(::oneflow::XavierInitializerConf* proto_xavierinitializerconf) const {
  proto_xavierinitializerconf->Clear();
  // required_or_optional field: variance_norm
  if (this->has_variance_norm()) {
    proto_xavierinitializerconf->set_variance_norm(static_cast<std::remove_const<std::remove_reference<decltype(proto_xavierinitializerconf->variance_norm())>::type>::type>(variance_norm()));
    }
  // required_or_optional field: data_format
  if (this->has_data_format()) {
    proto_xavierinitializerconf->set_data_format(data_format());
    }

}

::std::string ConstXavierInitializerConf::_XavierInitializerConf_::DebugString() const {
  ::oneflow::XavierInitializerConf proto_xavierinitializerconf;
  this->ToProto(&proto_xavierinitializerconf);
  return proto_xavierinitializerconf.DebugString();
}

void ConstXavierInitializerConf::_XavierInitializerConf_::Clear() {
  clear_variance_norm();
  clear_data_format();
}

void ConstXavierInitializerConf::_XavierInitializerConf_::CopyFrom(const _XavierInitializerConf_& other) {
  if (other.has_variance_norm()) {
    set_variance_norm(other.variance_norm());
  } else {
    clear_variance_norm();
  }
  if (other.has_data_format()) {
    set_data_format(other.data_format());
  } else {
    clear_data_format();
  }
}


// optional field variance_norm
bool ConstXavierInitializerConf::_XavierInitializerConf_::has_variance_norm() const {
  return has_variance_norm_;
}
const ::oneflow::cfg::VarianceNorm& ConstXavierInitializerConf::_XavierInitializerConf_::variance_norm() const {
  if (has_variance_norm_) { return variance_norm_; }
  static const ::oneflow::cfg::VarianceNorm default_static_value = ::oneflow::cfg::VarianceNorm();
  return default_static_value;
}
void ConstXavierInitializerConf::_XavierInitializerConf_::clear_variance_norm() {
  has_variance_norm_ = false;
}
void ConstXavierInitializerConf::_XavierInitializerConf_::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  variance_norm_ = value;
  has_variance_norm_ = true;
}
::oneflow::cfg::VarianceNorm* ConstXavierInitializerConf::_XavierInitializerConf_::mutable_variance_norm() {
  has_variance_norm_ = true;
  return &variance_norm_;
}

// optional field data_format
bool ConstXavierInitializerConf::_XavierInitializerConf_::has_data_format() const {
  return has_data_format_;
}
const ::std::string& ConstXavierInitializerConf::_XavierInitializerConf_::data_format() const {
  if (has_data_format_) { return data_format_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstXavierInitializerConf::_XavierInitializerConf_::clear_data_format() {
  has_data_format_ = false;
}
void ConstXavierInitializerConf::_XavierInitializerConf_::set_data_format(const ::std::string& value) {
  data_format_ = value;
  has_data_format_ = true;
}
::std::string* ConstXavierInitializerConf::_XavierInitializerConf_::mutable_data_format() {
  has_data_format_ = true;
  return &data_format_;
}


int ConstXavierInitializerConf::_XavierInitializerConf_::compare(const _XavierInitializerConf_& other) {
  if (!(has_variance_norm() == other.has_variance_norm())) {
    return has_variance_norm() < other.has_variance_norm() ? -1 : 1;
  } else if (!(variance_norm() == other.variance_norm())) {
    return variance_norm() < other.variance_norm() ? -1 : 1;
  }
  if (!(has_data_format() == other.has_data_format())) {
    return has_data_format() < other.has_data_format() ? -1 : 1;
  } else if (!(data_format() == other.data_format())) {
    return data_format() < other.data_format() ? -1 : 1;
  }
  return 0;
}

bool ConstXavierInitializerConf::_XavierInitializerConf_::operator==(const _XavierInitializerConf_& other) const {
  return true
    && has_variance_norm() == other.has_variance_norm() 
    && variance_norm() == other.variance_norm()
    && has_data_format() == other.has_data_format() 
    && data_format() == other.data_format()
  ;
}

std::size_t ConstXavierInitializerConf::_XavierInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_variance_norm() ? std::hash<::oneflow::cfg::VarianceNorm>()(variance_norm()) : 0)
    ^ (has_data_format() ? std::hash<::std::string>()(data_format()) : 0)
  ;
}

bool ConstXavierInitializerConf::_XavierInitializerConf_::operator<(const _XavierInitializerConf_& other) const {
  return false
    || !(has_variance_norm() == other.has_variance_norm()) ? 
      has_variance_norm() < other.has_variance_norm() : false
    || !(variance_norm() == other.variance_norm()) ? 
      variance_norm() < other.variance_norm() : false
    || !(has_data_format() == other.has_data_format()) ? 
      has_data_format() < other.has_data_format() : false
    || !(data_format() == other.data_format()) ? 
      data_format() < other.data_format() : false
  ;
}

using _XavierInitializerConf_ =  ConstXavierInitializerConf::_XavierInitializerConf_;
ConstXavierInitializerConf::ConstXavierInitializerConf(const ::std::shared_ptr<_XavierInitializerConf_>& data): data_(data) {}
ConstXavierInitializerConf::ConstXavierInitializerConf(): data_(::std::make_shared<_XavierInitializerConf_>()) {}
ConstXavierInitializerConf::ConstXavierInitializerConf(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf) {
  BuildFromProto(proto_xavierinitializerconf);
}
ConstXavierInitializerConf::ConstXavierInitializerConf(const ConstXavierInitializerConf&) = default;
ConstXavierInitializerConf::ConstXavierInitializerConf(ConstXavierInitializerConf&&) noexcept = default;
ConstXavierInitializerConf::~ConstXavierInitializerConf() = default;

void ConstXavierInitializerConf::ToProto(PbMessage* proto_xavierinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::XavierInitializerConf*>(proto_xavierinitializerconf));
}
  
::std::string ConstXavierInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstXavierInitializerConf::__Empty__() const {
  return !data_;
}

int ConstXavierInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"variance_norm", 1},
    {"data_format", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstXavierInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstXavierInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::VarianceNorm),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstXavierInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &variance_norm();
    case 2: return &data_format();
    default: return nullptr;
  }
}

// required or optional field variance_norm
bool ConstXavierInitializerConf::has_variance_norm() const {
  return __SharedPtrOrDefault__()->has_variance_norm();
}
const ::oneflow::cfg::VarianceNorm& ConstXavierInitializerConf::variance_norm() const {
  return __SharedPtrOrDefault__()->variance_norm();
}
// used by pybind11 only
// required or optional field data_format
bool ConstXavierInitializerConf::has_data_format() const {
  return __SharedPtrOrDefault__()->has_data_format();
}
const ::std::string& ConstXavierInitializerConf::data_format() const {
  return __SharedPtrOrDefault__()->data_format();
}
// used by pybind11 only

::std::shared_ptr<ConstXavierInitializerConf> ConstXavierInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstXavierInitializerConf>(data_);
}
int64_t ConstXavierInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstXavierInitializerConf::operator==(const ConstXavierInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstXavierInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstXavierInitializerConf::operator<(const ConstXavierInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_XavierInitializerConf_>& ConstXavierInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_XavierInitializerConf_> default_ptr = std::make_shared<_XavierInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_XavierInitializerConf_>& ConstXavierInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _XavierInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstXavierInitializerConf
void ConstXavierInitializerConf::BuildFromProto(const PbMessage& proto_xavierinitializerconf) {
  data_ = ::std::make_shared<_XavierInitializerConf_>(dynamic_cast<const ::oneflow::XavierInitializerConf&>(proto_xavierinitializerconf));
}

XavierInitializerConf::XavierInitializerConf(const ::std::shared_ptr<_XavierInitializerConf_>& data)
  : ConstXavierInitializerConf(data) {}
XavierInitializerConf::XavierInitializerConf(const XavierInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<XavierInitializerConf> resize
XavierInitializerConf::XavierInitializerConf(XavierInitializerConf&&) noexcept = default; 
XavierInitializerConf::XavierInitializerConf(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf) {
  InitFromProto(proto_xavierinitializerconf);
}
XavierInitializerConf::XavierInitializerConf() = default;

XavierInitializerConf::~XavierInitializerConf() = default;

void XavierInitializerConf::InitFromProto(const PbMessage& proto_xavierinitializerconf) {
  BuildFromProto(proto_xavierinitializerconf);
}
  
void* XavierInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_variance_norm();
    case 2: return mutable_data_format();
    default: return nullptr;
  }
}

bool XavierInitializerConf::operator==(const XavierInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t XavierInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool XavierInitializerConf::operator<(const XavierInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void XavierInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void XavierInitializerConf::CopyFrom(const XavierInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
XavierInitializerConf& XavierInitializerConf::operator=(const XavierInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field variance_norm
void XavierInitializerConf::clear_variance_norm() {
  return __SharedPtr__()->clear_variance_norm();
}
void XavierInitializerConf::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  return __SharedPtr__()->set_variance_norm(value);
}
::oneflow::cfg::VarianceNorm* XavierInitializerConf::mutable_variance_norm() {
  return  __SharedPtr__()->mutable_variance_norm();
}
// required or optional field data_format
void XavierInitializerConf::clear_data_format() {
  return __SharedPtr__()->clear_data_format();
}
void XavierInitializerConf::set_data_format(const ::std::string& value) {
  return __SharedPtr__()->set_data_format(value);
}
::std::string* XavierInitializerConf::mutable_data_format() {
  return  __SharedPtr__()->mutable_data_format();
}

::std::shared_ptr<XavierInitializerConf> XavierInitializerConf::__SharedMutable__() {
  return ::std::make_shared<XavierInitializerConf>(__SharedPtr__());
}
ConstMsraInitializerConf::_MsraInitializerConf_::_MsraInitializerConf_() { Clear(); }
ConstMsraInitializerConf::_MsraInitializerConf_::_MsraInitializerConf_(const _MsraInitializerConf_& other) { CopyFrom(other); }
ConstMsraInitializerConf::_MsraInitializerConf_::_MsraInitializerConf_(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf) {
  InitFromProto(proto_msrainitializerconf);
}
ConstMsraInitializerConf::_MsraInitializerConf_::_MsraInitializerConf_(_MsraInitializerConf_&& other) = default;
ConstMsraInitializerConf::_MsraInitializerConf_::~_MsraInitializerConf_() = default;

void ConstMsraInitializerConf::_MsraInitializerConf_::InitFromProto(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf) {
  Clear();
  // required_or_optional field: variance_norm
  if (proto_msrainitializerconf.has_variance_norm()) {
  set_variance_norm(static_cast<std::remove_reference<std::remove_const<decltype(variance_norm())>::type>::type>(proto_msrainitializerconf.variance_norm()));
  }
  // required_or_optional field: data_format
  if (proto_msrainitializerconf.has_data_format()) {
    set_data_format(proto_msrainitializerconf.data_format());
  }
    
}

void ConstMsraInitializerConf::_MsraInitializerConf_::ToProto(::oneflow::MsraInitializerConf* proto_msrainitializerconf) const {
  proto_msrainitializerconf->Clear();
  // required_or_optional field: variance_norm
  if (this->has_variance_norm()) {
    proto_msrainitializerconf->set_variance_norm(static_cast<std::remove_const<std::remove_reference<decltype(proto_msrainitializerconf->variance_norm())>::type>::type>(variance_norm()));
    }
  // required_or_optional field: data_format
  if (this->has_data_format()) {
    proto_msrainitializerconf->set_data_format(data_format());
    }

}

::std::string ConstMsraInitializerConf::_MsraInitializerConf_::DebugString() const {
  ::oneflow::MsraInitializerConf proto_msrainitializerconf;
  this->ToProto(&proto_msrainitializerconf);
  return proto_msrainitializerconf.DebugString();
}

void ConstMsraInitializerConf::_MsraInitializerConf_::Clear() {
  clear_variance_norm();
  clear_data_format();
}

void ConstMsraInitializerConf::_MsraInitializerConf_::CopyFrom(const _MsraInitializerConf_& other) {
  if (other.has_variance_norm()) {
    set_variance_norm(other.variance_norm());
  } else {
    clear_variance_norm();
  }
  if (other.has_data_format()) {
    set_data_format(other.data_format());
  } else {
    clear_data_format();
  }
}


// optional field variance_norm
bool ConstMsraInitializerConf::_MsraInitializerConf_::has_variance_norm() const {
  return has_variance_norm_;
}
const ::oneflow::cfg::VarianceNorm& ConstMsraInitializerConf::_MsraInitializerConf_::variance_norm() const {
  if (has_variance_norm_) { return variance_norm_; }
  static const ::oneflow::cfg::VarianceNorm default_static_value = ::oneflow::cfg::VarianceNorm();
  return default_static_value;
}
void ConstMsraInitializerConf::_MsraInitializerConf_::clear_variance_norm() {
  has_variance_norm_ = false;
}
void ConstMsraInitializerConf::_MsraInitializerConf_::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  variance_norm_ = value;
  has_variance_norm_ = true;
}
::oneflow::cfg::VarianceNorm* ConstMsraInitializerConf::_MsraInitializerConf_::mutable_variance_norm() {
  has_variance_norm_ = true;
  return &variance_norm_;
}

// optional field data_format
bool ConstMsraInitializerConf::_MsraInitializerConf_::has_data_format() const {
  return has_data_format_;
}
const ::std::string& ConstMsraInitializerConf::_MsraInitializerConf_::data_format() const {
  if (has_data_format_) { return data_format_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstMsraInitializerConf::_MsraInitializerConf_::clear_data_format() {
  has_data_format_ = false;
}
void ConstMsraInitializerConf::_MsraInitializerConf_::set_data_format(const ::std::string& value) {
  data_format_ = value;
  has_data_format_ = true;
}
::std::string* ConstMsraInitializerConf::_MsraInitializerConf_::mutable_data_format() {
  has_data_format_ = true;
  return &data_format_;
}


int ConstMsraInitializerConf::_MsraInitializerConf_::compare(const _MsraInitializerConf_& other) {
  if (!(has_variance_norm() == other.has_variance_norm())) {
    return has_variance_norm() < other.has_variance_norm() ? -1 : 1;
  } else if (!(variance_norm() == other.variance_norm())) {
    return variance_norm() < other.variance_norm() ? -1 : 1;
  }
  if (!(has_data_format() == other.has_data_format())) {
    return has_data_format() < other.has_data_format() ? -1 : 1;
  } else if (!(data_format() == other.data_format())) {
    return data_format() < other.data_format() ? -1 : 1;
  }
  return 0;
}

bool ConstMsraInitializerConf::_MsraInitializerConf_::operator==(const _MsraInitializerConf_& other) const {
  return true
    && has_variance_norm() == other.has_variance_norm() 
    && variance_norm() == other.variance_norm()
    && has_data_format() == other.has_data_format() 
    && data_format() == other.data_format()
  ;
}

std::size_t ConstMsraInitializerConf::_MsraInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_variance_norm() ? std::hash<::oneflow::cfg::VarianceNorm>()(variance_norm()) : 0)
    ^ (has_data_format() ? std::hash<::std::string>()(data_format()) : 0)
  ;
}

bool ConstMsraInitializerConf::_MsraInitializerConf_::operator<(const _MsraInitializerConf_& other) const {
  return false
    || !(has_variance_norm() == other.has_variance_norm()) ? 
      has_variance_norm() < other.has_variance_norm() : false
    || !(variance_norm() == other.variance_norm()) ? 
      variance_norm() < other.variance_norm() : false
    || !(has_data_format() == other.has_data_format()) ? 
      has_data_format() < other.has_data_format() : false
    || !(data_format() == other.data_format()) ? 
      data_format() < other.data_format() : false
  ;
}

using _MsraInitializerConf_ =  ConstMsraInitializerConf::_MsraInitializerConf_;
ConstMsraInitializerConf::ConstMsraInitializerConf(const ::std::shared_ptr<_MsraInitializerConf_>& data): data_(data) {}
ConstMsraInitializerConf::ConstMsraInitializerConf(): data_(::std::make_shared<_MsraInitializerConf_>()) {}
ConstMsraInitializerConf::ConstMsraInitializerConf(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf) {
  BuildFromProto(proto_msrainitializerconf);
}
ConstMsraInitializerConf::ConstMsraInitializerConf(const ConstMsraInitializerConf&) = default;
ConstMsraInitializerConf::ConstMsraInitializerConf(ConstMsraInitializerConf&&) noexcept = default;
ConstMsraInitializerConf::~ConstMsraInitializerConf() = default;

void ConstMsraInitializerConf::ToProto(PbMessage* proto_msrainitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MsraInitializerConf*>(proto_msrainitializerconf));
}
  
::std::string ConstMsraInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMsraInitializerConf::__Empty__() const {
  return !data_;
}

int ConstMsraInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"variance_norm", 1},
    {"data_format", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMsraInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMsraInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::VarianceNorm),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMsraInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &variance_norm();
    case 2: return &data_format();
    default: return nullptr;
  }
}

// required or optional field variance_norm
bool ConstMsraInitializerConf::has_variance_norm() const {
  return __SharedPtrOrDefault__()->has_variance_norm();
}
const ::oneflow::cfg::VarianceNorm& ConstMsraInitializerConf::variance_norm() const {
  return __SharedPtrOrDefault__()->variance_norm();
}
// used by pybind11 only
// required or optional field data_format
bool ConstMsraInitializerConf::has_data_format() const {
  return __SharedPtrOrDefault__()->has_data_format();
}
const ::std::string& ConstMsraInitializerConf::data_format() const {
  return __SharedPtrOrDefault__()->data_format();
}
// used by pybind11 only

::std::shared_ptr<ConstMsraInitializerConf> ConstMsraInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstMsraInitializerConf>(data_);
}
int64_t ConstMsraInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMsraInitializerConf::operator==(const ConstMsraInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMsraInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMsraInitializerConf::operator<(const ConstMsraInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MsraInitializerConf_>& ConstMsraInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MsraInitializerConf_> default_ptr = std::make_shared<_MsraInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_MsraInitializerConf_>& ConstMsraInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _MsraInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMsraInitializerConf
void ConstMsraInitializerConf::BuildFromProto(const PbMessage& proto_msrainitializerconf) {
  data_ = ::std::make_shared<_MsraInitializerConf_>(dynamic_cast<const ::oneflow::MsraInitializerConf&>(proto_msrainitializerconf));
}

MsraInitializerConf::MsraInitializerConf(const ::std::shared_ptr<_MsraInitializerConf_>& data)
  : ConstMsraInitializerConf(data) {}
MsraInitializerConf::MsraInitializerConf(const MsraInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MsraInitializerConf> resize
MsraInitializerConf::MsraInitializerConf(MsraInitializerConf&&) noexcept = default; 
MsraInitializerConf::MsraInitializerConf(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf) {
  InitFromProto(proto_msrainitializerconf);
}
MsraInitializerConf::MsraInitializerConf() = default;

MsraInitializerConf::~MsraInitializerConf() = default;

void MsraInitializerConf::InitFromProto(const PbMessage& proto_msrainitializerconf) {
  BuildFromProto(proto_msrainitializerconf);
}
  
void* MsraInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_variance_norm();
    case 2: return mutable_data_format();
    default: return nullptr;
  }
}

bool MsraInitializerConf::operator==(const MsraInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MsraInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MsraInitializerConf::operator<(const MsraInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MsraInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void MsraInitializerConf::CopyFrom(const MsraInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MsraInitializerConf& MsraInitializerConf::operator=(const MsraInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field variance_norm
void MsraInitializerConf::clear_variance_norm() {
  return __SharedPtr__()->clear_variance_norm();
}
void MsraInitializerConf::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  return __SharedPtr__()->set_variance_norm(value);
}
::oneflow::cfg::VarianceNorm* MsraInitializerConf::mutable_variance_norm() {
  return  __SharedPtr__()->mutable_variance_norm();
}
// required or optional field data_format
void MsraInitializerConf::clear_data_format() {
  return __SharedPtr__()->clear_data_format();
}
void MsraInitializerConf::set_data_format(const ::std::string& value) {
  return __SharedPtr__()->set_data_format(value);
}
::std::string* MsraInitializerConf::mutable_data_format() {
  return  __SharedPtr__()->mutable_data_format();
}

::std::shared_ptr<MsraInitializerConf> MsraInitializerConf::__SharedMutable__() {
  return ::std::make_shared<MsraInitializerConf>(__SharedPtr__());
}
ConstRangeInitializerConf::_RangeInitializerConf_::_RangeInitializerConf_() { Clear(); }
ConstRangeInitializerConf::_RangeInitializerConf_::_RangeInitializerConf_(const _RangeInitializerConf_& other) { CopyFrom(other); }
ConstRangeInitializerConf::_RangeInitializerConf_::_RangeInitializerConf_(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf) {
  InitFromProto(proto_rangeinitializerconf);
}
ConstRangeInitializerConf::_RangeInitializerConf_::_RangeInitializerConf_(_RangeInitializerConf_&& other) = default;
ConstRangeInitializerConf::_RangeInitializerConf_::~_RangeInitializerConf_() = default;

void ConstRangeInitializerConf::_RangeInitializerConf_::InitFromProto(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf) {
  Clear();
  // required_or_optional field: start
  if (proto_rangeinitializerconf.has_start()) {
    set_start(proto_rangeinitializerconf.start());
  }
  // required_or_optional field: stride
  if (proto_rangeinitializerconf.has_stride()) {
    set_stride(proto_rangeinitializerconf.stride());
  }
  // required_or_optional field: axis
  if (proto_rangeinitializerconf.has_axis()) {
    set_axis(proto_rangeinitializerconf.axis());
  }
    
}

void ConstRangeInitializerConf::_RangeInitializerConf_::ToProto(::oneflow::RangeInitializerConf* proto_rangeinitializerconf) const {
  proto_rangeinitializerconf->Clear();
  // required_or_optional field: start
  if (this->has_start()) {
    proto_rangeinitializerconf->set_start(start());
    }
  // required_or_optional field: stride
  if (this->has_stride()) {
    proto_rangeinitializerconf->set_stride(stride());
    }
  // required_or_optional field: axis
  if (this->has_axis()) {
    proto_rangeinitializerconf->set_axis(axis());
    }

}

::std::string ConstRangeInitializerConf::_RangeInitializerConf_::DebugString() const {
  ::oneflow::RangeInitializerConf proto_rangeinitializerconf;
  this->ToProto(&proto_rangeinitializerconf);
  return proto_rangeinitializerconf.DebugString();
}

void ConstRangeInitializerConf::_RangeInitializerConf_::Clear() {
  clear_start();
  clear_stride();
  clear_axis();
}

void ConstRangeInitializerConf::_RangeInitializerConf_::CopyFrom(const _RangeInitializerConf_& other) {
  if (other.has_start()) {
    set_start(other.start());
  } else {
    clear_start();
  }
  if (other.has_stride()) {
    set_stride(other.stride());
  } else {
    clear_stride();
  }
  if (other.has_axis()) {
    set_axis(other.axis());
  } else {
    clear_axis();
  }
}


// optional field start
bool ConstRangeInitializerConf::_RangeInitializerConf_::has_start() const {
  return has_start_;
}
const double& ConstRangeInitializerConf::_RangeInitializerConf_::start() const {
  if (has_start_) { return start_; }
  static const double default_static_value =
    double(0.0);
  return default_static_value;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::clear_start() {
  has_start_ = false;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::set_start(const double& value) {
  start_ = value;
  has_start_ = true;
}
double* ConstRangeInitializerConf::_RangeInitializerConf_::mutable_start() {
  has_start_ = true;
  return &start_;
}

// optional field stride
bool ConstRangeInitializerConf::_RangeInitializerConf_::has_stride() const {
  return has_stride_;
}
const double& ConstRangeInitializerConf::_RangeInitializerConf_::stride() const {
  if (has_stride_) { return stride_; }
  static const double default_static_value =
    double(1.0);
  return default_static_value;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::clear_stride() {
  has_stride_ = false;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::set_stride(const double& value) {
  stride_ = value;
  has_stride_ = true;
}
double* ConstRangeInitializerConf::_RangeInitializerConf_::mutable_stride() {
  has_stride_ = true;
  return &stride_;
}

// optional field axis
bool ConstRangeInitializerConf::_RangeInitializerConf_::has_axis() const {
  return has_axis_;
}
const int64_t& ConstRangeInitializerConf::_RangeInitializerConf_::axis() const {
  if (has_axis_) { return axis_; }
  static const int64_t default_static_value =
    int64_t(-1);
  return default_static_value;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::clear_axis() {
  has_axis_ = false;
}
void ConstRangeInitializerConf::_RangeInitializerConf_::set_axis(const int64_t& value) {
  axis_ = value;
  has_axis_ = true;
}
int64_t* ConstRangeInitializerConf::_RangeInitializerConf_::mutable_axis() {
  has_axis_ = true;
  return &axis_;
}


int ConstRangeInitializerConf::_RangeInitializerConf_::compare(const _RangeInitializerConf_& other) {
  if (!(has_start() == other.has_start())) {
    return has_start() < other.has_start() ? -1 : 1;
  } else if (!(start() == other.start())) {
    return start() < other.start() ? -1 : 1;
  }
  if (!(has_stride() == other.has_stride())) {
    return has_stride() < other.has_stride() ? -1 : 1;
  } else if (!(stride() == other.stride())) {
    return stride() < other.stride() ? -1 : 1;
  }
  if (!(has_axis() == other.has_axis())) {
    return has_axis() < other.has_axis() ? -1 : 1;
  } else if (!(axis() == other.axis())) {
    return axis() < other.axis() ? -1 : 1;
  }
  return 0;
}

bool ConstRangeInitializerConf::_RangeInitializerConf_::operator==(const _RangeInitializerConf_& other) const {
  return true
    && has_start() == other.has_start() 
    && start() == other.start()
    && has_stride() == other.has_stride() 
    && stride() == other.stride()
    && has_axis() == other.has_axis() 
    && axis() == other.axis()
  ;
}

std::size_t ConstRangeInitializerConf::_RangeInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_start() ? std::hash<double>()(start()) : 0)
    ^ (has_stride() ? std::hash<double>()(stride()) : 0)
    ^ (has_axis() ? std::hash<int64_t>()(axis()) : 0)
  ;
}

bool ConstRangeInitializerConf::_RangeInitializerConf_::operator<(const _RangeInitializerConf_& other) const {
  return false
    || !(has_start() == other.has_start()) ? 
      has_start() < other.has_start() : false
    || !(start() == other.start()) ? 
      start() < other.start() : false
    || !(has_stride() == other.has_stride()) ? 
      has_stride() < other.has_stride() : false
    || !(stride() == other.stride()) ? 
      stride() < other.stride() : false
    || !(has_axis() == other.has_axis()) ? 
      has_axis() < other.has_axis() : false
    || !(axis() == other.axis()) ? 
      axis() < other.axis() : false
  ;
}

using _RangeInitializerConf_ =  ConstRangeInitializerConf::_RangeInitializerConf_;
ConstRangeInitializerConf::ConstRangeInitializerConf(const ::std::shared_ptr<_RangeInitializerConf_>& data): data_(data) {}
ConstRangeInitializerConf::ConstRangeInitializerConf(): data_(::std::make_shared<_RangeInitializerConf_>()) {}
ConstRangeInitializerConf::ConstRangeInitializerConf(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf) {
  BuildFromProto(proto_rangeinitializerconf);
}
ConstRangeInitializerConf::ConstRangeInitializerConf(const ConstRangeInitializerConf&) = default;
ConstRangeInitializerConf::ConstRangeInitializerConf(ConstRangeInitializerConf&&) noexcept = default;
ConstRangeInitializerConf::~ConstRangeInitializerConf() = default;

void ConstRangeInitializerConf::ToProto(PbMessage* proto_rangeinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RangeInitializerConf*>(proto_rangeinitializerconf));
}
  
::std::string ConstRangeInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRangeInitializerConf::__Empty__() const {
  return !data_;
}

int ConstRangeInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"start", 1},
    {"stride", 2},
    {"axis", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRangeInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRangeInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRangeInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &start();
    case 2: return &stride();
    case 3: return &axis();
    default: return nullptr;
  }
}

// required or optional field start
bool ConstRangeInitializerConf::has_start() const {
  return __SharedPtrOrDefault__()->has_start();
}
const double& ConstRangeInitializerConf::start() const {
  return __SharedPtrOrDefault__()->start();
}
// used by pybind11 only
// required or optional field stride
bool ConstRangeInitializerConf::has_stride() const {
  return __SharedPtrOrDefault__()->has_stride();
}
const double& ConstRangeInitializerConf::stride() const {
  return __SharedPtrOrDefault__()->stride();
}
// used by pybind11 only
// required or optional field axis
bool ConstRangeInitializerConf::has_axis() const {
  return __SharedPtrOrDefault__()->has_axis();
}
const int64_t& ConstRangeInitializerConf::axis() const {
  return __SharedPtrOrDefault__()->axis();
}
// used by pybind11 only

::std::shared_ptr<ConstRangeInitializerConf> ConstRangeInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstRangeInitializerConf>(data_);
}
int64_t ConstRangeInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRangeInitializerConf::operator==(const ConstRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRangeInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRangeInitializerConf::operator<(const ConstRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RangeInitializerConf_>& ConstRangeInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RangeInitializerConf_> default_ptr = std::make_shared<_RangeInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RangeInitializerConf_>& ConstRangeInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RangeInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRangeInitializerConf
void ConstRangeInitializerConf::BuildFromProto(const PbMessage& proto_rangeinitializerconf) {
  data_ = ::std::make_shared<_RangeInitializerConf_>(dynamic_cast<const ::oneflow::RangeInitializerConf&>(proto_rangeinitializerconf));
}

RangeInitializerConf::RangeInitializerConf(const ::std::shared_ptr<_RangeInitializerConf_>& data)
  : ConstRangeInitializerConf(data) {}
RangeInitializerConf::RangeInitializerConf(const RangeInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RangeInitializerConf> resize
RangeInitializerConf::RangeInitializerConf(RangeInitializerConf&&) noexcept = default; 
RangeInitializerConf::RangeInitializerConf(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf) {
  InitFromProto(proto_rangeinitializerconf);
}
RangeInitializerConf::RangeInitializerConf() = default;

RangeInitializerConf::~RangeInitializerConf() = default;

void RangeInitializerConf::InitFromProto(const PbMessage& proto_rangeinitializerconf) {
  BuildFromProto(proto_rangeinitializerconf);
}
  
void* RangeInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_start();
    case 2: return mutable_stride();
    case 3: return mutable_axis();
    default: return nullptr;
  }
}

bool RangeInitializerConf::operator==(const RangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RangeInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RangeInitializerConf::operator<(const RangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RangeInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void RangeInitializerConf::CopyFrom(const RangeInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RangeInitializerConf& RangeInitializerConf::operator=(const RangeInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field start
void RangeInitializerConf::clear_start() {
  return __SharedPtr__()->clear_start();
}
void RangeInitializerConf::set_start(const double& value) {
  return __SharedPtr__()->set_start(value);
}
double* RangeInitializerConf::mutable_start() {
  return  __SharedPtr__()->mutable_start();
}
// required or optional field stride
void RangeInitializerConf::clear_stride() {
  return __SharedPtr__()->clear_stride();
}
void RangeInitializerConf::set_stride(const double& value) {
  return __SharedPtr__()->set_stride(value);
}
double* RangeInitializerConf::mutable_stride() {
  return  __SharedPtr__()->mutable_stride();
}
// required or optional field axis
void RangeInitializerConf::clear_axis() {
  return __SharedPtr__()->clear_axis();
}
void RangeInitializerConf::set_axis(const int64_t& value) {
  return __SharedPtr__()->set_axis(value);
}
int64_t* RangeInitializerConf::mutable_axis() {
  return  __SharedPtr__()->mutable_axis();
}

::std::shared_ptr<RangeInitializerConf> RangeInitializerConf::__SharedMutable__() {
  return ::std::make_shared<RangeInitializerConf>(__SharedPtr__());
}
ConstIntRangeInitializerConf::_IntRangeInitializerConf_::_IntRangeInitializerConf_() { Clear(); }
ConstIntRangeInitializerConf::_IntRangeInitializerConf_::_IntRangeInitializerConf_(const _IntRangeInitializerConf_& other) { CopyFrom(other); }
ConstIntRangeInitializerConf::_IntRangeInitializerConf_::_IntRangeInitializerConf_(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf) {
  InitFromProto(proto_intrangeinitializerconf);
}
ConstIntRangeInitializerConf::_IntRangeInitializerConf_::_IntRangeInitializerConf_(_IntRangeInitializerConf_&& other) = default;
ConstIntRangeInitializerConf::_IntRangeInitializerConf_::~_IntRangeInitializerConf_() = default;

void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::InitFromProto(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf) {
  Clear();
  // required_or_optional field: start
  if (proto_intrangeinitializerconf.has_start()) {
    set_start(proto_intrangeinitializerconf.start());
  }
  // required_or_optional field: stride
  if (proto_intrangeinitializerconf.has_stride()) {
    set_stride(proto_intrangeinitializerconf.stride());
  }
  // required_or_optional field: axis
  if (proto_intrangeinitializerconf.has_axis()) {
    set_axis(proto_intrangeinitializerconf.axis());
  }
    
}

void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::ToProto(::oneflow::IntRangeInitializerConf* proto_intrangeinitializerconf) const {
  proto_intrangeinitializerconf->Clear();
  // required_or_optional field: start
  if (this->has_start()) {
    proto_intrangeinitializerconf->set_start(start());
    }
  // required_or_optional field: stride
  if (this->has_stride()) {
    proto_intrangeinitializerconf->set_stride(stride());
    }
  // required_or_optional field: axis
  if (this->has_axis()) {
    proto_intrangeinitializerconf->set_axis(axis());
    }

}

::std::string ConstIntRangeInitializerConf::_IntRangeInitializerConf_::DebugString() const {
  ::oneflow::IntRangeInitializerConf proto_intrangeinitializerconf;
  this->ToProto(&proto_intrangeinitializerconf);
  return proto_intrangeinitializerconf.DebugString();
}

void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::Clear() {
  clear_start();
  clear_stride();
  clear_axis();
}

void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::CopyFrom(const _IntRangeInitializerConf_& other) {
  if (other.has_start()) {
    set_start(other.start());
  } else {
    clear_start();
  }
  if (other.has_stride()) {
    set_stride(other.stride());
  } else {
    clear_stride();
  }
  if (other.has_axis()) {
    set_axis(other.axis());
  } else {
    clear_axis();
  }
}


// optional field start
bool ConstIntRangeInitializerConf::_IntRangeInitializerConf_::has_start() const {
  return has_start_;
}
const int64_t& ConstIntRangeInitializerConf::_IntRangeInitializerConf_::start() const {
  if (has_start_) { return start_; }
  static const int64_t default_static_value =
    int64_t(0);
  return default_static_value;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::clear_start() {
  has_start_ = false;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::set_start(const int64_t& value) {
  start_ = value;
  has_start_ = true;
}
int64_t* ConstIntRangeInitializerConf::_IntRangeInitializerConf_::mutable_start() {
  has_start_ = true;
  return &start_;
}

// optional field stride
bool ConstIntRangeInitializerConf::_IntRangeInitializerConf_::has_stride() const {
  return has_stride_;
}
const int64_t& ConstIntRangeInitializerConf::_IntRangeInitializerConf_::stride() const {
  if (has_stride_) { return stride_; }
  static const int64_t default_static_value =
    int64_t(1);
  return default_static_value;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::clear_stride() {
  has_stride_ = false;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::set_stride(const int64_t& value) {
  stride_ = value;
  has_stride_ = true;
}
int64_t* ConstIntRangeInitializerConf::_IntRangeInitializerConf_::mutable_stride() {
  has_stride_ = true;
  return &stride_;
}

// optional field axis
bool ConstIntRangeInitializerConf::_IntRangeInitializerConf_::has_axis() const {
  return has_axis_;
}
const int64_t& ConstIntRangeInitializerConf::_IntRangeInitializerConf_::axis() const {
  if (has_axis_) { return axis_; }
  static const int64_t default_static_value =
    int64_t(-1);
  return default_static_value;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::clear_axis() {
  has_axis_ = false;
}
void ConstIntRangeInitializerConf::_IntRangeInitializerConf_::set_axis(const int64_t& value) {
  axis_ = value;
  has_axis_ = true;
}
int64_t* ConstIntRangeInitializerConf::_IntRangeInitializerConf_::mutable_axis() {
  has_axis_ = true;
  return &axis_;
}


int ConstIntRangeInitializerConf::_IntRangeInitializerConf_::compare(const _IntRangeInitializerConf_& other) {
  if (!(has_start() == other.has_start())) {
    return has_start() < other.has_start() ? -1 : 1;
  } else if (!(start() == other.start())) {
    return start() < other.start() ? -1 : 1;
  }
  if (!(has_stride() == other.has_stride())) {
    return has_stride() < other.has_stride() ? -1 : 1;
  } else if (!(stride() == other.stride())) {
    return stride() < other.stride() ? -1 : 1;
  }
  if (!(has_axis() == other.has_axis())) {
    return has_axis() < other.has_axis() ? -1 : 1;
  } else if (!(axis() == other.axis())) {
    return axis() < other.axis() ? -1 : 1;
  }
  return 0;
}

bool ConstIntRangeInitializerConf::_IntRangeInitializerConf_::operator==(const _IntRangeInitializerConf_& other) const {
  return true
    && has_start() == other.has_start() 
    && start() == other.start()
    && has_stride() == other.has_stride() 
    && stride() == other.stride()
    && has_axis() == other.has_axis() 
    && axis() == other.axis()
  ;
}

std::size_t ConstIntRangeInitializerConf::_IntRangeInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_start() ? std::hash<int64_t>()(start()) : 0)
    ^ (has_stride() ? std::hash<int64_t>()(stride()) : 0)
    ^ (has_axis() ? std::hash<int64_t>()(axis()) : 0)
  ;
}

bool ConstIntRangeInitializerConf::_IntRangeInitializerConf_::operator<(const _IntRangeInitializerConf_& other) const {
  return false
    || !(has_start() == other.has_start()) ? 
      has_start() < other.has_start() : false
    || !(start() == other.start()) ? 
      start() < other.start() : false
    || !(has_stride() == other.has_stride()) ? 
      has_stride() < other.has_stride() : false
    || !(stride() == other.stride()) ? 
      stride() < other.stride() : false
    || !(has_axis() == other.has_axis()) ? 
      has_axis() < other.has_axis() : false
    || !(axis() == other.axis()) ? 
      axis() < other.axis() : false
  ;
}

using _IntRangeInitializerConf_ =  ConstIntRangeInitializerConf::_IntRangeInitializerConf_;
ConstIntRangeInitializerConf::ConstIntRangeInitializerConf(const ::std::shared_ptr<_IntRangeInitializerConf_>& data): data_(data) {}
ConstIntRangeInitializerConf::ConstIntRangeInitializerConf(): data_(::std::make_shared<_IntRangeInitializerConf_>()) {}
ConstIntRangeInitializerConf::ConstIntRangeInitializerConf(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf) {
  BuildFromProto(proto_intrangeinitializerconf);
}
ConstIntRangeInitializerConf::ConstIntRangeInitializerConf(const ConstIntRangeInitializerConf&) = default;
ConstIntRangeInitializerConf::ConstIntRangeInitializerConf(ConstIntRangeInitializerConf&&) noexcept = default;
ConstIntRangeInitializerConf::~ConstIntRangeInitializerConf() = default;

void ConstIntRangeInitializerConf::ToProto(PbMessage* proto_intrangeinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::IntRangeInitializerConf*>(proto_intrangeinitializerconf));
}
  
::std::string ConstIntRangeInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstIntRangeInitializerConf::__Empty__() const {
  return !data_;
}

int ConstIntRangeInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"start", 1},
    {"stride", 2},
    {"axis", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstIntRangeInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstIntRangeInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstIntRangeInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &start();
    case 2: return &stride();
    case 3: return &axis();
    default: return nullptr;
  }
}

// required or optional field start
bool ConstIntRangeInitializerConf::has_start() const {
  return __SharedPtrOrDefault__()->has_start();
}
const int64_t& ConstIntRangeInitializerConf::start() const {
  return __SharedPtrOrDefault__()->start();
}
// used by pybind11 only
// required or optional field stride
bool ConstIntRangeInitializerConf::has_stride() const {
  return __SharedPtrOrDefault__()->has_stride();
}
const int64_t& ConstIntRangeInitializerConf::stride() const {
  return __SharedPtrOrDefault__()->stride();
}
// used by pybind11 only
// required or optional field axis
bool ConstIntRangeInitializerConf::has_axis() const {
  return __SharedPtrOrDefault__()->has_axis();
}
const int64_t& ConstIntRangeInitializerConf::axis() const {
  return __SharedPtrOrDefault__()->axis();
}
// used by pybind11 only

::std::shared_ptr<ConstIntRangeInitializerConf> ConstIntRangeInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstIntRangeInitializerConf>(data_);
}
int64_t ConstIntRangeInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstIntRangeInitializerConf::operator==(const ConstIntRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstIntRangeInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstIntRangeInitializerConf::operator<(const ConstIntRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_IntRangeInitializerConf_>& ConstIntRangeInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_IntRangeInitializerConf_> default_ptr = std::make_shared<_IntRangeInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_IntRangeInitializerConf_>& ConstIntRangeInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _IntRangeInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstIntRangeInitializerConf
void ConstIntRangeInitializerConf::BuildFromProto(const PbMessage& proto_intrangeinitializerconf) {
  data_ = ::std::make_shared<_IntRangeInitializerConf_>(dynamic_cast<const ::oneflow::IntRangeInitializerConf&>(proto_intrangeinitializerconf));
}

IntRangeInitializerConf::IntRangeInitializerConf(const ::std::shared_ptr<_IntRangeInitializerConf_>& data)
  : ConstIntRangeInitializerConf(data) {}
IntRangeInitializerConf::IntRangeInitializerConf(const IntRangeInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<IntRangeInitializerConf> resize
IntRangeInitializerConf::IntRangeInitializerConf(IntRangeInitializerConf&&) noexcept = default; 
IntRangeInitializerConf::IntRangeInitializerConf(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf) {
  InitFromProto(proto_intrangeinitializerconf);
}
IntRangeInitializerConf::IntRangeInitializerConf() = default;

IntRangeInitializerConf::~IntRangeInitializerConf() = default;

void IntRangeInitializerConf::InitFromProto(const PbMessage& proto_intrangeinitializerconf) {
  BuildFromProto(proto_intrangeinitializerconf);
}
  
void* IntRangeInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_start();
    case 2: return mutable_stride();
    case 3: return mutable_axis();
    default: return nullptr;
  }
}

bool IntRangeInitializerConf::operator==(const IntRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t IntRangeInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool IntRangeInitializerConf::operator<(const IntRangeInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void IntRangeInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void IntRangeInitializerConf::CopyFrom(const IntRangeInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
IntRangeInitializerConf& IntRangeInitializerConf::operator=(const IntRangeInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field start
void IntRangeInitializerConf::clear_start() {
  return __SharedPtr__()->clear_start();
}
void IntRangeInitializerConf::set_start(const int64_t& value) {
  return __SharedPtr__()->set_start(value);
}
int64_t* IntRangeInitializerConf::mutable_start() {
  return  __SharedPtr__()->mutable_start();
}
// required or optional field stride
void IntRangeInitializerConf::clear_stride() {
  return __SharedPtr__()->clear_stride();
}
void IntRangeInitializerConf::set_stride(const int64_t& value) {
  return __SharedPtr__()->set_stride(value);
}
int64_t* IntRangeInitializerConf::mutable_stride() {
  return  __SharedPtr__()->mutable_stride();
}
// required or optional field axis
void IntRangeInitializerConf::clear_axis() {
  return __SharedPtr__()->clear_axis();
}
void IntRangeInitializerConf::set_axis(const int64_t& value) {
  return __SharedPtr__()->set_axis(value);
}
int64_t* IntRangeInitializerConf::mutable_axis() {
  return  __SharedPtr__()->mutable_axis();
}

::std::shared_ptr<IntRangeInitializerConf> IntRangeInitializerConf::__SharedMutable__() {
  return ::std::make_shared<IntRangeInitializerConf>(__SharedPtr__());
}
ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::_VarianceScalingInitializerConf_() { Clear(); }
ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::_VarianceScalingInitializerConf_(const _VarianceScalingInitializerConf_& other) { CopyFrom(other); }
ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::_VarianceScalingInitializerConf_(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf) {
  InitFromProto(proto_variancescalinginitializerconf);
}
ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::_VarianceScalingInitializerConf_(_VarianceScalingInitializerConf_&& other) = default;
ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::~_VarianceScalingInitializerConf_() = default;

void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::InitFromProto(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf) {
  Clear();
  // required_or_optional field: scale
  if (proto_variancescalinginitializerconf.has_scale()) {
    set_scale(proto_variancescalinginitializerconf.scale());
  }
  // required_or_optional field: variance_norm
  if (proto_variancescalinginitializerconf.has_variance_norm()) {
  set_variance_norm(static_cast<std::remove_reference<std::remove_const<decltype(variance_norm())>::type>::type>(proto_variancescalinginitializerconf.variance_norm()));
  }
  // required_or_optional field: distribution
  if (proto_variancescalinginitializerconf.has_distribution()) {
  set_distribution(static_cast<std::remove_reference<std::remove_const<decltype(distribution())>::type>::type>(proto_variancescalinginitializerconf.distribution()));
  }
  // required_or_optional field: data_format
  if (proto_variancescalinginitializerconf.has_data_format()) {
    set_data_format(proto_variancescalinginitializerconf.data_format());
  }
    
}

void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::ToProto(::oneflow::VarianceScalingInitializerConf* proto_variancescalinginitializerconf) const {
  proto_variancescalinginitializerconf->Clear();
  // required_or_optional field: scale
  if (this->has_scale()) {
    proto_variancescalinginitializerconf->set_scale(scale());
    }
  // required_or_optional field: variance_norm
  if (this->has_variance_norm()) {
    proto_variancescalinginitializerconf->set_variance_norm(static_cast<std::remove_const<std::remove_reference<decltype(proto_variancescalinginitializerconf->variance_norm())>::type>::type>(variance_norm()));
    }
  // required_or_optional field: distribution
  if (this->has_distribution()) {
    proto_variancescalinginitializerconf->set_distribution(static_cast<std::remove_const<std::remove_reference<decltype(proto_variancescalinginitializerconf->distribution())>::type>::type>(distribution()));
    }
  // required_or_optional field: data_format
  if (this->has_data_format()) {
    proto_variancescalinginitializerconf->set_data_format(data_format());
    }

}

::std::string ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::DebugString() const {
  ::oneflow::VarianceScalingInitializerConf proto_variancescalinginitializerconf;
  this->ToProto(&proto_variancescalinginitializerconf);
  return proto_variancescalinginitializerconf.DebugString();
}

void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::Clear() {
  clear_scale();
  clear_variance_norm();
  clear_distribution();
  clear_data_format();
}

void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::CopyFrom(const _VarianceScalingInitializerConf_& other) {
  if (other.has_scale()) {
    set_scale(other.scale());
  } else {
    clear_scale();
  }
  if (other.has_variance_norm()) {
    set_variance_norm(other.variance_norm());
  } else {
    clear_variance_norm();
  }
  if (other.has_distribution()) {
    set_distribution(other.distribution());
  } else {
    clear_distribution();
  }
  if (other.has_data_format()) {
    set_data_format(other.data_format());
  } else {
    clear_data_format();
  }
}


// optional field scale
bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::has_scale() const {
  return has_scale_;
}
const float& ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::scale() const {
  if (has_scale_) { return scale_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::clear_scale() {
  has_scale_ = false;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::set_scale(const float& value) {
  scale_ = value;
  has_scale_ = true;
}
float* ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::mutable_scale() {
  has_scale_ = true;
  return &scale_;
}

// optional field variance_norm
bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::has_variance_norm() const {
  return has_variance_norm_;
}
const ::oneflow::cfg::VarianceNorm& ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::variance_norm() const {
  if (has_variance_norm_) { return variance_norm_; }
  static const ::oneflow::cfg::VarianceNorm default_static_value = ::oneflow::cfg::VarianceNorm();
  return default_static_value;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::clear_variance_norm() {
  has_variance_norm_ = false;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  variance_norm_ = value;
  has_variance_norm_ = true;
}
::oneflow::cfg::VarianceNorm* ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::mutable_variance_norm() {
  has_variance_norm_ = true;
  return &variance_norm_;
}

// optional field distribution
bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::has_distribution() const {
  return has_distribution_;
}
const ::oneflow::cfg::RandomDistribution& ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::distribution() const {
  if (has_distribution_) { return distribution_; }
  static const ::oneflow::cfg::RandomDistribution default_static_value = ::oneflow::cfg::RandomDistribution();
  return default_static_value;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::clear_distribution() {
  has_distribution_ = false;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::set_distribution(const ::oneflow::cfg::RandomDistribution& value) {
  distribution_ = value;
  has_distribution_ = true;
}
::oneflow::cfg::RandomDistribution* ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::mutable_distribution() {
  has_distribution_ = true;
  return &distribution_;
}

// optional field data_format
bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::has_data_format() const {
  return has_data_format_;
}
const ::std::string& ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::data_format() const {
  if (has_data_format_) { return data_format_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::clear_data_format() {
  has_data_format_ = false;
}
void ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::set_data_format(const ::std::string& value) {
  data_format_ = value;
  has_data_format_ = true;
}
::std::string* ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::mutable_data_format() {
  has_data_format_ = true;
  return &data_format_;
}


int ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::compare(const _VarianceScalingInitializerConf_& other) {
  if (!(has_scale() == other.has_scale())) {
    return has_scale() < other.has_scale() ? -1 : 1;
  } else if (!(scale() == other.scale())) {
    return scale() < other.scale() ? -1 : 1;
  }
  if (!(has_variance_norm() == other.has_variance_norm())) {
    return has_variance_norm() < other.has_variance_norm() ? -1 : 1;
  } else if (!(variance_norm() == other.variance_norm())) {
    return variance_norm() < other.variance_norm() ? -1 : 1;
  }
  if (!(has_distribution() == other.has_distribution())) {
    return has_distribution() < other.has_distribution() ? -1 : 1;
  } else if (!(distribution() == other.distribution())) {
    return distribution() < other.distribution() ? -1 : 1;
  }
  if (!(has_data_format() == other.has_data_format())) {
    return has_data_format() < other.has_data_format() ? -1 : 1;
  } else if (!(data_format() == other.data_format())) {
    return data_format() < other.data_format() ? -1 : 1;
  }
  return 0;
}

bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::operator==(const _VarianceScalingInitializerConf_& other) const {
  return true
    && has_scale() == other.has_scale() 
    && scale() == other.scale()
    && has_variance_norm() == other.has_variance_norm() 
    && variance_norm() == other.variance_norm()
    && has_distribution() == other.has_distribution() 
    && distribution() == other.distribution()
    && has_data_format() == other.has_data_format() 
    && data_format() == other.data_format()
  ;
}

std::size_t ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::__CalcHash__() const {
  return 0
    ^ (has_scale() ? std::hash<float>()(scale()) : 0)
    ^ (has_variance_norm() ? std::hash<::oneflow::cfg::VarianceNorm>()(variance_norm()) : 0)
    ^ (has_distribution() ? std::hash<::oneflow::cfg::RandomDistribution>()(distribution()) : 0)
    ^ (has_data_format() ? std::hash<::std::string>()(data_format()) : 0)
  ;
}

bool ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_::operator<(const _VarianceScalingInitializerConf_& other) const {
  return false
    || !(has_scale() == other.has_scale()) ? 
      has_scale() < other.has_scale() : false
    || !(scale() == other.scale()) ? 
      scale() < other.scale() : false
    || !(has_variance_norm() == other.has_variance_norm()) ? 
      has_variance_norm() < other.has_variance_norm() : false
    || !(variance_norm() == other.variance_norm()) ? 
      variance_norm() < other.variance_norm() : false
    || !(has_distribution() == other.has_distribution()) ? 
      has_distribution() < other.has_distribution() : false
    || !(distribution() == other.distribution()) ? 
      distribution() < other.distribution() : false
    || !(has_data_format() == other.has_data_format()) ? 
      has_data_format() < other.has_data_format() : false
    || !(data_format() == other.data_format()) ? 
      data_format() < other.data_format() : false
  ;
}

using _VarianceScalingInitializerConf_ =  ConstVarianceScalingInitializerConf::_VarianceScalingInitializerConf_;
ConstVarianceScalingInitializerConf::ConstVarianceScalingInitializerConf(const ::std::shared_ptr<_VarianceScalingInitializerConf_>& data): data_(data) {}
ConstVarianceScalingInitializerConf::ConstVarianceScalingInitializerConf(): data_(::std::make_shared<_VarianceScalingInitializerConf_>()) {}
ConstVarianceScalingInitializerConf::ConstVarianceScalingInitializerConf(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf) {
  BuildFromProto(proto_variancescalinginitializerconf);
}
ConstVarianceScalingInitializerConf::ConstVarianceScalingInitializerConf(const ConstVarianceScalingInitializerConf&) = default;
ConstVarianceScalingInitializerConf::ConstVarianceScalingInitializerConf(ConstVarianceScalingInitializerConf&&) noexcept = default;
ConstVarianceScalingInitializerConf::~ConstVarianceScalingInitializerConf() = default;

void ConstVarianceScalingInitializerConf::ToProto(PbMessage* proto_variancescalinginitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::VarianceScalingInitializerConf*>(proto_variancescalinginitializerconf));
}
  
::std::string ConstVarianceScalingInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstVarianceScalingInitializerConf::__Empty__() const {
  return !data_;
}

int ConstVarianceScalingInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"scale", 1},
    {"variance_norm", 2},
    {"distribution", 3},
    {"data_format", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstVarianceScalingInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstVarianceScalingInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::VarianceNorm),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RandomDistribution),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstVarianceScalingInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &scale();
    case 2: return &variance_norm();
    case 3: return &distribution();
    case 4: return &data_format();
    default: return nullptr;
  }
}

// required or optional field scale
bool ConstVarianceScalingInitializerConf::has_scale() const {
  return __SharedPtrOrDefault__()->has_scale();
}
const float& ConstVarianceScalingInitializerConf::scale() const {
  return __SharedPtrOrDefault__()->scale();
}
// used by pybind11 only
// required or optional field variance_norm
bool ConstVarianceScalingInitializerConf::has_variance_norm() const {
  return __SharedPtrOrDefault__()->has_variance_norm();
}
const ::oneflow::cfg::VarianceNorm& ConstVarianceScalingInitializerConf::variance_norm() const {
  return __SharedPtrOrDefault__()->variance_norm();
}
// used by pybind11 only
// required or optional field distribution
bool ConstVarianceScalingInitializerConf::has_distribution() const {
  return __SharedPtrOrDefault__()->has_distribution();
}
const ::oneflow::cfg::RandomDistribution& ConstVarianceScalingInitializerConf::distribution() const {
  return __SharedPtrOrDefault__()->distribution();
}
// used by pybind11 only
// required or optional field data_format
bool ConstVarianceScalingInitializerConf::has_data_format() const {
  return __SharedPtrOrDefault__()->has_data_format();
}
const ::std::string& ConstVarianceScalingInitializerConf::data_format() const {
  return __SharedPtrOrDefault__()->data_format();
}
// used by pybind11 only

::std::shared_ptr<ConstVarianceScalingInitializerConf> ConstVarianceScalingInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstVarianceScalingInitializerConf>(data_);
}
int64_t ConstVarianceScalingInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstVarianceScalingInitializerConf::operator==(const ConstVarianceScalingInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstVarianceScalingInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstVarianceScalingInitializerConf::operator<(const ConstVarianceScalingInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_VarianceScalingInitializerConf_>& ConstVarianceScalingInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_VarianceScalingInitializerConf_> default_ptr = std::make_shared<_VarianceScalingInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_VarianceScalingInitializerConf_>& ConstVarianceScalingInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _VarianceScalingInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstVarianceScalingInitializerConf
void ConstVarianceScalingInitializerConf::BuildFromProto(const PbMessage& proto_variancescalinginitializerconf) {
  data_ = ::std::make_shared<_VarianceScalingInitializerConf_>(dynamic_cast<const ::oneflow::VarianceScalingInitializerConf&>(proto_variancescalinginitializerconf));
}

VarianceScalingInitializerConf::VarianceScalingInitializerConf(const ::std::shared_ptr<_VarianceScalingInitializerConf_>& data)
  : ConstVarianceScalingInitializerConf(data) {}
VarianceScalingInitializerConf::VarianceScalingInitializerConf(const VarianceScalingInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<VarianceScalingInitializerConf> resize
VarianceScalingInitializerConf::VarianceScalingInitializerConf(VarianceScalingInitializerConf&&) noexcept = default; 
VarianceScalingInitializerConf::VarianceScalingInitializerConf(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf) {
  InitFromProto(proto_variancescalinginitializerconf);
}
VarianceScalingInitializerConf::VarianceScalingInitializerConf() = default;

VarianceScalingInitializerConf::~VarianceScalingInitializerConf() = default;

void VarianceScalingInitializerConf::InitFromProto(const PbMessage& proto_variancescalinginitializerconf) {
  BuildFromProto(proto_variancescalinginitializerconf);
}
  
void* VarianceScalingInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_scale();
    case 2: return mutable_variance_norm();
    case 3: return mutable_distribution();
    case 4: return mutable_data_format();
    default: return nullptr;
  }
}

bool VarianceScalingInitializerConf::operator==(const VarianceScalingInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t VarianceScalingInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool VarianceScalingInitializerConf::operator<(const VarianceScalingInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void VarianceScalingInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void VarianceScalingInitializerConf::CopyFrom(const VarianceScalingInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
VarianceScalingInitializerConf& VarianceScalingInitializerConf::operator=(const VarianceScalingInitializerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field scale
void VarianceScalingInitializerConf::clear_scale() {
  return __SharedPtr__()->clear_scale();
}
void VarianceScalingInitializerConf::set_scale(const float& value) {
  return __SharedPtr__()->set_scale(value);
}
float* VarianceScalingInitializerConf::mutable_scale() {
  return  __SharedPtr__()->mutable_scale();
}
// required or optional field variance_norm
void VarianceScalingInitializerConf::clear_variance_norm() {
  return __SharedPtr__()->clear_variance_norm();
}
void VarianceScalingInitializerConf::set_variance_norm(const ::oneflow::cfg::VarianceNorm& value) {
  return __SharedPtr__()->set_variance_norm(value);
}
::oneflow::cfg::VarianceNorm* VarianceScalingInitializerConf::mutable_variance_norm() {
  return  __SharedPtr__()->mutable_variance_norm();
}
// required or optional field distribution
void VarianceScalingInitializerConf::clear_distribution() {
  return __SharedPtr__()->clear_distribution();
}
void VarianceScalingInitializerConf::set_distribution(const ::oneflow::cfg::RandomDistribution& value) {
  return __SharedPtr__()->set_distribution(value);
}
::oneflow::cfg::RandomDistribution* VarianceScalingInitializerConf::mutable_distribution() {
  return  __SharedPtr__()->mutable_distribution();
}
// required or optional field data_format
void VarianceScalingInitializerConf::clear_data_format() {
  return __SharedPtr__()->clear_data_format();
}
void VarianceScalingInitializerConf::set_data_format(const ::std::string& value) {
  return __SharedPtr__()->set_data_format(value);
}
::std::string* VarianceScalingInitializerConf::mutable_data_format() {
  return  __SharedPtr__()->mutable_data_format();
}

::std::shared_ptr<VarianceScalingInitializerConf> VarianceScalingInitializerConf::__SharedMutable__() {
  return ::std::make_shared<VarianceScalingInitializerConf>(__SharedPtr__());
}
ConstEmptyInitializerConf::_EmptyInitializerConf_::_EmptyInitializerConf_() { Clear(); }
ConstEmptyInitializerConf::_EmptyInitializerConf_::_EmptyInitializerConf_(const _EmptyInitializerConf_& other) { CopyFrom(other); }
ConstEmptyInitializerConf::_EmptyInitializerConf_::_EmptyInitializerConf_(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf) {
  InitFromProto(proto_emptyinitializerconf);
}
ConstEmptyInitializerConf::_EmptyInitializerConf_::_EmptyInitializerConf_(_EmptyInitializerConf_&& other) = default;
ConstEmptyInitializerConf::_EmptyInitializerConf_::~_EmptyInitializerConf_() = default;

void ConstEmptyInitializerConf::_EmptyInitializerConf_::InitFromProto(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf) {
  Clear();
    
}

void ConstEmptyInitializerConf::_EmptyInitializerConf_::ToProto(::oneflow::EmptyInitializerConf* proto_emptyinitializerconf) const {
  proto_emptyinitializerconf->Clear();

}

::std::string ConstEmptyInitializerConf::_EmptyInitializerConf_::DebugString() const {
  ::oneflow::EmptyInitializerConf proto_emptyinitializerconf;
  this->ToProto(&proto_emptyinitializerconf);
  return proto_emptyinitializerconf.DebugString();
}

void ConstEmptyInitializerConf::_EmptyInitializerConf_::Clear() {
}

void ConstEmptyInitializerConf::_EmptyInitializerConf_::CopyFrom(const _EmptyInitializerConf_& other) {
}



int ConstEmptyInitializerConf::_EmptyInitializerConf_::compare(const _EmptyInitializerConf_& other) {
  return 0;
}

bool ConstEmptyInitializerConf::_EmptyInitializerConf_::operator==(const _EmptyInitializerConf_& other) const {
  return true
  ;
}

std::size_t ConstEmptyInitializerConf::_EmptyInitializerConf_::__CalcHash__() const {
  return 0
  ;
}

bool ConstEmptyInitializerConf::_EmptyInitializerConf_::operator<(const _EmptyInitializerConf_& other) const {
  return false
  ;
}

using _EmptyInitializerConf_ =  ConstEmptyInitializerConf::_EmptyInitializerConf_;
ConstEmptyInitializerConf::ConstEmptyInitializerConf(const ::std::shared_ptr<_EmptyInitializerConf_>& data): data_(data) {}
ConstEmptyInitializerConf::ConstEmptyInitializerConf(): data_(::std::make_shared<_EmptyInitializerConf_>()) {}
ConstEmptyInitializerConf::ConstEmptyInitializerConf(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf) {
  BuildFromProto(proto_emptyinitializerconf);
}
ConstEmptyInitializerConf::ConstEmptyInitializerConf(const ConstEmptyInitializerConf&) = default;
ConstEmptyInitializerConf::ConstEmptyInitializerConf(ConstEmptyInitializerConf&&) noexcept = default;
ConstEmptyInitializerConf::~ConstEmptyInitializerConf() = default;

void ConstEmptyInitializerConf::ToProto(PbMessage* proto_emptyinitializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::EmptyInitializerConf*>(proto_emptyinitializerconf));
}
  
::std::string ConstEmptyInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstEmptyInitializerConf::__Empty__() const {
  return !data_;
}

int ConstEmptyInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstEmptyInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstEmptyInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstEmptyInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstEmptyInitializerConf> ConstEmptyInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstEmptyInitializerConf>(data_);
}
int64_t ConstEmptyInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstEmptyInitializerConf::operator==(const ConstEmptyInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstEmptyInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstEmptyInitializerConf::operator<(const ConstEmptyInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_EmptyInitializerConf_>& ConstEmptyInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_EmptyInitializerConf_> default_ptr = std::make_shared<_EmptyInitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_EmptyInitializerConf_>& ConstEmptyInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _EmptyInitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstEmptyInitializerConf
void ConstEmptyInitializerConf::BuildFromProto(const PbMessage& proto_emptyinitializerconf) {
  data_ = ::std::make_shared<_EmptyInitializerConf_>(dynamic_cast<const ::oneflow::EmptyInitializerConf&>(proto_emptyinitializerconf));
}

EmptyInitializerConf::EmptyInitializerConf(const ::std::shared_ptr<_EmptyInitializerConf_>& data)
  : ConstEmptyInitializerConf(data) {}
EmptyInitializerConf::EmptyInitializerConf(const EmptyInitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<EmptyInitializerConf> resize
EmptyInitializerConf::EmptyInitializerConf(EmptyInitializerConf&&) noexcept = default; 
EmptyInitializerConf::EmptyInitializerConf(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf) {
  InitFromProto(proto_emptyinitializerconf);
}
EmptyInitializerConf::EmptyInitializerConf() = default;

EmptyInitializerConf::~EmptyInitializerConf() = default;

void EmptyInitializerConf::InitFromProto(const PbMessage& proto_emptyinitializerconf) {
  BuildFromProto(proto_emptyinitializerconf);
}
  
void* EmptyInitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool EmptyInitializerConf::operator==(const EmptyInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t EmptyInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool EmptyInitializerConf::operator<(const EmptyInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void EmptyInitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void EmptyInitializerConf::CopyFrom(const EmptyInitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
EmptyInitializerConf& EmptyInitializerConf::operator=(const EmptyInitializerConf& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<EmptyInitializerConf> EmptyInitializerConf::__SharedMutable__() {
  return ::std::make_shared<EmptyInitializerConf>(__SharedPtr__());
}
ConstInitializerConf::_InitializerConf_::_InitializerConf_() { Clear(); }
ConstInitializerConf::_InitializerConf_::_InitializerConf_(const _InitializerConf_& other) { CopyFrom(other); }
ConstInitializerConf::_InitializerConf_::_InitializerConf_(const ::oneflow::InitializerConf& proto_initializerconf) {
  InitFromProto(proto_initializerconf);
}
ConstInitializerConf::_InitializerConf_::_InitializerConf_(_InitializerConf_&& other) = default;
ConstInitializerConf::_InitializerConf_::~_InitializerConf_() = default;

void ConstInitializerConf::_InitializerConf_::InitFromProto(const ::oneflow::InitializerConf& proto_initializerconf) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_initializerconf.type_case()));
  switch (type_case) {
    case kConstantConf: {
      *mutable_constant_conf() = ::oneflow::cfg::ConstantInitializerConf(proto_initializerconf.constant_conf());
      break;
  }
    case kConstantIntConf: {
      *mutable_constant_int_conf() = ::oneflow::cfg::ConstantIntInitializerConf(proto_initializerconf.constant_int_conf());
      break;
  }
    case kRandomUniformConf: {
      *mutable_random_uniform_conf() = ::oneflow::cfg::RandomUniformInitializerConf(proto_initializerconf.random_uniform_conf());
      break;
  }
    case kRandomUniformIntConf: {
      *mutable_random_uniform_int_conf() = ::oneflow::cfg::RandomUniformIntInitializerConf(proto_initializerconf.random_uniform_int_conf());
      break;
  }
    case kRandomNormalConf: {
      *mutable_random_normal_conf() = ::oneflow::cfg::RandomNormalInitializerConf(proto_initializerconf.random_normal_conf());
      break;
  }
    case kTruncatedNormalConf: {
      *mutable_truncated_normal_conf() = ::oneflow::cfg::TruncatedNormalInitializerConf(proto_initializerconf.truncated_normal_conf());
      break;
  }
    case kXavierConf: {
      *mutable_xavier_conf() = ::oneflow::cfg::XavierInitializerConf(proto_initializerconf.xavier_conf());
      break;
  }
    case kMsraConf: {
      *mutable_msra_conf() = ::oneflow::cfg::MsraInitializerConf(proto_initializerconf.msra_conf());
      break;
  }
    case kRangeConf: {
      *mutable_range_conf() = ::oneflow::cfg::RangeInitializerConf(proto_initializerconf.range_conf());
      break;
  }
    case kIntRangeConf: {
      *mutable_int_range_conf() = ::oneflow::cfg::IntRangeInitializerConf(proto_initializerconf.int_range_conf());
      break;
  }
    case kVarianceScalingConf: {
      *mutable_variance_scaling_conf() = ::oneflow::cfg::VarianceScalingInitializerConf(proto_initializerconf.variance_scaling_conf());
      break;
  }
    case kEmptyConf: {
      *mutable_empty_conf() = ::oneflow::cfg::EmptyInitializerConf(proto_initializerconf.empty_conf());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstInitializerConf::_InitializerConf_::ToProto(::oneflow::InitializerConf* proto_initializerconf) const {
  proto_initializerconf->Clear();

  // oneof field: type
  ::oneflow::InitializerConf::TypeCase type_case = ::oneflow::InitializerConf::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::InitializerConf::kConstantConf: {
      ::oneflow::ConstantInitializerConf of_proto_constant_conf;
      constant_conf().ToProto(&of_proto_constant_conf);
      proto_initializerconf->mutable_constant_conf()->CopyFrom(of_proto_constant_conf);
      break;
    }
    case ::oneflow::InitializerConf::kConstantIntConf: {
      ::oneflow::ConstantIntInitializerConf of_proto_constant_int_conf;
      constant_int_conf().ToProto(&of_proto_constant_int_conf);
      proto_initializerconf->mutable_constant_int_conf()->CopyFrom(of_proto_constant_int_conf);
      break;
    }
    case ::oneflow::InitializerConf::kRandomUniformConf: {
      ::oneflow::RandomUniformInitializerConf of_proto_random_uniform_conf;
      random_uniform_conf().ToProto(&of_proto_random_uniform_conf);
      proto_initializerconf->mutable_random_uniform_conf()->CopyFrom(of_proto_random_uniform_conf);
      break;
    }
    case ::oneflow::InitializerConf::kRandomUniformIntConf: {
      ::oneflow::RandomUniformIntInitializerConf of_proto_random_uniform_int_conf;
      random_uniform_int_conf().ToProto(&of_proto_random_uniform_int_conf);
      proto_initializerconf->mutable_random_uniform_int_conf()->CopyFrom(of_proto_random_uniform_int_conf);
      break;
    }
    case ::oneflow::InitializerConf::kRandomNormalConf: {
      ::oneflow::RandomNormalInitializerConf of_proto_random_normal_conf;
      random_normal_conf().ToProto(&of_proto_random_normal_conf);
      proto_initializerconf->mutable_random_normal_conf()->CopyFrom(of_proto_random_normal_conf);
      break;
    }
    case ::oneflow::InitializerConf::kTruncatedNormalConf: {
      ::oneflow::TruncatedNormalInitializerConf of_proto_truncated_normal_conf;
      truncated_normal_conf().ToProto(&of_proto_truncated_normal_conf);
      proto_initializerconf->mutable_truncated_normal_conf()->CopyFrom(of_proto_truncated_normal_conf);
      break;
    }
    case ::oneflow::InitializerConf::kXavierConf: {
      ::oneflow::XavierInitializerConf of_proto_xavier_conf;
      xavier_conf().ToProto(&of_proto_xavier_conf);
      proto_initializerconf->mutable_xavier_conf()->CopyFrom(of_proto_xavier_conf);
      break;
    }
    case ::oneflow::InitializerConf::kMsraConf: {
      ::oneflow::MsraInitializerConf of_proto_msra_conf;
      msra_conf().ToProto(&of_proto_msra_conf);
      proto_initializerconf->mutable_msra_conf()->CopyFrom(of_proto_msra_conf);
      break;
    }
    case ::oneflow::InitializerConf::kRangeConf: {
      ::oneflow::RangeInitializerConf of_proto_range_conf;
      range_conf().ToProto(&of_proto_range_conf);
      proto_initializerconf->mutable_range_conf()->CopyFrom(of_proto_range_conf);
      break;
    }
    case ::oneflow::InitializerConf::kIntRangeConf: {
      ::oneflow::IntRangeInitializerConf of_proto_int_range_conf;
      int_range_conf().ToProto(&of_proto_int_range_conf);
      proto_initializerconf->mutable_int_range_conf()->CopyFrom(of_proto_int_range_conf);
      break;
    }
    case ::oneflow::InitializerConf::kVarianceScalingConf: {
      ::oneflow::VarianceScalingInitializerConf of_proto_variance_scaling_conf;
      variance_scaling_conf().ToProto(&of_proto_variance_scaling_conf);
      proto_initializerconf->mutable_variance_scaling_conf()->CopyFrom(of_proto_variance_scaling_conf);
      break;
    }
    case ::oneflow::InitializerConf::kEmptyConf: {
      ::oneflow::EmptyInitializerConf of_proto_empty_conf;
      empty_conf().ToProto(&of_proto_empty_conf);
      proto_initializerconf->mutable_empty_conf()->CopyFrom(of_proto_empty_conf);
      break;
    }
    case ::oneflow::InitializerConf::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstInitializerConf::_InitializerConf_::DebugString() const {
  ::oneflow::InitializerConf proto_initializerconf;
  this->ToProto(&proto_initializerconf);
  return proto_initializerconf.DebugString();
}

void ConstInitializerConf::_InitializerConf_::Clear() {
  clear_type();
}

void ConstInitializerConf::_InitializerConf_::CopyFrom(const _InitializerConf_& other) {
  type_copy_from(other);
}


// oneof field type: constant_conf
bool ConstInitializerConf::_InitializerConf_::has_constant_conf() const {
  return type_case() == kConstantConf;
}
void ConstInitializerConf::_InitializerConf_::clear_constant_conf() {
  if (has_constant_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ConstantInitializerConf& ConstInitializerConf::_InitializerConf_::constant_conf() const {
  if (has_constant_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>*>(&(type_.constant_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::ConstantInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ConstantInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_constant_conf() {
  if(!has_constant_conf()) {
    clear_type();
    new (&(type_.constant_conf_)) ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>(new ::oneflow::cfg::ConstantInitializerConf());
  }
  type_case_ = kConstantConf;
  ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>*>(&(type_.constant_conf_));
  return  (*ptr).get();
}

// oneof field type: constant_int_conf
bool ConstInitializerConf::_InitializerConf_::has_constant_int_conf() const {
  return type_case() == kConstantIntConf;
}
void ConstInitializerConf::_InitializerConf_::clear_constant_int_conf() {
  if (has_constant_int_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_int_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ConstantIntInitializerConf& ConstInitializerConf::_InitializerConf_::constant_int_conf() const {
  if (has_constant_int_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>*>(&(type_.constant_int_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::ConstantIntInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ConstantIntInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_constant_int_conf() {
  if(!has_constant_int_conf()) {
    clear_type();
    new (&(type_.constant_int_conf_)) ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>(new ::oneflow::cfg::ConstantIntInitializerConf());
  }
  type_case_ = kConstantIntConf;
  ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>*>(&(type_.constant_int_conf_));
  return  (*ptr).get();
}

// oneof field type: random_uniform_conf
bool ConstInitializerConf::_InitializerConf_::has_random_uniform_conf() const {
  return type_case() == kRandomUniformConf;
}
void ConstInitializerConf::_InitializerConf_::clear_random_uniform_conf() {
  if (has_random_uniform_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_uniform_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::RandomUniformInitializerConf& ConstInitializerConf::_InitializerConf_::random_uniform_conf() const {
  if (has_random_uniform_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>*>(&(type_.random_uniform_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::RandomUniformInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RandomUniformInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_random_uniform_conf() {
  if(!has_random_uniform_conf()) {
    clear_type();
    new (&(type_.random_uniform_conf_)) ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>(new ::oneflow::cfg::RandomUniformInitializerConf());
  }
  type_case_ = kRandomUniformConf;
  ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>*>(&(type_.random_uniform_conf_));
  return  (*ptr).get();
}

// oneof field type: random_uniform_int_conf
bool ConstInitializerConf::_InitializerConf_::has_random_uniform_int_conf() const {
  return type_case() == kRandomUniformIntConf;
}
void ConstInitializerConf::_InitializerConf_::clear_random_uniform_int_conf() {
  if (has_random_uniform_int_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_uniform_int_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::RandomUniformIntInitializerConf& ConstInitializerConf::_InitializerConf_::random_uniform_int_conf() const {
  if (has_random_uniform_int_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>*>(&(type_.random_uniform_int_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::RandomUniformIntInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RandomUniformIntInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_random_uniform_int_conf() {
  if(!has_random_uniform_int_conf()) {
    clear_type();
    new (&(type_.random_uniform_int_conf_)) ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>(new ::oneflow::cfg::RandomUniformIntInitializerConf());
  }
  type_case_ = kRandomUniformIntConf;
  ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>*>(&(type_.random_uniform_int_conf_));
  return  (*ptr).get();
}

// oneof field type: random_normal_conf
bool ConstInitializerConf::_InitializerConf_::has_random_normal_conf() const {
  return type_case() == kRandomNormalConf;
}
void ConstInitializerConf::_InitializerConf_::clear_random_normal_conf() {
  if (has_random_normal_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_normal_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::RandomNormalInitializerConf& ConstInitializerConf::_InitializerConf_::random_normal_conf() const {
  if (has_random_normal_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>*>(&(type_.random_normal_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::RandomNormalInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RandomNormalInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_random_normal_conf() {
  if(!has_random_normal_conf()) {
    clear_type();
    new (&(type_.random_normal_conf_)) ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>(new ::oneflow::cfg::RandomNormalInitializerConf());
  }
  type_case_ = kRandomNormalConf;
  ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>*>(&(type_.random_normal_conf_));
  return  (*ptr).get();
}

// oneof field type: truncated_normal_conf
bool ConstInitializerConf::_InitializerConf_::has_truncated_normal_conf() const {
  return type_case() == kTruncatedNormalConf;
}
void ConstInitializerConf::_InitializerConf_::clear_truncated_normal_conf() {
  if (has_truncated_normal_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.truncated_normal_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TruncatedNormalInitializerConf& ConstInitializerConf::_InitializerConf_::truncated_normal_conf() const {
  if (has_truncated_normal_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>*>(&(type_.truncated_normal_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::TruncatedNormalInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::TruncatedNormalInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_truncated_normal_conf() {
  if(!has_truncated_normal_conf()) {
    clear_type();
    new (&(type_.truncated_normal_conf_)) ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>(new ::oneflow::cfg::TruncatedNormalInitializerConf());
  }
  type_case_ = kTruncatedNormalConf;
  ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>*>(&(type_.truncated_normal_conf_));
  return  (*ptr).get();
}

// oneof field type: xavier_conf
bool ConstInitializerConf::_InitializerConf_::has_xavier_conf() const {
  return type_case() == kXavierConf;
}
void ConstInitializerConf::_InitializerConf_::clear_xavier_conf() {
  if (has_xavier_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.xavier_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::XavierInitializerConf& ConstInitializerConf::_InitializerConf_::xavier_conf() const {
  if (has_xavier_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>*>(&(type_.xavier_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::XavierInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::XavierInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_xavier_conf() {
  if(!has_xavier_conf()) {
    clear_type();
    new (&(type_.xavier_conf_)) ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>(new ::oneflow::cfg::XavierInitializerConf());
  }
  type_case_ = kXavierConf;
  ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>*>(&(type_.xavier_conf_));
  return  (*ptr).get();
}

// oneof field type: msra_conf
bool ConstInitializerConf::_InitializerConf_::has_msra_conf() const {
  return type_case() == kMsraConf;
}
void ConstInitializerConf::_InitializerConf_::clear_msra_conf() {
  if (has_msra_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.msra_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::MsraInitializerConf& ConstInitializerConf::_InitializerConf_::msra_conf() const {
  if (has_msra_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>*>(&(type_.msra_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::MsraInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::MsraInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_msra_conf() {
  if(!has_msra_conf()) {
    clear_type();
    new (&(type_.msra_conf_)) ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>(new ::oneflow::cfg::MsraInitializerConf());
  }
  type_case_ = kMsraConf;
  ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>*>(&(type_.msra_conf_));
  return  (*ptr).get();
}

// oneof field type: range_conf
bool ConstInitializerConf::_InitializerConf_::has_range_conf() const {
  return type_case() == kRangeConf;
}
void ConstInitializerConf::_InitializerConf_::clear_range_conf() {
  if (has_range_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.range_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::RangeInitializerConf& ConstInitializerConf::_InitializerConf_::range_conf() const {
  if (has_range_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>*>(&(type_.range_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::RangeInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RangeInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_range_conf() {
  if(!has_range_conf()) {
    clear_type();
    new (&(type_.range_conf_)) ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>(new ::oneflow::cfg::RangeInitializerConf());
  }
  type_case_ = kRangeConf;
  ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>*>(&(type_.range_conf_));
  return  (*ptr).get();
}

// oneof field type: int_range_conf
bool ConstInitializerConf::_InitializerConf_::has_int_range_conf() const {
  return type_case() == kIntRangeConf;
}
void ConstInitializerConf::_InitializerConf_::clear_int_range_conf() {
  if (has_int_range_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.int_range_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::IntRangeInitializerConf& ConstInitializerConf::_InitializerConf_::int_range_conf() const {
  if (has_int_range_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>*>(&(type_.int_range_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::IntRangeInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::IntRangeInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_int_range_conf() {
  if(!has_int_range_conf()) {
    clear_type();
    new (&(type_.int_range_conf_)) ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>(new ::oneflow::cfg::IntRangeInitializerConf());
  }
  type_case_ = kIntRangeConf;
  ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>*>(&(type_.int_range_conf_));
  return  (*ptr).get();
}

// oneof field type: variance_scaling_conf
bool ConstInitializerConf::_InitializerConf_::has_variance_scaling_conf() const {
  return type_case() == kVarianceScalingConf;
}
void ConstInitializerConf::_InitializerConf_::clear_variance_scaling_conf() {
  if (has_variance_scaling_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.variance_scaling_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::VarianceScalingInitializerConf& ConstInitializerConf::_InitializerConf_::variance_scaling_conf() const {
  if (has_variance_scaling_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>*>(&(type_.variance_scaling_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::VarianceScalingInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::VarianceScalingInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_variance_scaling_conf() {
  if(!has_variance_scaling_conf()) {
    clear_type();
    new (&(type_.variance_scaling_conf_)) ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>(new ::oneflow::cfg::VarianceScalingInitializerConf());
  }
  type_case_ = kVarianceScalingConf;
  ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>*>(&(type_.variance_scaling_conf_));
  return  (*ptr).get();
}

// oneof field type: empty_conf
bool ConstInitializerConf::_InitializerConf_::has_empty_conf() const {
  return type_case() == kEmptyConf;
}
void ConstInitializerConf::_InitializerConf_::clear_empty_conf() {
  if (has_empty_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.empty_conf_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::EmptyInitializerConf& ConstInitializerConf::_InitializerConf_::empty_conf() const {
  if (has_empty_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>*>(&(type_.empty_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::EmptyInitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::EmptyInitializerConf* ConstInitializerConf::_InitializerConf_::mutable_empty_conf() {
  if(!has_empty_conf()) {
    clear_type();
    new (&(type_.empty_conf_)) ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>(new ::oneflow::cfg::EmptyInitializerConf());
  }
  type_case_ = kEmptyConf;
  ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>*>(&(type_.empty_conf_));
  return  (*ptr).get();
}
ConstInitializerConf::TypeCase ConstInitializerConf::_InitializerConf_::type_case() const {
  return type_case_;
}
bool ConstInitializerConf::_InitializerConf_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstInitializerConf::_InitializerConf_::clear_type() {
  switch (type_case()) {
    case kConstantConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kConstantIntConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.constant_int_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRandomUniformConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_uniform_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRandomUniformIntConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_uniform_int_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRandomNormalConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.random_normal_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kTruncatedNormalConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.truncated_normal_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kXavierConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.xavier_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMsraConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.msra_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRangeConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.range_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kIntRangeConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.int_range_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kVarianceScalingConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.variance_scaling_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kEmptyConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.empty_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstInitializerConf::_InitializerConf_::type_copy_from(const _InitializerConf_& other) {
  switch (other.type_case()) {
    case kConstantConf: {
      mutable_constant_conf()->CopyFrom(other.constant_conf());
      break;
    }
    case kConstantIntConf: {
      mutable_constant_int_conf()->CopyFrom(other.constant_int_conf());
      break;
    }
    case kRandomUniformConf: {
      mutable_random_uniform_conf()->CopyFrom(other.random_uniform_conf());
      break;
    }
    case kRandomUniformIntConf: {
      mutable_random_uniform_int_conf()->CopyFrom(other.random_uniform_int_conf());
      break;
    }
    case kRandomNormalConf: {
      mutable_random_normal_conf()->CopyFrom(other.random_normal_conf());
      break;
    }
    case kTruncatedNormalConf: {
      mutable_truncated_normal_conf()->CopyFrom(other.truncated_normal_conf());
      break;
    }
    case kXavierConf: {
      mutable_xavier_conf()->CopyFrom(other.xavier_conf());
      break;
    }
    case kMsraConf: {
      mutable_msra_conf()->CopyFrom(other.msra_conf());
      break;
    }
    case kRangeConf: {
      mutable_range_conf()->CopyFrom(other.range_conf());
      break;
    }
    case kIntRangeConf: {
      mutable_int_range_conf()->CopyFrom(other.int_range_conf());
      break;
    }
    case kVarianceScalingConf: {
      mutable_variance_scaling_conf()->CopyFrom(other.variance_scaling_conf());
      break;
    }
    case kEmptyConf: {
      mutable_empty_conf()->CopyFrom(other.empty_conf());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstInitializerConf::_InitializerConf_::compare(const _InitializerConf_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kConstantConf: {
      if (!(constant_conf() == other.constant_conf())) {
        return constant_conf() < other.constant_conf() ? -1 : 1;
      }
      break;
    }
    case kConstantIntConf: {
      if (!(constant_int_conf() == other.constant_int_conf())) {
        return constant_int_conf() < other.constant_int_conf() ? -1 : 1;
      }
      break;
    }
    case kRandomUniformConf: {
      if (!(random_uniform_conf() == other.random_uniform_conf())) {
        return random_uniform_conf() < other.random_uniform_conf() ? -1 : 1;
      }
      break;
    }
    case kRandomUniformIntConf: {
      if (!(random_uniform_int_conf() == other.random_uniform_int_conf())) {
        return random_uniform_int_conf() < other.random_uniform_int_conf() ? -1 : 1;
      }
      break;
    }
    case kRandomNormalConf: {
      if (!(random_normal_conf() == other.random_normal_conf())) {
        return random_normal_conf() < other.random_normal_conf() ? -1 : 1;
      }
      break;
    }
    case kTruncatedNormalConf: {
      if (!(truncated_normal_conf() == other.truncated_normal_conf())) {
        return truncated_normal_conf() < other.truncated_normal_conf() ? -1 : 1;
      }
      break;
    }
    case kXavierConf: {
      if (!(xavier_conf() == other.xavier_conf())) {
        return xavier_conf() < other.xavier_conf() ? -1 : 1;
      }
      break;
    }
    case kMsraConf: {
      if (!(msra_conf() == other.msra_conf())) {
        return msra_conf() < other.msra_conf() ? -1 : 1;
      }
      break;
    }
    case kRangeConf: {
      if (!(range_conf() == other.range_conf())) {
        return range_conf() < other.range_conf() ? -1 : 1;
      }
      break;
    }
    case kIntRangeConf: {
      if (!(int_range_conf() == other.int_range_conf())) {
        return int_range_conf() < other.int_range_conf() ? -1 : 1;
      }
      break;
    }
    case kVarianceScalingConf: {
      if (!(variance_scaling_conf() == other.variance_scaling_conf())) {
        return variance_scaling_conf() < other.variance_scaling_conf() ? -1 : 1;
      }
      break;
    }
    case kEmptyConf: {
      if (!(empty_conf() == other.empty_conf())) {
        return empty_conf() < other.empty_conf() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstInitializerConf::_InitializerConf_::operator==(const _InitializerConf_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kConstantConf ? 
      constant_conf() == other.constant_conf() : true)
    && (type_case() == kConstantIntConf ? 
      constant_int_conf() == other.constant_int_conf() : true)
    && (type_case() == kRandomUniformConf ? 
      random_uniform_conf() == other.random_uniform_conf() : true)
    && (type_case() == kRandomUniformIntConf ? 
      random_uniform_int_conf() == other.random_uniform_int_conf() : true)
    && (type_case() == kRandomNormalConf ? 
      random_normal_conf() == other.random_normal_conf() : true)
    && (type_case() == kTruncatedNormalConf ? 
      truncated_normal_conf() == other.truncated_normal_conf() : true)
    && (type_case() == kXavierConf ? 
      xavier_conf() == other.xavier_conf() : true)
    && (type_case() == kMsraConf ? 
      msra_conf() == other.msra_conf() : true)
    && (type_case() == kRangeConf ? 
      range_conf() == other.range_conf() : true)
    && (type_case() == kIntRangeConf ? 
      int_range_conf() == other.int_range_conf() : true)
    && (type_case() == kVarianceScalingConf ? 
      variance_scaling_conf() == other.variance_scaling_conf() : true)
    && (type_case() == kEmptyConf ? 
      empty_conf() == other.empty_conf() : true)
  ;
}

std::size_t ConstInitializerConf::_InitializerConf_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_constant_conf() ? std::hash<::oneflow::cfg::ConstantInitializerConf>()(constant_conf()) : 0)
    ^ (has_constant_int_conf() ? std::hash<::oneflow::cfg::ConstantIntInitializerConf>()(constant_int_conf()) : 0)
    ^ (has_random_uniform_conf() ? std::hash<::oneflow::cfg::RandomUniformInitializerConf>()(random_uniform_conf()) : 0)
    ^ (has_random_uniform_int_conf() ? std::hash<::oneflow::cfg::RandomUniformIntInitializerConf>()(random_uniform_int_conf()) : 0)
    ^ (has_random_normal_conf() ? std::hash<::oneflow::cfg::RandomNormalInitializerConf>()(random_normal_conf()) : 0)
    ^ (has_truncated_normal_conf() ? std::hash<::oneflow::cfg::TruncatedNormalInitializerConf>()(truncated_normal_conf()) : 0)
    ^ (has_xavier_conf() ? std::hash<::oneflow::cfg::XavierInitializerConf>()(xavier_conf()) : 0)
    ^ (has_msra_conf() ? std::hash<::oneflow::cfg::MsraInitializerConf>()(msra_conf()) : 0)
    ^ (has_range_conf() ? std::hash<::oneflow::cfg::RangeInitializerConf>()(range_conf()) : 0)
    ^ (has_int_range_conf() ? std::hash<::oneflow::cfg::IntRangeInitializerConf>()(int_range_conf()) : 0)
    ^ (has_variance_scaling_conf() ? std::hash<::oneflow::cfg::VarianceScalingInitializerConf>()(variance_scaling_conf()) : 0)
    ^ (has_empty_conf() ? std::hash<::oneflow::cfg::EmptyInitializerConf>()(empty_conf()) : 0)
  ;
}

bool ConstInitializerConf::_InitializerConf_::operator<(const _InitializerConf_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kConstantConf) && 
      !(constant_conf() == other.constant_conf())) ? 
        constant_conf() < other.constant_conf() : false
    || ((type_case() == kConstantIntConf) && 
      !(constant_int_conf() == other.constant_int_conf())) ? 
        constant_int_conf() < other.constant_int_conf() : false
    || ((type_case() == kRandomUniformConf) && 
      !(random_uniform_conf() == other.random_uniform_conf())) ? 
        random_uniform_conf() < other.random_uniform_conf() : false
    || ((type_case() == kRandomUniformIntConf) && 
      !(random_uniform_int_conf() == other.random_uniform_int_conf())) ? 
        random_uniform_int_conf() < other.random_uniform_int_conf() : false
    || ((type_case() == kRandomNormalConf) && 
      !(random_normal_conf() == other.random_normal_conf())) ? 
        random_normal_conf() < other.random_normal_conf() : false
    || ((type_case() == kTruncatedNormalConf) && 
      !(truncated_normal_conf() == other.truncated_normal_conf())) ? 
        truncated_normal_conf() < other.truncated_normal_conf() : false
    || ((type_case() == kXavierConf) && 
      !(xavier_conf() == other.xavier_conf())) ? 
        xavier_conf() < other.xavier_conf() : false
    || ((type_case() == kMsraConf) && 
      !(msra_conf() == other.msra_conf())) ? 
        msra_conf() < other.msra_conf() : false
    || ((type_case() == kRangeConf) && 
      !(range_conf() == other.range_conf())) ? 
        range_conf() < other.range_conf() : false
    || ((type_case() == kIntRangeConf) && 
      !(int_range_conf() == other.int_range_conf())) ? 
        int_range_conf() < other.int_range_conf() : false
    || ((type_case() == kVarianceScalingConf) && 
      !(variance_scaling_conf() == other.variance_scaling_conf())) ? 
        variance_scaling_conf() < other.variance_scaling_conf() : false
    || ((type_case() == kEmptyConf) && 
      !(empty_conf() == other.empty_conf())) ? 
        empty_conf() < other.empty_conf() : false
  ;
}

using _InitializerConf_ =  ConstInitializerConf::_InitializerConf_;
ConstInitializerConf::ConstInitializerConf(const ::std::shared_ptr<_InitializerConf_>& data): data_(data) {}
ConstInitializerConf::ConstInitializerConf(): data_(::std::make_shared<_InitializerConf_>()) {}
ConstInitializerConf::ConstInitializerConf(const ::oneflow::InitializerConf& proto_initializerconf) {
  BuildFromProto(proto_initializerconf);
}
ConstInitializerConf::ConstInitializerConf(const ConstInitializerConf&) = default;
ConstInitializerConf::ConstInitializerConf(ConstInitializerConf&&) noexcept = default;
ConstInitializerConf::~ConstInitializerConf() = default;

void ConstInitializerConf::ToProto(PbMessage* proto_initializerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InitializerConf*>(proto_initializerconf));
}
  
::std::string ConstInitializerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInitializerConf::__Empty__() const {
  return !data_;
}

int ConstInitializerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"constant_conf", 1},
    {"constant_int_conf", 2},
    {"random_uniform_conf", 3},
    {"random_uniform_int_conf", 4},
    {"random_normal_conf", 5},
    {"truncated_normal_conf", 6},
    {"xavier_conf", 7},
    {"msra_conf", 8},
    {"range_conf", 9},
    {"int_range_conf", 10},
    {"variance_scaling_conf", 11},
    {"empty_conf", 12},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInitializerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInitializerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ConstantInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstConstantInitializerConf),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ConstantIntInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstConstantIntInitializerConf),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RandomUniformInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRandomUniformInitializerConf),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RandomUniformIntInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRandomUniformIntInitializerConf),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RandomNormalInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRandomNormalInitializerConf),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TruncatedNormalInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTruncatedNormalInitializerConf),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::XavierInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstXavierInitializerConf),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MsraInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMsraInitializerConf),
      };
      return type_indices;
    }
    case 9: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RangeInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRangeInitializerConf),
      };
      return type_indices;
    }
    case 10: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::IntRangeInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstIntRangeInitializerConf),
      };
      return type_indices;
    }
    case 11: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::VarianceScalingInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstVarianceScalingInitializerConf),
      };
      return type_indices;
    }
    case 12: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::EmptyInitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstEmptyInitializerConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInitializerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &constant_conf();
    case 2: return &constant_int_conf();
    case 3: return &random_uniform_conf();
    case 4: return &random_uniform_int_conf();
    case 5: return &random_normal_conf();
    case 6: return &truncated_normal_conf();
    case 7: return &xavier_conf();
    case 8: return &msra_conf();
    case 9: return &range_conf();
    case 10: return &int_range_conf();
    case 11: return &variance_scaling_conf();
    case 12: return &empty_conf();
    default: return nullptr;
  }
}

 // oneof field type: constant_conf
bool ConstInitializerConf::has_constant_conf() const {
  return __SharedPtrOrDefault__()->has_constant_conf();
}
const ::oneflow::cfg::ConstantInitializerConf& ConstInitializerConf::constant_conf() const {
  return __SharedPtrOrDefault__()->constant_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstConstantInitializerConf> ConstInitializerConf::shared_const_constant_conf() const {
  return constant_conf().__SharedConst__();
}
 // oneof field type: constant_int_conf
bool ConstInitializerConf::has_constant_int_conf() const {
  return __SharedPtrOrDefault__()->has_constant_int_conf();
}
const ::oneflow::cfg::ConstantIntInitializerConf& ConstInitializerConf::constant_int_conf() const {
  return __SharedPtrOrDefault__()->constant_int_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstConstantIntInitializerConf> ConstInitializerConf::shared_const_constant_int_conf() const {
  return constant_int_conf().__SharedConst__();
}
 // oneof field type: random_uniform_conf
bool ConstInitializerConf::has_random_uniform_conf() const {
  return __SharedPtrOrDefault__()->has_random_uniform_conf();
}
const ::oneflow::cfg::RandomUniformInitializerConf& ConstInitializerConf::random_uniform_conf() const {
  return __SharedPtrOrDefault__()->random_uniform_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRandomUniformInitializerConf> ConstInitializerConf::shared_const_random_uniform_conf() const {
  return random_uniform_conf().__SharedConst__();
}
 // oneof field type: random_uniform_int_conf
bool ConstInitializerConf::has_random_uniform_int_conf() const {
  return __SharedPtrOrDefault__()->has_random_uniform_int_conf();
}
const ::oneflow::cfg::RandomUniformIntInitializerConf& ConstInitializerConf::random_uniform_int_conf() const {
  return __SharedPtrOrDefault__()->random_uniform_int_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRandomUniformIntInitializerConf> ConstInitializerConf::shared_const_random_uniform_int_conf() const {
  return random_uniform_int_conf().__SharedConst__();
}
 // oneof field type: random_normal_conf
bool ConstInitializerConf::has_random_normal_conf() const {
  return __SharedPtrOrDefault__()->has_random_normal_conf();
}
const ::oneflow::cfg::RandomNormalInitializerConf& ConstInitializerConf::random_normal_conf() const {
  return __SharedPtrOrDefault__()->random_normal_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRandomNormalInitializerConf> ConstInitializerConf::shared_const_random_normal_conf() const {
  return random_normal_conf().__SharedConst__();
}
 // oneof field type: truncated_normal_conf
bool ConstInitializerConf::has_truncated_normal_conf() const {
  return __SharedPtrOrDefault__()->has_truncated_normal_conf();
}
const ::oneflow::cfg::TruncatedNormalInitializerConf& ConstInitializerConf::truncated_normal_conf() const {
  return __SharedPtrOrDefault__()->truncated_normal_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTruncatedNormalInitializerConf> ConstInitializerConf::shared_const_truncated_normal_conf() const {
  return truncated_normal_conf().__SharedConst__();
}
 // oneof field type: xavier_conf
bool ConstInitializerConf::has_xavier_conf() const {
  return __SharedPtrOrDefault__()->has_xavier_conf();
}
const ::oneflow::cfg::XavierInitializerConf& ConstInitializerConf::xavier_conf() const {
  return __SharedPtrOrDefault__()->xavier_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstXavierInitializerConf> ConstInitializerConf::shared_const_xavier_conf() const {
  return xavier_conf().__SharedConst__();
}
 // oneof field type: msra_conf
bool ConstInitializerConf::has_msra_conf() const {
  return __SharedPtrOrDefault__()->has_msra_conf();
}
const ::oneflow::cfg::MsraInitializerConf& ConstInitializerConf::msra_conf() const {
  return __SharedPtrOrDefault__()->msra_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMsraInitializerConf> ConstInitializerConf::shared_const_msra_conf() const {
  return msra_conf().__SharedConst__();
}
 // oneof field type: range_conf
bool ConstInitializerConf::has_range_conf() const {
  return __SharedPtrOrDefault__()->has_range_conf();
}
const ::oneflow::cfg::RangeInitializerConf& ConstInitializerConf::range_conf() const {
  return __SharedPtrOrDefault__()->range_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRangeInitializerConf> ConstInitializerConf::shared_const_range_conf() const {
  return range_conf().__SharedConst__();
}
 // oneof field type: int_range_conf
bool ConstInitializerConf::has_int_range_conf() const {
  return __SharedPtrOrDefault__()->has_int_range_conf();
}
const ::oneflow::cfg::IntRangeInitializerConf& ConstInitializerConf::int_range_conf() const {
  return __SharedPtrOrDefault__()->int_range_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstIntRangeInitializerConf> ConstInitializerConf::shared_const_int_range_conf() const {
  return int_range_conf().__SharedConst__();
}
 // oneof field type: variance_scaling_conf
bool ConstInitializerConf::has_variance_scaling_conf() const {
  return __SharedPtrOrDefault__()->has_variance_scaling_conf();
}
const ::oneflow::cfg::VarianceScalingInitializerConf& ConstInitializerConf::variance_scaling_conf() const {
  return __SharedPtrOrDefault__()->variance_scaling_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstVarianceScalingInitializerConf> ConstInitializerConf::shared_const_variance_scaling_conf() const {
  return variance_scaling_conf().__SharedConst__();
}
 // oneof field type: empty_conf
bool ConstInitializerConf::has_empty_conf() const {
  return __SharedPtrOrDefault__()->has_empty_conf();
}
const ::oneflow::cfg::EmptyInitializerConf& ConstInitializerConf::empty_conf() const {
  return __SharedPtrOrDefault__()->empty_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstEmptyInitializerConf> ConstInitializerConf::shared_const_empty_conf() const {
  return empty_conf().__SharedConst__();
}
ConstInitializerConf::TypeCase ConstInitializerConf::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstInitializerConf::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstInitializerConf> ConstInitializerConf::__SharedConst__() const {
  return ::std::make_shared<ConstInitializerConf>(data_);
}
int64_t ConstInitializerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInitializerConf::operator==(const ConstInitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInitializerConf::operator<(const ConstInitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InitializerConf_>& ConstInitializerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InitializerConf_> default_ptr = std::make_shared<_InitializerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_InitializerConf_>& ConstInitializerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _InitializerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInitializerConf
void ConstInitializerConf::BuildFromProto(const PbMessage& proto_initializerconf) {
  data_ = ::std::make_shared<_InitializerConf_>(dynamic_cast<const ::oneflow::InitializerConf&>(proto_initializerconf));
}

InitializerConf::InitializerConf(const ::std::shared_ptr<_InitializerConf_>& data)
  : ConstInitializerConf(data) {}
InitializerConf::InitializerConf(const InitializerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InitializerConf> resize
InitializerConf::InitializerConf(InitializerConf&&) noexcept = default; 
InitializerConf::InitializerConf(const ::oneflow::InitializerConf& proto_initializerconf) {
  InitFromProto(proto_initializerconf);
}
InitializerConf::InitializerConf() = default;

InitializerConf::~InitializerConf() = default;

void InitializerConf::InitFromProto(const PbMessage& proto_initializerconf) {
  BuildFromProto(proto_initializerconf);
}
  
void* InitializerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_constant_conf();
    case 2: return mutable_constant_int_conf();
    case 3: return mutable_random_uniform_conf();
    case 4: return mutable_random_uniform_int_conf();
    case 5: return mutable_random_normal_conf();
    case 6: return mutable_truncated_normal_conf();
    case 7: return mutable_xavier_conf();
    case 8: return mutable_msra_conf();
    case 9: return mutable_range_conf();
    case 10: return mutable_int_range_conf();
    case 11: return mutable_variance_scaling_conf();
    case 12: return mutable_empty_conf();
    default: return nullptr;
  }
}

bool InitializerConf::operator==(const InitializerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InitializerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InitializerConf::operator<(const InitializerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InitializerConf::Clear() {
  if (data_) { data_.reset(); }
}
void InitializerConf::CopyFrom(const InitializerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InitializerConf& InitializerConf::operator=(const InitializerConf& other) {
  CopyFrom(other);
  return *this;
}

void InitializerConf::clear_constant_conf() {
  return __SharedPtr__()->clear_constant_conf();
}
::oneflow::cfg::ConstantInitializerConf* InitializerConf::mutable_constant_conf() {
  return __SharedPtr__()->mutable_constant_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf> InitializerConf::shared_mutable_constant_conf() {
  return mutable_constant_conf()->__SharedMutable__();
}
void InitializerConf::clear_constant_int_conf() {
  return __SharedPtr__()->clear_constant_int_conf();
}
::oneflow::cfg::ConstantIntInitializerConf* InitializerConf::mutable_constant_int_conf() {
  return __SharedPtr__()->mutable_constant_int_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf> InitializerConf::shared_mutable_constant_int_conf() {
  return mutable_constant_int_conf()->__SharedMutable__();
}
void InitializerConf::clear_random_uniform_conf() {
  return __SharedPtr__()->clear_random_uniform_conf();
}
::oneflow::cfg::RandomUniformInitializerConf* InitializerConf::mutable_random_uniform_conf() {
  return __SharedPtr__()->mutable_random_uniform_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf> InitializerConf::shared_mutable_random_uniform_conf() {
  return mutable_random_uniform_conf()->__SharedMutable__();
}
void InitializerConf::clear_random_uniform_int_conf() {
  return __SharedPtr__()->clear_random_uniform_int_conf();
}
::oneflow::cfg::RandomUniformIntInitializerConf* InitializerConf::mutable_random_uniform_int_conf() {
  return __SharedPtr__()->mutable_random_uniform_int_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf> InitializerConf::shared_mutable_random_uniform_int_conf() {
  return mutable_random_uniform_int_conf()->__SharedMutable__();
}
void InitializerConf::clear_random_normal_conf() {
  return __SharedPtr__()->clear_random_normal_conf();
}
::oneflow::cfg::RandomNormalInitializerConf* InitializerConf::mutable_random_normal_conf() {
  return __SharedPtr__()->mutable_random_normal_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf> InitializerConf::shared_mutable_random_normal_conf() {
  return mutable_random_normal_conf()->__SharedMutable__();
}
void InitializerConf::clear_truncated_normal_conf() {
  return __SharedPtr__()->clear_truncated_normal_conf();
}
::oneflow::cfg::TruncatedNormalInitializerConf* InitializerConf::mutable_truncated_normal_conf() {
  return __SharedPtr__()->mutable_truncated_normal_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf> InitializerConf::shared_mutable_truncated_normal_conf() {
  return mutable_truncated_normal_conf()->__SharedMutable__();
}
void InitializerConf::clear_xavier_conf() {
  return __SharedPtr__()->clear_xavier_conf();
}
::oneflow::cfg::XavierInitializerConf* InitializerConf::mutable_xavier_conf() {
  return __SharedPtr__()->mutable_xavier_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::XavierInitializerConf> InitializerConf::shared_mutable_xavier_conf() {
  return mutable_xavier_conf()->__SharedMutable__();
}
void InitializerConf::clear_msra_conf() {
  return __SharedPtr__()->clear_msra_conf();
}
::oneflow::cfg::MsraInitializerConf* InitializerConf::mutable_msra_conf() {
  return __SharedPtr__()->mutable_msra_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MsraInitializerConf> InitializerConf::shared_mutable_msra_conf() {
  return mutable_msra_conf()->__SharedMutable__();
}
void InitializerConf::clear_range_conf() {
  return __SharedPtr__()->clear_range_conf();
}
::oneflow::cfg::RangeInitializerConf* InitializerConf::mutable_range_conf() {
  return __SharedPtr__()->mutable_range_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RangeInitializerConf> InitializerConf::shared_mutable_range_conf() {
  return mutable_range_conf()->__SharedMutable__();
}
void InitializerConf::clear_int_range_conf() {
  return __SharedPtr__()->clear_int_range_conf();
}
::oneflow::cfg::IntRangeInitializerConf* InitializerConf::mutable_int_range_conf() {
  return __SharedPtr__()->mutable_int_range_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf> InitializerConf::shared_mutable_int_range_conf() {
  return mutable_int_range_conf()->__SharedMutable__();
}
void InitializerConf::clear_variance_scaling_conf() {
  return __SharedPtr__()->clear_variance_scaling_conf();
}
::oneflow::cfg::VarianceScalingInitializerConf* InitializerConf::mutable_variance_scaling_conf() {
  return __SharedPtr__()->mutable_variance_scaling_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf> InitializerConf::shared_mutable_variance_scaling_conf() {
  return mutable_variance_scaling_conf()->__SharedMutable__();
}
void InitializerConf::clear_empty_conf() {
  return __SharedPtr__()->clear_empty_conf();
}
::oneflow::cfg::EmptyInitializerConf* InitializerConf::mutable_empty_conf() {
  return __SharedPtr__()->mutable_empty_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf> InitializerConf::shared_mutable_empty_conf() {
  return mutable_empty_conf()->__SharedMutable__();
}

::std::shared_ptr<InitializerConf> InitializerConf::__SharedMutable__() {
  return ::std::make_shared<InitializerConf>(__SharedPtr__());
}
ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::_InitializeWithSnapshotConf_() { Clear(); }
ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::_InitializeWithSnapshotConf_(const _InitializeWithSnapshotConf_& other) { CopyFrom(other); }
ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::_InitializeWithSnapshotConf_(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf) {
  InitFromProto(proto_initializewithsnapshotconf);
}
ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::_InitializeWithSnapshotConf_(_InitializeWithSnapshotConf_&& other) = default;
ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::~_InitializeWithSnapshotConf_() = default;

void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::InitFromProto(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf) {
  Clear();
  // required_or_optional field: path
  if (proto_initializewithsnapshotconf.has_path()) {
    set_path(proto_initializewithsnapshotconf.path());
  }
  // required_or_optional field: key
  if (proto_initializewithsnapshotconf.has_key()) {
    set_key(proto_initializewithsnapshotconf.key());
  }
    
}

void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::ToProto(::oneflow::InitializeWithSnapshotConf* proto_initializewithsnapshotconf) const {
  proto_initializewithsnapshotconf->Clear();
  // required_or_optional field: path
  if (this->has_path()) {
    proto_initializewithsnapshotconf->set_path(path());
    }
  // required_or_optional field: key
  if (this->has_key()) {
    proto_initializewithsnapshotconf->set_key(key());
    }

}

::std::string ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::DebugString() const {
  ::oneflow::InitializeWithSnapshotConf proto_initializewithsnapshotconf;
  this->ToProto(&proto_initializewithsnapshotconf);
  return proto_initializewithsnapshotconf.DebugString();
}

void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::Clear() {
  clear_path();
  clear_key();
}

void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::CopyFrom(const _InitializeWithSnapshotConf_& other) {
  if (other.has_path()) {
    set_path(other.path());
  } else {
    clear_path();
  }
  if (other.has_key()) {
    set_key(other.key());
  } else {
    clear_key();
  }
}


// optional field path
bool ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::has_path() const {
  return has_path_;
}
const ::std::string& ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::path() const {
  if (has_path_) { return path_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::clear_path() {
  has_path_ = false;
}
void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::set_path(const ::std::string& value) {
  path_ = value;
  has_path_ = true;
}
::std::string* ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::mutable_path() {
  has_path_ = true;
  return &path_;
}

// optional field key
bool ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::has_key() const {
  return has_key_;
}
const ::std::string& ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::key() const {
  if (has_key_) { return key_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::clear_key() {
  has_key_ = false;
}
void ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::set_key(const ::std::string& value) {
  key_ = value;
  has_key_ = true;
}
::std::string* ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::mutable_key() {
  has_key_ = true;
  return &key_;
}


int ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::compare(const _InitializeWithSnapshotConf_& other) {
  if (!(has_path() == other.has_path())) {
    return has_path() < other.has_path() ? -1 : 1;
  } else if (!(path() == other.path())) {
    return path() < other.path() ? -1 : 1;
  }
  if (!(has_key() == other.has_key())) {
    return has_key() < other.has_key() ? -1 : 1;
  } else if (!(key() == other.key())) {
    return key() < other.key() ? -1 : 1;
  }
  return 0;
}

bool ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::operator==(const _InitializeWithSnapshotConf_& other) const {
  return true
    && has_path() == other.has_path() 
    && path() == other.path()
    && has_key() == other.has_key() 
    && key() == other.key()
  ;
}

std::size_t ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::__CalcHash__() const {
  return 0
    ^ (has_path() ? std::hash<::std::string>()(path()) : 0)
    ^ (has_key() ? std::hash<::std::string>()(key()) : 0)
  ;
}

bool ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_::operator<(const _InitializeWithSnapshotConf_& other) const {
  return false
    || !(has_path() == other.has_path()) ? 
      has_path() < other.has_path() : false
    || !(path() == other.path()) ? 
      path() < other.path() : false
    || !(has_key() == other.has_key()) ? 
      has_key() < other.has_key() : false
    || !(key() == other.key()) ? 
      key() < other.key() : false
  ;
}

using _InitializeWithSnapshotConf_ =  ConstInitializeWithSnapshotConf::_InitializeWithSnapshotConf_;
ConstInitializeWithSnapshotConf::ConstInitializeWithSnapshotConf(const ::std::shared_ptr<_InitializeWithSnapshotConf_>& data): data_(data) {}
ConstInitializeWithSnapshotConf::ConstInitializeWithSnapshotConf(): data_(::std::make_shared<_InitializeWithSnapshotConf_>()) {}
ConstInitializeWithSnapshotConf::ConstInitializeWithSnapshotConf(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf) {
  BuildFromProto(proto_initializewithsnapshotconf);
}
ConstInitializeWithSnapshotConf::ConstInitializeWithSnapshotConf(const ConstInitializeWithSnapshotConf&) = default;
ConstInitializeWithSnapshotConf::ConstInitializeWithSnapshotConf(ConstInitializeWithSnapshotConf&&) noexcept = default;
ConstInitializeWithSnapshotConf::~ConstInitializeWithSnapshotConf() = default;

void ConstInitializeWithSnapshotConf::ToProto(PbMessage* proto_initializewithsnapshotconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InitializeWithSnapshotConf*>(proto_initializewithsnapshotconf));
}
  
::std::string ConstInitializeWithSnapshotConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInitializeWithSnapshotConf::__Empty__() const {
  return !data_;
}

int ConstInitializeWithSnapshotConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"path", 1},
    {"key", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInitializeWithSnapshotConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInitializeWithSnapshotConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInitializeWithSnapshotConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &path();
    case 2: return &key();
    default: return nullptr;
  }
}

// required or optional field path
bool ConstInitializeWithSnapshotConf::has_path() const {
  return __SharedPtrOrDefault__()->has_path();
}
const ::std::string& ConstInitializeWithSnapshotConf::path() const {
  return __SharedPtrOrDefault__()->path();
}
// used by pybind11 only
// required or optional field key
bool ConstInitializeWithSnapshotConf::has_key() const {
  return __SharedPtrOrDefault__()->has_key();
}
const ::std::string& ConstInitializeWithSnapshotConf::key() const {
  return __SharedPtrOrDefault__()->key();
}
// used by pybind11 only

::std::shared_ptr<ConstInitializeWithSnapshotConf> ConstInitializeWithSnapshotConf::__SharedConst__() const {
  return ::std::make_shared<ConstInitializeWithSnapshotConf>(data_);
}
int64_t ConstInitializeWithSnapshotConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInitializeWithSnapshotConf::operator==(const ConstInitializeWithSnapshotConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInitializeWithSnapshotConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInitializeWithSnapshotConf::operator<(const ConstInitializeWithSnapshotConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InitializeWithSnapshotConf_>& ConstInitializeWithSnapshotConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InitializeWithSnapshotConf_> default_ptr = std::make_shared<_InitializeWithSnapshotConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_InitializeWithSnapshotConf_>& ConstInitializeWithSnapshotConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _InitializeWithSnapshotConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInitializeWithSnapshotConf
void ConstInitializeWithSnapshotConf::BuildFromProto(const PbMessage& proto_initializewithsnapshotconf) {
  data_ = ::std::make_shared<_InitializeWithSnapshotConf_>(dynamic_cast<const ::oneflow::InitializeWithSnapshotConf&>(proto_initializewithsnapshotconf));
}

InitializeWithSnapshotConf::InitializeWithSnapshotConf(const ::std::shared_ptr<_InitializeWithSnapshotConf_>& data)
  : ConstInitializeWithSnapshotConf(data) {}
InitializeWithSnapshotConf::InitializeWithSnapshotConf(const InitializeWithSnapshotConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InitializeWithSnapshotConf> resize
InitializeWithSnapshotConf::InitializeWithSnapshotConf(InitializeWithSnapshotConf&&) noexcept = default; 
InitializeWithSnapshotConf::InitializeWithSnapshotConf(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf) {
  InitFromProto(proto_initializewithsnapshotconf);
}
InitializeWithSnapshotConf::InitializeWithSnapshotConf() = default;

InitializeWithSnapshotConf::~InitializeWithSnapshotConf() = default;

void InitializeWithSnapshotConf::InitFromProto(const PbMessage& proto_initializewithsnapshotconf) {
  BuildFromProto(proto_initializewithsnapshotconf);
}
  
void* InitializeWithSnapshotConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_path();
    case 2: return mutable_key();
    default: return nullptr;
  }
}

bool InitializeWithSnapshotConf::operator==(const InitializeWithSnapshotConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InitializeWithSnapshotConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InitializeWithSnapshotConf::operator<(const InitializeWithSnapshotConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InitializeWithSnapshotConf::Clear() {
  if (data_) { data_.reset(); }
}
void InitializeWithSnapshotConf::CopyFrom(const InitializeWithSnapshotConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InitializeWithSnapshotConf& InitializeWithSnapshotConf::operator=(const InitializeWithSnapshotConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field path
void InitializeWithSnapshotConf::clear_path() {
  return __SharedPtr__()->clear_path();
}
void InitializeWithSnapshotConf::set_path(const ::std::string& value) {
  return __SharedPtr__()->set_path(value);
}
::std::string* InitializeWithSnapshotConf::mutable_path() {
  return  __SharedPtr__()->mutable_path();
}
// required or optional field key
void InitializeWithSnapshotConf::clear_key() {
  return __SharedPtr__()->clear_key();
}
void InitializeWithSnapshotConf::set_key(const ::std::string& value) {
  return __SharedPtr__()->set_key(value);
}
::std::string* InitializeWithSnapshotConf::mutable_key() {
  return  __SharedPtr__()->mutable_key();
}

::std::shared_ptr<InitializeWithSnapshotConf> InitializeWithSnapshotConf::__SharedMutable__() {
  return ::std::make_shared<InitializeWithSnapshotConf>(__SharedPtr__());
}


}
} // namespace oneflow
