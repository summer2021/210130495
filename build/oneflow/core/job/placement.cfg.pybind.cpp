#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/common/shape.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.placement", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstLogicalBlobId> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstLogicalBlobId> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(const ::oneflow::cfg::LogicalBlobId&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstPlacementGroup> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstPlacementGroup> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(const ::oneflow::cfg::PlacementGroup&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::PlacementGroup> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::PlacementGroup> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>> registry(m, "Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstBlobPlacementGroup> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstBlobPlacementGroup> (Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_, std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_>> registry(m, "_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(const ::oneflow::cfg::BlobPlacementGroup&))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> (_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstParallelContext, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelContext>> registry(m, "ConstParallelContext");
    registry.def("__id__", &::oneflow::cfg::ParallelContext::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelContext::DebugString);
    registry.def("__repr__", &ConstParallelContext::DebugString);

    registry.def("has_parallel_id", &ConstParallelContext::has_parallel_id);
    registry.def("parallel_id", &ConstParallelContext::parallel_id);

    registry.def("has_parallel_num", &ConstParallelContext::has_parallel_num);
    registry.def("parallel_num", &ConstParallelContext::parallel_num);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelContext, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelContext>> registry(m, "ParallelContext");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelContext::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelContext::*)(const ConstParallelContext&))&::oneflow::cfg::ParallelContext::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelContext::*)(const ::oneflow::cfg::ParallelContext&))&::oneflow::cfg::ParallelContext::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelContext::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelContext::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelContext::DebugString);



    registry.def("has_parallel_id", &::oneflow::cfg::ParallelContext::has_parallel_id);
    registry.def("clear_parallel_id", &::oneflow::cfg::ParallelContext::clear_parallel_id);
    registry.def("parallel_id", &::oneflow::cfg::ParallelContext::parallel_id);
    registry.def("set_parallel_id", &::oneflow::cfg::ParallelContext::set_parallel_id);

    registry.def("has_parallel_num", &::oneflow::cfg::ParallelContext::has_parallel_num);
    registry.def("clear_parallel_num", &::oneflow::cfg::ParallelContext::clear_parallel_num);
    registry.def("parallel_num", &::oneflow::cfg::ParallelContext::parallel_num);
    registry.def("set_parallel_num", &::oneflow::cfg::ParallelContext::set_parallel_num);
  }
  {
    pybind11::class_<ConstParallelConf, ::oneflow::cfg::Message, std::shared_ptr<ConstParallelConf>> registry(m, "ConstParallelConf");
    registry.def("__id__", &::oneflow::cfg::ParallelConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstParallelConf::DebugString);
    registry.def("__repr__", &ConstParallelConf::DebugString);

    registry.def("device_name_size", &ConstParallelConf::device_name_size);
    registry.def("device_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (ConstParallelConf::*)() const)&ConstParallelConf::shared_const_device_name);
    registry.def("device_name", (const ::std::string& (ConstParallelConf::*)(::std::size_t) const)&ConstParallelConf::device_name);

    registry.def("has_device_tag", &ConstParallelConf::has_device_tag);
    registry.def("device_tag", &ConstParallelConf::device_tag);

    registry.def("has_hierarchy", &ConstParallelConf::has_hierarchy);
    registry.def("hierarchy", &ConstParallelConf::shared_const_hierarchy);
  }
  {
    pybind11::class_<::oneflow::cfg::ParallelConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ParallelConf>> registry(m, "ParallelConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ParallelConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelConf::*)(const ConstParallelConf&))&::oneflow::cfg::ParallelConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ParallelConf::*)(const ::oneflow::cfg::ParallelConf&))&::oneflow::cfg::ParallelConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ParallelConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ParallelConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ParallelConf::DebugString);



    registry.def("device_name_size", &::oneflow::cfg::ParallelConf::device_name_size);
    registry.def("clear_device_name", &::oneflow::cfg::ParallelConf::clear_device_name);
    registry.def("mutable_device_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ParallelConf::*)())&::oneflow::cfg::ParallelConf::shared_mutable_device_name);
    registry.def("device_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ParallelConf::*)() const)&::oneflow::cfg::ParallelConf::shared_const_device_name);
    registry.def("device_name", (const ::std::string& (::oneflow::cfg::ParallelConf::*)(::std::size_t) const)&::oneflow::cfg::ParallelConf::device_name);
    registry.def("add_device_name", &::oneflow::cfg::ParallelConf::add_device_name);
    registry.def("set_device_name", &::oneflow::cfg::ParallelConf::set_device_name);

    registry.def("has_device_tag", &::oneflow::cfg::ParallelConf::has_device_tag);
    registry.def("clear_device_tag", &::oneflow::cfg::ParallelConf::clear_device_tag);
    registry.def("device_tag", &::oneflow::cfg::ParallelConf::device_tag);
    registry.def("set_device_tag", &::oneflow::cfg::ParallelConf::set_device_tag);

    registry.def("has_hierarchy", &::oneflow::cfg::ParallelConf::has_hierarchy);
    registry.def("clear_hierarchy", &::oneflow::cfg::ParallelConf::clear_hierarchy);
    registry.def("hierarchy", &::oneflow::cfg::ParallelConf::shared_const_hierarchy);
    registry.def("mutable_hierarchy", &::oneflow::cfg::ParallelConf::shared_mutable_hierarchy);
  }
  {
    pybind11::class_<ConstOpNameSet, ::oneflow::cfg::Message, std::shared_ptr<ConstOpNameSet>> registry(m, "ConstOpNameSet");
    registry.def("__id__", &::oneflow::cfg::OpNameSet::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpNameSet::DebugString);
    registry.def("__repr__", &ConstOpNameSet::DebugString);

    registry.def("op_name_size", &ConstOpNameSet::op_name_size);
    registry.def("op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (ConstOpNameSet::*)() const)&ConstOpNameSet::shared_const_op_name);
    registry.def("op_name", (const ::std::string& (ConstOpNameSet::*)(::std::size_t) const)&ConstOpNameSet::op_name);
  }
  {
    pybind11::class_<::oneflow::cfg::OpNameSet, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpNameSet>> registry(m, "OpNameSet");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpNameSet::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameSet::*)(const ConstOpNameSet&))&::oneflow::cfg::OpNameSet::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameSet::*)(const ::oneflow::cfg::OpNameSet&))&::oneflow::cfg::OpNameSet::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpNameSet::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpNameSet::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpNameSet::DebugString);



    registry.def("op_name_size", &::oneflow::cfg::OpNameSet::op_name_size);
    registry.def("clear_op_name", &::oneflow::cfg::OpNameSet::clear_op_name);
    registry.def("mutable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpNameSet::*)())&::oneflow::cfg::OpNameSet::shared_mutable_op_name);
    registry.def("op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpNameSet::*)() const)&::oneflow::cfg::OpNameSet::shared_const_op_name);
    registry.def("op_name", (const ::std::string& (::oneflow::cfg::OpNameSet::*)(::std::size_t) const)&::oneflow::cfg::OpNameSet::op_name);
    registry.def("add_op_name", &::oneflow::cfg::OpNameSet::add_op_name);
    registry.def("set_op_name", &::oneflow::cfg::OpNameSet::set_op_name);
  }
  {
    pybind11::class_<ConstPlacementGroup, ::oneflow::cfg::Message, std::shared_ptr<ConstPlacementGroup>> registry(m, "ConstPlacementGroup");
    registry.def("__id__", &::oneflow::cfg::PlacementGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPlacementGroup::DebugString);
    registry.def("__repr__", &ConstPlacementGroup::DebugString);

    registry.def("has_op_set", &ConstPlacementGroup::has_op_set);
    registry.def("op_set", &ConstPlacementGroup::shared_const_op_set);

    registry.def("has_parallel_conf", &ConstPlacementGroup::has_parallel_conf);
    registry.def("parallel_conf", &ConstPlacementGroup::shared_const_parallel_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::PlacementGroup, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::PlacementGroup>> registry(m, "PlacementGroup");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::PlacementGroup::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::PlacementGroup::*)(const ConstPlacementGroup&))&::oneflow::cfg::PlacementGroup::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::PlacementGroup::*)(const ::oneflow::cfg::PlacementGroup&))&::oneflow::cfg::PlacementGroup::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::PlacementGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::PlacementGroup::DebugString);
    registry.def("__repr__", &::oneflow::cfg::PlacementGroup::DebugString);



    registry.def("has_op_set", &::oneflow::cfg::PlacementGroup::has_op_set);
    registry.def("clear_op_set", &::oneflow::cfg::PlacementGroup::clear_op_set);
    registry.def("op_set", &::oneflow::cfg::PlacementGroup::shared_const_op_set);
    registry.def("mutable_op_set", &::oneflow::cfg::PlacementGroup::shared_mutable_op_set);

    registry.def("has_parallel_conf", &::oneflow::cfg::PlacementGroup::has_parallel_conf);
    registry.def("clear_parallel_conf", &::oneflow::cfg::PlacementGroup::clear_parallel_conf);
    registry.def("parallel_conf", &::oneflow::cfg::PlacementGroup::shared_const_parallel_conf);
    registry.def("mutable_parallel_conf", &::oneflow::cfg::PlacementGroup::shared_mutable_parallel_conf);
  }
  {
    pybind11::class_<ConstBlobPlacementGroup, ::oneflow::cfg::Message, std::shared_ptr<ConstBlobPlacementGroup>> registry(m, "ConstBlobPlacementGroup");
    registry.def("__id__", &::oneflow::cfg::BlobPlacementGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBlobPlacementGroup::DebugString);
    registry.def("__repr__", &ConstBlobPlacementGroup::DebugString);

    registry.def("lbi_size", &ConstBlobPlacementGroup::lbi_size);
    registry.def("lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> (ConstBlobPlacementGroup::*)() const)&ConstBlobPlacementGroup::shared_const_lbi);
    registry.def("lbi", (::std::shared_ptr<ConstLogicalBlobId> (ConstBlobPlacementGroup::*)(::std::size_t) const)&ConstBlobPlacementGroup::shared_const_lbi);

    registry.def("has_parallel_conf", &ConstBlobPlacementGroup::has_parallel_conf);
    registry.def("parallel_conf", &ConstBlobPlacementGroup::shared_const_parallel_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::BlobPlacementGroup, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BlobPlacementGroup>> registry(m, "BlobPlacementGroup");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BlobPlacementGroup::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobPlacementGroup::*)(const ConstBlobPlacementGroup&))&::oneflow::cfg::BlobPlacementGroup::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BlobPlacementGroup::*)(const ::oneflow::cfg::BlobPlacementGroup&))&::oneflow::cfg::BlobPlacementGroup::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BlobPlacementGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BlobPlacementGroup::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BlobPlacementGroup::DebugString);



    registry.def("lbi_size", &::oneflow::cfg::BlobPlacementGroup::lbi_size);
    registry.def("clear_lbi", &::oneflow::cfg::BlobPlacementGroup::clear_lbi);
    registry.def("mutable_lbi", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> (::oneflow::cfg::BlobPlacementGroup::*)())&::oneflow::cfg::BlobPlacementGroup::shared_mutable_lbi);
    registry.def("lbi", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> (::oneflow::cfg::BlobPlacementGroup::*)() const)&::oneflow::cfg::BlobPlacementGroup::shared_const_lbi);
    registry.def("lbi", (::std::shared_ptr<ConstLogicalBlobId> (::oneflow::cfg::BlobPlacementGroup::*)(::std::size_t) const)&::oneflow::cfg::BlobPlacementGroup::shared_const_lbi);
    registry.def("mutable_lbi", (::std::shared_ptr<::oneflow::cfg::LogicalBlobId> (::oneflow::cfg::BlobPlacementGroup::*)(::std::size_t))&::oneflow::cfg::BlobPlacementGroup::shared_mutable_lbi);

    registry.def("has_parallel_conf", &::oneflow::cfg::BlobPlacementGroup::has_parallel_conf);
    registry.def("clear_parallel_conf", &::oneflow::cfg::BlobPlacementGroup::clear_parallel_conf);
    registry.def("parallel_conf", &::oneflow::cfg::BlobPlacementGroup::shared_const_parallel_conf);
    registry.def("mutable_parallel_conf", &::oneflow::cfg::BlobPlacementGroup::shared_mutable_parallel_conf);
  }
  {
    pybind11::class_<ConstPlacement, ::oneflow::cfg::Message, std::shared_ptr<ConstPlacement>> registry(m, "ConstPlacement");
    registry.def("__id__", &::oneflow::cfg::Placement::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstPlacement::DebugString);
    registry.def("__repr__", &ConstPlacement::DebugString);

    registry.def("placement_group_size", &ConstPlacement::placement_group_size);
    registry.def("placement_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> (ConstPlacement::*)() const)&ConstPlacement::shared_const_placement_group);
    registry.def("placement_group", (::std::shared_ptr<ConstPlacementGroup> (ConstPlacement::*)(::std::size_t) const)&ConstPlacement::shared_const_placement_group);

    registry.def("blob_placement_group_size", &ConstPlacement::blob_placement_group_size);
    registry.def("blob_placement_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> (ConstPlacement::*)() const)&ConstPlacement::shared_const_blob_placement_group);
    registry.def("blob_placement_group", (::std::shared_ptr<ConstBlobPlacementGroup> (ConstPlacement::*)(::std::size_t) const)&ConstPlacement::shared_const_blob_placement_group);
  }
  {
    pybind11::class_<::oneflow::cfg::Placement, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::Placement>> registry(m, "Placement");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::Placement::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::Placement::*)(const ConstPlacement&))&::oneflow::cfg::Placement::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::Placement::*)(const ::oneflow::cfg::Placement&))&::oneflow::cfg::Placement::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::Placement::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::Placement::DebugString);
    registry.def("__repr__", &::oneflow::cfg::Placement::DebugString);



    registry.def("placement_group_size", &::oneflow::cfg::Placement::placement_group_size);
    registry.def("clear_placement_group", &::oneflow::cfg::Placement::clear_placement_group);
    registry.def("mutable_placement_group", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> (::oneflow::cfg::Placement::*)())&::oneflow::cfg::Placement::shared_mutable_placement_group);
    registry.def("placement_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> (::oneflow::cfg::Placement::*)() const)&::oneflow::cfg::Placement::shared_const_placement_group);
    registry.def("placement_group", (::std::shared_ptr<ConstPlacementGroup> (::oneflow::cfg::Placement::*)(::std::size_t) const)&::oneflow::cfg::Placement::shared_const_placement_group);
    registry.def("mutable_placement_group", (::std::shared_ptr<::oneflow::cfg::PlacementGroup> (::oneflow::cfg::Placement::*)(::std::size_t))&::oneflow::cfg::Placement::shared_mutable_placement_group);

    registry.def("blob_placement_group_size", &::oneflow::cfg::Placement::blob_placement_group_size);
    registry.def("clear_blob_placement_group", &::oneflow::cfg::Placement::clear_blob_placement_group);
    registry.def("mutable_blob_placement_group", (::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> (::oneflow::cfg::Placement::*)())&::oneflow::cfg::Placement::shared_mutable_blob_placement_group);
    registry.def("blob_placement_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> (::oneflow::cfg::Placement::*)() const)&::oneflow::cfg::Placement::shared_const_blob_placement_group);
    registry.def("blob_placement_group", (::std::shared_ptr<ConstBlobPlacementGroup> (::oneflow::cfg::Placement::*)(::std::size_t) const)&::oneflow::cfg::Placement::shared_const_blob_placement_group);
    registry.def("mutable_blob_placement_group", (::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> (::oneflow::cfg::Placement::*)(::std::size_t))&::oneflow::cfg::Placement::shared_mutable_blob_placement_group);
  }
}