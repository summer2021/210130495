#ifndef CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class ParallelSignature_BnInOp2parallelDescSymbolIdEntry;
class ParallelSignature;

namespace cfg {


class ParallelSignature;
class ConstParallelSignature;


class _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_; 
class Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_;

class ConstParallelSignature : public ::oneflow::cfg::Message {
 public:

  class _ParallelSignature_ {
   public:
    _ParallelSignature_();
    explicit _ParallelSignature_(const _ParallelSignature_& other);
    explicit _ParallelSignature_(_ParallelSignature_&& other);
    _ParallelSignature_(const ::oneflow::ParallelSignature& proto_parallelsignature);
    ~_ParallelSignature_();

    void InitFromProto(const ::oneflow::ParallelSignature& proto_parallelsignature);

    void ToProto(::oneflow::ParallelSignature* proto_parallelsignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelSignature_& other);
  
      // optional field op_parallel_desc_symbol_id
     public:
    bool has_op_parallel_desc_symbol_id() const;
    const int64_t& op_parallel_desc_symbol_id() const;
    void clear_op_parallel_desc_symbol_id();
    void set_op_parallel_desc_symbol_id(const int64_t& value);
    int64_t* mutable_op_parallel_desc_symbol_id();
   protected:
    bool has_op_parallel_desc_symbol_id_ = false;
    int64_t op_parallel_desc_symbol_id_;
      
     public:
    ::std::size_t bn_in_op2parallel_desc_symbol_id_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& bn_in_op2parallel_desc_symbol_id() const;

    _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ * mutable_bn_in_op2parallel_desc_symbol_id();

    const int64_t& bn_in_op2parallel_desc_symbol_id(::std::string key) const;

    void clear_bn_in_op2parallel_desc_symbol_id();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> bn_in_op2parallel_desc_symbol_id_;
         
   public:
    int compare(const _ParallelSignature_& other);

    bool operator==(const _ParallelSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelSignature_& other) const;
  };

  ConstParallelSignature(const ::std::shared_ptr<_ParallelSignature_>& data);
  ConstParallelSignature(const ConstParallelSignature&);
  ConstParallelSignature(ConstParallelSignature&&) noexcept;
  ConstParallelSignature();
  ConstParallelSignature(const ::oneflow::ParallelSignature& proto_parallelsignature);
  virtual ~ConstParallelSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_parallelsignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_parallel_desc_symbol_id
 public:
  bool has_op_parallel_desc_symbol_id() const;
  const int64_t& op_parallel_desc_symbol_id() const;
  // used by pybind11 only
  // map field bn_in_op2parallel_desc_symbol_id
 public:
  ::std::size_t bn_in_op2parallel_desc_symbol_id_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& bn_in_op2parallel_desc_symbol_id() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> shared_const_bn_in_op2parallel_desc_symbol_id() const;

 public:
  ::std::shared_ptr<ConstParallelSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelSignature& other) const;
 protected:
  const ::std::shared_ptr<_ParallelSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelSignature
  void BuildFromProto(const PbMessage& proto_parallelsignature);
  
  ::std::shared_ptr<_ParallelSignature_> data_;
};

class ParallelSignature final : public ConstParallelSignature {
 public:
  ParallelSignature(const ::std::shared_ptr<_ParallelSignature_>& data);
  ParallelSignature(const ParallelSignature& other);
  // enable nothrow for ::std::vector<ParallelSignature> resize 
  ParallelSignature(ParallelSignature&&) noexcept;
  ParallelSignature();
  explicit ParallelSignature(const ::oneflow::ParallelSignature& proto_parallelsignature);

  ~ParallelSignature() override;

  void InitFromProto(const PbMessage& proto_parallelsignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelSignature& other) const;
  void Clear();
  void CopyFrom(const ParallelSignature& other);
  ParallelSignature& operator=(const ParallelSignature& other);

  // required or optional field op_parallel_desc_symbol_id
 public:
  void clear_op_parallel_desc_symbol_id();
  void set_op_parallel_desc_symbol_id(const int64_t& value);
  int64_t* mutable_op_parallel_desc_symbol_id();
  // repeated field bn_in_op2parallel_desc_symbol_id
 public:
  void clear_bn_in_op2parallel_desc_symbol_id();

  const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ & bn_in_op2parallel_desc_symbol_id() const;

  _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_* mutable_bn_in_op2parallel_desc_symbol_id();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> shared_mutable_bn_in_op2parallel_desc_symbol_id();

  ::std::shared_ptr<ParallelSignature> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ : public ::oneflow::cfg::_MapField_<::std::string, int64_t> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_(const ::std::shared_ptr<::std::map<::std::string, int64_t>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const;
  // used by pybind11 only
  const int64_t& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ final : public Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_(const ::std::shared_ptr<::std::map<::std::string, int64_t>>& data);
  _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_();
  ~_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H__MapField___std__string_int64_t_> __SharedMutable__();

  void Set(const ::std::string& key, const int64_t& value);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstParallelSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelSignature> {
  std::size_t operator()(const ::oneflow::cfg::ParallelSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_PARALLEL_SIGNATURE_CFG_H_