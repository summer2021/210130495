#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/job/initializer_conf.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.job.initializer_conf", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::VarianceNorm> enm(m, "VarianceNorm");
    enm.value("kFanIn", ::oneflow::cfg::kFanIn);
    enm.value("kFanOut", ::oneflow::cfg::kFanOut);
    enm.value("kAverage", ::oneflow::cfg::kAverage);
    m.attr("kFanIn") = enm.attr("kFanIn");
    m.attr("kFanOut") = enm.attr("kFanOut");
    m.attr("kAverage") = enm.attr("kAverage");
  }
  {
    pybind11::enum_<::oneflow::cfg::RandomDistribution> enm(m, "RandomDistribution");
    enm.value("kRandomUniform", ::oneflow::cfg::kRandomUniform);
    enm.value("kRandomNormal", ::oneflow::cfg::kRandomNormal);
    enm.value("kTruncatedNormal", ::oneflow::cfg::kTruncatedNormal);
    m.attr("kRandomUniform") = enm.attr("kRandomUniform");
    m.attr("kRandomNormal") = enm.attr("kRandomNormal");
    m.attr("kTruncatedNormal") = enm.attr("kTruncatedNormal");
  }


  {
    pybind11::class_<ConstConstantInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstConstantInitializerConf>> registry(m, "ConstConstantInitializerConf");
    registry.def("__id__", &::oneflow::cfg::ConstantInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstConstantInitializerConf::DebugString);
    registry.def("__repr__", &ConstConstantInitializerConf::DebugString);

    registry.def("has_value", &ConstConstantInitializerConf::has_value);
    registry.def("value", &ConstConstantInitializerConf::value);
  }
  {
    pybind11::class_<::oneflow::cfg::ConstantInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>> registry(m, "ConstantInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ConstantInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantInitializerConf::*)(const ConstConstantInitializerConf&))&::oneflow::cfg::ConstantInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantInitializerConf::*)(const ::oneflow::cfg::ConstantInitializerConf&))&::oneflow::cfg::ConstantInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ConstantInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ConstantInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ConstantInitializerConf::DebugString);



    registry.def("has_value", &::oneflow::cfg::ConstantInitializerConf::has_value);
    registry.def("clear_value", &::oneflow::cfg::ConstantInitializerConf::clear_value);
    registry.def("value", &::oneflow::cfg::ConstantInitializerConf::value);
    registry.def("set_value", &::oneflow::cfg::ConstantInitializerConf::set_value);
  }
  {
    pybind11::class_<ConstConstantIntInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstConstantIntInitializerConf>> registry(m, "ConstConstantIntInitializerConf");
    registry.def("__id__", &::oneflow::cfg::ConstantIntInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstConstantIntInitializerConf::DebugString);
    registry.def("__repr__", &ConstConstantIntInitializerConf::DebugString);

    registry.def("has_value", &ConstConstantIntInitializerConf::has_value);
    registry.def("value", &ConstConstantIntInitializerConf::value);
  }
  {
    pybind11::class_<::oneflow::cfg::ConstantIntInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>> registry(m, "ConstantIntInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ConstantIntInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantIntInitializerConf::*)(const ConstConstantIntInitializerConf&))&::oneflow::cfg::ConstantIntInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantIntInitializerConf::*)(const ::oneflow::cfg::ConstantIntInitializerConf&))&::oneflow::cfg::ConstantIntInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ConstantIntInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ConstantIntInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ConstantIntInitializerConf::DebugString);



    registry.def("has_value", &::oneflow::cfg::ConstantIntInitializerConf::has_value);
    registry.def("clear_value", &::oneflow::cfg::ConstantIntInitializerConf::clear_value);
    registry.def("value", &::oneflow::cfg::ConstantIntInitializerConf::value);
    registry.def("set_value", &::oneflow::cfg::ConstantIntInitializerConf::set_value);
  }
  {
    pybind11::class_<ConstRandomUniformInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRandomUniformInitializerConf>> registry(m, "ConstRandomUniformInitializerConf");
    registry.def("__id__", &::oneflow::cfg::RandomUniformInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRandomUniformInitializerConf::DebugString);
    registry.def("__repr__", &ConstRandomUniformInitializerConf::DebugString);

    registry.def("has_min", &ConstRandomUniformInitializerConf::has_min);
    registry.def("min", &ConstRandomUniformInitializerConf::min);

    registry.def("has_max", &ConstRandomUniformInitializerConf::has_max);
    registry.def("max", &ConstRandomUniformInitializerConf::max);
  }
  {
    pybind11::class_<::oneflow::cfg::RandomUniformInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>> registry(m, "RandomUniformInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RandomUniformInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomUniformInitializerConf::*)(const ConstRandomUniformInitializerConf&))&::oneflow::cfg::RandomUniformInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomUniformInitializerConf::*)(const ::oneflow::cfg::RandomUniformInitializerConf&))&::oneflow::cfg::RandomUniformInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RandomUniformInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RandomUniformInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RandomUniformInitializerConf::DebugString);



    registry.def("has_min", &::oneflow::cfg::RandomUniformInitializerConf::has_min);
    registry.def("clear_min", &::oneflow::cfg::RandomUniformInitializerConf::clear_min);
    registry.def("min", &::oneflow::cfg::RandomUniformInitializerConf::min);
    registry.def("set_min", &::oneflow::cfg::RandomUniformInitializerConf::set_min);

    registry.def("has_max", &::oneflow::cfg::RandomUniformInitializerConf::has_max);
    registry.def("clear_max", &::oneflow::cfg::RandomUniformInitializerConf::clear_max);
    registry.def("max", &::oneflow::cfg::RandomUniformInitializerConf::max);
    registry.def("set_max", &::oneflow::cfg::RandomUniformInitializerConf::set_max);
  }
  {
    pybind11::class_<ConstRandomUniformIntInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRandomUniformIntInitializerConf>> registry(m, "ConstRandomUniformIntInitializerConf");
    registry.def("__id__", &::oneflow::cfg::RandomUniformIntInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRandomUniformIntInitializerConf::DebugString);
    registry.def("__repr__", &ConstRandomUniformIntInitializerConf::DebugString);

    registry.def("has_min", &ConstRandomUniformIntInitializerConf::has_min);
    registry.def("min", &ConstRandomUniformIntInitializerConf::min);

    registry.def("has_max", &ConstRandomUniformIntInitializerConf::has_max);
    registry.def("max", &ConstRandomUniformIntInitializerConf::max);
  }
  {
    pybind11::class_<::oneflow::cfg::RandomUniformIntInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>> registry(m, "RandomUniformIntInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RandomUniformIntInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomUniformIntInitializerConf::*)(const ConstRandomUniformIntInitializerConf&))&::oneflow::cfg::RandomUniformIntInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomUniformIntInitializerConf::*)(const ::oneflow::cfg::RandomUniformIntInitializerConf&))&::oneflow::cfg::RandomUniformIntInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RandomUniformIntInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RandomUniformIntInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RandomUniformIntInitializerConf::DebugString);



    registry.def("has_min", &::oneflow::cfg::RandomUniformIntInitializerConf::has_min);
    registry.def("clear_min", &::oneflow::cfg::RandomUniformIntInitializerConf::clear_min);
    registry.def("min", &::oneflow::cfg::RandomUniformIntInitializerConf::min);
    registry.def("set_min", &::oneflow::cfg::RandomUniformIntInitializerConf::set_min);

    registry.def("has_max", &::oneflow::cfg::RandomUniformIntInitializerConf::has_max);
    registry.def("clear_max", &::oneflow::cfg::RandomUniformIntInitializerConf::clear_max);
    registry.def("max", &::oneflow::cfg::RandomUniformIntInitializerConf::max);
    registry.def("set_max", &::oneflow::cfg::RandomUniformIntInitializerConf::set_max);
  }
  {
    pybind11::class_<ConstRandomNormalInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRandomNormalInitializerConf>> registry(m, "ConstRandomNormalInitializerConf");
    registry.def("__id__", &::oneflow::cfg::RandomNormalInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRandomNormalInitializerConf::DebugString);
    registry.def("__repr__", &ConstRandomNormalInitializerConf::DebugString);

    registry.def("has_mean", &ConstRandomNormalInitializerConf::has_mean);
    registry.def("mean", &ConstRandomNormalInitializerConf::mean);

    registry.def("has_std", &ConstRandomNormalInitializerConf::has_std);
    registry.def("std", &ConstRandomNormalInitializerConf::std);
  }
  {
    pybind11::class_<::oneflow::cfg::RandomNormalInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>> registry(m, "RandomNormalInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RandomNormalInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomNormalInitializerConf::*)(const ConstRandomNormalInitializerConf&))&::oneflow::cfg::RandomNormalInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RandomNormalInitializerConf::*)(const ::oneflow::cfg::RandomNormalInitializerConf&))&::oneflow::cfg::RandomNormalInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RandomNormalInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RandomNormalInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RandomNormalInitializerConf::DebugString);



    registry.def("has_mean", &::oneflow::cfg::RandomNormalInitializerConf::has_mean);
    registry.def("clear_mean", &::oneflow::cfg::RandomNormalInitializerConf::clear_mean);
    registry.def("mean", &::oneflow::cfg::RandomNormalInitializerConf::mean);
    registry.def("set_mean", &::oneflow::cfg::RandomNormalInitializerConf::set_mean);

    registry.def("has_std", &::oneflow::cfg::RandomNormalInitializerConf::has_std);
    registry.def("clear_std", &::oneflow::cfg::RandomNormalInitializerConf::clear_std);
    registry.def("std", &::oneflow::cfg::RandomNormalInitializerConf::std);
    registry.def("set_std", &::oneflow::cfg::RandomNormalInitializerConf::set_std);
  }
  {
    pybind11::class_<ConstTruncatedNormalInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstTruncatedNormalInitializerConf>> registry(m, "ConstTruncatedNormalInitializerConf");
    registry.def("__id__", &::oneflow::cfg::TruncatedNormalInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstTruncatedNormalInitializerConf::DebugString);
    registry.def("__repr__", &ConstTruncatedNormalInitializerConf::DebugString);

    registry.def("has_mean", &ConstTruncatedNormalInitializerConf::has_mean);
    registry.def("mean", &ConstTruncatedNormalInitializerConf::mean);

    registry.def("has_std", &ConstTruncatedNormalInitializerConf::has_std);
    registry.def("std", &ConstTruncatedNormalInitializerConf::std);
  }
  {
    pybind11::class_<::oneflow::cfg::TruncatedNormalInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>> registry(m, "TruncatedNormalInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::TruncatedNormalInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::TruncatedNormalInitializerConf::*)(const ConstTruncatedNormalInitializerConf&))&::oneflow::cfg::TruncatedNormalInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::TruncatedNormalInitializerConf::*)(const ::oneflow::cfg::TruncatedNormalInitializerConf&))&::oneflow::cfg::TruncatedNormalInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::TruncatedNormalInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::TruncatedNormalInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::TruncatedNormalInitializerConf::DebugString);



    registry.def("has_mean", &::oneflow::cfg::TruncatedNormalInitializerConf::has_mean);
    registry.def("clear_mean", &::oneflow::cfg::TruncatedNormalInitializerConf::clear_mean);
    registry.def("mean", &::oneflow::cfg::TruncatedNormalInitializerConf::mean);
    registry.def("set_mean", &::oneflow::cfg::TruncatedNormalInitializerConf::set_mean);

    registry.def("has_std", &::oneflow::cfg::TruncatedNormalInitializerConf::has_std);
    registry.def("clear_std", &::oneflow::cfg::TruncatedNormalInitializerConf::clear_std);
    registry.def("std", &::oneflow::cfg::TruncatedNormalInitializerConf::std);
    registry.def("set_std", &::oneflow::cfg::TruncatedNormalInitializerConf::set_std);
  }
  {
    pybind11::class_<ConstXavierInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstXavierInitializerConf>> registry(m, "ConstXavierInitializerConf");
    registry.def("__id__", &::oneflow::cfg::XavierInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXavierInitializerConf::DebugString);
    registry.def("__repr__", &ConstXavierInitializerConf::DebugString);

    registry.def("has_variance_norm", &ConstXavierInitializerConf::has_variance_norm);
    registry.def("variance_norm", &ConstXavierInitializerConf::variance_norm);

    registry.def("has_data_format", &ConstXavierInitializerConf::has_data_format);
    registry.def("data_format", &ConstXavierInitializerConf::data_format);
  }
  {
    pybind11::class_<::oneflow::cfg::XavierInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XavierInitializerConf>> registry(m, "XavierInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XavierInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XavierInitializerConf::*)(const ConstXavierInitializerConf&))&::oneflow::cfg::XavierInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XavierInitializerConf::*)(const ::oneflow::cfg::XavierInitializerConf&))&::oneflow::cfg::XavierInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XavierInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XavierInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XavierInitializerConf::DebugString);



    registry.def("has_variance_norm", &::oneflow::cfg::XavierInitializerConf::has_variance_norm);
    registry.def("clear_variance_norm", &::oneflow::cfg::XavierInitializerConf::clear_variance_norm);
    registry.def("variance_norm", &::oneflow::cfg::XavierInitializerConf::variance_norm);
    registry.def("set_variance_norm", &::oneflow::cfg::XavierInitializerConf::set_variance_norm);

    registry.def("has_data_format", &::oneflow::cfg::XavierInitializerConf::has_data_format);
    registry.def("clear_data_format", &::oneflow::cfg::XavierInitializerConf::clear_data_format);
    registry.def("data_format", &::oneflow::cfg::XavierInitializerConf::data_format);
    registry.def("set_data_format", &::oneflow::cfg::XavierInitializerConf::set_data_format);
  }
  {
    pybind11::class_<ConstMsraInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstMsraInitializerConf>> registry(m, "ConstMsraInitializerConf");
    registry.def("__id__", &::oneflow::cfg::MsraInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstMsraInitializerConf::DebugString);
    registry.def("__repr__", &ConstMsraInitializerConf::DebugString);

    registry.def("has_variance_norm", &ConstMsraInitializerConf::has_variance_norm);
    registry.def("variance_norm", &ConstMsraInitializerConf::variance_norm);

    registry.def("has_data_format", &ConstMsraInitializerConf::has_data_format);
    registry.def("data_format", &ConstMsraInitializerConf::data_format);
  }
  {
    pybind11::class_<::oneflow::cfg::MsraInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::MsraInitializerConf>> registry(m, "MsraInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::MsraInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::MsraInitializerConf::*)(const ConstMsraInitializerConf&))&::oneflow::cfg::MsraInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::MsraInitializerConf::*)(const ::oneflow::cfg::MsraInitializerConf&))&::oneflow::cfg::MsraInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::MsraInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::MsraInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::MsraInitializerConf::DebugString);



    registry.def("has_variance_norm", &::oneflow::cfg::MsraInitializerConf::has_variance_norm);
    registry.def("clear_variance_norm", &::oneflow::cfg::MsraInitializerConf::clear_variance_norm);
    registry.def("variance_norm", &::oneflow::cfg::MsraInitializerConf::variance_norm);
    registry.def("set_variance_norm", &::oneflow::cfg::MsraInitializerConf::set_variance_norm);

    registry.def("has_data_format", &::oneflow::cfg::MsraInitializerConf::has_data_format);
    registry.def("clear_data_format", &::oneflow::cfg::MsraInitializerConf::clear_data_format);
    registry.def("data_format", &::oneflow::cfg::MsraInitializerConf::data_format);
    registry.def("set_data_format", &::oneflow::cfg::MsraInitializerConf::set_data_format);
  }
  {
    pybind11::class_<ConstRangeInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstRangeInitializerConf>> registry(m, "ConstRangeInitializerConf");
    registry.def("__id__", &::oneflow::cfg::RangeInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstRangeInitializerConf::DebugString);
    registry.def("__repr__", &ConstRangeInitializerConf::DebugString);

    registry.def("has_start", &ConstRangeInitializerConf::has_start);
    registry.def("start", &ConstRangeInitializerConf::start);

    registry.def("has_stride", &ConstRangeInitializerConf::has_stride);
    registry.def("stride", &ConstRangeInitializerConf::stride);

    registry.def("has_axis", &ConstRangeInitializerConf::has_axis);
    registry.def("axis", &ConstRangeInitializerConf::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::RangeInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::RangeInitializerConf>> registry(m, "RangeInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::RangeInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::RangeInitializerConf::*)(const ConstRangeInitializerConf&))&::oneflow::cfg::RangeInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::RangeInitializerConf::*)(const ::oneflow::cfg::RangeInitializerConf&))&::oneflow::cfg::RangeInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::RangeInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::RangeInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::RangeInitializerConf::DebugString);



    registry.def("has_start", &::oneflow::cfg::RangeInitializerConf::has_start);
    registry.def("clear_start", &::oneflow::cfg::RangeInitializerConf::clear_start);
    registry.def("start", &::oneflow::cfg::RangeInitializerConf::start);
    registry.def("set_start", &::oneflow::cfg::RangeInitializerConf::set_start);

    registry.def("has_stride", &::oneflow::cfg::RangeInitializerConf::has_stride);
    registry.def("clear_stride", &::oneflow::cfg::RangeInitializerConf::clear_stride);
    registry.def("stride", &::oneflow::cfg::RangeInitializerConf::stride);
    registry.def("set_stride", &::oneflow::cfg::RangeInitializerConf::set_stride);

    registry.def("has_axis", &::oneflow::cfg::RangeInitializerConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::RangeInitializerConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::RangeInitializerConf::axis);
    registry.def("set_axis", &::oneflow::cfg::RangeInitializerConf::set_axis);
  }
  {
    pybind11::class_<ConstIntRangeInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstIntRangeInitializerConf>> registry(m, "ConstIntRangeInitializerConf");
    registry.def("__id__", &::oneflow::cfg::IntRangeInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstIntRangeInitializerConf::DebugString);
    registry.def("__repr__", &ConstIntRangeInitializerConf::DebugString);

    registry.def("has_start", &ConstIntRangeInitializerConf::has_start);
    registry.def("start", &ConstIntRangeInitializerConf::start);

    registry.def("has_stride", &ConstIntRangeInitializerConf::has_stride);
    registry.def("stride", &ConstIntRangeInitializerConf::stride);

    registry.def("has_axis", &ConstIntRangeInitializerConf::has_axis);
    registry.def("axis", &ConstIntRangeInitializerConf::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::IntRangeInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>> registry(m, "IntRangeInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::IntRangeInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::IntRangeInitializerConf::*)(const ConstIntRangeInitializerConf&))&::oneflow::cfg::IntRangeInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::IntRangeInitializerConf::*)(const ::oneflow::cfg::IntRangeInitializerConf&))&::oneflow::cfg::IntRangeInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::IntRangeInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::IntRangeInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::IntRangeInitializerConf::DebugString);



    registry.def("has_start", &::oneflow::cfg::IntRangeInitializerConf::has_start);
    registry.def("clear_start", &::oneflow::cfg::IntRangeInitializerConf::clear_start);
    registry.def("start", &::oneflow::cfg::IntRangeInitializerConf::start);
    registry.def("set_start", &::oneflow::cfg::IntRangeInitializerConf::set_start);

    registry.def("has_stride", &::oneflow::cfg::IntRangeInitializerConf::has_stride);
    registry.def("clear_stride", &::oneflow::cfg::IntRangeInitializerConf::clear_stride);
    registry.def("stride", &::oneflow::cfg::IntRangeInitializerConf::stride);
    registry.def("set_stride", &::oneflow::cfg::IntRangeInitializerConf::set_stride);

    registry.def("has_axis", &::oneflow::cfg::IntRangeInitializerConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::IntRangeInitializerConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::IntRangeInitializerConf::axis);
    registry.def("set_axis", &::oneflow::cfg::IntRangeInitializerConf::set_axis);
  }
  {
    pybind11::class_<ConstVarianceScalingInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstVarianceScalingInitializerConf>> registry(m, "ConstVarianceScalingInitializerConf");
    registry.def("__id__", &::oneflow::cfg::VarianceScalingInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstVarianceScalingInitializerConf::DebugString);
    registry.def("__repr__", &ConstVarianceScalingInitializerConf::DebugString);

    registry.def("has_scale", &ConstVarianceScalingInitializerConf::has_scale);
    registry.def("scale", &ConstVarianceScalingInitializerConf::scale);

    registry.def("has_variance_norm", &ConstVarianceScalingInitializerConf::has_variance_norm);
    registry.def("variance_norm", &ConstVarianceScalingInitializerConf::variance_norm);

    registry.def("has_distribution", &ConstVarianceScalingInitializerConf::has_distribution);
    registry.def("distribution", &ConstVarianceScalingInitializerConf::distribution);

    registry.def("has_data_format", &ConstVarianceScalingInitializerConf::has_data_format);
    registry.def("data_format", &ConstVarianceScalingInitializerConf::data_format);
  }
  {
    pybind11::class_<::oneflow::cfg::VarianceScalingInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>> registry(m, "VarianceScalingInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::VarianceScalingInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::VarianceScalingInitializerConf::*)(const ConstVarianceScalingInitializerConf&))&::oneflow::cfg::VarianceScalingInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::VarianceScalingInitializerConf::*)(const ::oneflow::cfg::VarianceScalingInitializerConf&))&::oneflow::cfg::VarianceScalingInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::VarianceScalingInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::VarianceScalingInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::VarianceScalingInitializerConf::DebugString);



    registry.def("has_scale", &::oneflow::cfg::VarianceScalingInitializerConf::has_scale);
    registry.def("clear_scale", &::oneflow::cfg::VarianceScalingInitializerConf::clear_scale);
    registry.def("scale", &::oneflow::cfg::VarianceScalingInitializerConf::scale);
    registry.def("set_scale", &::oneflow::cfg::VarianceScalingInitializerConf::set_scale);

    registry.def("has_variance_norm", &::oneflow::cfg::VarianceScalingInitializerConf::has_variance_norm);
    registry.def("clear_variance_norm", &::oneflow::cfg::VarianceScalingInitializerConf::clear_variance_norm);
    registry.def("variance_norm", &::oneflow::cfg::VarianceScalingInitializerConf::variance_norm);
    registry.def("set_variance_norm", &::oneflow::cfg::VarianceScalingInitializerConf::set_variance_norm);

    registry.def("has_distribution", &::oneflow::cfg::VarianceScalingInitializerConf::has_distribution);
    registry.def("clear_distribution", &::oneflow::cfg::VarianceScalingInitializerConf::clear_distribution);
    registry.def("distribution", &::oneflow::cfg::VarianceScalingInitializerConf::distribution);
    registry.def("set_distribution", &::oneflow::cfg::VarianceScalingInitializerConf::set_distribution);

    registry.def("has_data_format", &::oneflow::cfg::VarianceScalingInitializerConf::has_data_format);
    registry.def("clear_data_format", &::oneflow::cfg::VarianceScalingInitializerConf::clear_data_format);
    registry.def("data_format", &::oneflow::cfg::VarianceScalingInitializerConf::data_format);
    registry.def("set_data_format", &::oneflow::cfg::VarianceScalingInitializerConf::set_data_format);
  }
  {
    pybind11::class_<ConstEmptyInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstEmptyInitializerConf>> registry(m, "ConstEmptyInitializerConf");
    registry.def("__id__", &::oneflow::cfg::EmptyInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstEmptyInitializerConf::DebugString);
    registry.def("__repr__", &ConstEmptyInitializerConf::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::EmptyInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>> registry(m, "EmptyInitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::EmptyInitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::EmptyInitializerConf::*)(const ConstEmptyInitializerConf&))&::oneflow::cfg::EmptyInitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::EmptyInitializerConf::*)(const ::oneflow::cfg::EmptyInitializerConf&))&::oneflow::cfg::EmptyInitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::EmptyInitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::EmptyInitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::EmptyInitializerConf::DebugString);


  }
  {
    pybind11::class_<ConstInitializerConf, ::oneflow::cfg::Message, std::shared_ptr<ConstInitializerConf>> registry(m, "ConstInitializerConf");
    registry.def("__id__", &::oneflow::cfg::InitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstInitializerConf::DebugString);
    registry.def("__repr__", &ConstInitializerConf::DebugString);

    registry.def("has_constant_conf", &ConstInitializerConf::has_constant_conf);
    registry.def("constant_conf", &ConstInitializerConf::shared_const_constant_conf);

    registry.def("has_constant_int_conf", &ConstInitializerConf::has_constant_int_conf);
    registry.def("constant_int_conf", &ConstInitializerConf::shared_const_constant_int_conf);

    registry.def("has_random_uniform_conf", &ConstInitializerConf::has_random_uniform_conf);
    registry.def("random_uniform_conf", &ConstInitializerConf::shared_const_random_uniform_conf);

    registry.def("has_random_uniform_int_conf", &ConstInitializerConf::has_random_uniform_int_conf);
    registry.def("random_uniform_int_conf", &ConstInitializerConf::shared_const_random_uniform_int_conf);

    registry.def("has_random_normal_conf", &ConstInitializerConf::has_random_normal_conf);
    registry.def("random_normal_conf", &ConstInitializerConf::shared_const_random_normal_conf);

    registry.def("has_truncated_normal_conf", &ConstInitializerConf::has_truncated_normal_conf);
    registry.def("truncated_normal_conf", &ConstInitializerConf::shared_const_truncated_normal_conf);

    registry.def("has_xavier_conf", &ConstInitializerConf::has_xavier_conf);
    registry.def("xavier_conf", &ConstInitializerConf::shared_const_xavier_conf);

    registry.def("has_msra_conf", &ConstInitializerConf::has_msra_conf);
    registry.def("msra_conf", &ConstInitializerConf::shared_const_msra_conf);

    registry.def("has_range_conf", &ConstInitializerConf::has_range_conf);
    registry.def("range_conf", &ConstInitializerConf::shared_const_range_conf);

    registry.def("has_int_range_conf", &ConstInitializerConf::has_int_range_conf);
    registry.def("int_range_conf", &ConstInitializerConf::shared_const_int_range_conf);

    registry.def("has_variance_scaling_conf", &ConstInitializerConf::has_variance_scaling_conf);
    registry.def("variance_scaling_conf", &ConstInitializerConf::shared_const_variance_scaling_conf);

    registry.def("has_empty_conf", &ConstInitializerConf::has_empty_conf);
    registry.def("empty_conf", &ConstInitializerConf::shared_const_empty_conf);
    registry.def("type_case",  &ConstInitializerConf::type_case);
    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantConf; })
        .def_property_readonly_static("kConstantIntConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantIntConf; })
        .def_property_readonly_static("kRandomUniformConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformConf; })
        .def_property_readonly_static("kRandomUniformIntConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformIntConf; })
        .def_property_readonly_static("kRandomNormalConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomNormalConf; })
        .def_property_readonly_static("kTruncatedNormalConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kTruncatedNormalConf; })
        .def_property_readonly_static("kXavierConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kXavierConf; })
        .def_property_readonly_static("kMsraConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kMsraConf; })
        .def_property_readonly_static("kRangeConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRangeConf; })
        .def_property_readonly_static("kIntRangeConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kIntRangeConf; })
        .def_property_readonly_static("kVarianceScalingConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kVarianceScalingConf; })
        .def_property_readonly_static("kEmptyConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kEmptyConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::InitializerConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::InitializerConf>> registry(m, "InitializerConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::InitializerConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::InitializerConf::*)(const ConstInitializerConf&))&::oneflow::cfg::InitializerConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::InitializerConf::*)(const ::oneflow::cfg::InitializerConf&))&::oneflow::cfg::InitializerConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::InitializerConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::InitializerConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::InitializerConf::DebugString);

    registry.def_property_readonly_static("TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::TYPE_NOT_SET; })
        .def_property_readonly_static("kConstantConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantConf; })
        .def_property_readonly_static("kConstantIntConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantIntConf; })
        .def_property_readonly_static("kRandomUniformConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformConf; })
        .def_property_readonly_static("kRandomUniformIntConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformIntConf; })
        .def_property_readonly_static("kRandomNormalConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomNormalConf; })
        .def_property_readonly_static("kTruncatedNormalConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kTruncatedNormalConf; })
        .def_property_readonly_static("kXavierConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kXavierConf; })
        .def_property_readonly_static("kMsraConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kMsraConf; })
        .def_property_readonly_static("kRangeConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRangeConf; })
        .def_property_readonly_static("kIntRangeConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kIntRangeConf; })
        .def_property_readonly_static("kVarianceScalingConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kVarianceScalingConf; })
        .def_property_readonly_static("kEmptyConf", [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kEmptyConf; })
        ;


    registry.def("has_constant_conf", &::oneflow::cfg::InitializerConf::has_constant_conf);
    registry.def("clear_constant_conf", &::oneflow::cfg::InitializerConf::clear_constant_conf);
    registry.def_property_readonly_static("kConstantConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantConf; });
    registry.def("constant_conf", &::oneflow::cfg::InitializerConf::constant_conf);
    registry.def("mutable_constant_conf", &::oneflow::cfg::InitializerConf::shared_mutable_constant_conf);

    registry.def("has_constant_int_conf", &::oneflow::cfg::InitializerConf::has_constant_int_conf);
    registry.def("clear_constant_int_conf", &::oneflow::cfg::InitializerConf::clear_constant_int_conf);
    registry.def_property_readonly_static("kConstantIntConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kConstantIntConf; });
    registry.def("constant_int_conf", &::oneflow::cfg::InitializerConf::constant_int_conf);
    registry.def("mutable_constant_int_conf", &::oneflow::cfg::InitializerConf::shared_mutable_constant_int_conf);

    registry.def("has_random_uniform_conf", &::oneflow::cfg::InitializerConf::has_random_uniform_conf);
    registry.def("clear_random_uniform_conf", &::oneflow::cfg::InitializerConf::clear_random_uniform_conf);
    registry.def_property_readonly_static("kRandomUniformConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformConf; });
    registry.def("random_uniform_conf", &::oneflow::cfg::InitializerConf::random_uniform_conf);
    registry.def("mutable_random_uniform_conf", &::oneflow::cfg::InitializerConf::shared_mutable_random_uniform_conf);

    registry.def("has_random_uniform_int_conf", &::oneflow::cfg::InitializerConf::has_random_uniform_int_conf);
    registry.def("clear_random_uniform_int_conf", &::oneflow::cfg::InitializerConf::clear_random_uniform_int_conf);
    registry.def_property_readonly_static("kRandomUniformIntConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomUniformIntConf; });
    registry.def("random_uniform_int_conf", &::oneflow::cfg::InitializerConf::random_uniform_int_conf);
    registry.def("mutable_random_uniform_int_conf", &::oneflow::cfg::InitializerConf::shared_mutable_random_uniform_int_conf);

    registry.def("has_random_normal_conf", &::oneflow::cfg::InitializerConf::has_random_normal_conf);
    registry.def("clear_random_normal_conf", &::oneflow::cfg::InitializerConf::clear_random_normal_conf);
    registry.def_property_readonly_static("kRandomNormalConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRandomNormalConf; });
    registry.def("random_normal_conf", &::oneflow::cfg::InitializerConf::random_normal_conf);
    registry.def("mutable_random_normal_conf", &::oneflow::cfg::InitializerConf::shared_mutable_random_normal_conf);

    registry.def("has_truncated_normal_conf", &::oneflow::cfg::InitializerConf::has_truncated_normal_conf);
    registry.def("clear_truncated_normal_conf", &::oneflow::cfg::InitializerConf::clear_truncated_normal_conf);
    registry.def_property_readonly_static("kTruncatedNormalConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kTruncatedNormalConf; });
    registry.def("truncated_normal_conf", &::oneflow::cfg::InitializerConf::truncated_normal_conf);
    registry.def("mutable_truncated_normal_conf", &::oneflow::cfg::InitializerConf::shared_mutable_truncated_normal_conf);

    registry.def("has_xavier_conf", &::oneflow::cfg::InitializerConf::has_xavier_conf);
    registry.def("clear_xavier_conf", &::oneflow::cfg::InitializerConf::clear_xavier_conf);
    registry.def_property_readonly_static("kXavierConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kXavierConf; });
    registry.def("xavier_conf", &::oneflow::cfg::InitializerConf::xavier_conf);
    registry.def("mutable_xavier_conf", &::oneflow::cfg::InitializerConf::shared_mutable_xavier_conf);

    registry.def("has_msra_conf", &::oneflow::cfg::InitializerConf::has_msra_conf);
    registry.def("clear_msra_conf", &::oneflow::cfg::InitializerConf::clear_msra_conf);
    registry.def_property_readonly_static("kMsraConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kMsraConf; });
    registry.def("msra_conf", &::oneflow::cfg::InitializerConf::msra_conf);
    registry.def("mutable_msra_conf", &::oneflow::cfg::InitializerConf::shared_mutable_msra_conf);

    registry.def("has_range_conf", &::oneflow::cfg::InitializerConf::has_range_conf);
    registry.def("clear_range_conf", &::oneflow::cfg::InitializerConf::clear_range_conf);
    registry.def_property_readonly_static("kRangeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kRangeConf; });
    registry.def("range_conf", &::oneflow::cfg::InitializerConf::range_conf);
    registry.def("mutable_range_conf", &::oneflow::cfg::InitializerConf::shared_mutable_range_conf);

    registry.def("has_int_range_conf", &::oneflow::cfg::InitializerConf::has_int_range_conf);
    registry.def("clear_int_range_conf", &::oneflow::cfg::InitializerConf::clear_int_range_conf);
    registry.def_property_readonly_static("kIntRangeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kIntRangeConf; });
    registry.def("int_range_conf", &::oneflow::cfg::InitializerConf::int_range_conf);
    registry.def("mutable_int_range_conf", &::oneflow::cfg::InitializerConf::shared_mutable_int_range_conf);

    registry.def("has_variance_scaling_conf", &::oneflow::cfg::InitializerConf::has_variance_scaling_conf);
    registry.def("clear_variance_scaling_conf", &::oneflow::cfg::InitializerConf::clear_variance_scaling_conf);
    registry.def_property_readonly_static("kVarianceScalingConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kVarianceScalingConf; });
    registry.def("variance_scaling_conf", &::oneflow::cfg::InitializerConf::variance_scaling_conf);
    registry.def("mutable_variance_scaling_conf", &::oneflow::cfg::InitializerConf::shared_mutable_variance_scaling_conf);

    registry.def("has_empty_conf", &::oneflow::cfg::InitializerConf::has_empty_conf);
    registry.def("clear_empty_conf", &::oneflow::cfg::InitializerConf::clear_empty_conf);
    registry.def_property_readonly_static("kEmptyConf",
        [](const pybind11::object&){ return ::oneflow::cfg::InitializerConf::kEmptyConf; });
    registry.def("empty_conf", &::oneflow::cfg::InitializerConf::empty_conf);
    registry.def("mutable_empty_conf", &::oneflow::cfg::InitializerConf::shared_mutable_empty_conf);
    pybind11::enum_<::oneflow::cfg::InitializerConf::TypeCase>(registry, "TypeCase")
        .value("TYPE_NOT_SET", ::oneflow::cfg::InitializerConf::TYPE_NOT_SET)
        .value("kConstantConf", ::oneflow::cfg::InitializerConf::kConstantConf)
        .value("kConstantIntConf", ::oneflow::cfg::InitializerConf::kConstantIntConf)
        .value("kRandomUniformConf", ::oneflow::cfg::InitializerConf::kRandomUniformConf)
        .value("kRandomUniformIntConf", ::oneflow::cfg::InitializerConf::kRandomUniformIntConf)
        .value("kRandomNormalConf", ::oneflow::cfg::InitializerConf::kRandomNormalConf)
        .value("kTruncatedNormalConf", ::oneflow::cfg::InitializerConf::kTruncatedNormalConf)
        .value("kXavierConf", ::oneflow::cfg::InitializerConf::kXavierConf)
        .value("kMsraConf", ::oneflow::cfg::InitializerConf::kMsraConf)
        .value("kRangeConf", ::oneflow::cfg::InitializerConf::kRangeConf)
        .value("kIntRangeConf", ::oneflow::cfg::InitializerConf::kIntRangeConf)
        .value("kVarianceScalingConf", ::oneflow::cfg::InitializerConf::kVarianceScalingConf)
        .value("kEmptyConf", ::oneflow::cfg::InitializerConf::kEmptyConf)
        ;
    registry.def("type_case",  &::oneflow::cfg::InitializerConf::type_case);
  }
  {
    pybind11::class_<ConstInitializeWithSnapshotConf, ::oneflow::cfg::Message, std::shared_ptr<ConstInitializeWithSnapshotConf>> registry(m, "ConstInitializeWithSnapshotConf");
    registry.def("__id__", &::oneflow::cfg::InitializeWithSnapshotConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstInitializeWithSnapshotConf::DebugString);
    registry.def("__repr__", &ConstInitializeWithSnapshotConf::DebugString);

    registry.def("has_path", &ConstInitializeWithSnapshotConf::has_path);
    registry.def("path", &ConstInitializeWithSnapshotConf::path);

    registry.def("has_key", &ConstInitializeWithSnapshotConf::has_key);
    registry.def("key", &ConstInitializeWithSnapshotConf::key);
  }
  {
    pybind11::class_<::oneflow::cfg::InitializeWithSnapshotConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::InitializeWithSnapshotConf>> registry(m, "InitializeWithSnapshotConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::InitializeWithSnapshotConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::InitializeWithSnapshotConf::*)(const ConstInitializeWithSnapshotConf&))&::oneflow::cfg::InitializeWithSnapshotConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::InitializeWithSnapshotConf::*)(const ::oneflow::cfg::InitializeWithSnapshotConf&))&::oneflow::cfg::InitializeWithSnapshotConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::InitializeWithSnapshotConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::InitializeWithSnapshotConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::InitializeWithSnapshotConf::DebugString);



    registry.def("has_path", &::oneflow::cfg::InitializeWithSnapshotConf::has_path);
    registry.def("clear_path", &::oneflow::cfg::InitializeWithSnapshotConf::clear_path);
    registry.def("path", &::oneflow::cfg::InitializeWithSnapshotConf::path);
    registry.def("set_path", &::oneflow::cfg::InitializeWithSnapshotConf::set_path);

    registry.def("has_key", &::oneflow::cfg::InitializeWithSnapshotConf::has_key);
    registry.def("clear_key", &::oneflow::cfg::InitializeWithSnapshotConf::clear_key);
    registry.def("key", &::oneflow::cfg::InitializeWithSnapshotConf::key);
    registry.def("set_key", &::oneflow::cfg::InitializeWithSnapshotConf::set_key);
  }
}