#include "oneflow/core/job/resource.cfg.h"
#include "oneflow/core/common/device_type.cfg.h"
#include "oneflow/core/job/resource.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstCollectiveBoxingConf::_CollectiveBoxingConf_::_CollectiveBoxingConf_() { Clear(); }
ConstCollectiveBoxingConf::_CollectiveBoxingConf_::_CollectiveBoxingConf_(const _CollectiveBoxingConf_& other) { CopyFrom(other); }
ConstCollectiveBoxingConf::_CollectiveBoxingConf_::_CollectiveBoxingConf_(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf) {
  InitFromProto(proto_collectiveboxingconf);
}
ConstCollectiveBoxingConf::_CollectiveBoxingConf_::_CollectiveBoxingConf_(_CollectiveBoxingConf_&& other) = default;
ConstCollectiveBoxingConf::_CollectiveBoxingConf_::~_CollectiveBoxingConf_() = default;

void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::InitFromProto(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf) {
  Clear();
  // required_or_optional field: enable_fusion
  if (proto_collectiveboxingconf.has_enable_fusion()) {
    set_enable_fusion(proto_collectiveboxingconf.enable_fusion());
  }
  // required_or_optional field: num_callback_threads
  if (proto_collectiveboxingconf.has_num_callback_threads()) {
    set_num_callback_threads(proto_collectiveboxingconf.num_callback_threads());
  }
  // required_or_optional field: nccl_num_streams
  if (proto_collectiveboxingconf.has_nccl_num_streams()) {
    set_nccl_num_streams(proto_collectiveboxingconf.nccl_num_streams());
  }
  // required_or_optional field: nccl_fusion_threshold_mb
  if (proto_collectiveboxingconf.has_nccl_fusion_threshold_mb()) {
    set_nccl_fusion_threshold_mb(proto_collectiveboxingconf.nccl_fusion_threshold_mb());
  }
  // required_or_optional field: nccl_fusion_all_reduce
  if (proto_collectiveboxingconf.has_nccl_fusion_all_reduce()) {
    set_nccl_fusion_all_reduce(proto_collectiveboxingconf.nccl_fusion_all_reduce());
  }
  // required_or_optional field: nccl_fusion_reduce_scatter
  if (proto_collectiveboxingconf.has_nccl_fusion_reduce_scatter()) {
    set_nccl_fusion_reduce_scatter(proto_collectiveboxingconf.nccl_fusion_reduce_scatter());
  }
  // required_or_optional field: nccl_fusion_all_gather
  if (proto_collectiveboxingconf.has_nccl_fusion_all_gather()) {
    set_nccl_fusion_all_gather(proto_collectiveboxingconf.nccl_fusion_all_gather());
  }
  // required_or_optional field: nccl_fusion_reduce
  if (proto_collectiveboxingconf.has_nccl_fusion_reduce()) {
    set_nccl_fusion_reduce(proto_collectiveboxingconf.nccl_fusion_reduce());
  }
  // required_or_optional field: nccl_fusion_broadcast
  if (proto_collectiveboxingconf.has_nccl_fusion_broadcast()) {
    set_nccl_fusion_broadcast(proto_collectiveboxingconf.nccl_fusion_broadcast());
  }
  // required_or_optional field: nccl_fusion_all_reduce_use_buffer
  if (proto_collectiveboxingconf.has_nccl_fusion_all_reduce_use_buffer()) {
    set_nccl_fusion_all_reduce_use_buffer(proto_collectiveboxingconf.nccl_fusion_all_reduce_use_buffer());
  }
  // required_or_optional field: nccl_fusion_max_ops
  if (proto_collectiveboxingconf.has_nccl_fusion_max_ops()) {
    set_nccl_fusion_max_ops(proto_collectiveboxingconf.nccl_fusion_max_ops());
  }
  // required_or_optional field: nccl_enable_all_to_all
  if (proto_collectiveboxingconf.has_nccl_enable_all_to_all()) {
    set_nccl_enable_all_to_all(proto_collectiveboxingconf.nccl_enable_all_to_all());
  }
  // required_or_optional field: nccl_enable_mixed_fusion
  if (proto_collectiveboxingconf.has_nccl_enable_mixed_fusion()) {
    set_nccl_enable_mixed_fusion(proto_collectiveboxingconf.nccl_enable_mixed_fusion());
  }
    
}

void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::ToProto(::oneflow::CollectiveBoxingConf* proto_collectiveboxingconf) const {
  proto_collectiveboxingconf->Clear();
  // required_or_optional field: enable_fusion
  if (this->has_enable_fusion()) {
    proto_collectiveboxingconf->set_enable_fusion(enable_fusion());
    }
  // required_or_optional field: num_callback_threads
  if (this->has_num_callback_threads()) {
    proto_collectiveboxingconf->set_num_callback_threads(num_callback_threads());
    }
  // required_or_optional field: nccl_num_streams
  if (this->has_nccl_num_streams()) {
    proto_collectiveboxingconf->set_nccl_num_streams(nccl_num_streams());
    }
  // required_or_optional field: nccl_fusion_threshold_mb
  if (this->has_nccl_fusion_threshold_mb()) {
    proto_collectiveboxingconf->set_nccl_fusion_threshold_mb(nccl_fusion_threshold_mb());
    }
  // required_or_optional field: nccl_fusion_all_reduce
  if (this->has_nccl_fusion_all_reduce()) {
    proto_collectiveboxingconf->set_nccl_fusion_all_reduce(nccl_fusion_all_reduce());
    }
  // required_or_optional field: nccl_fusion_reduce_scatter
  if (this->has_nccl_fusion_reduce_scatter()) {
    proto_collectiveboxingconf->set_nccl_fusion_reduce_scatter(nccl_fusion_reduce_scatter());
    }
  // required_or_optional field: nccl_fusion_all_gather
  if (this->has_nccl_fusion_all_gather()) {
    proto_collectiveboxingconf->set_nccl_fusion_all_gather(nccl_fusion_all_gather());
    }
  // required_or_optional field: nccl_fusion_reduce
  if (this->has_nccl_fusion_reduce()) {
    proto_collectiveboxingconf->set_nccl_fusion_reduce(nccl_fusion_reduce());
    }
  // required_or_optional field: nccl_fusion_broadcast
  if (this->has_nccl_fusion_broadcast()) {
    proto_collectiveboxingconf->set_nccl_fusion_broadcast(nccl_fusion_broadcast());
    }
  // required_or_optional field: nccl_fusion_all_reduce_use_buffer
  if (this->has_nccl_fusion_all_reduce_use_buffer()) {
    proto_collectiveboxingconf->set_nccl_fusion_all_reduce_use_buffer(nccl_fusion_all_reduce_use_buffer());
    }
  // required_or_optional field: nccl_fusion_max_ops
  if (this->has_nccl_fusion_max_ops()) {
    proto_collectiveboxingconf->set_nccl_fusion_max_ops(nccl_fusion_max_ops());
    }
  // required_or_optional field: nccl_enable_all_to_all
  if (this->has_nccl_enable_all_to_all()) {
    proto_collectiveboxingconf->set_nccl_enable_all_to_all(nccl_enable_all_to_all());
    }
  // required_or_optional field: nccl_enable_mixed_fusion
  if (this->has_nccl_enable_mixed_fusion()) {
    proto_collectiveboxingconf->set_nccl_enable_mixed_fusion(nccl_enable_mixed_fusion());
    }

}

::std::string ConstCollectiveBoxingConf::_CollectiveBoxingConf_::DebugString() const {
  ::oneflow::CollectiveBoxingConf proto_collectiveboxingconf;
  this->ToProto(&proto_collectiveboxingconf);
  return proto_collectiveboxingconf.DebugString();
}

void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::Clear() {
  clear_enable_fusion();
  clear_num_callback_threads();
  clear_nccl_num_streams();
  clear_nccl_fusion_threshold_mb();
  clear_nccl_fusion_all_reduce();
  clear_nccl_fusion_reduce_scatter();
  clear_nccl_fusion_all_gather();
  clear_nccl_fusion_reduce();
  clear_nccl_fusion_broadcast();
  clear_nccl_fusion_all_reduce_use_buffer();
  clear_nccl_fusion_max_ops();
  clear_nccl_enable_all_to_all();
  clear_nccl_enable_mixed_fusion();
}

void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::CopyFrom(const _CollectiveBoxingConf_& other) {
  if (other.has_enable_fusion()) {
    set_enable_fusion(other.enable_fusion());
  } else {
    clear_enable_fusion();
  }
  if (other.has_num_callback_threads()) {
    set_num_callback_threads(other.num_callback_threads());
  } else {
    clear_num_callback_threads();
  }
  if (other.has_nccl_num_streams()) {
    set_nccl_num_streams(other.nccl_num_streams());
  } else {
    clear_nccl_num_streams();
  }
  if (other.has_nccl_fusion_threshold_mb()) {
    set_nccl_fusion_threshold_mb(other.nccl_fusion_threshold_mb());
  } else {
    clear_nccl_fusion_threshold_mb();
  }
  if (other.has_nccl_fusion_all_reduce()) {
    set_nccl_fusion_all_reduce(other.nccl_fusion_all_reduce());
  } else {
    clear_nccl_fusion_all_reduce();
  }
  if (other.has_nccl_fusion_reduce_scatter()) {
    set_nccl_fusion_reduce_scatter(other.nccl_fusion_reduce_scatter());
  } else {
    clear_nccl_fusion_reduce_scatter();
  }
  if (other.has_nccl_fusion_all_gather()) {
    set_nccl_fusion_all_gather(other.nccl_fusion_all_gather());
  } else {
    clear_nccl_fusion_all_gather();
  }
  if (other.has_nccl_fusion_reduce()) {
    set_nccl_fusion_reduce(other.nccl_fusion_reduce());
  } else {
    clear_nccl_fusion_reduce();
  }
  if (other.has_nccl_fusion_broadcast()) {
    set_nccl_fusion_broadcast(other.nccl_fusion_broadcast());
  } else {
    clear_nccl_fusion_broadcast();
  }
  if (other.has_nccl_fusion_all_reduce_use_buffer()) {
    set_nccl_fusion_all_reduce_use_buffer(other.nccl_fusion_all_reduce_use_buffer());
  } else {
    clear_nccl_fusion_all_reduce_use_buffer();
  }
  if (other.has_nccl_fusion_max_ops()) {
    set_nccl_fusion_max_ops(other.nccl_fusion_max_ops());
  } else {
    clear_nccl_fusion_max_ops();
  }
  if (other.has_nccl_enable_all_to_all()) {
    set_nccl_enable_all_to_all(other.nccl_enable_all_to_all());
  } else {
    clear_nccl_enable_all_to_all();
  }
  if (other.has_nccl_enable_mixed_fusion()) {
    set_nccl_enable_mixed_fusion(other.nccl_enable_mixed_fusion());
  } else {
    clear_nccl_enable_mixed_fusion();
  }
}


// optional field enable_fusion
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_enable_fusion() const {
  return has_enable_fusion_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::enable_fusion() const {
  if (has_enable_fusion_) { return enable_fusion_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_enable_fusion() {
  has_enable_fusion_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_enable_fusion(const bool& value) {
  enable_fusion_ = value;
  has_enable_fusion_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_enable_fusion() {
  has_enable_fusion_ = true;
  return &enable_fusion_;
}

// optional field num_callback_threads
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_num_callback_threads() const {
  return has_num_callback_threads_;
}
const int64_t& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::num_callback_threads() const {
  if (has_num_callback_threads_) { return num_callback_threads_; }
  static const int64_t default_static_value =
    int64_t(4);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_num_callback_threads() {
  has_num_callback_threads_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_num_callback_threads(const int64_t& value) {
  num_callback_threads_ = value;
  has_num_callback_threads_ = true;
}
int64_t* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_num_callback_threads() {
  has_num_callback_threads_ = true;
  return &num_callback_threads_;
}

// optional field nccl_num_streams
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_num_streams() const {
  return has_nccl_num_streams_;
}
const int64_t& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_num_streams() const {
  if (has_nccl_num_streams_) { return nccl_num_streams_; }
  static const int64_t default_static_value =
    int64_t(1);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_num_streams() {
  has_nccl_num_streams_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_num_streams(const int64_t& value) {
  nccl_num_streams_ = value;
  has_nccl_num_streams_ = true;
}
int64_t* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_num_streams() {
  has_nccl_num_streams_ = true;
  return &nccl_num_streams_;
}

// optional field nccl_fusion_threshold_mb
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_threshold_mb() const {
  return has_nccl_fusion_threshold_mb_;
}
const int64_t& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_threshold_mb() const {
  if (has_nccl_fusion_threshold_mb_) { return nccl_fusion_threshold_mb_; }
  static const int64_t default_static_value =
    int64_t(16);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_threshold_mb() {
  has_nccl_fusion_threshold_mb_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_threshold_mb(const int64_t& value) {
  nccl_fusion_threshold_mb_ = value;
  has_nccl_fusion_threshold_mb_ = true;
}
int64_t* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_threshold_mb() {
  has_nccl_fusion_threshold_mb_ = true;
  return &nccl_fusion_threshold_mb_;
}

// optional field nccl_fusion_all_reduce
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_all_reduce() const {
  return has_nccl_fusion_all_reduce_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_all_reduce() const {
  if (has_nccl_fusion_all_reduce_) { return nccl_fusion_all_reduce_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_all_reduce() {
  has_nccl_fusion_all_reduce_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_all_reduce(const bool& value) {
  nccl_fusion_all_reduce_ = value;
  has_nccl_fusion_all_reduce_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_all_reduce() {
  has_nccl_fusion_all_reduce_ = true;
  return &nccl_fusion_all_reduce_;
}

// optional field nccl_fusion_reduce_scatter
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_reduce_scatter() const {
  return has_nccl_fusion_reduce_scatter_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_reduce_scatter() const {
  if (has_nccl_fusion_reduce_scatter_) { return nccl_fusion_reduce_scatter_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_reduce_scatter() {
  has_nccl_fusion_reduce_scatter_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_reduce_scatter(const bool& value) {
  nccl_fusion_reduce_scatter_ = value;
  has_nccl_fusion_reduce_scatter_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_reduce_scatter() {
  has_nccl_fusion_reduce_scatter_ = true;
  return &nccl_fusion_reduce_scatter_;
}

// optional field nccl_fusion_all_gather
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_all_gather() const {
  return has_nccl_fusion_all_gather_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_all_gather() const {
  if (has_nccl_fusion_all_gather_) { return nccl_fusion_all_gather_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_all_gather() {
  has_nccl_fusion_all_gather_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_all_gather(const bool& value) {
  nccl_fusion_all_gather_ = value;
  has_nccl_fusion_all_gather_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_all_gather() {
  has_nccl_fusion_all_gather_ = true;
  return &nccl_fusion_all_gather_;
}

// optional field nccl_fusion_reduce
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_reduce() const {
  return has_nccl_fusion_reduce_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_reduce() const {
  if (has_nccl_fusion_reduce_) { return nccl_fusion_reduce_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_reduce() {
  has_nccl_fusion_reduce_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_reduce(const bool& value) {
  nccl_fusion_reduce_ = value;
  has_nccl_fusion_reduce_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_reduce() {
  has_nccl_fusion_reduce_ = true;
  return &nccl_fusion_reduce_;
}

// optional field nccl_fusion_broadcast
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_broadcast() const {
  return has_nccl_fusion_broadcast_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_broadcast() const {
  if (has_nccl_fusion_broadcast_) { return nccl_fusion_broadcast_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_broadcast() {
  has_nccl_fusion_broadcast_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_broadcast(const bool& value) {
  nccl_fusion_broadcast_ = value;
  has_nccl_fusion_broadcast_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_broadcast() {
  has_nccl_fusion_broadcast_ = true;
  return &nccl_fusion_broadcast_;
}

// optional field nccl_fusion_all_reduce_use_buffer
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_all_reduce_use_buffer() const {
  return has_nccl_fusion_all_reduce_use_buffer_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_all_reduce_use_buffer() const {
  if (has_nccl_fusion_all_reduce_use_buffer_) { return nccl_fusion_all_reduce_use_buffer_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_all_reduce_use_buffer() {
  has_nccl_fusion_all_reduce_use_buffer_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_all_reduce_use_buffer(const bool& value) {
  nccl_fusion_all_reduce_use_buffer_ = value;
  has_nccl_fusion_all_reduce_use_buffer_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_all_reduce_use_buffer() {
  has_nccl_fusion_all_reduce_use_buffer_ = true;
  return &nccl_fusion_all_reduce_use_buffer_;
}

// optional field nccl_fusion_max_ops
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_fusion_max_ops() const {
  return has_nccl_fusion_max_ops_;
}
const int64_t& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_fusion_max_ops() const {
  if (has_nccl_fusion_max_ops_) { return nccl_fusion_max_ops_; }
  static const int64_t default_static_value =
    int64_t(64);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_fusion_max_ops() {
  has_nccl_fusion_max_ops_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_fusion_max_ops(const int64_t& value) {
  nccl_fusion_max_ops_ = value;
  has_nccl_fusion_max_ops_ = true;
}
int64_t* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_fusion_max_ops() {
  has_nccl_fusion_max_ops_ = true;
  return &nccl_fusion_max_ops_;
}

// optional field nccl_enable_all_to_all
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_enable_all_to_all() const {
  return has_nccl_enable_all_to_all_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_enable_all_to_all() const {
  if (has_nccl_enable_all_to_all_) { return nccl_enable_all_to_all_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_enable_all_to_all() {
  has_nccl_enable_all_to_all_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_enable_all_to_all(const bool& value) {
  nccl_enable_all_to_all_ = value;
  has_nccl_enable_all_to_all_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_enable_all_to_all() {
  has_nccl_enable_all_to_all_ = true;
  return &nccl_enable_all_to_all_;
}

// optional field nccl_enable_mixed_fusion
bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::has_nccl_enable_mixed_fusion() const {
  return has_nccl_enable_mixed_fusion_;
}
const bool& ConstCollectiveBoxingConf::_CollectiveBoxingConf_::nccl_enable_mixed_fusion() const {
  if (has_nccl_enable_mixed_fusion_) { return nccl_enable_mixed_fusion_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::clear_nccl_enable_mixed_fusion() {
  has_nccl_enable_mixed_fusion_ = false;
}
void ConstCollectiveBoxingConf::_CollectiveBoxingConf_::set_nccl_enable_mixed_fusion(const bool& value) {
  nccl_enable_mixed_fusion_ = value;
  has_nccl_enable_mixed_fusion_ = true;
}
bool* ConstCollectiveBoxingConf::_CollectiveBoxingConf_::mutable_nccl_enable_mixed_fusion() {
  has_nccl_enable_mixed_fusion_ = true;
  return &nccl_enable_mixed_fusion_;
}


int ConstCollectiveBoxingConf::_CollectiveBoxingConf_::compare(const _CollectiveBoxingConf_& other) {
  if (!(has_enable_fusion() == other.has_enable_fusion())) {
    return has_enable_fusion() < other.has_enable_fusion() ? -1 : 1;
  } else if (!(enable_fusion() == other.enable_fusion())) {
    return enable_fusion() < other.enable_fusion() ? -1 : 1;
  }
  if (!(has_num_callback_threads() == other.has_num_callback_threads())) {
    return has_num_callback_threads() < other.has_num_callback_threads() ? -1 : 1;
  } else if (!(num_callback_threads() == other.num_callback_threads())) {
    return num_callback_threads() < other.num_callback_threads() ? -1 : 1;
  }
  if (!(has_nccl_num_streams() == other.has_nccl_num_streams())) {
    return has_nccl_num_streams() < other.has_nccl_num_streams() ? -1 : 1;
  } else if (!(nccl_num_streams() == other.nccl_num_streams())) {
    return nccl_num_streams() < other.nccl_num_streams() ? -1 : 1;
  }
  if (!(has_nccl_fusion_threshold_mb() == other.has_nccl_fusion_threshold_mb())) {
    return has_nccl_fusion_threshold_mb() < other.has_nccl_fusion_threshold_mb() ? -1 : 1;
  } else if (!(nccl_fusion_threshold_mb() == other.nccl_fusion_threshold_mb())) {
    return nccl_fusion_threshold_mb() < other.nccl_fusion_threshold_mb() ? -1 : 1;
  }
  if (!(has_nccl_fusion_all_reduce() == other.has_nccl_fusion_all_reduce())) {
    return has_nccl_fusion_all_reduce() < other.has_nccl_fusion_all_reduce() ? -1 : 1;
  } else if (!(nccl_fusion_all_reduce() == other.nccl_fusion_all_reduce())) {
    return nccl_fusion_all_reduce() < other.nccl_fusion_all_reduce() ? -1 : 1;
  }
  if (!(has_nccl_fusion_reduce_scatter() == other.has_nccl_fusion_reduce_scatter())) {
    return has_nccl_fusion_reduce_scatter() < other.has_nccl_fusion_reduce_scatter() ? -1 : 1;
  } else if (!(nccl_fusion_reduce_scatter() == other.nccl_fusion_reduce_scatter())) {
    return nccl_fusion_reduce_scatter() < other.nccl_fusion_reduce_scatter() ? -1 : 1;
  }
  if (!(has_nccl_fusion_all_gather() == other.has_nccl_fusion_all_gather())) {
    return has_nccl_fusion_all_gather() < other.has_nccl_fusion_all_gather() ? -1 : 1;
  } else if (!(nccl_fusion_all_gather() == other.nccl_fusion_all_gather())) {
    return nccl_fusion_all_gather() < other.nccl_fusion_all_gather() ? -1 : 1;
  }
  if (!(has_nccl_fusion_reduce() == other.has_nccl_fusion_reduce())) {
    return has_nccl_fusion_reduce() < other.has_nccl_fusion_reduce() ? -1 : 1;
  } else if (!(nccl_fusion_reduce() == other.nccl_fusion_reduce())) {
    return nccl_fusion_reduce() < other.nccl_fusion_reduce() ? -1 : 1;
  }
  if (!(has_nccl_fusion_broadcast() == other.has_nccl_fusion_broadcast())) {
    return has_nccl_fusion_broadcast() < other.has_nccl_fusion_broadcast() ? -1 : 1;
  } else if (!(nccl_fusion_broadcast() == other.nccl_fusion_broadcast())) {
    return nccl_fusion_broadcast() < other.nccl_fusion_broadcast() ? -1 : 1;
  }
  if (!(has_nccl_fusion_all_reduce_use_buffer() == other.has_nccl_fusion_all_reduce_use_buffer())) {
    return has_nccl_fusion_all_reduce_use_buffer() < other.has_nccl_fusion_all_reduce_use_buffer() ? -1 : 1;
  } else if (!(nccl_fusion_all_reduce_use_buffer() == other.nccl_fusion_all_reduce_use_buffer())) {
    return nccl_fusion_all_reduce_use_buffer() < other.nccl_fusion_all_reduce_use_buffer() ? -1 : 1;
  }
  if (!(has_nccl_fusion_max_ops() == other.has_nccl_fusion_max_ops())) {
    return has_nccl_fusion_max_ops() < other.has_nccl_fusion_max_ops() ? -1 : 1;
  } else if (!(nccl_fusion_max_ops() == other.nccl_fusion_max_ops())) {
    return nccl_fusion_max_ops() < other.nccl_fusion_max_ops() ? -1 : 1;
  }
  if (!(has_nccl_enable_all_to_all() == other.has_nccl_enable_all_to_all())) {
    return has_nccl_enable_all_to_all() < other.has_nccl_enable_all_to_all() ? -1 : 1;
  } else if (!(nccl_enable_all_to_all() == other.nccl_enable_all_to_all())) {
    return nccl_enable_all_to_all() < other.nccl_enable_all_to_all() ? -1 : 1;
  }
  if (!(has_nccl_enable_mixed_fusion() == other.has_nccl_enable_mixed_fusion())) {
    return has_nccl_enable_mixed_fusion() < other.has_nccl_enable_mixed_fusion() ? -1 : 1;
  } else if (!(nccl_enable_mixed_fusion() == other.nccl_enable_mixed_fusion())) {
    return nccl_enable_mixed_fusion() < other.nccl_enable_mixed_fusion() ? -1 : 1;
  }
  return 0;
}

bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::operator==(const _CollectiveBoxingConf_& other) const {
  return true
    && has_enable_fusion() == other.has_enable_fusion() 
    && enable_fusion() == other.enable_fusion()
    && has_num_callback_threads() == other.has_num_callback_threads() 
    && num_callback_threads() == other.num_callback_threads()
    && has_nccl_num_streams() == other.has_nccl_num_streams() 
    && nccl_num_streams() == other.nccl_num_streams()
    && has_nccl_fusion_threshold_mb() == other.has_nccl_fusion_threshold_mb() 
    && nccl_fusion_threshold_mb() == other.nccl_fusion_threshold_mb()
    && has_nccl_fusion_all_reduce() == other.has_nccl_fusion_all_reduce() 
    && nccl_fusion_all_reduce() == other.nccl_fusion_all_reduce()
    && has_nccl_fusion_reduce_scatter() == other.has_nccl_fusion_reduce_scatter() 
    && nccl_fusion_reduce_scatter() == other.nccl_fusion_reduce_scatter()
    && has_nccl_fusion_all_gather() == other.has_nccl_fusion_all_gather() 
    && nccl_fusion_all_gather() == other.nccl_fusion_all_gather()
    && has_nccl_fusion_reduce() == other.has_nccl_fusion_reduce() 
    && nccl_fusion_reduce() == other.nccl_fusion_reduce()
    && has_nccl_fusion_broadcast() == other.has_nccl_fusion_broadcast() 
    && nccl_fusion_broadcast() == other.nccl_fusion_broadcast()
    && has_nccl_fusion_all_reduce_use_buffer() == other.has_nccl_fusion_all_reduce_use_buffer() 
    && nccl_fusion_all_reduce_use_buffer() == other.nccl_fusion_all_reduce_use_buffer()
    && has_nccl_fusion_max_ops() == other.has_nccl_fusion_max_ops() 
    && nccl_fusion_max_ops() == other.nccl_fusion_max_ops()
    && has_nccl_enable_all_to_all() == other.has_nccl_enable_all_to_all() 
    && nccl_enable_all_to_all() == other.nccl_enable_all_to_all()
    && has_nccl_enable_mixed_fusion() == other.has_nccl_enable_mixed_fusion() 
    && nccl_enable_mixed_fusion() == other.nccl_enable_mixed_fusion()
  ;
}

std::size_t ConstCollectiveBoxingConf::_CollectiveBoxingConf_::__CalcHash__() const {
  return 0
    ^ (has_enable_fusion() ? std::hash<bool>()(enable_fusion()) : 0)
    ^ (has_num_callback_threads() ? std::hash<int64_t>()(num_callback_threads()) : 0)
    ^ (has_nccl_num_streams() ? std::hash<int64_t>()(nccl_num_streams()) : 0)
    ^ (has_nccl_fusion_threshold_mb() ? std::hash<int64_t>()(nccl_fusion_threshold_mb()) : 0)
    ^ (has_nccl_fusion_all_reduce() ? std::hash<bool>()(nccl_fusion_all_reduce()) : 0)
    ^ (has_nccl_fusion_reduce_scatter() ? std::hash<bool>()(nccl_fusion_reduce_scatter()) : 0)
    ^ (has_nccl_fusion_all_gather() ? std::hash<bool>()(nccl_fusion_all_gather()) : 0)
    ^ (has_nccl_fusion_reduce() ? std::hash<bool>()(nccl_fusion_reduce()) : 0)
    ^ (has_nccl_fusion_broadcast() ? std::hash<bool>()(nccl_fusion_broadcast()) : 0)
    ^ (has_nccl_fusion_all_reduce_use_buffer() ? std::hash<bool>()(nccl_fusion_all_reduce_use_buffer()) : 0)
    ^ (has_nccl_fusion_max_ops() ? std::hash<int64_t>()(nccl_fusion_max_ops()) : 0)
    ^ (has_nccl_enable_all_to_all() ? std::hash<bool>()(nccl_enable_all_to_all()) : 0)
    ^ (has_nccl_enable_mixed_fusion() ? std::hash<bool>()(nccl_enable_mixed_fusion()) : 0)
  ;
}

bool ConstCollectiveBoxingConf::_CollectiveBoxingConf_::operator<(const _CollectiveBoxingConf_& other) const {
  return false
    || !(has_enable_fusion() == other.has_enable_fusion()) ? 
      has_enable_fusion() < other.has_enable_fusion() : false
    || !(enable_fusion() == other.enable_fusion()) ? 
      enable_fusion() < other.enable_fusion() : false
    || !(has_num_callback_threads() == other.has_num_callback_threads()) ? 
      has_num_callback_threads() < other.has_num_callback_threads() : false
    || !(num_callback_threads() == other.num_callback_threads()) ? 
      num_callback_threads() < other.num_callback_threads() : false
    || !(has_nccl_num_streams() == other.has_nccl_num_streams()) ? 
      has_nccl_num_streams() < other.has_nccl_num_streams() : false
    || !(nccl_num_streams() == other.nccl_num_streams()) ? 
      nccl_num_streams() < other.nccl_num_streams() : false
    || !(has_nccl_fusion_threshold_mb() == other.has_nccl_fusion_threshold_mb()) ? 
      has_nccl_fusion_threshold_mb() < other.has_nccl_fusion_threshold_mb() : false
    || !(nccl_fusion_threshold_mb() == other.nccl_fusion_threshold_mb()) ? 
      nccl_fusion_threshold_mb() < other.nccl_fusion_threshold_mb() : false
    || !(has_nccl_fusion_all_reduce() == other.has_nccl_fusion_all_reduce()) ? 
      has_nccl_fusion_all_reduce() < other.has_nccl_fusion_all_reduce() : false
    || !(nccl_fusion_all_reduce() == other.nccl_fusion_all_reduce()) ? 
      nccl_fusion_all_reduce() < other.nccl_fusion_all_reduce() : false
    || !(has_nccl_fusion_reduce_scatter() == other.has_nccl_fusion_reduce_scatter()) ? 
      has_nccl_fusion_reduce_scatter() < other.has_nccl_fusion_reduce_scatter() : false
    || !(nccl_fusion_reduce_scatter() == other.nccl_fusion_reduce_scatter()) ? 
      nccl_fusion_reduce_scatter() < other.nccl_fusion_reduce_scatter() : false
    || !(has_nccl_fusion_all_gather() == other.has_nccl_fusion_all_gather()) ? 
      has_nccl_fusion_all_gather() < other.has_nccl_fusion_all_gather() : false
    || !(nccl_fusion_all_gather() == other.nccl_fusion_all_gather()) ? 
      nccl_fusion_all_gather() < other.nccl_fusion_all_gather() : false
    || !(has_nccl_fusion_reduce() == other.has_nccl_fusion_reduce()) ? 
      has_nccl_fusion_reduce() < other.has_nccl_fusion_reduce() : false
    || !(nccl_fusion_reduce() == other.nccl_fusion_reduce()) ? 
      nccl_fusion_reduce() < other.nccl_fusion_reduce() : false
    || !(has_nccl_fusion_broadcast() == other.has_nccl_fusion_broadcast()) ? 
      has_nccl_fusion_broadcast() < other.has_nccl_fusion_broadcast() : false
    || !(nccl_fusion_broadcast() == other.nccl_fusion_broadcast()) ? 
      nccl_fusion_broadcast() < other.nccl_fusion_broadcast() : false
    || !(has_nccl_fusion_all_reduce_use_buffer() == other.has_nccl_fusion_all_reduce_use_buffer()) ? 
      has_nccl_fusion_all_reduce_use_buffer() < other.has_nccl_fusion_all_reduce_use_buffer() : false
    || !(nccl_fusion_all_reduce_use_buffer() == other.nccl_fusion_all_reduce_use_buffer()) ? 
      nccl_fusion_all_reduce_use_buffer() < other.nccl_fusion_all_reduce_use_buffer() : false
    || !(has_nccl_fusion_max_ops() == other.has_nccl_fusion_max_ops()) ? 
      has_nccl_fusion_max_ops() < other.has_nccl_fusion_max_ops() : false
    || !(nccl_fusion_max_ops() == other.nccl_fusion_max_ops()) ? 
      nccl_fusion_max_ops() < other.nccl_fusion_max_ops() : false
    || !(has_nccl_enable_all_to_all() == other.has_nccl_enable_all_to_all()) ? 
      has_nccl_enable_all_to_all() < other.has_nccl_enable_all_to_all() : false
    || !(nccl_enable_all_to_all() == other.nccl_enable_all_to_all()) ? 
      nccl_enable_all_to_all() < other.nccl_enable_all_to_all() : false
    || !(has_nccl_enable_mixed_fusion() == other.has_nccl_enable_mixed_fusion()) ? 
      has_nccl_enable_mixed_fusion() < other.has_nccl_enable_mixed_fusion() : false
    || !(nccl_enable_mixed_fusion() == other.nccl_enable_mixed_fusion()) ? 
      nccl_enable_mixed_fusion() < other.nccl_enable_mixed_fusion() : false
  ;
}

using _CollectiveBoxingConf_ =  ConstCollectiveBoxingConf::_CollectiveBoxingConf_;
ConstCollectiveBoxingConf::ConstCollectiveBoxingConf(const ::std::shared_ptr<_CollectiveBoxingConf_>& data): data_(data) {}
ConstCollectiveBoxingConf::ConstCollectiveBoxingConf(): data_(::std::make_shared<_CollectiveBoxingConf_>()) {}
ConstCollectiveBoxingConf::ConstCollectiveBoxingConf(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf) {
  BuildFromProto(proto_collectiveboxingconf);
}
ConstCollectiveBoxingConf::ConstCollectiveBoxingConf(const ConstCollectiveBoxingConf&) = default;
ConstCollectiveBoxingConf::ConstCollectiveBoxingConf(ConstCollectiveBoxingConf&&) noexcept = default;
ConstCollectiveBoxingConf::~ConstCollectiveBoxingConf() = default;

void ConstCollectiveBoxingConf::ToProto(PbMessage* proto_collectiveboxingconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::CollectiveBoxingConf*>(proto_collectiveboxingconf));
}
  
::std::string ConstCollectiveBoxingConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCollectiveBoxingConf::__Empty__() const {
  return !data_;
}

int ConstCollectiveBoxingConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"enable_fusion", 1},
    {"num_callback_threads", 2},
    {"nccl_num_streams", 101},
    {"nccl_fusion_threshold_mb", 102},
    {"nccl_fusion_all_reduce", 103},
    {"nccl_fusion_reduce_scatter", 104},
    {"nccl_fusion_all_gather", 105},
    {"nccl_fusion_reduce", 106},
    {"nccl_fusion_broadcast", 107},
    {"nccl_fusion_all_reduce_use_buffer", 108},
    {"nccl_fusion_max_ops", 109},
    {"nccl_enable_all_to_all", 110},
    {"nccl_enable_mixed_fusion", 111},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstCollectiveBoxingConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 101:
    case 102:
    case 103:
    case 104:
    case 105:
    case 106:
    case 107:
    case 108:
    case 109:
    case 110:
    case 111:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstCollectiveBoxingConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 101: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 102: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 103: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 104: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 105: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 106: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 107: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 108: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 109: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 110: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 111: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCollectiveBoxingConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &enable_fusion();
    case 2: return &num_callback_threads();
    case 101: return &nccl_num_streams();
    case 102: return &nccl_fusion_threshold_mb();
    case 103: return &nccl_fusion_all_reduce();
    case 104: return &nccl_fusion_reduce_scatter();
    case 105: return &nccl_fusion_all_gather();
    case 106: return &nccl_fusion_reduce();
    case 107: return &nccl_fusion_broadcast();
    case 108: return &nccl_fusion_all_reduce_use_buffer();
    case 109: return &nccl_fusion_max_ops();
    case 110: return &nccl_enable_all_to_all();
    case 111: return &nccl_enable_mixed_fusion();
    default: return nullptr;
  }
}

// required or optional field enable_fusion
bool ConstCollectiveBoxingConf::has_enable_fusion() const {
  return __SharedPtrOrDefault__()->has_enable_fusion();
}
const bool& ConstCollectiveBoxingConf::enable_fusion() const {
  return __SharedPtrOrDefault__()->enable_fusion();
}
// used by pybind11 only
// required or optional field num_callback_threads
bool ConstCollectiveBoxingConf::has_num_callback_threads() const {
  return __SharedPtrOrDefault__()->has_num_callback_threads();
}
const int64_t& ConstCollectiveBoxingConf::num_callback_threads() const {
  return __SharedPtrOrDefault__()->num_callback_threads();
}
// used by pybind11 only
// required or optional field nccl_num_streams
bool ConstCollectiveBoxingConf::has_nccl_num_streams() const {
  return __SharedPtrOrDefault__()->has_nccl_num_streams();
}
const int64_t& ConstCollectiveBoxingConf::nccl_num_streams() const {
  return __SharedPtrOrDefault__()->nccl_num_streams();
}
// used by pybind11 only
// required or optional field nccl_fusion_threshold_mb
bool ConstCollectiveBoxingConf::has_nccl_fusion_threshold_mb() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_threshold_mb();
}
const int64_t& ConstCollectiveBoxingConf::nccl_fusion_threshold_mb() const {
  return __SharedPtrOrDefault__()->nccl_fusion_threshold_mb();
}
// used by pybind11 only
// required or optional field nccl_fusion_all_reduce
bool ConstCollectiveBoxingConf::has_nccl_fusion_all_reduce() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_all_reduce();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_all_reduce() const {
  return __SharedPtrOrDefault__()->nccl_fusion_all_reduce();
}
// used by pybind11 only
// required or optional field nccl_fusion_reduce_scatter
bool ConstCollectiveBoxingConf::has_nccl_fusion_reduce_scatter() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_reduce_scatter();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_reduce_scatter() const {
  return __SharedPtrOrDefault__()->nccl_fusion_reduce_scatter();
}
// used by pybind11 only
// required or optional field nccl_fusion_all_gather
bool ConstCollectiveBoxingConf::has_nccl_fusion_all_gather() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_all_gather();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_all_gather() const {
  return __SharedPtrOrDefault__()->nccl_fusion_all_gather();
}
// used by pybind11 only
// required or optional field nccl_fusion_reduce
bool ConstCollectiveBoxingConf::has_nccl_fusion_reduce() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_reduce();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_reduce() const {
  return __SharedPtrOrDefault__()->nccl_fusion_reduce();
}
// used by pybind11 only
// required or optional field nccl_fusion_broadcast
bool ConstCollectiveBoxingConf::has_nccl_fusion_broadcast() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_broadcast();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_broadcast() const {
  return __SharedPtrOrDefault__()->nccl_fusion_broadcast();
}
// used by pybind11 only
// required or optional field nccl_fusion_all_reduce_use_buffer
bool ConstCollectiveBoxingConf::has_nccl_fusion_all_reduce_use_buffer() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_all_reduce_use_buffer();
}
const bool& ConstCollectiveBoxingConf::nccl_fusion_all_reduce_use_buffer() const {
  return __SharedPtrOrDefault__()->nccl_fusion_all_reduce_use_buffer();
}
// used by pybind11 only
// required or optional field nccl_fusion_max_ops
bool ConstCollectiveBoxingConf::has_nccl_fusion_max_ops() const {
  return __SharedPtrOrDefault__()->has_nccl_fusion_max_ops();
}
const int64_t& ConstCollectiveBoxingConf::nccl_fusion_max_ops() const {
  return __SharedPtrOrDefault__()->nccl_fusion_max_ops();
}
// used by pybind11 only
// required or optional field nccl_enable_all_to_all
bool ConstCollectiveBoxingConf::has_nccl_enable_all_to_all() const {
  return __SharedPtrOrDefault__()->has_nccl_enable_all_to_all();
}
const bool& ConstCollectiveBoxingConf::nccl_enable_all_to_all() const {
  return __SharedPtrOrDefault__()->nccl_enable_all_to_all();
}
// used by pybind11 only
// required or optional field nccl_enable_mixed_fusion
bool ConstCollectiveBoxingConf::has_nccl_enable_mixed_fusion() const {
  return __SharedPtrOrDefault__()->has_nccl_enable_mixed_fusion();
}
const bool& ConstCollectiveBoxingConf::nccl_enable_mixed_fusion() const {
  return __SharedPtrOrDefault__()->nccl_enable_mixed_fusion();
}
// used by pybind11 only

::std::shared_ptr<ConstCollectiveBoxingConf> ConstCollectiveBoxingConf::__SharedConst__() const {
  return ::std::make_shared<ConstCollectiveBoxingConf>(data_);
}
int64_t ConstCollectiveBoxingConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCollectiveBoxingConf::operator==(const ConstCollectiveBoxingConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCollectiveBoxingConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCollectiveBoxingConf::operator<(const ConstCollectiveBoxingConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CollectiveBoxingConf_>& ConstCollectiveBoxingConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CollectiveBoxingConf_> default_ptr = std::make_shared<_CollectiveBoxingConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_CollectiveBoxingConf_>& ConstCollectiveBoxingConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _CollectiveBoxingConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCollectiveBoxingConf
void ConstCollectiveBoxingConf::BuildFromProto(const PbMessage& proto_collectiveboxingconf) {
  data_ = ::std::make_shared<_CollectiveBoxingConf_>(dynamic_cast<const ::oneflow::CollectiveBoxingConf&>(proto_collectiveboxingconf));
}

CollectiveBoxingConf::CollectiveBoxingConf(const ::std::shared_ptr<_CollectiveBoxingConf_>& data)
  : ConstCollectiveBoxingConf(data) {}
CollectiveBoxingConf::CollectiveBoxingConf(const CollectiveBoxingConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CollectiveBoxingConf> resize
CollectiveBoxingConf::CollectiveBoxingConf(CollectiveBoxingConf&&) noexcept = default; 
CollectiveBoxingConf::CollectiveBoxingConf(const ::oneflow::CollectiveBoxingConf& proto_collectiveboxingconf) {
  InitFromProto(proto_collectiveboxingconf);
}
CollectiveBoxingConf::CollectiveBoxingConf() = default;

CollectiveBoxingConf::~CollectiveBoxingConf() = default;

void CollectiveBoxingConf::InitFromProto(const PbMessage& proto_collectiveboxingconf) {
  BuildFromProto(proto_collectiveboxingconf);
}
  
void* CollectiveBoxingConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_enable_fusion();
    case 2: return mutable_num_callback_threads();
    case 101: return mutable_nccl_num_streams();
    case 102: return mutable_nccl_fusion_threshold_mb();
    case 103: return mutable_nccl_fusion_all_reduce();
    case 104: return mutable_nccl_fusion_reduce_scatter();
    case 105: return mutable_nccl_fusion_all_gather();
    case 106: return mutable_nccl_fusion_reduce();
    case 107: return mutable_nccl_fusion_broadcast();
    case 108: return mutable_nccl_fusion_all_reduce_use_buffer();
    case 109: return mutable_nccl_fusion_max_ops();
    case 110: return mutable_nccl_enable_all_to_all();
    case 111: return mutable_nccl_enable_mixed_fusion();
    default: return nullptr;
  }
}

bool CollectiveBoxingConf::operator==(const CollectiveBoxingConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CollectiveBoxingConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CollectiveBoxingConf::operator<(const CollectiveBoxingConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CollectiveBoxingConf::Clear() {
  if (data_) { data_.reset(); }
}
void CollectiveBoxingConf::CopyFrom(const CollectiveBoxingConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CollectiveBoxingConf& CollectiveBoxingConf::operator=(const CollectiveBoxingConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field enable_fusion
void CollectiveBoxingConf::clear_enable_fusion() {
  return __SharedPtr__()->clear_enable_fusion();
}
void CollectiveBoxingConf::set_enable_fusion(const bool& value) {
  return __SharedPtr__()->set_enable_fusion(value);
}
bool* CollectiveBoxingConf::mutable_enable_fusion() {
  return  __SharedPtr__()->mutable_enable_fusion();
}
// required or optional field num_callback_threads
void CollectiveBoxingConf::clear_num_callback_threads() {
  return __SharedPtr__()->clear_num_callback_threads();
}
void CollectiveBoxingConf::set_num_callback_threads(const int64_t& value) {
  return __SharedPtr__()->set_num_callback_threads(value);
}
int64_t* CollectiveBoxingConf::mutable_num_callback_threads() {
  return  __SharedPtr__()->mutable_num_callback_threads();
}
// required or optional field nccl_num_streams
void CollectiveBoxingConf::clear_nccl_num_streams() {
  return __SharedPtr__()->clear_nccl_num_streams();
}
void CollectiveBoxingConf::set_nccl_num_streams(const int64_t& value) {
  return __SharedPtr__()->set_nccl_num_streams(value);
}
int64_t* CollectiveBoxingConf::mutable_nccl_num_streams() {
  return  __SharedPtr__()->mutable_nccl_num_streams();
}
// required or optional field nccl_fusion_threshold_mb
void CollectiveBoxingConf::clear_nccl_fusion_threshold_mb() {
  return __SharedPtr__()->clear_nccl_fusion_threshold_mb();
}
void CollectiveBoxingConf::set_nccl_fusion_threshold_mb(const int64_t& value) {
  return __SharedPtr__()->set_nccl_fusion_threshold_mb(value);
}
int64_t* CollectiveBoxingConf::mutable_nccl_fusion_threshold_mb() {
  return  __SharedPtr__()->mutable_nccl_fusion_threshold_mb();
}
// required or optional field nccl_fusion_all_reduce
void CollectiveBoxingConf::clear_nccl_fusion_all_reduce() {
  return __SharedPtr__()->clear_nccl_fusion_all_reduce();
}
void CollectiveBoxingConf::set_nccl_fusion_all_reduce(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_all_reduce(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_all_reduce() {
  return  __SharedPtr__()->mutable_nccl_fusion_all_reduce();
}
// required or optional field nccl_fusion_reduce_scatter
void CollectiveBoxingConf::clear_nccl_fusion_reduce_scatter() {
  return __SharedPtr__()->clear_nccl_fusion_reduce_scatter();
}
void CollectiveBoxingConf::set_nccl_fusion_reduce_scatter(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_reduce_scatter(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_reduce_scatter() {
  return  __SharedPtr__()->mutable_nccl_fusion_reduce_scatter();
}
// required or optional field nccl_fusion_all_gather
void CollectiveBoxingConf::clear_nccl_fusion_all_gather() {
  return __SharedPtr__()->clear_nccl_fusion_all_gather();
}
void CollectiveBoxingConf::set_nccl_fusion_all_gather(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_all_gather(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_all_gather() {
  return  __SharedPtr__()->mutable_nccl_fusion_all_gather();
}
// required or optional field nccl_fusion_reduce
void CollectiveBoxingConf::clear_nccl_fusion_reduce() {
  return __SharedPtr__()->clear_nccl_fusion_reduce();
}
void CollectiveBoxingConf::set_nccl_fusion_reduce(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_reduce(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_reduce() {
  return  __SharedPtr__()->mutable_nccl_fusion_reduce();
}
// required or optional field nccl_fusion_broadcast
void CollectiveBoxingConf::clear_nccl_fusion_broadcast() {
  return __SharedPtr__()->clear_nccl_fusion_broadcast();
}
void CollectiveBoxingConf::set_nccl_fusion_broadcast(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_broadcast(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_broadcast() {
  return  __SharedPtr__()->mutable_nccl_fusion_broadcast();
}
// required or optional field nccl_fusion_all_reduce_use_buffer
void CollectiveBoxingConf::clear_nccl_fusion_all_reduce_use_buffer() {
  return __SharedPtr__()->clear_nccl_fusion_all_reduce_use_buffer();
}
void CollectiveBoxingConf::set_nccl_fusion_all_reduce_use_buffer(const bool& value) {
  return __SharedPtr__()->set_nccl_fusion_all_reduce_use_buffer(value);
}
bool* CollectiveBoxingConf::mutable_nccl_fusion_all_reduce_use_buffer() {
  return  __SharedPtr__()->mutable_nccl_fusion_all_reduce_use_buffer();
}
// required or optional field nccl_fusion_max_ops
void CollectiveBoxingConf::clear_nccl_fusion_max_ops() {
  return __SharedPtr__()->clear_nccl_fusion_max_ops();
}
void CollectiveBoxingConf::set_nccl_fusion_max_ops(const int64_t& value) {
  return __SharedPtr__()->set_nccl_fusion_max_ops(value);
}
int64_t* CollectiveBoxingConf::mutable_nccl_fusion_max_ops() {
  return  __SharedPtr__()->mutable_nccl_fusion_max_ops();
}
// required or optional field nccl_enable_all_to_all
void CollectiveBoxingConf::clear_nccl_enable_all_to_all() {
  return __SharedPtr__()->clear_nccl_enable_all_to_all();
}
void CollectiveBoxingConf::set_nccl_enable_all_to_all(const bool& value) {
  return __SharedPtr__()->set_nccl_enable_all_to_all(value);
}
bool* CollectiveBoxingConf::mutable_nccl_enable_all_to_all() {
  return  __SharedPtr__()->mutable_nccl_enable_all_to_all();
}
// required or optional field nccl_enable_mixed_fusion
void CollectiveBoxingConf::clear_nccl_enable_mixed_fusion() {
  return __SharedPtr__()->clear_nccl_enable_mixed_fusion();
}
void CollectiveBoxingConf::set_nccl_enable_mixed_fusion(const bool& value) {
  return __SharedPtr__()->set_nccl_enable_mixed_fusion(value);
}
bool* CollectiveBoxingConf::mutable_nccl_enable_mixed_fusion() {
  return  __SharedPtr__()->mutable_nccl_enable_mixed_fusion();
}

::std::shared_ptr<CollectiveBoxingConf> CollectiveBoxingConf::__SharedMutable__() {
  return ::std::make_shared<CollectiveBoxingConf>(__SharedPtr__());
}
ConstCudnnConfig::_CudnnConfig_::_CudnnConfig_() { Clear(); }
ConstCudnnConfig::_CudnnConfig_::_CudnnConfig_(const _CudnnConfig_& other) { CopyFrom(other); }
ConstCudnnConfig::_CudnnConfig_::_CudnnConfig_(const ::oneflow::CudnnConfig& proto_cudnnconfig) {
  InitFromProto(proto_cudnnconfig);
}
ConstCudnnConfig::_CudnnConfig_::_CudnnConfig_(_CudnnConfig_&& other) = default;
ConstCudnnConfig::_CudnnConfig_::~_CudnnConfig_() = default;

void ConstCudnnConfig::_CudnnConfig_::InitFromProto(const ::oneflow::CudnnConfig& proto_cudnnconfig) {
  Clear();
  // required_or_optional field: enable_cudnn
  if (proto_cudnnconfig.has_enable_cudnn()) {
    set_enable_cudnn(proto_cudnnconfig.enable_cudnn());
  }
  // required_or_optional field: cudnn_buf_limit_mbyte
  if (proto_cudnnconfig.has_cudnn_buf_limit_mbyte()) {
    set_cudnn_buf_limit_mbyte(proto_cudnnconfig.cudnn_buf_limit_mbyte());
  }
  // required_or_optional field: cudnn_conv_force_fwd_algo
  if (proto_cudnnconfig.has_cudnn_conv_force_fwd_algo()) {
    set_cudnn_conv_force_fwd_algo(proto_cudnnconfig.cudnn_conv_force_fwd_algo());
  }
  // required_or_optional field: cudnn_conv_force_bwd_data_algo
  if (proto_cudnnconfig.has_cudnn_conv_force_bwd_data_algo()) {
    set_cudnn_conv_force_bwd_data_algo(proto_cudnnconfig.cudnn_conv_force_bwd_data_algo());
  }
  // required_or_optional field: cudnn_conv_force_bwd_filter_algo
  if (proto_cudnnconfig.has_cudnn_conv_force_bwd_filter_algo()) {
    set_cudnn_conv_force_bwd_filter_algo(proto_cudnnconfig.cudnn_conv_force_bwd_filter_algo());
  }
  // required_or_optional field: cudnn_conv_heuristic_search_algo
  if (proto_cudnnconfig.has_cudnn_conv_heuristic_search_algo()) {
    set_cudnn_conv_heuristic_search_algo(proto_cudnnconfig.cudnn_conv_heuristic_search_algo());
  }
  // required_or_optional field: cudnn_conv_use_deterministic_algo_only
  if (proto_cudnnconfig.has_cudnn_conv_use_deterministic_algo_only()) {
    set_cudnn_conv_use_deterministic_algo_only(proto_cudnnconfig.cudnn_conv_use_deterministic_algo_only());
  }
  // required_or_optional field: enable_cudnn_fused_normalization_add_relu
  if (proto_cudnnconfig.has_enable_cudnn_fused_normalization_add_relu()) {
    set_enable_cudnn_fused_normalization_add_relu(proto_cudnnconfig.enable_cudnn_fused_normalization_add_relu());
  }
  // required_or_optional field: cudnn_conv_enable_pseudo_half
  if (proto_cudnnconfig.has_cudnn_conv_enable_pseudo_half()) {
    set_cudnn_conv_enable_pseudo_half(proto_cudnnconfig.cudnn_conv_enable_pseudo_half());
  }
    
}

void ConstCudnnConfig::_CudnnConfig_::ToProto(::oneflow::CudnnConfig* proto_cudnnconfig) const {
  proto_cudnnconfig->Clear();
  // required_or_optional field: enable_cudnn
  if (this->has_enable_cudnn()) {
    proto_cudnnconfig->set_enable_cudnn(enable_cudnn());
    }
  // required_or_optional field: cudnn_buf_limit_mbyte
  if (this->has_cudnn_buf_limit_mbyte()) {
    proto_cudnnconfig->set_cudnn_buf_limit_mbyte(cudnn_buf_limit_mbyte());
    }
  // required_or_optional field: cudnn_conv_force_fwd_algo
  if (this->has_cudnn_conv_force_fwd_algo()) {
    proto_cudnnconfig->set_cudnn_conv_force_fwd_algo(cudnn_conv_force_fwd_algo());
    }
  // required_or_optional field: cudnn_conv_force_bwd_data_algo
  if (this->has_cudnn_conv_force_bwd_data_algo()) {
    proto_cudnnconfig->set_cudnn_conv_force_bwd_data_algo(cudnn_conv_force_bwd_data_algo());
    }
  // required_or_optional field: cudnn_conv_force_bwd_filter_algo
  if (this->has_cudnn_conv_force_bwd_filter_algo()) {
    proto_cudnnconfig->set_cudnn_conv_force_bwd_filter_algo(cudnn_conv_force_bwd_filter_algo());
    }
  // required_or_optional field: cudnn_conv_heuristic_search_algo
  if (this->has_cudnn_conv_heuristic_search_algo()) {
    proto_cudnnconfig->set_cudnn_conv_heuristic_search_algo(cudnn_conv_heuristic_search_algo());
    }
  // required_or_optional field: cudnn_conv_use_deterministic_algo_only
  if (this->has_cudnn_conv_use_deterministic_algo_only()) {
    proto_cudnnconfig->set_cudnn_conv_use_deterministic_algo_only(cudnn_conv_use_deterministic_algo_only());
    }
  // required_or_optional field: enable_cudnn_fused_normalization_add_relu
  if (this->has_enable_cudnn_fused_normalization_add_relu()) {
    proto_cudnnconfig->set_enable_cudnn_fused_normalization_add_relu(enable_cudnn_fused_normalization_add_relu());
    }
  // required_or_optional field: cudnn_conv_enable_pseudo_half
  if (this->has_cudnn_conv_enable_pseudo_half()) {
    proto_cudnnconfig->set_cudnn_conv_enable_pseudo_half(cudnn_conv_enable_pseudo_half());
    }

}

::std::string ConstCudnnConfig::_CudnnConfig_::DebugString() const {
  ::oneflow::CudnnConfig proto_cudnnconfig;
  this->ToProto(&proto_cudnnconfig);
  return proto_cudnnconfig.DebugString();
}

void ConstCudnnConfig::_CudnnConfig_::Clear() {
  clear_enable_cudnn();
  clear_cudnn_buf_limit_mbyte();
  clear_cudnn_conv_force_fwd_algo();
  clear_cudnn_conv_force_bwd_data_algo();
  clear_cudnn_conv_force_bwd_filter_algo();
  clear_cudnn_conv_heuristic_search_algo();
  clear_cudnn_conv_use_deterministic_algo_only();
  clear_enable_cudnn_fused_normalization_add_relu();
  clear_cudnn_conv_enable_pseudo_half();
}

void ConstCudnnConfig::_CudnnConfig_::CopyFrom(const _CudnnConfig_& other) {
  if (other.has_enable_cudnn()) {
    set_enable_cudnn(other.enable_cudnn());
  } else {
    clear_enable_cudnn();
  }
  if (other.has_cudnn_buf_limit_mbyte()) {
    set_cudnn_buf_limit_mbyte(other.cudnn_buf_limit_mbyte());
  } else {
    clear_cudnn_buf_limit_mbyte();
  }
  if (other.has_cudnn_conv_force_fwd_algo()) {
    set_cudnn_conv_force_fwd_algo(other.cudnn_conv_force_fwd_algo());
  } else {
    clear_cudnn_conv_force_fwd_algo();
  }
  if (other.has_cudnn_conv_force_bwd_data_algo()) {
    set_cudnn_conv_force_bwd_data_algo(other.cudnn_conv_force_bwd_data_algo());
  } else {
    clear_cudnn_conv_force_bwd_data_algo();
  }
  if (other.has_cudnn_conv_force_bwd_filter_algo()) {
    set_cudnn_conv_force_bwd_filter_algo(other.cudnn_conv_force_bwd_filter_algo());
  } else {
    clear_cudnn_conv_force_bwd_filter_algo();
  }
  if (other.has_cudnn_conv_heuristic_search_algo()) {
    set_cudnn_conv_heuristic_search_algo(other.cudnn_conv_heuristic_search_algo());
  } else {
    clear_cudnn_conv_heuristic_search_algo();
  }
  if (other.has_cudnn_conv_use_deterministic_algo_only()) {
    set_cudnn_conv_use_deterministic_algo_only(other.cudnn_conv_use_deterministic_algo_only());
  } else {
    clear_cudnn_conv_use_deterministic_algo_only();
  }
  if (other.has_enable_cudnn_fused_normalization_add_relu()) {
    set_enable_cudnn_fused_normalization_add_relu(other.enable_cudnn_fused_normalization_add_relu());
  } else {
    clear_enable_cudnn_fused_normalization_add_relu();
  }
  if (other.has_cudnn_conv_enable_pseudo_half()) {
    set_cudnn_conv_enable_pseudo_half(other.cudnn_conv_enable_pseudo_half());
  } else {
    clear_cudnn_conv_enable_pseudo_half();
  }
}


// optional field enable_cudnn
bool ConstCudnnConfig::_CudnnConfig_::has_enable_cudnn() const {
  return has_enable_cudnn_;
}
const bool& ConstCudnnConfig::_CudnnConfig_::enable_cudnn() const {
  if (has_enable_cudnn_) { return enable_cudnn_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_enable_cudnn() {
  has_enable_cudnn_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_enable_cudnn(const bool& value) {
  enable_cudnn_ = value;
  has_enable_cudnn_ = true;
}
bool* ConstCudnnConfig::_CudnnConfig_::mutable_enable_cudnn() {
  has_enable_cudnn_ = true;
  return &enable_cudnn_;
}

// optional field cudnn_buf_limit_mbyte
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_buf_limit_mbyte() const {
  return has_cudnn_buf_limit_mbyte_;
}
const int64_t& ConstCudnnConfig::_CudnnConfig_::cudnn_buf_limit_mbyte() const {
  if (has_cudnn_buf_limit_mbyte_) { return cudnn_buf_limit_mbyte_; }
  static const int64_t default_static_value =
    int64_t(1024);
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_buf_limit_mbyte() {
  has_cudnn_buf_limit_mbyte_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_buf_limit_mbyte(const int64_t& value) {
  cudnn_buf_limit_mbyte_ = value;
  has_cudnn_buf_limit_mbyte_ = true;
}
int64_t* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_buf_limit_mbyte() {
  has_cudnn_buf_limit_mbyte_ = true;
  return &cudnn_buf_limit_mbyte_;
}

// optional field cudnn_conv_force_fwd_algo
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_force_fwd_algo() const {
  return has_cudnn_conv_force_fwd_algo_;
}
const int32_t& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_force_fwd_algo() const {
  if (has_cudnn_conv_force_fwd_algo_) { return cudnn_conv_force_fwd_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_force_fwd_algo() {
  has_cudnn_conv_force_fwd_algo_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_force_fwd_algo(const int32_t& value) {
  cudnn_conv_force_fwd_algo_ = value;
  has_cudnn_conv_force_fwd_algo_ = true;
}
int32_t* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_force_fwd_algo() {
  has_cudnn_conv_force_fwd_algo_ = true;
  return &cudnn_conv_force_fwd_algo_;
}

// optional field cudnn_conv_force_bwd_data_algo
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_force_bwd_data_algo() const {
  return has_cudnn_conv_force_bwd_data_algo_;
}
const int32_t& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_force_bwd_data_algo() const {
  if (has_cudnn_conv_force_bwd_data_algo_) { return cudnn_conv_force_bwd_data_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_force_bwd_data_algo() {
  has_cudnn_conv_force_bwd_data_algo_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_force_bwd_data_algo(const int32_t& value) {
  cudnn_conv_force_bwd_data_algo_ = value;
  has_cudnn_conv_force_bwd_data_algo_ = true;
}
int32_t* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_force_bwd_data_algo() {
  has_cudnn_conv_force_bwd_data_algo_ = true;
  return &cudnn_conv_force_bwd_data_algo_;
}

// optional field cudnn_conv_force_bwd_filter_algo
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_force_bwd_filter_algo() const {
  return has_cudnn_conv_force_bwd_filter_algo_;
}
const int32_t& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_force_bwd_filter_algo() const {
  if (has_cudnn_conv_force_bwd_filter_algo_) { return cudnn_conv_force_bwd_filter_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_force_bwd_filter_algo() {
  has_cudnn_conv_force_bwd_filter_algo_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_force_bwd_filter_algo(const int32_t& value) {
  cudnn_conv_force_bwd_filter_algo_ = value;
  has_cudnn_conv_force_bwd_filter_algo_ = true;
}
int32_t* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_force_bwd_filter_algo() {
  has_cudnn_conv_force_bwd_filter_algo_ = true;
  return &cudnn_conv_force_bwd_filter_algo_;
}

// optional field cudnn_conv_heuristic_search_algo
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_heuristic_search_algo() const {
  return has_cudnn_conv_heuristic_search_algo_;
}
const bool& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_heuristic_search_algo() const {
  if (has_cudnn_conv_heuristic_search_algo_) { return cudnn_conv_heuristic_search_algo_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_heuristic_search_algo() {
  has_cudnn_conv_heuristic_search_algo_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_heuristic_search_algo(const bool& value) {
  cudnn_conv_heuristic_search_algo_ = value;
  has_cudnn_conv_heuristic_search_algo_ = true;
}
bool* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_heuristic_search_algo() {
  has_cudnn_conv_heuristic_search_algo_ = true;
  return &cudnn_conv_heuristic_search_algo_;
}

// optional field cudnn_conv_use_deterministic_algo_only
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_use_deterministic_algo_only() const {
  return has_cudnn_conv_use_deterministic_algo_only_;
}
const bool& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_use_deterministic_algo_only() const {
  if (has_cudnn_conv_use_deterministic_algo_only_) { return cudnn_conv_use_deterministic_algo_only_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_use_deterministic_algo_only() {
  has_cudnn_conv_use_deterministic_algo_only_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_use_deterministic_algo_only(const bool& value) {
  cudnn_conv_use_deterministic_algo_only_ = value;
  has_cudnn_conv_use_deterministic_algo_only_ = true;
}
bool* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_use_deterministic_algo_only() {
  has_cudnn_conv_use_deterministic_algo_only_ = true;
  return &cudnn_conv_use_deterministic_algo_only_;
}

// optional field enable_cudnn_fused_normalization_add_relu
bool ConstCudnnConfig::_CudnnConfig_::has_enable_cudnn_fused_normalization_add_relu() const {
  return has_enable_cudnn_fused_normalization_add_relu_;
}
const bool& ConstCudnnConfig::_CudnnConfig_::enable_cudnn_fused_normalization_add_relu() const {
  if (has_enable_cudnn_fused_normalization_add_relu_) { return enable_cudnn_fused_normalization_add_relu_; }
  static const bool default_static_value = bool();
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_enable_cudnn_fused_normalization_add_relu() {
  has_enable_cudnn_fused_normalization_add_relu_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_enable_cudnn_fused_normalization_add_relu(const bool& value) {
  enable_cudnn_fused_normalization_add_relu_ = value;
  has_enable_cudnn_fused_normalization_add_relu_ = true;
}
bool* ConstCudnnConfig::_CudnnConfig_::mutable_enable_cudnn_fused_normalization_add_relu() {
  has_enable_cudnn_fused_normalization_add_relu_ = true;
  return &enable_cudnn_fused_normalization_add_relu_;
}

// optional field cudnn_conv_enable_pseudo_half
bool ConstCudnnConfig::_CudnnConfig_::has_cudnn_conv_enable_pseudo_half() const {
  return has_cudnn_conv_enable_pseudo_half_;
}
const bool& ConstCudnnConfig::_CudnnConfig_::cudnn_conv_enable_pseudo_half() const {
  if (has_cudnn_conv_enable_pseudo_half_) { return cudnn_conv_enable_pseudo_half_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstCudnnConfig::_CudnnConfig_::clear_cudnn_conv_enable_pseudo_half() {
  has_cudnn_conv_enable_pseudo_half_ = false;
}
void ConstCudnnConfig::_CudnnConfig_::set_cudnn_conv_enable_pseudo_half(const bool& value) {
  cudnn_conv_enable_pseudo_half_ = value;
  has_cudnn_conv_enable_pseudo_half_ = true;
}
bool* ConstCudnnConfig::_CudnnConfig_::mutable_cudnn_conv_enable_pseudo_half() {
  has_cudnn_conv_enable_pseudo_half_ = true;
  return &cudnn_conv_enable_pseudo_half_;
}


int ConstCudnnConfig::_CudnnConfig_::compare(const _CudnnConfig_& other) {
  if (!(has_enable_cudnn() == other.has_enable_cudnn())) {
    return has_enable_cudnn() < other.has_enable_cudnn() ? -1 : 1;
  } else if (!(enable_cudnn() == other.enable_cudnn())) {
    return enable_cudnn() < other.enable_cudnn() ? -1 : 1;
  }
  if (!(has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte())) {
    return has_cudnn_buf_limit_mbyte() < other.has_cudnn_buf_limit_mbyte() ? -1 : 1;
  } else if (!(cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte())) {
    return cudnn_buf_limit_mbyte() < other.cudnn_buf_limit_mbyte() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo())) {
    return has_cudnn_conv_force_fwd_algo() < other.has_cudnn_conv_force_fwd_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo())) {
    return cudnn_conv_force_fwd_algo() < other.cudnn_conv_force_fwd_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo())) {
    return has_cudnn_conv_force_bwd_data_algo() < other.has_cudnn_conv_force_bwd_data_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo())) {
    return cudnn_conv_force_bwd_data_algo() < other.cudnn_conv_force_bwd_data_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo())) {
    return has_cudnn_conv_force_bwd_filter_algo() < other.has_cudnn_conv_force_bwd_filter_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo())) {
    return cudnn_conv_force_bwd_filter_algo() < other.cudnn_conv_force_bwd_filter_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo())) {
    return has_cudnn_conv_heuristic_search_algo() < other.has_cudnn_conv_heuristic_search_algo() ? -1 : 1;
  } else if (!(cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo())) {
    return cudnn_conv_heuristic_search_algo() < other.cudnn_conv_heuristic_search_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only())) {
    return has_cudnn_conv_use_deterministic_algo_only() < other.has_cudnn_conv_use_deterministic_algo_only() ? -1 : 1;
  } else if (!(cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only())) {
    return cudnn_conv_use_deterministic_algo_only() < other.cudnn_conv_use_deterministic_algo_only() ? -1 : 1;
  }
  if (!(has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu())) {
    return has_enable_cudnn_fused_normalization_add_relu() < other.has_enable_cudnn_fused_normalization_add_relu() ? -1 : 1;
  } else if (!(enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu())) {
    return enable_cudnn_fused_normalization_add_relu() < other.enable_cudnn_fused_normalization_add_relu() ? -1 : 1;
  }
  if (!(has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half())) {
    return has_cudnn_conv_enable_pseudo_half() < other.has_cudnn_conv_enable_pseudo_half() ? -1 : 1;
  } else if (!(cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half())) {
    return cudnn_conv_enable_pseudo_half() < other.cudnn_conv_enable_pseudo_half() ? -1 : 1;
  }
  return 0;
}

bool ConstCudnnConfig::_CudnnConfig_::operator==(const _CudnnConfig_& other) const {
  return true
    && has_enable_cudnn() == other.has_enable_cudnn() 
    && enable_cudnn() == other.enable_cudnn()
    && has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte() 
    && cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte()
    && has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo() 
    && cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo()
    && has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo() 
    && cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo()
    && has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo() 
    && cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo()
    && has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo() 
    && cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo()
    && has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only() 
    && cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only()
    && has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu() 
    && enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu()
    && has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half() 
    && cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half()
  ;
}

std::size_t ConstCudnnConfig::_CudnnConfig_::__CalcHash__() const {
  return 0
    ^ (has_enable_cudnn() ? std::hash<bool>()(enable_cudnn()) : 0)
    ^ (has_cudnn_buf_limit_mbyte() ? std::hash<int64_t>()(cudnn_buf_limit_mbyte()) : 0)
    ^ (has_cudnn_conv_force_fwd_algo() ? std::hash<int32_t>()(cudnn_conv_force_fwd_algo()) : 0)
    ^ (has_cudnn_conv_force_bwd_data_algo() ? std::hash<int32_t>()(cudnn_conv_force_bwd_data_algo()) : 0)
    ^ (has_cudnn_conv_force_bwd_filter_algo() ? std::hash<int32_t>()(cudnn_conv_force_bwd_filter_algo()) : 0)
    ^ (has_cudnn_conv_heuristic_search_algo() ? std::hash<bool>()(cudnn_conv_heuristic_search_algo()) : 0)
    ^ (has_cudnn_conv_use_deterministic_algo_only() ? std::hash<bool>()(cudnn_conv_use_deterministic_algo_only()) : 0)
    ^ (has_enable_cudnn_fused_normalization_add_relu() ? std::hash<bool>()(enable_cudnn_fused_normalization_add_relu()) : 0)
    ^ (has_cudnn_conv_enable_pseudo_half() ? std::hash<bool>()(cudnn_conv_enable_pseudo_half()) : 0)
  ;
}

bool ConstCudnnConfig::_CudnnConfig_::operator<(const _CudnnConfig_& other) const {
  return false
    || !(has_enable_cudnn() == other.has_enable_cudnn()) ? 
      has_enable_cudnn() < other.has_enable_cudnn() : false
    || !(enable_cudnn() == other.enable_cudnn()) ? 
      enable_cudnn() < other.enable_cudnn() : false
    || !(has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte()) ? 
      has_cudnn_buf_limit_mbyte() < other.has_cudnn_buf_limit_mbyte() : false
    || !(cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte()) ? 
      cudnn_buf_limit_mbyte() < other.cudnn_buf_limit_mbyte() : false
    || !(has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo()) ? 
      has_cudnn_conv_force_fwd_algo() < other.has_cudnn_conv_force_fwd_algo() : false
    || !(cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo()) ? 
      cudnn_conv_force_fwd_algo() < other.cudnn_conv_force_fwd_algo() : false
    || !(has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo()) ? 
      has_cudnn_conv_force_bwd_data_algo() < other.has_cudnn_conv_force_bwd_data_algo() : false
    || !(cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo()) ? 
      cudnn_conv_force_bwd_data_algo() < other.cudnn_conv_force_bwd_data_algo() : false
    || !(has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo()) ? 
      has_cudnn_conv_force_bwd_filter_algo() < other.has_cudnn_conv_force_bwd_filter_algo() : false
    || !(cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo()) ? 
      cudnn_conv_force_bwd_filter_algo() < other.cudnn_conv_force_bwd_filter_algo() : false
    || !(has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo()) ? 
      has_cudnn_conv_heuristic_search_algo() < other.has_cudnn_conv_heuristic_search_algo() : false
    || !(cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo()) ? 
      cudnn_conv_heuristic_search_algo() < other.cudnn_conv_heuristic_search_algo() : false
    || !(has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only()) ? 
      has_cudnn_conv_use_deterministic_algo_only() < other.has_cudnn_conv_use_deterministic_algo_only() : false
    || !(cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only()) ? 
      cudnn_conv_use_deterministic_algo_only() < other.cudnn_conv_use_deterministic_algo_only() : false
    || !(has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu()) ? 
      has_enable_cudnn_fused_normalization_add_relu() < other.has_enable_cudnn_fused_normalization_add_relu() : false
    || !(enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu()) ? 
      enable_cudnn_fused_normalization_add_relu() < other.enable_cudnn_fused_normalization_add_relu() : false
    || !(has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half()) ? 
      has_cudnn_conv_enable_pseudo_half() < other.has_cudnn_conv_enable_pseudo_half() : false
    || !(cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half()) ? 
      cudnn_conv_enable_pseudo_half() < other.cudnn_conv_enable_pseudo_half() : false
  ;
}

using _CudnnConfig_ =  ConstCudnnConfig::_CudnnConfig_;
ConstCudnnConfig::ConstCudnnConfig(const ::std::shared_ptr<_CudnnConfig_>& data): data_(data) {}
ConstCudnnConfig::ConstCudnnConfig(): data_(::std::make_shared<_CudnnConfig_>()) {}
ConstCudnnConfig::ConstCudnnConfig(const ::oneflow::CudnnConfig& proto_cudnnconfig) {
  BuildFromProto(proto_cudnnconfig);
}
ConstCudnnConfig::ConstCudnnConfig(const ConstCudnnConfig&) = default;
ConstCudnnConfig::ConstCudnnConfig(ConstCudnnConfig&&) noexcept = default;
ConstCudnnConfig::~ConstCudnnConfig() = default;

void ConstCudnnConfig::ToProto(PbMessage* proto_cudnnconfig) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::CudnnConfig*>(proto_cudnnconfig));
}
  
::std::string ConstCudnnConfig::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCudnnConfig::__Empty__() const {
  return !data_;
}

int ConstCudnnConfig::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"enable_cudnn", 1},
    {"cudnn_buf_limit_mbyte", 2},
    {"cudnn_conv_force_fwd_algo", 3},
    {"cudnn_conv_force_bwd_data_algo", 4},
    {"cudnn_conv_force_bwd_filter_algo", 5},
    {"cudnn_conv_heuristic_search_algo", 6},
    {"cudnn_conv_use_deterministic_algo_only", 7},
    {"enable_cudnn_fused_normalization_add_relu", 8},
    {"cudnn_conv_enable_pseudo_half", 9},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstCudnnConfig::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstCudnnConfig::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 9: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCudnnConfig::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &enable_cudnn();
    case 2: return &cudnn_buf_limit_mbyte();
    case 3: return &cudnn_conv_force_fwd_algo();
    case 4: return &cudnn_conv_force_bwd_data_algo();
    case 5: return &cudnn_conv_force_bwd_filter_algo();
    case 6: return &cudnn_conv_heuristic_search_algo();
    case 7: return &cudnn_conv_use_deterministic_algo_only();
    case 8: return &enable_cudnn_fused_normalization_add_relu();
    case 9: return &cudnn_conv_enable_pseudo_half();
    default: return nullptr;
  }
}

// required or optional field enable_cudnn
bool ConstCudnnConfig::has_enable_cudnn() const {
  return __SharedPtrOrDefault__()->has_enable_cudnn();
}
const bool& ConstCudnnConfig::enable_cudnn() const {
  return __SharedPtrOrDefault__()->enable_cudnn();
}
// used by pybind11 only
// required or optional field cudnn_buf_limit_mbyte
bool ConstCudnnConfig::has_cudnn_buf_limit_mbyte() const {
  return __SharedPtrOrDefault__()->has_cudnn_buf_limit_mbyte();
}
const int64_t& ConstCudnnConfig::cudnn_buf_limit_mbyte() const {
  return __SharedPtrOrDefault__()->cudnn_buf_limit_mbyte();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_fwd_algo
bool ConstCudnnConfig::has_cudnn_conv_force_fwd_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_fwd_algo();
}
const int32_t& ConstCudnnConfig::cudnn_conv_force_fwd_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_fwd_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_bwd_data_algo
bool ConstCudnnConfig::has_cudnn_conv_force_bwd_data_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_bwd_data_algo();
}
const int32_t& ConstCudnnConfig::cudnn_conv_force_bwd_data_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_bwd_data_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_bwd_filter_algo
bool ConstCudnnConfig::has_cudnn_conv_force_bwd_filter_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_bwd_filter_algo();
}
const int32_t& ConstCudnnConfig::cudnn_conv_force_bwd_filter_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_bwd_filter_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_heuristic_search_algo
bool ConstCudnnConfig::has_cudnn_conv_heuristic_search_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_heuristic_search_algo();
}
const bool& ConstCudnnConfig::cudnn_conv_heuristic_search_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_heuristic_search_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_use_deterministic_algo_only
bool ConstCudnnConfig::has_cudnn_conv_use_deterministic_algo_only() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_use_deterministic_algo_only();
}
const bool& ConstCudnnConfig::cudnn_conv_use_deterministic_algo_only() const {
  return __SharedPtrOrDefault__()->cudnn_conv_use_deterministic_algo_only();
}
// used by pybind11 only
// required or optional field enable_cudnn_fused_normalization_add_relu
bool ConstCudnnConfig::has_enable_cudnn_fused_normalization_add_relu() const {
  return __SharedPtrOrDefault__()->has_enable_cudnn_fused_normalization_add_relu();
}
const bool& ConstCudnnConfig::enable_cudnn_fused_normalization_add_relu() const {
  return __SharedPtrOrDefault__()->enable_cudnn_fused_normalization_add_relu();
}
// used by pybind11 only
// required or optional field cudnn_conv_enable_pseudo_half
bool ConstCudnnConfig::has_cudnn_conv_enable_pseudo_half() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_enable_pseudo_half();
}
const bool& ConstCudnnConfig::cudnn_conv_enable_pseudo_half() const {
  return __SharedPtrOrDefault__()->cudnn_conv_enable_pseudo_half();
}
// used by pybind11 only

::std::shared_ptr<ConstCudnnConfig> ConstCudnnConfig::__SharedConst__() const {
  return ::std::make_shared<ConstCudnnConfig>(data_);
}
int64_t ConstCudnnConfig::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCudnnConfig::operator==(const ConstCudnnConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCudnnConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCudnnConfig::operator<(const ConstCudnnConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CudnnConfig_>& ConstCudnnConfig::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CudnnConfig_> default_ptr = std::make_shared<_CudnnConfig_>();
  return default_ptr;
}
const ::std::shared_ptr<_CudnnConfig_>& ConstCudnnConfig::__SharedPtr__() {
  if (!data_) { data_.reset(new _CudnnConfig_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCudnnConfig
void ConstCudnnConfig::BuildFromProto(const PbMessage& proto_cudnnconfig) {
  data_ = ::std::make_shared<_CudnnConfig_>(dynamic_cast<const ::oneflow::CudnnConfig&>(proto_cudnnconfig));
}

CudnnConfig::CudnnConfig(const ::std::shared_ptr<_CudnnConfig_>& data)
  : ConstCudnnConfig(data) {}
CudnnConfig::CudnnConfig(const CudnnConfig& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CudnnConfig> resize
CudnnConfig::CudnnConfig(CudnnConfig&&) noexcept = default; 
CudnnConfig::CudnnConfig(const ::oneflow::CudnnConfig& proto_cudnnconfig) {
  InitFromProto(proto_cudnnconfig);
}
CudnnConfig::CudnnConfig() = default;

CudnnConfig::~CudnnConfig() = default;

void CudnnConfig::InitFromProto(const PbMessage& proto_cudnnconfig) {
  BuildFromProto(proto_cudnnconfig);
}
  
void* CudnnConfig::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_enable_cudnn();
    case 2: return mutable_cudnn_buf_limit_mbyte();
    case 3: return mutable_cudnn_conv_force_fwd_algo();
    case 4: return mutable_cudnn_conv_force_bwd_data_algo();
    case 5: return mutable_cudnn_conv_force_bwd_filter_algo();
    case 6: return mutable_cudnn_conv_heuristic_search_algo();
    case 7: return mutable_cudnn_conv_use_deterministic_algo_only();
    case 8: return mutable_enable_cudnn_fused_normalization_add_relu();
    case 9: return mutable_cudnn_conv_enable_pseudo_half();
    default: return nullptr;
  }
}

bool CudnnConfig::operator==(const CudnnConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CudnnConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CudnnConfig::operator<(const CudnnConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CudnnConfig::Clear() {
  if (data_) { data_.reset(); }
}
void CudnnConfig::CopyFrom(const CudnnConfig& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CudnnConfig& CudnnConfig::operator=(const CudnnConfig& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field enable_cudnn
void CudnnConfig::clear_enable_cudnn() {
  return __SharedPtr__()->clear_enable_cudnn();
}
void CudnnConfig::set_enable_cudnn(const bool& value) {
  return __SharedPtr__()->set_enable_cudnn(value);
}
bool* CudnnConfig::mutable_enable_cudnn() {
  return  __SharedPtr__()->mutable_enable_cudnn();
}
// required or optional field cudnn_buf_limit_mbyte
void CudnnConfig::clear_cudnn_buf_limit_mbyte() {
  return __SharedPtr__()->clear_cudnn_buf_limit_mbyte();
}
void CudnnConfig::set_cudnn_buf_limit_mbyte(const int64_t& value) {
  return __SharedPtr__()->set_cudnn_buf_limit_mbyte(value);
}
int64_t* CudnnConfig::mutable_cudnn_buf_limit_mbyte() {
  return  __SharedPtr__()->mutable_cudnn_buf_limit_mbyte();
}
// required or optional field cudnn_conv_force_fwd_algo
void CudnnConfig::clear_cudnn_conv_force_fwd_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_fwd_algo();
}
void CudnnConfig::set_cudnn_conv_force_fwd_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_fwd_algo(value);
}
int32_t* CudnnConfig::mutable_cudnn_conv_force_fwd_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_fwd_algo();
}
// required or optional field cudnn_conv_force_bwd_data_algo
void CudnnConfig::clear_cudnn_conv_force_bwd_data_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_bwd_data_algo();
}
void CudnnConfig::set_cudnn_conv_force_bwd_data_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_bwd_data_algo(value);
}
int32_t* CudnnConfig::mutable_cudnn_conv_force_bwd_data_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_bwd_data_algo();
}
// required or optional field cudnn_conv_force_bwd_filter_algo
void CudnnConfig::clear_cudnn_conv_force_bwd_filter_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_bwd_filter_algo();
}
void CudnnConfig::set_cudnn_conv_force_bwd_filter_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_bwd_filter_algo(value);
}
int32_t* CudnnConfig::mutable_cudnn_conv_force_bwd_filter_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_bwd_filter_algo();
}
// required or optional field cudnn_conv_heuristic_search_algo
void CudnnConfig::clear_cudnn_conv_heuristic_search_algo() {
  return __SharedPtr__()->clear_cudnn_conv_heuristic_search_algo();
}
void CudnnConfig::set_cudnn_conv_heuristic_search_algo(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_heuristic_search_algo(value);
}
bool* CudnnConfig::mutable_cudnn_conv_heuristic_search_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_heuristic_search_algo();
}
// required or optional field cudnn_conv_use_deterministic_algo_only
void CudnnConfig::clear_cudnn_conv_use_deterministic_algo_only() {
  return __SharedPtr__()->clear_cudnn_conv_use_deterministic_algo_only();
}
void CudnnConfig::set_cudnn_conv_use_deterministic_algo_only(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_use_deterministic_algo_only(value);
}
bool* CudnnConfig::mutable_cudnn_conv_use_deterministic_algo_only() {
  return  __SharedPtr__()->mutable_cudnn_conv_use_deterministic_algo_only();
}
// required or optional field enable_cudnn_fused_normalization_add_relu
void CudnnConfig::clear_enable_cudnn_fused_normalization_add_relu() {
  return __SharedPtr__()->clear_enable_cudnn_fused_normalization_add_relu();
}
void CudnnConfig::set_enable_cudnn_fused_normalization_add_relu(const bool& value) {
  return __SharedPtr__()->set_enable_cudnn_fused_normalization_add_relu(value);
}
bool* CudnnConfig::mutable_enable_cudnn_fused_normalization_add_relu() {
  return  __SharedPtr__()->mutable_enable_cudnn_fused_normalization_add_relu();
}
// required or optional field cudnn_conv_enable_pseudo_half
void CudnnConfig::clear_cudnn_conv_enable_pseudo_half() {
  return __SharedPtr__()->clear_cudnn_conv_enable_pseudo_half();
}
void CudnnConfig::set_cudnn_conv_enable_pseudo_half(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_enable_pseudo_half(value);
}
bool* CudnnConfig::mutable_cudnn_conv_enable_pseudo_half() {
  return  __SharedPtr__()->mutable_cudnn_conv_enable_pseudo_half();
}

::std::shared_ptr<CudnnConfig> CudnnConfig::__SharedMutable__() {
  return ::std::make_shared<CudnnConfig>(__SharedPtr__());
}
ConstResource::_Resource_::_Resource_() { Clear(); }
ConstResource::_Resource_::_Resource_(const _Resource_& other) { CopyFrom(other); }
ConstResource::_Resource_::_Resource_(const ::oneflow::Resource& proto_resource) {
  InitFromProto(proto_resource);
}
ConstResource::_Resource_::_Resource_(_Resource_&& other) = default;
ConstResource::_Resource_::~_Resource_() = default;

void ConstResource::_Resource_::InitFromProto(const ::oneflow::Resource& proto_resource) {
  Clear();
  // required_or_optional field: machine_num
  if (proto_resource.has_machine_num()) {
    set_machine_num(proto_resource.machine_num());
  }
  // required_or_optional field: gpu_device_num
  if (proto_resource.has_gpu_device_num()) {
    set_gpu_device_num(proto_resource.gpu_device_num());
  }
  // required_or_optional field: cpu_device_num
  if (proto_resource.has_cpu_device_num()) {
    set_cpu_device_num(proto_resource.cpu_device_num());
  }
  // required_or_optional field: comm_net_worker_num
  if (proto_resource.has_comm_net_worker_num()) {
    set_comm_net_worker_num(proto_resource.comm_net_worker_num());
  }
  // required_or_optional field: max_mdsave_worker_num
  if (proto_resource.has_max_mdsave_worker_num()) {
    set_max_mdsave_worker_num(proto_resource.max_mdsave_worker_num());
  }
  // required_or_optional field: reserved_host_mem_mbyte
  if (proto_resource.has_reserved_host_mem_mbyte()) {
    set_reserved_host_mem_mbyte(proto_resource.reserved_host_mem_mbyte());
  }
  // required_or_optional field: reserved_device_mem_mbyte
  if (proto_resource.has_reserved_device_mem_mbyte()) {
    set_reserved_device_mem_mbyte(proto_resource.reserved_device_mem_mbyte());
  }
  // required_or_optional field: compute_thread_pool_size
  if (proto_resource.has_compute_thread_pool_size()) {
    set_compute_thread_pool_size(proto_resource.compute_thread_pool_size());
  }
  // required_or_optional field: enable_thread_local_cache
  if (proto_resource.has_enable_thread_local_cache()) {
    set_enable_thread_local_cache(proto_resource.enable_thread_local_cache());
  }
  // required_or_optional field: thread_local_cache_max_size
  if (proto_resource.has_thread_local_cache_max_size()) {
    set_thread_local_cache_max_size(proto_resource.thread_local_cache_max_size());
  }
  // required_or_optional field: enable_debug_mode
  if (proto_resource.has_enable_debug_mode()) {
    set_enable_debug_mode(proto_resource.enable_debug_mode());
  }
  // required_or_optional field: enable_tensor_float_32_compute
  if (proto_resource.has_enable_tensor_float_32_compute()) {
    set_enable_tensor_float_32_compute(proto_resource.enable_tensor_float_32_compute());
  }
  // required_or_optional field: enable_mem_chain_merge
  if (proto_resource.has_enable_mem_chain_merge()) {
    set_enable_mem_chain_merge(proto_resource.enable_mem_chain_merge());
  }
  // required_or_optional field: collective_boxing_conf
  if (proto_resource.has_collective_boxing_conf()) {
  *mutable_collective_boxing_conf() = ::oneflow::cfg::CollectiveBoxingConf(proto_resource.collective_boxing_conf());      
  }
  // required_or_optional field: nccl_use_compute_stream
  if (proto_resource.has_nccl_use_compute_stream()) {
    set_nccl_use_compute_stream(proto_resource.nccl_use_compute_stream());
  }
  // required_or_optional field: disable_group_boxing_by_dst_parallel
  if (proto_resource.has_disable_group_boxing_by_dst_parallel()) {
    set_disable_group_boxing_by_dst_parallel(proto_resource.disable_group_boxing_by_dst_parallel());
  }
  // required_or_optional field: cudnn_conf
  if (proto_resource.has_cudnn_conf()) {
  *mutable_cudnn_conf() = ::oneflow::cfg::CudnnConfig(proto_resource.cudnn_conf());      
  }
  // required_or_optional field: enable_model_io_v2
  if (proto_resource.has_enable_model_io_v2()) {
    set_enable_model_io_v2(proto_resource.enable_model_io_v2());
  }
  // required_or_optional field: enable_legacy_model_io
  if (proto_resource.has_enable_legacy_model_io()) {
    set_enable_legacy_model_io(proto_resource.enable_legacy_model_io());
  }
    
}

void ConstResource::_Resource_::ToProto(::oneflow::Resource* proto_resource) const {
  proto_resource->Clear();
  // required_or_optional field: machine_num
  if (this->has_machine_num()) {
    proto_resource->set_machine_num(machine_num());
    }
  // required_or_optional field: gpu_device_num
  if (this->has_gpu_device_num()) {
    proto_resource->set_gpu_device_num(gpu_device_num());
    }
  // required_or_optional field: cpu_device_num
  if (this->has_cpu_device_num()) {
    proto_resource->set_cpu_device_num(cpu_device_num());
    }
  // required_or_optional field: comm_net_worker_num
  if (this->has_comm_net_worker_num()) {
    proto_resource->set_comm_net_worker_num(comm_net_worker_num());
    }
  // required_or_optional field: max_mdsave_worker_num
  if (this->has_max_mdsave_worker_num()) {
    proto_resource->set_max_mdsave_worker_num(max_mdsave_worker_num());
    }
  // required_or_optional field: reserved_host_mem_mbyte
  if (this->has_reserved_host_mem_mbyte()) {
    proto_resource->set_reserved_host_mem_mbyte(reserved_host_mem_mbyte());
    }
  // required_or_optional field: reserved_device_mem_mbyte
  if (this->has_reserved_device_mem_mbyte()) {
    proto_resource->set_reserved_device_mem_mbyte(reserved_device_mem_mbyte());
    }
  // required_or_optional field: compute_thread_pool_size
  if (this->has_compute_thread_pool_size()) {
    proto_resource->set_compute_thread_pool_size(compute_thread_pool_size());
    }
  // required_or_optional field: enable_thread_local_cache
  if (this->has_enable_thread_local_cache()) {
    proto_resource->set_enable_thread_local_cache(enable_thread_local_cache());
    }
  // required_or_optional field: thread_local_cache_max_size
  if (this->has_thread_local_cache_max_size()) {
    proto_resource->set_thread_local_cache_max_size(thread_local_cache_max_size());
    }
  // required_or_optional field: enable_debug_mode
  if (this->has_enable_debug_mode()) {
    proto_resource->set_enable_debug_mode(enable_debug_mode());
    }
  // required_or_optional field: enable_tensor_float_32_compute
  if (this->has_enable_tensor_float_32_compute()) {
    proto_resource->set_enable_tensor_float_32_compute(enable_tensor_float_32_compute());
    }
  // required_or_optional field: enable_mem_chain_merge
  if (this->has_enable_mem_chain_merge()) {
    proto_resource->set_enable_mem_chain_merge(enable_mem_chain_merge());
    }
  // required_or_optional field: collective_boxing_conf
  if (this->has_collective_boxing_conf()) {
    ::oneflow::CollectiveBoxingConf proto_collective_boxing_conf;
    collective_boxing_conf().ToProto(&proto_collective_boxing_conf);
    proto_resource->mutable_collective_boxing_conf()->CopyFrom(proto_collective_boxing_conf);
    }
  // required_or_optional field: nccl_use_compute_stream
  if (this->has_nccl_use_compute_stream()) {
    proto_resource->set_nccl_use_compute_stream(nccl_use_compute_stream());
    }
  // required_or_optional field: disable_group_boxing_by_dst_parallel
  if (this->has_disable_group_boxing_by_dst_parallel()) {
    proto_resource->set_disable_group_boxing_by_dst_parallel(disable_group_boxing_by_dst_parallel());
    }
  // required_or_optional field: cudnn_conf
  if (this->has_cudnn_conf()) {
    ::oneflow::CudnnConfig proto_cudnn_conf;
    cudnn_conf().ToProto(&proto_cudnn_conf);
    proto_resource->mutable_cudnn_conf()->CopyFrom(proto_cudnn_conf);
    }
  // required_or_optional field: enable_model_io_v2
  if (this->has_enable_model_io_v2()) {
    proto_resource->set_enable_model_io_v2(enable_model_io_v2());
    }
  // required_or_optional field: enable_legacy_model_io
  if (this->has_enable_legacy_model_io()) {
    proto_resource->set_enable_legacy_model_io(enable_legacy_model_io());
    }

}

::std::string ConstResource::_Resource_::DebugString() const {
  ::oneflow::Resource proto_resource;
  this->ToProto(&proto_resource);
  return proto_resource.DebugString();
}

void ConstResource::_Resource_::Clear() {
  clear_machine_num();
  clear_gpu_device_num();
  clear_cpu_device_num();
  clear_comm_net_worker_num();
  clear_max_mdsave_worker_num();
  clear_reserved_host_mem_mbyte();
  clear_reserved_device_mem_mbyte();
  clear_compute_thread_pool_size();
  clear_enable_thread_local_cache();
  clear_thread_local_cache_max_size();
  clear_enable_debug_mode();
  clear_enable_tensor_float_32_compute();
  clear_enable_mem_chain_merge();
  clear_collective_boxing_conf();
  clear_nccl_use_compute_stream();
  clear_disable_group_boxing_by_dst_parallel();
  clear_cudnn_conf();
  clear_enable_model_io_v2();
  clear_enable_legacy_model_io();
}

void ConstResource::_Resource_::CopyFrom(const _Resource_& other) {
  if (other.has_machine_num()) {
    set_machine_num(other.machine_num());
  } else {
    clear_machine_num();
  }
  if (other.has_gpu_device_num()) {
    set_gpu_device_num(other.gpu_device_num());
  } else {
    clear_gpu_device_num();
  }
  if (other.has_cpu_device_num()) {
    set_cpu_device_num(other.cpu_device_num());
  } else {
    clear_cpu_device_num();
  }
  if (other.has_comm_net_worker_num()) {
    set_comm_net_worker_num(other.comm_net_worker_num());
  } else {
    clear_comm_net_worker_num();
  }
  if (other.has_max_mdsave_worker_num()) {
    set_max_mdsave_worker_num(other.max_mdsave_worker_num());
  } else {
    clear_max_mdsave_worker_num();
  }
  if (other.has_reserved_host_mem_mbyte()) {
    set_reserved_host_mem_mbyte(other.reserved_host_mem_mbyte());
  } else {
    clear_reserved_host_mem_mbyte();
  }
  if (other.has_reserved_device_mem_mbyte()) {
    set_reserved_device_mem_mbyte(other.reserved_device_mem_mbyte());
  } else {
    clear_reserved_device_mem_mbyte();
  }
  if (other.has_compute_thread_pool_size()) {
    set_compute_thread_pool_size(other.compute_thread_pool_size());
  } else {
    clear_compute_thread_pool_size();
  }
  if (other.has_enable_thread_local_cache()) {
    set_enable_thread_local_cache(other.enable_thread_local_cache());
  } else {
    clear_enable_thread_local_cache();
  }
  if (other.has_thread_local_cache_max_size()) {
    set_thread_local_cache_max_size(other.thread_local_cache_max_size());
  } else {
    clear_thread_local_cache_max_size();
  }
  if (other.has_enable_debug_mode()) {
    set_enable_debug_mode(other.enable_debug_mode());
  } else {
    clear_enable_debug_mode();
  }
  if (other.has_enable_tensor_float_32_compute()) {
    set_enable_tensor_float_32_compute(other.enable_tensor_float_32_compute());
  } else {
    clear_enable_tensor_float_32_compute();
  }
  if (other.has_enable_mem_chain_merge()) {
    set_enable_mem_chain_merge(other.enable_mem_chain_merge());
  } else {
    clear_enable_mem_chain_merge();
  }
  if (other.has_collective_boxing_conf()) {
    mutable_collective_boxing_conf()->CopyFrom(other.collective_boxing_conf());
  } else {
    clear_collective_boxing_conf();
  }
  if (other.has_nccl_use_compute_stream()) {
    set_nccl_use_compute_stream(other.nccl_use_compute_stream());
  } else {
    clear_nccl_use_compute_stream();
  }
  if (other.has_disable_group_boxing_by_dst_parallel()) {
    set_disable_group_boxing_by_dst_parallel(other.disable_group_boxing_by_dst_parallel());
  } else {
    clear_disable_group_boxing_by_dst_parallel();
  }
  if (other.has_cudnn_conf()) {
    mutable_cudnn_conf()->CopyFrom(other.cudnn_conf());
  } else {
    clear_cudnn_conf();
  }
  if (other.has_enable_model_io_v2()) {
    set_enable_model_io_v2(other.enable_model_io_v2());
  } else {
    clear_enable_model_io_v2();
  }
  if (other.has_enable_legacy_model_io()) {
    set_enable_legacy_model_io(other.enable_legacy_model_io());
  } else {
    clear_enable_legacy_model_io();
  }
}


// optional field machine_num
bool ConstResource::_Resource_::has_machine_num() const {
  return has_machine_num_;
}
const int32_t& ConstResource::_Resource_::machine_num() const {
  if (has_machine_num_) { return machine_num_; }
  static const int32_t default_static_value =
    int32_t(0);
  return default_static_value;
}
void ConstResource::_Resource_::clear_machine_num() {
  has_machine_num_ = false;
}
void ConstResource::_Resource_::set_machine_num(const int32_t& value) {
  machine_num_ = value;
  has_machine_num_ = true;
}
int32_t* ConstResource::_Resource_::mutable_machine_num() {
  has_machine_num_ = true;
  return &machine_num_;
}

// optional field gpu_device_num
bool ConstResource::_Resource_::has_gpu_device_num() const {
  return has_gpu_device_num_;
}
const int32_t& ConstResource::_Resource_::gpu_device_num() const {
  if (has_gpu_device_num_) { return gpu_device_num_; }
  static const int32_t default_static_value =
    int32_t(0);
  return default_static_value;
}
void ConstResource::_Resource_::clear_gpu_device_num() {
  has_gpu_device_num_ = false;
}
void ConstResource::_Resource_::set_gpu_device_num(const int32_t& value) {
  gpu_device_num_ = value;
  has_gpu_device_num_ = true;
}
int32_t* ConstResource::_Resource_::mutable_gpu_device_num() {
  has_gpu_device_num_ = true;
  return &gpu_device_num_;
}

// optional field cpu_device_num
bool ConstResource::_Resource_::has_cpu_device_num() const {
  return has_cpu_device_num_;
}
const int32_t& ConstResource::_Resource_::cpu_device_num() const {
  if (has_cpu_device_num_) { return cpu_device_num_; }
  static const int32_t default_static_value =
    int32_t(0);
  return default_static_value;
}
void ConstResource::_Resource_::clear_cpu_device_num() {
  has_cpu_device_num_ = false;
}
void ConstResource::_Resource_::set_cpu_device_num(const int32_t& value) {
  cpu_device_num_ = value;
  has_cpu_device_num_ = true;
}
int32_t* ConstResource::_Resource_::mutable_cpu_device_num() {
  has_cpu_device_num_ = true;
  return &cpu_device_num_;
}

// optional field comm_net_worker_num
bool ConstResource::_Resource_::has_comm_net_worker_num() const {
  return has_comm_net_worker_num_;
}
const int32_t& ConstResource::_Resource_::comm_net_worker_num() const {
  if (has_comm_net_worker_num_) { return comm_net_worker_num_; }
  static const int32_t default_static_value =
    int32_t(4);
  return default_static_value;
}
void ConstResource::_Resource_::clear_comm_net_worker_num() {
  has_comm_net_worker_num_ = false;
}
void ConstResource::_Resource_::set_comm_net_worker_num(const int32_t& value) {
  comm_net_worker_num_ = value;
  has_comm_net_worker_num_ = true;
}
int32_t* ConstResource::_Resource_::mutable_comm_net_worker_num() {
  has_comm_net_worker_num_ = true;
  return &comm_net_worker_num_;
}

// optional field max_mdsave_worker_num
bool ConstResource::_Resource_::has_max_mdsave_worker_num() const {
  return has_max_mdsave_worker_num_;
}
const int32_t& ConstResource::_Resource_::max_mdsave_worker_num() const {
  if (has_max_mdsave_worker_num_) { return max_mdsave_worker_num_; }
  static const int32_t default_static_value =
    int32_t(64);
  return default_static_value;
}
void ConstResource::_Resource_::clear_max_mdsave_worker_num() {
  has_max_mdsave_worker_num_ = false;
}
void ConstResource::_Resource_::set_max_mdsave_worker_num(const int32_t& value) {
  max_mdsave_worker_num_ = value;
  has_max_mdsave_worker_num_ = true;
}
int32_t* ConstResource::_Resource_::mutable_max_mdsave_worker_num() {
  has_max_mdsave_worker_num_ = true;
  return &max_mdsave_worker_num_;
}

// optional field reserved_host_mem_mbyte
bool ConstResource::_Resource_::has_reserved_host_mem_mbyte() const {
  return has_reserved_host_mem_mbyte_;
}
const uint64_t& ConstResource::_Resource_::reserved_host_mem_mbyte() const {
  if (has_reserved_host_mem_mbyte_) { return reserved_host_mem_mbyte_; }
  static const uint64_t default_static_value =
    uint64_t(500);
  return default_static_value;
}
void ConstResource::_Resource_::clear_reserved_host_mem_mbyte() {
  has_reserved_host_mem_mbyte_ = false;
}
void ConstResource::_Resource_::set_reserved_host_mem_mbyte(const uint64_t& value) {
  reserved_host_mem_mbyte_ = value;
  has_reserved_host_mem_mbyte_ = true;
}
uint64_t* ConstResource::_Resource_::mutable_reserved_host_mem_mbyte() {
  has_reserved_host_mem_mbyte_ = true;
  return &reserved_host_mem_mbyte_;
}

// optional field reserved_device_mem_mbyte
bool ConstResource::_Resource_::has_reserved_device_mem_mbyte() const {
  return has_reserved_device_mem_mbyte_;
}
const uint64_t& ConstResource::_Resource_::reserved_device_mem_mbyte() const {
  if (has_reserved_device_mem_mbyte_) { return reserved_device_mem_mbyte_; }
  static const uint64_t default_static_value =
    uint64_t(500);
  return default_static_value;
}
void ConstResource::_Resource_::clear_reserved_device_mem_mbyte() {
  has_reserved_device_mem_mbyte_ = false;
}
void ConstResource::_Resource_::set_reserved_device_mem_mbyte(const uint64_t& value) {
  reserved_device_mem_mbyte_ = value;
  has_reserved_device_mem_mbyte_ = true;
}
uint64_t* ConstResource::_Resource_::mutable_reserved_device_mem_mbyte() {
  has_reserved_device_mem_mbyte_ = true;
  return &reserved_device_mem_mbyte_;
}

// optional field compute_thread_pool_size
bool ConstResource::_Resource_::has_compute_thread_pool_size() const {
  return has_compute_thread_pool_size_;
}
const int32_t& ConstResource::_Resource_::compute_thread_pool_size() const {
  if (has_compute_thread_pool_size_) { return compute_thread_pool_size_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstResource::_Resource_::clear_compute_thread_pool_size() {
  has_compute_thread_pool_size_ = false;
}
void ConstResource::_Resource_::set_compute_thread_pool_size(const int32_t& value) {
  compute_thread_pool_size_ = value;
  has_compute_thread_pool_size_ = true;
}
int32_t* ConstResource::_Resource_::mutable_compute_thread_pool_size() {
  has_compute_thread_pool_size_ = true;
  return &compute_thread_pool_size_;
}

// optional field enable_thread_local_cache
bool ConstResource::_Resource_::has_enable_thread_local_cache() const {
  return has_enable_thread_local_cache_;
}
const bool& ConstResource::_Resource_::enable_thread_local_cache() const {
  if (has_enable_thread_local_cache_) { return enable_thread_local_cache_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_thread_local_cache() {
  has_enable_thread_local_cache_ = false;
}
void ConstResource::_Resource_::set_enable_thread_local_cache(const bool& value) {
  enable_thread_local_cache_ = value;
  has_enable_thread_local_cache_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_thread_local_cache() {
  has_enable_thread_local_cache_ = true;
  return &enable_thread_local_cache_;
}

// optional field thread_local_cache_max_size
bool ConstResource::_Resource_::has_thread_local_cache_max_size() const {
  return has_thread_local_cache_max_size_;
}
const int64_t& ConstResource::_Resource_::thread_local_cache_max_size() const {
  if (has_thread_local_cache_max_size_) { return thread_local_cache_max_size_; }
  static const int64_t default_static_value =
    int64_t(67108864);
  return default_static_value;
}
void ConstResource::_Resource_::clear_thread_local_cache_max_size() {
  has_thread_local_cache_max_size_ = false;
}
void ConstResource::_Resource_::set_thread_local_cache_max_size(const int64_t& value) {
  thread_local_cache_max_size_ = value;
  has_thread_local_cache_max_size_ = true;
}
int64_t* ConstResource::_Resource_::mutable_thread_local_cache_max_size() {
  has_thread_local_cache_max_size_ = true;
  return &thread_local_cache_max_size_;
}

// optional field enable_debug_mode
bool ConstResource::_Resource_::has_enable_debug_mode() const {
  return has_enable_debug_mode_;
}
const bool& ConstResource::_Resource_::enable_debug_mode() const {
  if (has_enable_debug_mode_) { return enable_debug_mode_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_debug_mode() {
  has_enable_debug_mode_ = false;
}
void ConstResource::_Resource_::set_enable_debug_mode(const bool& value) {
  enable_debug_mode_ = value;
  has_enable_debug_mode_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_debug_mode() {
  has_enable_debug_mode_ = true;
  return &enable_debug_mode_;
}

// optional field enable_tensor_float_32_compute
bool ConstResource::_Resource_::has_enable_tensor_float_32_compute() const {
  return has_enable_tensor_float_32_compute_;
}
const bool& ConstResource::_Resource_::enable_tensor_float_32_compute() const {
  if (has_enable_tensor_float_32_compute_) { return enable_tensor_float_32_compute_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_tensor_float_32_compute() {
  has_enable_tensor_float_32_compute_ = false;
}
void ConstResource::_Resource_::set_enable_tensor_float_32_compute(const bool& value) {
  enable_tensor_float_32_compute_ = value;
  has_enable_tensor_float_32_compute_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_tensor_float_32_compute() {
  has_enable_tensor_float_32_compute_ = true;
  return &enable_tensor_float_32_compute_;
}

// optional field enable_mem_chain_merge
bool ConstResource::_Resource_::has_enable_mem_chain_merge() const {
  return has_enable_mem_chain_merge_;
}
const bool& ConstResource::_Resource_::enable_mem_chain_merge() const {
  if (has_enable_mem_chain_merge_) { return enable_mem_chain_merge_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_mem_chain_merge() {
  has_enable_mem_chain_merge_ = false;
}
void ConstResource::_Resource_::set_enable_mem_chain_merge(const bool& value) {
  enable_mem_chain_merge_ = value;
  has_enable_mem_chain_merge_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_mem_chain_merge() {
  has_enable_mem_chain_merge_ = true;
  return &enable_mem_chain_merge_;
}

// optional field collective_boxing_conf
bool ConstResource::_Resource_::has_collective_boxing_conf() const {
  return has_collective_boxing_conf_;
}
const ::oneflow::cfg::CollectiveBoxingConf& ConstResource::_Resource_::collective_boxing_conf() const {
  if (!collective_boxing_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::CollectiveBoxingConf>();
    return *default_static_value;
  }
  return *(collective_boxing_conf_.get());
}
void ConstResource::_Resource_::clear_collective_boxing_conf() {
  if (collective_boxing_conf_) {
    collective_boxing_conf_->Clear();
  }
  has_collective_boxing_conf_ = false;
}
::oneflow::cfg::CollectiveBoxingConf* ConstResource::_Resource_::mutable_collective_boxing_conf() {
  if (!collective_boxing_conf_) {
    collective_boxing_conf_ = ::std::make_shared<::oneflow::cfg::CollectiveBoxingConf>();
  }
  has_collective_boxing_conf_ = true;
  return collective_boxing_conf_.get();
}

// optional field nccl_use_compute_stream
bool ConstResource::_Resource_::has_nccl_use_compute_stream() const {
  return has_nccl_use_compute_stream_;
}
const bool& ConstResource::_Resource_::nccl_use_compute_stream() const {
  if (has_nccl_use_compute_stream_) { return nccl_use_compute_stream_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstResource::_Resource_::clear_nccl_use_compute_stream() {
  has_nccl_use_compute_stream_ = false;
}
void ConstResource::_Resource_::set_nccl_use_compute_stream(const bool& value) {
  nccl_use_compute_stream_ = value;
  has_nccl_use_compute_stream_ = true;
}
bool* ConstResource::_Resource_::mutable_nccl_use_compute_stream() {
  has_nccl_use_compute_stream_ = true;
  return &nccl_use_compute_stream_;
}

// optional field disable_group_boxing_by_dst_parallel
bool ConstResource::_Resource_::has_disable_group_boxing_by_dst_parallel() const {
  return has_disable_group_boxing_by_dst_parallel_;
}
const bool& ConstResource::_Resource_::disable_group_boxing_by_dst_parallel() const {
  if (has_disable_group_boxing_by_dst_parallel_) { return disable_group_boxing_by_dst_parallel_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstResource::_Resource_::clear_disable_group_boxing_by_dst_parallel() {
  has_disable_group_boxing_by_dst_parallel_ = false;
}
void ConstResource::_Resource_::set_disable_group_boxing_by_dst_parallel(const bool& value) {
  disable_group_boxing_by_dst_parallel_ = value;
  has_disable_group_boxing_by_dst_parallel_ = true;
}
bool* ConstResource::_Resource_::mutable_disable_group_boxing_by_dst_parallel() {
  has_disable_group_boxing_by_dst_parallel_ = true;
  return &disable_group_boxing_by_dst_parallel_;
}

// optional field cudnn_conf
bool ConstResource::_Resource_::has_cudnn_conf() const {
  return has_cudnn_conf_;
}
const ::oneflow::cfg::CudnnConfig& ConstResource::_Resource_::cudnn_conf() const {
  if (!cudnn_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::CudnnConfig> default_static_value =
      ::std::make_shared<::oneflow::cfg::CudnnConfig>();
    return *default_static_value;
  }
  return *(cudnn_conf_.get());
}
void ConstResource::_Resource_::clear_cudnn_conf() {
  if (cudnn_conf_) {
    cudnn_conf_->Clear();
  }
  has_cudnn_conf_ = false;
}
::oneflow::cfg::CudnnConfig* ConstResource::_Resource_::mutable_cudnn_conf() {
  if (!cudnn_conf_) {
    cudnn_conf_ = ::std::make_shared<::oneflow::cfg::CudnnConfig>();
  }
  has_cudnn_conf_ = true;
  return cudnn_conf_.get();
}

// optional field enable_model_io_v2
bool ConstResource::_Resource_::has_enable_model_io_v2() const {
  return has_enable_model_io_v2_;
}
const bool& ConstResource::_Resource_::enable_model_io_v2() const {
  if (has_enable_model_io_v2_) { return enable_model_io_v2_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_model_io_v2() {
  has_enable_model_io_v2_ = false;
}
void ConstResource::_Resource_::set_enable_model_io_v2(const bool& value) {
  enable_model_io_v2_ = value;
  has_enable_model_io_v2_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_model_io_v2() {
  has_enable_model_io_v2_ = true;
  return &enable_model_io_v2_;
}

// optional field enable_legacy_model_io
bool ConstResource::_Resource_::has_enable_legacy_model_io() const {
  return has_enable_legacy_model_io_;
}
const bool& ConstResource::_Resource_::enable_legacy_model_io() const {
  if (has_enable_legacy_model_io_) { return enable_legacy_model_io_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstResource::_Resource_::clear_enable_legacy_model_io() {
  has_enable_legacy_model_io_ = false;
}
void ConstResource::_Resource_::set_enable_legacy_model_io(const bool& value) {
  enable_legacy_model_io_ = value;
  has_enable_legacy_model_io_ = true;
}
bool* ConstResource::_Resource_::mutable_enable_legacy_model_io() {
  has_enable_legacy_model_io_ = true;
  return &enable_legacy_model_io_;
}


int ConstResource::_Resource_::compare(const _Resource_& other) {
  if (!(has_machine_num() == other.has_machine_num())) {
    return has_machine_num() < other.has_machine_num() ? -1 : 1;
  } else if (!(machine_num() == other.machine_num())) {
    return machine_num() < other.machine_num() ? -1 : 1;
  }
  if (!(has_gpu_device_num() == other.has_gpu_device_num())) {
    return has_gpu_device_num() < other.has_gpu_device_num() ? -1 : 1;
  } else if (!(gpu_device_num() == other.gpu_device_num())) {
    return gpu_device_num() < other.gpu_device_num() ? -1 : 1;
  }
  if (!(has_cpu_device_num() == other.has_cpu_device_num())) {
    return has_cpu_device_num() < other.has_cpu_device_num() ? -1 : 1;
  } else if (!(cpu_device_num() == other.cpu_device_num())) {
    return cpu_device_num() < other.cpu_device_num() ? -1 : 1;
  }
  if (!(has_comm_net_worker_num() == other.has_comm_net_worker_num())) {
    return has_comm_net_worker_num() < other.has_comm_net_worker_num() ? -1 : 1;
  } else if (!(comm_net_worker_num() == other.comm_net_worker_num())) {
    return comm_net_worker_num() < other.comm_net_worker_num() ? -1 : 1;
  }
  if (!(has_max_mdsave_worker_num() == other.has_max_mdsave_worker_num())) {
    return has_max_mdsave_worker_num() < other.has_max_mdsave_worker_num() ? -1 : 1;
  } else if (!(max_mdsave_worker_num() == other.max_mdsave_worker_num())) {
    return max_mdsave_worker_num() < other.max_mdsave_worker_num() ? -1 : 1;
  }
  if (!(has_reserved_host_mem_mbyte() == other.has_reserved_host_mem_mbyte())) {
    return has_reserved_host_mem_mbyte() < other.has_reserved_host_mem_mbyte() ? -1 : 1;
  } else if (!(reserved_host_mem_mbyte() == other.reserved_host_mem_mbyte())) {
    return reserved_host_mem_mbyte() < other.reserved_host_mem_mbyte() ? -1 : 1;
  }
  if (!(has_reserved_device_mem_mbyte() == other.has_reserved_device_mem_mbyte())) {
    return has_reserved_device_mem_mbyte() < other.has_reserved_device_mem_mbyte() ? -1 : 1;
  } else if (!(reserved_device_mem_mbyte() == other.reserved_device_mem_mbyte())) {
    return reserved_device_mem_mbyte() < other.reserved_device_mem_mbyte() ? -1 : 1;
  }
  if (!(has_compute_thread_pool_size() == other.has_compute_thread_pool_size())) {
    return has_compute_thread_pool_size() < other.has_compute_thread_pool_size() ? -1 : 1;
  } else if (!(compute_thread_pool_size() == other.compute_thread_pool_size())) {
    return compute_thread_pool_size() < other.compute_thread_pool_size() ? -1 : 1;
  }
  if (!(has_enable_thread_local_cache() == other.has_enable_thread_local_cache())) {
    return has_enable_thread_local_cache() < other.has_enable_thread_local_cache() ? -1 : 1;
  } else if (!(enable_thread_local_cache() == other.enable_thread_local_cache())) {
    return enable_thread_local_cache() < other.enable_thread_local_cache() ? -1 : 1;
  }
  if (!(has_thread_local_cache_max_size() == other.has_thread_local_cache_max_size())) {
    return has_thread_local_cache_max_size() < other.has_thread_local_cache_max_size() ? -1 : 1;
  } else if (!(thread_local_cache_max_size() == other.thread_local_cache_max_size())) {
    return thread_local_cache_max_size() < other.thread_local_cache_max_size() ? -1 : 1;
  }
  if (!(has_enable_debug_mode() == other.has_enable_debug_mode())) {
    return has_enable_debug_mode() < other.has_enable_debug_mode() ? -1 : 1;
  } else if (!(enable_debug_mode() == other.enable_debug_mode())) {
    return enable_debug_mode() < other.enable_debug_mode() ? -1 : 1;
  }
  if (!(has_enable_tensor_float_32_compute() == other.has_enable_tensor_float_32_compute())) {
    return has_enable_tensor_float_32_compute() < other.has_enable_tensor_float_32_compute() ? -1 : 1;
  } else if (!(enable_tensor_float_32_compute() == other.enable_tensor_float_32_compute())) {
    return enable_tensor_float_32_compute() < other.enable_tensor_float_32_compute() ? -1 : 1;
  }
  if (!(has_enable_mem_chain_merge() == other.has_enable_mem_chain_merge())) {
    return has_enable_mem_chain_merge() < other.has_enable_mem_chain_merge() ? -1 : 1;
  } else if (!(enable_mem_chain_merge() == other.enable_mem_chain_merge())) {
    return enable_mem_chain_merge() < other.enable_mem_chain_merge() ? -1 : 1;
  }
  if (!(has_collective_boxing_conf() == other.has_collective_boxing_conf())) {
    return has_collective_boxing_conf() < other.has_collective_boxing_conf() ? -1 : 1;
  } else if (!(collective_boxing_conf() == other.collective_boxing_conf())) {
    return collective_boxing_conf() < other.collective_boxing_conf() ? -1 : 1;
  }
  if (!(has_nccl_use_compute_stream() == other.has_nccl_use_compute_stream())) {
    return has_nccl_use_compute_stream() < other.has_nccl_use_compute_stream() ? -1 : 1;
  } else if (!(nccl_use_compute_stream() == other.nccl_use_compute_stream())) {
    return nccl_use_compute_stream() < other.nccl_use_compute_stream() ? -1 : 1;
  }
  if (!(has_disable_group_boxing_by_dst_parallel() == other.has_disable_group_boxing_by_dst_parallel())) {
    return has_disable_group_boxing_by_dst_parallel() < other.has_disable_group_boxing_by_dst_parallel() ? -1 : 1;
  } else if (!(disable_group_boxing_by_dst_parallel() == other.disable_group_boxing_by_dst_parallel())) {
    return disable_group_boxing_by_dst_parallel() < other.disable_group_boxing_by_dst_parallel() ? -1 : 1;
  }
  if (!(has_cudnn_conf() == other.has_cudnn_conf())) {
    return has_cudnn_conf() < other.has_cudnn_conf() ? -1 : 1;
  } else if (!(cudnn_conf() == other.cudnn_conf())) {
    return cudnn_conf() < other.cudnn_conf() ? -1 : 1;
  }
  if (!(has_enable_model_io_v2() == other.has_enable_model_io_v2())) {
    return has_enable_model_io_v2() < other.has_enable_model_io_v2() ? -1 : 1;
  } else if (!(enable_model_io_v2() == other.enable_model_io_v2())) {
    return enable_model_io_v2() < other.enable_model_io_v2() ? -1 : 1;
  }
  if (!(has_enable_legacy_model_io() == other.has_enable_legacy_model_io())) {
    return has_enable_legacy_model_io() < other.has_enable_legacy_model_io() ? -1 : 1;
  } else if (!(enable_legacy_model_io() == other.enable_legacy_model_io())) {
    return enable_legacy_model_io() < other.enable_legacy_model_io() ? -1 : 1;
  }
  return 0;
}

bool ConstResource::_Resource_::operator==(const _Resource_& other) const {
  return true
    && has_machine_num() == other.has_machine_num() 
    && machine_num() == other.machine_num()
    && has_gpu_device_num() == other.has_gpu_device_num() 
    && gpu_device_num() == other.gpu_device_num()
    && has_cpu_device_num() == other.has_cpu_device_num() 
    && cpu_device_num() == other.cpu_device_num()
    && has_comm_net_worker_num() == other.has_comm_net_worker_num() 
    && comm_net_worker_num() == other.comm_net_worker_num()
    && has_max_mdsave_worker_num() == other.has_max_mdsave_worker_num() 
    && max_mdsave_worker_num() == other.max_mdsave_worker_num()
    && has_reserved_host_mem_mbyte() == other.has_reserved_host_mem_mbyte() 
    && reserved_host_mem_mbyte() == other.reserved_host_mem_mbyte()
    && has_reserved_device_mem_mbyte() == other.has_reserved_device_mem_mbyte() 
    && reserved_device_mem_mbyte() == other.reserved_device_mem_mbyte()
    && has_compute_thread_pool_size() == other.has_compute_thread_pool_size() 
    && compute_thread_pool_size() == other.compute_thread_pool_size()
    && has_enable_thread_local_cache() == other.has_enable_thread_local_cache() 
    && enable_thread_local_cache() == other.enable_thread_local_cache()
    && has_thread_local_cache_max_size() == other.has_thread_local_cache_max_size() 
    && thread_local_cache_max_size() == other.thread_local_cache_max_size()
    && has_enable_debug_mode() == other.has_enable_debug_mode() 
    && enable_debug_mode() == other.enable_debug_mode()
    && has_enable_tensor_float_32_compute() == other.has_enable_tensor_float_32_compute() 
    && enable_tensor_float_32_compute() == other.enable_tensor_float_32_compute()
    && has_enable_mem_chain_merge() == other.has_enable_mem_chain_merge() 
    && enable_mem_chain_merge() == other.enable_mem_chain_merge()
    && has_collective_boxing_conf() == other.has_collective_boxing_conf() 
    && collective_boxing_conf() == other.collective_boxing_conf()
    && has_nccl_use_compute_stream() == other.has_nccl_use_compute_stream() 
    && nccl_use_compute_stream() == other.nccl_use_compute_stream()
    && has_disable_group_boxing_by_dst_parallel() == other.has_disable_group_boxing_by_dst_parallel() 
    && disable_group_boxing_by_dst_parallel() == other.disable_group_boxing_by_dst_parallel()
    && has_cudnn_conf() == other.has_cudnn_conf() 
    && cudnn_conf() == other.cudnn_conf()
    && has_enable_model_io_v2() == other.has_enable_model_io_v2() 
    && enable_model_io_v2() == other.enable_model_io_v2()
    && has_enable_legacy_model_io() == other.has_enable_legacy_model_io() 
    && enable_legacy_model_io() == other.enable_legacy_model_io()
  ;
}

std::size_t ConstResource::_Resource_::__CalcHash__() const {
  return 0
    ^ (has_machine_num() ? std::hash<int32_t>()(machine_num()) : 0)
    ^ (has_gpu_device_num() ? std::hash<int32_t>()(gpu_device_num()) : 0)
    ^ (has_cpu_device_num() ? std::hash<int32_t>()(cpu_device_num()) : 0)
    ^ (has_comm_net_worker_num() ? std::hash<int32_t>()(comm_net_worker_num()) : 0)
    ^ (has_max_mdsave_worker_num() ? std::hash<int32_t>()(max_mdsave_worker_num()) : 0)
    ^ (has_reserved_host_mem_mbyte() ? std::hash<uint64_t>()(reserved_host_mem_mbyte()) : 0)
    ^ (has_reserved_device_mem_mbyte() ? std::hash<uint64_t>()(reserved_device_mem_mbyte()) : 0)
    ^ (has_compute_thread_pool_size() ? std::hash<int32_t>()(compute_thread_pool_size()) : 0)
    ^ (has_enable_thread_local_cache() ? std::hash<bool>()(enable_thread_local_cache()) : 0)
    ^ (has_thread_local_cache_max_size() ? std::hash<int64_t>()(thread_local_cache_max_size()) : 0)
    ^ (has_enable_debug_mode() ? std::hash<bool>()(enable_debug_mode()) : 0)
    ^ (has_enable_tensor_float_32_compute() ? std::hash<bool>()(enable_tensor_float_32_compute()) : 0)
    ^ (has_enable_mem_chain_merge() ? std::hash<bool>()(enable_mem_chain_merge()) : 0)
    ^ (has_collective_boxing_conf() ? std::hash<::oneflow::cfg::CollectiveBoxingConf>()(collective_boxing_conf()) : 0)
    ^ (has_nccl_use_compute_stream() ? std::hash<bool>()(nccl_use_compute_stream()) : 0)
    ^ (has_disable_group_boxing_by_dst_parallel() ? std::hash<bool>()(disable_group_boxing_by_dst_parallel()) : 0)
    ^ (has_cudnn_conf() ? std::hash<::oneflow::cfg::CudnnConfig>()(cudnn_conf()) : 0)
    ^ (has_enable_model_io_v2() ? std::hash<bool>()(enable_model_io_v2()) : 0)
    ^ (has_enable_legacy_model_io() ? std::hash<bool>()(enable_legacy_model_io()) : 0)
  ;
}

bool ConstResource::_Resource_::operator<(const _Resource_& other) const {
  return false
    || !(has_machine_num() == other.has_machine_num()) ? 
      has_machine_num() < other.has_machine_num() : false
    || !(machine_num() == other.machine_num()) ? 
      machine_num() < other.machine_num() : false
    || !(has_gpu_device_num() == other.has_gpu_device_num()) ? 
      has_gpu_device_num() < other.has_gpu_device_num() : false
    || !(gpu_device_num() == other.gpu_device_num()) ? 
      gpu_device_num() < other.gpu_device_num() : false
    || !(has_cpu_device_num() == other.has_cpu_device_num()) ? 
      has_cpu_device_num() < other.has_cpu_device_num() : false
    || !(cpu_device_num() == other.cpu_device_num()) ? 
      cpu_device_num() < other.cpu_device_num() : false
    || !(has_comm_net_worker_num() == other.has_comm_net_worker_num()) ? 
      has_comm_net_worker_num() < other.has_comm_net_worker_num() : false
    || !(comm_net_worker_num() == other.comm_net_worker_num()) ? 
      comm_net_worker_num() < other.comm_net_worker_num() : false
    || !(has_max_mdsave_worker_num() == other.has_max_mdsave_worker_num()) ? 
      has_max_mdsave_worker_num() < other.has_max_mdsave_worker_num() : false
    || !(max_mdsave_worker_num() == other.max_mdsave_worker_num()) ? 
      max_mdsave_worker_num() < other.max_mdsave_worker_num() : false
    || !(has_reserved_host_mem_mbyte() == other.has_reserved_host_mem_mbyte()) ? 
      has_reserved_host_mem_mbyte() < other.has_reserved_host_mem_mbyte() : false
    || !(reserved_host_mem_mbyte() == other.reserved_host_mem_mbyte()) ? 
      reserved_host_mem_mbyte() < other.reserved_host_mem_mbyte() : false
    || !(has_reserved_device_mem_mbyte() == other.has_reserved_device_mem_mbyte()) ? 
      has_reserved_device_mem_mbyte() < other.has_reserved_device_mem_mbyte() : false
    || !(reserved_device_mem_mbyte() == other.reserved_device_mem_mbyte()) ? 
      reserved_device_mem_mbyte() < other.reserved_device_mem_mbyte() : false
    || !(has_compute_thread_pool_size() == other.has_compute_thread_pool_size()) ? 
      has_compute_thread_pool_size() < other.has_compute_thread_pool_size() : false
    || !(compute_thread_pool_size() == other.compute_thread_pool_size()) ? 
      compute_thread_pool_size() < other.compute_thread_pool_size() : false
    || !(has_enable_thread_local_cache() == other.has_enable_thread_local_cache()) ? 
      has_enable_thread_local_cache() < other.has_enable_thread_local_cache() : false
    || !(enable_thread_local_cache() == other.enable_thread_local_cache()) ? 
      enable_thread_local_cache() < other.enable_thread_local_cache() : false
    || !(has_thread_local_cache_max_size() == other.has_thread_local_cache_max_size()) ? 
      has_thread_local_cache_max_size() < other.has_thread_local_cache_max_size() : false
    || !(thread_local_cache_max_size() == other.thread_local_cache_max_size()) ? 
      thread_local_cache_max_size() < other.thread_local_cache_max_size() : false
    || !(has_enable_debug_mode() == other.has_enable_debug_mode()) ? 
      has_enable_debug_mode() < other.has_enable_debug_mode() : false
    || !(enable_debug_mode() == other.enable_debug_mode()) ? 
      enable_debug_mode() < other.enable_debug_mode() : false
    || !(has_enable_tensor_float_32_compute() == other.has_enable_tensor_float_32_compute()) ? 
      has_enable_tensor_float_32_compute() < other.has_enable_tensor_float_32_compute() : false
    || !(enable_tensor_float_32_compute() == other.enable_tensor_float_32_compute()) ? 
      enable_tensor_float_32_compute() < other.enable_tensor_float_32_compute() : false
    || !(has_enable_mem_chain_merge() == other.has_enable_mem_chain_merge()) ? 
      has_enable_mem_chain_merge() < other.has_enable_mem_chain_merge() : false
    || !(enable_mem_chain_merge() == other.enable_mem_chain_merge()) ? 
      enable_mem_chain_merge() < other.enable_mem_chain_merge() : false
    || !(has_collective_boxing_conf() == other.has_collective_boxing_conf()) ? 
      has_collective_boxing_conf() < other.has_collective_boxing_conf() : false
    || !(collective_boxing_conf() == other.collective_boxing_conf()) ? 
      collective_boxing_conf() < other.collective_boxing_conf() : false
    || !(has_nccl_use_compute_stream() == other.has_nccl_use_compute_stream()) ? 
      has_nccl_use_compute_stream() < other.has_nccl_use_compute_stream() : false
    || !(nccl_use_compute_stream() == other.nccl_use_compute_stream()) ? 
      nccl_use_compute_stream() < other.nccl_use_compute_stream() : false
    || !(has_disable_group_boxing_by_dst_parallel() == other.has_disable_group_boxing_by_dst_parallel()) ? 
      has_disable_group_boxing_by_dst_parallel() < other.has_disable_group_boxing_by_dst_parallel() : false
    || !(disable_group_boxing_by_dst_parallel() == other.disable_group_boxing_by_dst_parallel()) ? 
      disable_group_boxing_by_dst_parallel() < other.disable_group_boxing_by_dst_parallel() : false
    || !(has_cudnn_conf() == other.has_cudnn_conf()) ? 
      has_cudnn_conf() < other.has_cudnn_conf() : false
    || !(cudnn_conf() == other.cudnn_conf()) ? 
      cudnn_conf() < other.cudnn_conf() : false
    || !(has_enable_model_io_v2() == other.has_enable_model_io_v2()) ? 
      has_enable_model_io_v2() < other.has_enable_model_io_v2() : false
    || !(enable_model_io_v2() == other.enable_model_io_v2()) ? 
      enable_model_io_v2() < other.enable_model_io_v2() : false
    || !(has_enable_legacy_model_io() == other.has_enable_legacy_model_io()) ? 
      has_enable_legacy_model_io() < other.has_enable_legacy_model_io() : false
    || !(enable_legacy_model_io() == other.enable_legacy_model_io()) ? 
      enable_legacy_model_io() < other.enable_legacy_model_io() : false
  ;
}

using _Resource_ =  ConstResource::_Resource_;
ConstResource::ConstResource(const ::std::shared_ptr<_Resource_>& data): data_(data) {}
ConstResource::ConstResource(): data_(::std::make_shared<_Resource_>()) {}
ConstResource::ConstResource(const ::oneflow::Resource& proto_resource) {
  BuildFromProto(proto_resource);
}
ConstResource::ConstResource(const ConstResource&) = default;
ConstResource::ConstResource(ConstResource&&) noexcept = default;
ConstResource::~ConstResource() = default;

void ConstResource::ToProto(PbMessage* proto_resource) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::Resource*>(proto_resource));
}
  
::std::string ConstResource::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstResource::__Empty__() const {
  return !data_;
}

int ConstResource::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"machine_num", 1},
    {"gpu_device_num", 4},
    {"cpu_device_num", 5},
    {"comm_net_worker_num", 6},
    {"max_mdsave_worker_num", 7},
    {"reserved_host_mem_mbyte", 12},
    {"reserved_device_mem_mbyte", 13},
    {"compute_thread_pool_size", 15},
    {"enable_thread_local_cache", 16},
    {"thread_local_cache_max_size", 17},
    {"enable_debug_mode", 18},
    {"enable_tensor_float_32_compute", 20},
    {"enable_mem_chain_merge", 21},
    {"collective_boxing_conf", 19},
    {"nccl_use_compute_stream", 30},
    {"disable_group_boxing_by_dst_parallel", 31},
    {"cudnn_conf", 32},
    {"enable_model_io_v2", 41},
    {"enable_legacy_model_io", 42},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstResource::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 4:
    case 5:
    case 6:
    case 7:
    case 12:
    case 13:
    case 15:
    case 16:
    case 17:
    case 18:
    case 20:
    case 21:
    case 19:
    case 30:
    case 31:
    case 32:
    case 41:
    case 42:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstResource::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 12: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(uint64_t),
      };
      return type_indices;
    }
    case 13: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(uint64_t),
      };
      return type_indices;
    }
    case 15: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 16: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 17: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 18: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 20: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 21: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 19: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::CollectiveBoxingConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstCollectiveBoxingConf),
      };
      return type_indices;
    }
    case 30: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 31: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 32: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::CudnnConfig),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstCudnnConfig),
      };
      return type_indices;
    }
    case 41: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 42: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstResource::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &machine_num();
    case 4: return &gpu_device_num();
    case 5: return &cpu_device_num();
    case 6: return &comm_net_worker_num();
    case 7: return &max_mdsave_worker_num();
    case 12: return &reserved_host_mem_mbyte();
    case 13: return &reserved_device_mem_mbyte();
    case 15: return &compute_thread_pool_size();
    case 16: return &enable_thread_local_cache();
    case 17: return &thread_local_cache_max_size();
    case 18: return &enable_debug_mode();
    case 20: return &enable_tensor_float_32_compute();
    case 21: return &enable_mem_chain_merge();
    case 19: return &collective_boxing_conf();
    case 30: return &nccl_use_compute_stream();
    case 31: return &disable_group_boxing_by_dst_parallel();
    case 32: return &cudnn_conf();
    case 41: return &enable_model_io_v2();
    case 42: return &enable_legacy_model_io();
    default: return nullptr;
  }
}

// required or optional field machine_num
bool ConstResource::has_machine_num() const {
  return __SharedPtrOrDefault__()->has_machine_num();
}
const int32_t& ConstResource::machine_num() const {
  return __SharedPtrOrDefault__()->machine_num();
}
// used by pybind11 only
// required or optional field gpu_device_num
bool ConstResource::has_gpu_device_num() const {
  return __SharedPtrOrDefault__()->has_gpu_device_num();
}
const int32_t& ConstResource::gpu_device_num() const {
  return __SharedPtrOrDefault__()->gpu_device_num();
}
// used by pybind11 only
// required or optional field cpu_device_num
bool ConstResource::has_cpu_device_num() const {
  return __SharedPtrOrDefault__()->has_cpu_device_num();
}
const int32_t& ConstResource::cpu_device_num() const {
  return __SharedPtrOrDefault__()->cpu_device_num();
}
// used by pybind11 only
// required or optional field comm_net_worker_num
bool ConstResource::has_comm_net_worker_num() const {
  return __SharedPtrOrDefault__()->has_comm_net_worker_num();
}
const int32_t& ConstResource::comm_net_worker_num() const {
  return __SharedPtrOrDefault__()->comm_net_worker_num();
}
// used by pybind11 only
// required or optional field max_mdsave_worker_num
bool ConstResource::has_max_mdsave_worker_num() const {
  return __SharedPtrOrDefault__()->has_max_mdsave_worker_num();
}
const int32_t& ConstResource::max_mdsave_worker_num() const {
  return __SharedPtrOrDefault__()->max_mdsave_worker_num();
}
// used by pybind11 only
// required or optional field reserved_host_mem_mbyte
bool ConstResource::has_reserved_host_mem_mbyte() const {
  return __SharedPtrOrDefault__()->has_reserved_host_mem_mbyte();
}
const uint64_t& ConstResource::reserved_host_mem_mbyte() const {
  return __SharedPtrOrDefault__()->reserved_host_mem_mbyte();
}
// used by pybind11 only
// required or optional field reserved_device_mem_mbyte
bool ConstResource::has_reserved_device_mem_mbyte() const {
  return __SharedPtrOrDefault__()->has_reserved_device_mem_mbyte();
}
const uint64_t& ConstResource::reserved_device_mem_mbyte() const {
  return __SharedPtrOrDefault__()->reserved_device_mem_mbyte();
}
// used by pybind11 only
// required or optional field compute_thread_pool_size
bool ConstResource::has_compute_thread_pool_size() const {
  return __SharedPtrOrDefault__()->has_compute_thread_pool_size();
}
const int32_t& ConstResource::compute_thread_pool_size() const {
  return __SharedPtrOrDefault__()->compute_thread_pool_size();
}
// used by pybind11 only
// required or optional field enable_thread_local_cache
bool ConstResource::has_enable_thread_local_cache() const {
  return __SharedPtrOrDefault__()->has_enable_thread_local_cache();
}
const bool& ConstResource::enable_thread_local_cache() const {
  return __SharedPtrOrDefault__()->enable_thread_local_cache();
}
// used by pybind11 only
// required or optional field thread_local_cache_max_size
bool ConstResource::has_thread_local_cache_max_size() const {
  return __SharedPtrOrDefault__()->has_thread_local_cache_max_size();
}
const int64_t& ConstResource::thread_local_cache_max_size() const {
  return __SharedPtrOrDefault__()->thread_local_cache_max_size();
}
// used by pybind11 only
// required or optional field enable_debug_mode
bool ConstResource::has_enable_debug_mode() const {
  return __SharedPtrOrDefault__()->has_enable_debug_mode();
}
const bool& ConstResource::enable_debug_mode() const {
  return __SharedPtrOrDefault__()->enable_debug_mode();
}
// used by pybind11 only
// required or optional field enable_tensor_float_32_compute
bool ConstResource::has_enable_tensor_float_32_compute() const {
  return __SharedPtrOrDefault__()->has_enable_tensor_float_32_compute();
}
const bool& ConstResource::enable_tensor_float_32_compute() const {
  return __SharedPtrOrDefault__()->enable_tensor_float_32_compute();
}
// used by pybind11 only
// required or optional field enable_mem_chain_merge
bool ConstResource::has_enable_mem_chain_merge() const {
  return __SharedPtrOrDefault__()->has_enable_mem_chain_merge();
}
const bool& ConstResource::enable_mem_chain_merge() const {
  return __SharedPtrOrDefault__()->enable_mem_chain_merge();
}
// used by pybind11 only
// required or optional field collective_boxing_conf
bool ConstResource::has_collective_boxing_conf() const {
  return __SharedPtrOrDefault__()->has_collective_boxing_conf();
}
const ::oneflow::cfg::CollectiveBoxingConf& ConstResource::collective_boxing_conf() const {
  return __SharedPtrOrDefault__()->collective_boxing_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstCollectiveBoxingConf> ConstResource::shared_const_collective_boxing_conf() const {
  return collective_boxing_conf().__SharedConst__();
}
// required or optional field nccl_use_compute_stream
bool ConstResource::has_nccl_use_compute_stream() const {
  return __SharedPtrOrDefault__()->has_nccl_use_compute_stream();
}
const bool& ConstResource::nccl_use_compute_stream() const {
  return __SharedPtrOrDefault__()->nccl_use_compute_stream();
}
// used by pybind11 only
// required or optional field disable_group_boxing_by_dst_parallel
bool ConstResource::has_disable_group_boxing_by_dst_parallel() const {
  return __SharedPtrOrDefault__()->has_disable_group_boxing_by_dst_parallel();
}
const bool& ConstResource::disable_group_boxing_by_dst_parallel() const {
  return __SharedPtrOrDefault__()->disable_group_boxing_by_dst_parallel();
}
// used by pybind11 only
// required or optional field cudnn_conf
bool ConstResource::has_cudnn_conf() const {
  return __SharedPtrOrDefault__()->has_cudnn_conf();
}
const ::oneflow::cfg::CudnnConfig& ConstResource::cudnn_conf() const {
  return __SharedPtrOrDefault__()->cudnn_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstCudnnConfig> ConstResource::shared_const_cudnn_conf() const {
  return cudnn_conf().__SharedConst__();
}
// required or optional field enable_model_io_v2
bool ConstResource::has_enable_model_io_v2() const {
  return __SharedPtrOrDefault__()->has_enable_model_io_v2();
}
const bool& ConstResource::enable_model_io_v2() const {
  return __SharedPtrOrDefault__()->enable_model_io_v2();
}
// used by pybind11 only
// required or optional field enable_legacy_model_io
bool ConstResource::has_enable_legacy_model_io() const {
  return __SharedPtrOrDefault__()->has_enable_legacy_model_io();
}
const bool& ConstResource::enable_legacy_model_io() const {
  return __SharedPtrOrDefault__()->enable_legacy_model_io();
}
// used by pybind11 only

::std::shared_ptr<ConstResource> ConstResource::__SharedConst__() const {
  return ::std::make_shared<ConstResource>(data_);
}
int64_t ConstResource::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstResource::operator==(const ConstResource& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstResource::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstResource::operator<(const ConstResource& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_Resource_>& ConstResource::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_Resource_> default_ptr = std::make_shared<_Resource_>();
  return default_ptr;
}
const ::std::shared_ptr<_Resource_>& ConstResource::__SharedPtr__() {
  if (!data_) { data_.reset(new _Resource_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstResource
void ConstResource::BuildFromProto(const PbMessage& proto_resource) {
  data_ = ::std::make_shared<_Resource_>(dynamic_cast<const ::oneflow::Resource&>(proto_resource));
}

Resource::Resource(const ::std::shared_ptr<_Resource_>& data)
  : ConstResource(data) {}
Resource::Resource(const Resource& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<Resource> resize
Resource::Resource(Resource&&) noexcept = default; 
Resource::Resource(const ::oneflow::Resource& proto_resource) {
  InitFromProto(proto_resource);
}
Resource::Resource() = default;

Resource::~Resource() = default;

void Resource::InitFromProto(const PbMessage& proto_resource) {
  BuildFromProto(proto_resource);
}
  
void* Resource::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_machine_num();
    case 4: return mutable_gpu_device_num();
    case 5: return mutable_cpu_device_num();
    case 6: return mutable_comm_net_worker_num();
    case 7: return mutable_max_mdsave_worker_num();
    case 12: return mutable_reserved_host_mem_mbyte();
    case 13: return mutable_reserved_device_mem_mbyte();
    case 15: return mutable_compute_thread_pool_size();
    case 16: return mutable_enable_thread_local_cache();
    case 17: return mutable_thread_local_cache_max_size();
    case 18: return mutable_enable_debug_mode();
    case 20: return mutable_enable_tensor_float_32_compute();
    case 21: return mutable_enable_mem_chain_merge();
    case 19: return mutable_collective_boxing_conf();
    case 30: return mutable_nccl_use_compute_stream();
    case 31: return mutable_disable_group_boxing_by_dst_parallel();
    case 32: return mutable_cudnn_conf();
    case 41: return mutable_enable_model_io_v2();
    case 42: return mutable_enable_legacy_model_io();
    default: return nullptr;
  }
}

bool Resource::operator==(const Resource& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t Resource::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool Resource::operator<(const Resource& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void Resource::Clear() {
  if (data_) { data_.reset(); }
}
void Resource::CopyFrom(const Resource& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
Resource& Resource::operator=(const Resource& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field machine_num
void Resource::clear_machine_num() {
  return __SharedPtr__()->clear_machine_num();
}
void Resource::set_machine_num(const int32_t& value) {
  return __SharedPtr__()->set_machine_num(value);
}
int32_t* Resource::mutable_machine_num() {
  return  __SharedPtr__()->mutable_machine_num();
}
// required or optional field gpu_device_num
void Resource::clear_gpu_device_num() {
  return __SharedPtr__()->clear_gpu_device_num();
}
void Resource::set_gpu_device_num(const int32_t& value) {
  return __SharedPtr__()->set_gpu_device_num(value);
}
int32_t* Resource::mutable_gpu_device_num() {
  return  __SharedPtr__()->mutable_gpu_device_num();
}
// required or optional field cpu_device_num
void Resource::clear_cpu_device_num() {
  return __SharedPtr__()->clear_cpu_device_num();
}
void Resource::set_cpu_device_num(const int32_t& value) {
  return __SharedPtr__()->set_cpu_device_num(value);
}
int32_t* Resource::mutable_cpu_device_num() {
  return  __SharedPtr__()->mutable_cpu_device_num();
}
// required or optional field comm_net_worker_num
void Resource::clear_comm_net_worker_num() {
  return __SharedPtr__()->clear_comm_net_worker_num();
}
void Resource::set_comm_net_worker_num(const int32_t& value) {
  return __SharedPtr__()->set_comm_net_worker_num(value);
}
int32_t* Resource::mutable_comm_net_worker_num() {
  return  __SharedPtr__()->mutable_comm_net_worker_num();
}
// required or optional field max_mdsave_worker_num
void Resource::clear_max_mdsave_worker_num() {
  return __SharedPtr__()->clear_max_mdsave_worker_num();
}
void Resource::set_max_mdsave_worker_num(const int32_t& value) {
  return __SharedPtr__()->set_max_mdsave_worker_num(value);
}
int32_t* Resource::mutable_max_mdsave_worker_num() {
  return  __SharedPtr__()->mutable_max_mdsave_worker_num();
}
// required or optional field reserved_host_mem_mbyte
void Resource::clear_reserved_host_mem_mbyte() {
  return __SharedPtr__()->clear_reserved_host_mem_mbyte();
}
void Resource::set_reserved_host_mem_mbyte(const uint64_t& value) {
  return __SharedPtr__()->set_reserved_host_mem_mbyte(value);
}
uint64_t* Resource::mutable_reserved_host_mem_mbyte() {
  return  __SharedPtr__()->mutable_reserved_host_mem_mbyte();
}
// required or optional field reserved_device_mem_mbyte
void Resource::clear_reserved_device_mem_mbyte() {
  return __SharedPtr__()->clear_reserved_device_mem_mbyte();
}
void Resource::set_reserved_device_mem_mbyte(const uint64_t& value) {
  return __SharedPtr__()->set_reserved_device_mem_mbyte(value);
}
uint64_t* Resource::mutable_reserved_device_mem_mbyte() {
  return  __SharedPtr__()->mutable_reserved_device_mem_mbyte();
}
// required or optional field compute_thread_pool_size
void Resource::clear_compute_thread_pool_size() {
  return __SharedPtr__()->clear_compute_thread_pool_size();
}
void Resource::set_compute_thread_pool_size(const int32_t& value) {
  return __SharedPtr__()->set_compute_thread_pool_size(value);
}
int32_t* Resource::mutable_compute_thread_pool_size() {
  return  __SharedPtr__()->mutable_compute_thread_pool_size();
}
// required or optional field enable_thread_local_cache
void Resource::clear_enable_thread_local_cache() {
  return __SharedPtr__()->clear_enable_thread_local_cache();
}
void Resource::set_enable_thread_local_cache(const bool& value) {
  return __SharedPtr__()->set_enable_thread_local_cache(value);
}
bool* Resource::mutable_enable_thread_local_cache() {
  return  __SharedPtr__()->mutable_enable_thread_local_cache();
}
// required or optional field thread_local_cache_max_size
void Resource::clear_thread_local_cache_max_size() {
  return __SharedPtr__()->clear_thread_local_cache_max_size();
}
void Resource::set_thread_local_cache_max_size(const int64_t& value) {
  return __SharedPtr__()->set_thread_local_cache_max_size(value);
}
int64_t* Resource::mutable_thread_local_cache_max_size() {
  return  __SharedPtr__()->mutable_thread_local_cache_max_size();
}
// required or optional field enable_debug_mode
void Resource::clear_enable_debug_mode() {
  return __SharedPtr__()->clear_enable_debug_mode();
}
void Resource::set_enable_debug_mode(const bool& value) {
  return __SharedPtr__()->set_enable_debug_mode(value);
}
bool* Resource::mutable_enable_debug_mode() {
  return  __SharedPtr__()->mutable_enable_debug_mode();
}
// required or optional field enable_tensor_float_32_compute
void Resource::clear_enable_tensor_float_32_compute() {
  return __SharedPtr__()->clear_enable_tensor_float_32_compute();
}
void Resource::set_enable_tensor_float_32_compute(const bool& value) {
  return __SharedPtr__()->set_enable_tensor_float_32_compute(value);
}
bool* Resource::mutable_enable_tensor_float_32_compute() {
  return  __SharedPtr__()->mutable_enable_tensor_float_32_compute();
}
// required or optional field enable_mem_chain_merge
void Resource::clear_enable_mem_chain_merge() {
  return __SharedPtr__()->clear_enable_mem_chain_merge();
}
void Resource::set_enable_mem_chain_merge(const bool& value) {
  return __SharedPtr__()->set_enable_mem_chain_merge(value);
}
bool* Resource::mutable_enable_mem_chain_merge() {
  return  __SharedPtr__()->mutable_enable_mem_chain_merge();
}
// required or optional field collective_boxing_conf
void Resource::clear_collective_boxing_conf() {
  return __SharedPtr__()->clear_collective_boxing_conf();
}
::oneflow::cfg::CollectiveBoxingConf* Resource::mutable_collective_boxing_conf() {
  return __SharedPtr__()->mutable_collective_boxing_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::CollectiveBoxingConf> Resource::shared_mutable_collective_boxing_conf() {
  return mutable_collective_boxing_conf()->__SharedMutable__();
}
// required or optional field nccl_use_compute_stream
void Resource::clear_nccl_use_compute_stream() {
  return __SharedPtr__()->clear_nccl_use_compute_stream();
}
void Resource::set_nccl_use_compute_stream(const bool& value) {
  return __SharedPtr__()->set_nccl_use_compute_stream(value);
}
bool* Resource::mutable_nccl_use_compute_stream() {
  return  __SharedPtr__()->mutable_nccl_use_compute_stream();
}
// required or optional field disable_group_boxing_by_dst_parallel
void Resource::clear_disable_group_boxing_by_dst_parallel() {
  return __SharedPtr__()->clear_disable_group_boxing_by_dst_parallel();
}
void Resource::set_disable_group_boxing_by_dst_parallel(const bool& value) {
  return __SharedPtr__()->set_disable_group_boxing_by_dst_parallel(value);
}
bool* Resource::mutable_disable_group_boxing_by_dst_parallel() {
  return  __SharedPtr__()->mutable_disable_group_boxing_by_dst_parallel();
}
// required or optional field cudnn_conf
void Resource::clear_cudnn_conf() {
  return __SharedPtr__()->clear_cudnn_conf();
}
::oneflow::cfg::CudnnConfig* Resource::mutable_cudnn_conf() {
  return __SharedPtr__()->mutable_cudnn_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::CudnnConfig> Resource::shared_mutable_cudnn_conf() {
  return mutable_cudnn_conf()->__SharedMutable__();
}
// required or optional field enable_model_io_v2
void Resource::clear_enable_model_io_v2() {
  return __SharedPtr__()->clear_enable_model_io_v2();
}
void Resource::set_enable_model_io_v2(const bool& value) {
  return __SharedPtr__()->set_enable_model_io_v2(value);
}
bool* Resource::mutable_enable_model_io_v2() {
  return  __SharedPtr__()->mutable_enable_model_io_v2();
}
// required or optional field enable_legacy_model_io
void Resource::clear_enable_legacy_model_io() {
  return __SharedPtr__()->clear_enable_legacy_model_io();
}
void Resource::set_enable_legacy_model_io(const bool& value) {
  return __SharedPtr__()->set_enable_legacy_model_io(value);
}
bool* Resource::mutable_enable_legacy_model_io() {
  return  __SharedPtr__()->mutable_enable_legacy_model_io();
}

::std::shared_ptr<Resource> Resource::__SharedMutable__() {
  return ::std::make_shared<Resource>(__SharedPtr__());
}


}
} // namespace oneflow
