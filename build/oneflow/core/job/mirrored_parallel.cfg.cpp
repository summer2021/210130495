#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/job/mirrored_parallel.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstMirroredParallel::_MirroredParallel_::_MirroredParallel_() { Clear(); }
ConstMirroredParallel::_MirroredParallel_::_MirroredParallel_(const _MirroredParallel_& other) { CopyFrom(other); }
ConstMirroredParallel::_MirroredParallel_::_MirroredParallel_(const ::oneflow::MirroredParallel& proto_mirroredparallel) {
  InitFromProto(proto_mirroredparallel);
}
ConstMirroredParallel::_MirroredParallel_::_MirroredParallel_(_MirroredParallel_&& other) = default;
ConstMirroredParallel::_MirroredParallel_::~_MirroredParallel_() = default;

void ConstMirroredParallel::_MirroredParallel_::InitFromProto(const ::oneflow::MirroredParallel& proto_mirroredparallel) {
  Clear();
    
}

void ConstMirroredParallel::_MirroredParallel_::ToProto(::oneflow::MirroredParallel* proto_mirroredparallel) const {
  proto_mirroredparallel->Clear();

}

::std::string ConstMirroredParallel::_MirroredParallel_::DebugString() const {
  ::oneflow::MirroredParallel proto_mirroredparallel;
  this->ToProto(&proto_mirroredparallel);
  return proto_mirroredparallel.DebugString();
}

void ConstMirroredParallel::_MirroredParallel_::Clear() {
}

void ConstMirroredParallel::_MirroredParallel_::CopyFrom(const _MirroredParallel_& other) {
}



int ConstMirroredParallel::_MirroredParallel_::compare(const _MirroredParallel_& other) {
  return 0;
}

bool ConstMirroredParallel::_MirroredParallel_::operator==(const _MirroredParallel_& other) const {
  return true
  ;
}

std::size_t ConstMirroredParallel::_MirroredParallel_::__CalcHash__() const {
  return 0
  ;
}

bool ConstMirroredParallel::_MirroredParallel_::operator<(const _MirroredParallel_& other) const {
  return false
  ;
}

using _MirroredParallel_ =  ConstMirroredParallel::_MirroredParallel_;
ConstMirroredParallel::ConstMirroredParallel(const ::std::shared_ptr<_MirroredParallel_>& data): data_(data) {}
ConstMirroredParallel::ConstMirroredParallel(): data_(::std::make_shared<_MirroredParallel_>()) {}
ConstMirroredParallel::ConstMirroredParallel(const ::oneflow::MirroredParallel& proto_mirroredparallel) {
  BuildFromProto(proto_mirroredparallel);
}
ConstMirroredParallel::ConstMirroredParallel(const ConstMirroredParallel&) = default;
ConstMirroredParallel::ConstMirroredParallel(ConstMirroredParallel&&) noexcept = default;
ConstMirroredParallel::~ConstMirroredParallel() = default;

void ConstMirroredParallel::ToProto(PbMessage* proto_mirroredparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MirroredParallel*>(proto_mirroredparallel));
}
  
::std::string ConstMirroredParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMirroredParallel::__Empty__() const {
  return !data_;
}

int ConstMirroredParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstMirroredParallel::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstMirroredParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMirroredParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstMirroredParallel> ConstMirroredParallel::__SharedConst__() const {
  return ::std::make_shared<ConstMirroredParallel>(data_);
}
int64_t ConstMirroredParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMirroredParallel::operator==(const ConstMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMirroredParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMirroredParallel::operator<(const ConstMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MirroredParallel_>& ConstMirroredParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MirroredParallel_> default_ptr = std::make_shared<_MirroredParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_MirroredParallel_>& ConstMirroredParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _MirroredParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMirroredParallel
void ConstMirroredParallel::BuildFromProto(const PbMessage& proto_mirroredparallel) {
  data_ = ::std::make_shared<_MirroredParallel_>(dynamic_cast<const ::oneflow::MirroredParallel&>(proto_mirroredparallel));
}

MirroredParallel::MirroredParallel(const ::std::shared_ptr<_MirroredParallel_>& data)
  : ConstMirroredParallel(data) {}
MirroredParallel::MirroredParallel(const MirroredParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MirroredParallel> resize
MirroredParallel::MirroredParallel(MirroredParallel&&) noexcept = default; 
MirroredParallel::MirroredParallel(const ::oneflow::MirroredParallel& proto_mirroredparallel) {
  InitFromProto(proto_mirroredparallel);
}
MirroredParallel::MirroredParallel() = default;

MirroredParallel::~MirroredParallel() = default;

void MirroredParallel::InitFromProto(const PbMessage& proto_mirroredparallel) {
  BuildFromProto(proto_mirroredparallel);
}
  
void* MirroredParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool MirroredParallel::operator==(const MirroredParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MirroredParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MirroredParallel::operator<(const MirroredParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MirroredParallel::Clear() {
  if (data_) { data_.reset(); }
}
void MirroredParallel::CopyFrom(const MirroredParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MirroredParallel& MirroredParallel::operator=(const MirroredParallel& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<MirroredParallel> MirroredParallel::__SharedMutable__() {
  return ::std::make_shared<MirroredParallel>(__SharedPtr__());
}
ConstOptMirroredParallel::_OptMirroredParallel_::_OptMirroredParallel_() { Clear(); }
ConstOptMirroredParallel::_OptMirroredParallel_::_OptMirroredParallel_(const _OptMirroredParallel_& other) { CopyFrom(other); }
ConstOptMirroredParallel::_OptMirroredParallel_::_OptMirroredParallel_(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel) {
  InitFromProto(proto_optmirroredparallel);
}
ConstOptMirroredParallel::_OptMirroredParallel_::_OptMirroredParallel_(_OptMirroredParallel_&& other) = default;
ConstOptMirroredParallel::_OptMirroredParallel_::~_OptMirroredParallel_() = default;

void ConstOptMirroredParallel::_OptMirroredParallel_::InitFromProto(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel) {
  Clear();
  // required_or_optional field: mirrored_parallel
  if (proto_optmirroredparallel.has_mirrored_parallel()) {
  *mutable_mirrored_parallel() = ::oneflow::cfg::MirroredParallel(proto_optmirroredparallel.mirrored_parallel());      
  }
    
}

void ConstOptMirroredParallel::_OptMirroredParallel_::ToProto(::oneflow::OptMirroredParallel* proto_optmirroredparallel) const {
  proto_optmirroredparallel->Clear();
  // required_or_optional field: mirrored_parallel
  if (this->has_mirrored_parallel()) {
    ::oneflow::MirroredParallel proto_mirrored_parallel;
    mirrored_parallel().ToProto(&proto_mirrored_parallel);
    proto_optmirroredparallel->mutable_mirrored_parallel()->CopyFrom(proto_mirrored_parallel);
    }

}

::std::string ConstOptMirroredParallel::_OptMirroredParallel_::DebugString() const {
  ::oneflow::OptMirroredParallel proto_optmirroredparallel;
  this->ToProto(&proto_optmirroredparallel);
  return proto_optmirroredparallel.DebugString();
}

void ConstOptMirroredParallel::_OptMirroredParallel_::Clear() {
  clear_mirrored_parallel();
}

void ConstOptMirroredParallel::_OptMirroredParallel_::CopyFrom(const _OptMirroredParallel_& other) {
  if (other.has_mirrored_parallel()) {
    mutable_mirrored_parallel()->CopyFrom(other.mirrored_parallel());
  } else {
    clear_mirrored_parallel();
  }
}


// optional field mirrored_parallel
bool ConstOptMirroredParallel::_OptMirroredParallel_::has_mirrored_parallel() const {
  return has_mirrored_parallel_;
}
const ::oneflow::cfg::MirroredParallel& ConstOptMirroredParallel::_OptMirroredParallel_::mirrored_parallel() const {
  if (!mirrored_parallel_) {
    static const ::std::shared_ptr<::oneflow::cfg::MirroredParallel> default_static_value =
      ::std::make_shared<::oneflow::cfg::MirroredParallel>();
    return *default_static_value;
  }
  return *(mirrored_parallel_.get());
}
void ConstOptMirroredParallel::_OptMirroredParallel_::clear_mirrored_parallel() {
  if (mirrored_parallel_) {
    mirrored_parallel_->Clear();
  }
  has_mirrored_parallel_ = false;
}
::oneflow::cfg::MirroredParallel* ConstOptMirroredParallel::_OptMirroredParallel_::mutable_mirrored_parallel() {
  if (!mirrored_parallel_) {
    mirrored_parallel_ = ::std::make_shared<::oneflow::cfg::MirroredParallel>();
  }
  has_mirrored_parallel_ = true;
  return mirrored_parallel_.get();
}


int ConstOptMirroredParallel::_OptMirroredParallel_::compare(const _OptMirroredParallel_& other) {
  if (!(has_mirrored_parallel() == other.has_mirrored_parallel())) {
    return has_mirrored_parallel() < other.has_mirrored_parallel() ? -1 : 1;
  } else if (!(mirrored_parallel() == other.mirrored_parallel())) {
    return mirrored_parallel() < other.mirrored_parallel() ? -1 : 1;
  }
  return 0;
}

bool ConstOptMirroredParallel::_OptMirroredParallel_::operator==(const _OptMirroredParallel_& other) const {
  return true
    && has_mirrored_parallel() == other.has_mirrored_parallel() 
    && mirrored_parallel() == other.mirrored_parallel()
  ;
}

std::size_t ConstOptMirroredParallel::_OptMirroredParallel_::__CalcHash__() const {
  return 0
    ^ (has_mirrored_parallel() ? std::hash<::oneflow::cfg::MirroredParallel>()(mirrored_parallel()) : 0)
  ;
}

bool ConstOptMirroredParallel::_OptMirroredParallel_::operator<(const _OptMirroredParallel_& other) const {
  return false
    || !(has_mirrored_parallel() == other.has_mirrored_parallel()) ? 
      has_mirrored_parallel() < other.has_mirrored_parallel() : false
    || !(mirrored_parallel() == other.mirrored_parallel()) ? 
      mirrored_parallel() < other.mirrored_parallel() : false
  ;
}

using _OptMirroredParallel_ =  ConstOptMirroredParallel::_OptMirroredParallel_;
ConstOptMirroredParallel::ConstOptMirroredParallel(const ::std::shared_ptr<_OptMirroredParallel_>& data): data_(data) {}
ConstOptMirroredParallel::ConstOptMirroredParallel(): data_(::std::make_shared<_OptMirroredParallel_>()) {}
ConstOptMirroredParallel::ConstOptMirroredParallel(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel) {
  BuildFromProto(proto_optmirroredparallel);
}
ConstOptMirroredParallel::ConstOptMirroredParallel(const ConstOptMirroredParallel&) = default;
ConstOptMirroredParallel::ConstOptMirroredParallel(ConstOptMirroredParallel&&) noexcept = default;
ConstOptMirroredParallel::~ConstOptMirroredParallel() = default;

void ConstOptMirroredParallel::ToProto(PbMessage* proto_optmirroredparallel) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OptMirroredParallel*>(proto_optmirroredparallel));
}
  
::std::string ConstOptMirroredParallel::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOptMirroredParallel::__Empty__() const {
  return !data_;
}

int ConstOptMirroredParallel::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"mirrored_parallel", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOptMirroredParallel::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOptMirroredParallel::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MirroredParallel),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMirroredParallel),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOptMirroredParallel::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &mirrored_parallel();
    default: return nullptr;
  }
}

// required or optional field mirrored_parallel
bool ConstOptMirroredParallel::has_mirrored_parallel() const {
  return __SharedPtrOrDefault__()->has_mirrored_parallel();
}
const ::oneflow::cfg::MirroredParallel& ConstOptMirroredParallel::mirrored_parallel() const {
  return __SharedPtrOrDefault__()->mirrored_parallel();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMirroredParallel> ConstOptMirroredParallel::shared_const_mirrored_parallel() const {
  return mirrored_parallel().__SharedConst__();
}

::std::shared_ptr<ConstOptMirroredParallel> ConstOptMirroredParallel::__SharedConst__() const {
  return ::std::make_shared<ConstOptMirroredParallel>(data_);
}
int64_t ConstOptMirroredParallel::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOptMirroredParallel::operator==(const ConstOptMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOptMirroredParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOptMirroredParallel::operator<(const ConstOptMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OptMirroredParallel_>& ConstOptMirroredParallel::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OptMirroredParallel_> default_ptr = std::make_shared<_OptMirroredParallel_>();
  return default_ptr;
}
const ::std::shared_ptr<_OptMirroredParallel_>& ConstOptMirroredParallel::__SharedPtr__() {
  if (!data_) { data_.reset(new _OptMirroredParallel_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOptMirroredParallel
void ConstOptMirroredParallel::BuildFromProto(const PbMessage& proto_optmirroredparallel) {
  data_ = ::std::make_shared<_OptMirroredParallel_>(dynamic_cast<const ::oneflow::OptMirroredParallel&>(proto_optmirroredparallel));
}

OptMirroredParallel::OptMirroredParallel(const ::std::shared_ptr<_OptMirroredParallel_>& data)
  : ConstOptMirroredParallel(data) {}
OptMirroredParallel::OptMirroredParallel(const OptMirroredParallel& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OptMirroredParallel> resize
OptMirroredParallel::OptMirroredParallel(OptMirroredParallel&&) noexcept = default; 
OptMirroredParallel::OptMirroredParallel(const ::oneflow::OptMirroredParallel& proto_optmirroredparallel) {
  InitFromProto(proto_optmirroredparallel);
}
OptMirroredParallel::OptMirroredParallel() = default;

OptMirroredParallel::~OptMirroredParallel() = default;

void OptMirroredParallel::InitFromProto(const PbMessage& proto_optmirroredparallel) {
  BuildFromProto(proto_optmirroredparallel);
}
  
void* OptMirroredParallel::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_mirrored_parallel();
    default: return nullptr;
  }
}

bool OptMirroredParallel::operator==(const OptMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OptMirroredParallel::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OptMirroredParallel::operator<(const OptMirroredParallel& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OptMirroredParallel::Clear() {
  if (data_) { data_.reset(); }
}
void OptMirroredParallel::CopyFrom(const OptMirroredParallel& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OptMirroredParallel& OptMirroredParallel::operator=(const OptMirroredParallel& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field mirrored_parallel
void OptMirroredParallel::clear_mirrored_parallel() {
  return __SharedPtr__()->clear_mirrored_parallel();
}
::oneflow::cfg::MirroredParallel* OptMirroredParallel::mutable_mirrored_parallel() {
  return __SharedPtr__()->mutable_mirrored_parallel();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MirroredParallel> OptMirroredParallel::shared_mutable_mirrored_parallel() {
  return mutable_mirrored_parallel()->__SharedMutable__();
}

::std::shared_ptr<OptMirroredParallel> OptMirroredParallel::__SharedMutable__() {
  return ::std::make_shared<OptMirroredParallel>(__SharedPtr__());
}
ConstMirroredSignature::_MirroredSignature_::_MirroredSignature_() { Clear(); }
ConstMirroredSignature::_MirroredSignature_::_MirroredSignature_(const _MirroredSignature_& other) { CopyFrom(other); }
ConstMirroredSignature::_MirroredSignature_::_MirroredSignature_(const ::oneflow::MirroredSignature& proto_mirroredsignature) {
  InitFromProto(proto_mirroredsignature);
}
ConstMirroredSignature::_MirroredSignature_::_MirroredSignature_(_MirroredSignature_&& other) = default;
ConstMirroredSignature::_MirroredSignature_::~_MirroredSignature_() = default;

void ConstMirroredSignature::_MirroredSignature_::InitFromProto(const ::oneflow::MirroredSignature& proto_mirroredsignature) {
  Clear();
  // map field : bn_in_op2opt_mirrored_parallel
  if (!proto_mirroredsignature.bn_in_op2opt_mirrored_parallel().empty()) {
_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_&  mut_bn_in_op2opt_mirrored_parallel = *mutable_bn_in_op2opt_mirrored_parallel();
    for (const auto& pair : proto_mirroredsignature.bn_in_op2opt_mirrored_parallel()) {
      mut_bn_in_op2opt_mirrored_parallel[pair.first] = ::oneflow::cfg::OptMirroredParallel(pair.second);
    }
  }
    
}

void ConstMirroredSignature::_MirroredSignature_::ToProto(::oneflow::MirroredSignature* proto_mirroredsignature) const {
  proto_mirroredsignature->Clear();
  // map field : bn_in_op2opt_mirrored_parallel
  if (!bn_in_op2opt_mirrored_parallel().empty()) {
    auto& mut_bn_in_op2opt_mirrored_parallel = *(proto_mirroredsignature->mutable_bn_in_op2opt_mirrored_parallel());
    for (const auto& pair : bn_in_op2opt_mirrored_parallel()) {
      ::oneflow::OptMirroredParallel proto_bn_in_op2opt_mirrored_parallel_value;
      pair.second.ToProto(&proto_bn_in_op2opt_mirrored_parallel_value);
      mut_bn_in_op2opt_mirrored_parallel[pair.first] = proto_bn_in_op2opt_mirrored_parallel_value;
    }
  }

}

::std::string ConstMirroredSignature::_MirroredSignature_::DebugString() const {
  ::oneflow::MirroredSignature proto_mirroredsignature;
  this->ToProto(&proto_mirroredsignature);
  return proto_mirroredsignature.DebugString();
}

void ConstMirroredSignature::_MirroredSignature_::Clear() {
  clear_bn_in_op2opt_mirrored_parallel();
}

void ConstMirroredSignature::_MirroredSignature_::CopyFrom(const _MirroredSignature_& other) {
  mutable_bn_in_op2opt_mirrored_parallel()->CopyFrom(other.bn_in_op2opt_mirrored_parallel());
}


::std::size_t ConstMirroredSignature::_MirroredSignature_::bn_in_op2opt_mirrored_parallel_size() const {
  if (!bn_in_op2opt_mirrored_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>();
    return default_static_value->size();
  }
  return bn_in_op2opt_mirrored_parallel_->size();
}
const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& ConstMirroredSignature::_MirroredSignature_::bn_in_op2opt_mirrored_parallel() const {
  if (!bn_in_op2opt_mirrored_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>();
    return *(default_static_value.get());
  }
  return *(bn_in_op2opt_mirrored_parallel_.get());
}

_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ * ConstMirroredSignature::_MirroredSignature_::mutable_bn_in_op2opt_mirrored_parallel() {
  if (!bn_in_op2opt_mirrored_parallel_) {
    bn_in_op2opt_mirrored_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>();
  }
  return bn_in_op2opt_mirrored_parallel_.get();
}

const ::oneflow::cfg::OptMirroredParallel& ConstMirroredSignature::_MirroredSignature_::bn_in_op2opt_mirrored_parallel(::std::string key) const {
  if (!bn_in_op2opt_mirrored_parallel_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>();
    return default_static_value->at(key);
  }
  return bn_in_op2opt_mirrored_parallel_->at(key);
}

void ConstMirroredSignature::_MirroredSignature_::clear_bn_in_op2opt_mirrored_parallel() {
  if (!bn_in_op2opt_mirrored_parallel_) {
    bn_in_op2opt_mirrored_parallel_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>();
  }
  return bn_in_op2opt_mirrored_parallel_->Clear();
}



int ConstMirroredSignature::_MirroredSignature_::compare(const _MirroredSignature_& other) {
  if (!(bn_in_op2opt_mirrored_parallel() == other.bn_in_op2opt_mirrored_parallel())) {
    return bn_in_op2opt_mirrored_parallel() < other.bn_in_op2opt_mirrored_parallel() ? -1 : 1;
  }
  return 0;
}

bool ConstMirroredSignature::_MirroredSignature_::operator==(const _MirroredSignature_& other) const {
  return true
    && bn_in_op2opt_mirrored_parallel() == other.bn_in_op2opt_mirrored_parallel()
  ;
}

std::size_t ConstMirroredSignature::_MirroredSignature_::__CalcHash__() const {
  return 0
    ^ bn_in_op2opt_mirrored_parallel().__CalcHash__()
  ;
}

bool ConstMirroredSignature::_MirroredSignature_::operator<(const _MirroredSignature_& other) const {
  return false
    || !(bn_in_op2opt_mirrored_parallel() == other.bn_in_op2opt_mirrored_parallel()) ? 
      bn_in_op2opt_mirrored_parallel() < other.bn_in_op2opt_mirrored_parallel() : false
  ;
}

using _MirroredSignature_ =  ConstMirroredSignature::_MirroredSignature_;
ConstMirroredSignature::ConstMirroredSignature(const ::std::shared_ptr<_MirroredSignature_>& data): data_(data) {}
ConstMirroredSignature::ConstMirroredSignature(): data_(::std::make_shared<_MirroredSignature_>()) {}
ConstMirroredSignature::ConstMirroredSignature(const ::oneflow::MirroredSignature& proto_mirroredsignature) {
  BuildFromProto(proto_mirroredsignature);
}
ConstMirroredSignature::ConstMirroredSignature(const ConstMirroredSignature&) = default;
ConstMirroredSignature::ConstMirroredSignature(ConstMirroredSignature&&) noexcept = default;
ConstMirroredSignature::~ConstMirroredSignature() = default;

void ConstMirroredSignature::ToProto(PbMessage* proto_mirroredsignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MirroredSignature*>(proto_mirroredsignature));
}
  
::std::string ConstMirroredSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMirroredSignature::__Empty__() const {
  return !data_;
}

int ConstMirroredSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bn_in_op2opt_mirrored_parallel", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMirroredSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMirroredSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OptMirroredParallel>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMirroredSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bn_in_op2opt_mirrored_parallel();
    default: return nullptr;
  }
}

// map field bn_in_op2opt_mirrored_parallel
::std::size_t ConstMirroredSignature::bn_in_op2opt_mirrored_parallel_size() const {
  return __SharedPtrOrDefault__()->bn_in_op2opt_mirrored_parallel_size();
}

const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& ConstMirroredSignature::bn_in_op2opt_mirrored_parallel() const {
  return __SharedPtrOrDefault__()->bn_in_op2opt_mirrored_parallel();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> ConstMirroredSignature::shared_const_bn_in_op2opt_mirrored_parallel() const {
  return bn_in_op2opt_mirrored_parallel().__SharedConst__();
}

::std::shared_ptr<ConstMirroredSignature> ConstMirroredSignature::__SharedConst__() const {
  return ::std::make_shared<ConstMirroredSignature>(data_);
}
int64_t ConstMirroredSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMirroredSignature::operator==(const ConstMirroredSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMirroredSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMirroredSignature::operator<(const ConstMirroredSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MirroredSignature_>& ConstMirroredSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MirroredSignature_> default_ptr = std::make_shared<_MirroredSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_MirroredSignature_>& ConstMirroredSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _MirroredSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMirroredSignature
void ConstMirroredSignature::BuildFromProto(const PbMessage& proto_mirroredsignature) {
  data_ = ::std::make_shared<_MirroredSignature_>(dynamic_cast<const ::oneflow::MirroredSignature&>(proto_mirroredsignature));
}

MirroredSignature::MirroredSignature(const ::std::shared_ptr<_MirroredSignature_>& data)
  : ConstMirroredSignature(data) {}
MirroredSignature::MirroredSignature(const MirroredSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MirroredSignature> resize
MirroredSignature::MirroredSignature(MirroredSignature&&) noexcept = default; 
MirroredSignature::MirroredSignature(const ::oneflow::MirroredSignature& proto_mirroredsignature) {
  InitFromProto(proto_mirroredsignature);
}
MirroredSignature::MirroredSignature() = default;

MirroredSignature::~MirroredSignature() = default;

void MirroredSignature::InitFromProto(const PbMessage& proto_mirroredsignature) {
  BuildFromProto(proto_mirroredsignature);
}
  
void* MirroredSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bn_in_op2opt_mirrored_parallel();
    default: return nullptr;
  }
}

bool MirroredSignature::operator==(const MirroredSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MirroredSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MirroredSignature::operator<(const MirroredSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MirroredSignature::Clear() {
  if (data_) { data_.reset(); }
}
void MirroredSignature::CopyFrom(const MirroredSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MirroredSignature& MirroredSignature::operator=(const MirroredSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field bn_in_op2opt_mirrored_parallel
void MirroredSignature::clear_bn_in_op2opt_mirrored_parallel() {
  return __SharedPtr__()->clear_bn_in_op2opt_mirrored_parallel();
}

const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_ & MirroredSignature::bn_in_op2opt_mirrored_parallel() const {
  return __SharedConst__()->bn_in_op2opt_mirrored_parallel();
}

_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_* MirroredSignature::mutable_bn_in_op2opt_mirrored_parallel() {
  return __SharedPtr__()->mutable_bn_in_op2opt_mirrored_parallel();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> MirroredSignature::shared_mutable_bn_in_op2opt_mirrored_parallel() {
  return mutable_bn_in_op2opt_mirrored_parallel()->__SharedMutable__();
}

::std::shared_ptr<MirroredSignature> MirroredSignature::__SharedMutable__() {
  return ::std::make_shared<MirroredSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OptMirroredParallel>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OptMirroredParallel>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_() = default;
Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::~Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::OptMirroredParallel>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::OptMirroredParallel& Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstOptMirroredParallel> Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ConstOptMirroredParallel> Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ConstOptMirroredParallel> Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OptMirroredParallel>>& data): Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_(data) {}
_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_() = default;
_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::~_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_() = default;

void _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OptMirroredParallel>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OptMirroredParallel>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::operator==(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::OptMirroredParallel>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::operator<(const _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_> _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::OptMirroredParallel> _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ::oneflow::cfg::OptMirroredParallel> _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_, ::oneflow::cfg::OptMirroredParallel> _CFG_ONEFLOW_CORE_JOB_MIRRORED_PARALLEL_CFG_H__MapField___std__string_OptMirroredParallel_::shared_mut_end() { return end(); }

}
} // namespace oneflow
