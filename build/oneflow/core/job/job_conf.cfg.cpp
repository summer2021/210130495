#include "oneflow/core/job/job_conf.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/job/initializer_conf.cfg.h"
#include "oneflow/core/job/learning_rate_schedule_conf.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/operator/interface_blob_conf.cfg.h"
#include "oneflow/core/job/job_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::_NaiveModelUpdateConf_() { Clear(); }
ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::_NaiveModelUpdateConf_(const _NaiveModelUpdateConf_& other) { CopyFrom(other); }
ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::_NaiveModelUpdateConf_(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf) {
  InitFromProto(proto_naivemodelupdateconf);
}
ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::_NaiveModelUpdateConf_(_NaiveModelUpdateConf_&& other) = default;
ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::~_NaiveModelUpdateConf_() = default;

void ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::InitFromProto(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf) {
  Clear();
    
}

void ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::ToProto(::oneflow::NaiveModelUpdateConf* proto_naivemodelupdateconf) const {
  proto_naivemodelupdateconf->Clear();

}

::std::string ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::DebugString() const {
  ::oneflow::NaiveModelUpdateConf proto_naivemodelupdateconf;
  this->ToProto(&proto_naivemodelupdateconf);
  return proto_naivemodelupdateconf.DebugString();
}

void ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::Clear() {
}

void ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::CopyFrom(const _NaiveModelUpdateConf_& other) {
}



int ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::compare(const _NaiveModelUpdateConf_& other) {
  return 0;
}

bool ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::operator==(const _NaiveModelUpdateConf_& other) const {
  return true
  ;
}

std::size_t ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::__CalcHash__() const {
  return 0
  ;
}

bool ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_::operator<(const _NaiveModelUpdateConf_& other) const {
  return false
  ;
}

using _NaiveModelUpdateConf_ =  ConstNaiveModelUpdateConf::_NaiveModelUpdateConf_;
ConstNaiveModelUpdateConf::ConstNaiveModelUpdateConf(const ::std::shared_ptr<_NaiveModelUpdateConf_>& data): data_(data) {}
ConstNaiveModelUpdateConf::ConstNaiveModelUpdateConf(): data_(::std::make_shared<_NaiveModelUpdateConf_>()) {}
ConstNaiveModelUpdateConf::ConstNaiveModelUpdateConf(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf) {
  BuildFromProto(proto_naivemodelupdateconf);
}
ConstNaiveModelUpdateConf::ConstNaiveModelUpdateConf(const ConstNaiveModelUpdateConf&) = default;
ConstNaiveModelUpdateConf::ConstNaiveModelUpdateConf(ConstNaiveModelUpdateConf&&) noexcept = default;
ConstNaiveModelUpdateConf::~ConstNaiveModelUpdateConf() = default;

void ConstNaiveModelUpdateConf::ToProto(PbMessage* proto_naivemodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::NaiveModelUpdateConf*>(proto_naivemodelupdateconf));
}
  
::std::string ConstNaiveModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstNaiveModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstNaiveModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstNaiveModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstNaiveModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstNaiveModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstNaiveModelUpdateConf> ConstNaiveModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstNaiveModelUpdateConf>(data_);
}
int64_t ConstNaiveModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstNaiveModelUpdateConf::operator==(const ConstNaiveModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstNaiveModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstNaiveModelUpdateConf::operator<(const ConstNaiveModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_NaiveModelUpdateConf_>& ConstNaiveModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_NaiveModelUpdateConf_> default_ptr = std::make_shared<_NaiveModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_NaiveModelUpdateConf_>& ConstNaiveModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _NaiveModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstNaiveModelUpdateConf
void ConstNaiveModelUpdateConf::BuildFromProto(const PbMessage& proto_naivemodelupdateconf) {
  data_ = ::std::make_shared<_NaiveModelUpdateConf_>(dynamic_cast<const ::oneflow::NaiveModelUpdateConf&>(proto_naivemodelupdateconf));
}

NaiveModelUpdateConf::NaiveModelUpdateConf(const ::std::shared_ptr<_NaiveModelUpdateConf_>& data)
  : ConstNaiveModelUpdateConf(data) {}
NaiveModelUpdateConf::NaiveModelUpdateConf(const NaiveModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<NaiveModelUpdateConf> resize
NaiveModelUpdateConf::NaiveModelUpdateConf(NaiveModelUpdateConf&&) noexcept = default; 
NaiveModelUpdateConf::NaiveModelUpdateConf(const ::oneflow::NaiveModelUpdateConf& proto_naivemodelupdateconf) {
  InitFromProto(proto_naivemodelupdateconf);
}
NaiveModelUpdateConf::NaiveModelUpdateConf() = default;

NaiveModelUpdateConf::~NaiveModelUpdateConf() = default;

void NaiveModelUpdateConf::InitFromProto(const PbMessage& proto_naivemodelupdateconf) {
  BuildFromProto(proto_naivemodelupdateconf);
}
  
void* NaiveModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool NaiveModelUpdateConf::operator==(const NaiveModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t NaiveModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool NaiveModelUpdateConf::operator<(const NaiveModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void NaiveModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void NaiveModelUpdateConf::CopyFrom(const NaiveModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
NaiveModelUpdateConf& NaiveModelUpdateConf::operator=(const NaiveModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<NaiveModelUpdateConf> NaiveModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<NaiveModelUpdateConf>(__SharedPtr__());
}
ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::_MomentumModelUpdateConf_() { Clear(); }
ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::_MomentumModelUpdateConf_(const _MomentumModelUpdateConf_& other) { CopyFrom(other); }
ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::_MomentumModelUpdateConf_(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf) {
  InitFromProto(proto_momentummodelupdateconf);
}
ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::_MomentumModelUpdateConf_(_MomentumModelUpdateConf_&& other) = default;
ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::~_MomentumModelUpdateConf_() = default;

void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::InitFromProto(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf) {
  Clear();
  // required_or_optional field: beta
  if (proto_momentummodelupdateconf.has_beta()) {
    set_beta(proto_momentummodelupdateconf.beta());
  }
    
}

void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::ToProto(::oneflow::MomentumModelUpdateConf* proto_momentummodelupdateconf) const {
  proto_momentummodelupdateconf->Clear();
  // required_or_optional field: beta
  if (this->has_beta()) {
    proto_momentummodelupdateconf->set_beta(beta());
    }

}

::std::string ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::DebugString() const {
  ::oneflow::MomentumModelUpdateConf proto_momentummodelupdateconf;
  this->ToProto(&proto_momentummodelupdateconf);
  return proto_momentummodelupdateconf.DebugString();
}

void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::Clear() {
  clear_beta();
}

void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::CopyFrom(const _MomentumModelUpdateConf_& other) {
  if (other.has_beta()) {
    set_beta(other.beta());
  } else {
    clear_beta();
  }
}


// optional field beta
bool ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::has_beta() const {
  return has_beta_;
}
const float& ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::beta() const {
  if (has_beta_) { return beta_; }
  static const float default_static_value =
    float(0.8999999761581421);
  return default_static_value;
}
void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::clear_beta() {
  has_beta_ = false;
}
void ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::set_beta(const float& value) {
  beta_ = value;
  has_beta_ = true;
}
float* ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::mutable_beta() {
  has_beta_ = true;
  return &beta_;
}


int ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::compare(const _MomentumModelUpdateConf_& other) {
  if (!(has_beta() == other.has_beta())) {
    return has_beta() < other.has_beta() ? -1 : 1;
  } else if (!(beta() == other.beta())) {
    return beta() < other.beta() ? -1 : 1;
  }
  return 0;
}

bool ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::operator==(const _MomentumModelUpdateConf_& other) const {
  return true
    && has_beta() == other.has_beta() 
    && beta() == other.beta()
  ;
}

std::size_t ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_beta() ? std::hash<float>()(beta()) : 0)
  ;
}

bool ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_::operator<(const _MomentumModelUpdateConf_& other) const {
  return false
    || !(has_beta() == other.has_beta()) ? 
      has_beta() < other.has_beta() : false
    || !(beta() == other.beta()) ? 
      beta() < other.beta() : false
  ;
}

using _MomentumModelUpdateConf_ =  ConstMomentumModelUpdateConf::_MomentumModelUpdateConf_;
ConstMomentumModelUpdateConf::ConstMomentumModelUpdateConf(const ::std::shared_ptr<_MomentumModelUpdateConf_>& data): data_(data) {}
ConstMomentumModelUpdateConf::ConstMomentumModelUpdateConf(): data_(::std::make_shared<_MomentumModelUpdateConf_>()) {}
ConstMomentumModelUpdateConf::ConstMomentumModelUpdateConf(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf) {
  BuildFromProto(proto_momentummodelupdateconf);
}
ConstMomentumModelUpdateConf::ConstMomentumModelUpdateConf(const ConstMomentumModelUpdateConf&) = default;
ConstMomentumModelUpdateConf::ConstMomentumModelUpdateConf(ConstMomentumModelUpdateConf&&) noexcept = default;
ConstMomentumModelUpdateConf::~ConstMomentumModelUpdateConf() = default;

void ConstMomentumModelUpdateConf::ToProto(PbMessage* proto_momentummodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MomentumModelUpdateConf*>(proto_momentummodelupdateconf));
}
  
::std::string ConstMomentumModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMomentumModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstMomentumModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"beta", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMomentumModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMomentumModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMomentumModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &beta();
    default: return nullptr;
  }
}

// required or optional field beta
bool ConstMomentumModelUpdateConf::has_beta() const {
  return __SharedPtrOrDefault__()->has_beta();
}
const float& ConstMomentumModelUpdateConf::beta() const {
  return __SharedPtrOrDefault__()->beta();
}
// used by pybind11 only

::std::shared_ptr<ConstMomentumModelUpdateConf> ConstMomentumModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstMomentumModelUpdateConf>(data_);
}
int64_t ConstMomentumModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMomentumModelUpdateConf::operator==(const ConstMomentumModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMomentumModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMomentumModelUpdateConf::operator<(const ConstMomentumModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MomentumModelUpdateConf_>& ConstMomentumModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MomentumModelUpdateConf_> default_ptr = std::make_shared<_MomentumModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_MomentumModelUpdateConf_>& ConstMomentumModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _MomentumModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMomentumModelUpdateConf
void ConstMomentumModelUpdateConf::BuildFromProto(const PbMessage& proto_momentummodelupdateconf) {
  data_ = ::std::make_shared<_MomentumModelUpdateConf_>(dynamic_cast<const ::oneflow::MomentumModelUpdateConf&>(proto_momentummodelupdateconf));
}

MomentumModelUpdateConf::MomentumModelUpdateConf(const ::std::shared_ptr<_MomentumModelUpdateConf_>& data)
  : ConstMomentumModelUpdateConf(data) {}
MomentumModelUpdateConf::MomentumModelUpdateConf(const MomentumModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MomentumModelUpdateConf> resize
MomentumModelUpdateConf::MomentumModelUpdateConf(MomentumModelUpdateConf&&) noexcept = default; 
MomentumModelUpdateConf::MomentumModelUpdateConf(const ::oneflow::MomentumModelUpdateConf& proto_momentummodelupdateconf) {
  InitFromProto(proto_momentummodelupdateconf);
}
MomentumModelUpdateConf::MomentumModelUpdateConf() = default;

MomentumModelUpdateConf::~MomentumModelUpdateConf() = default;

void MomentumModelUpdateConf::InitFromProto(const PbMessage& proto_momentummodelupdateconf) {
  BuildFromProto(proto_momentummodelupdateconf);
}
  
void* MomentumModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_beta();
    default: return nullptr;
  }
}

bool MomentumModelUpdateConf::operator==(const MomentumModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MomentumModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MomentumModelUpdateConf::operator<(const MomentumModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MomentumModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void MomentumModelUpdateConf::CopyFrom(const MomentumModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MomentumModelUpdateConf& MomentumModelUpdateConf::operator=(const MomentumModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field beta
void MomentumModelUpdateConf::clear_beta() {
  return __SharedPtr__()->clear_beta();
}
void MomentumModelUpdateConf::set_beta(const float& value) {
  return __SharedPtr__()->set_beta(value);
}
float* MomentumModelUpdateConf::mutable_beta() {
  return  __SharedPtr__()->mutable_beta();
}

::std::shared_ptr<MomentumModelUpdateConf> MomentumModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<MomentumModelUpdateConf>(__SharedPtr__());
}
ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::_RMSPropModelUpdateConf_() { Clear(); }
ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::_RMSPropModelUpdateConf_(const _RMSPropModelUpdateConf_& other) { CopyFrom(other); }
ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::_RMSPropModelUpdateConf_(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf) {
  InitFromProto(proto_rmspropmodelupdateconf);
}
ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::_RMSPropModelUpdateConf_(_RMSPropModelUpdateConf_&& other) = default;
ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::~_RMSPropModelUpdateConf_() = default;

void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::InitFromProto(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf) {
  Clear();
  // required_or_optional field: decay_rate
  if (proto_rmspropmodelupdateconf.has_decay_rate()) {
    set_decay_rate(proto_rmspropmodelupdateconf.decay_rate());
  }
  // required_or_optional field: epsilon
  if (proto_rmspropmodelupdateconf.has_epsilon()) {
    set_epsilon(proto_rmspropmodelupdateconf.epsilon());
  }
  // required_or_optional field: centered
  if (proto_rmspropmodelupdateconf.has_centered()) {
    set_centered(proto_rmspropmodelupdateconf.centered());
  }
    
}

void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::ToProto(::oneflow::RMSPropModelUpdateConf* proto_rmspropmodelupdateconf) const {
  proto_rmspropmodelupdateconf->Clear();
  // required_or_optional field: decay_rate
  if (this->has_decay_rate()) {
    proto_rmspropmodelupdateconf->set_decay_rate(decay_rate());
    }
  // required_or_optional field: epsilon
  if (this->has_epsilon()) {
    proto_rmspropmodelupdateconf->set_epsilon(epsilon());
    }
  // required_or_optional field: centered
  if (this->has_centered()) {
    proto_rmspropmodelupdateconf->set_centered(centered());
    }

}

::std::string ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::DebugString() const {
  ::oneflow::RMSPropModelUpdateConf proto_rmspropmodelupdateconf;
  this->ToProto(&proto_rmspropmodelupdateconf);
  return proto_rmspropmodelupdateconf.DebugString();
}

void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::Clear() {
  clear_decay_rate();
  clear_epsilon();
  clear_centered();
}

void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::CopyFrom(const _RMSPropModelUpdateConf_& other) {
  if (other.has_decay_rate()) {
    set_decay_rate(other.decay_rate());
  } else {
    clear_decay_rate();
  }
  if (other.has_epsilon()) {
    set_epsilon(other.epsilon());
  } else {
    clear_epsilon();
  }
  if (other.has_centered()) {
    set_centered(other.centered());
  } else {
    clear_centered();
  }
}


// optional field decay_rate
bool ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::has_decay_rate() const {
  return has_decay_rate_;
}
const float& ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::decay_rate() const {
  if (has_decay_rate_) { return decay_rate_; }
  static const float default_static_value =
    float(0.9900000095367432);
  return default_static_value;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::clear_decay_rate() {
  has_decay_rate_ = false;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::set_decay_rate(const float& value) {
  decay_rate_ = value;
  has_decay_rate_ = true;
}
float* ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::mutable_decay_rate() {
  has_decay_rate_ = true;
  return &decay_rate_;
}

// optional field epsilon
bool ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::has_epsilon() const {
  return has_epsilon_;
}
const float& ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::epsilon() const {
  if (has_epsilon_) { return epsilon_; }
  static const float default_static_value =
    float(9.99999993922529e-09);
  return default_static_value;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::clear_epsilon() {
  has_epsilon_ = false;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::set_epsilon(const float& value) {
  epsilon_ = value;
  has_epsilon_ = true;
}
float* ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::mutable_epsilon() {
  has_epsilon_ = true;
  return &epsilon_;
}

// optional field centered
bool ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::has_centered() const {
  return has_centered_;
}
const bool& ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::centered() const {
  if (has_centered_) { return centered_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::clear_centered() {
  has_centered_ = false;
}
void ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::set_centered(const bool& value) {
  centered_ = value;
  has_centered_ = true;
}
bool* ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::mutable_centered() {
  has_centered_ = true;
  return &centered_;
}


int ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::compare(const _RMSPropModelUpdateConf_& other) {
  if (!(has_decay_rate() == other.has_decay_rate())) {
    return has_decay_rate() < other.has_decay_rate() ? -1 : 1;
  } else if (!(decay_rate() == other.decay_rate())) {
    return decay_rate() < other.decay_rate() ? -1 : 1;
  }
  if (!(has_epsilon() == other.has_epsilon())) {
    return has_epsilon() < other.has_epsilon() ? -1 : 1;
  } else if (!(epsilon() == other.epsilon())) {
    return epsilon() < other.epsilon() ? -1 : 1;
  }
  if (!(has_centered() == other.has_centered())) {
    return has_centered() < other.has_centered() ? -1 : 1;
  } else if (!(centered() == other.centered())) {
    return centered() < other.centered() ? -1 : 1;
  }
  return 0;
}

bool ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::operator==(const _RMSPropModelUpdateConf_& other) const {
  return true
    && has_decay_rate() == other.has_decay_rate() 
    && decay_rate() == other.decay_rate()
    && has_epsilon() == other.has_epsilon() 
    && epsilon() == other.epsilon()
    && has_centered() == other.has_centered() 
    && centered() == other.centered()
  ;
}

std::size_t ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_decay_rate() ? std::hash<float>()(decay_rate()) : 0)
    ^ (has_epsilon() ? std::hash<float>()(epsilon()) : 0)
    ^ (has_centered() ? std::hash<bool>()(centered()) : 0)
  ;
}

bool ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_::operator<(const _RMSPropModelUpdateConf_& other) const {
  return false
    || !(has_decay_rate() == other.has_decay_rate()) ? 
      has_decay_rate() < other.has_decay_rate() : false
    || !(decay_rate() == other.decay_rate()) ? 
      decay_rate() < other.decay_rate() : false
    || !(has_epsilon() == other.has_epsilon()) ? 
      has_epsilon() < other.has_epsilon() : false
    || !(epsilon() == other.epsilon()) ? 
      epsilon() < other.epsilon() : false
    || !(has_centered() == other.has_centered()) ? 
      has_centered() < other.has_centered() : false
    || !(centered() == other.centered()) ? 
      centered() < other.centered() : false
  ;
}

using _RMSPropModelUpdateConf_ =  ConstRMSPropModelUpdateConf::_RMSPropModelUpdateConf_;
ConstRMSPropModelUpdateConf::ConstRMSPropModelUpdateConf(const ::std::shared_ptr<_RMSPropModelUpdateConf_>& data): data_(data) {}
ConstRMSPropModelUpdateConf::ConstRMSPropModelUpdateConf(): data_(::std::make_shared<_RMSPropModelUpdateConf_>()) {}
ConstRMSPropModelUpdateConf::ConstRMSPropModelUpdateConf(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf) {
  BuildFromProto(proto_rmspropmodelupdateconf);
}
ConstRMSPropModelUpdateConf::ConstRMSPropModelUpdateConf(const ConstRMSPropModelUpdateConf&) = default;
ConstRMSPropModelUpdateConf::ConstRMSPropModelUpdateConf(ConstRMSPropModelUpdateConf&&) noexcept = default;
ConstRMSPropModelUpdateConf::~ConstRMSPropModelUpdateConf() = default;

void ConstRMSPropModelUpdateConf::ToProto(PbMessage* proto_rmspropmodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::RMSPropModelUpdateConf*>(proto_rmspropmodelupdateconf));
}
  
::std::string ConstRMSPropModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRMSPropModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstRMSPropModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"decay_rate", 1},
    {"epsilon", 2},
    {"centered", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRMSPropModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRMSPropModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRMSPropModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &decay_rate();
    case 2: return &epsilon();
    case 3: return &centered();
    default: return nullptr;
  }
}

// required or optional field decay_rate
bool ConstRMSPropModelUpdateConf::has_decay_rate() const {
  return __SharedPtrOrDefault__()->has_decay_rate();
}
const float& ConstRMSPropModelUpdateConf::decay_rate() const {
  return __SharedPtrOrDefault__()->decay_rate();
}
// used by pybind11 only
// required or optional field epsilon
bool ConstRMSPropModelUpdateConf::has_epsilon() const {
  return __SharedPtrOrDefault__()->has_epsilon();
}
const float& ConstRMSPropModelUpdateConf::epsilon() const {
  return __SharedPtrOrDefault__()->epsilon();
}
// used by pybind11 only
// required or optional field centered
bool ConstRMSPropModelUpdateConf::has_centered() const {
  return __SharedPtrOrDefault__()->has_centered();
}
const bool& ConstRMSPropModelUpdateConf::centered() const {
  return __SharedPtrOrDefault__()->centered();
}
// used by pybind11 only

::std::shared_ptr<ConstRMSPropModelUpdateConf> ConstRMSPropModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstRMSPropModelUpdateConf>(data_);
}
int64_t ConstRMSPropModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRMSPropModelUpdateConf::operator==(const ConstRMSPropModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRMSPropModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRMSPropModelUpdateConf::operator<(const ConstRMSPropModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RMSPropModelUpdateConf_>& ConstRMSPropModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RMSPropModelUpdateConf_> default_ptr = std::make_shared<_RMSPropModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_RMSPropModelUpdateConf_>& ConstRMSPropModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _RMSPropModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRMSPropModelUpdateConf
void ConstRMSPropModelUpdateConf::BuildFromProto(const PbMessage& proto_rmspropmodelupdateconf) {
  data_ = ::std::make_shared<_RMSPropModelUpdateConf_>(dynamic_cast<const ::oneflow::RMSPropModelUpdateConf&>(proto_rmspropmodelupdateconf));
}

RMSPropModelUpdateConf::RMSPropModelUpdateConf(const ::std::shared_ptr<_RMSPropModelUpdateConf_>& data)
  : ConstRMSPropModelUpdateConf(data) {}
RMSPropModelUpdateConf::RMSPropModelUpdateConf(const RMSPropModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RMSPropModelUpdateConf> resize
RMSPropModelUpdateConf::RMSPropModelUpdateConf(RMSPropModelUpdateConf&&) noexcept = default; 
RMSPropModelUpdateConf::RMSPropModelUpdateConf(const ::oneflow::RMSPropModelUpdateConf& proto_rmspropmodelupdateconf) {
  InitFromProto(proto_rmspropmodelupdateconf);
}
RMSPropModelUpdateConf::RMSPropModelUpdateConf() = default;

RMSPropModelUpdateConf::~RMSPropModelUpdateConf() = default;

void RMSPropModelUpdateConf::InitFromProto(const PbMessage& proto_rmspropmodelupdateconf) {
  BuildFromProto(proto_rmspropmodelupdateconf);
}
  
void* RMSPropModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_decay_rate();
    case 2: return mutable_epsilon();
    case 3: return mutable_centered();
    default: return nullptr;
  }
}

bool RMSPropModelUpdateConf::operator==(const RMSPropModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RMSPropModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RMSPropModelUpdateConf::operator<(const RMSPropModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RMSPropModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void RMSPropModelUpdateConf::CopyFrom(const RMSPropModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RMSPropModelUpdateConf& RMSPropModelUpdateConf::operator=(const RMSPropModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field decay_rate
void RMSPropModelUpdateConf::clear_decay_rate() {
  return __SharedPtr__()->clear_decay_rate();
}
void RMSPropModelUpdateConf::set_decay_rate(const float& value) {
  return __SharedPtr__()->set_decay_rate(value);
}
float* RMSPropModelUpdateConf::mutable_decay_rate() {
  return  __SharedPtr__()->mutable_decay_rate();
}
// required or optional field epsilon
void RMSPropModelUpdateConf::clear_epsilon() {
  return __SharedPtr__()->clear_epsilon();
}
void RMSPropModelUpdateConf::set_epsilon(const float& value) {
  return __SharedPtr__()->set_epsilon(value);
}
float* RMSPropModelUpdateConf::mutable_epsilon() {
  return  __SharedPtr__()->mutable_epsilon();
}
// required or optional field centered
void RMSPropModelUpdateConf::clear_centered() {
  return __SharedPtr__()->clear_centered();
}
void RMSPropModelUpdateConf::set_centered(const bool& value) {
  return __SharedPtr__()->set_centered(value);
}
bool* RMSPropModelUpdateConf::mutable_centered() {
  return  __SharedPtr__()->mutable_centered();
}

::std::shared_ptr<RMSPropModelUpdateConf> RMSPropModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<RMSPropModelUpdateConf>(__SharedPtr__());
}
ConstLARSModelUpdateConf::_LARSModelUpdateConf_::_LARSModelUpdateConf_() { Clear(); }
ConstLARSModelUpdateConf::_LARSModelUpdateConf_::_LARSModelUpdateConf_(const _LARSModelUpdateConf_& other) { CopyFrom(other); }
ConstLARSModelUpdateConf::_LARSModelUpdateConf_::_LARSModelUpdateConf_(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf) {
  InitFromProto(proto_larsmodelupdateconf);
}
ConstLARSModelUpdateConf::_LARSModelUpdateConf_::_LARSModelUpdateConf_(_LARSModelUpdateConf_&& other) = default;
ConstLARSModelUpdateConf::_LARSModelUpdateConf_::~_LARSModelUpdateConf_() = default;

void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::InitFromProto(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf) {
  Clear();
  // required_or_optional field: momentum_beta
  if (proto_larsmodelupdateconf.has_momentum_beta()) {
    set_momentum_beta(proto_larsmodelupdateconf.momentum_beta());
  }
  // required_or_optional field: epsilon
  if (proto_larsmodelupdateconf.has_epsilon()) {
    set_epsilon(proto_larsmodelupdateconf.epsilon());
  }
  // required_or_optional field: lars_coefficient
  if (proto_larsmodelupdateconf.has_lars_coefficient()) {
    set_lars_coefficient(proto_larsmodelupdateconf.lars_coefficient());
  }
    
}

void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::ToProto(::oneflow::LARSModelUpdateConf* proto_larsmodelupdateconf) const {
  proto_larsmodelupdateconf->Clear();
  // required_or_optional field: momentum_beta
  if (this->has_momentum_beta()) {
    proto_larsmodelupdateconf->set_momentum_beta(momentum_beta());
    }
  // required_or_optional field: epsilon
  if (this->has_epsilon()) {
    proto_larsmodelupdateconf->set_epsilon(epsilon());
    }
  // required_or_optional field: lars_coefficient
  if (this->has_lars_coefficient()) {
    proto_larsmodelupdateconf->set_lars_coefficient(lars_coefficient());
    }

}

::std::string ConstLARSModelUpdateConf::_LARSModelUpdateConf_::DebugString() const {
  ::oneflow::LARSModelUpdateConf proto_larsmodelupdateconf;
  this->ToProto(&proto_larsmodelupdateconf);
  return proto_larsmodelupdateconf.DebugString();
}

void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::Clear() {
  clear_momentum_beta();
  clear_epsilon();
  clear_lars_coefficient();
}

void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::CopyFrom(const _LARSModelUpdateConf_& other) {
  if (other.has_momentum_beta()) {
    set_momentum_beta(other.momentum_beta());
  } else {
    clear_momentum_beta();
  }
  if (other.has_epsilon()) {
    set_epsilon(other.epsilon());
  } else {
    clear_epsilon();
  }
  if (other.has_lars_coefficient()) {
    set_lars_coefficient(other.lars_coefficient());
  } else {
    clear_lars_coefficient();
  }
}


// optional field momentum_beta
bool ConstLARSModelUpdateConf::_LARSModelUpdateConf_::has_momentum_beta() const {
  return has_momentum_beta_;
}
const float& ConstLARSModelUpdateConf::_LARSModelUpdateConf_::momentum_beta() const {
  if (has_momentum_beta_) { return momentum_beta_; }
  static const float default_static_value =
    float(0.8999999761581421);
  return default_static_value;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::clear_momentum_beta() {
  has_momentum_beta_ = false;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::set_momentum_beta(const float& value) {
  momentum_beta_ = value;
  has_momentum_beta_ = true;
}
float* ConstLARSModelUpdateConf::_LARSModelUpdateConf_::mutable_momentum_beta() {
  has_momentum_beta_ = true;
  return &momentum_beta_;
}

// optional field epsilon
bool ConstLARSModelUpdateConf::_LARSModelUpdateConf_::has_epsilon() const {
  return has_epsilon_;
}
const float& ConstLARSModelUpdateConf::_LARSModelUpdateConf_::epsilon() const {
  if (has_epsilon_) { return epsilon_; }
  static const float default_static_value =
    float(9.999999717180685e-10);
  return default_static_value;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::clear_epsilon() {
  has_epsilon_ = false;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::set_epsilon(const float& value) {
  epsilon_ = value;
  has_epsilon_ = true;
}
float* ConstLARSModelUpdateConf::_LARSModelUpdateConf_::mutable_epsilon() {
  has_epsilon_ = true;
  return &epsilon_;
}

// optional field lars_coefficient
bool ConstLARSModelUpdateConf::_LARSModelUpdateConf_::has_lars_coefficient() const {
  return has_lars_coefficient_;
}
const float& ConstLARSModelUpdateConf::_LARSModelUpdateConf_::lars_coefficient() const {
  if (has_lars_coefficient_) { return lars_coefficient_; }
  static const float default_static_value =
    float(9.999999747378752e-05);
  return default_static_value;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::clear_lars_coefficient() {
  has_lars_coefficient_ = false;
}
void ConstLARSModelUpdateConf::_LARSModelUpdateConf_::set_lars_coefficient(const float& value) {
  lars_coefficient_ = value;
  has_lars_coefficient_ = true;
}
float* ConstLARSModelUpdateConf::_LARSModelUpdateConf_::mutable_lars_coefficient() {
  has_lars_coefficient_ = true;
  return &lars_coefficient_;
}


int ConstLARSModelUpdateConf::_LARSModelUpdateConf_::compare(const _LARSModelUpdateConf_& other) {
  if (!(has_momentum_beta() == other.has_momentum_beta())) {
    return has_momentum_beta() < other.has_momentum_beta() ? -1 : 1;
  } else if (!(momentum_beta() == other.momentum_beta())) {
    return momentum_beta() < other.momentum_beta() ? -1 : 1;
  }
  if (!(has_epsilon() == other.has_epsilon())) {
    return has_epsilon() < other.has_epsilon() ? -1 : 1;
  } else if (!(epsilon() == other.epsilon())) {
    return epsilon() < other.epsilon() ? -1 : 1;
  }
  if (!(has_lars_coefficient() == other.has_lars_coefficient())) {
    return has_lars_coefficient() < other.has_lars_coefficient() ? -1 : 1;
  } else if (!(lars_coefficient() == other.lars_coefficient())) {
    return lars_coefficient() < other.lars_coefficient() ? -1 : 1;
  }
  return 0;
}

bool ConstLARSModelUpdateConf::_LARSModelUpdateConf_::operator==(const _LARSModelUpdateConf_& other) const {
  return true
    && has_momentum_beta() == other.has_momentum_beta() 
    && momentum_beta() == other.momentum_beta()
    && has_epsilon() == other.has_epsilon() 
    && epsilon() == other.epsilon()
    && has_lars_coefficient() == other.has_lars_coefficient() 
    && lars_coefficient() == other.lars_coefficient()
  ;
}

std::size_t ConstLARSModelUpdateConf::_LARSModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_momentum_beta() ? std::hash<float>()(momentum_beta()) : 0)
    ^ (has_epsilon() ? std::hash<float>()(epsilon()) : 0)
    ^ (has_lars_coefficient() ? std::hash<float>()(lars_coefficient()) : 0)
  ;
}

bool ConstLARSModelUpdateConf::_LARSModelUpdateConf_::operator<(const _LARSModelUpdateConf_& other) const {
  return false
    || !(has_momentum_beta() == other.has_momentum_beta()) ? 
      has_momentum_beta() < other.has_momentum_beta() : false
    || !(momentum_beta() == other.momentum_beta()) ? 
      momentum_beta() < other.momentum_beta() : false
    || !(has_epsilon() == other.has_epsilon()) ? 
      has_epsilon() < other.has_epsilon() : false
    || !(epsilon() == other.epsilon()) ? 
      epsilon() < other.epsilon() : false
    || !(has_lars_coefficient() == other.has_lars_coefficient()) ? 
      has_lars_coefficient() < other.has_lars_coefficient() : false
    || !(lars_coefficient() == other.lars_coefficient()) ? 
      lars_coefficient() < other.lars_coefficient() : false
  ;
}

using _LARSModelUpdateConf_ =  ConstLARSModelUpdateConf::_LARSModelUpdateConf_;
ConstLARSModelUpdateConf::ConstLARSModelUpdateConf(const ::std::shared_ptr<_LARSModelUpdateConf_>& data): data_(data) {}
ConstLARSModelUpdateConf::ConstLARSModelUpdateConf(): data_(::std::make_shared<_LARSModelUpdateConf_>()) {}
ConstLARSModelUpdateConf::ConstLARSModelUpdateConf(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf) {
  BuildFromProto(proto_larsmodelupdateconf);
}
ConstLARSModelUpdateConf::ConstLARSModelUpdateConf(const ConstLARSModelUpdateConf&) = default;
ConstLARSModelUpdateConf::ConstLARSModelUpdateConf(ConstLARSModelUpdateConf&&) noexcept = default;
ConstLARSModelUpdateConf::~ConstLARSModelUpdateConf() = default;

void ConstLARSModelUpdateConf::ToProto(PbMessage* proto_larsmodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LARSModelUpdateConf*>(proto_larsmodelupdateconf));
}
  
::std::string ConstLARSModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLARSModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstLARSModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"momentum_beta", 1},
    {"epsilon", 2},
    {"lars_coefficient", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLARSModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLARSModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLARSModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &momentum_beta();
    case 2: return &epsilon();
    case 3: return &lars_coefficient();
    default: return nullptr;
  }
}

// required or optional field momentum_beta
bool ConstLARSModelUpdateConf::has_momentum_beta() const {
  return __SharedPtrOrDefault__()->has_momentum_beta();
}
const float& ConstLARSModelUpdateConf::momentum_beta() const {
  return __SharedPtrOrDefault__()->momentum_beta();
}
// used by pybind11 only
// required or optional field epsilon
bool ConstLARSModelUpdateConf::has_epsilon() const {
  return __SharedPtrOrDefault__()->has_epsilon();
}
const float& ConstLARSModelUpdateConf::epsilon() const {
  return __SharedPtrOrDefault__()->epsilon();
}
// used by pybind11 only
// required or optional field lars_coefficient
bool ConstLARSModelUpdateConf::has_lars_coefficient() const {
  return __SharedPtrOrDefault__()->has_lars_coefficient();
}
const float& ConstLARSModelUpdateConf::lars_coefficient() const {
  return __SharedPtrOrDefault__()->lars_coefficient();
}
// used by pybind11 only

::std::shared_ptr<ConstLARSModelUpdateConf> ConstLARSModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstLARSModelUpdateConf>(data_);
}
int64_t ConstLARSModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLARSModelUpdateConf::operator==(const ConstLARSModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLARSModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLARSModelUpdateConf::operator<(const ConstLARSModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LARSModelUpdateConf_>& ConstLARSModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LARSModelUpdateConf_> default_ptr = std::make_shared<_LARSModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LARSModelUpdateConf_>& ConstLARSModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LARSModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLARSModelUpdateConf
void ConstLARSModelUpdateConf::BuildFromProto(const PbMessage& proto_larsmodelupdateconf) {
  data_ = ::std::make_shared<_LARSModelUpdateConf_>(dynamic_cast<const ::oneflow::LARSModelUpdateConf&>(proto_larsmodelupdateconf));
}

LARSModelUpdateConf::LARSModelUpdateConf(const ::std::shared_ptr<_LARSModelUpdateConf_>& data)
  : ConstLARSModelUpdateConf(data) {}
LARSModelUpdateConf::LARSModelUpdateConf(const LARSModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LARSModelUpdateConf> resize
LARSModelUpdateConf::LARSModelUpdateConf(LARSModelUpdateConf&&) noexcept = default; 
LARSModelUpdateConf::LARSModelUpdateConf(const ::oneflow::LARSModelUpdateConf& proto_larsmodelupdateconf) {
  InitFromProto(proto_larsmodelupdateconf);
}
LARSModelUpdateConf::LARSModelUpdateConf() = default;

LARSModelUpdateConf::~LARSModelUpdateConf() = default;

void LARSModelUpdateConf::InitFromProto(const PbMessage& proto_larsmodelupdateconf) {
  BuildFromProto(proto_larsmodelupdateconf);
}
  
void* LARSModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_momentum_beta();
    case 2: return mutable_epsilon();
    case 3: return mutable_lars_coefficient();
    default: return nullptr;
  }
}

bool LARSModelUpdateConf::operator==(const LARSModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LARSModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LARSModelUpdateConf::operator<(const LARSModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LARSModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void LARSModelUpdateConf::CopyFrom(const LARSModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LARSModelUpdateConf& LARSModelUpdateConf::operator=(const LARSModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field momentum_beta
void LARSModelUpdateConf::clear_momentum_beta() {
  return __SharedPtr__()->clear_momentum_beta();
}
void LARSModelUpdateConf::set_momentum_beta(const float& value) {
  return __SharedPtr__()->set_momentum_beta(value);
}
float* LARSModelUpdateConf::mutable_momentum_beta() {
  return  __SharedPtr__()->mutable_momentum_beta();
}
// required or optional field epsilon
void LARSModelUpdateConf::clear_epsilon() {
  return __SharedPtr__()->clear_epsilon();
}
void LARSModelUpdateConf::set_epsilon(const float& value) {
  return __SharedPtr__()->set_epsilon(value);
}
float* LARSModelUpdateConf::mutable_epsilon() {
  return  __SharedPtr__()->mutable_epsilon();
}
// required or optional field lars_coefficient
void LARSModelUpdateConf::clear_lars_coefficient() {
  return __SharedPtr__()->clear_lars_coefficient();
}
void LARSModelUpdateConf::set_lars_coefficient(const float& value) {
  return __SharedPtr__()->set_lars_coefficient(value);
}
float* LARSModelUpdateConf::mutable_lars_coefficient() {
  return  __SharedPtr__()->mutable_lars_coefficient();
}

::std::shared_ptr<LARSModelUpdateConf> LARSModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<LARSModelUpdateConf>(__SharedPtr__());
}
ConstAdamModelUpdateConf::_AdamModelUpdateConf_::_AdamModelUpdateConf_() { Clear(); }
ConstAdamModelUpdateConf::_AdamModelUpdateConf_::_AdamModelUpdateConf_(const _AdamModelUpdateConf_& other) { CopyFrom(other); }
ConstAdamModelUpdateConf::_AdamModelUpdateConf_::_AdamModelUpdateConf_(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf) {
  InitFromProto(proto_adammodelupdateconf);
}
ConstAdamModelUpdateConf::_AdamModelUpdateConf_::_AdamModelUpdateConf_(_AdamModelUpdateConf_&& other) = default;
ConstAdamModelUpdateConf::_AdamModelUpdateConf_::~_AdamModelUpdateConf_() = default;

void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::InitFromProto(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf) {
  Clear();
  // required_or_optional field: beta1
  if (proto_adammodelupdateconf.has_beta1()) {
    set_beta1(proto_adammodelupdateconf.beta1());
  }
  // required_or_optional field: beta2
  if (proto_adammodelupdateconf.has_beta2()) {
    set_beta2(proto_adammodelupdateconf.beta2());
  }
  // required_or_optional field: epsilon
  if (proto_adammodelupdateconf.has_epsilon()) {
    set_epsilon(proto_adammodelupdateconf.epsilon());
  }
  // required_or_optional field: do_bias_correction
  if (proto_adammodelupdateconf.has_do_bias_correction()) {
    set_do_bias_correction(proto_adammodelupdateconf.do_bias_correction());
  }
    
}

void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::ToProto(::oneflow::AdamModelUpdateConf* proto_adammodelupdateconf) const {
  proto_adammodelupdateconf->Clear();
  // required_or_optional field: beta1
  if (this->has_beta1()) {
    proto_adammodelupdateconf->set_beta1(beta1());
    }
  // required_or_optional field: beta2
  if (this->has_beta2()) {
    proto_adammodelupdateconf->set_beta2(beta2());
    }
  // required_or_optional field: epsilon
  if (this->has_epsilon()) {
    proto_adammodelupdateconf->set_epsilon(epsilon());
    }
  // required_or_optional field: do_bias_correction
  if (this->has_do_bias_correction()) {
    proto_adammodelupdateconf->set_do_bias_correction(do_bias_correction());
    }

}

::std::string ConstAdamModelUpdateConf::_AdamModelUpdateConf_::DebugString() const {
  ::oneflow::AdamModelUpdateConf proto_adammodelupdateconf;
  this->ToProto(&proto_adammodelupdateconf);
  return proto_adammodelupdateconf.DebugString();
}

void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::Clear() {
  clear_beta1();
  clear_beta2();
  clear_epsilon();
  clear_do_bias_correction();
}

void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::CopyFrom(const _AdamModelUpdateConf_& other) {
  if (other.has_beta1()) {
    set_beta1(other.beta1());
  } else {
    clear_beta1();
  }
  if (other.has_beta2()) {
    set_beta2(other.beta2());
  } else {
    clear_beta2();
  }
  if (other.has_epsilon()) {
    set_epsilon(other.epsilon());
  } else {
    clear_epsilon();
  }
  if (other.has_do_bias_correction()) {
    set_do_bias_correction(other.do_bias_correction());
  } else {
    clear_do_bias_correction();
  }
}


// optional field beta1
bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::has_beta1() const {
  return has_beta1_;
}
const float& ConstAdamModelUpdateConf::_AdamModelUpdateConf_::beta1() const {
  if (has_beta1_) { return beta1_; }
  static const float default_static_value =
    float(0.8999999761581421);
  return default_static_value;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::clear_beta1() {
  has_beta1_ = false;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::set_beta1(const float& value) {
  beta1_ = value;
  has_beta1_ = true;
}
float* ConstAdamModelUpdateConf::_AdamModelUpdateConf_::mutable_beta1() {
  has_beta1_ = true;
  return &beta1_;
}

// optional field beta2
bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::has_beta2() const {
  return has_beta2_;
}
const float& ConstAdamModelUpdateConf::_AdamModelUpdateConf_::beta2() const {
  if (has_beta2_) { return beta2_; }
  static const float default_static_value =
    float(0.9990000128746033);
  return default_static_value;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::clear_beta2() {
  has_beta2_ = false;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::set_beta2(const float& value) {
  beta2_ = value;
  has_beta2_ = true;
}
float* ConstAdamModelUpdateConf::_AdamModelUpdateConf_::mutable_beta2() {
  has_beta2_ = true;
  return &beta2_;
}

// optional field epsilon
bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::has_epsilon() const {
  return has_epsilon_;
}
const float& ConstAdamModelUpdateConf::_AdamModelUpdateConf_::epsilon() const {
  if (has_epsilon_) { return epsilon_; }
  static const float default_static_value =
    float(9.99999993922529e-09);
  return default_static_value;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::clear_epsilon() {
  has_epsilon_ = false;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::set_epsilon(const float& value) {
  epsilon_ = value;
  has_epsilon_ = true;
}
float* ConstAdamModelUpdateConf::_AdamModelUpdateConf_::mutable_epsilon() {
  has_epsilon_ = true;
  return &epsilon_;
}

// optional field do_bias_correction
bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::has_do_bias_correction() const {
  return has_do_bias_correction_;
}
const bool& ConstAdamModelUpdateConf::_AdamModelUpdateConf_::do_bias_correction() const {
  if (has_do_bias_correction_) { return do_bias_correction_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::clear_do_bias_correction() {
  has_do_bias_correction_ = false;
}
void ConstAdamModelUpdateConf::_AdamModelUpdateConf_::set_do_bias_correction(const bool& value) {
  do_bias_correction_ = value;
  has_do_bias_correction_ = true;
}
bool* ConstAdamModelUpdateConf::_AdamModelUpdateConf_::mutable_do_bias_correction() {
  has_do_bias_correction_ = true;
  return &do_bias_correction_;
}


int ConstAdamModelUpdateConf::_AdamModelUpdateConf_::compare(const _AdamModelUpdateConf_& other) {
  if (!(has_beta1() == other.has_beta1())) {
    return has_beta1() < other.has_beta1() ? -1 : 1;
  } else if (!(beta1() == other.beta1())) {
    return beta1() < other.beta1() ? -1 : 1;
  }
  if (!(has_beta2() == other.has_beta2())) {
    return has_beta2() < other.has_beta2() ? -1 : 1;
  } else if (!(beta2() == other.beta2())) {
    return beta2() < other.beta2() ? -1 : 1;
  }
  if (!(has_epsilon() == other.has_epsilon())) {
    return has_epsilon() < other.has_epsilon() ? -1 : 1;
  } else if (!(epsilon() == other.epsilon())) {
    return epsilon() < other.epsilon() ? -1 : 1;
  }
  if (!(has_do_bias_correction() == other.has_do_bias_correction())) {
    return has_do_bias_correction() < other.has_do_bias_correction() ? -1 : 1;
  } else if (!(do_bias_correction() == other.do_bias_correction())) {
    return do_bias_correction() < other.do_bias_correction() ? -1 : 1;
  }
  return 0;
}

bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::operator==(const _AdamModelUpdateConf_& other) const {
  return true
    && has_beta1() == other.has_beta1() 
    && beta1() == other.beta1()
    && has_beta2() == other.has_beta2() 
    && beta2() == other.beta2()
    && has_epsilon() == other.has_epsilon() 
    && epsilon() == other.epsilon()
    && has_do_bias_correction() == other.has_do_bias_correction() 
    && do_bias_correction() == other.do_bias_correction()
  ;
}

std::size_t ConstAdamModelUpdateConf::_AdamModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_beta1() ? std::hash<float>()(beta1()) : 0)
    ^ (has_beta2() ? std::hash<float>()(beta2()) : 0)
    ^ (has_epsilon() ? std::hash<float>()(epsilon()) : 0)
    ^ (has_do_bias_correction() ? std::hash<bool>()(do_bias_correction()) : 0)
  ;
}

bool ConstAdamModelUpdateConf::_AdamModelUpdateConf_::operator<(const _AdamModelUpdateConf_& other) const {
  return false
    || !(has_beta1() == other.has_beta1()) ? 
      has_beta1() < other.has_beta1() : false
    || !(beta1() == other.beta1()) ? 
      beta1() < other.beta1() : false
    || !(has_beta2() == other.has_beta2()) ? 
      has_beta2() < other.has_beta2() : false
    || !(beta2() == other.beta2()) ? 
      beta2() < other.beta2() : false
    || !(has_epsilon() == other.has_epsilon()) ? 
      has_epsilon() < other.has_epsilon() : false
    || !(epsilon() == other.epsilon()) ? 
      epsilon() < other.epsilon() : false
    || !(has_do_bias_correction() == other.has_do_bias_correction()) ? 
      has_do_bias_correction() < other.has_do_bias_correction() : false
    || !(do_bias_correction() == other.do_bias_correction()) ? 
      do_bias_correction() < other.do_bias_correction() : false
  ;
}

using _AdamModelUpdateConf_ =  ConstAdamModelUpdateConf::_AdamModelUpdateConf_;
ConstAdamModelUpdateConf::ConstAdamModelUpdateConf(const ::std::shared_ptr<_AdamModelUpdateConf_>& data): data_(data) {}
ConstAdamModelUpdateConf::ConstAdamModelUpdateConf(): data_(::std::make_shared<_AdamModelUpdateConf_>()) {}
ConstAdamModelUpdateConf::ConstAdamModelUpdateConf(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf) {
  BuildFromProto(proto_adammodelupdateconf);
}
ConstAdamModelUpdateConf::ConstAdamModelUpdateConf(const ConstAdamModelUpdateConf&) = default;
ConstAdamModelUpdateConf::ConstAdamModelUpdateConf(ConstAdamModelUpdateConf&&) noexcept = default;
ConstAdamModelUpdateConf::~ConstAdamModelUpdateConf() = default;

void ConstAdamModelUpdateConf::ToProto(PbMessage* proto_adammodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AdamModelUpdateConf*>(proto_adammodelupdateconf));
}
  
::std::string ConstAdamModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAdamModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstAdamModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"beta1", 1},
    {"beta2", 2},
    {"epsilon", 3},
    {"do_bias_correction", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAdamModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAdamModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAdamModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &beta1();
    case 2: return &beta2();
    case 3: return &epsilon();
    case 4: return &do_bias_correction();
    default: return nullptr;
  }
}

// required or optional field beta1
bool ConstAdamModelUpdateConf::has_beta1() const {
  return __SharedPtrOrDefault__()->has_beta1();
}
const float& ConstAdamModelUpdateConf::beta1() const {
  return __SharedPtrOrDefault__()->beta1();
}
// used by pybind11 only
// required or optional field beta2
bool ConstAdamModelUpdateConf::has_beta2() const {
  return __SharedPtrOrDefault__()->has_beta2();
}
const float& ConstAdamModelUpdateConf::beta2() const {
  return __SharedPtrOrDefault__()->beta2();
}
// used by pybind11 only
// required or optional field epsilon
bool ConstAdamModelUpdateConf::has_epsilon() const {
  return __SharedPtrOrDefault__()->has_epsilon();
}
const float& ConstAdamModelUpdateConf::epsilon() const {
  return __SharedPtrOrDefault__()->epsilon();
}
// used by pybind11 only
// required or optional field do_bias_correction
bool ConstAdamModelUpdateConf::has_do_bias_correction() const {
  return __SharedPtrOrDefault__()->has_do_bias_correction();
}
const bool& ConstAdamModelUpdateConf::do_bias_correction() const {
  return __SharedPtrOrDefault__()->do_bias_correction();
}
// used by pybind11 only

::std::shared_ptr<ConstAdamModelUpdateConf> ConstAdamModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstAdamModelUpdateConf>(data_);
}
int64_t ConstAdamModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAdamModelUpdateConf::operator==(const ConstAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAdamModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAdamModelUpdateConf::operator<(const ConstAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AdamModelUpdateConf_>& ConstAdamModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AdamModelUpdateConf_> default_ptr = std::make_shared<_AdamModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_AdamModelUpdateConf_>& ConstAdamModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _AdamModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAdamModelUpdateConf
void ConstAdamModelUpdateConf::BuildFromProto(const PbMessage& proto_adammodelupdateconf) {
  data_ = ::std::make_shared<_AdamModelUpdateConf_>(dynamic_cast<const ::oneflow::AdamModelUpdateConf&>(proto_adammodelupdateconf));
}

AdamModelUpdateConf::AdamModelUpdateConf(const ::std::shared_ptr<_AdamModelUpdateConf_>& data)
  : ConstAdamModelUpdateConf(data) {}
AdamModelUpdateConf::AdamModelUpdateConf(const AdamModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AdamModelUpdateConf> resize
AdamModelUpdateConf::AdamModelUpdateConf(AdamModelUpdateConf&&) noexcept = default; 
AdamModelUpdateConf::AdamModelUpdateConf(const ::oneflow::AdamModelUpdateConf& proto_adammodelupdateconf) {
  InitFromProto(proto_adammodelupdateconf);
}
AdamModelUpdateConf::AdamModelUpdateConf() = default;

AdamModelUpdateConf::~AdamModelUpdateConf() = default;

void AdamModelUpdateConf::InitFromProto(const PbMessage& proto_adammodelupdateconf) {
  BuildFromProto(proto_adammodelupdateconf);
}
  
void* AdamModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_beta1();
    case 2: return mutable_beta2();
    case 3: return mutable_epsilon();
    case 4: return mutable_do_bias_correction();
    default: return nullptr;
  }
}

bool AdamModelUpdateConf::operator==(const AdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AdamModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AdamModelUpdateConf::operator<(const AdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AdamModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void AdamModelUpdateConf::CopyFrom(const AdamModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AdamModelUpdateConf& AdamModelUpdateConf::operator=(const AdamModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field beta1
void AdamModelUpdateConf::clear_beta1() {
  return __SharedPtr__()->clear_beta1();
}
void AdamModelUpdateConf::set_beta1(const float& value) {
  return __SharedPtr__()->set_beta1(value);
}
float* AdamModelUpdateConf::mutable_beta1() {
  return  __SharedPtr__()->mutable_beta1();
}
// required or optional field beta2
void AdamModelUpdateConf::clear_beta2() {
  return __SharedPtr__()->clear_beta2();
}
void AdamModelUpdateConf::set_beta2(const float& value) {
  return __SharedPtr__()->set_beta2(value);
}
float* AdamModelUpdateConf::mutable_beta2() {
  return  __SharedPtr__()->mutable_beta2();
}
// required or optional field epsilon
void AdamModelUpdateConf::clear_epsilon() {
  return __SharedPtr__()->clear_epsilon();
}
void AdamModelUpdateConf::set_epsilon(const float& value) {
  return __SharedPtr__()->set_epsilon(value);
}
float* AdamModelUpdateConf::mutable_epsilon() {
  return  __SharedPtr__()->mutable_epsilon();
}
// required or optional field do_bias_correction
void AdamModelUpdateConf::clear_do_bias_correction() {
  return __SharedPtr__()->clear_do_bias_correction();
}
void AdamModelUpdateConf::set_do_bias_correction(const bool& value) {
  return __SharedPtr__()->set_do_bias_correction(value);
}
bool* AdamModelUpdateConf::mutable_do_bias_correction() {
  return  __SharedPtr__()->mutable_do_bias_correction();
}

::std::shared_ptr<AdamModelUpdateConf> AdamModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<AdamModelUpdateConf>(__SharedPtr__());
}
ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::_LazyAdamModelUpdateConf_() { Clear(); }
ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::_LazyAdamModelUpdateConf_(const _LazyAdamModelUpdateConf_& other) { CopyFrom(other); }
ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::_LazyAdamModelUpdateConf_(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf) {
  InitFromProto(proto_lazyadammodelupdateconf);
}
ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::_LazyAdamModelUpdateConf_(_LazyAdamModelUpdateConf_&& other) = default;
ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::~_LazyAdamModelUpdateConf_() = default;

void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::InitFromProto(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf) {
  Clear();
  // required_or_optional field: beta1
  if (proto_lazyadammodelupdateconf.has_beta1()) {
    set_beta1(proto_lazyadammodelupdateconf.beta1());
  }
  // required_or_optional field: beta2
  if (proto_lazyadammodelupdateconf.has_beta2()) {
    set_beta2(proto_lazyadammodelupdateconf.beta2());
  }
  // required_or_optional field: epsilon
  if (proto_lazyadammodelupdateconf.has_epsilon()) {
    set_epsilon(proto_lazyadammodelupdateconf.epsilon());
  }
    
}

void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::ToProto(::oneflow::LazyAdamModelUpdateConf* proto_lazyadammodelupdateconf) const {
  proto_lazyadammodelupdateconf->Clear();
  // required_or_optional field: beta1
  if (this->has_beta1()) {
    proto_lazyadammodelupdateconf->set_beta1(beta1());
    }
  // required_or_optional field: beta2
  if (this->has_beta2()) {
    proto_lazyadammodelupdateconf->set_beta2(beta2());
    }
  // required_or_optional field: epsilon
  if (this->has_epsilon()) {
    proto_lazyadammodelupdateconf->set_epsilon(epsilon());
    }

}

::std::string ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::DebugString() const {
  ::oneflow::LazyAdamModelUpdateConf proto_lazyadammodelupdateconf;
  this->ToProto(&proto_lazyadammodelupdateconf);
  return proto_lazyadammodelupdateconf.DebugString();
}

void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::Clear() {
  clear_beta1();
  clear_beta2();
  clear_epsilon();
}

void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::CopyFrom(const _LazyAdamModelUpdateConf_& other) {
  if (other.has_beta1()) {
    set_beta1(other.beta1());
  } else {
    clear_beta1();
  }
  if (other.has_beta2()) {
    set_beta2(other.beta2());
  } else {
    clear_beta2();
  }
  if (other.has_epsilon()) {
    set_epsilon(other.epsilon());
  } else {
    clear_epsilon();
  }
}


// optional field beta1
bool ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::has_beta1() const {
  return has_beta1_;
}
const float& ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::beta1() const {
  if (has_beta1_) { return beta1_; }
  static const float default_static_value =
    float(0.8999999761581421);
  return default_static_value;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::clear_beta1() {
  has_beta1_ = false;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::set_beta1(const float& value) {
  beta1_ = value;
  has_beta1_ = true;
}
float* ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::mutable_beta1() {
  has_beta1_ = true;
  return &beta1_;
}

// optional field beta2
bool ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::has_beta2() const {
  return has_beta2_;
}
const float& ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::beta2() const {
  if (has_beta2_) { return beta2_; }
  static const float default_static_value =
    float(0.9990000128746033);
  return default_static_value;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::clear_beta2() {
  has_beta2_ = false;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::set_beta2(const float& value) {
  beta2_ = value;
  has_beta2_ = true;
}
float* ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::mutable_beta2() {
  has_beta2_ = true;
  return &beta2_;
}

// optional field epsilon
bool ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::has_epsilon() const {
  return has_epsilon_;
}
const float& ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::epsilon() const {
  if (has_epsilon_) { return epsilon_; }
  static const float default_static_value =
    float(9.99999993922529e-09);
  return default_static_value;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::clear_epsilon() {
  has_epsilon_ = false;
}
void ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::set_epsilon(const float& value) {
  epsilon_ = value;
  has_epsilon_ = true;
}
float* ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::mutable_epsilon() {
  has_epsilon_ = true;
  return &epsilon_;
}


int ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::compare(const _LazyAdamModelUpdateConf_& other) {
  if (!(has_beta1() == other.has_beta1())) {
    return has_beta1() < other.has_beta1() ? -1 : 1;
  } else if (!(beta1() == other.beta1())) {
    return beta1() < other.beta1() ? -1 : 1;
  }
  if (!(has_beta2() == other.has_beta2())) {
    return has_beta2() < other.has_beta2() ? -1 : 1;
  } else if (!(beta2() == other.beta2())) {
    return beta2() < other.beta2() ? -1 : 1;
  }
  if (!(has_epsilon() == other.has_epsilon())) {
    return has_epsilon() < other.has_epsilon() ? -1 : 1;
  } else if (!(epsilon() == other.epsilon())) {
    return epsilon() < other.epsilon() ? -1 : 1;
  }
  return 0;
}

bool ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::operator==(const _LazyAdamModelUpdateConf_& other) const {
  return true
    && has_beta1() == other.has_beta1() 
    && beta1() == other.beta1()
    && has_beta2() == other.has_beta2() 
    && beta2() == other.beta2()
    && has_epsilon() == other.has_epsilon() 
    && epsilon() == other.epsilon()
  ;
}

std::size_t ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_beta1() ? std::hash<float>()(beta1()) : 0)
    ^ (has_beta2() ? std::hash<float>()(beta2()) : 0)
    ^ (has_epsilon() ? std::hash<float>()(epsilon()) : 0)
  ;
}

bool ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_::operator<(const _LazyAdamModelUpdateConf_& other) const {
  return false
    || !(has_beta1() == other.has_beta1()) ? 
      has_beta1() < other.has_beta1() : false
    || !(beta1() == other.beta1()) ? 
      beta1() < other.beta1() : false
    || !(has_beta2() == other.has_beta2()) ? 
      has_beta2() < other.has_beta2() : false
    || !(beta2() == other.beta2()) ? 
      beta2() < other.beta2() : false
    || !(has_epsilon() == other.has_epsilon()) ? 
      has_epsilon() < other.has_epsilon() : false
    || !(epsilon() == other.epsilon()) ? 
      epsilon() < other.epsilon() : false
  ;
}

using _LazyAdamModelUpdateConf_ =  ConstLazyAdamModelUpdateConf::_LazyAdamModelUpdateConf_;
ConstLazyAdamModelUpdateConf::ConstLazyAdamModelUpdateConf(const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& data): data_(data) {}
ConstLazyAdamModelUpdateConf::ConstLazyAdamModelUpdateConf(): data_(::std::make_shared<_LazyAdamModelUpdateConf_>()) {}
ConstLazyAdamModelUpdateConf::ConstLazyAdamModelUpdateConf(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf) {
  BuildFromProto(proto_lazyadammodelupdateconf);
}
ConstLazyAdamModelUpdateConf::ConstLazyAdamModelUpdateConf(const ConstLazyAdamModelUpdateConf&) = default;
ConstLazyAdamModelUpdateConf::ConstLazyAdamModelUpdateConf(ConstLazyAdamModelUpdateConf&&) noexcept = default;
ConstLazyAdamModelUpdateConf::~ConstLazyAdamModelUpdateConf() = default;

void ConstLazyAdamModelUpdateConf::ToProto(PbMessage* proto_lazyadammodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LazyAdamModelUpdateConf*>(proto_lazyadammodelupdateconf));
}
  
::std::string ConstLazyAdamModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLazyAdamModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstLazyAdamModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"beta1", 1},
    {"beta2", 2},
    {"epsilon", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLazyAdamModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLazyAdamModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLazyAdamModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &beta1();
    case 2: return &beta2();
    case 3: return &epsilon();
    default: return nullptr;
  }
}

// required or optional field beta1
bool ConstLazyAdamModelUpdateConf::has_beta1() const {
  return __SharedPtrOrDefault__()->has_beta1();
}
const float& ConstLazyAdamModelUpdateConf::beta1() const {
  return __SharedPtrOrDefault__()->beta1();
}
// used by pybind11 only
// required or optional field beta2
bool ConstLazyAdamModelUpdateConf::has_beta2() const {
  return __SharedPtrOrDefault__()->has_beta2();
}
const float& ConstLazyAdamModelUpdateConf::beta2() const {
  return __SharedPtrOrDefault__()->beta2();
}
// used by pybind11 only
// required or optional field epsilon
bool ConstLazyAdamModelUpdateConf::has_epsilon() const {
  return __SharedPtrOrDefault__()->has_epsilon();
}
const float& ConstLazyAdamModelUpdateConf::epsilon() const {
  return __SharedPtrOrDefault__()->epsilon();
}
// used by pybind11 only

::std::shared_ptr<ConstLazyAdamModelUpdateConf> ConstLazyAdamModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstLazyAdamModelUpdateConf>(data_);
}
int64_t ConstLazyAdamModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLazyAdamModelUpdateConf::operator==(const ConstLazyAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLazyAdamModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLazyAdamModelUpdateConf::operator<(const ConstLazyAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& ConstLazyAdamModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LazyAdamModelUpdateConf_> default_ptr = std::make_shared<_LazyAdamModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& ConstLazyAdamModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LazyAdamModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLazyAdamModelUpdateConf
void ConstLazyAdamModelUpdateConf::BuildFromProto(const PbMessage& proto_lazyadammodelupdateconf) {
  data_ = ::std::make_shared<_LazyAdamModelUpdateConf_>(dynamic_cast<const ::oneflow::LazyAdamModelUpdateConf&>(proto_lazyadammodelupdateconf));
}

LazyAdamModelUpdateConf::LazyAdamModelUpdateConf(const ::std::shared_ptr<_LazyAdamModelUpdateConf_>& data)
  : ConstLazyAdamModelUpdateConf(data) {}
LazyAdamModelUpdateConf::LazyAdamModelUpdateConf(const LazyAdamModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LazyAdamModelUpdateConf> resize
LazyAdamModelUpdateConf::LazyAdamModelUpdateConf(LazyAdamModelUpdateConf&&) noexcept = default; 
LazyAdamModelUpdateConf::LazyAdamModelUpdateConf(const ::oneflow::LazyAdamModelUpdateConf& proto_lazyadammodelupdateconf) {
  InitFromProto(proto_lazyadammodelupdateconf);
}
LazyAdamModelUpdateConf::LazyAdamModelUpdateConf() = default;

LazyAdamModelUpdateConf::~LazyAdamModelUpdateConf() = default;

void LazyAdamModelUpdateConf::InitFromProto(const PbMessage& proto_lazyadammodelupdateconf) {
  BuildFromProto(proto_lazyadammodelupdateconf);
}
  
void* LazyAdamModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_beta1();
    case 2: return mutable_beta2();
    case 3: return mutable_epsilon();
    default: return nullptr;
  }
}

bool LazyAdamModelUpdateConf::operator==(const LazyAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LazyAdamModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LazyAdamModelUpdateConf::operator<(const LazyAdamModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LazyAdamModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void LazyAdamModelUpdateConf::CopyFrom(const LazyAdamModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LazyAdamModelUpdateConf& LazyAdamModelUpdateConf::operator=(const LazyAdamModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field beta1
void LazyAdamModelUpdateConf::clear_beta1() {
  return __SharedPtr__()->clear_beta1();
}
void LazyAdamModelUpdateConf::set_beta1(const float& value) {
  return __SharedPtr__()->set_beta1(value);
}
float* LazyAdamModelUpdateConf::mutable_beta1() {
  return  __SharedPtr__()->mutable_beta1();
}
// required or optional field beta2
void LazyAdamModelUpdateConf::clear_beta2() {
  return __SharedPtr__()->clear_beta2();
}
void LazyAdamModelUpdateConf::set_beta2(const float& value) {
  return __SharedPtr__()->set_beta2(value);
}
float* LazyAdamModelUpdateConf::mutable_beta2() {
  return  __SharedPtr__()->mutable_beta2();
}
// required or optional field epsilon
void LazyAdamModelUpdateConf::clear_epsilon() {
  return __SharedPtr__()->clear_epsilon();
}
void LazyAdamModelUpdateConf::set_epsilon(const float& value) {
  return __SharedPtr__()->set_epsilon(value);
}
float* LazyAdamModelUpdateConf::mutable_epsilon() {
  return  __SharedPtr__()->mutable_epsilon();
}

::std::shared_ptr<LazyAdamModelUpdateConf> LazyAdamModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<LazyAdamModelUpdateConf>(__SharedPtr__());
}
ConstLambModelUpdateConf::_LambModelUpdateConf_::_LambModelUpdateConf_() { Clear(); }
ConstLambModelUpdateConf::_LambModelUpdateConf_::_LambModelUpdateConf_(const _LambModelUpdateConf_& other) { CopyFrom(other); }
ConstLambModelUpdateConf::_LambModelUpdateConf_::_LambModelUpdateConf_(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf) {
  InitFromProto(proto_lambmodelupdateconf);
}
ConstLambModelUpdateConf::_LambModelUpdateConf_::_LambModelUpdateConf_(_LambModelUpdateConf_&& other) = default;
ConstLambModelUpdateConf::_LambModelUpdateConf_::~_LambModelUpdateConf_() = default;

void ConstLambModelUpdateConf::_LambModelUpdateConf_::InitFromProto(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf) {
  Clear();
  // required_or_optional field: beta1
  if (proto_lambmodelupdateconf.has_beta1()) {
    set_beta1(proto_lambmodelupdateconf.beta1());
  }
  // required_or_optional field: beta2
  if (proto_lambmodelupdateconf.has_beta2()) {
    set_beta2(proto_lambmodelupdateconf.beta2());
  }
  // required_or_optional field: epsilon
  if (proto_lambmodelupdateconf.has_epsilon()) {
    set_epsilon(proto_lambmodelupdateconf.epsilon());
  }
    
}

void ConstLambModelUpdateConf::_LambModelUpdateConf_::ToProto(::oneflow::LambModelUpdateConf* proto_lambmodelupdateconf) const {
  proto_lambmodelupdateconf->Clear();
  // required_or_optional field: beta1
  if (this->has_beta1()) {
    proto_lambmodelupdateconf->set_beta1(beta1());
    }
  // required_or_optional field: beta2
  if (this->has_beta2()) {
    proto_lambmodelupdateconf->set_beta2(beta2());
    }
  // required_or_optional field: epsilon
  if (this->has_epsilon()) {
    proto_lambmodelupdateconf->set_epsilon(epsilon());
    }

}

::std::string ConstLambModelUpdateConf::_LambModelUpdateConf_::DebugString() const {
  ::oneflow::LambModelUpdateConf proto_lambmodelupdateconf;
  this->ToProto(&proto_lambmodelupdateconf);
  return proto_lambmodelupdateconf.DebugString();
}

void ConstLambModelUpdateConf::_LambModelUpdateConf_::Clear() {
  clear_beta1();
  clear_beta2();
  clear_epsilon();
}

void ConstLambModelUpdateConf::_LambModelUpdateConf_::CopyFrom(const _LambModelUpdateConf_& other) {
  if (other.has_beta1()) {
    set_beta1(other.beta1());
  } else {
    clear_beta1();
  }
  if (other.has_beta2()) {
    set_beta2(other.beta2());
  } else {
    clear_beta2();
  }
  if (other.has_epsilon()) {
    set_epsilon(other.epsilon());
  } else {
    clear_epsilon();
  }
}


// optional field beta1
bool ConstLambModelUpdateConf::_LambModelUpdateConf_::has_beta1() const {
  return has_beta1_;
}
const float& ConstLambModelUpdateConf::_LambModelUpdateConf_::beta1() const {
  if (has_beta1_) { return beta1_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::clear_beta1() {
  has_beta1_ = false;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::set_beta1(const float& value) {
  beta1_ = value;
  has_beta1_ = true;
}
float* ConstLambModelUpdateConf::_LambModelUpdateConf_::mutable_beta1() {
  has_beta1_ = true;
  return &beta1_;
}

// optional field beta2
bool ConstLambModelUpdateConf::_LambModelUpdateConf_::has_beta2() const {
  return has_beta2_;
}
const float& ConstLambModelUpdateConf::_LambModelUpdateConf_::beta2() const {
  if (has_beta2_) { return beta2_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::clear_beta2() {
  has_beta2_ = false;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::set_beta2(const float& value) {
  beta2_ = value;
  has_beta2_ = true;
}
float* ConstLambModelUpdateConf::_LambModelUpdateConf_::mutable_beta2() {
  has_beta2_ = true;
  return &beta2_;
}

// optional field epsilon
bool ConstLambModelUpdateConf::_LambModelUpdateConf_::has_epsilon() const {
  return has_epsilon_;
}
const float& ConstLambModelUpdateConf::_LambModelUpdateConf_::epsilon() const {
  if (has_epsilon_) { return epsilon_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::clear_epsilon() {
  has_epsilon_ = false;
}
void ConstLambModelUpdateConf::_LambModelUpdateConf_::set_epsilon(const float& value) {
  epsilon_ = value;
  has_epsilon_ = true;
}
float* ConstLambModelUpdateConf::_LambModelUpdateConf_::mutable_epsilon() {
  has_epsilon_ = true;
  return &epsilon_;
}


int ConstLambModelUpdateConf::_LambModelUpdateConf_::compare(const _LambModelUpdateConf_& other) {
  if (!(has_beta1() == other.has_beta1())) {
    return has_beta1() < other.has_beta1() ? -1 : 1;
  } else if (!(beta1() == other.beta1())) {
    return beta1() < other.beta1() ? -1 : 1;
  }
  if (!(has_beta2() == other.has_beta2())) {
    return has_beta2() < other.has_beta2() ? -1 : 1;
  } else if (!(beta2() == other.beta2())) {
    return beta2() < other.beta2() ? -1 : 1;
  }
  if (!(has_epsilon() == other.has_epsilon())) {
    return has_epsilon() < other.has_epsilon() ? -1 : 1;
  } else if (!(epsilon() == other.epsilon())) {
    return epsilon() < other.epsilon() ? -1 : 1;
  }
  return 0;
}

bool ConstLambModelUpdateConf::_LambModelUpdateConf_::operator==(const _LambModelUpdateConf_& other) const {
  return true
    && has_beta1() == other.has_beta1() 
    && beta1() == other.beta1()
    && has_beta2() == other.has_beta2() 
    && beta2() == other.beta2()
    && has_epsilon() == other.has_epsilon() 
    && epsilon() == other.epsilon()
  ;
}

std::size_t ConstLambModelUpdateConf::_LambModelUpdateConf_::__CalcHash__() const {
  return 0
    ^ (has_beta1() ? std::hash<float>()(beta1()) : 0)
    ^ (has_beta2() ? std::hash<float>()(beta2()) : 0)
    ^ (has_epsilon() ? std::hash<float>()(epsilon()) : 0)
  ;
}

bool ConstLambModelUpdateConf::_LambModelUpdateConf_::operator<(const _LambModelUpdateConf_& other) const {
  return false
    || !(has_beta1() == other.has_beta1()) ? 
      has_beta1() < other.has_beta1() : false
    || !(beta1() == other.beta1()) ? 
      beta1() < other.beta1() : false
    || !(has_beta2() == other.has_beta2()) ? 
      has_beta2() < other.has_beta2() : false
    || !(beta2() == other.beta2()) ? 
      beta2() < other.beta2() : false
    || !(has_epsilon() == other.has_epsilon()) ? 
      has_epsilon() < other.has_epsilon() : false
    || !(epsilon() == other.epsilon()) ? 
      epsilon() < other.epsilon() : false
  ;
}

using _LambModelUpdateConf_ =  ConstLambModelUpdateConf::_LambModelUpdateConf_;
ConstLambModelUpdateConf::ConstLambModelUpdateConf(const ::std::shared_ptr<_LambModelUpdateConf_>& data): data_(data) {}
ConstLambModelUpdateConf::ConstLambModelUpdateConf(): data_(::std::make_shared<_LambModelUpdateConf_>()) {}
ConstLambModelUpdateConf::ConstLambModelUpdateConf(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf) {
  BuildFromProto(proto_lambmodelupdateconf);
}
ConstLambModelUpdateConf::ConstLambModelUpdateConf(const ConstLambModelUpdateConf&) = default;
ConstLambModelUpdateConf::ConstLambModelUpdateConf(ConstLambModelUpdateConf&&) noexcept = default;
ConstLambModelUpdateConf::~ConstLambModelUpdateConf() = default;

void ConstLambModelUpdateConf::ToProto(PbMessage* proto_lambmodelupdateconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::LambModelUpdateConf*>(proto_lambmodelupdateconf));
}
  
::std::string ConstLambModelUpdateConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstLambModelUpdateConf::__Empty__() const {
  return !data_;
}

int ConstLambModelUpdateConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"beta1", 1},
    {"beta2", 2},
    {"epsilon", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstLambModelUpdateConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstLambModelUpdateConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstLambModelUpdateConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &beta1();
    case 2: return &beta2();
    case 3: return &epsilon();
    default: return nullptr;
  }
}

// required or optional field beta1
bool ConstLambModelUpdateConf::has_beta1() const {
  return __SharedPtrOrDefault__()->has_beta1();
}
const float& ConstLambModelUpdateConf::beta1() const {
  return __SharedPtrOrDefault__()->beta1();
}
// used by pybind11 only
// required or optional field beta2
bool ConstLambModelUpdateConf::has_beta2() const {
  return __SharedPtrOrDefault__()->has_beta2();
}
const float& ConstLambModelUpdateConf::beta2() const {
  return __SharedPtrOrDefault__()->beta2();
}
// used by pybind11 only
// required or optional field epsilon
bool ConstLambModelUpdateConf::has_epsilon() const {
  return __SharedPtrOrDefault__()->has_epsilon();
}
const float& ConstLambModelUpdateConf::epsilon() const {
  return __SharedPtrOrDefault__()->epsilon();
}
// used by pybind11 only

::std::shared_ptr<ConstLambModelUpdateConf> ConstLambModelUpdateConf::__SharedConst__() const {
  return ::std::make_shared<ConstLambModelUpdateConf>(data_);
}
int64_t ConstLambModelUpdateConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstLambModelUpdateConf::operator==(const ConstLambModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstLambModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstLambModelUpdateConf::operator<(const ConstLambModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_LambModelUpdateConf_>& ConstLambModelUpdateConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_LambModelUpdateConf_> default_ptr = std::make_shared<_LambModelUpdateConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_LambModelUpdateConf_>& ConstLambModelUpdateConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _LambModelUpdateConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstLambModelUpdateConf
void ConstLambModelUpdateConf::BuildFromProto(const PbMessage& proto_lambmodelupdateconf) {
  data_ = ::std::make_shared<_LambModelUpdateConf_>(dynamic_cast<const ::oneflow::LambModelUpdateConf&>(proto_lambmodelupdateconf));
}

LambModelUpdateConf::LambModelUpdateConf(const ::std::shared_ptr<_LambModelUpdateConf_>& data)
  : ConstLambModelUpdateConf(data) {}
LambModelUpdateConf::LambModelUpdateConf(const LambModelUpdateConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<LambModelUpdateConf> resize
LambModelUpdateConf::LambModelUpdateConf(LambModelUpdateConf&&) noexcept = default; 
LambModelUpdateConf::LambModelUpdateConf(const ::oneflow::LambModelUpdateConf& proto_lambmodelupdateconf) {
  InitFromProto(proto_lambmodelupdateconf);
}
LambModelUpdateConf::LambModelUpdateConf() = default;

LambModelUpdateConf::~LambModelUpdateConf() = default;

void LambModelUpdateConf::InitFromProto(const PbMessage& proto_lambmodelupdateconf) {
  BuildFromProto(proto_lambmodelupdateconf);
}
  
void* LambModelUpdateConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_beta1();
    case 2: return mutable_beta2();
    case 3: return mutable_epsilon();
    default: return nullptr;
  }
}

bool LambModelUpdateConf::operator==(const LambModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t LambModelUpdateConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool LambModelUpdateConf::operator<(const LambModelUpdateConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void LambModelUpdateConf::Clear() {
  if (data_) { data_.reset(); }
}
void LambModelUpdateConf::CopyFrom(const LambModelUpdateConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
LambModelUpdateConf& LambModelUpdateConf::operator=(const LambModelUpdateConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field beta1
void LambModelUpdateConf::clear_beta1() {
  return __SharedPtr__()->clear_beta1();
}
void LambModelUpdateConf::set_beta1(const float& value) {
  return __SharedPtr__()->set_beta1(value);
}
float* LambModelUpdateConf::mutable_beta1() {
  return  __SharedPtr__()->mutable_beta1();
}
// required or optional field beta2
void LambModelUpdateConf::clear_beta2() {
  return __SharedPtr__()->clear_beta2();
}
void LambModelUpdateConf::set_beta2(const float& value) {
  return __SharedPtr__()->set_beta2(value);
}
float* LambModelUpdateConf::mutable_beta2() {
  return  __SharedPtr__()->mutable_beta2();
}
// required or optional field epsilon
void LambModelUpdateConf::clear_epsilon() {
  return __SharedPtr__()->clear_epsilon();
}
void LambModelUpdateConf::set_epsilon(const float& value) {
  return __SharedPtr__()->set_epsilon(value);
}
float* LambModelUpdateConf::mutable_epsilon() {
  return  __SharedPtr__()->mutable_epsilon();
}

::std::shared_ptr<LambModelUpdateConf> LambModelUpdateConf::__SharedMutable__() {
  return ::std::make_shared<LambModelUpdateConf>(__SharedPtr__());
}
ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::_ClipByGlobalNormConf_() { Clear(); }
ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::_ClipByGlobalNormConf_(const _ClipByGlobalNormConf_& other) { CopyFrom(other); }
ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::_ClipByGlobalNormConf_(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf) {
  InitFromProto(proto_clipbyglobalnormconf);
}
ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::_ClipByGlobalNormConf_(_ClipByGlobalNormConf_&& other) = default;
ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::~_ClipByGlobalNormConf_() = default;

void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::InitFromProto(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf) {
  Clear();
  // required_or_optional field: clip_norm
  if (proto_clipbyglobalnormconf.has_clip_norm()) {
    set_clip_norm(proto_clipbyglobalnormconf.clip_norm());
  }
  // required_or_optional field: global_norm
  if (proto_clipbyglobalnormconf.has_global_norm()) {
    set_global_norm(proto_clipbyglobalnormconf.global_norm());
  }
    
}

void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::ToProto(::oneflow::ClipByGlobalNormConf* proto_clipbyglobalnormconf) const {
  proto_clipbyglobalnormconf->Clear();
  // required_or_optional field: clip_norm
  if (this->has_clip_norm()) {
    proto_clipbyglobalnormconf->set_clip_norm(clip_norm());
    }
  // required_or_optional field: global_norm
  if (this->has_global_norm()) {
    proto_clipbyglobalnormconf->set_global_norm(global_norm());
    }

}

::std::string ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::DebugString() const {
  ::oneflow::ClipByGlobalNormConf proto_clipbyglobalnormconf;
  this->ToProto(&proto_clipbyglobalnormconf);
  return proto_clipbyglobalnormconf.DebugString();
}

void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::Clear() {
  clear_clip_norm();
  clear_global_norm();
}

void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::CopyFrom(const _ClipByGlobalNormConf_& other) {
  if (other.has_clip_norm()) {
    set_clip_norm(other.clip_norm());
  } else {
    clear_clip_norm();
  }
  if (other.has_global_norm()) {
    set_global_norm(other.global_norm());
  } else {
    clear_global_norm();
  }
}


// optional field clip_norm
bool ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::has_clip_norm() const {
  return has_clip_norm_;
}
const float& ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::clip_norm() const {
  if (has_clip_norm_) { return clip_norm_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::clear_clip_norm() {
  has_clip_norm_ = false;
}
void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::set_clip_norm(const float& value) {
  clip_norm_ = value;
  has_clip_norm_ = true;
}
float* ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::mutable_clip_norm() {
  has_clip_norm_ = true;
  return &clip_norm_;
}

// optional field global_norm
bool ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::has_global_norm() const {
  return has_global_norm_;
}
const float& ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::global_norm() const {
  if (has_global_norm_) { return global_norm_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::clear_global_norm() {
  has_global_norm_ = false;
}
void ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::set_global_norm(const float& value) {
  global_norm_ = value;
  has_global_norm_ = true;
}
float* ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::mutable_global_norm() {
  has_global_norm_ = true;
  return &global_norm_;
}


int ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::compare(const _ClipByGlobalNormConf_& other) {
  if (!(has_clip_norm() == other.has_clip_norm())) {
    return has_clip_norm() < other.has_clip_norm() ? -1 : 1;
  } else if (!(clip_norm() == other.clip_norm())) {
    return clip_norm() < other.clip_norm() ? -1 : 1;
  }
  if (!(has_global_norm() == other.has_global_norm())) {
    return has_global_norm() < other.has_global_norm() ? -1 : 1;
  } else if (!(global_norm() == other.global_norm())) {
    return global_norm() < other.global_norm() ? -1 : 1;
  }
  return 0;
}

bool ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::operator==(const _ClipByGlobalNormConf_& other) const {
  return true
    && has_clip_norm() == other.has_clip_norm() 
    && clip_norm() == other.clip_norm()
    && has_global_norm() == other.has_global_norm() 
    && global_norm() == other.global_norm()
  ;
}

std::size_t ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::__CalcHash__() const {
  return 0
    ^ (has_clip_norm() ? std::hash<float>()(clip_norm()) : 0)
    ^ (has_global_norm() ? std::hash<float>()(global_norm()) : 0)
  ;
}

bool ConstClipByGlobalNormConf::_ClipByGlobalNormConf_::operator<(const _ClipByGlobalNormConf_& other) const {
  return false
    || !(has_clip_norm() == other.has_clip_norm()) ? 
      has_clip_norm() < other.has_clip_norm() : false
    || !(clip_norm() == other.clip_norm()) ? 
      clip_norm() < other.clip_norm() : false
    || !(has_global_norm() == other.has_global_norm()) ? 
      has_global_norm() < other.has_global_norm() : false
    || !(global_norm() == other.global_norm()) ? 
      global_norm() < other.global_norm() : false
  ;
}

using _ClipByGlobalNormConf_ =  ConstClipByGlobalNormConf::_ClipByGlobalNormConf_;
ConstClipByGlobalNormConf::ConstClipByGlobalNormConf(const ::std::shared_ptr<_ClipByGlobalNormConf_>& data): data_(data) {}
ConstClipByGlobalNormConf::ConstClipByGlobalNormConf(): data_(::std::make_shared<_ClipByGlobalNormConf_>()) {}
ConstClipByGlobalNormConf::ConstClipByGlobalNormConf(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf) {
  BuildFromProto(proto_clipbyglobalnormconf);
}
ConstClipByGlobalNormConf::ConstClipByGlobalNormConf(const ConstClipByGlobalNormConf&) = default;
ConstClipByGlobalNormConf::ConstClipByGlobalNormConf(ConstClipByGlobalNormConf&&) noexcept = default;
ConstClipByGlobalNormConf::~ConstClipByGlobalNormConf() = default;

void ConstClipByGlobalNormConf::ToProto(PbMessage* proto_clipbyglobalnormconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClipByGlobalNormConf*>(proto_clipbyglobalnormconf));
}
  
::std::string ConstClipByGlobalNormConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClipByGlobalNormConf::__Empty__() const {
  return !data_;
}

int ConstClipByGlobalNormConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"clip_norm", 1},
    {"global_norm", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstClipByGlobalNormConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstClipByGlobalNormConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClipByGlobalNormConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &clip_norm();
    case 2: return &global_norm();
    default: return nullptr;
  }
}

// required or optional field clip_norm
bool ConstClipByGlobalNormConf::has_clip_norm() const {
  return __SharedPtrOrDefault__()->has_clip_norm();
}
const float& ConstClipByGlobalNormConf::clip_norm() const {
  return __SharedPtrOrDefault__()->clip_norm();
}
// used by pybind11 only
// required or optional field global_norm
bool ConstClipByGlobalNormConf::has_global_norm() const {
  return __SharedPtrOrDefault__()->has_global_norm();
}
const float& ConstClipByGlobalNormConf::global_norm() const {
  return __SharedPtrOrDefault__()->global_norm();
}
// used by pybind11 only

::std::shared_ptr<ConstClipByGlobalNormConf> ConstClipByGlobalNormConf::__SharedConst__() const {
  return ::std::make_shared<ConstClipByGlobalNormConf>(data_);
}
int64_t ConstClipByGlobalNormConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClipByGlobalNormConf::operator==(const ConstClipByGlobalNormConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClipByGlobalNormConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClipByGlobalNormConf::operator<(const ConstClipByGlobalNormConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClipByGlobalNormConf_>& ConstClipByGlobalNormConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClipByGlobalNormConf_> default_ptr = std::make_shared<_ClipByGlobalNormConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClipByGlobalNormConf_>& ConstClipByGlobalNormConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClipByGlobalNormConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClipByGlobalNormConf
void ConstClipByGlobalNormConf::BuildFromProto(const PbMessage& proto_clipbyglobalnormconf) {
  data_ = ::std::make_shared<_ClipByGlobalNormConf_>(dynamic_cast<const ::oneflow::ClipByGlobalNormConf&>(proto_clipbyglobalnormconf));
}

ClipByGlobalNormConf::ClipByGlobalNormConf(const ::std::shared_ptr<_ClipByGlobalNormConf_>& data)
  : ConstClipByGlobalNormConf(data) {}
ClipByGlobalNormConf::ClipByGlobalNormConf(const ClipByGlobalNormConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClipByGlobalNormConf> resize
ClipByGlobalNormConf::ClipByGlobalNormConf(ClipByGlobalNormConf&&) noexcept = default; 
ClipByGlobalNormConf::ClipByGlobalNormConf(const ::oneflow::ClipByGlobalNormConf& proto_clipbyglobalnormconf) {
  InitFromProto(proto_clipbyglobalnormconf);
}
ClipByGlobalNormConf::ClipByGlobalNormConf() = default;

ClipByGlobalNormConf::~ClipByGlobalNormConf() = default;

void ClipByGlobalNormConf::InitFromProto(const PbMessage& proto_clipbyglobalnormconf) {
  BuildFromProto(proto_clipbyglobalnormconf);
}
  
void* ClipByGlobalNormConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_clip_norm();
    case 2: return mutable_global_norm();
    default: return nullptr;
  }
}

bool ClipByGlobalNormConf::operator==(const ClipByGlobalNormConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClipByGlobalNormConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClipByGlobalNormConf::operator<(const ClipByGlobalNormConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClipByGlobalNormConf::Clear() {
  if (data_) { data_.reset(); }
}
void ClipByGlobalNormConf::CopyFrom(const ClipByGlobalNormConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClipByGlobalNormConf& ClipByGlobalNormConf::operator=(const ClipByGlobalNormConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field clip_norm
void ClipByGlobalNormConf::clear_clip_norm() {
  return __SharedPtr__()->clear_clip_norm();
}
void ClipByGlobalNormConf::set_clip_norm(const float& value) {
  return __SharedPtr__()->set_clip_norm(value);
}
float* ClipByGlobalNormConf::mutable_clip_norm() {
  return  __SharedPtr__()->mutable_clip_norm();
}
// required or optional field global_norm
void ClipByGlobalNormConf::clear_global_norm() {
  return __SharedPtr__()->clear_global_norm();
}
void ClipByGlobalNormConf::set_global_norm(const float& value) {
  return __SharedPtr__()->set_global_norm(value);
}
float* ClipByGlobalNormConf::mutable_global_norm() {
  return  __SharedPtr__()->mutable_global_norm();
}

::std::shared_ptr<ClipByGlobalNormConf> ClipByGlobalNormConf::__SharedMutable__() {
  return ::std::make_shared<ClipByGlobalNormConf>(__SharedPtr__());
}
ConstClipConf::_ClipConf_::_ClipConf_() { Clear(); }
ConstClipConf::_ClipConf_::_ClipConf_(const _ClipConf_& other) { CopyFrom(other); }
ConstClipConf::_ClipConf_::_ClipConf_(const ::oneflow::ClipConf& proto_clipconf) {
  InitFromProto(proto_clipconf);
}
ConstClipConf::_ClipConf_::_ClipConf_(_ClipConf_&& other) = default;
ConstClipConf::_ClipConf_::~_ClipConf_() = default;

void ConstClipConf::_ClipConf_::InitFromProto(const ::oneflow::ClipConf& proto_clipconf) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_clipconf.type_case()));
  switch (type_case) {
    case kClipByGlobalNorm: {
      *mutable_clip_by_global_norm() = ::oneflow::cfg::ClipByGlobalNormConf(proto_clipconf.clip_by_global_norm());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstClipConf::_ClipConf_::ToProto(::oneflow::ClipConf* proto_clipconf) const {
  proto_clipconf->Clear();

  // oneof field: type
  ::oneflow::ClipConf::TypeCase type_case = ::oneflow::ClipConf::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::ClipConf::kClipByGlobalNorm: {
      ::oneflow::ClipByGlobalNormConf of_proto_clip_by_global_norm;
      clip_by_global_norm().ToProto(&of_proto_clip_by_global_norm);
      proto_clipconf->mutable_clip_by_global_norm()->CopyFrom(of_proto_clip_by_global_norm);
      break;
    }
    case ::oneflow::ClipConf::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstClipConf::_ClipConf_::DebugString() const {
  ::oneflow::ClipConf proto_clipconf;
  this->ToProto(&proto_clipconf);
  return proto_clipconf.DebugString();
}

void ConstClipConf::_ClipConf_::Clear() {
  clear_type();
}

void ConstClipConf::_ClipConf_::CopyFrom(const _ClipConf_& other) {
  type_copy_from(other);
}


// oneof field type: clip_by_global_norm
bool ConstClipConf::_ClipConf_::has_clip_by_global_norm() const {
  return type_case() == kClipByGlobalNorm;
}
void ConstClipConf::_ClipConf_::clear_clip_by_global_norm() {
  if (has_clip_by_global_norm()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.clip_by_global_norm_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ClipByGlobalNormConf& ConstClipConf::_ClipConf_::clip_by_global_norm() const {
  if (has_clip_by_global_norm()) {
      const ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>*>(&(type_.clip_by_global_norm_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf> default_static_value = ::std::make_shared<::oneflow::cfg::ClipByGlobalNormConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ClipByGlobalNormConf* ConstClipConf::_ClipConf_::mutable_clip_by_global_norm() {
  if(!has_clip_by_global_norm()) {
    clear_type();
    new (&(type_.clip_by_global_norm_)) ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>(new ::oneflow::cfg::ClipByGlobalNormConf());
  }
  type_case_ = kClipByGlobalNorm;
  ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>*>(&(type_.clip_by_global_norm_));
  return  (*ptr).get();
}
ConstClipConf::TypeCase ConstClipConf::_ClipConf_::type_case() const {
  return type_case_;
}
bool ConstClipConf::_ClipConf_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstClipConf::_ClipConf_::clear_type() {
  switch (type_case()) {
    case kClipByGlobalNorm: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.clip_by_global_norm_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstClipConf::_ClipConf_::type_copy_from(const _ClipConf_& other) {
  switch (other.type_case()) {
    case kClipByGlobalNorm: {
      mutable_clip_by_global_norm()->CopyFrom(other.clip_by_global_norm());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstClipConf::_ClipConf_::compare(const _ClipConf_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kClipByGlobalNorm: {
      if (!(clip_by_global_norm() == other.clip_by_global_norm())) {
        return clip_by_global_norm() < other.clip_by_global_norm() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstClipConf::_ClipConf_::operator==(const _ClipConf_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kClipByGlobalNorm ? 
      clip_by_global_norm() == other.clip_by_global_norm() : true)
  ;
}

std::size_t ConstClipConf::_ClipConf_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_clip_by_global_norm() ? std::hash<::oneflow::cfg::ClipByGlobalNormConf>()(clip_by_global_norm()) : 0)
  ;
}

bool ConstClipConf::_ClipConf_::operator<(const _ClipConf_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kClipByGlobalNorm) && 
      !(clip_by_global_norm() == other.clip_by_global_norm())) ? 
        clip_by_global_norm() < other.clip_by_global_norm() : false
  ;
}

using _ClipConf_ =  ConstClipConf::_ClipConf_;
ConstClipConf::ConstClipConf(const ::std::shared_ptr<_ClipConf_>& data): data_(data) {}
ConstClipConf::ConstClipConf(): data_(::std::make_shared<_ClipConf_>()) {}
ConstClipConf::ConstClipConf(const ::oneflow::ClipConf& proto_clipconf) {
  BuildFromProto(proto_clipconf);
}
ConstClipConf::ConstClipConf(const ConstClipConf&) = default;
ConstClipConf::ConstClipConf(ConstClipConf&&) noexcept = default;
ConstClipConf::~ConstClipConf() = default;

void ConstClipConf::ToProto(PbMessage* proto_clipconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ClipConf*>(proto_clipconf));
}
  
::std::string ConstClipConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstClipConf::__Empty__() const {
  return !data_;
}

int ConstClipConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"clip_by_global_norm", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstClipConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstClipConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClipByGlobalNormConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClipByGlobalNormConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstClipConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &clip_by_global_norm();
    default: return nullptr;
  }
}

 // oneof field type: clip_by_global_norm
bool ConstClipConf::has_clip_by_global_norm() const {
  return __SharedPtrOrDefault__()->has_clip_by_global_norm();
}
const ::oneflow::cfg::ClipByGlobalNormConf& ConstClipConf::clip_by_global_norm() const {
  return __SharedPtrOrDefault__()->clip_by_global_norm();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClipByGlobalNormConf> ConstClipConf::shared_const_clip_by_global_norm() const {
  return clip_by_global_norm().__SharedConst__();
}
ConstClipConf::TypeCase ConstClipConf::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstClipConf::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstClipConf> ConstClipConf::__SharedConst__() const {
  return ::std::make_shared<ConstClipConf>(data_);
}
int64_t ConstClipConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstClipConf::operator==(const ConstClipConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstClipConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstClipConf::operator<(const ConstClipConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ClipConf_>& ConstClipConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ClipConf_> default_ptr = std::make_shared<_ClipConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ClipConf_>& ConstClipConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ClipConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstClipConf
void ConstClipConf::BuildFromProto(const PbMessage& proto_clipconf) {
  data_ = ::std::make_shared<_ClipConf_>(dynamic_cast<const ::oneflow::ClipConf&>(proto_clipconf));
}

ClipConf::ClipConf(const ::std::shared_ptr<_ClipConf_>& data)
  : ConstClipConf(data) {}
ClipConf::ClipConf(const ClipConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ClipConf> resize
ClipConf::ClipConf(ClipConf&&) noexcept = default; 
ClipConf::ClipConf(const ::oneflow::ClipConf& proto_clipconf) {
  InitFromProto(proto_clipconf);
}
ClipConf::ClipConf() = default;

ClipConf::~ClipConf() = default;

void ClipConf::InitFromProto(const PbMessage& proto_clipconf) {
  BuildFromProto(proto_clipconf);
}
  
void* ClipConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_clip_by_global_norm();
    default: return nullptr;
  }
}

bool ClipConf::operator==(const ClipConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ClipConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ClipConf::operator<(const ClipConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ClipConf::Clear() {
  if (data_) { data_.reset(); }
}
void ClipConf::CopyFrom(const ClipConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ClipConf& ClipConf::operator=(const ClipConf& other) {
  CopyFrom(other);
  return *this;
}

void ClipConf::clear_clip_by_global_norm() {
  return __SharedPtr__()->clear_clip_by_global_norm();
}
::oneflow::cfg::ClipByGlobalNormConf* ClipConf::mutable_clip_by_global_norm() {
  return __SharedPtr__()->mutable_clip_by_global_norm();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClipByGlobalNormConf> ClipConf::shared_mutable_clip_by_global_norm() {
  return mutable_clip_by_global_norm()->__SharedMutable__();
}

::std::shared_ptr<ClipConf> ClipConf::__SharedMutable__() {
  return ::std::make_shared<ClipConf>(__SharedPtr__());
}
ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::_WeightDecayFilterPatternSet_() { Clear(); }
ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::_WeightDecayFilterPatternSet_(const _WeightDecayFilterPatternSet_& other) { CopyFrom(other); }
ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::_WeightDecayFilterPatternSet_(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset) {
  InitFromProto(proto_weightdecayfilterpatternset);
}
ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::_WeightDecayFilterPatternSet_(_WeightDecayFilterPatternSet_&& other) = default;
ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::~_WeightDecayFilterPatternSet_() = default;

void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::InitFromProto(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset) {
  Clear();
  // repeated field: pattern
  if (!proto_weightdecayfilterpatternset.pattern().empty()) {
    for (const ::std::string& elem : proto_weightdecayfilterpatternset.pattern()) {
      add_pattern(elem);
    }
  }
    
}

void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::ToProto(::oneflow::WeightDecayFilterPatternSet* proto_weightdecayfilterpatternset) const {
  proto_weightdecayfilterpatternset->Clear();
  // repeated field: pattern
  if (!pattern().empty()) {
    for (const ::std::string& elem : pattern()) {
      proto_weightdecayfilterpatternset->add_pattern(elem);
    }
  }

}

::std::string ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::DebugString() const {
  ::oneflow::WeightDecayFilterPatternSet proto_weightdecayfilterpatternset;
  this->ToProto(&proto_weightdecayfilterpatternset);
  return proto_weightdecayfilterpatternset.DebugString();
}

void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::Clear() {
  clear_pattern();
}

void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::CopyFrom(const _WeightDecayFilterPatternSet_& other) {
  mutable_pattern()->CopyFrom(other.pattern());
}


// repeated field pattern
::std::size_t ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::pattern_size() const {
  if (!pattern_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return pattern_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::pattern() const {
  if (!pattern_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(pattern_.get());
}
const ::std::string& ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::pattern(::std::size_t index) const {
  if (!pattern_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return pattern_->Get(index);
}
void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::clear_pattern() {
  if (!pattern_) {
    pattern_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return pattern_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::mutable_pattern() {
  if (!pattern_) {
    pattern_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  pattern_.get();
}
::std::string* ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::mutable_pattern(::std::size_t index) {
  if (!pattern_) {
    pattern_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  pattern_->Mutable(index);
}
void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::add_pattern(const ::std::string& value) {
  if (!pattern_) {
    pattern_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return pattern_->Add(value);
}
void ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::set_pattern(::std::size_t index, const ::std::string& value) {
  if (!pattern_) {
    pattern_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return pattern_->Set(index, value);
}


int ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::compare(const _WeightDecayFilterPatternSet_& other) {
  if (!(pattern() == other.pattern())) {
    return pattern() < other.pattern() ? -1 : 1;
  }
  return 0;
}

bool ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::operator==(const _WeightDecayFilterPatternSet_& other) const {
  return true
    && pattern() == other.pattern()
  ;
}

std::size_t ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::__CalcHash__() const {
  return 0
    ^ pattern().__CalcHash__()
  ;
}

bool ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_::operator<(const _WeightDecayFilterPatternSet_& other) const {
  return false
    || !(pattern() == other.pattern()) ? 
      pattern() < other.pattern() : false
  ;
}

using _WeightDecayFilterPatternSet_ =  ConstWeightDecayFilterPatternSet::_WeightDecayFilterPatternSet_;
ConstWeightDecayFilterPatternSet::ConstWeightDecayFilterPatternSet(const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& data): data_(data) {}
ConstWeightDecayFilterPatternSet::ConstWeightDecayFilterPatternSet(): data_(::std::make_shared<_WeightDecayFilterPatternSet_>()) {}
ConstWeightDecayFilterPatternSet::ConstWeightDecayFilterPatternSet(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset) {
  BuildFromProto(proto_weightdecayfilterpatternset);
}
ConstWeightDecayFilterPatternSet::ConstWeightDecayFilterPatternSet(const ConstWeightDecayFilterPatternSet&) = default;
ConstWeightDecayFilterPatternSet::ConstWeightDecayFilterPatternSet(ConstWeightDecayFilterPatternSet&&) noexcept = default;
ConstWeightDecayFilterPatternSet::~ConstWeightDecayFilterPatternSet() = default;

void ConstWeightDecayFilterPatternSet::ToProto(PbMessage* proto_weightdecayfilterpatternset) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::WeightDecayFilterPatternSet*>(proto_weightdecayfilterpatternset));
}
  
::std::string ConstWeightDecayFilterPatternSet::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstWeightDecayFilterPatternSet::__Empty__() const {
  return !data_;
}

int ConstWeightDecayFilterPatternSet::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"pattern", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstWeightDecayFilterPatternSet::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstWeightDecayFilterPatternSet::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstWeightDecayFilterPatternSet::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &pattern();
    default: return nullptr;
  }
}

// repeated field pattern
::std::size_t ConstWeightDecayFilterPatternSet::pattern_size() const {
  return __SharedPtrOrDefault__()->pattern_size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstWeightDecayFilterPatternSet::pattern() const {
  return __SharedPtrOrDefault__()->pattern();
}
const ::std::string& ConstWeightDecayFilterPatternSet::pattern(::std::size_t index) const {
  return __SharedPtrOrDefault__()->pattern(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> ConstWeightDecayFilterPatternSet::shared_const_pattern() const {
  return pattern().__SharedConst__();
}

::std::shared_ptr<ConstWeightDecayFilterPatternSet> ConstWeightDecayFilterPatternSet::__SharedConst__() const {
  return ::std::make_shared<ConstWeightDecayFilterPatternSet>(data_);
}
int64_t ConstWeightDecayFilterPatternSet::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstWeightDecayFilterPatternSet::operator==(const ConstWeightDecayFilterPatternSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstWeightDecayFilterPatternSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstWeightDecayFilterPatternSet::operator<(const ConstWeightDecayFilterPatternSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& ConstWeightDecayFilterPatternSet::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_WeightDecayFilterPatternSet_> default_ptr = std::make_shared<_WeightDecayFilterPatternSet_>();
  return default_ptr;
}
const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& ConstWeightDecayFilterPatternSet::__SharedPtr__() {
  if (!data_) { data_.reset(new _WeightDecayFilterPatternSet_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstWeightDecayFilterPatternSet
void ConstWeightDecayFilterPatternSet::BuildFromProto(const PbMessage& proto_weightdecayfilterpatternset) {
  data_ = ::std::make_shared<_WeightDecayFilterPatternSet_>(dynamic_cast<const ::oneflow::WeightDecayFilterPatternSet&>(proto_weightdecayfilterpatternset));
}

WeightDecayFilterPatternSet::WeightDecayFilterPatternSet(const ::std::shared_ptr<_WeightDecayFilterPatternSet_>& data)
  : ConstWeightDecayFilterPatternSet(data) {}
WeightDecayFilterPatternSet::WeightDecayFilterPatternSet(const WeightDecayFilterPatternSet& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<WeightDecayFilterPatternSet> resize
WeightDecayFilterPatternSet::WeightDecayFilterPatternSet(WeightDecayFilterPatternSet&&) noexcept = default; 
WeightDecayFilterPatternSet::WeightDecayFilterPatternSet(const ::oneflow::WeightDecayFilterPatternSet& proto_weightdecayfilterpatternset) {
  InitFromProto(proto_weightdecayfilterpatternset);
}
WeightDecayFilterPatternSet::WeightDecayFilterPatternSet() = default;

WeightDecayFilterPatternSet::~WeightDecayFilterPatternSet() = default;

void WeightDecayFilterPatternSet::InitFromProto(const PbMessage& proto_weightdecayfilterpatternset) {
  BuildFromProto(proto_weightdecayfilterpatternset);
}
  
void* WeightDecayFilterPatternSet::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_pattern();
    default: return nullptr;
  }
}

bool WeightDecayFilterPatternSet::operator==(const WeightDecayFilterPatternSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t WeightDecayFilterPatternSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool WeightDecayFilterPatternSet::operator<(const WeightDecayFilterPatternSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void WeightDecayFilterPatternSet::Clear() {
  if (data_) { data_.reset(); }
}
void WeightDecayFilterPatternSet::CopyFrom(const WeightDecayFilterPatternSet& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
WeightDecayFilterPatternSet& WeightDecayFilterPatternSet::operator=(const WeightDecayFilterPatternSet& other) {
  CopyFrom(other);
  return *this;
}

// repeated field pattern
void WeightDecayFilterPatternSet::clear_pattern() {
  return __SharedPtr__()->clear_pattern();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* WeightDecayFilterPatternSet::mutable_pattern() {
  return __SharedPtr__()->mutable_pattern();
}
::std::string* WeightDecayFilterPatternSet::mutable_pattern(::std::size_t index) {
  return __SharedPtr__()->mutable_pattern(index);
}
void WeightDecayFilterPatternSet::add_pattern(const ::std::string& value) {
  return __SharedPtr__()->add_pattern(value);
}
void WeightDecayFilterPatternSet::set_pattern(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_pattern(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> WeightDecayFilterPatternSet::shared_mutable_pattern() {
  return mutable_pattern()->__SharedMutable__();
}

::std::shared_ptr<WeightDecayFilterPatternSet> WeightDecayFilterPatternSet::__SharedMutable__() {
  return ::std::make_shared<WeightDecayFilterPatternSet>(__SharedPtr__());
}
ConstWeightDecayConf::_WeightDecayConf_::_WeightDecayConf_() { Clear(); }
ConstWeightDecayConf::_WeightDecayConf_::_WeightDecayConf_(const _WeightDecayConf_& other) { CopyFrom(other); }
ConstWeightDecayConf::_WeightDecayConf_::_WeightDecayConf_(const ::oneflow::WeightDecayConf& proto_weightdecayconf) {
  InitFromProto(proto_weightdecayconf);
}
ConstWeightDecayConf::_WeightDecayConf_::_WeightDecayConf_(_WeightDecayConf_&& other) = default;
ConstWeightDecayConf::_WeightDecayConf_::~_WeightDecayConf_() = default;

void ConstWeightDecayConf::_WeightDecayConf_::InitFromProto(const ::oneflow::WeightDecayConf& proto_weightdecayconf) {
  Clear();
  // required_or_optional field: weight_decay_rate
  if (proto_weightdecayconf.has_weight_decay_rate()) {
    set_weight_decay_rate(proto_weightdecayconf.weight_decay_rate());
  }
  // oneof field: weight_decay_filter_type
  WeightDecayFilterTypeCase weight_decay_filter_type_case = WeightDecayFilterTypeCase(int(proto_weightdecayconf.weight_decay_filter_type_case()));
  switch (weight_decay_filter_type_case) {
    case kIncludes: {
      *mutable_includes() = ::oneflow::cfg::WeightDecayFilterPatternSet(proto_weightdecayconf.includes());
      break;
  }
    case kExcludes: {
      *mutable_excludes() = ::oneflow::cfg::WeightDecayFilterPatternSet(proto_weightdecayconf.excludes());
      break;
  }
    case WEIGHT_DECAY_FILTER_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstWeightDecayConf::_WeightDecayConf_::ToProto(::oneflow::WeightDecayConf* proto_weightdecayconf) const {
  proto_weightdecayconf->Clear();
  // required_or_optional field: weight_decay_rate
  if (this->has_weight_decay_rate()) {
    proto_weightdecayconf->set_weight_decay_rate(weight_decay_rate());
    }

  // oneof field: weight_decay_filter_type
  ::oneflow::WeightDecayConf::WeightDecayFilterTypeCase weight_decay_filter_type_case = ::oneflow::WeightDecayConf::WeightDecayFilterTypeCase(int(this->weight_decay_filter_type_case()));
  switch (weight_decay_filter_type_case) {
    case ::oneflow::WeightDecayConf::kIncludes: {
      ::oneflow::WeightDecayFilterPatternSet of_proto_includes;
      includes().ToProto(&of_proto_includes);
      proto_weightdecayconf->mutable_includes()->CopyFrom(of_proto_includes);
      break;
    }
    case ::oneflow::WeightDecayConf::kExcludes: {
      ::oneflow::WeightDecayFilterPatternSet of_proto_excludes;
      excludes().ToProto(&of_proto_excludes);
      proto_weightdecayconf->mutable_excludes()->CopyFrom(of_proto_excludes);
      break;
    }
    case ::oneflow::WeightDecayConf::WEIGHT_DECAY_FILTER_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstWeightDecayConf::_WeightDecayConf_::DebugString() const {
  ::oneflow::WeightDecayConf proto_weightdecayconf;
  this->ToProto(&proto_weightdecayconf);
  return proto_weightdecayconf.DebugString();
}

void ConstWeightDecayConf::_WeightDecayConf_::Clear() {
  clear_weight_decay_rate();
  clear_weight_decay_filter_type();
}

void ConstWeightDecayConf::_WeightDecayConf_::CopyFrom(const _WeightDecayConf_& other) {
  if (other.has_weight_decay_rate()) {
    set_weight_decay_rate(other.weight_decay_rate());
  } else {
    clear_weight_decay_rate();
  }
  weight_decay_filter_type_copy_from(other);
}


// optional field weight_decay_rate
bool ConstWeightDecayConf::_WeightDecayConf_::has_weight_decay_rate() const {
  return has_weight_decay_rate_;
}
const float& ConstWeightDecayConf::_WeightDecayConf_::weight_decay_rate() const {
  if (has_weight_decay_rate_) { return weight_decay_rate_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstWeightDecayConf::_WeightDecayConf_::clear_weight_decay_rate() {
  has_weight_decay_rate_ = false;
}
void ConstWeightDecayConf::_WeightDecayConf_::set_weight_decay_rate(const float& value) {
  weight_decay_rate_ = value;
  has_weight_decay_rate_ = true;
}
float* ConstWeightDecayConf::_WeightDecayConf_::mutable_weight_decay_rate() {
  has_weight_decay_rate_ = true;
  return &weight_decay_rate_;
}

// oneof field weight_decay_filter_type: includes
bool ConstWeightDecayConf::_WeightDecayConf_::has_includes() const {
  return weight_decay_filter_type_case() == kIncludes;
}
void ConstWeightDecayConf::_WeightDecayConf_::clear_includes() {
  if (has_includes()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(weight_decay_filter_type_.includes_));
      ptr->~Shared_ptr();
    }
    weight_decay_filter_type_case_ = WEIGHT_DECAY_FILTER_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::WeightDecayFilterPatternSet& ConstWeightDecayConf::_WeightDecayConf_::includes() const {
  if (has_includes()) {
      const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>*>(&(weight_decay_filter_type_.includes_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> default_static_value = ::std::make_shared<::oneflow::cfg::WeightDecayFilterPatternSet>();
    return *default_static_value;
    }
}
::oneflow::cfg::WeightDecayFilterPatternSet* ConstWeightDecayConf::_WeightDecayConf_::mutable_includes() {
  if(!has_includes()) {
    clear_weight_decay_filter_type();
    new (&(weight_decay_filter_type_.includes_)) ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>(new ::oneflow::cfg::WeightDecayFilterPatternSet());
  }
  weight_decay_filter_type_case_ = kIncludes;
  ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>*>(&(weight_decay_filter_type_.includes_));
  return  (*ptr).get();
}

// oneof field weight_decay_filter_type: excludes
bool ConstWeightDecayConf::_WeightDecayConf_::has_excludes() const {
  return weight_decay_filter_type_case() == kExcludes;
}
void ConstWeightDecayConf::_WeightDecayConf_::clear_excludes() {
  if (has_excludes()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(weight_decay_filter_type_.excludes_));
      ptr->~Shared_ptr();
    }
    weight_decay_filter_type_case_ = WEIGHT_DECAY_FILTER_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::WeightDecayFilterPatternSet& ConstWeightDecayConf::_WeightDecayConf_::excludes() const {
  if (has_excludes()) {
      const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>*>(&(weight_decay_filter_type_.excludes_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> default_static_value = ::std::make_shared<::oneflow::cfg::WeightDecayFilterPatternSet>();
    return *default_static_value;
    }
}
::oneflow::cfg::WeightDecayFilterPatternSet* ConstWeightDecayConf::_WeightDecayConf_::mutable_excludes() {
  if(!has_excludes()) {
    clear_weight_decay_filter_type();
    new (&(weight_decay_filter_type_.excludes_)) ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>(new ::oneflow::cfg::WeightDecayFilterPatternSet());
  }
  weight_decay_filter_type_case_ = kExcludes;
  ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>*>(&(weight_decay_filter_type_.excludes_));
  return  (*ptr).get();
}
ConstWeightDecayConf::WeightDecayFilterTypeCase ConstWeightDecayConf::_WeightDecayConf_::weight_decay_filter_type_case() const {
  return weight_decay_filter_type_case_;
}
bool ConstWeightDecayConf::_WeightDecayConf_::has_weight_decay_filter_type() const {
  return weight_decay_filter_type_case_ != WEIGHT_DECAY_FILTER_TYPE_NOT_SET;
}
void ConstWeightDecayConf::_WeightDecayConf_::clear_weight_decay_filter_type() {
  switch (weight_decay_filter_type_case()) {
    case kIncludes: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(weight_decay_filter_type_.includes_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kExcludes: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(weight_decay_filter_type_.excludes_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case WEIGHT_DECAY_FILTER_TYPE_NOT_SET: {
      break;
    }
  }
  weight_decay_filter_type_case_ = WEIGHT_DECAY_FILTER_TYPE_NOT_SET;
}
void ConstWeightDecayConf::_WeightDecayConf_::weight_decay_filter_type_copy_from(const _WeightDecayConf_& other) {
  switch (other.weight_decay_filter_type_case()) {
    case kIncludes: {
      mutable_includes()->CopyFrom(other.includes());
      break;
    }
    case kExcludes: {
      mutable_excludes()->CopyFrom(other.excludes());
      break;
    }
    case WEIGHT_DECAY_FILTER_TYPE_NOT_SET: {
      clear_weight_decay_filter_type();
    }
  }
}


int ConstWeightDecayConf::_WeightDecayConf_::compare(const _WeightDecayConf_& other) {
  if (!(has_weight_decay_rate() == other.has_weight_decay_rate())) {
    return has_weight_decay_rate() < other.has_weight_decay_rate() ? -1 : 1;
  } else if (!(weight_decay_rate() == other.weight_decay_rate())) {
    return weight_decay_rate() < other.weight_decay_rate() ? -1 : 1;
  }
  if (!(weight_decay_filter_type_case() == other.weight_decay_filter_type_case())) {
    return weight_decay_filter_type_case() < other.weight_decay_filter_type_case() ? -1 : 1;
  }
  switch (weight_decay_filter_type_case()) {
    case kIncludes: {
      if (!(includes() == other.includes())) {
        return includes() < other.includes() ? -1 : 1;
      }
      break;
    }
    case kExcludes: {
      if (!(excludes() == other.excludes())) {
        return excludes() < other.excludes() ? -1 : 1;
      }
      break;
    }
    case WEIGHT_DECAY_FILTER_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstWeightDecayConf::_WeightDecayConf_::operator==(const _WeightDecayConf_& other) const {
  return true
    && has_weight_decay_rate() == other.has_weight_decay_rate() 
    && weight_decay_rate() == other.weight_decay_rate()
    && weight_decay_filter_type_case() == other.weight_decay_filter_type_case()
    && (weight_decay_filter_type_case() == kIncludes ? 
      includes() == other.includes() : true)
    && (weight_decay_filter_type_case() == kExcludes ? 
      excludes() == other.excludes() : true)
  ;
}

std::size_t ConstWeightDecayConf::_WeightDecayConf_::__CalcHash__() const {
  return 0
    ^ (has_weight_decay_rate() ? std::hash<float>()(weight_decay_rate()) : 0)
    ^ static_cast<std::size_t>(weight_decay_filter_type_case())
    ^ (has_includes() ? std::hash<::oneflow::cfg::WeightDecayFilterPatternSet>()(includes()) : 0)
    ^ (has_excludes() ? std::hash<::oneflow::cfg::WeightDecayFilterPatternSet>()(excludes()) : 0)
  ;
}

bool ConstWeightDecayConf::_WeightDecayConf_::operator<(const _WeightDecayConf_& other) const {
  return false
    || !(has_weight_decay_rate() == other.has_weight_decay_rate()) ? 
      has_weight_decay_rate() < other.has_weight_decay_rate() : false
    || !(weight_decay_rate() == other.weight_decay_rate()) ? 
      weight_decay_rate() < other.weight_decay_rate() : false
    || !(weight_decay_filter_type_case() == other.weight_decay_filter_type_case()) ? 
      weight_decay_filter_type_case() < other.weight_decay_filter_type_case() : false
    || ((weight_decay_filter_type_case() == kIncludes) && 
      !(includes() == other.includes())) ? 
        includes() < other.includes() : false
    || ((weight_decay_filter_type_case() == kExcludes) && 
      !(excludes() == other.excludes())) ? 
        excludes() < other.excludes() : false
  ;
}

using _WeightDecayConf_ =  ConstWeightDecayConf::_WeightDecayConf_;
ConstWeightDecayConf::ConstWeightDecayConf(const ::std::shared_ptr<_WeightDecayConf_>& data): data_(data) {}
ConstWeightDecayConf::ConstWeightDecayConf(): data_(::std::make_shared<_WeightDecayConf_>()) {}
ConstWeightDecayConf::ConstWeightDecayConf(const ::oneflow::WeightDecayConf& proto_weightdecayconf) {
  BuildFromProto(proto_weightdecayconf);
}
ConstWeightDecayConf::ConstWeightDecayConf(const ConstWeightDecayConf&) = default;
ConstWeightDecayConf::ConstWeightDecayConf(ConstWeightDecayConf&&) noexcept = default;
ConstWeightDecayConf::~ConstWeightDecayConf() = default;

void ConstWeightDecayConf::ToProto(PbMessage* proto_weightdecayconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::WeightDecayConf*>(proto_weightdecayconf));
}
  
::std::string ConstWeightDecayConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstWeightDecayConf::__Empty__() const {
  return !data_;
}

int ConstWeightDecayConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"weight_decay_rate", 1},
    {"includes", 2},
    {"excludes", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstWeightDecayConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstWeightDecayConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WeightDecayFilterPatternSet),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWeightDecayFilterPatternSet),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WeightDecayFilterPatternSet),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWeightDecayFilterPatternSet),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstWeightDecayConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &weight_decay_rate();
    case 2: return &includes();
    case 3: return &excludes();
    default: return nullptr;
  }
}

// required or optional field weight_decay_rate
bool ConstWeightDecayConf::has_weight_decay_rate() const {
  return __SharedPtrOrDefault__()->has_weight_decay_rate();
}
const float& ConstWeightDecayConf::weight_decay_rate() const {
  return __SharedPtrOrDefault__()->weight_decay_rate();
}
// used by pybind11 only
 // oneof field weight_decay_filter_type: includes
bool ConstWeightDecayConf::has_includes() const {
  return __SharedPtrOrDefault__()->has_includes();
}
const ::oneflow::cfg::WeightDecayFilterPatternSet& ConstWeightDecayConf::includes() const {
  return __SharedPtrOrDefault__()->includes();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWeightDecayFilterPatternSet> ConstWeightDecayConf::shared_const_includes() const {
  return includes().__SharedConst__();
}
 // oneof field weight_decay_filter_type: excludes
bool ConstWeightDecayConf::has_excludes() const {
  return __SharedPtrOrDefault__()->has_excludes();
}
const ::oneflow::cfg::WeightDecayFilterPatternSet& ConstWeightDecayConf::excludes() const {
  return __SharedPtrOrDefault__()->excludes();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWeightDecayFilterPatternSet> ConstWeightDecayConf::shared_const_excludes() const {
  return excludes().__SharedConst__();
}
ConstWeightDecayConf::WeightDecayFilterTypeCase ConstWeightDecayConf::weight_decay_filter_type_case() const {
  return __SharedPtrOrDefault__()->weight_decay_filter_type_case();
}

bool ConstWeightDecayConf::has_weight_decay_filter_type() const {
  return __SharedPtrOrDefault__()->has_weight_decay_filter_type();
}

::std::shared_ptr<ConstWeightDecayConf> ConstWeightDecayConf::__SharedConst__() const {
  return ::std::make_shared<ConstWeightDecayConf>(data_);
}
int64_t ConstWeightDecayConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstWeightDecayConf::operator==(const ConstWeightDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstWeightDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstWeightDecayConf::operator<(const ConstWeightDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_WeightDecayConf_>& ConstWeightDecayConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_WeightDecayConf_> default_ptr = std::make_shared<_WeightDecayConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_WeightDecayConf_>& ConstWeightDecayConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _WeightDecayConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstWeightDecayConf
void ConstWeightDecayConf::BuildFromProto(const PbMessage& proto_weightdecayconf) {
  data_ = ::std::make_shared<_WeightDecayConf_>(dynamic_cast<const ::oneflow::WeightDecayConf&>(proto_weightdecayconf));
}

WeightDecayConf::WeightDecayConf(const ::std::shared_ptr<_WeightDecayConf_>& data)
  : ConstWeightDecayConf(data) {}
WeightDecayConf::WeightDecayConf(const WeightDecayConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<WeightDecayConf> resize
WeightDecayConf::WeightDecayConf(WeightDecayConf&&) noexcept = default; 
WeightDecayConf::WeightDecayConf(const ::oneflow::WeightDecayConf& proto_weightdecayconf) {
  InitFromProto(proto_weightdecayconf);
}
WeightDecayConf::WeightDecayConf() = default;

WeightDecayConf::~WeightDecayConf() = default;

void WeightDecayConf::InitFromProto(const PbMessage& proto_weightdecayconf) {
  BuildFromProto(proto_weightdecayconf);
}
  
void* WeightDecayConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_weight_decay_rate();
    case 2: return mutable_includes();
    case 3: return mutable_excludes();
    default: return nullptr;
  }
}

bool WeightDecayConf::operator==(const WeightDecayConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t WeightDecayConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool WeightDecayConf::operator<(const WeightDecayConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void WeightDecayConf::Clear() {
  if (data_) { data_.reset(); }
}
void WeightDecayConf::CopyFrom(const WeightDecayConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
WeightDecayConf& WeightDecayConf::operator=(const WeightDecayConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field weight_decay_rate
void WeightDecayConf::clear_weight_decay_rate() {
  return __SharedPtr__()->clear_weight_decay_rate();
}
void WeightDecayConf::set_weight_decay_rate(const float& value) {
  return __SharedPtr__()->set_weight_decay_rate(value);
}
float* WeightDecayConf::mutable_weight_decay_rate() {
  return  __SharedPtr__()->mutable_weight_decay_rate();
}
void WeightDecayConf::clear_includes() {
  return __SharedPtr__()->clear_includes();
}
::oneflow::cfg::WeightDecayFilterPatternSet* WeightDecayConf::mutable_includes() {
  return __SharedPtr__()->mutable_includes();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> WeightDecayConf::shared_mutable_includes() {
  return mutable_includes()->__SharedMutable__();
}
void WeightDecayConf::clear_excludes() {
  return __SharedPtr__()->clear_excludes();
}
::oneflow::cfg::WeightDecayFilterPatternSet* WeightDecayConf::mutable_excludes() {
  return __SharedPtr__()->mutable_excludes();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WeightDecayFilterPatternSet> WeightDecayConf::shared_mutable_excludes() {
  return mutable_excludes()->__SharedMutable__();
}

::std::shared_ptr<WeightDecayConf> WeightDecayConf::__SharedMutable__() {
  return ::std::make_shared<WeightDecayConf>(__SharedPtr__());
}
ConstOptimizerConf::_OptimizerConf_::_OptimizerConf_() { Clear(); }
ConstOptimizerConf::_OptimizerConf_::_OptimizerConf_(const _OptimizerConf_& other) { CopyFrom(other); }
ConstOptimizerConf::_OptimizerConf_::_OptimizerConf_(const ::oneflow::OptimizerConf& proto_optimizerconf) {
  InitFromProto(proto_optimizerconf);
}
ConstOptimizerConf::_OptimizerConf_::_OptimizerConf_(_OptimizerConf_&& other) = default;
ConstOptimizerConf::_OptimizerConf_::~_OptimizerConf_() = default;

void ConstOptimizerConf::_OptimizerConf_::InitFromProto(const ::oneflow::OptimizerConf& proto_optimizerconf) {
  Clear();
  // repeated field: variable_op_names
  if (!proto_optimizerconf.variable_op_names().empty()) {
    for (const ::std::string& elem : proto_optimizerconf.variable_op_names()) {
      add_variable_op_names(elem);
    }
  }
  // required_or_optional field: base_learning_rate
  if (proto_optimizerconf.has_base_learning_rate()) {
    set_base_learning_rate(proto_optimizerconf.base_learning_rate());
  }
  // required_or_optional field: warmup_conf
  if (proto_optimizerconf.has_warmup_conf()) {
  *mutable_warmup_conf() = ::oneflow::cfg::WarmupConf(proto_optimizerconf.warmup_conf());      
  }
  // required_or_optional field: learning_rate_decay
  if (proto_optimizerconf.has_learning_rate_decay()) {
  *mutable_learning_rate_decay() = ::oneflow::cfg::LearningRateDecayConf(proto_optimizerconf.learning_rate_decay());      
  }
  // required_or_optional field: learning_rate_lbn
  if (proto_optimizerconf.has_learning_rate_lbn()) {
    set_learning_rate_lbn(proto_optimizerconf.learning_rate_lbn());
  }
  // required_or_optional field: clip_conf
  if (proto_optimizerconf.has_clip_conf()) {
  *mutable_clip_conf() = ::oneflow::cfg::ClipConf(proto_optimizerconf.clip_conf());      
  }
  // required_or_optional field: weight_decay_conf
  if (proto_optimizerconf.has_weight_decay_conf()) {
  *mutable_weight_decay_conf() = ::oneflow::cfg::WeightDecayConf(proto_optimizerconf.weight_decay_conf());      
  }
  // oneof field: normal_mdupdt
  NormalMdupdtCase normal_mdupdt_case = NormalMdupdtCase(int(proto_optimizerconf.normal_mdupdt_case()));
  switch (normal_mdupdt_case) {
    case kNaiveConf: {
      *mutable_naive_conf() = ::oneflow::cfg::NaiveModelUpdateConf(proto_optimizerconf.naive_conf());
      break;
  }
    case kMomentumConf: {
      *mutable_momentum_conf() = ::oneflow::cfg::MomentumModelUpdateConf(proto_optimizerconf.momentum_conf());
      break;
  }
    case kRmspropConf: {
      *mutable_rmsprop_conf() = ::oneflow::cfg::RMSPropModelUpdateConf(proto_optimizerconf.rmsprop_conf());
      break;
  }
    case kLarsConf: {
      *mutable_lars_conf() = ::oneflow::cfg::LARSModelUpdateConf(proto_optimizerconf.lars_conf());
      break;
  }
    case kAdamConf: {
      *mutable_adam_conf() = ::oneflow::cfg::AdamModelUpdateConf(proto_optimizerconf.adam_conf());
      break;
  }
    case kLazyAdamConf: {
      *mutable_lazy_adam_conf() = ::oneflow::cfg::LazyAdamModelUpdateConf(proto_optimizerconf.lazy_adam_conf());
      break;
  }
    case kLambConf: {
      *mutable_lamb_conf() = ::oneflow::cfg::LambModelUpdateConf(proto_optimizerconf.lamb_conf());
      break;
  }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
      
}

void ConstOptimizerConf::_OptimizerConf_::ToProto(::oneflow::OptimizerConf* proto_optimizerconf) const {
  proto_optimizerconf->Clear();
  // repeated field: variable_op_names
  if (!variable_op_names().empty()) {
    for (const ::std::string& elem : variable_op_names()) {
      proto_optimizerconf->add_variable_op_names(elem);
    }
  }
  // required_or_optional field: base_learning_rate
  if (this->has_base_learning_rate()) {
    proto_optimizerconf->set_base_learning_rate(base_learning_rate());
    }
  // required_or_optional field: warmup_conf
  if (this->has_warmup_conf()) {
    ::oneflow::WarmupConf proto_warmup_conf;
    warmup_conf().ToProto(&proto_warmup_conf);
    proto_optimizerconf->mutable_warmup_conf()->CopyFrom(proto_warmup_conf);
    }
  // required_or_optional field: learning_rate_decay
  if (this->has_learning_rate_decay()) {
    ::oneflow::LearningRateDecayConf proto_learning_rate_decay;
    learning_rate_decay().ToProto(&proto_learning_rate_decay);
    proto_optimizerconf->mutable_learning_rate_decay()->CopyFrom(proto_learning_rate_decay);
    }
  // required_or_optional field: learning_rate_lbn
  if (this->has_learning_rate_lbn()) {
    proto_optimizerconf->set_learning_rate_lbn(learning_rate_lbn());
    }
  // required_or_optional field: clip_conf
  if (this->has_clip_conf()) {
    ::oneflow::ClipConf proto_clip_conf;
    clip_conf().ToProto(&proto_clip_conf);
    proto_optimizerconf->mutable_clip_conf()->CopyFrom(proto_clip_conf);
    }
  // required_or_optional field: weight_decay_conf
  if (this->has_weight_decay_conf()) {
    ::oneflow::WeightDecayConf proto_weight_decay_conf;
    weight_decay_conf().ToProto(&proto_weight_decay_conf);
    proto_optimizerconf->mutable_weight_decay_conf()->CopyFrom(proto_weight_decay_conf);
    }

  // oneof field: normal_mdupdt
  ::oneflow::OptimizerConf::NormalMdupdtCase normal_mdupdt_case = ::oneflow::OptimizerConf::NormalMdupdtCase(int(this->normal_mdupdt_case()));
  switch (normal_mdupdt_case) {
    case ::oneflow::OptimizerConf::kNaiveConf: {
      ::oneflow::NaiveModelUpdateConf of_proto_naive_conf;
      naive_conf().ToProto(&of_proto_naive_conf);
      proto_optimizerconf->mutable_naive_conf()->CopyFrom(of_proto_naive_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kMomentumConf: {
      ::oneflow::MomentumModelUpdateConf of_proto_momentum_conf;
      momentum_conf().ToProto(&of_proto_momentum_conf);
      proto_optimizerconf->mutable_momentum_conf()->CopyFrom(of_proto_momentum_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kRmspropConf: {
      ::oneflow::RMSPropModelUpdateConf of_proto_rmsprop_conf;
      rmsprop_conf().ToProto(&of_proto_rmsprop_conf);
      proto_optimizerconf->mutable_rmsprop_conf()->CopyFrom(of_proto_rmsprop_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kLarsConf: {
      ::oneflow::LARSModelUpdateConf of_proto_lars_conf;
      lars_conf().ToProto(&of_proto_lars_conf);
      proto_optimizerconf->mutable_lars_conf()->CopyFrom(of_proto_lars_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kAdamConf: {
      ::oneflow::AdamModelUpdateConf of_proto_adam_conf;
      adam_conf().ToProto(&of_proto_adam_conf);
      proto_optimizerconf->mutable_adam_conf()->CopyFrom(of_proto_adam_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kLazyAdamConf: {
      ::oneflow::LazyAdamModelUpdateConf of_proto_lazy_adam_conf;
      lazy_adam_conf().ToProto(&of_proto_lazy_adam_conf);
      proto_optimizerconf->mutable_lazy_adam_conf()->CopyFrom(of_proto_lazy_adam_conf);
      break;
    }
    case ::oneflow::OptimizerConf::kLambConf: {
      ::oneflow::LambModelUpdateConf of_proto_lamb_conf;
      lamb_conf().ToProto(&of_proto_lamb_conf);
      proto_optimizerconf->mutable_lamb_conf()->CopyFrom(of_proto_lamb_conf);
      break;
    }
    case ::oneflow::OptimizerConf::NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
}

::std::string ConstOptimizerConf::_OptimizerConf_::DebugString() const {
  ::oneflow::OptimizerConf proto_optimizerconf;
  this->ToProto(&proto_optimizerconf);
  return proto_optimizerconf.DebugString();
}

void ConstOptimizerConf::_OptimizerConf_::Clear() {
  clear_variable_op_names();
  clear_base_learning_rate();
  clear_warmup_conf();
  clear_learning_rate_decay();
  clear_learning_rate_lbn();
  clear_clip_conf();
  clear_weight_decay_conf();
  clear_normal_mdupdt();
}

void ConstOptimizerConf::_OptimizerConf_::CopyFrom(const _OptimizerConf_& other) {
  mutable_variable_op_names()->CopyFrom(other.variable_op_names());
  if (other.has_base_learning_rate()) {
    set_base_learning_rate(other.base_learning_rate());
  } else {
    clear_base_learning_rate();
  }
  if (other.has_warmup_conf()) {
    mutable_warmup_conf()->CopyFrom(other.warmup_conf());
  } else {
    clear_warmup_conf();
  }
  if (other.has_learning_rate_decay()) {
    mutable_learning_rate_decay()->CopyFrom(other.learning_rate_decay());
  } else {
    clear_learning_rate_decay();
  }
  if (other.has_learning_rate_lbn()) {
    set_learning_rate_lbn(other.learning_rate_lbn());
  } else {
    clear_learning_rate_lbn();
  }
  if (other.has_clip_conf()) {
    mutable_clip_conf()->CopyFrom(other.clip_conf());
  } else {
    clear_clip_conf();
  }
  if (other.has_weight_decay_conf()) {
    mutable_weight_decay_conf()->CopyFrom(other.weight_decay_conf());
  } else {
    clear_weight_decay_conf();
  }
  normal_mdupdt_copy_from(other);
}


// repeated field variable_op_names
::std::size_t ConstOptimizerConf::_OptimizerConf_::variable_op_names_size() const {
  if (!variable_op_names_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return variable_op_names_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstOptimizerConf::_OptimizerConf_::variable_op_names() const {
  if (!variable_op_names_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(variable_op_names_.get());
}
const ::std::string& ConstOptimizerConf::_OptimizerConf_::variable_op_names(::std::size_t index) const {
  if (!variable_op_names_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return variable_op_names_->Get(index);
}
void ConstOptimizerConf::_OptimizerConf_::clear_variable_op_names() {
  if (!variable_op_names_) {
    variable_op_names_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return variable_op_names_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* ConstOptimizerConf::_OptimizerConf_::mutable_variable_op_names() {
  if (!variable_op_names_) {
    variable_op_names_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  variable_op_names_.get();
}
::std::string* ConstOptimizerConf::_OptimizerConf_::mutable_variable_op_names(::std::size_t index) {
  if (!variable_op_names_) {
    variable_op_names_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  variable_op_names_->Mutable(index);
}
void ConstOptimizerConf::_OptimizerConf_::add_variable_op_names(const ::std::string& value) {
  if (!variable_op_names_) {
    variable_op_names_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return variable_op_names_->Add(value);
}
void ConstOptimizerConf::_OptimizerConf_::set_variable_op_names(::std::size_t index, const ::std::string& value) {
  if (!variable_op_names_) {
    variable_op_names_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return variable_op_names_->Set(index, value);
}

// optional field base_learning_rate
bool ConstOptimizerConf::_OptimizerConf_::has_base_learning_rate() const {
  return has_base_learning_rate_;
}
const float& ConstOptimizerConf::_OptimizerConf_::base_learning_rate() const {
  if (has_base_learning_rate_) { return base_learning_rate_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstOptimizerConf::_OptimizerConf_::clear_base_learning_rate() {
  has_base_learning_rate_ = false;
}
void ConstOptimizerConf::_OptimizerConf_::set_base_learning_rate(const float& value) {
  base_learning_rate_ = value;
  has_base_learning_rate_ = true;
}
float* ConstOptimizerConf::_OptimizerConf_::mutable_base_learning_rate() {
  has_base_learning_rate_ = true;
  return &base_learning_rate_;
}

// optional field warmup_conf
bool ConstOptimizerConf::_OptimizerConf_::has_warmup_conf() const {
  return has_warmup_conf_;
}
const ::oneflow::cfg::WarmupConf& ConstOptimizerConf::_OptimizerConf_::warmup_conf() const {
  if (!warmup_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::WarmupConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::WarmupConf>();
    return *default_static_value;
  }
  return *(warmup_conf_.get());
}
void ConstOptimizerConf::_OptimizerConf_::clear_warmup_conf() {
  if (warmup_conf_) {
    warmup_conf_->Clear();
  }
  has_warmup_conf_ = false;
}
::oneflow::cfg::WarmupConf* ConstOptimizerConf::_OptimizerConf_::mutable_warmup_conf() {
  if (!warmup_conf_) {
    warmup_conf_ = ::std::make_shared<::oneflow::cfg::WarmupConf>();
  }
  has_warmup_conf_ = true;
  return warmup_conf_.get();
}

// optional field learning_rate_decay
bool ConstOptimizerConf::_OptimizerConf_::has_learning_rate_decay() const {
  return has_learning_rate_decay_;
}
const ::oneflow::cfg::LearningRateDecayConf& ConstOptimizerConf::_OptimizerConf_::learning_rate_decay() const {
  if (!learning_rate_decay_) {
    static const ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::LearningRateDecayConf>();
    return *default_static_value;
  }
  return *(learning_rate_decay_.get());
}
void ConstOptimizerConf::_OptimizerConf_::clear_learning_rate_decay() {
  if (learning_rate_decay_) {
    learning_rate_decay_->Clear();
  }
  has_learning_rate_decay_ = false;
}
::oneflow::cfg::LearningRateDecayConf* ConstOptimizerConf::_OptimizerConf_::mutable_learning_rate_decay() {
  if (!learning_rate_decay_) {
    learning_rate_decay_ = ::std::make_shared<::oneflow::cfg::LearningRateDecayConf>();
  }
  has_learning_rate_decay_ = true;
  return learning_rate_decay_.get();
}

// optional field learning_rate_lbn
bool ConstOptimizerConf::_OptimizerConf_::has_learning_rate_lbn() const {
  return has_learning_rate_lbn_;
}
const ::std::string& ConstOptimizerConf::_OptimizerConf_::learning_rate_lbn() const {
  if (has_learning_rate_lbn_) { return learning_rate_lbn_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstOptimizerConf::_OptimizerConf_::clear_learning_rate_lbn() {
  has_learning_rate_lbn_ = false;
}
void ConstOptimizerConf::_OptimizerConf_::set_learning_rate_lbn(const ::std::string& value) {
  learning_rate_lbn_ = value;
  has_learning_rate_lbn_ = true;
}
::std::string* ConstOptimizerConf::_OptimizerConf_::mutable_learning_rate_lbn() {
  has_learning_rate_lbn_ = true;
  return &learning_rate_lbn_;
}

// optional field clip_conf
bool ConstOptimizerConf::_OptimizerConf_::has_clip_conf() const {
  return has_clip_conf_;
}
const ::oneflow::cfg::ClipConf& ConstOptimizerConf::_OptimizerConf_::clip_conf() const {
  if (!clip_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ClipConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ClipConf>();
    return *default_static_value;
  }
  return *(clip_conf_.get());
}
void ConstOptimizerConf::_OptimizerConf_::clear_clip_conf() {
  if (clip_conf_) {
    clip_conf_->Clear();
  }
  has_clip_conf_ = false;
}
::oneflow::cfg::ClipConf* ConstOptimizerConf::_OptimizerConf_::mutable_clip_conf() {
  if (!clip_conf_) {
    clip_conf_ = ::std::make_shared<::oneflow::cfg::ClipConf>();
  }
  has_clip_conf_ = true;
  return clip_conf_.get();
}

// optional field weight_decay_conf
bool ConstOptimizerConf::_OptimizerConf_::has_weight_decay_conf() const {
  return has_weight_decay_conf_;
}
const ::oneflow::cfg::WeightDecayConf& ConstOptimizerConf::_OptimizerConf_::weight_decay_conf() const {
  if (!weight_decay_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::WeightDecayConf>();
    return *default_static_value;
  }
  return *(weight_decay_conf_.get());
}
void ConstOptimizerConf::_OptimizerConf_::clear_weight_decay_conf() {
  if (weight_decay_conf_) {
    weight_decay_conf_->Clear();
  }
  has_weight_decay_conf_ = false;
}
::oneflow::cfg::WeightDecayConf* ConstOptimizerConf::_OptimizerConf_::mutable_weight_decay_conf() {
  if (!weight_decay_conf_) {
    weight_decay_conf_ = ::std::make_shared<::oneflow::cfg::WeightDecayConf>();
  }
  has_weight_decay_conf_ = true;
  return weight_decay_conf_.get();
}

// oneof field normal_mdupdt: naive_conf
bool ConstOptimizerConf::_OptimizerConf_::has_naive_conf() const {
  return normal_mdupdt_case() == kNaiveConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_naive_conf() {
  if (has_naive_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.naive_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::NaiveModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::naive_conf() const {
  if (has_naive_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>*>(&(normal_mdupdt_.naive_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::NaiveModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::NaiveModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_naive_conf() {
  if(!has_naive_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.naive_conf_)) ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>(new ::oneflow::cfg::NaiveModelUpdateConf());
  }
  normal_mdupdt_case_ = kNaiveConf;
  ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>*>(&(normal_mdupdt_.naive_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: momentum_conf
bool ConstOptimizerConf::_OptimizerConf_::has_momentum_conf() const {
  return normal_mdupdt_case() == kMomentumConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_momentum_conf() {
  if (has_momentum_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.momentum_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::MomentumModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::momentum_conf() const {
  if (has_momentum_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>*>(&(normal_mdupdt_.momentum_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::MomentumModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::MomentumModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_momentum_conf() {
  if(!has_momentum_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.momentum_conf_)) ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>(new ::oneflow::cfg::MomentumModelUpdateConf());
  }
  normal_mdupdt_case_ = kMomentumConf;
  ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>*>(&(normal_mdupdt_.momentum_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: rmsprop_conf
bool ConstOptimizerConf::_OptimizerConf_::has_rmsprop_conf() const {
  return normal_mdupdt_case() == kRmspropConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_rmsprop_conf() {
  if (has_rmsprop_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.rmsprop_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::RMSPropModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::rmsprop_conf() const {
  if (has_rmsprop_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>*>(&(normal_mdupdt_.rmsprop_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::RMSPropModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RMSPropModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_rmsprop_conf() {
  if(!has_rmsprop_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.rmsprop_conf_)) ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>(new ::oneflow::cfg::RMSPropModelUpdateConf());
  }
  normal_mdupdt_case_ = kRmspropConf;
  ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>*>(&(normal_mdupdt_.rmsprop_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lars_conf
bool ConstOptimizerConf::_OptimizerConf_::has_lars_conf() const {
  return normal_mdupdt_case() == kLarsConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_lars_conf() {
  if (has_lars_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lars_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LARSModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::lars_conf() const {
  if (has_lars_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>*>(&(normal_mdupdt_.lars_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LARSModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LARSModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_lars_conf() {
  if(!has_lars_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lars_conf_)) ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>(new ::oneflow::cfg::LARSModelUpdateConf());
  }
  normal_mdupdt_case_ = kLarsConf;
  ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>*>(&(normal_mdupdt_.lars_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: adam_conf
bool ConstOptimizerConf::_OptimizerConf_::has_adam_conf() const {
  return normal_mdupdt_case() == kAdamConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_adam_conf() {
  if (has_adam_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.adam_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::AdamModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::adam_conf() const {
  if (has_adam_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>*>(&(normal_mdupdt_.adam_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::AdamModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::AdamModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_adam_conf() {
  if(!has_adam_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.adam_conf_)) ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>(new ::oneflow::cfg::AdamModelUpdateConf());
  }
  normal_mdupdt_case_ = kAdamConf;
  ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>*>(&(normal_mdupdt_.adam_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lazy_adam_conf
bool ConstOptimizerConf::_OptimizerConf_::has_lazy_adam_conf() const {
  return normal_mdupdt_case() == kLazyAdamConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_lazy_adam_conf() {
  if (has_lazy_adam_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lazy_adam_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LazyAdamModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::lazy_adam_conf() const {
  if (has_lazy_adam_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>*>(&(normal_mdupdt_.lazy_adam_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LazyAdamModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LazyAdamModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_lazy_adam_conf() {
  if(!has_lazy_adam_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lazy_adam_conf_)) ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>(new ::oneflow::cfg::LazyAdamModelUpdateConf());
  }
  normal_mdupdt_case_ = kLazyAdamConf;
  ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>*>(&(normal_mdupdt_.lazy_adam_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lamb_conf
bool ConstOptimizerConf::_OptimizerConf_::has_lamb_conf() const {
  return normal_mdupdt_case() == kLambConf;
}
void ConstOptimizerConf::_OptimizerConf_::clear_lamb_conf() {
  if (has_lamb_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lamb_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LambModelUpdateConf& ConstOptimizerConf::_OptimizerConf_::lamb_conf() const {
  if (has_lamb_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>*>(&(normal_mdupdt_.lamb_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LambModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LambModelUpdateConf* ConstOptimizerConf::_OptimizerConf_::mutable_lamb_conf() {
  if(!has_lamb_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lamb_conf_)) ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>(new ::oneflow::cfg::LambModelUpdateConf());
  }
  normal_mdupdt_case_ = kLambConf;
  ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>*>(&(normal_mdupdt_.lamb_conf_));
  return  (*ptr).get();
}
ConstOptimizerConf::NormalMdupdtCase ConstOptimizerConf::_OptimizerConf_::normal_mdupdt_case() const {
  return normal_mdupdt_case_;
}
bool ConstOptimizerConf::_OptimizerConf_::has_normal_mdupdt() const {
  return normal_mdupdt_case_ != NORMAL_MDUPDT_NOT_SET;
}
void ConstOptimizerConf::_OptimizerConf_::clear_normal_mdupdt() {
  switch (normal_mdupdt_case()) {
    case kNaiveConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.naive_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMomentumConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.momentum_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRmspropConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.rmsprop_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLarsConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lars_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAdamConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.adam_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLazyAdamConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lazy_adam_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLambConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lamb_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
  normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
}
void ConstOptimizerConf::_OptimizerConf_::normal_mdupdt_copy_from(const _OptimizerConf_& other) {
  switch (other.normal_mdupdt_case()) {
    case kNaiveConf: {
      mutable_naive_conf()->CopyFrom(other.naive_conf());
      break;
    }
    case kMomentumConf: {
      mutable_momentum_conf()->CopyFrom(other.momentum_conf());
      break;
    }
    case kRmspropConf: {
      mutable_rmsprop_conf()->CopyFrom(other.rmsprop_conf());
      break;
    }
    case kLarsConf: {
      mutable_lars_conf()->CopyFrom(other.lars_conf());
      break;
    }
    case kAdamConf: {
      mutable_adam_conf()->CopyFrom(other.adam_conf());
      break;
    }
    case kLazyAdamConf: {
      mutable_lazy_adam_conf()->CopyFrom(other.lazy_adam_conf());
      break;
    }
    case kLambConf: {
      mutable_lamb_conf()->CopyFrom(other.lamb_conf());
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      clear_normal_mdupdt();
    }
  }
}


int ConstOptimizerConf::_OptimizerConf_::compare(const _OptimizerConf_& other) {
  if (!(variable_op_names() == other.variable_op_names())) {
    return variable_op_names() < other.variable_op_names() ? -1 : 1;
  }
  if (!(has_base_learning_rate() == other.has_base_learning_rate())) {
    return has_base_learning_rate() < other.has_base_learning_rate() ? -1 : 1;
  } else if (!(base_learning_rate() == other.base_learning_rate())) {
    return base_learning_rate() < other.base_learning_rate() ? -1 : 1;
  }
  if (!(has_warmup_conf() == other.has_warmup_conf())) {
    return has_warmup_conf() < other.has_warmup_conf() ? -1 : 1;
  } else if (!(warmup_conf() == other.warmup_conf())) {
    return warmup_conf() < other.warmup_conf() ? -1 : 1;
  }
  if (!(has_learning_rate_decay() == other.has_learning_rate_decay())) {
    return has_learning_rate_decay() < other.has_learning_rate_decay() ? -1 : 1;
  } else if (!(learning_rate_decay() == other.learning_rate_decay())) {
    return learning_rate_decay() < other.learning_rate_decay() ? -1 : 1;
  }
  if (!(has_learning_rate_lbn() == other.has_learning_rate_lbn())) {
    return has_learning_rate_lbn() < other.has_learning_rate_lbn() ? -1 : 1;
  } else if (!(learning_rate_lbn() == other.learning_rate_lbn())) {
    return learning_rate_lbn() < other.learning_rate_lbn() ? -1 : 1;
  }
  if (!(has_clip_conf() == other.has_clip_conf())) {
    return has_clip_conf() < other.has_clip_conf() ? -1 : 1;
  } else if (!(clip_conf() == other.clip_conf())) {
    return clip_conf() < other.clip_conf() ? -1 : 1;
  }
  if (!(has_weight_decay_conf() == other.has_weight_decay_conf())) {
    return has_weight_decay_conf() < other.has_weight_decay_conf() ? -1 : 1;
  } else if (!(weight_decay_conf() == other.weight_decay_conf())) {
    return weight_decay_conf() < other.weight_decay_conf() ? -1 : 1;
  }
  if (!(normal_mdupdt_case() == other.normal_mdupdt_case())) {
    return normal_mdupdt_case() < other.normal_mdupdt_case() ? -1 : 1;
  }
  switch (normal_mdupdt_case()) {
    case kNaiveConf: {
      if (!(naive_conf() == other.naive_conf())) {
        return naive_conf() < other.naive_conf() ? -1 : 1;
      }
      break;
    }
    case kMomentumConf: {
      if (!(momentum_conf() == other.momentum_conf())) {
        return momentum_conf() < other.momentum_conf() ? -1 : 1;
      }
      break;
    }
    case kRmspropConf: {
      if (!(rmsprop_conf() == other.rmsprop_conf())) {
        return rmsprop_conf() < other.rmsprop_conf() ? -1 : 1;
      }
      break;
    }
    case kLarsConf: {
      if (!(lars_conf() == other.lars_conf())) {
        return lars_conf() < other.lars_conf() ? -1 : 1;
      }
      break;
    }
    case kAdamConf: {
      if (!(adam_conf() == other.adam_conf())) {
        return adam_conf() < other.adam_conf() ? -1 : 1;
      }
      break;
    }
    case kLazyAdamConf: {
      if (!(lazy_adam_conf() == other.lazy_adam_conf())) {
        return lazy_adam_conf() < other.lazy_adam_conf() ? -1 : 1;
      }
      break;
    }
    case kLambConf: {
      if (!(lamb_conf() == other.lamb_conf())) {
        return lamb_conf() < other.lamb_conf() ? -1 : 1;
      }
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstOptimizerConf::_OptimizerConf_::operator==(const _OptimizerConf_& other) const {
  return true
    && variable_op_names() == other.variable_op_names()
    && has_base_learning_rate() == other.has_base_learning_rate() 
    && base_learning_rate() == other.base_learning_rate()
    && has_warmup_conf() == other.has_warmup_conf() 
    && warmup_conf() == other.warmup_conf()
    && has_learning_rate_decay() == other.has_learning_rate_decay() 
    && learning_rate_decay() == other.learning_rate_decay()
    && has_learning_rate_lbn() == other.has_learning_rate_lbn() 
    && learning_rate_lbn() == other.learning_rate_lbn()
    && has_clip_conf() == other.has_clip_conf() 
    && clip_conf() == other.clip_conf()
    && has_weight_decay_conf() == other.has_weight_decay_conf() 
    && weight_decay_conf() == other.weight_decay_conf()
    && normal_mdupdt_case() == other.normal_mdupdt_case()
    && (normal_mdupdt_case() == kNaiveConf ? 
      naive_conf() == other.naive_conf() : true)
    && (normal_mdupdt_case() == kMomentumConf ? 
      momentum_conf() == other.momentum_conf() : true)
    && (normal_mdupdt_case() == kRmspropConf ? 
      rmsprop_conf() == other.rmsprop_conf() : true)
    && (normal_mdupdt_case() == kLarsConf ? 
      lars_conf() == other.lars_conf() : true)
    && (normal_mdupdt_case() == kAdamConf ? 
      adam_conf() == other.adam_conf() : true)
    && (normal_mdupdt_case() == kLazyAdamConf ? 
      lazy_adam_conf() == other.lazy_adam_conf() : true)
    && (normal_mdupdt_case() == kLambConf ? 
      lamb_conf() == other.lamb_conf() : true)
  ;
}

std::size_t ConstOptimizerConf::_OptimizerConf_::__CalcHash__() const {
  return 0
    ^ variable_op_names().__CalcHash__()
    ^ (has_base_learning_rate() ? std::hash<float>()(base_learning_rate()) : 0)
    ^ (has_warmup_conf() ? std::hash<::oneflow::cfg::WarmupConf>()(warmup_conf()) : 0)
    ^ (has_learning_rate_decay() ? std::hash<::oneflow::cfg::LearningRateDecayConf>()(learning_rate_decay()) : 0)
    ^ (has_learning_rate_lbn() ? std::hash<::std::string>()(learning_rate_lbn()) : 0)
    ^ (has_clip_conf() ? std::hash<::oneflow::cfg::ClipConf>()(clip_conf()) : 0)
    ^ (has_weight_decay_conf() ? std::hash<::oneflow::cfg::WeightDecayConf>()(weight_decay_conf()) : 0)
    ^ static_cast<std::size_t>(normal_mdupdt_case())
    ^ (has_naive_conf() ? std::hash<::oneflow::cfg::NaiveModelUpdateConf>()(naive_conf()) : 0)
    ^ (has_momentum_conf() ? std::hash<::oneflow::cfg::MomentumModelUpdateConf>()(momentum_conf()) : 0)
    ^ (has_rmsprop_conf() ? std::hash<::oneflow::cfg::RMSPropModelUpdateConf>()(rmsprop_conf()) : 0)
    ^ (has_lars_conf() ? std::hash<::oneflow::cfg::LARSModelUpdateConf>()(lars_conf()) : 0)
    ^ (has_adam_conf() ? std::hash<::oneflow::cfg::AdamModelUpdateConf>()(adam_conf()) : 0)
    ^ (has_lazy_adam_conf() ? std::hash<::oneflow::cfg::LazyAdamModelUpdateConf>()(lazy_adam_conf()) : 0)
    ^ (has_lamb_conf() ? std::hash<::oneflow::cfg::LambModelUpdateConf>()(lamb_conf()) : 0)
  ;
}

bool ConstOptimizerConf::_OptimizerConf_::operator<(const _OptimizerConf_& other) const {
  return false
    || !(variable_op_names() == other.variable_op_names()) ? 
      variable_op_names() < other.variable_op_names() : false
    || !(has_base_learning_rate() == other.has_base_learning_rate()) ? 
      has_base_learning_rate() < other.has_base_learning_rate() : false
    || !(base_learning_rate() == other.base_learning_rate()) ? 
      base_learning_rate() < other.base_learning_rate() : false
    || !(has_warmup_conf() == other.has_warmup_conf()) ? 
      has_warmup_conf() < other.has_warmup_conf() : false
    || !(warmup_conf() == other.warmup_conf()) ? 
      warmup_conf() < other.warmup_conf() : false
    || !(has_learning_rate_decay() == other.has_learning_rate_decay()) ? 
      has_learning_rate_decay() < other.has_learning_rate_decay() : false
    || !(learning_rate_decay() == other.learning_rate_decay()) ? 
      learning_rate_decay() < other.learning_rate_decay() : false
    || !(has_learning_rate_lbn() == other.has_learning_rate_lbn()) ? 
      has_learning_rate_lbn() < other.has_learning_rate_lbn() : false
    || !(learning_rate_lbn() == other.learning_rate_lbn()) ? 
      learning_rate_lbn() < other.learning_rate_lbn() : false
    || !(has_clip_conf() == other.has_clip_conf()) ? 
      has_clip_conf() < other.has_clip_conf() : false
    || !(clip_conf() == other.clip_conf()) ? 
      clip_conf() < other.clip_conf() : false
    || !(has_weight_decay_conf() == other.has_weight_decay_conf()) ? 
      has_weight_decay_conf() < other.has_weight_decay_conf() : false
    || !(weight_decay_conf() == other.weight_decay_conf()) ? 
      weight_decay_conf() < other.weight_decay_conf() : false
    || !(normal_mdupdt_case() == other.normal_mdupdt_case()) ? 
      normal_mdupdt_case() < other.normal_mdupdt_case() : false
    || ((normal_mdupdt_case() == kNaiveConf) && 
      !(naive_conf() == other.naive_conf())) ? 
        naive_conf() < other.naive_conf() : false
    || ((normal_mdupdt_case() == kMomentumConf) && 
      !(momentum_conf() == other.momentum_conf())) ? 
        momentum_conf() < other.momentum_conf() : false
    || ((normal_mdupdt_case() == kRmspropConf) && 
      !(rmsprop_conf() == other.rmsprop_conf())) ? 
        rmsprop_conf() < other.rmsprop_conf() : false
    || ((normal_mdupdt_case() == kLarsConf) && 
      !(lars_conf() == other.lars_conf())) ? 
        lars_conf() < other.lars_conf() : false
    || ((normal_mdupdt_case() == kAdamConf) && 
      !(adam_conf() == other.adam_conf())) ? 
        adam_conf() < other.adam_conf() : false
    || ((normal_mdupdt_case() == kLazyAdamConf) && 
      !(lazy_adam_conf() == other.lazy_adam_conf())) ? 
        lazy_adam_conf() < other.lazy_adam_conf() : false
    || ((normal_mdupdt_case() == kLambConf) && 
      !(lamb_conf() == other.lamb_conf())) ? 
        lamb_conf() < other.lamb_conf() : false
  ;
}

using _OptimizerConf_ =  ConstOptimizerConf::_OptimizerConf_;
ConstOptimizerConf::ConstOptimizerConf(const ::std::shared_ptr<_OptimizerConf_>& data): data_(data) {}
ConstOptimizerConf::ConstOptimizerConf(): data_(::std::make_shared<_OptimizerConf_>()) {}
ConstOptimizerConf::ConstOptimizerConf(const ::oneflow::OptimizerConf& proto_optimizerconf) {
  BuildFromProto(proto_optimizerconf);
}
ConstOptimizerConf::ConstOptimizerConf(const ConstOptimizerConf&) = default;
ConstOptimizerConf::ConstOptimizerConf(ConstOptimizerConf&&) noexcept = default;
ConstOptimizerConf::~ConstOptimizerConf() = default;

void ConstOptimizerConf::ToProto(PbMessage* proto_optimizerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OptimizerConf*>(proto_optimizerconf));
}
  
::std::string ConstOptimizerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOptimizerConf::__Empty__() const {
  return !data_;
}

int ConstOptimizerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"variable_op_names", 1},
    {"base_learning_rate", 2},
    {"warmup_conf", 3},
    {"learning_rate_decay", 4},
    {"learning_rate_lbn", 5},
    {"clip_conf", 6},
    {"weight_decay_conf", 7},
    {"naive_conf", 1000},
    {"momentum_conf", 1001},
    {"rmsprop_conf", 1002},
    {"lars_conf", 1003},
    {"adam_conf", 1004},
    {"lazy_adam_conf", 1005},
    {"lamb_conf", 1006},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOptimizerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 1000:
    case 1001:
    case 1002:
    case 1003:
    case 1004:
    case 1005:
    case 1006:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOptimizerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WarmupConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWarmupConf),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LearningRateDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLearningRateDecayConf),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClipConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClipConf),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WeightDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWeightDecayConf),
      };
      return type_indices;
    }
    case 1000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::NaiveModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstNaiveModelUpdateConf),
      };
      return type_indices;
    }
    case 1001: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MomentumModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMomentumModelUpdateConf),
      };
      return type_indices;
    }
    case 1002: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RMSPropModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRMSPropModelUpdateConf),
      };
      return type_indices;
    }
    case 1003: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LARSModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLARSModelUpdateConf),
      };
      return type_indices;
    }
    case 1004: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AdamModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAdamModelUpdateConf),
      };
      return type_indices;
    }
    case 1005: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LazyAdamModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLazyAdamModelUpdateConf),
      };
      return type_indices;
    }
    case 1006: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LambModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLambModelUpdateConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOptimizerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &variable_op_names();
    case 2: return &base_learning_rate();
    case 3: return &warmup_conf();
    case 4: return &learning_rate_decay();
    case 5: return &learning_rate_lbn();
    case 6: return &clip_conf();
    case 7: return &weight_decay_conf();
    case 1000: return &naive_conf();
    case 1001: return &momentum_conf();
    case 1002: return &rmsprop_conf();
    case 1003: return &lars_conf();
    case 1004: return &adam_conf();
    case 1005: return &lazy_adam_conf();
    case 1006: return &lamb_conf();
    default: return nullptr;
  }
}

// repeated field variable_op_names
::std::size_t ConstOptimizerConf::variable_op_names_size() const {
  return __SharedPtrOrDefault__()->variable_op_names_size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstOptimizerConf::variable_op_names() const {
  return __SharedPtrOrDefault__()->variable_op_names();
}
const ::std::string& ConstOptimizerConf::variable_op_names(::std::size_t index) const {
  return __SharedPtrOrDefault__()->variable_op_names(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> ConstOptimizerConf::shared_const_variable_op_names() const {
  return variable_op_names().__SharedConst__();
}
// required or optional field base_learning_rate
bool ConstOptimizerConf::has_base_learning_rate() const {
  return __SharedPtrOrDefault__()->has_base_learning_rate();
}
const float& ConstOptimizerConf::base_learning_rate() const {
  return __SharedPtrOrDefault__()->base_learning_rate();
}
// used by pybind11 only
// required or optional field warmup_conf
bool ConstOptimizerConf::has_warmup_conf() const {
  return __SharedPtrOrDefault__()->has_warmup_conf();
}
const ::oneflow::cfg::WarmupConf& ConstOptimizerConf::warmup_conf() const {
  return __SharedPtrOrDefault__()->warmup_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWarmupConf> ConstOptimizerConf::shared_const_warmup_conf() const {
  return warmup_conf().__SharedConst__();
}
// required or optional field learning_rate_decay
bool ConstOptimizerConf::has_learning_rate_decay() const {
  return __SharedPtrOrDefault__()->has_learning_rate_decay();
}
const ::oneflow::cfg::LearningRateDecayConf& ConstOptimizerConf::learning_rate_decay() const {
  return __SharedPtrOrDefault__()->learning_rate_decay();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLearningRateDecayConf> ConstOptimizerConf::shared_const_learning_rate_decay() const {
  return learning_rate_decay().__SharedConst__();
}
// required or optional field learning_rate_lbn
bool ConstOptimizerConf::has_learning_rate_lbn() const {
  return __SharedPtrOrDefault__()->has_learning_rate_lbn();
}
const ::std::string& ConstOptimizerConf::learning_rate_lbn() const {
  return __SharedPtrOrDefault__()->learning_rate_lbn();
}
// used by pybind11 only
// required or optional field clip_conf
bool ConstOptimizerConf::has_clip_conf() const {
  return __SharedPtrOrDefault__()->has_clip_conf();
}
const ::oneflow::cfg::ClipConf& ConstOptimizerConf::clip_conf() const {
  return __SharedPtrOrDefault__()->clip_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClipConf> ConstOptimizerConf::shared_const_clip_conf() const {
  return clip_conf().__SharedConst__();
}
// required or optional field weight_decay_conf
bool ConstOptimizerConf::has_weight_decay_conf() const {
  return __SharedPtrOrDefault__()->has_weight_decay_conf();
}
const ::oneflow::cfg::WeightDecayConf& ConstOptimizerConf::weight_decay_conf() const {
  return __SharedPtrOrDefault__()->weight_decay_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWeightDecayConf> ConstOptimizerConf::shared_const_weight_decay_conf() const {
  return weight_decay_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: naive_conf
bool ConstOptimizerConf::has_naive_conf() const {
  return __SharedPtrOrDefault__()->has_naive_conf();
}
const ::oneflow::cfg::NaiveModelUpdateConf& ConstOptimizerConf::naive_conf() const {
  return __SharedPtrOrDefault__()->naive_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstNaiveModelUpdateConf> ConstOptimizerConf::shared_const_naive_conf() const {
  return naive_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: momentum_conf
bool ConstOptimizerConf::has_momentum_conf() const {
  return __SharedPtrOrDefault__()->has_momentum_conf();
}
const ::oneflow::cfg::MomentumModelUpdateConf& ConstOptimizerConf::momentum_conf() const {
  return __SharedPtrOrDefault__()->momentum_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMomentumModelUpdateConf> ConstOptimizerConf::shared_const_momentum_conf() const {
  return momentum_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: rmsprop_conf
bool ConstOptimizerConf::has_rmsprop_conf() const {
  return __SharedPtrOrDefault__()->has_rmsprop_conf();
}
const ::oneflow::cfg::RMSPropModelUpdateConf& ConstOptimizerConf::rmsprop_conf() const {
  return __SharedPtrOrDefault__()->rmsprop_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRMSPropModelUpdateConf> ConstOptimizerConf::shared_const_rmsprop_conf() const {
  return rmsprop_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lars_conf
bool ConstOptimizerConf::has_lars_conf() const {
  return __SharedPtrOrDefault__()->has_lars_conf();
}
const ::oneflow::cfg::LARSModelUpdateConf& ConstOptimizerConf::lars_conf() const {
  return __SharedPtrOrDefault__()->lars_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLARSModelUpdateConf> ConstOptimizerConf::shared_const_lars_conf() const {
  return lars_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: adam_conf
bool ConstOptimizerConf::has_adam_conf() const {
  return __SharedPtrOrDefault__()->has_adam_conf();
}
const ::oneflow::cfg::AdamModelUpdateConf& ConstOptimizerConf::adam_conf() const {
  return __SharedPtrOrDefault__()->adam_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAdamModelUpdateConf> ConstOptimizerConf::shared_const_adam_conf() const {
  return adam_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lazy_adam_conf
bool ConstOptimizerConf::has_lazy_adam_conf() const {
  return __SharedPtrOrDefault__()->has_lazy_adam_conf();
}
const ::oneflow::cfg::LazyAdamModelUpdateConf& ConstOptimizerConf::lazy_adam_conf() const {
  return __SharedPtrOrDefault__()->lazy_adam_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLazyAdamModelUpdateConf> ConstOptimizerConf::shared_const_lazy_adam_conf() const {
  return lazy_adam_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lamb_conf
bool ConstOptimizerConf::has_lamb_conf() const {
  return __SharedPtrOrDefault__()->has_lamb_conf();
}
const ::oneflow::cfg::LambModelUpdateConf& ConstOptimizerConf::lamb_conf() const {
  return __SharedPtrOrDefault__()->lamb_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLambModelUpdateConf> ConstOptimizerConf::shared_const_lamb_conf() const {
  return lamb_conf().__SharedConst__();
}
ConstOptimizerConf::NormalMdupdtCase ConstOptimizerConf::normal_mdupdt_case() const {
  return __SharedPtrOrDefault__()->normal_mdupdt_case();
}

bool ConstOptimizerConf::has_normal_mdupdt() const {
  return __SharedPtrOrDefault__()->has_normal_mdupdt();
}

::std::shared_ptr<ConstOptimizerConf> ConstOptimizerConf::__SharedConst__() const {
  return ::std::make_shared<ConstOptimizerConf>(data_);
}
int64_t ConstOptimizerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOptimizerConf::operator==(const ConstOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOptimizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOptimizerConf::operator<(const ConstOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OptimizerConf_>& ConstOptimizerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OptimizerConf_> default_ptr = std::make_shared<_OptimizerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_OptimizerConf_>& ConstOptimizerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _OptimizerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOptimizerConf
void ConstOptimizerConf::BuildFromProto(const PbMessage& proto_optimizerconf) {
  data_ = ::std::make_shared<_OptimizerConf_>(dynamic_cast<const ::oneflow::OptimizerConf&>(proto_optimizerconf));
}

OptimizerConf::OptimizerConf(const ::std::shared_ptr<_OptimizerConf_>& data)
  : ConstOptimizerConf(data) {}
OptimizerConf::OptimizerConf(const OptimizerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OptimizerConf> resize
OptimizerConf::OptimizerConf(OptimizerConf&&) noexcept = default; 
OptimizerConf::OptimizerConf(const ::oneflow::OptimizerConf& proto_optimizerconf) {
  InitFromProto(proto_optimizerconf);
}
OptimizerConf::OptimizerConf() = default;

OptimizerConf::~OptimizerConf() = default;

void OptimizerConf::InitFromProto(const PbMessage& proto_optimizerconf) {
  BuildFromProto(proto_optimizerconf);
}
  
void* OptimizerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_variable_op_names();
    case 2: return mutable_base_learning_rate();
    case 3: return mutable_warmup_conf();
    case 4: return mutable_learning_rate_decay();
    case 5: return mutable_learning_rate_lbn();
    case 6: return mutable_clip_conf();
    case 7: return mutable_weight_decay_conf();
    case 1000: return mutable_naive_conf();
    case 1001: return mutable_momentum_conf();
    case 1002: return mutable_rmsprop_conf();
    case 1003: return mutable_lars_conf();
    case 1004: return mutable_adam_conf();
    case 1005: return mutable_lazy_adam_conf();
    case 1006: return mutable_lamb_conf();
    default: return nullptr;
  }
}

bool OptimizerConf::operator==(const OptimizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OptimizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OptimizerConf::operator<(const OptimizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OptimizerConf::Clear() {
  if (data_) { data_.reset(); }
}
void OptimizerConf::CopyFrom(const OptimizerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OptimizerConf& OptimizerConf::operator=(const OptimizerConf& other) {
  CopyFrom(other);
  return *this;
}

// repeated field variable_op_names
void OptimizerConf::clear_variable_op_names() {
  return __SharedPtr__()->clear_variable_op_names();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* OptimizerConf::mutable_variable_op_names() {
  return __SharedPtr__()->mutable_variable_op_names();
}
::std::string* OptimizerConf::mutable_variable_op_names(::std::size_t index) {
  return __SharedPtr__()->mutable_variable_op_names(index);
}
void OptimizerConf::add_variable_op_names(const ::std::string& value) {
  return __SharedPtr__()->add_variable_op_names(value);
}
void OptimizerConf::set_variable_op_names(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_variable_op_names(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> OptimizerConf::shared_mutable_variable_op_names() {
  return mutable_variable_op_names()->__SharedMutable__();
}
// required or optional field base_learning_rate
void OptimizerConf::clear_base_learning_rate() {
  return __SharedPtr__()->clear_base_learning_rate();
}
void OptimizerConf::set_base_learning_rate(const float& value) {
  return __SharedPtr__()->set_base_learning_rate(value);
}
float* OptimizerConf::mutable_base_learning_rate() {
  return  __SharedPtr__()->mutable_base_learning_rate();
}
// required or optional field warmup_conf
void OptimizerConf::clear_warmup_conf() {
  return __SharedPtr__()->clear_warmup_conf();
}
::oneflow::cfg::WarmupConf* OptimizerConf::mutable_warmup_conf() {
  return __SharedPtr__()->mutable_warmup_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WarmupConf> OptimizerConf::shared_mutable_warmup_conf() {
  return mutable_warmup_conf()->__SharedMutable__();
}
// required or optional field learning_rate_decay
void OptimizerConf::clear_learning_rate_decay() {
  return __SharedPtr__()->clear_learning_rate_decay();
}
::oneflow::cfg::LearningRateDecayConf* OptimizerConf::mutable_learning_rate_decay() {
  return __SharedPtr__()->mutable_learning_rate_decay();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> OptimizerConf::shared_mutable_learning_rate_decay() {
  return mutable_learning_rate_decay()->__SharedMutable__();
}
// required or optional field learning_rate_lbn
void OptimizerConf::clear_learning_rate_lbn() {
  return __SharedPtr__()->clear_learning_rate_lbn();
}
void OptimizerConf::set_learning_rate_lbn(const ::std::string& value) {
  return __SharedPtr__()->set_learning_rate_lbn(value);
}
::std::string* OptimizerConf::mutable_learning_rate_lbn() {
  return  __SharedPtr__()->mutable_learning_rate_lbn();
}
// required or optional field clip_conf
void OptimizerConf::clear_clip_conf() {
  return __SharedPtr__()->clear_clip_conf();
}
::oneflow::cfg::ClipConf* OptimizerConf::mutable_clip_conf() {
  return __SharedPtr__()->mutable_clip_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClipConf> OptimizerConf::shared_mutable_clip_conf() {
  return mutable_clip_conf()->__SharedMutable__();
}
// required or optional field weight_decay_conf
void OptimizerConf::clear_weight_decay_conf() {
  return __SharedPtr__()->clear_weight_decay_conf();
}
::oneflow::cfg::WeightDecayConf* OptimizerConf::mutable_weight_decay_conf() {
  return __SharedPtr__()->mutable_weight_decay_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WeightDecayConf> OptimizerConf::shared_mutable_weight_decay_conf() {
  return mutable_weight_decay_conf()->__SharedMutable__();
}
void OptimizerConf::clear_naive_conf() {
  return __SharedPtr__()->clear_naive_conf();
}
::oneflow::cfg::NaiveModelUpdateConf* OptimizerConf::mutable_naive_conf() {
  return __SharedPtr__()->mutable_naive_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> OptimizerConf::shared_mutable_naive_conf() {
  return mutable_naive_conf()->__SharedMutable__();
}
void OptimizerConf::clear_momentum_conf() {
  return __SharedPtr__()->clear_momentum_conf();
}
::oneflow::cfg::MomentumModelUpdateConf* OptimizerConf::mutable_momentum_conf() {
  return __SharedPtr__()->mutable_momentum_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> OptimizerConf::shared_mutable_momentum_conf() {
  return mutable_momentum_conf()->__SharedMutable__();
}
void OptimizerConf::clear_rmsprop_conf() {
  return __SharedPtr__()->clear_rmsprop_conf();
}
::oneflow::cfg::RMSPropModelUpdateConf* OptimizerConf::mutable_rmsprop_conf() {
  return __SharedPtr__()->mutable_rmsprop_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> OptimizerConf::shared_mutable_rmsprop_conf() {
  return mutable_rmsprop_conf()->__SharedMutable__();
}
void OptimizerConf::clear_lars_conf() {
  return __SharedPtr__()->clear_lars_conf();
}
::oneflow::cfg::LARSModelUpdateConf* OptimizerConf::mutable_lars_conf() {
  return __SharedPtr__()->mutable_lars_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> OptimizerConf::shared_mutable_lars_conf() {
  return mutable_lars_conf()->__SharedMutable__();
}
void OptimizerConf::clear_adam_conf() {
  return __SharedPtr__()->clear_adam_conf();
}
::oneflow::cfg::AdamModelUpdateConf* OptimizerConf::mutable_adam_conf() {
  return __SharedPtr__()->mutable_adam_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> OptimizerConf::shared_mutable_adam_conf() {
  return mutable_adam_conf()->__SharedMutable__();
}
void OptimizerConf::clear_lazy_adam_conf() {
  return __SharedPtr__()->clear_lazy_adam_conf();
}
::oneflow::cfg::LazyAdamModelUpdateConf* OptimizerConf::mutable_lazy_adam_conf() {
  return __SharedPtr__()->mutable_lazy_adam_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> OptimizerConf::shared_mutable_lazy_adam_conf() {
  return mutable_lazy_adam_conf()->__SharedMutable__();
}
void OptimizerConf::clear_lamb_conf() {
  return __SharedPtr__()->clear_lamb_conf();
}
::oneflow::cfg::LambModelUpdateConf* OptimizerConf::mutable_lamb_conf() {
  return __SharedPtr__()->mutable_lamb_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> OptimizerConf::shared_mutable_lamb_conf() {
  return mutable_lamb_conf()->__SharedMutable__();
}

::std::shared_ptr<OptimizerConf> OptimizerConf::__SharedMutable__() {
  return ::std::make_shared<OptimizerConf>(__SharedPtr__());
}
ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::_NormalModelUpdateOpUserConf_() { Clear(); }
ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::_NormalModelUpdateOpUserConf_(const _NormalModelUpdateOpUserConf_& other) { CopyFrom(other); }
ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::_NormalModelUpdateOpUserConf_(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf) {
  InitFromProto(proto_normalmodelupdateopuserconf);
}
ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::_NormalModelUpdateOpUserConf_(_NormalModelUpdateOpUserConf_&& other) = default;
ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::~_NormalModelUpdateOpUserConf_() = default;

void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::InitFromProto(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf) {
  Clear();
  // required_or_optional field: learning_rate_decay
  if (proto_normalmodelupdateopuserconf.has_learning_rate_decay()) {
  *mutable_learning_rate_decay() = ::oneflow::cfg::LearningRateDecayConf(proto_normalmodelupdateopuserconf.learning_rate_decay());      
  }
  // required_or_optional field: warmup_conf
  if (proto_normalmodelupdateopuserconf.has_warmup_conf()) {
  *mutable_warmup_conf() = ::oneflow::cfg::WarmupConf(proto_normalmodelupdateopuserconf.warmup_conf());      
  }
  // required_or_optional field: clip_conf
  if (proto_normalmodelupdateopuserconf.has_clip_conf()) {
  *mutable_clip_conf() = ::oneflow::cfg::ClipConf(proto_normalmodelupdateopuserconf.clip_conf());      
  }
  // required_or_optional field: weight_decay_conf
  if (proto_normalmodelupdateopuserconf.has_weight_decay_conf()) {
  *mutable_weight_decay_conf() = ::oneflow::cfg::WeightDecayConf(proto_normalmodelupdateopuserconf.weight_decay_conf());      
  }
  // oneof field: normal_mdupdt
  NormalMdupdtCase normal_mdupdt_case = NormalMdupdtCase(int(proto_normalmodelupdateopuserconf.normal_mdupdt_case()));
  switch (normal_mdupdt_case) {
    case kNaiveConf: {
      *mutable_naive_conf() = ::oneflow::cfg::NaiveModelUpdateConf(proto_normalmodelupdateopuserconf.naive_conf());
      break;
  }
    case kMomentumConf: {
      *mutable_momentum_conf() = ::oneflow::cfg::MomentumModelUpdateConf(proto_normalmodelupdateopuserconf.momentum_conf());
      break;
  }
    case kRmspropConf: {
      *mutable_rmsprop_conf() = ::oneflow::cfg::RMSPropModelUpdateConf(proto_normalmodelupdateopuserconf.rmsprop_conf());
      break;
  }
    case kLarsConf: {
      *mutable_lars_conf() = ::oneflow::cfg::LARSModelUpdateConf(proto_normalmodelupdateopuserconf.lars_conf());
      break;
  }
    case kAdamConf: {
      *mutable_adam_conf() = ::oneflow::cfg::AdamModelUpdateConf(proto_normalmodelupdateopuserconf.adam_conf());
      break;
  }
    case kLazyAdamConf: {
      *mutable_lazy_adam_conf() = ::oneflow::cfg::LazyAdamModelUpdateConf(proto_normalmodelupdateopuserconf.lazy_adam_conf());
      break;
  }
    case kLambConf: {
      *mutable_lamb_conf() = ::oneflow::cfg::LambModelUpdateConf(proto_normalmodelupdateopuserconf.lamb_conf());
      break;
  }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
      
}

void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::ToProto(::oneflow::NormalModelUpdateOpUserConf* proto_normalmodelupdateopuserconf) const {
  proto_normalmodelupdateopuserconf->Clear();
  // required_or_optional field: learning_rate_decay
  if (this->has_learning_rate_decay()) {
    ::oneflow::LearningRateDecayConf proto_learning_rate_decay;
    learning_rate_decay().ToProto(&proto_learning_rate_decay);
    proto_normalmodelupdateopuserconf->mutable_learning_rate_decay()->CopyFrom(proto_learning_rate_decay);
    }
  // required_or_optional field: warmup_conf
  if (this->has_warmup_conf()) {
    ::oneflow::WarmupConf proto_warmup_conf;
    warmup_conf().ToProto(&proto_warmup_conf);
    proto_normalmodelupdateopuserconf->mutable_warmup_conf()->CopyFrom(proto_warmup_conf);
    }
  // required_or_optional field: clip_conf
  if (this->has_clip_conf()) {
    ::oneflow::ClipConf proto_clip_conf;
    clip_conf().ToProto(&proto_clip_conf);
    proto_normalmodelupdateopuserconf->mutable_clip_conf()->CopyFrom(proto_clip_conf);
    }
  // required_or_optional field: weight_decay_conf
  if (this->has_weight_decay_conf()) {
    ::oneflow::WeightDecayConf proto_weight_decay_conf;
    weight_decay_conf().ToProto(&proto_weight_decay_conf);
    proto_normalmodelupdateopuserconf->mutable_weight_decay_conf()->CopyFrom(proto_weight_decay_conf);
    }

  // oneof field: normal_mdupdt
  ::oneflow::NormalModelUpdateOpUserConf::NormalMdupdtCase normal_mdupdt_case = ::oneflow::NormalModelUpdateOpUserConf::NormalMdupdtCase(int(this->normal_mdupdt_case()));
  switch (normal_mdupdt_case) {
    case ::oneflow::NormalModelUpdateOpUserConf::kNaiveConf: {
      ::oneflow::NaiveModelUpdateConf of_proto_naive_conf;
      naive_conf().ToProto(&of_proto_naive_conf);
      proto_normalmodelupdateopuserconf->mutable_naive_conf()->CopyFrom(of_proto_naive_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kMomentumConf: {
      ::oneflow::MomentumModelUpdateConf of_proto_momentum_conf;
      momentum_conf().ToProto(&of_proto_momentum_conf);
      proto_normalmodelupdateopuserconf->mutable_momentum_conf()->CopyFrom(of_proto_momentum_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kRmspropConf: {
      ::oneflow::RMSPropModelUpdateConf of_proto_rmsprop_conf;
      rmsprop_conf().ToProto(&of_proto_rmsprop_conf);
      proto_normalmodelupdateopuserconf->mutable_rmsprop_conf()->CopyFrom(of_proto_rmsprop_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kLarsConf: {
      ::oneflow::LARSModelUpdateConf of_proto_lars_conf;
      lars_conf().ToProto(&of_proto_lars_conf);
      proto_normalmodelupdateopuserconf->mutable_lars_conf()->CopyFrom(of_proto_lars_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kAdamConf: {
      ::oneflow::AdamModelUpdateConf of_proto_adam_conf;
      adam_conf().ToProto(&of_proto_adam_conf);
      proto_normalmodelupdateopuserconf->mutable_adam_conf()->CopyFrom(of_proto_adam_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kLazyAdamConf: {
      ::oneflow::LazyAdamModelUpdateConf of_proto_lazy_adam_conf;
      lazy_adam_conf().ToProto(&of_proto_lazy_adam_conf);
      proto_normalmodelupdateopuserconf->mutable_lazy_adam_conf()->CopyFrom(of_proto_lazy_adam_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::kLambConf: {
      ::oneflow::LambModelUpdateConf of_proto_lamb_conf;
      lamb_conf().ToProto(&of_proto_lamb_conf);
      proto_normalmodelupdateopuserconf->mutable_lamb_conf()->CopyFrom(of_proto_lamb_conf);
      break;
    }
    case ::oneflow::NormalModelUpdateOpUserConf::NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
}

::std::string ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::DebugString() const {
  ::oneflow::NormalModelUpdateOpUserConf proto_normalmodelupdateopuserconf;
  this->ToProto(&proto_normalmodelupdateopuserconf);
  return proto_normalmodelupdateopuserconf.DebugString();
}

void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::Clear() {
  clear_learning_rate_decay();
  clear_warmup_conf();
  clear_clip_conf();
  clear_weight_decay_conf();
  clear_normal_mdupdt();
}

void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::CopyFrom(const _NormalModelUpdateOpUserConf_& other) {
  if (other.has_learning_rate_decay()) {
    mutable_learning_rate_decay()->CopyFrom(other.learning_rate_decay());
  } else {
    clear_learning_rate_decay();
  }
  if (other.has_warmup_conf()) {
    mutable_warmup_conf()->CopyFrom(other.warmup_conf());
  } else {
    clear_warmup_conf();
  }
  if (other.has_clip_conf()) {
    mutable_clip_conf()->CopyFrom(other.clip_conf());
  } else {
    clear_clip_conf();
  }
  if (other.has_weight_decay_conf()) {
    mutable_weight_decay_conf()->CopyFrom(other.weight_decay_conf());
  } else {
    clear_weight_decay_conf();
  }
  normal_mdupdt_copy_from(other);
}


// optional field learning_rate_decay
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_learning_rate_decay() const {
  return has_learning_rate_decay_;
}
const ::oneflow::cfg::LearningRateDecayConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::learning_rate_decay() const {
  if (!learning_rate_decay_) {
    static const ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::LearningRateDecayConf>();
    return *default_static_value;
  }
  return *(learning_rate_decay_.get());
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_learning_rate_decay() {
  if (learning_rate_decay_) {
    learning_rate_decay_->Clear();
  }
  has_learning_rate_decay_ = false;
}
::oneflow::cfg::LearningRateDecayConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_learning_rate_decay() {
  if (!learning_rate_decay_) {
    learning_rate_decay_ = ::std::make_shared<::oneflow::cfg::LearningRateDecayConf>();
  }
  has_learning_rate_decay_ = true;
  return learning_rate_decay_.get();
}

// optional field warmup_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_warmup_conf() const {
  return has_warmup_conf_;
}
const ::oneflow::cfg::WarmupConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::warmup_conf() const {
  if (!warmup_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::WarmupConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::WarmupConf>();
    return *default_static_value;
  }
  return *(warmup_conf_.get());
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_warmup_conf() {
  if (warmup_conf_) {
    warmup_conf_->Clear();
  }
  has_warmup_conf_ = false;
}
::oneflow::cfg::WarmupConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_warmup_conf() {
  if (!warmup_conf_) {
    warmup_conf_ = ::std::make_shared<::oneflow::cfg::WarmupConf>();
  }
  has_warmup_conf_ = true;
  return warmup_conf_.get();
}

// optional field clip_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_clip_conf() const {
  return has_clip_conf_;
}
const ::oneflow::cfg::ClipConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clip_conf() const {
  if (!clip_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ClipConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ClipConf>();
    return *default_static_value;
  }
  return *(clip_conf_.get());
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_clip_conf() {
  if (clip_conf_) {
    clip_conf_->Clear();
  }
  has_clip_conf_ = false;
}
::oneflow::cfg::ClipConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_clip_conf() {
  if (!clip_conf_) {
    clip_conf_ = ::std::make_shared<::oneflow::cfg::ClipConf>();
  }
  has_clip_conf_ = true;
  return clip_conf_.get();
}

// optional field weight_decay_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_weight_decay_conf() const {
  return has_weight_decay_conf_;
}
const ::oneflow::cfg::WeightDecayConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::weight_decay_conf() const {
  if (!weight_decay_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::WeightDecayConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::WeightDecayConf>();
    return *default_static_value;
  }
  return *(weight_decay_conf_.get());
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_weight_decay_conf() {
  if (weight_decay_conf_) {
    weight_decay_conf_->Clear();
  }
  has_weight_decay_conf_ = false;
}
::oneflow::cfg::WeightDecayConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_weight_decay_conf() {
  if (!weight_decay_conf_) {
    weight_decay_conf_ = ::std::make_shared<::oneflow::cfg::WeightDecayConf>();
  }
  has_weight_decay_conf_ = true;
  return weight_decay_conf_.get();
}

// oneof field normal_mdupdt: naive_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_naive_conf() const {
  return normal_mdupdt_case() == kNaiveConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_naive_conf() {
  if (has_naive_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.naive_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::NaiveModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::naive_conf() const {
  if (has_naive_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>*>(&(normal_mdupdt_.naive_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::NaiveModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::NaiveModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_naive_conf() {
  if(!has_naive_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.naive_conf_)) ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>(new ::oneflow::cfg::NaiveModelUpdateConf());
  }
  normal_mdupdt_case_ = kNaiveConf;
  ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>*>(&(normal_mdupdt_.naive_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: momentum_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_momentum_conf() const {
  return normal_mdupdt_case() == kMomentumConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_momentum_conf() {
  if (has_momentum_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.momentum_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::MomentumModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::momentum_conf() const {
  if (has_momentum_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>*>(&(normal_mdupdt_.momentum_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::MomentumModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::MomentumModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_momentum_conf() {
  if(!has_momentum_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.momentum_conf_)) ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>(new ::oneflow::cfg::MomentumModelUpdateConf());
  }
  normal_mdupdt_case_ = kMomentumConf;
  ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>*>(&(normal_mdupdt_.momentum_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: rmsprop_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_rmsprop_conf() const {
  return normal_mdupdt_case() == kRmspropConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_rmsprop_conf() {
  if (has_rmsprop_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.rmsprop_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::RMSPropModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::rmsprop_conf() const {
  if (has_rmsprop_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>*>(&(normal_mdupdt_.rmsprop_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::RMSPropModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::RMSPropModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_rmsprop_conf() {
  if(!has_rmsprop_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.rmsprop_conf_)) ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>(new ::oneflow::cfg::RMSPropModelUpdateConf());
  }
  normal_mdupdt_case_ = kRmspropConf;
  ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>*>(&(normal_mdupdt_.rmsprop_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lars_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_lars_conf() const {
  return normal_mdupdt_case() == kLarsConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_lars_conf() {
  if (has_lars_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lars_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LARSModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::lars_conf() const {
  if (has_lars_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>*>(&(normal_mdupdt_.lars_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LARSModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LARSModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_lars_conf() {
  if(!has_lars_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lars_conf_)) ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>(new ::oneflow::cfg::LARSModelUpdateConf());
  }
  normal_mdupdt_case_ = kLarsConf;
  ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>*>(&(normal_mdupdt_.lars_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: adam_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_adam_conf() const {
  return normal_mdupdt_case() == kAdamConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_adam_conf() {
  if (has_adam_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.adam_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::AdamModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::adam_conf() const {
  if (has_adam_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>*>(&(normal_mdupdt_.adam_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::AdamModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::AdamModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_adam_conf() {
  if(!has_adam_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.adam_conf_)) ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>(new ::oneflow::cfg::AdamModelUpdateConf());
  }
  normal_mdupdt_case_ = kAdamConf;
  ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>*>(&(normal_mdupdt_.adam_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lazy_adam_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_lazy_adam_conf() const {
  return normal_mdupdt_case() == kLazyAdamConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_lazy_adam_conf() {
  if (has_lazy_adam_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lazy_adam_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LazyAdamModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::lazy_adam_conf() const {
  if (has_lazy_adam_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>*>(&(normal_mdupdt_.lazy_adam_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LazyAdamModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LazyAdamModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_lazy_adam_conf() {
  if(!has_lazy_adam_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lazy_adam_conf_)) ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>(new ::oneflow::cfg::LazyAdamModelUpdateConf());
  }
  normal_mdupdt_case_ = kLazyAdamConf;
  ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>*>(&(normal_mdupdt_.lazy_adam_conf_));
  return  (*ptr).get();
}

// oneof field normal_mdupdt: lamb_conf
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_lamb_conf() const {
  return normal_mdupdt_case() == kLambConf;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_lamb_conf() {
  if (has_lamb_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lamb_conf_));
      ptr->~Shared_ptr();
    }
    normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
  }
}

const ::oneflow::cfg::LambModelUpdateConf& ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::lamb_conf() const {
  if (has_lamb_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>*>(&(normal_mdupdt_.lamb_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> default_static_value = ::std::make_shared<::oneflow::cfg::LambModelUpdateConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::LambModelUpdateConf* ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::mutable_lamb_conf() {
  if(!has_lamb_conf()) {
    clear_normal_mdupdt();
    new (&(normal_mdupdt_.lamb_conf_)) ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>(new ::oneflow::cfg::LambModelUpdateConf());
  }
  normal_mdupdt_case_ = kLambConf;
  ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>*>(&(normal_mdupdt_.lamb_conf_));
  return  (*ptr).get();
}
ConstNormalModelUpdateOpUserConf::NormalMdupdtCase ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::normal_mdupdt_case() const {
  return normal_mdupdt_case_;
}
bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::has_normal_mdupdt() const {
  return normal_mdupdt_case_ != NORMAL_MDUPDT_NOT_SET;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::clear_normal_mdupdt() {
  switch (normal_mdupdt_case()) {
    case kNaiveConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.naive_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMomentumConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.momentum_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kRmspropConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.rmsprop_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLarsConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lars_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAdamConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.adam_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLazyAdamConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lazy_adam_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kLambConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(normal_mdupdt_.lamb_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
  normal_mdupdt_case_ = NORMAL_MDUPDT_NOT_SET;
}
void ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::normal_mdupdt_copy_from(const _NormalModelUpdateOpUserConf_& other) {
  switch (other.normal_mdupdt_case()) {
    case kNaiveConf: {
      mutable_naive_conf()->CopyFrom(other.naive_conf());
      break;
    }
    case kMomentumConf: {
      mutable_momentum_conf()->CopyFrom(other.momentum_conf());
      break;
    }
    case kRmspropConf: {
      mutable_rmsprop_conf()->CopyFrom(other.rmsprop_conf());
      break;
    }
    case kLarsConf: {
      mutable_lars_conf()->CopyFrom(other.lars_conf());
      break;
    }
    case kAdamConf: {
      mutable_adam_conf()->CopyFrom(other.adam_conf());
      break;
    }
    case kLazyAdamConf: {
      mutable_lazy_adam_conf()->CopyFrom(other.lazy_adam_conf());
      break;
    }
    case kLambConf: {
      mutable_lamb_conf()->CopyFrom(other.lamb_conf());
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      clear_normal_mdupdt();
    }
  }
}


int ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::compare(const _NormalModelUpdateOpUserConf_& other) {
  if (!(has_learning_rate_decay() == other.has_learning_rate_decay())) {
    return has_learning_rate_decay() < other.has_learning_rate_decay() ? -1 : 1;
  } else if (!(learning_rate_decay() == other.learning_rate_decay())) {
    return learning_rate_decay() < other.learning_rate_decay() ? -1 : 1;
  }
  if (!(has_warmup_conf() == other.has_warmup_conf())) {
    return has_warmup_conf() < other.has_warmup_conf() ? -1 : 1;
  } else if (!(warmup_conf() == other.warmup_conf())) {
    return warmup_conf() < other.warmup_conf() ? -1 : 1;
  }
  if (!(has_clip_conf() == other.has_clip_conf())) {
    return has_clip_conf() < other.has_clip_conf() ? -1 : 1;
  } else if (!(clip_conf() == other.clip_conf())) {
    return clip_conf() < other.clip_conf() ? -1 : 1;
  }
  if (!(has_weight_decay_conf() == other.has_weight_decay_conf())) {
    return has_weight_decay_conf() < other.has_weight_decay_conf() ? -1 : 1;
  } else if (!(weight_decay_conf() == other.weight_decay_conf())) {
    return weight_decay_conf() < other.weight_decay_conf() ? -1 : 1;
  }
  if (!(normal_mdupdt_case() == other.normal_mdupdt_case())) {
    return normal_mdupdt_case() < other.normal_mdupdt_case() ? -1 : 1;
  }
  switch (normal_mdupdt_case()) {
    case kNaiveConf: {
      if (!(naive_conf() == other.naive_conf())) {
        return naive_conf() < other.naive_conf() ? -1 : 1;
      }
      break;
    }
    case kMomentumConf: {
      if (!(momentum_conf() == other.momentum_conf())) {
        return momentum_conf() < other.momentum_conf() ? -1 : 1;
      }
      break;
    }
    case kRmspropConf: {
      if (!(rmsprop_conf() == other.rmsprop_conf())) {
        return rmsprop_conf() < other.rmsprop_conf() ? -1 : 1;
      }
      break;
    }
    case kLarsConf: {
      if (!(lars_conf() == other.lars_conf())) {
        return lars_conf() < other.lars_conf() ? -1 : 1;
      }
      break;
    }
    case kAdamConf: {
      if (!(adam_conf() == other.adam_conf())) {
        return adam_conf() < other.adam_conf() ? -1 : 1;
      }
      break;
    }
    case kLazyAdamConf: {
      if (!(lazy_adam_conf() == other.lazy_adam_conf())) {
        return lazy_adam_conf() < other.lazy_adam_conf() ? -1 : 1;
      }
      break;
    }
    case kLambConf: {
      if (!(lamb_conf() == other.lamb_conf())) {
        return lamb_conf() < other.lamb_conf() ? -1 : 1;
      }
      break;
    }
    case NORMAL_MDUPDT_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::operator==(const _NormalModelUpdateOpUserConf_& other) const {
  return true
    && has_learning_rate_decay() == other.has_learning_rate_decay() 
    && learning_rate_decay() == other.learning_rate_decay()
    && has_warmup_conf() == other.has_warmup_conf() 
    && warmup_conf() == other.warmup_conf()
    && has_clip_conf() == other.has_clip_conf() 
    && clip_conf() == other.clip_conf()
    && has_weight_decay_conf() == other.has_weight_decay_conf() 
    && weight_decay_conf() == other.weight_decay_conf()
    && normal_mdupdt_case() == other.normal_mdupdt_case()
    && (normal_mdupdt_case() == kNaiveConf ? 
      naive_conf() == other.naive_conf() : true)
    && (normal_mdupdt_case() == kMomentumConf ? 
      momentum_conf() == other.momentum_conf() : true)
    && (normal_mdupdt_case() == kRmspropConf ? 
      rmsprop_conf() == other.rmsprop_conf() : true)
    && (normal_mdupdt_case() == kLarsConf ? 
      lars_conf() == other.lars_conf() : true)
    && (normal_mdupdt_case() == kAdamConf ? 
      adam_conf() == other.adam_conf() : true)
    && (normal_mdupdt_case() == kLazyAdamConf ? 
      lazy_adam_conf() == other.lazy_adam_conf() : true)
    && (normal_mdupdt_case() == kLambConf ? 
      lamb_conf() == other.lamb_conf() : true)
  ;
}

std::size_t ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::__CalcHash__() const {
  return 0
    ^ (has_learning_rate_decay() ? std::hash<::oneflow::cfg::LearningRateDecayConf>()(learning_rate_decay()) : 0)
    ^ (has_warmup_conf() ? std::hash<::oneflow::cfg::WarmupConf>()(warmup_conf()) : 0)
    ^ (has_clip_conf() ? std::hash<::oneflow::cfg::ClipConf>()(clip_conf()) : 0)
    ^ (has_weight_decay_conf() ? std::hash<::oneflow::cfg::WeightDecayConf>()(weight_decay_conf()) : 0)
    ^ static_cast<std::size_t>(normal_mdupdt_case())
    ^ (has_naive_conf() ? std::hash<::oneflow::cfg::NaiveModelUpdateConf>()(naive_conf()) : 0)
    ^ (has_momentum_conf() ? std::hash<::oneflow::cfg::MomentumModelUpdateConf>()(momentum_conf()) : 0)
    ^ (has_rmsprop_conf() ? std::hash<::oneflow::cfg::RMSPropModelUpdateConf>()(rmsprop_conf()) : 0)
    ^ (has_lars_conf() ? std::hash<::oneflow::cfg::LARSModelUpdateConf>()(lars_conf()) : 0)
    ^ (has_adam_conf() ? std::hash<::oneflow::cfg::AdamModelUpdateConf>()(adam_conf()) : 0)
    ^ (has_lazy_adam_conf() ? std::hash<::oneflow::cfg::LazyAdamModelUpdateConf>()(lazy_adam_conf()) : 0)
    ^ (has_lamb_conf() ? std::hash<::oneflow::cfg::LambModelUpdateConf>()(lamb_conf()) : 0)
  ;
}

bool ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_::operator<(const _NormalModelUpdateOpUserConf_& other) const {
  return false
    || !(has_learning_rate_decay() == other.has_learning_rate_decay()) ? 
      has_learning_rate_decay() < other.has_learning_rate_decay() : false
    || !(learning_rate_decay() == other.learning_rate_decay()) ? 
      learning_rate_decay() < other.learning_rate_decay() : false
    || !(has_warmup_conf() == other.has_warmup_conf()) ? 
      has_warmup_conf() < other.has_warmup_conf() : false
    || !(warmup_conf() == other.warmup_conf()) ? 
      warmup_conf() < other.warmup_conf() : false
    || !(has_clip_conf() == other.has_clip_conf()) ? 
      has_clip_conf() < other.has_clip_conf() : false
    || !(clip_conf() == other.clip_conf()) ? 
      clip_conf() < other.clip_conf() : false
    || !(has_weight_decay_conf() == other.has_weight_decay_conf()) ? 
      has_weight_decay_conf() < other.has_weight_decay_conf() : false
    || !(weight_decay_conf() == other.weight_decay_conf()) ? 
      weight_decay_conf() < other.weight_decay_conf() : false
    || !(normal_mdupdt_case() == other.normal_mdupdt_case()) ? 
      normal_mdupdt_case() < other.normal_mdupdt_case() : false
    || ((normal_mdupdt_case() == kNaiveConf) && 
      !(naive_conf() == other.naive_conf())) ? 
        naive_conf() < other.naive_conf() : false
    || ((normal_mdupdt_case() == kMomentumConf) && 
      !(momentum_conf() == other.momentum_conf())) ? 
        momentum_conf() < other.momentum_conf() : false
    || ((normal_mdupdt_case() == kRmspropConf) && 
      !(rmsprop_conf() == other.rmsprop_conf())) ? 
        rmsprop_conf() < other.rmsprop_conf() : false
    || ((normal_mdupdt_case() == kLarsConf) && 
      !(lars_conf() == other.lars_conf())) ? 
        lars_conf() < other.lars_conf() : false
    || ((normal_mdupdt_case() == kAdamConf) && 
      !(adam_conf() == other.adam_conf())) ? 
        adam_conf() < other.adam_conf() : false
    || ((normal_mdupdt_case() == kLazyAdamConf) && 
      !(lazy_adam_conf() == other.lazy_adam_conf())) ? 
        lazy_adam_conf() < other.lazy_adam_conf() : false
    || ((normal_mdupdt_case() == kLambConf) && 
      !(lamb_conf() == other.lamb_conf())) ? 
        lamb_conf() < other.lamb_conf() : false
  ;
}

using _NormalModelUpdateOpUserConf_ =  ConstNormalModelUpdateOpUserConf::_NormalModelUpdateOpUserConf_;
ConstNormalModelUpdateOpUserConf::ConstNormalModelUpdateOpUserConf(const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& data): data_(data) {}
ConstNormalModelUpdateOpUserConf::ConstNormalModelUpdateOpUserConf(): data_(::std::make_shared<_NormalModelUpdateOpUserConf_>()) {}
ConstNormalModelUpdateOpUserConf::ConstNormalModelUpdateOpUserConf(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf) {
  BuildFromProto(proto_normalmodelupdateopuserconf);
}
ConstNormalModelUpdateOpUserConf::ConstNormalModelUpdateOpUserConf(const ConstNormalModelUpdateOpUserConf&) = default;
ConstNormalModelUpdateOpUserConf::ConstNormalModelUpdateOpUserConf(ConstNormalModelUpdateOpUserConf&&) noexcept = default;
ConstNormalModelUpdateOpUserConf::~ConstNormalModelUpdateOpUserConf() = default;

void ConstNormalModelUpdateOpUserConf::ToProto(PbMessage* proto_normalmodelupdateopuserconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::NormalModelUpdateOpUserConf*>(proto_normalmodelupdateopuserconf));
}
  
::std::string ConstNormalModelUpdateOpUserConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstNormalModelUpdateOpUserConf::__Empty__() const {
  return !data_;
}

int ConstNormalModelUpdateOpUserConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"learning_rate_decay", 1},
    {"warmup_conf", 2},
    {"clip_conf", 3},
    {"weight_decay_conf", 4},
    {"naive_conf", 1000},
    {"momentum_conf", 1001},
    {"rmsprop_conf", 1002},
    {"lars_conf", 1003},
    {"adam_conf", 1004},
    {"lazy_adam_conf", 1005},
    {"lamb_conf", 1006},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstNormalModelUpdateOpUserConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 1000:
    case 1001:
    case 1002:
    case 1003:
    case 1004:
    case 1005:
    case 1006:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstNormalModelUpdateOpUserConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LearningRateDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLearningRateDecayConf),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WarmupConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWarmupConf),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ClipConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstClipConf),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::WeightDecayConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstWeightDecayConf),
      };
      return type_indices;
    }
    case 1000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::NaiveModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstNaiveModelUpdateConf),
      };
      return type_indices;
    }
    case 1001: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MomentumModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMomentumModelUpdateConf),
      };
      return type_indices;
    }
    case 1002: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::RMSPropModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstRMSPropModelUpdateConf),
      };
      return type_indices;
    }
    case 1003: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LARSModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLARSModelUpdateConf),
      };
      return type_indices;
    }
    case 1004: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AdamModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAdamModelUpdateConf),
      };
      return type_indices;
    }
    case 1005: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LazyAdamModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLazyAdamModelUpdateConf),
      };
      return type_indices;
    }
    case 1006: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LambModelUpdateConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLambModelUpdateConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstNormalModelUpdateOpUserConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &learning_rate_decay();
    case 2: return &warmup_conf();
    case 3: return &clip_conf();
    case 4: return &weight_decay_conf();
    case 1000: return &naive_conf();
    case 1001: return &momentum_conf();
    case 1002: return &rmsprop_conf();
    case 1003: return &lars_conf();
    case 1004: return &adam_conf();
    case 1005: return &lazy_adam_conf();
    case 1006: return &lamb_conf();
    default: return nullptr;
  }
}

// required or optional field learning_rate_decay
bool ConstNormalModelUpdateOpUserConf::has_learning_rate_decay() const {
  return __SharedPtrOrDefault__()->has_learning_rate_decay();
}
const ::oneflow::cfg::LearningRateDecayConf& ConstNormalModelUpdateOpUserConf::learning_rate_decay() const {
  return __SharedPtrOrDefault__()->learning_rate_decay();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLearningRateDecayConf> ConstNormalModelUpdateOpUserConf::shared_const_learning_rate_decay() const {
  return learning_rate_decay().__SharedConst__();
}
// required or optional field warmup_conf
bool ConstNormalModelUpdateOpUserConf::has_warmup_conf() const {
  return __SharedPtrOrDefault__()->has_warmup_conf();
}
const ::oneflow::cfg::WarmupConf& ConstNormalModelUpdateOpUserConf::warmup_conf() const {
  return __SharedPtrOrDefault__()->warmup_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWarmupConf> ConstNormalModelUpdateOpUserConf::shared_const_warmup_conf() const {
  return warmup_conf().__SharedConst__();
}
// required or optional field clip_conf
bool ConstNormalModelUpdateOpUserConf::has_clip_conf() const {
  return __SharedPtrOrDefault__()->has_clip_conf();
}
const ::oneflow::cfg::ClipConf& ConstNormalModelUpdateOpUserConf::clip_conf() const {
  return __SharedPtrOrDefault__()->clip_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstClipConf> ConstNormalModelUpdateOpUserConf::shared_const_clip_conf() const {
  return clip_conf().__SharedConst__();
}
// required or optional field weight_decay_conf
bool ConstNormalModelUpdateOpUserConf::has_weight_decay_conf() const {
  return __SharedPtrOrDefault__()->has_weight_decay_conf();
}
const ::oneflow::cfg::WeightDecayConf& ConstNormalModelUpdateOpUserConf::weight_decay_conf() const {
  return __SharedPtrOrDefault__()->weight_decay_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstWeightDecayConf> ConstNormalModelUpdateOpUserConf::shared_const_weight_decay_conf() const {
  return weight_decay_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: naive_conf
bool ConstNormalModelUpdateOpUserConf::has_naive_conf() const {
  return __SharedPtrOrDefault__()->has_naive_conf();
}
const ::oneflow::cfg::NaiveModelUpdateConf& ConstNormalModelUpdateOpUserConf::naive_conf() const {
  return __SharedPtrOrDefault__()->naive_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstNaiveModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_naive_conf() const {
  return naive_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: momentum_conf
bool ConstNormalModelUpdateOpUserConf::has_momentum_conf() const {
  return __SharedPtrOrDefault__()->has_momentum_conf();
}
const ::oneflow::cfg::MomentumModelUpdateConf& ConstNormalModelUpdateOpUserConf::momentum_conf() const {
  return __SharedPtrOrDefault__()->momentum_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMomentumModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_momentum_conf() const {
  return momentum_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: rmsprop_conf
bool ConstNormalModelUpdateOpUserConf::has_rmsprop_conf() const {
  return __SharedPtrOrDefault__()->has_rmsprop_conf();
}
const ::oneflow::cfg::RMSPropModelUpdateConf& ConstNormalModelUpdateOpUserConf::rmsprop_conf() const {
  return __SharedPtrOrDefault__()->rmsprop_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstRMSPropModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_rmsprop_conf() const {
  return rmsprop_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lars_conf
bool ConstNormalModelUpdateOpUserConf::has_lars_conf() const {
  return __SharedPtrOrDefault__()->has_lars_conf();
}
const ::oneflow::cfg::LARSModelUpdateConf& ConstNormalModelUpdateOpUserConf::lars_conf() const {
  return __SharedPtrOrDefault__()->lars_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLARSModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_lars_conf() const {
  return lars_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: adam_conf
bool ConstNormalModelUpdateOpUserConf::has_adam_conf() const {
  return __SharedPtrOrDefault__()->has_adam_conf();
}
const ::oneflow::cfg::AdamModelUpdateConf& ConstNormalModelUpdateOpUserConf::adam_conf() const {
  return __SharedPtrOrDefault__()->adam_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAdamModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_adam_conf() const {
  return adam_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lazy_adam_conf
bool ConstNormalModelUpdateOpUserConf::has_lazy_adam_conf() const {
  return __SharedPtrOrDefault__()->has_lazy_adam_conf();
}
const ::oneflow::cfg::LazyAdamModelUpdateConf& ConstNormalModelUpdateOpUserConf::lazy_adam_conf() const {
  return __SharedPtrOrDefault__()->lazy_adam_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLazyAdamModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_lazy_adam_conf() const {
  return lazy_adam_conf().__SharedConst__();
}
 // oneof field normal_mdupdt: lamb_conf
bool ConstNormalModelUpdateOpUserConf::has_lamb_conf() const {
  return __SharedPtrOrDefault__()->has_lamb_conf();
}
const ::oneflow::cfg::LambModelUpdateConf& ConstNormalModelUpdateOpUserConf::lamb_conf() const {
  return __SharedPtrOrDefault__()->lamb_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLambModelUpdateConf> ConstNormalModelUpdateOpUserConf::shared_const_lamb_conf() const {
  return lamb_conf().__SharedConst__();
}
ConstNormalModelUpdateOpUserConf::NormalMdupdtCase ConstNormalModelUpdateOpUserConf::normal_mdupdt_case() const {
  return __SharedPtrOrDefault__()->normal_mdupdt_case();
}

bool ConstNormalModelUpdateOpUserConf::has_normal_mdupdt() const {
  return __SharedPtrOrDefault__()->has_normal_mdupdt();
}

::std::shared_ptr<ConstNormalModelUpdateOpUserConf> ConstNormalModelUpdateOpUserConf::__SharedConst__() const {
  return ::std::make_shared<ConstNormalModelUpdateOpUserConf>(data_);
}
int64_t ConstNormalModelUpdateOpUserConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstNormalModelUpdateOpUserConf::operator==(const ConstNormalModelUpdateOpUserConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstNormalModelUpdateOpUserConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstNormalModelUpdateOpUserConf::operator<(const ConstNormalModelUpdateOpUserConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& ConstNormalModelUpdateOpUserConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_NormalModelUpdateOpUserConf_> default_ptr = std::make_shared<_NormalModelUpdateOpUserConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& ConstNormalModelUpdateOpUserConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _NormalModelUpdateOpUserConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstNormalModelUpdateOpUserConf
void ConstNormalModelUpdateOpUserConf::BuildFromProto(const PbMessage& proto_normalmodelupdateopuserconf) {
  data_ = ::std::make_shared<_NormalModelUpdateOpUserConf_>(dynamic_cast<const ::oneflow::NormalModelUpdateOpUserConf&>(proto_normalmodelupdateopuserconf));
}

NormalModelUpdateOpUserConf::NormalModelUpdateOpUserConf(const ::std::shared_ptr<_NormalModelUpdateOpUserConf_>& data)
  : ConstNormalModelUpdateOpUserConf(data) {}
NormalModelUpdateOpUserConf::NormalModelUpdateOpUserConf(const NormalModelUpdateOpUserConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<NormalModelUpdateOpUserConf> resize
NormalModelUpdateOpUserConf::NormalModelUpdateOpUserConf(NormalModelUpdateOpUserConf&&) noexcept = default; 
NormalModelUpdateOpUserConf::NormalModelUpdateOpUserConf(const ::oneflow::NormalModelUpdateOpUserConf& proto_normalmodelupdateopuserconf) {
  InitFromProto(proto_normalmodelupdateopuserconf);
}
NormalModelUpdateOpUserConf::NormalModelUpdateOpUserConf() = default;

NormalModelUpdateOpUserConf::~NormalModelUpdateOpUserConf() = default;

void NormalModelUpdateOpUserConf::InitFromProto(const PbMessage& proto_normalmodelupdateopuserconf) {
  BuildFromProto(proto_normalmodelupdateopuserconf);
}
  
void* NormalModelUpdateOpUserConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_learning_rate_decay();
    case 2: return mutable_warmup_conf();
    case 3: return mutable_clip_conf();
    case 4: return mutable_weight_decay_conf();
    case 1000: return mutable_naive_conf();
    case 1001: return mutable_momentum_conf();
    case 1002: return mutable_rmsprop_conf();
    case 1003: return mutable_lars_conf();
    case 1004: return mutable_adam_conf();
    case 1005: return mutable_lazy_adam_conf();
    case 1006: return mutable_lamb_conf();
    default: return nullptr;
  }
}

bool NormalModelUpdateOpUserConf::operator==(const NormalModelUpdateOpUserConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t NormalModelUpdateOpUserConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool NormalModelUpdateOpUserConf::operator<(const NormalModelUpdateOpUserConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void NormalModelUpdateOpUserConf::Clear() {
  if (data_) { data_.reset(); }
}
void NormalModelUpdateOpUserConf::CopyFrom(const NormalModelUpdateOpUserConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
NormalModelUpdateOpUserConf& NormalModelUpdateOpUserConf::operator=(const NormalModelUpdateOpUserConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field learning_rate_decay
void NormalModelUpdateOpUserConf::clear_learning_rate_decay() {
  return __SharedPtr__()->clear_learning_rate_decay();
}
::oneflow::cfg::LearningRateDecayConf* NormalModelUpdateOpUserConf::mutable_learning_rate_decay() {
  return __SharedPtr__()->mutable_learning_rate_decay();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> NormalModelUpdateOpUserConf::shared_mutable_learning_rate_decay() {
  return mutable_learning_rate_decay()->__SharedMutable__();
}
// required or optional field warmup_conf
void NormalModelUpdateOpUserConf::clear_warmup_conf() {
  return __SharedPtr__()->clear_warmup_conf();
}
::oneflow::cfg::WarmupConf* NormalModelUpdateOpUserConf::mutable_warmup_conf() {
  return __SharedPtr__()->mutable_warmup_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WarmupConf> NormalModelUpdateOpUserConf::shared_mutable_warmup_conf() {
  return mutable_warmup_conf()->__SharedMutable__();
}
// required or optional field clip_conf
void NormalModelUpdateOpUserConf::clear_clip_conf() {
  return __SharedPtr__()->clear_clip_conf();
}
::oneflow::cfg::ClipConf* NormalModelUpdateOpUserConf::mutable_clip_conf() {
  return __SharedPtr__()->mutable_clip_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ClipConf> NormalModelUpdateOpUserConf::shared_mutable_clip_conf() {
  return mutable_clip_conf()->__SharedMutable__();
}
// required or optional field weight_decay_conf
void NormalModelUpdateOpUserConf::clear_weight_decay_conf() {
  return __SharedPtr__()->clear_weight_decay_conf();
}
::oneflow::cfg::WeightDecayConf* NormalModelUpdateOpUserConf::mutable_weight_decay_conf() {
  return __SharedPtr__()->mutable_weight_decay_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::WeightDecayConf> NormalModelUpdateOpUserConf::shared_mutable_weight_decay_conf() {
  return mutable_weight_decay_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_naive_conf() {
  return __SharedPtr__()->clear_naive_conf();
}
::oneflow::cfg::NaiveModelUpdateConf* NormalModelUpdateOpUserConf::mutable_naive_conf() {
  return __SharedPtr__()->mutable_naive_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::NaiveModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_naive_conf() {
  return mutable_naive_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_momentum_conf() {
  return __SharedPtr__()->clear_momentum_conf();
}
::oneflow::cfg::MomentumModelUpdateConf* NormalModelUpdateOpUserConf::mutable_momentum_conf() {
  return __SharedPtr__()->mutable_momentum_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MomentumModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_momentum_conf() {
  return mutable_momentum_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_rmsprop_conf() {
  return __SharedPtr__()->clear_rmsprop_conf();
}
::oneflow::cfg::RMSPropModelUpdateConf* NormalModelUpdateOpUserConf::mutable_rmsprop_conf() {
  return __SharedPtr__()->mutable_rmsprop_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::RMSPropModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_rmsprop_conf() {
  return mutable_rmsprop_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_lars_conf() {
  return __SharedPtr__()->clear_lars_conf();
}
::oneflow::cfg::LARSModelUpdateConf* NormalModelUpdateOpUserConf::mutable_lars_conf() {
  return __SharedPtr__()->mutable_lars_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LARSModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_lars_conf() {
  return mutable_lars_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_adam_conf() {
  return __SharedPtr__()->clear_adam_conf();
}
::oneflow::cfg::AdamModelUpdateConf* NormalModelUpdateOpUserConf::mutable_adam_conf() {
  return __SharedPtr__()->mutable_adam_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AdamModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_adam_conf() {
  return mutable_adam_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_lazy_adam_conf() {
  return __SharedPtr__()->clear_lazy_adam_conf();
}
::oneflow::cfg::LazyAdamModelUpdateConf* NormalModelUpdateOpUserConf::mutable_lazy_adam_conf() {
  return __SharedPtr__()->mutable_lazy_adam_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LazyAdamModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_lazy_adam_conf() {
  return mutable_lazy_adam_conf()->__SharedMutable__();
}
void NormalModelUpdateOpUserConf::clear_lamb_conf() {
  return __SharedPtr__()->clear_lamb_conf();
}
::oneflow::cfg::LambModelUpdateConf* NormalModelUpdateOpUserConf::mutable_lamb_conf() {
  return __SharedPtr__()->mutable_lamb_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LambModelUpdateConf> NormalModelUpdateOpUserConf::shared_mutable_lamb_conf() {
  return mutable_lamb_conf()->__SharedMutable__();
}

::std::shared_ptr<NormalModelUpdateOpUserConf> NormalModelUpdateOpUserConf::__SharedMutable__() {
  return ::std::make_shared<NormalModelUpdateOpUserConf>(__SharedPtr__());
}
ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::_DynamicLossScalePolicy_() { Clear(); }
ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::_DynamicLossScalePolicy_(const _DynamicLossScalePolicy_& other) { CopyFrom(other); }
ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::_DynamicLossScalePolicy_(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy) {
  InitFromProto(proto_dynamiclossscalepolicy);
}
ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::_DynamicLossScalePolicy_(_DynamicLossScalePolicy_&& other) = default;
ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::~_DynamicLossScalePolicy_() = default;

void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::InitFromProto(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy) {
  Clear();
  // required_or_optional field: initial_loss_scale
  if (proto_dynamiclossscalepolicy.has_initial_loss_scale()) {
    set_initial_loss_scale(proto_dynamiclossscalepolicy.initial_loss_scale());
  }
  // required_or_optional field: increment_period
  if (proto_dynamiclossscalepolicy.has_increment_period()) {
    set_increment_period(proto_dynamiclossscalepolicy.increment_period());
  }
  // required_or_optional field: multiplier
  if (proto_dynamiclossscalepolicy.has_multiplier()) {
    set_multiplier(proto_dynamiclossscalepolicy.multiplier());
  }
    
}

void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::ToProto(::oneflow::DynamicLossScalePolicy* proto_dynamiclossscalepolicy) const {
  proto_dynamiclossscalepolicy->Clear();
  // required_or_optional field: initial_loss_scale
  if (this->has_initial_loss_scale()) {
    proto_dynamiclossscalepolicy->set_initial_loss_scale(initial_loss_scale());
    }
  // required_or_optional field: increment_period
  if (this->has_increment_period()) {
    proto_dynamiclossscalepolicy->set_increment_period(increment_period());
    }
  // required_or_optional field: multiplier
  if (this->has_multiplier()) {
    proto_dynamiclossscalepolicy->set_multiplier(multiplier());
    }

}

::std::string ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::DebugString() const {
  ::oneflow::DynamicLossScalePolicy proto_dynamiclossscalepolicy;
  this->ToProto(&proto_dynamiclossscalepolicy);
  return proto_dynamiclossscalepolicy.DebugString();
}

void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::Clear() {
  clear_initial_loss_scale();
  clear_increment_period();
  clear_multiplier();
}

void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::CopyFrom(const _DynamicLossScalePolicy_& other) {
  if (other.has_initial_loss_scale()) {
    set_initial_loss_scale(other.initial_loss_scale());
  } else {
    clear_initial_loss_scale();
  }
  if (other.has_increment_period()) {
    set_increment_period(other.increment_period());
  } else {
    clear_increment_period();
  }
  if (other.has_multiplier()) {
    set_multiplier(other.multiplier());
  } else {
    clear_multiplier();
  }
}


// optional field initial_loss_scale
bool ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::has_initial_loss_scale() const {
  return has_initial_loss_scale_;
}
const float& ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::initial_loss_scale() const {
  if (has_initial_loss_scale_) { return initial_loss_scale_; }
  static const float default_static_value =
    float(1073741824.0);
  return default_static_value;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::clear_initial_loss_scale() {
  has_initial_loss_scale_ = false;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::set_initial_loss_scale(const float& value) {
  initial_loss_scale_ = value;
  has_initial_loss_scale_ = true;
}
float* ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::mutable_initial_loss_scale() {
  has_initial_loss_scale_ = true;
  return &initial_loss_scale_;
}

// optional field increment_period
bool ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::has_increment_period() const {
  return has_increment_period_;
}
const float& ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::increment_period() const {
  if (has_increment_period_) { return increment_period_; }
  static const float default_static_value =
    float(2000.0);
  return default_static_value;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::clear_increment_period() {
  has_increment_period_ = false;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::set_increment_period(const float& value) {
  increment_period_ = value;
  has_increment_period_ = true;
}
float* ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::mutable_increment_period() {
  has_increment_period_ = true;
  return &increment_period_;
}

// optional field multiplier
bool ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::has_multiplier() const {
  return has_multiplier_;
}
const float& ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::multiplier() const {
  if (has_multiplier_) { return multiplier_; }
  static const float default_static_value =
    float(2.0);
  return default_static_value;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::clear_multiplier() {
  has_multiplier_ = false;
}
void ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::set_multiplier(const float& value) {
  multiplier_ = value;
  has_multiplier_ = true;
}
float* ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::mutable_multiplier() {
  has_multiplier_ = true;
  return &multiplier_;
}


int ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::compare(const _DynamicLossScalePolicy_& other) {
  if (!(has_initial_loss_scale() == other.has_initial_loss_scale())) {
    return has_initial_loss_scale() < other.has_initial_loss_scale() ? -1 : 1;
  } else if (!(initial_loss_scale() == other.initial_loss_scale())) {
    return initial_loss_scale() < other.initial_loss_scale() ? -1 : 1;
  }
  if (!(has_increment_period() == other.has_increment_period())) {
    return has_increment_period() < other.has_increment_period() ? -1 : 1;
  } else if (!(increment_period() == other.increment_period())) {
    return increment_period() < other.increment_period() ? -1 : 1;
  }
  if (!(has_multiplier() == other.has_multiplier())) {
    return has_multiplier() < other.has_multiplier() ? -1 : 1;
  } else if (!(multiplier() == other.multiplier())) {
    return multiplier() < other.multiplier() ? -1 : 1;
  }
  return 0;
}

bool ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::operator==(const _DynamicLossScalePolicy_& other) const {
  return true
    && has_initial_loss_scale() == other.has_initial_loss_scale() 
    && initial_loss_scale() == other.initial_loss_scale()
    && has_increment_period() == other.has_increment_period() 
    && increment_period() == other.increment_period()
    && has_multiplier() == other.has_multiplier() 
    && multiplier() == other.multiplier()
  ;
}

std::size_t ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::__CalcHash__() const {
  return 0
    ^ (has_initial_loss_scale() ? std::hash<float>()(initial_loss_scale()) : 0)
    ^ (has_increment_period() ? std::hash<float>()(increment_period()) : 0)
    ^ (has_multiplier() ? std::hash<float>()(multiplier()) : 0)
  ;
}

bool ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_::operator<(const _DynamicLossScalePolicy_& other) const {
  return false
    || !(has_initial_loss_scale() == other.has_initial_loss_scale()) ? 
      has_initial_loss_scale() < other.has_initial_loss_scale() : false
    || !(initial_loss_scale() == other.initial_loss_scale()) ? 
      initial_loss_scale() < other.initial_loss_scale() : false
    || !(has_increment_period() == other.has_increment_period()) ? 
      has_increment_period() < other.has_increment_period() : false
    || !(increment_period() == other.increment_period()) ? 
      increment_period() < other.increment_period() : false
    || !(has_multiplier() == other.has_multiplier()) ? 
      has_multiplier() < other.has_multiplier() : false
    || !(multiplier() == other.multiplier()) ? 
      multiplier() < other.multiplier() : false
  ;
}

using _DynamicLossScalePolicy_ =  ConstDynamicLossScalePolicy::_DynamicLossScalePolicy_;
ConstDynamicLossScalePolicy::ConstDynamicLossScalePolicy(const ::std::shared_ptr<_DynamicLossScalePolicy_>& data): data_(data) {}
ConstDynamicLossScalePolicy::ConstDynamicLossScalePolicy(): data_(::std::make_shared<_DynamicLossScalePolicy_>()) {}
ConstDynamicLossScalePolicy::ConstDynamicLossScalePolicy(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy) {
  BuildFromProto(proto_dynamiclossscalepolicy);
}
ConstDynamicLossScalePolicy::ConstDynamicLossScalePolicy(const ConstDynamicLossScalePolicy&) = default;
ConstDynamicLossScalePolicy::ConstDynamicLossScalePolicy(ConstDynamicLossScalePolicy&&) noexcept = default;
ConstDynamicLossScalePolicy::~ConstDynamicLossScalePolicy() = default;

void ConstDynamicLossScalePolicy::ToProto(PbMessage* proto_dynamiclossscalepolicy) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::DynamicLossScalePolicy*>(proto_dynamiclossscalepolicy));
}
  
::std::string ConstDynamicLossScalePolicy::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstDynamicLossScalePolicy::__Empty__() const {
  return !data_;
}

int ConstDynamicLossScalePolicy::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"initial_loss_scale", 1},
    {"increment_period", 2},
    {"multiplier", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstDynamicLossScalePolicy::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstDynamicLossScalePolicy::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstDynamicLossScalePolicy::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &initial_loss_scale();
    case 2: return &increment_period();
    case 3: return &multiplier();
    default: return nullptr;
  }
}

// required or optional field initial_loss_scale
bool ConstDynamicLossScalePolicy::has_initial_loss_scale() const {
  return __SharedPtrOrDefault__()->has_initial_loss_scale();
}
const float& ConstDynamicLossScalePolicy::initial_loss_scale() const {
  return __SharedPtrOrDefault__()->initial_loss_scale();
}
// used by pybind11 only
// required or optional field increment_period
bool ConstDynamicLossScalePolicy::has_increment_period() const {
  return __SharedPtrOrDefault__()->has_increment_period();
}
const float& ConstDynamicLossScalePolicy::increment_period() const {
  return __SharedPtrOrDefault__()->increment_period();
}
// used by pybind11 only
// required or optional field multiplier
bool ConstDynamicLossScalePolicy::has_multiplier() const {
  return __SharedPtrOrDefault__()->has_multiplier();
}
const float& ConstDynamicLossScalePolicy::multiplier() const {
  return __SharedPtrOrDefault__()->multiplier();
}
// used by pybind11 only

::std::shared_ptr<ConstDynamicLossScalePolicy> ConstDynamicLossScalePolicy::__SharedConst__() const {
  return ::std::make_shared<ConstDynamicLossScalePolicy>(data_);
}
int64_t ConstDynamicLossScalePolicy::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstDynamicLossScalePolicy::operator==(const ConstDynamicLossScalePolicy& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstDynamicLossScalePolicy::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstDynamicLossScalePolicy::operator<(const ConstDynamicLossScalePolicy& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_DynamicLossScalePolicy_>& ConstDynamicLossScalePolicy::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_DynamicLossScalePolicy_> default_ptr = std::make_shared<_DynamicLossScalePolicy_>();
  return default_ptr;
}
const ::std::shared_ptr<_DynamicLossScalePolicy_>& ConstDynamicLossScalePolicy::__SharedPtr__() {
  if (!data_) { data_.reset(new _DynamicLossScalePolicy_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstDynamicLossScalePolicy
void ConstDynamicLossScalePolicy::BuildFromProto(const PbMessage& proto_dynamiclossscalepolicy) {
  data_ = ::std::make_shared<_DynamicLossScalePolicy_>(dynamic_cast<const ::oneflow::DynamicLossScalePolicy&>(proto_dynamiclossscalepolicy));
}

DynamicLossScalePolicy::DynamicLossScalePolicy(const ::std::shared_ptr<_DynamicLossScalePolicy_>& data)
  : ConstDynamicLossScalePolicy(data) {}
DynamicLossScalePolicy::DynamicLossScalePolicy(const DynamicLossScalePolicy& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<DynamicLossScalePolicy> resize
DynamicLossScalePolicy::DynamicLossScalePolicy(DynamicLossScalePolicy&&) noexcept = default; 
DynamicLossScalePolicy::DynamicLossScalePolicy(const ::oneflow::DynamicLossScalePolicy& proto_dynamiclossscalepolicy) {
  InitFromProto(proto_dynamiclossscalepolicy);
}
DynamicLossScalePolicy::DynamicLossScalePolicy() = default;

DynamicLossScalePolicy::~DynamicLossScalePolicy() = default;

void DynamicLossScalePolicy::InitFromProto(const PbMessage& proto_dynamiclossscalepolicy) {
  BuildFromProto(proto_dynamiclossscalepolicy);
}
  
void* DynamicLossScalePolicy::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_initial_loss_scale();
    case 2: return mutable_increment_period();
    case 3: return mutable_multiplier();
    default: return nullptr;
  }
}

bool DynamicLossScalePolicy::operator==(const DynamicLossScalePolicy& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t DynamicLossScalePolicy::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool DynamicLossScalePolicy::operator<(const DynamicLossScalePolicy& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void DynamicLossScalePolicy::Clear() {
  if (data_) { data_.reset(); }
}
void DynamicLossScalePolicy::CopyFrom(const DynamicLossScalePolicy& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
DynamicLossScalePolicy& DynamicLossScalePolicy::operator=(const DynamicLossScalePolicy& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field initial_loss_scale
void DynamicLossScalePolicy::clear_initial_loss_scale() {
  return __SharedPtr__()->clear_initial_loss_scale();
}
void DynamicLossScalePolicy::set_initial_loss_scale(const float& value) {
  return __SharedPtr__()->set_initial_loss_scale(value);
}
float* DynamicLossScalePolicy::mutable_initial_loss_scale() {
  return  __SharedPtr__()->mutable_initial_loss_scale();
}
// required or optional field increment_period
void DynamicLossScalePolicy::clear_increment_period() {
  return __SharedPtr__()->clear_increment_period();
}
void DynamicLossScalePolicy::set_increment_period(const float& value) {
  return __SharedPtr__()->set_increment_period(value);
}
float* DynamicLossScalePolicy::mutable_increment_period() {
  return  __SharedPtr__()->mutable_increment_period();
}
// required or optional field multiplier
void DynamicLossScalePolicy::clear_multiplier() {
  return __SharedPtr__()->clear_multiplier();
}
void DynamicLossScalePolicy::set_multiplier(const float& value) {
  return __SharedPtr__()->set_multiplier(value);
}
float* DynamicLossScalePolicy::mutable_multiplier() {
  return  __SharedPtr__()->mutable_multiplier();
}

::std::shared_ptr<DynamicLossScalePolicy> DynamicLossScalePolicy::__SharedMutable__() {
  return ::std::make_shared<DynamicLossScalePolicy>(__SharedPtr__());
}
ConstTrainConf::_TrainConf_::_TrainConf_() { Clear(); }
ConstTrainConf::_TrainConf_::_TrainConf_(const _TrainConf_& other) { CopyFrom(other); }
ConstTrainConf::_TrainConf_::_TrainConf_(const ::oneflow::TrainConf& proto_trainconf) {
  InitFromProto(proto_trainconf);
}
ConstTrainConf::_TrainConf_::_TrainConf_(_TrainConf_&& other) = default;
ConstTrainConf::_TrainConf_::~_TrainConf_() = default;

void ConstTrainConf::_TrainConf_::InitFromProto(const ::oneflow::TrainConf& proto_trainconf) {
  Clear();
  // repeated field: optimizer_conf
  if (!proto_trainconf.optimizer_conf().empty()) {
    for (const ::oneflow::OptimizerConf& elem : proto_trainconf.optimizer_conf() ) {
      *mutable_optimizer_conf()->Add() = ::oneflow::cfg::OptimizerConf(elem);
    }
  }
  // repeated field: loss_lbn
  if (!proto_trainconf.loss_lbn().empty()) {
    for (const ::std::string& elem : proto_trainconf.loss_lbn()) {
      add_loss_lbn(elem);
    }
  }
  // required_or_optional field: train_step_lbn
  if (proto_trainconf.has_train_step_lbn()) {
    set_train_step_lbn(proto_trainconf.train_step_lbn());
  }
  // required_or_optional field: model_update_conf
  if (proto_trainconf.has_model_update_conf()) {
  *mutable_model_update_conf() = ::oneflow::cfg::NormalModelUpdateOpUserConf(proto_trainconf.model_update_conf());      
  }
  // required_or_optional field: primary_lr
  if (proto_trainconf.has_primary_lr()) {
    set_primary_lr(proto_trainconf.primary_lr());
  }
  // required_or_optional field: secondary_lr
  if (proto_trainconf.has_secondary_lr()) {
    set_secondary_lr(proto_trainconf.secondary_lr());
  }
  // required_or_optional field: primary_lr_lbn
  if (proto_trainconf.has_primary_lr_lbn()) {
    set_primary_lr_lbn(proto_trainconf.primary_lr_lbn());
  }
  // required_or_optional field: secondary_lr_lbn
  if (proto_trainconf.has_secondary_lr_lbn()) {
    set_secondary_lr_lbn(proto_trainconf.secondary_lr_lbn());
  }
  // oneof field: loss_scale_policy
  LossScalePolicyCase loss_scale_policy_case = LossScalePolicyCase(int(proto_trainconf.loss_scale_policy_case()));
  switch (loss_scale_policy_case) {
    case kLossScaleFactor: {
      set_loss_scale_factor(proto_trainconf.loss_scale_factor());
      break;
  }
    case kDynamicLossScalePolicy: {
      *mutable_dynamic_loss_scale_policy() = ::oneflow::cfg::DynamicLossScalePolicy(proto_trainconf.dynamic_loss_scale_policy());
      break;
  }
    case LOSS_SCALE_POLICY_NOT_SET: {
      break;
    }
  }
      
}

void ConstTrainConf::_TrainConf_::ToProto(::oneflow::TrainConf* proto_trainconf) const {
  proto_trainconf->Clear();
  // repeated field: optimizer_conf
  if (!optimizer_conf().empty()) {
    for (const ::oneflow::cfg::OptimizerConf& elem : optimizer_conf() ) {
      ::oneflow::OptimizerConf proto_optimizer_conf_elem;
      elem.ToProto(&proto_optimizer_conf_elem);
      *proto_trainconf->mutable_optimizer_conf()->Add() = proto_optimizer_conf_elem;
    }
  }
  // repeated field: loss_lbn
  if (!loss_lbn().empty()) {
    for (const ::std::string& elem : loss_lbn()) {
      proto_trainconf->add_loss_lbn(elem);
    }
  }
  // required_or_optional field: train_step_lbn
  if (this->has_train_step_lbn()) {
    proto_trainconf->set_train_step_lbn(train_step_lbn());
    }
  // required_or_optional field: model_update_conf
  if (this->has_model_update_conf()) {
    ::oneflow::NormalModelUpdateOpUserConf proto_model_update_conf;
    model_update_conf().ToProto(&proto_model_update_conf);
    proto_trainconf->mutable_model_update_conf()->CopyFrom(proto_model_update_conf);
    }
  // required_or_optional field: primary_lr
  if (this->has_primary_lr()) {
    proto_trainconf->set_primary_lr(primary_lr());
    }
  // required_or_optional field: secondary_lr
  if (this->has_secondary_lr()) {
    proto_trainconf->set_secondary_lr(secondary_lr());
    }
  // required_or_optional field: primary_lr_lbn
  if (this->has_primary_lr_lbn()) {
    proto_trainconf->set_primary_lr_lbn(primary_lr_lbn());
    }
  // required_or_optional field: secondary_lr_lbn
  if (this->has_secondary_lr_lbn()) {
    proto_trainconf->set_secondary_lr_lbn(secondary_lr_lbn());
    }

  // oneof field: loss_scale_policy
  ::oneflow::TrainConf::LossScalePolicyCase loss_scale_policy_case = ::oneflow::TrainConf::LossScalePolicyCase(int(this->loss_scale_policy_case()));
  switch (loss_scale_policy_case) {
    case ::oneflow::TrainConf::kLossScaleFactor: {
      proto_trainconf->set_loss_scale_factor(loss_scale_factor());
      break;
    }
    case ::oneflow::TrainConf::kDynamicLossScalePolicy: {
      ::oneflow::DynamicLossScalePolicy of_proto_dynamic_loss_scale_policy;
      dynamic_loss_scale_policy().ToProto(&of_proto_dynamic_loss_scale_policy);
      proto_trainconf->mutable_dynamic_loss_scale_policy()->CopyFrom(of_proto_dynamic_loss_scale_policy);
      break;
    }
    case ::oneflow::TrainConf::LOSS_SCALE_POLICY_NOT_SET: {
      break;
    }
  }
}

::std::string ConstTrainConf::_TrainConf_::DebugString() const {
  ::oneflow::TrainConf proto_trainconf;
  this->ToProto(&proto_trainconf);
  return proto_trainconf.DebugString();
}

void ConstTrainConf::_TrainConf_::Clear() {
  clear_optimizer_conf();
  clear_loss_lbn();
  clear_train_step_lbn();
  clear_model_update_conf();
  clear_primary_lr();
  clear_secondary_lr();
  clear_primary_lr_lbn();
  clear_secondary_lr_lbn();
  clear_loss_scale_policy();
}

void ConstTrainConf::_TrainConf_::CopyFrom(const _TrainConf_& other) {
  mutable_optimizer_conf()->CopyFrom(other.optimizer_conf());
  mutable_loss_lbn()->CopyFrom(other.loss_lbn());
  if (other.has_train_step_lbn()) {
    set_train_step_lbn(other.train_step_lbn());
  } else {
    clear_train_step_lbn();
  }
  if (other.has_model_update_conf()) {
    mutable_model_update_conf()->CopyFrom(other.model_update_conf());
  } else {
    clear_model_update_conf();
  }
  if (other.has_primary_lr()) {
    set_primary_lr(other.primary_lr());
  } else {
    clear_primary_lr();
  }
  if (other.has_secondary_lr()) {
    set_secondary_lr(other.secondary_lr());
  } else {
    clear_secondary_lr();
  }
  if (other.has_primary_lr_lbn()) {
    set_primary_lr_lbn(other.primary_lr_lbn());
  } else {
    clear_primary_lr_lbn();
  }
  if (other.has_secondary_lr_lbn()) {
    set_secondary_lr_lbn(other.secondary_lr_lbn());
  } else {
    clear_secondary_lr_lbn();
  }
  loss_scale_policy_copy_from(other);
}


// repeated field optimizer_conf
::std::size_t ConstTrainConf::_TrainConf_::optimizer_conf_size() const {
  if (!optimizer_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
    return default_static_value->size();
  }
  return optimizer_conf_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& ConstTrainConf::_TrainConf_::optimizer_conf() const {
  if (!optimizer_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
    return *(default_static_value.get());
  }
  return *(optimizer_conf_.get());
}
const ::oneflow::cfg::OptimizerConf& ConstTrainConf::_TrainConf_::optimizer_conf(::std::size_t index) const {
  if (!optimizer_conf_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
    return default_static_value->Get(index);
  }
  return optimizer_conf_->Get(index);
}
void ConstTrainConf::_TrainConf_::clear_optimizer_conf() {
  if (!optimizer_conf_) {
    optimizer_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
  }
  return optimizer_conf_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_* ConstTrainConf::_TrainConf_::mutable_optimizer_conf() {
  if (!optimizer_conf_) {
    optimizer_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
  }
  return  optimizer_conf_.get();
}
::oneflow::cfg::OptimizerConf* ConstTrainConf::_TrainConf_::mutable_optimizer_conf(::std::size_t index) {
  if (!optimizer_conf_) {
    optimizer_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
  }
  return  optimizer_conf_->Mutable(index);
}
::oneflow::cfg::OptimizerConf* ConstTrainConf::_TrainConf_::add_optimizer_conf() {
  if (!optimizer_conf_) {
    optimizer_conf_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>();
  }
  return optimizer_conf_->Add();
}

// repeated field loss_lbn
::std::size_t ConstTrainConf::_TrainConf_::loss_lbn_size() const {
  if (!loss_lbn_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return loss_lbn_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstTrainConf::_TrainConf_::loss_lbn() const {
  if (!loss_lbn_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(loss_lbn_.get());
}
const ::std::string& ConstTrainConf::_TrainConf_::loss_lbn(::std::size_t index) const {
  if (!loss_lbn_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return loss_lbn_->Get(index);
}
void ConstTrainConf::_TrainConf_::clear_loss_lbn() {
  if (!loss_lbn_) {
    loss_lbn_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return loss_lbn_->Clear();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* ConstTrainConf::_TrainConf_::mutable_loss_lbn() {
  if (!loss_lbn_) {
    loss_lbn_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  loss_lbn_.get();
}
::std::string* ConstTrainConf::_TrainConf_::mutable_loss_lbn(::std::size_t index) {
  if (!loss_lbn_) {
    loss_lbn_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  loss_lbn_->Mutable(index);
}
void ConstTrainConf::_TrainConf_::add_loss_lbn(const ::std::string& value) {
  if (!loss_lbn_) {
    loss_lbn_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return loss_lbn_->Add(value);
}
void ConstTrainConf::_TrainConf_::set_loss_lbn(::std::size_t index, const ::std::string& value) {
  if (!loss_lbn_) {
    loss_lbn_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return loss_lbn_->Set(index, value);
}

// optional field train_step_lbn
bool ConstTrainConf::_TrainConf_::has_train_step_lbn() const {
  return has_train_step_lbn_;
}
const ::std::string& ConstTrainConf::_TrainConf_::train_step_lbn() const {
  if (has_train_step_lbn_) { return train_step_lbn_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstTrainConf::_TrainConf_::clear_train_step_lbn() {
  has_train_step_lbn_ = false;
}
void ConstTrainConf::_TrainConf_::set_train_step_lbn(const ::std::string& value) {
  train_step_lbn_ = value;
  has_train_step_lbn_ = true;
}
::std::string* ConstTrainConf::_TrainConf_::mutable_train_step_lbn() {
  has_train_step_lbn_ = true;
  return &train_step_lbn_;
}

// oneof field loss_scale_policy: loss_scale_factor
bool ConstTrainConf::_TrainConf_::has_loss_scale_factor() const {
  return loss_scale_policy_case() == kLossScaleFactor;
}
void ConstTrainConf::_TrainConf_::clear_loss_scale_factor() {
  if (has_loss_scale_factor()) {
    loss_scale_policy_.loss_scale_factor_ = float();
    loss_scale_policy_case_ = LOSS_SCALE_POLICY_NOT_SET;
  }
}

const float& ConstTrainConf::_TrainConf_::loss_scale_factor() const {
  if (has_loss_scale_factor()) {
      return loss_scale_policy_.loss_scale_factor_;
    } else {
      static const float default_static_value = float();
    return default_static_value;
    }
}
void ConstTrainConf::_TrainConf_::set_loss_scale_factor(const float& value) {
  if(!has_loss_scale_factor()) {
    clear_loss_scale_policy();
    }
  loss_scale_policy_case_ = kLossScaleFactor;
    loss_scale_policy_.loss_scale_factor_ = value;
  }
float* ConstTrainConf::_TrainConf_::mutable_loss_scale_factor() {
  if(!has_loss_scale_factor()) {
    clear_loss_scale_policy();
    }
    loss_scale_policy_case_ = kLossScaleFactor;
  return  &loss_scale_policy_.loss_scale_factor_;
  }

// oneof field loss_scale_policy: dynamic_loss_scale_policy
bool ConstTrainConf::_TrainConf_::has_dynamic_loss_scale_policy() const {
  return loss_scale_policy_case() == kDynamicLossScalePolicy;
}
void ConstTrainConf::_TrainConf_::clear_dynamic_loss_scale_policy() {
  if (has_dynamic_loss_scale_policy()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(loss_scale_policy_.dynamic_loss_scale_policy_));
      ptr->~Shared_ptr();
    }
    loss_scale_policy_case_ = LOSS_SCALE_POLICY_NOT_SET;
  }
}

const ::oneflow::cfg::DynamicLossScalePolicy& ConstTrainConf::_TrainConf_::dynamic_loss_scale_policy() const {
  if (has_dynamic_loss_scale_policy()) {
      const ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>*>(&(loss_scale_policy_.dynamic_loss_scale_policy_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy> default_static_value = ::std::make_shared<::oneflow::cfg::DynamicLossScalePolicy>();
    return *default_static_value;
    }
}
::oneflow::cfg::DynamicLossScalePolicy* ConstTrainConf::_TrainConf_::mutable_dynamic_loss_scale_policy() {
  if(!has_dynamic_loss_scale_policy()) {
    clear_loss_scale_policy();
    new (&(loss_scale_policy_.dynamic_loss_scale_policy_)) ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>(new ::oneflow::cfg::DynamicLossScalePolicy());
  }
  loss_scale_policy_case_ = kDynamicLossScalePolicy;
  ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>*>(&(loss_scale_policy_.dynamic_loss_scale_policy_));
  return  (*ptr).get();
}

// optional field model_update_conf
bool ConstTrainConf::_TrainConf_::has_model_update_conf() const {
  return has_model_update_conf_;
}
const ::oneflow::cfg::NormalModelUpdateOpUserConf& ConstTrainConf::_TrainConf_::model_update_conf() const {
  if (!model_update_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::NormalModelUpdateOpUserConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::NormalModelUpdateOpUserConf>();
    return *default_static_value;
  }
  return *(model_update_conf_.get());
}
void ConstTrainConf::_TrainConf_::clear_model_update_conf() {
  if (model_update_conf_) {
    model_update_conf_->Clear();
  }
  has_model_update_conf_ = false;
}
::oneflow::cfg::NormalModelUpdateOpUserConf* ConstTrainConf::_TrainConf_::mutable_model_update_conf() {
  if (!model_update_conf_) {
    model_update_conf_ = ::std::make_shared<::oneflow::cfg::NormalModelUpdateOpUserConf>();
  }
  has_model_update_conf_ = true;
  return model_update_conf_.get();
}

// optional field primary_lr
bool ConstTrainConf::_TrainConf_::has_primary_lr() const {
  return has_primary_lr_;
}
const float& ConstTrainConf::_TrainConf_::primary_lr() const {
  if (has_primary_lr_) { return primary_lr_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstTrainConf::_TrainConf_::clear_primary_lr() {
  has_primary_lr_ = false;
}
void ConstTrainConf::_TrainConf_::set_primary_lr(const float& value) {
  primary_lr_ = value;
  has_primary_lr_ = true;
}
float* ConstTrainConf::_TrainConf_::mutable_primary_lr() {
  has_primary_lr_ = true;
  return &primary_lr_;
}

// optional field secondary_lr
bool ConstTrainConf::_TrainConf_::has_secondary_lr() const {
  return has_secondary_lr_;
}
const float& ConstTrainConf::_TrainConf_::secondary_lr() const {
  if (has_secondary_lr_) { return secondary_lr_; }
  static const float default_static_value = float();
  return default_static_value;
}
void ConstTrainConf::_TrainConf_::clear_secondary_lr() {
  has_secondary_lr_ = false;
}
void ConstTrainConf::_TrainConf_::set_secondary_lr(const float& value) {
  secondary_lr_ = value;
  has_secondary_lr_ = true;
}
float* ConstTrainConf::_TrainConf_::mutable_secondary_lr() {
  has_secondary_lr_ = true;
  return &secondary_lr_;
}

// optional field primary_lr_lbn
bool ConstTrainConf::_TrainConf_::has_primary_lr_lbn() const {
  return has_primary_lr_lbn_;
}
const ::std::string& ConstTrainConf::_TrainConf_::primary_lr_lbn() const {
  if (has_primary_lr_lbn_) { return primary_lr_lbn_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstTrainConf::_TrainConf_::clear_primary_lr_lbn() {
  has_primary_lr_lbn_ = false;
}
void ConstTrainConf::_TrainConf_::set_primary_lr_lbn(const ::std::string& value) {
  primary_lr_lbn_ = value;
  has_primary_lr_lbn_ = true;
}
::std::string* ConstTrainConf::_TrainConf_::mutable_primary_lr_lbn() {
  has_primary_lr_lbn_ = true;
  return &primary_lr_lbn_;
}

// optional field secondary_lr_lbn
bool ConstTrainConf::_TrainConf_::has_secondary_lr_lbn() const {
  return has_secondary_lr_lbn_;
}
const ::std::string& ConstTrainConf::_TrainConf_::secondary_lr_lbn() const {
  if (has_secondary_lr_lbn_) { return secondary_lr_lbn_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstTrainConf::_TrainConf_::clear_secondary_lr_lbn() {
  has_secondary_lr_lbn_ = false;
}
void ConstTrainConf::_TrainConf_::set_secondary_lr_lbn(const ::std::string& value) {
  secondary_lr_lbn_ = value;
  has_secondary_lr_lbn_ = true;
}
::std::string* ConstTrainConf::_TrainConf_::mutable_secondary_lr_lbn() {
  has_secondary_lr_lbn_ = true;
  return &secondary_lr_lbn_;
}
ConstTrainConf::LossScalePolicyCase ConstTrainConf::_TrainConf_::loss_scale_policy_case() const {
  return loss_scale_policy_case_;
}
bool ConstTrainConf::_TrainConf_::has_loss_scale_policy() const {
  return loss_scale_policy_case_ != LOSS_SCALE_POLICY_NOT_SET;
}
void ConstTrainConf::_TrainConf_::clear_loss_scale_policy() {
  switch (loss_scale_policy_case()) {
    case kLossScaleFactor: {
      loss_scale_policy_.loss_scale_factor_ = float();
      break;
    }
    case kDynamicLossScalePolicy: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(loss_scale_policy_.dynamic_loss_scale_policy_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case LOSS_SCALE_POLICY_NOT_SET: {
      break;
    }
  }
  loss_scale_policy_case_ = LOSS_SCALE_POLICY_NOT_SET;
}
void ConstTrainConf::_TrainConf_::loss_scale_policy_copy_from(const _TrainConf_& other) {
  switch (other.loss_scale_policy_case()) {
    case kLossScaleFactor: {
      set_loss_scale_factor(other.loss_scale_factor());
      break;
    }
    case kDynamicLossScalePolicy: {
      mutable_dynamic_loss_scale_policy()->CopyFrom(other.dynamic_loss_scale_policy());
      break;
    }
    case LOSS_SCALE_POLICY_NOT_SET: {
      clear_loss_scale_policy();
    }
  }
}


int ConstTrainConf::_TrainConf_::compare(const _TrainConf_& other) {
  if (!(optimizer_conf() == other.optimizer_conf())) {
    return optimizer_conf() < other.optimizer_conf() ? -1 : 1;
  }
  if (!(loss_lbn() == other.loss_lbn())) {
    return loss_lbn() < other.loss_lbn() ? -1 : 1;
  }
  if (!(has_train_step_lbn() == other.has_train_step_lbn())) {
    return has_train_step_lbn() < other.has_train_step_lbn() ? -1 : 1;
  } else if (!(train_step_lbn() == other.train_step_lbn())) {
    return train_step_lbn() < other.train_step_lbn() ? -1 : 1;
  }
  if (!(has_model_update_conf() == other.has_model_update_conf())) {
    return has_model_update_conf() < other.has_model_update_conf() ? -1 : 1;
  } else if (!(model_update_conf() == other.model_update_conf())) {
    return model_update_conf() < other.model_update_conf() ? -1 : 1;
  }
  if (!(has_primary_lr() == other.has_primary_lr())) {
    return has_primary_lr() < other.has_primary_lr() ? -1 : 1;
  } else if (!(primary_lr() == other.primary_lr())) {
    return primary_lr() < other.primary_lr() ? -1 : 1;
  }
  if (!(has_secondary_lr() == other.has_secondary_lr())) {
    return has_secondary_lr() < other.has_secondary_lr() ? -1 : 1;
  } else if (!(secondary_lr() == other.secondary_lr())) {
    return secondary_lr() < other.secondary_lr() ? -1 : 1;
  }
  if (!(has_primary_lr_lbn() == other.has_primary_lr_lbn())) {
    return has_primary_lr_lbn() < other.has_primary_lr_lbn() ? -1 : 1;
  } else if (!(primary_lr_lbn() == other.primary_lr_lbn())) {
    return primary_lr_lbn() < other.primary_lr_lbn() ? -1 : 1;
  }
  if (!(has_secondary_lr_lbn() == other.has_secondary_lr_lbn())) {
    return has_secondary_lr_lbn() < other.has_secondary_lr_lbn() ? -1 : 1;
  } else if (!(secondary_lr_lbn() == other.secondary_lr_lbn())) {
    return secondary_lr_lbn() < other.secondary_lr_lbn() ? -1 : 1;
  }
  if (!(loss_scale_policy_case() == other.loss_scale_policy_case())) {
    return loss_scale_policy_case() < other.loss_scale_policy_case() ? -1 : 1;
  }
  switch (loss_scale_policy_case()) {
    case kLossScaleFactor: {
      if (!(loss_scale_factor() == other.loss_scale_factor())) {
        return loss_scale_factor() < other.loss_scale_factor() ? -1 : 1;
      }
      break;
    }
    case kDynamicLossScalePolicy: {
      if (!(dynamic_loss_scale_policy() == other.dynamic_loss_scale_policy())) {
        return dynamic_loss_scale_policy() < other.dynamic_loss_scale_policy() ? -1 : 1;
      }
      break;
    }
    case LOSS_SCALE_POLICY_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstTrainConf::_TrainConf_::operator==(const _TrainConf_& other) const {
  return true
    && optimizer_conf() == other.optimizer_conf()
    && loss_lbn() == other.loss_lbn()
    && has_train_step_lbn() == other.has_train_step_lbn() 
    && train_step_lbn() == other.train_step_lbn()
    && has_model_update_conf() == other.has_model_update_conf() 
    && model_update_conf() == other.model_update_conf()
    && has_primary_lr() == other.has_primary_lr() 
    && primary_lr() == other.primary_lr()
    && has_secondary_lr() == other.has_secondary_lr() 
    && secondary_lr() == other.secondary_lr()
    && has_primary_lr_lbn() == other.has_primary_lr_lbn() 
    && primary_lr_lbn() == other.primary_lr_lbn()
    && has_secondary_lr_lbn() == other.has_secondary_lr_lbn() 
    && secondary_lr_lbn() == other.secondary_lr_lbn()
    && loss_scale_policy_case() == other.loss_scale_policy_case()
    && (loss_scale_policy_case() == kLossScaleFactor ? 
      loss_scale_factor() == other.loss_scale_factor() : true)
    && (loss_scale_policy_case() == kDynamicLossScalePolicy ? 
      dynamic_loss_scale_policy() == other.dynamic_loss_scale_policy() : true)
  ;
}

std::size_t ConstTrainConf::_TrainConf_::__CalcHash__() const {
  return 0
    ^ optimizer_conf().__CalcHash__()
    ^ loss_lbn().__CalcHash__()
    ^ (has_train_step_lbn() ? std::hash<::std::string>()(train_step_lbn()) : 0)
    ^ (has_model_update_conf() ? std::hash<::oneflow::cfg::NormalModelUpdateOpUserConf>()(model_update_conf()) : 0)
    ^ (has_primary_lr() ? std::hash<float>()(primary_lr()) : 0)
    ^ (has_secondary_lr() ? std::hash<float>()(secondary_lr()) : 0)
    ^ (has_primary_lr_lbn() ? std::hash<::std::string>()(primary_lr_lbn()) : 0)
    ^ (has_secondary_lr_lbn() ? std::hash<::std::string>()(secondary_lr_lbn()) : 0)
    ^ static_cast<std::size_t>(loss_scale_policy_case())
    ^ (has_loss_scale_factor() ? std::hash<float>()(loss_scale_factor()) : 0)
    ^ (has_dynamic_loss_scale_policy() ? std::hash<::oneflow::cfg::DynamicLossScalePolicy>()(dynamic_loss_scale_policy()) : 0)
  ;
}

bool ConstTrainConf::_TrainConf_::operator<(const _TrainConf_& other) const {
  return false
    || !(optimizer_conf() == other.optimizer_conf()) ? 
      optimizer_conf() < other.optimizer_conf() : false
    || !(loss_lbn() == other.loss_lbn()) ? 
      loss_lbn() < other.loss_lbn() : false
    || !(has_train_step_lbn() == other.has_train_step_lbn()) ? 
      has_train_step_lbn() < other.has_train_step_lbn() : false
    || !(train_step_lbn() == other.train_step_lbn()) ? 
      train_step_lbn() < other.train_step_lbn() : false
    || !(has_model_update_conf() == other.has_model_update_conf()) ? 
      has_model_update_conf() < other.has_model_update_conf() : false
    || !(model_update_conf() == other.model_update_conf()) ? 
      model_update_conf() < other.model_update_conf() : false
    || !(has_primary_lr() == other.has_primary_lr()) ? 
      has_primary_lr() < other.has_primary_lr() : false
    || !(primary_lr() == other.primary_lr()) ? 
      primary_lr() < other.primary_lr() : false
    || !(has_secondary_lr() == other.has_secondary_lr()) ? 
      has_secondary_lr() < other.has_secondary_lr() : false
    || !(secondary_lr() == other.secondary_lr()) ? 
      secondary_lr() < other.secondary_lr() : false
    || !(has_primary_lr_lbn() == other.has_primary_lr_lbn()) ? 
      has_primary_lr_lbn() < other.has_primary_lr_lbn() : false
    || !(primary_lr_lbn() == other.primary_lr_lbn()) ? 
      primary_lr_lbn() < other.primary_lr_lbn() : false
    || !(has_secondary_lr_lbn() == other.has_secondary_lr_lbn()) ? 
      has_secondary_lr_lbn() < other.has_secondary_lr_lbn() : false
    || !(secondary_lr_lbn() == other.secondary_lr_lbn()) ? 
      secondary_lr_lbn() < other.secondary_lr_lbn() : false
    || !(loss_scale_policy_case() == other.loss_scale_policy_case()) ? 
      loss_scale_policy_case() < other.loss_scale_policy_case() : false
    || ((loss_scale_policy_case() == kLossScaleFactor) && 
      !(loss_scale_factor() == other.loss_scale_factor())) ? 
        loss_scale_factor() < other.loss_scale_factor() : false
    || ((loss_scale_policy_case() == kDynamicLossScalePolicy) && 
      !(dynamic_loss_scale_policy() == other.dynamic_loss_scale_policy())) ? 
        dynamic_loss_scale_policy() < other.dynamic_loss_scale_policy() : false
  ;
}

using _TrainConf_ =  ConstTrainConf::_TrainConf_;
ConstTrainConf::ConstTrainConf(const ::std::shared_ptr<_TrainConf_>& data): data_(data) {}
ConstTrainConf::ConstTrainConf(): data_(::std::make_shared<_TrainConf_>()) {}
ConstTrainConf::ConstTrainConf(const ::oneflow::TrainConf& proto_trainconf) {
  BuildFromProto(proto_trainconf);
}
ConstTrainConf::ConstTrainConf(const ConstTrainConf&) = default;
ConstTrainConf::ConstTrainConf(ConstTrainConf&&) noexcept = default;
ConstTrainConf::~ConstTrainConf() = default;

void ConstTrainConf::ToProto(PbMessage* proto_trainconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::TrainConf*>(proto_trainconf));
}
  
::std::string ConstTrainConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstTrainConf::__Empty__() const {
  return !data_;
}

int ConstTrainConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"optimizer_conf", 1},
    {"loss_lbn", 2},
    {"train_step_lbn", 3},
    {"loss_scale_factor", 4},
    {"dynamic_loss_scale_policy", 5},
    {"model_update_conf", 101},
    {"primary_lr", 102},
    {"secondary_lr", 103},
    {"primary_lr_lbn", 104},
    {"secondary_lr_lbn", 105},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstTrainConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 101:
    case 102:
    case 103:
    case 104:
    case 105:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstTrainConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OptimizerConf>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DynamicLossScalePolicy),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstDynamicLossScalePolicy),
      };
      return type_indices;
    }
    case 101: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::NormalModelUpdateOpUserConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstNormalModelUpdateOpUserConf),
      };
      return type_indices;
    }
    case 102: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 103: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 104: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 105: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstTrainConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &optimizer_conf();
    case 2: return &loss_lbn();
    case 3: return &train_step_lbn();
    case 4: return &loss_scale_factor();
    case 5: return &dynamic_loss_scale_policy();
    case 101: return &model_update_conf();
    case 102: return &primary_lr();
    case 103: return &secondary_lr();
    case 104: return &primary_lr_lbn();
    case 105: return &secondary_lr_lbn();
    default: return nullptr;
  }
}

// repeated field optimizer_conf
::std::size_t ConstTrainConf::optimizer_conf_size() const {
  return __SharedPtrOrDefault__()->optimizer_conf_size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& ConstTrainConf::optimizer_conf() const {
  return __SharedPtrOrDefault__()->optimizer_conf();
}
const ::oneflow::cfg::OptimizerConf& ConstTrainConf::optimizer_conf(::std::size_t index) const {
  return __SharedPtrOrDefault__()->optimizer_conf(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> ConstTrainConf::shared_const_optimizer_conf() const {
  return optimizer_conf().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstOptimizerConf> ConstTrainConf::shared_const_optimizer_conf(::std::size_t index) const {
  return optimizer_conf(index).__SharedConst__();
}
// repeated field loss_lbn
::std::size_t ConstTrainConf::loss_lbn_size() const {
  return __SharedPtrOrDefault__()->loss_lbn_size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& ConstTrainConf::loss_lbn() const {
  return __SharedPtrOrDefault__()->loss_lbn();
}
const ::std::string& ConstTrainConf::loss_lbn(::std::size_t index) const {
  return __SharedPtrOrDefault__()->loss_lbn(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> ConstTrainConf::shared_const_loss_lbn() const {
  return loss_lbn().__SharedConst__();
}
// required or optional field train_step_lbn
bool ConstTrainConf::has_train_step_lbn() const {
  return __SharedPtrOrDefault__()->has_train_step_lbn();
}
const ::std::string& ConstTrainConf::train_step_lbn() const {
  return __SharedPtrOrDefault__()->train_step_lbn();
}
// used by pybind11 only
 // oneof field loss_scale_policy: loss_scale_factor
bool ConstTrainConf::has_loss_scale_factor() const {
  return __SharedPtrOrDefault__()->has_loss_scale_factor();
}
const float& ConstTrainConf::loss_scale_factor() const {
  return __SharedPtrOrDefault__()->loss_scale_factor();
}

// used by pybind11 only
 // oneof field loss_scale_policy: dynamic_loss_scale_policy
bool ConstTrainConf::has_dynamic_loss_scale_policy() const {
  return __SharedPtrOrDefault__()->has_dynamic_loss_scale_policy();
}
const ::oneflow::cfg::DynamicLossScalePolicy& ConstTrainConf::dynamic_loss_scale_policy() const {
  return __SharedPtrOrDefault__()->dynamic_loss_scale_policy();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstDynamicLossScalePolicy> ConstTrainConf::shared_const_dynamic_loss_scale_policy() const {
  return dynamic_loss_scale_policy().__SharedConst__();
}
// required or optional field model_update_conf
bool ConstTrainConf::has_model_update_conf() const {
  return __SharedPtrOrDefault__()->has_model_update_conf();
}
const ::oneflow::cfg::NormalModelUpdateOpUserConf& ConstTrainConf::model_update_conf() const {
  return __SharedPtrOrDefault__()->model_update_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstNormalModelUpdateOpUserConf> ConstTrainConf::shared_const_model_update_conf() const {
  return model_update_conf().__SharedConst__();
}
// required or optional field primary_lr
bool ConstTrainConf::has_primary_lr() const {
  return __SharedPtrOrDefault__()->has_primary_lr();
}
const float& ConstTrainConf::primary_lr() const {
  return __SharedPtrOrDefault__()->primary_lr();
}
// used by pybind11 only
// required or optional field secondary_lr
bool ConstTrainConf::has_secondary_lr() const {
  return __SharedPtrOrDefault__()->has_secondary_lr();
}
const float& ConstTrainConf::secondary_lr() const {
  return __SharedPtrOrDefault__()->secondary_lr();
}
// used by pybind11 only
// required or optional field primary_lr_lbn
bool ConstTrainConf::has_primary_lr_lbn() const {
  return __SharedPtrOrDefault__()->has_primary_lr_lbn();
}
const ::std::string& ConstTrainConf::primary_lr_lbn() const {
  return __SharedPtrOrDefault__()->primary_lr_lbn();
}
// used by pybind11 only
// required or optional field secondary_lr_lbn
bool ConstTrainConf::has_secondary_lr_lbn() const {
  return __SharedPtrOrDefault__()->has_secondary_lr_lbn();
}
const ::std::string& ConstTrainConf::secondary_lr_lbn() const {
  return __SharedPtrOrDefault__()->secondary_lr_lbn();
}
// used by pybind11 only
ConstTrainConf::LossScalePolicyCase ConstTrainConf::loss_scale_policy_case() const {
  return __SharedPtrOrDefault__()->loss_scale_policy_case();
}

bool ConstTrainConf::has_loss_scale_policy() const {
  return __SharedPtrOrDefault__()->has_loss_scale_policy();
}

::std::shared_ptr<ConstTrainConf> ConstTrainConf::__SharedConst__() const {
  return ::std::make_shared<ConstTrainConf>(data_);
}
int64_t ConstTrainConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstTrainConf::operator==(const ConstTrainConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstTrainConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstTrainConf::operator<(const ConstTrainConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_TrainConf_>& ConstTrainConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_TrainConf_> default_ptr = std::make_shared<_TrainConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_TrainConf_>& ConstTrainConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _TrainConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstTrainConf
void ConstTrainConf::BuildFromProto(const PbMessage& proto_trainconf) {
  data_ = ::std::make_shared<_TrainConf_>(dynamic_cast<const ::oneflow::TrainConf&>(proto_trainconf));
}

TrainConf::TrainConf(const ::std::shared_ptr<_TrainConf_>& data)
  : ConstTrainConf(data) {}
TrainConf::TrainConf(const TrainConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<TrainConf> resize
TrainConf::TrainConf(TrainConf&&) noexcept = default; 
TrainConf::TrainConf(const ::oneflow::TrainConf& proto_trainconf) {
  InitFromProto(proto_trainconf);
}
TrainConf::TrainConf() = default;

TrainConf::~TrainConf() = default;

void TrainConf::InitFromProto(const PbMessage& proto_trainconf) {
  BuildFromProto(proto_trainconf);
}
  
void* TrainConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_optimizer_conf();
    case 2: return mutable_loss_lbn();
    case 3: return mutable_train_step_lbn();
    case 4: return mutable_loss_scale_factor();
    case 5: return mutable_dynamic_loss_scale_policy();
    case 101: return mutable_model_update_conf();
    case 102: return mutable_primary_lr();
    case 103: return mutable_secondary_lr();
    case 104: return mutable_primary_lr_lbn();
    case 105: return mutable_secondary_lr_lbn();
    default: return nullptr;
  }
}

bool TrainConf::operator==(const TrainConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t TrainConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool TrainConf::operator<(const TrainConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void TrainConf::Clear() {
  if (data_) { data_.reset(); }
}
void TrainConf::CopyFrom(const TrainConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
TrainConf& TrainConf::operator=(const TrainConf& other) {
  CopyFrom(other);
  return *this;
}

// repeated field optimizer_conf
void TrainConf::clear_optimizer_conf() {
  return __SharedPtr__()->clear_optimizer_conf();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_* TrainConf::mutable_optimizer_conf() {
  return __SharedPtr__()->mutable_optimizer_conf();
}
::oneflow::cfg::OptimizerConf* TrainConf::mutable_optimizer_conf(::std::size_t index) {
  return __SharedPtr__()->mutable_optimizer_conf(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> TrainConf::shared_mutable_optimizer_conf() {
  return mutable_optimizer_conf()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OptimizerConf> TrainConf::shared_mutable_optimizer_conf(::std::size_t index) {
  return mutable_optimizer_conf(index)->__SharedMutable__();
}
::oneflow::cfg::OptimizerConf* TrainConf::add_optimizer_conf() {
  return __SharedPtr__()->add_optimizer_conf();
}
// repeated field loss_lbn
void TrainConf::clear_loss_lbn() {
  return __SharedPtr__()->clear_loss_lbn();
}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_* TrainConf::mutable_loss_lbn() {
  return __SharedPtr__()->mutable_loss_lbn();
}
::std::string* TrainConf::mutable_loss_lbn(::std::size_t index) {
  return __SharedPtr__()->mutable_loss_lbn(index);
}
void TrainConf::add_loss_lbn(const ::std::string& value) {
  return __SharedPtr__()->add_loss_lbn(value);
}
void TrainConf::set_loss_lbn(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_loss_lbn(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> TrainConf::shared_mutable_loss_lbn() {
  return mutable_loss_lbn()->__SharedMutable__();
}
// required or optional field train_step_lbn
void TrainConf::clear_train_step_lbn() {
  return __SharedPtr__()->clear_train_step_lbn();
}
void TrainConf::set_train_step_lbn(const ::std::string& value) {
  return __SharedPtr__()->set_train_step_lbn(value);
}
::std::string* TrainConf::mutable_train_step_lbn() {
  return  __SharedPtr__()->mutable_train_step_lbn();
}
void TrainConf::clear_loss_scale_factor() {
  return __SharedPtr__()->clear_loss_scale_factor();
}
void TrainConf::set_loss_scale_factor(const float& value) {
  return __SharedPtr__()->set_loss_scale_factor(value);
}
float* TrainConf::mutable_loss_scale_factor() {
  return  __SharedPtr__()->mutable_loss_scale_factor();
}
void TrainConf::clear_dynamic_loss_scale_policy() {
  return __SharedPtr__()->clear_dynamic_loss_scale_policy();
}
::oneflow::cfg::DynamicLossScalePolicy* TrainConf::mutable_dynamic_loss_scale_policy() {
  return __SharedPtr__()->mutable_dynamic_loss_scale_policy();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::DynamicLossScalePolicy> TrainConf::shared_mutable_dynamic_loss_scale_policy() {
  return mutable_dynamic_loss_scale_policy()->__SharedMutable__();
}
// required or optional field model_update_conf
void TrainConf::clear_model_update_conf() {
  return __SharedPtr__()->clear_model_update_conf();
}
::oneflow::cfg::NormalModelUpdateOpUserConf* TrainConf::mutable_model_update_conf() {
  return __SharedPtr__()->mutable_model_update_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::NormalModelUpdateOpUserConf> TrainConf::shared_mutable_model_update_conf() {
  return mutable_model_update_conf()->__SharedMutable__();
}
// required or optional field primary_lr
void TrainConf::clear_primary_lr() {
  return __SharedPtr__()->clear_primary_lr();
}
void TrainConf::set_primary_lr(const float& value) {
  return __SharedPtr__()->set_primary_lr(value);
}
float* TrainConf::mutable_primary_lr() {
  return  __SharedPtr__()->mutable_primary_lr();
}
// required or optional field secondary_lr
void TrainConf::clear_secondary_lr() {
  return __SharedPtr__()->clear_secondary_lr();
}
void TrainConf::set_secondary_lr(const float& value) {
  return __SharedPtr__()->set_secondary_lr(value);
}
float* TrainConf::mutable_secondary_lr() {
  return  __SharedPtr__()->mutable_secondary_lr();
}
// required or optional field primary_lr_lbn
void TrainConf::clear_primary_lr_lbn() {
  return __SharedPtr__()->clear_primary_lr_lbn();
}
void TrainConf::set_primary_lr_lbn(const ::std::string& value) {
  return __SharedPtr__()->set_primary_lr_lbn(value);
}
::std::string* TrainConf::mutable_primary_lr_lbn() {
  return  __SharedPtr__()->mutable_primary_lr_lbn();
}
// required or optional field secondary_lr_lbn
void TrainConf::clear_secondary_lr_lbn() {
  return __SharedPtr__()->clear_secondary_lr_lbn();
}
void TrainConf::set_secondary_lr_lbn(const ::std::string& value) {
  return __SharedPtr__()->set_secondary_lr_lbn(value);
}
::std::string* TrainConf::mutable_secondary_lr_lbn() {
  return  __SharedPtr__()->mutable_secondary_lr_lbn();
}

::std::shared_ptr<TrainConf> TrainConf::__SharedMutable__() {
  return ::std::make_shared<TrainConf>(__SharedPtr__());
}
ConstPredictConf::_PredictConf_::_PredictConf_() { Clear(); }
ConstPredictConf::_PredictConf_::_PredictConf_(const _PredictConf_& other) { CopyFrom(other); }
ConstPredictConf::_PredictConf_::_PredictConf_(const ::oneflow::PredictConf& proto_predictconf) {
  InitFromProto(proto_predictconf);
}
ConstPredictConf::_PredictConf_::_PredictConf_(_PredictConf_&& other) = default;
ConstPredictConf::_PredictConf_::~_PredictConf_() = default;

void ConstPredictConf::_PredictConf_::InitFromProto(const ::oneflow::PredictConf& proto_predictconf) {
  Clear();
    
}

void ConstPredictConf::_PredictConf_::ToProto(::oneflow::PredictConf* proto_predictconf) const {
  proto_predictconf->Clear();

}

::std::string ConstPredictConf::_PredictConf_::DebugString() const {
  ::oneflow::PredictConf proto_predictconf;
  this->ToProto(&proto_predictconf);
  return proto_predictconf.DebugString();
}

void ConstPredictConf::_PredictConf_::Clear() {
}

void ConstPredictConf::_PredictConf_::CopyFrom(const _PredictConf_& other) {
}



int ConstPredictConf::_PredictConf_::compare(const _PredictConf_& other) {
  return 0;
}

bool ConstPredictConf::_PredictConf_::operator==(const _PredictConf_& other) const {
  return true
  ;
}

std::size_t ConstPredictConf::_PredictConf_::__CalcHash__() const {
  return 0
  ;
}

bool ConstPredictConf::_PredictConf_::operator<(const _PredictConf_& other) const {
  return false
  ;
}

using _PredictConf_ =  ConstPredictConf::_PredictConf_;
ConstPredictConf::ConstPredictConf(const ::std::shared_ptr<_PredictConf_>& data): data_(data) {}
ConstPredictConf::ConstPredictConf(): data_(::std::make_shared<_PredictConf_>()) {}
ConstPredictConf::ConstPredictConf(const ::oneflow::PredictConf& proto_predictconf) {
  BuildFromProto(proto_predictconf);
}
ConstPredictConf::ConstPredictConf(const ConstPredictConf&) = default;
ConstPredictConf::ConstPredictConf(ConstPredictConf&&) noexcept = default;
ConstPredictConf::~ConstPredictConf() = default;

void ConstPredictConf::ToProto(PbMessage* proto_predictconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::PredictConf*>(proto_predictconf));
}
  
::std::string ConstPredictConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstPredictConf::__Empty__() const {
  return !data_;
}

int ConstPredictConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstPredictConf::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstPredictConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstPredictConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstPredictConf> ConstPredictConf::__SharedConst__() const {
  return ::std::make_shared<ConstPredictConf>(data_);
}
int64_t ConstPredictConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstPredictConf::operator==(const ConstPredictConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstPredictConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstPredictConf::operator<(const ConstPredictConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_PredictConf_>& ConstPredictConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_PredictConf_> default_ptr = std::make_shared<_PredictConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_PredictConf_>& ConstPredictConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _PredictConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstPredictConf
void ConstPredictConf::BuildFromProto(const PbMessage& proto_predictconf) {
  data_ = ::std::make_shared<_PredictConf_>(dynamic_cast<const ::oneflow::PredictConf&>(proto_predictconf));
}

PredictConf::PredictConf(const ::std::shared_ptr<_PredictConf_>& data)
  : ConstPredictConf(data) {}
PredictConf::PredictConf(const PredictConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<PredictConf> resize
PredictConf::PredictConf(PredictConf&&) noexcept = default; 
PredictConf::PredictConf(const ::oneflow::PredictConf& proto_predictconf) {
  InitFromProto(proto_predictconf);
}
PredictConf::PredictConf() = default;

PredictConf::~PredictConf() = default;

void PredictConf::InitFromProto(const PbMessage& proto_predictconf) {
  BuildFromProto(proto_predictconf);
}
  
void* PredictConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool PredictConf::operator==(const PredictConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t PredictConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool PredictConf::operator<(const PredictConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void PredictConf::Clear() {
  if (data_) { data_.reset(); }
}
void PredictConf::CopyFrom(const PredictConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
PredictConf& PredictConf::operator=(const PredictConf& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<PredictConf> PredictConf::__SharedMutable__() {
  return ::std::make_shared<PredictConf>(__SharedPtr__());
}
ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::_MemoryAllocationAlgorithmConf_() { Clear(); }
ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::_MemoryAllocationAlgorithmConf_(const _MemoryAllocationAlgorithmConf_& other) { CopyFrom(other); }
ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::_MemoryAllocationAlgorithmConf_(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf) {
  InitFromProto(proto_memoryallocationalgorithmconf);
}
ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::_MemoryAllocationAlgorithmConf_(_MemoryAllocationAlgorithmConf_&& other) = default;
ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::~_MemoryAllocationAlgorithmConf_() = default;

void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::InitFromProto(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf) {
  Clear();
  // required_or_optional field: use_mem_size_first_algo
  if (proto_memoryallocationalgorithmconf.has_use_mem_size_first_algo()) {
    set_use_mem_size_first_algo(proto_memoryallocationalgorithmconf.use_mem_size_first_algo());
  }
  // required_or_optional field: use_mutual_exclusion_first_algo
  if (proto_memoryallocationalgorithmconf.has_use_mutual_exclusion_first_algo()) {
    set_use_mutual_exclusion_first_algo(proto_memoryallocationalgorithmconf.use_mutual_exclusion_first_algo());
  }
  // required_or_optional field: use_time_line_algo
  if (proto_memoryallocationalgorithmconf.has_use_time_line_algo()) {
    set_use_time_line_algo(proto_memoryallocationalgorithmconf.use_time_line_algo());
  }
    
}

void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::ToProto(::oneflow::MemoryAllocationAlgorithmConf* proto_memoryallocationalgorithmconf) const {
  proto_memoryallocationalgorithmconf->Clear();
  // required_or_optional field: use_mem_size_first_algo
  if (this->has_use_mem_size_first_algo()) {
    proto_memoryallocationalgorithmconf->set_use_mem_size_first_algo(use_mem_size_first_algo());
    }
  // required_or_optional field: use_mutual_exclusion_first_algo
  if (this->has_use_mutual_exclusion_first_algo()) {
    proto_memoryallocationalgorithmconf->set_use_mutual_exclusion_first_algo(use_mutual_exclusion_first_algo());
    }
  // required_or_optional field: use_time_line_algo
  if (this->has_use_time_line_algo()) {
    proto_memoryallocationalgorithmconf->set_use_time_line_algo(use_time_line_algo());
    }

}

::std::string ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::DebugString() const {
  ::oneflow::MemoryAllocationAlgorithmConf proto_memoryallocationalgorithmconf;
  this->ToProto(&proto_memoryallocationalgorithmconf);
  return proto_memoryallocationalgorithmconf.DebugString();
}

void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::Clear() {
  clear_use_mem_size_first_algo();
  clear_use_mutual_exclusion_first_algo();
  clear_use_time_line_algo();
}

void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::CopyFrom(const _MemoryAllocationAlgorithmConf_& other) {
  if (other.has_use_mem_size_first_algo()) {
    set_use_mem_size_first_algo(other.use_mem_size_first_algo());
  } else {
    clear_use_mem_size_first_algo();
  }
  if (other.has_use_mutual_exclusion_first_algo()) {
    set_use_mutual_exclusion_first_algo(other.use_mutual_exclusion_first_algo());
  } else {
    clear_use_mutual_exclusion_first_algo();
  }
  if (other.has_use_time_line_algo()) {
    set_use_time_line_algo(other.use_time_line_algo());
  } else {
    clear_use_time_line_algo();
  }
}


// optional field use_mem_size_first_algo
bool ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::has_use_mem_size_first_algo() const {
  return has_use_mem_size_first_algo_;
}
const bool& ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::use_mem_size_first_algo() const {
  if (has_use_mem_size_first_algo_) { return use_mem_size_first_algo_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::clear_use_mem_size_first_algo() {
  has_use_mem_size_first_algo_ = false;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::set_use_mem_size_first_algo(const bool& value) {
  use_mem_size_first_algo_ = value;
  has_use_mem_size_first_algo_ = true;
}
bool* ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::mutable_use_mem_size_first_algo() {
  has_use_mem_size_first_algo_ = true;
  return &use_mem_size_first_algo_;
}

// optional field use_mutual_exclusion_first_algo
bool ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::has_use_mutual_exclusion_first_algo() const {
  return has_use_mutual_exclusion_first_algo_;
}
const bool& ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::use_mutual_exclusion_first_algo() const {
  if (has_use_mutual_exclusion_first_algo_) { return use_mutual_exclusion_first_algo_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::clear_use_mutual_exclusion_first_algo() {
  has_use_mutual_exclusion_first_algo_ = false;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::set_use_mutual_exclusion_first_algo(const bool& value) {
  use_mutual_exclusion_first_algo_ = value;
  has_use_mutual_exclusion_first_algo_ = true;
}
bool* ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::mutable_use_mutual_exclusion_first_algo() {
  has_use_mutual_exclusion_first_algo_ = true;
  return &use_mutual_exclusion_first_algo_;
}

// optional field use_time_line_algo
bool ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::has_use_time_line_algo() const {
  return has_use_time_line_algo_;
}
const bool& ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::use_time_line_algo() const {
  if (has_use_time_line_algo_) { return use_time_line_algo_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::clear_use_time_line_algo() {
  has_use_time_line_algo_ = false;
}
void ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::set_use_time_line_algo(const bool& value) {
  use_time_line_algo_ = value;
  has_use_time_line_algo_ = true;
}
bool* ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::mutable_use_time_line_algo() {
  has_use_time_line_algo_ = true;
  return &use_time_line_algo_;
}


int ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::compare(const _MemoryAllocationAlgorithmConf_& other) {
  if (!(has_use_mem_size_first_algo() == other.has_use_mem_size_first_algo())) {
    return has_use_mem_size_first_algo() < other.has_use_mem_size_first_algo() ? -1 : 1;
  } else if (!(use_mem_size_first_algo() == other.use_mem_size_first_algo())) {
    return use_mem_size_first_algo() < other.use_mem_size_first_algo() ? -1 : 1;
  }
  if (!(has_use_mutual_exclusion_first_algo() == other.has_use_mutual_exclusion_first_algo())) {
    return has_use_mutual_exclusion_first_algo() < other.has_use_mutual_exclusion_first_algo() ? -1 : 1;
  } else if (!(use_mutual_exclusion_first_algo() == other.use_mutual_exclusion_first_algo())) {
    return use_mutual_exclusion_first_algo() < other.use_mutual_exclusion_first_algo() ? -1 : 1;
  }
  if (!(has_use_time_line_algo() == other.has_use_time_line_algo())) {
    return has_use_time_line_algo() < other.has_use_time_line_algo() ? -1 : 1;
  } else if (!(use_time_line_algo() == other.use_time_line_algo())) {
    return use_time_line_algo() < other.use_time_line_algo() ? -1 : 1;
  }
  return 0;
}

bool ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::operator==(const _MemoryAllocationAlgorithmConf_& other) const {
  return true
    && has_use_mem_size_first_algo() == other.has_use_mem_size_first_algo() 
    && use_mem_size_first_algo() == other.use_mem_size_first_algo()
    && has_use_mutual_exclusion_first_algo() == other.has_use_mutual_exclusion_first_algo() 
    && use_mutual_exclusion_first_algo() == other.use_mutual_exclusion_first_algo()
    && has_use_time_line_algo() == other.has_use_time_line_algo() 
    && use_time_line_algo() == other.use_time_line_algo()
  ;
}

std::size_t ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::__CalcHash__() const {
  return 0
    ^ (has_use_mem_size_first_algo() ? std::hash<bool>()(use_mem_size_first_algo()) : 0)
    ^ (has_use_mutual_exclusion_first_algo() ? std::hash<bool>()(use_mutual_exclusion_first_algo()) : 0)
    ^ (has_use_time_line_algo() ? std::hash<bool>()(use_time_line_algo()) : 0)
  ;
}

bool ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_::operator<(const _MemoryAllocationAlgorithmConf_& other) const {
  return false
    || !(has_use_mem_size_first_algo() == other.has_use_mem_size_first_algo()) ? 
      has_use_mem_size_first_algo() < other.has_use_mem_size_first_algo() : false
    || !(use_mem_size_first_algo() == other.use_mem_size_first_algo()) ? 
      use_mem_size_first_algo() < other.use_mem_size_first_algo() : false
    || !(has_use_mutual_exclusion_first_algo() == other.has_use_mutual_exclusion_first_algo()) ? 
      has_use_mutual_exclusion_first_algo() < other.has_use_mutual_exclusion_first_algo() : false
    || !(use_mutual_exclusion_first_algo() == other.use_mutual_exclusion_first_algo()) ? 
      use_mutual_exclusion_first_algo() < other.use_mutual_exclusion_first_algo() : false
    || !(has_use_time_line_algo() == other.has_use_time_line_algo()) ? 
      has_use_time_line_algo() < other.has_use_time_line_algo() : false
    || !(use_time_line_algo() == other.use_time_line_algo()) ? 
      use_time_line_algo() < other.use_time_line_algo() : false
  ;
}

using _MemoryAllocationAlgorithmConf_ =  ConstMemoryAllocationAlgorithmConf::_MemoryAllocationAlgorithmConf_;
ConstMemoryAllocationAlgorithmConf::ConstMemoryAllocationAlgorithmConf(const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& data): data_(data) {}
ConstMemoryAllocationAlgorithmConf::ConstMemoryAllocationAlgorithmConf(): data_(::std::make_shared<_MemoryAllocationAlgorithmConf_>()) {}
ConstMemoryAllocationAlgorithmConf::ConstMemoryAllocationAlgorithmConf(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf) {
  BuildFromProto(proto_memoryallocationalgorithmconf);
}
ConstMemoryAllocationAlgorithmConf::ConstMemoryAllocationAlgorithmConf(const ConstMemoryAllocationAlgorithmConf&) = default;
ConstMemoryAllocationAlgorithmConf::ConstMemoryAllocationAlgorithmConf(ConstMemoryAllocationAlgorithmConf&&) noexcept = default;
ConstMemoryAllocationAlgorithmConf::~ConstMemoryAllocationAlgorithmConf() = default;

void ConstMemoryAllocationAlgorithmConf::ToProto(PbMessage* proto_memoryallocationalgorithmconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::MemoryAllocationAlgorithmConf*>(proto_memoryallocationalgorithmconf));
}
  
::std::string ConstMemoryAllocationAlgorithmConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstMemoryAllocationAlgorithmConf::__Empty__() const {
  return !data_;
}

int ConstMemoryAllocationAlgorithmConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"use_mem_size_first_algo", 1},
    {"use_mutual_exclusion_first_algo", 2},
    {"use_time_line_algo", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstMemoryAllocationAlgorithmConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstMemoryAllocationAlgorithmConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstMemoryAllocationAlgorithmConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &use_mem_size_first_algo();
    case 2: return &use_mutual_exclusion_first_algo();
    case 3: return &use_time_line_algo();
    default: return nullptr;
  }
}

// required or optional field use_mem_size_first_algo
bool ConstMemoryAllocationAlgorithmConf::has_use_mem_size_first_algo() const {
  return __SharedPtrOrDefault__()->has_use_mem_size_first_algo();
}
const bool& ConstMemoryAllocationAlgorithmConf::use_mem_size_first_algo() const {
  return __SharedPtrOrDefault__()->use_mem_size_first_algo();
}
// used by pybind11 only
// required or optional field use_mutual_exclusion_first_algo
bool ConstMemoryAllocationAlgorithmConf::has_use_mutual_exclusion_first_algo() const {
  return __SharedPtrOrDefault__()->has_use_mutual_exclusion_first_algo();
}
const bool& ConstMemoryAllocationAlgorithmConf::use_mutual_exclusion_first_algo() const {
  return __SharedPtrOrDefault__()->use_mutual_exclusion_first_algo();
}
// used by pybind11 only
// required or optional field use_time_line_algo
bool ConstMemoryAllocationAlgorithmConf::has_use_time_line_algo() const {
  return __SharedPtrOrDefault__()->has_use_time_line_algo();
}
const bool& ConstMemoryAllocationAlgorithmConf::use_time_line_algo() const {
  return __SharedPtrOrDefault__()->use_time_line_algo();
}
// used by pybind11 only

::std::shared_ptr<ConstMemoryAllocationAlgorithmConf> ConstMemoryAllocationAlgorithmConf::__SharedConst__() const {
  return ::std::make_shared<ConstMemoryAllocationAlgorithmConf>(data_);
}
int64_t ConstMemoryAllocationAlgorithmConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstMemoryAllocationAlgorithmConf::operator==(const ConstMemoryAllocationAlgorithmConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstMemoryAllocationAlgorithmConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstMemoryAllocationAlgorithmConf::operator<(const ConstMemoryAllocationAlgorithmConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& ConstMemoryAllocationAlgorithmConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_> default_ptr = std::make_shared<_MemoryAllocationAlgorithmConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& ConstMemoryAllocationAlgorithmConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _MemoryAllocationAlgorithmConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstMemoryAllocationAlgorithmConf
void ConstMemoryAllocationAlgorithmConf::BuildFromProto(const PbMessage& proto_memoryallocationalgorithmconf) {
  data_ = ::std::make_shared<_MemoryAllocationAlgorithmConf_>(dynamic_cast<const ::oneflow::MemoryAllocationAlgorithmConf&>(proto_memoryallocationalgorithmconf));
}

MemoryAllocationAlgorithmConf::MemoryAllocationAlgorithmConf(const ::std::shared_ptr<_MemoryAllocationAlgorithmConf_>& data)
  : ConstMemoryAllocationAlgorithmConf(data) {}
MemoryAllocationAlgorithmConf::MemoryAllocationAlgorithmConf(const MemoryAllocationAlgorithmConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<MemoryAllocationAlgorithmConf> resize
MemoryAllocationAlgorithmConf::MemoryAllocationAlgorithmConf(MemoryAllocationAlgorithmConf&&) noexcept = default; 
MemoryAllocationAlgorithmConf::MemoryAllocationAlgorithmConf(const ::oneflow::MemoryAllocationAlgorithmConf& proto_memoryallocationalgorithmconf) {
  InitFromProto(proto_memoryallocationalgorithmconf);
}
MemoryAllocationAlgorithmConf::MemoryAllocationAlgorithmConf() = default;

MemoryAllocationAlgorithmConf::~MemoryAllocationAlgorithmConf() = default;

void MemoryAllocationAlgorithmConf::InitFromProto(const PbMessage& proto_memoryallocationalgorithmconf) {
  BuildFromProto(proto_memoryallocationalgorithmconf);
}
  
void* MemoryAllocationAlgorithmConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_use_mem_size_first_algo();
    case 2: return mutable_use_mutual_exclusion_first_algo();
    case 3: return mutable_use_time_line_algo();
    default: return nullptr;
  }
}

bool MemoryAllocationAlgorithmConf::operator==(const MemoryAllocationAlgorithmConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t MemoryAllocationAlgorithmConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool MemoryAllocationAlgorithmConf::operator<(const MemoryAllocationAlgorithmConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void MemoryAllocationAlgorithmConf::Clear() {
  if (data_) { data_.reset(); }
}
void MemoryAllocationAlgorithmConf::CopyFrom(const MemoryAllocationAlgorithmConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
MemoryAllocationAlgorithmConf& MemoryAllocationAlgorithmConf::operator=(const MemoryAllocationAlgorithmConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field use_mem_size_first_algo
void MemoryAllocationAlgorithmConf::clear_use_mem_size_first_algo() {
  return __SharedPtr__()->clear_use_mem_size_first_algo();
}
void MemoryAllocationAlgorithmConf::set_use_mem_size_first_algo(const bool& value) {
  return __SharedPtr__()->set_use_mem_size_first_algo(value);
}
bool* MemoryAllocationAlgorithmConf::mutable_use_mem_size_first_algo() {
  return  __SharedPtr__()->mutable_use_mem_size_first_algo();
}
// required or optional field use_mutual_exclusion_first_algo
void MemoryAllocationAlgorithmConf::clear_use_mutual_exclusion_first_algo() {
  return __SharedPtr__()->clear_use_mutual_exclusion_first_algo();
}
void MemoryAllocationAlgorithmConf::set_use_mutual_exclusion_first_algo(const bool& value) {
  return __SharedPtr__()->set_use_mutual_exclusion_first_algo(value);
}
bool* MemoryAllocationAlgorithmConf::mutable_use_mutual_exclusion_first_algo() {
  return  __SharedPtr__()->mutable_use_mutual_exclusion_first_algo();
}
// required or optional field use_time_line_algo
void MemoryAllocationAlgorithmConf::clear_use_time_line_algo() {
  return __SharedPtr__()->clear_use_time_line_algo();
}
void MemoryAllocationAlgorithmConf::set_use_time_line_algo(const bool& value) {
  return __SharedPtr__()->set_use_time_line_algo(value);
}
bool* MemoryAllocationAlgorithmConf::mutable_use_time_line_algo() {
  return  __SharedPtr__()->mutable_use_time_line_algo();
}

::std::shared_ptr<MemoryAllocationAlgorithmConf> MemoryAllocationAlgorithmConf::__SharedMutable__() {
  return ::std::make_shared<MemoryAllocationAlgorithmConf>(__SharedPtr__());
}
ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::_XrtConfig_XlaConfig_() { Clear(); }
ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::_XrtConfig_XlaConfig_(const _XrtConfig_XlaConfig_& other) { CopyFrom(other); }
ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::_XrtConfig_XlaConfig_(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig) {
  InitFromProto(proto_xrtconfig_xlaconfig);
}
ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::_XrtConfig_XlaConfig_(_XrtConfig_XlaConfig_&& other) = default;
ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::~_XrtConfig_XlaConfig_() = default;

void ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::InitFromProto(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig) {
  Clear();
    
}

void ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::ToProto(::oneflow::XrtConfig_XlaConfig* proto_xrtconfig_xlaconfig) const {
  proto_xrtconfig_xlaconfig->Clear();

}

::std::string ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::DebugString() const {
  ::oneflow::XrtConfig_XlaConfig proto_xrtconfig_xlaconfig;
  this->ToProto(&proto_xrtconfig_xlaconfig);
  return proto_xrtconfig_xlaconfig.DebugString();
}

void ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::Clear() {
}

void ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::CopyFrom(const _XrtConfig_XlaConfig_& other) {
}



int ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::compare(const _XrtConfig_XlaConfig_& other) {
  return 0;
}

bool ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::operator==(const _XrtConfig_XlaConfig_& other) const {
  return true
  ;
}

std::size_t ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::__CalcHash__() const {
  return 0
  ;
}

bool ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_::operator<(const _XrtConfig_XlaConfig_& other) const {
  return false
  ;
}

using _XrtConfig_XlaConfig_ =  ConstXrtConfig_XlaConfig::_XrtConfig_XlaConfig_;
ConstXrtConfig_XlaConfig::ConstXrtConfig_XlaConfig(const ::std::shared_ptr<_XrtConfig_XlaConfig_>& data): data_(data) {}
ConstXrtConfig_XlaConfig::ConstXrtConfig_XlaConfig(): data_(::std::make_shared<_XrtConfig_XlaConfig_>()) {}
ConstXrtConfig_XlaConfig::ConstXrtConfig_XlaConfig(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig) {
  BuildFromProto(proto_xrtconfig_xlaconfig);
}
ConstXrtConfig_XlaConfig::ConstXrtConfig_XlaConfig(const ConstXrtConfig_XlaConfig&) = default;
ConstXrtConfig_XlaConfig::ConstXrtConfig_XlaConfig(ConstXrtConfig_XlaConfig&&) noexcept = default;
ConstXrtConfig_XlaConfig::~ConstXrtConfig_XlaConfig() = default;

void ConstXrtConfig_XlaConfig::ToProto(PbMessage* proto_xrtconfig_xlaconfig) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::XrtConfig_XlaConfig*>(proto_xrtconfig_xlaconfig));
}
  
::std::string ConstXrtConfig_XlaConfig::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstXrtConfig_XlaConfig::__Empty__() const {
  return !data_;
}

int ConstXrtConfig_XlaConfig::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstXrtConfig_XlaConfig::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstXrtConfig_XlaConfig::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstXrtConfig_XlaConfig::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstXrtConfig_XlaConfig> ConstXrtConfig_XlaConfig::__SharedConst__() const {
  return ::std::make_shared<ConstXrtConfig_XlaConfig>(data_);
}
int64_t ConstXrtConfig_XlaConfig::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstXrtConfig_XlaConfig::operator==(const ConstXrtConfig_XlaConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstXrtConfig_XlaConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstXrtConfig_XlaConfig::operator<(const ConstXrtConfig_XlaConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_XrtConfig_XlaConfig_>& ConstXrtConfig_XlaConfig::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_XrtConfig_XlaConfig_> default_ptr = std::make_shared<_XrtConfig_XlaConfig_>();
  return default_ptr;
}
const ::std::shared_ptr<_XrtConfig_XlaConfig_>& ConstXrtConfig_XlaConfig::__SharedPtr__() {
  if (!data_) { data_.reset(new _XrtConfig_XlaConfig_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig_XlaConfig
void ConstXrtConfig_XlaConfig::BuildFromProto(const PbMessage& proto_xrtconfig_xlaconfig) {
  data_ = ::std::make_shared<_XrtConfig_XlaConfig_>(dynamic_cast<const ::oneflow::XrtConfig_XlaConfig&>(proto_xrtconfig_xlaconfig));
}

XrtConfig_XlaConfig::XrtConfig_XlaConfig(const ::std::shared_ptr<_XrtConfig_XlaConfig_>& data)
  : ConstXrtConfig_XlaConfig(data) {}
XrtConfig_XlaConfig::XrtConfig_XlaConfig(const XrtConfig_XlaConfig& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<XrtConfig_XlaConfig> resize
XrtConfig_XlaConfig::XrtConfig_XlaConfig(XrtConfig_XlaConfig&&) noexcept = default; 
XrtConfig_XlaConfig::XrtConfig_XlaConfig(const ::oneflow::XrtConfig_XlaConfig& proto_xrtconfig_xlaconfig) {
  InitFromProto(proto_xrtconfig_xlaconfig);
}
XrtConfig_XlaConfig::XrtConfig_XlaConfig() = default;

XrtConfig_XlaConfig::~XrtConfig_XlaConfig() = default;

void XrtConfig_XlaConfig::InitFromProto(const PbMessage& proto_xrtconfig_xlaconfig) {
  BuildFromProto(proto_xrtconfig_xlaconfig);
}
  
void* XrtConfig_XlaConfig::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool XrtConfig_XlaConfig::operator==(const XrtConfig_XlaConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t XrtConfig_XlaConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool XrtConfig_XlaConfig::operator<(const XrtConfig_XlaConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void XrtConfig_XlaConfig::Clear() {
  if (data_) { data_.reset(); }
}
void XrtConfig_XlaConfig::CopyFrom(const XrtConfig_XlaConfig& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
XrtConfig_XlaConfig& XrtConfig_XlaConfig::operator=(const XrtConfig_XlaConfig& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<XrtConfig_XlaConfig> XrtConfig_XlaConfig::__SharedMutable__() {
  return ::std::make_shared<XrtConfig_XlaConfig>(__SharedPtr__());
}
ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::_XrtConfig_TensorRTConfig_() { Clear(); }
ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::_XrtConfig_TensorRTConfig_(const _XrtConfig_TensorRTConfig_& other) { CopyFrom(other); }
ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::_XrtConfig_TensorRTConfig_(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig) {
  InitFromProto(proto_xrtconfig_tensorrtconfig);
}
ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::_XrtConfig_TensorRTConfig_(_XrtConfig_TensorRTConfig_&& other) = default;
ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::~_XrtConfig_TensorRTConfig_() = default;

void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::InitFromProto(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig) {
  Clear();
  // required_or_optional field: use_fp16
  if (proto_xrtconfig_tensorrtconfig.has_use_fp16()) {
    set_use_fp16(proto_xrtconfig_tensorrtconfig.use_fp16());
  }
  // required_or_optional field: use_int8
  if (proto_xrtconfig_tensorrtconfig.has_use_int8()) {
    set_use_int8(proto_xrtconfig_tensorrtconfig.use_int8());
  }
  // required_or_optional field: int8_calibration
  if (proto_xrtconfig_tensorrtconfig.has_int8_calibration()) {
    set_int8_calibration(proto_xrtconfig_tensorrtconfig.int8_calibration());
  }
    
}

void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::ToProto(::oneflow::XrtConfig_TensorRTConfig* proto_xrtconfig_tensorrtconfig) const {
  proto_xrtconfig_tensorrtconfig->Clear();
  // required_or_optional field: use_fp16
  if (this->has_use_fp16()) {
    proto_xrtconfig_tensorrtconfig->set_use_fp16(use_fp16());
    }
  // required_or_optional field: use_int8
  if (this->has_use_int8()) {
    proto_xrtconfig_tensorrtconfig->set_use_int8(use_int8());
    }
  // required_or_optional field: int8_calibration
  if (this->has_int8_calibration()) {
    proto_xrtconfig_tensorrtconfig->set_int8_calibration(int8_calibration());
    }

}

::std::string ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::DebugString() const {
  ::oneflow::XrtConfig_TensorRTConfig proto_xrtconfig_tensorrtconfig;
  this->ToProto(&proto_xrtconfig_tensorrtconfig);
  return proto_xrtconfig_tensorrtconfig.DebugString();
}

void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::Clear() {
  clear_use_fp16();
  clear_use_int8();
  clear_int8_calibration();
}

void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::CopyFrom(const _XrtConfig_TensorRTConfig_& other) {
  if (other.has_use_fp16()) {
    set_use_fp16(other.use_fp16());
  } else {
    clear_use_fp16();
  }
  if (other.has_use_int8()) {
    set_use_int8(other.use_int8());
  } else {
    clear_use_int8();
  }
  if (other.has_int8_calibration()) {
    set_int8_calibration(other.int8_calibration());
  } else {
    clear_int8_calibration();
  }
}


// optional field use_fp16
bool ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::has_use_fp16() const {
  return has_use_fp16_;
}
const bool& ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::use_fp16() const {
  if (has_use_fp16_) { return use_fp16_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::clear_use_fp16() {
  has_use_fp16_ = false;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::set_use_fp16(const bool& value) {
  use_fp16_ = value;
  has_use_fp16_ = true;
}
bool* ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::mutable_use_fp16() {
  has_use_fp16_ = true;
  return &use_fp16_;
}

// optional field use_int8
bool ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::has_use_int8() const {
  return has_use_int8_;
}
const bool& ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::use_int8() const {
  if (has_use_int8_) { return use_int8_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::clear_use_int8() {
  has_use_int8_ = false;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::set_use_int8(const bool& value) {
  use_int8_ = value;
  has_use_int8_ = true;
}
bool* ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::mutable_use_int8() {
  has_use_int8_ = true;
  return &use_int8_;
}

// optional field int8_calibration
bool ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::has_int8_calibration() const {
  return has_int8_calibration_;
}
const ::std::string& ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::int8_calibration() const {
  if (has_int8_calibration_) { return int8_calibration_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::clear_int8_calibration() {
  has_int8_calibration_ = false;
}
void ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::set_int8_calibration(const ::std::string& value) {
  int8_calibration_ = value;
  has_int8_calibration_ = true;
}
::std::string* ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::mutable_int8_calibration() {
  has_int8_calibration_ = true;
  return &int8_calibration_;
}


int ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::compare(const _XrtConfig_TensorRTConfig_& other) {
  if (!(has_use_fp16() == other.has_use_fp16())) {
    return has_use_fp16() < other.has_use_fp16() ? -1 : 1;
  } else if (!(use_fp16() == other.use_fp16())) {
    return use_fp16() < other.use_fp16() ? -1 : 1;
  }
  if (!(has_use_int8() == other.has_use_int8())) {
    return has_use_int8() < other.has_use_int8() ? -1 : 1;
  } else if (!(use_int8() == other.use_int8())) {
    return use_int8() < other.use_int8() ? -1 : 1;
  }
  if (!(has_int8_calibration() == other.has_int8_calibration())) {
    return has_int8_calibration() < other.has_int8_calibration() ? -1 : 1;
  } else if (!(int8_calibration() == other.int8_calibration())) {
    return int8_calibration() < other.int8_calibration() ? -1 : 1;
  }
  return 0;
}

bool ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::operator==(const _XrtConfig_TensorRTConfig_& other) const {
  return true
    && has_use_fp16() == other.has_use_fp16() 
    && use_fp16() == other.use_fp16()
    && has_use_int8() == other.has_use_int8() 
    && use_int8() == other.use_int8()
    && has_int8_calibration() == other.has_int8_calibration() 
    && int8_calibration() == other.int8_calibration()
  ;
}

std::size_t ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::__CalcHash__() const {
  return 0
    ^ (has_use_fp16() ? std::hash<bool>()(use_fp16()) : 0)
    ^ (has_use_int8() ? std::hash<bool>()(use_int8()) : 0)
    ^ (has_int8_calibration() ? std::hash<::std::string>()(int8_calibration()) : 0)
  ;
}

bool ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_::operator<(const _XrtConfig_TensorRTConfig_& other) const {
  return false
    || !(has_use_fp16() == other.has_use_fp16()) ? 
      has_use_fp16() < other.has_use_fp16() : false
    || !(use_fp16() == other.use_fp16()) ? 
      use_fp16() < other.use_fp16() : false
    || !(has_use_int8() == other.has_use_int8()) ? 
      has_use_int8() < other.has_use_int8() : false
    || !(use_int8() == other.use_int8()) ? 
      use_int8() < other.use_int8() : false
    || !(has_int8_calibration() == other.has_int8_calibration()) ? 
      has_int8_calibration() < other.has_int8_calibration() : false
    || !(int8_calibration() == other.int8_calibration()) ? 
      int8_calibration() < other.int8_calibration() : false
  ;
}

using _XrtConfig_TensorRTConfig_ =  ConstXrtConfig_TensorRTConfig::_XrtConfig_TensorRTConfig_;
ConstXrtConfig_TensorRTConfig::ConstXrtConfig_TensorRTConfig(const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& data): data_(data) {}
ConstXrtConfig_TensorRTConfig::ConstXrtConfig_TensorRTConfig(): data_(::std::make_shared<_XrtConfig_TensorRTConfig_>()) {}
ConstXrtConfig_TensorRTConfig::ConstXrtConfig_TensorRTConfig(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig) {
  BuildFromProto(proto_xrtconfig_tensorrtconfig);
}
ConstXrtConfig_TensorRTConfig::ConstXrtConfig_TensorRTConfig(const ConstXrtConfig_TensorRTConfig&) = default;
ConstXrtConfig_TensorRTConfig::ConstXrtConfig_TensorRTConfig(ConstXrtConfig_TensorRTConfig&&) noexcept = default;
ConstXrtConfig_TensorRTConfig::~ConstXrtConfig_TensorRTConfig() = default;

void ConstXrtConfig_TensorRTConfig::ToProto(PbMessage* proto_xrtconfig_tensorrtconfig) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::XrtConfig_TensorRTConfig*>(proto_xrtconfig_tensorrtconfig));
}
  
::std::string ConstXrtConfig_TensorRTConfig::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstXrtConfig_TensorRTConfig::__Empty__() const {
  return !data_;
}

int ConstXrtConfig_TensorRTConfig::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"use_fp16", 1},
    {"use_int8", 2},
    {"int8_calibration", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstXrtConfig_TensorRTConfig::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstXrtConfig_TensorRTConfig::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstXrtConfig_TensorRTConfig::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &use_fp16();
    case 2: return &use_int8();
    case 3: return &int8_calibration();
    default: return nullptr;
  }
}

// required or optional field use_fp16
bool ConstXrtConfig_TensorRTConfig::has_use_fp16() const {
  return __SharedPtrOrDefault__()->has_use_fp16();
}
const bool& ConstXrtConfig_TensorRTConfig::use_fp16() const {
  return __SharedPtrOrDefault__()->use_fp16();
}
// used by pybind11 only
// required or optional field use_int8
bool ConstXrtConfig_TensorRTConfig::has_use_int8() const {
  return __SharedPtrOrDefault__()->has_use_int8();
}
const bool& ConstXrtConfig_TensorRTConfig::use_int8() const {
  return __SharedPtrOrDefault__()->use_int8();
}
// used by pybind11 only
// required or optional field int8_calibration
bool ConstXrtConfig_TensorRTConfig::has_int8_calibration() const {
  return __SharedPtrOrDefault__()->has_int8_calibration();
}
const ::std::string& ConstXrtConfig_TensorRTConfig::int8_calibration() const {
  return __SharedPtrOrDefault__()->int8_calibration();
}
// used by pybind11 only

::std::shared_ptr<ConstXrtConfig_TensorRTConfig> ConstXrtConfig_TensorRTConfig::__SharedConst__() const {
  return ::std::make_shared<ConstXrtConfig_TensorRTConfig>(data_);
}
int64_t ConstXrtConfig_TensorRTConfig::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstXrtConfig_TensorRTConfig::operator==(const ConstXrtConfig_TensorRTConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstXrtConfig_TensorRTConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstXrtConfig_TensorRTConfig::operator<(const ConstXrtConfig_TensorRTConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& ConstXrtConfig_TensorRTConfig::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_XrtConfig_TensorRTConfig_> default_ptr = std::make_shared<_XrtConfig_TensorRTConfig_>();
  return default_ptr;
}
const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& ConstXrtConfig_TensorRTConfig::__SharedPtr__() {
  if (!data_) { data_.reset(new _XrtConfig_TensorRTConfig_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig_TensorRTConfig
void ConstXrtConfig_TensorRTConfig::BuildFromProto(const PbMessage& proto_xrtconfig_tensorrtconfig) {
  data_ = ::std::make_shared<_XrtConfig_TensorRTConfig_>(dynamic_cast<const ::oneflow::XrtConfig_TensorRTConfig&>(proto_xrtconfig_tensorrtconfig));
}

XrtConfig_TensorRTConfig::XrtConfig_TensorRTConfig(const ::std::shared_ptr<_XrtConfig_TensorRTConfig_>& data)
  : ConstXrtConfig_TensorRTConfig(data) {}
XrtConfig_TensorRTConfig::XrtConfig_TensorRTConfig(const XrtConfig_TensorRTConfig& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<XrtConfig_TensorRTConfig> resize
XrtConfig_TensorRTConfig::XrtConfig_TensorRTConfig(XrtConfig_TensorRTConfig&&) noexcept = default; 
XrtConfig_TensorRTConfig::XrtConfig_TensorRTConfig(const ::oneflow::XrtConfig_TensorRTConfig& proto_xrtconfig_tensorrtconfig) {
  InitFromProto(proto_xrtconfig_tensorrtconfig);
}
XrtConfig_TensorRTConfig::XrtConfig_TensorRTConfig() = default;

XrtConfig_TensorRTConfig::~XrtConfig_TensorRTConfig() = default;

void XrtConfig_TensorRTConfig::InitFromProto(const PbMessage& proto_xrtconfig_tensorrtconfig) {
  BuildFromProto(proto_xrtconfig_tensorrtconfig);
}
  
void* XrtConfig_TensorRTConfig::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_use_fp16();
    case 2: return mutable_use_int8();
    case 3: return mutable_int8_calibration();
    default: return nullptr;
  }
}

bool XrtConfig_TensorRTConfig::operator==(const XrtConfig_TensorRTConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t XrtConfig_TensorRTConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool XrtConfig_TensorRTConfig::operator<(const XrtConfig_TensorRTConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void XrtConfig_TensorRTConfig::Clear() {
  if (data_) { data_.reset(); }
}
void XrtConfig_TensorRTConfig::CopyFrom(const XrtConfig_TensorRTConfig& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
XrtConfig_TensorRTConfig& XrtConfig_TensorRTConfig::operator=(const XrtConfig_TensorRTConfig& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field use_fp16
void XrtConfig_TensorRTConfig::clear_use_fp16() {
  return __SharedPtr__()->clear_use_fp16();
}
void XrtConfig_TensorRTConfig::set_use_fp16(const bool& value) {
  return __SharedPtr__()->set_use_fp16(value);
}
bool* XrtConfig_TensorRTConfig::mutable_use_fp16() {
  return  __SharedPtr__()->mutable_use_fp16();
}
// required or optional field use_int8
void XrtConfig_TensorRTConfig::clear_use_int8() {
  return __SharedPtr__()->clear_use_int8();
}
void XrtConfig_TensorRTConfig::set_use_int8(const bool& value) {
  return __SharedPtr__()->set_use_int8(value);
}
bool* XrtConfig_TensorRTConfig::mutable_use_int8() {
  return  __SharedPtr__()->mutable_use_int8();
}
// required or optional field int8_calibration
void XrtConfig_TensorRTConfig::clear_int8_calibration() {
  return __SharedPtr__()->clear_int8_calibration();
}
void XrtConfig_TensorRTConfig::set_int8_calibration(const ::std::string& value) {
  return __SharedPtr__()->set_int8_calibration(value);
}
::std::string* XrtConfig_TensorRTConfig::mutable_int8_calibration() {
  return  __SharedPtr__()->mutable_int8_calibration();
}

::std::shared_ptr<XrtConfig_TensorRTConfig> XrtConfig_TensorRTConfig::__SharedMutable__() {
  return ::std::make_shared<XrtConfig_TensorRTConfig>(__SharedPtr__());
}
ConstXrtConfig::_XrtConfig_::_XrtConfig_() { Clear(); }
ConstXrtConfig::_XrtConfig_::_XrtConfig_(const _XrtConfig_& other) { CopyFrom(other); }
ConstXrtConfig::_XrtConfig_::_XrtConfig_(const ::oneflow::XrtConfig& proto_xrtconfig) {
  InitFromProto(proto_xrtconfig);
}
ConstXrtConfig::_XrtConfig_::_XrtConfig_(_XrtConfig_&& other) = default;
ConstXrtConfig::_XrtConfig_::~_XrtConfig_() = default;

void ConstXrtConfig::_XrtConfig_::InitFromProto(const ::oneflow::XrtConfig& proto_xrtconfig) {
  Clear();
  // required_or_optional field: use_xla_jit
  if (proto_xrtconfig.has_use_xla_jit()) {
    set_use_xla_jit(proto_xrtconfig.use_xla_jit());
  }
  // required_or_optional field: use_tensorrt
  if (proto_xrtconfig.has_use_tensorrt()) {
    set_use_tensorrt(proto_xrtconfig.use_tensorrt());
  }
  // required_or_optional field: xla_config
  if (proto_xrtconfig.has_xla_config()) {
  *mutable_xla_config() = ::oneflow::cfg::XrtConfig_XlaConfig(proto_xrtconfig.xla_config());      
  }
  // required_or_optional field: tensorrt_config
  if (proto_xrtconfig.has_tensorrt_config()) {
  *mutable_tensorrt_config() = ::oneflow::cfg::XrtConfig_TensorRTConfig(proto_xrtconfig.tensorrt_config());      
  }
    
}

void ConstXrtConfig::_XrtConfig_::ToProto(::oneflow::XrtConfig* proto_xrtconfig) const {
  proto_xrtconfig->Clear();
  // required_or_optional field: use_xla_jit
  if (this->has_use_xla_jit()) {
    proto_xrtconfig->set_use_xla_jit(use_xla_jit());
    }
  // required_or_optional field: use_tensorrt
  if (this->has_use_tensorrt()) {
    proto_xrtconfig->set_use_tensorrt(use_tensorrt());
    }
  // required_or_optional field: xla_config
  if (this->has_xla_config()) {
    ::oneflow::XrtConfig_XlaConfig proto_xla_config;
    xla_config().ToProto(&proto_xla_config);
    proto_xrtconfig->mutable_xla_config()->CopyFrom(proto_xla_config);
    }
  // required_or_optional field: tensorrt_config
  if (this->has_tensorrt_config()) {
    ::oneflow::XrtConfig_TensorRTConfig proto_tensorrt_config;
    tensorrt_config().ToProto(&proto_tensorrt_config);
    proto_xrtconfig->mutable_tensorrt_config()->CopyFrom(proto_tensorrt_config);
    }

}

::std::string ConstXrtConfig::_XrtConfig_::DebugString() const {
  ::oneflow::XrtConfig proto_xrtconfig;
  this->ToProto(&proto_xrtconfig);
  return proto_xrtconfig.DebugString();
}

void ConstXrtConfig::_XrtConfig_::Clear() {
  clear_use_xla_jit();
  clear_use_tensorrt();
  clear_xla_config();
  clear_tensorrt_config();
}

void ConstXrtConfig::_XrtConfig_::CopyFrom(const _XrtConfig_& other) {
  if (other.has_use_xla_jit()) {
    set_use_xla_jit(other.use_xla_jit());
  } else {
    clear_use_xla_jit();
  }
  if (other.has_use_tensorrt()) {
    set_use_tensorrt(other.use_tensorrt());
  } else {
    clear_use_tensorrt();
  }
  if (other.has_xla_config()) {
    mutable_xla_config()->CopyFrom(other.xla_config());
  } else {
    clear_xla_config();
  }
  if (other.has_tensorrt_config()) {
    mutable_tensorrt_config()->CopyFrom(other.tensorrt_config());
  } else {
    clear_tensorrt_config();
  }
}


// optional field use_xla_jit
bool ConstXrtConfig::_XrtConfig_::has_use_xla_jit() const {
  return has_use_xla_jit_;
}
const bool& ConstXrtConfig::_XrtConfig_::use_xla_jit() const {
  if (has_use_xla_jit_) { return use_xla_jit_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstXrtConfig::_XrtConfig_::clear_use_xla_jit() {
  has_use_xla_jit_ = false;
}
void ConstXrtConfig::_XrtConfig_::set_use_xla_jit(const bool& value) {
  use_xla_jit_ = value;
  has_use_xla_jit_ = true;
}
bool* ConstXrtConfig::_XrtConfig_::mutable_use_xla_jit() {
  has_use_xla_jit_ = true;
  return &use_xla_jit_;
}

// optional field use_tensorrt
bool ConstXrtConfig::_XrtConfig_::has_use_tensorrt() const {
  return has_use_tensorrt_;
}
const bool& ConstXrtConfig::_XrtConfig_::use_tensorrt() const {
  if (has_use_tensorrt_) { return use_tensorrt_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstXrtConfig::_XrtConfig_::clear_use_tensorrt() {
  has_use_tensorrt_ = false;
}
void ConstXrtConfig::_XrtConfig_::set_use_tensorrt(const bool& value) {
  use_tensorrt_ = value;
  has_use_tensorrt_ = true;
}
bool* ConstXrtConfig::_XrtConfig_::mutable_use_tensorrt() {
  has_use_tensorrt_ = true;
  return &use_tensorrt_;
}

// optional field xla_config
bool ConstXrtConfig::_XrtConfig_::has_xla_config() const {
  return has_xla_config_;
}
const ::oneflow::cfg::XrtConfig_XlaConfig& ConstXrtConfig::_XrtConfig_::xla_config() const {
  if (!xla_config_) {
    static const ::std::shared_ptr<::oneflow::cfg::XrtConfig_XlaConfig> default_static_value =
      ::std::make_shared<::oneflow::cfg::XrtConfig_XlaConfig>();
    return *default_static_value;
  }
  return *(xla_config_.get());
}
void ConstXrtConfig::_XrtConfig_::clear_xla_config() {
  if (xla_config_) {
    xla_config_->Clear();
  }
  has_xla_config_ = false;
}
::oneflow::cfg::XrtConfig_XlaConfig* ConstXrtConfig::_XrtConfig_::mutable_xla_config() {
  if (!xla_config_) {
    xla_config_ = ::std::make_shared<::oneflow::cfg::XrtConfig_XlaConfig>();
  }
  has_xla_config_ = true;
  return xla_config_.get();
}

// optional field tensorrt_config
bool ConstXrtConfig::_XrtConfig_::has_tensorrt_config() const {
  return has_tensorrt_config_;
}
const ::oneflow::cfg::XrtConfig_TensorRTConfig& ConstXrtConfig::_XrtConfig_::tensorrt_config() const {
  if (!tensorrt_config_) {
    static const ::std::shared_ptr<::oneflow::cfg::XrtConfig_TensorRTConfig> default_static_value =
      ::std::make_shared<::oneflow::cfg::XrtConfig_TensorRTConfig>();
    return *default_static_value;
  }
  return *(tensorrt_config_.get());
}
void ConstXrtConfig::_XrtConfig_::clear_tensorrt_config() {
  if (tensorrt_config_) {
    tensorrt_config_->Clear();
  }
  has_tensorrt_config_ = false;
}
::oneflow::cfg::XrtConfig_TensorRTConfig* ConstXrtConfig::_XrtConfig_::mutable_tensorrt_config() {
  if (!tensorrt_config_) {
    tensorrt_config_ = ::std::make_shared<::oneflow::cfg::XrtConfig_TensorRTConfig>();
  }
  has_tensorrt_config_ = true;
  return tensorrt_config_.get();
}


int ConstXrtConfig::_XrtConfig_::compare(const _XrtConfig_& other) {
  if (!(has_use_xla_jit() == other.has_use_xla_jit())) {
    return has_use_xla_jit() < other.has_use_xla_jit() ? -1 : 1;
  } else if (!(use_xla_jit() == other.use_xla_jit())) {
    return use_xla_jit() < other.use_xla_jit() ? -1 : 1;
  }
  if (!(has_use_tensorrt() == other.has_use_tensorrt())) {
    return has_use_tensorrt() < other.has_use_tensorrt() ? -1 : 1;
  } else if (!(use_tensorrt() == other.use_tensorrt())) {
    return use_tensorrt() < other.use_tensorrt() ? -1 : 1;
  }
  if (!(has_xla_config() == other.has_xla_config())) {
    return has_xla_config() < other.has_xla_config() ? -1 : 1;
  } else if (!(xla_config() == other.xla_config())) {
    return xla_config() < other.xla_config() ? -1 : 1;
  }
  if (!(has_tensorrt_config() == other.has_tensorrt_config())) {
    return has_tensorrt_config() < other.has_tensorrt_config() ? -1 : 1;
  } else if (!(tensorrt_config() == other.tensorrt_config())) {
    return tensorrt_config() < other.tensorrt_config() ? -1 : 1;
  }
  return 0;
}

bool ConstXrtConfig::_XrtConfig_::operator==(const _XrtConfig_& other) const {
  return true
    && has_use_xla_jit() == other.has_use_xla_jit() 
    && use_xla_jit() == other.use_xla_jit()
    && has_use_tensorrt() == other.has_use_tensorrt() 
    && use_tensorrt() == other.use_tensorrt()
    && has_xla_config() == other.has_xla_config() 
    && xla_config() == other.xla_config()
    && has_tensorrt_config() == other.has_tensorrt_config() 
    && tensorrt_config() == other.tensorrt_config()
  ;
}

std::size_t ConstXrtConfig::_XrtConfig_::__CalcHash__() const {
  return 0
    ^ (has_use_xla_jit() ? std::hash<bool>()(use_xla_jit()) : 0)
    ^ (has_use_tensorrt() ? std::hash<bool>()(use_tensorrt()) : 0)
    ^ (has_xla_config() ? std::hash<::oneflow::cfg::XrtConfig_XlaConfig>()(xla_config()) : 0)
    ^ (has_tensorrt_config() ? std::hash<::oneflow::cfg::XrtConfig_TensorRTConfig>()(tensorrt_config()) : 0)
  ;
}

bool ConstXrtConfig::_XrtConfig_::operator<(const _XrtConfig_& other) const {
  return false
    || !(has_use_xla_jit() == other.has_use_xla_jit()) ? 
      has_use_xla_jit() < other.has_use_xla_jit() : false
    || !(use_xla_jit() == other.use_xla_jit()) ? 
      use_xla_jit() < other.use_xla_jit() : false
    || !(has_use_tensorrt() == other.has_use_tensorrt()) ? 
      has_use_tensorrt() < other.has_use_tensorrt() : false
    || !(use_tensorrt() == other.use_tensorrt()) ? 
      use_tensorrt() < other.use_tensorrt() : false
    || !(has_xla_config() == other.has_xla_config()) ? 
      has_xla_config() < other.has_xla_config() : false
    || !(xla_config() == other.xla_config()) ? 
      xla_config() < other.xla_config() : false
    || !(has_tensorrt_config() == other.has_tensorrt_config()) ? 
      has_tensorrt_config() < other.has_tensorrt_config() : false
    || !(tensorrt_config() == other.tensorrt_config()) ? 
      tensorrt_config() < other.tensorrt_config() : false
  ;
}

using _XrtConfig_ =  ConstXrtConfig::_XrtConfig_;
ConstXrtConfig::ConstXrtConfig(const ::std::shared_ptr<_XrtConfig_>& data): data_(data) {}
ConstXrtConfig::ConstXrtConfig(): data_(::std::make_shared<_XrtConfig_>()) {}
ConstXrtConfig::ConstXrtConfig(const ::oneflow::XrtConfig& proto_xrtconfig) {
  BuildFromProto(proto_xrtconfig);
}
ConstXrtConfig::ConstXrtConfig(const ConstXrtConfig&) = default;
ConstXrtConfig::ConstXrtConfig(ConstXrtConfig&&) noexcept = default;
ConstXrtConfig::~ConstXrtConfig() = default;

void ConstXrtConfig::ToProto(PbMessage* proto_xrtconfig) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::XrtConfig*>(proto_xrtconfig));
}
  
::std::string ConstXrtConfig::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstXrtConfig::__Empty__() const {
  return !data_;
}

int ConstXrtConfig::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"use_xla_jit", 1},
    {"use_tensorrt", 2},
    {"xla_config", 3},
    {"tensorrt_config", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstXrtConfig::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstXrtConfig::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::XrtConfig_XlaConfig),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstXrtConfig_XlaConfig),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::XrtConfig_TensorRTConfig),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstXrtConfig_TensorRTConfig),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstXrtConfig::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &use_xla_jit();
    case 2: return &use_tensorrt();
    case 3: return &xla_config();
    case 4: return &tensorrt_config();
    default: return nullptr;
  }
}

// required or optional field use_xla_jit
bool ConstXrtConfig::has_use_xla_jit() const {
  return __SharedPtrOrDefault__()->has_use_xla_jit();
}
const bool& ConstXrtConfig::use_xla_jit() const {
  return __SharedPtrOrDefault__()->use_xla_jit();
}
// used by pybind11 only
// required or optional field use_tensorrt
bool ConstXrtConfig::has_use_tensorrt() const {
  return __SharedPtrOrDefault__()->has_use_tensorrt();
}
const bool& ConstXrtConfig::use_tensorrt() const {
  return __SharedPtrOrDefault__()->use_tensorrt();
}
// used by pybind11 only
// required or optional field xla_config
bool ConstXrtConfig::has_xla_config() const {
  return __SharedPtrOrDefault__()->has_xla_config();
}
const ::oneflow::cfg::XrtConfig_XlaConfig& ConstXrtConfig::xla_config() const {
  return __SharedPtrOrDefault__()->xla_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstXrtConfig_XlaConfig> ConstXrtConfig::shared_const_xla_config() const {
  return xla_config().__SharedConst__();
}
// required or optional field tensorrt_config
bool ConstXrtConfig::has_tensorrt_config() const {
  return __SharedPtrOrDefault__()->has_tensorrt_config();
}
const ::oneflow::cfg::XrtConfig_TensorRTConfig& ConstXrtConfig::tensorrt_config() const {
  return __SharedPtrOrDefault__()->tensorrt_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstXrtConfig_TensorRTConfig> ConstXrtConfig::shared_const_tensorrt_config() const {
  return tensorrt_config().__SharedConst__();
}

::std::shared_ptr<ConstXrtConfig> ConstXrtConfig::__SharedConst__() const {
  return ::std::make_shared<ConstXrtConfig>(data_);
}
int64_t ConstXrtConfig::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstXrtConfig::operator==(const ConstXrtConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstXrtConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstXrtConfig::operator<(const ConstXrtConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_XrtConfig_>& ConstXrtConfig::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_XrtConfig_> default_ptr = std::make_shared<_XrtConfig_>();
  return default_ptr;
}
const ::std::shared_ptr<_XrtConfig_>& ConstXrtConfig::__SharedPtr__() {
  if (!data_) { data_.reset(new _XrtConfig_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstXrtConfig
void ConstXrtConfig::BuildFromProto(const PbMessage& proto_xrtconfig) {
  data_ = ::std::make_shared<_XrtConfig_>(dynamic_cast<const ::oneflow::XrtConfig&>(proto_xrtconfig));
}

XrtConfig::XrtConfig(const ::std::shared_ptr<_XrtConfig_>& data)
  : ConstXrtConfig(data) {}
XrtConfig::XrtConfig(const XrtConfig& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<XrtConfig> resize
XrtConfig::XrtConfig(XrtConfig&&) noexcept = default; 
XrtConfig::XrtConfig(const ::oneflow::XrtConfig& proto_xrtconfig) {
  InitFromProto(proto_xrtconfig);
}
XrtConfig::XrtConfig() = default;

XrtConfig::~XrtConfig() = default;

void XrtConfig::InitFromProto(const PbMessage& proto_xrtconfig) {
  BuildFromProto(proto_xrtconfig);
}
  
void* XrtConfig::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_use_xla_jit();
    case 2: return mutable_use_tensorrt();
    case 3: return mutable_xla_config();
    case 4: return mutable_tensorrt_config();
    default: return nullptr;
  }
}

bool XrtConfig::operator==(const XrtConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t XrtConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool XrtConfig::operator<(const XrtConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void XrtConfig::Clear() {
  if (data_) { data_.reset(); }
}
void XrtConfig::CopyFrom(const XrtConfig& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
XrtConfig& XrtConfig::operator=(const XrtConfig& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field use_xla_jit
void XrtConfig::clear_use_xla_jit() {
  return __SharedPtr__()->clear_use_xla_jit();
}
void XrtConfig::set_use_xla_jit(const bool& value) {
  return __SharedPtr__()->set_use_xla_jit(value);
}
bool* XrtConfig::mutable_use_xla_jit() {
  return  __SharedPtr__()->mutable_use_xla_jit();
}
// required or optional field use_tensorrt
void XrtConfig::clear_use_tensorrt() {
  return __SharedPtr__()->clear_use_tensorrt();
}
void XrtConfig::set_use_tensorrt(const bool& value) {
  return __SharedPtr__()->set_use_tensorrt(value);
}
bool* XrtConfig::mutable_use_tensorrt() {
  return  __SharedPtr__()->mutable_use_tensorrt();
}
// required or optional field xla_config
void XrtConfig::clear_xla_config() {
  return __SharedPtr__()->clear_xla_config();
}
::oneflow::cfg::XrtConfig_XlaConfig* XrtConfig::mutable_xla_config() {
  return __SharedPtr__()->mutable_xla_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::XrtConfig_XlaConfig> XrtConfig::shared_mutable_xla_config() {
  return mutable_xla_config()->__SharedMutable__();
}
// required or optional field tensorrt_config
void XrtConfig::clear_tensorrt_config() {
  return __SharedPtr__()->clear_tensorrt_config();
}
::oneflow::cfg::XrtConfig_TensorRTConfig* XrtConfig::mutable_tensorrt_config() {
  return __SharedPtr__()->mutable_tensorrt_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::XrtConfig_TensorRTConfig> XrtConfig::shared_mutable_tensorrt_config() {
  return mutable_tensorrt_config()->__SharedMutable__();
}

::std::shared_ptr<XrtConfig> XrtConfig::__SharedMutable__() {
  return ::std::make_shared<XrtConfig>(__SharedPtr__());
}
ConstQatConfig::_QatConfig_::_QatConfig_() { Clear(); }
ConstQatConfig::_QatConfig_::_QatConfig_(const _QatConfig_& other) { CopyFrom(other); }
ConstQatConfig::_QatConfig_::_QatConfig_(const ::oneflow::QatConfig& proto_qatconfig) {
  InitFromProto(proto_qatconfig);
}
ConstQatConfig::_QatConfig_::_QatConfig_(_QatConfig_&& other) = default;
ConstQatConfig::_QatConfig_::~_QatConfig_() = default;

void ConstQatConfig::_QatConfig_::InitFromProto(const ::oneflow::QatConfig& proto_qatconfig) {
  Clear();
  // required_or_optional field: per_channel_weight_quantization
  if (proto_qatconfig.has_per_channel_weight_quantization()) {
    set_per_channel_weight_quantization(proto_qatconfig.per_channel_weight_quantization());
  }
  // required_or_optional field: symmetric
  if (proto_qatconfig.has_symmetric()) {
    set_symmetric(proto_qatconfig.symmetric());
  }
  // required_or_optional field: moving_min_max_momentum
  if (proto_qatconfig.has_moving_min_max_momentum()) {
    set_moving_min_max_momentum(proto_qatconfig.moving_min_max_momentum());
  }
  // required_or_optional field: moving_min_max_stop_update_after_iters
  if (proto_qatconfig.has_moving_min_max_stop_update_after_iters()) {
    set_moving_min_max_stop_update_after_iters(proto_qatconfig.moving_min_max_stop_update_after_iters());
  }
  // required_or_optional field: target_backend
  if (proto_qatconfig.has_target_backend()) {
    set_target_backend(proto_qatconfig.target_backend());
  }
    
}

void ConstQatConfig::_QatConfig_::ToProto(::oneflow::QatConfig* proto_qatconfig) const {
  proto_qatconfig->Clear();
  // required_or_optional field: per_channel_weight_quantization
  if (this->has_per_channel_weight_quantization()) {
    proto_qatconfig->set_per_channel_weight_quantization(per_channel_weight_quantization());
    }
  // required_or_optional field: symmetric
  if (this->has_symmetric()) {
    proto_qatconfig->set_symmetric(symmetric());
    }
  // required_or_optional field: moving_min_max_momentum
  if (this->has_moving_min_max_momentum()) {
    proto_qatconfig->set_moving_min_max_momentum(moving_min_max_momentum());
    }
  // required_or_optional field: moving_min_max_stop_update_after_iters
  if (this->has_moving_min_max_stop_update_after_iters()) {
    proto_qatconfig->set_moving_min_max_stop_update_after_iters(moving_min_max_stop_update_after_iters());
    }
  // required_or_optional field: target_backend
  if (this->has_target_backend()) {
    proto_qatconfig->set_target_backend(target_backend());
    }

}

::std::string ConstQatConfig::_QatConfig_::DebugString() const {
  ::oneflow::QatConfig proto_qatconfig;
  this->ToProto(&proto_qatconfig);
  return proto_qatconfig.DebugString();
}

void ConstQatConfig::_QatConfig_::Clear() {
  clear_per_channel_weight_quantization();
  clear_symmetric();
  clear_moving_min_max_momentum();
  clear_moving_min_max_stop_update_after_iters();
  clear_target_backend();
}

void ConstQatConfig::_QatConfig_::CopyFrom(const _QatConfig_& other) {
  if (other.has_per_channel_weight_quantization()) {
    set_per_channel_weight_quantization(other.per_channel_weight_quantization());
  } else {
    clear_per_channel_weight_quantization();
  }
  if (other.has_symmetric()) {
    set_symmetric(other.symmetric());
  } else {
    clear_symmetric();
  }
  if (other.has_moving_min_max_momentum()) {
    set_moving_min_max_momentum(other.moving_min_max_momentum());
  } else {
    clear_moving_min_max_momentum();
  }
  if (other.has_moving_min_max_stop_update_after_iters()) {
    set_moving_min_max_stop_update_after_iters(other.moving_min_max_stop_update_after_iters());
  } else {
    clear_moving_min_max_stop_update_after_iters();
  }
  if (other.has_target_backend()) {
    set_target_backend(other.target_backend());
  } else {
    clear_target_backend();
  }
}


// optional field per_channel_weight_quantization
bool ConstQatConfig::_QatConfig_::has_per_channel_weight_quantization() const {
  return has_per_channel_weight_quantization_;
}
const bool& ConstQatConfig::_QatConfig_::per_channel_weight_quantization() const {
  if (has_per_channel_weight_quantization_) { return per_channel_weight_quantization_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstQatConfig::_QatConfig_::clear_per_channel_weight_quantization() {
  has_per_channel_weight_quantization_ = false;
}
void ConstQatConfig::_QatConfig_::set_per_channel_weight_quantization(const bool& value) {
  per_channel_weight_quantization_ = value;
  has_per_channel_weight_quantization_ = true;
}
bool* ConstQatConfig::_QatConfig_::mutable_per_channel_weight_quantization() {
  has_per_channel_weight_quantization_ = true;
  return &per_channel_weight_quantization_;
}

// optional field symmetric
bool ConstQatConfig::_QatConfig_::has_symmetric() const {
  return has_symmetric_;
}
const bool& ConstQatConfig::_QatConfig_::symmetric() const {
  if (has_symmetric_) { return symmetric_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstQatConfig::_QatConfig_::clear_symmetric() {
  has_symmetric_ = false;
}
void ConstQatConfig::_QatConfig_::set_symmetric(const bool& value) {
  symmetric_ = value;
  has_symmetric_ = true;
}
bool* ConstQatConfig::_QatConfig_::mutable_symmetric() {
  has_symmetric_ = true;
  return &symmetric_;
}

// optional field moving_min_max_momentum
bool ConstQatConfig::_QatConfig_::has_moving_min_max_momentum() const {
  return has_moving_min_max_momentum_;
}
const float& ConstQatConfig::_QatConfig_::moving_min_max_momentum() const {
  if (has_moving_min_max_momentum_) { return moving_min_max_momentum_; }
  static const float default_static_value =
    float(0.949999988079071);
  return default_static_value;
}
void ConstQatConfig::_QatConfig_::clear_moving_min_max_momentum() {
  has_moving_min_max_momentum_ = false;
}
void ConstQatConfig::_QatConfig_::set_moving_min_max_momentum(const float& value) {
  moving_min_max_momentum_ = value;
  has_moving_min_max_momentum_ = true;
}
float* ConstQatConfig::_QatConfig_::mutable_moving_min_max_momentum() {
  has_moving_min_max_momentum_ = true;
  return &moving_min_max_momentum_;
}

// optional field moving_min_max_stop_update_after_iters
bool ConstQatConfig::_QatConfig_::has_moving_min_max_stop_update_after_iters() const {
  return has_moving_min_max_stop_update_after_iters_;
}
const int64_t& ConstQatConfig::_QatConfig_::moving_min_max_stop_update_after_iters() const {
  if (has_moving_min_max_stop_update_after_iters_) { return moving_min_max_stop_update_after_iters_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstQatConfig::_QatConfig_::clear_moving_min_max_stop_update_after_iters() {
  has_moving_min_max_stop_update_after_iters_ = false;
}
void ConstQatConfig::_QatConfig_::set_moving_min_max_stop_update_after_iters(const int64_t& value) {
  moving_min_max_stop_update_after_iters_ = value;
  has_moving_min_max_stop_update_after_iters_ = true;
}
int64_t* ConstQatConfig::_QatConfig_::mutable_moving_min_max_stop_update_after_iters() {
  has_moving_min_max_stop_update_after_iters_ = true;
  return &moving_min_max_stop_update_after_iters_;
}

// optional field target_backend
bool ConstQatConfig::_QatConfig_::has_target_backend() const {
  return has_target_backend_;
}
const ::std::string& ConstQatConfig::_QatConfig_::target_backend() const {
  if (has_target_backend_) { return target_backend_; }
  static const ::std::string default_static_value =
    ::std::string("");
  return default_static_value;
}
void ConstQatConfig::_QatConfig_::clear_target_backend() {
  has_target_backend_ = false;
}
void ConstQatConfig::_QatConfig_::set_target_backend(const ::std::string& value) {
  target_backend_ = value;
  has_target_backend_ = true;
}
::std::string* ConstQatConfig::_QatConfig_::mutable_target_backend() {
  has_target_backend_ = true;
  return &target_backend_;
}


int ConstQatConfig::_QatConfig_::compare(const _QatConfig_& other) {
  if (!(has_per_channel_weight_quantization() == other.has_per_channel_weight_quantization())) {
    return has_per_channel_weight_quantization() < other.has_per_channel_weight_quantization() ? -1 : 1;
  } else if (!(per_channel_weight_quantization() == other.per_channel_weight_quantization())) {
    return per_channel_weight_quantization() < other.per_channel_weight_quantization() ? -1 : 1;
  }
  if (!(has_symmetric() == other.has_symmetric())) {
    return has_symmetric() < other.has_symmetric() ? -1 : 1;
  } else if (!(symmetric() == other.symmetric())) {
    return symmetric() < other.symmetric() ? -1 : 1;
  }
  if (!(has_moving_min_max_momentum() == other.has_moving_min_max_momentum())) {
    return has_moving_min_max_momentum() < other.has_moving_min_max_momentum() ? -1 : 1;
  } else if (!(moving_min_max_momentum() == other.moving_min_max_momentum())) {
    return moving_min_max_momentum() < other.moving_min_max_momentum() ? -1 : 1;
  }
  if (!(has_moving_min_max_stop_update_after_iters() == other.has_moving_min_max_stop_update_after_iters())) {
    return has_moving_min_max_stop_update_after_iters() < other.has_moving_min_max_stop_update_after_iters() ? -1 : 1;
  } else if (!(moving_min_max_stop_update_after_iters() == other.moving_min_max_stop_update_after_iters())) {
    return moving_min_max_stop_update_after_iters() < other.moving_min_max_stop_update_after_iters() ? -1 : 1;
  }
  if (!(has_target_backend() == other.has_target_backend())) {
    return has_target_backend() < other.has_target_backend() ? -1 : 1;
  } else if (!(target_backend() == other.target_backend())) {
    return target_backend() < other.target_backend() ? -1 : 1;
  }
  return 0;
}

bool ConstQatConfig::_QatConfig_::operator==(const _QatConfig_& other) const {
  return true
    && has_per_channel_weight_quantization() == other.has_per_channel_weight_quantization() 
    && per_channel_weight_quantization() == other.per_channel_weight_quantization()
    && has_symmetric() == other.has_symmetric() 
    && symmetric() == other.symmetric()
    && has_moving_min_max_momentum() == other.has_moving_min_max_momentum() 
    && moving_min_max_momentum() == other.moving_min_max_momentum()
    && has_moving_min_max_stop_update_after_iters() == other.has_moving_min_max_stop_update_after_iters() 
    && moving_min_max_stop_update_after_iters() == other.moving_min_max_stop_update_after_iters()
    && has_target_backend() == other.has_target_backend() 
    && target_backend() == other.target_backend()
  ;
}

std::size_t ConstQatConfig::_QatConfig_::__CalcHash__() const {
  return 0
    ^ (has_per_channel_weight_quantization() ? std::hash<bool>()(per_channel_weight_quantization()) : 0)
    ^ (has_symmetric() ? std::hash<bool>()(symmetric()) : 0)
    ^ (has_moving_min_max_momentum() ? std::hash<float>()(moving_min_max_momentum()) : 0)
    ^ (has_moving_min_max_stop_update_after_iters() ? std::hash<int64_t>()(moving_min_max_stop_update_after_iters()) : 0)
    ^ (has_target_backend() ? std::hash<::std::string>()(target_backend()) : 0)
  ;
}

bool ConstQatConfig::_QatConfig_::operator<(const _QatConfig_& other) const {
  return false
    || !(has_per_channel_weight_quantization() == other.has_per_channel_weight_quantization()) ? 
      has_per_channel_weight_quantization() < other.has_per_channel_weight_quantization() : false
    || !(per_channel_weight_quantization() == other.per_channel_weight_quantization()) ? 
      per_channel_weight_quantization() < other.per_channel_weight_quantization() : false
    || !(has_symmetric() == other.has_symmetric()) ? 
      has_symmetric() < other.has_symmetric() : false
    || !(symmetric() == other.symmetric()) ? 
      symmetric() < other.symmetric() : false
    || !(has_moving_min_max_momentum() == other.has_moving_min_max_momentum()) ? 
      has_moving_min_max_momentum() < other.has_moving_min_max_momentum() : false
    || !(moving_min_max_momentum() == other.moving_min_max_momentum()) ? 
      moving_min_max_momentum() < other.moving_min_max_momentum() : false
    || !(has_moving_min_max_stop_update_after_iters() == other.has_moving_min_max_stop_update_after_iters()) ? 
      has_moving_min_max_stop_update_after_iters() < other.has_moving_min_max_stop_update_after_iters() : false
    || !(moving_min_max_stop_update_after_iters() == other.moving_min_max_stop_update_after_iters()) ? 
      moving_min_max_stop_update_after_iters() < other.moving_min_max_stop_update_after_iters() : false
    || !(has_target_backend() == other.has_target_backend()) ? 
      has_target_backend() < other.has_target_backend() : false
    || !(target_backend() == other.target_backend()) ? 
      target_backend() < other.target_backend() : false
  ;
}

using _QatConfig_ =  ConstQatConfig::_QatConfig_;
ConstQatConfig::ConstQatConfig(const ::std::shared_ptr<_QatConfig_>& data): data_(data) {}
ConstQatConfig::ConstQatConfig(): data_(::std::make_shared<_QatConfig_>()) {}
ConstQatConfig::ConstQatConfig(const ::oneflow::QatConfig& proto_qatconfig) {
  BuildFromProto(proto_qatconfig);
}
ConstQatConfig::ConstQatConfig(const ConstQatConfig&) = default;
ConstQatConfig::ConstQatConfig(ConstQatConfig&&) noexcept = default;
ConstQatConfig::~ConstQatConfig() = default;

void ConstQatConfig::ToProto(PbMessage* proto_qatconfig) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::QatConfig*>(proto_qatconfig));
}
  
::std::string ConstQatConfig::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstQatConfig::__Empty__() const {
  return !data_;
}

int ConstQatConfig::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"per_channel_weight_quantization", 1},
    {"symmetric", 2},
    {"moving_min_max_momentum", 3},
    {"moving_min_max_stop_update_after_iters", 4},
    {"target_backend", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstQatConfig::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstQatConfig::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstQatConfig::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &per_channel_weight_quantization();
    case 2: return &symmetric();
    case 3: return &moving_min_max_momentum();
    case 4: return &moving_min_max_stop_update_after_iters();
    case 5: return &target_backend();
    default: return nullptr;
  }
}

// required or optional field per_channel_weight_quantization
bool ConstQatConfig::has_per_channel_weight_quantization() const {
  return __SharedPtrOrDefault__()->has_per_channel_weight_quantization();
}
const bool& ConstQatConfig::per_channel_weight_quantization() const {
  return __SharedPtrOrDefault__()->per_channel_weight_quantization();
}
// used by pybind11 only
// required or optional field symmetric
bool ConstQatConfig::has_symmetric() const {
  return __SharedPtrOrDefault__()->has_symmetric();
}
const bool& ConstQatConfig::symmetric() const {
  return __SharedPtrOrDefault__()->symmetric();
}
// used by pybind11 only
// required or optional field moving_min_max_momentum
bool ConstQatConfig::has_moving_min_max_momentum() const {
  return __SharedPtrOrDefault__()->has_moving_min_max_momentum();
}
const float& ConstQatConfig::moving_min_max_momentum() const {
  return __SharedPtrOrDefault__()->moving_min_max_momentum();
}
// used by pybind11 only
// required or optional field moving_min_max_stop_update_after_iters
bool ConstQatConfig::has_moving_min_max_stop_update_after_iters() const {
  return __SharedPtrOrDefault__()->has_moving_min_max_stop_update_after_iters();
}
const int64_t& ConstQatConfig::moving_min_max_stop_update_after_iters() const {
  return __SharedPtrOrDefault__()->moving_min_max_stop_update_after_iters();
}
// used by pybind11 only
// required or optional field target_backend
bool ConstQatConfig::has_target_backend() const {
  return __SharedPtrOrDefault__()->has_target_backend();
}
const ::std::string& ConstQatConfig::target_backend() const {
  return __SharedPtrOrDefault__()->target_backend();
}
// used by pybind11 only

::std::shared_ptr<ConstQatConfig> ConstQatConfig::__SharedConst__() const {
  return ::std::make_shared<ConstQatConfig>(data_);
}
int64_t ConstQatConfig::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstQatConfig::operator==(const ConstQatConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstQatConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstQatConfig::operator<(const ConstQatConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_QatConfig_>& ConstQatConfig::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_QatConfig_> default_ptr = std::make_shared<_QatConfig_>();
  return default_ptr;
}
const ::std::shared_ptr<_QatConfig_>& ConstQatConfig::__SharedPtr__() {
  if (!data_) { data_.reset(new _QatConfig_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstQatConfig
void ConstQatConfig::BuildFromProto(const PbMessage& proto_qatconfig) {
  data_ = ::std::make_shared<_QatConfig_>(dynamic_cast<const ::oneflow::QatConfig&>(proto_qatconfig));
}

QatConfig::QatConfig(const ::std::shared_ptr<_QatConfig_>& data)
  : ConstQatConfig(data) {}
QatConfig::QatConfig(const QatConfig& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<QatConfig> resize
QatConfig::QatConfig(QatConfig&&) noexcept = default; 
QatConfig::QatConfig(const ::oneflow::QatConfig& proto_qatconfig) {
  InitFromProto(proto_qatconfig);
}
QatConfig::QatConfig() = default;

QatConfig::~QatConfig() = default;

void QatConfig::InitFromProto(const PbMessage& proto_qatconfig) {
  BuildFromProto(proto_qatconfig);
}
  
void* QatConfig::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_per_channel_weight_quantization();
    case 2: return mutable_symmetric();
    case 3: return mutable_moving_min_max_momentum();
    case 4: return mutable_moving_min_max_stop_update_after_iters();
    case 5: return mutable_target_backend();
    default: return nullptr;
  }
}

bool QatConfig::operator==(const QatConfig& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t QatConfig::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool QatConfig::operator<(const QatConfig& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void QatConfig::Clear() {
  if (data_) { data_.reset(); }
}
void QatConfig::CopyFrom(const QatConfig& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
QatConfig& QatConfig::operator=(const QatConfig& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field per_channel_weight_quantization
void QatConfig::clear_per_channel_weight_quantization() {
  return __SharedPtr__()->clear_per_channel_weight_quantization();
}
void QatConfig::set_per_channel_weight_quantization(const bool& value) {
  return __SharedPtr__()->set_per_channel_weight_quantization(value);
}
bool* QatConfig::mutable_per_channel_weight_quantization() {
  return  __SharedPtr__()->mutable_per_channel_weight_quantization();
}
// required or optional field symmetric
void QatConfig::clear_symmetric() {
  return __SharedPtr__()->clear_symmetric();
}
void QatConfig::set_symmetric(const bool& value) {
  return __SharedPtr__()->set_symmetric(value);
}
bool* QatConfig::mutable_symmetric() {
  return  __SharedPtr__()->mutable_symmetric();
}
// required or optional field moving_min_max_momentum
void QatConfig::clear_moving_min_max_momentum() {
  return __SharedPtr__()->clear_moving_min_max_momentum();
}
void QatConfig::set_moving_min_max_momentum(const float& value) {
  return __SharedPtr__()->set_moving_min_max_momentum(value);
}
float* QatConfig::mutable_moving_min_max_momentum() {
  return  __SharedPtr__()->mutable_moving_min_max_momentum();
}
// required or optional field moving_min_max_stop_update_after_iters
void QatConfig::clear_moving_min_max_stop_update_after_iters() {
  return __SharedPtr__()->clear_moving_min_max_stop_update_after_iters();
}
void QatConfig::set_moving_min_max_stop_update_after_iters(const int64_t& value) {
  return __SharedPtr__()->set_moving_min_max_stop_update_after_iters(value);
}
int64_t* QatConfig::mutable_moving_min_max_stop_update_after_iters() {
  return  __SharedPtr__()->mutable_moving_min_max_stop_update_after_iters();
}
// required or optional field target_backend
void QatConfig::clear_target_backend() {
  return __SharedPtr__()->clear_target_backend();
}
void QatConfig::set_target_backend(const ::std::string& value) {
  return __SharedPtr__()->set_target_backend(value);
}
::std::string* QatConfig::mutable_target_backend() {
  return  __SharedPtr__()->mutable_target_backend();
}

::std::shared_ptr<QatConfig> QatConfig::__SharedMutable__() {
  return ::std::make_shared<QatConfig>(__SharedPtr__());
}
ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::_IndexedSlicesOptimizerConf_() { Clear(); }
ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::_IndexedSlicesOptimizerConf_(const _IndexedSlicesOptimizerConf_& other) { CopyFrom(other); }
ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::_IndexedSlicesOptimizerConf_(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf) {
  InitFromProto(proto_indexedslicesoptimizerconf);
}
ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::_IndexedSlicesOptimizerConf_(_IndexedSlicesOptimizerConf_&& other) = default;
ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::~_IndexedSlicesOptimizerConf_() = default;

void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::InitFromProto(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf) {
  Clear();
  // required_or_optional field: enable
  if (proto_indexedslicesoptimizerconf.has_enable()) {
    set_enable(proto_indexedslicesoptimizerconf.enable());
  }
  // required_or_optional field: include_op_names
  if (proto_indexedslicesoptimizerconf.has_include_op_names()) {
  *mutable_include_op_names() = ::oneflow::cfg::OpNameSet(proto_indexedslicesoptimizerconf.include_op_names());      
  }
    
}

void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::ToProto(::oneflow::IndexedSlicesOptimizerConf* proto_indexedslicesoptimizerconf) const {
  proto_indexedslicesoptimizerconf->Clear();
  // required_or_optional field: enable
  if (this->has_enable()) {
    proto_indexedslicesoptimizerconf->set_enable(enable());
    }
  // required_or_optional field: include_op_names
  if (this->has_include_op_names()) {
    ::oneflow::OpNameSet proto_include_op_names;
    include_op_names().ToProto(&proto_include_op_names);
    proto_indexedslicesoptimizerconf->mutable_include_op_names()->CopyFrom(proto_include_op_names);
    }

}

::std::string ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::DebugString() const {
  ::oneflow::IndexedSlicesOptimizerConf proto_indexedslicesoptimizerconf;
  this->ToProto(&proto_indexedslicesoptimizerconf);
  return proto_indexedslicesoptimizerconf.DebugString();
}

void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::Clear() {
  clear_enable();
  clear_include_op_names();
}

void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::CopyFrom(const _IndexedSlicesOptimizerConf_& other) {
  if (other.has_enable()) {
    set_enable(other.enable());
  } else {
    clear_enable();
  }
  if (other.has_include_op_names()) {
    mutable_include_op_names()->CopyFrom(other.include_op_names());
  } else {
    clear_include_op_names();
  }
}


// optional field enable
bool ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::has_enable() const {
  return has_enable_;
}
const bool& ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::enable() const {
  if (has_enable_) { return enable_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::clear_enable() {
  has_enable_ = false;
}
void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::set_enable(const bool& value) {
  enable_ = value;
  has_enable_ = true;
}
bool* ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::mutable_enable() {
  has_enable_ = true;
  return &enable_;
}

// optional field include_op_names
bool ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::has_include_op_names() const {
  return has_include_op_names_;
}
const ::oneflow::cfg::OpNameSet& ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::include_op_names() const {
  if (!include_op_names_) {
    static const ::std::shared_ptr<::oneflow::cfg::OpNameSet> default_static_value =
      ::std::make_shared<::oneflow::cfg::OpNameSet>();
    return *default_static_value;
  }
  return *(include_op_names_.get());
}
void ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::clear_include_op_names() {
  if (include_op_names_) {
    include_op_names_->Clear();
  }
  has_include_op_names_ = false;
}
::oneflow::cfg::OpNameSet* ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::mutable_include_op_names() {
  if (!include_op_names_) {
    include_op_names_ = ::std::make_shared<::oneflow::cfg::OpNameSet>();
  }
  has_include_op_names_ = true;
  return include_op_names_.get();
}


int ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::compare(const _IndexedSlicesOptimizerConf_& other) {
  if (!(has_enable() == other.has_enable())) {
    return has_enable() < other.has_enable() ? -1 : 1;
  } else if (!(enable() == other.enable())) {
    return enable() < other.enable() ? -1 : 1;
  }
  if (!(has_include_op_names() == other.has_include_op_names())) {
    return has_include_op_names() < other.has_include_op_names() ? -1 : 1;
  } else if (!(include_op_names() == other.include_op_names())) {
    return include_op_names() < other.include_op_names() ? -1 : 1;
  }
  return 0;
}

bool ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::operator==(const _IndexedSlicesOptimizerConf_& other) const {
  return true
    && has_enable() == other.has_enable() 
    && enable() == other.enable()
    && has_include_op_names() == other.has_include_op_names() 
    && include_op_names() == other.include_op_names()
  ;
}

std::size_t ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::__CalcHash__() const {
  return 0
    ^ (has_enable() ? std::hash<bool>()(enable()) : 0)
    ^ (has_include_op_names() ? std::hash<::oneflow::cfg::OpNameSet>()(include_op_names()) : 0)
  ;
}

bool ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_::operator<(const _IndexedSlicesOptimizerConf_& other) const {
  return false
    || !(has_enable() == other.has_enable()) ? 
      has_enable() < other.has_enable() : false
    || !(enable() == other.enable()) ? 
      enable() < other.enable() : false
    || !(has_include_op_names() == other.has_include_op_names()) ? 
      has_include_op_names() < other.has_include_op_names() : false
    || !(include_op_names() == other.include_op_names()) ? 
      include_op_names() < other.include_op_names() : false
  ;
}

using _IndexedSlicesOptimizerConf_ =  ConstIndexedSlicesOptimizerConf::_IndexedSlicesOptimizerConf_;
ConstIndexedSlicesOptimizerConf::ConstIndexedSlicesOptimizerConf(const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& data): data_(data) {}
ConstIndexedSlicesOptimizerConf::ConstIndexedSlicesOptimizerConf(): data_(::std::make_shared<_IndexedSlicesOptimizerConf_>()) {}
ConstIndexedSlicesOptimizerConf::ConstIndexedSlicesOptimizerConf(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf) {
  BuildFromProto(proto_indexedslicesoptimizerconf);
}
ConstIndexedSlicesOptimizerConf::ConstIndexedSlicesOptimizerConf(const ConstIndexedSlicesOptimizerConf&) = default;
ConstIndexedSlicesOptimizerConf::ConstIndexedSlicesOptimizerConf(ConstIndexedSlicesOptimizerConf&&) noexcept = default;
ConstIndexedSlicesOptimizerConf::~ConstIndexedSlicesOptimizerConf() = default;

void ConstIndexedSlicesOptimizerConf::ToProto(PbMessage* proto_indexedslicesoptimizerconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::IndexedSlicesOptimizerConf*>(proto_indexedslicesoptimizerconf));
}
  
::std::string ConstIndexedSlicesOptimizerConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstIndexedSlicesOptimizerConf::__Empty__() const {
  return !data_;
}

int ConstIndexedSlicesOptimizerConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"enable", 1},
    {"include_op_names", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstIndexedSlicesOptimizerConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstIndexedSlicesOptimizerConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpNameSet),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpNameSet),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstIndexedSlicesOptimizerConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &enable();
    case 2: return &include_op_names();
    default: return nullptr;
  }
}

// required or optional field enable
bool ConstIndexedSlicesOptimizerConf::has_enable() const {
  return __SharedPtrOrDefault__()->has_enable();
}
const bool& ConstIndexedSlicesOptimizerConf::enable() const {
  return __SharedPtrOrDefault__()->enable();
}
// used by pybind11 only
// required or optional field include_op_names
bool ConstIndexedSlicesOptimizerConf::has_include_op_names() const {
  return __SharedPtrOrDefault__()->has_include_op_names();
}
const ::oneflow::cfg::OpNameSet& ConstIndexedSlicesOptimizerConf::include_op_names() const {
  return __SharedPtrOrDefault__()->include_op_names();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpNameSet> ConstIndexedSlicesOptimizerConf::shared_const_include_op_names() const {
  return include_op_names().__SharedConst__();
}

::std::shared_ptr<ConstIndexedSlicesOptimizerConf> ConstIndexedSlicesOptimizerConf::__SharedConst__() const {
  return ::std::make_shared<ConstIndexedSlicesOptimizerConf>(data_);
}
int64_t ConstIndexedSlicesOptimizerConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstIndexedSlicesOptimizerConf::operator==(const ConstIndexedSlicesOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstIndexedSlicesOptimizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstIndexedSlicesOptimizerConf::operator<(const ConstIndexedSlicesOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& ConstIndexedSlicesOptimizerConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_IndexedSlicesOptimizerConf_> default_ptr = std::make_shared<_IndexedSlicesOptimizerConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& ConstIndexedSlicesOptimizerConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _IndexedSlicesOptimizerConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstIndexedSlicesOptimizerConf
void ConstIndexedSlicesOptimizerConf::BuildFromProto(const PbMessage& proto_indexedslicesoptimizerconf) {
  data_ = ::std::make_shared<_IndexedSlicesOptimizerConf_>(dynamic_cast<const ::oneflow::IndexedSlicesOptimizerConf&>(proto_indexedslicesoptimizerconf));
}

IndexedSlicesOptimizerConf::IndexedSlicesOptimizerConf(const ::std::shared_ptr<_IndexedSlicesOptimizerConf_>& data)
  : ConstIndexedSlicesOptimizerConf(data) {}
IndexedSlicesOptimizerConf::IndexedSlicesOptimizerConf(const IndexedSlicesOptimizerConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<IndexedSlicesOptimizerConf> resize
IndexedSlicesOptimizerConf::IndexedSlicesOptimizerConf(IndexedSlicesOptimizerConf&&) noexcept = default; 
IndexedSlicesOptimizerConf::IndexedSlicesOptimizerConf(const ::oneflow::IndexedSlicesOptimizerConf& proto_indexedslicesoptimizerconf) {
  InitFromProto(proto_indexedslicesoptimizerconf);
}
IndexedSlicesOptimizerConf::IndexedSlicesOptimizerConf() = default;

IndexedSlicesOptimizerConf::~IndexedSlicesOptimizerConf() = default;

void IndexedSlicesOptimizerConf::InitFromProto(const PbMessage& proto_indexedslicesoptimizerconf) {
  BuildFromProto(proto_indexedslicesoptimizerconf);
}
  
void* IndexedSlicesOptimizerConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_enable();
    case 2: return mutable_include_op_names();
    default: return nullptr;
  }
}

bool IndexedSlicesOptimizerConf::operator==(const IndexedSlicesOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t IndexedSlicesOptimizerConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool IndexedSlicesOptimizerConf::operator<(const IndexedSlicesOptimizerConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void IndexedSlicesOptimizerConf::Clear() {
  if (data_) { data_.reset(); }
}
void IndexedSlicesOptimizerConf::CopyFrom(const IndexedSlicesOptimizerConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
IndexedSlicesOptimizerConf& IndexedSlicesOptimizerConf::operator=(const IndexedSlicesOptimizerConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field enable
void IndexedSlicesOptimizerConf::clear_enable() {
  return __SharedPtr__()->clear_enable();
}
void IndexedSlicesOptimizerConf::set_enable(const bool& value) {
  return __SharedPtr__()->set_enable(value);
}
bool* IndexedSlicesOptimizerConf::mutable_enable() {
  return  __SharedPtr__()->mutable_enable();
}
// required or optional field include_op_names
void IndexedSlicesOptimizerConf::clear_include_op_names() {
  return __SharedPtr__()->clear_include_op_names();
}
::oneflow::cfg::OpNameSet* IndexedSlicesOptimizerConf::mutable_include_op_names() {
  return __SharedPtr__()->mutable_include_op_names();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpNameSet> IndexedSlicesOptimizerConf::shared_mutable_include_op_names() {
  return mutable_include_op_names()->__SharedMutable__();
}

::std::shared_ptr<IndexedSlicesOptimizerConf> IndexedSlicesOptimizerConf::__SharedMutable__() {
  return ::std::make_shared<IndexedSlicesOptimizerConf>(__SharedPtr__());
}
ConstParallelBlobConf::_ParallelBlobConf_::_ParallelBlobConf_() { Clear(); }
ConstParallelBlobConf::_ParallelBlobConf_::_ParallelBlobConf_(const _ParallelBlobConf_& other) { CopyFrom(other); }
ConstParallelBlobConf::_ParallelBlobConf_::_ParallelBlobConf_(const ::oneflow::ParallelBlobConf& proto_parallelblobconf) {
  InitFromProto(proto_parallelblobconf);
}
ConstParallelBlobConf::_ParallelBlobConf_::_ParallelBlobConf_(_ParallelBlobConf_&& other) = default;
ConstParallelBlobConf::_ParallelBlobConf_::~_ParallelBlobConf_() = default;

void ConstParallelBlobConf::_ParallelBlobConf_::InitFromProto(const ::oneflow::ParallelBlobConf& proto_parallelblobconf) {
  Clear();
  // required_or_optional field: logical_blob_desc_conf
  if (proto_parallelblobconf.has_logical_blob_desc_conf()) {
  *mutable_logical_blob_desc_conf() = ::oneflow::cfg::BlobDescProto(proto_parallelblobconf.logical_blob_desc_conf());      
  }
  // required_or_optional field: parallel_conf
  if (proto_parallelblobconf.has_parallel_conf()) {
  *mutable_parallel_conf() = ::oneflow::cfg::ParallelConf(proto_parallelblobconf.parallel_conf());      
  }
  // required_or_optional field: parallel_distribution
  if (proto_parallelblobconf.has_parallel_distribution()) {
  *mutable_parallel_distribution() = ::oneflow::cfg::ParallelDistribution(proto_parallelblobconf.parallel_distribution());      
  }
    
}

void ConstParallelBlobConf::_ParallelBlobConf_::ToProto(::oneflow::ParallelBlobConf* proto_parallelblobconf) const {
  proto_parallelblobconf->Clear();
  // required_or_optional field: logical_blob_desc_conf
  if (this->has_logical_blob_desc_conf()) {
    ::oneflow::BlobDescProto proto_logical_blob_desc_conf;
    logical_blob_desc_conf().ToProto(&proto_logical_blob_desc_conf);
    proto_parallelblobconf->mutable_logical_blob_desc_conf()->CopyFrom(proto_logical_blob_desc_conf);
    }
  // required_or_optional field: parallel_conf
  if (this->has_parallel_conf()) {
    ::oneflow::ParallelConf proto_parallel_conf;
    parallel_conf().ToProto(&proto_parallel_conf);
    proto_parallelblobconf->mutable_parallel_conf()->CopyFrom(proto_parallel_conf);
    }
  // required_or_optional field: parallel_distribution
  if (this->has_parallel_distribution()) {
    ::oneflow::ParallelDistribution proto_parallel_distribution;
    parallel_distribution().ToProto(&proto_parallel_distribution);
    proto_parallelblobconf->mutable_parallel_distribution()->CopyFrom(proto_parallel_distribution);
    }

}

::std::string ConstParallelBlobConf::_ParallelBlobConf_::DebugString() const {
  ::oneflow::ParallelBlobConf proto_parallelblobconf;
  this->ToProto(&proto_parallelblobconf);
  return proto_parallelblobconf.DebugString();
}

void ConstParallelBlobConf::_ParallelBlobConf_::Clear() {
  clear_logical_blob_desc_conf();
  clear_parallel_conf();
  clear_parallel_distribution();
}

void ConstParallelBlobConf::_ParallelBlobConf_::CopyFrom(const _ParallelBlobConf_& other) {
  if (other.has_logical_blob_desc_conf()) {
    mutable_logical_blob_desc_conf()->CopyFrom(other.logical_blob_desc_conf());
  } else {
    clear_logical_blob_desc_conf();
  }
  if (other.has_parallel_conf()) {
    mutable_parallel_conf()->CopyFrom(other.parallel_conf());
  } else {
    clear_parallel_conf();
  }
  if (other.has_parallel_distribution()) {
    mutable_parallel_distribution()->CopyFrom(other.parallel_distribution());
  } else {
    clear_parallel_distribution();
  }
}


// optional field logical_blob_desc_conf
bool ConstParallelBlobConf::_ParallelBlobConf_::has_logical_blob_desc_conf() const {
  return has_logical_blob_desc_conf_;
}
const ::oneflow::cfg::BlobDescProto& ConstParallelBlobConf::_ParallelBlobConf_::logical_blob_desc_conf() const {
  if (!logical_blob_desc_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::BlobDescProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::BlobDescProto>();
    return *default_static_value;
  }
  return *(logical_blob_desc_conf_.get());
}
void ConstParallelBlobConf::_ParallelBlobConf_::clear_logical_blob_desc_conf() {
  if (logical_blob_desc_conf_) {
    logical_blob_desc_conf_->Clear();
  }
  has_logical_blob_desc_conf_ = false;
}
::oneflow::cfg::BlobDescProto* ConstParallelBlobConf::_ParallelBlobConf_::mutable_logical_blob_desc_conf() {
  if (!logical_blob_desc_conf_) {
    logical_blob_desc_conf_ = ::std::make_shared<::oneflow::cfg::BlobDescProto>();
  }
  has_logical_blob_desc_conf_ = true;
  return logical_blob_desc_conf_.get();
}

// optional field parallel_conf
bool ConstParallelBlobConf::_ParallelBlobConf_::has_parallel_conf() const {
  return has_parallel_conf_;
}
const ::oneflow::cfg::ParallelConf& ConstParallelBlobConf::_ParallelBlobConf_::parallel_conf() const {
  if (!parallel_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelConf>();
    return *default_static_value;
  }
  return *(parallel_conf_.get());
}
void ConstParallelBlobConf::_ParallelBlobConf_::clear_parallel_conf() {
  if (parallel_conf_) {
    parallel_conf_->Clear();
  }
  has_parallel_conf_ = false;
}
::oneflow::cfg::ParallelConf* ConstParallelBlobConf::_ParallelBlobConf_::mutable_parallel_conf() {
  if (!parallel_conf_) {
    parallel_conf_ = ::std::make_shared<::oneflow::cfg::ParallelConf>();
  }
  has_parallel_conf_ = true;
  return parallel_conf_.get();
}

// optional field parallel_distribution
bool ConstParallelBlobConf::_ParallelBlobConf_::has_parallel_distribution() const {
  return has_parallel_distribution_;
}
const ::oneflow::cfg::ParallelDistribution& ConstParallelBlobConf::_ParallelBlobConf_::parallel_distribution() const {
  if (!parallel_distribution_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelDistribution>();
    return *default_static_value;
  }
  return *(parallel_distribution_.get());
}
void ConstParallelBlobConf::_ParallelBlobConf_::clear_parallel_distribution() {
  if (parallel_distribution_) {
    parallel_distribution_->Clear();
  }
  has_parallel_distribution_ = false;
}
::oneflow::cfg::ParallelDistribution* ConstParallelBlobConf::_ParallelBlobConf_::mutable_parallel_distribution() {
  if (!parallel_distribution_) {
    parallel_distribution_ = ::std::make_shared<::oneflow::cfg::ParallelDistribution>();
  }
  has_parallel_distribution_ = true;
  return parallel_distribution_.get();
}


int ConstParallelBlobConf::_ParallelBlobConf_::compare(const _ParallelBlobConf_& other) {
  if (!(has_logical_blob_desc_conf() == other.has_logical_blob_desc_conf())) {
    return has_logical_blob_desc_conf() < other.has_logical_blob_desc_conf() ? -1 : 1;
  } else if (!(logical_blob_desc_conf() == other.logical_blob_desc_conf())) {
    return logical_blob_desc_conf() < other.logical_blob_desc_conf() ? -1 : 1;
  }
  if (!(has_parallel_conf() == other.has_parallel_conf())) {
    return has_parallel_conf() < other.has_parallel_conf() ? -1 : 1;
  } else if (!(parallel_conf() == other.parallel_conf())) {
    return parallel_conf() < other.parallel_conf() ? -1 : 1;
  }
  if (!(has_parallel_distribution() == other.has_parallel_distribution())) {
    return has_parallel_distribution() < other.has_parallel_distribution() ? -1 : 1;
  } else if (!(parallel_distribution() == other.parallel_distribution())) {
    return parallel_distribution() < other.parallel_distribution() ? -1 : 1;
  }
  return 0;
}

bool ConstParallelBlobConf::_ParallelBlobConf_::operator==(const _ParallelBlobConf_& other) const {
  return true
    && has_logical_blob_desc_conf() == other.has_logical_blob_desc_conf() 
    && logical_blob_desc_conf() == other.logical_blob_desc_conf()
    && has_parallel_conf() == other.has_parallel_conf() 
    && parallel_conf() == other.parallel_conf()
    && has_parallel_distribution() == other.has_parallel_distribution() 
    && parallel_distribution() == other.parallel_distribution()
  ;
}

std::size_t ConstParallelBlobConf::_ParallelBlobConf_::__CalcHash__() const {
  return 0
    ^ (has_logical_blob_desc_conf() ? std::hash<::oneflow::cfg::BlobDescProto>()(logical_blob_desc_conf()) : 0)
    ^ (has_parallel_conf() ? std::hash<::oneflow::cfg::ParallelConf>()(parallel_conf()) : 0)
    ^ (has_parallel_distribution() ? std::hash<::oneflow::cfg::ParallelDistribution>()(parallel_distribution()) : 0)
  ;
}

bool ConstParallelBlobConf::_ParallelBlobConf_::operator<(const _ParallelBlobConf_& other) const {
  return false
    || !(has_logical_blob_desc_conf() == other.has_logical_blob_desc_conf()) ? 
      has_logical_blob_desc_conf() < other.has_logical_blob_desc_conf() : false
    || !(logical_blob_desc_conf() == other.logical_blob_desc_conf()) ? 
      logical_blob_desc_conf() < other.logical_blob_desc_conf() : false
    || !(has_parallel_conf() == other.has_parallel_conf()) ? 
      has_parallel_conf() < other.has_parallel_conf() : false
    || !(parallel_conf() == other.parallel_conf()) ? 
      parallel_conf() < other.parallel_conf() : false
    || !(has_parallel_distribution() == other.has_parallel_distribution()) ? 
      has_parallel_distribution() < other.has_parallel_distribution() : false
    || !(parallel_distribution() == other.parallel_distribution()) ? 
      parallel_distribution() < other.parallel_distribution() : false
  ;
}

using _ParallelBlobConf_ =  ConstParallelBlobConf::_ParallelBlobConf_;
ConstParallelBlobConf::ConstParallelBlobConf(const ::std::shared_ptr<_ParallelBlobConf_>& data): data_(data) {}
ConstParallelBlobConf::ConstParallelBlobConf(): data_(::std::make_shared<_ParallelBlobConf_>()) {}
ConstParallelBlobConf::ConstParallelBlobConf(const ::oneflow::ParallelBlobConf& proto_parallelblobconf) {
  BuildFromProto(proto_parallelblobconf);
}
ConstParallelBlobConf::ConstParallelBlobConf(const ConstParallelBlobConf&) = default;
ConstParallelBlobConf::ConstParallelBlobConf(ConstParallelBlobConf&&) noexcept = default;
ConstParallelBlobConf::~ConstParallelBlobConf() = default;

void ConstParallelBlobConf::ToProto(PbMessage* proto_parallelblobconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ParallelBlobConf*>(proto_parallelblobconf));
}
  
::std::string ConstParallelBlobConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstParallelBlobConf::__Empty__() const {
  return !data_;
}

int ConstParallelBlobConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"logical_blob_desc_conf", 1},
    {"parallel_conf", 2},
    {"parallel_distribution", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstParallelBlobConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstParallelBlobConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobDescProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobDescProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConf),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelDistribution),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelDistribution),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstParallelBlobConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &logical_blob_desc_conf();
    case 2: return &parallel_conf();
    case 3: return &parallel_distribution();
    default: return nullptr;
  }
}

// required or optional field logical_blob_desc_conf
bool ConstParallelBlobConf::has_logical_blob_desc_conf() const {
  return __SharedPtrOrDefault__()->has_logical_blob_desc_conf();
}
const ::oneflow::cfg::BlobDescProto& ConstParallelBlobConf::logical_blob_desc_conf() const {
  return __SharedPtrOrDefault__()->logical_blob_desc_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobDescProto> ConstParallelBlobConf::shared_const_logical_blob_desc_conf() const {
  return logical_blob_desc_conf().__SharedConst__();
}
// required or optional field parallel_conf
bool ConstParallelBlobConf::has_parallel_conf() const {
  return __SharedPtrOrDefault__()->has_parallel_conf();
}
const ::oneflow::cfg::ParallelConf& ConstParallelBlobConf::parallel_conf() const {
  return __SharedPtrOrDefault__()->parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConf> ConstParallelBlobConf::shared_const_parallel_conf() const {
  return parallel_conf().__SharedConst__();
}
// required or optional field parallel_distribution
bool ConstParallelBlobConf::has_parallel_distribution() const {
  return __SharedPtrOrDefault__()->has_parallel_distribution();
}
const ::oneflow::cfg::ParallelDistribution& ConstParallelBlobConf::parallel_distribution() const {
  return __SharedPtrOrDefault__()->parallel_distribution();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelDistribution> ConstParallelBlobConf::shared_const_parallel_distribution() const {
  return parallel_distribution().__SharedConst__();
}

::std::shared_ptr<ConstParallelBlobConf> ConstParallelBlobConf::__SharedConst__() const {
  return ::std::make_shared<ConstParallelBlobConf>(data_);
}
int64_t ConstParallelBlobConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstParallelBlobConf::operator==(const ConstParallelBlobConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstParallelBlobConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstParallelBlobConf::operator<(const ConstParallelBlobConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ParallelBlobConf_>& ConstParallelBlobConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ParallelBlobConf_> default_ptr = std::make_shared<_ParallelBlobConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_ParallelBlobConf_>& ConstParallelBlobConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _ParallelBlobConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstParallelBlobConf
void ConstParallelBlobConf::BuildFromProto(const PbMessage& proto_parallelblobconf) {
  data_ = ::std::make_shared<_ParallelBlobConf_>(dynamic_cast<const ::oneflow::ParallelBlobConf&>(proto_parallelblobconf));
}

ParallelBlobConf::ParallelBlobConf(const ::std::shared_ptr<_ParallelBlobConf_>& data)
  : ConstParallelBlobConf(data) {}
ParallelBlobConf::ParallelBlobConf(const ParallelBlobConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ParallelBlobConf> resize
ParallelBlobConf::ParallelBlobConf(ParallelBlobConf&&) noexcept = default; 
ParallelBlobConf::ParallelBlobConf(const ::oneflow::ParallelBlobConf& proto_parallelblobconf) {
  InitFromProto(proto_parallelblobconf);
}
ParallelBlobConf::ParallelBlobConf() = default;

ParallelBlobConf::~ParallelBlobConf() = default;

void ParallelBlobConf::InitFromProto(const PbMessage& proto_parallelblobconf) {
  BuildFromProto(proto_parallelblobconf);
}
  
void* ParallelBlobConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_logical_blob_desc_conf();
    case 2: return mutable_parallel_conf();
    case 3: return mutable_parallel_distribution();
    default: return nullptr;
  }
}

bool ParallelBlobConf::operator==(const ParallelBlobConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ParallelBlobConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ParallelBlobConf::operator<(const ParallelBlobConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ParallelBlobConf::Clear() {
  if (data_) { data_.reset(); }
}
void ParallelBlobConf::CopyFrom(const ParallelBlobConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ParallelBlobConf& ParallelBlobConf::operator=(const ParallelBlobConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field logical_blob_desc_conf
void ParallelBlobConf::clear_logical_blob_desc_conf() {
  return __SharedPtr__()->clear_logical_blob_desc_conf();
}
::oneflow::cfg::BlobDescProto* ParallelBlobConf::mutable_logical_blob_desc_conf() {
  return __SharedPtr__()->mutable_logical_blob_desc_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobDescProto> ParallelBlobConf::shared_mutable_logical_blob_desc_conf() {
  return mutable_logical_blob_desc_conf()->__SharedMutable__();
}
// required or optional field parallel_conf
void ParallelBlobConf::clear_parallel_conf() {
  return __SharedPtr__()->clear_parallel_conf();
}
::oneflow::cfg::ParallelConf* ParallelBlobConf::mutable_parallel_conf() {
  return __SharedPtr__()->mutable_parallel_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConf> ParallelBlobConf::shared_mutable_parallel_conf() {
  return mutable_parallel_conf()->__SharedMutable__();
}
// required or optional field parallel_distribution
void ParallelBlobConf::clear_parallel_distribution() {
  return __SharedPtr__()->clear_parallel_distribution();
}
::oneflow::cfg::ParallelDistribution* ParallelBlobConf::mutable_parallel_distribution() {
  return __SharedPtr__()->mutable_parallel_distribution();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelDistribution> ParallelBlobConf::shared_mutable_parallel_distribution() {
  return mutable_parallel_distribution()->__SharedMutable__();
}

::std::shared_ptr<ParallelBlobConf> ParallelBlobConf::__SharedMutable__() {
  return ::std::make_shared<ParallelBlobConf>(__SharedPtr__());
}
ConstJobInputDef::_JobInputDef_::_JobInputDef_() { Clear(); }
ConstJobInputDef::_JobInputDef_::_JobInputDef_(const _JobInputDef_& other) { CopyFrom(other); }
ConstJobInputDef::_JobInputDef_::_JobInputDef_(const ::oneflow::JobInputDef& proto_jobinputdef) {
  InitFromProto(proto_jobinputdef);
}
ConstJobInputDef::_JobInputDef_::_JobInputDef_(_JobInputDef_&& other) = default;
ConstJobInputDef::_JobInputDef_::~_JobInputDef_() = default;

void ConstJobInputDef::_JobInputDef_::InitFromProto(const ::oneflow::JobInputDef& proto_jobinputdef) {
  Clear();
  // required_or_optional field: lbi
  if (proto_jobinputdef.has_lbi()) {
  *mutable_lbi() = ::oneflow::cfg::LogicalBlobId(proto_jobinputdef.lbi());      
  }
  // required_or_optional field: blob_conf
  if (proto_jobinputdef.has_blob_conf()) {
  *mutable_blob_conf() = ::oneflow::cfg::InterfaceBlobConf(proto_jobinputdef.blob_conf());      
  }
    
}

void ConstJobInputDef::_JobInputDef_::ToProto(::oneflow::JobInputDef* proto_jobinputdef) const {
  proto_jobinputdef->Clear();
  // required_or_optional field: lbi
  if (this->has_lbi()) {
    ::oneflow::LogicalBlobId proto_lbi;
    lbi().ToProto(&proto_lbi);
    proto_jobinputdef->mutable_lbi()->CopyFrom(proto_lbi);
    }
  // required_or_optional field: blob_conf
  if (this->has_blob_conf()) {
    ::oneflow::InterfaceBlobConf proto_blob_conf;
    blob_conf().ToProto(&proto_blob_conf);
    proto_jobinputdef->mutable_blob_conf()->CopyFrom(proto_blob_conf);
    }

}

::std::string ConstJobInputDef::_JobInputDef_::DebugString() const {
  ::oneflow::JobInputDef proto_jobinputdef;
  this->ToProto(&proto_jobinputdef);
  return proto_jobinputdef.DebugString();
}

void ConstJobInputDef::_JobInputDef_::Clear() {
  clear_lbi();
  clear_blob_conf();
}

void ConstJobInputDef::_JobInputDef_::CopyFrom(const _JobInputDef_& other) {
  if (other.has_lbi()) {
    mutable_lbi()->CopyFrom(other.lbi());
  } else {
    clear_lbi();
  }
  if (other.has_blob_conf()) {
    mutable_blob_conf()->CopyFrom(other.blob_conf());
  } else {
    clear_blob_conf();
  }
}


// optional field lbi
bool ConstJobInputDef::_JobInputDef_::has_lbi() const {
  return has_lbi_;
}
const ::oneflow::cfg::LogicalBlobId& ConstJobInputDef::_JobInputDef_::lbi() const {
  if (!lbi_) {
    static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> default_static_value =
      ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
    return *default_static_value;
  }
  return *(lbi_.get());
}
void ConstJobInputDef::_JobInputDef_::clear_lbi() {
  if (lbi_) {
    lbi_->Clear();
  }
  has_lbi_ = false;
}
::oneflow::cfg::LogicalBlobId* ConstJobInputDef::_JobInputDef_::mutable_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
  }
  has_lbi_ = true;
  return lbi_.get();
}

// optional field blob_conf
bool ConstJobInputDef::_JobInputDef_::has_blob_conf() const {
  return has_blob_conf_;
}
const ::oneflow::cfg::InterfaceBlobConf& ConstJobInputDef::_JobInputDef_::blob_conf() const {
  if (!blob_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::InterfaceBlobConf>();
    return *default_static_value;
  }
  return *(blob_conf_.get());
}
void ConstJobInputDef::_JobInputDef_::clear_blob_conf() {
  if (blob_conf_) {
    blob_conf_->Clear();
  }
  has_blob_conf_ = false;
}
::oneflow::cfg::InterfaceBlobConf* ConstJobInputDef::_JobInputDef_::mutable_blob_conf() {
  if (!blob_conf_) {
    blob_conf_ = ::std::make_shared<::oneflow::cfg::InterfaceBlobConf>();
  }
  has_blob_conf_ = true;
  return blob_conf_.get();
}


int ConstJobInputDef::_JobInputDef_::compare(const _JobInputDef_& other) {
  if (!(has_lbi() == other.has_lbi())) {
    return has_lbi() < other.has_lbi() ? -1 : 1;
  } else if (!(lbi() == other.lbi())) {
    return lbi() < other.lbi() ? -1 : 1;
  }
  if (!(has_blob_conf() == other.has_blob_conf())) {
    return has_blob_conf() < other.has_blob_conf() ? -1 : 1;
  } else if (!(blob_conf() == other.blob_conf())) {
    return blob_conf() < other.blob_conf() ? -1 : 1;
  }
  return 0;
}

bool ConstJobInputDef::_JobInputDef_::operator==(const _JobInputDef_& other) const {
  return true
    && has_lbi() == other.has_lbi() 
    && lbi() == other.lbi()
    && has_blob_conf() == other.has_blob_conf() 
    && blob_conf() == other.blob_conf()
  ;
}

std::size_t ConstJobInputDef::_JobInputDef_::__CalcHash__() const {
  return 0
    ^ (has_lbi() ? std::hash<::oneflow::cfg::LogicalBlobId>()(lbi()) : 0)
    ^ (has_blob_conf() ? std::hash<::oneflow::cfg::InterfaceBlobConf>()(blob_conf()) : 0)
  ;
}

bool ConstJobInputDef::_JobInputDef_::operator<(const _JobInputDef_& other) const {
  return false
    || !(has_lbi() == other.has_lbi()) ? 
      has_lbi() < other.has_lbi() : false
    || !(lbi() == other.lbi()) ? 
      lbi() < other.lbi() : false
    || !(has_blob_conf() == other.has_blob_conf()) ? 
      has_blob_conf() < other.has_blob_conf() : false
    || !(blob_conf() == other.blob_conf()) ? 
      blob_conf() < other.blob_conf() : false
  ;
}

using _JobInputDef_ =  ConstJobInputDef::_JobInputDef_;
ConstJobInputDef::ConstJobInputDef(const ::std::shared_ptr<_JobInputDef_>& data): data_(data) {}
ConstJobInputDef::ConstJobInputDef(): data_(::std::make_shared<_JobInputDef_>()) {}
ConstJobInputDef::ConstJobInputDef(const ::oneflow::JobInputDef& proto_jobinputdef) {
  BuildFromProto(proto_jobinputdef);
}
ConstJobInputDef::ConstJobInputDef(const ConstJobInputDef&) = default;
ConstJobInputDef::ConstJobInputDef(ConstJobInputDef&&) noexcept = default;
ConstJobInputDef::~ConstJobInputDef() = default;

void ConstJobInputDef::ToProto(PbMessage* proto_jobinputdef) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobInputDef*>(proto_jobinputdef));
}
  
::std::string ConstJobInputDef::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobInputDef::__Empty__() const {
  return !data_;
}

int ConstJobInputDef::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"lbi", 1},
    {"blob_conf", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstJobInputDef::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstJobInputDef::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobId),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::InterfaceBlobConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInterfaceBlobConf),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobInputDef::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &lbi();
    case 2: return &blob_conf();
    default: return nullptr;
  }
}

// required or optional field lbi
bool ConstJobInputDef::has_lbi() const {
  return __SharedPtrOrDefault__()->has_lbi();
}
const ::oneflow::cfg::LogicalBlobId& ConstJobInputDef::lbi() const {
  return __SharedPtrOrDefault__()->lbi();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstJobInputDef::shared_const_lbi() const {
  return lbi().__SharedConst__();
}
// required or optional field blob_conf
bool ConstJobInputDef::has_blob_conf() const {
  return __SharedPtrOrDefault__()->has_blob_conf();
}
const ::oneflow::cfg::InterfaceBlobConf& ConstJobInputDef::blob_conf() const {
  return __SharedPtrOrDefault__()->blob_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInterfaceBlobConf> ConstJobInputDef::shared_const_blob_conf() const {
  return blob_conf().__SharedConst__();
}

::std::shared_ptr<ConstJobInputDef> ConstJobInputDef::__SharedConst__() const {
  return ::std::make_shared<ConstJobInputDef>(data_);
}
int64_t ConstJobInputDef::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobInputDef::operator==(const ConstJobInputDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobInputDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobInputDef::operator<(const ConstJobInputDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobInputDef_>& ConstJobInputDef::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobInputDef_> default_ptr = std::make_shared<_JobInputDef_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobInputDef_>& ConstJobInputDef::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobInputDef_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobInputDef
void ConstJobInputDef::BuildFromProto(const PbMessage& proto_jobinputdef) {
  data_ = ::std::make_shared<_JobInputDef_>(dynamic_cast<const ::oneflow::JobInputDef&>(proto_jobinputdef));
}

JobInputDef::JobInputDef(const ::std::shared_ptr<_JobInputDef_>& data)
  : ConstJobInputDef(data) {}
JobInputDef::JobInputDef(const JobInputDef& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobInputDef> resize
JobInputDef::JobInputDef(JobInputDef&&) noexcept = default; 
JobInputDef::JobInputDef(const ::oneflow::JobInputDef& proto_jobinputdef) {
  InitFromProto(proto_jobinputdef);
}
JobInputDef::JobInputDef() = default;

JobInputDef::~JobInputDef() = default;

void JobInputDef::InitFromProto(const PbMessage& proto_jobinputdef) {
  BuildFromProto(proto_jobinputdef);
}
  
void* JobInputDef::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_lbi();
    case 2: return mutable_blob_conf();
    default: return nullptr;
  }
}

bool JobInputDef::operator==(const JobInputDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobInputDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobInputDef::operator<(const JobInputDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobInputDef::Clear() {
  if (data_) { data_.reset(); }
}
void JobInputDef::CopyFrom(const JobInputDef& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobInputDef& JobInputDef::operator=(const JobInputDef& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field lbi
void JobInputDef::clear_lbi() {
  return __SharedPtr__()->clear_lbi();
}
::oneflow::cfg::LogicalBlobId* JobInputDef::mutable_lbi() {
  return __SharedPtr__()->mutable_lbi();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> JobInputDef::shared_mutable_lbi() {
  return mutable_lbi()->__SharedMutable__();
}
// required or optional field blob_conf
void JobInputDef::clear_blob_conf() {
  return __SharedPtr__()->clear_blob_conf();
}
::oneflow::cfg::InterfaceBlobConf* JobInputDef::mutable_blob_conf() {
  return __SharedPtr__()->mutable_blob_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> JobInputDef::shared_mutable_blob_conf() {
  return mutable_blob_conf()->__SharedMutable__();
}

::std::shared_ptr<JobInputDef> JobInputDef::__SharedMutable__() {
  return ::std::make_shared<JobInputDef>(__SharedPtr__());
}
ConstJobOutputDef::_JobOutputDef_::_JobOutputDef_() { Clear(); }
ConstJobOutputDef::_JobOutputDef_::_JobOutputDef_(const _JobOutputDef_& other) { CopyFrom(other); }
ConstJobOutputDef::_JobOutputDef_::_JobOutputDef_(const ::oneflow::JobOutputDef& proto_joboutputdef) {
  InitFromProto(proto_joboutputdef);
}
ConstJobOutputDef::_JobOutputDef_::_JobOutputDef_(_JobOutputDef_&& other) = default;
ConstJobOutputDef::_JobOutputDef_::~_JobOutputDef_() = default;

void ConstJobOutputDef::_JobOutputDef_::InitFromProto(const ::oneflow::JobOutputDef& proto_joboutputdef) {
  Clear();
  // required_or_optional field: lbi
  if (proto_joboutputdef.has_lbi()) {
  *mutable_lbi() = ::oneflow::cfg::LogicalBlobId(proto_joboutputdef.lbi());      
  }
    
}

void ConstJobOutputDef::_JobOutputDef_::ToProto(::oneflow::JobOutputDef* proto_joboutputdef) const {
  proto_joboutputdef->Clear();
  // required_or_optional field: lbi
  if (this->has_lbi()) {
    ::oneflow::LogicalBlobId proto_lbi;
    lbi().ToProto(&proto_lbi);
    proto_joboutputdef->mutable_lbi()->CopyFrom(proto_lbi);
    }

}

::std::string ConstJobOutputDef::_JobOutputDef_::DebugString() const {
  ::oneflow::JobOutputDef proto_joboutputdef;
  this->ToProto(&proto_joboutputdef);
  return proto_joboutputdef.DebugString();
}

void ConstJobOutputDef::_JobOutputDef_::Clear() {
  clear_lbi();
}

void ConstJobOutputDef::_JobOutputDef_::CopyFrom(const _JobOutputDef_& other) {
  if (other.has_lbi()) {
    mutable_lbi()->CopyFrom(other.lbi());
  } else {
    clear_lbi();
  }
}


// optional field lbi
bool ConstJobOutputDef::_JobOutputDef_::has_lbi() const {
  return has_lbi_;
}
const ::oneflow::cfg::LogicalBlobId& ConstJobOutputDef::_JobOutputDef_::lbi() const {
  if (!lbi_) {
    static const ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> default_static_value =
      ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
    return *default_static_value;
  }
  return *(lbi_.get());
}
void ConstJobOutputDef::_JobOutputDef_::clear_lbi() {
  if (lbi_) {
    lbi_->Clear();
  }
  has_lbi_ = false;
}
::oneflow::cfg::LogicalBlobId* ConstJobOutputDef::_JobOutputDef_::mutable_lbi() {
  if (!lbi_) {
    lbi_ = ::std::make_shared<::oneflow::cfg::LogicalBlobId>();
  }
  has_lbi_ = true;
  return lbi_.get();
}


int ConstJobOutputDef::_JobOutputDef_::compare(const _JobOutputDef_& other) {
  if (!(has_lbi() == other.has_lbi())) {
    return has_lbi() < other.has_lbi() ? -1 : 1;
  } else if (!(lbi() == other.lbi())) {
    return lbi() < other.lbi() ? -1 : 1;
  }
  return 0;
}

bool ConstJobOutputDef::_JobOutputDef_::operator==(const _JobOutputDef_& other) const {
  return true
    && has_lbi() == other.has_lbi() 
    && lbi() == other.lbi()
  ;
}

std::size_t ConstJobOutputDef::_JobOutputDef_::__CalcHash__() const {
  return 0
    ^ (has_lbi() ? std::hash<::oneflow::cfg::LogicalBlobId>()(lbi()) : 0)
  ;
}

bool ConstJobOutputDef::_JobOutputDef_::operator<(const _JobOutputDef_& other) const {
  return false
    || !(has_lbi() == other.has_lbi()) ? 
      has_lbi() < other.has_lbi() : false
    || !(lbi() == other.lbi()) ? 
      lbi() < other.lbi() : false
  ;
}

using _JobOutputDef_ =  ConstJobOutputDef::_JobOutputDef_;
ConstJobOutputDef::ConstJobOutputDef(const ::std::shared_ptr<_JobOutputDef_>& data): data_(data) {}
ConstJobOutputDef::ConstJobOutputDef(): data_(::std::make_shared<_JobOutputDef_>()) {}
ConstJobOutputDef::ConstJobOutputDef(const ::oneflow::JobOutputDef& proto_joboutputdef) {
  BuildFromProto(proto_joboutputdef);
}
ConstJobOutputDef::ConstJobOutputDef(const ConstJobOutputDef&) = default;
ConstJobOutputDef::ConstJobOutputDef(ConstJobOutputDef&&) noexcept = default;
ConstJobOutputDef::~ConstJobOutputDef() = default;

void ConstJobOutputDef::ToProto(PbMessage* proto_joboutputdef) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobOutputDef*>(proto_joboutputdef));
}
  
::std::string ConstJobOutputDef::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobOutputDef::__Empty__() const {
  return !data_;
}

int ConstJobOutputDef::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"lbi", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstJobOutputDef::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstJobOutputDef::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::LogicalBlobId),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstLogicalBlobId),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobOutputDef::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &lbi();
    default: return nullptr;
  }
}

// required or optional field lbi
bool ConstJobOutputDef::has_lbi() const {
  return __SharedPtrOrDefault__()->has_lbi();
}
const ::oneflow::cfg::LogicalBlobId& ConstJobOutputDef::lbi() const {
  return __SharedPtrOrDefault__()->lbi();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> ConstJobOutputDef::shared_const_lbi() const {
  return lbi().__SharedConst__();
}

::std::shared_ptr<ConstJobOutputDef> ConstJobOutputDef::__SharedConst__() const {
  return ::std::make_shared<ConstJobOutputDef>(data_);
}
int64_t ConstJobOutputDef::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobOutputDef::operator==(const ConstJobOutputDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobOutputDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobOutputDef::operator<(const ConstJobOutputDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobOutputDef_>& ConstJobOutputDef::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobOutputDef_> default_ptr = std::make_shared<_JobOutputDef_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobOutputDef_>& ConstJobOutputDef::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobOutputDef_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobOutputDef
void ConstJobOutputDef::BuildFromProto(const PbMessage& proto_joboutputdef) {
  data_ = ::std::make_shared<_JobOutputDef_>(dynamic_cast<const ::oneflow::JobOutputDef&>(proto_joboutputdef));
}

JobOutputDef::JobOutputDef(const ::std::shared_ptr<_JobOutputDef_>& data)
  : ConstJobOutputDef(data) {}
JobOutputDef::JobOutputDef(const JobOutputDef& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobOutputDef> resize
JobOutputDef::JobOutputDef(JobOutputDef&&) noexcept = default; 
JobOutputDef::JobOutputDef(const ::oneflow::JobOutputDef& proto_joboutputdef) {
  InitFromProto(proto_joboutputdef);
}
JobOutputDef::JobOutputDef() = default;

JobOutputDef::~JobOutputDef() = default;

void JobOutputDef::InitFromProto(const PbMessage& proto_joboutputdef) {
  BuildFromProto(proto_joboutputdef);
}
  
void* JobOutputDef::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_lbi();
    default: return nullptr;
  }
}

bool JobOutputDef::operator==(const JobOutputDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobOutputDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobOutputDef::operator<(const JobOutputDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobOutputDef::Clear() {
  if (data_) { data_.reset(); }
}
void JobOutputDef::CopyFrom(const JobOutputDef& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobOutputDef& JobOutputDef::operator=(const JobOutputDef& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field lbi
void JobOutputDef::clear_lbi() {
  return __SharedPtr__()->clear_lbi();
}
::oneflow::cfg::LogicalBlobId* JobOutputDef::mutable_lbi() {
  return __SharedPtr__()->mutable_lbi();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::LogicalBlobId> JobOutputDef::shared_mutable_lbi() {
  return mutable_lbi()->__SharedMutable__();
}

::std::shared_ptr<JobOutputDef> JobOutputDef::__SharedMutable__() {
  return ::std::make_shared<JobOutputDef>(__SharedPtr__());
}
ConstJobSignatureDef::_JobSignatureDef_::_JobSignatureDef_() { Clear(); }
ConstJobSignatureDef::_JobSignatureDef_::_JobSignatureDef_(const _JobSignatureDef_& other) { CopyFrom(other); }
ConstJobSignatureDef::_JobSignatureDef_::_JobSignatureDef_(const ::oneflow::JobSignatureDef& proto_jobsignaturedef) {
  InitFromProto(proto_jobsignaturedef);
}
ConstJobSignatureDef::_JobSignatureDef_::_JobSignatureDef_(_JobSignatureDef_&& other) = default;
ConstJobSignatureDef::_JobSignatureDef_::~_JobSignatureDef_() = default;

void ConstJobSignatureDef::_JobSignatureDef_::InitFromProto(const ::oneflow::JobSignatureDef& proto_jobsignaturedef) {
  Clear();
  // map field : inputs
  if (!proto_jobsignaturedef.inputs().empty()) {
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_&  mut_inputs = *mutable_inputs();
    for (const auto& pair : proto_jobsignaturedef.inputs()) {
      mut_inputs[pair.first] = ::oneflow::cfg::JobInputDef(pair.second);
    }
  }
  // map field : outputs
  if (!proto_jobsignaturedef.outputs().empty()) {
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_&  mut_outputs = *mutable_outputs();
    for (const auto& pair : proto_jobsignaturedef.outputs()) {
      mut_outputs[pair.first] = ::oneflow::cfg::JobOutputDef(pair.second);
    }
  }
    
}

void ConstJobSignatureDef::_JobSignatureDef_::ToProto(::oneflow::JobSignatureDef* proto_jobsignaturedef) const {
  proto_jobsignaturedef->Clear();
  // map field : inputs
  if (!inputs().empty()) {
    auto& mut_inputs = *(proto_jobsignaturedef->mutable_inputs());
    for (const auto& pair : inputs()) {
      ::oneflow::JobInputDef proto_inputs_value;
      pair.second.ToProto(&proto_inputs_value);
      mut_inputs[pair.first] = proto_inputs_value;
    }
  }
  // map field : outputs
  if (!outputs().empty()) {
    auto& mut_outputs = *(proto_jobsignaturedef->mutable_outputs());
    for (const auto& pair : outputs()) {
      ::oneflow::JobOutputDef proto_outputs_value;
      pair.second.ToProto(&proto_outputs_value);
      mut_outputs[pair.first] = proto_outputs_value;
    }
  }

}

::std::string ConstJobSignatureDef::_JobSignatureDef_::DebugString() const {
  ::oneflow::JobSignatureDef proto_jobsignaturedef;
  this->ToProto(&proto_jobsignaturedef);
  return proto_jobsignaturedef.DebugString();
}

void ConstJobSignatureDef::_JobSignatureDef_::Clear() {
  clear_inputs();
  clear_outputs();
}

void ConstJobSignatureDef::_JobSignatureDef_::CopyFrom(const _JobSignatureDef_& other) {
  mutable_inputs()->CopyFrom(other.inputs());
  mutable_outputs()->CopyFrom(other.outputs());
}


::std::size_t ConstJobSignatureDef::_JobSignatureDef_::inputs_size() const {
  if (!inputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>();
    return default_static_value->size();
  }
  return inputs_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& ConstJobSignatureDef::_JobSignatureDef_::inputs() const {
  if (!inputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>();
    return *(default_static_value.get());
  }
  return *(inputs_.get());
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ * ConstJobSignatureDef::_JobSignatureDef_::mutable_inputs() {
  if (!inputs_) {
    inputs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>();
  }
  return inputs_.get();
}

const ::oneflow::cfg::JobInputDef& ConstJobSignatureDef::_JobSignatureDef_::inputs(::std::string key) const {
  if (!inputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>();
    return default_static_value->at(key);
  }
  return inputs_->at(key);
}

void ConstJobSignatureDef::_JobSignatureDef_::clear_inputs() {
  if (!inputs_) {
    inputs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>();
  }
  return inputs_->Clear();
}


::std::size_t ConstJobSignatureDef::_JobSignatureDef_::outputs_size() const {
  if (!outputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>();
    return default_static_value->size();
  }
  return outputs_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& ConstJobSignatureDef::_JobSignatureDef_::outputs() const {
  if (!outputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>();
    return *(default_static_value.get());
  }
  return *(outputs_.get());
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ * ConstJobSignatureDef::_JobSignatureDef_::mutable_outputs() {
  if (!outputs_) {
    outputs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>();
  }
  return outputs_.get();
}

const ::oneflow::cfg::JobOutputDef& ConstJobSignatureDef::_JobSignatureDef_::outputs(::std::string key) const {
  if (!outputs_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>();
    return default_static_value->at(key);
  }
  return outputs_->at(key);
}

void ConstJobSignatureDef::_JobSignatureDef_::clear_outputs() {
  if (!outputs_) {
    outputs_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>();
  }
  return outputs_->Clear();
}



int ConstJobSignatureDef::_JobSignatureDef_::compare(const _JobSignatureDef_& other) {
  if (!(inputs() == other.inputs())) {
    return inputs() < other.inputs() ? -1 : 1;
  }
  if (!(outputs() == other.outputs())) {
    return outputs() < other.outputs() ? -1 : 1;
  }
  return 0;
}

bool ConstJobSignatureDef::_JobSignatureDef_::operator==(const _JobSignatureDef_& other) const {
  return true
    && inputs() == other.inputs()
    && outputs() == other.outputs()
  ;
}

std::size_t ConstJobSignatureDef::_JobSignatureDef_::__CalcHash__() const {
  return 0
    ^ inputs().__CalcHash__()
    ^ outputs().__CalcHash__()
  ;
}

bool ConstJobSignatureDef::_JobSignatureDef_::operator<(const _JobSignatureDef_& other) const {
  return false
    || !(inputs() == other.inputs()) ? 
      inputs() < other.inputs() : false
    || !(outputs() == other.outputs()) ? 
      outputs() < other.outputs() : false
  ;
}

using _JobSignatureDef_ =  ConstJobSignatureDef::_JobSignatureDef_;
ConstJobSignatureDef::ConstJobSignatureDef(const ::std::shared_ptr<_JobSignatureDef_>& data): data_(data) {}
ConstJobSignatureDef::ConstJobSignatureDef(): data_(::std::make_shared<_JobSignatureDef_>()) {}
ConstJobSignatureDef::ConstJobSignatureDef(const ::oneflow::JobSignatureDef& proto_jobsignaturedef) {
  BuildFromProto(proto_jobsignaturedef);
}
ConstJobSignatureDef::ConstJobSignatureDef(const ConstJobSignatureDef&) = default;
ConstJobSignatureDef::ConstJobSignatureDef(ConstJobSignatureDef&&) noexcept = default;
ConstJobSignatureDef::~ConstJobSignatureDef() = default;

void ConstJobSignatureDef::ToProto(PbMessage* proto_jobsignaturedef) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobSignatureDef*>(proto_jobsignaturedef));
}
  
::std::string ConstJobSignatureDef::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobSignatureDef::__Empty__() const {
  return !data_;
}

int ConstJobSignatureDef::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"inputs", 1},
    {"outputs", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstJobSignatureDef::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstJobSignatureDef::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobInputDef>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobOutputDef>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobSignatureDef::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &inputs();
    case 2: return &outputs();
    default: return nullptr;
  }
}

// map field inputs
::std::size_t ConstJobSignatureDef::inputs_size() const {
  return __SharedPtrOrDefault__()->inputs_size();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& ConstJobSignatureDef::inputs() const {
  return __SharedPtrOrDefault__()->inputs();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> ConstJobSignatureDef::shared_const_inputs() const {
  return inputs().__SharedConst__();
}
// map field outputs
::std::size_t ConstJobSignatureDef::outputs_size() const {
  return __SharedPtrOrDefault__()->outputs_size();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& ConstJobSignatureDef::outputs() const {
  return __SharedPtrOrDefault__()->outputs();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> ConstJobSignatureDef::shared_const_outputs() const {
  return outputs().__SharedConst__();
}

::std::shared_ptr<ConstJobSignatureDef> ConstJobSignatureDef::__SharedConst__() const {
  return ::std::make_shared<ConstJobSignatureDef>(data_);
}
int64_t ConstJobSignatureDef::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobSignatureDef::operator==(const ConstJobSignatureDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobSignatureDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobSignatureDef::operator<(const ConstJobSignatureDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobSignatureDef_>& ConstJobSignatureDef::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobSignatureDef_> default_ptr = std::make_shared<_JobSignatureDef_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobSignatureDef_>& ConstJobSignatureDef::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobSignatureDef_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobSignatureDef
void ConstJobSignatureDef::BuildFromProto(const PbMessage& proto_jobsignaturedef) {
  data_ = ::std::make_shared<_JobSignatureDef_>(dynamic_cast<const ::oneflow::JobSignatureDef&>(proto_jobsignaturedef));
}

JobSignatureDef::JobSignatureDef(const ::std::shared_ptr<_JobSignatureDef_>& data)
  : ConstJobSignatureDef(data) {}
JobSignatureDef::JobSignatureDef(const JobSignatureDef& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobSignatureDef> resize
JobSignatureDef::JobSignatureDef(JobSignatureDef&&) noexcept = default; 
JobSignatureDef::JobSignatureDef(const ::oneflow::JobSignatureDef& proto_jobsignaturedef) {
  InitFromProto(proto_jobsignaturedef);
}
JobSignatureDef::JobSignatureDef() = default;

JobSignatureDef::~JobSignatureDef() = default;

void JobSignatureDef::InitFromProto(const PbMessage& proto_jobsignaturedef) {
  BuildFromProto(proto_jobsignaturedef);
}
  
void* JobSignatureDef::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_inputs();
    case 2: return mutable_outputs();
    default: return nullptr;
  }
}

bool JobSignatureDef::operator==(const JobSignatureDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobSignatureDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobSignatureDef::operator<(const JobSignatureDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobSignatureDef::Clear() {
  if (data_) { data_.reset(); }
}
void JobSignatureDef::CopyFrom(const JobSignatureDef& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobSignatureDef& JobSignatureDef::operator=(const JobSignatureDef& other) {
  CopyFrom(other);
  return *this;
}

// repeated field inputs
void JobSignatureDef::clear_inputs() {
  return __SharedPtr__()->clear_inputs();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_ & JobSignatureDef::inputs() const {
  return __SharedConst__()->inputs();
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_* JobSignatureDef::mutable_inputs() {
  return __SharedPtr__()->mutable_inputs();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> JobSignatureDef::shared_mutable_inputs() {
  return mutable_inputs()->__SharedMutable__();
}
// repeated field outputs
void JobSignatureDef::clear_outputs() {
  return __SharedPtr__()->clear_outputs();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_ & JobSignatureDef::outputs() const {
  return __SharedConst__()->outputs();
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_* JobSignatureDef::mutable_outputs() {
  return __SharedPtr__()->mutable_outputs();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> JobSignatureDef::shared_mutable_outputs() {
  return mutable_outputs()->__SharedMutable__();
}

::std::shared_ptr<JobSignatureDef> JobSignatureDef::__SharedMutable__() {
  return ::std::make_shared<JobSignatureDef>(__SharedPtr__());
}
ConstJobConfigProto::_JobConfigProto_::_JobConfigProto_() { Clear(); }
ConstJobConfigProto::_JobConfigProto_::_JobConfigProto_(const _JobConfigProto_& other) { CopyFrom(other); }
ConstJobConfigProto::_JobConfigProto_::_JobConfigProto_(const ::oneflow::JobConfigProto& proto_jobconfigproto) {
  InitFromProto(proto_jobconfigproto);
}
ConstJobConfigProto::_JobConfigProto_::_JobConfigProto_(_JobConfigProto_&& other) = default;
ConstJobConfigProto::_JobConfigProto_::~_JobConfigProto_() = default;

void ConstJobConfigProto::_JobConfigProto_::InitFromProto(const ::oneflow::JobConfigProto& proto_jobconfigproto) {
  Clear();
  // required_or_optional field: job_name
  if (proto_jobconfigproto.has_job_name()) {
    set_job_name(proto_jobconfigproto.job_name());
  }
  // required_or_optional field: default_data_type
  if (proto_jobconfigproto.has_default_data_type()) {
  set_default_data_type(static_cast<std::remove_reference<std::remove_const<decltype(default_data_type())>::type>::type>(proto_jobconfigproto.default_data_type()));
  }
  // required_or_optional field: memory_allocation_algorithm_conf
  if (proto_jobconfigproto.has_memory_allocation_algorithm_conf()) {
  *mutable_memory_allocation_algorithm_conf() = ::oneflow::cfg::MemoryAllocationAlgorithmConf(proto_jobconfigproto.memory_allocation_algorithm_conf());      
  }
  // required_or_optional field: xrt_config
  if (proto_jobconfigproto.has_xrt_config()) {
  *mutable_xrt_config() = ::oneflow::cfg::XrtConfig(proto_jobconfigproto.xrt_config());      
  }
  // required_or_optional field: indexed_slices_optimizer_conf
  if (proto_jobconfigproto.has_indexed_slices_optimizer_conf()) {
  *mutable_indexed_slices_optimizer_conf() = ::oneflow::cfg::IndexedSlicesOptimizerConf(proto_jobconfigproto.indexed_slices_optimizer_conf());      
  }
  // required_or_optional field: enable_fuse_model_update_ops
  if (proto_jobconfigproto.has_enable_fuse_model_update_ops()) {
    set_enable_fuse_model_update_ops(proto_jobconfigproto.enable_fuse_model_update_ops());
  }
  // required_or_optional field: enable_gradients_stats_aggregation
  if (proto_jobconfigproto.has_enable_gradients_stats_aggregation()) {
    set_enable_gradients_stats_aggregation(proto_jobconfigproto.enable_gradients_stats_aggregation());
  }
  // required_or_optional field: optimizer_placement_optimization_mode
  if (proto_jobconfigproto.has_optimizer_placement_optimization_mode()) {
    set_optimizer_placement_optimization_mode(proto_jobconfigproto.optimizer_placement_optimization_mode());
  }
  // required_or_optional field: optimizer_placement_optimization_threshold
  if (proto_jobconfigproto.has_optimizer_placement_optimization_threshold()) {
    set_optimizer_placement_optimization_threshold(proto_jobconfigproto.optimizer_placement_optimization_threshold());
  }
  // required_or_optional field: qat_config
  if (proto_jobconfigproto.has_qat_config()) {
  *mutable_qat_config() = ::oneflow::cfg::QatConfig(proto_jobconfigproto.qat_config());      
  }
  // required_or_optional field: enable_cudnn
  if (proto_jobconfigproto.has_enable_cudnn()) {
    set_enable_cudnn(proto_jobconfigproto.enable_cudnn());
  }
  // required_or_optional field: cudnn_buf_limit_mbyte
  if (proto_jobconfigproto.has_cudnn_buf_limit_mbyte()) {
    set_cudnn_buf_limit_mbyte(proto_jobconfigproto.cudnn_buf_limit_mbyte());
  }
  // required_or_optional field: cudnn_conv_force_fwd_algo
  if (proto_jobconfigproto.has_cudnn_conv_force_fwd_algo()) {
    set_cudnn_conv_force_fwd_algo(proto_jobconfigproto.cudnn_conv_force_fwd_algo());
  }
  // required_or_optional field: cudnn_conv_force_bwd_data_algo
  if (proto_jobconfigproto.has_cudnn_conv_force_bwd_data_algo()) {
    set_cudnn_conv_force_bwd_data_algo(proto_jobconfigproto.cudnn_conv_force_bwd_data_algo());
  }
  // required_or_optional field: cudnn_conv_force_bwd_filter_algo
  if (proto_jobconfigproto.has_cudnn_conv_force_bwd_filter_algo()) {
    set_cudnn_conv_force_bwd_filter_algo(proto_jobconfigproto.cudnn_conv_force_bwd_filter_algo());
  }
  // required_or_optional field: cudnn_conv_heuristic_search_algo
  if (proto_jobconfigproto.has_cudnn_conv_heuristic_search_algo()) {
    set_cudnn_conv_heuristic_search_algo(proto_jobconfigproto.cudnn_conv_heuristic_search_algo());
  }
  // required_or_optional field: cudnn_conv_use_deterministic_algo_only
  if (proto_jobconfigproto.has_cudnn_conv_use_deterministic_algo_only()) {
    set_cudnn_conv_use_deterministic_algo_only(proto_jobconfigproto.cudnn_conv_use_deterministic_algo_only());
  }
  // required_or_optional field: enable_cudnn_fused_normalization_add_relu
  if (proto_jobconfigproto.has_enable_cudnn_fused_normalization_add_relu()) {
    set_enable_cudnn_fused_normalization_add_relu(proto_jobconfigproto.enable_cudnn_fused_normalization_add_relu());
  }
  // required_or_optional field: enable_fuse_add_to_output
  if (proto_jobconfigproto.has_enable_fuse_add_to_output()) {
    set_enable_fuse_add_to_output(proto_jobconfigproto.enable_fuse_add_to_output());
  }
  // required_or_optional field: enable_fuse_cast_scale
  if (proto_jobconfigproto.has_enable_fuse_cast_scale()) {
    set_enable_fuse_cast_scale(proto_jobconfigproto.enable_fuse_cast_scale());
  }
  // required_or_optional field: num_gradient_accumulation_steps
  if (proto_jobconfigproto.has_num_gradient_accumulation_steps()) {
    set_num_gradient_accumulation_steps(proto_jobconfigproto.num_gradient_accumulation_steps());
  }
  // required_or_optional field: enable_reuse_mem
  if (proto_jobconfigproto.has_enable_reuse_mem()) {
    set_enable_reuse_mem(proto_jobconfigproto.enable_reuse_mem());
  }
  // required_or_optional field: enable_inplace
  if (proto_jobconfigproto.has_enable_inplace()) {
    set_enable_inplace(proto_jobconfigproto.enable_inplace());
  }
  // required_or_optional field: enable_inplace_in_reduce_struct
  if (proto_jobconfigproto.has_enable_inplace_in_reduce_struct()) {
    set_enable_inplace_in_reduce_struct(proto_jobconfigproto.enable_inplace_in_reduce_struct());
  }
  // required_or_optional field: do_parallel_cast_before_widening_type_cast
  if (proto_jobconfigproto.has_do_parallel_cast_before_widening_type_cast()) {
    set_do_parallel_cast_before_widening_type_cast(proto_jobconfigproto.do_parallel_cast_before_widening_type_cast());
  }
  // required_or_optional field: prune_parallel_cast_ops
  if (proto_jobconfigproto.has_prune_parallel_cast_ops()) {
    set_prune_parallel_cast_ops(proto_jobconfigproto.prune_parallel_cast_ops());
  }
  // required_or_optional field: prune_cast_to_static_shape_ops
  if (proto_jobconfigproto.has_prune_cast_to_static_shape_ops()) {
    set_prune_cast_to_static_shape_ops(proto_jobconfigproto.prune_cast_to_static_shape_ops());
  }
  // required_or_optional field: prune_amp_white_identity_ops
  if (proto_jobconfigproto.has_prune_amp_white_identity_ops()) {
    set_prune_amp_white_identity_ops(proto_jobconfigproto.prune_amp_white_identity_ops());
  }
  // required_or_optional field: cudnn_conv_enable_pseudo_half
  if (proto_jobconfigproto.has_cudnn_conv_enable_pseudo_half()) {
    set_cudnn_conv_enable_pseudo_half(proto_jobconfigproto.cudnn_conv_enable_pseudo_half());
  }
  // required_or_optional field: enable_auto_mixed_precision
  if (proto_jobconfigproto.has_enable_auto_mixed_precision()) {
    set_enable_auto_mixed_precision(proto_jobconfigproto.enable_auto_mixed_precision());
  }
  // required_or_optional field: enable_quantization_aware_training
  if (proto_jobconfigproto.has_enable_quantization_aware_training()) {
    set_enable_quantization_aware_training(proto_jobconfigproto.enable_quantization_aware_training());
  }
  // required_or_optional field: concurrency_width
  if (proto_jobconfigproto.has_concurrency_width()) {
    set_concurrency_width(proto_jobconfigproto.concurrency_width());
  }
  // map field : flag_name2flag_value
  if (!proto_jobconfigproto.flag_name2flag_value().empty()) {
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_&  mut_flag_name2flag_value = *mutable_flag_name2flag_value();
    for (const auto& pair : proto_jobconfigproto.flag_name2flag_value()) {
      mut_flag_name2flag_value[pair.first] = ::oneflow::cfg::AttrValue(pair.second);
    }
  }
  // required_or_optional field: logical_object_id
  if (proto_jobconfigproto.has_logical_object_id()) {
    set_logical_object_id(proto_jobconfigproto.logical_object_id());
  }
  // required_or_optional field: signature
  if (proto_jobconfigproto.has_signature()) {
  *mutable_signature() = ::oneflow::cfg::JobSignatureDef(proto_jobconfigproto.signature());      
  }
  // oneof field: job_type
  JobTypeCase job_type_case = JobTypeCase(int(proto_jobconfigproto.job_type_case()));
  switch (job_type_case) {
    case kTrainConf: {
      *mutable_train_conf() = ::oneflow::cfg::TrainConf(proto_jobconfigproto.train_conf());
      break;
  }
    case kPredictConf: {
      *mutable_predict_conf() = ::oneflow::cfg::PredictConf(proto_jobconfigproto.predict_conf());
      break;
  }
    case JOB_TYPE_NOT_SET: {
      break;
    }
  }
    // oneof field: default_initialize_conf
  DefaultInitializeConfCase default_initialize_conf_case = DefaultInitializeConfCase(int(proto_jobconfigproto.default_initialize_conf_case()));
  switch (default_initialize_conf_case) {
    case kDefaultInitializerConf: {
      *mutable_default_initializer_conf() = ::oneflow::cfg::InitializerConf(proto_jobconfigproto.default_initializer_conf());
      break;
  }
    case kDefaultInitializeWithSnapshotPath: {
      set_default_initialize_with_snapshot_path(proto_jobconfigproto.default_initialize_with_snapshot_path());
      break;
  }
    case DEFAULT_INITIALIZE_CONF_NOT_SET: {
      break;
    }
  }
      
}

void ConstJobConfigProto::_JobConfigProto_::ToProto(::oneflow::JobConfigProto* proto_jobconfigproto) const {
  proto_jobconfigproto->Clear();
  // required_or_optional field: job_name
  if (this->has_job_name()) {
    proto_jobconfigproto->set_job_name(job_name());
    }
  // required_or_optional field: default_data_type
  if (this->has_default_data_type()) {
    proto_jobconfigproto->set_default_data_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_jobconfigproto->default_data_type())>::type>::type>(default_data_type()));
    }
  // required_or_optional field: memory_allocation_algorithm_conf
  if (this->has_memory_allocation_algorithm_conf()) {
    ::oneflow::MemoryAllocationAlgorithmConf proto_memory_allocation_algorithm_conf;
    memory_allocation_algorithm_conf().ToProto(&proto_memory_allocation_algorithm_conf);
    proto_jobconfigproto->mutable_memory_allocation_algorithm_conf()->CopyFrom(proto_memory_allocation_algorithm_conf);
    }
  // required_or_optional field: xrt_config
  if (this->has_xrt_config()) {
    ::oneflow::XrtConfig proto_xrt_config;
    xrt_config().ToProto(&proto_xrt_config);
    proto_jobconfigproto->mutable_xrt_config()->CopyFrom(proto_xrt_config);
    }
  // required_or_optional field: indexed_slices_optimizer_conf
  if (this->has_indexed_slices_optimizer_conf()) {
    ::oneflow::IndexedSlicesOptimizerConf proto_indexed_slices_optimizer_conf;
    indexed_slices_optimizer_conf().ToProto(&proto_indexed_slices_optimizer_conf);
    proto_jobconfigproto->mutable_indexed_slices_optimizer_conf()->CopyFrom(proto_indexed_slices_optimizer_conf);
    }
  // required_or_optional field: enable_fuse_model_update_ops
  if (this->has_enable_fuse_model_update_ops()) {
    proto_jobconfigproto->set_enable_fuse_model_update_ops(enable_fuse_model_update_ops());
    }
  // required_or_optional field: enable_gradients_stats_aggregation
  if (this->has_enable_gradients_stats_aggregation()) {
    proto_jobconfigproto->set_enable_gradients_stats_aggregation(enable_gradients_stats_aggregation());
    }
  // required_or_optional field: optimizer_placement_optimization_mode
  if (this->has_optimizer_placement_optimization_mode()) {
    proto_jobconfigproto->set_optimizer_placement_optimization_mode(optimizer_placement_optimization_mode());
    }
  // required_or_optional field: optimizer_placement_optimization_threshold
  if (this->has_optimizer_placement_optimization_threshold()) {
    proto_jobconfigproto->set_optimizer_placement_optimization_threshold(optimizer_placement_optimization_threshold());
    }
  // required_or_optional field: qat_config
  if (this->has_qat_config()) {
    ::oneflow::QatConfig proto_qat_config;
    qat_config().ToProto(&proto_qat_config);
    proto_jobconfigproto->mutable_qat_config()->CopyFrom(proto_qat_config);
    }
  // required_or_optional field: enable_cudnn
  if (this->has_enable_cudnn()) {
    proto_jobconfigproto->set_enable_cudnn(enable_cudnn());
    }
  // required_or_optional field: cudnn_buf_limit_mbyte
  if (this->has_cudnn_buf_limit_mbyte()) {
    proto_jobconfigproto->set_cudnn_buf_limit_mbyte(cudnn_buf_limit_mbyte());
    }
  // required_or_optional field: cudnn_conv_force_fwd_algo
  if (this->has_cudnn_conv_force_fwd_algo()) {
    proto_jobconfigproto->set_cudnn_conv_force_fwd_algo(cudnn_conv_force_fwd_algo());
    }
  // required_or_optional field: cudnn_conv_force_bwd_data_algo
  if (this->has_cudnn_conv_force_bwd_data_algo()) {
    proto_jobconfigproto->set_cudnn_conv_force_bwd_data_algo(cudnn_conv_force_bwd_data_algo());
    }
  // required_or_optional field: cudnn_conv_force_bwd_filter_algo
  if (this->has_cudnn_conv_force_bwd_filter_algo()) {
    proto_jobconfigproto->set_cudnn_conv_force_bwd_filter_algo(cudnn_conv_force_bwd_filter_algo());
    }
  // required_or_optional field: cudnn_conv_heuristic_search_algo
  if (this->has_cudnn_conv_heuristic_search_algo()) {
    proto_jobconfigproto->set_cudnn_conv_heuristic_search_algo(cudnn_conv_heuristic_search_algo());
    }
  // required_or_optional field: cudnn_conv_use_deterministic_algo_only
  if (this->has_cudnn_conv_use_deterministic_algo_only()) {
    proto_jobconfigproto->set_cudnn_conv_use_deterministic_algo_only(cudnn_conv_use_deterministic_algo_only());
    }
  // required_or_optional field: enable_cudnn_fused_normalization_add_relu
  if (this->has_enable_cudnn_fused_normalization_add_relu()) {
    proto_jobconfigproto->set_enable_cudnn_fused_normalization_add_relu(enable_cudnn_fused_normalization_add_relu());
    }
  // required_or_optional field: enable_fuse_add_to_output
  if (this->has_enable_fuse_add_to_output()) {
    proto_jobconfigproto->set_enable_fuse_add_to_output(enable_fuse_add_to_output());
    }
  // required_or_optional field: enable_fuse_cast_scale
  if (this->has_enable_fuse_cast_scale()) {
    proto_jobconfigproto->set_enable_fuse_cast_scale(enable_fuse_cast_scale());
    }
  // required_or_optional field: num_gradient_accumulation_steps
  if (this->has_num_gradient_accumulation_steps()) {
    proto_jobconfigproto->set_num_gradient_accumulation_steps(num_gradient_accumulation_steps());
    }
  // required_or_optional field: enable_reuse_mem
  if (this->has_enable_reuse_mem()) {
    proto_jobconfigproto->set_enable_reuse_mem(enable_reuse_mem());
    }
  // required_or_optional field: enable_inplace
  if (this->has_enable_inplace()) {
    proto_jobconfigproto->set_enable_inplace(enable_inplace());
    }
  // required_or_optional field: enable_inplace_in_reduce_struct
  if (this->has_enable_inplace_in_reduce_struct()) {
    proto_jobconfigproto->set_enable_inplace_in_reduce_struct(enable_inplace_in_reduce_struct());
    }
  // required_or_optional field: do_parallel_cast_before_widening_type_cast
  if (this->has_do_parallel_cast_before_widening_type_cast()) {
    proto_jobconfigproto->set_do_parallel_cast_before_widening_type_cast(do_parallel_cast_before_widening_type_cast());
    }
  // required_or_optional field: prune_parallel_cast_ops
  if (this->has_prune_parallel_cast_ops()) {
    proto_jobconfigproto->set_prune_parallel_cast_ops(prune_parallel_cast_ops());
    }
  // required_or_optional field: prune_cast_to_static_shape_ops
  if (this->has_prune_cast_to_static_shape_ops()) {
    proto_jobconfigproto->set_prune_cast_to_static_shape_ops(prune_cast_to_static_shape_ops());
    }
  // required_or_optional field: prune_amp_white_identity_ops
  if (this->has_prune_amp_white_identity_ops()) {
    proto_jobconfigproto->set_prune_amp_white_identity_ops(prune_amp_white_identity_ops());
    }
  // required_or_optional field: cudnn_conv_enable_pseudo_half
  if (this->has_cudnn_conv_enable_pseudo_half()) {
    proto_jobconfigproto->set_cudnn_conv_enable_pseudo_half(cudnn_conv_enable_pseudo_half());
    }
  // required_or_optional field: enable_auto_mixed_precision
  if (this->has_enable_auto_mixed_precision()) {
    proto_jobconfigproto->set_enable_auto_mixed_precision(enable_auto_mixed_precision());
    }
  // required_or_optional field: enable_quantization_aware_training
  if (this->has_enable_quantization_aware_training()) {
    proto_jobconfigproto->set_enable_quantization_aware_training(enable_quantization_aware_training());
    }
  // required_or_optional field: concurrency_width
  if (this->has_concurrency_width()) {
    proto_jobconfigproto->set_concurrency_width(concurrency_width());
    }
  // map field : flag_name2flag_value
  if (!flag_name2flag_value().empty()) {
    auto& mut_flag_name2flag_value = *(proto_jobconfigproto->mutable_flag_name2flag_value());
    for (const auto& pair : flag_name2flag_value()) {
      ::oneflow::AttrValue proto_flag_name2flag_value_value;
      pair.second.ToProto(&proto_flag_name2flag_value_value);
      mut_flag_name2flag_value[pair.first] = proto_flag_name2flag_value_value;
    }
  }
  // required_or_optional field: logical_object_id
  if (this->has_logical_object_id()) {
    proto_jobconfigproto->set_logical_object_id(logical_object_id());
    }
  // required_or_optional field: signature
  if (this->has_signature()) {
    ::oneflow::JobSignatureDef proto_signature;
    signature().ToProto(&proto_signature);
    proto_jobconfigproto->mutable_signature()->CopyFrom(proto_signature);
    }

  // oneof field: job_type
  ::oneflow::JobConfigProto::JobTypeCase job_type_case = ::oneflow::JobConfigProto::JobTypeCase(int(this->job_type_case()));
  switch (job_type_case) {
    case ::oneflow::JobConfigProto::kTrainConf: {
      ::oneflow::TrainConf of_proto_train_conf;
      train_conf().ToProto(&of_proto_train_conf);
      proto_jobconfigproto->mutable_train_conf()->CopyFrom(of_proto_train_conf);
      break;
    }
    case ::oneflow::JobConfigProto::kPredictConf: {
      ::oneflow::PredictConf of_proto_predict_conf;
      predict_conf().ToProto(&of_proto_predict_conf);
      proto_jobconfigproto->mutable_predict_conf()->CopyFrom(of_proto_predict_conf);
      break;
    }
    case ::oneflow::JobConfigProto::JOB_TYPE_NOT_SET: {
      break;
    }
  }
  // oneof field: default_initialize_conf
  ::oneflow::JobConfigProto::DefaultInitializeConfCase default_initialize_conf_case = ::oneflow::JobConfigProto::DefaultInitializeConfCase(int(this->default_initialize_conf_case()));
  switch (default_initialize_conf_case) {
    case ::oneflow::JobConfigProto::kDefaultInitializerConf: {
      ::oneflow::InitializerConf of_proto_default_initializer_conf;
      default_initializer_conf().ToProto(&of_proto_default_initializer_conf);
      proto_jobconfigproto->mutable_default_initializer_conf()->CopyFrom(of_proto_default_initializer_conf);
      break;
    }
    case ::oneflow::JobConfigProto::kDefaultInitializeWithSnapshotPath: {
      proto_jobconfigproto->set_default_initialize_with_snapshot_path(default_initialize_with_snapshot_path());
      break;
    }
    case ::oneflow::JobConfigProto::DEFAULT_INITIALIZE_CONF_NOT_SET: {
      break;
    }
  }
}

::std::string ConstJobConfigProto::_JobConfigProto_::DebugString() const {
  ::oneflow::JobConfigProto proto_jobconfigproto;
  this->ToProto(&proto_jobconfigproto);
  return proto_jobconfigproto.DebugString();
}

void ConstJobConfigProto::_JobConfigProto_::Clear() {
  clear_job_name();
  clear_default_data_type();
  clear_memory_allocation_algorithm_conf();
  clear_xrt_config();
  clear_indexed_slices_optimizer_conf();
  clear_enable_fuse_model_update_ops();
  clear_enable_gradients_stats_aggregation();
  clear_optimizer_placement_optimization_mode();
  clear_optimizer_placement_optimization_threshold();
  clear_qat_config();
  clear_enable_cudnn();
  clear_cudnn_buf_limit_mbyte();
  clear_cudnn_conv_force_fwd_algo();
  clear_cudnn_conv_force_bwd_data_algo();
  clear_cudnn_conv_force_bwd_filter_algo();
  clear_cudnn_conv_heuristic_search_algo();
  clear_cudnn_conv_use_deterministic_algo_only();
  clear_enable_cudnn_fused_normalization_add_relu();
  clear_enable_fuse_add_to_output();
  clear_enable_fuse_cast_scale();
  clear_num_gradient_accumulation_steps();
  clear_enable_reuse_mem();
  clear_enable_inplace();
  clear_enable_inplace_in_reduce_struct();
  clear_do_parallel_cast_before_widening_type_cast();
  clear_prune_parallel_cast_ops();
  clear_prune_cast_to_static_shape_ops();
  clear_prune_amp_white_identity_ops();
  clear_cudnn_conv_enable_pseudo_half();
  clear_enable_auto_mixed_precision();
  clear_enable_quantization_aware_training();
  clear_concurrency_width();
  clear_flag_name2flag_value();
  clear_logical_object_id();
  clear_signature();
  clear_job_type();
  clear_default_initialize_conf();
}

void ConstJobConfigProto::_JobConfigProto_::CopyFrom(const _JobConfigProto_& other) {
  if (other.has_job_name()) {
    set_job_name(other.job_name());
  } else {
    clear_job_name();
  }
  if (other.has_default_data_type()) {
    set_default_data_type(other.default_data_type());
  } else {
    clear_default_data_type();
  }
  if (other.has_memory_allocation_algorithm_conf()) {
    mutable_memory_allocation_algorithm_conf()->CopyFrom(other.memory_allocation_algorithm_conf());
  } else {
    clear_memory_allocation_algorithm_conf();
  }
  if (other.has_xrt_config()) {
    mutable_xrt_config()->CopyFrom(other.xrt_config());
  } else {
    clear_xrt_config();
  }
  if (other.has_indexed_slices_optimizer_conf()) {
    mutable_indexed_slices_optimizer_conf()->CopyFrom(other.indexed_slices_optimizer_conf());
  } else {
    clear_indexed_slices_optimizer_conf();
  }
  if (other.has_enable_fuse_model_update_ops()) {
    set_enable_fuse_model_update_ops(other.enable_fuse_model_update_ops());
  } else {
    clear_enable_fuse_model_update_ops();
  }
  if (other.has_enable_gradients_stats_aggregation()) {
    set_enable_gradients_stats_aggregation(other.enable_gradients_stats_aggregation());
  } else {
    clear_enable_gradients_stats_aggregation();
  }
  if (other.has_optimizer_placement_optimization_mode()) {
    set_optimizer_placement_optimization_mode(other.optimizer_placement_optimization_mode());
  } else {
    clear_optimizer_placement_optimization_mode();
  }
  if (other.has_optimizer_placement_optimization_threshold()) {
    set_optimizer_placement_optimization_threshold(other.optimizer_placement_optimization_threshold());
  } else {
    clear_optimizer_placement_optimization_threshold();
  }
  if (other.has_qat_config()) {
    mutable_qat_config()->CopyFrom(other.qat_config());
  } else {
    clear_qat_config();
  }
  if (other.has_enable_cudnn()) {
    set_enable_cudnn(other.enable_cudnn());
  } else {
    clear_enable_cudnn();
  }
  if (other.has_cudnn_buf_limit_mbyte()) {
    set_cudnn_buf_limit_mbyte(other.cudnn_buf_limit_mbyte());
  } else {
    clear_cudnn_buf_limit_mbyte();
  }
  if (other.has_cudnn_conv_force_fwd_algo()) {
    set_cudnn_conv_force_fwd_algo(other.cudnn_conv_force_fwd_algo());
  } else {
    clear_cudnn_conv_force_fwd_algo();
  }
  if (other.has_cudnn_conv_force_bwd_data_algo()) {
    set_cudnn_conv_force_bwd_data_algo(other.cudnn_conv_force_bwd_data_algo());
  } else {
    clear_cudnn_conv_force_bwd_data_algo();
  }
  if (other.has_cudnn_conv_force_bwd_filter_algo()) {
    set_cudnn_conv_force_bwd_filter_algo(other.cudnn_conv_force_bwd_filter_algo());
  } else {
    clear_cudnn_conv_force_bwd_filter_algo();
  }
  if (other.has_cudnn_conv_heuristic_search_algo()) {
    set_cudnn_conv_heuristic_search_algo(other.cudnn_conv_heuristic_search_algo());
  } else {
    clear_cudnn_conv_heuristic_search_algo();
  }
  if (other.has_cudnn_conv_use_deterministic_algo_only()) {
    set_cudnn_conv_use_deterministic_algo_only(other.cudnn_conv_use_deterministic_algo_only());
  } else {
    clear_cudnn_conv_use_deterministic_algo_only();
  }
  if (other.has_enable_cudnn_fused_normalization_add_relu()) {
    set_enable_cudnn_fused_normalization_add_relu(other.enable_cudnn_fused_normalization_add_relu());
  } else {
    clear_enable_cudnn_fused_normalization_add_relu();
  }
  if (other.has_enable_fuse_add_to_output()) {
    set_enable_fuse_add_to_output(other.enable_fuse_add_to_output());
  } else {
    clear_enable_fuse_add_to_output();
  }
  if (other.has_enable_fuse_cast_scale()) {
    set_enable_fuse_cast_scale(other.enable_fuse_cast_scale());
  } else {
    clear_enable_fuse_cast_scale();
  }
  if (other.has_num_gradient_accumulation_steps()) {
    set_num_gradient_accumulation_steps(other.num_gradient_accumulation_steps());
  } else {
    clear_num_gradient_accumulation_steps();
  }
  if (other.has_enable_reuse_mem()) {
    set_enable_reuse_mem(other.enable_reuse_mem());
  } else {
    clear_enable_reuse_mem();
  }
  if (other.has_enable_inplace()) {
    set_enable_inplace(other.enable_inplace());
  } else {
    clear_enable_inplace();
  }
  if (other.has_enable_inplace_in_reduce_struct()) {
    set_enable_inplace_in_reduce_struct(other.enable_inplace_in_reduce_struct());
  } else {
    clear_enable_inplace_in_reduce_struct();
  }
  if (other.has_do_parallel_cast_before_widening_type_cast()) {
    set_do_parallel_cast_before_widening_type_cast(other.do_parallel_cast_before_widening_type_cast());
  } else {
    clear_do_parallel_cast_before_widening_type_cast();
  }
  if (other.has_prune_parallel_cast_ops()) {
    set_prune_parallel_cast_ops(other.prune_parallel_cast_ops());
  } else {
    clear_prune_parallel_cast_ops();
  }
  if (other.has_prune_cast_to_static_shape_ops()) {
    set_prune_cast_to_static_shape_ops(other.prune_cast_to_static_shape_ops());
  } else {
    clear_prune_cast_to_static_shape_ops();
  }
  if (other.has_prune_amp_white_identity_ops()) {
    set_prune_amp_white_identity_ops(other.prune_amp_white_identity_ops());
  } else {
    clear_prune_amp_white_identity_ops();
  }
  if (other.has_cudnn_conv_enable_pseudo_half()) {
    set_cudnn_conv_enable_pseudo_half(other.cudnn_conv_enable_pseudo_half());
  } else {
    clear_cudnn_conv_enable_pseudo_half();
  }
  if (other.has_enable_auto_mixed_precision()) {
    set_enable_auto_mixed_precision(other.enable_auto_mixed_precision());
  } else {
    clear_enable_auto_mixed_precision();
  }
  if (other.has_enable_quantization_aware_training()) {
    set_enable_quantization_aware_training(other.enable_quantization_aware_training());
  } else {
    clear_enable_quantization_aware_training();
  }
  if (other.has_concurrency_width()) {
    set_concurrency_width(other.concurrency_width());
  } else {
    clear_concurrency_width();
  }
  mutable_flag_name2flag_value()->CopyFrom(other.flag_name2flag_value());
  if (other.has_logical_object_id()) {
    set_logical_object_id(other.logical_object_id());
  } else {
    clear_logical_object_id();
  }
  if (other.has_signature()) {
    mutable_signature()->CopyFrom(other.signature());
  } else {
    clear_signature();
  }
  job_type_copy_from(other);
  default_initialize_conf_copy_from(other);
}


// optional field job_name
bool ConstJobConfigProto::_JobConfigProto_::has_job_name() const {
  return has_job_name_;
}
const ::std::string& ConstJobConfigProto::_JobConfigProto_::job_name() const {
  if (has_job_name_) { return job_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_job_name() {
  has_job_name_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_job_name(const ::std::string& value) {
  job_name_ = value;
  has_job_name_ = true;
}
::std::string* ConstJobConfigProto::_JobConfigProto_::mutable_job_name() {
  has_job_name_ = true;
  return &job_name_;
}

// oneof field job_type: train_conf
bool ConstJobConfigProto::_JobConfigProto_::has_train_conf() const {
  return job_type_case() == kTrainConf;
}
void ConstJobConfigProto::_JobConfigProto_::clear_train_conf() {
  if (has_train_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TrainConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(job_type_.train_conf_));
      ptr->~Shared_ptr();
    }
    job_type_case_ = JOB_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::TrainConf& ConstJobConfigProto::_JobConfigProto_::train_conf() const {
  if (has_train_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::TrainConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::TrainConf>*>(&(job_type_.train_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::TrainConf> default_static_value = ::std::make_shared<::oneflow::cfg::TrainConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::TrainConf* ConstJobConfigProto::_JobConfigProto_::mutable_train_conf() {
  if(!has_train_conf()) {
    clear_job_type();
    new (&(job_type_.train_conf_)) ::std::shared_ptr<::oneflow::cfg::TrainConf>(new ::oneflow::cfg::TrainConf());
  }
  job_type_case_ = kTrainConf;
  ::std::shared_ptr<::oneflow::cfg::TrainConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::TrainConf>*>(&(job_type_.train_conf_));
  return  (*ptr).get();
}

// oneof field job_type: predict_conf
bool ConstJobConfigProto::_JobConfigProto_::has_predict_conf() const {
  return job_type_case() == kPredictConf;
}
void ConstJobConfigProto::_JobConfigProto_::clear_predict_conf() {
  if (has_predict_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PredictConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(job_type_.predict_conf_));
      ptr->~Shared_ptr();
    }
    job_type_case_ = JOB_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::PredictConf& ConstJobConfigProto::_JobConfigProto_::predict_conf() const {
  if (has_predict_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::PredictConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::PredictConf>*>(&(job_type_.predict_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::PredictConf> default_static_value = ::std::make_shared<::oneflow::cfg::PredictConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::PredictConf* ConstJobConfigProto::_JobConfigProto_::mutable_predict_conf() {
  if(!has_predict_conf()) {
    clear_job_type();
    new (&(job_type_.predict_conf_)) ::std::shared_ptr<::oneflow::cfg::PredictConf>(new ::oneflow::cfg::PredictConf());
  }
  job_type_case_ = kPredictConf;
  ::std::shared_ptr<::oneflow::cfg::PredictConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::PredictConf>*>(&(job_type_.predict_conf_));
  return  (*ptr).get();
}

// optional field default_data_type
bool ConstJobConfigProto::_JobConfigProto_::has_default_data_type() const {
  return has_default_data_type_;
}
const ::oneflow::cfg::DataType& ConstJobConfigProto::_JobConfigProto_::default_data_type() const {
  if (has_default_data_type_) { return default_data_type_; }
  static const ::oneflow::cfg::DataType default_static_value =
    ::oneflow::cfg::DataType(2);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_default_data_type() {
  has_default_data_type_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_default_data_type(const ::oneflow::cfg::DataType& value) {
  default_data_type_ = value;
  has_default_data_type_ = true;
}
::oneflow::cfg::DataType* ConstJobConfigProto::_JobConfigProto_::mutable_default_data_type() {
  has_default_data_type_ = true;
  return &default_data_type_;
}

// oneof field default_initialize_conf: default_initializer_conf
bool ConstJobConfigProto::_JobConfigProto_::has_default_initializer_conf() const {
  return default_initialize_conf_case() == kDefaultInitializerConf;
}
void ConstJobConfigProto::_JobConfigProto_::clear_default_initializer_conf() {
  if (has_default_initializer_conf()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InitializerConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(default_initialize_conf_.default_initializer_conf_));
      ptr->~Shared_ptr();
    }
    default_initialize_conf_case_ = DEFAULT_INITIALIZE_CONF_NOT_SET;
  }
}

const ::oneflow::cfg::InitializerConf& ConstJobConfigProto::_JobConfigProto_::default_initializer_conf() const {
  if (has_default_initializer_conf()) {
      const ::std::shared_ptr<::oneflow::cfg::InitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::InitializerConf>*>(&(default_initialize_conf_.default_initializer_conf_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::InitializerConf> default_static_value = ::std::make_shared<::oneflow::cfg::InitializerConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::InitializerConf* ConstJobConfigProto::_JobConfigProto_::mutable_default_initializer_conf() {
  if(!has_default_initializer_conf()) {
    clear_default_initialize_conf();
    new (&(default_initialize_conf_.default_initializer_conf_)) ::std::shared_ptr<::oneflow::cfg::InitializerConf>(new ::oneflow::cfg::InitializerConf());
  }
  default_initialize_conf_case_ = kDefaultInitializerConf;
  ::std::shared_ptr<::oneflow::cfg::InitializerConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::InitializerConf>*>(&(default_initialize_conf_.default_initializer_conf_));
  return  (*ptr).get();
}

// oneof field default_initialize_conf: default_initialize_with_snapshot_path
bool ConstJobConfigProto::_JobConfigProto_::has_default_initialize_with_snapshot_path() const {
  return default_initialize_conf_case() == kDefaultInitializeWithSnapshotPath;
}
void ConstJobConfigProto::_JobConfigProto_::clear_default_initialize_with_snapshot_path() {
  if (has_default_initialize_with_snapshot_path()) {
    {
      using String = ::std::string;
      String* ptr = reinterpret_cast<String*>(&(default_initialize_conf_.default_initialize_with_snapshot_path_)[0]);
      ptr->~String();
    }
    default_initialize_conf_case_ = DEFAULT_INITIALIZE_CONF_NOT_SET;
  }
}

const ::std::string& ConstJobConfigProto::_JobConfigProto_::default_initialize_with_snapshot_path() const {
  if (has_default_initialize_with_snapshot_path()) {
      const ::std::string* ptr = reinterpret_cast<const ::std::string*>(&(default_initialize_conf_.default_initialize_with_snapshot_path_)[0]);
    return *ptr;
    } else {
      static const ::std::string default_static_value = ::std::string();
    return default_static_value;
    }
}
void ConstJobConfigProto::_JobConfigProto_::set_default_initialize_with_snapshot_path(const ::std::string& value) {
  if(!has_default_initialize_with_snapshot_path()) {
    clear_default_initialize_conf();
      new (&(default_initialize_conf_.default_initialize_with_snapshot_path_)) std::string();
    }
  default_initialize_conf_case_ = kDefaultInitializeWithSnapshotPath;
    std::string* ptr = reinterpret_cast<std::string*>(&(default_initialize_conf_.default_initialize_with_snapshot_path_)[0]);
  *ptr = value;
  }
::std::string* ConstJobConfigProto::_JobConfigProto_::mutable_default_initialize_with_snapshot_path() {
  if(!has_default_initialize_with_snapshot_path()) {
    clear_default_initialize_conf();
      new (&(default_initialize_conf_.default_initialize_with_snapshot_path_)) std::string();
    }
    ::std::string* ptr = reinterpret_cast<::std::string*>(&(default_initialize_conf_.default_initialize_with_snapshot_path_)[0]);
  return ptr;
  }

// optional field memory_allocation_algorithm_conf
bool ConstJobConfigProto::_JobConfigProto_::has_memory_allocation_algorithm_conf() const {
  return has_memory_allocation_algorithm_conf_;
}
const ::oneflow::cfg::MemoryAllocationAlgorithmConf& ConstJobConfigProto::_JobConfigProto_::memory_allocation_algorithm_conf() const {
  if (!memory_allocation_algorithm_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::MemoryAllocationAlgorithmConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::MemoryAllocationAlgorithmConf>();
    return *default_static_value;
  }
  return *(memory_allocation_algorithm_conf_.get());
}
void ConstJobConfigProto::_JobConfigProto_::clear_memory_allocation_algorithm_conf() {
  if (memory_allocation_algorithm_conf_) {
    memory_allocation_algorithm_conf_->Clear();
  }
  has_memory_allocation_algorithm_conf_ = false;
}
::oneflow::cfg::MemoryAllocationAlgorithmConf* ConstJobConfigProto::_JobConfigProto_::mutable_memory_allocation_algorithm_conf() {
  if (!memory_allocation_algorithm_conf_) {
    memory_allocation_algorithm_conf_ = ::std::make_shared<::oneflow::cfg::MemoryAllocationAlgorithmConf>();
  }
  has_memory_allocation_algorithm_conf_ = true;
  return memory_allocation_algorithm_conf_.get();
}

// optional field xrt_config
bool ConstJobConfigProto::_JobConfigProto_::has_xrt_config() const {
  return has_xrt_config_;
}
const ::oneflow::cfg::XrtConfig& ConstJobConfigProto::_JobConfigProto_::xrt_config() const {
  if (!xrt_config_) {
    static const ::std::shared_ptr<::oneflow::cfg::XrtConfig> default_static_value =
      ::std::make_shared<::oneflow::cfg::XrtConfig>();
    return *default_static_value;
  }
  return *(xrt_config_.get());
}
void ConstJobConfigProto::_JobConfigProto_::clear_xrt_config() {
  if (xrt_config_) {
    xrt_config_->Clear();
  }
  has_xrt_config_ = false;
}
::oneflow::cfg::XrtConfig* ConstJobConfigProto::_JobConfigProto_::mutable_xrt_config() {
  if (!xrt_config_) {
    xrt_config_ = ::std::make_shared<::oneflow::cfg::XrtConfig>();
  }
  has_xrt_config_ = true;
  return xrt_config_.get();
}

// optional field indexed_slices_optimizer_conf
bool ConstJobConfigProto::_JobConfigProto_::has_indexed_slices_optimizer_conf() const {
  return has_indexed_slices_optimizer_conf_;
}
const ::oneflow::cfg::IndexedSlicesOptimizerConf& ConstJobConfigProto::_JobConfigProto_::indexed_slices_optimizer_conf() const {
  if (!indexed_slices_optimizer_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::IndexedSlicesOptimizerConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::IndexedSlicesOptimizerConf>();
    return *default_static_value;
  }
  return *(indexed_slices_optimizer_conf_.get());
}
void ConstJobConfigProto::_JobConfigProto_::clear_indexed_slices_optimizer_conf() {
  if (indexed_slices_optimizer_conf_) {
    indexed_slices_optimizer_conf_->Clear();
  }
  has_indexed_slices_optimizer_conf_ = false;
}
::oneflow::cfg::IndexedSlicesOptimizerConf* ConstJobConfigProto::_JobConfigProto_::mutable_indexed_slices_optimizer_conf() {
  if (!indexed_slices_optimizer_conf_) {
    indexed_slices_optimizer_conf_ = ::std::make_shared<::oneflow::cfg::IndexedSlicesOptimizerConf>();
  }
  has_indexed_slices_optimizer_conf_ = true;
  return indexed_slices_optimizer_conf_.get();
}

// optional field enable_fuse_model_update_ops
bool ConstJobConfigProto::_JobConfigProto_::has_enable_fuse_model_update_ops() const {
  return has_enable_fuse_model_update_ops_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_fuse_model_update_ops() const {
  if (has_enable_fuse_model_update_ops_) { return enable_fuse_model_update_ops_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_fuse_model_update_ops() {
  has_enable_fuse_model_update_ops_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_fuse_model_update_ops(const bool& value) {
  enable_fuse_model_update_ops_ = value;
  has_enable_fuse_model_update_ops_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_fuse_model_update_ops() {
  has_enable_fuse_model_update_ops_ = true;
  return &enable_fuse_model_update_ops_;
}

// optional field enable_gradients_stats_aggregation
bool ConstJobConfigProto::_JobConfigProto_::has_enable_gradients_stats_aggregation() const {
  return has_enable_gradients_stats_aggregation_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_gradients_stats_aggregation() const {
  if (has_enable_gradients_stats_aggregation_) { return enable_gradients_stats_aggregation_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_gradients_stats_aggregation() {
  has_enable_gradients_stats_aggregation_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_gradients_stats_aggregation(const bool& value) {
  enable_gradients_stats_aggregation_ = value;
  has_enable_gradients_stats_aggregation_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_gradients_stats_aggregation() {
  has_enable_gradients_stats_aggregation_ = true;
  return &enable_gradients_stats_aggregation_;
}

// optional field optimizer_placement_optimization_mode
bool ConstJobConfigProto::_JobConfigProto_::has_optimizer_placement_optimization_mode() const {
  return has_optimizer_placement_optimization_mode_;
}
const ::std::string& ConstJobConfigProto::_JobConfigProto_::optimizer_placement_optimization_mode() const {
  if (has_optimizer_placement_optimization_mode_) { return optimizer_placement_optimization_mode_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_optimizer_placement_optimization_mode() {
  has_optimizer_placement_optimization_mode_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_optimizer_placement_optimization_mode(const ::std::string& value) {
  optimizer_placement_optimization_mode_ = value;
  has_optimizer_placement_optimization_mode_ = true;
}
::std::string* ConstJobConfigProto::_JobConfigProto_::mutable_optimizer_placement_optimization_mode() {
  has_optimizer_placement_optimization_mode_ = true;
  return &optimizer_placement_optimization_mode_;
}

// optional field optimizer_placement_optimization_threshold
bool ConstJobConfigProto::_JobConfigProto_::has_optimizer_placement_optimization_threshold() const {
  return has_optimizer_placement_optimization_threshold_;
}
const int64_t& ConstJobConfigProto::_JobConfigProto_::optimizer_placement_optimization_threshold() const {
  if (has_optimizer_placement_optimization_threshold_) { return optimizer_placement_optimization_threshold_; }
  static const int64_t default_static_value =
    int64_t(1024);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_optimizer_placement_optimization_threshold() {
  has_optimizer_placement_optimization_threshold_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_optimizer_placement_optimization_threshold(const int64_t& value) {
  optimizer_placement_optimization_threshold_ = value;
  has_optimizer_placement_optimization_threshold_ = true;
}
int64_t* ConstJobConfigProto::_JobConfigProto_::mutable_optimizer_placement_optimization_threshold() {
  has_optimizer_placement_optimization_threshold_ = true;
  return &optimizer_placement_optimization_threshold_;
}

// optional field qat_config
bool ConstJobConfigProto::_JobConfigProto_::has_qat_config() const {
  return has_qat_config_;
}
const ::oneflow::cfg::QatConfig& ConstJobConfigProto::_JobConfigProto_::qat_config() const {
  if (!qat_config_) {
    static const ::std::shared_ptr<::oneflow::cfg::QatConfig> default_static_value =
      ::std::make_shared<::oneflow::cfg::QatConfig>();
    return *default_static_value;
  }
  return *(qat_config_.get());
}
void ConstJobConfigProto::_JobConfigProto_::clear_qat_config() {
  if (qat_config_) {
    qat_config_->Clear();
  }
  has_qat_config_ = false;
}
::oneflow::cfg::QatConfig* ConstJobConfigProto::_JobConfigProto_::mutable_qat_config() {
  if (!qat_config_) {
    qat_config_ = ::std::make_shared<::oneflow::cfg::QatConfig>();
  }
  has_qat_config_ = true;
  return qat_config_.get();
}

// optional field enable_cudnn
bool ConstJobConfigProto::_JobConfigProto_::has_enable_cudnn() const {
  return has_enable_cudnn_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_cudnn() const {
  if (has_enable_cudnn_) { return enable_cudnn_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_cudnn() {
  has_enable_cudnn_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_cudnn(const bool& value) {
  enable_cudnn_ = value;
  has_enable_cudnn_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_cudnn() {
  has_enable_cudnn_ = true;
  return &enable_cudnn_;
}

// optional field cudnn_buf_limit_mbyte
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_buf_limit_mbyte() const {
  return has_cudnn_buf_limit_mbyte_;
}
const int64_t& ConstJobConfigProto::_JobConfigProto_::cudnn_buf_limit_mbyte() const {
  if (has_cudnn_buf_limit_mbyte_) { return cudnn_buf_limit_mbyte_; }
  static const int64_t default_static_value =
    int64_t(1024);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_buf_limit_mbyte() {
  has_cudnn_buf_limit_mbyte_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_buf_limit_mbyte(const int64_t& value) {
  cudnn_buf_limit_mbyte_ = value;
  has_cudnn_buf_limit_mbyte_ = true;
}
int64_t* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_buf_limit_mbyte() {
  has_cudnn_buf_limit_mbyte_ = true;
  return &cudnn_buf_limit_mbyte_;
}

// optional field cudnn_conv_force_fwd_algo
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_force_fwd_algo() const {
  return has_cudnn_conv_force_fwd_algo_;
}
const int32_t& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_force_fwd_algo() const {
  if (has_cudnn_conv_force_fwd_algo_) { return cudnn_conv_force_fwd_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_force_fwd_algo() {
  has_cudnn_conv_force_fwd_algo_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_force_fwd_algo(const int32_t& value) {
  cudnn_conv_force_fwd_algo_ = value;
  has_cudnn_conv_force_fwd_algo_ = true;
}
int32_t* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_force_fwd_algo() {
  has_cudnn_conv_force_fwd_algo_ = true;
  return &cudnn_conv_force_fwd_algo_;
}

// optional field cudnn_conv_force_bwd_data_algo
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_force_bwd_data_algo() const {
  return has_cudnn_conv_force_bwd_data_algo_;
}
const int32_t& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_force_bwd_data_algo() const {
  if (has_cudnn_conv_force_bwd_data_algo_) { return cudnn_conv_force_bwd_data_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_force_bwd_data_algo() {
  has_cudnn_conv_force_bwd_data_algo_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_force_bwd_data_algo(const int32_t& value) {
  cudnn_conv_force_bwd_data_algo_ = value;
  has_cudnn_conv_force_bwd_data_algo_ = true;
}
int32_t* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_force_bwd_data_algo() {
  has_cudnn_conv_force_bwd_data_algo_ = true;
  return &cudnn_conv_force_bwd_data_algo_;
}

// optional field cudnn_conv_force_bwd_filter_algo
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_force_bwd_filter_algo() const {
  return has_cudnn_conv_force_bwd_filter_algo_;
}
const int32_t& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_force_bwd_filter_algo() const {
  if (has_cudnn_conv_force_bwd_filter_algo_) { return cudnn_conv_force_bwd_filter_algo_; }
  static const int32_t default_static_value = int32_t();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_force_bwd_filter_algo() {
  has_cudnn_conv_force_bwd_filter_algo_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_force_bwd_filter_algo(const int32_t& value) {
  cudnn_conv_force_bwd_filter_algo_ = value;
  has_cudnn_conv_force_bwd_filter_algo_ = true;
}
int32_t* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_force_bwd_filter_algo() {
  has_cudnn_conv_force_bwd_filter_algo_ = true;
  return &cudnn_conv_force_bwd_filter_algo_;
}

// optional field cudnn_conv_heuristic_search_algo
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_heuristic_search_algo() const {
  return has_cudnn_conv_heuristic_search_algo_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_heuristic_search_algo() const {
  if (has_cudnn_conv_heuristic_search_algo_) { return cudnn_conv_heuristic_search_algo_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_heuristic_search_algo() {
  has_cudnn_conv_heuristic_search_algo_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_heuristic_search_algo(const bool& value) {
  cudnn_conv_heuristic_search_algo_ = value;
  has_cudnn_conv_heuristic_search_algo_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_heuristic_search_algo() {
  has_cudnn_conv_heuristic_search_algo_ = true;
  return &cudnn_conv_heuristic_search_algo_;
}

// optional field cudnn_conv_use_deterministic_algo_only
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_use_deterministic_algo_only() const {
  return has_cudnn_conv_use_deterministic_algo_only_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_use_deterministic_algo_only() const {
  if (has_cudnn_conv_use_deterministic_algo_only_) { return cudnn_conv_use_deterministic_algo_only_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_use_deterministic_algo_only() {
  has_cudnn_conv_use_deterministic_algo_only_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_use_deterministic_algo_only(const bool& value) {
  cudnn_conv_use_deterministic_algo_only_ = value;
  has_cudnn_conv_use_deterministic_algo_only_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_use_deterministic_algo_only() {
  has_cudnn_conv_use_deterministic_algo_only_ = true;
  return &cudnn_conv_use_deterministic_algo_only_;
}

// optional field enable_cudnn_fused_normalization_add_relu
bool ConstJobConfigProto::_JobConfigProto_::has_enable_cudnn_fused_normalization_add_relu() const {
  return has_enable_cudnn_fused_normalization_add_relu_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_cudnn_fused_normalization_add_relu() const {
  if (has_enable_cudnn_fused_normalization_add_relu_) { return enable_cudnn_fused_normalization_add_relu_; }
  static const bool default_static_value = bool();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_cudnn_fused_normalization_add_relu() {
  has_enable_cudnn_fused_normalization_add_relu_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_cudnn_fused_normalization_add_relu(const bool& value) {
  enable_cudnn_fused_normalization_add_relu_ = value;
  has_enable_cudnn_fused_normalization_add_relu_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_cudnn_fused_normalization_add_relu() {
  has_enable_cudnn_fused_normalization_add_relu_ = true;
  return &enable_cudnn_fused_normalization_add_relu_;
}

// optional field enable_fuse_add_to_output
bool ConstJobConfigProto::_JobConfigProto_::has_enable_fuse_add_to_output() const {
  return has_enable_fuse_add_to_output_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_fuse_add_to_output() const {
  if (has_enable_fuse_add_to_output_) { return enable_fuse_add_to_output_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_fuse_add_to_output() {
  has_enable_fuse_add_to_output_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_fuse_add_to_output(const bool& value) {
  enable_fuse_add_to_output_ = value;
  has_enable_fuse_add_to_output_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_fuse_add_to_output() {
  has_enable_fuse_add_to_output_ = true;
  return &enable_fuse_add_to_output_;
}

// optional field enable_fuse_cast_scale
bool ConstJobConfigProto::_JobConfigProto_::has_enable_fuse_cast_scale() const {
  return has_enable_fuse_cast_scale_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_fuse_cast_scale() const {
  if (has_enable_fuse_cast_scale_) { return enable_fuse_cast_scale_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_fuse_cast_scale() {
  has_enable_fuse_cast_scale_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_fuse_cast_scale(const bool& value) {
  enable_fuse_cast_scale_ = value;
  has_enable_fuse_cast_scale_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_fuse_cast_scale() {
  has_enable_fuse_cast_scale_ = true;
  return &enable_fuse_cast_scale_;
}

// optional field num_gradient_accumulation_steps
bool ConstJobConfigProto::_JobConfigProto_::has_num_gradient_accumulation_steps() const {
  return has_num_gradient_accumulation_steps_;
}
const int64_t& ConstJobConfigProto::_JobConfigProto_::num_gradient_accumulation_steps() const {
  if (has_num_gradient_accumulation_steps_) { return num_gradient_accumulation_steps_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_num_gradient_accumulation_steps() {
  has_num_gradient_accumulation_steps_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_num_gradient_accumulation_steps(const int64_t& value) {
  num_gradient_accumulation_steps_ = value;
  has_num_gradient_accumulation_steps_ = true;
}
int64_t* ConstJobConfigProto::_JobConfigProto_::mutable_num_gradient_accumulation_steps() {
  has_num_gradient_accumulation_steps_ = true;
  return &num_gradient_accumulation_steps_;
}

// optional field enable_reuse_mem
bool ConstJobConfigProto::_JobConfigProto_::has_enable_reuse_mem() const {
  return has_enable_reuse_mem_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_reuse_mem() const {
  if (has_enable_reuse_mem_) { return enable_reuse_mem_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_reuse_mem() {
  has_enable_reuse_mem_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_reuse_mem(const bool& value) {
  enable_reuse_mem_ = value;
  has_enable_reuse_mem_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_reuse_mem() {
  has_enable_reuse_mem_ = true;
  return &enable_reuse_mem_;
}

// optional field enable_inplace
bool ConstJobConfigProto::_JobConfigProto_::has_enable_inplace() const {
  return has_enable_inplace_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_inplace() const {
  if (has_enable_inplace_) { return enable_inplace_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_inplace() {
  has_enable_inplace_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_inplace(const bool& value) {
  enable_inplace_ = value;
  has_enable_inplace_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_inplace() {
  has_enable_inplace_ = true;
  return &enable_inplace_;
}

// optional field enable_inplace_in_reduce_struct
bool ConstJobConfigProto::_JobConfigProto_::has_enable_inplace_in_reduce_struct() const {
  return has_enable_inplace_in_reduce_struct_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_inplace_in_reduce_struct() const {
  if (has_enable_inplace_in_reduce_struct_) { return enable_inplace_in_reduce_struct_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_inplace_in_reduce_struct() {
  has_enable_inplace_in_reduce_struct_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_inplace_in_reduce_struct(const bool& value) {
  enable_inplace_in_reduce_struct_ = value;
  has_enable_inplace_in_reduce_struct_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_inplace_in_reduce_struct() {
  has_enable_inplace_in_reduce_struct_ = true;
  return &enable_inplace_in_reduce_struct_;
}

// optional field do_parallel_cast_before_widening_type_cast
bool ConstJobConfigProto::_JobConfigProto_::has_do_parallel_cast_before_widening_type_cast() const {
  return has_do_parallel_cast_before_widening_type_cast_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::do_parallel_cast_before_widening_type_cast() const {
  if (has_do_parallel_cast_before_widening_type_cast_) { return do_parallel_cast_before_widening_type_cast_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_do_parallel_cast_before_widening_type_cast() {
  has_do_parallel_cast_before_widening_type_cast_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_do_parallel_cast_before_widening_type_cast(const bool& value) {
  do_parallel_cast_before_widening_type_cast_ = value;
  has_do_parallel_cast_before_widening_type_cast_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_do_parallel_cast_before_widening_type_cast() {
  has_do_parallel_cast_before_widening_type_cast_ = true;
  return &do_parallel_cast_before_widening_type_cast_;
}

// optional field prune_parallel_cast_ops
bool ConstJobConfigProto::_JobConfigProto_::has_prune_parallel_cast_ops() const {
  return has_prune_parallel_cast_ops_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::prune_parallel_cast_ops() const {
  if (has_prune_parallel_cast_ops_) { return prune_parallel_cast_ops_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_prune_parallel_cast_ops() {
  has_prune_parallel_cast_ops_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_prune_parallel_cast_ops(const bool& value) {
  prune_parallel_cast_ops_ = value;
  has_prune_parallel_cast_ops_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_prune_parallel_cast_ops() {
  has_prune_parallel_cast_ops_ = true;
  return &prune_parallel_cast_ops_;
}

// optional field prune_cast_to_static_shape_ops
bool ConstJobConfigProto::_JobConfigProto_::has_prune_cast_to_static_shape_ops() const {
  return has_prune_cast_to_static_shape_ops_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::prune_cast_to_static_shape_ops() const {
  if (has_prune_cast_to_static_shape_ops_) { return prune_cast_to_static_shape_ops_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_prune_cast_to_static_shape_ops() {
  has_prune_cast_to_static_shape_ops_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_prune_cast_to_static_shape_ops(const bool& value) {
  prune_cast_to_static_shape_ops_ = value;
  has_prune_cast_to_static_shape_ops_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_prune_cast_to_static_shape_ops() {
  has_prune_cast_to_static_shape_ops_ = true;
  return &prune_cast_to_static_shape_ops_;
}

// optional field prune_amp_white_identity_ops
bool ConstJobConfigProto::_JobConfigProto_::has_prune_amp_white_identity_ops() const {
  return has_prune_amp_white_identity_ops_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::prune_amp_white_identity_ops() const {
  if (has_prune_amp_white_identity_ops_) { return prune_amp_white_identity_ops_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_prune_amp_white_identity_ops() {
  has_prune_amp_white_identity_ops_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_prune_amp_white_identity_ops(const bool& value) {
  prune_amp_white_identity_ops_ = value;
  has_prune_amp_white_identity_ops_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_prune_amp_white_identity_ops() {
  has_prune_amp_white_identity_ops_ = true;
  return &prune_amp_white_identity_ops_;
}

// optional field cudnn_conv_enable_pseudo_half
bool ConstJobConfigProto::_JobConfigProto_::has_cudnn_conv_enable_pseudo_half() const {
  return has_cudnn_conv_enable_pseudo_half_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::cudnn_conv_enable_pseudo_half() const {
  if (has_cudnn_conv_enable_pseudo_half_) { return cudnn_conv_enable_pseudo_half_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_cudnn_conv_enable_pseudo_half() {
  has_cudnn_conv_enable_pseudo_half_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_cudnn_conv_enable_pseudo_half(const bool& value) {
  cudnn_conv_enable_pseudo_half_ = value;
  has_cudnn_conv_enable_pseudo_half_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_cudnn_conv_enable_pseudo_half() {
  has_cudnn_conv_enable_pseudo_half_ = true;
  return &cudnn_conv_enable_pseudo_half_;
}

// optional field enable_auto_mixed_precision
bool ConstJobConfigProto::_JobConfigProto_::has_enable_auto_mixed_precision() const {
  return has_enable_auto_mixed_precision_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_auto_mixed_precision() const {
  if (has_enable_auto_mixed_precision_) { return enable_auto_mixed_precision_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_auto_mixed_precision() {
  has_enable_auto_mixed_precision_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_auto_mixed_precision(const bool& value) {
  enable_auto_mixed_precision_ = value;
  has_enable_auto_mixed_precision_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_auto_mixed_precision() {
  has_enable_auto_mixed_precision_ = true;
  return &enable_auto_mixed_precision_;
}

// optional field enable_quantization_aware_training
bool ConstJobConfigProto::_JobConfigProto_::has_enable_quantization_aware_training() const {
  return has_enable_quantization_aware_training_;
}
const bool& ConstJobConfigProto::_JobConfigProto_::enable_quantization_aware_training() const {
  if (has_enable_quantization_aware_training_) { return enable_quantization_aware_training_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_enable_quantization_aware_training() {
  has_enable_quantization_aware_training_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_enable_quantization_aware_training(const bool& value) {
  enable_quantization_aware_training_ = value;
  has_enable_quantization_aware_training_ = true;
}
bool* ConstJobConfigProto::_JobConfigProto_::mutable_enable_quantization_aware_training() {
  has_enable_quantization_aware_training_ = true;
  return &enable_quantization_aware_training_;
}

// optional field concurrency_width
bool ConstJobConfigProto::_JobConfigProto_::has_concurrency_width() const {
  return has_concurrency_width_;
}
const int64_t& ConstJobConfigProto::_JobConfigProto_::concurrency_width() const {
  if (has_concurrency_width_) { return concurrency_width_; }
  static const int64_t default_static_value =
    int64_t(128);
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_concurrency_width() {
  has_concurrency_width_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_concurrency_width(const int64_t& value) {
  concurrency_width_ = value;
  has_concurrency_width_ = true;
}
int64_t* ConstJobConfigProto::_JobConfigProto_::mutable_concurrency_width() {
  has_concurrency_width_ = true;
  return &concurrency_width_;
}

::std::size_t ConstJobConfigProto::_JobConfigProto_::flag_name2flag_value_size() const {
  if (!flag_name2flag_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->size();
  }
  return flag_name2flag_value_->size();
}
const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& ConstJobConfigProto::_JobConfigProto_::flag_name2flag_value() const {
  if (!flag_name2flag_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return *(default_static_value.get());
  }
  return *(flag_name2flag_value_.get());
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ * ConstJobConfigProto::_JobConfigProto_::mutable_flag_name2flag_value() {
  if (!flag_name2flag_value_) {
    flag_name2flag_value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>();
  }
  return flag_name2flag_value_.get();
}

const ::oneflow::cfg::AttrValue& ConstJobConfigProto::_JobConfigProto_::flag_name2flag_value(::std::string key) const {
  if (!flag_name2flag_value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->at(key);
  }
  return flag_name2flag_value_->at(key);
}

void ConstJobConfigProto::_JobConfigProto_::clear_flag_name2flag_value() {
  if (!flag_name2flag_value_) {
    flag_name2flag_value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>();
  }
  return flag_name2flag_value_->Clear();
}


// optional field logical_object_id
bool ConstJobConfigProto::_JobConfigProto_::has_logical_object_id() const {
  return has_logical_object_id_;
}
const int64_t& ConstJobConfigProto::_JobConfigProto_::logical_object_id() const {
  if (has_logical_object_id_) { return logical_object_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstJobConfigProto::_JobConfigProto_::clear_logical_object_id() {
  has_logical_object_id_ = false;
}
void ConstJobConfigProto::_JobConfigProto_::set_logical_object_id(const int64_t& value) {
  logical_object_id_ = value;
  has_logical_object_id_ = true;
}
int64_t* ConstJobConfigProto::_JobConfigProto_::mutable_logical_object_id() {
  has_logical_object_id_ = true;
  return &logical_object_id_;
}

// optional field signature
bool ConstJobConfigProto::_JobConfigProto_::has_signature() const {
  return has_signature_;
}
const ::oneflow::cfg::JobSignatureDef& ConstJobConfigProto::_JobConfigProto_::signature() const {
  if (!signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::JobSignatureDef> default_static_value =
      ::std::make_shared<::oneflow::cfg::JobSignatureDef>();
    return *default_static_value;
  }
  return *(signature_.get());
}
void ConstJobConfigProto::_JobConfigProto_::clear_signature() {
  if (signature_) {
    signature_->Clear();
  }
  has_signature_ = false;
}
::oneflow::cfg::JobSignatureDef* ConstJobConfigProto::_JobConfigProto_::mutable_signature() {
  if (!signature_) {
    signature_ = ::std::make_shared<::oneflow::cfg::JobSignatureDef>();
  }
  has_signature_ = true;
  return signature_.get();
}
ConstJobConfigProto::JobTypeCase ConstJobConfigProto::_JobConfigProto_::job_type_case() const {
  return job_type_case_;
}
bool ConstJobConfigProto::_JobConfigProto_::has_job_type() const {
  return job_type_case_ != JOB_TYPE_NOT_SET;
}
void ConstJobConfigProto::_JobConfigProto_::clear_job_type() {
  switch (job_type_case()) {
    case kTrainConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::TrainConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(job_type_.train_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kPredictConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::PredictConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(job_type_.predict_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case JOB_TYPE_NOT_SET: {
      break;
    }
  }
  job_type_case_ = JOB_TYPE_NOT_SET;
}
void ConstJobConfigProto::_JobConfigProto_::job_type_copy_from(const _JobConfigProto_& other) {
  switch (other.job_type_case()) {
    case kTrainConf: {
      mutable_train_conf()->CopyFrom(other.train_conf());
      break;
    }
    case kPredictConf: {
      mutable_predict_conf()->CopyFrom(other.predict_conf());
      break;
    }
    case JOB_TYPE_NOT_SET: {
      clear_job_type();
    }
  }
}
ConstJobConfigProto::DefaultInitializeConfCase ConstJobConfigProto::_JobConfigProto_::default_initialize_conf_case() const {
  return default_initialize_conf_case_;
}
bool ConstJobConfigProto::_JobConfigProto_::has_default_initialize_conf() const {
  return default_initialize_conf_case_ != DEFAULT_INITIALIZE_CONF_NOT_SET;
}
void ConstJobConfigProto::_JobConfigProto_::clear_default_initialize_conf() {
  switch (default_initialize_conf_case()) {
    case kDefaultInitializerConf: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::InitializerConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(default_initialize_conf_.default_initializer_conf_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kDefaultInitializeWithSnapshotPath: {
      {
        using String = ::std::string;
        String* ptr = reinterpret_cast<String*>(&(default_initialize_conf_.default_initialize_with_snapshot_path_)[0]);
        ptr->~String();
      }
      break;
    }
    case DEFAULT_INITIALIZE_CONF_NOT_SET: {
      break;
    }
  }
  default_initialize_conf_case_ = DEFAULT_INITIALIZE_CONF_NOT_SET;
}
void ConstJobConfigProto::_JobConfigProto_::default_initialize_conf_copy_from(const _JobConfigProto_& other) {
  switch (other.default_initialize_conf_case()) {
    case kDefaultInitializerConf: {
      mutable_default_initializer_conf()->CopyFrom(other.default_initializer_conf());
      break;
    }
    case kDefaultInitializeWithSnapshotPath: {
      set_default_initialize_with_snapshot_path(other.default_initialize_with_snapshot_path());
      break;
    }
    case DEFAULT_INITIALIZE_CONF_NOT_SET: {
      clear_default_initialize_conf();
    }
  }
}


int ConstJobConfigProto::_JobConfigProto_::compare(const _JobConfigProto_& other) {
  if (!(has_job_name() == other.has_job_name())) {
    return has_job_name() < other.has_job_name() ? -1 : 1;
  } else if (!(job_name() == other.job_name())) {
    return job_name() < other.job_name() ? -1 : 1;
  }
  if (!(has_default_data_type() == other.has_default_data_type())) {
    return has_default_data_type() < other.has_default_data_type() ? -1 : 1;
  } else if (!(default_data_type() == other.default_data_type())) {
    return default_data_type() < other.default_data_type() ? -1 : 1;
  }
  if (!(has_memory_allocation_algorithm_conf() == other.has_memory_allocation_algorithm_conf())) {
    return has_memory_allocation_algorithm_conf() < other.has_memory_allocation_algorithm_conf() ? -1 : 1;
  } else if (!(memory_allocation_algorithm_conf() == other.memory_allocation_algorithm_conf())) {
    return memory_allocation_algorithm_conf() < other.memory_allocation_algorithm_conf() ? -1 : 1;
  }
  if (!(has_xrt_config() == other.has_xrt_config())) {
    return has_xrt_config() < other.has_xrt_config() ? -1 : 1;
  } else if (!(xrt_config() == other.xrt_config())) {
    return xrt_config() < other.xrt_config() ? -1 : 1;
  }
  if (!(has_indexed_slices_optimizer_conf() == other.has_indexed_slices_optimizer_conf())) {
    return has_indexed_slices_optimizer_conf() < other.has_indexed_slices_optimizer_conf() ? -1 : 1;
  } else if (!(indexed_slices_optimizer_conf() == other.indexed_slices_optimizer_conf())) {
    return indexed_slices_optimizer_conf() < other.indexed_slices_optimizer_conf() ? -1 : 1;
  }
  if (!(has_enable_fuse_model_update_ops() == other.has_enable_fuse_model_update_ops())) {
    return has_enable_fuse_model_update_ops() < other.has_enable_fuse_model_update_ops() ? -1 : 1;
  } else if (!(enable_fuse_model_update_ops() == other.enable_fuse_model_update_ops())) {
    return enable_fuse_model_update_ops() < other.enable_fuse_model_update_ops() ? -1 : 1;
  }
  if (!(has_enable_gradients_stats_aggregation() == other.has_enable_gradients_stats_aggregation())) {
    return has_enable_gradients_stats_aggregation() < other.has_enable_gradients_stats_aggregation() ? -1 : 1;
  } else if (!(enable_gradients_stats_aggregation() == other.enable_gradients_stats_aggregation())) {
    return enable_gradients_stats_aggregation() < other.enable_gradients_stats_aggregation() ? -1 : 1;
  }
  if (!(has_optimizer_placement_optimization_mode() == other.has_optimizer_placement_optimization_mode())) {
    return has_optimizer_placement_optimization_mode() < other.has_optimizer_placement_optimization_mode() ? -1 : 1;
  } else if (!(optimizer_placement_optimization_mode() == other.optimizer_placement_optimization_mode())) {
    return optimizer_placement_optimization_mode() < other.optimizer_placement_optimization_mode() ? -1 : 1;
  }
  if (!(has_optimizer_placement_optimization_threshold() == other.has_optimizer_placement_optimization_threshold())) {
    return has_optimizer_placement_optimization_threshold() < other.has_optimizer_placement_optimization_threshold() ? -1 : 1;
  } else if (!(optimizer_placement_optimization_threshold() == other.optimizer_placement_optimization_threshold())) {
    return optimizer_placement_optimization_threshold() < other.optimizer_placement_optimization_threshold() ? -1 : 1;
  }
  if (!(has_qat_config() == other.has_qat_config())) {
    return has_qat_config() < other.has_qat_config() ? -1 : 1;
  } else if (!(qat_config() == other.qat_config())) {
    return qat_config() < other.qat_config() ? -1 : 1;
  }
  if (!(has_enable_cudnn() == other.has_enable_cudnn())) {
    return has_enable_cudnn() < other.has_enable_cudnn() ? -1 : 1;
  } else if (!(enable_cudnn() == other.enable_cudnn())) {
    return enable_cudnn() < other.enable_cudnn() ? -1 : 1;
  }
  if (!(has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte())) {
    return has_cudnn_buf_limit_mbyte() < other.has_cudnn_buf_limit_mbyte() ? -1 : 1;
  } else if (!(cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte())) {
    return cudnn_buf_limit_mbyte() < other.cudnn_buf_limit_mbyte() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo())) {
    return has_cudnn_conv_force_fwd_algo() < other.has_cudnn_conv_force_fwd_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo())) {
    return cudnn_conv_force_fwd_algo() < other.cudnn_conv_force_fwd_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo())) {
    return has_cudnn_conv_force_bwd_data_algo() < other.has_cudnn_conv_force_bwd_data_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo())) {
    return cudnn_conv_force_bwd_data_algo() < other.cudnn_conv_force_bwd_data_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo())) {
    return has_cudnn_conv_force_bwd_filter_algo() < other.has_cudnn_conv_force_bwd_filter_algo() ? -1 : 1;
  } else if (!(cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo())) {
    return cudnn_conv_force_bwd_filter_algo() < other.cudnn_conv_force_bwd_filter_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo())) {
    return has_cudnn_conv_heuristic_search_algo() < other.has_cudnn_conv_heuristic_search_algo() ? -1 : 1;
  } else if (!(cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo())) {
    return cudnn_conv_heuristic_search_algo() < other.cudnn_conv_heuristic_search_algo() ? -1 : 1;
  }
  if (!(has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only())) {
    return has_cudnn_conv_use_deterministic_algo_only() < other.has_cudnn_conv_use_deterministic_algo_only() ? -1 : 1;
  } else if (!(cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only())) {
    return cudnn_conv_use_deterministic_algo_only() < other.cudnn_conv_use_deterministic_algo_only() ? -1 : 1;
  }
  if (!(has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu())) {
    return has_enable_cudnn_fused_normalization_add_relu() < other.has_enable_cudnn_fused_normalization_add_relu() ? -1 : 1;
  } else if (!(enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu())) {
    return enable_cudnn_fused_normalization_add_relu() < other.enable_cudnn_fused_normalization_add_relu() ? -1 : 1;
  }
  if (!(has_enable_fuse_add_to_output() == other.has_enable_fuse_add_to_output())) {
    return has_enable_fuse_add_to_output() < other.has_enable_fuse_add_to_output() ? -1 : 1;
  } else if (!(enable_fuse_add_to_output() == other.enable_fuse_add_to_output())) {
    return enable_fuse_add_to_output() < other.enable_fuse_add_to_output() ? -1 : 1;
  }
  if (!(has_enable_fuse_cast_scale() == other.has_enable_fuse_cast_scale())) {
    return has_enable_fuse_cast_scale() < other.has_enable_fuse_cast_scale() ? -1 : 1;
  } else if (!(enable_fuse_cast_scale() == other.enable_fuse_cast_scale())) {
    return enable_fuse_cast_scale() < other.enable_fuse_cast_scale() ? -1 : 1;
  }
  if (!(has_num_gradient_accumulation_steps() == other.has_num_gradient_accumulation_steps())) {
    return has_num_gradient_accumulation_steps() < other.has_num_gradient_accumulation_steps() ? -1 : 1;
  } else if (!(num_gradient_accumulation_steps() == other.num_gradient_accumulation_steps())) {
    return num_gradient_accumulation_steps() < other.num_gradient_accumulation_steps() ? -1 : 1;
  }
  if (!(has_enable_reuse_mem() == other.has_enable_reuse_mem())) {
    return has_enable_reuse_mem() < other.has_enable_reuse_mem() ? -1 : 1;
  } else if (!(enable_reuse_mem() == other.enable_reuse_mem())) {
    return enable_reuse_mem() < other.enable_reuse_mem() ? -1 : 1;
  }
  if (!(has_enable_inplace() == other.has_enable_inplace())) {
    return has_enable_inplace() < other.has_enable_inplace() ? -1 : 1;
  } else if (!(enable_inplace() == other.enable_inplace())) {
    return enable_inplace() < other.enable_inplace() ? -1 : 1;
  }
  if (!(has_enable_inplace_in_reduce_struct() == other.has_enable_inplace_in_reduce_struct())) {
    return has_enable_inplace_in_reduce_struct() < other.has_enable_inplace_in_reduce_struct() ? -1 : 1;
  } else if (!(enable_inplace_in_reduce_struct() == other.enable_inplace_in_reduce_struct())) {
    return enable_inplace_in_reduce_struct() < other.enable_inplace_in_reduce_struct() ? -1 : 1;
  }
  if (!(has_do_parallel_cast_before_widening_type_cast() == other.has_do_parallel_cast_before_widening_type_cast())) {
    return has_do_parallel_cast_before_widening_type_cast() < other.has_do_parallel_cast_before_widening_type_cast() ? -1 : 1;
  } else if (!(do_parallel_cast_before_widening_type_cast() == other.do_parallel_cast_before_widening_type_cast())) {
    return do_parallel_cast_before_widening_type_cast() < other.do_parallel_cast_before_widening_type_cast() ? -1 : 1;
  }
  if (!(has_prune_parallel_cast_ops() == other.has_prune_parallel_cast_ops())) {
    return has_prune_parallel_cast_ops() < other.has_prune_parallel_cast_ops() ? -1 : 1;
  } else if (!(prune_parallel_cast_ops() == other.prune_parallel_cast_ops())) {
    return prune_parallel_cast_ops() < other.prune_parallel_cast_ops() ? -1 : 1;
  }
  if (!(has_prune_cast_to_static_shape_ops() == other.has_prune_cast_to_static_shape_ops())) {
    return has_prune_cast_to_static_shape_ops() < other.has_prune_cast_to_static_shape_ops() ? -1 : 1;
  } else if (!(prune_cast_to_static_shape_ops() == other.prune_cast_to_static_shape_ops())) {
    return prune_cast_to_static_shape_ops() < other.prune_cast_to_static_shape_ops() ? -1 : 1;
  }
  if (!(has_prune_amp_white_identity_ops() == other.has_prune_amp_white_identity_ops())) {
    return has_prune_amp_white_identity_ops() < other.has_prune_amp_white_identity_ops() ? -1 : 1;
  } else if (!(prune_amp_white_identity_ops() == other.prune_amp_white_identity_ops())) {
    return prune_amp_white_identity_ops() < other.prune_amp_white_identity_ops() ? -1 : 1;
  }
  if (!(has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half())) {
    return has_cudnn_conv_enable_pseudo_half() < other.has_cudnn_conv_enable_pseudo_half() ? -1 : 1;
  } else if (!(cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half())) {
    return cudnn_conv_enable_pseudo_half() < other.cudnn_conv_enable_pseudo_half() ? -1 : 1;
  }
  if (!(has_enable_auto_mixed_precision() == other.has_enable_auto_mixed_precision())) {
    return has_enable_auto_mixed_precision() < other.has_enable_auto_mixed_precision() ? -1 : 1;
  } else if (!(enable_auto_mixed_precision() == other.enable_auto_mixed_precision())) {
    return enable_auto_mixed_precision() < other.enable_auto_mixed_precision() ? -1 : 1;
  }
  if (!(has_enable_quantization_aware_training() == other.has_enable_quantization_aware_training())) {
    return has_enable_quantization_aware_training() < other.has_enable_quantization_aware_training() ? -1 : 1;
  } else if (!(enable_quantization_aware_training() == other.enable_quantization_aware_training())) {
    return enable_quantization_aware_training() < other.enable_quantization_aware_training() ? -1 : 1;
  }
  if (!(has_concurrency_width() == other.has_concurrency_width())) {
    return has_concurrency_width() < other.has_concurrency_width() ? -1 : 1;
  } else if (!(concurrency_width() == other.concurrency_width())) {
    return concurrency_width() < other.concurrency_width() ? -1 : 1;
  }
  if (!(flag_name2flag_value() == other.flag_name2flag_value())) {
    return flag_name2flag_value() < other.flag_name2flag_value() ? -1 : 1;
  }
  if (!(has_logical_object_id() == other.has_logical_object_id())) {
    return has_logical_object_id() < other.has_logical_object_id() ? -1 : 1;
  } else if (!(logical_object_id() == other.logical_object_id())) {
    return logical_object_id() < other.logical_object_id() ? -1 : 1;
  }
  if (!(has_signature() == other.has_signature())) {
    return has_signature() < other.has_signature() ? -1 : 1;
  } else if (!(signature() == other.signature())) {
    return signature() < other.signature() ? -1 : 1;
  }
  if (!(job_type_case() == other.job_type_case())) {
    return job_type_case() < other.job_type_case() ? -1 : 1;
  }
  switch (job_type_case()) {
    case kTrainConf: {
      if (!(train_conf() == other.train_conf())) {
        return train_conf() < other.train_conf() ? -1 : 1;
      }
      break;
    }
    case kPredictConf: {
      if (!(predict_conf() == other.predict_conf())) {
        return predict_conf() < other.predict_conf() ? -1 : 1;
      }
      break;
    }
    case JOB_TYPE_NOT_SET: {
      break;
    }
  }
  if (!(default_initialize_conf_case() == other.default_initialize_conf_case())) {
    return default_initialize_conf_case() < other.default_initialize_conf_case() ? -1 : 1;
  }
  switch (default_initialize_conf_case()) {
    case kDefaultInitializerConf: {
      if (!(default_initializer_conf() == other.default_initializer_conf())) {
        return default_initializer_conf() < other.default_initializer_conf() ? -1 : 1;
      }
      break;
    }
    case kDefaultInitializeWithSnapshotPath: {
      if (!(default_initialize_with_snapshot_path() == other.default_initialize_with_snapshot_path())) {
        return default_initialize_with_snapshot_path() < other.default_initialize_with_snapshot_path() ? -1 : 1;
      }
      break;
    }
    case DEFAULT_INITIALIZE_CONF_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstJobConfigProto::_JobConfigProto_::operator==(const _JobConfigProto_& other) const {
  return true
    && has_job_name() == other.has_job_name() 
    && job_name() == other.job_name()
    && has_default_data_type() == other.has_default_data_type() 
    && default_data_type() == other.default_data_type()
    && has_memory_allocation_algorithm_conf() == other.has_memory_allocation_algorithm_conf() 
    && memory_allocation_algorithm_conf() == other.memory_allocation_algorithm_conf()
    && has_xrt_config() == other.has_xrt_config() 
    && xrt_config() == other.xrt_config()
    && has_indexed_slices_optimizer_conf() == other.has_indexed_slices_optimizer_conf() 
    && indexed_slices_optimizer_conf() == other.indexed_slices_optimizer_conf()
    && has_enable_fuse_model_update_ops() == other.has_enable_fuse_model_update_ops() 
    && enable_fuse_model_update_ops() == other.enable_fuse_model_update_ops()
    && has_enable_gradients_stats_aggregation() == other.has_enable_gradients_stats_aggregation() 
    && enable_gradients_stats_aggregation() == other.enable_gradients_stats_aggregation()
    && has_optimizer_placement_optimization_mode() == other.has_optimizer_placement_optimization_mode() 
    && optimizer_placement_optimization_mode() == other.optimizer_placement_optimization_mode()
    && has_optimizer_placement_optimization_threshold() == other.has_optimizer_placement_optimization_threshold() 
    && optimizer_placement_optimization_threshold() == other.optimizer_placement_optimization_threshold()
    && has_qat_config() == other.has_qat_config() 
    && qat_config() == other.qat_config()
    && has_enable_cudnn() == other.has_enable_cudnn() 
    && enable_cudnn() == other.enable_cudnn()
    && has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte() 
    && cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte()
    && has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo() 
    && cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo()
    && has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo() 
    && cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo()
    && has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo() 
    && cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo()
    && has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo() 
    && cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo()
    && has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only() 
    && cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only()
    && has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu() 
    && enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu()
    && has_enable_fuse_add_to_output() == other.has_enable_fuse_add_to_output() 
    && enable_fuse_add_to_output() == other.enable_fuse_add_to_output()
    && has_enable_fuse_cast_scale() == other.has_enable_fuse_cast_scale() 
    && enable_fuse_cast_scale() == other.enable_fuse_cast_scale()
    && has_num_gradient_accumulation_steps() == other.has_num_gradient_accumulation_steps() 
    && num_gradient_accumulation_steps() == other.num_gradient_accumulation_steps()
    && has_enable_reuse_mem() == other.has_enable_reuse_mem() 
    && enable_reuse_mem() == other.enable_reuse_mem()
    && has_enable_inplace() == other.has_enable_inplace() 
    && enable_inplace() == other.enable_inplace()
    && has_enable_inplace_in_reduce_struct() == other.has_enable_inplace_in_reduce_struct() 
    && enable_inplace_in_reduce_struct() == other.enable_inplace_in_reduce_struct()
    && has_do_parallel_cast_before_widening_type_cast() == other.has_do_parallel_cast_before_widening_type_cast() 
    && do_parallel_cast_before_widening_type_cast() == other.do_parallel_cast_before_widening_type_cast()
    && has_prune_parallel_cast_ops() == other.has_prune_parallel_cast_ops() 
    && prune_parallel_cast_ops() == other.prune_parallel_cast_ops()
    && has_prune_cast_to_static_shape_ops() == other.has_prune_cast_to_static_shape_ops() 
    && prune_cast_to_static_shape_ops() == other.prune_cast_to_static_shape_ops()
    && has_prune_amp_white_identity_ops() == other.has_prune_amp_white_identity_ops() 
    && prune_amp_white_identity_ops() == other.prune_amp_white_identity_ops()
    && has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half() 
    && cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half()
    && has_enable_auto_mixed_precision() == other.has_enable_auto_mixed_precision() 
    && enable_auto_mixed_precision() == other.enable_auto_mixed_precision()
    && has_enable_quantization_aware_training() == other.has_enable_quantization_aware_training() 
    && enable_quantization_aware_training() == other.enable_quantization_aware_training()
    && has_concurrency_width() == other.has_concurrency_width() 
    && concurrency_width() == other.concurrency_width()
    && flag_name2flag_value() == other.flag_name2flag_value()
    && has_logical_object_id() == other.has_logical_object_id() 
    && logical_object_id() == other.logical_object_id()
    && has_signature() == other.has_signature() 
    && signature() == other.signature()
    && job_type_case() == other.job_type_case()
    && (job_type_case() == kTrainConf ? 
      train_conf() == other.train_conf() : true)
    && (job_type_case() == kPredictConf ? 
      predict_conf() == other.predict_conf() : true)
    && default_initialize_conf_case() == other.default_initialize_conf_case()
    && (default_initialize_conf_case() == kDefaultInitializerConf ? 
      default_initializer_conf() == other.default_initializer_conf() : true)
    && (default_initialize_conf_case() == kDefaultInitializeWithSnapshotPath ? 
      default_initialize_with_snapshot_path() == other.default_initialize_with_snapshot_path() : true)
  ;
}

std::size_t ConstJobConfigProto::_JobConfigProto_::__CalcHash__() const {
  return 0
    ^ (has_job_name() ? std::hash<::std::string>()(job_name()) : 0)
    ^ (has_default_data_type() ? std::hash<::oneflow::cfg::DataType>()(default_data_type()) : 0)
    ^ (has_memory_allocation_algorithm_conf() ? std::hash<::oneflow::cfg::MemoryAllocationAlgorithmConf>()(memory_allocation_algorithm_conf()) : 0)
    ^ (has_xrt_config() ? std::hash<::oneflow::cfg::XrtConfig>()(xrt_config()) : 0)
    ^ (has_indexed_slices_optimizer_conf() ? std::hash<::oneflow::cfg::IndexedSlicesOptimizerConf>()(indexed_slices_optimizer_conf()) : 0)
    ^ (has_enable_fuse_model_update_ops() ? std::hash<bool>()(enable_fuse_model_update_ops()) : 0)
    ^ (has_enable_gradients_stats_aggregation() ? std::hash<bool>()(enable_gradients_stats_aggregation()) : 0)
    ^ (has_optimizer_placement_optimization_mode() ? std::hash<::std::string>()(optimizer_placement_optimization_mode()) : 0)
    ^ (has_optimizer_placement_optimization_threshold() ? std::hash<int64_t>()(optimizer_placement_optimization_threshold()) : 0)
    ^ (has_qat_config() ? std::hash<::oneflow::cfg::QatConfig>()(qat_config()) : 0)
    ^ (has_enable_cudnn() ? std::hash<bool>()(enable_cudnn()) : 0)
    ^ (has_cudnn_buf_limit_mbyte() ? std::hash<int64_t>()(cudnn_buf_limit_mbyte()) : 0)
    ^ (has_cudnn_conv_force_fwd_algo() ? std::hash<int32_t>()(cudnn_conv_force_fwd_algo()) : 0)
    ^ (has_cudnn_conv_force_bwd_data_algo() ? std::hash<int32_t>()(cudnn_conv_force_bwd_data_algo()) : 0)
    ^ (has_cudnn_conv_force_bwd_filter_algo() ? std::hash<int32_t>()(cudnn_conv_force_bwd_filter_algo()) : 0)
    ^ (has_cudnn_conv_heuristic_search_algo() ? std::hash<bool>()(cudnn_conv_heuristic_search_algo()) : 0)
    ^ (has_cudnn_conv_use_deterministic_algo_only() ? std::hash<bool>()(cudnn_conv_use_deterministic_algo_only()) : 0)
    ^ (has_enable_cudnn_fused_normalization_add_relu() ? std::hash<bool>()(enable_cudnn_fused_normalization_add_relu()) : 0)
    ^ (has_enable_fuse_add_to_output() ? std::hash<bool>()(enable_fuse_add_to_output()) : 0)
    ^ (has_enable_fuse_cast_scale() ? std::hash<bool>()(enable_fuse_cast_scale()) : 0)
    ^ (has_num_gradient_accumulation_steps() ? std::hash<int64_t>()(num_gradient_accumulation_steps()) : 0)
    ^ (has_enable_reuse_mem() ? std::hash<bool>()(enable_reuse_mem()) : 0)
    ^ (has_enable_inplace() ? std::hash<bool>()(enable_inplace()) : 0)
    ^ (has_enable_inplace_in_reduce_struct() ? std::hash<bool>()(enable_inplace_in_reduce_struct()) : 0)
    ^ (has_do_parallel_cast_before_widening_type_cast() ? std::hash<bool>()(do_parallel_cast_before_widening_type_cast()) : 0)
    ^ (has_prune_parallel_cast_ops() ? std::hash<bool>()(prune_parallel_cast_ops()) : 0)
    ^ (has_prune_cast_to_static_shape_ops() ? std::hash<bool>()(prune_cast_to_static_shape_ops()) : 0)
    ^ (has_prune_amp_white_identity_ops() ? std::hash<bool>()(prune_amp_white_identity_ops()) : 0)
    ^ (has_cudnn_conv_enable_pseudo_half() ? std::hash<bool>()(cudnn_conv_enable_pseudo_half()) : 0)
    ^ (has_enable_auto_mixed_precision() ? std::hash<bool>()(enable_auto_mixed_precision()) : 0)
    ^ (has_enable_quantization_aware_training() ? std::hash<bool>()(enable_quantization_aware_training()) : 0)
    ^ (has_concurrency_width() ? std::hash<int64_t>()(concurrency_width()) : 0)
    ^ flag_name2flag_value().__CalcHash__()
    ^ (has_logical_object_id() ? std::hash<int64_t>()(logical_object_id()) : 0)
    ^ (has_signature() ? std::hash<::oneflow::cfg::JobSignatureDef>()(signature()) : 0)
    ^ static_cast<std::size_t>(job_type_case())
    ^ (has_train_conf() ? std::hash<::oneflow::cfg::TrainConf>()(train_conf()) : 0)
    ^ (has_predict_conf() ? std::hash<::oneflow::cfg::PredictConf>()(predict_conf()) : 0)
    ^ static_cast<std::size_t>(default_initialize_conf_case())
    ^ (has_default_initializer_conf() ? std::hash<::oneflow::cfg::InitializerConf>()(default_initializer_conf()) : 0)
    ^ (has_default_initialize_with_snapshot_path() ? std::hash<::std::string>()(default_initialize_with_snapshot_path()) : 0)
  ;
}

bool ConstJobConfigProto::_JobConfigProto_::operator<(const _JobConfigProto_& other) const {
  return false
    || !(has_job_name() == other.has_job_name()) ? 
      has_job_name() < other.has_job_name() : false
    || !(job_name() == other.job_name()) ? 
      job_name() < other.job_name() : false
    || !(has_default_data_type() == other.has_default_data_type()) ? 
      has_default_data_type() < other.has_default_data_type() : false
    || !(default_data_type() == other.default_data_type()) ? 
      default_data_type() < other.default_data_type() : false
    || !(has_memory_allocation_algorithm_conf() == other.has_memory_allocation_algorithm_conf()) ? 
      has_memory_allocation_algorithm_conf() < other.has_memory_allocation_algorithm_conf() : false
    || !(memory_allocation_algorithm_conf() == other.memory_allocation_algorithm_conf()) ? 
      memory_allocation_algorithm_conf() < other.memory_allocation_algorithm_conf() : false
    || !(has_xrt_config() == other.has_xrt_config()) ? 
      has_xrt_config() < other.has_xrt_config() : false
    || !(xrt_config() == other.xrt_config()) ? 
      xrt_config() < other.xrt_config() : false
    || !(has_indexed_slices_optimizer_conf() == other.has_indexed_slices_optimizer_conf()) ? 
      has_indexed_slices_optimizer_conf() < other.has_indexed_slices_optimizer_conf() : false
    || !(indexed_slices_optimizer_conf() == other.indexed_slices_optimizer_conf()) ? 
      indexed_slices_optimizer_conf() < other.indexed_slices_optimizer_conf() : false
    || !(has_enable_fuse_model_update_ops() == other.has_enable_fuse_model_update_ops()) ? 
      has_enable_fuse_model_update_ops() < other.has_enable_fuse_model_update_ops() : false
    || !(enable_fuse_model_update_ops() == other.enable_fuse_model_update_ops()) ? 
      enable_fuse_model_update_ops() < other.enable_fuse_model_update_ops() : false
    || !(has_enable_gradients_stats_aggregation() == other.has_enable_gradients_stats_aggregation()) ? 
      has_enable_gradients_stats_aggregation() < other.has_enable_gradients_stats_aggregation() : false
    || !(enable_gradients_stats_aggregation() == other.enable_gradients_stats_aggregation()) ? 
      enable_gradients_stats_aggregation() < other.enable_gradients_stats_aggregation() : false
    || !(has_optimizer_placement_optimization_mode() == other.has_optimizer_placement_optimization_mode()) ? 
      has_optimizer_placement_optimization_mode() < other.has_optimizer_placement_optimization_mode() : false
    || !(optimizer_placement_optimization_mode() == other.optimizer_placement_optimization_mode()) ? 
      optimizer_placement_optimization_mode() < other.optimizer_placement_optimization_mode() : false
    || !(has_optimizer_placement_optimization_threshold() == other.has_optimizer_placement_optimization_threshold()) ? 
      has_optimizer_placement_optimization_threshold() < other.has_optimizer_placement_optimization_threshold() : false
    || !(optimizer_placement_optimization_threshold() == other.optimizer_placement_optimization_threshold()) ? 
      optimizer_placement_optimization_threshold() < other.optimizer_placement_optimization_threshold() : false
    || !(has_qat_config() == other.has_qat_config()) ? 
      has_qat_config() < other.has_qat_config() : false
    || !(qat_config() == other.qat_config()) ? 
      qat_config() < other.qat_config() : false
    || !(has_enable_cudnn() == other.has_enable_cudnn()) ? 
      has_enable_cudnn() < other.has_enable_cudnn() : false
    || !(enable_cudnn() == other.enable_cudnn()) ? 
      enable_cudnn() < other.enable_cudnn() : false
    || !(has_cudnn_buf_limit_mbyte() == other.has_cudnn_buf_limit_mbyte()) ? 
      has_cudnn_buf_limit_mbyte() < other.has_cudnn_buf_limit_mbyte() : false
    || !(cudnn_buf_limit_mbyte() == other.cudnn_buf_limit_mbyte()) ? 
      cudnn_buf_limit_mbyte() < other.cudnn_buf_limit_mbyte() : false
    || !(has_cudnn_conv_force_fwd_algo() == other.has_cudnn_conv_force_fwd_algo()) ? 
      has_cudnn_conv_force_fwd_algo() < other.has_cudnn_conv_force_fwd_algo() : false
    || !(cudnn_conv_force_fwd_algo() == other.cudnn_conv_force_fwd_algo()) ? 
      cudnn_conv_force_fwd_algo() < other.cudnn_conv_force_fwd_algo() : false
    || !(has_cudnn_conv_force_bwd_data_algo() == other.has_cudnn_conv_force_bwd_data_algo()) ? 
      has_cudnn_conv_force_bwd_data_algo() < other.has_cudnn_conv_force_bwd_data_algo() : false
    || !(cudnn_conv_force_bwd_data_algo() == other.cudnn_conv_force_bwd_data_algo()) ? 
      cudnn_conv_force_bwd_data_algo() < other.cudnn_conv_force_bwd_data_algo() : false
    || !(has_cudnn_conv_force_bwd_filter_algo() == other.has_cudnn_conv_force_bwd_filter_algo()) ? 
      has_cudnn_conv_force_bwd_filter_algo() < other.has_cudnn_conv_force_bwd_filter_algo() : false
    || !(cudnn_conv_force_bwd_filter_algo() == other.cudnn_conv_force_bwd_filter_algo()) ? 
      cudnn_conv_force_bwd_filter_algo() < other.cudnn_conv_force_bwd_filter_algo() : false
    || !(has_cudnn_conv_heuristic_search_algo() == other.has_cudnn_conv_heuristic_search_algo()) ? 
      has_cudnn_conv_heuristic_search_algo() < other.has_cudnn_conv_heuristic_search_algo() : false
    || !(cudnn_conv_heuristic_search_algo() == other.cudnn_conv_heuristic_search_algo()) ? 
      cudnn_conv_heuristic_search_algo() < other.cudnn_conv_heuristic_search_algo() : false
    || !(has_cudnn_conv_use_deterministic_algo_only() == other.has_cudnn_conv_use_deterministic_algo_only()) ? 
      has_cudnn_conv_use_deterministic_algo_only() < other.has_cudnn_conv_use_deterministic_algo_only() : false
    || !(cudnn_conv_use_deterministic_algo_only() == other.cudnn_conv_use_deterministic_algo_only()) ? 
      cudnn_conv_use_deterministic_algo_only() < other.cudnn_conv_use_deterministic_algo_only() : false
    || !(has_enable_cudnn_fused_normalization_add_relu() == other.has_enable_cudnn_fused_normalization_add_relu()) ? 
      has_enable_cudnn_fused_normalization_add_relu() < other.has_enable_cudnn_fused_normalization_add_relu() : false
    || !(enable_cudnn_fused_normalization_add_relu() == other.enable_cudnn_fused_normalization_add_relu()) ? 
      enable_cudnn_fused_normalization_add_relu() < other.enable_cudnn_fused_normalization_add_relu() : false
    || !(has_enable_fuse_add_to_output() == other.has_enable_fuse_add_to_output()) ? 
      has_enable_fuse_add_to_output() < other.has_enable_fuse_add_to_output() : false
    || !(enable_fuse_add_to_output() == other.enable_fuse_add_to_output()) ? 
      enable_fuse_add_to_output() < other.enable_fuse_add_to_output() : false
    || !(has_enable_fuse_cast_scale() == other.has_enable_fuse_cast_scale()) ? 
      has_enable_fuse_cast_scale() < other.has_enable_fuse_cast_scale() : false
    || !(enable_fuse_cast_scale() == other.enable_fuse_cast_scale()) ? 
      enable_fuse_cast_scale() < other.enable_fuse_cast_scale() : false
    || !(has_num_gradient_accumulation_steps() == other.has_num_gradient_accumulation_steps()) ? 
      has_num_gradient_accumulation_steps() < other.has_num_gradient_accumulation_steps() : false
    || !(num_gradient_accumulation_steps() == other.num_gradient_accumulation_steps()) ? 
      num_gradient_accumulation_steps() < other.num_gradient_accumulation_steps() : false
    || !(has_enable_reuse_mem() == other.has_enable_reuse_mem()) ? 
      has_enable_reuse_mem() < other.has_enable_reuse_mem() : false
    || !(enable_reuse_mem() == other.enable_reuse_mem()) ? 
      enable_reuse_mem() < other.enable_reuse_mem() : false
    || !(has_enable_inplace() == other.has_enable_inplace()) ? 
      has_enable_inplace() < other.has_enable_inplace() : false
    || !(enable_inplace() == other.enable_inplace()) ? 
      enable_inplace() < other.enable_inplace() : false
    || !(has_enable_inplace_in_reduce_struct() == other.has_enable_inplace_in_reduce_struct()) ? 
      has_enable_inplace_in_reduce_struct() < other.has_enable_inplace_in_reduce_struct() : false
    || !(enable_inplace_in_reduce_struct() == other.enable_inplace_in_reduce_struct()) ? 
      enable_inplace_in_reduce_struct() < other.enable_inplace_in_reduce_struct() : false
    || !(has_do_parallel_cast_before_widening_type_cast() == other.has_do_parallel_cast_before_widening_type_cast()) ? 
      has_do_parallel_cast_before_widening_type_cast() < other.has_do_parallel_cast_before_widening_type_cast() : false
    || !(do_parallel_cast_before_widening_type_cast() == other.do_parallel_cast_before_widening_type_cast()) ? 
      do_parallel_cast_before_widening_type_cast() < other.do_parallel_cast_before_widening_type_cast() : false
    || !(has_prune_parallel_cast_ops() == other.has_prune_parallel_cast_ops()) ? 
      has_prune_parallel_cast_ops() < other.has_prune_parallel_cast_ops() : false
    || !(prune_parallel_cast_ops() == other.prune_parallel_cast_ops()) ? 
      prune_parallel_cast_ops() < other.prune_parallel_cast_ops() : false
    || !(has_prune_cast_to_static_shape_ops() == other.has_prune_cast_to_static_shape_ops()) ? 
      has_prune_cast_to_static_shape_ops() < other.has_prune_cast_to_static_shape_ops() : false
    || !(prune_cast_to_static_shape_ops() == other.prune_cast_to_static_shape_ops()) ? 
      prune_cast_to_static_shape_ops() < other.prune_cast_to_static_shape_ops() : false
    || !(has_prune_amp_white_identity_ops() == other.has_prune_amp_white_identity_ops()) ? 
      has_prune_amp_white_identity_ops() < other.has_prune_amp_white_identity_ops() : false
    || !(prune_amp_white_identity_ops() == other.prune_amp_white_identity_ops()) ? 
      prune_amp_white_identity_ops() < other.prune_amp_white_identity_ops() : false
    || !(has_cudnn_conv_enable_pseudo_half() == other.has_cudnn_conv_enable_pseudo_half()) ? 
      has_cudnn_conv_enable_pseudo_half() < other.has_cudnn_conv_enable_pseudo_half() : false
    || !(cudnn_conv_enable_pseudo_half() == other.cudnn_conv_enable_pseudo_half()) ? 
      cudnn_conv_enable_pseudo_half() < other.cudnn_conv_enable_pseudo_half() : false
    || !(has_enable_auto_mixed_precision() == other.has_enable_auto_mixed_precision()) ? 
      has_enable_auto_mixed_precision() < other.has_enable_auto_mixed_precision() : false
    || !(enable_auto_mixed_precision() == other.enable_auto_mixed_precision()) ? 
      enable_auto_mixed_precision() < other.enable_auto_mixed_precision() : false
    || !(has_enable_quantization_aware_training() == other.has_enable_quantization_aware_training()) ? 
      has_enable_quantization_aware_training() < other.has_enable_quantization_aware_training() : false
    || !(enable_quantization_aware_training() == other.enable_quantization_aware_training()) ? 
      enable_quantization_aware_training() < other.enable_quantization_aware_training() : false
    || !(has_concurrency_width() == other.has_concurrency_width()) ? 
      has_concurrency_width() < other.has_concurrency_width() : false
    || !(concurrency_width() == other.concurrency_width()) ? 
      concurrency_width() < other.concurrency_width() : false
    || !(flag_name2flag_value() == other.flag_name2flag_value()) ? 
      flag_name2flag_value() < other.flag_name2flag_value() : false
    || !(has_logical_object_id() == other.has_logical_object_id()) ? 
      has_logical_object_id() < other.has_logical_object_id() : false
    || !(logical_object_id() == other.logical_object_id()) ? 
      logical_object_id() < other.logical_object_id() : false
    || !(has_signature() == other.has_signature()) ? 
      has_signature() < other.has_signature() : false
    || !(signature() == other.signature()) ? 
      signature() < other.signature() : false
    || !(job_type_case() == other.job_type_case()) ? 
      job_type_case() < other.job_type_case() : false
    || ((job_type_case() == kTrainConf) && 
      !(train_conf() == other.train_conf())) ? 
        train_conf() < other.train_conf() : false
    || ((job_type_case() == kPredictConf) && 
      !(predict_conf() == other.predict_conf())) ? 
        predict_conf() < other.predict_conf() : false
    || !(default_initialize_conf_case() == other.default_initialize_conf_case()) ? 
      default_initialize_conf_case() < other.default_initialize_conf_case() : false
    || ((default_initialize_conf_case() == kDefaultInitializerConf) && 
      !(default_initializer_conf() == other.default_initializer_conf())) ? 
        default_initializer_conf() < other.default_initializer_conf() : false
    || ((default_initialize_conf_case() == kDefaultInitializeWithSnapshotPath) && 
      !(default_initialize_with_snapshot_path() == other.default_initialize_with_snapshot_path())) ? 
        default_initialize_with_snapshot_path() < other.default_initialize_with_snapshot_path() : false
  ;
}

using _JobConfigProto_ =  ConstJobConfigProto::_JobConfigProto_;
ConstJobConfigProto::ConstJobConfigProto(const ::std::shared_ptr<_JobConfigProto_>& data): data_(data) {}
ConstJobConfigProto::ConstJobConfigProto(): data_(::std::make_shared<_JobConfigProto_>()) {}
ConstJobConfigProto::ConstJobConfigProto(const ::oneflow::JobConfigProto& proto_jobconfigproto) {
  BuildFromProto(proto_jobconfigproto);
}
ConstJobConfigProto::ConstJobConfigProto(const ConstJobConfigProto&) = default;
ConstJobConfigProto::ConstJobConfigProto(ConstJobConfigProto&&) noexcept = default;
ConstJobConfigProto::~ConstJobConfigProto() = default;

void ConstJobConfigProto::ToProto(PbMessage* proto_jobconfigproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::JobConfigProto*>(proto_jobconfigproto));
}
  
::std::string ConstJobConfigProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstJobConfigProto::__Empty__() const {
  return !data_;
}

int ConstJobConfigProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"job_name", 1},
    {"train_conf", 3},
    {"predict_conf", 4},
    {"default_data_type", 8},
    {"default_initializer_conf", 10},
    {"default_initialize_with_snapshot_path", 11},
    {"memory_allocation_algorithm_conf", 102},
    {"xrt_config", 103},
    {"indexed_slices_optimizer_conf", 104},
    {"enable_fuse_model_update_ops", 105},
    {"enable_gradients_stats_aggregation", 106},
    {"optimizer_placement_optimization_mode", 107},
    {"optimizer_placement_optimization_threshold", 108},
    {"qat_config", 109},
    {"enable_cudnn", 200},
    {"cudnn_buf_limit_mbyte", 201},
    {"cudnn_conv_force_fwd_algo", 202},
    {"cudnn_conv_force_bwd_data_algo", 203},
    {"cudnn_conv_force_bwd_filter_algo", 204},
    {"cudnn_conv_heuristic_search_algo", 205},
    {"cudnn_conv_use_deterministic_algo_only", 206},
    {"enable_cudnn_fused_normalization_add_relu", 207},
    {"enable_fuse_add_to_output", 208},
    {"enable_fuse_cast_scale", 209},
    {"num_gradient_accumulation_steps", 210},
    {"enable_reuse_mem", 300},
    {"enable_inplace", 301},
    {"enable_inplace_in_reduce_struct", 302},
    {"do_parallel_cast_before_widening_type_cast", 403},
    {"prune_parallel_cast_ops", 509},
    {"prune_cast_to_static_shape_ops", 510},
    {"prune_amp_white_identity_ops", 511},
    {"cudnn_conv_enable_pseudo_half", 600},
    {"enable_auto_mixed_precision", 602},
    {"enable_quantization_aware_training", 603},
    {"concurrency_width", 1000},
    {"flag_name2flag_value", 2000},
    {"logical_object_id", 3000},
    {"signature", 4000},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstJobConfigProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 3:
    case 4:
    case 8:
    case 10:
    case 11:
    case 102:
    case 103:
    case 104:
    case 105:
    case 106:
    case 107:
    case 108:
    case 109:
    case 200:
    case 201:
    case 202:
    case 203:
    case 204:
    case 205:
    case 206:
    case 207:
    case 208:
    case 209:
    case 210:
    case 300:
    case 301:
    case 302:
    case 403:
    case 509:
    case 510:
    case 511:
    case 600:
    case 602:
    case 603:
    case 1000:
    case 2000:
    case 3000:
    case 4000:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstJobConfigProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::TrainConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstTrainConf),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::PredictConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstPredictConf),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    case 10: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::InitializerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInitializerConf),
      };
      return type_indices;
    }
    case 11: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 102: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MemoryAllocationAlgorithmConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMemoryAllocationAlgorithmConf),
      };
      return type_indices;
    }
    case 103: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::XrtConfig),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstXrtConfig),
      };
      return type_indices;
    }
    case 104: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::IndexedSlicesOptimizerConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstIndexedSlicesOptimizerConf),
      };
      return type_indices;
    }
    case 105: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 106: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 107: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 108: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 109: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::QatConfig),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstQatConfig),
      };
      return type_indices;
    }
    case 200: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 201: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 202: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 203: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 204: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 205: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 206: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 207: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 208: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 209: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 210: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 300: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 301: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 302: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 403: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 509: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 510: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 511: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 600: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 602: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 603: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 1000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>)
      };
      return type_indices;
    }
    case 3000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 4000: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobSignatureDef),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobSignatureDef),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstJobConfigProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &job_name();
    case 3: return &train_conf();
    case 4: return &predict_conf();
    case 8: return &default_data_type();
    case 10: return &default_initializer_conf();
    case 11: return &default_initialize_with_snapshot_path();
    case 102: return &memory_allocation_algorithm_conf();
    case 103: return &xrt_config();
    case 104: return &indexed_slices_optimizer_conf();
    case 105: return &enable_fuse_model_update_ops();
    case 106: return &enable_gradients_stats_aggregation();
    case 107: return &optimizer_placement_optimization_mode();
    case 108: return &optimizer_placement_optimization_threshold();
    case 109: return &qat_config();
    case 200: return &enable_cudnn();
    case 201: return &cudnn_buf_limit_mbyte();
    case 202: return &cudnn_conv_force_fwd_algo();
    case 203: return &cudnn_conv_force_bwd_data_algo();
    case 204: return &cudnn_conv_force_bwd_filter_algo();
    case 205: return &cudnn_conv_heuristic_search_algo();
    case 206: return &cudnn_conv_use_deterministic_algo_only();
    case 207: return &enable_cudnn_fused_normalization_add_relu();
    case 208: return &enable_fuse_add_to_output();
    case 209: return &enable_fuse_cast_scale();
    case 210: return &num_gradient_accumulation_steps();
    case 300: return &enable_reuse_mem();
    case 301: return &enable_inplace();
    case 302: return &enable_inplace_in_reduce_struct();
    case 403: return &do_parallel_cast_before_widening_type_cast();
    case 509: return &prune_parallel_cast_ops();
    case 510: return &prune_cast_to_static_shape_ops();
    case 511: return &prune_amp_white_identity_ops();
    case 600: return &cudnn_conv_enable_pseudo_half();
    case 602: return &enable_auto_mixed_precision();
    case 603: return &enable_quantization_aware_training();
    case 1000: return &concurrency_width();
    case 2000: return &flag_name2flag_value();
    case 3000: return &logical_object_id();
    case 4000: return &signature();
    default: return nullptr;
  }
}

// required or optional field job_name
bool ConstJobConfigProto::has_job_name() const {
  return __SharedPtrOrDefault__()->has_job_name();
}
const ::std::string& ConstJobConfigProto::job_name() const {
  return __SharedPtrOrDefault__()->job_name();
}
// used by pybind11 only
 // oneof field job_type: train_conf
bool ConstJobConfigProto::has_train_conf() const {
  return __SharedPtrOrDefault__()->has_train_conf();
}
const ::oneflow::cfg::TrainConf& ConstJobConfigProto::train_conf() const {
  return __SharedPtrOrDefault__()->train_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstTrainConf> ConstJobConfigProto::shared_const_train_conf() const {
  return train_conf().__SharedConst__();
}
 // oneof field job_type: predict_conf
bool ConstJobConfigProto::has_predict_conf() const {
  return __SharedPtrOrDefault__()->has_predict_conf();
}
const ::oneflow::cfg::PredictConf& ConstJobConfigProto::predict_conf() const {
  return __SharedPtrOrDefault__()->predict_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstPredictConf> ConstJobConfigProto::shared_const_predict_conf() const {
  return predict_conf().__SharedConst__();
}
// required or optional field default_data_type
bool ConstJobConfigProto::has_default_data_type() const {
  return __SharedPtrOrDefault__()->has_default_data_type();
}
const ::oneflow::cfg::DataType& ConstJobConfigProto::default_data_type() const {
  return __SharedPtrOrDefault__()->default_data_type();
}
// used by pybind11 only
 // oneof field default_initialize_conf: default_initializer_conf
bool ConstJobConfigProto::has_default_initializer_conf() const {
  return __SharedPtrOrDefault__()->has_default_initializer_conf();
}
const ::oneflow::cfg::InitializerConf& ConstJobConfigProto::default_initializer_conf() const {
  return __SharedPtrOrDefault__()->default_initializer_conf();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInitializerConf> ConstJobConfigProto::shared_const_default_initializer_conf() const {
  return default_initializer_conf().__SharedConst__();
}
 // oneof field default_initialize_conf: default_initialize_with_snapshot_path
bool ConstJobConfigProto::has_default_initialize_with_snapshot_path() const {
  return __SharedPtrOrDefault__()->has_default_initialize_with_snapshot_path();
}
const ::std::string& ConstJobConfigProto::default_initialize_with_snapshot_path() const {
  return __SharedPtrOrDefault__()->default_initialize_with_snapshot_path();
}

// used by pybind11 only
// required or optional field memory_allocation_algorithm_conf
bool ConstJobConfigProto::has_memory_allocation_algorithm_conf() const {
  return __SharedPtrOrDefault__()->has_memory_allocation_algorithm_conf();
}
const ::oneflow::cfg::MemoryAllocationAlgorithmConf& ConstJobConfigProto::memory_allocation_algorithm_conf() const {
  return __SharedPtrOrDefault__()->memory_allocation_algorithm_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMemoryAllocationAlgorithmConf> ConstJobConfigProto::shared_const_memory_allocation_algorithm_conf() const {
  return memory_allocation_algorithm_conf().__SharedConst__();
}
// required or optional field xrt_config
bool ConstJobConfigProto::has_xrt_config() const {
  return __SharedPtrOrDefault__()->has_xrt_config();
}
const ::oneflow::cfg::XrtConfig& ConstJobConfigProto::xrt_config() const {
  return __SharedPtrOrDefault__()->xrt_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstXrtConfig> ConstJobConfigProto::shared_const_xrt_config() const {
  return xrt_config().__SharedConst__();
}
// required or optional field indexed_slices_optimizer_conf
bool ConstJobConfigProto::has_indexed_slices_optimizer_conf() const {
  return __SharedPtrOrDefault__()->has_indexed_slices_optimizer_conf();
}
const ::oneflow::cfg::IndexedSlicesOptimizerConf& ConstJobConfigProto::indexed_slices_optimizer_conf() const {
  return __SharedPtrOrDefault__()->indexed_slices_optimizer_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstIndexedSlicesOptimizerConf> ConstJobConfigProto::shared_const_indexed_slices_optimizer_conf() const {
  return indexed_slices_optimizer_conf().__SharedConst__();
}
// required or optional field enable_fuse_model_update_ops
bool ConstJobConfigProto::has_enable_fuse_model_update_ops() const {
  return __SharedPtrOrDefault__()->has_enable_fuse_model_update_ops();
}
const bool& ConstJobConfigProto::enable_fuse_model_update_ops() const {
  return __SharedPtrOrDefault__()->enable_fuse_model_update_ops();
}
// used by pybind11 only
// required or optional field enable_gradients_stats_aggregation
bool ConstJobConfigProto::has_enable_gradients_stats_aggregation() const {
  return __SharedPtrOrDefault__()->has_enable_gradients_stats_aggregation();
}
const bool& ConstJobConfigProto::enable_gradients_stats_aggregation() const {
  return __SharedPtrOrDefault__()->enable_gradients_stats_aggregation();
}
// used by pybind11 only
// required or optional field optimizer_placement_optimization_mode
bool ConstJobConfigProto::has_optimizer_placement_optimization_mode() const {
  return __SharedPtrOrDefault__()->has_optimizer_placement_optimization_mode();
}
const ::std::string& ConstJobConfigProto::optimizer_placement_optimization_mode() const {
  return __SharedPtrOrDefault__()->optimizer_placement_optimization_mode();
}
// used by pybind11 only
// required or optional field optimizer_placement_optimization_threshold
bool ConstJobConfigProto::has_optimizer_placement_optimization_threshold() const {
  return __SharedPtrOrDefault__()->has_optimizer_placement_optimization_threshold();
}
const int64_t& ConstJobConfigProto::optimizer_placement_optimization_threshold() const {
  return __SharedPtrOrDefault__()->optimizer_placement_optimization_threshold();
}
// used by pybind11 only
// required or optional field qat_config
bool ConstJobConfigProto::has_qat_config() const {
  return __SharedPtrOrDefault__()->has_qat_config();
}
const ::oneflow::cfg::QatConfig& ConstJobConfigProto::qat_config() const {
  return __SharedPtrOrDefault__()->qat_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstQatConfig> ConstJobConfigProto::shared_const_qat_config() const {
  return qat_config().__SharedConst__();
}
// required or optional field enable_cudnn
bool ConstJobConfigProto::has_enable_cudnn() const {
  return __SharedPtrOrDefault__()->has_enable_cudnn();
}
const bool& ConstJobConfigProto::enable_cudnn() const {
  return __SharedPtrOrDefault__()->enable_cudnn();
}
// used by pybind11 only
// required or optional field cudnn_buf_limit_mbyte
bool ConstJobConfigProto::has_cudnn_buf_limit_mbyte() const {
  return __SharedPtrOrDefault__()->has_cudnn_buf_limit_mbyte();
}
const int64_t& ConstJobConfigProto::cudnn_buf_limit_mbyte() const {
  return __SharedPtrOrDefault__()->cudnn_buf_limit_mbyte();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_fwd_algo
bool ConstJobConfigProto::has_cudnn_conv_force_fwd_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_fwd_algo();
}
const int32_t& ConstJobConfigProto::cudnn_conv_force_fwd_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_fwd_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_bwd_data_algo
bool ConstJobConfigProto::has_cudnn_conv_force_bwd_data_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_bwd_data_algo();
}
const int32_t& ConstJobConfigProto::cudnn_conv_force_bwd_data_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_bwd_data_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_force_bwd_filter_algo
bool ConstJobConfigProto::has_cudnn_conv_force_bwd_filter_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_force_bwd_filter_algo();
}
const int32_t& ConstJobConfigProto::cudnn_conv_force_bwd_filter_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_force_bwd_filter_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_heuristic_search_algo
bool ConstJobConfigProto::has_cudnn_conv_heuristic_search_algo() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_heuristic_search_algo();
}
const bool& ConstJobConfigProto::cudnn_conv_heuristic_search_algo() const {
  return __SharedPtrOrDefault__()->cudnn_conv_heuristic_search_algo();
}
// used by pybind11 only
// required or optional field cudnn_conv_use_deterministic_algo_only
bool ConstJobConfigProto::has_cudnn_conv_use_deterministic_algo_only() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_use_deterministic_algo_only();
}
const bool& ConstJobConfigProto::cudnn_conv_use_deterministic_algo_only() const {
  return __SharedPtrOrDefault__()->cudnn_conv_use_deterministic_algo_only();
}
// used by pybind11 only
// required or optional field enable_cudnn_fused_normalization_add_relu
bool ConstJobConfigProto::has_enable_cudnn_fused_normalization_add_relu() const {
  return __SharedPtrOrDefault__()->has_enable_cudnn_fused_normalization_add_relu();
}
const bool& ConstJobConfigProto::enable_cudnn_fused_normalization_add_relu() const {
  return __SharedPtrOrDefault__()->enable_cudnn_fused_normalization_add_relu();
}
// used by pybind11 only
// required or optional field enable_fuse_add_to_output
bool ConstJobConfigProto::has_enable_fuse_add_to_output() const {
  return __SharedPtrOrDefault__()->has_enable_fuse_add_to_output();
}
const bool& ConstJobConfigProto::enable_fuse_add_to_output() const {
  return __SharedPtrOrDefault__()->enable_fuse_add_to_output();
}
// used by pybind11 only
// required or optional field enable_fuse_cast_scale
bool ConstJobConfigProto::has_enable_fuse_cast_scale() const {
  return __SharedPtrOrDefault__()->has_enable_fuse_cast_scale();
}
const bool& ConstJobConfigProto::enable_fuse_cast_scale() const {
  return __SharedPtrOrDefault__()->enable_fuse_cast_scale();
}
// used by pybind11 only
// required or optional field num_gradient_accumulation_steps
bool ConstJobConfigProto::has_num_gradient_accumulation_steps() const {
  return __SharedPtrOrDefault__()->has_num_gradient_accumulation_steps();
}
const int64_t& ConstJobConfigProto::num_gradient_accumulation_steps() const {
  return __SharedPtrOrDefault__()->num_gradient_accumulation_steps();
}
// used by pybind11 only
// required or optional field enable_reuse_mem
bool ConstJobConfigProto::has_enable_reuse_mem() const {
  return __SharedPtrOrDefault__()->has_enable_reuse_mem();
}
const bool& ConstJobConfigProto::enable_reuse_mem() const {
  return __SharedPtrOrDefault__()->enable_reuse_mem();
}
// used by pybind11 only
// required or optional field enable_inplace
bool ConstJobConfigProto::has_enable_inplace() const {
  return __SharedPtrOrDefault__()->has_enable_inplace();
}
const bool& ConstJobConfigProto::enable_inplace() const {
  return __SharedPtrOrDefault__()->enable_inplace();
}
// used by pybind11 only
// required or optional field enable_inplace_in_reduce_struct
bool ConstJobConfigProto::has_enable_inplace_in_reduce_struct() const {
  return __SharedPtrOrDefault__()->has_enable_inplace_in_reduce_struct();
}
const bool& ConstJobConfigProto::enable_inplace_in_reduce_struct() const {
  return __SharedPtrOrDefault__()->enable_inplace_in_reduce_struct();
}
// used by pybind11 only
// required or optional field do_parallel_cast_before_widening_type_cast
bool ConstJobConfigProto::has_do_parallel_cast_before_widening_type_cast() const {
  return __SharedPtrOrDefault__()->has_do_parallel_cast_before_widening_type_cast();
}
const bool& ConstJobConfigProto::do_parallel_cast_before_widening_type_cast() const {
  return __SharedPtrOrDefault__()->do_parallel_cast_before_widening_type_cast();
}
// used by pybind11 only
// required or optional field prune_parallel_cast_ops
bool ConstJobConfigProto::has_prune_parallel_cast_ops() const {
  return __SharedPtrOrDefault__()->has_prune_parallel_cast_ops();
}
const bool& ConstJobConfigProto::prune_parallel_cast_ops() const {
  return __SharedPtrOrDefault__()->prune_parallel_cast_ops();
}
// used by pybind11 only
// required or optional field prune_cast_to_static_shape_ops
bool ConstJobConfigProto::has_prune_cast_to_static_shape_ops() const {
  return __SharedPtrOrDefault__()->has_prune_cast_to_static_shape_ops();
}
const bool& ConstJobConfigProto::prune_cast_to_static_shape_ops() const {
  return __SharedPtrOrDefault__()->prune_cast_to_static_shape_ops();
}
// used by pybind11 only
// required or optional field prune_amp_white_identity_ops
bool ConstJobConfigProto::has_prune_amp_white_identity_ops() const {
  return __SharedPtrOrDefault__()->has_prune_amp_white_identity_ops();
}
const bool& ConstJobConfigProto::prune_amp_white_identity_ops() const {
  return __SharedPtrOrDefault__()->prune_amp_white_identity_ops();
}
// used by pybind11 only
// required or optional field cudnn_conv_enable_pseudo_half
bool ConstJobConfigProto::has_cudnn_conv_enable_pseudo_half() const {
  return __SharedPtrOrDefault__()->has_cudnn_conv_enable_pseudo_half();
}
const bool& ConstJobConfigProto::cudnn_conv_enable_pseudo_half() const {
  return __SharedPtrOrDefault__()->cudnn_conv_enable_pseudo_half();
}
// used by pybind11 only
// required or optional field enable_auto_mixed_precision
bool ConstJobConfigProto::has_enable_auto_mixed_precision() const {
  return __SharedPtrOrDefault__()->has_enable_auto_mixed_precision();
}
const bool& ConstJobConfigProto::enable_auto_mixed_precision() const {
  return __SharedPtrOrDefault__()->enable_auto_mixed_precision();
}
// used by pybind11 only
// required or optional field enable_quantization_aware_training
bool ConstJobConfigProto::has_enable_quantization_aware_training() const {
  return __SharedPtrOrDefault__()->has_enable_quantization_aware_training();
}
const bool& ConstJobConfigProto::enable_quantization_aware_training() const {
  return __SharedPtrOrDefault__()->enable_quantization_aware_training();
}
// used by pybind11 only
// required or optional field concurrency_width
bool ConstJobConfigProto::has_concurrency_width() const {
  return __SharedPtrOrDefault__()->has_concurrency_width();
}
const int64_t& ConstJobConfigProto::concurrency_width() const {
  return __SharedPtrOrDefault__()->concurrency_width();
}
// used by pybind11 only
// map field flag_name2flag_value
::std::size_t ConstJobConfigProto::flag_name2flag_value_size() const {
  return __SharedPtrOrDefault__()->flag_name2flag_value_size();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& ConstJobConfigProto::flag_name2flag_value() const {
  return __SharedPtrOrDefault__()->flag_name2flag_value();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> ConstJobConfigProto::shared_const_flag_name2flag_value() const {
  return flag_name2flag_value().__SharedConst__();
}
// required or optional field logical_object_id
bool ConstJobConfigProto::has_logical_object_id() const {
  return __SharedPtrOrDefault__()->has_logical_object_id();
}
const int64_t& ConstJobConfigProto::logical_object_id() const {
  return __SharedPtrOrDefault__()->logical_object_id();
}
// used by pybind11 only
// required or optional field signature
bool ConstJobConfigProto::has_signature() const {
  return __SharedPtrOrDefault__()->has_signature();
}
const ::oneflow::cfg::JobSignatureDef& ConstJobConfigProto::signature() const {
  return __SharedPtrOrDefault__()->signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobSignatureDef> ConstJobConfigProto::shared_const_signature() const {
  return signature().__SharedConst__();
}
ConstJobConfigProto::JobTypeCase ConstJobConfigProto::job_type_case() const {
  return __SharedPtrOrDefault__()->job_type_case();
}

bool ConstJobConfigProto::has_job_type() const {
  return __SharedPtrOrDefault__()->has_job_type();
}
ConstJobConfigProto::DefaultInitializeConfCase ConstJobConfigProto::default_initialize_conf_case() const {
  return __SharedPtrOrDefault__()->default_initialize_conf_case();
}

bool ConstJobConfigProto::has_default_initialize_conf() const {
  return __SharedPtrOrDefault__()->has_default_initialize_conf();
}

::std::shared_ptr<ConstJobConfigProto> ConstJobConfigProto::__SharedConst__() const {
  return ::std::make_shared<ConstJobConfigProto>(data_);
}
int64_t ConstJobConfigProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstJobConfigProto::operator==(const ConstJobConfigProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstJobConfigProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstJobConfigProto::operator<(const ConstJobConfigProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_JobConfigProto_>& ConstJobConfigProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_JobConfigProto_> default_ptr = std::make_shared<_JobConfigProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_JobConfigProto_>& ConstJobConfigProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _JobConfigProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstJobConfigProto
void ConstJobConfigProto::BuildFromProto(const PbMessage& proto_jobconfigproto) {
  data_ = ::std::make_shared<_JobConfigProto_>(dynamic_cast<const ::oneflow::JobConfigProto&>(proto_jobconfigproto));
}

JobConfigProto::JobConfigProto(const ::std::shared_ptr<_JobConfigProto_>& data)
  : ConstJobConfigProto(data) {}
JobConfigProto::JobConfigProto(const JobConfigProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<JobConfigProto> resize
JobConfigProto::JobConfigProto(JobConfigProto&&) noexcept = default; 
JobConfigProto::JobConfigProto(const ::oneflow::JobConfigProto& proto_jobconfigproto) {
  InitFromProto(proto_jobconfigproto);
}
JobConfigProto::JobConfigProto() = default;

JobConfigProto::~JobConfigProto() = default;

void JobConfigProto::InitFromProto(const PbMessage& proto_jobconfigproto) {
  BuildFromProto(proto_jobconfigproto);
}
  
void* JobConfigProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_job_name();
    case 3: return mutable_train_conf();
    case 4: return mutable_predict_conf();
    case 8: return mutable_default_data_type();
    case 10: return mutable_default_initializer_conf();
    case 11: return mutable_default_initialize_with_snapshot_path();
    case 102: return mutable_memory_allocation_algorithm_conf();
    case 103: return mutable_xrt_config();
    case 104: return mutable_indexed_slices_optimizer_conf();
    case 105: return mutable_enable_fuse_model_update_ops();
    case 106: return mutable_enable_gradients_stats_aggregation();
    case 107: return mutable_optimizer_placement_optimization_mode();
    case 108: return mutable_optimizer_placement_optimization_threshold();
    case 109: return mutable_qat_config();
    case 200: return mutable_enable_cudnn();
    case 201: return mutable_cudnn_buf_limit_mbyte();
    case 202: return mutable_cudnn_conv_force_fwd_algo();
    case 203: return mutable_cudnn_conv_force_bwd_data_algo();
    case 204: return mutable_cudnn_conv_force_bwd_filter_algo();
    case 205: return mutable_cudnn_conv_heuristic_search_algo();
    case 206: return mutable_cudnn_conv_use_deterministic_algo_only();
    case 207: return mutable_enable_cudnn_fused_normalization_add_relu();
    case 208: return mutable_enable_fuse_add_to_output();
    case 209: return mutable_enable_fuse_cast_scale();
    case 210: return mutable_num_gradient_accumulation_steps();
    case 300: return mutable_enable_reuse_mem();
    case 301: return mutable_enable_inplace();
    case 302: return mutable_enable_inplace_in_reduce_struct();
    case 403: return mutable_do_parallel_cast_before_widening_type_cast();
    case 509: return mutable_prune_parallel_cast_ops();
    case 510: return mutable_prune_cast_to_static_shape_ops();
    case 511: return mutable_prune_amp_white_identity_ops();
    case 600: return mutable_cudnn_conv_enable_pseudo_half();
    case 602: return mutable_enable_auto_mixed_precision();
    case 603: return mutable_enable_quantization_aware_training();
    case 1000: return mutable_concurrency_width();
    case 2000: return mutable_flag_name2flag_value();
    case 3000: return mutable_logical_object_id();
    case 4000: return mutable_signature();
    default: return nullptr;
  }
}

bool JobConfigProto::operator==(const JobConfigProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t JobConfigProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool JobConfigProto::operator<(const JobConfigProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void JobConfigProto::Clear() {
  if (data_) { data_.reset(); }
}
void JobConfigProto::CopyFrom(const JobConfigProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
JobConfigProto& JobConfigProto::operator=(const JobConfigProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field job_name
void JobConfigProto::clear_job_name() {
  return __SharedPtr__()->clear_job_name();
}
void JobConfigProto::set_job_name(const ::std::string& value) {
  return __SharedPtr__()->set_job_name(value);
}
::std::string* JobConfigProto::mutable_job_name() {
  return  __SharedPtr__()->mutable_job_name();
}
void JobConfigProto::clear_train_conf() {
  return __SharedPtr__()->clear_train_conf();
}
::oneflow::cfg::TrainConf* JobConfigProto::mutable_train_conf() {
  return __SharedPtr__()->mutable_train_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::TrainConf> JobConfigProto::shared_mutable_train_conf() {
  return mutable_train_conf()->__SharedMutable__();
}
void JobConfigProto::clear_predict_conf() {
  return __SharedPtr__()->clear_predict_conf();
}
::oneflow::cfg::PredictConf* JobConfigProto::mutable_predict_conf() {
  return __SharedPtr__()->mutable_predict_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::PredictConf> JobConfigProto::shared_mutable_predict_conf() {
  return mutable_predict_conf()->__SharedMutable__();
}
// required or optional field default_data_type
void JobConfigProto::clear_default_data_type() {
  return __SharedPtr__()->clear_default_data_type();
}
void JobConfigProto::set_default_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_default_data_type(value);
}
::oneflow::cfg::DataType* JobConfigProto::mutable_default_data_type() {
  return  __SharedPtr__()->mutable_default_data_type();
}
void JobConfigProto::clear_default_initializer_conf() {
  return __SharedPtr__()->clear_default_initializer_conf();
}
::oneflow::cfg::InitializerConf* JobConfigProto::mutable_default_initializer_conf() {
  return __SharedPtr__()->mutable_default_initializer_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::InitializerConf> JobConfigProto::shared_mutable_default_initializer_conf() {
  return mutable_default_initializer_conf()->__SharedMutable__();
}
void JobConfigProto::clear_default_initialize_with_snapshot_path() {
  return __SharedPtr__()->clear_default_initialize_with_snapshot_path();
}
void JobConfigProto::set_default_initialize_with_snapshot_path(const ::std::string& value) {
  return __SharedPtr__()->set_default_initialize_with_snapshot_path(value);
}
::std::string* JobConfigProto::mutable_default_initialize_with_snapshot_path() {
  return  __SharedPtr__()->mutable_default_initialize_with_snapshot_path();
}
// required or optional field memory_allocation_algorithm_conf
void JobConfigProto::clear_memory_allocation_algorithm_conf() {
  return __SharedPtr__()->clear_memory_allocation_algorithm_conf();
}
::oneflow::cfg::MemoryAllocationAlgorithmConf* JobConfigProto::mutable_memory_allocation_algorithm_conf() {
  return __SharedPtr__()->mutable_memory_allocation_algorithm_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MemoryAllocationAlgorithmConf> JobConfigProto::shared_mutable_memory_allocation_algorithm_conf() {
  return mutable_memory_allocation_algorithm_conf()->__SharedMutable__();
}
// required or optional field xrt_config
void JobConfigProto::clear_xrt_config() {
  return __SharedPtr__()->clear_xrt_config();
}
::oneflow::cfg::XrtConfig* JobConfigProto::mutable_xrt_config() {
  return __SharedPtr__()->mutable_xrt_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::XrtConfig> JobConfigProto::shared_mutable_xrt_config() {
  return mutable_xrt_config()->__SharedMutable__();
}
// required or optional field indexed_slices_optimizer_conf
void JobConfigProto::clear_indexed_slices_optimizer_conf() {
  return __SharedPtr__()->clear_indexed_slices_optimizer_conf();
}
::oneflow::cfg::IndexedSlicesOptimizerConf* JobConfigProto::mutable_indexed_slices_optimizer_conf() {
  return __SharedPtr__()->mutable_indexed_slices_optimizer_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::IndexedSlicesOptimizerConf> JobConfigProto::shared_mutable_indexed_slices_optimizer_conf() {
  return mutable_indexed_slices_optimizer_conf()->__SharedMutable__();
}
// required or optional field enable_fuse_model_update_ops
void JobConfigProto::clear_enable_fuse_model_update_ops() {
  return __SharedPtr__()->clear_enable_fuse_model_update_ops();
}
void JobConfigProto::set_enable_fuse_model_update_ops(const bool& value) {
  return __SharedPtr__()->set_enable_fuse_model_update_ops(value);
}
bool* JobConfigProto::mutable_enable_fuse_model_update_ops() {
  return  __SharedPtr__()->mutable_enable_fuse_model_update_ops();
}
// required or optional field enable_gradients_stats_aggregation
void JobConfigProto::clear_enable_gradients_stats_aggregation() {
  return __SharedPtr__()->clear_enable_gradients_stats_aggregation();
}
void JobConfigProto::set_enable_gradients_stats_aggregation(const bool& value) {
  return __SharedPtr__()->set_enable_gradients_stats_aggregation(value);
}
bool* JobConfigProto::mutable_enable_gradients_stats_aggregation() {
  return  __SharedPtr__()->mutable_enable_gradients_stats_aggregation();
}
// required or optional field optimizer_placement_optimization_mode
void JobConfigProto::clear_optimizer_placement_optimization_mode() {
  return __SharedPtr__()->clear_optimizer_placement_optimization_mode();
}
void JobConfigProto::set_optimizer_placement_optimization_mode(const ::std::string& value) {
  return __SharedPtr__()->set_optimizer_placement_optimization_mode(value);
}
::std::string* JobConfigProto::mutable_optimizer_placement_optimization_mode() {
  return  __SharedPtr__()->mutable_optimizer_placement_optimization_mode();
}
// required or optional field optimizer_placement_optimization_threshold
void JobConfigProto::clear_optimizer_placement_optimization_threshold() {
  return __SharedPtr__()->clear_optimizer_placement_optimization_threshold();
}
void JobConfigProto::set_optimizer_placement_optimization_threshold(const int64_t& value) {
  return __SharedPtr__()->set_optimizer_placement_optimization_threshold(value);
}
int64_t* JobConfigProto::mutable_optimizer_placement_optimization_threshold() {
  return  __SharedPtr__()->mutable_optimizer_placement_optimization_threshold();
}
// required or optional field qat_config
void JobConfigProto::clear_qat_config() {
  return __SharedPtr__()->clear_qat_config();
}
::oneflow::cfg::QatConfig* JobConfigProto::mutable_qat_config() {
  return __SharedPtr__()->mutable_qat_config();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::QatConfig> JobConfigProto::shared_mutable_qat_config() {
  return mutable_qat_config()->__SharedMutable__();
}
// required or optional field enable_cudnn
void JobConfigProto::clear_enable_cudnn() {
  return __SharedPtr__()->clear_enable_cudnn();
}
void JobConfigProto::set_enable_cudnn(const bool& value) {
  return __SharedPtr__()->set_enable_cudnn(value);
}
bool* JobConfigProto::mutable_enable_cudnn() {
  return  __SharedPtr__()->mutable_enable_cudnn();
}
// required or optional field cudnn_buf_limit_mbyte
void JobConfigProto::clear_cudnn_buf_limit_mbyte() {
  return __SharedPtr__()->clear_cudnn_buf_limit_mbyte();
}
void JobConfigProto::set_cudnn_buf_limit_mbyte(const int64_t& value) {
  return __SharedPtr__()->set_cudnn_buf_limit_mbyte(value);
}
int64_t* JobConfigProto::mutable_cudnn_buf_limit_mbyte() {
  return  __SharedPtr__()->mutable_cudnn_buf_limit_mbyte();
}
// required or optional field cudnn_conv_force_fwd_algo
void JobConfigProto::clear_cudnn_conv_force_fwd_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_fwd_algo();
}
void JobConfigProto::set_cudnn_conv_force_fwd_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_fwd_algo(value);
}
int32_t* JobConfigProto::mutable_cudnn_conv_force_fwd_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_fwd_algo();
}
// required or optional field cudnn_conv_force_bwd_data_algo
void JobConfigProto::clear_cudnn_conv_force_bwd_data_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_bwd_data_algo();
}
void JobConfigProto::set_cudnn_conv_force_bwd_data_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_bwd_data_algo(value);
}
int32_t* JobConfigProto::mutable_cudnn_conv_force_bwd_data_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_bwd_data_algo();
}
// required or optional field cudnn_conv_force_bwd_filter_algo
void JobConfigProto::clear_cudnn_conv_force_bwd_filter_algo() {
  return __SharedPtr__()->clear_cudnn_conv_force_bwd_filter_algo();
}
void JobConfigProto::set_cudnn_conv_force_bwd_filter_algo(const int32_t& value) {
  return __SharedPtr__()->set_cudnn_conv_force_bwd_filter_algo(value);
}
int32_t* JobConfigProto::mutable_cudnn_conv_force_bwd_filter_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_force_bwd_filter_algo();
}
// required or optional field cudnn_conv_heuristic_search_algo
void JobConfigProto::clear_cudnn_conv_heuristic_search_algo() {
  return __SharedPtr__()->clear_cudnn_conv_heuristic_search_algo();
}
void JobConfigProto::set_cudnn_conv_heuristic_search_algo(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_heuristic_search_algo(value);
}
bool* JobConfigProto::mutable_cudnn_conv_heuristic_search_algo() {
  return  __SharedPtr__()->mutable_cudnn_conv_heuristic_search_algo();
}
// required or optional field cudnn_conv_use_deterministic_algo_only
void JobConfigProto::clear_cudnn_conv_use_deterministic_algo_only() {
  return __SharedPtr__()->clear_cudnn_conv_use_deterministic_algo_only();
}
void JobConfigProto::set_cudnn_conv_use_deterministic_algo_only(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_use_deterministic_algo_only(value);
}
bool* JobConfigProto::mutable_cudnn_conv_use_deterministic_algo_only() {
  return  __SharedPtr__()->mutable_cudnn_conv_use_deterministic_algo_only();
}
// required or optional field enable_cudnn_fused_normalization_add_relu
void JobConfigProto::clear_enable_cudnn_fused_normalization_add_relu() {
  return __SharedPtr__()->clear_enable_cudnn_fused_normalization_add_relu();
}
void JobConfigProto::set_enable_cudnn_fused_normalization_add_relu(const bool& value) {
  return __SharedPtr__()->set_enable_cudnn_fused_normalization_add_relu(value);
}
bool* JobConfigProto::mutable_enable_cudnn_fused_normalization_add_relu() {
  return  __SharedPtr__()->mutable_enable_cudnn_fused_normalization_add_relu();
}
// required or optional field enable_fuse_add_to_output
void JobConfigProto::clear_enable_fuse_add_to_output() {
  return __SharedPtr__()->clear_enable_fuse_add_to_output();
}
void JobConfigProto::set_enable_fuse_add_to_output(const bool& value) {
  return __SharedPtr__()->set_enable_fuse_add_to_output(value);
}
bool* JobConfigProto::mutable_enable_fuse_add_to_output() {
  return  __SharedPtr__()->mutable_enable_fuse_add_to_output();
}
// required or optional field enable_fuse_cast_scale
void JobConfigProto::clear_enable_fuse_cast_scale() {
  return __SharedPtr__()->clear_enable_fuse_cast_scale();
}
void JobConfigProto::set_enable_fuse_cast_scale(const bool& value) {
  return __SharedPtr__()->set_enable_fuse_cast_scale(value);
}
bool* JobConfigProto::mutable_enable_fuse_cast_scale() {
  return  __SharedPtr__()->mutable_enable_fuse_cast_scale();
}
// required or optional field num_gradient_accumulation_steps
void JobConfigProto::clear_num_gradient_accumulation_steps() {
  return __SharedPtr__()->clear_num_gradient_accumulation_steps();
}
void JobConfigProto::set_num_gradient_accumulation_steps(const int64_t& value) {
  return __SharedPtr__()->set_num_gradient_accumulation_steps(value);
}
int64_t* JobConfigProto::mutable_num_gradient_accumulation_steps() {
  return  __SharedPtr__()->mutable_num_gradient_accumulation_steps();
}
// required or optional field enable_reuse_mem
void JobConfigProto::clear_enable_reuse_mem() {
  return __SharedPtr__()->clear_enable_reuse_mem();
}
void JobConfigProto::set_enable_reuse_mem(const bool& value) {
  return __SharedPtr__()->set_enable_reuse_mem(value);
}
bool* JobConfigProto::mutable_enable_reuse_mem() {
  return  __SharedPtr__()->mutable_enable_reuse_mem();
}
// required or optional field enable_inplace
void JobConfigProto::clear_enable_inplace() {
  return __SharedPtr__()->clear_enable_inplace();
}
void JobConfigProto::set_enable_inplace(const bool& value) {
  return __SharedPtr__()->set_enable_inplace(value);
}
bool* JobConfigProto::mutable_enable_inplace() {
  return  __SharedPtr__()->mutable_enable_inplace();
}
// required or optional field enable_inplace_in_reduce_struct
void JobConfigProto::clear_enable_inplace_in_reduce_struct() {
  return __SharedPtr__()->clear_enable_inplace_in_reduce_struct();
}
void JobConfigProto::set_enable_inplace_in_reduce_struct(const bool& value) {
  return __SharedPtr__()->set_enable_inplace_in_reduce_struct(value);
}
bool* JobConfigProto::mutable_enable_inplace_in_reduce_struct() {
  return  __SharedPtr__()->mutable_enable_inplace_in_reduce_struct();
}
// required or optional field do_parallel_cast_before_widening_type_cast
void JobConfigProto::clear_do_parallel_cast_before_widening_type_cast() {
  return __SharedPtr__()->clear_do_parallel_cast_before_widening_type_cast();
}
void JobConfigProto::set_do_parallel_cast_before_widening_type_cast(const bool& value) {
  return __SharedPtr__()->set_do_parallel_cast_before_widening_type_cast(value);
}
bool* JobConfigProto::mutable_do_parallel_cast_before_widening_type_cast() {
  return  __SharedPtr__()->mutable_do_parallel_cast_before_widening_type_cast();
}
// required or optional field prune_parallel_cast_ops
void JobConfigProto::clear_prune_parallel_cast_ops() {
  return __SharedPtr__()->clear_prune_parallel_cast_ops();
}
void JobConfigProto::set_prune_parallel_cast_ops(const bool& value) {
  return __SharedPtr__()->set_prune_parallel_cast_ops(value);
}
bool* JobConfigProto::mutable_prune_parallel_cast_ops() {
  return  __SharedPtr__()->mutable_prune_parallel_cast_ops();
}
// required or optional field prune_cast_to_static_shape_ops
void JobConfigProto::clear_prune_cast_to_static_shape_ops() {
  return __SharedPtr__()->clear_prune_cast_to_static_shape_ops();
}
void JobConfigProto::set_prune_cast_to_static_shape_ops(const bool& value) {
  return __SharedPtr__()->set_prune_cast_to_static_shape_ops(value);
}
bool* JobConfigProto::mutable_prune_cast_to_static_shape_ops() {
  return  __SharedPtr__()->mutable_prune_cast_to_static_shape_ops();
}
// required or optional field prune_amp_white_identity_ops
void JobConfigProto::clear_prune_amp_white_identity_ops() {
  return __SharedPtr__()->clear_prune_amp_white_identity_ops();
}
void JobConfigProto::set_prune_amp_white_identity_ops(const bool& value) {
  return __SharedPtr__()->set_prune_amp_white_identity_ops(value);
}
bool* JobConfigProto::mutable_prune_amp_white_identity_ops() {
  return  __SharedPtr__()->mutable_prune_amp_white_identity_ops();
}
// required or optional field cudnn_conv_enable_pseudo_half
void JobConfigProto::clear_cudnn_conv_enable_pseudo_half() {
  return __SharedPtr__()->clear_cudnn_conv_enable_pseudo_half();
}
void JobConfigProto::set_cudnn_conv_enable_pseudo_half(const bool& value) {
  return __SharedPtr__()->set_cudnn_conv_enable_pseudo_half(value);
}
bool* JobConfigProto::mutable_cudnn_conv_enable_pseudo_half() {
  return  __SharedPtr__()->mutable_cudnn_conv_enable_pseudo_half();
}
// required or optional field enable_auto_mixed_precision
void JobConfigProto::clear_enable_auto_mixed_precision() {
  return __SharedPtr__()->clear_enable_auto_mixed_precision();
}
void JobConfigProto::set_enable_auto_mixed_precision(const bool& value) {
  return __SharedPtr__()->set_enable_auto_mixed_precision(value);
}
bool* JobConfigProto::mutable_enable_auto_mixed_precision() {
  return  __SharedPtr__()->mutable_enable_auto_mixed_precision();
}
// required or optional field enable_quantization_aware_training
void JobConfigProto::clear_enable_quantization_aware_training() {
  return __SharedPtr__()->clear_enable_quantization_aware_training();
}
void JobConfigProto::set_enable_quantization_aware_training(const bool& value) {
  return __SharedPtr__()->set_enable_quantization_aware_training(value);
}
bool* JobConfigProto::mutable_enable_quantization_aware_training() {
  return  __SharedPtr__()->mutable_enable_quantization_aware_training();
}
// required or optional field concurrency_width
void JobConfigProto::clear_concurrency_width() {
  return __SharedPtr__()->clear_concurrency_width();
}
void JobConfigProto::set_concurrency_width(const int64_t& value) {
  return __SharedPtr__()->set_concurrency_width(value);
}
int64_t* JobConfigProto::mutable_concurrency_width() {
  return  __SharedPtr__()->mutable_concurrency_width();
}
// repeated field flag_name2flag_value
void JobConfigProto::clear_flag_name2flag_value() {
  return __SharedPtr__()->clear_flag_name2flag_value();
}

const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_ & JobConfigProto::flag_name2flag_value() const {
  return __SharedConst__()->flag_name2flag_value();
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_* JobConfigProto::mutable_flag_name2flag_value() {
  return __SharedPtr__()->mutable_flag_name2flag_value();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> JobConfigProto::shared_mutable_flag_name2flag_value() {
  return mutable_flag_name2flag_value()->__SharedMutable__();
}
// required or optional field logical_object_id
void JobConfigProto::clear_logical_object_id() {
  return __SharedPtr__()->clear_logical_object_id();
}
void JobConfigProto::set_logical_object_id(const int64_t& value) {
  return __SharedPtr__()->set_logical_object_id(value);
}
int64_t* JobConfigProto::mutable_logical_object_id() {
  return  __SharedPtr__()->mutable_logical_object_id();
}
// required or optional field signature
void JobConfigProto::clear_signature() {
  return __SharedPtr__()->clear_signature();
}
::oneflow::cfg::JobSignatureDef* JobConfigProto::mutable_signature() {
  return __SharedPtr__()->mutable_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobSignatureDef> JobConfigProto::shared_mutable_signature() {
  return mutable_signature()->__SharedMutable__();
}

::std::shared_ptr<JobConfigProto> JobConfigProto::__SharedMutable__() {
  return ::std::make_shared<JobConfigProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OptimizerConf>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OptimizerConf>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_() = default;
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_() = default;


bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OptimizerConf>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstOptimizerConf> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OptimizerConf>>& data): Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_(data) {}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_() = default;
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_() = default;

void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OptimizerConf>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OptimizerConf>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OptimizerConf>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::OptimizerConf> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OptimizerConf> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__RepeatedField_OptimizerConf_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobInputDef>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobInputDef>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_() = default;
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobInputDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::JobInputDef& Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstJobInputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ConstJobInputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ConstJobInputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobInputDef>>& data): Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_(data) {}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_() = default;
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_() = default;

void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobInputDef>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobInputDef>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobInputDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::JobInputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ::oneflow::cfg::JobInputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_, ::oneflow::cfg::JobInputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobInputDef_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobOutputDef>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobOutputDef>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_() = default;
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobOutputDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::JobOutputDef& Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstJobOutputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ConstJobOutputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ConstJobOutputDef> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::JobOutputDef>>& data): Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_(data) {}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_() = default;
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_() = default;

void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobOutputDef>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::JobOutputDef>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::JobOutputDef>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::JobOutputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ::oneflow::cfg::JobOutputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_, ::oneflow::cfg::JobOutputDef> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_JobOutputDef_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>(data) {}
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_() = default;
Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::~Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_() = default;

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::operator==(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::operator<(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::AttrValue& Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_(data) {}
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_() = default;
_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::~_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_() = default;

void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::operator==(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::operator<(const _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_JOB_JOB_CONF_CFG_H__MapField___std__string_AttrValue_::shared_mut_end() { return end(); }

}
} // namespace oneflow
