#ifndef CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstLogicalBlobId;
class LogicalBlobId;
}
}
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {

// forward declare proto class;
class ParallelContext;
class ParallelConf;
class OpNameSet;
class PlacementGroup;
class BlobPlacementGroup;
class Placement;

namespace cfg {


class ParallelContext;
class ConstParallelContext;

class ParallelConf;
class ConstParallelConf;

class OpNameSet;
class ConstOpNameSet;

class PlacementGroup;
class ConstPlacementGroup;

class BlobPlacementGroup;
class ConstBlobPlacementGroup;

class Placement;
class ConstPlacement;



class ConstParallelContext : public ::oneflow::cfg::Message {
 public:

  class _ParallelContext_ {
   public:
    _ParallelContext_();
    explicit _ParallelContext_(const _ParallelContext_& other);
    explicit _ParallelContext_(_ParallelContext_&& other);
    _ParallelContext_(const ::oneflow::ParallelContext& proto_parallelcontext);
    ~_ParallelContext_();

    void InitFromProto(const ::oneflow::ParallelContext& proto_parallelcontext);

    void ToProto(::oneflow::ParallelContext* proto_parallelcontext) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelContext_& other);
  
      // optional field parallel_id
     public:
    bool has_parallel_id() const;
    const int64_t& parallel_id() const;
    void clear_parallel_id();
    void set_parallel_id(const int64_t& value);
    int64_t* mutable_parallel_id();
   protected:
    bool has_parallel_id_ = false;
    int64_t parallel_id_;
      
      // optional field parallel_num
     public:
    bool has_parallel_num() const;
    const int64_t& parallel_num() const;
    void clear_parallel_num();
    void set_parallel_num(const int64_t& value);
    int64_t* mutable_parallel_num();
   protected:
    bool has_parallel_num_ = false;
    int64_t parallel_num_;
           
   public:
    int compare(const _ParallelContext_& other);

    bool operator==(const _ParallelContext_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelContext_& other) const;
  };

  ConstParallelContext(const ::std::shared_ptr<_ParallelContext_>& data);
  ConstParallelContext(const ConstParallelContext&);
  ConstParallelContext(ConstParallelContext&&) noexcept;
  ConstParallelContext();
  ConstParallelContext(const ::oneflow::ParallelContext& proto_parallelcontext);
  virtual ~ConstParallelContext() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_parallelcontext) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field parallel_id
 public:
  bool has_parallel_id() const;
  const int64_t& parallel_id() const;
  // used by pybind11 only
  // required or optional field parallel_num
 public:
  bool has_parallel_num() const;
  const int64_t& parallel_num() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstParallelContext> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelContext& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelContext& other) const;
 protected:
  const ::std::shared_ptr<_ParallelContext_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelContext_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelContext
  void BuildFromProto(const PbMessage& proto_parallelcontext);
  
  ::std::shared_ptr<_ParallelContext_> data_;
};

class ParallelContext final : public ConstParallelContext {
 public:
  ParallelContext(const ::std::shared_ptr<_ParallelContext_>& data);
  ParallelContext(const ParallelContext& other);
  // enable nothrow for ::std::vector<ParallelContext> resize 
  ParallelContext(ParallelContext&&) noexcept;
  ParallelContext();
  explicit ParallelContext(const ::oneflow::ParallelContext& proto_parallelcontext);

  ~ParallelContext() override;

  void InitFromProto(const PbMessage& proto_parallelcontext) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelContext& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelContext& other) const;
  void Clear();
  void CopyFrom(const ParallelContext& other);
  ParallelContext& operator=(const ParallelContext& other);

  // required or optional field parallel_id
 public:
  void clear_parallel_id();
  void set_parallel_id(const int64_t& value);
  int64_t* mutable_parallel_id();
  // required or optional field parallel_num
 public:
  void clear_parallel_num();
  void set_parallel_num(const int64_t& value);
  int64_t* mutable_parallel_num();

  ::std::shared_ptr<ParallelContext> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_;

class ConstParallelConf : public ::oneflow::cfg::Message {
 public:

  class _ParallelConf_ {
   public:
    _ParallelConf_();
    explicit _ParallelConf_(const _ParallelConf_& other);
    explicit _ParallelConf_(_ParallelConf_&& other);
    _ParallelConf_(const ::oneflow::ParallelConf& proto_parallelconf);
    ~_ParallelConf_();

    void InitFromProto(const ::oneflow::ParallelConf& proto_parallelconf);

    void ToProto(::oneflow::ParallelConf* proto_parallelconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ParallelConf_& other);
  
      // repeated field device_name
   public:
    ::std::size_t device_name_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& device_name() const;
    const ::std::string& device_name(::std::size_t index) const;
    void clear_device_name();
    _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* mutable_device_name();
    ::std::string* mutable_device_name(::std::size_t index);
      void add_device_name(const ::std::string& value);
    void set_device_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> device_name_;
    
      // optional field device_tag
     public:
    bool has_device_tag() const;
    const ::std::string& device_tag() const;
    void clear_device_tag();
    void set_device_tag(const ::std::string& value);
    ::std::string* mutable_device_tag();
   protected:
    bool has_device_tag_ = false;
    ::std::string device_tag_;
      
      // optional field hierarchy
     public:
    bool has_hierarchy() const;
    const ::oneflow::cfg::ShapeProto& hierarchy() const;
    void clear_hierarchy();
    ::oneflow::cfg::ShapeProto* mutable_hierarchy();
   protected:
    bool has_hierarchy_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> hierarchy_;
           
   public:
    int compare(const _ParallelConf_& other);

    bool operator==(const _ParallelConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ParallelConf_& other) const;
  };

  ConstParallelConf(const ::std::shared_ptr<_ParallelConf_>& data);
  ConstParallelConf(const ConstParallelConf&);
  ConstParallelConf(ConstParallelConf&&) noexcept;
  ConstParallelConf();
  ConstParallelConf(const ::oneflow::ParallelConf& proto_parallelconf);
  virtual ~ConstParallelConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_parallelconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field device_name
 public:
  ::std::size_t device_name_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& device_name() const;
  const ::std::string& device_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> shared_const_device_name() const;
  // required or optional field device_tag
 public:
  bool has_device_tag() const;
  const ::std::string& device_tag() const;
  // used by pybind11 only
  // required or optional field hierarchy
 public:
  bool has_hierarchy() const;
  const ::oneflow::cfg::ShapeProto& hierarchy() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_hierarchy() const;

 public:
  ::std::shared_ptr<ConstParallelConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstParallelConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstParallelConf& other) const;
 protected:
  const ::std::shared_ptr<_ParallelConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ParallelConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstParallelConf
  void BuildFromProto(const PbMessage& proto_parallelconf);
  
  ::std::shared_ptr<_ParallelConf_> data_;
};

class ParallelConf final : public ConstParallelConf {
 public:
  ParallelConf(const ::std::shared_ptr<_ParallelConf_>& data);
  ParallelConf(const ParallelConf& other);
  // enable nothrow for ::std::vector<ParallelConf> resize 
  ParallelConf(ParallelConf&&) noexcept;
  ParallelConf();
  explicit ParallelConf(const ::oneflow::ParallelConf& proto_parallelconf);

  ~ParallelConf() override;

  void InitFromProto(const PbMessage& proto_parallelconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ParallelConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ParallelConf& other) const;
  void Clear();
  void CopyFrom(const ParallelConf& other);
  ParallelConf& operator=(const ParallelConf& other);

  // repeated field device_name
 public:
  void clear_device_name();
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* mutable_device_name();
  ::std::string* mutable_device_name(::std::size_t index);
  void add_device_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> shared_mutable_device_name();
  void set_device_name(::std::size_t index, const ::std::string& value);
  // required or optional field device_tag
 public:
  void clear_device_tag();
  void set_device_tag(const ::std::string& value);
  ::std::string* mutable_device_tag();
  // required or optional field hierarchy
 public:
  void clear_hierarchy();
  ::oneflow::cfg::ShapeProto* mutable_hierarchy();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_hierarchy();

  ::std::shared_ptr<ParallelConf> __SharedMutable__();
};


class ConstOpNameSet : public ::oneflow::cfg::Message {
 public:

  class _OpNameSet_ {
   public:
    _OpNameSet_();
    explicit _OpNameSet_(const _OpNameSet_& other);
    explicit _OpNameSet_(_OpNameSet_&& other);
    _OpNameSet_(const ::oneflow::OpNameSet& proto_opnameset);
    ~_OpNameSet_();

    void InitFromProto(const ::oneflow::OpNameSet& proto_opnameset);

    void ToProto(::oneflow::OpNameSet* proto_opnameset) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNameSet_& other);
  
      // repeated field op_name
   public:
    ::std::size_t op_name_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& op_name() const;
    const ::std::string& op_name(::std::size_t index) const;
    void clear_op_name();
    _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* mutable_op_name();
    ::std::string* mutable_op_name(::std::size_t index);
      void add_op_name(const ::std::string& value);
    void set_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> op_name_;
         
   public:
    int compare(const _OpNameSet_& other);

    bool operator==(const _OpNameSet_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNameSet_& other) const;
  };

  ConstOpNameSet(const ::std::shared_ptr<_OpNameSet_>& data);
  ConstOpNameSet(const ConstOpNameSet&);
  ConstOpNameSet(ConstOpNameSet&&) noexcept;
  ConstOpNameSet();
  ConstOpNameSet(const ::oneflow::OpNameSet& proto_opnameset);
  virtual ~ConstOpNameSet() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnameset) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_name
 public:
  ::std::size_t op_name_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& op_name() const;
  const ::std::string& op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> shared_const_op_name() const;

 public:
  ::std::shared_ptr<ConstOpNameSet> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNameSet& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNameSet& other) const;
 protected:
  const ::std::shared_ptr<_OpNameSet_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNameSet_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNameSet
  void BuildFromProto(const PbMessage& proto_opnameset);
  
  ::std::shared_ptr<_OpNameSet_> data_;
};

class OpNameSet final : public ConstOpNameSet {
 public:
  OpNameSet(const ::std::shared_ptr<_OpNameSet_>& data);
  OpNameSet(const OpNameSet& other);
  // enable nothrow for ::std::vector<OpNameSet> resize 
  OpNameSet(OpNameSet&&) noexcept;
  OpNameSet();
  explicit OpNameSet(const ::oneflow::OpNameSet& proto_opnameset);

  ~OpNameSet() override;

  void InitFromProto(const PbMessage& proto_opnameset) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNameSet& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNameSet& other) const;
  void Clear();
  void CopyFrom(const OpNameSet& other);
  OpNameSet& operator=(const OpNameSet& other);

  // repeated field op_name
 public:
  void clear_op_name();
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_* mutable_op_name();
  ::std::string* mutable_op_name(::std::size_t index);
  void add_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> shared_mutable_op_name();
  void set_op_name(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<OpNameSet> __SharedMutable__();
};


class ConstPlacementGroup : public ::oneflow::cfg::Message {
 public:

  class _PlacementGroup_ {
   public:
    _PlacementGroup_();
    explicit _PlacementGroup_(const _PlacementGroup_& other);
    explicit _PlacementGroup_(_PlacementGroup_&& other);
    _PlacementGroup_(const ::oneflow::PlacementGroup& proto_placementgroup);
    ~_PlacementGroup_();

    void InitFromProto(const ::oneflow::PlacementGroup& proto_placementgroup);

    void ToProto(::oneflow::PlacementGroup* proto_placementgroup) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _PlacementGroup_& other);
  
      // optional field op_set
     public:
    bool has_op_set() const;
    const ::oneflow::cfg::OpNameSet& op_set() const;
    void clear_op_set();
    ::oneflow::cfg::OpNameSet* mutable_op_set();
   protected:
    bool has_op_set_ = false;
    ::std::shared_ptr<::oneflow::cfg::OpNameSet> op_set_;
      
      // optional field parallel_conf
     public:
    bool has_parallel_conf() const;
    const ::oneflow::cfg::ParallelConf& parallel_conf() const;
    void clear_parallel_conf();
    ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
   protected:
    bool has_parallel_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelConf> parallel_conf_;
           
   public:
    int compare(const _PlacementGroup_& other);

    bool operator==(const _PlacementGroup_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _PlacementGroup_& other) const;
  };

  ConstPlacementGroup(const ::std::shared_ptr<_PlacementGroup_>& data);
  ConstPlacementGroup(const ConstPlacementGroup&);
  ConstPlacementGroup(ConstPlacementGroup&&) noexcept;
  ConstPlacementGroup();
  ConstPlacementGroup(const ::oneflow::PlacementGroup& proto_placementgroup);
  virtual ~ConstPlacementGroup() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_placementgroup) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_set
 public:
  bool has_op_set() const;
  const ::oneflow::cfg::OpNameSet& op_set() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpNameSet> shared_const_op_set() const;
  // required or optional field parallel_conf
 public:
  bool has_parallel_conf() const;
  const ::oneflow::cfg::ParallelConf& parallel_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConf> shared_const_parallel_conf() const;

 public:
  ::std::shared_ptr<ConstPlacementGroup> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPlacementGroup& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPlacementGroup& other) const;
 protected:
  const ::std::shared_ptr<_PlacementGroup_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_PlacementGroup_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPlacementGroup
  void BuildFromProto(const PbMessage& proto_placementgroup);
  
  ::std::shared_ptr<_PlacementGroup_> data_;
};

class PlacementGroup final : public ConstPlacementGroup {
 public:
  PlacementGroup(const ::std::shared_ptr<_PlacementGroup_>& data);
  PlacementGroup(const PlacementGroup& other);
  // enable nothrow for ::std::vector<PlacementGroup> resize 
  PlacementGroup(PlacementGroup&&) noexcept;
  PlacementGroup();
  explicit PlacementGroup(const ::oneflow::PlacementGroup& proto_placementgroup);

  ~PlacementGroup() override;

  void InitFromProto(const PbMessage& proto_placementgroup) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const PlacementGroup& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const PlacementGroup& other) const;
  void Clear();
  void CopyFrom(const PlacementGroup& other);
  PlacementGroup& operator=(const PlacementGroup& other);

  // required or optional field op_set
 public:
  void clear_op_set();
  ::oneflow::cfg::OpNameSet* mutable_op_set();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpNameSet> shared_mutable_op_set();
  // required or optional field parallel_conf
 public:
  void clear_parallel_conf();
  ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConf> shared_mutable_parallel_conf();

  ::std::shared_ptr<PlacementGroup> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_;
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_;

class ConstBlobPlacementGroup : public ::oneflow::cfg::Message {
 public:

  class _BlobPlacementGroup_ {
   public:
    _BlobPlacementGroup_();
    explicit _BlobPlacementGroup_(const _BlobPlacementGroup_& other);
    explicit _BlobPlacementGroup_(_BlobPlacementGroup_&& other);
    _BlobPlacementGroup_(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup);
    ~_BlobPlacementGroup_();

    void InitFromProto(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup);

    void ToProto(::oneflow::BlobPlacementGroup* proto_blobplacementgroup) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BlobPlacementGroup_& other);
  
      // repeated field lbi
   public:
    ::std::size_t lbi_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi(::std::size_t index) const;
    void clear_lbi();
    _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_* mutable_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi(::std::size_t index);
      ::oneflow::cfg::LogicalBlobId* add_lbi();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> lbi_;
    
      // optional field parallel_conf
     public:
    bool has_parallel_conf() const;
    const ::oneflow::cfg::ParallelConf& parallel_conf() const;
    void clear_parallel_conf();
    ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
   protected:
    bool has_parallel_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelConf> parallel_conf_;
           
   public:
    int compare(const _BlobPlacementGroup_& other);

    bool operator==(const _BlobPlacementGroup_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BlobPlacementGroup_& other) const;
  };

  ConstBlobPlacementGroup(const ::std::shared_ptr<_BlobPlacementGroup_>& data);
  ConstBlobPlacementGroup(const ConstBlobPlacementGroup&);
  ConstBlobPlacementGroup(ConstBlobPlacementGroup&&) noexcept;
  ConstBlobPlacementGroup();
  ConstBlobPlacementGroup(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup);
  virtual ~ConstBlobPlacementGroup() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_blobplacementgroup) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field lbi
 public:
  ::std::size_t lbi_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> shared_const_lbi() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi(::std::size_t index) const;
  // required or optional field parallel_conf
 public:
  bool has_parallel_conf() const;
  const ::oneflow::cfg::ParallelConf& parallel_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConf> shared_const_parallel_conf() const;

 public:
  ::std::shared_ptr<ConstBlobPlacementGroup> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBlobPlacementGroup& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBlobPlacementGroup& other) const;
 protected:
  const ::std::shared_ptr<_BlobPlacementGroup_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BlobPlacementGroup_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBlobPlacementGroup
  void BuildFromProto(const PbMessage& proto_blobplacementgroup);
  
  ::std::shared_ptr<_BlobPlacementGroup_> data_;
};

class BlobPlacementGroup final : public ConstBlobPlacementGroup {
 public:
  BlobPlacementGroup(const ::std::shared_ptr<_BlobPlacementGroup_>& data);
  BlobPlacementGroup(const BlobPlacementGroup& other);
  // enable nothrow for ::std::vector<BlobPlacementGroup> resize 
  BlobPlacementGroup(BlobPlacementGroup&&) noexcept;
  BlobPlacementGroup();
  explicit BlobPlacementGroup(const ::oneflow::BlobPlacementGroup& proto_blobplacementgroup);

  ~BlobPlacementGroup() override;

  void InitFromProto(const PbMessage& proto_blobplacementgroup) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BlobPlacementGroup& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BlobPlacementGroup& other) const;
  void Clear();
  void CopyFrom(const BlobPlacementGroup& other);
  BlobPlacementGroup& operator=(const BlobPlacementGroup& other);

  // repeated field lbi
 public:
  void clear_lbi();
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_* mutable_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> shared_mutable_lbi();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi(::std::size_t index);
  ::oneflow::cfg::LogicalBlobId* add_lbi();
  // required or optional field parallel_conf
 public:
  void clear_parallel_conf();
  ::oneflow::cfg::ParallelConf* mutable_parallel_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConf> shared_mutable_parallel_conf();

  ::std::shared_ptr<BlobPlacementGroup> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_;
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_;
class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_;
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_;

class ConstPlacement : public ::oneflow::cfg::Message {
 public:

  class _Placement_ {
   public:
    _Placement_();
    explicit _Placement_(const _Placement_& other);
    explicit _Placement_(_Placement_&& other);
    _Placement_(const ::oneflow::Placement& proto_placement);
    ~_Placement_();

    void InitFromProto(const ::oneflow::Placement& proto_placement);

    void ToProto(::oneflow::Placement* proto_placement) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _Placement_& other);
  
      // repeated field placement_group
   public:
    ::std::size_t placement_group_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& placement_group() const;
    const ::oneflow::cfg::PlacementGroup& placement_group(::std::size_t index) const;
    void clear_placement_group();
    _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_* mutable_placement_group();
    ::oneflow::cfg::PlacementGroup* mutable_placement_group(::std::size_t index);
      ::oneflow::cfg::PlacementGroup* add_placement_group();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> placement_group_;
    
      // repeated field blob_placement_group
   public:
    ::std::size_t blob_placement_group_size() const;
    const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& blob_placement_group() const;
    const ::oneflow::cfg::BlobPlacementGroup& blob_placement_group(::std::size_t index) const;
    void clear_blob_placement_group();
    _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_* mutable_blob_placement_group();
    ::oneflow::cfg::BlobPlacementGroup* mutable_blob_placement_group(::std::size_t index);
      ::oneflow::cfg::BlobPlacementGroup* add_blob_placement_group();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> blob_placement_group_;
         
   public:
    int compare(const _Placement_& other);

    bool operator==(const _Placement_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _Placement_& other) const;
  };

  ConstPlacement(const ::std::shared_ptr<_Placement_>& data);
  ConstPlacement(const ConstPlacement&);
  ConstPlacement(ConstPlacement&&) noexcept;
  ConstPlacement();
  ConstPlacement(const ::oneflow::Placement& proto_placement);
  virtual ~ConstPlacement() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_placement) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field placement_group
 public:
  ::std::size_t placement_group_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& placement_group() const;
  const ::oneflow::cfg::PlacementGroup& placement_group(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> shared_const_placement_group() const;
  ::std::shared_ptr<::oneflow::cfg::ConstPlacementGroup> shared_const_placement_group(::std::size_t index) const;
  // repeated field blob_placement_group
 public:
  ::std::size_t blob_placement_group_size() const;
  const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& blob_placement_group() const;
  const ::oneflow::cfg::BlobPlacementGroup& blob_placement_group(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> shared_const_blob_placement_group() const;
  ::std::shared_ptr<::oneflow::cfg::ConstBlobPlacementGroup> shared_const_blob_placement_group(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstPlacement> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstPlacement& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstPlacement& other) const;
 protected:
  const ::std::shared_ptr<_Placement_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_Placement_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstPlacement
  void BuildFromProto(const PbMessage& proto_placement);
  
  ::std::shared_ptr<_Placement_> data_;
};

class Placement final : public ConstPlacement {
 public:
  Placement(const ::std::shared_ptr<_Placement_>& data);
  Placement(const Placement& other);
  // enable nothrow for ::std::vector<Placement> resize 
  Placement(Placement&&) noexcept;
  Placement();
  explicit Placement(const ::oneflow::Placement& proto_placement);

  ~Placement() override;

  void InitFromProto(const PbMessage& proto_placement) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const Placement& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Placement& other) const;
  void Clear();
  void CopyFrom(const Placement& other);
  Placement& operator=(const Placement& other);

  // repeated field placement_group
 public:
  void clear_placement_group();
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_* mutable_placement_group();
  ::oneflow::cfg::PlacementGroup* mutable_placement_group(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> shared_mutable_placement_group();
  ::std::shared_ptr<::oneflow::cfg::PlacementGroup> shared_mutable_placement_group(::std::size_t index);
  ::oneflow::cfg::PlacementGroup* add_placement_group();
  // repeated field blob_placement_group
 public:
  void clear_blob_placement_group();
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_* mutable_blob_placement_group();
  ::oneflow::cfg::BlobPlacementGroup* mutable_blob_placement_group(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> shared_mutable_blob_placement_group();
  ::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> shared_mutable_blob_placement_group(::std::size_t index);
  ::oneflow::cfg::BlobPlacementGroup* add_blob_placement_group();

  ::std::shared_ptr<Placement> __SharedMutable__();
};







// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};










// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::LogicalBlobId> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_ final : public Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::LogicalBlobId>>& data);
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_();
  ~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_LogicalBlobId_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::PlacementGroup> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::PlacementGroup>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstPlacementGroup> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_ final : public Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::PlacementGroup>>& data);
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_();
  ~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_PlacementGroup_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::PlacementGroup> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::PlacementGroup> __SharedMutable__(::std::size_t index);
};

// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::BlobPlacementGroup> {
 public:
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::BlobPlacementGroup>>& data);
  Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_();
  ~Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstBlobPlacementGroup> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_ final : public Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_ {
 public:
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::BlobPlacementGroup>>& data);
  _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_();
  ~_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H__RepeatedField_BlobPlacementGroup_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::BlobPlacementGroup> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstParallelContext> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelContext& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelContext> {
  std::size_t operator()(const ::oneflow::cfg::ParallelContext& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstParallelConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstParallelConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ParallelConf> {
  std::size_t operator()(const ::oneflow::cfg::ParallelConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpNameSet> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNameSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNameSet> {
  std::size_t operator()(const ::oneflow::cfg::OpNameSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPlacementGroup> {
  std::size_t operator()(const ::oneflow::cfg::ConstPlacementGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::PlacementGroup> {
  std::size_t operator()(const ::oneflow::cfg::PlacementGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBlobPlacementGroup> {
  std::size_t operator()(const ::oneflow::cfg::ConstBlobPlacementGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BlobPlacementGroup> {
  std::size_t operator()(const ::oneflow::cfg::BlobPlacementGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstPlacement> {
  std::size_t operator()(const ::oneflow::cfg::ConstPlacement& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::Placement> {
  std::size_t operator()(const ::oneflow::cfg::Placement& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_PLACEMENT_CFG_H_