#ifndef CFG_ONEFLOW_CORE_JOB_INITIALIZER_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_JOB_INITIALIZER_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum RandomDistribution : unsigned int;
}
}
namespace oneflow {
namespace cfg {
enum VarianceNorm : unsigned int;
}
}

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class ConstantInitializerConf;
class ConstantIntInitializerConf;
class RandomUniformInitializerConf;
class RandomUniformIntInitializerConf;
class RandomNormalInitializerConf;
class TruncatedNormalInitializerConf;
class XavierInitializerConf;
class MsraInitializerConf;
class RangeInitializerConf;
class IntRangeInitializerConf;
class VarianceScalingInitializerConf;
class EmptyInitializerConf;
class InitializerConf;
class InitializeWithSnapshotConf;

namespace cfg {


class ConstantInitializerConf;
class ConstConstantInitializerConf;

class ConstantIntInitializerConf;
class ConstConstantIntInitializerConf;

class RandomUniformInitializerConf;
class ConstRandomUniformInitializerConf;

class RandomUniformIntInitializerConf;
class ConstRandomUniformIntInitializerConf;

class RandomNormalInitializerConf;
class ConstRandomNormalInitializerConf;

class TruncatedNormalInitializerConf;
class ConstTruncatedNormalInitializerConf;

class XavierInitializerConf;
class ConstXavierInitializerConf;

class MsraInitializerConf;
class ConstMsraInitializerConf;

class RangeInitializerConf;
class ConstRangeInitializerConf;

class IntRangeInitializerConf;
class ConstIntRangeInitializerConf;

class VarianceScalingInitializerConf;
class ConstVarianceScalingInitializerConf;

class EmptyInitializerConf;
class ConstEmptyInitializerConf;

class InitializerConf;
class ConstInitializerConf;

class InitializeWithSnapshotConf;
class ConstInitializeWithSnapshotConf;

enum VarianceNorm : unsigned int {
  kFanIn = 0,
  kFanOut = 1,
  kAverage = 2,
};

inline ::std::string VarianceNorm_Name(VarianceNorm value) {
  switch (value) {
  case kFanIn: { return "kFanIn"; }
  case kFanOut: { return "kFanOut"; }
  case kAverage: { return "kAverage"; }
  default:
    return "";
  }
}

enum RandomDistribution : unsigned int {
  kRandomUniform = 0,
  kRandomNormal = 1,
  kTruncatedNormal = 2,
};

inline ::std::string RandomDistribution_Name(RandomDistribution value) {
  switch (value) {
  case kRandomUniform: { return "kRandomUniform"; }
  case kRandomNormal: { return "kRandomNormal"; }
  case kTruncatedNormal: { return "kTruncatedNormal"; }
  default:
    return "";
  }
}



class ConstConstantInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _ConstantInitializerConf_ {
   public:
    _ConstantInitializerConf_();
    explicit _ConstantInitializerConf_(const _ConstantInitializerConf_& other);
    explicit _ConstantInitializerConf_(_ConstantInitializerConf_&& other);
    _ConstantInitializerConf_(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf);
    ~_ConstantInitializerConf_();

    void InitFromProto(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf);

    void ToProto(::oneflow::ConstantInitializerConf* proto_constantinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConstantInitializerConf_& other);
  
      // optional field value
     public:
    bool has_value() const;
    const float& value() const;
    void clear_value();
    void set_value(const float& value);
    float* mutable_value();
   protected:
    bool has_value_ = false;
    float value_;
           
   public:
    int compare(const _ConstantInitializerConf_& other);

    bool operator==(const _ConstantInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConstantInitializerConf_& other) const;
  };

  ConstConstantInitializerConf(const ::std::shared_ptr<_ConstantInitializerConf_>& data);
  ConstConstantInitializerConf(const ConstConstantInitializerConf&);
  ConstConstantInitializerConf(ConstConstantInitializerConf&&) noexcept;
  ConstConstantInitializerConf();
  ConstConstantInitializerConf(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf);
  virtual ~ConstConstantInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_constantinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field value
 public:
  bool has_value() const;
  const float& value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstConstantInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConstantInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConstantInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_ConstantInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConstantInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConstantInitializerConf
  void BuildFromProto(const PbMessage& proto_constantinitializerconf);
  
  ::std::shared_ptr<_ConstantInitializerConf_> data_;
};

class ConstantInitializerConf final : public ConstConstantInitializerConf {
 public:
  ConstantInitializerConf(const ::std::shared_ptr<_ConstantInitializerConf_>& data);
  ConstantInitializerConf(const ConstantInitializerConf& other);
  // enable nothrow for ::std::vector<ConstantInitializerConf> resize 
  ConstantInitializerConf(ConstantInitializerConf&&) noexcept;
  ConstantInitializerConf();
  explicit ConstantInitializerConf(const ::oneflow::ConstantInitializerConf& proto_constantinitializerconf);

  ~ConstantInitializerConf() override;

  void InitFromProto(const PbMessage& proto_constantinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConstantInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConstantInitializerConf& other) const;
  void Clear();
  void CopyFrom(const ConstantInitializerConf& other);
  ConstantInitializerConf& operator=(const ConstantInitializerConf& other);

  // required or optional field value
 public:
  void clear_value();
  void set_value(const float& value);
  float* mutable_value();

  ::std::shared_ptr<ConstantInitializerConf> __SharedMutable__();
};


class ConstConstantIntInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _ConstantIntInitializerConf_ {
   public:
    _ConstantIntInitializerConf_();
    explicit _ConstantIntInitializerConf_(const _ConstantIntInitializerConf_& other);
    explicit _ConstantIntInitializerConf_(_ConstantIntInitializerConf_&& other);
    _ConstantIntInitializerConf_(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf);
    ~_ConstantIntInitializerConf_();

    void InitFromProto(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf);

    void ToProto(::oneflow::ConstantIntInitializerConf* proto_constantintinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConstantIntInitializerConf_& other);
  
      // optional field value
     public:
    bool has_value() const;
    const int64_t& value() const;
    void clear_value();
    void set_value(const int64_t& value);
    int64_t* mutable_value();
   protected:
    bool has_value_ = false;
    int64_t value_;
           
   public:
    int compare(const _ConstantIntInitializerConf_& other);

    bool operator==(const _ConstantIntInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConstantIntInitializerConf_& other) const;
  };

  ConstConstantIntInitializerConf(const ::std::shared_ptr<_ConstantIntInitializerConf_>& data);
  ConstConstantIntInitializerConf(const ConstConstantIntInitializerConf&);
  ConstConstantIntInitializerConf(ConstConstantIntInitializerConf&&) noexcept;
  ConstConstantIntInitializerConf();
  ConstConstantIntInitializerConf(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf);
  virtual ~ConstConstantIntInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_constantintinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field value
 public:
  bool has_value() const;
  const int64_t& value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstConstantIntInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConstantIntInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConstantIntInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_ConstantIntInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConstantIntInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConstantIntInitializerConf
  void BuildFromProto(const PbMessage& proto_constantintinitializerconf);
  
  ::std::shared_ptr<_ConstantIntInitializerConf_> data_;
};

class ConstantIntInitializerConf final : public ConstConstantIntInitializerConf {
 public:
  ConstantIntInitializerConf(const ::std::shared_ptr<_ConstantIntInitializerConf_>& data);
  ConstantIntInitializerConf(const ConstantIntInitializerConf& other);
  // enable nothrow for ::std::vector<ConstantIntInitializerConf> resize 
  ConstantIntInitializerConf(ConstantIntInitializerConf&&) noexcept;
  ConstantIntInitializerConf();
  explicit ConstantIntInitializerConf(const ::oneflow::ConstantIntInitializerConf& proto_constantintinitializerconf);

  ~ConstantIntInitializerConf() override;

  void InitFromProto(const PbMessage& proto_constantintinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConstantIntInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConstantIntInitializerConf& other) const;
  void Clear();
  void CopyFrom(const ConstantIntInitializerConf& other);
  ConstantIntInitializerConf& operator=(const ConstantIntInitializerConf& other);

  // required or optional field value
 public:
  void clear_value();
  void set_value(const int64_t& value);
  int64_t* mutable_value();

  ::std::shared_ptr<ConstantIntInitializerConf> __SharedMutable__();
};


class ConstRandomUniformInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _RandomUniformInitializerConf_ {
   public:
    _RandomUniformInitializerConf_();
    explicit _RandomUniformInitializerConf_(const _RandomUniformInitializerConf_& other);
    explicit _RandomUniformInitializerConf_(_RandomUniformInitializerConf_&& other);
    _RandomUniformInitializerConf_(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf);
    ~_RandomUniformInitializerConf_();

    void InitFromProto(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf);

    void ToProto(::oneflow::RandomUniformInitializerConf* proto_randomuniforminitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RandomUniformInitializerConf_& other);
  
      // optional field min
     public:
    bool has_min() const;
    const float& min() const;
    void clear_min();
    void set_min(const float& value);
    float* mutable_min();
   protected:
    bool has_min_ = false;
    float min_;
      
      // optional field max
     public:
    bool has_max() const;
    const float& max() const;
    void clear_max();
    void set_max(const float& value);
    float* mutable_max();
   protected:
    bool has_max_ = false;
    float max_;
           
   public:
    int compare(const _RandomUniformInitializerConf_& other);

    bool operator==(const _RandomUniformInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RandomUniformInitializerConf_& other) const;
  };

  ConstRandomUniformInitializerConf(const ::std::shared_ptr<_RandomUniformInitializerConf_>& data);
  ConstRandomUniformInitializerConf(const ConstRandomUniformInitializerConf&);
  ConstRandomUniformInitializerConf(ConstRandomUniformInitializerConf&&) noexcept;
  ConstRandomUniformInitializerConf();
  ConstRandomUniformInitializerConf(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf);
  virtual ~ConstRandomUniformInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_randomuniforminitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field min
 public:
  bool has_min() const;
  const float& min() const;
  // used by pybind11 only
  // required or optional field max
 public:
  bool has_max() const;
  const float& max() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRandomUniformInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRandomUniformInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRandomUniformInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_RandomUniformInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RandomUniformInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRandomUniformInitializerConf
  void BuildFromProto(const PbMessage& proto_randomuniforminitializerconf);
  
  ::std::shared_ptr<_RandomUniformInitializerConf_> data_;
};

class RandomUniformInitializerConf final : public ConstRandomUniformInitializerConf {
 public:
  RandomUniformInitializerConf(const ::std::shared_ptr<_RandomUniformInitializerConf_>& data);
  RandomUniformInitializerConf(const RandomUniformInitializerConf& other);
  // enable nothrow for ::std::vector<RandomUniformInitializerConf> resize 
  RandomUniformInitializerConf(RandomUniformInitializerConf&&) noexcept;
  RandomUniformInitializerConf();
  explicit RandomUniformInitializerConf(const ::oneflow::RandomUniformInitializerConf& proto_randomuniforminitializerconf);

  ~RandomUniformInitializerConf() override;

  void InitFromProto(const PbMessage& proto_randomuniforminitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RandomUniformInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RandomUniformInitializerConf& other) const;
  void Clear();
  void CopyFrom(const RandomUniformInitializerConf& other);
  RandomUniformInitializerConf& operator=(const RandomUniformInitializerConf& other);

  // required or optional field min
 public:
  void clear_min();
  void set_min(const float& value);
  float* mutable_min();
  // required or optional field max
 public:
  void clear_max();
  void set_max(const float& value);
  float* mutable_max();

  ::std::shared_ptr<RandomUniformInitializerConf> __SharedMutable__();
};


class ConstRandomUniformIntInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _RandomUniformIntInitializerConf_ {
   public:
    _RandomUniformIntInitializerConf_();
    explicit _RandomUniformIntInitializerConf_(const _RandomUniformIntInitializerConf_& other);
    explicit _RandomUniformIntInitializerConf_(_RandomUniformIntInitializerConf_&& other);
    _RandomUniformIntInitializerConf_(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf);
    ~_RandomUniformIntInitializerConf_();

    void InitFromProto(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf);

    void ToProto(::oneflow::RandomUniformIntInitializerConf* proto_randomuniformintinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RandomUniformIntInitializerConf_& other);
  
      // optional field min
     public:
    bool has_min() const;
    const int32_t& min() const;
    void clear_min();
    void set_min(const int32_t& value);
    int32_t* mutable_min();
   protected:
    bool has_min_ = false;
    int32_t min_;
      
      // optional field max
     public:
    bool has_max() const;
    const int32_t& max() const;
    void clear_max();
    void set_max(const int32_t& value);
    int32_t* mutable_max();
   protected:
    bool has_max_ = false;
    int32_t max_;
           
   public:
    int compare(const _RandomUniformIntInitializerConf_& other);

    bool operator==(const _RandomUniformIntInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RandomUniformIntInitializerConf_& other) const;
  };

  ConstRandomUniformIntInitializerConf(const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& data);
  ConstRandomUniformIntInitializerConf(const ConstRandomUniformIntInitializerConf&);
  ConstRandomUniformIntInitializerConf(ConstRandomUniformIntInitializerConf&&) noexcept;
  ConstRandomUniformIntInitializerConf();
  ConstRandomUniformIntInitializerConf(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf);
  virtual ~ConstRandomUniformIntInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_randomuniformintinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field min
 public:
  bool has_min() const;
  const int32_t& min() const;
  // used by pybind11 only
  // required or optional field max
 public:
  bool has_max() const;
  const int32_t& max() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRandomUniformIntInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRandomUniformIntInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRandomUniformIntInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRandomUniformIntInitializerConf
  void BuildFromProto(const PbMessage& proto_randomuniformintinitializerconf);
  
  ::std::shared_ptr<_RandomUniformIntInitializerConf_> data_;
};

class RandomUniformIntInitializerConf final : public ConstRandomUniformIntInitializerConf {
 public:
  RandomUniformIntInitializerConf(const ::std::shared_ptr<_RandomUniformIntInitializerConf_>& data);
  RandomUniformIntInitializerConf(const RandomUniformIntInitializerConf& other);
  // enable nothrow for ::std::vector<RandomUniformIntInitializerConf> resize 
  RandomUniformIntInitializerConf(RandomUniformIntInitializerConf&&) noexcept;
  RandomUniformIntInitializerConf();
  explicit RandomUniformIntInitializerConf(const ::oneflow::RandomUniformIntInitializerConf& proto_randomuniformintinitializerconf);

  ~RandomUniformIntInitializerConf() override;

  void InitFromProto(const PbMessage& proto_randomuniformintinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RandomUniformIntInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RandomUniformIntInitializerConf& other) const;
  void Clear();
  void CopyFrom(const RandomUniformIntInitializerConf& other);
  RandomUniformIntInitializerConf& operator=(const RandomUniformIntInitializerConf& other);

  // required or optional field min
 public:
  void clear_min();
  void set_min(const int32_t& value);
  int32_t* mutable_min();
  // required or optional field max
 public:
  void clear_max();
  void set_max(const int32_t& value);
  int32_t* mutable_max();

  ::std::shared_ptr<RandomUniformIntInitializerConf> __SharedMutable__();
};


class ConstRandomNormalInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _RandomNormalInitializerConf_ {
   public:
    _RandomNormalInitializerConf_();
    explicit _RandomNormalInitializerConf_(const _RandomNormalInitializerConf_& other);
    explicit _RandomNormalInitializerConf_(_RandomNormalInitializerConf_&& other);
    _RandomNormalInitializerConf_(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf);
    ~_RandomNormalInitializerConf_();

    void InitFromProto(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf);

    void ToProto(::oneflow::RandomNormalInitializerConf* proto_randomnormalinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RandomNormalInitializerConf_& other);
  
      // optional field mean
     public:
    bool has_mean() const;
    const float& mean() const;
    void clear_mean();
    void set_mean(const float& value);
    float* mutable_mean();
   protected:
    bool has_mean_ = false;
    float mean_;
      
      // optional field std
     public:
    bool has_std() const;
    const float& std() const;
    void clear_std();
    void set_std(const float& value);
    float* mutable_std();
   protected:
    bool has_std_ = false;
    float std_;
           
   public:
    int compare(const _RandomNormalInitializerConf_& other);

    bool operator==(const _RandomNormalInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RandomNormalInitializerConf_& other) const;
  };

  ConstRandomNormalInitializerConf(const ::std::shared_ptr<_RandomNormalInitializerConf_>& data);
  ConstRandomNormalInitializerConf(const ConstRandomNormalInitializerConf&);
  ConstRandomNormalInitializerConf(ConstRandomNormalInitializerConf&&) noexcept;
  ConstRandomNormalInitializerConf();
  ConstRandomNormalInitializerConf(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf);
  virtual ~ConstRandomNormalInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_randomnormalinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field mean
 public:
  bool has_mean() const;
  const float& mean() const;
  // used by pybind11 only
  // required or optional field std
 public:
  bool has_std() const;
  const float& std() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRandomNormalInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRandomNormalInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRandomNormalInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_RandomNormalInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RandomNormalInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRandomNormalInitializerConf
  void BuildFromProto(const PbMessage& proto_randomnormalinitializerconf);
  
  ::std::shared_ptr<_RandomNormalInitializerConf_> data_;
};

class RandomNormalInitializerConf final : public ConstRandomNormalInitializerConf {
 public:
  RandomNormalInitializerConf(const ::std::shared_ptr<_RandomNormalInitializerConf_>& data);
  RandomNormalInitializerConf(const RandomNormalInitializerConf& other);
  // enable nothrow for ::std::vector<RandomNormalInitializerConf> resize 
  RandomNormalInitializerConf(RandomNormalInitializerConf&&) noexcept;
  RandomNormalInitializerConf();
  explicit RandomNormalInitializerConf(const ::oneflow::RandomNormalInitializerConf& proto_randomnormalinitializerconf);

  ~RandomNormalInitializerConf() override;

  void InitFromProto(const PbMessage& proto_randomnormalinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RandomNormalInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RandomNormalInitializerConf& other) const;
  void Clear();
  void CopyFrom(const RandomNormalInitializerConf& other);
  RandomNormalInitializerConf& operator=(const RandomNormalInitializerConf& other);

  // required or optional field mean
 public:
  void clear_mean();
  void set_mean(const float& value);
  float* mutable_mean();
  // required or optional field std
 public:
  void clear_std();
  void set_std(const float& value);
  float* mutable_std();

  ::std::shared_ptr<RandomNormalInitializerConf> __SharedMutable__();
};


class ConstTruncatedNormalInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _TruncatedNormalInitializerConf_ {
   public:
    _TruncatedNormalInitializerConf_();
    explicit _TruncatedNormalInitializerConf_(const _TruncatedNormalInitializerConf_& other);
    explicit _TruncatedNormalInitializerConf_(_TruncatedNormalInitializerConf_&& other);
    _TruncatedNormalInitializerConf_(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf);
    ~_TruncatedNormalInitializerConf_();

    void InitFromProto(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf);

    void ToProto(::oneflow::TruncatedNormalInitializerConf* proto_truncatednormalinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TruncatedNormalInitializerConf_& other);
  
      // optional field mean
     public:
    bool has_mean() const;
    const float& mean() const;
    void clear_mean();
    void set_mean(const float& value);
    float* mutable_mean();
   protected:
    bool has_mean_ = false;
    float mean_;
      
      // optional field std
     public:
    bool has_std() const;
    const float& std() const;
    void clear_std();
    void set_std(const float& value);
    float* mutable_std();
   protected:
    bool has_std_ = false;
    float std_;
           
   public:
    int compare(const _TruncatedNormalInitializerConf_& other);

    bool operator==(const _TruncatedNormalInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TruncatedNormalInitializerConf_& other) const;
  };

  ConstTruncatedNormalInitializerConf(const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& data);
  ConstTruncatedNormalInitializerConf(const ConstTruncatedNormalInitializerConf&);
  ConstTruncatedNormalInitializerConf(ConstTruncatedNormalInitializerConf&&) noexcept;
  ConstTruncatedNormalInitializerConf();
  ConstTruncatedNormalInitializerConf(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf);
  virtual ~ConstTruncatedNormalInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_truncatednormalinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field mean
 public:
  bool has_mean() const;
  const float& mean() const;
  // used by pybind11 only
  // required or optional field std
 public:
  bool has_std() const;
  const float& std() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstTruncatedNormalInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTruncatedNormalInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTruncatedNormalInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTruncatedNormalInitializerConf
  void BuildFromProto(const PbMessage& proto_truncatednormalinitializerconf);
  
  ::std::shared_ptr<_TruncatedNormalInitializerConf_> data_;
};

class TruncatedNormalInitializerConf final : public ConstTruncatedNormalInitializerConf {
 public:
  TruncatedNormalInitializerConf(const ::std::shared_ptr<_TruncatedNormalInitializerConf_>& data);
  TruncatedNormalInitializerConf(const TruncatedNormalInitializerConf& other);
  // enable nothrow for ::std::vector<TruncatedNormalInitializerConf> resize 
  TruncatedNormalInitializerConf(TruncatedNormalInitializerConf&&) noexcept;
  TruncatedNormalInitializerConf();
  explicit TruncatedNormalInitializerConf(const ::oneflow::TruncatedNormalInitializerConf& proto_truncatednormalinitializerconf);

  ~TruncatedNormalInitializerConf() override;

  void InitFromProto(const PbMessage& proto_truncatednormalinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TruncatedNormalInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TruncatedNormalInitializerConf& other) const;
  void Clear();
  void CopyFrom(const TruncatedNormalInitializerConf& other);
  TruncatedNormalInitializerConf& operator=(const TruncatedNormalInitializerConf& other);

  // required or optional field mean
 public:
  void clear_mean();
  void set_mean(const float& value);
  float* mutable_mean();
  // required or optional field std
 public:
  void clear_std();
  void set_std(const float& value);
  float* mutable_std();

  ::std::shared_ptr<TruncatedNormalInitializerConf> __SharedMutable__();
};


class ConstXavierInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _XavierInitializerConf_ {
   public:
    _XavierInitializerConf_();
    explicit _XavierInitializerConf_(const _XavierInitializerConf_& other);
    explicit _XavierInitializerConf_(_XavierInitializerConf_&& other);
    _XavierInitializerConf_(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf);
    ~_XavierInitializerConf_();

    void InitFromProto(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf);

    void ToProto(::oneflow::XavierInitializerConf* proto_xavierinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XavierInitializerConf_& other);
  
      // optional field variance_norm
     public:
    bool has_variance_norm() const;
    const ::oneflow::cfg::VarianceNorm& variance_norm() const;
    void clear_variance_norm();
    void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
    ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
   protected:
    bool has_variance_norm_ = false;
    ::oneflow::cfg::VarianceNorm variance_norm_;
      
      // optional field data_format
     public:
    bool has_data_format() const;
    const ::std::string& data_format() const;
    void clear_data_format();
    void set_data_format(const ::std::string& value);
    ::std::string* mutable_data_format();
   protected:
    bool has_data_format_ = false;
    ::std::string data_format_;
           
   public:
    int compare(const _XavierInitializerConf_& other);

    bool operator==(const _XavierInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XavierInitializerConf_& other) const;
  };

  ConstXavierInitializerConf(const ::std::shared_ptr<_XavierInitializerConf_>& data);
  ConstXavierInitializerConf(const ConstXavierInitializerConf&);
  ConstXavierInitializerConf(ConstXavierInitializerConf&&) noexcept;
  ConstXavierInitializerConf();
  ConstXavierInitializerConf(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf);
  virtual ~ConstXavierInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xavierinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field variance_norm
 public:
  bool has_variance_norm() const;
  const ::oneflow::cfg::VarianceNorm& variance_norm() const;
  // used by pybind11 only
  // required or optional field data_format
 public:
  bool has_data_format() const;
  const ::std::string& data_format() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstXavierInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXavierInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXavierInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_XavierInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XavierInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXavierInitializerConf
  void BuildFromProto(const PbMessage& proto_xavierinitializerconf);
  
  ::std::shared_ptr<_XavierInitializerConf_> data_;
};

class XavierInitializerConf final : public ConstXavierInitializerConf {
 public:
  XavierInitializerConf(const ::std::shared_ptr<_XavierInitializerConf_>& data);
  XavierInitializerConf(const XavierInitializerConf& other);
  // enable nothrow for ::std::vector<XavierInitializerConf> resize 
  XavierInitializerConf(XavierInitializerConf&&) noexcept;
  XavierInitializerConf();
  explicit XavierInitializerConf(const ::oneflow::XavierInitializerConf& proto_xavierinitializerconf);

  ~XavierInitializerConf() override;

  void InitFromProto(const PbMessage& proto_xavierinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XavierInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XavierInitializerConf& other) const;
  void Clear();
  void CopyFrom(const XavierInitializerConf& other);
  XavierInitializerConf& operator=(const XavierInitializerConf& other);

  // required or optional field variance_norm
 public:
  void clear_variance_norm();
  void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
  ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
  // required or optional field data_format
 public:
  void clear_data_format();
  void set_data_format(const ::std::string& value);
  ::std::string* mutable_data_format();

  ::std::shared_ptr<XavierInitializerConf> __SharedMutable__();
};


class ConstMsraInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _MsraInitializerConf_ {
   public:
    _MsraInitializerConf_();
    explicit _MsraInitializerConf_(const _MsraInitializerConf_& other);
    explicit _MsraInitializerConf_(_MsraInitializerConf_&& other);
    _MsraInitializerConf_(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf);
    ~_MsraInitializerConf_();

    void InitFromProto(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf);

    void ToProto(::oneflow::MsraInitializerConf* proto_msrainitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _MsraInitializerConf_& other);
  
      // optional field variance_norm
     public:
    bool has_variance_norm() const;
    const ::oneflow::cfg::VarianceNorm& variance_norm() const;
    void clear_variance_norm();
    void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
    ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
   protected:
    bool has_variance_norm_ = false;
    ::oneflow::cfg::VarianceNorm variance_norm_;
      
      // optional field data_format
     public:
    bool has_data_format() const;
    const ::std::string& data_format() const;
    void clear_data_format();
    void set_data_format(const ::std::string& value);
    ::std::string* mutable_data_format();
   protected:
    bool has_data_format_ = false;
    ::std::string data_format_;
           
   public:
    int compare(const _MsraInitializerConf_& other);

    bool operator==(const _MsraInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _MsraInitializerConf_& other) const;
  };

  ConstMsraInitializerConf(const ::std::shared_ptr<_MsraInitializerConf_>& data);
  ConstMsraInitializerConf(const ConstMsraInitializerConf&);
  ConstMsraInitializerConf(ConstMsraInitializerConf&&) noexcept;
  ConstMsraInitializerConf();
  ConstMsraInitializerConf(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf);
  virtual ~ConstMsraInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_msrainitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field variance_norm
 public:
  bool has_variance_norm() const;
  const ::oneflow::cfg::VarianceNorm& variance_norm() const;
  // used by pybind11 only
  // required or optional field data_format
 public:
  bool has_data_format() const;
  const ::std::string& data_format() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstMsraInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstMsraInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstMsraInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_MsraInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_MsraInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstMsraInitializerConf
  void BuildFromProto(const PbMessage& proto_msrainitializerconf);
  
  ::std::shared_ptr<_MsraInitializerConf_> data_;
};

class MsraInitializerConf final : public ConstMsraInitializerConf {
 public:
  MsraInitializerConf(const ::std::shared_ptr<_MsraInitializerConf_>& data);
  MsraInitializerConf(const MsraInitializerConf& other);
  // enable nothrow for ::std::vector<MsraInitializerConf> resize 
  MsraInitializerConf(MsraInitializerConf&&) noexcept;
  MsraInitializerConf();
  explicit MsraInitializerConf(const ::oneflow::MsraInitializerConf& proto_msrainitializerconf);

  ~MsraInitializerConf() override;

  void InitFromProto(const PbMessage& proto_msrainitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const MsraInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const MsraInitializerConf& other) const;
  void Clear();
  void CopyFrom(const MsraInitializerConf& other);
  MsraInitializerConf& operator=(const MsraInitializerConf& other);

  // required or optional field variance_norm
 public:
  void clear_variance_norm();
  void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
  ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
  // required or optional field data_format
 public:
  void clear_data_format();
  void set_data_format(const ::std::string& value);
  ::std::string* mutable_data_format();

  ::std::shared_ptr<MsraInitializerConf> __SharedMutable__();
};


class ConstRangeInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _RangeInitializerConf_ {
   public:
    _RangeInitializerConf_();
    explicit _RangeInitializerConf_(const _RangeInitializerConf_& other);
    explicit _RangeInitializerConf_(_RangeInitializerConf_&& other);
    _RangeInitializerConf_(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf);
    ~_RangeInitializerConf_();

    void InitFromProto(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf);

    void ToProto(::oneflow::RangeInitializerConf* proto_rangeinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RangeInitializerConf_& other);
  
      // optional field start
     public:
    bool has_start() const;
    const double& start() const;
    void clear_start();
    void set_start(const double& value);
    double* mutable_start();
   protected:
    bool has_start_ = false;
    double start_;
      
      // optional field stride
     public:
    bool has_stride() const;
    const double& stride() const;
    void clear_stride();
    void set_stride(const double& value);
    double* mutable_stride();
   protected:
    bool has_stride_ = false;
    double stride_;
      
      // optional field axis
     public:
    bool has_axis() const;
    const int64_t& axis() const;
    void clear_axis();
    void set_axis(const int64_t& value);
    int64_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int64_t axis_;
           
   public:
    int compare(const _RangeInitializerConf_& other);

    bool operator==(const _RangeInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RangeInitializerConf_& other) const;
  };

  ConstRangeInitializerConf(const ::std::shared_ptr<_RangeInitializerConf_>& data);
  ConstRangeInitializerConf(const ConstRangeInitializerConf&);
  ConstRangeInitializerConf(ConstRangeInitializerConf&&) noexcept;
  ConstRangeInitializerConf();
  ConstRangeInitializerConf(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf);
  virtual ~ConstRangeInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_rangeinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field start
 public:
  bool has_start() const;
  const double& start() const;
  // used by pybind11 only
  // required or optional field stride
 public:
  bool has_stride() const;
  const double& stride() const;
  // used by pybind11 only
  // required or optional field axis
 public:
  bool has_axis() const;
  const int64_t& axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRangeInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRangeInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRangeInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_RangeInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RangeInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRangeInitializerConf
  void BuildFromProto(const PbMessage& proto_rangeinitializerconf);
  
  ::std::shared_ptr<_RangeInitializerConf_> data_;
};

class RangeInitializerConf final : public ConstRangeInitializerConf {
 public:
  RangeInitializerConf(const ::std::shared_ptr<_RangeInitializerConf_>& data);
  RangeInitializerConf(const RangeInitializerConf& other);
  // enable nothrow for ::std::vector<RangeInitializerConf> resize 
  RangeInitializerConf(RangeInitializerConf&&) noexcept;
  RangeInitializerConf();
  explicit RangeInitializerConf(const ::oneflow::RangeInitializerConf& proto_rangeinitializerconf);

  ~RangeInitializerConf() override;

  void InitFromProto(const PbMessage& proto_rangeinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RangeInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RangeInitializerConf& other) const;
  void Clear();
  void CopyFrom(const RangeInitializerConf& other);
  RangeInitializerConf& operator=(const RangeInitializerConf& other);

  // required or optional field start
 public:
  void clear_start();
  void set_start(const double& value);
  double* mutable_start();
  // required or optional field stride
 public:
  void clear_stride();
  void set_stride(const double& value);
  double* mutable_stride();
  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int64_t& value);
  int64_t* mutable_axis();

  ::std::shared_ptr<RangeInitializerConf> __SharedMutable__();
};


class ConstIntRangeInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _IntRangeInitializerConf_ {
   public:
    _IntRangeInitializerConf_();
    explicit _IntRangeInitializerConf_(const _IntRangeInitializerConf_& other);
    explicit _IntRangeInitializerConf_(_IntRangeInitializerConf_&& other);
    _IntRangeInitializerConf_(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf);
    ~_IntRangeInitializerConf_();

    void InitFromProto(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf);

    void ToProto(::oneflow::IntRangeInitializerConf* proto_intrangeinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _IntRangeInitializerConf_& other);
  
      // optional field start
     public:
    bool has_start() const;
    const int64_t& start() const;
    void clear_start();
    void set_start(const int64_t& value);
    int64_t* mutable_start();
   protected:
    bool has_start_ = false;
    int64_t start_;
      
      // optional field stride
     public:
    bool has_stride() const;
    const int64_t& stride() const;
    void clear_stride();
    void set_stride(const int64_t& value);
    int64_t* mutable_stride();
   protected:
    bool has_stride_ = false;
    int64_t stride_;
      
      // optional field axis
     public:
    bool has_axis() const;
    const int64_t& axis() const;
    void clear_axis();
    void set_axis(const int64_t& value);
    int64_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int64_t axis_;
           
   public:
    int compare(const _IntRangeInitializerConf_& other);

    bool operator==(const _IntRangeInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _IntRangeInitializerConf_& other) const;
  };

  ConstIntRangeInitializerConf(const ::std::shared_ptr<_IntRangeInitializerConf_>& data);
  ConstIntRangeInitializerConf(const ConstIntRangeInitializerConf&);
  ConstIntRangeInitializerConf(ConstIntRangeInitializerConf&&) noexcept;
  ConstIntRangeInitializerConf();
  ConstIntRangeInitializerConf(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf);
  virtual ~ConstIntRangeInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_intrangeinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field start
 public:
  bool has_start() const;
  const int64_t& start() const;
  // used by pybind11 only
  // required or optional field stride
 public:
  bool has_stride() const;
  const int64_t& stride() const;
  // used by pybind11 only
  // required or optional field axis
 public:
  bool has_axis() const;
  const int64_t& axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstIntRangeInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstIntRangeInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstIntRangeInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_IntRangeInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_IntRangeInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstIntRangeInitializerConf
  void BuildFromProto(const PbMessage& proto_intrangeinitializerconf);
  
  ::std::shared_ptr<_IntRangeInitializerConf_> data_;
};

class IntRangeInitializerConf final : public ConstIntRangeInitializerConf {
 public:
  IntRangeInitializerConf(const ::std::shared_ptr<_IntRangeInitializerConf_>& data);
  IntRangeInitializerConf(const IntRangeInitializerConf& other);
  // enable nothrow for ::std::vector<IntRangeInitializerConf> resize 
  IntRangeInitializerConf(IntRangeInitializerConf&&) noexcept;
  IntRangeInitializerConf();
  explicit IntRangeInitializerConf(const ::oneflow::IntRangeInitializerConf& proto_intrangeinitializerconf);

  ~IntRangeInitializerConf() override;

  void InitFromProto(const PbMessage& proto_intrangeinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const IntRangeInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const IntRangeInitializerConf& other) const;
  void Clear();
  void CopyFrom(const IntRangeInitializerConf& other);
  IntRangeInitializerConf& operator=(const IntRangeInitializerConf& other);

  // required or optional field start
 public:
  void clear_start();
  void set_start(const int64_t& value);
  int64_t* mutable_start();
  // required or optional field stride
 public:
  void clear_stride();
  void set_stride(const int64_t& value);
  int64_t* mutable_stride();
  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int64_t& value);
  int64_t* mutable_axis();

  ::std::shared_ptr<IntRangeInitializerConf> __SharedMutable__();
};


class ConstVarianceScalingInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _VarianceScalingInitializerConf_ {
   public:
    _VarianceScalingInitializerConf_();
    explicit _VarianceScalingInitializerConf_(const _VarianceScalingInitializerConf_& other);
    explicit _VarianceScalingInitializerConf_(_VarianceScalingInitializerConf_&& other);
    _VarianceScalingInitializerConf_(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf);
    ~_VarianceScalingInitializerConf_();

    void InitFromProto(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf);

    void ToProto(::oneflow::VarianceScalingInitializerConf* proto_variancescalinginitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _VarianceScalingInitializerConf_& other);
  
      // optional field scale
     public:
    bool has_scale() const;
    const float& scale() const;
    void clear_scale();
    void set_scale(const float& value);
    float* mutable_scale();
   protected:
    bool has_scale_ = false;
    float scale_;
      
      // optional field variance_norm
     public:
    bool has_variance_norm() const;
    const ::oneflow::cfg::VarianceNorm& variance_norm() const;
    void clear_variance_norm();
    void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
    ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
   protected:
    bool has_variance_norm_ = false;
    ::oneflow::cfg::VarianceNorm variance_norm_;
      
      // optional field distribution
     public:
    bool has_distribution() const;
    const ::oneflow::cfg::RandomDistribution& distribution() const;
    void clear_distribution();
    void set_distribution(const ::oneflow::cfg::RandomDistribution& value);
    ::oneflow::cfg::RandomDistribution* mutable_distribution();
   protected:
    bool has_distribution_ = false;
    ::oneflow::cfg::RandomDistribution distribution_;
      
      // optional field data_format
     public:
    bool has_data_format() const;
    const ::std::string& data_format() const;
    void clear_data_format();
    void set_data_format(const ::std::string& value);
    ::std::string* mutable_data_format();
   protected:
    bool has_data_format_ = false;
    ::std::string data_format_;
           
   public:
    int compare(const _VarianceScalingInitializerConf_& other);

    bool operator==(const _VarianceScalingInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _VarianceScalingInitializerConf_& other) const;
  };

  ConstVarianceScalingInitializerConf(const ::std::shared_ptr<_VarianceScalingInitializerConf_>& data);
  ConstVarianceScalingInitializerConf(const ConstVarianceScalingInitializerConf&);
  ConstVarianceScalingInitializerConf(ConstVarianceScalingInitializerConf&&) noexcept;
  ConstVarianceScalingInitializerConf();
  ConstVarianceScalingInitializerConf(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf);
  virtual ~ConstVarianceScalingInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_variancescalinginitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field scale
 public:
  bool has_scale() const;
  const float& scale() const;
  // used by pybind11 only
  // required or optional field variance_norm
 public:
  bool has_variance_norm() const;
  const ::oneflow::cfg::VarianceNorm& variance_norm() const;
  // used by pybind11 only
  // required or optional field distribution
 public:
  bool has_distribution() const;
  const ::oneflow::cfg::RandomDistribution& distribution() const;
  // used by pybind11 only
  // required or optional field data_format
 public:
  bool has_data_format() const;
  const ::std::string& data_format() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstVarianceScalingInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstVarianceScalingInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstVarianceScalingInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_VarianceScalingInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_VarianceScalingInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstVarianceScalingInitializerConf
  void BuildFromProto(const PbMessage& proto_variancescalinginitializerconf);
  
  ::std::shared_ptr<_VarianceScalingInitializerConf_> data_;
};

class VarianceScalingInitializerConf final : public ConstVarianceScalingInitializerConf {
 public:
  VarianceScalingInitializerConf(const ::std::shared_ptr<_VarianceScalingInitializerConf_>& data);
  VarianceScalingInitializerConf(const VarianceScalingInitializerConf& other);
  // enable nothrow for ::std::vector<VarianceScalingInitializerConf> resize 
  VarianceScalingInitializerConf(VarianceScalingInitializerConf&&) noexcept;
  VarianceScalingInitializerConf();
  explicit VarianceScalingInitializerConf(const ::oneflow::VarianceScalingInitializerConf& proto_variancescalinginitializerconf);

  ~VarianceScalingInitializerConf() override;

  void InitFromProto(const PbMessage& proto_variancescalinginitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const VarianceScalingInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const VarianceScalingInitializerConf& other) const;
  void Clear();
  void CopyFrom(const VarianceScalingInitializerConf& other);
  VarianceScalingInitializerConf& operator=(const VarianceScalingInitializerConf& other);

  // required or optional field scale
 public:
  void clear_scale();
  void set_scale(const float& value);
  float* mutable_scale();
  // required or optional field variance_norm
 public:
  void clear_variance_norm();
  void set_variance_norm(const ::oneflow::cfg::VarianceNorm& value);
  ::oneflow::cfg::VarianceNorm* mutable_variance_norm();
  // required or optional field distribution
 public:
  void clear_distribution();
  void set_distribution(const ::oneflow::cfg::RandomDistribution& value);
  ::oneflow::cfg::RandomDistribution* mutable_distribution();
  // required or optional field data_format
 public:
  void clear_data_format();
  void set_data_format(const ::std::string& value);
  ::std::string* mutable_data_format();

  ::std::shared_ptr<VarianceScalingInitializerConf> __SharedMutable__();
};


class ConstEmptyInitializerConf : public ::oneflow::cfg::Message {
 public:

  class _EmptyInitializerConf_ {
   public:
    _EmptyInitializerConf_();
    explicit _EmptyInitializerConf_(const _EmptyInitializerConf_& other);
    explicit _EmptyInitializerConf_(_EmptyInitializerConf_&& other);
    _EmptyInitializerConf_(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf);
    ~_EmptyInitializerConf_();

    void InitFromProto(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf);

    void ToProto(::oneflow::EmptyInitializerConf* proto_emptyinitializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _EmptyInitializerConf_& other);
       
   public:
    int compare(const _EmptyInitializerConf_& other);

    bool operator==(const _EmptyInitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _EmptyInitializerConf_& other) const;
  };

  ConstEmptyInitializerConf(const ::std::shared_ptr<_EmptyInitializerConf_>& data);
  ConstEmptyInitializerConf(const ConstEmptyInitializerConf&);
  ConstEmptyInitializerConf(ConstEmptyInitializerConf&&) noexcept;
  ConstEmptyInitializerConf();
  ConstEmptyInitializerConf(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf);
  virtual ~ConstEmptyInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_emptyinitializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstEmptyInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstEmptyInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstEmptyInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_EmptyInitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_EmptyInitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstEmptyInitializerConf
  void BuildFromProto(const PbMessage& proto_emptyinitializerconf);
  
  ::std::shared_ptr<_EmptyInitializerConf_> data_;
};

class EmptyInitializerConf final : public ConstEmptyInitializerConf {
 public:
  EmptyInitializerConf(const ::std::shared_ptr<_EmptyInitializerConf_>& data);
  EmptyInitializerConf(const EmptyInitializerConf& other);
  // enable nothrow for ::std::vector<EmptyInitializerConf> resize 
  EmptyInitializerConf(EmptyInitializerConf&&) noexcept;
  EmptyInitializerConf();
  explicit EmptyInitializerConf(const ::oneflow::EmptyInitializerConf& proto_emptyinitializerconf);

  ~EmptyInitializerConf() override;

  void InitFromProto(const PbMessage& proto_emptyinitializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const EmptyInitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const EmptyInitializerConf& other) const;
  void Clear();
  void CopyFrom(const EmptyInitializerConf& other);
  EmptyInitializerConf& operator=(const EmptyInitializerConf& other);


  ::std::shared_ptr<EmptyInitializerConf> __SharedMutable__();
};


class ConstInitializerConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kConstantConf = 1,
    kConstantIntConf = 2,
    kRandomUniformConf = 3,
    kRandomUniformIntConf = 4,
    kRandomNormalConf = 5,
    kTruncatedNormalConf = 6,
    kXavierConf = 7,
    kMsraConf = 8,
    kRangeConf = 9,
    kIntRangeConf = 10,
    kVarianceScalingConf = 11,
    kEmptyConf = 12,
   };

  class _InitializerConf_ {
   public:
    _InitializerConf_();
    explicit _InitializerConf_(const _InitializerConf_& other);
    explicit _InitializerConf_(_InitializerConf_&& other);
    _InitializerConf_(const ::oneflow::InitializerConf& proto_initializerconf);
    ~_InitializerConf_();

    void InitFromProto(const ::oneflow::InitializerConf& proto_initializerconf);

    void ToProto(::oneflow::InitializerConf* proto_initializerconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InitializerConf_& other);
  
     // oneof field type: constant_conf
   public:
    bool has_constant_conf() const;
    void clear_constant_conf();
    const ::oneflow::cfg::ConstantInitializerConf& constant_conf() const;
      ::oneflow::cfg::ConstantInitializerConf* mutable_constant_conf();
      
     // oneof field type: constant_int_conf
   public:
    bool has_constant_int_conf() const;
    void clear_constant_int_conf();
    const ::oneflow::cfg::ConstantIntInitializerConf& constant_int_conf() const;
      ::oneflow::cfg::ConstantIntInitializerConf* mutable_constant_int_conf();
      
     // oneof field type: random_uniform_conf
   public:
    bool has_random_uniform_conf() const;
    void clear_random_uniform_conf();
    const ::oneflow::cfg::RandomUniformInitializerConf& random_uniform_conf() const;
      ::oneflow::cfg::RandomUniformInitializerConf* mutable_random_uniform_conf();
      
     // oneof field type: random_uniform_int_conf
   public:
    bool has_random_uniform_int_conf() const;
    void clear_random_uniform_int_conf();
    const ::oneflow::cfg::RandomUniformIntInitializerConf& random_uniform_int_conf() const;
      ::oneflow::cfg::RandomUniformIntInitializerConf* mutable_random_uniform_int_conf();
      
     // oneof field type: random_normal_conf
   public:
    bool has_random_normal_conf() const;
    void clear_random_normal_conf();
    const ::oneflow::cfg::RandomNormalInitializerConf& random_normal_conf() const;
      ::oneflow::cfg::RandomNormalInitializerConf* mutable_random_normal_conf();
      
     // oneof field type: truncated_normal_conf
   public:
    bool has_truncated_normal_conf() const;
    void clear_truncated_normal_conf();
    const ::oneflow::cfg::TruncatedNormalInitializerConf& truncated_normal_conf() const;
      ::oneflow::cfg::TruncatedNormalInitializerConf* mutable_truncated_normal_conf();
      
     // oneof field type: xavier_conf
   public:
    bool has_xavier_conf() const;
    void clear_xavier_conf();
    const ::oneflow::cfg::XavierInitializerConf& xavier_conf() const;
      ::oneflow::cfg::XavierInitializerConf* mutable_xavier_conf();
      
     // oneof field type: msra_conf
   public:
    bool has_msra_conf() const;
    void clear_msra_conf();
    const ::oneflow::cfg::MsraInitializerConf& msra_conf() const;
      ::oneflow::cfg::MsraInitializerConf* mutable_msra_conf();
      
     // oneof field type: range_conf
   public:
    bool has_range_conf() const;
    void clear_range_conf();
    const ::oneflow::cfg::RangeInitializerConf& range_conf() const;
      ::oneflow::cfg::RangeInitializerConf* mutable_range_conf();
      
     // oneof field type: int_range_conf
   public:
    bool has_int_range_conf() const;
    void clear_int_range_conf();
    const ::oneflow::cfg::IntRangeInitializerConf& int_range_conf() const;
      ::oneflow::cfg::IntRangeInitializerConf* mutable_int_range_conf();
      
     // oneof field type: variance_scaling_conf
   public:
    bool has_variance_scaling_conf() const;
    void clear_variance_scaling_conf();
    const ::oneflow::cfg::VarianceScalingInitializerConf& variance_scaling_conf() const;
      ::oneflow::cfg::VarianceScalingInitializerConf* mutable_variance_scaling_conf();
      
     // oneof field type: empty_conf
   public:
    bool has_empty_conf() const;
    void clear_empty_conf();
    const ::oneflow::cfg::EmptyInitializerConf& empty_conf() const;
      ::oneflow::cfg::EmptyInitializerConf* mutable_empty_conf();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _InitializerConf_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char constant_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf>)];
            char constant_int_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf>)];
            char random_uniform_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf>)];
            char random_uniform_int_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf>)];
            char random_normal_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf>)];
            char truncated_normal_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf>)];
            char xavier_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::XavierInitializerConf>)];
            char msra_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::MsraInitializerConf>)];
            char range_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::RangeInitializerConf>)];
            char int_range_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf>)];
            char variance_scaling_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf>)];
            char empty_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf>)];
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _InitializerConf_& other);

    bool operator==(const _InitializerConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InitializerConf_& other) const;
  };

  ConstInitializerConf(const ::std::shared_ptr<_InitializerConf_>& data);
  ConstInitializerConf(const ConstInitializerConf&);
  ConstInitializerConf(ConstInitializerConf&&) noexcept;
  ConstInitializerConf();
  ConstInitializerConf(const ::oneflow::InitializerConf& proto_initializerconf);
  virtual ~ConstInitializerConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_initializerconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: constant_conf
 public:
  bool has_constant_conf() const;
  const ::oneflow::cfg::ConstantInitializerConf& constant_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConstantInitializerConf> shared_const_constant_conf() const;
 // oneof field type: constant_int_conf
 public:
  bool has_constant_int_conf() const;
  const ::oneflow::cfg::ConstantIntInitializerConf& constant_int_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConstantIntInitializerConf> shared_const_constant_int_conf() const;
 // oneof field type: random_uniform_conf
 public:
  bool has_random_uniform_conf() const;
  const ::oneflow::cfg::RandomUniformInitializerConf& random_uniform_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRandomUniformInitializerConf> shared_const_random_uniform_conf() const;
 // oneof field type: random_uniform_int_conf
 public:
  bool has_random_uniform_int_conf() const;
  const ::oneflow::cfg::RandomUniformIntInitializerConf& random_uniform_int_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRandomUniformIntInitializerConf> shared_const_random_uniform_int_conf() const;
 // oneof field type: random_normal_conf
 public:
  bool has_random_normal_conf() const;
  const ::oneflow::cfg::RandomNormalInitializerConf& random_normal_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRandomNormalInitializerConf> shared_const_random_normal_conf() const;
 // oneof field type: truncated_normal_conf
 public:
  bool has_truncated_normal_conf() const;
  const ::oneflow::cfg::TruncatedNormalInitializerConf& truncated_normal_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTruncatedNormalInitializerConf> shared_const_truncated_normal_conf() const;
 // oneof field type: xavier_conf
 public:
  bool has_xavier_conf() const;
  const ::oneflow::cfg::XavierInitializerConf& xavier_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXavierInitializerConf> shared_const_xavier_conf() const;
 // oneof field type: msra_conf
 public:
  bool has_msra_conf() const;
  const ::oneflow::cfg::MsraInitializerConf& msra_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMsraInitializerConf> shared_const_msra_conf() const;
 // oneof field type: range_conf
 public:
  bool has_range_conf() const;
  const ::oneflow::cfg::RangeInitializerConf& range_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRangeInitializerConf> shared_const_range_conf() const;
 // oneof field type: int_range_conf
 public:
  bool has_int_range_conf() const;
  const ::oneflow::cfg::IntRangeInitializerConf& int_range_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstIntRangeInitializerConf> shared_const_int_range_conf() const;
 // oneof field type: variance_scaling_conf
 public:
  bool has_variance_scaling_conf() const;
  const ::oneflow::cfg::VarianceScalingInitializerConf& variance_scaling_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstVarianceScalingInitializerConf> shared_const_variance_scaling_conf() const;
 // oneof field type: empty_conf
 public:
  bool has_empty_conf() const;
  const ::oneflow::cfg::EmptyInitializerConf& empty_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstEmptyInitializerConf> shared_const_empty_conf() const;
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstInitializerConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInitializerConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInitializerConf& other) const;
 protected:
  const ::std::shared_ptr<_InitializerConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InitializerConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInitializerConf
  void BuildFromProto(const PbMessage& proto_initializerconf);
  
  ::std::shared_ptr<_InitializerConf_> data_;
};

class InitializerConf final : public ConstInitializerConf {
 public:
  InitializerConf(const ::std::shared_ptr<_InitializerConf_>& data);
  InitializerConf(const InitializerConf& other);
  // enable nothrow for ::std::vector<InitializerConf> resize 
  InitializerConf(InitializerConf&&) noexcept;
  InitializerConf();
  explicit InitializerConf(const ::oneflow::InitializerConf& proto_initializerconf);

  ~InitializerConf() override;

  void InitFromProto(const PbMessage& proto_initializerconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InitializerConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InitializerConf& other) const;
  void Clear();
  void CopyFrom(const InitializerConf& other);
  InitializerConf& operator=(const InitializerConf& other);

  void clear_constant_conf();
  ::oneflow::cfg::ConstantInitializerConf* mutable_constant_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstantInitializerConf> shared_mutable_constant_conf();
  void clear_constant_int_conf();
  ::oneflow::cfg::ConstantIntInitializerConf* mutable_constant_int_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstantIntInitializerConf> shared_mutable_constant_int_conf();
  void clear_random_uniform_conf();
  ::oneflow::cfg::RandomUniformInitializerConf* mutable_random_uniform_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RandomUniformInitializerConf> shared_mutable_random_uniform_conf();
  void clear_random_uniform_int_conf();
  ::oneflow::cfg::RandomUniformIntInitializerConf* mutable_random_uniform_int_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RandomUniformIntInitializerConf> shared_mutable_random_uniform_int_conf();
  void clear_random_normal_conf();
  ::oneflow::cfg::RandomNormalInitializerConf* mutable_random_normal_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RandomNormalInitializerConf> shared_mutable_random_normal_conf();
  void clear_truncated_normal_conf();
  ::oneflow::cfg::TruncatedNormalInitializerConf* mutable_truncated_normal_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TruncatedNormalInitializerConf> shared_mutable_truncated_normal_conf();
  void clear_xavier_conf();
  ::oneflow::cfg::XavierInitializerConf* mutable_xavier_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XavierInitializerConf> shared_mutable_xavier_conf();
  void clear_msra_conf();
  ::oneflow::cfg::MsraInitializerConf* mutable_msra_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MsraInitializerConf> shared_mutable_msra_conf();
  void clear_range_conf();
  ::oneflow::cfg::RangeInitializerConf* mutable_range_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RangeInitializerConf> shared_mutable_range_conf();
  void clear_int_range_conf();
  ::oneflow::cfg::IntRangeInitializerConf* mutable_int_range_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::IntRangeInitializerConf> shared_mutable_int_range_conf();
  void clear_variance_scaling_conf();
  ::oneflow::cfg::VarianceScalingInitializerConf* mutable_variance_scaling_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::VarianceScalingInitializerConf> shared_mutable_variance_scaling_conf();
  void clear_empty_conf();
  ::oneflow::cfg::EmptyInitializerConf* mutable_empty_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::EmptyInitializerConf> shared_mutable_empty_conf();

  ::std::shared_ptr<InitializerConf> __SharedMutable__();
};


class ConstInitializeWithSnapshotConf : public ::oneflow::cfg::Message {
 public:

  class _InitializeWithSnapshotConf_ {
   public:
    _InitializeWithSnapshotConf_();
    explicit _InitializeWithSnapshotConf_(const _InitializeWithSnapshotConf_& other);
    explicit _InitializeWithSnapshotConf_(_InitializeWithSnapshotConf_&& other);
    _InitializeWithSnapshotConf_(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf);
    ~_InitializeWithSnapshotConf_();

    void InitFromProto(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf);

    void ToProto(::oneflow::InitializeWithSnapshotConf* proto_initializewithsnapshotconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InitializeWithSnapshotConf_& other);
  
      // optional field path
     public:
    bool has_path() const;
    const ::std::string& path() const;
    void clear_path();
    void set_path(const ::std::string& value);
    ::std::string* mutable_path();
   protected:
    bool has_path_ = false;
    ::std::string path_;
      
      // optional field key
     public:
    bool has_key() const;
    const ::std::string& key() const;
    void clear_key();
    void set_key(const ::std::string& value);
    ::std::string* mutable_key();
   protected:
    bool has_key_ = false;
    ::std::string key_;
           
   public:
    int compare(const _InitializeWithSnapshotConf_& other);

    bool operator==(const _InitializeWithSnapshotConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InitializeWithSnapshotConf_& other) const;
  };

  ConstInitializeWithSnapshotConf(const ::std::shared_ptr<_InitializeWithSnapshotConf_>& data);
  ConstInitializeWithSnapshotConf(const ConstInitializeWithSnapshotConf&);
  ConstInitializeWithSnapshotConf(ConstInitializeWithSnapshotConf&&) noexcept;
  ConstInitializeWithSnapshotConf();
  ConstInitializeWithSnapshotConf(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf);
  virtual ~ConstInitializeWithSnapshotConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_initializewithsnapshotconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field path
 public:
  bool has_path() const;
  const ::std::string& path() const;
  // used by pybind11 only
  // required or optional field key
 public:
  bool has_key() const;
  const ::std::string& key() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstInitializeWithSnapshotConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInitializeWithSnapshotConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInitializeWithSnapshotConf& other) const;
 protected:
  const ::std::shared_ptr<_InitializeWithSnapshotConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InitializeWithSnapshotConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInitializeWithSnapshotConf
  void BuildFromProto(const PbMessage& proto_initializewithsnapshotconf);
  
  ::std::shared_ptr<_InitializeWithSnapshotConf_> data_;
};

class InitializeWithSnapshotConf final : public ConstInitializeWithSnapshotConf {
 public:
  InitializeWithSnapshotConf(const ::std::shared_ptr<_InitializeWithSnapshotConf_>& data);
  InitializeWithSnapshotConf(const InitializeWithSnapshotConf& other);
  // enable nothrow for ::std::vector<InitializeWithSnapshotConf> resize 
  InitializeWithSnapshotConf(InitializeWithSnapshotConf&&) noexcept;
  InitializeWithSnapshotConf();
  explicit InitializeWithSnapshotConf(const ::oneflow::InitializeWithSnapshotConf& proto_initializewithsnapshotconf);

  ~InitializeWithSnapshotConf() override;

  void InitFromProto(const PbMessage& proto_initializewithsnapshotconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InitializeWithSnapshotConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InitializeWithSnapshotConf& other) const;
  void Clear();
  void CopyFrom(const InitializeWithSnapshotConf& other);
  InitializeWithSnapshotConf& operator=(const InitializeWithSnapshotConf& other);

  // required or optional field path
 public:
  void clear_path();
  void set_path(const ::std::string& value);
  ::std::string* mutable_path();
  // required or optional field key
 public:
  void clear_key();
  void set_key(const ::std::string& value);
  ::std::string* mutable_key();

  ::std::shared_ptr<InitializeWithSnapshotConf> __SharedMutable__();
};













































} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::VarianceNorm> {
  std::size_t operator()(::oneflow::cfg::VarianceNorm enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};
template<>
struct hash<::oneflow::cfg::RandomDistribution> {
  std::size_t operator()(::oneflow::cfg::RandomDistribution enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstConstantInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstConstantInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstantInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstantInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstConstantIntInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstConstantIntInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstantIntInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstantIntInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRandomUniformInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRandomUniformInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RandomUniformInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::RandomUniformInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRandomUniformIntInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRandomUniformIntInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RandomUniformIntInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::RandomUniformIntInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRandomNormalInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRandomNormalInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RandomNormalInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::RandomNormalInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTruncatedNormalInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstTruncatedNormalInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TruncatedNormalInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::TruncatedNormalInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXavierInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstXavierInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XavierInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::XavierInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstMsraInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstMsraInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::MsraInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::MsraInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstRangeInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstRangeInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::RangeInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::RangeInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstIntRangeInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstIntRangeInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::IntRangeInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::IntRangeInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstVarianceScalingInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstVarianceScalingInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::VarianceScalingInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::VarianceScalingInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstEmptyInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstEmptyInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::EmptyInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::EmptyInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstInitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InitializerConf> {
  std::size_t operator()(const ::oneflow::cfg::InitializerConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInitializeWithSnapshotConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstInitializeWithSnapshotConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InitializeWithSnapshotConf> {
  std::size_t operator()(const ::oneflow::cfg::InitializeWithSnapshotConf& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_JOB_INITIALIZER_CONF_CFG_H_