#include "oneflow/core/vm/instruction.cfg.h"
#include "oneflow/core/vm/instruction.pb.h"

namespace oneflow {
namespace vm {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::_CurrentGlobalDeviceIdProto_() { Clear(); }
ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::_CurrentGlobalDeviceIdProto_(const _CurrentGlobalDeviceIdProto_& other) { CopyFrom(other); }
ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::_CurrentGlobalDeviceIdProto_(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto) {
  InitFromProto(proto_currentglobaldeviceidproto);
}
ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::_CurrentGlobalDeviceIdProto_(_CurrentGlobalDeviceIdProto_&& other) = default;
ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::~_CurrentGlobalDeviceIdProto_() = default;

void ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::InitFromProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto) {
  Clear();
    
}

void ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::ToProto(::oneflow::vm::CurrentGlobalDeviceIdProto* proto_currentglobaldeviceidproto) const {
  proto_currentglobaldeviceidproto->Clear();

}

::std::string ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::DebugString() const {
  ::oneflow::vm::CurrentGlobalDeviceIdProto proto_currentglobaldeviceidproto;
  this->ToProto(&proto_currentglobaldeviceidproto);
  return proto_currentglobaldeviceidproto.DebugString();
}

void ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::Clear() {
}

void ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::CopyFrom(const _CurrentGlobalDeviceIdProto_& other) {
}



int ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::compare(const _CurrentGlobalDeviceIdProto_& other) {
  return 0;
}

bool ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::operator==(const _CurrentGlobalDeviceIdProto_& other) const {
  return true
  ;
}

std::size_t ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::__CalcHash__() const {
  return 0
  ;
}

bool ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_::operator<(const _CurrentGlobalDeviceIdProto_& other) const {
  return false
  ;
}

using _CurrentGlobalDeviceIdProto_ =  ConstCurrentGlobalDeviceIdProto::_CurrentGlobalDeviceIdProto_;
ConstCurrentGlobalDeviceIdProto::ConstCurrentGlobalDeviceIdProto(const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& data): data_(data) {}
ConstCurrentGlobalDeviceIdProto::ConstCurrentGlobalDeviceIdProto(): data_(::std::make_shared<_CurrentGlobalDeviceIdProto_>()) {}
ConstCurrentGlobalDeviceIdProto::ConstCurrentGlobalDeviceIdProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto) {
  BuildFromProto(proto_currentglobaldeviceidproto);
}
ConstCurrentGlobalDeviceIdProto::ConstCurrentGlobalDeviceIdProto(const ConstCurrentGlobalDeviceIdProto&) = default;
ConstCurrentGlobalDeviceIdProto::ConstCurrentGlobalDeviceIdProto(ConstCurrentGlobalDeviceIdProto&&) noexcept = default;
ConstCurrentGlobalDeviceIdProto::~ConstCurrentGlobalDeviceIdProto() = default;

void ConstCurrentGlobalDeviceIdProto::ToProto(PbMessage* proto_currentglobaldeviceidproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::CurrentGlobalDeviceIdProto*>(proto_currentglobaldeviceidproto));
}
  
::std::string ConstCurrentGlobalDeviceIdProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstCurrentGlobalDeviceIdProto::__Empty__() const {
  return !data_;
}

int ConstCurrentGlobalDeviceIdProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstCurrentGlobalDeviceIdProto::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstCurrentGlobalDeviceIdProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstCurrentGlobalDeviceIdProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstCurrentGlobalDeviceIdProto> ConstCurrentGlobalDeviceIdProto::__SharedConst__() const {
  return ::std::make_shared<ConstCurrentGlobalDeviceIdProto>(data_);
}
int64_t ConstCurrentGlobalDeviceIdProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstCurrentGlobalDeviceIdProto::operator==(const ConstCurrentGlobalDeviceIdProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstCurrentGlobalDeviceIdProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstCurrentGlobalDeviceIdProto::operator<(const ConstCurrentGlobalDeviceIdProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& ConstCurrentGlobalDeviceIdProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_> default_ptr = std::make_shared<_CurrentGlobalDeviceIdProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& ConstCurrentGlobalDeviceIdProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _CurrentGlobalDeviceIdProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstCurrentGlobalDeviceIdProto
void ConstCurrentGlobalDeviceIdProto::BuildFromProto(const PbMessage& proto_currentglobaldeviceidproto) {
  data_ = ::std::make_shared<_CurrentGlobalDeviceIdProto_>(dynamic_cast<const ::oneflow::vm::CurrentGlobalDeviceIdProto&>(proto_currentglobaldeviceidproto));
}

CurrentGlobalDeviceIdProto::CurrentGlobalDeviceIdProto(const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& data)
  : ConstCurrentGlobalDeviceIdProto(data) {}
CurrentGlobalDeviceIdProto::CurrentGlobalDeviceIdProto(const CurrentGlobalDeviceIdProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<CurrentGlobalDeviceIdProto> resize
CurrentGlobalDeviceIdProto::CurrentGlobalDeviceIdProto(CurrentGlobalDeviceIdProto&&) noexcept = default; 
CurrentGlobalDeviceIdProto::CurrentGlobalDeviceIdProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto) {
  InitFromProto(proto_currentglobaldeviceidproto);
}
CurrentGlobalDeviceIdProto::CurrentGlobalDeviceIdProto() = default;

CurrentGlobalDeviceIdProto::~CurrentGlobalDeviceIdProto() = default;

void CurrentGlobalDeviceIdProto::InitFromProto(const PbMessage& proto_currentglobaldeviceidproto) {
  BuildFromProto(proto_currentglobaldeviceidproto);
}
  
void* CurrentGlobalDeviceIdProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool CurrentGlobalDeviceIdProto::operator==(const CurrentGlobalDeviceIdProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t CurrentGlobalDeviceIdProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool CurrentGlobalDeviceIdProto::operator<(const CurrentGlobalDeviceIdProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void CurrentGlobalDeviceIdProto::Clear() {
  if (data_) { data_.reset(); }
}
void CurrentGlobalDeviceIdProto::CopyFrom(const CurrentGlobalDeviceIdProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
CurrentGlobalDeviceIdProto& CurrentGlobalDeviceIdProto::operator=(const CurrentGlobalDeviceIdProto& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<CurrentGlobalDeviceIdProto> CurrentGlobalDeviceIdProto::__SharedMutable__() {
  return ::std::make_shared<CurrentGlobalDeviceIdProto>(__SharedPtr__());
}
ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::_SoleMirroredObjectProto_() { Clear(); }
ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::_SoleMirroredObjectProto_(const _SoleMirroredObjectProto_& other) { CopyFrom(other); }
ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::_SoleMirroredObjectProto_(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto) {
  InitFromProto(proto_solemirroredobjectproto);
}
ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::_SoleMirroredObjectProto_(_SoleMirroredObjectProto_&& other) = default;
ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::~_SoleMirroredObjectProto_() = default;

void ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::InitFromProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto) {
  Clear();
    
}

void ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::ToProto(::oneflow::vm::SoleMirroredObjectProto* proto_solemirroredobjectproto) const {
  proto_solemirroredobjectproto->Clear();

}

::std::string ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::DebugString() const {
  ::oneflow::vm::SoleMirroredObjectProto proto_solemirroredobjectproto;
  this->ToProto(&proto_solemirroredobjectproto);
  return proto_solemirroredobjectproto.DebugString();
}

void ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::Clear() {
}

void ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::CopyFrom(const _SoleMirroredObjectProto_& other) {
}



int ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::compare(const _SoleMirroredObjectProto_& other) {
  return 0;
}

bool ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::operator==(const _SoleMirroredObjectProto_& other) const {
  return true
  ;
}

std::size_t ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::__CalcHash__() const {
  return 0
  ;
}

bool ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_::operator<(const _SoleMirroredObjectProto_& other) const {
  return false
  ;
}

using _SoleMirroredObjectProto_ =  ConstSoleMirroredObjectProto::_SoleMirroredObjectProto_;
ConstSoleMirroredObjectProto::ConstSoleMirroredObjectProto(const ::std::shared_ptr<_SoleMirroredObjectProto_>& data): data_(data) {}
ConstSoleMirroredObjectProto::ConstSoleMirroredObjectProto(): data_(::std::make_shared<_SoleMirroredObjectProto_>()) {}
ConstSoleMirroredObjectProto::ConstSoleMirroredObjectProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto) {
  BuildFromProto(proto_solemirroredobjectproto);
}
ConstSoleMirroredObjectProto::ConstSoleMirroredObjectProto(const ConstSoleMirroredObjectProto&) = default;
ConstSoleMirroredObjectProto::ConstSoleMirroredObjectProto(ConstSoleMirroredObjectProto&&) noexcept = default;
ConstSoleMirroredObjectProto::~ConstSoleMirroredObjectProto() = default;

void ConstSoleMirroredObjectProto::ToProto(PbMessage* proto_solemirroredobjectproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::SoleMirroredObjectProto*>(proto_solemirroredobjectproto));
}
  
::std::string ConstSoleMirroredObjectProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstSoleMirroredObjectProto::__Empty__() const {
  return !data_;
}

int ConstSoleMirroredObjectProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstSoleMirroredObjectProto::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstSoleMirroredObjectProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstSoleMirroredObjectProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstSoleMirroredObjectProto> ConstSoleMirroredObjectProto::__SharedConst__() const {
  return ::std::make_shared<ConstSoleMirroredObjectProto>(data_);
}
int64_t ConstSoleMirroredObjectProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstSoleMirroredObjectProto::operator==(const ConstSoleMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstSoleMirroredObjectProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstSoleMirroredObjectProto::operator<(const ConstSoleMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_SoleMirroredObjectProto_>& ConstSoleMirroredObjectProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_SoleMirroredObjectProto_> default_ptr = std::make_shared<_SoleMirroredObjectProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_SoleMirroredObjectProto_>& ConstSoleMirroredObjectProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _SoleMirroredObjectProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstSoleMirroredObjectProto
void ConstSoleMirroredObjectProto::BuildFromProto(const PbMessage& proto_solemirroredobjectproto) {
  data_ = ::std::make_shared<_SoleMirroredObjectProto_>(dynamic_cast<const ::oneflow::vm::SoleMirroredObjectProto&>(proto_solemirroredobjectproto));
}

SoleMirroredObjectProto::SoleMirroredObjectProto(const ::std::shared_ptr<_SoleMirroredObjectProto_>& data)
  : ConstSoleMirroredObjectProto(data) {}
SoleMirroredObjectProto::SoleMirroredObjectProto(const SoleMirroredObjectProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<SoleMirroredObjectProto> resize
SoleMirroredObjectProto::SoleMirroredObjectProto(SoleMirroredObjectProto&&) noexcept = default; 
SoleMirroredObjectProto::SoleMirroredObjectProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto) {
  InitFromProto(proto_solemirroredobjectproto);
}
SoleMirroredObjectProto::SoleMirroredObjectProto() = default;

SoleMirroredObjectProto::~SoleMirroredObjectProto() = default;

void SoleMirroredObjectProto::InitFromProto(const PbMessage& proto_solemirroredobjectproto) {
  BuildFromProto(proto_solemirroredobjectproto);
}
  
void* SoleMirroredObjectProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool SoleMirroredObjectProto::operator==(const SoleMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t SoleMirroredObjectProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool SoleMirroredObjectProto::operator<(const SoleMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void SoleMirroredObjectProto::Clear() {
  if (data_) { data_.reset(); }
}
void SoleMirroredObjectProto::CopyFrom(const SoleMirroredObjectProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
SoleMirroredObjectProto& SoleMirroredObjectProto::operator=(const SoleMirroredObjectProto& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<SoleMirroredObjectProto> SoleMirroredObjectProto::__SharedMutable__() {
  return ::std::make_shared<SoleMirroredObjectProto>(__SharedPtr__());
}
ConstAllMirroredObjectProto::_AllMirroredObjectProto_::_AllMirroredObjectProto_() { Clear(); }
ConstAllMirroredObjectProto::_AllMirroredObjectProto_::_AllMirroredObjectProto_(const _AllMirroredObjectProto_& other) { CopyFrom(other); }
ConstAllMirroredObjectProto::_AllMirroredObjectProto_::_AllMirroredObjectProto_(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto) {
  InitFromProto(proto_allmirroredobjectproto);
}
ConstAllMirroredObjectProto::_AllMirroredObjectProto_::_AllMirroredObjectProto_(_AllMirroredObjectProto_&& other) = default;
ConstAllMirroredObjectProto::_AllMirroredObjectProto_::~_AllMirroredObjectProto_() = default;

void ConstAllMirroredObjectProto::_AllMirroredObjectProto_::InitFromProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto) {
  Clear();
    
}

void ConstAllMirroredObjectProto::_AllMirroredObjectProto_::ToProto(::oneflow::vm::AllMirroredObjectProto* proto_allmirroredobjectproto) const {
  proto_allmirroredobjectproto->Clear();

}

::std::string ConstAllMirroredObjectProto::_AllMirroredObjectProto_::DebugString() const {
  ::oneflow::vm::AllMirroredObjectProto proto_allmirroredobjectproto;
  this->ToProto(&proto_allmirroredobjectproto);
  return proto_allmirroredobjectproto.DebugString();
}

void ConstAllMirroredObjectProto::_AllMirroredObjectProto_::Clear() {
}

void ConstAllMirroredObjectProto::_AllMirroredObjectProto_::CopyFrom(const _AllMirroredObjectProto_& other) {
}



int ConstAllMirroredObjectProto::_AllMirroredObjectProto_::compare(const _AllMirroredObjectProto_& other) {
  return 0;
}

bool ConstAllMirroredObjectProto::_AllMirroredObjectProto_::operator==(const _AllMirroredObjectProto_& other) const {
  return true
  ;
}

std::size_t ConstAllMirroredObjectProto::_AllMirroredObjectProto_::__CalcHash__() const {
  return 0
  ;
}

bool ConstAllMirroredObjectProto::_AllMirroredObjectProto_::operator<(const _AllMirroredObjectProto_& other) const {
  return false
  ;
}

using _AllMirroredObjectProto_ =  ConstAllMirroredObjectProto::_AllMirroredObjectProto_;
ConstAllMirroredObjectProto::ConstAllMirroredObjectProto(const ::std::shared_ptr<_AllMirroredObjectProto_>& data): data_(data) {}
ConstAllMirroredObjectProto::ConstAllMirroredObjectProto(): data_(::std::make_shared<_AllMirroredObjectProto_>()) {}
ConstAllMirroredObjectProto::ConstAllMirroredObjectProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto) {
  BuildFromProto(proto_allmirroredobjectproto);
}
ConstAllMirroredObjectProto::ConstAllMirroredObjectProto(const ConstAllMirroredObjectProto&) = default;
ConstAllMirroredObjectProto::ConstAllMirroredObjectProto(ConstAllMirroredObjectProto&&) noexcept = default;
ConstAllMirroredObjectProto::~ConstAllMirroredObjectProto() = default;

void ConstAllMirroredObjectProto::ToProto(PbMessage* proto_allmirroredobjectproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::AllMirroredObjectProto*>(proto_allmirroredobjectproto));
}
  
::std::string ConstAllMirroredObjectProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAllMirroredObjectProto::__Empty__() const {
  return !data_;
}

int ConstAllMirroredObjectProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstAllMirroredObjectProto::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstAllMirroredObjectProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAllMirroredObjectProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstAllMirroredObjectProto> ConstAllMirroredObjectProto::__SharedConst__() const {
  return ::std::make_shared<ConstAllMirroredObjectProto>(data_);
}
int64_t ConstAllMirroredObjectProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAllMirroredObjectProto::operator==(const ConstAllMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAllMirroredObjectProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAllMirroredObjectProto::operator<(const ConstAllMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AllMirroredObjectProto_>& ConstAllMirroredObjectProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AllMirroredObjectProto_> default_ptr = std::make_shared<_AllMirroredObjectProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_AllMirroredObjectProto_>& ConstAllMirroredObjectProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _AllMirroredObjectProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAllMirroredObjectProto
void ConstAllMirroredObjectProto::BuildFromProto(const PbMessage& proto_allmirroredobjectproto) {
  data_ = ::std::make_shared<_AllMirroredObjectProto_>(dynamic_cast<const ::oneflow::vm::AllMirroredObjectProto&>(proto_allmirroredobjectproto));
}

AllMirroredObjectProto::AllMirroredObjectProto(const ::std::shared_ptr<_AllMirroredObjectProto_>& data)
  : ConstAllMirroredObjectProto(data) {}
AllMirroredObjectProto::AllMirroredObjectProto(const AllMirroredObjectProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AllMirroredObjectProto> resize
AllMirroredObjectProto::AllMirroredObjectProto(AllMirroredObjectProto&&) noexcept = default; 
AllMirroredObjectProto::AllMirroredObjectProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto) {
  InitFromProto(proto_allmirroredobjectproto);
}
AllMirroredObjectProto::AllMirroredObjectProto() = default;

AllMirroredObjectProto::~AllMirroredObjectProto() = default;

void AllMirroredObjectProto::InitFromProto(const PbMessage& proto_allmirroredobjectproto) {
  BuildFromProto(proto_allmirroredobjectproto);
}
  
void* AllMirroredObjectProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool AllMirroredObjectProto::operator==(const AllMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AllMirroredObjectProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AllMirroredObjectProto::operator<(const AllMirroredObjectProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AllMirroredObjectProto::Clear() {
  if (data_) { data_.reset(); }
}
void AllMirroredObjectProto::CopyFrom(const AllMirroredObjectProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AllMirroredObjectProto& AllMirroredObjectProto::operator=(const AllMirroredObjectProto& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<AllMirroredObjectProto> AllMirroredObjectProto::__SharedMutable__() {
  return ::std::make_shared<AllMirroredObjectProto>(__SharedPtr__());
}
ConstOperandProto::_OperandProto_::_OperandProto_() { Clear(); }
ConstOperandProto::_OperandProto_::_OperandProto_(const _OperandProto_& other) { CopyFrom(other); }
ConstOperandProto::_OperandProto_::_OperandProto_(const ::oneflow::vm::OperandProto& proto_operandproto) {
  InitFromProto(proto_operandproto);
}
ConstOperandProto::_OperandProto_::_OperandProto_(_OperandProto_&& other) = default;
ConstOperandProto::_OperandProto_::~_OperandProto_() = default;

void ConstOperandProto::_OperandProto_::InitFromProto(const ::oneflow::vm::OperandProto& proto_operandproto) {
  Clear();
  // required_or_optional field: logical_object_id
  if (proto_operandproto.has_logical_object_id()) {
    set_logical_object_id(proto_operandproto.logical_object_id());
  }
  // oneof field: operand_type
  OperandTypeCase operand_type_case = OperandTypeCase(int(proto_operandproto.operand_type_case()));
  switch (operand_type_case) {
    case kCurrentGlobalDeviceId: {
      *mutable_current_global_device_id() = ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto(proto_operandproto.current_global_device_id());
      break;
  }
    case kSoleMirroredObject: {
      *mutable_sole_mirrored_object() = ::oneflow::vm::cfg::SoleMirroredObjectProto(proto_operandproto.sole_mirrored_object());
      break;
  }
    case kAllMirroredObject: {
      *mutable_all_mirrored_object() = ::oneflow::vm::cfg::AllMirroredObjectProto(proto_operandproto.all_mirrored_object());
      break;
  }
    case OPERAND_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstOperandProto::_OperandProto_::ToProto(::oneflow::vm::OperandProto* proto_operandproto) const {
  proto_operandproto->Clear();
  // required_or_optional field: logical_object_id
  if (this->has_logical_object_id()) {
    proto_operandproto->set_logical_object_id(logical_object_id());
    }

  // oneof field: operand_type
  ::oneflow::vm::OperandProto::OperandTypeCase operand_type_case = ::oneflow::vm::OperandProto::OperandTypeCase(int(this->operand_type_case()));
  switch (operand_type_case) {
    case ::oneflow::vm::OperandProto::kCurrentGlobalDeviceId: {
      ::oneflow::vm::CurrentGlobalDeviceIdProto of_proto_current_global_device_id;
      current_global_device_id().ToProto(&of_proto_current_global_device_id);
      proto_operandproto->mutable_current_global_device_id()->CopyFrom(of_proto_current_global_device_id);
      break;
    }
    case ::oneflow::vm::OperandProto::kSoleMirroredObject: {
      ::oneflow::vm::SoleMirroredObjectProto of_proto_sole_mirrored_object;
      sole_mirrored_object().ToProto(&of_proto_sole_mirrored_object);
      proto_operandproto->mutable_sole_mirrored_object()->CopyFrom(of_proto_sole_mirrored_object);
      break;
    }
    case ::oneflow::vm::OperandProto::kAllMirroredObject: {
      ::oneflow::vm::AllMirroredObjectProto of_proto_all_mirrored_object;
      all_mirrored_object().ToProto(&of_proto_all_mirrored_object);
      proto_operandproto->mutable_all_mirrored_object()->CopyFrom(of_proto_all_mirrored_object);
      break;
    }
    case ::oneflow::vm::OperandProto::OPERAND_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstOperandProto::_OperandProto_::DebugString() const {
  ::oneflow::vm::OperandProto proto_operandproto;
  this->ToProto(&proto_operandproto);
  return proto_operandproto.DebugString();
}

void ConstOperandProto::_OperandProto_::Clear() {
  clear_logical_object_id();
  clear_operand_type();
}

void ConstOperandProto::_OperandProto_::CopyFrom(const _OperandProto_& other) {
  if (other.has_logical_object_id()) {
    set_logical_object_id(other.logical_object_id());
  } else {
    clear_logical_object_id();
  }
  operand_type_copy_from(other);
}


// optional field logical_object_id
bool ConstOperandProto::_OperandProto_::has_logical_object_id() const {
  return has_logical_object_id_;
}
const int64_t& ConstOperandProto::_OperandProto_::logical_object_id() const {
  if (has_logical_object_id_) { return logical_object_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstOperandProto::_OperandProto_::clear_logical_object_id() {
  has_logical_object_id_ = false;
}
void ConstOperandProto::_OperandProto_::set_logical_object_id(const int64_t& value) {
  logical_object_id_ = value;
  has_logical_object_id_ = true;
}
int64_t* ConstOperandProto::_OperandProto_::mutable_logical_object_id() {
  has_logical_object_id_ = true;
  return &logical_object_id_;
}

// oneof field operand_type: current_global_device_id
bool ConstOperandProto::_OperandProto_::has_current_global_device_id() const {
  return operand_type_case() == kCurrentGlobalDeviceId;
}
void ConstOperandProto::_OperandProto_::clear_current_global_device_id() {
  if (has_current_global_device_id()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.current_global_device_id_));
      ptr->~Shared_ptr();
    }
    operand_type_case_ = OPERAND_TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto& ConstOperandProto::_OperandProto_::current_global_device_id() const {
  if (has_current_global_device_id()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>*>(&(operand_type_.current_global_device_id_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::CurrentGlobalDeviceIdProto* ConstOperandProto::_OperandProto_::mutable_current_global_device_id() {
  if(!has_current_global_device_id()) {
    clear_operand_type();
    new (&(operand_type_.current_global_device_id_)) ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>(new ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto());
  }
  operand_type_case_ = kCurrentGlobalDeviceId;
  ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>*>(&(operand_type_.current_global_device_id_));
  return  (*ptr).get();
}

// oneof field operand_type: sole_mirrored_object
bool ConstOperandProto::_OperandProto_::has_sole_mirrored_object() const {
  return operand_type_case() == kSoleMirroredObject;
}
void ConstOperandProto::_OperandProto_::clear_sole_mirrored_object() {
  if (has_sole_mirrored_object()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.sole_mirrored_object_));
      ptr->~Shared_ptr();
    }
    operand_type_case_ = OPERAND_TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::SoleMirroredObjectProto& ConstOperandProto::_OperandProto_::sole_mirrored_object() const {
  if (has_sole_mirrored_object()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>*>(&(operand_type_.sole_mirrored_object_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::SoleMirroredObjectProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::SoleMirroredObjectProto* ConstOperandProto::_OperandProto_::mutable_sole_mirrored_object() {
  if(!has_sole_mirrored_object()) {
    clear_operand_type();
    new (&(operand_type_.sole_mirrored_object_)) ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>(new ::oneflow::vm::cfg::SoleMirroredObjectProto());
  }
  operand_type_case_ = kSoleMirroredObject;
  ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>*>(&(operand_type_.sole_mirrored_object_));
  return  (*ptr).get();
}

// oneof field operand_type: all_mirrored_object
bool ConstOperandProto::_OperandProto_::has_all_mirrored_object() const {
  return operand_type_case() == kAllMirroredObject;
}
void ConstOperandProto::_OperandProto_::clear_all_mirrored_object() {
  if (has_all_mirrored_object()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.all_mirrored_object_));
      ptr->~Shared_ptr();
    }
    operand_type_case_ = OPERAND_TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::AllMirroredObjectProto& ConstOperandProto::_OperandProto_::all_mirrored_object() const {
  if (has_all_mirrored_object()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>*>(&(operand_type_.all_mirrored_object_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::AllMirroredObjectProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::AllMirroredObjectProto* ConstOperandProto::_OperandProto_::mutable_all_mirrored_object() {
  if(!has_all_mirrored_object()) {
    clear_operand_type();
    new (&(operand_type_.all_mirrored_object_)) ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>(new ::oneflow::vm::cfg::AllMirroredObjectProto());
  }
  operand_type_case_ = kAllMirroredObject;
  ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>*>(&(operand_type_.all_mirrored_object_));
  return  (*ptr).get();
}
ConstOperandProto::OperandTypeCase ConstOperandProto::_OperandProto_::operand_type_case() const {
  return operand_type_case_;
}
bool ConstOperandProto::_OperandProto_::has_operand_type() const {
  return operand_type_case_ != OPERAND_TYPE_NOT_SET;
}
void ConstOperandProto::_OperandProto_::clear_operand_type() {
  switch (operand_type_case()) {
    case kCurrentGlobalDeviceId: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.current_global_device_id_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kSoleMirroredObject: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.sole_mirrored_object_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAllMirroredObject: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(operand_type_.all_mirrored_object_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case OPERAND_TYPE_NOT_SET: {
      break;
    }
  }
  operand_type_case_ = OPERAND_TYPE_NOT_SET;
}
void ConstOperandProto::_OperandProto_::operand_type_copy_from(const _OperandProto_& other) {
  switch (other.operand_type_case()) {
    case kCurrentGlobalDeviceId: {
      mutable_current_global_device_id()->CopyFrom(other.current_global_device_id());
      break;
    }
    case kSoleMirroredObject: {
      mutable_sole_mirrored_object()->CopyFrom(other.sole_mirrored_object());
      break;
    }
    case kAllMirroredObject: {
      mutable_all_mirrored_object()->CopyFrom(other.all_mirrored_object());
      break;
    }
    case OPERAND_TYPE_NOT_SET: {
      clear_operand_type();
    }
  }
}


int ConstOperandProto::_OperandProto_::compare(const _OperandProto_& other) {
  if (!(has_logical_object_id() == other.has_logical_object_id())) {
    return has_logical_object_id() < other.has_logical_object_id() ? -1 : 1;
  } else if (!(logical_object_id() == other.logical_object_id())) {
    return logical_object_id() < other.logical_object_id() ? -1 : 1;
  }
  if (!(operand_type_case() == other.operand_type_case())) {
    return operand_type_case() < other.operand_type_case() ? -1 : 1;
  }
  switch (operand_type_case()) {
    case kCurrentGlobalDeviceId: {
      if (!(current_global_device_id() == other.current_global_device_id())) {
        return current_global_device_id() < other.current_global_device_id() ? -1 : 1;
      }
      break;
    }
    case kSoleMirroredObject: {
      if (!(sole_mirrored_object() == other.sole_mirrored_object())) {
        return sole_mirrored_object() < other.sole_mirrored_object() ? -1 : 1;
      }
      break;
    }
    case kAllMirroredObject: {
      if (!(all_mirrored_object() == other.all_mirrored_object())) {
        return all_mirrored_object() < other.all_mirrored_object() ? -1 : 1;
      }
      break;
    }
    case OPERAND_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstOperandProto::_OperandProto_::operator==(const _OperandProto_& other) const {
  return true
    && has_logical_object_id() == other.has_logical_object_id() 
    && logical_object_id() == other.logical_object_id()
    && operand_type_case() == other.operand_type_case()
    && (operand_type_case() == kCurrentGlobalDeviceId ? 
      current_global_device_id() == other.current_global_device_id() : true)
    && (operand_type_case() == kSoleMirroredObject ? 
      sole_mirrored_object() == other.sole_mirrored_object() : true)
    && (operand_type_case() == kAllMirroredObject ? 
      all_mirrored_object() == other.all_mirrored_object() : true)
  ;
}

std::size_t ConstOperandProto::_OperandProto_::__CalcHash__() const {
  return 0
    ^ (has_logical_object_id() ? std::hash<int64_t>()(logical_object_id()) : 0)
    ^ static_cast<std::size_t>(operand_type_case())
    ^ (has_current_global_device_id() ? std::hash<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>()(current_global_device_id()) : 0)
    ^ (has_sole_mirrored_object() ? std::hash<::oneflow::vm::cfg::SoleMirroredObjectProto>()(sole_mirrored_object()) : 0)
    ^ (has_all_mirrored_object() ? std::hash<::oneflow::vm::cfg::AllMirroredObjectProto>()(all_mirrored_object()) : 0)
  ;
}

bool ConstOperandProto::_OperandProto_::operator<(const _OperandProto_& other) const {
  return false
    || !(has_logical_object_id() == other.has_logical_object_id()) ? 
      has_logical_object_id() < other.has_logical_object_id() : false
    || !(logical_object_id() == other.logical_object_id()) ? 
      logical_object_id() < other.logical_object_id() : false
    || !(operand_type_case() == other.operand_type_case()) ? 
      operand_type_case() < other.operand_type_case() : false
    || ((operand_type_case() == kCurrentGlobalDeviceId) && 
      !(current_global_device_id() == other.current_global_device_id())) ? 
        current_global_device_id() < other.current_global_device_id() : false
    || ((operand_type_case() == kSoleMirroredObject) && 
      !(sole_mirrored_object() == other.sole_mirrored_object())) ? 
        sole_mirrored_object() < other.sole_mirrored_object() : false
    || ((operand_type_case() == kAllMirroredObject) && 
      !(all_mirrored_object() == other.all_mirrored_object())) ? 
        all_mirrored_object() < other.all_mirrored_object() : false
  ;
}

using _OperandProto_ =  ConstOperandProto::_OperandProto_;
ConstOperandProto::ConstOperandProto(const ::std::shared_ptr<_OperandProto_>& data): data_(data) {}
ConstOperandProto::ConstOperandProto(): data_(::std::make_shared<_OperandProto_>()) {}
ConstOperandProto::ConstOperandProto(const ::oneflow::vm::OperandProto& proto_operandproto) {
  BuildFromProto(proto_operandproto);
}
ConstOperandProto::ConstOperandProto(const ConstOperandProto&) = default;
ConstOperandProto::ConstOperandProto(ConstOperandProto&&) noexcept = default;
ConstOperandProto::~ConstOperandProto() = default;

void ConstOperandProto::ToProto(PbMessage* proto_operandproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::OperandProto*>(proto_operandproto));
}
  
::std::string ConstOperandProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOperandProto::__Empty__() const {
  return !data_;
}

int ConstOperandProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"logical_object_id", 1},
    {"current_global_device_id", 2},
    {"sole_mirrored_object", 3},
    {"all_mirrored_object", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOperandProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOperandProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::CurrentGlobalDeviceIdProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstCurrentGlobalDeviceIdProto),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::SoleMirroredObjectProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstSoleMirroredObjectProto),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::AllMirroredObjectProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstAllMirroredObjectProto),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOperandProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &logical_object_id();
    case 2: return &current_global_device_id();
    case 3: return &sole_mirrored_object();
    case 4: return &all_mirrored_object();
    default: return nullptr;
  }
}

// required or optional field logical_object_id
bool ConstOperandProto::has_logical_object_id() const {
  return __SharedPtrOrDefault__()->has_logical_object_id();
}
const int64_t& ConstOperandProto::logical_object_id() const {
  return __SharedPtrOrDefault__()->logical_object_id();
}
// used by pybind11 only
 // oneof field operand_type: current_global_device_id
bool ConstOperandProto::has_current_global_device_id() const {
  return __SharedPtrOrDefault__()->has_current_global_device_id();
}
const ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto& ConstOperandProto::current_global_device_id() const {
  return __SharedPtrOrDefault__()->current_global_device_id();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstCurrentGlobalDeviceIdProto> ConstOperandProto::shared_const_current_global_device_id() const {
  return current_global_device_id().__SharedConst__();
}
 // oneof field operand_type: sole_mirrored_object
bool ConstOperandProto::has_sole_mirrored_object() const {
  return __SharedPtrOrDefault__()->has_sole_mirrored_object();
}
const ::oneflow::vm::cfg::SoleMirroredObjectProto& ConstOperandProto::sole_mirrored_object() const {
  return __SharedPtrOrDefault__()->sole_mirrored_object();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstSoleMirroredObjectProto> ConstOperandProto::shared_const_sole_mirrored_object() const {
  return sole_mirrored_object().__SharedConst__();
}
 // oneof field operand_type: all_mirrored_object
bool ConstOperandProto::has_all_mirrored_object() const {
  return __SharedPtrOrDefault__()->has_all_mirrored_object();
}
const ::oneflow::vm::cfg::AllMirroredObjectProto& ConstOperandProto::all_mirrored_object() const {
  return __SharedPtrOrDefault__()->all_mirrored_object();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstAllMirroredObjectProto> ConstOperandProto::shared_const_all_mirrored_object() const {
  return all_mirrored_object().__SharedConst__();
}
ConstOperandProto::OperandTypeCase ConstOperandProto::operand_type_case() const {
  return __SharedPtrOrDefault__()->operand_type_case();
}

bool ConstOperandProto::has_operand_type() const {
  return __SharedPtrOrDefault__()->has_operand_type();
}

::std::shared_ptr<ConstOperandProto> ConstOperandProto::__SharedConst__() const {
  return ::std::make_shared<ConstOperandProto>(data_);
}
int64_t ConstOperandProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOperandProto::operator==(const ConstOperandProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOperandProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOperandProto::operator<(const ConstOperandProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OperandProto_>& ConstOperandProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OperandProto_> default_ptr = std::make_shared<_OperandProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_OperandProto_>& ConstOperandProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _OperandProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOperandProto
void ConstOperandProto::BuildFromProto(const PbMessage& proto_operandproto) {
  data_ = ::std::make_shared<_OperandProto_>(dynamic_cast<const ::oneflow::vm::OperandProto&>(proto_operandproto));
}

OperandProto::OperandProto(const ::std::shared_ptr<_OperandProto_>& data)
  : ConstOperandProto(data) {}
OperandProto::OperandProto(const OperandProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OperandProto> resize
OperandProto::OperandProto(OperandProto&&) noexcept = default; 
OperandProto::OperandProto(const ::oneflow::vm::OperandProto& proto_operandproto) {
  InitFromProto(proto_operandproto);
}
OperandProto::OperandProto() = default;

OperandProto::~OperandProto() = default;

void OperandProto::InitFromProto(const PbMessage& proto_operandproto) {
  BuildFromProto(proto_operandproto);
}
  
void* OperandProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_logical_object_id();
    case 2: return mutable_current_global_device_id();
    case 3: return mutable_sole_mirrored_object();
    case 4: return mutable_all_mirrored_object();
    default: return nullptr;
  }
}

bool OperandProto::operator==(const OperandProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OperandProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OperandProto::operator<(const OperandProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OperandProto::Clear() {
  if (data_) { data_.reset(); }
}
void OperandProto::CopyFrom(const OperandProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OperandProto& OperandProto::operator=(const OperandProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field logical_object_id
void OperandProto::clear_logical_object_id() {
  return __SharedPtr__()->clear_logical_object_id();
}
void OperandProto::set_logical_object_id(const int64_t& value) {
  return __SharedPtr__()->set_logical_object_id(value);
}
int64_t* OperandProto::mutable_logical_object_id() {
  return  __SharedPtr__()->mutable_logical_object_id();
}
void OperandProto::clear_current_global_device_id() {
  return __SharedPtr__()->clear_current_global_device_id();
}
::oneflow::vm::cfg::CurrentGlobalDeviceIdProto* OperandProto::mutable_current_global_device_id() {
  return __SharedPtr__()->mutable_current_global_device_id();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto> OperandProto::shared_mutable_current_global_device_id() {
  return mutable_current_global_device_id()->__SharedMutable__();
}
void OperandProto::clear_sole_mirrored_object() {
  return __SharedPtr__()->clear_sole_mirrored_object();
}
::oneflow::vm::cfg::SoleMirroredObjectProto* OperandProto::mutable_sole_mirrored_object() {
  return __SharedPtr__()->mutable_sole_mirrored_object();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto> OperandProto::shared_mutable_sole_mirrored_object() {
  return mutable_sole_mirrored_object()->__SharedMutable__();
}
void OperandProto::clear_all_mirrored_object() {
  return __SharedPtr__()->clear_all_mirrored_object();
}
::oneflow::vm::cfg::AllMirroredObjectProto* OperandProto::mutable_all_mirrored_object() {
  return __SharedPtr__()->mutable_all_mirrored_object();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto> OperandProto::shared_mutable_all_mirrored_object() {
  return mutable_all_mirrored_object()->__SharedMutable__();
}

::std::shared_ptr<OperandProto> OperandProto::__SharedMutable__() {
  return ::std::make_shared<OperandProto>(__SharedPtr__());
}
ConstOperandSeparatorProto::_OperandSeparatorProto_::_OperandSeparatorProto_() { Clear(); }
ConstOperandSeparatorProto::_OperandSeparatorProto_::_OperandSeparatorProto_(const _OperandSeparatorProto_& other) { CopyFrom(other); }
ConstOperandSeparatorProto::_OperandSeparatorProto_::_OperandSeparatorProto_(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto) {
  InitFromProto(proto_operandseparatorproto);
}
ConstOperandSeparatorProto::_OperandSeparatorProto_::_OperandSeparatorProto_(_OperandSeparatorProto_&& other) = default;
ConstOperandSeparatorProto::_OperandSeparatorProto_::~_OperandSeparatorProto_() = default;

void ConstOperandSeparatorProto::_OperandSeparatorProto_::InitFromProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto) {
  Clear();
    
}

void ConstOperandSeparatorProto::_OperandSeparatorProto_::ToProto(::oneflow::vm::OperandSeparatorProto* proto_operandseparatorproto) const {
  proto_operandseparatorproto->Clear();

}

::std::string ConstOperandSeparatorProto::_OperandSeparatorProto_::DebugString() const {
  ::oneflow::vm::OperandSeparatorProto proto_operandseparatorproto;
  this->ToProto(&proto_operandseparatorproto);
  return proto_operandseparatorproto.DebugString();
}

void ConstOperandSeparatorProto::_OperandSeparatorProto_::Clear() {
}

void ConstOperandSeparatorProto::_OperandSeparatorProto_::CopyFrom(const _OperandSeparatorProto_& other) {
}



int ConstOperandSeparatorProto::_OperandSeparatorProto_::compare(const _OperandSeparatorProto_& other) {
  return 0;
}

bool ConstOperandSeparatorProto::_OperandSeparatorProto_::operator==(const _OperandSeparatorProto_& other) const {
  return true
  ;
}

std::size_t ConstOperandSeparatorProto::_OperandSeparatorProto_::__CalcHash__() const {
  return 0
  ;
}

bool ConstOperandSeparatorProto::_OperandSeparatorProto_::operator<(const _OperandSeparatorProto_& other) const {
  return false
  ;
}

using _OperandSeparatorProto_ =  ConstOperandSeparatorProto::_OperandSeparatorProto_;
ConstOperandSeparatorProto::ConstOperandSeparatorProto(const ::std::shared_ptr<_OperandSeparatorProto_>& data): data_(data) {}
ConstOperandSeparatorProto::ConstOperandSeparatorProto(): data_(::std::make_shared<_OperandSeparatorProto_>()) {}
ConstOperandSeparatorProto::ConstOperandSeparatorProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto) {
  BuildFromProto(proto_operandseparatorproto);
}
ConstOperandSeparatorProto::ConstOperandSeparatorProto(const ConstOperandSeparatorProto&) = default;
ConstOperandSeparatorProto::ConstOperandSeparatorProto(ConstOperandSeparatorProto&&) noexcept = default;
ConstOperandSeparatorProto::~ConstOperandSeparatorProto() = default;

void ConstOperandSeparatorProto::ToProto(PbMessage* proto_operandseparatorproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::OperandSeparatorProto*>(proto_operandseparatorproto));
}
  
::std::string ConstOperandSeparatorProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOperandSeparatorProto::__Empty__() const {
  return !data_;
}

int ConstOperandSeparatorProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  return 0;
}

bool ConstOperandSeparatorProto::FieldDefined4FieldNumber(int field_number) const  {
  return false;
}

const ::std::set<::std::type_index>& ConstOperandSeparatorProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOperandSeparatorProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    default: return nullptr;
  }
}


::std::shared_ptr<ConstOperandSeparatorProto> ConstOperandSeparatorProto::__SharedConst__() const {
  return ::std::make_shared<ConstOperandSeparatorProto>(data_);
}
int64_t ConstOperandSeparatorProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOperandSeparatorProto::operator==(const ConstOperandSeparatorProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOperandSeparatorProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOperandSeparatorProto::operator<(const ConstOperandSeparatorProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OperandSeparatorProto_>& ConstOperandSeparatorProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OperandSeparatorProto_> default_ptr = std::make_shared<_OperandSeparatorProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_OperandSeparatorProto_>& ConstOperandSeparatorProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _OperandSeparatorProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOperandSeparatorProto
void ConstOperandSeparatorProto::BuildFromProto(const PbMessage& proto_operandseparatorproto) {
  data_ = ::std::make_shared<_OperandSeparatorProto_>(dynamic_cast<const ::oneflow::vm::OperandSeparatorProto&>(proto_operandseparatorproto));
}

OperandSeparatorProto::OperandSeparatorProto(const ::std::shared_ptr<_OperandSeparatorProto_>& data)
  : ConstOperandSeparatorProto(data) {}
OperandSeparatorProto::OperandSeparatorProto(const OperandSeparatorProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OperandSeparatorProto> resize
OperandSeparatorProto::OperandSeparatorProto(OperandSeparatorProto&&) noexcept = default; 
OperandSeparatorProto::OperandSeparatorProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto) {
  InitFromProto(proto_operandseparatorproto);
}
OperandSeparatorProto::OperandSeparatorProto() = default;

OperandSeparatorProto::~OperandSeparatorProto() = default;

void OperandSeparatorProto::InitFromProto(const PbMessage& proto_operandseparatorproto) {
  BuildFromProto(proto_operandseparatorproto);
}
  
void* OperandSeparatorProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    default: return nullptr;
  }
}

bool OperandSeparatorProto::operator==(const OperandSeparatorProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OperandSeparatorProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OperandSeparatorProto::operator<(const OperandSeparatorProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OperandSeparatorProto::Clear() {
  if (data_) { data_.reset(); }
}
void OperandSeparatorProto::CopyFrom(const OperandSeparatorProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OperandSeparatorProto& OperandSeparatorProto::operator=(const OperandSeparatorProto& other) {
  CopyFrom(other);
  return *this;
}


::std::shared_ptr<OperandSeparatorProto> OperandSeparatorProto::__SharedMutable__() {
  return ::std::make_shared<OperandSeparatorProto>(__SharedPtr__());
}
ConstInstructionOperandProto::_InstructionOperandProto_::_InstructionOperandProto_() { Clear(); }
ConstInstructionOperandProto::_InstructionOperandProto_::_InstructionOperandProto_(const _InstructionOperandProto_& other) { CopyFrom(other); }
ConstInstructionOperandProto::_InstructionOperandProto_::_InstructionOperandProto_(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto) {
  InitFromProto(proto_instructionoperandproto);
}
ConstInstructionOperandProto::_InstructionOperandProto_::_InstructionOperandProto_(_InstructionOperandProto_&& other) = default;
ConstInstructionOperandProto::_InstructionOperandProto_::~_InstructionOperandProto_() = default;

void ConstInstructionOperandProto::_InstructionOperandProto_::InitFromProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto) {
  Clear();
  // oneof field: type
  TypeCase type_case = TypeCase(int(proto_instructionoperandproto.type_case()));
  switch (type_case) {
    case kConstOperand: {
      *mutable_const_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.const_operand());
      break;
  }
    case kMutOperand: {
      *mutable_mut_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.mut_operand());
      break;
  }
    case kMut2Operand: {
      *mutable_mut2_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.mut2_operand());
      break;
  }
    case kDelOperand: {
      *mutable_del_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.del_operand());
      break;
  }
    case kSymbolOperand: {
      *mutable_symbol_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.symbol_operand());
      break;
  }
    case kInitSymbolOperand: {
      *mutable_init_symbol_operand() = ::oneflow::vm::cfg::OperandProto(proto_instructionoperandproto.init_symbol_operand());
      break;
  }
    case kSeparator: {
      *mutable_separator() = ::oneflow::vm::cfg::OperandSeparatorProto(proto_instructionoperandproto.separator());
      break;
  }
    case kDoubleOperand: {
      set_double_operand(proto_instructionoperandproto.double_operand());
      break;
  }
    case kInt64Operand: {
      set_int64_operand(proto_instructionoperandproto.int64_operand());
      break;
  }
    case kUint64Operand: {
      set_uint64_operand(proto_instructionoperandproto.uint64_operand());
      break;
  }
    case kBoolOperand: {
      set_bool_operand(proto_instructionoperandproto.bool_operand());
      break;
  }
    case TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstInstructionOperandProto::_InstructionOperandProto_::ToProto(::oneflow::vm::InstructionOperandProto* proto_instructionoperandproto) const {
  proto_instructionoperandproto->Clear();

  // oneof field: type
  ::oneflow::vm::InstructionOperandProto::TypeCase type_case = ::oneflow::vm::InstructionOperandProto::TypeCase(int(this->type_case()));
  switch (type_case) {
    case ::oneflow::vm::InstructionOperandProto::kConstOperand: {
      ::oneflow::vm::OperandProto of_proto_const_operand;
      const_operand().ToProto(&of_proto_const_operand);
      proto_instructionoperandproto->mutable_const_operand()->CopyFrom(of_proto_const_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kMutOperand: {
      ::oneflow::vm::OperandProto of_proto_mut_operand;
      mut_operand().ToProto(&of_proto_mut_operand);
      proto_instructionoperandproto->mutable_mut_operand()->CopyFrom(of_proto_mut_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kMut2Operand: {
      ::oneflow::vm::OperandProto of_proto_mut2_operand;
      mut2_operand().ToProto(&of_proto_mut2_operand);
      proto_instructionoperandproto->mutable_mut2_operand()->CopyFrom(of_proto_mut2_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kDelOperand: {
      ::oneflow::vm::OperandProto of_proto_del_operand;
      del_operand().ToProto(&of_proto_del_operand);
      proto_instructionoperandproto->mutable_del_operand()->CopyFrom(of_proto_del_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kSymbolOperand: {
      ::oneflow::vm::OperandProto of_proto_symbol_operand;
      symbol_operand().ToProto(&of_proto_symbol_operand);
      proto_instructionoperandproto->mutable_symbol_operand()->CopyFrom(of_proto_symbol_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kInitSymbolOperand: {
      ::oneflow::vm::OperandProto of_proto_init_symbol_operand;
      init_symbol_operand().ToProto(&of_proto_init_symbol_operand);
      proto_instructionoperandproto->mutable_init_symbol_operand()->CopyFrom(of_proto_init_symbol_operand);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kSeparator: {
      ::oneflow::vm::OperandSeparatorProto of_proto_separator;
      separator().ToProto(&of_proto_separator);
      proto_instructionoperandproto->mutable_separator()->CopyFrom(of_proto_separator);
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kDoubleOperand: {
      proto_instructionoperandproto->set_double_operand(double_operand());
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kInt64Operand: {
      proto_instructionoperandproto->set_int64_operand(int64_operand());
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kUint64Operand: {
      proto_instructionoperandproto->set_uint64_operand(uint64_operand());
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::kBoolOperand: {
      proto_instructionoperandproto->set_bool_operand(bool_operand());
      break;
    }
    case ::oneflow::vm::InstructionOperandProto::TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstInstructionOperandProto::_InstructionOperandProto_::DebugString() const {
  ::oneflow::vm::InstructionOperandProto proto_instructionoperandproto;
  this->ToProto(&proto_instructionoperandproto);
  return proto_instructionoperandproto.DebugString();
}

void ConstInstructionOperandProto::_InstructionOperandProto_::Clear() {
  clear_type();
}

void ConstInstructionOperandProto::_InstructionOperandProto_::CopyFrom(const _InstructionOperandProto_& other) {
  type_copy_from(other);
}


// oneof field type: const_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_const_operand() const {
  return type_case() == kConstOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_const_operand() {
  if (has_const_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.const_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::const_operand() const {
  if (has_const_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.const_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_const_operand() {
  if(!has_const_operand()) {
    clear_type();
    new (&(type_.const_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kConstOperand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.const_operand_));
  return  (*ptr).get();
}

// oneof field type: mut_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_mut_operand() const {
  return type_case() == kMutOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_mut_operand() {
  if (has_mut_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.mut_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::mut_operand() const {
  if (has_mut_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.mut_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_mut_operand() {
  if(!has_mut_operand()) {
    clear_type();
    new (&(type_.mut_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kMutOperand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.mut_operand_));
  return  (*ptr).get();
}

// oneof field type: mut2_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_mut2_operand() const {
  return type_case() == kMut2Operand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_mut2_operand() {
  if (has_mut2_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.mut2_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::mut2_operand() const {
  if (has_mut2_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.mut2_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_mut2_operand() {
  if(!has_mut2_operand()) {
    clear_type();
    new (&(type_.mut2_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kMut2Operand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.mut2_operand_));
  return  (*ptr).get();
}

// oneof field type: del_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_del_operand() const {
  return type_case() == kDelOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_del_operand() {
  if (has_del_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.del_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::del_operand() const {
  if (has_del_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.del_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_del_operand() {
  if(!has_del_operand()) {
    clear_type();
    new (&(type_.del_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kDelOperand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.del_operand_));
  return  (*ptr).get();
}

// oneof field type: symbol_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_symbol_operand() const {
  return type_case() == kSymbolOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_symbol_operand() {
  if (has_symbol_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.symbol_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::symbol_operand() const {
  if (has_symbol_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.symbol_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_symbol_operand() {
  if(!has_symbol_operand()) {
    clear_type();
    new (&(type_.symbol_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kSymbolOperand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.symbol_operand_));
  return  (*ptr).get();
}

// oneof field type: init_symbol_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_init_symbol_operand() const {
  return type_case() == kInitSymbolOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_init_symbol_operand() {
  if (has_init_symbol_operand()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.init_symbol_operand_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::_InstructionOperandProto_::init_symbol_operand() const {
  if (has_init_symbol_operand()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.init_symbol_operand_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_init_symbol_operand() {
  if(!has_init_symbol_operand()) {
    clear_type();
    new (&(type_.init_symbol_operand_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>(new ::oneflow::vm::cfg::OperandProto());
  }
  type_case_ = kInitSymbolOperand;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandProto>*>(&(type_.init_symbol_operand_));
  return  (*ptr).get();
}

// oneof field type: separator
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_separator() const {
  return type_case() == kSeparator;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_separator() {
  if (has_separator()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.separator_));
      ptr->~Shared_ptr();
    }
    type_case_ = TYPE_NOT_SET;
  }
}

const ::oneflow::vm::cfg::OperandSeparatorProto& ConstInstructionOperandProto::_InstructionOperandProto_::separator() const {
  if (has_separator()) {
      const ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>*>(&(type_.separator_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto> default_static_value = ::std::make_shared<::oneflow::vm::cfg::OperandSeparatorProto>();
    return *default_static_value;
    }
}
::oneflow::vm::cfg::OperandSeparatorProto* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_separator() {
  if(!has_separator()) {
    clear_type();
    new (&(type_.separator_)) ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>(new ::oneflow::vm::cfg::OperandSeparatorProto());
  }
  type_case_ = kSeparator;
  ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>*>(&(type_.separator_));
  return  (*ptr).get();
}

// oneof field type: double_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_double_operand() const {
  return type_case() == kDoubleOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_double_operand() {
  if (has_double_operand()) {
    type_.double_operand_ = double();
    type_case_ = TYPE_NOT_SET;
  }
}

const double& ConstInstructionOperandProto::_InstructionOperandProto_::double_operand() const {
  if (has_double_operand()) {
      return type_.double_operand_;
    } else {
      static const double default_static_value = double();
    return default_static_value;
    }
}
void ConstInstructionOperandProto::_InstructionOperandProto_::set_double_operand(const double& value) {
  if(!has_double_operand()) {
    clear_type();
    }
  type_case_ = kDoubleOperand;
    type_.double_operand_ = value;
  }
double* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_double_operand() {
  if(!has_double_operand()) {
    clear_type();
    }
    type_case_ = kDoubleOperand;
  return  &type_.double_operand_;
  }

// oneof field type: int64_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_int64_operand() const {
  return type_case() == kInt64Operand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_int64_operand() {
  if (has_int64_operand()) {
    type_.int64_operand_ = int64_t();
    type_case_ = TYPE_NOT_SET;
  }
}

const int64_t& ConstInstructionOperandProto::_InstructionOperandProto_::int64_operand() const {
  if (has_int64_operand()) {
      return type_.int64_operand_;
    } else {
      static const int64_t default_static_value = int64_t();
    return default_static_value;
    }
}
void ConstInstructionOperandProto::_InstructionOperandProto_::set_int64_operand(const int64_t& value) {
  if(!has_int64_operand()) {
    clear_type();
    }
  type_case_ = kInt64Operand;
    type_.int64_operand_ = value;
  }
int64_t* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_int64_operand() {
  if(!has_int64_operand()) {
    clear_type();
    }
    type_case_ = kInt64Operand;
  return  &type_.int64_operand_;
  }

// oneof field type: uint64_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_uint64_operand() const {
  return type_case() == kUint64Operand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_uint64_operand() {
  if (has_uint64_operand()) {
    type_.uint64_operand_ = uint64_t();
    type_case_ = TYPE_NOT_SET;
  }
}

const uint64_t& ConstInstructionOperandProto::_InstructionOperandProto_::uint64_operand() const {
  if (has_uint64_operand()) {
      return type_.uint64_operand_;
    } else {
      static const uint64_t default_static_value = uint64_t();
    return default_static_value;
    }
}
void ConstInstructionOperandProto::_InstructionOperandProto_::set_uint64_operand(const uint64_t& value) {
  if(!has_uint64_operand()) {
    clear_type();
    }
  type_case_ = kUint64Operand;
    type_.uint64_operand_ = value;
  }
uint64_t* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_uint64_operand() {
  if(!has_uint64_operand()) {
    clear_type();
    }
    type_case_ = kUint64Operand;
  return  &type_.uint64_operand_;
  }

// oneof field type: bool_operand
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_bool_operand() const {
  return type_case() == kBoolOperand;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_bool_operand() {
  if (has_bool_operand()) {
    type_.bool_operand_ = bool();
    type_case_ = TYPE_NOT_SET;
  }
}

const bool& ConstInstructionOperandProto::_InstructionOperandProto_::bool_operand() const {
  if (has_bool_operand()) {
      return type_.bool_operand_;
    } else {
      static const bool default_static_value = bool();
    return default_static_value;
    }
}
void ConstInstructionOperandProto::_InstructionOperandProto_::set_bool_operand(const bool& value) {
  if(!has_bool_operand()) {
    clear_type();
    }
  type_case_ = kBoolOperand;
    type_.bool_operand_ = value;
  }
bool* ConstInstructionOperandProto::_InstructionOperandProto_::mutable_bool_operand() {
  if(!has_bool_operand()) {
    clear_type();
    }
    type_case_ = kBoolOperand;
  return  &type_.bool_operand_;
  }
ConstInstructionOperandProto::TypeCase ConstInstructionOperandProto::_InstructionOperandProto_::type_case() const {
  return type_case_;
}
bool ConstInstructionOperandProto::_InstructionOperandProto_::has_type() const {
  return type_case_ != TYPE_NOT_SET;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::clear_type() {
  switch (type_case()) {
    case kConstOperand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.const_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMutOperand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.mut_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kMut2Operand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.mut2_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kDelOperand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.del_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kSymbolOperand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.symbol_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kInitSymbolOperand: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.init_symbol_operand_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kSeparator: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(type_.separator_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kDoubleOperand: {
      type_.double_operand_ = double();
      break;
    }
    case kInt64Operand: {
      type_.int64_operand_ = int64_t();
      break;
    }
    case kUint64Operand: {
      type_.uint64_operand_ = uint64_t();
      break;
    }
    case kBoolOperand: {
      type_.bool_operand_ = bool();
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  type_case_ = TYPE_NOT_SET;
}
void ConstInstructionOperandProto::_InstructionOperandProto_::type_copy_from(const _InstructionOperandProto_& other) {
  switch (other.type_case()) {
    case kConstOperand: {
      mutable_const_operand()->CopyFrom(other.const_operand());
      break;
    }
    case kMutOperand: {
      mutable_mut_operand()->CopyFrom(other.mut_operand());
      break;
    }
    case kMut2Operand: {
      mutable_mut2_operand()->CopyFrom(other.mut2_operand());
      break;
    }
    case kDelOperand: {
      mutable_del_operand()->CopyFrom(other.del_operand());
      break;
    }
    case kSymbolOperand: {
      mutable_symbol_operand()->CopyFrom(other.symbol_operand());
      break;
    }
    case kInitSymbolOperand: {
      mutable_init_symbol_operand()->CopyFrom(other.init_symbol_operand());
      break;
    }
    case kSeparator: {
      mutable_separator()->CopyFrom(other.separator());
      break;
    }
    case kDoubleOperand: {
      set_double_operand(other.double_operand());
      break;
    }
    case kInt64Operand: {
      set_int64_operand(other.int64_operand());
      break;
    }
    case kUint64Operand: {
      set_uint64_operand(other.uint64_operand());
      break;
    }
    case kBoolOperand: {
      set_bool_operand(other.bool_operand());
      break;
    }
    case TYPE_NOT_SET: {
      clear_type();
    }
  }
}


int ConstInstructionOperandProto::_InstructionOperandProto_::compare(const _InstructionOperandProto_& other) {
  if (!(type_case() == other.type_case())) {
    return type_case() < other.type_case() ? -1 : 1;
  }
  switch (type_case()) {
    case kConstOperand: {
      if (!(const_operand() == other.const_operand())) {
        return const_operand() < other.const_operand() ? -1 : 1;
      }
      break;
    }
    case kMutOperand: {
      if (!(mut_operand() == other.mut_operand())) {
        return mut_operand() < other.mut_operand() ? -1 : 1;
      }
      break;
    }
    case kMut2Operand: {
      if (!(mut2_operand() == other.mut2_operand())) {
        return mut2_operand() < other.mut2_operand() ? -1 : 1;
      }
      break;
    }
    case kDelOperand: {
      if (!(del_operand() == other.del_operand())) {
        return del_operand() < other.del_operand() ? -1 : 1;
      }
      break;
    }
    case kSymbolOperand: {
      if (!(symbol_operand() == other.symbol_operand())) {
        return symbol_operand() < other.symbol_operand() ? -1 : 1;
      }
      break;
    }
    case kInitSymbolOperand: {
      if (!(init_symbol_operand() == other.init_symbol_operand())) {
        return init_symbol_operand() < other.init_symbol_operand() ? -1 : 1;
      }
      break;
    }
    case kSeparator: {
      if (!(separator() == other.separator())) {
        return separator() < other.separator() ? -1 : 1;
      }
      break;
    }
    case kDoubleOperand: {
      if (!(double_operand() == other.double_operand())) {
        return double_operand() < other.double_operand() ? -1 : 1;
      }
      break;
    }
    case kInt64Operand: {
      if (!(int64_operand() == other.int64_operand())) {
        return int64_operand() < other.int64_operand() ? -1 : 1;
      }
      break;
    }
    case kUint64Operand: {
      if (!(uint64_operand() == other.uint64_operand())) {
        return uint64_operand() < other.uint64_operand() ? -1 : 1;
      }
      break;
    }
    case kBoolOperand: {
      if (!(bool_operand() == other.bool_operand())) {
        return bool_operand() < other.bool_operand() ? -1 : 1;
      }
      break;
    }
    case TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstInstructionOperandProto::_InstructionOperandProto_::operator==(const _InstructionOperandProto_& other) const {
  return true
    && type_case() == other.type_case()
    && (type_case() == kConstOperand ? 
      const_operand() == other.const_operand() : true)
    && (type_case() == kMutOperand ? 
      mut_operand() == other.mut_operand() : true)
    && (type_case() == kMut2Operand ? 
      mut2_operand() == other.mut2_operand() : true)
    && (type_case() == kDelOperand ? 
      del_operand() == other.del_operand() : true)
    && (type_case() == kSymbolOperand ? 
      symbol_operand() == other.symbol_operand() : true)
    && (type_case() == kInitSymbolOperand ? 
      init_symbol_operand() == other.init_symbol_operand() : true)
    && (type_case() == kSeparator ? 
      separator() == other.separator() : true)
    && (type_case() == kDoubleOperand ? 
      double_operand() == other.double_operand() : true)
    && (type_case() == kInt64Operand ? 
      int64_operand() == other.int64_operand() : true)
    && (type_case() == kUint64Operand ? 
      uint64_operand() == other.uint64_operand() : true)
    && (type_case() == kBoolOperand ? 
      bool_operand() == other.bool_operand() : true)
  ;
}

std::size_t ConstInstructionOperandProto::_InstructionOperandProto_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(type_case())
    ^ (has_const_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(const_operand()) : 0)
    ^ (has_mut_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(mut_operand()) : 0)
    ^ (has_mut2_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(mut2_operand()) : 0)
    ^ (has_del_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(del_operand()) : 0)
    ^ (has_symbol_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(symbol_operand()) : 0)
    ^ (has_init_symbol_operand() ? std::hash<::oneflow::vm::cfg::OperandProto>()(init_symbol_operand()) : 0)
    ^ (has_separator() ? std::hash<::oneflow::vm::cfg::OperandSeparatorProto>()(separator()) : 0)
    ^ (has_double_operand() ? std::hash<double>()(double_operand()) : 0)
    ^ (has_int64_operand() ? std::hash<int64_t>()(int64_operand()) : 0)
    ^ (has_uint64_operand() ? std::hash<uint64_t>()(uint64_operand()) : 0)
    ^ (has_bool_operand() ? std::hash<bool>()(bool_operand()) : 0)
  ;
}

bool ConstInstructionOperandProto::_InstructionOperandProto_::operator<(const _InstructionOperandProto_& other) const {
  return false
    || !(type_case() == other.type_case()) ? 
      type_case() < other.type_case() : false
    || ((type_case() == kConstOperand) && 
      !(const_operand() == other.const_operand())) ? 
        const_operand() < other.const_operand() : false
    || ((type_case() == kMutOperand) && 
      !(mut_operand() == other.mut_operand())) ? 
        mut_operand() < other.mut_operand() : false
    || ((type_case() == kMut2Operand) && 
      !(mut2_operand() == other.mut2_operand())) ? 
        mut2_operand() < other.mut2_operand() : false
    || ((type_case() == kDelOperand) && 
      !(del_operand() == other.del_operand())) ? 
        del_operand() < other.del_operand() : false
    || ((type_case() == kSymbolOperand) && 
      !(symbol_operand() == other.symbol_operand())) ? 
        symbol_operand() < other.symbol_operand() : false
    || ((type_case() == kInitSymbolOperand) && 
      !(init_symbol_operand() == other.init_symbol_operand())) ? 
        init_symbol_operand() < other.init_symbol_operand() : false
    || ((type_case() == kSeparator) && 
      !(separator() == other.separator())) ? 
        separator() < other.separator() : false
    || ((type_case() == kDoubleOperand) && 
      !(double_operand() == other.double_operand())) ? 
        double_operand() < other.double_operand() : false
    || ((type_case() == kInt64Operand) && 
      !(int64_operand() == other.int64_operand())) ? 
        int64_operand() < other.int64_operand() : false
    || ((type_case() == kUint64Operand) && 
      !(uint64_operand() == other.uint64_operand())) ? 
        uint64_operand() < other.uint64_operand() : false
    || ((type_case() == kBoolOperand) && 
      !(bool_operand() == other.bool_operand())) ? 
        bool_operand() < other.bool_operand() : false
  ;
}

using _InstructionOperandProto_ =  ConstInstructionOperandProto::_InstructionOperandProto_;
ConstInstructionOperandProto::ConstInstructionOperandProto(const ::std::shared_ptr<_InstructionOperandProto_>& data): data_(data) {}
ConstInstructionOperandProto::ConstInstructionOperandProto(): data_(::std::make_shared<_InstructionOperandProto_>()) {}
ConstInstructionOperandProto::ConstInstructionOperandProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto) {
  BuildFromProto(proto_instructionoperandproto);
}
ConstInstructionOperandProto::ConstInstructionOperandProto(const ConstInstructionOperandProto&) = default;
ConstInstructionOperandProto::ConstInstructionOperandProto(ConstInstructionOperandProto&&) noexcept = default;
ConstInstructionOperandProto::~ConstInstructionOperandProto() = default;

void ConstInstructionOperandProto::ToProto(PbMessage* proto_instructionoperandproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::InstructionOperandProto*>(proto_instructionoperandproto));
}
  
::std::string ConstInstructionOperandProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInstructionOperandProto::__Empty__() const {
  return !data_;
}

int ConstInstructionOperandProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"const_operand", 1},
    {"mut_operand", 2},
    {"mut2_operand", 3},
    {"del_operand", 4},
    {"symbol_operand", 5},
    {"init_symbol_operand", 6},
    {"separator", 7},
    {"double_operand", 8},
    {"int64_operand", 9},
    {"uint64_operand", 10},
    {"bool_operand", 11},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInstructionOperandProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInstructionOperandProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandProto),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::OperandSeparatorProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstOperandSeparatorProto),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 9: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 10: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(uint64_t),
      };
      return type_indices;
    }
    case 11: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInstructionOperandProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &const_operand();
    case 2: return &mut_operand();
    case 3: return &mut2_operand();
    case 4: return &del_operand();
    case 5: return &symbol_operand();
    case 6: return &init_symbol_operand();
    case 7: return &separator();
    case 8: return &double_operand();
    case 9: return &int64_operand();
    case 10: return &uint64_operand();
    case 11: return &bool_operand();
    default: return nullptr;
  }
}

 // oneof field type: const_operand
bool ConstInstructionOperandProto::has_const_operand() const {
  return __SharedPtrOrDefault__()->has_const_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::const_operand() const {
  return __SharedPtrOrDefault__()->const_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_const_operand() const {
  return const_operand().__SharedConst__();
}
 // oneof field type: mut_operand
bool ConstInstructionOperandProto::has_mut_operand() const {
  return __SharedPtrOrDefault__()->has_mut_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::mut_operand() const {
  return __SharedPtrOrDefault__()->mut_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_mut_operand() const {
  return mut_operand().__SharedConst__();
}
 // oneof field type: mut2_operand
bool ConstInstructionOperandProto::has_mut2_operand() const {
  return __SharedPtrOrDefault__()->has_mut2_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::mut2_operand() const {
  return __SharedPtrOrDefault__()->mut2_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_mut2_operand() const {
  return mut2_operand().__SharedConst__();
}
 // oneof field type: del_operand
bool ConstInstructionOperandProto::has_del_operand() const {
  return __SharedPtrOrDefault__()->has_del_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::del_operand() const {
  return __SharedPtrOrDefault__()->del_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_del_operand() const {
  return del_operand().__SharedConst__();
}
 // oneof field type: symbol_operand
bool ConstInstructionOperandProto::has_symbol_operand() const {
  return __SharedPtrOrDefault__()->has_symbol_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::symbol_operand() const {
  return __SharedPtrOrDefault__()->symbol_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_symbol_operand() const {
  return symbol_operand().__SharedConst__();
}
 // oneof field type: init_symbol_operand
bool ConstInstructionOperandProto::has_init_symbol_operand() const {
  return __SharedPtrOrDefault__()->has_init_symbol_operand();
}
const ::oneflow::vm::cfg::OperandProto& ConstInstructionOperandProto::init_symbol_operand() const {
  return __SharedPtrOrDefault__()->init_symbol_operand();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> ConstInstructionOperandProto::shared_const_init_symbol_operand() const {
  return init_symbol_operand().__SharedConst__();
}
 // oneof field type: separator
bool ConstInstructionOperandProto::has_separator() const {
  return __SharedPtrOrDefault__()->has_separator();
}
const ::oneflow::vm::cfg::OperandSeparatorProto& ConstInstructionOperandProto::separator() const {
  return __SharedPtrOrDefault__()->separator();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstOperandSeparatorProto> ConstInstructionOperandProto::shared_const_separator() const {
  return separator().__SharedConst__();
}
 // oneof field type: double_operand
bool ConstInstructionOperandProto::has_double_operand() const {
  return __SharedPtrOrDefault__()->has_double_operand();
}
const double& ConstInstructionOperandProto::double_operand() const {
  return __SharedPtrOrDefault__()->double_operand();
}

// used by pybind11 only
 // oneof field type: int64_operand
bool ConstInstructionOperandProto::has_int64_operand() const {
  return __SharedPtrOrDefault__()->has_int64_operand();
}
const int64_t& ConstInstructionOperandProto::int64_operand() const {
  return __SharedPtrOrDefault__()->int64_operand();
}

// used by pybind11 only
 // oneof field type: uint64_operand
bool ConstInstructionOperandProto::has_uint64_operand() const {
  return __SharedPtrOrDefault__()->has_uint64_operand();
}
const uint64_t& ConstInstructionOperandProto::uint64_operand() const {
  return __SharedPtrOrDefault__()->uint64_operand();
}

// used by pybind11 only
 // oneof field type: bool_operand
bool ConstInstructionOperandProto::has_bool_operand() const {
  return __SharedPtrOrDefault__()->has_bool_operand();
}
const bool& ConstInstructionOperandProto::bool_operand() const {
  return __SharedPtrOrDefault__()->bool_operand();
}

// used by pybind11 only
ConstInstructionOperandProto::TypeCase ConstInstructionOperandProto::type_case() const {
  return __SharedPtrOrDefault__()->type_case();
}

bool ConstInstructionOperandProto::has_type() const {
  return __SharedPtrOrDefault__()->has_type();
}

::std::shared_ptr<ConstInstructionOperandProto> ConstInstructionOperandProto::__SharedConst__() const {
  return ::std::make_shared<ConstInstructionOperandProto>(data_);
}
int64_t ConstInstructionOperandProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInstructionOperandProto::operator==(const ConstInstructionOperandProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInstructionOperandProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInstructionOperandProto::operator<(const ConstInstructionOperandProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InstructionOperandProto_>& ConstInstructionOperandProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InstructionOperandProto_> default_ptr = std::make_shared<_InstructionOperandProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_InstructionOperandProto_>& ConstInstructionOperandProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _InstructionOperandProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInstructionOperandProto
void ConstInstructionOperandProto::BuildFromProto(const PbMessage& proto_instructionoperandproto) {
  data_ = ::std::make_shared<_InstructionOperandProto_>(dynamic_cast<const ::oneflow::vm::InstructionOperandProto&>(proto_instructionoperandproto));
}

InstructionOperandProto::InstructionOperandProto(const ::std::shared_ptr<_InstructionOperandProto_>& data)
  : ConstInstructionOperandProto(data) {}
InstructionOperandProto::InstructionOperandProto(const InstructionOperandProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InstructionOperandProto> resize
InstructionOperandProto::InstructionOperandProto(InstructionOperandProto&&) noexcept = default; 
InstructionOperandProto::InstructionOperandProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto) {
  InitFromProto(proto_instructionoperandproto);
}
InstructionOperandProto::InstructionOperandProto() = default;

InstructionOperandProto::~InstructionOperandProto() = default;

void InstructionOperandProto::InitFromProto(const PbMessage& proto_instructionoperandproto) {
  BuildFromProto(proto_instructionoperandproto);
}
  
void* InstructionOperandProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_const_operand();
    case 2: return mutable_mut_operand();
    case 3: return mutable_mut2_operand();
    case 4: return mutable_del_operand();
    case 5: return mutable_symbol_operand();
    case 6: return mutable_init_symbol_operand();
    case 7: return mutable_separator();
    case 8: return mutable_double_operand();
    case 9: return mutable_int64_operand();
    case 10: return mutable_uint64_operand();
    case 11: return mutable_bool_operand();
    default: return nullptr;
  }
}

bool InstructionOperandProto::operator==(const InstructionOperandProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InstructionOperandProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InstructionOperandProto::operator<(const InstructionOperandProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InstructionOperandProto::Clear() {
  if (data_) { data_.reset(); }
}
void InstructionOperandProto::CopyFrom(const InstructionOperandProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InstructionOperandProto& InstructionOperandProto::operator=(const InstructionOperandProto& other) {
  CopyFrom(other);
  return *this;
}

void InstructionOperandProto::clear_const_operand() {
  return __SharedPtr__()->clear_const_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_const_operand() {
  return __SharedPtr__()->mutable_const_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_const_operand() {
  return mutable_const_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_mut_operand() {
  return __SharedPtr__()->clear_mut_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_mut_operand() {
  return __SharedPtr__()->mutable_mut_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_mut_operand() {
  return mutable_mut_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_mut2_operand() {
  return __SharedPtr__()->clear_mut2_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_mut2_operand() {
  return __SharedPtr__()->mutable_mut2_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_mut2_operand() {
  return mutable_mut2_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_del_operand() {
  return __SharedPtr__()->clear_del_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_del_operand() {
  return __SharedPtr__()->mutable_del_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_del_operand() {
  return mutable_del_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_symbol_operand() {
  return __SharedPtr__()->clear_symbol_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_symbol_operand() {
  return __SharedPtr__()->mutable_symbol_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_symbol_operand() {
  return mutable_symbol_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_init_symbol_operand() {
  return __SharedPtr__()->clear_init_symbol_operand();
}
::oneflow::vm::cfg::OperandProto* InstructionOperandProto::mutable_init_symbol_operand() {
  return __SharedPtr__()->mutable_init_symbol_operand();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandProto> InstructionOperandProto::shared_mutable_init_symbol_operand() {
  return mutable_init_symbol_operand()->__SharedMutable__();
}
void InstructionOperandProto::clear_separator() {
  return __SharedPtr__()->clear_separator();
}
::oneflow::vm::cfg::OperandSeparatorProto* InstructionOperandProto::mutable_separator() {
  return __SharedPtr__()->mutable_separator();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto> InstructionOperandProto::shared_mutable_separator() {
  return mutable_separator()->__SharedMutable__();
}
void InstructionOperandProto::clear_double_operand() {
  return __SharedPtr__()->clear_double_operand();
}
void InstructionOperandProto::set_double_operand(const double& value) {
  return __SharedPtr__()->set_double_operand(value);
}
double* InstructionOperandProto::mutable_double_operand() {
  return  __SharedPtr__()->mutable_double_operand();
}
void InstructionOperandProto::clear_int64_operand() {
  return __SharedPtr__()->clear_int64_operand();
}
void InstructionOperandProto::set_int64_operand(const int64_t& value) {
  return __SharedPtr__()->set_int64_operand(value);
}
int64_t* InstructionOperandProto::mutable_int64_operand() {
  return  __SharedPtr__()->mutable_int64_operand();
}
void InstructionOperandProto::clear_uint64_operand() {
  return __SharedPtr__()->clear_uint64_operand();
}
void InstructionOperandProto::set_uint64_operand(const uint64_t& value) {
  return __SharedPtr__()->set_uint64_operand(value);
}
uint64_t* InstructionOperandProto::mutable_uint64_operand() {
  return  __SharedPtr__()->mutable_uint64_operand();
}
void InstructionOperandProto::clear_bool_operand() {
  return __SharedPtr__()->clear_bool_operand();
}
void InstructionOperandProto::set_bool_operand(const bool& value) {
  return __SharedPtr__()->set_bool_operand(value);
}
bool* InstructionOperandProto::mutable_bool_operand() {
  return  __SharedPtr__()->mutable_bool_operand();
}

::std::shared_ptr<InstructionOperandProto> InstructionOperandProto::__SharedMutable__() {
  return ::std::make_shared<InstructionOperandProto>(__SharedPtr__());
}
ConstInstructionProto::_InstructionProto_::_InstructionProto_() { Clear(); }
ConstInstructionProto::_InstructionProto_::_InstructionProto_(const _InstructionProto_& other) { CopyFrom(other); }
ConstInstructionProto::_InstructionProto_::_InstructionProto_(const ::oneflow::vm::InstructionProto& proto_instructionproto) {
  InitFromProto(proto_instructionproto);
}
ConstInstructionProto::_InstructionProto_::_InstructionProto_(_InstructionProto_&& other) = default;
ConstInstructionProto::_InstructionProto_::~_InstructionProto_() = default;

void ConstInstructionProto::_InstructionProto_::InitFromProto(const ::oneflow::vm::InstructionProto& proto_instructionproto) {
  Clear();
  // required_or_optional field: instr_type_name
  if (proto_instructionproto.has_instr_type_name()) {
    set_instr_type_name(proto_instructionproto.instr_type_name());
  }
  // required_or_optional field: parallel_desc_symbol_id
  if (proto_instructionproto.has_parallel_desc_symbol_id()) {
    set_parallel_desc_symbol_id(proto_instructionproto.parallel_desc_symbol_id());
  }
  // repeated field: operand
  if (!proto_instructionproto.operand().empty()) {
    for (const ::oneflow::vm::InstructionOperandProto& elem : proto_instructionproto.operand() ) {
      *mutable_operand()->Add() = ::oneflow::vm::cfg::InstructionOperandProto(elem);
    }
  }
    
}

void ConstInstructionProto::_InstructionProto_::ToProto(::oneflow::vm::InstructionProto* proto_instructionproto) const {
  proto_instructionproto->Clear();
  // required_or_optional field: instr_type_name
  if (this->has_instr_type_name()) {
    proto_instructionproto->set_instr_type_name(instr_type_name());
    }
  // required_or_optional field: parallel_desc_symbol_id
  if (this->has_parallel_desc_symbol_id()) {
    proto_instructionproto->set_parallel_desc_symbol_id(parallel_desc_symbol_id());
    }
  // repeated field: operand
  if (!operand().empty()) {
    for (const ::oneflow::vm::cfg::InstructionOperandProto& elem : operand() ) {
      ::oneflow::vm::InstructionOperandProto proto_operand_elem;
      elem.ToProto(&proto_operand_elem);
      *proto_instructionproto->mutable_operand()->Add() = proto_operand_elem;
    }
  }

}

::std::string ConstInstructionProto::_InstructionProto_::DebugString() const {
  ::oneflow::vm::InstructionProto proto_instructionproto;
  this->ToProto(&proto_instructionproto);
  return proto_instructionproto.DebugString();
}

void ConstInstructionProto::_InstructionProto_::Clear() {
  clear_instr_type_name();
  clear_parallel_desc_symbol_id();
  clear_operand();
}

void ConstInstructionProto::_InstructionProto_::CopyFrom(const _InstructionProto_& other) {
  if (other.has_instr_type_name()) {
    set_instr_type_name(other.instr_type_name());
  } else {
    clear_instr_type_name();
  }
  if (other.has_parallel_desc_symbol_id()) {
    set_parallel_desc_symbol_id(other.parallel_desc_symbol_id());
  } else {
    clear_parallel_desc_symbol_id();
  }
  mutable_operand()->CopyFrom(other.operand());
}


// optional field instr_type_name
bool ConstInstructionProto::_InstructionProto_::has_instr_type_name() const {
  return has_instr_type_name_;
}
const ::std::string& ConstInstructionProto::_InstructionProto_::instr_type_name() const {
  if (has_instr_type_name_) { return instr_type_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstInstructionProto::_InstructionProto_::clear_instr_type_name() {
  has_instr_type_name_ = false;
}
void ConstInstructionProto::_InstructionProto_::set_instr_type_name(const ::std::string& value) {
  instr_type_name_ = value;
  has_instr_type_name_ = true;
}
::std::string* ConstInstructionProto::_InstructionProto_::mutable_instr_type_name() {
  has_instr_type_name_ = true;
  return &instr_type_name_;
}

// optional field parallel_desc_symbol_id
bool ConstInstructionProto::_InstructionProto_::has_parallel_desc_symbol_id() const {
  return has_parallel_desc_symbol_id_;
}
const int64_t& ConstInstructionProto::_InstructionProto_::parallel_desc_symbol_id() const {
  if (has_parallel_desc_symbol_id_) { return parallel_desc_symbol_id_; }
  static const int64_t default_static_value =
    int64_t(0);
  return default_static_value;
}
void ConstInstructionProto::_InstructionProto_::clear_parallel_desc_symbol_id() {
  has_parallel_desc_symbol_id_ = false;
}
void ConstInstructionProto::_InstructionProto_::set_parallel_desc_symbol_id(const int64_t& value) {
  parallel_desc_symbol_id_ = value;
  has_parallel_desc_symbol_id_ = true;
}
int64_t* ConstInstructionProto::_InstructionProto_::mutable_parallel_desc_symbol_id() {
  has_parallel_desc_symbol_id_ = true;
  return &parallel_desc_symbol_id_;
}

// repeated field operand
::std::size_t ConstInstructionProto::_InstructionProto_::operand_size() const {
  if (!operand_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
    return default_static_value->size();
  }
  return operand_->size();
}
const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& ConstInstructionProto::_InstructionProto_::operand() const {
  if (!operand_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
    return *(default_static_value.get());
  }
  return *(operand_.get());
}
const ::oneflow::vm::cfg::InstructionOperandProto& ConstInstructionProto::_InstructionProto_::operand(::std::size_t index) const {
  if (!operand_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
    return default_static_value->Get(index);
  }
  return operand_->Get(index);
}
void ConstInstructionProto::_InstructionProto_::clear_operand() {
  if (!operand_) {
    operand_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
  }
  return operand_->Clear();
}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_* ConstInstructionProto::_InstructionProto_::mutable_operand() {
  if (!operand_) {
    operand_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
  }
  return  operand_.get();
}
::oneflow::vm::cfg::InstructionOperandProto* ConstInstructionProto::_InstructionProto_::mutable_operand(::std::size_t index) {
  if (!operand_) {
    operand_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
  }
  return  operand_->Mutable(index);
}
::oneflow::vm::cfg::InstructionOperandProto* ConstInstructionProto::_InstructionProto_::add_operand() {
  if (!operand_) {
    operand_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>();
  }
  return operand_->Add();
}


int ConstInstructionProto::_InstructionProto_::compare(const _InstructionProto_& other) {
  if (!(has_instr_type_name() == other.has_instr_type_name())) {
    return has_instr_type_name() < other.has_instr_type_name() ? -1 : 1;
  } else if (!(instr_type_name() == other.instr_type_name())) {
    return instr_type_name() < other.instr_type_name() ? -1 : 1;
  }
  if (!(has_parallel_desc_symbol_id() == other.has_parallel_desc_symbol_id())) {
    return has_parallel_desc_symbol_id() < other.has_parallel_desc_symbol_id() ? -1 : 1;
  } else if (!(parallel_desc_symbol_id() == other.parallel_desc_symbol_id())) {
    return parallel_desc_symbol_id() < other.parallel_desc_symbol_id() ? -1 : 1;
  }
  if (!(operand() == other.operand())) {
    return operand() < other.operand() ? -1 : 1;
  }
  return 0;
}

bool ConstInstructionProto::_InstructionProto_::operator==(const _InstructionProto_& other) const {
  return true
    && has_instr_type_name() == other.has_instr_type_name() 
    && instr_type_name() == other.instr_type_name()
    && has_parallel_desc_symbol_id() == other.has_parallel_desc_symbol_id() 
    && parallel_desc_symbol_id() == other.parallel_desc_symbol_id()
    && operand() == other.operand()
  ;
}

std::size_t ConstInstructionProto::_InstructionProto_::__CalcHash__() const {
  return 0
    ^ (has_instr_type_name() ? std::hash<::std::string>()(instr_type_name()) : 0)
    ^ (has_parallel_desc_symbol_id() ? std::hash<int64_t>()(parallel_desc_symbol_id()) : 0)
    ^ operand().__CalcHash__()
  ;
}

bool ConstInstructionProto::_InstructionProto_::operator<(const _InstructionProto_& other) const {
  return false
    || !(has_instr_type_name() == other.has_instr_type_name()) ? 
      has_instr_type_name() < other.has_instr_type_name() : false
    || !(instr_type_name() == other.instr_type_name()) ? 
      instr_type_name() < other.instr_type_name() : false
    || !(has_parallel_desc_symbol_id() == other.has_parallel_desc_symbol_id()) ? 
      has_parallel_desc_symbol_id() < other.has_parallel_desc_symbol_id() : false
    || !(parallel_desc_symbol_id() == other.parallel_desc_symbol_id()) ? 
      parallel_desc_symbol_id() < other.parallel_desc_symbol_id() : false
    || !(operand() == other.operand()) ? 
      operand() < other.operand() : false
  ;
}

using _InstructionProto_ =  ConstInstructionProto::_InstructionProto_;
ConstInstructionProto::ConstInstructionProto(const ::std::shared_ptr<_InstructionProto_>& data): data_(data) {}
ConstInstructionProto::ConstInstructionProto(): data_(::std::make_shared<_InstructionProto_>()) {}
ConstInstructionProto::ConstInstructionProto(const ::oneflow::vm::InstructionProto& proto_instructionproto) {
  BuildFromProto(proto_instructionproto);
}
ConstInstructionProto::ConstInstructionProto(const ConstInstructionProto&) = default;
ConstInstructionProto::ConstInstructionProto(ConstInstructionProto&&) noexcept = default;
ConstInstructionProto::~ConstInstructionProto() = default;

void ConstInstructionProto::ToProto(PbMessage* proto_instructionproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::InstructionProto*>(proto_instructionproto));
}
  
::std::string ConstInstructionProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInstructionProto::__Empty__() const {
  return !data_;
}

int ConstInstructionProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"instr_type_name", 1},
    {"parallel_desc_symbol_id", 2},
    {"operand", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInstructionProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInstructionProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionOperandProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInstructionProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &instr_type_name();
    case 2: return &parallel_desc_symbol_id();
    case 3: return &operand();
    default: return nullptr;
  }
}

// required or optional field instr_type_name
bool ConstInstructionProto::has_instr_type_name() const {
  return __SharedPtrOrDefault__()->has_instr_type_name();
}
const ::std::string& ConstInstructionProto::instr_type_name() const {
  return __SharedPtrOrDefault__()->instr_type_name();
}
// used by pybind11 only
// required or optional field parallel_desc_symbol_id
bool ConstInstructionProto::has_parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->has_parallel_desc_symbol_id();
}
const int64_t& ConstInstructionProto::parallel_desc_symbol_id() const {
  return __SharedPtrOrDefault__()->parallel_desc_symbol_id();
}
// used by pybind11 only
// repeated field operand
::std::size_t ConstInstructionProto::operand_size() const {
  return __SharedPtrOrDefault__()->operand_size();
}
const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& ConstInstructionProto::operand() const {
  return __SharedPtrOrDefault__()->operand();
}
const ::oneflow::vm::cfg::InstructionOperandProto& ConstInstructionProto::operand(::std::size_t index) const {
  return __SharedPtrOrDefault__()->operand(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> ConstInstructionProto::shared_const_operand() const {
  return operand().__SharedConst__();
}
::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionOperandProto> ConstInstructionProto::shared_const_operand(::std::size_t index) const {
  return operand(index).__SharedConst__();
}

::std::shared_ptr<ConstInstructionProto> ConstInstructionProto::__SharedConst__() const {
  return ::std::make_shared<ConstInstructionProto>(data_);
}
int64_t ConstInstructionProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInstructionProto::operator==(const ConstInstructionProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInstructionProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInstructionProto::operator<(const ConstInstructionProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InstructionProto_>& ConstInstructionProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InstructionProto_> default_ptr = std::make_shared<_InstructionProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_InstructionProto_>& ConstInstructionProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _InstructionProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInstructionProto
void ConstInstructionProto::BuildFromProto(const PbMessage& proto_instructionproto) {
  data_ = ::std::make_shared<_InstructionProto_>(dynamic_cast<const ::oneflow::vm::InstructionProto&>(proto_instructionproto));
}

InstructionProto::InstructionProto(const ::std::shared_ptr<_InstructionProto_>& data)
  : ConstInstructionProto(data) {}
InstructionProto::InstructionProto(const InstructionProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InstructionProto> resize
InstructionProto::InstructionProto(InstructionProto&&) noexcept = default; 
InstructionProto::InstructionProto(const ::oneflow::vm::InstructionProto& proto_instructionproto) {
  InitFromProto(proto_instructionproto);
}
InstructionProto::InstructionProto() = default;

InstructionProto::~InstructionProto() = default;

void InstructionProto::InitFromProto(const PbMessage& proto_instructionproto) {
  BuildFromProto(proto_instructionproto);
}
  
void* InstructionProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_instr_type_name();
    case 2: return mutable_parallel_desc_symbol_id();
    case 3: return mutable_operand();
    default: return nullptr;
  }
}

bool InstructionProto::operator==(const InstructionProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InstructionProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InstructionProto::operator<(const InstructionProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InstructionProto::Clear() {
  if (data_) { data_.reset(); }
}
void InstructionProto::CopyFrom(const InstructionProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InstructionProto& InstructionProto::operator=(const InstructionProto& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field instr_type_name
void InstructionProto::clear_instr_type_name() {
  return __SharedPtr__()->clear_instr_type_name();
}
void InstructionProto::set_instr_type_name(const ::std::string& value) {
  return __SharedPtr__()->set_instr_type_name(value);
}
::std::string* InstructionProto::mutable_instr_type_name() {
  return  __SharedPtr__()->mutable_instr_type_name();
}
// required or optional field parallel_desc_symbol_id
void InstructionProto::clear_parallel_desc_symbol_id() {
  return __SharedPtr__()->clear_parallel_desc_symbol_id();
}
void InstructionProto::set_parallel_desc_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_parallel_desc_symbol_id(value);
}
int64_t* InstructionProto::mutable_parallel_desc_symbol_id() {
  return  __SharedPtr__()->mutable_parallel_desc_symbol_id();
}
// repeated field operand
void InstructionProto::clear_operand() {
  return __SharedPtr__()->clear_operand();
}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_* InstructionProto::mutable_operand() {
  return __SharedPtr__()->mutable_operand();
}
::oneflow::vm::cfg::InstructionOperandProto* InstructionProto::mutable_operand(::std::size_t index) {
  return __SharedPtr__()->mutable_operand(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> InstructionProto::shared_mutable_operand() {
  return mutable_operand()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> InstructionProto::shared_mutable_operand(::std::size_t index) {
  return mutable_operand(index)->__SharedMutable__();
}
::oneflow::vm::cfg::InstructionOperandProto* InstructionProto::add_operand() {
  return __SharedPtr__()->add_operand();
}

::std::shared_ptr<InstructionProto> InstructionProto::__SharedMutable__() {
  return ::std::make_shared<InstructionProto>(__SharedPtr__());
}
ConstInstructionListProto::_InstructionListProto_::_InstructionListProto_() { Clear(); }
ConstInstructionListProto::_InstructionListProto_::_InstructionListProto_(const _InstructionListProto_& other) { CopyFrom(other); }
ConstInstructionListProto::_InstructionListProto_::_InstructionListProto_(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto) {
  InitFromProto(proto_instructionlistproto);
}
ConstInstructionListProto::_InstructionListProto_::_InstructionListProto_(_InstructionListProto_&& other) = default;
ConstInstructionListProto::_InstructionListProto_::~_InstructionListProto_() = default;

void ConstInstructionListProto::_InstructionListProto_::InitFromProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto) {
  Clear();
  // repeated field: instruction
  if (!proto_instructionlistproto.instruction().empty()) {
    for (const ::oneflow::vm::InstructionProto& elem : proto_instructionlistproto.instruction() ) {
      *mutable_instruction()->Add() = ::oneflow::vm::cfg::InstructionProto(elem);
    }
  }
    
}

void ConstInstructionListProto::_InstructionListProto_::ToProto(::oneflow::vm::InstructionListProto* proto_instructionlistproto) const {
  proto_instructionlistproto->Clear();
  // repeated field: instruction
  if (!instruction().empty()) {
    for (const ::oneflow::vm::cfg::InstructionProto& elem : instruction() ) {
      ::oneflow::vm::InstructionProto proto_instruction_elem;
      elem.ToProto(&proto_instruction_elem);
      *proto_instructionlistproto->mutable_instruction()->Add() = proto_instruction_elem;
    }
  }

}

::std::string ConstInstructionListProto::_InstructionListProto_::DebugString() const {
  ::oneflow::vm::InstructionListProto proto_instructionlistproto;
  this->ToProto(&proto_instructionlistproto);
  return proto_instructionlistproto.DebugString();
}

void ConstInstructionListProto::_InstructionListProto_::Clear() {
  clear_instruction();
}

void ConstInstructionListProto::_InstructionListProto_::CopyFrom(const _InstructionListProto_& other) {
  mutable_instruction()->CopyFrom(other.instruction());
}


// repeated field instruction
::std::size_t ConstInstructionListProto::_InstructionListProto_::instruction_size() const {
  if (!instruction_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
    return default_static_value->size();
  }
  return instruction_->size();
}
const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& ConstInstructionListProto::_InstructionListProto_::instruction() const {
  if (!instruction_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
    return *(default_static_value.get());
  }
  return *(instruction_.get());
}
const ::oneflow::vm::cfg::InstructionProto& ConstInstructionListProto::_InstructionListProto_::instruction(::std::size_t index) const {
  if (!instruction_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
    return default_static_value->Get(index);
  }
  return instruction_->Get(index);
}
void ConstInstructionListProto::_InstructionListProto_::clear_instruction() {
  if (!instruction_) {
    instruction_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
  }
  return instruction_->Clear();
}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_* ConstInstructionListProto::_InstructionListProto_::mutable_instruction() {
  if (!instruction_) {
    instruction_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
  }
  return  instruction_.get();
}
::oneflow::vm::cfg::InstructionProto* ConstInstructionListProto::_InstructionListProto_::mutable_instruction(::std::size_t index) {
  if (!instruction_) {
    instruction_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
  }
  return  instruction_->Mutable(index);
}
::oneflow::vm::cfg::InstructionProto* ConstInstructionListProto::_InstructionListProto_::add_instruction() {
  if (!instruction_) {
    instruction_ = ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>();
  }
  return instruction_->Add();
}


int ConstInstructionListProto::_InstructionListProto_::compare(const _InstructionListProto_& other) {
  if (!(instruction() == other.instruction())) {
    return instruction() < other.instruction() ? -1 : 1;
  }
  return 0;
}

bool ConstInstructionListProto::_InstructionListProto_::operator==(const _InstructionListProto_& other) const {
  return true
    && instruction() == other.instruction()
  ;
}

std::size_t ConstInstructionListProto::_InstructionListProto_::__CalcHash__() const {
  return 0
    ^ instruction().__CalcHash__()
  ;
}

bool ConstInstructionListProto::_InstructionListProto_::operator<(const _InstructionListProto_& other) const {
  return false
    || !(instruction() == other.instruction()) ? 
      instruction() < other.instruction() : false
  ;
}

using _InstructionListProto_ =  ConstInstructionListProto::_InstructionListProto_;
ConstInstructionListProto::ConstInstructionListProto(const ::std::shared_ptr<_InstructionListProto_>& data): data_(data) {}
ConstInstructionListProto::ConstInstructionListProto(): data_(::std::make_shared<_InstructionListProto_>()) {}
ConstInstructionListProto::ConstInstructionListProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto) {
  BuildFromProto(proto_instructionlistproto);
}
ConstInstructionListProto::ConstInstructionListProto(const ConstInstructionListProto&) = default;
ConstInstructionListProto::ConstInstructionListProto(ConstInstructionListProto&&) noexcept = default;
ConstInstructionListProto::~ConstInstructionListProto() = default;

void ConstInstructionListProto::ToProto(PbMessage* proto_instructionlistproto) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::InstructionListProto*>(proto_instructionlistproto));
}
  
::std::string ConstInstructionListProto::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInstructionListProto::__Empty__() const {
  return !data_;
}

int ConstInstructionListProto::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"instruction", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInstructionListProto::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInstructionListProto::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInstructionListProto::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &instruction();
    default: return nullptr;
  }
}

// repeated field instruction
::std::size_t ConstInstructionListProto::instruction_size() const {
  return __SharedPtrOrDefault__()->instruction_size();
}
const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& ConstInstructionListProto::instruction() const {
  return __SharedPtrOrDefault__()->instruction();
}
const ::oneflow::vm::cfg::InstructionProto& ConstInstructionListProto::instruction(::std::size_t index) const {
  return __SharedPtrOrDefault__()->instruction(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> ConstInstructionListProto::shared_const_instruction() const {
  return instruction().__SharedConst__();
}
::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionProto> ConstInstructionListProto::shared_const_instruction(::std::size_t index) const {
  return instruction(index).__SharedConst__();
}

::std::shared_ptr<ConstInstructionListProto> ConstInstructionListProto::__SharedConst__() const {
  return ::std::make_shared<ConstInstructionListProto>(data_);
}
int64_t ConstInstructionListProto::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInstructionListProto::operator==(const ConstInstructionListProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInstructionListProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInstructionListProto::operator<(const ConstInstructionListProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InstructionListProto_>& ConstInstructionListProto::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InstructionListProto_> default_ptr = std::make_shared<_InstructionListProto_>();
  return default_ptr;
}
const ::std::shared_ptr<_InstructionListProto_>& ConstInstructionListProto::__SharedPtr__() {
  if (!data_) { data_.reset(new _InstructionListProto_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInstructionListProto
void ConstInstructionListProto::BuildFromProto(const PbMessage& proto_instructionlistproto) {
  data_ = ::std::make_shared<_InstructionListProto_>(dynamic_cast<const ::oneflow::vm::InstructionListProto&>(proto_instructionlistproto));
}

InstructionListProto::InstructionListProto(const ::std::shared_ptr<_InstructionListProto_>& data)
  : ConstInstructionListProto(data) {}
InstructionListProto::InstructionListProto(const InstructionListProto& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InstructionListProto> resize
InstructionListProto::InstructionListProto(InstructionListProto&&) noexcept = default; 
InstructionListProto::InstructionListProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto) {
  InitFromProto(proto_instructionlistproto);
}
InstructionListProto::InstructionListProto() = default;

InstructionListProto::~InstructionListProto() = default;

void InstructionListProto::InitFromProto(const PbMessage& proto_instructionlistproto) {
  BuildFromProto(proto_instructionlistproto);
}
  
void* InstructionListProto::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_instruction();
    default: return nullptr;
  }
}

bool InstructionListProto::operator==(const InstructionListProto& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InstructionListProto::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InstructionListProto::operator<(const InstructionListProto& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InstructionListProto::Clear() {
  if (data_) { data_.reset(); }
}
void InstructionListProto::CopyFrom(const InstructionListProto& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InstructionListProto& InstructionListProto::operator=(const InstructionListProto& other) {
  CopyFrom(other);
  return *this;
}

// repeated field instruction
void InstructionListProto::clear_instruction() {
  return __SharedPtr__()->clear_instruction();
}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_* InstructionListProto::mutable_instruction() {
  return __SharedPtr__()->mutable_instruction();
}
::oneflow::vm::cfg::InstructionProto* InstructionListProto::mutable_instruction(::std::size_t index) {
  return __SharedPtr__()->mutable_instruction(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> InstructionListProto::shared_mutable_instruction() {
  return mutable_instruction()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> InstructionListProto::shared_mutable_instruction(::std::size_t index) {
  return mutable_instruction(index)->__SharedMutable__();
}
::oneflow::vm::cfg::InstructionProto* InstructionListProto::add_instruction() {
  return __SharedPtr__()->add_instruction();
}

::std::shared_ptr<InstructionListProto> InstructionListProto::__SharedMutable__() {
  return ::std::make_shared<InstructionListProto>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionOperandProto>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionOperandProto>(data) {}
Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_() = default;
Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::~Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_() = default;


bool Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::operator==(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::InstructionOperandProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::operator<(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionOperandProto> Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionOperandProto>>& data): Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_(data) {}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_() = default;
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::~_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_() = default;

void _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionOperandProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::CopyFrom(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionOperandProto>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::operator==(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::InstructionOperandProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::operator<(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionProto>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionProto>(data) {}
Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_() = default;
Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::~Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_() = default;


bool Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::operator==(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::InstructionProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::operator<(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionProto> Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionProto>>& data): Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_(data) {}
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_() = default;
_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::~_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_() = default;

void _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::CopyFrom(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionProto>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::operator==(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::InstructionProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::operator<(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
} // namespace vm
