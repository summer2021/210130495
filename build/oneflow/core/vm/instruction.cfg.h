#ifndef CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H_
#define CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {
namespace vm {

// forward declare proto class;
class CurrentGlobalDeviceIdProto;
class SoleMirroredObjectProto;
class AllMirroredObjectProto;
class OperandProto;
class OperandSeparatorProto;
class InstructionOperandProto;
class InstructionProto;
class InstructionListProto;

namespace cfg {


class CurrentGlobalDeviceIdProto;
class ConstCurrentGlobalDeviceIdProto;

class SoleMirroredObjectProto;
class ConstSoleMirroredObjectProto;

class AllMirroredObjectProto;
class ConstAllMirroredObjectProto;

class OperandProto;
class ConstOperandProto;

class OperandSeparatorProto;
class ConstOperandSeparatorProto;

class InstructionOperandProto;
class ConstInstructionOperandProto;

class InstructionProto;
class ConstInstructionProto;

class InstructionListProto;
class ConstInstructionListProto;



class ConstCurrentGlobalDeviceIdProto : public ::oneflow::cfg::Message {
 public:

  class _CurrentGlobalDeviceIdProto_ {
   public:
    _CurrentGlobalDeviceIdProto_();
    explicit _CurrentGlobalDeviceIdProto_(const _CurrentGlobalDeviceIdProto_& other);
    explicit _CurrentGlobalDeviceIdProto_(_CurrentGlobalDeviceIdProto_&& other);
    _CurrentGlobalDeviceIdProto_(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto);
    ~_CurrentGlobalDeviceIdProto_();

    void InitFromProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto);

    void ToProto(::oneflow::vm::CurrentGlobalDeviceIdProto* proto_currentglobaldeviceidproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CurrentGlobalDeviceIdProto_& other);
       
   public:
    int compare(const _CurrentGlobalDeviceIdProto_& other);

    bool operator==(const _CurrentGlobalDeviceIdProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CurrentGlobalDeviceIdProto_& other) const;
  };

  ConstCurrentGlobalDeviceIdProto(const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& data);
  ConstCurrentGlobalDeviceIdProto(const ConstCurrentGlobalDeviceIdProto&);
  ConstCurrentGlobalDeviceIdProto(ConstCurrentGlobalDeviceIdProto&&) noexcept;
  ConstCurrentGlobalDeviceIdProto();
  ConstCurrentGlobalDeviceIdProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto);
  virtual ~ConstCurrentGlobalDeviceIdProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_currentglobaldeviceidproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstCurrentGlobalDeviceIdProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCurrentGlobalDeviceIdProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCurrentGlobalDeviceIdProto& other) const;
 protected:
  const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCurrentGlobalDeviceIdProto
  void BuildFromProto(const PbMessage& proto_currentglobaldeviceidproto);
  
  ::std::shared_ptr<_CurrentGlobalDeviceIdProto_> data_;
};

class CurrentGlobalDeviceIdProto final : public ConstCurrentGlobalDeviceIdProto {
 public:
  CurrentGlobalDeviceIdProto(const ::std::shared_ptr<_CurrentGlobalDeviceIdProto_>& data);
  CurrentGlobalDeviceIdProto(const CurrentGlobalDeviceIdProto& other);
  // enable nothrow for ::std::vector<CurrentGlobalDeviceIdProto> resize 
  CurrentGlobalDeviceIdProto(CurrentGlobalDeviceIdProto&&) noexcept;
  CurrentGlobalDeviceIdProto();
  explicit CurrentGlobalDeviceIdProto(const ::oneflow::vm::CurrentGlobalDeviceIdProto& proto_currentglobaldeviceidproto);

  ~CurrentGlobalDeviceIdProto() override;

  void InitFromProto(const PbMessage& proto_currentglobaldeviceidproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CurrentGlobalDeviceIdProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CurrentGlobalDeviceIdProto& other) const;
  void Clear();
  void CopyFrom(const CurrentGlobalDeviceIdProto& other);
  CurrentGlobalDeviceIdProto& operator=(const CurrentGlobalDeviceIdProto& other);


  ::std::shared_ptr<CurrentGlobalDeviceIdProto> __SharedMutable__();
};


class ConstSoleMirroredObjectProto : public ::oneflow::cfg::Message {
 public:

  class _SoleMirroredObjectProto_ {
   public:
    _SoleMirroredObjectProto_();
    explicit _SoleMirroredObjectProto_(const _SoleMirroredObjectProto_& other);
    explicit _SoleMirroredObjectProto_(_SoleMirroredObjectProto_&& other);
    _SoleMirroredObjectProto_(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto);
    ~_SoleMirroredObjectProto_();

    void InitFromProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto);

    void ToProto(::oneflow::vm::SoleMirroredObjectProto* proto_solemirroredobjectproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SoleMirroredObjectProto_& other);
       
   public:
    int compare(const _SoleMirroredObjectProto_& other);

    bool operator==(const _SoleMirroredObjectProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SoleMirroredObjectProto_& other) const;
  };

  ConstSoleMirroredObjectProto(const ::std::shared_ptr<_SoleMirroredObjectProto_>& data);
  ConstSoleMirroredObjectProto(const ConstSoleMirroredObjectProto&);
  ConstSoleMirroredObjectProto(ConstSoleMirroredObjectProto&&) noexcept;
  ConstSoleMirroredObjectProto();
  ConstSoleMirroredObjectProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto);
  virtual ~ConstSoleMirroredObjectProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_solemirroredobjectproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstSoleMirroredObjectProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSoleMirroredObjectProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSoleMirroredObjectProto& other) const;
 protected:
  const ::std::shared_ptr<_SoleMirroredObjectProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SoleMirroredObjectProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSoleMirroredObjectProto
  void BuildFromProto(const PbMessage& proto_solemirroredobjectproto);
  
  ::std::shared_ptr<_SoleMirroredObjectProto_> data_;
};

class SoleMirroredObjectProto final : public ConstSoleMirroredObjectProto {
 public:
  SoleMirroredObjectProto(const ::std::shared_ptr<_SoleMirroredObjectProto_>& data);
  SoleMirroredObjectProto(const SoleMirroredObjectProto& other);
  // enable nothrow for ::std::vector<SoleMirroredObjectProto> resize 
  SoleMirroredObjectProto(SoleMirroredObjectProto&&) noexcept;
  SoleMirroredObjectProto();
  explicit SoleMirroredObjectProto(const ::oneflow::vm::SoleMirroredObjectProto& proto_solemirroredobjectproto);

  ~SoleMirroredObjectProto() override;

  void InitFromProto(const PbMessage& proto_solemirroredobjectproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SoleMirroredObjectProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SoleMirroredObjectProto& other) const;
  void Clear();
  void CopyFrom(const SoleMirroredObjectProto& other);
  SoleMirroredObjectProto& operator=(const SoleMirroredObjectProto& other);


  ::std::shared_ptr<SoleMirroredObjectProto> __SharedMutable__();
};


class ConstAllMirroredObjectProto : public ::oneflow::cfg::Message {
 public:

  class _AllMirroredObjectProto_ {
   public:
    _AllMirroredObjectProto_();
    explicit _AllMirroredObjectProto_(const _AllMirroredObjectProto_& other);
    explicit _AllMirroredObjectProto_(_AllMirroredObjectProto_&& other);
    _AllMirroredObjectProto_(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto);
    ~_AllMirroredObjectProto_();

    void InitFromProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto);

    void ToProto(::oneflow::vm::AllMirroredObjectProto* proto_allmirroredobjectproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AllMirroredObjectProto_& other);
       
   public:
    int compare(const _AllMirroredObjectProto_& other);

    bool operator==(const _AllMirroredObjectProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AllMirroredObjectProto_& other) const;
  };

  ConstAllMirroredObjectProto(const ::std::shared_ptr<_AllMirroredObjectProto_>& data);
  ConstAllMirroredObjectProto(const ConstAllMirroredObjectProto&);
  ConstAllMirroredObjectProto(ConstAllMirroredObjectProto&&) noexcept;
  ConstAllMirroredObjectProto();
  ConstAllMirroredObjectProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto);
  virtual ~ConstAllMirroredObjectProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_allmirroredobjectproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstAllMirroredObjectProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAllMirroredObjectProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAllMirroredObjectProto& other) const;
 protected:
  const ::std::shared_ptr<_AllMirroredObjectProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AllMirroredObjectProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAllMirroredObjectProto
  void BuildFromProto(const PbMessage& proto_allmirroredobjectproto);
  
  ::std::shared_ptr<_AllMirroredObjectProto_> data_;
};

class AllMirroredObjectProto final : public ConstAllMirroredObjectProto {
 public:
  AllMirroredObjectProto(const ::std::shared_ptr<_AllMirroredObjectProto_>& data);
  AllMirroredObjectProto(const AllMirroredObjectProto& other);
  // enable nothrow for ::std::vector<AllMirroredObjectProto> resize 
  AllMirroredObjectProto(AllMirroredObjectProto&&) noexcept;
  AllMirroredObjectProto();
  explicit AllMirroredObjectProto(const ::oneflow::vm::AllMirroredObjectProto& proto_allmirroredobjectproto);

  ~AllMirroredObjectProto() override;

  void InitFromProto(const PbMessage& proto_allmirroredobjectproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AllMirroredObjectProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AllMirroredObjectProto& other) const;
  void Clear();
  void CopyFrom(const AllMirroredObjectProto& other);
  AllMirroredObjectProto& operator=(const AllMirroredObjectProto& other);


  ::std::shared_ptr<AllMirroredObjectProto> __SharedMutable__();
};


class ConstOperandProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum operand_type
 enum OperandTypeCase : unsigned int {
  OPERAND_TYPE_NOT_SET = 0,
    kCurrentGlobalDeviceId = 2,
    kSoleMirroredObject = 3,
    kAllMirroredObject = 4,
   };

  class _OperandProto_ {
   public:
    _OperandProto_();
    explicit _OperandProto_(const _OperandProto_& other);
    explicit _OperandProto_(_OperandProto_&& other);
    _OperandProto_(const ::oneflow::vm::OperandProto& proto_operandproto);
    ~_OperandProto_();

    void InitFromProto(const ::oneflow::vm::OperandProto& proto_operandproto);

    void ToProto(::oneflow::vm::OperandProto* proto_operandproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OperandProto_& other);
  
      // optional field logical_object_id
     public:
    bool has_logical_object_id() const;
    const int64_t& logical_object_id() const;
    void clear_logical_object_id();
    void set_logical_object_id(const int64_t& value);
    int64_t* mutable_logical_object_id();
   protected:
    bool has_logical_object_id_ = false;
    int64_t logical_object_id_;
      
     // oneof field operand_type: current_global_device_id
   public:
    bool has_current_global_device_id() const;
    void clear_current_global_device_id();
    const ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto& current_global_device_id() const;
      ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto* mutable_current_global_device_id();
      
     // oneof field operand_type: sole_mirrored_object
   public:
    bool has_sole_mirrored_object() const;
    void clear_sole_mirrored_object();
    const ::oneflow::vm::cfg::SoleMirroredObjectProto& sole_mirrored_object() const;
      ::oneflow::vm::cfg::SoleMirroredObjectProto* mutable_sole_mirrored_object();
      
     // oneof field operand_type: all_mirrored_object
   public:
    bool has_all_mirrored_object() const;
    void clear_all_mirrored_object();
    const ::oneflow::vm::cfg::AllMirroredObjectProto& all_mirrored_object() const;
      ::oneflow::vm::cfg::AllMirroredObjectProto* mutable_all_mirrored_object();
           
   public:
    // oneof operand_type
    OperandTypeCase operand_type_case() const;
    bool has_operand_type() const;
   protected:
    void clear_operand_type();
    void operand_type_copy_from(const _OperandProto_& other);
    union OperandTypeUnion {
      // 64-bit aligned
      uint64_t __operand_type_for_padding_64bit__;
          char current_global_device_id_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto>)];
            char sole_mirrored_object_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto>)];
            char all_mirrored_object_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto>)];
        } operand_type_;
    OperandTypeCase operand_type_case_ = OPERAND_TYPE_NOT_SET;
     
   public:
    int compare(const _OperandProto_& other);

    bool operator==(const _OperandProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OperandProto_& other) const;
  };

  ConstOperandProto(const ::std::shared_ptr<_OperandProto_>& data);
  ConstOperandProto(const ConstOperandProto&);
  ConstOperandProto(ConstOperandProto&&) noexcept;
  ConstOperandProto();
  ConstOperandProto(const ::oneflow::vm::OperandProto& proto_operandproto);
  virtual ~ConstOperandProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_operandproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field logical_object_id
 public:
  bool has_logical_object_id() const;
  const int64_t& logical_object_id() const;
  // used by pybind11 only
 // oneof field operand_type: current_global_device_id
 public:
  bool has_current_global_device_id() const;
  const ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto& current_global_device_id() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstCurrentGlobalDeviceIdProto> shared_const_current_global_device_id() const;
 // oneof field operand_type: sole_mirrored_object
 public:
  bool has_sole_mirrored_object() const;
  const ::oneflow::vm::cfg::SoleMirroredObjectProto& sole_mirrored_object() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstSoleMirroredObjectProto> shared_const_sole_mirrored_object() const;
 // oneof field operand_type: all_mirrored_object
 public:
  bool has_all_mirrored_object() const;
  const ::oneflow::vm::cfg::AllMirroredObjectProto& all_mirrored_object() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstAllMirroredObjectProto> shared_const_all_mirrored_object() const;
 public:
  OperandTypeCase operand_type_case() const;
 protected:
  bool has_operand_type() const;

 public:
  ::std::shared_ptr<ConstOperandProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOperandProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOperandProto& other) const;
 protected:
  const ::std::shared_ptr<_OperandProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OperandProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOperandProto
  void BuildFromProto(const PbMessage& proto_operandproto);
  
  ::std::shared_ptr<_OperandProto_> data_;
};

class OperandProto final : public ConstOperandProto {
 public:
  OperandProto(const ::std::shared_ptr<_OperandProto_>& data);
  OperandProto(const OperandProto& other);
  // enable nothrow for ::std::vector<OperandProto> resize 
  OperandProto(OperandProto&&) noexcept;
  OperandProto();
  explicit OperandProto(const ::oneflow::vm::OperandProto& proto_operandproto);

  ~OperandProto() override;

  void InitFromProto(const PbMessage& proto_operandproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OperandProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OperandProto& other) const;
  void Clear();
  void CopyFrom(const OperandProto& other);
  OperandProto& operator=(const OperandProto& other);

  // required or optional field logical_object_id
 public:
  void clear_logical_object_id();
  void set_logical_object_id(const int64_t& value);
  int64_t* mutable_logical_object_id();
  void clear_current_global_device_id();
  ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto* mutable_current_global_device_id();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto> shared_mutable_current_global_device_id();
  void clear_sole_mirrored_object();
  ::oneflow::vm::cfg::SoleMirroredObjectProto* mutable_sole_mirrored_object();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::SoleMirroredObjectProto> shared_mutable_sole_mirrored_object();
  void clear_all_mirrored_object();
  ::oneflow::vm::cfg::AllMirroredObjectProto* mutable_all_mirrored_object();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::AllMirroredObjectProto> shared_mutable_all_mirrored_object();

  ::std::shared_ptr<OperandProto> __SharedMutable__();
};


class ConstOperandSeparatorProto : public ::oneflow::cfg::Message {
 public:

  class _OperandSeparatorProto_ {
   public:
    _OperandSeparatorProto_();
    explicit _OperandSeparatorProto_(const _OperandSeparatorProto_& other);
    explicit _OperandSeparatorProto_(_OperandSeparatorProto_&& other);
    _OperandSeparatorProto_(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto);
    ~_OperandSeparatorProto_();

    void InitFromProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto);

    void ToProto(::oneflow::vm::OperandSeparatorProto* proto_operandseparatorproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OperandSeparatorProto_& other);
       
   public:
    int compare(const _OperandSeparatorProto_& other);

    bool operator==(const _OperandSeparatorProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OperandSeparatorProto_& other) const;
  };

  ConstOperandSeparatorProto(const ::std::shared_ptr<_OperandSeparatorProto_>& data);
  ConstOperandSeparatorProto(const ConstOperandSeparatorProto&);
  ConstOperandSeparatorProto(ConstOperandSeparatorProto&&) noexcept;
  ConstOperandSeparatorProto();
  ConstOperandSeparatorProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto);
  virtual ~ConstOperandSeparatorProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_operandseparatorproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstOperandSeparatorProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOperandSeparatorProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOperandSeparatorProto& other) const;
 protected:
  const ::std::shared_ptr<_OperandSeparatorProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OperandSeparatorProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOperandSeparatorProto
  void BuildFromProto(const PbMessage& proto_operandseparatorproto);
  
  ::std::shared_ptr<_OperandSeparatorProto_> data_;
};

class OperandSeparatorProto final : public ConstOperandSeparatorProto {
 public:
  OperandSeparatorProto(const ::std::shared_ptr<_OperandSeparatorProto_>& data);
  OperandSeparatorProto(const OperandSeparatorProto& other);
  // enable nothrow for ::std::vector<OperandSeparatorProto> resize 
  OperandSeparatorProto(OperandSeparatorProto&&) noexcept;
  OperandSeparatorProto();
  explicit OperandSeparatorProto(const ::oneflow::vm::OperandSeparatorProto& proto_operandseparatorproto);

  ~OperandSeparatorProto() override;

  void InitFromProto(const PbMessage& proto_operandseparatorproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OperandSeparatorProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OperandSeparatorProto& other) const;
  void Clear();
  void CopyFrom(const OperandSeparatorProto& other);
  OperandSeparatorProto& operator=(const OperandSeparatorProto& other);


  ::std::shared_ptr<OperandSeparatorProto> __SharedMutable__();
};


class ConstInstructionOperandProto : public ::oneflow::cfg::Message {
 public:

 // oneof enum type
 enum TypeCase : unsigned int {
  TYPE_NOT_SET = 0,
    kConstOperand = 1,
    kMutOperand = 2,
    kMut2Operand = 3,
    kDelOperand = 4,
    kSymbolOperand = 5,
    kInitSymbolOperand = 6,
    kSeparator = 7,
    kDoubleOperand = 8,
    kInt64Operand = 9,
    kUint64Operand = 10,
    kBoolOperand = 11,
   };

  class _InstructionOperandProto_ {
   public:
    _InstructionOperandProto_();
    explicit _InstructionOperandProto_(const _InstructionOperandProto_& other);
    explicit _InstructionOperandProto_(_InstructionOperandProto_&& other);
    _InstructionOperandProto_(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto);
    ~_InstructionOperandProto_();

    void InitFromProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto);

    void ToProto(::oneflow::vm::InstructionOperandProto* proto_instructionoperandproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InstructionOperandProto_& other);
  
     // oneof field type: const_operand
   public:
    bool has_const_operand() const;
    void clear_const_operand();
    const ::oneflow::vm::cfg::OperandProto& const_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_const_operand();
      
     // oneof field type: mut_operand
   public:
    bool has_mut_operand() const;
    void clear_mut_operand();
    const ::oneflow::vm::cfg::OperandProto& mut_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_mut_operand();
      
     // oneof field type: mut2_operand
   public:
    bool has_mut2_operand() const;
    void clear_mut2_operand();
    const ::oneflow::vm::cfg::OperandProto& mut2_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_mut2_operand();
      
     // oneof field type: del_operand
   public:
    bool has_del_operand() const;
    void clear_del_operand();
    const ::oneflow::vm::cfg::OperandProto& del_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_del_operand();
      
     // oneof field type: symbol_operand
   public:
    bool has_symbol_operand() const;
    void clear_symbol_operand();
    const ::oneflow::vm::cfg::OperandProto& symbol_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_symbol_operand();
      
     // oneof field type: init_symbol_operand
   public:
    bool has_init_symbol_operand() const;
    void clear_init_symbol_operand();
    const ::oneflow::vm::cfg::OperandProto& init_symbol_operand() const;
      ::oneflow::vm::cfg::OperandProto* mutable_init_symbol_operand();
      
     // oneof field type: separator
   public:
    bool has_separator() const;
    void clear_separator();
    const ::oneflow::vm::cfg::OperandSeparatorProto& separator() const;
      ::oneflow::vm::cfg::OperandSeparatorProto* mutable_separator();
      
     // oneof field type: double_operand
   public:
    bool has_double_operand() const;
    void clear_double_operand();
    const double& double_operand() const;
      void set_double_operand(const double& value);
    double* mutable_double_operand();
      
     // oneof field type: int64_operand
   public:
    bool has_int64_operand() const;
    void clear_int64_operand();
    const int64_t& int64_operand() const;
      void set_int64_operand(const int64_t& value);
    int64_t* mutable_int64_operand();
      
     // oneof field type: uint64_operand
   public:
    bool has_uint64_operand() const;
    void clear_uint64_operand();
    const uint64_t& uint64_operand() const;
      void set_uint64_operand(const uint64_t& value);
    uint64_t* mutable_uint64_operand();
      
     // oneof field type: bool_operand
   public:
    bool has_bool_operand() const;
    void clear_bool_operand();
    const bool& bool_operand() const;
      void set_bool_operand(const bool& value);
    bool* mutable_bool_operand();
           
   public:
    // oneof type
    TypeCase type_case() const;
    bool has_type() const;
   protected:
    void clear_type();
    void type_copy_from(const _InstructionOperandProto_& other);
    union TypeUnion {
      // 64-bit aligned
      uint64_t __type_for_padding_64bit__;
          char const_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char mut_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char mut2_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char del_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char symbol_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char init_symbol_operand_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandProto>)];
            char separator_[sizeof(::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto>)];
            double double_operand_;
            int64_t int64_operand_;
            uint64_t uint64_operand_;
            bool bool_operand_;
        } type_;
    TypeCase type_case_ = TYPE_NOT_SET;
     
   public:
    int compare(const _InstructionOperandProto_& other);

    bool operator==(const _InstructionOperandProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InstructionOperandProto_& other) const;
  };

  ConstInstructionOperandProto(const ::std::shared_ptr<_InstructionOperandProto_>& data);
  ConstInstructionOperandProto(const ConstInstructionOperandProto&);
  ConstInstructionOperandProto(ConstInstructionOperandProto&&) noexcept;
  ConstInstructionOperandProto();
  ConstInstructionOperandProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto);
  virtual ~ConstInstructionOperandProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_instructionoperandproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field type: const_operand
 public:
  bool has_const_operand() const;
  const ::oneflow::vm::cfg::OperandProto& const_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_const_operand() const;
 // oneof field type: mut_operand
 public:
  bool has_mut_operand() const;
  const ::oneflow::vm::cfg::OperandProto& mut_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_mut_operand() const;
 // oneof field type: mut2_operand
 public:
  bool has_mut2_operand() const;
  const ::oneflow::vm::cfg::OperandProto& mut2_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_mut2_operand() const;
 // oneof field type: del_operand
 public:
  bool has_del_operand() const;
  const ::oneflow::vm::cfg::OperandProto& del_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_del_operand() const;
 // oneof field type: symbol_operand
 public:
  bool has_symbol_operand() const;
  const ::oneflow::vm::cfg::OperandProto& symbol_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_symbol_operand() const;
 // oneof field type: init_symbol_operand
 public:
  bool has_init_symbol_operand() const;
  const ::oneflow::vm::cfg::OperandProto& init_symbol_operand() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandProto> shared_const_init_symbol_operand() const;
 // oneof field type: separator
 public:
  bool has_separator() const;
  const ::oneflow::vm::cfg::OperandSeparatorProto& separator() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstOperandSeparatorProto> shared_const_separator() const;
 // oneof field type: double_operand
 public:
  bool has_double_operand() const;
  const double& double_operand() const;
  // used by pybind11 only
 // oneof field type: int64_operand
 public:
  bool has_int64_operand() const;
  const int64_t& int64_operand() const;
  // used by pybind11 only
 // oneof field type: uint64_operand
 public:
  bool has_uint64_operand() const;
  const uint64_t& uint64_operand() const;
  // used by pybind11 only
 // oneof field type: bool_operand
 public:
  bool has_bool_operand() const;
  const bool& bool_operand() const;
  // used by pybind11 only
 public:
  TypeCase type_case() const;
 protected:
  bool has_type() const;

 public:
  ::std::shared_ptr<ConstInstructionOperandProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInstructionOperandProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInstructionOperandProto& other) const;
 protected:
  const ::std::shared_ptr<_InstructionOperandProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InstructionOperandProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInstructionOperandProto
  void BuildFromProto(const PbMessage& proto_instructionoperandproto);
  
  ::std::shared_ptr<_InstructionOperandProto_> data_;
};

class InstructionOperandProto final : public ConstInstructionOperandProto {
 public:
  InstructionOperandProto(const ::std::shared_ptr<_InstructionOperandProto_>& data);
  InstructionOperandProto(const InstructionOperandProto& other);
  // enable nothrow for ::std::vector<InstructionOperandProto> resize 
  InstructionOperandProto(InstructionOperandProto&&) noexcept;
  InstructionOperandProto();
  explicit InstructionOperandProto(const ::oneflow::vm::InstructionOperandProto& proto_instructionoperandproto);

  ~InstructionOperandProto() override;

  void InitFromProto(const PbMessage& proto_instructionoperandproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InstructionOperandProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InstructionOperandProto& other) const;
  void Clear();
  void CopyFrom(const InstructionOperandProto& other);
  InstructionOperandProto& operator=(const InstructionOperandProto& other);

  void clear_const_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_const_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_const_operand();
  void clear_mut_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_mut_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_mut_operand();
  void clear_mut2_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_mut2_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_mut2_operand();
  void clear_del_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_del_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_del_operand();
  void clear_symbol_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_symbol_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_symbol_operand();
  void clear_init_symbol_operand();
  ::oneflow::vm::cfg::OperandProto* mutable_init_symbol_operand();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandProto> shared_mutable_init_symbol_operand();
  void clear_separator();
  ::oneflow::vm::cfg::OperandSeparatorProto* mutable_separator();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::OperandSeparatorProto> shared_mutable_separator();
  void clear_double_operand();
  void set_double_operand(const double& value);
  double* mutable_double_operand();
  void clear_int64_operand();
  void set_int64_operand(const int64_t& value);
  int64_t* mutable_int64_operand();
  void clear_uint64_operand();
  void set_uint64_operand(const uint64_t& value);
  uint64_t* mutable_uint64_operand();
  void clear_bool_operand();
  void set_bool_operand(const bool& value);
  bool* mutable_bool_operand();

  ::std::shared_ptr<InstructionOperandProto> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_;
class Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_;

class ConstInstructionProto : public ::oneflow::cfg::Message {
 public:

  class _InstructionProto_ {
   public:
    _InstructionProto_();
    explicit _InstructionProto_(const _InstructionProto_& other);
    explicit _InstructionProto_(_InstructionProto_&& other);
    _InstructionProto_(const ::oneflow::vm::InstructionProto& proto_instructionproto);
    ~_InstructionProto_();

    void InitFromProto(const ::oneflow::vm::InstructionProto& proto_instructionproto);

    void ToProto(::oneflow::vm::InstructionProto* proto_instructionproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InstructionProto_& other);
  
      // optional field instr_type_name
     public:
    bool has_instr_type_name() const;
    const ::std::string& instr_type_name() const;
    void clear_instr_type_name();
    void set_instr_type_name(const ::std::string& value);
    ::std::string* mutable_instr_type_name();
   protected:
    bool has_instr_type_name_ = false;
    ::std::string instr_type_name_;
      
      // optional field parallel_desc_symbol_id
     public:
    bool has_parallel_desc_symbol_id() const;
    const int64_t& parallel_desc_symbol_id() const;
    void clear_parallel_desc_symbol_id();
    void set_parallel_desc_symbol_id(const int64_t& value);
    int64_t* mutable_parallel_desc_symbol_id();
   protected:
    bool has_parallel_desc_symbol_id_ = false;
    int64_t parallel_desc_symbol_id_;
      
      // repeated field operand
   public:
    ::std::size_t operand_size() const;
    const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& operand() const;
    const ::oneflow::vm::cfg::InstructionOperandProto& operand(::std::size_t index) const;
    void clear_operand();
    _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_* mutable_operand();
    ::oneflow::vm::cfg::InstructionOperandProto* mutable_operand(::std::size_t index);
      ::oneflow::vm::cfg::InstructionOperandProto* add_operand();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> operand_;
         
   public:
    int compare(const _InstructionProto_& other);

    bool operator==(const _InstructionProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InstructionProto_& other) const;
  };

  ConstInstructionProto(const ::std::shared_ptr<_InstructionProto_>& data);
  ConstInstructionProto(const ConstInstructionProto&);
  ConstInstructionProto(ConstInstructionProto&&) noexcept;
  ConstInstructionProto();
  ConstInstructionProto(const ::oneflow::vm::InstructionProto& proto_instructionproto);
  virtual ~ConstInstructionProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_instructionproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field instr_type_name
 public:
  bool has_instr_type_name() const;
  const ::std::string& instr_type_name() const;
  // used by pybind11 only
  // required or optional field parallel_desc_symbol_id
 public:
  bool has_parallel_desc_symbol_id() const;
  const int64_t& parallel_desc_symbol_id() const;
  // used by pybind11 only
  // repeated field operand
 public:
  ::std::size_t operand_size() const;
  const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& operand() const;
  const ::oneflow::vm::cfg::InstructionOperandProto& operand(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> shared_const_operand() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionOperandProto> shared_const_operand(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstInstructionProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInstructionProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInstructionProto& other) const;
 protected:
  const ::std::shared_ptr<_InstructionProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InstructionProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInstructionProto
  void BuildFromProto(const PbMessage& proto_instructionproto);
  
  ::std::shared_ptr<_InstructionProto_> data_;
};

class InstructionProto final : public ConstInstructionProto {
 public:
  InstructionProto(const ::std::shared_ptr<_InstructionProto_>& data);
  InstructionProto(const InstructionProto& other);
  // enable nothrow for ::std::vector<InstructionProto> resize 
  InstructionProto(InstructionProto&&) noexcept;
  InstructionProto();
  explicit InstructionProto(const ::oneflow::vm::InstructionProto& proto_instructionproto);

  ~InstructionProto() override;

  void InitFromProto(const PbMessage& proto_instructionproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InstructionProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InstructionProto& other) const;
  void Clear();
  void CopyFrom(const InstructionProto& other);
  InstructionProto& operator=(const InstructionProto& other);

  // required or optional field instr_type_name
 public:
  void clear_instr_type_name();
  void set_instr_type_name(const ::std::string& value);
  ::std::string* mutable_instr_type_name();
  // required or optional field parallel_desc_symbol_id
 public:
  void clear_parallel_desc_symbol_id();
  void set_parallel_desc_symbol_id(const int64_t& value);
  int64_t* mutable_parallel_desc_symbol_id();
  // repeated field operand
 public:
  void clear_operand();
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_* mutable_operand();
  ::oneflow::vm::cfg::InstructionOperandProto* mutable_operand(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> shared_mutable_operand();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> shared_mutable_operand(::std::size_t index);
  ::oneflow::vm::cfg::InstructionOperandProto* add_operand();

  ::std::shared_ptr<InstructionProto> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_;
class Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_;

class ConstInstructionListProto : public ::oneflow::cfg::Message {
 public:

  class _InstructionListProto_ {
   public:
    _InstructionListProto_();
    explicit _InstructionListProto_(const _InstructionListProto_& other);
    explicit _InstructionListProto_(_InstructionListProto_&& other);
    _InstructionListProto_(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto);
    ~_InstructionListProto_();

    void InitFromProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto);

    void ToProto(::oneflow::vm::InstructionListProto* proto_instructionlistproto) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InstructionListProto_& other);
  
      // repeated field instruction
   public:
    ::std::size_t instruction_size() const;
    const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& instruction() const;
    const ::oneflow::vm::cfg::InstructionProto& instruction(::std::size_t index) const;
    void clear_instruction();
    _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_* mutable_instruction();
    ::oneflow::vm::cfg::InstructionProto* mutable_instruction(::std::size_t index);
      ::oneflow::vm::cfg::InstructionProto* add_instruction();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> instruction_;
         
   public:
    int compare(const _InstructionListProto_& other);

    bool operator==(const _InstructionListProto_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InstructionListProto_& other) const;
  };

  ConstInstructionListProto(const ::std::shared_ptr<_InstructionListProto_>& data);
  ConstInstructionListProto(const ConstInstructionListProto&);
  ConstInstructionListProto(ConstInstructionListProto&&) noexcept;
  ConstInstructionListProto();
  ConstInstructionListProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto);
  virtual ~ConstInstructionListProto() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_instructionlistproto) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field instruction
 public:
  ::std::size_t instruction_size() const;
  const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& instruction() const;
  const ::oneflow::vm::cfg::InstructionProto& instruction(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> shared_const_instruction() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionProto> shared_const_instruction(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstInstructionListProto> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInstructionListProto& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInstructionListProto& other) const;
 protected:
  const ::std::shared_ptr<_InstructionListProto_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InstructionListProto_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInstructionListProto
  void BuildFromProto(const PbMessage& proto_instructionlistproto);
  
  ::std::shared_ptr<_InstructionListProto_> data_;
};

class InstructionListProto final : public ConstInstructionListProto {
 public:
  InstructionListProto(const ::std::shared_ptr<_InstructionListProto_>& data);
  InstructionListProto(const InstructionListProto& other);
  // enable nothrow for ::std::vector<InstructionListProto> resize 
  InstructionListProto(InstructionListProto&&) noexcept;
  InstructionListProto();
  explicit InstructionListProto(const ::oneflow::vm::InstructionListProto& proto_instructionlistproto);

  ~InstructionListProto() override;

  void InitFromProto(const PbMessage& proto_instructionlistproto) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InstructionListProto& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InstructionListProto& other) const;
  void Clear();
  void CopyFrom(const InstructionListProto& other);
  InstructionListProto& operator=(const InstructionListProto& other);

  // repeated field instruction
 public:
  void clear_instruction();
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_* mutable_instruction();
  ::oneflow::vm::cfg::InstructionProto* mutable_instruction(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> shared_mutable_instruction();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> shared_mutable_instruction(::std::size_t index);
  ::oneflow::vm::cfg::InstructionProto* add_instruction();

  ::std::shared_ptr<InstructionListProto> __SharedMutable__();
};






















// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionOperandProto> {
 public:
  Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionOperandProto>>& data);
  Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_();
  ~Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionOperandProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_ final : public Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_ {
 public:
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionOperandProto>>& data);
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_();
  ~_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionOperandProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionOperandProto> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::InstructionProto> {
 public:
  Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionProto>>& data);
  Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_();
  ~Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_ final : public Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_ {
 public:
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::InstructionProto>>& data);
  _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_();
  ~_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H__RepeatedField_InstructionProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionProto> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow
} // namespace vm

namespace std {



template<>
struct hash<::oneflow::vm::cfg::ConstCurrentGlobalDeviceIdProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstCurrentGlobalDeviceIdProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::CurrentGlobalDeviceIdProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::CurrentGlobalDeviceIdProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstSoleMirroredObjectProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstSoleMirroredObjectProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::SoleMirroredObjectProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::SoleMirroredObjectProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstAllMirroredObjectProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstAllMirroredObjectProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::AllMirroredObjectProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::AllMirroredObjectProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstOperandProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstOperandProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::OperandProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::OperandProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstOperandSeparatorProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstOperandSeparatorProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::OperandSeparatorProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::OperandSeparatorProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstInstructionOperandProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstInstructionOperandProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::InstructionOperandProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::InstructionOperandProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstInstructionProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstInstructionProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::InstructionProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::InstructionProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstInstructionListProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstInstructionListProto& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::InstructionListProto> {
  std::size_t operator()(const ::oneflow::vm::cfg::InstructionListProto& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_VM_INSTRUCTION_CFG_H_