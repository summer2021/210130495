/*
Copyright 2020 The OneFlow Authors. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Generated from oneflow/core/functional/functional_api.yaml. DO NOT EDIT!

#include "oneflow/core/functional/functional_api.yaml.h"
#include "oneflow/core/functional/function_library.h"

namespace oneflow {
namespace one {
namespace functional {

Maybe<one::Tensor> AddN(const TensorTuple& inputs, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AddN"));
  return op->call<Maybe<one::Tensor>, const TensorTuple&, bool>(inputs, inplace);
}

Maybe<one::Tensor> Add(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Add"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, bool>(x, y, inplace);
}

Maybe<one::Tensor> ScalarAdd(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarAdd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&, bool>(x, alpha, inplace);
}

Maybe<one::Tensor> ScalarAddByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarAddByTensor"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, bool>(x, scalar, inplace);
}

Maybe<one::Tensor> BroadcastAdd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastAdd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> ScalarSubByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarSubByTensor"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, scalar);
}

Maybe<one::Tensor> BroadcastSub(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastSub"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> Multiply(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Multiply"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> ScalarMul(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarMul"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&>(x, alpha);
}

Maybe<one::Tensor> ScalarMulByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarMulByTensor"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, scalar);
}

Maybe<one::Tensor> BroadcastMul(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastMul"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> ScalarDivByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarDivByTensor"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, scalar);
}

Maybe<one::Tensor> BroadcastDiv(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastDiv"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastDivGrad(const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& z, const std::shared_ptr<one::Tensor>& dz) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastDivGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(y, z, dz);
}

Maybe<one::Tensor> BroadcastEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastEqual"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastNotEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastNotEqual"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastGreater(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastGreater"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastGreaterEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastGreaterEqual"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastLogicalAnd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastLogicalAnd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastLogicalOr(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastLogicalOr"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastLess(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastLess"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> BroadcastLessEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastLessEqual"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> Pow(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Pow"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> ScalarPow(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScalarPow"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&>(x, alpha);
}

Maybe<one::Tensor> ReduceSum(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& axis, bool keepdims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReduceSum"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&, bool>(x, axis, keepdims);
}

Maybe<one::Tensor> Roll(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& shifts, const std::vector<int32_t>& dims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Roll"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&>(x, shifts, dims);
}

Maybe<one::Tensor> ReduceMean(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& axis, bool keepdims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReduceMean"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&, bool>(x, axis, keepdims);
}

Maybe<one::Tensor> Transpose(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& perm) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Transpose"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(x, perm);
}

Maybe<one::Tensor> Reciprocal(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Reciprocal"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> ReciprocalNoNan(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReciprocalNoNan"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> ImageFlip(const std::shared_ptr<one::Tensor>& x, int32_t flip_code) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ImageFlip"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int32_t>(x, flip_code);
}

Maybe<one::Tensor> Sin(const std::shared_ptr<one::Tensor>& x, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Sin"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, bool>(x, inplace);
}

Maybe<one::Tensor> Cos(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Cos"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Cosh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Cosh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> BroadcastFMod(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastFMod"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> Log(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Log"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Sqrt(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Sqrt"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Rsqrt(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Rsqrt"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Square(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Square"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Relu(const std::shared_ptr<one::Tensor>& x, bool inplace) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Relu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, bool>(x, inplace);
}

Maybe<one::Tensor> ReluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dy, y);
}

Maybe<one::Tensor> HardTanh(const std::shared_ptr<one::Tensor>& x, double min_val, double max_val) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardTanh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, double, double>(x, min_val, max_val);
}

Maybe<one::Tensor> HardTanhGrad(const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, double min_val, double max_val) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardTanhGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, double, double>(y, dy, min_val, max_val);
}

Maybe<one::Tensor> Tan(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Tan"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Tanh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Tanh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Elu(const std::shared_ptr<one::Tensor>& x, double alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Elu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, double>(x, alpha);
}

Maybe<one::Tensor> EluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, double alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("EluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, double>(x, dy, alpha);
}

Maybe<one::Tensor> Gelu(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Gelu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> GeluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("GeluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dy, x);
}

Maybe<one::Tensor> Sigmoid(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Sigmoid"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> HardSigmoid(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardSigmoid"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> HardSigmoidGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardSigmoidGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dy, x);
}

Maybe<one::Tensor> Softmax(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Softmax"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> HardSwish(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardSwish"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> HardSwishGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("HardSwishGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dy, x);
}

Maybe<one::Tensor> LeakyRelu(const std::shared_ptr<one::Tensor>& x, float alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LeakyRelu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float>(x, alpha);
}

Maybe<one::Tensor> LeakyReluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, float alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LeakyReluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float>(x, dy, alpha);
}

Maybe<one::Tensor> Normalization(const std::shared_ptr<one::Tensor>& x, const Optional<one::Tensor>& moving_mean, const Optional<one::Tensor>& moving_variance, const std::shared_ptr<one::Tensor>& gamma, const std::shared_ptr<one::Tensor>& beta, int32_t axis, float epsilon, float momentum, bool is_training) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Normalization"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Optional<one::Tensor>&, const Optional<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t, float, float, bool>(x, moving_mean, moving_variance, gamma, beta, axis, epsilon, momentum, is_training);
}

Maybe<one::TensorTuple> NormalizationGrad(const std::shared_ptr<one::Tensor>& grad, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& mean, const std::shared_ptr<one::Tensor>& inv_variance, const std::shared_ptr<one::Tensor>& gamma, float epsilon, int32_t axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("NormalizationGrad"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, int32_t>(grad, x, mean, inv_variance, gamma, epsilon, axis);
}

Maybe<one::Tensor> Range(int64_t start, int64_t limit, int64_t delta, const DataType& dtype) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Range"));
  return op->call<Maybe<one::Tensor>, int64_t, int64_t, int64_t, const DataType&>(start, limit, delta, dtype);
}

Maybe<one::Tensor> Flatten(const std::shared_ptr<one::Tensor>& x, int32_t start_dim, int32_t end_dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Flatten"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int32_t, int32_t>(x, start_dim, end_dim);
}

Maybe<one::Tensor> ArgMax(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ArgMax"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::TensorTuple> ArgWhere(const std::shared_ptr<one::Tensor>& x, const DataType& dtype) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ArgWhere"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const DataType&>(x, dtype);
}

Maybe<one::Tensor> BroadcastLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& like, const std::vector<int32_t>& broadcast_axes) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BroadcastLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(x, like, broadcast_axes);
}

Maybe<one::Tensor> Cast(const std::shared_ptr<one::Tensor>& x, const DataType& dtype) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Cast"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const DataType&>(x, dtype);
}

Maybe<one::Tensor> Constant(const Shape& shape, const Scalar& value, const DataType& dtype, const Optional<Symbol<Device>>& device) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Constant"));
  return op->call<Maybe<one::Tensor>, const Shape&, const Scalar&, const DataType&, const Optional<Symbol<Device>>&>(shape, value, dtype, device);
}

Maybe<one::Tensor> ConsistentConstant(const Shape& shape, const Scalar& value, const DataType& dtype, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConsistentConstant"));
  return op->call<Maybe<one::Tensor>, const Shape&, const Scalar&, const DataType&, const Symbol<ParallelDesc>&, const std::vector<Symbol<cfg::SbpParallel>>&>(shape, value, dtype, placement, sbp_tuple);
}

Maybe<one::Tensor> Empty(const Shape& shape, const DataType& dtype, const Optional<Symbol<Device>>& device) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Empty"));
  return op->call<Maybe<one::Tensor>, const Shape&, const DataType&, const Optional<Symbol<Device>>&>(shape, dtype, device);
}

Maybe<one::Tensor> ConsistentEmpty(const Shape& shape, const DataType& dtype, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConsistentEmpty"));
  return op->call<Maybe<one::Tensor>, const Shape&, const DataType&, const Symbol<ParallelDesc>&, const std::vector<Symbol<cfg::SbpParallel>>&>(shape, dtype, placement, sbp_tuple);
}

Maybe<one::Tensor> ZerosLike(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ZerosLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> OnesLike(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("OnesLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Bernoulli(const std::shared_ptr<one::Tensor>& x, const DataType& dtype, const Optional<one::Generator>& generator) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Bernoulli"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const DataType&, const Optional<one::Generator>&>(x, dtype, generator);
}

Maybe<one::Tensor> Concat(const TensorTuple& inputs, int64_t axis, int64_t max_dim_size) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Concat"));
  return op->call<Maybe<one::Tensor>, const TensorTuple&, int64_t, int64_t>(inputs, axis, max_dim_size);
}

Maybe<one::Tensor> BiasAdd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& bias, int32_t axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BiasAdd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t>(x, bias, axis);
}

Maybe<one::Tensor> Conv1d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Conv1d"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Optional<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, int32_t>(x, weight, bias, stride, padding, dilation, groups);
}

Maybe<one::Tensor> Conv2d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Conv2d"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Optional<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, int32_t>(x, weight, bias, stride, padding, dilation, groups);
}

Maybe<one::Tensor> FakeQuantization(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& scale, const std::shared_ptr<one::Tensor>& zero_point, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("FakeQuantization"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t, const std::string&>(in, scale, zero_point, quantization_formula, quantization_bit, quantization_scheme);
}

Maybe<one::Tensor> Quantization(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& scale, const std::shared_ptr<one::Tensor>& zero_point, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Quantization"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t, const std::string&>(in, scale, zero_point, quantization_formula, quantization_bit, quantization_scheme);
}

Maybe<one::TensorTuple> MinMaxObserver(const std::shared_ptr<one::Tensor>& in, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme, bool per_layer_quantization) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("MinMaxObserver"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t, const std::string&, bool>(in, quantization_formula, quantization_bit, quantization_scheme, per_layer_quantization);
}

Maybe<one::TensorTuple> MovingAverageMinMaxObserver(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& current_train_step, const std::shared_ptr<one::Tensor>& moving_max, const std::shared_ptr<one::Tensor>& moving_min, bool training, const std::string& quantization_formula, int64_t stop_update_after_iters, int32_t quantization_bit, const std::string& quantization_scheme, float momentum) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("MovingAverageMinMaxObserver"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, bool, const std::string&, int64_t, int32_t, const std::string&, float>(in, current_train_step, moving_max, moving_min, training, quantization_formula, stop_update_after_iters, quantization_bit, quantization_scheme, momentum);
}

Maybe<one::Tensor> Conv3d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Conv3d"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Optional<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, int32_t>(x, weight, bias, stride, padding, dilation, groups);
}

Maybe<one::Tensor> ConvDataGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& weight, const std::shared_ptr<one::Tensor>& x, int32_t num_spatial_dims, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& strides, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& dilation_rate, int32_t groups, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConvDataGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, int32_t, const std::string&>(dy, weight, x, num_spatial_dims, kernel_size, strides, padding_before, dilation_rate, groups, data_format);
}

Maybe<one::Tensor> ConvFilterGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, int32_t num_spatial_dims, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& strides, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& dilation_rate, int32_t groups, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConvFilterGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, int32_t, const std::string&>(dy, x, num_spatial_dims, kernel_size, strides, padding_before, dilation_rate, groups, data_format);
}

Maybe<one::Tensor> ConvBiasGrad(const std::shared_ptr<one::Tensor>& dy, int32_t num_spatial_dims, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConvBiasGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int32_t, const std::string&>(dy, num_spatial_dims, data_format);
}

Maybe<one::Tensor> Expand(const std::shared_ptr<one::Tensor>& x, const Shape& shape) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Expand"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Shape&>(x, shape);
}

Maybe<one::Tensor> ExpandDims(const std::shared_ptr<one::Tensor>& x, int32_t axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ExpandDims"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int32_t>(x, axis);
}

Maybe<one::Tensor> Exp(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Exp"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Gather(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& indices, int64_t axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Gather"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int64_t>(x, indices, axis);
}

Maybe<one::Tensor> DimGather(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& indices, int32_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DimGather"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t>(x, indices, dim);
}

Maybe<one::Tensor> GatherNd(const std::shared_ptr<one::Tensor>& params, const std::shared_ptr<one::Tensor>& indices) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("GatherNd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(params, indices);
}

Maybe<one::Tensor> ScatterNd(const std::shared_ptr<one::Tensor>& indices, const std::shared_ptr<one::Tensor>& updates, const Shape& shape) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScatterNd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Shape&>(indices, updates, shape);
}

Maybe<one::Tensor> ScatterNdLike(const std::shared_ptr<one::Tensor>& like, const std::shared_ptr<one::Tensor>& updates, const std::shared_ptr<one::Tensor>& indices) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ScatterNdLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(like, updates, indices);
}

Maybe<one::Tensor> MatMul(const std::shared_ptr<one::Tensor>& a, const std::shared_ptr<one::Tensor>& b, bool transpose_a, bool transpose_b, double alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("MatMul"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, bool, bool, double>(a, b, transpose_a, transpose_b, alpha);
}

Maybe<one::Tensor> BatchMatMul(const std::shared_ptr<one::Tensor>& a, const std::shared_ptr<one::Tensor>& b, bool transpose_a, bool transpose_b, double alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("BatchMatMul"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, bool, bool, double>(a, b, transpose_a, transpose_b, alpha);
}

Maybe<one::Tensor> SparseSoftmaxCrossEntropy(const std::shared_ptr<one::Tensor>& logits, const std::shared_ptr<one::Tensor>& label, int64_t depth) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SparseSoftmaxCrossEntropy"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int64_t>(logits, label, depth);
}

Maybe<one::Tensor> SmoothL1Loss(const std::shared_ptr<one::Tensor>& logits, const std::shared_ptr<one::Tensor>& label, float beta) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SmoothL1Loss"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float>(logits, label, beta);
}

Maybe<one::Tensor> SmoothL1LossGrad(const std::shared_ptr<one::Tensor>& loss_grad, const std::shared_ptr<one::Tensor>& prediction, const std::shared_ptr<one::Tensor>& label, float beta) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SmoothL1LossGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float>(loss_grad, prediction, label, beta);
}

Maybe<one::Tensor> Where(const std::shared_ptr<one::Tensor>& condition, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Where"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(condition, x, y);
}

Maybe<one::Tensor> Negative(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Negative"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> LayerNormAffine(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& gamma, const std::shared_ptr<one::Tensor>& beta, int64_t begin_norm_axis, int64_t begin_params_axis, double epsilon) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LayerNormAffine"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int64_t, int64_t, double>(x, gamma, beta, begin_norm_axis, begin_params_axis, epsilon);
}

Maybe<one::Tensor> LayerNorm(const std::shared_ptr<one::Tensor>& x, int64_t begin_norm_axis, int64_t begin_params_axis, double epsilon) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LayerNorm"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int64_t, int64_t, double>(x, begin_norm_axis, begin_params_axis, epsilon);
}

Maybe<one::Tensor> AvgPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::string& data_format, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AvgPool2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::string&, bool>(x, kernel_size, stride, padding, padding_before, padding_after, data_format, ceil_mode);
}

Maybe<one::Tensor> MaxPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::string& data_format, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("MaxPool2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::string&, bool>(x, kernel_size, stride, padding, padding_before, padding_after, data_format, ceil_mode);
}

Maybe<one::Tensor> AdaptiveAvgPool1D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AdaptiveAvgPool1D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&>(x, output_size);
}

Maybe<one::Tensor> AdaptiveAvgPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AdaptiveAvgPool2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&>(x, output_size);
}

Maybe<one::Tensor> AdaptiveAvgPool3D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AdaptiveAvgPool3D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&>(x, output_size);
}

Maybe<one::Tensor> AdaptivePoolNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AdaptivePoolNdGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t>(x, dy, mode, ndims);
}

Maybe<one::Tensor> PoolNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims, const std::string& data_format, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::vector<int32_t>& pool_size, const std::vector<int32_t>& strides, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("PoolNdGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t, const std::string&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool>(x, y, dy, mode, ndims, data_format, padding, padding_before, padding_after, pool_size, strides, ceil_mode);
}

Maybe<one::TensorTuple> Maxpool1D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Maxpool1D"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool>(x, data_format, padding, kernel_size, stride, dilation, return_indices, ceil_mode);
}

Maybe<one::TensorTuple> Maxpool2D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Maxpool2D"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool>(x, data_format, padding, kernel_size, stride, dilation, return_indices, ceil_mode);
}

Maybe<one::TensorTuple> Maxpool3D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Maxpool3D"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool>(x, data_format, padding, kernel_size, stride, dilation, return_indices, ceil_mode);
}

Maybe<one::Tensor> PoolingNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& indice, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("PoolingNdGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::string&, int32_t, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool>(x, y, indice, dy, mode, ndims, data_format, padding, kernel_size, stride, dilation, return_indices, ceil_mode);
}

Maybe<one::Tensor> PRelu(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("PRelu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, alpha);
}

Maybe<one::TensorTuple> PReluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& alpha) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("PReluGrad"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dy, x, alpha);
}

Maybe<one::Tensor> Reshape(const std::shared_ptr<one::Tensor>& x, const Shape& shape) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Reshape"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Shape&>(x, shape);
}

Maybe<one::Tensor> Slice(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Slice"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::vector<int64_t>&, const std::vector<int64_t>&>(x, start, stop, step);
}

Maybe<one::Tensor> SliceGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& like, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SliceGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::vector<int64_t>&, const std::vector<int64_t>&>(dy, like, start, stop, step);
}

Maybe<one::Tensor> SliceUpdate(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& update, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SliceUpdate"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::vector<int64_t>&, const std::vector<int64_t>&>(x, update, start, stop, step);
}

Maybe<one::Tensor> LogicalSlice(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LogicalSlice"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::vector<int64_t>&, const std::vector<int64_t>&>(x, start, stop, step);
}

Maybe<void> LogicalSliceAssign(const std::shared_ptr<one::Tensor>& ref, const std::shared_ptr<one::Tensor>& value, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LogicalSliceAssign"));
  return op->call<Maybe<void>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::vector<int64_t>&, const std::vector<int64_t>&>(ref, value, start, stop, step);
}

Maybe<one::Tensor> Squeeze(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Squeeze"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(x, dim);
}

Maybe<one::Tensor> Copy(const std::shared_ptr<one::Tensor>& x, const std::string& device_type, int64_t device_id) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Copy"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::string&, int64_t>(x, device_type, device_id);
}

Maybe<one::Tensor> Flip(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& dims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Flip"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(x, dims);
}

Maybe<one::Tensor> FlipGrad(const std::shared_ptr<one::Tensor>& dy, const std::vector<int32_t>& dims) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("FlipGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(dy, dims);
}

Maybe<one::Tensor> Upsample(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& interpolation, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Upsample"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, bool, const std::string&, const std::string&>(x, height_scale, width_scale, align_corners, interpolation, data_format);
}

Maybe<one::Tensor> UpsampleLinear1D(const std::shared_ptr<one::Tensor>& x, float scale_factor, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleLinear1D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, bool, const std::string&>(x, scale_factor, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleLinear1DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float scale_factor, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleLinear1DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, bool, const std::string&>(dy, x, scale_factor, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleNearest1D(const std::shared_ptr<one::Tensor>& x, float scale_factor, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest1D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, const std::string&>(x, scale_factor, data_format);
}

Maybe<one::Tensor> UpsampleNearest1DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float scale_factor, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest1DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, const std::string&>(dy, x, scale_factor, data_format);
}

Maybe<one::Tensor> UpsampleNearest2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, const std::string&>(x, height_scale, width_scale, data_format);
}

Maybe<one::Tensor> UpsampleNearest2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest2DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, float, const std::string&>(dy, x, height_scale, width_scale, data_format);
}

Maybe<one::Tensor> UpsampleBilinear2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleBilinear2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, bool, const std::string&>(x, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleBilinear2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleBilinear2DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, float, bool, const std::string&>(dy, x, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleBicubic2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleBicubic2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, bool, const std::string&>(x, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleBicubic2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleBicubic2DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, float, bool, const std::string&>(dy, x, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleNearest3D(const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest3D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, float, const std::string&>(x, depth_scale, height_scale, width_scale, data_format);
}

Maybe<one::Tensor> UpsampleNearest3DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleNearest3DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, float, float, const std::string&>(dy, x, depth_scale, height_scale, width_scale, data_format);
}

Maybe<one::Tensor> UpsampleTrilinear3D(const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleTrilinear3D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, float, float, bool, const std::string&>(x, depth_scale, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> UpsampleTrilinear3DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, bool align_corners, const std::string& data_format) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UpsampleTrilinear3DGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, float, float, bool, const std::string&>(dy, x, depth_scale, height_scale, width_scale, align_corners, data_format);
}

Maybe<one::Tensor> Abs(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Abs"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Acos(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Acos"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Acosh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Acosh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Asin(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Asin"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Asinh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Asinh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Atan(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Atan"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Atanh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Atanh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Ceil(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Ceil"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Erf(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Erf"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Erfc(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Erfc"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Expm1(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Expm1"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Floor(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Floor"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Lgamma(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Lgamma"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Log1p(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Log1p"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> LogSigmoid(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("LogSigmoid"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Rint(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Rint"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Round(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Round"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Sign(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Sign"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Sinh(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Sinh"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> Softplus(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Softplus"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> UnsortedSegmentSumLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& segment_ids, const std::shared_ptr<one::Tensor>& like, int64_t axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("UnsortedSegmentSumLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int64_t>(x, segment_ids, like, axis);
}

Maybe<one::Tensor> Triu(const std::shared_ptr<one::Tensor>& x, int64_t diagonal) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Triu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int64_t>(x, diagonal);
}

Maybe<one::Tensor> ClipByScalar(const std::shared_ptr<one::Tensor>& x, const Scalar& min, const Scalar& max) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalar"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&, const Scalar&>(x, min, max);
}

Maybe<one::Tensor> ClipByScalarGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& min, const Scalar& max) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalarGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Scalar&, const Scalar&>(dy, x, min, max);
}

Maybe<one::Tensor> ClipByScalarMin(const std::shared_ptr<one::Tensor>& x, const Scalar& min) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalarMin"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&>(x, min);
}

Maybe<one::Tensor> ClipByScalarMinGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& min) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalarMinGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Scalar&>(dy, x, min);
}

Maybe<one::Tensor> ClipByScalarMax(const std::shared_ptr<one::Tensor>& x, const Scalar& max) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalarMax"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Scalar&>(x, max);
}

Maybe<one::Tensor> ClipByScalarMaxGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& max) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ClipByScalarMaxGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const Scalar&>(dy, x, max);
}

Maybe<one::Tensor> Dropout(const std::shared_ptr<one::Tensor>& x, float p, const Optional<one::Generator>& generator) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Dropout"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, float, const Optional<one::Generator>&>(x, p, generator);
}

Maybe<one::Tensor> Pad(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& pad, const std::string& mode, const Scalar& value) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Pad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::string&, const Scalar&>(x, pad, mode, value);
}

Maybe<one::Tensor> PadGrad(const std::shared_ptr<one::Tensor>& dy, const std::vector<int64_t>& pad, const std::string& mode, const Scalar& value) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("PadGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::vector<int64_t>&, const std::string&, const Scalar&>(dy, pad, mode, value);
}

Maybe<one::Tensor> Silu(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Silu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> SiluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SiluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, dy);
}

Maybe<one::Tensor> Mish(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Mish"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> MishGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("MishGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, dy);
}

Maybe<one::Tensor> Selu(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Selu"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> SeluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SeluGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, dy);
}

Maybe<one::Tensor> SoftSign(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SoftSign"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> SoftSignGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SoftSignGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, dy);
}

Maybe<one::Tensor> Diag(const std::shared_ptr<one::Tensor>& x, int32_t diagonal) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Diag"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, int32_t>(x, diagonal);
}

Maybe<one::Tensor> DiagGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& in, int32_t diagonal) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DiagGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t>(dy, in, diagonal);
}

Maybe<one::Tensor> TensorGetItem(const std::shared_ptr<one::Tensor>& x, const TensorIndex& index) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("TensorGetItem"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const TensorIndex&>(x, index);
}

Maybe<one::Tensor> DimScatter(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, const std::shared_ptr<one::Tensor>& src, int32_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DimScatter"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t>(input, index, src, dim);
}

Maybe<one::Tensor> DimScatterAdd(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, const std::shared_ptr<one::Tensor>& src, int32_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DimScatterAdd"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t>(input, index, src, dim);
}

Maybe<one::Tensor> DimScatterUpdateScalar(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, float src, int32_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DimScatterUpdateScalar"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, int32_t>(input, index, src, dim);
}

Maybe<one::Tensor> DimScatterAddScalar(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, float src, int32_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("DimScatterAddScalar"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, float, int32_t>(input, index, src, dim);
}

Maybe<void> TensorSetItem(const std::shared_ptr<one::Tensor>& x, const TensorIndex& index, const std::shared_ptr<one::Tensor>& value) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("TensorSetItem"));
  return op->call<Maybe<void>, const std::shared_ptr<one::Tensor>&, const TensorIndex&, const std::shared_ptr<one::Tensor>&>(x, index, value);
}

Maybe<one::Tensor> Avgpool1D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Avgpool1D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool, int64_t>(x, data_format, padding, kernel_size, stride, ceil_mode, count_include_pad, divisor_override);
}

Maybe<one::Tensor> Avgpool2D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Avgpool2D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool, int64_t>(x, data_format, padding, kernel_size, stride, ceil_mode, count_include_pad, divisor_override);
}

Maybe<one::Tensor> Avgpool3D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Avgpool3D"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool, int64_t>(x, data_format, padding, kernel_size, stride, ceil_mode, count_include_pad, divisor_override);
}

Maybe<one::Tensor> AvgPoolingNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, int32_t ndims, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AvgPoolingNdGrad"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, int32_t, const std::string&, const std::vector<int32_t>&, const std::vector<int32_t>&, const std::vector<int32_t>&, bool, bool, int64_t>(x, y, dy, ndims, data_format, padding, kernel_size, stride, ceil_mode, count_include_pad, divisor_override);
}

Maybe<one::Tensor> Minimum(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Minimum"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::Tensor> Maximum(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Maximum"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, y);
}

Maybe<one::TensorTuple> ElementwiseMinGrad(const std::shared_ptr<one::Tensor>& dz, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ElementwiseMinGrad"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dz, x, y);
}

Maybe<one::TensorTuple> ElementwiseMaxGrad(const std::shared_ptr<one::Tensor>& dz, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ElementwiseMaxGrad"));
  return op->call<Maybe<one::TensorTuple>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(dz, x, y);
}

Maybe<one::Tensor> Stack(const TensorTuple& inputs, int64_t dim) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Stack"));
  return op->call<Maybe<one::Tensor>, const TensorTuple&, int64_t>(inputs, dim);
}

Maybe<one::Tensor> ToConsistent(const std::shared_ptr<one::Tensor>& x, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp, const Optional<Shape>& shape) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ToConsistent"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const Symbol<ParallelDesc>&, const std::vector<Symbol<cfg::SbpParallel>>&, const Optional<Shape>&>(x, placement, sbp, shape);
}

Maybe<one::Tensor> ConsistentToLocal(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConsistentToLocal"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> AllReduce(const std::shared_ptr<one::Tensor>& x) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("AllReduce"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(x);
}

Maybe<one::Tensor> SelectFirst(const TensorTuple& inputs) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("SelectFirst"));
  return op->call<Maybe<one::Tensor>, const TensorTuple&>(inputs);
}

Maybe<one::Tensor> CastLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& like) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("CastLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(x, like);
}

Maybe<one::Tensor> Identity(const std::shared_ptr<one::Tensor>& in) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("Identity"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&>(in);
}

Maybe<one::Tensor> ReshapeLike(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& like) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReshapeLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&>(in, like);
}

Maybe<one::Tensor> ReduceSumLike(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& like, const std::vector<int32_t>& axis) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ReduceSumLike"));
  return op->call<Maybe<one::Tensor>, const std::shared_ptr<one::Tensor>&, const std::shared_ptr<one::Tensor>&, const std::vector<int32_t>&>(in, like, axis);
}

Maybe<one::Tensor> RandN(const Shape& shape, const Optional<DataType>& dtype, const Optional<Symbol<Device>>& device, const Optional<one::Generator>& generator) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("RandN"));
  return op->call<Maybe<one::Tensor>, const Shape&, const Optional<DataType>&, const Optional<Symbol<Device>>&, const Optional<one::Generator>&>(shape, dtype, device, generator);
}

Maybe<one::Tensor> ConsistentRandN(const Shape& shape, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple, const Optional<DataType>& dtype, const Optional<one::Generator>& generator) {
  static thread_local const auto& op = CHECK_JUST(FunctionLibrary::Global()->find("ConsistentRandN"));
  return op->call<Maybe<one::Tensor>, const Shape&, const Symbol<ParallelDesc>&, const std::vector<Symbol<cfg::SbpParallel>>&, const Optional<DataType>&, const Optional<one::Generator>&>(shape, placement, sbp_tuple, dtype, generator);
}

}  // namespace functional
}  // namespace one
}  // namespace oneflow
