/*
Copyright 2020 The OneFlow Authors. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Generated from oneflow/core/functional/functional_api.yaml. DO NOT EDIT!

#ifndef ONEFLOW_CORE_FUNCTIONAL_GENERATED_FUNCTIONAL_API_H_
#define ONEFLOW_CORE_FUNCTIONAL_GENERATED_FUNCTIONAL_API_H_

#include "oneflow/core/common/optional.h"
#include "oneflow/core/framework/tensor.h"
#include "oneflow/core/framework/tensor_tuple.h"
#include "oneflow/core/framework/random_generator.h"
#include "oneflow/core/functional/scalar.h"
#include "oneflow/core/functional/tensor_index.h"

namespace oneflow {
namespace one {
namespace functional {

Maybe<one::Tensor> AddN(const TensorTuple& inputs, bool inplace);

Maybe<one::Tensor> Add(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, bool inplace);

Maybe<one::Tensor> ScalarAdd(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha, bool inplace);

Maybe<one::Tensor> ScalarAddByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar, bool inplace);

Maybe<one::Tensor> BroadcastAdd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> ScalarSubByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar);

Maybe<one::Tensor> BroadcastSub(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Multiply(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> ScalarMul(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha);

Maybe<one::Tensor> ScalarMulByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar);

Maybe<one::Tensor> BroadcastMul(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> ScalarDivByTensor(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& scalar);

Maybe<one::Tensor> BroadcastDiv(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastDivGrad(const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& z, const std::shared_ptr<one::Tensor>& dz);

Maybe<one::Tensor> BroadcastEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastNotEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastGreater(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastGreaterEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastLogicalAnd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastLogicalOr(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastLess(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> BroadcastLessEqual(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Pow(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> ScalarPow(const std::shared_ptr<one::Tensor>& x, const Scalar& alpha);

Maybe<one::Tensor> ReduceSum(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& axis, bool keepdims);

Maybe<one::Tensor> Roll(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& shifts, const std::vector<int32_t>& dims);

Maybe<one::Tensor> ReduceMean(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& axis, bool keepdims);

Maybe<one::Tensor> Transpose(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& perm);

Maybe<one::Tensor> Reciprocal(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> ReciprocalNoNan(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> ImageFlip(const std::shared_ptr<one::Tensor>& x, int32_t flip_code);

Maybe<one::Tensor> Sin(const std::shared_ptr<one::Tensor>& x, bool inplace);

Maybe<one::Tensor> Cos(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Cosh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> BroadcastFMod(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Log(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Sqrt(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Rsqrt(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Square(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Relu(const std::shared_ptr<one::Tensor>& x, bool inplace);

Maybe<one::Tensor> ReluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> HardTanh(const std::shared_ptr<one::Tensor>& x, double min_val, double max_val);

Maybe<one::Tensor> HardTanhGrad(const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, double min_val, double max_val);

Maybe<one::Tensor> Tan(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Tanh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Elu(const std::shared_ptr<one::Tensor>& x, double alpha);

Maybe<one::Tensor> EluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, double alpha);

Maybe<one::Tensor> Gelu(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> GeluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Sigmoid(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> HardSigmoid(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> HardSigmoidGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Softmax(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> HardSwish(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> HardSwishGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> LeakyRelu(const std::shared_ptr<one::Tensor>& x, float alpha);

Maybe<one::Tensor> LeakyReluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, float alpha);

Maybe<one::Tensor> Normalization(const std::shared_ptr<one::Tensor>& x, const Optional<one::Tensor>& moving_mean, const Optional<one::Tensor>& moving_variance, const std::shared_ptr<one::Tensor>& gamma, const std::shared_ptr<one::Tensor>& beta, int32_t axis, float epsilon, float momentum, bool is_training);

Maybe<one::TensorTuple> NormalizationGrad(const std::shared_ptr<one::Tensor>& grad, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& mean, const std::shared_ptr<one::Tensor>& inv_variance, const std::shared_ptr<one::Tensor>& gamma, float epsilon, int32_t axis);

Maybe<one::Tensor> Range(int64_t start, int64_t limit, int64_t delta, const DataType& dtype);

Maybe<one::Tensor> Flatten(const std::shared_ptr<one::Tensor>& x, int32_t start_dim, int32_t end_dim);

Maybe<one::Tensor> ArgMax(const std::shared_ptr<one::Tensor>& x);

Maybe<one::TensorTuple> ArgWhere(const std::shared_ptr<one::Tensor>& x, const DataType& dtype);

Maybe<one::Tensor> BroadcastLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& like, const std::vector<int32_t>& broadcast_axes);

Maybe<one::Tensor> Cast(const std::shared_ptr<one::Tensor>& x, const DataType& dtype);

Maybe<one::Tensor> Constant(const Shape& shape, const Scalar& value, const DataType& dtype, const Optional<Symbol<Device>>& device);

Maybe<one::Tensor> ConsistentConstant(const Shape& shape, const Scalar& value, const DataType& dtype, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple);

Maybe<one::Tensor> Empty(const Shape& shape, const DataType& dtype, const Optional<Symbol<Device>>& device);

Maybe<one::Tensor> ConsistentEmpty(const Shape& shape, const DataType& dtype, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple);

Maybe<one::Tensor> ZerosLike(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> OnesLike(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Bernoulli(const std::shared_ptr<one::Tensor>& x, const DataType& dtype, const Optional<one::Generator>& generator);

Maybe<one::Tensor> Concat(const TensorTuple& inputs, int64_t axis, int64_t max_dim_size);

Maybe<one::Tensor> BiasAdd(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& bias, int32_t axis);

Maybe<one::Tensor> Conv1d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups);

Maybe<one::Tensor> Conv2d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups);

Maybe<one::Tensor> FakeQuantization(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& scale, const std::shared_ptr<one::Tensor>& zero_point, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme);

Maybe<one::Tensor> Quantization(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& scale, const std::shared_ptr<one::Tensor>& zero_point, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme);

Maybe<one::TensorTuple> MinMaxObserver(const std::shared_ptr<one::Tensor>& in, const std::string& quantization_formula, int32_t quantization_bit, const std::string& quantization_scheme, bool per_layer_quantization);

Maybe<one::TensorTuple> MovingAverageMinMaxObserver(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& current_train_step, const std::shared_ptr<one::Tensor>& moving_max, const std::shared_ptr<one::Tensor>& moving_min, bool training, const std::string& quantization_formula, int64_t stop_update_after_iters, int32_t quantization_bit, const std::string& quantization_scheme, float momentum);

Maybe<one::Tensor> Conv3d(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& weight, const Optional<one::Tensor>& bias, const std::vector<int32_t>& stride, const std::vector<int32_t>& padding, const std::vector<int32_t>& dilation, int32_t groups);

Maybe<one::Tensor> ConvDataGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& weight, const std::shared_ptr<one::Tensor>& x, int32_t num_spatial_dims, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& strides, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& dilation_rate, int32_t groups, const std::string& data_format);

Maybe<one::Tensor> ConvFilterGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, int32_t num_spatial_dims, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& strides, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& dilation_rate, int32_t groups, const std::string& data_format);

Maybe<one::Tensor> ConvBiasGrad(const std::shared_ptr<one::Tensor>& dy, int32_t num_spatial_dims, const std::string& data_format);

Maybe<one::Tensor> Expand(const std::shared_ptr<one::Tensor>& x, const Shape& shape);

Maybe<one::Tensor> ExpandDims(const std::shared_ptr<one::Tensor>& x, int32_t axis);

Maybe<one::Tensor> Exp(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Gather(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& indices, int64_t axis);

Maybe<one::Tensor> DimGather(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& indices, int32_t dim);

Maybe<one::Tensor> GatherNd(const std::shared_ptr<one::Tensor>& params, const std::shared_ptr<one::Tensor>& indices);

Maybe<one::Tensor> ScatterNd(const std::shared_ptr<one::Tensor>& indices, const std::shared_ptr<one::Tensor>& updates, const Shape& shape);

Maybe<one::Tensor> ScatterNdLike(const std::shared_ptr<one::Tensor>& like, const std::shared_ptr<one::Tensor>& updates, const std::shared_ptr<one::Tensor>& indices);

Maybe<one::Tensor> MatMul(const std::shared_ptr<one::Tensor>& a, const std::shared_ptr<one::Tensor>& b, bool transpose_a, bool transpose_b, double alpha);

Maybe<one::Tensor> BatchMatMul(const std::shared_ptr<one::Tensor>& a, const std::shared_ptr<one::Tensor>& b, bool transpose_a, bool transpose_b, double alpha);

Maybe<one::Tensor> SparseSoftmaxCrossEntropy(const std::shared_ptr<one::Tensor>& logits, const std::shared_ptr<one::Tensor>& label, int64_t depth);

Maybe<one::Tensor> SmoothL1Loss(const std::shared_ptr<one::Tensor>& logits, const std::shared_ptr<one::Tensor>& label, float beta);

Maybe<one::Tensor> SmoothL1LossGrad(const std::shared_ptr<one::Tensor>& loss_grad, const std::shared_ptr<one::Tensor>& prediction, const std::shared_ptr<one::Tensor>& label, float beta);

Maybe<one::Tensor> Where(const std::shared_ptr<one::Tensor>& condition, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Negative(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> LayerNormAffine(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& gamma, const std::shared_ptr<one::Tensor>& beta, int64_t begin_norm_axis, int64_t begin_params_axis, double epsilon);

Maybe<one::Tensor> LayerNorm(const std::shared_ptr<one::Tensor>& x, int64_t begin_norm_axis, int64_t begin_params_axis, double epsilon);

Maybe<one::Tensor> AvgPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::string& data_format, bool ceil_mode);

Maybe<one::Tensor> MaxPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::string& data_format, bool ceil_mode);

Maybe<one::Tensor> AdaptiveAvgPool1D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size);

Maybe<one::Tensor> AdaptiveAvgPool2D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size);

Maybe<one::Tensor> AdaptiveAvgPool3D(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& output_size);

Maybe<one::Tensor> AdaptivePoolNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims);

Maybe<one::Tensor> PoolNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims, const std::string& data_format, const std::string& padding, const std::vector<int32_t>& padding_before, const std::vector<int32_t>& padding_after, const std::vector<int32_t>& pool_size, const std::vector<int32_t>& strides, bool ceil_mode);

Maybe<one::TensorTuple> Maxpool1D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode);

Maybe<one::TensorTuple> Maxpool2D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode);

Maybe<one::TensorTuple> Maxpool3D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode);

Maybe<one::Tensor> PoolingNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& indice, const std::shared_ptr<one::Tensor>& dy, const std::string& mode, int32_t ndims, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, const std::vector<int32_t>& dilation, bool return_indices, bool ceil_mode);

Maybe<one::Tensor> PRelu(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& alpha);

Maybe<one::TensorTuple> PReluGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& alpha);

Maybe<one::Tensor> Reshape(const std::shared_ptr<one::Tensor>& x, const Shape& shape);

Maybe<one::Tensor> Slice(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step);

Maybe<one::Tensor> SliceGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& like, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step);

Maybe<one::Tensor> SliceUpdate(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& update, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step);

Maybe<one::Tensor> LogicalSlice(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step);

Maybe<void> LogicalSliceAssign(const std::shared_ptr<one::Tensor>& ref, const std::shared_ptr<one::Tensor>& value, const std::vector<int64_t>& start, const std::vector<int64_t>& stop, const std::vector<int64_t>& step);

Maybe<one::Tensor> Squeeze(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& dim);

Maybe<one::Tensor> Copy(const std::shared_ptr<one::Tensor>& x, const std::string& device_type, int64_t device_id);

Maybe<one::Tensor> Flip(const std::shared_ptr<one::Tensor>& x, const std::vector<int32_t>& dims);

Maybe<one::Tensor> FlipGrad(const std::shared_ptr<one::Tensor>& dy, const std::vector<int32_t>& dims);

Maybe<one::Tensor> Upsample(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& interpolation, const std::string& data_format);

Maybe<one::Tensor> UpsampleLinear1D(const std::shared_ptr<one::Tensor>& x, float scale_factor, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleLinear1DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float scale_factor, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest1D(const std::shared_ptr<one::Tensor>& x, float scale_factor, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest1DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float scale_factor, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, const std::string& data_format);

Maybe<one::Tensor> UpsampleBilinear2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleBilinear2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleBicubic2D(const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleBicubic2DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest3D(const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, const std::string& data_format);

Maybe<one::Tensor> UpsampleNearest3DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, const std::string& data_format);

Maybe<one::Tensor> UpsampleTrilinear3D(const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> UpsampleTrilinear3DGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, float depth_scale, float height_scale, float width_scale, bool align_corners, const std::string& data_format);

Maybe<one::Tensor> Abs(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Acos(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Acosh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Asin(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Asinh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Atan(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Atanh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Ceil(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Erf(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Erfc(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Expm1(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Floor(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Lgamma(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Log1p(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> LogSigmoid(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Rint(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Round(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Sign(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Sinh(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> Softplus(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> UnsortedSegmentSumLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& segment_ids, const std::shared_ptr<one::Tensor>& like, int64_t axis);

Maybe<one::Tensor> Triu(const std::shared_ptr<one::Tensor>& x, int64_t diagonal);

Maybe<one::Tensor> ClipByScalar(const std::shared_ptr<one::Tensor>& x, const Scalar& min, const Scalar& max);

Maybe<one::Tensor> ClipByScalarGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& min, const Scalar& max);

Maybe<one::Tensor> ClipByScalarMin(const std::shared_ptr<one::Tensor>& x, const Scalar& min);

Maybe<one::Tensor> ClipByScalarMinGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& min);

Maybe<one::Tensor> ClipByScalarMax(const std::shared_ptr<one::Tensor>& x, const Scalar& max);

Maybe<one::Tensor> ClipByScalarMaxGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& x, const Scalar& max);

Maybe<one::Tensor> Dropout(const std::shared_ptr<one::Tensor>& x, float p, const Optional<one::Generator>& generator);

Maybe<one::Tensor> Pad(const std::shared_ptr<one::Tensor>& x, const std::vector<int64_t>& pad, const std::string& mode, const Scalar& value);

Maybe<one::Tensor> PadGrad(const std::shared_ptr<one::Tensor>& dy, const std::vector<int64_t>& pad, const std::string& mode, const Scalar& value);

Maybe<one::Tensor> Silu(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> SiluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy);

Maybe<one::Tensor> Mish(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> MishGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy);

Maybe<one::Tensor> Selu(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> SeluGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy);

Maybe<one::Tensor> SoftSign(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> SoftSignGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& dy);

Maybe<one::Tensor> Diag(const std::shared_ptr<one::Tensor>& x, int32_t diagonal);

Maybe<one::Tensor> DiagGrad(const std::shared_ptr<one::Tensor>& dy, const std::shared_ptr<one::Tensor>& in, int32_t diagonal);

Maybe<one::Tensor> TensorGetItem(const std::shared_ptr<one::Tensor>& x, const TensorIndex& index);

Maybe<one::Tensor> DimScatter(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, const std::shared_ptr<one::Tensor>& src, int32_t dim);

Maybe<one::Tensor> DimScatterAdd(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, const std::shared_ptr<one::Tensor>& src, int32_t dim);

Maybe<one::Tensor> DimScatterUpdateScalar(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, float src, int32_t dim);

Maybe<one::Tensor> DimScatterAddScalar(const std::shared_ptr<one::Tensor>& input, const std::shared_ptr<one::Tensor>& index, float src, int32_t dim);

Maybe<void> TensorSetItem(const std::shared_ptr<one::Tensor>& x, const TensorIndex& index, const std::shared_ptr<one::Tensor>& value);

Maybe<one::Tensor> Avgpool1D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override);

Maybe<one::Tensor> Avgpool2D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override);

Maybe<one::Tensor> Avgpool3D(const std::shared_ptr<one::Tensor>& x, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override);

Maybe<one::Tensor> AvgPoolingNdGrad(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y, const std::shared_ptr<one::Tensor>& dy, int32_t ndims, const std::string& data_format, const std::vector<int32_t>& padding, const std::vector<int32_t>& kernel_size, const std::vector<int32_t>& stride, bool ceil_mode, bool count_include_pad, int64_t divisor_override);

Maybe<one::Tensor> Minimum(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Maximum(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::TensorTuple> ElementwiseMinGrad(const std::shared_ptr<one::Tensor>& dz, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::TensorTuple> ElementwiseMaxGrad(const std::shared_ptr<one::Tensor>& dz, const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& y);

Maybe<one::Tensor> Stack(const TensorTuple& inputs, int64_t dim);

Maybe<one::Tensor> ToConsistent(const std::shared_ptr<one::Tensor>& x, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp, const Optional<Shape>& shape);

Maybe<one::Tensor> ConsistentToLocal(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> AllReduce(const std::shared_ptr<one::Tensor>& x);

Maybe<one::Tensor> SelectFirst(const TensorTuple& inputs);

Maybe<one::Tensor> CastLike(const std::shared_ptr<one::Tensor>& x, const std::shared_ptr<one::Tensor>& like);

Maybe<one::Tensor> Identity(const std::shared_ptr<one::Tensor>& in);

Maybe<one::Tensor> ReshapeLike(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& like);

Maybe<one::Tensor> ReduceSumLike(const std::shared_ptr<one::Tensor>& in, const std::shared_ptr<one::Tensor>& like, const std::vector<int32_t>& axis);

Maybe<one::Tensor> RandN(const Shape& shape, const Optional<DataType>& dtype, const Optional<Symbol<Device>>& device, const Optional<one::Generator>& generator);

Maybe<one::Tensor> ConsistentRandN(const Shape& shape, const Symbol<ParallelDesc>& placement, const std::vector<Symbol<cfg::SbpParallel>>& sbp_tuple, const Optional<DataType>& dtype, const Optional<one::Generator>& generator);

}  // namespace functional
}  // namespace one
}  // namespace oneflow

#endif  // ONEFLOW_CORE_FUNCTIONAL_GENERATED_FUNCTIONAL_API_H_