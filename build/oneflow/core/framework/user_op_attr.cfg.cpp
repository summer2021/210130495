#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/framework/user_op_attr.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstAttrValue_ListInt32::_AttrValue_ListInt32_::_AttrValue_ListInt32_() { Clear(); }
ConstAttrValue_ListInt32::_AttrValue_ListInt32_::_AttrValue_ListInt32_(const _AttrValue_ListInt32_& other) { CopyFrom(other); }
ConstAttrValue_ListInt32::_AttrValue_ListInt32_::_AttrValue_ListInt32_(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32) {
  InitFromProto(proto_attrvalue_listint32);
}
ConstAttrValue_ListInt32::_AttrValue_ListInt32_::_AttrValue_ListInt32_(_AttrValue_ListInt32_&& other) = default;
ConstAttrValue_ListInt32::_AttrValue_ListInt32_::~_AttrValue_ListInt32_() = default;

void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::InitFromProto(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_listint32.val().empty()) {
    for (const int32_t& elem : proto_attrvalue_listint32.val()) {
      add_val(elem);
    }
  }
    
}

void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::ToProto(::oneflow::AttrValue_ListInt32* proto_attrvalue_listint32) const {
  proto_attrvalue_listint32->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const int32_t& elem : val()) {
      proto_attrvalue_listint32->add_val(elem);
    }
  }

}

::std::string ConstAttrValue_ListInt32::_AttrValue_ListInt32_::DebugString() const {
  ::oneflow::AttrValue_ListInt32 proto_attrvalue_listint32;
  this->ToProto(&proto_attrvalue_listint32);
  return proto_attrvalue_listint32.DebugString();
}

void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::Clear() {
  clear_val();
}

void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::CopyFrom(const _AttrValue_ListInt32_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListInt32::_AttrValue_ListInt32_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& ConstAttrValue_ListInt32::_AttrValue_ListInt32_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const int32_t& ConstAttrValue_ListInt32::_AttrValue_ListInt32_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_* ConstAttrValue_ListInt32::_AttrValue_ListInt32_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
  }
  return  val_.get();
}
int32_t* ConstAttrValue_ListInt32::_AttrValue_ListInt32_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
  }
  return  val_->Mutable(index);
}
void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::add_val(const int32_t& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
  }
  return val_->Add(value);
}
void ConstAttrValue_ListInt32::_AttrValue_ListInt32_::set_val(::std::size_t index, const int32_t& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>();
  }
  return val_->Set(index, value);
}


int ConstAttrValue_ListInt32::_AttrValue_ListInt32_::compare(const _AttrValue_ListInt32_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListInt32::_AttrValue_ListInt32_::operator==(const _AttrValue_ListInt32_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListInt32::_AttrValue_ListInt32_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListInt32::_AttrValue_ListInt32_::operator<(const _AttrValue_ListInt32_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListInt32_ =  ConstAttrValue_ListInt32::_AttrValue_ListInt32_;
ConstAttrValue_ListInt32::ConstAttrValue_ListInt32(const ::std::shared_ptr<_AttrValue_ListInt32_>& data): data_(data) {}
ConstAttrValue_ListInt32::ConstAttrValue_ListInt32(): data_(::std::make_shared<_AttrValue_ListInt32_>()) {}
ConstAttrValue_ListInt32::ConstAttrValue_ListInt32(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32) {
  BuildFromProto(proto_attrvalue_listint32);
}
ConstAttrValue_ListInt32::ConstAttrValue_ListInt32(const ConstAttrValue_ListInt32&) = default;
ConstAttrValue_ListInt32::ConstAttrValue_ListInt32(ConstAttrValue_ListInt32&&) noexcept = default;
ConstAttrValue_ListInt32::~ConstAttrValue_ListInt32() = default;

void ConstAttrValue_ListInt32::ToProto(PbMessage* proto_attrvalue_listint32) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListInt32*>(proto_attrvalue_listint32));
}
  
::std::string ConstAttrValue_ListInt32::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListInt32::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListInt32::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListInt32::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListInt32::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int32_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListInt32::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListInt32::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& ConstAttrValue_ListInt32::val() const {
  return __SharedPtrOrDefault__()->val();
}
const int32_t& ConstAttrValue_ListInt32::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> ConstAttrValue_ListInt32::shared_const_val() const {
  return val().__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListInt32> ConstAttrValue_ListInt32::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListInt32>(data_);
}
int64_t ConstAttrValue_ListInt32::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListInt32::operator==(const ConstAttrValue_ListInt32& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListInt32::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListInt32::operator<(const ConstAttrValue_ListInt32& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListInt32_>& ConstAttrValue_ListInt32::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListInt32_> default_ptr = std::make_shared<_AttrValue_ListInt32_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListInt32_>& ConstAttrValue_ListInt32::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListInt32_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListInt32
void ConstAttrValue_ListInt32::BuildFromProto(const PbMessage& proto_attrvalue_listint32) {
  data_ = ::std::make_shared<_AttrValue_ListInt32_>(dynamic_cast<const ::oneflow::AttrValue_ListInt32&>(proto_attrvalue_listint32));
}

AttrValue_ListInt32::AttrValue_ListInt32(const ::std::shared_ptr<_AttrValue_ListInt32_>& data)
  : ConstAttrValue_ListInt32(data) {}
AttrValue_ListInt32::AttrValue_ListInt32(const AttrValue_ListInt32& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListInt32> resize
AttrValue_ListInt32::AttrValue_ListInt32(AttrValue_ListInt32&&) noexcept = default; 
AttrValue_ListInt32::AttrValue_ListInt32(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32) {
  InitFromProto(proto_attrvalue_listint32);
}
AttrValue_ListInt32::AttrValue_ListInt32() = default;

AttrValue_ListInt32::~AttrValue_ListInt32() = default;

void AttrValue_ListInt32::InitFromProto(const PbMessage& proto_attrvalue_listint32) {
  BuildFromProto(proto_attrvalue_listint32);
}
  
void* AttrValue_ListInt32::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListInt32::operator==(const AttrValue_ListInt32& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListInt32::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListInt32::operator<(const AttrValue_ListInt32& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListInt32::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListInt32::CopyFrom(const AttrValue_ListInt32& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListInt32& AttrValue_ListInt32::operator=(const AttrValue_ListInt32& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListInt32::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_* AttrValue_ListInt32::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
int32_t* AttrValue_ListInt32::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
void AttrValue_ListInt32::add_val(const int32_t& value) {
  return __SharedPtr__()->add_val(value);
}
void AttrValue_ListInt32::set_val(::std::size_t index, const int32_t& value) {
  return __SharedPtr__()->set_val(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> AttrValue_ListInt32::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}

::std::shared_ptr<AttrValue_ListInt32> AttrValue_ListInt32::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListInt32>(__SharedPtr__());
}
ConstAttrValue_ListInt64::_AttrValue_ListInt64_::_AttrValue_ListInt64_() { Clear(); }
ConstAttrValue_ListInt64::_AttrValue_ListInt64_::_AttrValue_ListInt64_(const _AttrValue_ListInt64_& other) { CopyFrom(other); }
ConstAttrValue_ListInt64::_AttrValue_ListInt64_::_AttrValue_ListInt64_(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64) {
  InitFromProto(proto_attrvalue_listint64);
}
ConstAttrValue_ListInt64::_AttrValue_ListInt64_::_AttrValue_ListInt64_(_AttrValue_ListInt64_&& other) = default;
ConstAttrValue_ListInt64::_AttrValue_ListInt64_::~_AttrValue_ListInt64_() = default;

void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::InitFromProto(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_listint64.val().empty()) {
    for (const int64_t& elem : proto_attrvalue_listint64.val()) {
      add_val(elem);
    }
  }
    
}

void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::ToProto(::oneflow::AttrValue_ListInt64* proto_attrvalue_listint64) const {
  proto_attrvalue_listint64->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const int64_t& elem : val()) {
      proto_attrvalue_listint64->add_val(elem);
    }
  }

}

::std::string ConstAttrValue_ListInt64::_AttrValue_ListInt64_::DebugString() const {
  ::oneflow::AttrValue_ListInt64 proto_attrvalue_listint64;
  this->ToProto(&proto_attrvalue_listint64);
  return proto_attrvalue_listint64.DebugString();
}

void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::Clear() {
  clear_val();
}

void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::CopyFrom(const _AttrValue_ListInt64_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListInt64::_AttrValue_ListInt64_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& ConstAttrValue_ListInt64::_AttrValue_ListInt64_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const int64_t& ConstAttrValue_ListInt64::_AttrValue_ListInt64_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_* ConstAttrValue_ListInt64::_AttrValue_ListInt64_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
  }
  return  val_.get();
}
int64_t* ConstAttrValue_ListInt64::_AttrValue_ListInt64_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
  }
  return  val_->Mutable(index);
}
void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::add_val(const int64_t& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
  }
  return val_->Add(value);
}
void ConstAttrValue_ListInt64::_AttrValue_ListInt64_::set_val(::std::size_t index, const int64_t& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>();
  }
  return val_->Set(index, value);
}


int ConstAttrValue_ListInt64::_AttrValue_ListInt64_::compare(const _AttrValue_ListInt64_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListInt64::_AttrValue_ListInt64_::operator==(const _AttrValue_ListInt64_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListInt64::_AttrValue_ListInt64_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListInt64::_AttrValue_ListInt64_::operator<(const _AttrValue_ListInt64_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListInt64_ =  ConstAttrValue_ListInt64::_AttrValue_ListInt64_;
ConstAttrValue_ListInt64::ConstAttrValue_ListInt64(const ::std::shared_ptr<_AttrValue_ListInt64_>& data): data_(data) {}
ConstAttrValue_ListInt64::ConstAttrValue_ListInt64(): data_(::std::make_shared<_AttrValue_ListInt64_>()) {}
ConstAttrValue_ListInt64::ConstAttrValue_ListInt64(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64) {
  BuildFromProto(proto_attrvalue_listint64);
}
ConstAttrValue_ListInt64::ConstAttrValue_ListInt64(const ConstAttrValue_ListInt64&) = default;
ConstAttrValue_ListInt64::ConstAttrValue_ListInt64(ConstAttrValue_ListInt64&&) noexcept = default;
ConstAttrValue_ListInt64::~ConstAttrValue_ListInt64() = default;

void ConstAttrValue_ListInt64::ToProto(PbMessage* proto_attrvalue_listint64) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListInt64*>(proto_attrvalue_listint64));
}
  
::std::string ConstAttrValue_ListInt64::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListInt64::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListInt64::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListInt64::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListInt64::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int64_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListInt64::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListInt64::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& ConstAttrValue_ListInt64::val() const {
  return __SharedPtrOrDefault__()->val();
}
const int64_t& ConstAttrValue_ListInt64::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> ConstAttrValue_ListInt64::shared_const_val() const {
  return val().__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListInt64> ConstAttrValue_ListInt64::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListInt64>(data_);
}
int64_t ConstAttrValue_ListInt64::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListInt64::operator==(const ConstAttrValue_ListInt64& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListInt64::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListInt64::operator<(const ConstAttrValue_ListInt64& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListInt64_>& ConstAttrValue_ListInt64::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListInt64_> default_ptr = std::make_shared<_AttrValue_ListInt64_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListInt64_>& ConstAttrValue_ListInt64::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListInt64_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListInt64
void ConstAttrValue_ListInt64::BuildFromProto(const PbMessage& proto_attrvalue_listint64) {
  data_ = ::std::make_shared<_AttrValue_ListInt64_>(dynamic_cast<const ::oneflow::AttrValue_ListInt64&>(proto_attrvalue_listint64));
}

AttrValue_ListInt64::AttrValue_ListInt64(const ::std::shared_ptr<_AttrValue_ListInt64_>& data)
  : ConstAttrValue_ListInt64(data) {}
AttrValue_ListInt64::AttrValue_ListInt64(const AttrValue_ListInt64& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListInt64> resize
AttrValue_ListInt64::AttrValue_ListInt64(AttrValue_ListInt64&&) noexcept = default; 
AttrValue_ListInt64::AttrValue_ListInt64(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64) {
  InitFromProto(proto_attrvalue_listint64);
}
AttrValue_ListInt64::AttrValue_ListInt64() = default;

AttrValue_ListInt64::~AttrValue_ListInt64() = default;

void AttrValue_ListInt64::InitFromProto(const PbMessage& proto_attrvalue_listint64) {
  BuildFromProto(proto_attrvalue_listint64);
}
  
void* AttrValue_ListInt64::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListInt64::operator==(const AttrValue_ListInt64& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListInt64::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListInt64::operator<(const AttrValue_ListInt64& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListInt64::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListInt64::CopyFrom(const AttrValue_ListInt64& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListInt64& AttrValue_ListInt64::operator=(const AttrValue_ListInt64& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListInt64::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_* AttrValue_ListInt64::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
int64_t* AttrValue_ListInt64::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
void AttrValue_ListInt64::add_val(const int64_t& value) {
  return __SharedPtr__()->add_val(value);
}
void AttrValue_ListInt64::set_val(::std::size_t index, const int64_t& value) {
  return __SharedPtr__()->set_val(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> AttrValue_ListInt64::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}

::std::shared_ptr<AttrValue_ListInt64> AttrValue_ListInt64::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListInt64>(__SharedPtr__());
}
ConstAttrValue_ListFloat::_AttrValue_ListFloat_::_AttrValue_ListFloat_() { Clear(); }
ConstAttrValue_ListFloat::_AttrValue_ListFloat_::_AttrValue_ListFloat_(const _AttrValue_ListFloat_& other) { CopyFrom(other); }
ConstAttrValue_ListFloat::_AttrValue_ListFloat_::_AttrValue_ListFloat_(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat) {
  InitFromProto(proto_attrvalue_listfloat);
}
ConstAttrValue_ListFloat::_AttrValue_ListFloat_::_AttrValue_ListFloat_(_AttrValue_ListFloat_&& other) = default;
ConstAttrValue_ListFloat::_AttrValue_ListFloat_::~_AttrValue_ListFloat_() = default;

void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::InitFromProto(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_listfloat.val().empty()) {
    for (const float& elem : proto_attrvalue_listfloat.val()) {
      add_val(elem);
    }
  }
    
}

void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::ToProto(::oneflow::AttrValue_ListFloat* proto_attrvalue_listfloat) const {
  proto_attrvalue_listfloat->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const float& elem : val()) {
      proto_attrvalue_listfloat->add_val(elem);
    }
  }

}

::std::string ConstAttrValue_ListFloat::_AttrValue_ListFloat_::DebugString() const {
  ::oneflow::AttrValue_ListFloat proto_attrvalue_listfloat;
  this->ToProto(&proto_attrvalue_listfloat);
  return proto_attrvalue_listfloat.DebugString();
}

void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::Clear() {
  clear_val();
}

void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::CopyFrom(const _AttrValue_ListFloat_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListFloat::_AttrValue_ListFloat_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& ConstAttrValue_ListFloat::_AttrValue_ListFloat_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const float& ConstAttrValue_ListFloat::_AttrValue_ListFloat_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_* ConstAttrValue_ListFloat::_AttrValue_ListFloat_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
  }
  return  val_.get();
}
float* ConstAttrValue_ListFloat::_AttrValue_ListFloat_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
  }
  return  val_->Mutable(index);
}
void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::add_val(const float& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
  }
  return val_->Add(value);
}
void ConstAttrValue_ListFloat::_AttrValue_ListFloat_::set_val(::std::size_t index, const float& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>();
  }
  return val_->Set(index, value);
}


int ConstAttrValue_ListFloat::_AttrValue_ListFloat_::compare(const _AttrValue_ListFloat_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListFloat::_AttrValue_ListFloat_::operator==(const _AttrValue_ListFloat_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListFloat::_AttrValue_ListFloat_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListFloat::_AttrValue_ListFloat_::operator<(const _AttrValue_ListFloat_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListFloat_ =  ConstAttrValue_ListFloat::_AttrValue_ListFloat_;
ConstAttrValue_ListFloat::ConstAttrValue_ListFloat(const ::std::shared_ptr<_AttrValue_ListFloat_>& data): data_(data) {}
ConstAttrValue_ListFloat::ConstAttrValue_ListFloat(): data_(::std::make_shared<_AttrValue_ListFloat_>()) {}
ConstAttrValue_ListFloat::ConstAttrValue_ListFloat(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat) {
  BuildFromProto(proto_attrvalue_listfloat);
}
ConstAttrValue_ListFloat::ConstAttrValue_ListFloat(const ConstAttrValue_ListFloat&) = default;
ConstAttrValue_ListFloat::ConstAttrValue_ListFloat(ConstAttrValue_ListFloat&&) noexcept = default;
ConstAttrValue_ListFloat::~ConstAttrValue_ListFloat() = default;

void ConstAttrValue_ListFloat::ToProto(PbMessage* proto_attrvalue_listfloat) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListFloat*>(proto_attrvalue_listfloat));
}
  
::std::string ConstAttrValue_ListFloat::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListFloat::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListFloat::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListFloat::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListFloat::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<float>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListFloat::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListFloat::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& ConstAttrValue_ListFloat::val() const {
  return __SharedPtrOrDefault__()->val();
}
const float& ConstAttrValue_ListFloat::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> ConstAttrValue_ListFloat::shared_const_val() const {
  return val().__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListFloat> ConstAttrValue_ListFloat::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListFloat>(data_);
}
int64_t ConstAttrValue_ListFloat::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListFloat::operator==(const ConstAttrValue_ListFloat& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListFloat::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListFloat::operator<(const ConstAttrValue_ListFloat& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListFloat_>& ConstAttrValue_ListFloat::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListFloat_> default_ptr = std::make_shared<_AttrValue_ListFloat_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListFloat_>& ConstAttrValue_ListFloat::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListFloat_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListFloat
void ConstAttrValue_ListFloat::BuildFromProto(const PbMessage& proto_attrvalue_listfloat) {
  data_ = ::std::make_shared<_AttrValue_ListFloat_>(dynamic_cast<const ::oneflow::AttrValue_ListFloat&>(proto_attrvalue_listfloat));
}

AttrValue_ListFloat::AttrValue_ListFloat(const ::std::shared_ptr<_AttrValue_ListFloat_>& data)
  : ConstAttrValue_ListFloat(data) {}
AttrValue_ListFloat::AttrValue_ListFloat(const AttrValue_ListFloat& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListFloat> resize
AttrValue_ListFloat::AttrValue_ListFloat(AttrValue_ListFloat&&) noexcept = default; 
AttrValue_ListFloat::AttrValue_ListFloat(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat) {
  InitFromProto(proto_attrvalue_listfloat);
}
AttrValue_ListFloat::AttrValue_ListFloat() = default;

AttrValue_ListFloat::~AttrValue_ListFloat() = default;

void AttrValue_ListFloat::InitFromProto(const PbMessage& proto_attrvalue_listfloat) {
  BuildFromProto(proto_attrvalue_listfloat);
}
  
void* AttrValue_ListFloat::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListFloat::operator==(const AttrValue_ListFloat& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListFloat::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListFloat::operator<(const AttrValue_ListFloat& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListFloat::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListFloat::CopyFrom(const AttrValue_ListFloat& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListFloat& AttrValue_ListFloat::operator=(const AttrValue_ListFloat& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListFloat::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_* AttrValue_ListFloat::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
float* AttrValue_ListFloat::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
void AttrValue_ListFloat::add_val(const float& value) {
  return __SharedPtr__()->add_val(value);
}
void AttrValue_ListFloat::set_val(::std::size_t index, const float& value) {
  return __SharedPtr__()->set_val(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> AttrValue_ListFloat::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}

::std::shared_ptr<AttrValue_ListFloat> AttrValue_ListFloat::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListFloat>(__SharedPtr__());
}
ConstAttrValue_ListDataType::_AttrValue_ListDataType_::_AttrValue_ListDataType_() { Clear(); }
ConstAttrValue_ListDataType::_AttrValue_ListDataType_::_AttrValue_ListDataType_(const _AttrValue_ListDataType_& other) { CopyFrom(other); }
ConstAttrValue_ListDataType::_AttrValue_ListDataType_::_AttrValue_ListDataType_(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype) {
  InitFromProto(proto_attrvalue_listdatatype);
}
ConstAttrValue_ListDataType::_AttrValue_ListDataType_::_AttrValue_ListDataType_(_AttrValue_ListDataType_&& other) = default;
ConstAttrValue_ListDataType::_AttrValue_ListDataType_::~_AttrValue_ListDataType_() = default;

void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::InitFromProto(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_listdatatype.val().empty()) {
    for (const int& elem : proto_attrvalue_listdatatype.val() ) {
      add_val(::oneflow::cfg::DataType(elem));
    }
  }
    
}

void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::ToProto(::oneflow::AttrValue_ListDataType* proto_attrvalue_listdatatype) const {
  proto_attrvalue_listdatatype->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const int& elem : val() ) {
      proto_attrvalue_listdatatype->add_val(::oneflow::DataType(elem));
    }
  }

}

::std::string ConstAttrValue_ListDataType::_AttrValue_ListDataType_::DebugString() const {
  ::oneflow::AttrValue_ListDataType proto_attrvalue_listdatatype;
  this->ToProto(&proto_attrvalue_listdatatype);
  return proto_attrvalue_listdatatype.DebugString();
}

void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::Clear() {
  clear_val();
}

void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::CopyFrom(const _AttrValue_ListDataType_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListDataType::_AttrValue_ListDataType_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& ConstAttrValue_ListDataType::_AttrValue_ListDataType_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const ::oneflow::cfg::DataType& ConstAttrValue_ListDataType::_AttrValue_ListDataType_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_* ConstAttrValue_ListDataType::_AttrValue_ListDataType_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
  }
  return  val_.get();
}
::oneflow::cfg::DataType* ConstAttrValue_ListDataType::_AttrValue_ListDataType_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
  }
  return  val_->Mutable(index);
}
void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::add_val(const ::oneflow::cfg::DataType& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
  }
  return val_->Add(value);
}
void ConstAttrValue_ListDataType::_AttrValue_ListDataType_::set_val(::std::size_t index, const ::oneflow::cfg::DataType& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>();
  }
  return val_->Set(index, value);
}


int ConstAttrValue_ListDataType::_AttrValue_ListDataType_::compare(const _AttrValue_ListDataType_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListDataType::_AttrValue_ListDataType_::operator==(const _AttrValue_ListDataType_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListDataType::_AttrValue_ListDataType_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListDataType::_AttrValue_ListDataType_::operator<(const _AttrValue_ListDataType_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListDataType_ =  ConstAttrValue_ListDataType::_AttrValue_ListDataType_;
ConstAttrValue_ListDataType::ConstAttrValue_ListDataType(const ::std::shared_ptr<_AttrValue_ListDataType_>& data): data_(data) {}
ConstAttrValue_ListDataType::ConstAttrValue_ListDataType(): data_(::std::make_shared<_AttrValue_ListDataType_>()) {}
ConstAttrValue_ListDataType::ConstAttrValue_ListDataType(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype) {
  BuildFromProto(proto_attrvalue_listdatatype);
}
ConstAttrValue_ListDataType::ConstAttrValue_ListDataType(const ConstAttrValue_ListDataType&) = default;
ConstAttrValue_ListDataType::ConstAttrValue_ListDataType(ConstAttrValue_ListDataType&&) noexcept = default;
ConstAttrValue_ListDataType::~ConstAttrValue_ListDataType() = default;

void ConstAttrValue_ListDataType::ToProto(PbMessage* proto_attrvalue_listdatatype) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListDataType*>(proto_attrvalue_listdatatype));
}
  
::std::string ConstAttrValue_ListDataType::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListDataType::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListDataType::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListDataType::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListDataType::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::DataType>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListDataType::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListDataType::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& ConstAttrValue_ListDataType::val() const {
  return __SharedPtrOrDefault__()->val();
}
const ::oneflow::cfg::DataType& ConstAttrValue_ListDataType::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> ConstAttrValue_ListDataType::shared_const_val() const {
  return val().__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListDataType> ConstAttrValue_ListDataType::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListDataType>(data_);
}
int64_t ConstAttrValue_ListDataType::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListDataType::operator==(const ConstAttrValue_ListDataType& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListDataType::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListDataType::operator<(const ConstAttrValue_ListDataType& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListDataType_>& ConstAttrValue_ListDataType::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListDataType_> default_ptr = std::make_shared<_AttrValue_ListDataType_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListDataType_>& ConstAttrValue_ListDataType::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListDataType_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListDataType
void ConstAttrValue_ListDataType::BuildFromProto(const PbMessage& proto_attrvalue_listdatatype) {
  data_ = ::std::make_shared<_AttrValue_ListDataType_>(dynamic_cast<const ::oneflow::AttrValue_ListDataType&>(proto_attrvalue_listdatatype));
}

AttrValue_ListDataType::AttrValue_ListDataType(const ::std::shared_ptr<_AttrValue_ListDataType_>& data)
  : ConstAttrValue_ListDataType(data) {}
AttrValue_ListDataType::AttrValue_ListDataType(const AttrValue_ListDataType& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListDataType> resize
AttrValue_ListDataType::AttrValue_ListDataType(AttrValue_ListDataType&&) noexcept = default; 
AttrValue_ListDataType::AttrValue_ListDataType(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype) {
  InitFromProto(proto_attrvalue_listdatatype);
}
AttrValue_ListDataType::AttrValue_ListDataType() = default;

AttrValue_ListDataType::~AttrValue_ListDataType() = default;

void AttrValue_ListDataType::InitFromProto(const PbMessage& proto_attrvalue_listdatatype) {
  BuildFromProto(proto_attrvalue_listdatatype);
}
  
void* AttrValue_ListDataType::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListDataType::operator==(const AttrValue_ListDataType& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListDataType::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListDataType::operator<(const AttrValue_ListDataType& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListDataType::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListDataType::CopyFrom(const AttrValue_ListDataType& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListDataType& AttrValue_ListDataType::operator=(const AttrValue_ListDataType& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListDataType::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_* AttrValue_ListDataType::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
::oneflow::cfg::DataType* AttrValue_ListDataType::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
void AttrValue_ListDataType::add_val(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->add_val(value);
}
void AttrValue_ListDataType::set_val(::std::size_t index, const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_val(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> AttrValue_ListDataType::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}

::std::shared_ptr<AttrValue_ListDataType> AttrValue_ListDataType::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListDataType>(__SharedPtr__());
}
ConstAttrValue_ListShape::_AttrValue_ListShape_::_AttrValue_ListShape_() { Clear(); }
ConstAttrValue_ListShape::_AttrValue_ListShape_::_AttrValue_ListShape_(const _AttrValue_ListShape_& other) { CopyFrom(other); }
ConstAttrValue_ListShape::_AttrValue_ListShape_::_AttrValue_ListShape_(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape) {
  InitFromProto(proto_attrvalue_listshape);
}
ConstAttrValue_ListShape::_AttrValue_ListShape_::_AttrValue_ListShape_(_AttrValue_ListShape_&& other) = default;
ConstAttrValue_ListShape::_AttrValue_ListShape_::~_AttrValue_ListShape_() = default;

void ConstAttrValue_ListShape::_AttrValue_ListShape_::InitFromProto(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_listshape.val().empty()) {
    for (const ::oneflow::ShapeProto& elem : proto_attrvalue_listshape.val() ) {
      *mutable_val()->Add() = ::oneflow::cfg::ShapeProto(elem);
    }
  }
    
}

void ConstAttrValue_ListShape::_AttrValue_ListShape_::ToProto(::oneflow::AttrValue_ListShape* proto_attrvalue_listshape) const {
  proto_attrvalue_listshape->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const ::oneflow::cfg::ShapeProto& elem : val() ) {
      ::oneflow::ShapeProto proto_val_elem;
      elem.ToProto(&proto_val_elem);
      *proto_attrvalue_listshape->mutable_val()->Add() = proto_val_elem;
    }
  }

}

::std::string ConstAttrValue_ListShape::_AttrValue_ListShape_::DebugString() const {
  ::oneflow::AttrValue_ListShape proto_attrvalue_listshape;
  this->ToProto(&proto_attrvalue_listshape);
  return proto_attrvalue_listshape.DebugString();
}

void ConstAttrValue_ListShape::_AttrValue_ListShape_::Clear() {
  clear_val();
}

void ConstAttrValue_ListShape::_AttrValue_ListShape_::CopyFrom(const _AttrValue_ListShape_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListShape::_AttrValue_ListShape_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& ConstAttrValue_ListShape::_AttrValue_ListShape_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const ::oneflow::cfg::ShapeProto& ConstAttrValue_ListShape::_AttrValue_ListShape_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListShape::_AttrValue_ListShape_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_* ConstAttrValue_ListShape::_AttrValue_ListShape_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
  }
  return  val_.get();
}
::oneflow::cfg::ShapeProto* ConstAttrValue_ListShape::_AttrValue_ListShape_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
  }
  return  val_->Mutable(index);
}
::oneflow::cfg::ShapeProto* ConstAttrValue_ListShape::_AttrValue_ListShape_::add_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>();
  }
  return val_->Add();
}


int ConstAttrValue_ListShape::_AttrValue_ListShape_::compare(const _AttrValue_ListShape_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListShape::_AttrValue_ListShape_::operator==(const _AttrValue_ListShape_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListShape::_AttrValue_ListShape_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListShape::_AttrValue_ListShape_::operator<(const _AttrValue_ListShape_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListShape_ =  ConstAttrValue_ListShape::_AttrValue_ListShape_;
ConstAttrValue_ListShape::ConstAttrValue_ListShape(const ::std::shared_ptr<_AttrValue_ListShape_>& data): data_(data) {}
ConstAttrValue_ListShape::ConstAttrValue_ListShape(): data_(::std::make_shared<_AttrValue_ListShape_>()) {}
ConstAttrValue_ListShape::ConstAttrValue_ListShape(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape) {
  BuildFromProto(proto_attrvalue_listshape);
}
ConstAttrValue_ListShape::ConstAttrValue_ListShape(const ConstAttrValue_ListShape&) = default;
ConstAttrValue_ListShape::ConstAttrValue_ListShape(ConstAttrValue_ListShape&&) noexcept = default;
ConstAttrValue_ListShape::~ConstAttrValue_ListShape() = default;

void ConstAttrValue_ListShape::ToProto(PbMessage* proto_attrvalue_listshape) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListShape*>(proto_attrvalue_listshape));
}
  
::std::string ConstAttrValue_ListShape::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListShape::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListShape::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListShape::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListShape::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeProto>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListShape::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListShape::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& ConstAttrValue_ListShape::val() const {
  return __SharedPtrOrDefault__()->val();
}
const ::oneflow::cfg::ShapeProto& ConstAttrValue_ListShape::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> ConstAttrValue_ListShape::shared_const_val() const {
  return val().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstAttrValue_ListShape::shared_const_val(::std::size_t index) const {
  return val(index).__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListShape> ConstAttrValue_ListShape::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListShape>(data_);
}
int64_t ConstAttrValue_ListShape::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListShape::operator==(const ConstAttrValue_ListShape& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListShape::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListShape::operator<(const ConstAttrValue_ListShape& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListShape_>& ConstAttrValue_ListShape::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListShape_> default_ptr = std::make_shared<_AttrValue_ListShape_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListShape_>& ConstAttrValue_ListShape::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListShape_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListShape
void ConstAttrValue_ListShape::BuildFromProto(const PbMessage& proto_attrvalue_listshape) {
  data_ = ::std::make_shared<_AttrValue_ListShape_>(dynamic_cast<const ::oneflow::AttrValue_ListShape&>(proto_attrvalue_listshape));
}

AttrValue_ListShape::AttrValue_ListShape(const ::std::shared_ptr<_AttrValue_ListShape_>& data)
  : ConstAttrValue_ListShape(data) {}
AttrValue_ListShape::AttrValue_ListShape(const AttrValue_ListShape& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListShape> resize
AttrValue_ListShape::AttrValue_ListShape(AttrValue_ListShape&&) noexcept = default; 
AttrValue_ListShape::AttrValue_ListShape(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape) {
  InitFromProto(proto_attrvalue_listshape);
}
AttrValue_ListShape::AttrValue_ListShape() = default;

AttrValue_ListShape::~AttrValue_ListShape() = default;

void AttrValue_ListShape::InitFromProto(const PbMessage& proto_attrvalue_listshape) {
  BuildFromProto(proto_attrvalue_listshape);
}
  
void* AttrValue_ListShape::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListShape::operator==(const AttrValue_ListShape& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListShape::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListShape::operator<(const AttrValue_ListShape& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListShape::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListShape::CopyFrom(const AttrValue_ListShape& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListShape& AttrValue_ListShape::operator=(const AttrValue_ListShape& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListShape::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_* AttrValue_ListShape::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
::oneflow::cfg::ShapeProto* AttrValue_ListShape::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> AttrValue_ListShape::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ShapeProto> AttrValue_ListShape::shared_mutable_val(::std::size_t index) {
  return mutable_val(index)->__SharedMutable__();
}
::oneflow::cfg::ShapeProto* AttrValue_ListShape::add_val() {
  return __SharedPtr__()->add_val();
}

::std::shared_ptr<AttrValue_ListShape> AttrValue_ListShape::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListShape>(__SharedPtr__());
}
ConstAttrValue_ListString::_AttrValue_ListString_::_AttrValue_ListString_() { Clear(); }
ConstAttrValue_ListString::_AttrValue_ListString_::_AttrValue_ListString_(const _AttrValue_ListString_& other) { CopyFrom(other); }
ConstAttrValue_ListString::_AttrValue_ListString_::_AttrValue_ListString_(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring) {
  InitFromProto(proto_attrvalue_liststring);
}
ConstAttrValue_ListString::_AttrValue_ListString_::_AttrValue_ListString_(_AttrValue_ListString_&& other) = default;
ConstAttrValue_ListString::_AttrValue_ListString_::~_AttrValue_ListString_() = default;

void ConstAttrValue_ListString::_AttrValue_ListString_::InitFromProto(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring) {
  Clear();
  // repeated field: val
  if (!proto_attrvalue_liststring.val().empty()) {
    for (const ::std::string& elem : proto_attrvalue_liststring.val()) {
      add_val(elem);
    }
  }
    
}

void ConstAttrValue_ListString::_AttrValue_ListString_::ToProto(::oneflow::AttrValue_ListString* proto_attrvalue_liststring) const {
  proto_attrvalue_liststring->Clear();
  // repeated field: val
  if (!val().empty()) {
    for (const ::std::string& elem : val()) {
      proto_attrvalue_liststring->add_val(elem);
    }
  }

}

::std::string ConstAttrValue_ListString::_AttrValue_ListString_::DebugString() const {
  ::oneflow::AttrValue_ListString proto_attrvalue_liststring;
  this->ToProto(&proto_attrvalue_liststring);
  return proto_attrvalue_liststring.DebugString();
}

void ConstAttrValue_ListString::_AttrValue_ListString_::Clear() {
  clear_val();
}

void ConstAttrValue_ListString::_AttrValue_ListString_::CopyFrom(const _AttrValue_ListString_& other) {
  mutable_val()->CopyFrom(other.val());
}


// repeated field val
::std::size_t ConstAttrValue_ListString::_AttrValue_ListString_::val_size() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return val_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& ConstAttrValue_ListString::_AttrValue_ListString_::val() const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(val_.get());
}
const ::std::string& ConstAttrValue_ListString::_AttrValue_ListString_::val(::std::size_t index) const {
  if (!val_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return val_->Get(index);
}
void ConstAttrValue_ListString::_AttrValue_ListString_::clear_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
  }
  return val_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_* ConstAttrValue_ListString::_AttrValue_ListString_::mutable_val() {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
  }
  return  val_.get();
}
::std::string* ConstAttrValue_ListString::_AttrValue_ListString_::mutable_val(::std::size_t index) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
  }
  return  val_->Mutable(index);
}
void ConstAttrValue_ListString::_AttrValue_ListString_::add_val(const ::std::string& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
  }
  return val_->Add(value);
}
void ConstAttrValue_ListString::_AttrValue_ListString_::set_val(::std::size_t index, const ::std::string& value) {
  if (!val_) {
    val_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>();
  }
  return val_->Set(index, value);
}


int ConstAttrValue_ListString::_AttrValue_ListString_::compare(const _AttrValue_ListString_& other) {
  if (!(val() == other.val())) {
    return val() < other.val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrValue_ListString::_AttrValue_ListString_::operator==(const _AttrValue_ListString_& other) const {
  return true
    && val() == other.val()
  ;
}

std::size_t ConstAttrValue_ListString::_AttrValue_ListString_::__CalcHash__() const {
  return 0
    ^ val().__CalcHash__()
  ;
}

bool ConstAttrValue_ListString::_AttrValue_ListString_::operator<(const _AttrValue_ListString_& other) const {
  return false
    || !(val() == other.val()) ? 
      val() < other.val() : false
  ;
}

using _AttrValue_ListString_ =  ConstAttrValue_ListString::_AttrValue_ListString_;
ConstAttrValue_ListString::ConstAttrValue_ListString(const ::std::shared_ptr<_AttrValue_ListString_>& data): data_(data) {}
ConstAttrValue_ListString::ConstAttrValue_ListString(): data_(::std::make_shared<_AttrValue_ListString_>()) {}
ConstAttrValue_ListString::ConstAttrValue_ListString(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring) {
  BuildFromProto(proto_attrvalue_liststring);
}
ConstAttrValue_ListString::ConstAttrValue_ListString(const ConstAttrValue_ListString&) = default;
ConstAttrValue_ListString::ConstAttrValue_ListString(ConstAttrValue_ListString&&) noexcept = default;
ConstAttrValue_ListString::~ConstAttrValue_ListString() = default;

void ConstAttrValue_ListString::ToProto(PbMessage* proto_attrvalue_liststring) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue_ListString*>(proto_attrvalue_liststring));
}
  
::std::string ConstAttrValue_ListString::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue_ListString::__Empty__() const {
  return !data_;
}

int ConstAttrValue_ListString::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"val", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue_ListString::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue_ListString::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue_ListString::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &val();
    default: return nullptr;
  }
}

// repeated field val
::std::size_t ConstAttrValue_ListString::val_size() const {
  return __SharedPtrOrDefault__()->val_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& ConstAttrValue_ListString::val() const {
  return __SharedPtrOrDefault__()->val();
}
const ::std::string& ConstAttrValue_ListString::val(::std::size_t index) const {
  return __SharedPtrOrDefault__()->val(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> ConstAttrValue_ListString::shared_const_val() const {
  return val().__SharedConst__();
}

::std::shared_ptr<ConstAttrValue_ListString> ConstAttrValue_ListString::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue_ListString>(data_);
}
int64_t ConstAttrValue_ListString::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue_ListString::operator==(const ConstAttrValue_ListString& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue_ListString::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue_ListString::operator<(const ConstAttrValue_ListString& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_ListString_>& ConstAttrValue_ListString::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_ListString_> default_ptr = std::make_shared<_AttrValue_ListString_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_ListString_>& ConstAttrValue_ListString::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_ListString_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListString
void ConstAttrValue_ListString::BuildFromProto(const PbMessage& proto_attrvalue_liststring) {
  data_ = ::std::make_shared<_AttrValue_ListString_>(dynamic_cast<const ::oneflow::AttrValue_ListString&>(proto_attrvalue_liststring));
}

AttrValue_ListString::AttrValue_ListString(const ::std::shared_ptr<_AttrValue_ListString_>& data)
  : ConstAttrValue_ListString(data) {}
AttrValue_ListString::AttrValue_ListString(const AttrValue_ListString& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue_ListString> resize
AttrValue_ListString::AttrValue_ListString(AttrValue_ListString&&) noexcept = default; 
AttrValue_ListString::AttrValue_ListString(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring) {
  InitFromProto(proto_attrvalue_liststring);
}
AttrValue_ListString::AttrValue_ListString() = default;

AttrValue_ListString::~AttrValue_ListString() = default;

void AttrValue_ListString::InitFromProto(const PbMessage& proto_attrvalue_liststring) {
  BuildFromProto(proto_attrvalue_liststring);
}
  
void* AttrValue_ListString::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_val();
    default: return nullptr;
  }
}

bool AttrValue_ListString::operator==(const AttrValue_ListString& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue_ListString::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue_ListString::operator<(const AttrValue_ListString& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue_ListString::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue_ListString::CopyFrom(const AttrValue_ListString& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue_ListString& AttrValue_ListString::operator=(const AttrValue_ListString& other) {
  CopyFrom(other);
  return *this;
}

// repeated field val
void AttrValue_ListString::clear_val() {
  return __SharedPtr__()->clear_val();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_* AttrValue_ListString::mutable_val() {
  return __SharedPtr__()->mutable_val();
}
::std::string* AttrValue_ListString::mutable_val(::std::size_t index) {
  return __SharedPtr__()->mutable_val(index);
}
void AttrValue_ListString::add_val(const ::std::string& value) {
  return __SharedPtr__()->add_val(value);
}
void AttrValue_ListString::set_val(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_val(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> AttrValue_ListString::shared_mutable_val() {
  return mutable_val()->__SharedMutable__();
}

::std::shared_ptr<AttrValue_ListString> AttrValue_ListString::__SharedMutable__() {
  return ::std::make_shared<AttrValue_ListString>(__SharedPtr__());
}
ConstAttrValue::_AttrValue_::_AttrValue_() { Clear(); }
ConstAttrValue::_AttrValue_::_AttrValue_(const _AttrValue_& other) { CopyFrom(other); }
ConstAttrValue::_AttrValue_::_AttrValue_(const ::oneflow::AttrValue& proto_attrvalue) {
  InitFromProto(proto_attrvalue);
}
ConstAttrValue::_AttrValue_::_AttrValue_(_AttrValue_&& other) = default;
ConstAttrValue::_AttrValue_::~_AttrValue_() = default;

void ConstAttrValue::_AttrValue_::InitFromProto(const ::oneflow::AttrValue& proto_attrvalue) {
  Clear();
  // oneof field: value
  ValueCase value_case = ValueCase(int(proto_attrvalue.value_case()));
  switch (value_case) {
    case kAtInt32: {
      set_at_int32(proto_attrvalue.at_int32());
      break;
  }
    case kAtInt64: {
      set_at_int64(proto_attrvalue.at_int64());
      break;
  }
    case kAtBool: {
      set_at_bool(proto_attrvalue.at_bool());
      break;
  }
    case kAtFloat: {
      set_at_float(proto_attrvalue.at_float());
      break;
  }
    case kAtDouble: {
      set_at_double(proto_attrvalue.at_double());
      break;
  }
    case kAtString: {
      set_at_string(proto_attrvalue.at_string());
      break;
  }
    case kAtShape: {
      *mutable_at_shape() = ::oneflow::cfg::ShapeProto(proto_attrvalue.at_shape());
      break;
  }
    case kAtDataType: {
      set_at_data_type(static_cast<std::remove_const<std::remove_reference<decltype(at_data_type())>::type>::type>(proto_attrvalue.at_data_type()));
      break;
  }
    case kAtListInt32: {
      *mutable_at_list_int32() = ::oneflow::cfg::AttrValue_ListInt32(proto_attrvalue.at_list_int32());
      break;
  }
    case kAtListInt64: {
      *mutable_at_list_int64() = ::oneflow::cfg::AttrValue_ListInt64(proto_attrvalue.at_list_int64());
      break;
  }
    case kAtListFloat: {
      *mutable_at_list_float() = ::oneflow::cfg::AttrValue_ListFloat(proto_attrvalue.at_list_float());
      break;
  }
    case kAtListDataType: {
      *mutable_at_list_data_type() = ::oneflow::cfg::AttrValue_ListDataType(proto_attrvalue.at_list_data_type());
      break;
  }
    case kAtListShape: {
      *mutable_at_list_shape() = ::oneflow::cfg::AttrValue_ListShape(proto_attrvalue.at_list_shape());
      break;
  }
    case kAtListString: {
      *mutable_at_list_string() = ::oneflow::cfg::AttrValue_ListString(proto_attrvalue.at_list_string());
      break;
  }
    case VALUE_NOT_SET: {
      break;
    }
  }
      
}

void ConstAttrValue::_AttrValue_::ToProto(::oneflow::AttrValue* proto_attrvalue) const {
  proto_attrvalue->Clear();

  // oneof field: value
  ::oneflow::AttrValue::ValueCase value_case = ::oneflow::AttrValue::ValueCase(int(this->value_case()));
  switch (value_case) {
    case ::oneflow::AttrValue::kAtInt32: {
      proto_attrvalue->set_at_int32(at_int32());
      break;
    }
    case ::oneflow::AttrValue::kAtInt64: {
      proto_attrvalue->set_at_int64(at_int64());
      break;
    }
    case ::oneflow::AttrValue::kAtBool: {
      proto_attrvalue->set_at_bool(at_bool());
      break;
    }
    case ::oneflow::AttrValue::kAtFloat: {
      proto_attrvalue->set_at_float(at_float());
      break;
    }
    case ::oneflow::AttrValue::kAtDouble: {
      proto_attrvalue->set_at_double(at_double());
      break;
    }
    case ::oneflow::AttrValue::kAtString: {
      proto_attrvalue->set_at_string(at_string());
      break;
    }
    case ::oneflow::AttrValue::kAtShape: {
      ::oneflow::ShapeProto of_proto_at_shape;
      at_shape().ToProto(&of_proto_at_shape);
      proto_attrvalue->mutable_at_shape()->CopyFrom(of_proto_at_shape);
      break;
    }
    case ::oneflow::AttrValue::kAtDataType: {
      proto_attrvalue->set_at_data_type(static_cast<decltype(proto_attrvalue->at_data_type())>(at_data_type()));
      break;
    }
    case ::oneflow::AttrValue::kAtListInt32: {
      ::oneflow::AttrValue_ListInt32 of_proto_at_list_int32;
      at_list_int32().ToProto(&of_proto_at_list_int32);
      proto_attrvalue->mutable_at_list_int32()->CopyFrom(of_proto_at_list_int32);
      break;
    }
    case ::oneflow::AttrValue::kAtListInt64: {
      ::oneflow::AttrValue_ListInt64 of_proto_at_list_int64;
      at_list_int64().ToProto(&of_proto_at_list_int64);
      proto_attrvalue->mutable_at_list_int64()->CopyFrom(of_proto_at_list_int64);
      break;
    }
    case ::oneflow::AttrValue::kAtListFloat: {
      ::oneflow::AttrValue_ListFloat of_proto_at_list_float;
      at_list_float().ToProto(&of_proto_at_list_float);
      proto_attrvalue->mutable_at_list_float()->CopyFrom(of_proto_at_list_float);
      break;
    }
    case ::oneflow::AttrValue::kAtListDataType: {
      ::oneflow::AttrValue_ListDataType of_proto_at_list_data_type;
      at_list_data_type().ToProto(&of_proto_at_list_data_type);
      proto_attrvalue->mutable_at_list_data_type()->CopyFrom(of_proto_at_list_data_type);
      break;
    }
    case ::oneflow::AttrValue::kAtListShape: {
      ::oneflow::AttrValue_ListShape of_proto_at_list_shape;
      at_list_shape().ToProto(&of_proto_at_list_shape);
      proto_attrvalue->mutable_at_list_shape()->CopyFrom(of_proto_at_list_shape);
      break;
    }
    case ::oneflow::AttrValue::kAtListString: {
      ::oneflow::AttrValue_ListString of_proto_at_list_string;
      at_list_string().ToProto(&of_proto_at_list_string);
      proto_attrvalue->mutable_at_list_string()->CopyFrom(of_proto_at_list_string);
      break;
    }
    case ::oneflow::AttrValue::VALUE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstAttrValue::_AttrValue_::DebugString() const {
  ::oneflow::AttrValue proto_attrvalue;
  this->ToProto(&proto_attrvalue);
  return proto_attrvalue.DebugString();
}

void ConstAttrValue::_AttrValue_::Clear() {
  clear_value();
}

void ConstAttrValue::_AttrValue_::CopyFrom(const _AttrValue_& other) {
  value_copy_from(other);
}


// oneof field value: at_int32
bool ConstAttrValue::_AttrValue_::has_at_int32() const {
  return value_case() == kAtInt32;
}
void ConstAttrValue::_AttrValue_::clear_at_int32() {
  if (has_at_int32()) {
    value_.at_int32_ = int32_t();
    value_case_ = VALUE_NOT_SET;
  }
}

const int32_t& ConstAttrValue::_AttrValue_::at_int32() const {
  if (has_at_int32()) {
      return value_.at_int32_;
    } else {
      static const int32_t default_static_value = int32_t();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_int32(const int32_t& value) {
  if(!has_at_int32()) {
    clear_value();
    }
  value_case_ = kAtInt32;
    value_.at_int32_ = value;
  }
int32_t* ConstAttrValue::_AttrValue_::mutable_at_int32() {
  if(!has_at_int32()) {
    clear_value();
    }
    value_case_ = kAtInt32;
  return  &value_.at_int32_;
  }

// oneof field value: at_int64
bool ConstAttrValue::_AttrValue_::has_at_int64() const {
  return value_case() == kAtInt64;
}
void ConstAttrValue::_AttrValue_::clear_at_int64() {
  if (has_at_int64()) {
    value_.at_int64_ = int64_t();
    value_case_ = VALUE_NOT_SET;
  }
}

const int64_t& ConstAttrValue::_AttrValue_::at_int64() const {
  if (has_at_int64()) {
      return value_.at_int64_;
    } else {
      static const int64_t default_static_value = int64_t();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_int64(const int64_t& value) {
  if(!has_at_int64()) {
    clear_value();
    }
  value_case_ = kAtInt64;
    value_.at_int64_ = value;
  }
int64_t* ConstAttrValue::_AttrValue_::mutable_at_int64() {
  if(!has_at_int64()) {
    clear_value();
    }
    value_case_ = kAtInt64;
  return  &value_.at_int64_;
  }

// oneof field value: at_bool
bool ConstAttrValue::_AttrValue_::has_at_bool() const {
  return value_case() == kAtBool;
}
void ConstAttrValue::_AttrValue_::clear_at_bool() {
  if (has_at_bool()) {
    value_.at_bool_ = bool();
    value_case_ = VALUE_NOT_SET;
  }
}

const bool& ConstAttrValue::_AttrValue_::at_bool() const {
  if (has_at_bool()) {
      return value_.at_bool_;
    } else {
      static const bool default_static_value = bool();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_bool(const bool& value) {
  if(!has_at_bool()) {
    clear_value();
    }
  value_case_ = kAtBool;
    value_.at_bool_ = value;
  }
bool* ConstAttrValue::_AttrValue_::mutable_at_bool() {
  if(!has_at_bool()) {
    clear_value();
    }
    value_case_ = kAtBool;
  return  &value_.at_bool_;
  }

// oneof field value: at_float
bool ConstAttrValue::_AttrValue_::has_at_float() const {
  return value_case() == kAtFloat;
}
void ConstAttrValue::_AttrValue_::clear_at_float() {
  if (has_at_float()) {
    value_.at_float_ = float();
    value_case_ = VALUE_NOT_SET;
  }
}

const float& ConstAttrValue::_AttrValue_::at_float() const {
  if (has_at_float()) {
      return value_.at_float_;
    } else {
      static const float default_static_value = float();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_float(const float& value) {
  if(!has_at_float()) {
    clear_value();
    }
  value_case_ = kAtFloat;
    value_.at_float_ = value;
  }
float* ConstAttrValue::_AttrValue_::mutable_at_float() {
  if(!has_at_float()) {
    clear_value();
    }
    value_case_ = kAtFloat;
  return  &value_.at_float_;
  }

// oneof field value: at_double
bool ConstAttrValue::_AttrValue_::has_at_double() const {
  return value_case() == kAtDouble;
}
void ConstAttrValue::_AttrValue_::clear_at_double() {
  if (has_at_double()) {
    value_.at_double_ = double();
    value_case_ = VALUE_NOT_SET;
  }
}

const double& ConstAttrValue::_AttrValue_::at_double() const {
  if (has_at_double()) {
      return value_.at_double_;
    } else {
      static const double default_static_value = double();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_double(const double& value) {
  if(!has_at_double()) {
    clear_value();
    }
  value_case_ = kAtDouble;
    value_.at_double_ = value;
  }
double* ConstAttrValue::_AttrValue_::mutable_at_double() {
  if(!has_at_double()) {
    clear_value();
    }
    value_case_ = kAtDouble;
  return  &value_.at_double_;
  }

// oneof field value: at_string
bool ConstAttrValue::_AttrValue_::has_at_string() const {
  return value_case() == kAtString;
}
void ConstAttrValue::_AttrValue_::clear_at_string() {
  if (has_at_string()) {
    {
      using String = ::std::string;
      String* ptr = reinterpret_cast<String*>(&(value_.at_string_)[0]);
      ptr->~String();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::std::string& ConstAttrValue::_AttrValue_::at_string() const {
  if (has_at_string()) {
      const ::std::string* ptr = reinterpret_cast<const ::std::string*>(&(value_.at_string_)[0]);
    return *ptr;
    } else {
      static const ::std::string default_static_value = ::std::string();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_string(const ::std::string& value) {
  if(!has_at_string()) {
    clear_value();
      new (&(value_.at_string_)) std::string();
    }
  value_case_ = kAtString;
    std::string* ptr = reinterpret_cast<std::string*>(&(value_.at_string_)[0]);
  *ptr = value;
  }
::std::string* ConstAttrValue::_AttrValue_::mutable_at_string() {
  if(!has_at_string()) {
    clear_value();
      new (&(value_.at_string_)) std::string();
    }
    ::std::string* ptr = reinterpret_cast<::std::string*>(&(value_.at_string_)[0]);
  return ptr;
  }

// oneof field value: at_shape
bool ConstAttrValue::_AttrValue_::has_at_shape() const {
  return value_case() == kAtShape;
}
void ConstAttrValue::_AttrValue_::clear_at_shape() {
  if (has_at_shape()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ShapeProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_shape_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::ShapeProto& ConstAttrValue::_AttrValue_::at_shape() const {
  if (has_at_shape()) {
      const ::std::shared_ptr<::oneflow::cfg::ShapeProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ShapeProto>*>(&(value_.at_shape_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value = ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
    }
}
::oneflow::cfg::ShapeProto* ConstAttrValue::_AttrValue_::mutable_at_shape() {
  if(!has_at_shape()) {
    clear_value();
    new (&(value_.at_shape_)) ::std::shared_ptr<::oneflow::cfg::ShapeProto>(new ::oneflow::cfg::ShapeProto());
  }
  value_case_ = kAtShape;
  ::std::shared_ptr<::oneflow::cfg::ShapeProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ShapeProto>*>(&(value_.at_shape_));
  return  (*ptr).get();
}

// oneof field value: at_data_type
bool ConstAttrValue::_AttrValue_::has_at_data_type() const {
  return value_case() == kAtDataType;
}
void ConstAttrValue::_AttrValue_::clear_at_data_type() {
  if (has_at_data_type()) {
    value_.at_data_type_ = DataType();
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::DataType& ConstAttrValue::_AttrValue_::at_data_type() const {
  if (has_at_data_type()) {
      return value_.at_data_type_;
    } else {
      static const ::oneflow::cfg::DataType default_static_value = ::oneflow::cfg::DataType();
    return default_static_value;
    }
}
void ConstAttrValue::_AttrValue_::set_at_data_type(const ::oneflow::cfg::DataType& value) {
  if(!has_at_data_type()) {
    clear_value();
    }
  value_case_ = kAtDataType;
    value_.at_data_type_ = value;
  }
::oneflow::cfg::DataType* ConstAttrValue::_AttrValue_::mutable_at_data_type() {
  if(!has_at_data_type()) {
    clear_value();
    }
    value_case_ = kAtDataType;
  return  &value_.at_data_type_;
  }

// oneof field value: at_list_int32
bool ConstAttrValue::_AttrValue_::has_at_list_int32() const {
  return value_case() == kAtListInt32;
}
void ConstAttrValue::_AttrValue_::clear_at_list_int32() {
  if (has_at_list_int32()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_int32_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListInt32& ConstAttrValue::_AttrValue_::at_list_int32() const {
  if (has_at_list_int32()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>*>(&(value_.at_list_int32_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListInt32>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListInt32* ConstAttrValue::_AttrValue_::mutable_at_list_int32() {
  if(!has_at_list_int32()) {
    clear_value();
    new (&(value_.at_list_int32_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>(new ::oneflow::cfg::AttrValue_ListInt32());
  }
  value_case_ = kAtListInt32;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>*>(&(value_.at_list_int32_));
  return  (*ptr).get();
}

// oneof field value: at_list_int64
bool ConstAttrValue::_AttrValue_::has_at_list_int64() const {
  return value_case() == kAtListInt64;
}
void ConstAttrValue::_AttrValue_::clear_at_list_int64() {
  if (has_at_list_int64()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_int64_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListInt64& ConstAttrValue::_AttrValue_::at_list_int64() const {
  if (has_at_list_int64()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>*>(&(value_.at_list_int64_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListInt64>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListInt64* ConstAttrValue::_AttrValue_::mutable_at_list_int64() {
  if(!has_at_list_int64()) {
    clear_value();
    new (&(value_.at_list_int64_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>(new ::oneflow::cfg::AttrValue_ListInt64());
  }
  value_case_ = kAtListInt64;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>*>(&(value_.at_list_int64_));
  return  (*ptr).get();
}

// oneof field value: at_list_float
bool ConstAttrValue::_AttrValue_::has_at_list_float() const {
  return value_case() == kAtListFloat;
}
void ConstAttrValue::_AttrValue_::clear_at_list_float() {
  if (has_at_list_float()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_float_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListFloat& ConstAttrValue::_AttrValue_::at_list_float() const {
  if (has_at_list_float()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>*>(&(value_.at_list_float_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListFloat>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListFloat* ConstAttrValue::_AttrValue_::mutable_at_list_float() {
  if(!has_at_list_float()) {
    clear_value();
    new (&(value_.at_list_float_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>(new ::oneflow::cfg::AttrValue_ListFloat());
  }
  value_case_ = kAtListFloat;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>*>(&(value_.at_list_float_));
  return  (*ptr).get();
}

// oneof field value: at_list_data_type
bool ConstAttrValue::_AttrValue_::has_at_list_data_type() const {
  return value_case() == kAtListDataType;
}
void ConstAttrValue::_AttrValue_::clear_at_list_data_type() {
  if (has_at_list_data_type()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_data_type_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListDataType& ConstAttrValue::_AttrValue_::at_list_data_type() const {
  if (has_at_list_data_type()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>*>(&(value_.at_list_data_type_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListDataType>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListDataType* ConstAttrValue::_AttrValue_::mutable_at_list_data_type() {
  if(!has_at_list_data_type()) {
    clear_value();
    new (&(value_.at_list_data_type_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>(new ::oneflow::cfg::AttrValue_ListDataType());
  }
  value_case_ = kAtListDataType;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>*>(&(value_.at_list_data_type_));
  return  (*ptr).get();
}

// oneof field value: at_list_shape
bool ConstAttrValue::_AttrValue_::has_at_list_shape() const {
  return value_case() == kAtListShape;
}
void ConstAttrValue::_AttrValue_::clear_at_list_shape() {
  if (has_at_list_shape()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_shape_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListShape& ConstAttrValue::_AttrValue_::at_list_shape() const {
  if (has_at_list_shape()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>*>(&(value_.at_list_shape_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListShape>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListShape* ConstAttrValue::_AttrValue_::mutable_at_list_shape() {
  if(!has_at_list_shape()) {
    clear_value();
    new (&(value_.at_list_shape_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>(new ::oneflow::cfg::AttrValue_ListShape());
  }
  value_case_ = kAtListShape;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>*>(&(value_.at_list_shape_));
  return  (*ptr).get();
}

// oneof field value: at_list_string
bool ConstAttrValue::_AttrValue_::has_at_list_string() const {
  return value_case() == kAtListString;
}
void ConstAttrValue::_AttrValue_::clear_at_list_string() {
  if (has_at_list_string()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_string_));
      ptr->~Shared_ptr();
    }
    value_case_ = VALUE_NOT_SET;
  }
}

const ::oneflow::cfg::AttrValue_ListString& ConstAttrValue::_AttrValue_::at_list_string() const {
  if (has_at_list_string()) {
      const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>*>(&(value_.at_list_string_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString> default_static_value = ::std::make_shared<::oneflow::cfg::AttrValue_ListString>();
    return *default_static_value;
    }
}
::oneflow::cfg::AttrValue_ListString* ConstAttrValue::_AttrValue_::mutable_at_list_string() {
  if(!has_at_list_string()) {
    clear_value();
    new (&(value_.at_list_string_)) ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>(new ::oneflow::cfg::AttrValue_ListString());
  }
  value_case_ = kAtListString;
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>*>(&(value_.at_list_string_));
  return  (*ptr).get();
}
ConstAttrValue::ValueCase ConstAttrValue::_AttrValue_::value_case() const {
  return value_case_;
}
bool ConstAttrValue::_AttrValue_::has_value() const {
  return value_case_ != VALUE_NOT_SET;
}
void ConstAttrValue::_AttrValue_::clear_value() {
  switch (value_case()) {
    case kAtInt32: {
      value_.at_int32_ = int32_t();
      break;
    }
    case kAtInt64: {
      value_.at_int64_ = int64_t();
      break;
    }
    case kAtBool: {
      value_.at_bool_ = bool();
      break;
    }
    case kAtFloat: {
      value_.at_float_ = float();
      break;
    }
    case kAtDouble: {
      value_.at_double_ = double();
      break;
    }
    case kAtString: {
      {
        using String = ::std::string;
        String* ptr = reinterpret_cast<String*>(&(value_.at_string_)[0]);
        ptr->~String();
      }
      break;
    }
    case kAtShape: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ShapeProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_shape_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtDataType: {
      value_.at_data_type_ = DataType();
      break;
    }
    case kAtListInt32: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_int32_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtListInt64: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_int64_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtListFloat: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_float_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtListDataType: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_data_type_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtListShape: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_shape_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kAtListString: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(value_.at_list_string_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
  value_case_ = VALUE_NOT_SET;
}
void ConstAttrValue::_AttrValue_::value_copy_from(const _AttrValue_& other) {
  switch (other.value_case()) {
    case kAtInt32: {
      set_at_int32(other.at_int32());
      break;
    }
    case kAtInt64: {
      set_at_int64(other.at_int64());
      break;
    }
    case kAtBool: {
      set_at_bool(other.at_bool());
      break;
    }
    case kAtFloat: {
      set_at_float(other.at_float());
      break;
    }
    case kAtDouble: {
      set_at_double(other.at_double());
      break;
    }
    case kAtString: {
      set_at_string(other.at_string());
      break;
    }
    case kAtShape: {
      mutable_at_shape()->CopyFrom(other.at_shape());
      break;
    }
    case kAtDataType: {
      set_at_data_type(other.at_data_type());
      break;
    }
    case kAtListInt32: {
      mutable_at_list_int32()->CopyFrom(other.at_list_int32());
      break;
    }
    case kAtListInt64: {
      mutable_at_list_int64()->CopyFrom(other.at_list_int64());
      break;
    }
    case kAtListFloat: {
      mutable_at_list_float()->CopyFrom(other.at_list_float());
      break;
    }
    case kAtListDataType: {
      mutable_at_list_data_type()->CopyFrom(other.at_list_data_type());
      break;
    }
    case kAtListShape: {
      mutable_at_list_shape()->CopyFrom(other.at_list_shape());
      break;
    }
    case kAtListString: {
      mutable_at_list_string()->CopyFrom(other.at_list_string());
      break;
    }
    case VALUE_NOT_SET: {
      clear_value();
    }
  }
}


int ConstAttrValue::_AttrValue_::compare(const _AttrValue_& other) {
  if (!(value_case() == other.value_case())) {
    return value_case() < other.value_case() ? -1 : 1;
  }
  switch (value_case()) {
    case kAtInt32: {
      if (!(at_int32() == other.at_int32())) {
        return at_int32() < other.at_int32() ? -1 : 1;
      }
      break;
    }
    case kAtInt64: {
      if (!(at_int64() == other.at_int64())) {
        return at_int64() < other.at_int64() ? -1 : 1;
      }
      break;
    }
    case kAtBool: {
      if (!(at_bool() == other.at_bool())) {
        return at_bool() < other.at_bool() ? -1 : 1;
      }
      break;
    }
    case kAtFloat: {
      if (!(at_float() == other.at_float())) {
        return at_float() < other.at_float() ? -1 : 1;
      }
      break;
    }
    case kAtDouble: {
      if (!(at_double() == other.at_double())) {
        return at_double() < other.at_double() ? -1 : 1;
      }
      break;
    }
    case kAtString: {
      if (!(at_string() == other.at_string())) {
        return at_string() < other.at_string() ? -1 : 1;
      }
      break;
    }
    case kAtShape: {
      if (!(at_shape() == other.at_shape())) {
        return at_shape() < other.at_shape() ? -1 : 1;
      }
      break;
    }
    case kAtDataType: {
      if (!(at_data_type() == other.at_data_type())) {
        return at_data_type() < other.at_data_type() ? -1 : 1;
      }
      break;
    }
    case kAtListInt32: {
      if (!(at_list_int32() == other.at_list_int32())) {
        return at_list_int32() < other.at_list_int32() ? -1 : 1;
      }
      break;
    }
    case kAtListInt64: {
      if (!(at_list_int64() == other.at_list_int64())) {
        return at_list_int64() < other.at_list_int64() ? -1 : 1;
      }
      break;
    }
    case kAtListFloat: {
      if (!(at_list_float() == other.at_list_float())) {
        return at_list_float() < other.at_list_float() ? -1 : 1;
      }
      break;
    }
    case kAtListDataType: {
      if (!(at_list_data_type() == other.at_list_data_type())) {
        return at_list_data_type() < other.at_list_data_type() ? -1 : 1;
      }
      break;
    }
    case kAtListShape: {
      if (!(at_list_shape() == other.at_list_shape())) {
        return at_list_shape() < other.at_list_shape() ? -1 : 1;
      }
      break;
    }
    case kAtListString: {
      if (!(at_list_string() == other.at_list_string())) {
        return at_list_string() < other.at_list_string() ? -1 : 1;
      }
      break;
    }
    case VALUE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstAttrValue::_AttrValue_::operator==(const _AttrValue_& other) const {
  return true
    && value_case() == other.value_case()
    && (value_case() == kAtInt32 ? 
      at_int32() == other.at_int32() : true)
    && (value_case() == kAtInt64 ? 
      at_int64() == other.at_int64() : true)
    && (value_case() == kAtBool ? 
      at_bool() == other.at_bool() : true)
    && (value_case() == kAtFloat ? 
      at_float() == other.at_float() : true)
    && (value_case() == kAtDouble ? 
      at_double() == other.at_double() : true)
    && (value_case() == kAtString ? 
      at_string() == other.at_string() : true)
    && (value_case() == kAtShape ? 
      at_shape() == other.at_shape() : true)
    && (value_case() == kAtDataType ? 
      at_data_type() == other.at_data_type() : true)
    && (value_case() == kAtListInt32 ? 
      at_list_int32() == other.at_list_int32() : true)
    && (value_case() == kAtListInt64 ? 
      at_list_int64() == other.at_list_int64() : true)
    && (value_case() == kAtListFloat ? 
      at_list_float() == other.at_list_float() : true)
    && (value_case() == kAtListDataType ? 
      at_list_data_type() == other.at_list_data_type() : true)
    && (value_case() == kAtListShape ? 
      at_list_shape() == other.at_list_shape() : true)
    && (value_case() == kAtListString ? 
      at_list_string() == other.at_list_string() : true)
  ;
}

std::size_t ConstAttrValue::_AttrValue_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(value_case())
    ^ (has_at_int32() ? std::hash<int32_t>()(at_int32()) : 0)
    ^ (has_at_int64() ? std::hash<int64_t>()(at_int64()) : 0)
    ^ (has_at_bool() ? std::hash<bool>()(at_bool()) : 0)
    ^ (has_at_float() ? std::hash<float>()(at_float()) : 0)
    ^ (has_at_double() ? std::hash<double>()(at_double()) : 0)
    ^ (has_at_string() ? std::hash<::std::string>()(at_string()) : 0)
    ^ (has_at_shape() ? std::hash<::oneflow::cfg::ShapeProto>()(at_shape()) : 0)
    ^ (has_at_data_type() ? std::hash<::oneflow::cfg::DataType>()(at_data_type()) : 0)
    ^ (has_at_list_int32() ? std::hash<::oneflow::cfg::AttrValue_ListInt32>()(at_list_int32()) : 0)
    ^ (has_at_list_int64() ? std::hash<::oneflow::cfg::AttrValue_ListInt64>()(at_list_int64()) : 0)
    ^ (has_at_list_float() ? std::hash<::oneflow::cfg::AttrValue_ListFloat>()(at_list_float()) : 0)
    ^ (has_at_list_data_type() ? std::hash<::oneflow::cfg::AttrValue_ListDataType>()(at_list_data_type()) : 0)
    ^ (has_at_list_shape() ? std::hash<::oneflow::cfg::AttrValue_ListShape>()(at_list_shape()) : 0)
    ^ (has_at_list_string() ? std::hash<::oneflow::cfg::AttrValue_ListString>()(at_list_string()) : 0)
  ;
}

bool ConstAttrValue::_AttrValue_::operator<(const _AttrValue_& other) const {
  return false
    || !(value_case() == other.value_case()) ? 
      value_case() < other.value_case() : false
    || ((value_case() == kAtInt32) && 
      !(at_int32() == other.at_int32())) ? 
        at_int32() < other.at_int32() : false
    || ((value_case() == kAtInt64) && 
      !(at_int64() == other.at_int64())) ? 
        at_int64() < other.at_int64() : false
    || ((value_case() == kAtBool) && 
      !(at_bool() == other.at_bool())) ? 
        at_bool() < other.at_bool() : false
    || ((value_case() == kAtFloat) && 
      !(at_float() == other.at_float())) ? 
        at_float() < other.at_float() : false
    || ((value_case() == kAtDouble) && 
      !(at_double() == other.at_double())) ? 
        at_double() < other.at_double() : false
    || ((value_case() == kAtString) && 
      !(at_string() == other.at_string())) ? 
        at_string() < other.at_string() : false
    || ((value_case() == kAtShape) && 
      !(at_shape() == other.at_shape())) ? 
        at_shape() < other.at_shape() : false
    || ((value_case() == kAtDataType) && 
      !(at_data_type() == other.at_data_type())) ? 
        at_data_type() < other.at_data_type() : false
    || ((value_case() == kAtListInt32) && 
      !(at_list_int32() == other.at_list_int32())) ? 
        at_list_int32() < other.at_list_int32() : false
    || ((value_case() == kAtListInt64) && 
      !(at_list_int64() == other.at_list_int64())) ? 
        at_list_int64() < other.at_list_int64() : false
    || ((value_case() == kAtListFloat) && 
      !(at_list_float() == other.at_list_float())) ? 
        at_list_float() < other.at_list_float() : false
    || ((value_case() == kAtListDataType) && 
      !(at_list_data_type() == other.at_list_data_type())) ? 
        at_list_data_type() < other.at_list_data_type() : false
    || ((value_case() == kAtListShape) && 
      !(at_list_shape() == other.at_list_shape())) ? 
        at_list_shape() < other.at_list_shape() : false
    || ((value_case() == kAtListString) && 
      !(at_list_string() == other.at_list_string())) ? 
        at_list_string() < other.at_list_string() : false
  ;
}

using _AttrValue_ =  ConstAttrValue::_AttrValue_;
ConstAttrValue::ConstAttrValue(const ::std::shared_ptr<_AttrValue_>& data): data_(data) {}
ConstAttrValue::ConstAttrValue(): data_(::std::make_shared<_AttrValue_>()) {}
ConstAttrValue::ConstAttrValue(const ::oneflow::AttrValue& proto_attrvalue) {
  BuildFromProto(proto_attrvalue);
}
ConstAttrValue::ConstAttrValue(const ConstAttrValue&) = default;
ConstAttrValue::ConstAttrValue(ConstAttrValue&&) noexcept = default;
ConstAttrValue::~ConstAttrValue() = default;

void ConstAttrValue::ToProto(PbMessage* proto_attrvalue) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrValue*>(proto_attrvalue));
}
  
::std::string ConstAttrValue::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrValue::__Empty__() const {
  return !data_;
}

int ConstAttrValue::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"at_int32", 1},
    {"at_int64", 2},
    {"at_bool", 3},
    {"at_float", 4},
    {"at_double", 5},
    {"at_string", 6},
    {"at_shape", 7},
    {"at_data_type", 8},
    {"at_list_int32", 9},
    {"at_list_int64", 10},
    {"at_list_float", 11},
    {"at_list_data_type", 12},
    {"at_list_shape", 13},
    {"at_list_string", 14},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrValue::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrValue::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int32_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(float),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(double),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    case 9: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListInt32),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListInt32),
      };
      return type_indices;
    }
    case 10: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListInt64),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListInt64),
      };
      return type_indices;
    }
    case 11: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListFloat),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListFloat),
      };
      return type_indices;
    }
    case 12: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListDataType),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListDataType),
      };
      return type_indices;
    }
    case 13: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListShape),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListShape),
      };
      return type_indices;
    }
    case 14: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue_ListString),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue_ListString),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrValue::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &at_int32();
    case 2: return &at_int64();
    case 3: return &at_bool();
    case 4: return &at_float();
    case 5: return &at_double();
    case 6: return &at_string();
    case 7: return &at_shape();
    case 8: return &at_data_type();
    case 9: return &at_list_int32();
    case 10: return &at_list_int64();
    case 11: return &at_list_float();
    case 12: return &at_list_data_type();
    case 13: return &at_list_shape();
    case 14: return &at_list_string();
    default: return nullptr;
  }
}

 // oneof field value: at_int32
bool ConstAttrValue::has_at_int32() const {
  return __SharedPtrOrDefault__()->has_at_int32();
}
const int32_t& ConstAttrValue::at_int32() const {
  return __SharedPtrOrDefault__()->at_int32();
}

// used by pybind11 only
 // oneof field value: at_int64
bool ConstAttrValue::has_at_int64() const {
  return __SharedPtrOrDefault__()->has_at_int64();
}
const int64_t& ConstAttrValue::at_int64() const {
  return __SharedPtrOrDefault__()->at_int64();
}

// used by pybind11 only
 // oneof field value: at_bool
bool ConstAttrValue::has_at_bool() const {
  return __SharedPtrOrDefault__()->has_at_bool();
}
const bool& ConstAttrValue::at_bool() const {
  return __SharedPtrOrDefault__()->at_bool();
}

// used by pybind11 only
 // oneof field value: at_float
bool ConstAttrValue::has_at_float() const {
  return __SharedPtrOrDefault__()->has_at_float();
}
const float& ConstAttrValue::at_float() const {
  return __SharedPtrOrDefault__()->at_float();
}

// used by pybind11 only
 // oneof field value: at_double
bool ConstAttrValue::has_at_double() const {
  return __SharedPtrOrDefault__()->has_at_double();
}
const double& ConstAttrValue::at_double() const {
  return __SharedPtrOrDefault__()->at_double();
}

// used by pybind11 only
 // oneof field value: at_string
bool ConstAttrValue::has_at_string() const {
  return __SharedPtrOrDefault__()->has_at_string();
}
const ::std::string& ConstAttrValue::at_string() const {
  return __SharedPtrOrDefault__()->at_string();
}

// used by pybind11 only
 // oneof field value: at_shape
bool ConstAttrValue::has_at_shape() const {
  return __SharedPtrOrDefault__()->has_at_shape();
}
const ::oneflow::cfg::ShapeProto& ConstAttrValue::at_shape() const {
  return __SharedPtrOrDefault__()->at_shape();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstAttrValue::shared_const_at_shape() const {
  return at_shape().__SharedConst__();
}
 // oneof field value: at_data_type
bool ConstAttrValue::has_at_data_type() const {
  return __SharedPtrOrDefault__()->has_at_data_type();
}
const ::oneflow::cfg::DataType& ConstAttrValue::at_data_type() const {
  return __SharedPtrOrDefault__()->at_data_type();
}

// used by pybind11 only
 // oneof field value: at_list_int32
bool ConstAttrValue::has_at_list_int32() const {
  return __SharedPtrOrDefault__()->has_at_list_int32();
}
const ::oneflow::cfg::AttrValue_ListInt32& ConstAttrValue::at_list_int32() const {
  return __SharedPtrOrDefault__()->at_list_int32();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListInt32> ConstAttrValue::shared_const_at_list_int32() const {
  return at_list_int32().__SharedConst__();
}
 // oneof field value: at_list_int64
bool ConstAttrValue::has_at_list_int64() const {
  return __SharedPtrOrDefault__()->has_at_list_int64();
}
const ::oneflow::cfg::AttrValue_ListInt64& ConstAttrValue::at_list_int64() const {
  return __SharedPtrOrDefault__()->at_list_int64();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListInt64> ConstAttrValue::shared_const_at_list_int64() const {
  return at_list_int64().__SharedConst__();
}
 // oneof field value: at_list_float
bool ConstAttrValue::has_at_list_float() const {
  return __SharedPtrOrDefault__()->has_at_list_float();
}
const ::oneflow::cfg::AttrValue_ListFloat& ConstAttrValue::at_list_float() const {
  return __SharedPtrOrDefault__()->at_list_float();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListFloat> ConstAttrValue::shared_const_at_list_float() const {
  return at_list_float().__SharedConst__();
}
 // oneof field value: at_list_data_type
bool ConstAttrValue::has_at_list_data_type() const {
  return __SharedPtrOrDefault__()->has_at_list_data_type();
}
const ::oneflow::cfg::AttrValue_ListDataType& ConstAttrValue::at_list_data_type() const {
  return __SharedPtrOrDefault__()->at_list_data_type();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListDataType> ConstAttrValue::shared_const_at_list_data_type() const {
  return at_list_data_type().__SharedConst__();
}
 // oneof field value: at_list_shape
bool ConstAttrValue::has_at_list_shape() const {
  return __SharedPtrOrDefault__()->has_at_list_shape();
}
const ::oneflow::cfg::AttrValue_ListShape& ConstAttrValue::at_list_shape() const {
  return __SharedPtrOrDefault__()->at_list_shape();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListShape> ConstAttrValue::shared_const_at_list_shape() const {
  return at_list_shape().__SharedConst__();
}
 // oneof field value: at_list_string
bool ConstAttrValue::has_at_list_string() const {
  return __SharedPtrOrDefault__()->has_at_list_string();
}
const ::oneflow::cfg::AttrValue_ListString& ConstAttrValue::at_list_string() const {
  return __SharedPtrOrDefault__()->at_list_string();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListString> ConstAttrValue::shared_const_at_list_string() const {
  return at_list_string().__SharedConst__();
}
ConstAttrValue::ValueCase ConstAttrValue::value_case() const {
  return __SharedPtrOrDefault__()->value_case();
}

bool ConstAttrValue::has_value() const {
  return __SharedPtrOrDefault__()->has_value();
}

::std::shared_ptr<ConstAttrValue> ConstAttrValue::__SharedConst__() const {
  return ::std::make_shared<ConstAttrValue>(data_);
}
int64_t ConstAttrValue::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrValue::operator==(const ConstAttrValue& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrValue::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrValue::operator<(const ConstAttrValue& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrValue_>& ConstAttrValue::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrValue_> default_ptr = std::make_shared<_AttrValue_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrValue_>& ConstAttrValue::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrValue_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrValue
void ConstAttrValue::BuildFromProto(const PbMessage& proto_attrvalue) {
  data_ = ::std::make_shared<_AttrValue_>(dynamic_cast<const ::oneflow::AttrValue&>(proto_attrvalue));
}

AttrValue::AttrValue(const ::std::shared_ptr<_AttrValue_>& data)
  : ConstAttrValue(data) {}
AttrValue::AttrValue(const AttrValue& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrValue> resize
AttrValue::AttrValue(AttrValue&&) noexcept = default; 
AttrValue::AttrValue(const ::oneflow::AttrValue& proto_attrvalue) {
  InitFromProto(proto_attrvalue);
}
AttrValue::AttrValue() = default;

AttrValue::~AttrValue() = default;

void AttrValue::InitFromProto(const PbMessage& proto_attrvalue) {
  BuildFromProto(proto_attrvalue);
}
  
void* AttrValue::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_at_int32();
    case 2: return mutable_at_int64();
    case 3: return mutable_at_bool();
    case 4: return mutable_at_float();
    case 5: return mutable_at_double();
    case 6: return mutable_at_string();
    case 7: return mutable_at_shape();
    case 8: return mutable_at_data_type();
    case 9: return mutable_at_list_int32();
    case 10: return mutable_at_list_int64();
    case 11: return mutable_at_list_float();
    case 12: return mutable_at_list_data_type();
    case 13: return mutable_at_list_shape();
    case 14: return mutable_at_list_string();
    default: return nullptr;
  }
}

bool AttrValue::operator==(const AttrValue& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrValue::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrValue::operator<(const AttrValue& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrValue::Clear() {
  if (data_) { data_.reset(); }
}
void AttrValue::CopyFrom(const AttrValue& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrValue& AttrValue::operator=(const AttrValue& other) {
  CopyFrom(other);
  return *this;
}

void AttrValue::clear_at_int32() {
  return __SharedPtr__()->clear_at_int32();
}
void AttrValue::set_at_int32(const int32_t& value) {
  return __SharedPtr__()->set_at_int32(value);
}
int32_t* AttrValue::mutable_at_int32() {
  return  __SharedPtr__()->mutable_at_int32();
}
void AttrValue::clear_at_int64() {
  return __SharedPtr__()->clear_at_int64();
}
void AttrValue::set_at_int64(const int64_t& value) {
  return __SharedPtr__()->set_at_int64(value);
}
int64_t* AttrValue::mutable_at_int64() {
  return  __SharedPtr__()->mutable_at_int64();
}
void AttrValue::clear_at_bool() {
  return __SharedPtr__()->clear_at_bool();
}
void AttrValue::set_at_bool(const bool& value) {
  return __SharedPtr__()->set_at_bool(value);
}
bool* AttrValue::mutable_at_bool() {
  return  __SharedPtr__()->mutable_at_bool();
}
void AttrValue::clear_at_float() {
  return __SharedPtr__()->clear_at_float();
}
void AttrValue::set_at_float(const float& value) {
  return __SharedPtr__()->set_at_float(value);
}
float* AttrValue::mutable_at_float() {
  return  __SharedPtr__()->mutable_at_float();
}
void AttrValue::clear_at_double() {
  return __SharedPtr__()->clear_at_double();
}
void AttrValue::set_at_double(const double& value) {
  return __SharedPtr__()->set_at_double(value);
}
double* AttrValue::mutable_at_double() {
  return  __SharedPtr__()->mutable_at_double();
}
void AttrValue::clear_at_string() {
  return __SharedPtr__()->clear_at_string();
}
void AttrValue::set_at_string(const ::std::string& value) {
  return __SharedPtr__()->set_at_string(value);
}
::std::string* AttrValue::mutable_at_string() {
  return  __SharedPtr__()->mutable_at_string();
}
void AttrValue::clear_at_shape() {
  return __SharedPtr__()->clear_at_shape();
}
::oneflow::cfg::ShapeProto* AttrValue::mutable_at_shape() {
  return __SharedPtr__()->mutable_at_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> AttrValue::shared_mutable_at_shape() {
  return mutable_at_shape()->__SharedMutable__();
}
void AttrValue::clear_at_data_type() {
  return __SharedPtr__()->clear_at_data_type();
}
void AttrValue::set_at_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_at_data_type(value);
}
::oneflow::cfg::DataType* AttrValue::mutable_at_data_type() {
  return  __SharedPtr__()->mutable_at_data_type();
}
void AttrValue::clear_at_list_int32() {
  return __SharedPtr__()->clear_at_list_int32();
}
::oneflow::cfg::AttrValue_ListInt32* AttrValue::mutable_at_list_int32() {
  return __SharedPtr__()->mutable_at_list_int32();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32> AttrValue::shared_mutable_at_list_int32() {
  return mutable_at_list_int32()->__SharedMutable__();
}
void AttrValue::clear_at_list_int64() {
  return __SharedPtr__()->clear_at_list_int64();
}
::oneflow::cfg::AttrValue_ListInt64* AttrValue::mutable_at_list_int64() {
  return __SharedPtr__()->mutable_at_list_int64();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64> AttrValue::shared_mutable_at_list_int64() {
  return mutable_at_list_int64()->__SharedMutable__();
}
void AttrValue::clear_at_list_float() {
  return __SharedPtr__()->clear_at_list_float();
}
::oneflow::cfg::AttrValue_ListFloat* AttrValue::mutable_at_list_float() {
  return __SharedPtr__()->mutable_at_list_float();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat> AttrValue::shared_mutable_at_list_float() {
  return mutable_at_list_float()->__SharedMutable__();
}
void AttrValue::clear_at_list_data_type() {
  return __SharedPtr__()->clear_at_list_data_type();
}
::oneflow::cfg::AttrValue_ListDataType* AttrValue::mutable_at_list_data_type() {
  return __SharedPtr__()->mutable_at_list_data_type();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType> AttrValue::shared_mutable_at_list_data_type() {
  return mutable_at_list_data_type()->__SharedMutable__();
}
void AttrValue::clear_at_list_shape() {
  return __SharedPtr__()->clear_at_list_shape();
}
::oneflow::cfg::AttrValue_ListShape* AttrValue::mutable_at_list_shape() {
  return __SharedPtr__()->mutable_at_list_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape> AttrValue::shared_mutable_at_list_shape() {
  return mutable_at_list_shape()->__SharedMutable__();
}
void AttrValue::clear_at_list_string() {
  return __SharedPtr__()->clear_at_list_string();
}
::oneflow::cfg::AttrValue_ListString* AttrValue::mutable_at_list_string() {
  return __SharedPtr__()->mutable_at_list_string();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue_ListString> AttrValue::shared_mutable_at_list_string() {
  return mutable_at_list_string()->__SharedMutable__();
}

::std::shared_ptr<AttrValue> AttrValue::__SharedMutable__() {
  return ::std::make_shared<AttrValue>(__SharedPtr__());
}
ConstAttrDef::_AttrDef_::_AttrDef_() { Clear(); }
ConstAttrDef::_AttrDef_::_AttrDef_(const _AttrDef_& other) { CopyFrom(other); }
ConstAttrDef::_AttrDef_::_AttrDef_(const ::oneflow::AttrDef& proto_attrdef) {
  InitFromProto(proto_attrdef);
}
ConstAttrDef::_AttrDef_::_AttrDef_(_AttrDef_&& other) = default;
ConstAttrDef::_AttrDef_::~_AttrDef_() = default;

void ConstAttrDef::_AttrDef_::InitFromProto(const ::oneflow::AttrDef& proto_attrdef) {
  Clear();
  // required_or_optional field: name
  if (proto_attrdef.has_name()) {
    set_name(proto_attrdef.name());
  }
  // required_or_optional field: description
  if (proto_attrdef.has_description()) {
    set_description(proto_attrdef.description());
  }
  // required_or_optional field: default_val
  if (proto_attrdef.has_default_val()) {
  *mutable_default_val() = ::oneflow::cfg::AttrValue(proto_attrdef.default_val());      
  }
    
}

void ConstAttrDef::_AttrDef_::ToProto(::oneflow::AttrDef* proto_attrdef) const {
  proto_attrdef->Clear();
  // required_or_optional field: name
  if (this->has_name()) {
    proto_attrdef->set_name(name());
    }
  // required_or_optional field: description
  if (this->has_description()) {
    proto_attrdef->set_description(description());
    }
  // required_or_optional field: default_val
  if (this->has_default_val()) {
    ::oneflow::AttrValue proto_default_val;
    default_val().ToProto(&proto_default_val);
    proto_attrdef->mutable_default_val()->CopyFrom(proto_default_val);
    }

}

::std::string ConstAttrDef::_AttrDef_::DebugString() const {
  ::oneflow::AttrDef proto_attrdef;
  this->ToProto(&proto_attrdef);
  return proto_attrdef.DebugString();
}

void ConstAttrDef::_AttrDef_::Clear() {
  clear_name();
  clear_description();
  clear_default_val();
}

void ConstAttrDef::_AttrDef_::CopyFrom(const _AttrDef_& other) {
  if (other.has_name()) {
    set_name(other.name());
  } else {
    clear_name();
  }
  if (other.has_description()) {
    set_description(other.description());
  } else {
    clear_description();
  }
  if (other.has_default_val()) {
    mutable_default_val()->CopyFrom(other.default_val());
  } else {
    clear_default_val();
  }
}


// optional field name
bool ConstAttrDef::_AttrDef_::has_name() const {
  return has_name_;
}
const ::std::string& ConstAttrDef::_AttrDef_::name() const {
  if (has_name_) { return name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstAttrDef::_AttrDef_::clear_name() {
  has_name_ = false;
}
void ConstAttrDef::_AttrDef_::set_name(const ::std::string& value) {
  name_ = value;
  has_name_ = true;
}
::std::string* ConstAttrDef::_AttrDef_::mutable_name() {
  has_name_ = true;
  return &name_;
}

// optional field description
bool ConstAttrDef::_AttrDef_::has_description() const {
  return has_description_;
}
const ::std::string& ConstAttrDef::_AttrDef_::description() const {
  if (has_description_) { return description_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstAttrDef::_AttrDef_::clear_description() {
  has_description_ = false;
}
void ConstAttrDef::_AttrDef_::set_description(const ::std::string& value) {
  description_ = value;
  has_description_ = true;
}
::std::string* ConstAttrDef::_AttrDef_::mutable_description() {
  has_description_ = true;
  return &description_;
}

// optional field default_val
bool ConstAttrDef::_AttrDef_::has_default_val() const {
  return has_default_val_;
}
const ::oneflow::cfg::AttrValue& ConstAttrDef::_AttrDef_::default_val() const {
  if (!default_val_) {
    static const ::std::shared_ptr<::oneflow::cfg::AttrValue> default_static_value =
      ::std::make_shared<::oneflow::cfg::AttrValue>();
    return *default_static_value;
  }
  return *(default_val_.get());
}
void ConstAttrDef::_AttrDef_::clear_default_val() {
  if (default_val_) {
    default_val_->Clear();
  }
  has_default_val_ = false;
}
::oneflow::cfg::AttrValue* ConstAttrDef::_AttrDef_::mutable_default_val() {
  if (!default_val_) {
    default_val_ = ::std::make_shared<::oneflow::cfg::AttrValue>();
  }
  has_default_val_ = true;
  return default_val_.get();
}


int ConstAttrDef::_AttrDef_::compare(const _AttrDef_& other) {
  if (!(has_name() == other.has_name())) {
    return has_name() < other.has_name() ? -1 : 1;
  } else if (!(name() == other.name())) {
    return name() < other.name() ? -1 : 1;
  }
  if (!(has_description() == other.has_description())) {
    return has_description() < other.has_description() ? -1 : 1;
  } else if (!(description() == other.description())) {
    return description() < other.description() ? -1 : 1;
  }
  if (!(has_default_val() == other.has_default_val())) {
    return has_default_val() < other.has_default_val() ? -1 : 1;
  } else if (!(default_val() == other.default_val())) {
    return default_val() < other.default_val() ? -1 : 1;
  }
  return 0;
}

bool ConstAttrDef::_AttrDef_::operator==(const _AttrDef_& other) const {
  return true
    && has_name() == other.has_name() 
    && name() == other.name()
    && has_description() == other.has_description() 
    && description() == other.description()
    && has_default_val() == other.has_default_val() 
    && default_val() == other.default_val()
  ;
}

std::size_t ConstAttrDef::_AttrDef_::__CalcHash__() const {
  return 0
    ^ (has_name() ? std::hash<::std::string>()(name()) : 0)
    ^ (has_description() ? std::hash<::std::string>()(description()) : 0)
    ^ (has_default_val() ? std::hash<::oneflow::cfg::AttrValue>()(default_val()) : 0)
  ;
}

bool ConstAttrDef::_AttrDef_::operator<(const _AttrDef_& other) const {
  return false
    || !(has_name() == other.has_name()) ? 
      has_name() < other.has_name() : false
    || !(name() == other.name()) ? 
      name() < other.name() : false
    || !(has_description() == other.has_description()) ? 
      has_description() < other.has_description() : false
    || !(description() == other.description()) ? 
      description() < other.description() : false
    || !(has_default_val() == other.has_default_val()) ? 
      has_default_val() < other.has_default_val() : false
    || !(default_val() == other.default_val()) ? 
      default_val() < other.default_val() : false
  ;
}

using _AttrDef_ =  ConstAttrDef::_AttrDef_;
ConstAttrDef::ConstAttrDef(const ::std::shared_ptr<_AttrDef_>& data): data_(data) {}
ConstAttrDef::ConstAttrDef(): data_(::std::make_shared<_AttrDef_>()) {}
ConstAttrDef::ConstAttrDef(const ::oneflow::AttrDef& proto_attrdef) {
  BuildFromProto(proto_attrdef);
}
ConstAttrDef::ConstAttrDef(const ConstAttrDef&) = default;
ConstAttrDef::ConstAttrDef(ConstAttrDef&&) noexcept = default;
ConstAttrDef::~ConstAttrDef() = default;

void ConstAttrDef::ToProto(PbMessage* proto_attrdef) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::AttrDef*>(proto_attrdef));
}
  
::std::string ConstAttrDef::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstAttrDef::__Empty__() const {
  return !data_;
}

int ConstAttrDef::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"name", 1},
    {"description", 2},
    {"default_val", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstAttrDef::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstAttrDef::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::AttrValue),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstAttrValue),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstAttrDef::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &name();
    case 2: return &description();
    case 3: return &default_val();
    default: return nullptr;
  }
}

// required or optional field name
bool ConstAttrDef::has_name() const {
  return __SharedPtrOrDefault__()->has_name();
}
const ::std::string& ConstAttrDef::name() const {
  return __SharedPtrOrDefault__()->name();
}
// used by pybind11 only
// required or optional field description
bool ConstAttrDef::has_description() const {
  return __SharedPtrOrDefault__()->has_description();
}
const ::std::string& ConstAttrDef::description() const {
  return __SharedPtrOrDefault__()->description();
}
// used by pybind11 only
// required or optional field default_val
bool ConstAttrDef::has_default_val() const {
  return __SharedPtrOrDefault__()->has_default_val();
}
const ::oneflow::cfg::AttrValue& ConstAttrDef::default_val() const {
  return __SharedPtrOrDefault__()->default_val();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstAttrValue> ConstAttrDef::shared_const_default_val() const {
  return default_val().__SharedConst__();
}

::std::shared_ptr<ConstAttrDef> ConstAttrDef::__SharedConst__() const {
  return ::std::make_shared<ConstAttrDef>(data_);
}
int64_t ConstAttrDef::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstAttrDef::operator==(const ConstAttrDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstAttrDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstAttrDef::operator<(const ConstAttrDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_AttrDef_>& ConstAttrDef::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_AttrDef_> default_ptr = std::make_shared<_AttrDef_>();
  return default_ptr;
}
const ::std::shared_ptr<_AttrDef_>& ConstAttrDef::__SharedPtr__() {
  if (!data_) { data_.reset(new _AttrDef_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstAttrDef
void ConstAttrDef::BuildFromProto(const PbMessage& proto_attrdef) {
  data_ = ::std::make_shared<_AttrDef_>(dynamic_cast<const ::oneflow::AttrDef&>(proto_attrdef));
}

AttrDef::AttrDef(const ::std::shared_ptr<_AttrDef_>& data)
  : ConstAttrDef(data) {}
AttrDef::AttrDef(const AttrDef& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<AttrDef> resize
AttrDef::AttrDef(AttrDef&&) noexcept = default; 
AttrDef::AttrDef(const ::oneflow::AttrDef& proto_attrdef) {
  InitFromProto(proto_attrdef);
}
AttrDef::AttrDef() = default;

AttrDef::~AttrDef() = default;

void AttrDef::InitFromProto(const PbMessage& proto_attrdef) {
  BuildFromProto(proto_attrdef);
}
  
void* AttrDef::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_name();
    case 2: return mutable_description();
    case 3: return mutable_default_val();
    default: return nullptr;
  }
}

bool AttrDef::operator==(const AttrDef& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t AttrDef::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool AttrDef::operator<(const AttrDef& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void AttrDef::Clear() {
  if (data_) { data_.reset(); }
}
void AttrDef::CopyFrom(const AttrDef& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
AttrDef& AttrDef::operator=(const AttrDef& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field name
void AttrDef::clear_name() {
  return __SharedPtr__()->clear_name();
}
void AttrDef::set_name(const ::std::string& value) {
  return __SharedPtr__()->set_name(value);
}
::std::string* AttrDef::mutable_name() {
  return  __SharedPtr__()->mutable_name();
}
// required or optional field description
void AttrDef::clear_description() {
  return __SharedPtr__()->clear_description();
}
void AttrDef::set_description(const ::std::string& value) {
  return __SharedPtr__()->set_description(value);
}
::std::string* AttrDef::mutable_description() {
  return  __SharedPtr__()->mutable_description();
}
// required or optional field default_val
void AttrDef::clear_default_val() {
  return __SharedPtr__()->clear_default_val();
}
::oneflow::cfg::AttrValue* AttrDef::mutable_default_val() {
  return __SharedPtr__()->mutable_default_val();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::AttrValue> AttrDef::shared_mutable_default_val() {
  return mutable_default_val()->__SharedMutable__();
}

::std::shared_ptr<AttrDef> AttrDef::__SharedMutable__() {
  return ::std::make_shared<AttrDef>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): ::oneflow::cfg::_RepeatedField_<int32_t>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): ::oneflow::cfg::_RepeatedField_<int64_t>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data): ::oneflow::cfg::_RepeatedField_<float>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<float>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) {
  ::oneflow::cfg::_RepeatedField_<float>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) {
  ::oneflow::cfg::_RepeatedField_<float>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<float>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::DataType>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::DataType>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::DataType>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::DataType>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::DataType>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::DataType>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::DataType>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeProto>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeProto>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ShapeProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeProto>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeProto>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeProto>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::ShapeProto>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::ShapeProto> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::ShapeProto> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

}
} // namespace oneflow
