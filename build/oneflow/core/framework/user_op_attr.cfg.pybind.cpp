#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.framework.user_op_attr", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::AttrType> enm(m, "AttrType");
    enm.value("kAtInt32", ::oneflow::cfg::kAtInt32);
    enm.value("kAtInt64", ::oneflow::cfg::kAtInt64);
    enm.value("kAtBool", ::oneflow::cfg::kAtBool);
    enm.value("kAtFloat", ::oneflow::cfg::kAtFloat);
    enm.value("kAtDouble", ::oneflow::cfg::kAtDouble);
    enm.value("kAtString", ::oneflow::cfg::kAtString);
    enm.value("kAtShape", ::oneflow::cfg::kAtShape);
    enm.value("kAtDataType", ::oneflow::cfg::kAtDataType);
    enm.value("kAtListInt32", ::oneflow::cfg::kAtListInt32);
    enm.value("kAtListInt64", ::oneflow::cfg::kAtListInt64);
    enm.value("kAtListFloat", ::oneflow::cfg::kAtListFloat);
    enm.value("kAtListDataType", ::oneflow::cfg::kAtListDataType);
    enm.value("kAtListShape", ::oneflow::cfg::kAtListShape);
    enm.value("kAtListString", ::oneflow::cfg::kAtListString);
    m.attr("kAtInt32") = enm.attr("kAtInt32");
    m.attr("kAtInt64") = enm.attr("kAtInt64");
    m.attr("kAtBool") = enm.attr("kAtBool");
    m.attr("kAtFloat") = enm.attr("kAtFloat");
    m.attr("kAtDouble") = enm.attr("kAtDouble");
    m.attr("kAtString") = enm.attr("kAtString");
    m.attr("kAtShape") = enm.attr("kAtShape");
    m.attr("kAtDataType") = enm.attr("kAtDataType");
    m.attr("kAtListInt32") = enm.attr("kAtListInt32");
    m.attr("kAtListInt64") = enm.attr("kAtListInt64");
    m.attr("kAtListFloat") = enm.attr("kAtListFloat");
    m.attr("kAtListDataType") = enm.attr("kAtListDataType");
    m.attr("kAtListShape") = enm.attr("kAtListShape");
    m.attr("kAtListString") = enm.attr("kAtListString");
  }


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::*)(const int32_t&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::*)(const int64_t&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::*)(const float&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::*)(const ::oneflow::cfg::DataType&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstShapeProto> (Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstShapeProto> (Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(const ::oneflow::cfg::ShapeProto&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::ShapeProto> (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::ShapeProto> (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<ConstAttrValue_ListInt32, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListInt32>> registry(m, "ConstAttrValue_ListInt32");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListInt32::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListInt32::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListInt32::DebugString);

    registry.def("val_size", &ConstAttrValue_ListInt32::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> (ConstAttrValue_ListInt32::*)() const)&ConstAttrValue_ListInt32::shared_const_val);
    registry.def("val", (const int32_t& (ConstAttrValue_ListInt32::*)(::std::size_t) const)&ConstAttrValue_ListInt32::val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListInt32, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>> registry(m, "AttrValue_ListInt32");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListInt32::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListInt32::*)(const ConstAttrValue_ListInt32&))&::oneflow::cfg::AttrValue_ListInt32::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListInt32::*)(const ::oneflow::cfg::AttrValue_ListInt32&))&::oneflow::cfg::AttrValue_ListInt32::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListInt32::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListInt32::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListInt32::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListInt32::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListInt32::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::AttrValue_ListInt32::*)())&::oneflow::cfg::AttrValue_ListInt32::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::AttrValue_ListInt32::*)() const)&::oneflow::cfg::AttrValue_ListInt32::shared_const_val);
    registry.def("val", (const int32_t& (::oneflow::cfg::AttrValue_ListInt32::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListInt32::val);
    registry.def("add_val", &::oneflow::cfg::AttrValue_ListInt32::add_val);
    registry.def("set_val", &::oneflow::cfg::AttrValue_ListInt32::set_val);
  }
  {
    pybind11::class_<ConstAttrValue_ListInt64, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListInt64>> registry(m, "ConstAttrValue_ListInt64");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListInt64::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListInt64::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListInt64::DebugString);

    registry.def("val_size", &ConstAttrValue_ListInt64::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> (ConstAttrValue_ListInt64::*)() const)&ConstAttrValue_ListInt64::shared_const_val);
    registry.def("val", (const int64_t& (ConstAttrValue_ListInt64::*)(::std::size_t) const)&ConstAttrValue_ListInt64::val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListInt64, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>> registry(m, "AttrValue_ListInt64");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListInt64::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListInt64::*)(const ConstAttrValue_ListInt64&))&::oneflow::cfg::AttrValue_ListInt64::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListInt64::*)(const ::oneflow::cfg::AttrValue_ListInt64&))&::oneflow::cfg::AttrValue_ListInt64::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListInt64::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListInt64::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListInt64::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListInt64::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListInt64::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::AttrValue_ListInt64::*)())&::oneflow::cfg::AttrValue_ListInt64::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> (::oneflow::cfg::AttrValue_ListInt64::*)() const)&::oneflow::cfg::AttrValue_ListInt64::shared_const_val);
    registry.def("val", (const int64_t& (::oneflow::cfg::AttrValue_ListInt64::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListInt64::val);
    registry.def("add_val", &::oneflow::cfg::AttrValue_ListInt64::add_val);
    registry.def("set_val", &::oneflow::cfg::AttrValue_ListInt64::set_val);
  }
  {
    pybind11::class_<ConstAttrValue_ListFloat, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListFloat>> registry(m, "ConstAttrValue_ListFloat");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListFloat::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListFloat::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListFloat::DebugString);

    registry.def("val_size", &ConstAttrValue_ListFloat::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> (ConstAttrValue_ListFloat::*)() const)&ConstAttrValue_ListFloat::shared_const_val);
    registry.def("val", (const float& (ConstAttrValue_ListFloat::*)(::std::size_t) const)&ConstAttrValue_ListFloat::val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListFloat, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>> registry(m, "AttrValue_ListFloat");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListFloat::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListFloat::*)(const ConstAttrValue_ListFloat&))&::oneflow::cfg::AttrValue_ListFloat::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListFloat::*)(const ::oneflow::cfg::AttrValue_ListFloat&))&::oneflow::cfg::AttrValue_ListFloat::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListFloat::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListFloat::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListFloat::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListFloat::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListFloat::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> (::oneflow::cfg::AttrValue_ListFloat::*)())&::oneflow::cfg::AttrValue_ListFloat::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> (::oneflow::cfg::AttrValue_ListFloat::*)() const)&::oneflow::cfg::AttrValue_ListFloat::shared_const_val);
    registry.def("val", (const float& (::oneflow::cfg::AttrValue_ListFloat::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListFloat::val);
    registry.def("add_val", &::oneflow::cfg::AttrValue_ListFloat::add_val);
    registry.def("set_val", &::oneflow::cfg::AttrValue_ListFloat::set_val);
  }
  {
    pybind11::class_<ConstAttrValue_ListDataType, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListDataType>> registry(m, "ConstAttrValue_ListDataType");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListDataType::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListDataType::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListDataType::DebugString);

    registry.def("val_size", &ConstAttrValue_ListDataType::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> (ConstAttrValue_ListDataType::*)() const)&ConstAttrValue_ListDataType::shared_const_val);
    registry.def("val", (const ::oneflow::cfg::DataType& (ConstAttrValue_ListDataType::*)(::std::size_t) const)&ConstAttrValue_ListDataType::val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListDataType, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>> registry(m, "AttrValue_ListDataType");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListDataType::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListDataType::*)(const ConstAttrValue_ListDataType&))&::oneflow::cfg::AttrValue_ListDataType::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListDataType::*)(const ::oneflow::cfg::AttrValue_ListDataType&))&::oneflow::cfg::AttrValue_ListDataType::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListDataType::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListDataType::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListDataType::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListDataType::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListDataType::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> (::oneflow::cfg::AttrValue_ListDataType::*)())&::oneflow::cfg::AttrValue_ListDataType::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> (::oneflow::cfg::AttrValue_ListDataType::*)() const)&::oneflow::cfg::AttrValue_ListDataType::shared_const_val);
    registry.def("val", (const ::oneflow::cfg::DataType& (::oneflow::cfg::AttrValue_ListDataType::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListDataType::val);
    registry.def("add_val", &::oneflow::cfg::AttrValue_ListDataType::add_val);
    registry.def("set_val", &::oneflow::cfg::AttrValue_ListDataType::set_val);
  }
  {
    pybind11::class_<ConstAttrValue_ListShape, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListShape>> registry(m, "ConstAttrValue_ListShape");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListShape::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListShape::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListShape::DebugString);

    registry.def("val_size", &ConstAttrValue_ListShape::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> (ConstAttrValue_ListShape::*)() const)&ConstAttrValue_ListShape::shared_const_val);
    registry.def("val", (::std::shared_ptr<ConstShapeProto> (ConstAttrValue_ListShape::*)(::std::size_t) const)&ConstAttrValue_ListShape::shared_const_val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListShape, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>> registry(m, "AttrValue_ListShape");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListShape::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListShape::*)(const ConstAttrValue_ListShape&))&::oneflow::cfg::AttrValue_ListShape::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListShape::*)(const ::oneflow::cfg::AttrValue_ListShape&))&::oneflow::cfg::AttrValue_ListShape::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListShape::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListShape::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListShape::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListShape::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListShape::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> (::oneflow::cfg::AttrValue_ListShape::*)())&::oneflow::cfg::AttrValue_ListShape::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> (::oneflow::cfg::AttrValue_ListShape::*)() const)&::oneflow::cfg::AttrValue_ListShape::shared_const_val);
    registry.def("val", (::std::shared_ptr<ConstShapeProto> (::oneflow::cfg::AttrValue_ListShape::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListShape::shared_const_val);
    registry.def("mutable_val", (::std::shared_ptr<::oneflow::cfg::ShapeProto> (::oneflow::cfg::AttrValue_ListShape::*)(::std::size_t))&::oneflow::cfg::AttrValue_ListShape::shared_mutable_val);
  }
  {
    pybind11::class_<ConstAttrValue_ListString, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue_ListString>> registry(m, "ConstAttrValue_ListString");
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListString::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue_ListString::DebugString);
    registry.def("__repr__", &ConstAttrValue_ListString::DebugString);

    registry.def("val_size", &ConstAttrValue_ListString::val_size);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> (ConstAttrValue_ListString::*)() const)&ConstAttrValue_ListString::shared_const_val);
    registry.def("val", (const ::std::string& (ConstAttrValue_ListString::*)(::std::size_t) const)&ConstAttrValue_ListString::val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue_ListString, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue_ListString>> registry(m, "AttrValue_ListString");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue_ListString::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListString::*)(const ConstAttrValue_ListString&))&::oneflow::cfg::AttrValue_ListString::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue_ListString::*)(const ::oneflow::cfg::AttrValue_ListString&))&::oneflow::cfg::AttrValue_ListString::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue_ListString::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue_ListString::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue_ListString::DebugString);



    registry.def("val_size", &::oneflow::cfg::AttrValue_ListString::val_size);
    registry.def("clear_val", &::oneflow::cfg::AttrValue_ListString::clear_val);
    registry.def("mutable_val", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::AttrValue_ListString::*)())&::oneflow::cfg::AttrValue_ListString::shared_mutable_val);
    registry.def("val", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::AttrValue_ListString::*)() const)&::oneflow::cfg::AttrValue_ListString::shared_const_val);
    registry.def("val", (const ::std::string& (::oneflow::cfg::AttrValue_ListString::*)(::std::size_t) const)&::oneflow::cfg::AttrValue_ListString::val);
    registry.def("add_val", &::oneflow::cfg::AttrValue_ListString::add_val);
    registry.def("set_val", &::oneflow::cfg::AttrValue_ListString::set_val);
  }
  {
    pybind11::class_<ConstAttrValue, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrValue>> registry(m, "ConstAttrValue");
    registry.def("__id__", &::oneflow::cfg::AttrValue::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrValue::DebugString);
    registry.def("__repr__", &ConstAttrValue::DebugString);

    registry.def("has_at_int32", &ConstAttrValue::has_at_int32);
    registry.def("at_int32", &ConstAttrValue::at_int32);

    registry.def("has_at_int64", &ConstAttrValue::has_at_int64);
    registry.def("at_int64", &ConstAttrValue::at_int64);

    registry.def("has_at_bool", &ConstAttrValue::has_at_bool);
    registry.def("at_bool", &ConstAttrValue::at_bool);

    registry.def("has_at_float", &ConstAttrValue::has_at_float);
    registry.def("at_float", &ConstAttrValue::at_float);

    registry.def("has_at_double", &ConstAttrValue::has_at_double);
    registry.def("at_double", &ConstAttrValue::at_double);

    registry.def("has_at_string", &ConstAttrValue::has_at_string);
    registry.def("at_string", &ConstAttrValue::at_string);

    registry.def("has_at_shape", &ConstAttrValue::has_at_shape);
    registry.def("at_shape", &ConstAttrValue::shared_const_at_shape);

    registry.def("has_at_data_type", &ConstAttrValue::has_at_data_type);
    registry.def("at_data_type", &ConstAttrValue::at_data_type);

    registry.def("has_at_list_int32", &ConstAttrValue::has_at_list_int32);
    registry.def("at_list_int32", &ConstAttrValue::shared_const_at_list_int32);

    registry.def("has_at_list_int64", &ConstAttrValue::has_at_list_int64);
    registry.def("at_list_int64", &ConstAttrValue::shared_const_at_list_int64);

    registry.def("has_at_list_float", &ConstAttrValue::has_at_list_float);
    registry.def("at_list_float", &ConstAttrValue::shared_const_at_list_float);

    registry.def("has_at_list_data_type", &ConstAttrValue::has_at_list_data_type);
    registry.def("at_list_data_type", &ConstAttrValue::shared_const_at_list_data_type);

    registry.def("has_at_list_shape", &ConstAttrValue::has_at_list_shape);
    registry.def("at_list_shape", &ConstAttrValue::shared_const_at_list_shape);

    registry.def("has_at_list_string", &ConstAttrValue::has_at_list_string);
    registry.def("at_list_string", &ConstAttrValue::shared_const_at_list_string);
    registry.def("value_case",  &ConstAttrValue::value_case);
    registry.def_property_readonly_static("VALUE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::VALUE_NOT_SET; })
        .def_property_readonly_static("kAtInt32", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt32; })
        .def_property_readonly_static("kAtInt64", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt64; })
        .def_property_readonly_static("kAtBool", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtBool; })
        .def_property_readonly_static("kAtFloat", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtFloat; })
        .def_property_readonly_static("kAtDouble", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDouble; })
        .def_property_readonly_static("kAtString", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtString; })
        .def_property_readonly_static("kAtShape", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtShape; })
        .def_property_readonly_static("kAtDataType", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDataType; })
        .def_property_readonly_static("kAtListInt32", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt32; })
        .def_property_readonly_static("kAtListInt64", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt64; })
        .def_property_readonly_static("kAtListFloat", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListFloat; })
        .def_property_readonly_static("kAtListDataType", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListDataType; })
        .def_property_readonly_static("kAtListShape", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListShape; })
        .def_property_readonly_static("kAtListString", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListString; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::AttrValue, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrValue>> registry(m, "AttrValue");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrValue::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue::*)(const ConstAttrValue&))&::oneflow::cfg::AttrValue::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrValue::*)(const ::oneflow::cfg::AttrValue&))&::oneflow::cfg::AttrValue::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrValue::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrValue::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrValue::DebugString);

    registry.def_property_readonly_static("VALUE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::VALUE_NOT_SET; })
        .def_property_readonly_static("kAtInt32", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt32; })
        .def_property_readonly_static("kAtInt64", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt64; })
        .def_property_readonly_static("kAtBool", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtBool; })
        .def_property_readonly_static("kAtFloat", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtFloat; })
        .def_property_readonly_static("kAtDouble", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDouble; })
        .def_property_readonly_static("kAtString", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtString; })
        .def_property_readonly_static("kAtShape", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtShape; })
        .def_property_readonly_static("kAtDataType", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDataType; })
        .def_property_readonly_static("kAtListInt32", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt32; })
        .def_property_readonly_static("kAtListInt64", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt64; })
        .def_property_readonly_static("kAtListFloat", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListFloat; })
        .def_property_readonly_static("kAtListDataType", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListDataType; })
        .def_property_readonly_static("kAtListShape", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListShape; })
        .def_property_readonly_static("kAtListString", [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListString; })
        ;


    registry.def("has_at_int32", &::oneflow::cfg::AttrValue::has_at_int32);
    registry.def("clear_at_int32", &::oneflow::cfg::AttrValue::clear_at_int32);
    registry.def_property_readonly_static("kAtInt32",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt32; });
    registry.def("at_int32", &::oneflow::cfg::AttrValue::at_int32);
    registry.def("set_at_int32", &::oneflow::cfg::AttrValue::set_at_int32);

    registry.def("has_at_int64", &::oneflow::cfg::AttrValue::has_at_int64);
    registry.def("clear_at_int64", &::oneflow::cfg::AttrValue::clear_at_int64);
    registry.def_property_readonly_static("kAtInt64",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtInt64; });
    registry.def("at_int64", &::oneflow::cfg::AttrValue::at_int64);
    registry.def("set_at_int64", &::oneflow::cfg::AttrValue::set_at_int64);

    registry.def("has_at_bool", &::oneflow::cfg::AttrValue::has_at_bool);
    registry.def("clear_at_bool", &::oneflow::cfg::AttrValue::clear_at_bool);
    registry.def_property_readonly_static("kAtBool",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtBool; });
    registry.def("at_bool", &::oneflow::cfg::AttrValue::at_bool);
    registry.def("set_at_bool", &::oneflow::cfg::AttrValue::set_at_bool);

    registry.def("has_at_float", &::oneflow::cfg::AttrValue::has_at_float);
    registry.def("clear_at_float", &::oneflow::cfg::AttrValue::clear_at_float);
    registry.def_property_readonly_static("kAtFloat",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtFloat; });
    registry.def("at_float", &::oneflow::cfg::AttrValue::at_float);
    registry.def("set_at_float", &::oneflow::cfg::AttrValue::set_at_float);

    registry.def("has_at_double", &::oneflow::cfg::AttrValue::has_at_double);
    registry.def("clear_at_double", &::oneflow::cfg::AttrValue::clear_at_double);
    registry.def_property_readonly_static("kAtDouble",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDouble; });
    registry.def("at_double", &::oneflow::cfg::AttrValue::at_double);
    registry.def("set_at_double", &::oneflow::cfg::AttrValue::set_at_double);

    registry.def("has_at_string", &::oneflow::cfg::AttrValue::has_at_string);
    registry.def("clear_at_string", &::oneflow::cfg::AttrValue::clear_at_string);
    registry.def_property_readonly_static("kAtString",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtString; });
    registry.def("at_string", &::oneflow::cfg::AttrValue::at_string);
    registry.def("set_at_string", &::oneflow::cfg::AttrValue::set_at_string);

    registry.def("has_at_shape", &::oneflow::cfg::AttrValue::has_at_shape);
    registry.def("clear_at_shape", &::oneflow::cfg::AttrValue::clear_at_shape);
    registry.def_property_readonly_static("kAtShape",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtShape; });
    registry.def("at_shape", &::oneflow::cfg::AttrValue::at_shape);
    registry.def("mutable_at_shape", &::oneflow::cfg::AttrValue::shared_mutable_at_shape);

    registry.def("has_at_data_type", &::oneflow::cfg::AttrValue::has_at_data_type);
    registry.def("clear_at_data_type", &::oneflow::cfg::AttrValue::clear_at_data_type);
    registry.def_property_readonly_static("kAtDataType",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtDataType; });
    registry.def("at_data_type", &::oneflow::cfg::AttrValue::at_data_type);
    registry.def("set_at_data_type", &::oneflow::cfg::AttrValue::set_at_data_type);

    registry.def("has_at_list_int32", &::oneflow::cfg::AttrValue::has_at_list_int32);
    registry.def("clear_at_list_int32", &::oneflow::cfg::AttrValue::clear_at_list_int32);
    registry.def_property_readonly_static("kAtListInt32",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt32; });
    registry.def("at_list_int32", &::oneflow::cfg::AttrValue::at_list_int32);
    registry.def("mutable_at_list_int32", &::oneflow::cfg::AttrValue::shared_mutable_at_list_int32);

    registry.def("has_at_list_int64", &::oneflow::cfg::AttrValue::has_at_list_int64);
    registry.def("clear_at_list_int64", &::oneflow::cfg::AttrValue::clear_at_list_int64);
    registry.def_property_readonly_static("kAtListInt64",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListInt64; });
    registry.def("at_list_int64", &::oneflow::cfg::AttrValue::at_list_int64);
    registry.def("mutable_at_list_int64", &::oneflow::cfg::AttrValue::shared_mutable_at_list_int64);

    registry.def("has_at_list_float", &::oneflow::cfg::AttrValue::has_at_list_float);
    registry.def("clear_at_list_float", &::oneflow::cfg::AttrValue::clear_at_list_float);
    registry.def_property_readonly_static("kAtListFloat",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListFloat; });
    registry.def("at_list_float", &::oneflow::cfg::AttrValue::at_list_float);
    registry.def("mutable_at_list_float", &::oneflow::cfg::AttrValue::shared_mutable_at_list_float);

    registry.def("has_at_list_data_type", &::oneflow::cfg::AttrValue::has_at_list_data_type);
    registry.def("clear_at_list_data_type", &::oneflow::cfg::AttrValue::clear_at_list_data_type);
    registry.def_property_readonly_static("kAtListDataType",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListDataType; });
    registry.def("at_list_data_type", &::oneflow::cfg::AttrValue::at_list_data_type);
    registry.def("mutable_at_list_data_type", &::oneflow::cfg::AttrValue::shared_mutable_at_list_data_type);

    registry.def("has_at_list_shape", &::oneflow::cfg::AttrValue::has_at_list_shape);
    registry.def("clear_at_list_shape", &::oneflow::cfg::AttrValue::clear_at_list_shape);
    registry.def_property_readonly_static("kAtListShape",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListShape; });
    registry.def("at_list_shape", &::oneflow::cfg::AttrValue::at_list_shape);
    registry.def("mutable_at_list_shape", &::oneflow::cfg::AttrValue::shared_mutable_at_list_shape);

    registry.def("has_at_list_string", &::oneflow::cfg::AttrValue::has_at_list_string);
    registry.def("clear_at_list_string", &::oneflow::cfg::AttrValue::clear_at_list_string);
    registry.def_property_readonly_static("kAtListString",
        [](const pybind11::object&){ return ::oneflow::cfg::AttrValue::kAtListString; });
    registry.def("at_list_string", &::oneflow::cfg::AttrValue::at_list_string);
    registry.def("mutable_at_list_string", &::oneflow::cfg::AttrValue::shared_mutable_at_list_string);
    pybind11::enum_<::oneflow::cfg::AttrValue::ValueCase>(registry, "ValueCase")
        .value("VALUE_NOT_SET", ::oneflow::cfg::AttrValue::VALUE_NOT_SET)
        .value("kAtInt32", ::oneflow::cfg::AttrValue::kAtInt32)
        .value("kAtInt64", ::oneflow::cfg::AttrValue::kAtInt64)
        .value("kAtBool", ::oneflow::cfg::AttrValue::kAtBool)
        .value("kAtFloat", ::oneflow::cfg::AttrValue::kAtFloat)
        .value("kAtDouble", ::oneflow::cfg::AttrValue::kAtDouble)
        .value("kAtString", ::oneflow::cfg::AttrValue::kAtString)
        .value("kAtShape", ::oneflow::cfg::AttrValue::kAtShape)
        .value("kAtDataType", ::oneflow::cfg::AttrValue::kAtDataType)
        .value("kAtListInt32", ::oneflow::cfg::AttrValue::kAtListInt32)
        .value("kAtListInt64", ::oneflow::cfg::AttrValue::kAtListInt64)
        .value("kAtListFloat", ::oneflow::cfg::AttrValue::kAtListFloat)
        .value("kAtListDataType", ::oneflow::cfg::AttrValue::kAtListDataType)
        .value("kAtListShape", ::oneflow::cfg::AttrValue::kAtListShape)
        .value("kAtListString", ::oneflow::cfg::AttrValue::kAtListString)
        ;
    registry.def("value_case",  &::oneflow::cfg::AttrValue::value_case);
  }
  {
    pybind11::class_<ConstAttrDef, ::oneflow::cfg::Message, std::shared_ptr<ConstAttrDef>> registry(m, "ConstAttrDef");
    registry.def("__id__", &::oneflow::cfg::AttrDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAttrDef::DebugString);
    registry.def("__repr__", &ConstAttrDef::DebugString);

    registry.def("has_name", &ConstAttrDef::has_name);
    registry.def("name", &ConstAttrDef::name);

    registry.def("has_description", &ConstAttrDef::has_description);
    registry.def("description", &ConstAttrDef::description);

    registry.def("has_default_val", &ConstAttrDef::has_default_val);
    registry.def("default_val", &ConstAttrDef::shared_const_default_val);
  }
  {
    pybind11::class_<::oneflow::cfg::AttrDef, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AttrDef>> registry(m, "AttrDef");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AttrDef::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrDef::*)(const ConstAttrDef&))&::oneflow::cfg::AttrDef::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AttrDef::*)(const ::oneflow::cfg::AttrDef&))&::oneflow::cfg::AttrDef::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AttrDef::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AttrDef::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AttrDef::DebugString);



    registry.def("has_name", &::oneflow::cfg::AttrDef::has_name);
    registry.def("clear_name", &::oneflow::cfg::AttrDef::clear_name);
    registry.def("name", &::oneflow::cfg::AttrDef::name);
    registry.def("set_name", &::oneflow::cfg::AttrDef::set_name);

    registry.def("has_description", &::oneflow::cfg::AttrDef::has_description);
    registry.def("clear_description", &::oneflow::cfg::AttrDef::clear_description);
    registry.def("description", &::oneflow::cfg::AttrDef::description);
    registry.def("set_description", &::oneflow::cfg::AttrDef::set_description);

    registry.def("has_default_val", &::oneflow::cfg::AttrDef::has_default_val);
    registry.def("clear_default_val", &::oneflow::cfg::AttrDef::clear_default_val);
    registry.def("default_val", &::oneflow::cfg::AttrDef::shared_const_default_val);
    registry.def("mutable_default_val", &::oneflow::cfg::AttrDef::shared_mutable_default_val);
  }
}