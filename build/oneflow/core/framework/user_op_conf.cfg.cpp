#include "oneflow/core/framework/user_op_conf.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"
#include "oneflow/core/framework/user_op_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstUserOpConf_ListString::_UserOpConf_ListString_::_UserOpConf_ListString_() { Clear(); }
ConstUserOpConf_ListString::_UserOpConf_ListString_::_UserOpConf_ListString_(const _UserOpConf_ListString_& other) { CopyFrom(other); }
ConstUserOpConf_ListString::_UserOpConf_ListString_::_UserOpConf_ListString_(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring) {
  InitFromProto(proto_useropconf_liststring);
}
ConstUserOpConf_ListString::_UserOpConf_ListString_::_UserOpConf_ListString_(_UserOpConf_ListString_&& other) = default;
ConstUserOpConf_ListString::_UserOpConf_ListString_::~_UserOpConf_ListString_() = default;

void ConstUserOpConf_ListString::_UserOpConf_ListString_::InitFromProto(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring) {
  Clear();
  // repeated field: s
  if (!proto_useropconf_liststring.s().empty()) {
    for (const ::std::string& elem : proto_useropconf_liststring.s()) {
      add_s(elem);
    }
  }
    
}

void ConstUserOpConf_ListString::_UserOpConf_ListString_::ToProto(::oneflow::UserOpConf_ListString* proto_useropconf_liststring) const {
  proto_useropconf_liststring->Clear();
  // repeated field: s
  if (!s().empty()) {
    for (const ::std::string& elem : s()) {
      proto_useropconf_liststring->add_s(elem);
    }
  }

}

::std::string ConstUserOpConf_ListString::_UserOpConf_ListString_::DebugString() const {
  ::oneflow::UserOpConf_ListString proto_useropconf_liststring;
  this->ToProto(&proto_useropconf_liststring);
  return proto_useropconf_liststring.DebugString();
}

void ConstUserOpConf_ListString::_UserOpConf_ListString_::Clear() {
  clear_s();
}

void ConstUserOpConf_ListString::_UserOpConf_ListString_::CopyFrom(const _UserOpConf_ListString_& other) {
  mutable_s()->CopyFrom(other.s());
}


// repeated field s
::std::size_t ConstUserOpConf_ListString::_UserOpConf_ListString_::s_size() const {
  if (!s_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return s_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& ConstUserOpConf_ListString::_UserOpConf_ListString_::s() const {
  if (!s_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(s_.get());
}
const ::std::string& ConstUserOpConf_ListString::_UserOpConf_ListString_::s(::std::size_t index) const {
  if (!s_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return s_->Get(index);
}
void ConstUserOpConf_ListString::_UserOpConf_ListString_::clear_s() {
  if (!s_) {
    s_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return s_->Clear();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_* ConstUserOpConf_ListString::_UserOpConf_ListString_::mutable_s() {
  if (!s_) {
    s_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  s_.get();
}
::std::string* ConstUserOpConf_ListString::_UserOpConf_ListString_::mutable_s(::std::size_t index) {
  if (!s_) {
    s_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return  s_->Mutable(index);
}
void ConstUserOpConf_ListString::_UserOpConf_ListString_::add_s(const ::std::string& value) {
  if (!s_) {
    s_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return s_->Add(value);
}
void ConstUserOpConf_ListString::_UserOpConf_ListString_::set_s(::std::size_t index, const ::std::string& value) {
  if (!s_) {
    s_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>();
  }
  return s_->Set(index, value);
}


int ConstUserOpConf_ListString::_UserOpConf_ListString_::compare(const _UserOpConf_ListString_& other) {
  if (!(s() == other.s())) {
    return s() < other.s() ? -1 : 1;
  }
  return 0;
}

bool ConstUserOpConf_ListString::_UserOpConf_ListString_::operator==(const _UserOpConf_ListString_& other) const {
  return true
    && s() == other.s()
  ;
}

std::size_t ConstUserOpConf_ListString::_UserOpConf_ListString_::__CalcHash__() const {
  return 0
    ^ s().__CalcHash__()
  ;
}

bool ConstUserOpConf_ListString::_UserOpConf_ListString_::operator<(const _UserOpConf_ListString_& other) const {
  return false
    || !(s() == other.s()) ? 
      s() < other.s() : false
  ;
}

using _UserOpConf_ListString_ =  ConstUserOpConf_ListString::_UserOpConf_ListString_;
ConstUserOpConf_ListString::ConstUserOpConf_ListString(const ::std::shared_ptr<_UserOpConf_ListString_>& data): data_(data) {}
ConstUserOpConf_ListString::ConstUserOpConf_ListString(): data_(::std::make_shared<_UserOpConf_ListString_>()) {}
ConstUserOpConf_ListString::ConstUserOpConf_ListString(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring) {
  BuildFromProto(proto_useropconf_liststring);
}
ConstUserOpConf_ListString::ConstUserOpConf_ListString(const ConstUserOpConf_ListString&) = default;
ConstUserOpConf_ListString::ConstUserOpConf_ListString(ConstUserOpConf_ListString&&) noexcept = default;
ConstUserOpConf_ListString::~ConstUserOpConf_ListString() = default;

void ConstUserOpConf_ListString::ToProto(PbMessage* proto_useropconf_liststring) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::UserOpConf_ListString*>(proto_useropconf_liststring));
}
  
::std::string ConstUserOpConf_ListString::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstUserOpConf_ListString::__Empty__() const {
  return !data_;
}

int ConstUserOpConf_ListString::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"s", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstUserOpConf_ListString::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstUserOpConf_ListString::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstUserOpConf_ListString::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &s();
    default: return nullptr;
  }
}

// repeated field s
::std::size_t ConstUserOpConf_ListString::s_size() const {
  return __SharedPtrOrDefault__()->s_size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& ConstUserOpConf_ListString::s() const {
  return __SharedPtrOrDefault__()->s();
}
const ::std::string& ConstUserOpConf_ListString::s(::std::size_t index) const {
  return __SharedPtrOrDefault__()->s(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> ConstUserOpConf_ListString::shared_const_s() const {
  return s().__SharedConst__();
}

::std::shared_ptr<ConstUserOpConf_ListString> ConstUserOpConf_ListString::__SharedConst__() const {
  return ::std::make_shared<ConstUserOpConf_ListString>(data_);
}
int64_t ConstUserOpConf_ListString::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstUserOpConf_ListString::operator==(const ConstUserOpConf_ListString& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstUserOpConf_ListString::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstUserOpConf_ListString::operator<(const ConstUserOpConf_ListString& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_UserOpConf_ListString_>& ConstUserOpConf_ListString::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_UserOpConf_ListString_> default_ptr = std::make_shared<_UserOpConf_ListString_>();
  return default_ptr;
}
const ::std::shared_ptr<_UserOpConf_ListString_>& ConstUserOpConf_ListString::__SharedPtr__() {
  if (!data_) { data_.reset(new _UserOpConf_ListString_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstUserOpConf_ListString
void ConstUserOpConf_ListString::BuildFromProto(const PbMessage& proto_useropconf_liststring) {
  data_ = ::std::make_shared<_UserOpConf_ListString_>(dynamic_cast<const ::oneflow::UserOpConf_ListString&>(proto_useropconf_liststring));
}

UserOpConf_ListString::UserOpConf_ListString(const ::std::shared_ptr<_UserOpConf_ListString_>& data)
  : ConstUserOpConf_ListString(data) {}
UserOpConf_ListString::UserOpConf_ListString(const UserOpConf_ListString& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<UserOpConf_ListString> resize
UserOpConf_ListString::UserOpConf_ListString(UserOpConf_ListString&&) noexcept = default; 
UserOpConf_ListString::UserOpConf_ListString(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring) {
  InitFromProto(proto_useropconf_liststring);
}
UserOpConf_ListString::UserOpConf_ListString() = default;

UserOpConf_ListString::~UserOpConf_ListString() = default;

void UserOpConf_ListString::InitFromProto(const PbMessage& proto_useropconf_liststring) {
  BuildFromProto(proto_useropconf_liststring);
}
  
void* UserOpConf_ListString::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_s();
    default: return nullptr;
  }
}

bool UserOpConf_ListString::operator==(const UserOpConf_ListString& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t UserOpConf_ListString::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool UserOpConf_ListString::operator<(const UserOpConf_ListString& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void UserOpConf_ListString::Clear() {
  if (data_) { data_.reset(); }
}
void UserOpConf_ListString::CopyFrom(const UserOpConf_ListString& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
UserOpConf_ListString& UserOpConf_ListString::operator=(const UserOpConf_ListString& other) {
  CopyFrom(other);
  return *this;
}

// repeated field s
void UserOpConf_ListString::clear_s() {
  return __SharedPtr__()->clear_s();
}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_* UserOpConf_ListString::mutable_s() {
  return __SharedPtr__()->mutable_s();
}
::std::string* UserOpConf_ListString::mutable_s(::std::size_t index) {
  return __SharedPtr__()->mutable_s(index);
}
void UserOpConf_ListString::add_s(const ::std::string& value) {
  return __SharedPtr__()->add_s(value);
}
void UserOpConf_ListString::set_s(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_s(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> UserOpConf_ListString::shared_mutable_s() {
  return mutable_s()->__SharedMutable__();
}

::std::shared_ptr<UserOpConf_ListString> UserOpConf_ListString::__SharedMutable__() {
  return ::std::make_shared<UserOpConf_ListString>(__SharedPtr__());
}
ConstUserOpConf::_UserOpConf_::_UserOpConf_() { Clear(); }
ConstUserOpConf::_UserOpConf_::_UserOpConf_(const _UserOpConf_& other) { CopyFrom(other); }
ConstUserOpConf::_UserOpConf_::_UserOpConf_(const ::oneflow::UserOpConf& proto_useropconf) {
  InitFromProto(proto_useropconf);
}
ConstUserOpConf::_UserOpConf_::_UserOpConf_(_UserOpConf_&& other) = default;
ConstUserOpConf::_UserOpConf_::~_UserOpConf_() = default;

void ConstUserOpConf::_UserOpConf_::InitFromProto(const ::oneflow::UserOpConf& proto_useropconf) {
  Clear();
  // required_or_optional field: op_type_name
  if (proto_useropconf.has_op_type_name()) {
    set_op_type_name(proto_useropconf.op_type_name());
  }
  // map field : input
  if (!proto_useropconf.input().empty()) {
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_&  mut_input = *mutable_input();
    for (const auto& pair : proto_useropconf.input()) {
      mut_input[pair.first] = ::oneflow::cfg::UserOpConf_ListString(pair.second);
    }
  }
  // map field : output
  if (!proto_useropconf.output().empty()) {
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_&  mut_output = *mutable_output();
    for (const auto& pair : proto_useropconf.output()) {
      mut_output[pair.first] = ::oneflow::cfg::UserOpConf_ListString(pair.second);
    }
  }
  // map field : attr
  if (!proto_useropconf.attr().empty()) {
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_&  mut_attr = *mutable_attr();
    for (const auto& pair : proto_useropconf.attr()) {
      mut_attr[pair.first] = ::oneflow::cfg::AttrValue(pair.second);
    }
  }
    
}

void ConstUserOpConf::_UserOpConf_::ToProto(::oneflow::UserOpConf* proto_useropconf) const {
  proto_useropconf->Clear();
  // required_or_optional field: op_type_name
  if (this->has_op_type_name()) {
    proto_useropconf->set_op_type_name(op_type_name());
    }
  // map field : input
  if (!input().empty()) {
    auto& mut_input = *(proto_useropconf->mutable_input());
    for (const auto& pair : input()) {
      ::oneflow::UserOpConf_ListString proto_input_value;
      pair.second.ToProto(&proto_input_value);
      mut_input[pair.first] = proto_input_value;
    }
  }
  // map field : output
  if (!output().empty()) {
    auto& mut_output = *(proto_useropconf->mutable_output());
    for (const auto& pair : output()) {
      ::oneflow::UserOpConf_ListString proto_output_value;
      pair.second.ToProto(&proto_output_value);
      mut_output[pair.first] = proto_output_value;
    }
  }
  // map field : attr
  if (!attr().empty()) {
    auto& mut_attr = *(proto_useropconf->mutable_attr());
    for (const auto& pair : attr()) {
      ::oneflow::AttrValue proto_attr_value;
      pair.second.ToProto(&proto_attr_value);
      mut_attr[pair.first] = proto_attr_value;
    }
  }

}

::std::string ConstUserOpConf::_UserOpConf_::DebugString() const {
  ::oneflow::UserOpConf proto_useropconf;
  this->ToProto(&proto_useropconf);
  return proto_useropconf.DebugString();
}

void ConstUserOpConf::_UserOpConf_::Clear() {
  clear_op_type_name();
  clear_input();
  clear_output();
  clear_attr();
}

void ConstUserOpConf::_UserOpConf_::CopyFrom(const _UserOpConf_& other) {
  if (other.has_op_type_name()) {
    set_op_type_name(other.op_type_name());
  } else {
    clear_op_type_name();
  }
  mutable_input()->CopyFrom(other.input());
  mutable_output()->CopyFrom(other.output());
  mutable_attr()->CopyFrom(other.attr());
}


// optional field op_type_name
bool ConstUserOpConf::_UserOpConf_::has_op_type_name() const {
  return has_op_type_name_;
}
const ::std::string& ConstUserOpConf::_UserOpConf_::op_type_name() const {
  if (has_op_type_name_) { return op_type_name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstUserOpConf::_UserOpConf_::clear_op_type_name() {
  has_op_type_name_ = false;
}
void ConstUserOpConf::_UserOpConf_::set_op_type_name(const ::std::string& value) {
  op_type_name_ = value;
  has_op_type_name_ = true;
}
::std::string* ConstUserOpConf::_UserOpConf_::mutable_op_type_name() {
  has_op_type_name_ = true;
  return &op_type_name_;
}

::std::size_t ConstUserOpConf::_UserOpConf_::input_size() const {
  if (!input_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return default_static_value->size();
  }
  return input_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& ConstUserOpConf::_UserOpConf_::input() const {
  if (!input_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return *(default_static_value.get());
  }
  return *(input_.get());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ * ConstUserOpConf::_UserOpConf_::mutable_input() {
  if (!input_) {
    input_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
  }
  return input_.get();
}

const ::oneflow::cfg::UserOpConf_ListString& ConstUserOpConf::_UserOpConf_::input(::std::string key) const {
  if (!input_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return default_static_value->at(key);
  }
  return input_->at(key);
}

void ConstUserOpConf::_UserOpConf_::clear_input() {
  if (!input_) {
    input_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
  }
  return input_->Clear();
}


::std::size_t ConstUserOpConf::_UserOpConf_::output_size() const {
  if (!output_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return default_static_value->size();
  }
  return output_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& ConstUserOpConf::_UserOpConf_::output() const {
  if (!output_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return *(default_static_value.get());
  }
  return *(output_.get());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ * ConstUserOpConf::_UserOpConf_::mutable_output() {
  if (!output_) {
    output_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
  }
  return output_.get();
}

const ::oneflow::cfg::UserOpConf_ListString& ConstUserOpConf::_UserOpConf_::output(::std::string key) const {
  if (!output_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
    return default_static_value->at(key);
  }
  return output_->at(key);
}

void ConstUserOpConf::_UserOpConf_::clear_output() {
  if (!output_) {
    output_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>();
  }
  return output_->Clear();
}


::std::size_t ConstUserOpConf::_UserOpConf_::attr_size() const {
  if (!attr_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->size();
  }
  return attr_->size();
}
const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& ConstUserOpConf::_UserOpConf_::attr() const {
  if (!attr_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return *(default_static_value.get());
  }
  return *(attr_.get());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ * ConstUserOpConf::_UserOpConf_::mutable_attr() {
  if (!attr_) {
    attr_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>();
  }
  return attr_.get();
}

const ::oneflow::cfg::AttrValue& ConstUserOpConf::_UserOpConf_::attr(::std::string key) const {
  if (!attr_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>();
    return default_static_value->at(key);
  }
  return attr_->at(key);
}

void ConstUserOpConf::_UserOpConf_::clear_attr() {
  if (!attr_) {
    attr_ = ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>();
  }
  return attr_->Clear();
}



int ConstUserOpConf::_UserOpConf_::compare(const _UserOpConf_& other) {
  if (!(has_op_type_name() == other.has_op_type_name())) {
    return has_op_type_name() < other.has_op_type_name() ? -1 : 1;
  } else if (!(op_type_name() == other.op_type_name())) {
    return op_type_name() < other.op_type_name() ? -1 : 1;
  }
  if (!(input() == other.input())) {
    return input() < other.input() ? -1 : 1;
  }
  if (!(output() == other.output())) {
    return output() < other.output() ? -1 : 1;
  }
  if (!(attr() == other.attr())) {
    return attr() < other.attr() ? -1 : 1;
  }
  return 0;
}

bool ConstUserOpConf::_UserOpConf_::operator==(const _UserOpConf_& other) const {
  return true
    && has_op_type_name() == other.has_op_type_name() 
    && op_type_name() == other.op_type_name()
    && input() == other.input()
    && output() == other.output()
    && attr() == other.attr()
  ;
}

std::size_t ConstUserOpConf::_UserOpConf_::__CalcHash__() const {
  return 0
    ^ (has_op_type_name() ? std::hash<::std::string>()(op_type_name()) : 0)
    ^ input().__CalcHash__()
    ^ output().__CalcHash__()
    ^ attr().__CalcHash__()
  ;
}

bool ConstUserOpConf::_UserOpConf_::operator<(const _UserOpConf_& other) const {
  return false
    || !(has_op_type_name() == other.has_op_type_name()) ? 
      has_op_type_name() < other.has_op_type_name() : false
    || !(op_type_name() == other.op_type_name()) ? 
      op_type_name() < other.op_type_name() : false
    || !(input() == other.input()) ? 
      input() < other.input() : false
    || !(output() == other.output()) ? 
      output() < other.output() : false
    || !(attr() == other.attr()) ? 
      attr() < other.attr() : false
  ;
}

using _UserOpConf_ =  ConstUserOpConf::_UserOpConf_;
ConstUserOpConf::ConstUserOpConf(const ::std::shared_ptr<_UserOpConf_>& data): data_(data) {}
ConstUserOpConf::ConstUserOpConf(): data_(::std::make_shared<_UserOpConf_>()) {}
ConstUserOpConf::ConstUserOpConf(const ::oneflow::UserOpConf& proto_useropconf) {
  BuildFromProto(proto_useropconf);
}
ConstUserOpConf::ConstUserOpConf(const ConstUserOpConf&) = default;
ConstUserOpConf::ConstUserOpConf(ConstUserOpConf&&) noexcept = default;
ConstUserOpConf::~ConstUserOpConf() = default;

void ConstUserOpConf::ToProto(PbMessage* proto_useropconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::UserOpConf*>(proto_useropconf));
}
  
::std::string ConstUserOpConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstUserOpConf::__Empty__() const {
  return !data_;
}

int ConstUserOpConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_type_name", 1},
    {"input", 2},
    {"output", 3},
    {"attr", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstUserOpConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstUserOpConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString>)
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString>)
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstUserOpConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_type_name();
    case 2: return &input();
    case 3: return &output();
    case 4: return &attr();
    default: return nullptr;
  }
}

// required or optional field op_type_name
bool ConstUserOpConf::has_op_type_name() const {
  return __SharedPtrOrDefault__()->has_op_type_name();
}
const ::std::string& ConstUserOpConf::op_type_name() const {
  return __SharedPtrOrDefault__()->op_type_name();
}
// used by pybind11 only
// map field input
::std::size_t ConstUserOpConf::input_size() const {
  return __SharedPtrOrDefault__()->input_size();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& ConstUserOpConf::input() const {
  return __SharedPtrOrDefault__()->input();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> ConstUserOpConf::shared_const_input() const {
  return input().__SharedConst__();
}
// map field output
::std::size_t ConstUserOpConf::output_size() const {
  return __SharedPtrOrDefault__()->output_size();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& ConstUserOpConf::output() const {
  return __SharedPtrOrDefault__()->output();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> ConstUserOpConf::shared_const_output() const {
  return output().__SharedConst__();
}
// map field attr
::std::size_t ConstUserOpConf::attr_size() const {
  return __SharedPtrOrDefault__()->attr_size();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& ConstUserOpConf::attr() const {
  return __SharedPtrOrDefault__()->attr();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> ConstUserOpConf::shared_const_attr() const {
  return attr().__SharedConst__();
}

::std::shared_ptr<ConstUserOpConf> ConstUserOpConf::__SharedConst__() const {
  return ::std::make_shared<ConstUserOpConf>(data_);
}
int64_t ConstUserOpConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstUserOpConf::operator==(const ConstUserOpConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstUserOpConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstUserOpConf::operator<(const ConstUserOpConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_UserOpConf_>& ConstUserOpConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_UserOpConf_> default_ptr = std::make_shared<_UserOpConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_UserOpConf_>& ConstUserOpConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _UserOpConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstUserOpConf
void ConstUserOpConf::BuildFromProto(const PbMessage& proto_useropconf) {
  data_ = ::std::make_shared<_UserOpConf_>(dynamic_cast<const ::oneflow::UserOpConf&>(proto_useropconf));
}

UserOpConf::UserOpConf(const ::std::shared_ptr<_UserOpConf_>& data)
  : ConstUserOpConf(data) {}
UserOpConf::UserOpConf(const UserOpConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<UserOpConf> resize
UserOpConf::UserOpConf(UserOpConf&&) noexcept = default; 
UserOpConf::UserOpConf(const ::oneflow::UserOpConf& proto_useropconf) {
  InitFromProto(proto_useropconf);
}
UserOpConf::UserOpConf() = default;

UserOpConf::~UserOpConf() = default;

void UserOpConf::InitFromProto(const PbMessage& proto_useropconf) {
  BuildFromProto(proto_useropconf);
}
  
void* UserOpConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_type_name();
    case 2: return mutable_input();
    case 3: return mutable_output();
    case 4: return mutable_attr();
    default: return nullptr;
  }
}

bool UserOpConf::operator==(const UserOpConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t UserOpConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool UserOpConf::operator<(const UserOpConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void UserOpConf::Clear() {
  if (data_) { data_.reset(); }
}
void UserOpConf::CopyFrom(const UserOpConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
UserOpConf& UserOpConf::operator=(const UserOpConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_type_name
void UserOpConf::clear_op_type_name() {
  return __SharedPtr__()->clear_op_type_name();
}
void UserOpConf::set_op_type_name(const ::std::string& value) {
  return __SharedPtr__()->set_op_type_name(value);
}
::std::string* UserOpConf::mutable_op_type_name() {
  return  __SharedPtr__()->mutable_op_type_name();
}
// repeated field input
void UserOpConf::clear_input() {
  return __SharedPtr__()->clear_input();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ & UserOpConf::input() const {
  return __SharedConst__()->input();
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_* UserOpConf::mutable_input() {
  return __SharedPtr__()->mutable_input();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> UserOpConf::shared_mutable_input() {
  return mutable_input()->__SharedMutable__();
}
// repeated field output
void UserOpConf::clear_output() {
  return __SharedPtr__()->clear_output();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ & UserOpConf::output() const {
  return __SharedConst__()->output();
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_* UserOpConf::mutable_output() {
  return __SharedPtr__()->mutable_output();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> UserOpConf::shared_mutable_output() {
  return mutable_output()->__SharedMutable__();
}
// repeated field attr
void UserOpConf::clear_attr() {
  return __SharedPtr__()->clear_attr();
}

const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ & UserOpConf::attr() const {
  return __SharedConst__()->attr();
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_* UserOpConf::mutable_attr() {
  return __SharedPtr__()->mutable_attr();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> UserOpConf::shared_mutable_attr() {
  return mutable_attr()->__SharedMutable__();
}

::std::shared_ptr<UserOpConf> UserOpConf::__SharedMutable__() {
  return ::std::make_shared<UserOpConf>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::UserOpConf_ListString>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_() = default;

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::UserOpConf_ListString>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::UserOpConf_ListString& Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstUserOpConf_ListString> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ConstUserOpConf_ListString> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ConstUserOpConf_ListString> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::UserOpConf_ListString>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::UserOpConf_ListString>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::UserOpConf_ListString> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ::oneflow::cfg::UserOpConf_ListString> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ::oneflow::cfg::UserOpConf_ListString> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>(data) {}
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_() = default;
Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_() = default;

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::AttrValue& Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstAttrValue> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue> Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data): Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_(data) {}
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_() = default;
_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_() = default;

void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::AttrValue>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue> _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::shared_mut_end() { return end(); }

}
} // namespace oneflow
