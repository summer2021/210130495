#ifndef CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H_
#define CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {

// forward declare proto class;
class AttrValue_ListInt32;
class AttrValue_ListInt64;
class AttrValue_ListFloat;
class AttrValue_ListDataType;
class AttrValue_ListShape;
class AttrValue_ListString;
class AttrValue;
class AttrDef;

namespace cfg {


class AttrValue_ListInt32;
class ConstAttrValue_ListInt32;

class AttrValue_ListInt64;
class ConstAttrValue_ListInt64;

class AttrValue_ListFloat;
class ConstAttrValue_ListFloat;

class AttrValue_ListDataType;
class ConstAttrValue_ListDataType;

class AttrValue_ListShape;
class ConstAttrValue_ListShape;

class AttrValue_ListString;
class ConstAttrValue_ListString;

class AttrValue;
class ConstAttrValue;

class AttrDef;
class ConstAttrDef;

enum AttrType : unsigned int {
  kAtInt32 = 1,
  kAtInt64 = 2,
  kAtBool = 3,
  kAtFloat = 4,
  kAtDouble = 5,
  kAtString = 6,
  kAtShape = 7,
  kAtDataType = 8,
  kAtListInt32 = 9,
  kAtListInt64 = 10,
  kAtListFloat = 11,
  kAtListDataType = 12,
  kAtListShape = 13,
  kAtListString = 14,
};

inline ::std::string AttrType_Name(AttrType value) {
  switch (value) {
  case kAtInt32: { return "kAtInt32"; }
  case kAtInt64: { return "kAtInt64"; }
  case kAtBool: { return "kAtBool"; }
  case kAtFloat: { return "kAtFloat"; }
  case kAtDouble: { return "kAtDouble"; }
  case kAtString: { return "kAtString"; }
  case kAtShape: { return "kAtShape"; }
  case kAtDataType: { return "kAtDataType"; }
  case kAtListInt32: { return "kAtListInt32"; }
  case kAtListInt64: { return "kAtListInt64"; }
  case kAtListFloat: { return "kAtListFloat"; }
  case kAtListDataType: { return "kAtListDataType"; }
  case kAtListShape: { return "kAtListShape"; }
  case kAtListString: { return "kAtListString"; }
  default:
    return "";
  }
}


class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_;

class ConstAttrValue_ListInt32 : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListInt32_ {
   public:
    _AttrValue_ListInt32_();
    explicit _AttrValue_ListInt32_(const _AttrValue_ListInt32_& other);
    explicit _AttrValue_ListInt32_(_AttrValue_ListInt32_&& other);
    _AttrValue_ListInt32_(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32);
    ~_AttrValue_ListInt32_();

    void InitFromProto(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32);

    void ToProto(::oneflow::AttrValue_ListInt32* proto_attrvalue_listint32) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListInt32_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& val() const;
    const int32_t& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_* mutable_val();
    int32_t* mutable_val(::std::size_t index);
      void add_val(const int32_t& value);
    void set_val(::std::size_t index, const int32_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> val_;
         
   public:
    int compare(const _AttrValue_ListInt32_& other);

    bool operator==(const _AttrValue_ListInt32_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListInt32_& other) const;
  };

  ConstAttrValue_ListInt32(const ::std::shared_ptr<_AttrValue_ListInt32_>& data);
  ConstAttrValue_ListInt32(const ConstAttrValue_ListInt32&);
  ConstAttrValue_ListInt32(ConstAttrValue_ListInt32&&) noexcept;
  ConstAttrValue_ListInt32();
  ConstAttrValue_ListInt32(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32);
  virtual ~ConstAttrValue_ListInt32() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_listint32) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& val() const;
  const int32_t& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> shared_const_val() const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListInt32> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListInt32& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListInt32& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListInt32_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListInt32_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListInt32
  void BuildFromProto(const PbMessage& proto_attrvalue_listint32);
  
  ::std::shared_ptr<_AttrValue_ListInt32_> data_;
};

class AttrValue_ListInt32 final : public ConstAttrValue_ListInt32 {
 public:
  AttrValue_ListInt32(const ::std::shared_ptr<_AttrValue_ListInt32_>& data);
  AttrValue_ListInt32(const AttrValue_ListInt32& other);
  // enable nothrow for ::std::vector<AttrValue_ListInt32> resize 
  AttrValue_ListInt32(AttrValue_ListInt32&&) noexcept;
  AttrValue_ListInt32();
  explicit AttrValue_ListInt32(const ::oneflow::AttrValue_ListInt32& proto_attrvalue_listint32);

  ~AttrValue_ListInt32() override;

  void InitFromProto(const PbMessage& proto_attrvalue_listint32) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListInt32& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListInt32& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListInt32& other);
  AttrValue_ListInt32& operator=(const AttrValue_ListInt32& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_* mutable_val();
  int32_t* mutable_val(::std::size_t index);
  void add_val(const int32_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> shared_mutable_val();
  void set_val(::std::size_t index, const int32_t& value);

  ::std::shared_ptr<AttrValue_ListInt32> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_;

class ConstAttrValue_ListInt64 : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListInt64_ {
   public:
    _AttrValue_ListInt64_();
    explicit _AttrValue_ListInt64_(const _AttrValue_ListInt64_& other);
    explicit _AttrValue_ListInt64_(_AttrValue_ListInt64_&& other);
    _AttrValue_ListInt64_(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64);
    ~_AttrValue_ListInt64_();

    void InitFromProto(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64);

    void ToProto(::oneflow::AttrValue_ListInt64* proto_attrvalue_listint64) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListInt64_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& val() const;
    const int64_t& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_* mutable_val();
    int64_t* mutable_val(::std::size_t index);
      void add_val(const int64_t& value);
    void set_val(::std::size_t index, const int64_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> val_;
         
   public:
    int compare(const _AttrValue_ListInt64_& other);

    bool operator==(const _AttrValue_ListInt64_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListInt64_& other) const;
  };

  ConstAttrValue_ListInt64(const ::std::shared_ptr<_AttrValue_ListInt64_>& data);
  ConstAttrValue_ListInt64(const ConstAttrValue_ListInt64&);
  ConstAttrValue_ListInt64(ConstAttrValue_ListInt64&&) noexcept;
  ConstAttrValue_ListInt64();
  ConstAttrValue_ListInt64(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64);
  virtual ~ConstAttrValue_ListInt64() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_listint64) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& val() const;
  const int64_t& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> shared_const_val() const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListInt64> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListInt64& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListInt64& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListInt64_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListInt64_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListInt64
  void BuildFromProto(const PbMessage& proto_attrvalue_listint64);
  
  ::std::shared_ptr<_AttrValue_ListInt64_> data_;
};

class AttrValue_ListInt64 final : public ConstAttrValue_ListInt64 {
 public:
  AttrValue_ListInt64(const ::std::shared_ptr<_AttrValue_ListInt64_>& data);
  AttrValue_ListInt64(const AttrValue_ListInt64& other);
  // enable nothrow for ::std::vector<AttrValue_ListInt64> resize 
  AttrValue_ListInt64(AttrValue_ListInt64&&) noexcept;
  AttrValue_ListInt64();
  explicit AttrValue_ListInt64(const ::oneflow::AttrValue_ListInt64& proto_attrvalue_listint64);

  ~AttrValue_ListInt64() override;

  void InitFromProto(const PbMessage& proto_attrvalue_listint64) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListInt64& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListInt64& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListInt64& other);
  AttrValue_ListInt64& operator=(const AttrValue_ListInt64& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_* mutable_val();
  int64_t* mutable_val(::std::size_t index);
  void add_val(const int64_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> shared_mutable_val();
  void set_val(::std::size_t index, const int64_t& value);

  ::std::shared_ptr<AttrValue_ListInt64> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_;

class ConstAttrValue_ListFloat : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListFloat_ {
   public:
    _AttrValue_ListFloat_();
    explicit _AttrValue_ListFloat_(const _AttrValue_ListFloat_& other);
    explicit _AttrValue_ListFloat_(_AttrValue_ListFloat_&& other);
    _AttrValue_ListFloat_(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat);
    ~_AttrValue_ListFloat_();

    void InitFromProto(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat);

    void ToProto(::oneflow::AttrValue_ListFloat* proto_attrvalue_listfloat) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListFloat_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& val() const;
    const float& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_* mutable_val();
    float* mutable_val(::std::size_t index);
      void add_val(const float& value);
    void set_val(::std::size_t index, const float& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> val_;
         
   public:
    int compare(const _AttrValue_ListFloat_& other);

    bool operator==(const _AttrValue_ListFloat_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListFloat_& other) const;
  };

  ConstAttrValue_ListFloat(const ::std::shared_ptr<_AttrValue_ListFloat_>& data);
  ConstAttrValue_ListFloat(const ConstAttrValue_ListFloat&);
  ConstAttrValue_ListFloat(ConstAttrValue_ListFloat&&) noexcept;
  ConstAttrValue_ListFloat();
  ConstAttrValue_ListFloat(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat);
  virtual ~ConstAttrValue_ListFloat() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_listfloat) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& val() const;
  const float& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> shared_const_val() const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListFloat> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListFloat& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListFloat& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListFloat_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListFloat_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListFloat
  void BuildFromProto(const PbMessage& proto_attrvalue_listfloat);
  
  ::std::shared_ptr<_AttrValue_ListFloat_> data_;
};

class AttrValue_ListFloat final : public ConstAttrValue_ListFloat {
 public:
  AttrValue_ListFloat(const ::std::shared_ptr<_AttrValue_ListFloat_>& data);
  AttrValue_ListFloat(const AttrValue_ListFloat& other);
  // enable nothrow for ::std::vector<AttrValue_ListFloat> resize 
  AttrValue_ListFloat(AttrValue_ListFloat&&) noexcept;
  AttrValue_ListFloat();
  explicit AttrValue_ListFloat(const ::oneflow::AttrValue_ListFloat& proto_attrvalue_listfloat);

  ~AttrValue_ListFloat() override;

  void InitFromProto(const PbMessage& proto_attrvalue_listfloat) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListFloat& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListFloat& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListFloat& other);
  AttrValue_ListFloat& operator=(const AttrValue_ListFloat& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_* mutable_val();
  float* mutable_val(::std::size_t index);
  void add_val(const float& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> shared_mutable_val();
  void set_val(::std::size_t index, const float& value);

  ::std::shared_ptr<AttrValue_ListFloat> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_;

class ConstAttrValue_ListDataType : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListDataType_ {
   public:
    _AttrValue_ListDataType_();
    explicit _AttrValue_ListDataType_(const _AttrValue_ListDataType_& other);
    explicit _AttrValue_ListDataType_(_AttrValue_ListDataType_&& other);
    _AttrValue_ListDataType_(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype);
    ~_AttrValue_ListDataType_();

    void InitFromProto(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype);

    void ToProto(::oneflow::AttrValue_ListDataType* proto_attrvalue_listdatatype) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListDataType_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& val() const;
    const ::oneflow::cfg::DataType& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_* mutable_val();
    ::oneflow::cfg::DataType* mutable_val(::std::size_t index);
      void add_val(const ::oneflow::cfg::DataType& value);
    void set_val(::std::size_t index, const ::oneflow::cfg::DataType& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> val_;
         
   public:
    int compare(const _AttrValue_ListDataType_& other);

    bool operator==(const _AttrValue_ListDataType_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListDataType_& other) const;
  };

  ConstAttrValue_ListDataType(const ::std::shared_ptr<_AttrValue_ListDataType_>& data);
  ConstAttrValue_ListDataType(const ConstAttrValue_ListDataType&);
  ConstAttrValue_ListDataType(ConstAttrValue_ListDataType&&) noexcept;
  ConstAttrValue_ListDataType();
  ConstAttrValue_ListDataType(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype);
  virtual ~ConstAttrValue_ListDataType() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_listdatatype) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& val() const;
  const ::oneflow::cfg::DataType& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> shared_const_val() const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListDataType> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListDataType& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListDataType& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListDataType_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListDataType_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListDataType
  void BuildFromProto(const PbMessage& proto_attrvalue_listdatatype);
  
  ::std::shared_ptr<_AttrValue_ListDataType_> data_;
};

class AttrValue_ListDataType final : public ConstAttrValue_ListDataType {
 public:
  AttrValue_ListDataType(const ::std::shared_ptr<_AttrValue_ListDataType_>& data);
  AttrValue_ListDataType(const AttrValue_ListDataType& other);
  // enable nothrow for ::std::vector<AttrValue_ListDataType> resize 
  AttrValue_ListDataType(AttrValue_ListDataType&&) noexcept;
  AttrValue_ListDataType();
  explicit AttrValue_ListDataType(const ::oneflow::AttrValue_ListDataType& proto_attrvalue_listdatatype);

  ~AttrValue_ListDataType() override;

  void InitFromProto(const PbMessage& proto_attrvalue_listdatatype) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListDataType& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListDataType& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListDataType& other);
  AttrValue_ListDataType& operator=(const AttrValue_ListDataType& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_* mutable_val();
  ::oneflow::cfg::DataType* mutable_val(::std::size_t index);
  void add_val(const ::oneflow::cfg::DataType& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> shared_mutable_val();
  void set_val(::std::size_t index, const ::oneflow::cfg::DataType& value);

  ::std::shared_ptr<AttrValue_ListDataType> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_;

class ConstAttrValue_ListShape : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListShape_ {
   public:
    _AttrValue_ListShape_();
    explicit _AttrValue_ListShape_(const _AttrValue_ListShape_& other);
    explicit _AttrValue_ListShape_(_AttrValue_ListShape_&& other);
    _AttrValue_ListShape_(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape);
    ~_AttrValue_ListShape_();

    void InitFromProto(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape);

    void ToProto(::oneflow::AttrValue_ListShape* proto_attrvalue_listshape) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListShape_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& val() const;
    const ::oneflow::cfg::ShapeProto& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_* mutable_val();
    ::oneflow::cfg::ShapeProto* mutable_val(::std::size_t index);
      ::oneflow::cfg::ShapeProto* add_val();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> val_;
         
   public:
    int compare(const _AttrValue_ListShape_& other);

    bool operator==(const _AttrValue_ListShape_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListShape_& other) const;
  };

  ConstAttrValue_ListShape(const ::std::shared_ptr<_AttrValue_ListShape_>& data);
  ConstAttrValue_ListShape(const ConstAttrValue_ListShape&);
  ConstAttrValue_ListShape(ConstAttrValue_ListShape&&) noexcept;
  ConstAttrValue_ListShape();
  ConstAttrValue_ListShape(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape);
  virtual ~ConstAttrValue_ListShape() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_listshape) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& val() const;
  const ::oneflow::cfg::ShapeProto& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> shared_const_val() const;
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_val(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListShape> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListShape& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListShape& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListShape_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListShape_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListShape
  void BuildFromProto(const PbMessage& proto_attrvalue_listshape);
  
  ::std::shared_ptr<_AttrValue_ListShape_> data_;
};

class AttrValue_ListShape final : public ConstAttrValue_ListShape {
 public:
  AttrValue_ListShape(const ::std::shared_ptr<_AttrValue_ListShape_>& data);
  AttrValue_ListShape(const AttrValue_ListShape& other);
  // enable nothrow for ::std::vector<AttrValue_ListShape> resize 
  AttrValue_ListShape(AttrValue_ListShape&&) noexcept;
  AttrValue_ListShape();
  explicit AttrValue_ListShape(const ::oneflow::AttrValue_ListShape& proto_attrvalue_listshape);

  ~AttrValue_ListShape() override;

  void InitFromProto(const PbMessage& proto_attrvalue_listshape) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListShape& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListShape& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListShape& other);
  AttrValue_ListShape& operator=(const AttrValue_ListShape& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_* mutable_val();
  ::oneflow::cfg::ShapeProto* mutable_val(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> shared_mutable_val();
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_val(::std::size_t index);
  ::oneflow::cfg::ShapeProto* add_val();

  ::std::shared_ptr<AttrValue_ListShape> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_;

class ConstAttrValue_ListString : public ::oneflow::cfg::Message {
 public:

  class _AttrValue_ListString_ {
   public:
    _AttrValue_ListString_();
    explicit _AttrValue_ListString_(const _AttrValue_ListString_& other);
    explicit _AttrValue_ListString_(_AttrValue_ListString_&& other);
    _AttrValue_ListString_(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring);
    ~_AttrValue_ListString_();

    void InitFromProto(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring);

    void ToProto(::oneflow::AttrValue_ListString* proto_attrvalue_liststring) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_ListString_& other);
  
      // repeated field val
   public:
    ::std::size_t val_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& val() const;
    const ::std::string& val(::std::size_t index) const;
    void clear_val();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_* mutable_val();
    ::std::string* mutable_val(::std::size_t index);
      void add_val(const ::std::string& value);
    void set_val(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> val_;
         
   public:
    int compare(const _AttrValue_ListString_& other);

    bool operator==(const _AttrValue_ListString_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_ListString_& other) const;
  };

  ConstAttrValue_ListString(const ::std::shared_ptr<_AttrValue_ListString_>& data);
  ConstAttrValue_ListString(const ConstAttrValue_ListString&);
  ConstAttrValue_ListString(ConstAttrValue_ListString&&) noexcept;
  ConstAttrValue_ListString();
  ConstAttrValue_ListString(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring);
  virtual ~ConstAttrValue_ListString() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue_liststring) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field val
 public:
  ::std::size_t val_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& val() const;
  const ::std::string& val(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> shared_const_val() const;

 public:
  ::std::shared_ptr<ConstAttrValue_ListString> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue_ListString& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue_ListString& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_ListString_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_ListString_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue_ListString
  void BuildFromProto(const PbMessage& proto_attrvalue_liststring);
  
  ::std::shared_ptr<_AttrValue_ListString_> data_;
};

class AttrValue_ListString final : public ConstAttrValue_ListString {
 public:
  AttrValue_ListString(const ::std::shared_ptr<_AttrValue_ListString_>& data);
  AttrValue_ListString(const AttrValue_ListString& other);
  // enable nothrow for ::std::vector<AttrValue_ListString> resize 
  AttrValue_ListString(AttrValue_ListString&&) noexcept;
  AttrValue_ListString();
  explicit AttrValue_ListString(const ::oneflow::AttrValue_ListString& proto_attrvalue_liststring);

  ~AttrValue_ListString() override;

  void InitFromProto(const PbMessage& proto_attrvalue_liststring) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue_ListString& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue_ListString& other) const;
  void Clear();
  void CopyFrom(const AttrValue_ListString& other);
  AttrValue_ListString& operator=(const AttrValue_ListString& other);

  // repeated field val
 public:
  void clear_val();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_* mutable_val();
  ::std::string* mutable_val(::std::size_t index);
  void add_val(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> shared_mutable_val();
  void set_val(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<AttrValue_ListString> __SharedMutable__();
};


class ConstAttrValue : public ::oneflow::cfg::Message {
 public:

 // oneof enum value
 enum ValueCase : unsigned int {
  VALUE_NOT_SET = 0,
    kAtInt32 = 1,
    kAtInt64 = 2,
    kAtBool = 3,
    kAtFloat = 4,
    kAtDouble = 5,
    kAtString = 6,
    kAtShape = 7,
    kAtDataType = 8,
    kAtListInt32 = 9,
    kAtListInt64 = 10,
    kAtListFloat = 11,
    kAtListDataType = 12,
    kAtListShape = 13,
    kAtListString = 14,
   };

  class _AttrValue_ {
   public:
    _AttrValue_();
    explicit _AttrValue_(const _AttrValue_& other);
    explicit _AttrValue_(_AttrValue_&& other);
    _AttrValue_(const ::oneflow::AttrValue& proto_attrvalue);
    ~_AttrValue_();

    void InitFromProto(const ::oneflow::AttrValue& proto_attrvalue);

    void ToProto(::oneflow::AttrValue* proto_attrvalue) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrValue_& other);
  
     // oneof field value: at_int32
   public:
    bool has_at_int32() const;
    void clear_at_int32();
    const int32_t& at_int32() const;
      void set_at_int32(const int32_t& value);
    int32_t* mutable_at_int32();
      
     // oneof field value: at_int64
   public:
    bool has_at_int64() const;
    void clear_at_int64();
    const int64_t& at_int64() const;
      void set_at_int64(const int64_t& value);
    int64_t* mutable_at_int64();
      
     // oneof field value: at_bool
   public:
    bool has_at_bool() const;
    void clear_at_bool();
    const bool& at_bool() const;
      void set_at_bool(const bool& value);
    bool* mutable_at_bool();
      
     // oneof field value: at_float
   public:
    bool has_at_float() const;
    void clear_at_float();
    const float& at_float() const;
      void set_at_float(const float& value);
    float* mutable_at_float();
      
     // oneof field value: at_double
   public:
    bool has_at_double() const;
    void clear_at_double();
    const double& at_double() const;
      void set_at_double(const double& value);
    double* mutable_at_double();
      
     // oneof field value: at_string
   public:
    bool has_at_string() const;
    void clear_at_string();
    const ::std::string& at_string() const;
      void set_at_string(const ::std::string& value);
    ::std::string* mutable_at_string();
      
     // oneof field value: at_shape
   public:
    bool has_at_shape() const;
    void clear_at_shape();
    const ::oneflow::cfg::ShapeProto& at_shape() const;
      ::oneflow::cfg::ShapeProto* mutable_at_shape();
      
     // oneof field value: at_data_type
   public:
    bool has_at_data_type() const;
    void clear_at_data_type();
    const ::oneflow::cfg::DataType& at_data_type() const;
      void set_at_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_at_data_type();
      
     // oneof field value: at_list_int32
   public:
    bool has_at_list_int32() const;
    void clear_at_list_int32();
    const ::oneflow::cfg::AttrValue_ListInt32& at_list_int32() const;
      ::oneflow::cfg::AttrValue_ListInt32* mutable_at_list_int32();
      
     // oneof field value: at_list_int64
   public:
    bool has_at_list_int64() const;
    void clear_at_list_int64();
    const ::oneflow::cfg::AttrValue_ListInt64& at_list_int64() const;
      ::oneflow::cfg::AttrValue_ListInt64* mutable_at_list_int64();
      
     // oneof field value: at_list_float
   public:
    bool has_at_list_float() const;
    void clear_at_list_float();
    const ::oneflow::cfg::AttrValue_ListFloat& at_list_float() const;
      ::oneflow::cfg::AttrValue_ListFloat* mutable_at_list_float();
      
     // oneof field value: at_list_data_type
   public:
    bool has_at_list_data_type() const;
    void clear_at_list_data_type();
    const ::oneflow::cfg::AttrValue_ListDataType& at_list_data_type() const;
      ::oneflow::cfg::AttrValue_ListDataType* mutable_at_list_data_type();
      
     // oneof field value: at_list_shape
   public:
    bool has_at_list_shape() const;
    void clear_at_list_shape();
    const ::oneflow::cfg::AttrValue_ListShape& at_list_shape() const;
      ::oneflow::cfg::AttrValue_ListShape* mutable_at_list_shape();
      
     // oneof field value: at_list_string
   public:
    bool has_at_list_string() const;
    void clear_at_list_string();
    const ::oneflow::cfg::AttrValue_ListString& at_list_string() const;
      ::oneflow::cfg::AttrValue_ListString* mutable_at_list_string();
           
   public:
    // oneof value
    ValueCase value_case() const;
    bool has_value() const;
   protected:
    void clear_value();
    void value_copy_from(const _AttrValue_& other);
    union ValueUnion {
      // 64-bit aligned
      uint64_t __value_for_padding_64bit__;
          int32_t at_int32_;
            int64_t at_int64_;
            bool at_bool_;
            float at_float_;
            double at_double_;
            char at_string_[sizeof(::std::string)];
            char at_shape_[sizeof(::std::shared_ptr<::oneflow::cfg::ShapeProto>)];
            DataType at_data_type_;
            char at_list_int32_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32>)];
            char at_list_int64_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64>)];
            char at_list_float_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat>)];
            char at_list_data_type_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType>)];
            char at_list_shape_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape>)];
            char at_list_string_[sizeof(::std::shared_ptr<::oneflow::cfg::AttrValue_ListString>)];
        } value_;
    ValueCase value_case_ = VALUE_NOT_SET;
     
   public:
    int compare(const _AttrValue_& other);

    bool operator==(const _AttrValue_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrValue_& other) const;
  };

  ConstAttrValue(const ::std::shared_ptr<_AttrValue_>& data);
  ConstAttrValue(const ConstAttrValue&);
  ConstAttrValue(ConstAttrValue&&) noexcept;
  ConstAttrValue();
  ConstAttrValue(const ::oneflow::AttrValue& proto_attrvalue);
  virtual ~ConstAttrValue() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrvalue) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field value: at_int32
 public:
  bool has_at_int32() const;
  const int32_t& at_int32() const;
  // used by pybind11 only
 // oneof field value: at_int64
 public:
  bool has_at_int64() const;
  const int64_t& at_int64() const;
  // used by pybind11 only
 // oneof field value: at_bool
 public:
  bool has_at_bool() const;
  const bool& at_bool() const;
  // used by pybind11 only
 // oneof field value: at_float
 public:
  bool has_at_float() const;
  const float& at_float() const;
  // used by pybind11 only
 // oneof field value: at_double
 public:
  bool has_at_double() const;
  const double& at_double() const;
  // used by pybind11 only
 // oneof field value: at_string
 public:
  bool has_at_string() const;
  const ::std::string& at_string() const;
  // used by pybind11 only
 // oneof field value: at_shape
 public:
  bool has_at_shape() const;
  const ::oneflow::cfg::ShapeProto& at_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_at_shape() const;
 // oneof field value: at_data_type
 public:
  bool has_at_data_type() const;
  const ::oneflow::cfg::DataType& at_data_type() const;
  // used by pybind11 only
 // oneof field value: at_list_int32
 public:
  bool has_at_list_int32() const;
  const ::oneflow::cfg::AttrValue_ListInt32& at_list_int32() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListInt32> shared_const_at_list_int32() const;
 // oneof field value: at_list_int64
 public:
  bool has_at_list_int64() const;
  const ::oneflow::cfg::AttrValue_ListInt64& at_list_int64() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListInt64> shared_const_at_list_int64() const;
 // oneof field value: at_list_float
 public:
  bool has_at_list_float() const;
  const ::oneflow::cfg::AttrValue_ListFloat& at_list_float() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListFloat> shared_const_at_list_float() const;
 // oneof field value: at_list_data_type
 public:
  bool has_at_list_data_type() const;
  const ::oneflow::cfg::AttrValue_ListDataType& at_list_data_type() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListDataType> shared_const_at_list_data_type() const;
 // oneof field value: at_list_shape
 public:
  bool has_at_list_shape() const;
  const ::oneflow::cfg::AttrValue_ListShape& at_list_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListShape> shared_const_at_list_shape() const;
 // oneof field value: at_list_string
 public:
  bool has_at_list_string() const;
  const ::oneflow::cfg::AttrValue_ListString& at_list_string() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue_ListString> shared_const_at_list_string() const;
 public:
  ValueCase value_case() const;
 protected:
  bool has_value() const;

 public:
  ::std::shared_ptr<ConstAttrValue> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrValue& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrValue& other) const;
 protected:
  const ::std::shared_ptr<_AttrValue_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrValue_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrValue
  void BuildFromProto(const PbMessage& proto_attrvalue);
  
  ::std::shared_ptr<_AttrValue_> data_;
};

class AttrValue final : public ConstAttrValue {
 public:
  AttrValue(const ::std::shared_ptr<_AttrValue_>& data);
  AttrValue(const AttrValue& other);
  // enable nothrow for ::std::vector<AttrValue> resize 
  AttrValue(AttrValue&&) noexcept;
  AttrValue();
  explicit AttrValue(const ::oneflow::AttrValue& proto_attrvalue);

  ~AttrValue() override;

  void InitFromProto(const PbMessage& proto_attrvalue) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrValue& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrValue& other) const;
  void Clear();
  void CopyFrom(const AttrValue& other);
  AttrValue& operator=(const AttrValue& other);

  void clear_at_int32();
  void set_at_int32(const int32_t& value);
  int32_t* mutable_at_int32();
  void clear_at_int64();
  void set_at_int64(const int64_t& value);
  int64_t* mutable_at_int64();
  void clear_at_bool();
  void set_at_bool(const bool& value);
  bool* mutable_at_bool();
  void clear_at_float();
  void set_at_float(const float& value);
  float* mutable_at_float();
  void clear_at_double();
  void set_at_double(const double& value);
  double* mutable_at_double();
  void clear_at_string();
  void set_at_string(const ::std::string& value);
  ::std::string* mutable_at_string();
  void clear_at_shape();
  ::oneflow::cfg::ShapeProto* mutable_at_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_at_shape();
  void clear_at_data_type();
  void set_at_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_at_data_type();
  void clear_at_list_int32();
  ::oneflow::cfg::AttrValue_ListInt32* mutable_at_list_int32();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt32> shared_mutable_at_list_int32();
  void clear_at_list_int64();
  ::oneflow::cfg::AttrValue_ListInt64* mutable_at_list_int64();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListInt64> shared_mutable_at_list_int64();
  void clear_at_list_float();
  ::oneflow::cfg::AttrValue_ListFloat* mutable_at_list_float();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListFloat> shared_mutable_at_list_float();
  void clear_at_list_data_type();
  ::oneflow::cfg::AttrValue_ListDataType* mutable_at_list_data_type();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListDataType> shared_mutable_at_list_data_type();
  void clear_at_list_shape();
  ::oneflow::cfg::AttrValue_ListShape* mutable_at_list_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListShape> shared_mutable_at_list_shape();
  void clear_at_list_string();
  ::oneflow::cfg::AttrValue_ListString* mutable_at_list_string();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue_ListString> shared_mutable_at_list_string();

  ::std::shared_ptr<AttrValue> __SharedMutable__();
};


class ConstAttrDef : public ::oneflow::cfg::Message {
 public:

  class _AttrDef_ {
   public:
    _AttrDef_();
    explicit _AttrDef_(const _AttrDef_& other);
    explicit _AttrDef_(_AttrDef_&& other);
    _AttrDef_(const ::oneflow::AttrDef& proto_attrdef);
    ~_AttrDef_();

    void InitFromProto(const ::oneflow::AttrDef& proto_attrdef);

    void ToProto(::oneflow::AttrDef* proto_attrdef) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AttrDef_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
      // optional field description
     public:
    bool has_description() const;
    const ::std::string& description() const;
    void clear_description();
    void set_description(const ::std::string& value);
    ::std::string* mutable_description();
   protected:
    bool has_description_ = false;
    ::std::string description_;
      
      // optional field default_val
     public:
    bool has_default_val() const;
    const ::oneflow::cfg::AttrValue& default_val() const;
    void clear_default_val();
    ::oneflow::cfg::AttrValue* mutable_default_val();
   protected:
    bool has_default_val_ = false;
    ::std::shared_ptr<::oneflow::cfg::AttrValue> default_val_;
           
   public:
    int compare(const _AttrDef_& other);

    bool operator==(const _AttrDef_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AttrDef_& other) const;
  };

  ConstAttrDef(const ::std::shared_ptr<_AttrDef_>& data);
  ConstAttrDef(const ConstAttrDef&);
  ConstAttrDef(ConstAttrDef&&) noexcept;
  ConstAttrDef();
  ConstAttrDef(const ::oneflow::AttrDef& proto_attrdef);
  virtual ~ConstAttrDef() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_attrdef) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // required or optional field description
 public:
  bool has_description() const;
  const ::std::string& description() const;
  // used by pybind11 only
  // required or optional field default_val
 public:
  bool has_default_val() const;
  const ::oneflow::cfg::AttrValue& default_val() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAttrValue> shared_const_default_val() const;

 public:
  ::std::shared_ptr<ConstAttrDef> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAttrDef& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAttrDef& other) const;
 protected:
  const ::std::shared_ptr<_AttrDef_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AttrDef_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAttrDef
  void BuildFromProto(const PbMessage& proto_attrdef);
  
  ::std::shared_ptr<_AttrDef_> data_;
};

class AttrDef final : public ConstAttrDef {
 public:
  AttrDef(const ::std::shared_ptr<_AttrDef_>& data);
  AttrDef(const AttrDef& other);
  // enable nothrow for ::std::vector<AttrDef> resize 
  AttrDef(AttrDef&&) noexcept;
  AttrDef();
  explicit AttrDef(const ::oneflow::AttrDef& proto_attrdef);

  ~AttrDef() override;

  void InitFromProto(const PbMessage& proto_attrdef) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AttrDef& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AttrDef& other) const;
  void Clear();
  void CopyFrom(const AttrDef& other);
  AttrDef& operator=(const AttrDef& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // required or optional field description
 public:
  void clear_description();
  void set_description(const ::std::string& value);
  ::std::string* mutable_description();
  // required or optional field default_val
 public:
  void clear_default_val();
  ::oneflow::cfg::AttrValue* mutable_default_val();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AttrValue> shared_mutable_default_val();

  ::std::shared_ptr<AttrDef> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_ : public ::oneflow::cfg::_RepeatedField_<int32_t> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int32_t_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_ : public ::oneflow::cfg::_RepeatedField_<int64_t> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_int64_t_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_ : public ::oneflow::cfg::_RepeatedField_<float> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_float_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::DataType> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::DataType>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::DataType>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_DataType_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::ShapeProto> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeProto>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::ShapeProto>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField_ShapeProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};









} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::AttrType> {
  std::size_t operator()(::oneflow::cfg::AttrType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListInt32> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListInt32& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListInt32> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListInt32& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListInt64> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListInt64& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListInt64> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListInt64& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListFloat> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListFloat& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListFloat> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListFloat& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListDataType> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListDataType& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListDataType> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListDataType& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListShape> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListShape& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListShape> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListShape& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue_ListString> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue_ListString& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue_ListString> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue_ListString& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrValue> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrValue& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrValue> {
  std::size_t operator()(const ::oneflow::cfg::AttrValue& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAttrDef> {
  std::size_t operator()(const ::oneflow::cfg::ConstAttrDef& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AttrDef> {
  std::size_t operator()(const ::oneflow::cfg::AttrDef& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_ATTR_CFG_H_