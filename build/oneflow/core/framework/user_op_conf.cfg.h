#ifndef CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstAttrValue;
class AttrValue;
}
}

namespace oneflow {

// forward declare proto class;
class UserOpConf_ListString;
class UserOpConf_InputEntry;
class UserOpConf_OutputEntry;
class UserOpConf_AttrEntry;
class UserOpConf;

namespace cfg {


class UserOpConf_ListString;
class ConstUserOpConf_ListString;

class UserOpConf;
class ConstUserOpConf;


class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_;

class ConstUserOpConf_ListString : public ::oneflow::cfg::Message {
 public:

  class _UserOpConf_ListString_ {
   public:
    _UserOpConf_ListString_();
    explicit _UserOpConf_ListString_(const _UserOpConf_ListString_& other);
    explicit _UserOpConf_ListString_(_UserOpConf_ListString_&& other);
    _UserOpConf_ListString_(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring);
    ~_UserOpConf_ListString_();

    void InitFromProto(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring);

    void ToProto(::oneflow::UserOpConf_ListString* proto_useropconf_liststring) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _UserOpConf_ListString_& other);
  
      // repeated field s
   public:
    ::std::size_t s_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& s() const;
    const ::std::string& s(::std::size_t index) const;
    void clear_s();
    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_s();
    ::std::string* mutable_s(::std::size_t index);
      void add_s(const ::std::string& value);
    void set_s(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> s_;
         
   public:
    int compare(const _UserOpConf_ListString_& other);

    bool operator==(const _UserOpConf_ListString_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _UserOpConf_ListString_& other) const;
  };

  ConstUserOpConf_ListString(const ::std::shared_ptr<_UserOpConf_ListString_>& data);
  ConstUserOpConf_ListString(const ConstUserOpConf_ListString&);
  ConstUserOpConf_ListString(ConstUserOpConf_ListString&&) noexcept;
  ConstUserOpConf_ListString();
  ConstUserOpConf_ListString(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring);
  virtual ~ConstUserOpConf_ListString() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_useropconf_liststring) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field s
 public:
  ::std::size_t s_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& s() const;
  const ::std::string& s(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_s() const;

 public:
  ::std::shared_ptr<ConstUserOpConf_ListString> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstUserOpConf_ListString& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstUserOpConf_ListString& other) const;
 protected:
  const ::std::shared_ptr<_UserOpConf_ListString_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_UserOpConf_ListString_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstUserOpConf_ListString
  void BuildFromProto(const PbMessage& proto_useropconf_liststring);
  
  ::std::shared_ptr<_UserOpConf_ListString_> data_;
};

class UserOpConf_ListString final : public ConstUserOpConf_ListString {
 public:
  UserOpConf_ListString(const ::std::shared_ptr<_UserOpConf_ListString_>& data);
  UserOpConf_ListString(const UserOpConf_ListString& other);
  // enable nothrow for ::std::vector<UserOpConf_ListString> resize 
  UserOpConf_ListString(UserOpConf_ListString&&) noexcept;
  UserOpConf_ListString();
  explicit UserOpConf_ListString(const ::oneflow::UserOpConf_ListString& proto_useropconf_liststring);

  ~UserOpConf_ListString() override;

  void InitFromProto(const PbMessage& proto_useropconf_liststring) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const UserOpConf_ListString& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const UserOpConf_ListString& other) const;
  void Clear();
  void CopyFrom(const UserOpConf_ListString& other);
  UserOpConf_ListString& operator=(const UserOpConf_ListString& other);

  // repeated field s
 public:
  void clear_s();
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_s();
  ::std::string* mutable_s(::std::size_t index);
  void add_s(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_s();
  void set_s(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<UserOpConf_ListString> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_; 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_;
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_; 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_;

class ConstUserOpConf : public ::oneflow::cfg::Message {
 public:

  class _UserOpConf_ {
   public:
    _UserOpConf_();
    explicit _UserOpConf_(const _UserOpConf_& other);
    explicit _UserOpConf_(_UserOpConf_&& other);
    _UserOpConf_(const ::oneflow::UserOpConf& proto_useropconf);
    ~_UserOpConf_();

    void InitFromProto(const ::oneflow::UserOpConf& proto_useropconf);

    void ToProto(::oneflow::UserOpConf* proto_useropconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _UserOpConf_& other);
  
      // optional field op_type_name
     public:
    bool has_op_type_name() const;
    const ::std::string& op_type_name() const;
    void clear_op_type_name();
    void set_op_type_name(const ::std::string& value);
    ::std::string* mutable_op_type_name();
   protected:
    bool has_op_type_name_ = false;
    ::std::string op_type_name_;
      
     public:
    ::std::size_t input_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& input() const;

    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ * mutable_input();

    const ::oneflow::cfg::UserOpConf_ListString& input(::std::string key) const;

    void clear_input();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> input_;
    
     public:
    ::std::size_t output_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& output() const;

    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ * mutable_output();

    const ::oneflow::cfg::UserOpConf_ListString& output(::std::string key) const;

    void clear_output();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> output_;
    
     public:
    ::std::size_t attr_size() const;
    const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& attr() const;

    _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ * mutable_attr();

    const ::oneflow::cfg::AttrValue& attr(::std::string key) const;

    void clear_attr();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> attr_;
         
   public:
    int compare(const _UserOpConf_& other);

    bool operator==(const _UserOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _UserOpConf_& other) const;
  };

  ConstUserOpConf(const ::std::shared_ptr<_UserOpConf_>& data);
  ConstUserOpConf(const ConstUserOpConf&);
  ConstUserOpConf(ConstUserOpConf&&) noexcept;
  ConstUserOpConf();
  ConstUserOpConf(const ::oneflow::UserOpConf& proto_useropconf);
  virtual ~ConstUserOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_useropconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_type_name
 public:
  bool has_op_type_name() const;
  const ::std::string& op_type_name() const;
  // used by pybind11 only
  // map field input
 public:
  ::std::size_t input_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& input() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> shared_const_input() const;
  // map field output
 public:
  ::std::size_t output_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& output() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> shared_const_output() const;
  // map field attr
 public:
  ::std::size_t attr_size() const;
  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& attr() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> shared_const_attr() const;

 public:
  ::std::shared_ptr<ConstUserOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstUserOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstUserOpConf& other) const;
 protected:
  const ::std::shared_ptr<_UserOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_UserOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstUserOpConf
  void BuildFromProto(const PbMessage& proto_useropconf);
  
  ::std::shared_ptr<_UserOpConf_> data_;
};

class UserOpConf final : public ConstUserOpConf {
 public:
  UserOpConf(const ::std::shared_ptr<_UserOpConf_>& data);
  UserOpConf(const UserOpConf& other);
  // enable nothrow for ::std::vector<UserOpConf> resize 
  UserOpConf(UserOpConf&&) noexcept;
  UserOpConf();
  explicit UserOpConf(const ::oneflow::UserOpConf& proto_useropconf);

  ~UserOpConf() override;

  void InitFromProto(const PbMessage& proto_useropconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const UserOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const UserOpConf& other) const;
  void Clear();
  void CopyFrom(const UserOpConf& other);
  UserOpConf& operator=(const UserOpConf& other);

  // required or optional field op_type_name
 public:
  void clear_op_type_name();
  void set_op_type_name(const ::std::string& value);
  ::std::string* mutable_op_type_name();
  // repeated field input
 public:
  void clear_input();

  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ & input() const;

  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_* mutable_input();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> shared_mutable_input();
  // repeated field output
 public:
  void clear_output();

  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ & output() const;

  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_* mutable_output();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> shared_mutable_output();
  // repeated field attr
 public:
  void clear_attr();

  const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ & attr() const;

  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_* mutable_attr();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> shared_mutable_attr();

  ::std::shared_ptr<UserOpConf> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::UserOpConf_ListString> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::UserOpConf_ListString>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::UserOpConf_ListString& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstUserOpConf_ListString> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ConstUserOpConf_ListString>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::UserOpConf_ListString>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::UserOpConf_ListString> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, ::oneflow::cfg::UserOpConf_ListString>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::AttrValue> {
 public:
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_();
  ~Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::AttrValue& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstAttrValue> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ConstAttrValue>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ final : public Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_ {
 public:
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::AttrValue>>& data);
  _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_();
  ~_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::AttrValue> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, ::oneflow::cfg::AttrValue>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstUserOpConf_ListString> {
  std::size_t operator()(const ::oneflow::cfg::ConstUserOpConf_ListString& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::UserOpConf_ListString> {
  std::size_t operator()(const ::oneflow::cfg::UserOpConf_ListString& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstUserOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstUserOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::UserOpConf> {
  std::size_t operator()(const ::oneflow::cfg::UserOpConf& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H_