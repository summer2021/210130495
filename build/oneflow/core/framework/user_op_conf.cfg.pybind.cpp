#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/framework/user_op_conf.cfg.h"
#include "oneflow/core/framework/user_op_attr.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.framework.user_op_conf", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstUserOpConf_ListString>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstUserOpConf_ListString>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstUserOpConf_ListString> (Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<UserOpConf_ListString>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<UserOpConf_ListString>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::UserOpConf_ListString> (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>> registry(m, "Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstAttrValue>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstAttrValue> (Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_, std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>> registry(m, "_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const _CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<AttrValue>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::AttrValue> (_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_::__SharedMutable__);
  }

  {
    pybind11::class_<ConstUserOpConf_ListString, ::oneflow::cfg::Message, std::shared_ptr<ConstUserOpConf_ListString>> registry(m, "ConstUserOpConf_ListString");
    registry.def("__id__", &::oneflow::cfg::UserOpConf_ListString::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstUserOpConf_ListString::DebugString);
    registry.def("__repr__", &ConstUserOpConf_ListString::DebugString);

    registry.def("s_size", &ConstUserOpConf_ListString::s_size);
    registry.def("s", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstUserOpConf_ListString::*)() const)&ConstUserOpConf_ListString::shared_const_s);
    registry.def("s", (const ::std::string& (ConstUserOpConf_ListString::*)(::std::size_t) const)&ConstUserOpConf_ListString::s);
  }
  {
    pybind11::class_<::oneflow::cfg::UserOpConf_ListString, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::UserOpConf_ListString>> registry(m, "UserOpConf_ListString");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::UserOpConf_ListString::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::UserOpConf_ListString::*)(const ConstUserOpConf_ListString&))&::oneflow::cfg::UserOpConf_ListString::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::UserOpConf_ListString::*)(const ::oneflow::cfg::UserOpConf_ListString&))&::oneflow::cfg::UserOpConf_ListString::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::UserOpConf_ListString::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::UserOpConf_ListString::DebugString);
    registry.def("__repr__", &::oneflow::cfg::UserOpConf_ListString::DebugString);



    registry.def("s_size", &::oneflow::cfg::UserOpConf_ListString::s_size);
    registry.def("clear_s", &::oneflow::cfg::UserOpConf_ListString::clear_s);
    registry.def("mutable_s", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::UserOpConf_ListString::*)())&::oneflow::cfg::UserOpConf_ListString::shared_mutable_s);
    registry.def("s", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::UserOpConf_ListString::*)() const)&::oneflow::cfg::UserOpConf_ListString::shared_const_s);
    registry.def("s", (const ::std::string& (::oneflow::cfg::UserOpConf_ListString::*)(::std::size_t) const)&::oneflow::cfg::UserOpConf_ListString::s);
    registry.def("add_s", &::oneflow::cfg::UserOpConf_ListString::add_s);
    registry.def("set_s", &::oneflow::cfg::UserOpConf_ListString::set_s);
  }
  {
    pybind11::class_<ConstUserOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstUserOpConf>> registry(m, "ConstUserOpConf");
    registry.def("__id__", &::oneflow::cfg::UserOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstUserOpConf::DebugString);
    registry.def("__repr__", &ConstUserOpConf::DebugString);

    registry.def("has_op_type_name", &ConstUserOpConf::has_op_type_name);
    registry.def("op_type_name", &ConstUserOpConf::op_type_name);

    registry.def("input_size", &ConstUserOpConf::input_size);
    registry.def("input", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (ConstUserOpConf::*)() const)&ConstUserOpConf::shared_const_input);

    registry.def("input", (::std::shared_ptr<ConstUserOpConf_ListString> (ConstUserOpConf::*)(const ::std::string&) const)&ConstUserOpConf::shared_const_input);

    registry.def("output_size", &ConstUserOpConf::output_size);
    registry.def("output", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (ConstUserOpConf::*)() const)&ConstUserOpConf::shared_const_output);

    registry.def("output", (::std::shared_ptr<ConstUserOpConf_ListString> (ConstUserOpConf::*)(const ::std::string&) const)&ConstUserOpConf::shared_const_output);

    registry.def("attr_size", &ConstUserOpConf::attr_size);
    registry.def("attr", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> (ConstUserOpConf::*)() const)&ConstUserOpConf::shared_const_attr);

    registry.def("attr", (::std::shared_ptr<ConstAttrValue> (ConstUserOpConf::*)(const ::std::string&) const)&ConstUserOpConf::shared_const_attr);
  }
  {
    pybind11::class_<::oneflow::cfg::UserOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::UserOpConf>> registry(m, "UserOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::UserOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::UserOpConf::*)(const ConstUserOpConf&))&::oneflow::cfg::UserOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::UserOpConf::*)(const ::oneflow::cfg::UserOpConf&))&::oneflow::cfg::UserOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::UserOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::UserOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::UserOpConf::DebugString);



    registry.def("has_op_type_name", &::oneflow::cfg::UserOpConf::has_op_type_name);
    registry.def("clear_op_type_name", &::oneflow::cfg::UserOpConf::clear_op_type_name);
    registry.def("op_type_name", &::oneflow::cfg::UserOpConf::op_type_name);
    registry.def("set_op_type_name", &::oneflow::cfg::UserOpConf::set_op_type_name);

    registry.def("input_size", &::oneflow::cfg::UserOpConf::input_size);
    registry.def("input", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (::oneflow::cfg::UserOpConf::*)() const)&::oneflow::cfg::UserOpConf::shared_const_input);
    registry.def("clear_input", &::oneflow::cfg::UserOpConf::clear_input);
    registry.def("mutable_input", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (::oneflow::cfg::UserOpConf::*)())&::oneflow::cfg::UserOpConf::shared_mutable_input);
    registry.def("input", (::std::shared_ptr<ConstUserOpConf_ListString> (::oneflow::cfg::UserOpConf::*)(const ::std::string&) const)&::oneflow::cfg::UserOpConf::shared_const_input);

    registry.def("output_size", &::oneflow::cfg::UserOpConf::output_size);
    registry.def("output", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (::oneflow::cfg::UserOpConf::*)() const)&::oneflow::cfg::UserOpConf::shared_const_output);
    registry.def("clear_output", &::oneflow::cfg::UserOpConf::clear_output);
    registry.def("mutable_output", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_UserOpConf_ListString_> (::oneflow::cfg::UserOpConf::*)())&::oneflow::cfg::UserOpConf::shared_mutable_output);
    registry.def("output", (::std::shared_ptr<ConstUserOpConf_ListString> (::oneflow::cfg::UserOpConf::*)(const ::std::string&) const)&::oneflow::cfg::UserOpConf::shared_const_output);

    registry.def("attr_size", &::oneflow::cfg::UserOpConf::attr_size);
    registry.def("attr", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::UserOpConf::*)() const)&::oneflow::cfg::UserOpConf::shared_const_attr);
    registry.def("clear_attr", &::oneflow::cfg::UserOpConf::clear_attr);
    registry.def("mutable_attr", (::std::shared_ptr<_CFG_ONEFLOW_CORE_FRAMEWORK_USER_OP_CONF_CFG_H__MapField___std__string_AttrValue_> (::oneflow::cfg::UserOpConf::*)())&::oneflow::cfg::UserOpConf::shared_mutable_attr);
    registry.def("attr", (::std::shared_ptr<ConstAttrValue> (::oneflow::cfg::UserOpConf::*)(const ::std::string&) const)&::oneflow::cfg::UserOpConf::shared_const_attr);
  }
}