#include "oneflow/core/record/record.cfg.h"
#include "oneflow/core/record/record.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstBytesList::_BytesList_::_BytesList_() { Clear(); }
ConstBytesList::_BytesList_::_BytesList_(const _BytesList_& other) { CopyFrom(other); }
ConstBytesList::_BytesList_::_BytesList_(const ::oneflow::BytesList& proto_byteslist) {
  InitFromProto(proto_byteslist);
}
ConstBytesList::_BytesList_::_BytesList_(_BytesList_&& other) = default;
ConstBytesList::_BytesList_::~_BytesList_() = default;

void ConstBytesList::_BytesList_::InitFromProto(const ::oneflow::BytesList& proto_byteslist) {
  Clear();
  // repeated field: value
  if (!proto_byteslist.value().empty()) {
    for (const ::std::string& elem : proto_byteslist.value()) {
      add_value(elem);
    }
  }
    
}

void ConstBytesList::_BytesList_::ToProto(::oneflow::BytesList* proto_byteslist) const {
  proto_byteslist->Clear();
  // repeated field: value
  if (!value().empty()) {
    for (const ::std::string& elem : value()) {
      proto_byteslist->add_value(elem);
    }
  }

}

::std::string ConstBytesList::_BytesList_::DebugString() const {
  ::oneflow::BytesList proto_byteslist;
  this->ToProto(&proto_byteslist);
  return proto_byteslist.DebugString();
}

void ConstBytesList::_BytesList_::Clear() {
  clear_value();
}

void ConstBytesList::_BytesList_::CopyFrom(const _BytesList_& other) {
  mutable_value()->CopyFrom(other.value());
}


// repeated field value
::std::size_t ConstBytesList::_BytesList_::value_size() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return value_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& ConstBytesList::_BytesList_::value() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(value_.get());
}
const ::std::string& ConstBytesList::_BytesList_::value(::std::size_t index) const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return value_->Get(index);
}
void ConstBytesList::_BytesList_::clear_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
  }
  return value_->Clear();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_* ConstBytesList::_BytesList_::mutable_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
  }
  return  value_.get();
}
::std::string* ConstBytesList::_BytesList_::mutable_value(::std::size_t index) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
  }
  return  value_->Mutable(index);
}
void ConstBytesList::_BytesList_::add_value(const ::std::string& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
  }
  return value_->Add(value);
}
void ConstBytesList::_BytesList_::set_value(::std::size_t index, const ::std::string& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>();
  }
  return value_->Set(index, value);
}


int ConstBytesList::_BytesList_::compare(const _BytesList_& other) {
  if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstBytesList::_BytesList_::operator==(const _BytesList_& other) const {
  return true
    && value() == other.value()
  ;
}

std::size_t ConstBytesList::_BytesList_::__CalcHash__() const {
  return 0
    ^ value().__CalcHash__()
  ;
}

bool ConstBytesList::_BytesList_::operator<(const _BytesList_& other) const {
  return false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _BytesList_ =  ConstBytesList::_BytesList_;
ConstBytesList::ConstBytesList(const ::std::shared_ptr<_BytesList_>& data): data_(data) {}
ConstBytesList::ConstBytesList(): data_(::std::make_shared<_BytesList_>()) {}
ConstBytesList::ConstBytesList(const ::oneflow::BytesList& proto_byteslist) {
  BuildFromProto(proto_byteslist);
}
ConstBytesList::ConstBytesList(const ConstBytesList&) = default;
ConstBytesList::ConstBytesList(ConstBytesList&&) noexcept = default;
ConstBytesList::~ConstBytesList() = default;

void ConstBytesList::ToProto(PbMessage* proto_byteslist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::BytesList*>(proto_byteslist));
}
  
::std::string ConstBytesList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstBytesList::__Empty__() const {
  return !data_;
}

int ConstBytesList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstBytesList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstBytesList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstBytesList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// repeated field value
::std::size_t ConstBytesList::value_size() const {
  return __SharedPtrOrDefault__()->value_size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& ConstBytesList::value() const {
  return __SharedPtrOrDefault__()->value();
}
const ::std::string& ConstBytesList::value(::std::size_t index) const {
  return __SharedPtrOrDefault__()->value(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> ConstBytesList::shared_const_value() const {
  return value().__SharedConst__();
}

::std::shared_ptr<ConstBytesList> ConstBytesList::__SharedConst__() const {
  return ::std::make_shared<ConstBytesList>(data_);
}
int64_t ConstBytesList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstBytesList::operator==(const ConstBytesList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstBytesList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstBytesList::operator<(const ConstBytesList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_BytesList_>& ConstBytesList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_BytesList_> default_ptr = std::make_shared<_BytesList_>();
  return default_ptr;
}
const ::std::shared_ptr<_BytesList_>& ConstBytesList::__SharedPtr__() {
  if (!data_) { data_.reset(new _BytesList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstBytesList
void ConstBytesList::BuildFromProto(const PbMessage& proto_byteslist) {
  data_ = ::std::make_shared<_BytesList_>(dynamic_cast<const ::oneflow::BytesList&>(proto_byteslist));
}

BytesList::BytesList(const ::std::shared_ptr<_BytesList_>& data)
  : ConstBytesList(data) {}
BytesList::BytesList(const BytesList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<BytesList> resize
BytesList::BytesList(BytesList&&) noexcept = default; 
BytesList::BytesList(const ::oneflow::BytesList& proto_byteslist) {
  InitFromProto(proto_byteslist);
}
BytesList::BytesList() = default;

BytesList::~BytesList() = default;

void BytesList::InitFromProto(const PbMessage& proto_byteslist) {
  BuildFromProto(proto_byteslist);
}
  
void* BytesList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool BytesList::operator==(const BytesList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t BytesList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool BytesList::operator<(const BytesList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void BytesList::Clear() {
  if (data_) { data_.reset(); }
}
void BytesList::CopyFrom(const BytesList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
BytesList& BytesList::operator=(const BytesList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field value
void BytesList::clear_value() {
  return __SharedPtr__()->clear_value();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_* BytesList::mutable_value() {
  return __SharedPtr__()->mutable_value();
}
::std::string* BytesList::mutable_value(::std::size_t index) {
  return __SharedPtr__()->mutable_value(index);
}
void BytesList::add_value(const ::std::string& value) {
  return __SharedPtr__()->add_value(value);
}
void BytesList::set_value(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_value(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> BytesList::shared_mutable_value() {
  return mutable_value()->__SharedMutable__();
}

::std::shared_ptr<BytesList> BytesList::__SharedMutable__() {
  return ::std::make_shared<BytesList>(__SharedPtr__());
}
ConstFloatList::_FloatList_::_FloatList_() { Clear(); }
ConstFloatList::_FloatList_::_FloatList_(const _FloatList_& other) { CopyFrom(other); }
ConstFloatList::_FloatList_::_FloatList_(const ::oneflow::FloatList& proto_floatlist) {
  InitFromProto(proto_floatlist);
}
ConstFloatList::_FloatList_::_FloatList_(_FloatList_&& other) = default;
ConstFloatList::_FloatList_::~_FloatList_() = default;

void ConstFloatList::_FloatList_::InitFromProto(const ::oneflow::FloatList& proto_floatlist) {
  Clear();
  // repeated field: value
  if (!proto_floatlist.value().empty()) {
    for (const float& elem : proto_floatlist.value()) {
      add_value(elem);
    }
  }
    
}

void ConstFloatList::_FloatList_::ToProto(::oneflow::FloatList* proto_floatlist) const {
  proto_floatlist->Clear();
  // repeated field: value
  if (!value().empty()) {
    for (const float& elem : value()) {
      proto_floatlist->add_value(elem);
    }
  }

}

::std::string ConstFloatList::_FloatList_::DebugString() const {
  ::oneflow::FloatList proto_floatlist;
  this->ToProto(&proto_floatlist);
  return proto_floatlist.DebugString();
}

void ConstFloatList::_FloatList_::Clear() {
  clear_value();
}

void ConstFloatList::_FloatList_::CopyFrom(const _FloatList_& other) {
  mutable_value()->CopyFrom(other.value());
}


// repeated field value
::std::size_t ConstFloatList::_FloatList_::value_size() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
    return default_static_value->size();
  }
  return value_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& ConstFloatList::_FloatList_::value() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
    return *(default_static_value.get());
  }
  return *(value_.get());
}
const float& ConstFloatList::_FloatList_::value(::std::size_t index) const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
    return default_static_value->Get(index);
  }
  return value_->Get(index);
}
void ConstFloatList::_FloatList_::clear_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
  }
  return value_->Clear();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_* ConstFloatList::_FloatList_::mutable_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
  }
  return  value_.get();
}
float* ConstFloatList::_FloatList_::mutable_value(::std::size_t index) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
  }
  return  value_->Mutable(index);
}
void ConstFloatList::_FloatList_::add_value(const float& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
  }
  return value_->Add(value);
}
void ConstFloatList::_FloatList_::set_value(::std::size_t index, const float& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>();
  }
  return value_->Set(index, value);
}


int ConstFloatList::_FloatList_::compare(const _FloatList_& other) {
  if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstFloatList::_FloatList_::operator==(const _FloatList_& other) const {
  return true
    && value() == other.value()
  ;
}

std::size_t ConstFloatList::_FloatList_::__CalcHash__() const {
  return 0
    ^ value().__CalcHash__()
  ;
}

bool ConstFloatList::_FloatList_::operator<(const _FloatList_& other) const {
  return false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _FloatList_ =  ConstFloatList::_FloatList_;
ConstFloatList::ConstFloatList(const ::std::shared_ptr<_FloatList_>& data): data_(data) {}
ConstFloatList::ConstFloatList(): data_(::std::make_shared<_FloatList_>()) {}
ConstFloatList::ConstFloatList(const ::oneflow::FloatList& proto_floatlist) {
  BuildFromProto(proto_floatlist);
}
ConstFloatList::ConstFloatList(const ConstFloatList&) = default;
ConstFloatList::ConstFloatList(ConstFloatList&&) noexcept = default;
ConstFloatList::~ConstFloatList() = default;

void ConstFloatList::ToProto(PbMessage* proto_floatlist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::FloatList*>(proto_floatlist));
}
  
::std::string ConstFloatList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstFloatList::__Empty__() const {
  return !data_;
}

int ConstFloatList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstFloatList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstFloatList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<float>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstFloatList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// repeated field value
::std::size_t ConstFloatList::value_size() const {
  return __SharedPtrOrDefault__()->value_size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& ConstFloatList::value() const {
  return __SharedPtrOrDefault__()->value();
}
const float& ConstFloatList::value(::std::size_t index) const {
  return __SharedPtrOrDefault__()->value(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> ConstFloatList::shared_const_value() const {
  return value().__SharedConst__();
}

::std::shared_ptr<ConstFloatList> ConstFloatList::__SharedConst__() const {
  return ::std::make_shared<ConstFloatList>(data_);
}
int64_t ConstFloatList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstFloatList::operator==(const ConstFloatList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstFloatList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstFloatList::operator<(const ConstFloatList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_FloatList_>& ConstFloatList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_FloatList_> default_ptr = std::make_shared<_FloatList_>();
  return default_ptr;
}
const ::std::shared_ptr<_FloatList_>& ConstFloatList::__SharedPtr__() {
  if (!data_) { data_.reset(new _FloatList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstFloatList
void ConstFloatList::BuildFromProto(const PbMessage& proto_floatlist) {
  data_ = ::std::make_shared<_FloatList_>(dynamic_cast<const ::oneflow::FloatList&>(proto_floatlist));
}

FloatList::FloatList(const ::std::shared_ptr<_FloatList_>& data)
  : ConstFloatList(data) {}
FloatList::FloatList(const FloatList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<FloatList> resize
FloatList::FloatList(FloatList&&) noexcept = default; 
FloatList::FloatList(const ::oneflow::FloatList& proto_floatlist) {
  InitFromProto(proto_floatlist);
}
FloatList::FloatList() = default;

FloatList::~FloatList() = default;

void FloatList::InitFromProto(const PbMessage& proto_floatlist) {
  BuildFromProto(proto_floatlist);
}
  
void* FloatList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool FloatList::operator==(const FloatList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t FloatList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool FloatList::operator<(const FloatList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void FloatList::Clear() {
  if (data_) { data_.reset(); }
}
void FloatList::CopyFrom(const FloatList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
FloatList& FloatList::operator=(const FloatList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field value
void FloatList::clear_value() {
  return __SharedPtr__()->clear_value();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_* FloatList::mutable_value() {
  return __SharedPtr__()->mutable_value();
}
float* FloatList::mutable_value(::std::size_t index) {
  return __SharedPtr__()->mutable_value(index);
}
void FloatList::add_value(const float& value) {
  return __SharedPtr__()->add_value(value);
}
void FloatList::set_value(::std::size_t index, const float& value) {
  return __SharedPtr__()->set_value(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> FloatList::shared_mutable_value() {
  return mutable_value()->__SharedMutable__();
}

::std::shared_ptr<FloatList> FloatList::__SharedMutable__() {
  return ::std::make_shared<FloatList>(__SharedPtr__());
}
ConstDoubleList::_DoubleList_::_DoubleList_() { Clear(); }
ConstDoubleList::_DoubleList_::_DoubleList_(const _DoubleList_& other) { CopyFrom(other); }
ConstDoubleList::_DoubleList_::_DoubleList_(const ::oneflow::DoubleList& proto_doublelist) {
  InitFromProto(proto_doublelist);
}
ConstDoubleList::_DoubleList_::_DoubleList_(_DoubleList_&& other) = default;
ConstDoubleList::_DoubleList_::~_DoubleList_() = default;

void ConstDoubleList::_DoubleList_::InitFromProto(const ::oneflow::DoubleList& proto_doublelist) {
  Clear();
  // repeated field: value
  if (!proto_doublelist.value().empty()) {
    for (const double& elem : proto_doublelist.value()) {
      add_value(elem);
    }
  }
    
}

void ConstDoubleList::_DoubleList_::ToProto(::oneflow::DoubleList* proto_doublelist) const {
  proto_doublelist->Clear();
  // repeated field: value
  if (!value().empty()) {
    for (const double& elem : value()) {
      proto_doublelist->add_value(elem);
    }
  }

}

::std::string ConstDoubleList::_DoubleList_::DebugString() const {
  ::oneflow::DoubleList proto_doublelist;
  this->ToProto(&proto_doublelist);
  return proto_doublelist.DebugString();
}

void ConstDoubleList::_DoubleList_::Clear() {
  clear_value();
}

void ConstDoubleList::_DoubleList_::CopyFrom(const _DoubleList_& other) {
  mutable_value()->CopyFrom(other.value());
}


// repeated field value
::std::size_t ConstDoubleList::_DoubleList_::value_size() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
    return default_static_value->size();
  }
  return value_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& ConstDoubleList::_DoubleList_::value() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
    return *(default_static_value.get());
  }
  return *(value_.get());
}
const double& ConstDoubleList::_DoubleList_::value(::std::size_t index) const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
    return default_static_value->Get(index);
  }
  return value_->Get(index);
}
void ConstDoubleList::_DoubleList_::clear_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
  }
  return value_->Clear();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_* ConstDoubleList::_DoubleList_::mutable_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
  }
  return  value_.get();
}
double* ConstDoubleList::_DoubleList_::mutable_value(::std::size_t index) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
  }
  return  value_->Mutable(index);
}
void ConstDoubleList::_DoubleList_::add_value(const double& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
  }
  return value_->Add(value);
}
void ConstDoubleList::_DoubleList_::set_value(::std::size_t index, const double& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>();
  }
  return value_->Set(index, value);
}


int ConstDoubleList::_DoubleList_::compare(const _DoubleList_& other) {
  if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstDoubleList::_DoubleList_::operator==(const _DoubleList_& other) const {
  return true
    && value() == other.value()
  ;
}

std::size_t ConstDoubleList::_DoubleList_::__CalcHash__() const {
  return 0
    ^ value().__CalcHash__()
  ;
}

bool ConstDoubleList::_DoubleList_::operator<(const _DoubleList_& other) const {
  return false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _DoubleList_ =  ConstDoubleList::_DoubleList_;
ConstDoubleList::ConstDoubleList(const ::std::shared_ptr<_DoubleList_>& data): data_(data) {}
ConstDoubleList::ConstDoubleList(): data_(::std::make_shared<_DoubleList_>()) {}
ConstDoubleList::ConstDoubleList(const ::oneflow::DoubleList& proto_doublelist) {
  BuildFromProto(proto_doublelist);
}
ConstDoubleList::ConstDoubleList(const ConstDoubleList&) = default;
ConstDoubleList::ConstDoubleList(ConstDoubleList&&) noexcept = default;
ConstDoubleList::~ConstDoubleList() = default;

void ConstDoubleList::ToProto(PbMessage* proto_doublelist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::DoubleList*>(proto_doublelist));
}
  
::std::string ConstDoubleList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstDoubleList::__Empty__() const {
  return !data_;
}

int ConstDoubleList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstDoubleList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstDoubleList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<double>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstDoubleList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// repeated field value
::std::size_t ConstDoubleList::value_size() const {
  return __SharedPtrOrDefault__()->value_size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& ConstDoubleList::value() const {
  return __SharedPtrOrDefault__()->value();
}
const double& ConstDoubleList::value(::std::size_t index) const {
  return __SharedPtrOrDefault__()->value(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> ConstDoubleList::shared_const_value() const {
  return value().__SharedConst__();
}

::std::shared_ptr<ConstDoubleList> ConstDoubleList::__SharedConst__() const {
  return ::std::make_shared<ConstDoubleList>(data_);
}
int64_t ConstDoubleList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstDoubleList::operator==(const ConstDoubleList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstDoubleList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstDoubleList::operator<(const ConstDoubleList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_DoubleList_>& ConstDoubleList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_DoubleList_> default_ptr = std::make_shared<_DoubleList_>();
  return default_ptr;
}
const ::std::shared_ptr<_DoubleList_>& ConstDoubleList::__SharedPtr__() {
  if (!data_) { data_.reset(new _DoubleList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstDoubleList
void ConstDoubleList::BuildFromProto(const PbMessage& proto_doublelist) {
  data_ = ::std::make_shared<_DoubleList_>(dynamic_cast<const ::oneflow::DoubleList&>(proto_doublelist));
}

DoubleList::DoubleList(const ::std::shared_ptr<_DoubleList_>& data)
  : ConstDoubleList(data) {}
DoubleList::DoubleList(const DoubleList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<DoubleList> resize
DoubleList::DoubleList(DoubleList&&) noexcept = default; 
DoubleList::DoubleList(const ::oneflow::DoubleList& proto_doublelist) {
  InitFromProto(proto_doublelist);
}
DoubleList::DoubleList() = default;

DoubleList::~DoubleList() = default;

void DoubleList::InitFromProto(const PbMessage& proto_doublelist) {
  BuildFromProto(proto_doublelist);
}
  
void* DoubleList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool DoubleList::operator==(const DoubleList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t DoubleList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool DoubleList::operator<(const DoubleList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void DoubleList::Clear() {
  if (data_) { data_.reset(); }
}
void DoubleList::CopyFrom(const DoubleList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
DoubleList& DoubleList::operator=(const DoubleList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field value
void DoubleList::clear_value() {
  return __SharedPtr__()->clear_value();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_* DoubleList::mutable_value() {
  return __SharedPtr__()->mutable_value();
}
double* DoubleList::mutable_value(::std::size_t index) {
  return __SharedPtr__()->mutable_value(index);
}
void DoubleList::add_value(const double& value) {
  return __SharedPtr__()->add_value(value);
}
void DoubleList::set_value(::std::size_t index, const double& value) {
  return __SharedPtr__()->set_value(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> DoubleList::shared_mutable_value() {
  return mutable_value()->__SharedMutable__();
}

::std::shared_ptr<DoubleList> DoubleList::__SharedMutable__() {
  return ::std::make_shared<DoubleList>(__SharedPtr__());
}
ConstInt32List::_Int32List_::_Int32List_() { Clear(); }
ConstInt32List::_Int32List_::_Int32List_(const _Int32List_& other) { CopyFrom(other); }
ConstInt32List::_Int32List_::_Int32List_(const ::oneflow::Int32List& proto_int32list) {
  InitFromProto(proto_int32list);
}
ConstInt32List::_Int32List_::_Int32List_(_Int32List_&& other) = default;
ConstInt32List::_Int32List_::~_Int32List_() = default;

void ConstInt32List::_Int32List_::InitFromProto(const ::oneflow::Int32List& proto_int32list) {
  Clear();
  // repeated field: value
  if (!proto_int32list.value().empty()) {
    for (const int32_t& elem : proto_int32list.value()) {
      add_value(elem);
    }
  }
    
}

void ConstInt32List::_Int32List_::ToProto(::oneflow::Int32List* proto_int32list) const {
  proto_int32list->Clear();
  // repeated field: value
  if (!value().empty()) {
    for (const int32_t& elem : value()) {
      proto_int32list->add_value(elem);
    }
  }

}

::std::string ConstInt32List::_Int32List_::DebugString() const {
  ::oneflow::Int32List proto_int32list;
  this->ToProto(&proto_int32list);
  return proto_int32list.DebugString();
}

void ConstInt32List::_Int32List_::Clear() {
  clear_value();
}

void ConstInt32List::_Int32List_::CopyFrom(const _Int32List_& other) {
  mutable_value()->CopyFrom(other.value());
}


// repeated field value
::std::size_t ConstInt32List::_Int32List_::value_size() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->size();
  }
  return value_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& ConstInt32List::_Int32List_::value() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
    return *(default_static_value.get());
  }
  return *(value_.get());
}
const int32_t& ConstInt32List::_Int32List_::value(::std::size_t index) const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
    return default_static_value->Get(index);
  }
  return value_->Get(index);
}
void ConstInt32List::_Int32List_::clear_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
  }
  return value_->Clear();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_* ConstInt32List::_Int32List_::mutable_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
  }
  return  value_.get();
}
int32_t* ConstInt32List::_Int32List_::mutable_value(::std::size_t index) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
  }
  return  value_->Mutable(index);
}
void ConstInt32List::_Int32List_::add_value(const int32_t& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
  }
  return value_->Add(value);
}
void ConstInt32List::_Int32List_::set_value(::std::size_t index, const int32_t& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>();
  }
  return value_->Set(index, value);
}


int ConstInt32List::_Int32List_::compare(const _Int32List_& other) {
  if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstInt32List::_Int32List_::operator==(const _Int32List_& other) const {
  return true
    && value() == other.value()
  ;
}

std::size_t ConstInt32List::_Int32List_::__CalcHash__() const {
  return 0
    ^ value().__CalcHash__()
  ;
}

bool ConstInt32List::_Int32List_::operator<(const _Int32List_& other) const {
  return false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _Int32List_ =  ConstInt32List::_Int32List_;
ConstInt32List::ConstInt32List(const ::std::shared_ptr<_Int32List_>& data): data_(data) {}
ConstInt32List::ConstInt32List(): data_(::std::make_shared<_Int32List_>()) {}
ConstInt32List::ConstInt32List(const ::oneflow::Int32List& proto_int32list) {
  BuildFromProto(proto_int32list);
}
ConstInt32List::ConstInt32List(const ConstInt32List&) = default;
ConstInt32List::ConstInt32List(ConstInt32List&&) noexcept = default;
ConstInt32List::~ConstInt32List() = default;

void ConstInt32List::ToProto(PbMessage* proto_int32list) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::Int32List*>(proto_int32list));
}
  
::std::string ConstInt32List::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInt32List::__Empty__() const {
  return !data_;
}

int ConstInt32List::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInt32List::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInt32List::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int32_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInt32List::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// repeated field value
::std::size_t ConstInt32List::value_size() const {
  return __SharedPtrOrDefault__()->value_size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& ConstInt32List::value() const {
  return __SharedPtrOrDefault__()->value();
}
const int32_t& ConstInt32List::value(::std::size_t index) const {
  return __SharedPtrOrDefault__()->value(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> ConstInt32List::shared_const_value() const {
  return value().__SharedConst__();
}

::std::shared_ptr<ConstInt32List> ConstInt32List::__SharedConst__() const {
  return ::std::make_shared<ConstInt32List>(data_);
}
int64_t ConstInt32List::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInt32List::operator==(const ConstInt32List& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInt32List::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInt32List::operator<(const ConstInt32List& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_Int32List_>& ConstInt32List::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_Int32List_> default_ptr = std::make_shared<_Int32List_>();
  return default_ptr;
}
const ::std::shared_ptr<_Int32List_>& ConstInt32List::__SharedPtr__() {
  if (!data_) { data_.reset(new _Int32List_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInt32List
void ConstInt32List::BuildFromProto(const PbMessage& proto_int32list) {
  data_ = ::std::make_shared<_Int32List_>(dynamic_cast<const ::oneflow::Int32List&>(proto_int32list));
}

Int32List::Int32List(const ::std::shared_ptr<_Int32List_>& data)
  : ConstInt32List(data) {}
Int32List::Int32List(const Int32List& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<Int32List> resize
Int32List::Int32List(Int32List&&) noexcept = default; 
Int32List::Int32List(const ::oneflow::Int32List& proto_int32list) {
  InitFromProto(proto_int32list);
}
Int32List::Int32List() = default;

Int32List::~Int32List() = default;

void Int32List::InitFromProto(const PbMessage& proto_int32list) {
  BuildFromProto(proto_int32list);
}
  
void* Int32List::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool Int32List::operator==(const Int32List& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t Int32List::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool Int32List::operator<(const Int32List& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void Int32List::Clear() {
  if (data_) { data_.reset(); }
}
void Int32List::CopyFrom(const Int32List& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
Int32List& Int32List::operator=(const Int32List& other) {
  CopyFrom(other);
  return *this;
}

// repeated field value
void Int32List::clear_value() {
  return __SharedPtr__()->clear_value();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_* Int32List::mutable_value() {
  return __SharedPtr__()->mutable_value();
}
int32_t* Int32List::mutable_value(::std::size_t index) {
  return __SharedPtr__()->mutable_value(index);
}
void Int32List::add_value(const int32_t& value) {
  return __SharedPtr__()->add_value(value);
}
void Int32List::set_value(::std::size_t index, const int32_t& value) {
  return __SharedPtr__()->set_value(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> Int32List::shared_mutable_value() {
  return mutable_value()->__SharedMutable__();
}

::std::shared_ptr<Int32List> Int32List::__SharedMutable__() {
  return ::std::make_shared<Int32List>(__SharedPtr__());
}
ConstInt64List::_Int64List_::_Int64List_() { Clear(); }
ConstInt64List::_Int64List_::_Int64List_(const _Int64List_& other) { CopyFrom(other); }
ConstInt64List::_Int64List_::_Int64List_(const ::oneflow::Int64List& proto_int64list) {
  InitFromProto(proto_int64list);
}
ConstInt64List::_Int64List_::_Int64List_(_Int64List_&& other) = default;
ConstInt64List::_Int64List_::~_Int64List_() = default;

void ConstInt64List::_Int64List_::InitFromProto(const ::oneflow::Int64List& proto_int64list) {
  Clear();
  // repeated field: value
  if (!proto_int64list.value().empty()) {
    for (const int64_t& elem : proto_int64list.value()) {
      add_value(elem);
    }
  }
    
}

void ConstInt64List::_Int64List_::ToProto(::oneflow::Int64List* proto_int64list) const {
  proto_int64list->Clear();
  // repeated field: value
  if (!value().empty()) {
    for (const int64_t& elem : value()) {
      proto_int64list->add_value(elem);
    }
  }

}

::std::string ConstInt64List::_Int64List_::DebugString() const {
  ::oneflow::Int64List proto_int64list;
  this->ToProto(&proto_int64list);
  return proto_int64list.DebugString();
}

void ConstInt64List::_Int64List_::Clear() {
  clear_value();
}

void ConstInt64List::_Int64List_::CopyFrom(const _Int64List_& other) {
  mutable_value()->CopyFrom(other.value());
}


// repeated field value
::std::size_t ConstInt64List::_Int64List_::value_size() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->size();
  }
  return value_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& ConstInt64List::_Int64List_::value() const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
    return *(default_static_value.get());
  }
  return *(value_.get());
}
const int64_t& ConstInt64List::_Int64List_::value(::std::size_t index) const {
  if (!value_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
    return default_static_value->Get(index);
  }
  return value_->Get(index);
}
void ConstInt64List::_Int64List_::clear_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
  }
  return value_->Clear();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_* ConstInt64List::_Int64List_::mutable_value() {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
  }
  return  value_.get();
}
int64_t* ConstInt64List::_Int64List_::mutable_value(::std::size_t index) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
  }
  return  value_->Mutable(index);
}
void ConstInt64List::_Int64List_::add_value(const int64_t& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
  }
  return value_->Add(value);
}
void ConstInt64List::_Int64List_::set_value(::std::size_t index, const int64_t& value) {
  if (!value_) {
    value_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>();
  }
  return value_->Set(index, value);
}


int ConstInt64List::_Int64List_::compare(const _Int64List_& other) {
  if (!(value() == other.value())) {
    return value() < other.value() ? -1 : 1;
  }
  return 0;
}

bool ConstInt64List::_Int64List_::operator==(const _Int64List_& other) const {
  return true
    && value() == other.value()
  ;
}

std::size_t ConstInt64List::_Int64List_::__CalcHash__() const {
  return 0
    ^ value().__CalcHash__()
  ;
}

bool ConstInt64List::_Int64List_::operator<(const _Int64List_& other) const {
  return false
    || !(value() == other.value()) ? 
      value() < other.value() : false
  ;
}

using _Int64List_ =  ConstInt64List::_Int64List_;
ConstInt64List::ConstInt64List(const ::std::shared_ptr<_Int64List_>& data): data_(data) {}
ConstInt64List::ConstInt64List(): data_(::std::make_shared<_Int64List_>()) {}
ConstInt64List::ConstInt64List(const ::oneflow::Int64List& proto_int64list) {
  BuildFromProto(proto_int64list);
}
ConstInt64List::ConstInt64List(const ConstInt64List&) = default;
ConstInt64List::ConstInt64List(ConstInt64List&&) noexcept = default;
ConstInt64List::~ConstInt64List() = default;

void ConstInt64List::ToProto(PbMessage* proto_int64list) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::Int64List*>(proto_int64list));
}
  
::std::string ConstInt64List::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInt64List::__Empty__() const {
  return !data_;
}

int ConstInt64List::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"value", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInt64List::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInt64List::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<int64_t>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInt64List::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &value();
    default: return nullptr;
  }
}

// repeated field value
::std::size_t ConstInt64List::value_size() const {
  return __SharedPtrOrDefault__()->value_size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& ConstInt64List::value() const {
  return __SharedPtrOrDefault__()->value();
}
const int64_t& ConstInt64List::value(::std::size_t index) const {
  return __SharedPtrOrDefault__()->value(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> ConstInt64List::shared_const_value() const {
  return value().__SharedConst__();
}

::std::shared_ptr<ConstInt64List> ConstInt64List::__SharedConst__() const {
  return ::std::make_shared<ConstInt64List>(data_);
}
int64_t ConstInt64List::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInt64List::operator==(const ConstInt64List& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInt64List::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInt64List::operator<(const ConstInt64List& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_Int64List_>& ConstInt64List::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_Int64List_> default_ptr = std::make_shared<_Int64List_>();
  return default_ptr;
}
const ::std::shared_ptr<_Int64List_>& ConstInt64List::__SharedPtr__() {
  if (!data_) { data_.reset(new _Int64List_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInt64List
void ConstInt64List::BuildFromProto(const PbMessage& proto_int64list) {
  data_ = ::std::make_shared<_Int64List_>(dynamic_cast<const ::oneflow::Int64List&>(proto_int64list));
}

Int64List::Int64List(const ::std::shared_ptr<_Int64List_>& data)
  : ConstInt64List(data) {}
Int64List::Int64List(const Int64List& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<Int64List> resize
Int64List::Int64List(Int64List&&) noexcept = default; 
Int64List::Int64List(const ::oneflow::Int64List& proto_int64list) {
  InitFromProto(proto_int64list);
}
Int64List::Int64List() = default;

Int64List::~Int64List() = default;

void Int64List::InitFromProto(const PbMessage& proto_int64list) {
  BuildFromProto(proto_int64list);
}
  
void* Int64List::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_value();
    default: return nullptr;
  }
}

bool Int64List::operator==(const Int64List& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t Int64List::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool Int64List::operator<(const Int64List& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void Int64List::Clear() {
  if (data_) { data_.reset(); }
}
void Int64List::CopyFrom(const Int64List& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
Int64List& Int64List::operator=(const Int64List& other) {
  CopyFrom(other);
  return *this;
}

// repeated field value
void Int64List::clear_value() {
  return __SharedPtr__()->clear_value();
}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_* Int64List::mutable_value() {
  return __SharedPtr__()->mutable_value();
}
int64_t* Int64List::mutable_value(::std::size_t index) {
  return __SharedPtr__()->mutable_value(index);
}
void Int64List::add_value(const int64_t& value) {
  return __SharedPtr__()->add_value(value);
}
void Int64List::set_value(::std::size_t index, const int64_t& value) {
  return __SharedPtr__()->set_value(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> Int64List::shared_mutable_value() {
  return mutable_value()->__SharedMutable__();
}

::std::shared_ptr<Int64List> Int64List::__SharedMutable__() {
  return ::std::make_shared<Int64List>(__SharedPtr__());
}
ConstFeature::_Feature_::_Feature_() { Clear(); }
ConstFeature::_Feature_::_Feature_(const _Feature_& other) { CopyFrom(other); }
ConstFeature::_Feature_::_Feature_(const ::oneflow::Feature& proto_feature) {
  InitFromProto(proto_feature);
}
ConstFeature::_Feature_::_Feature_(_Feature_&& other) = default;
ConstFeature::_Feature_::~_Feature_() = default;

void ConstFeature::_Feature_::InitFromProto(const ::oneflow::Feature& proto_feature) {
  Clear();
  // oneof field: kind
  KindCase kind_case = KindCase(int(proto_feature.kind_case()));
  switch (kind_case) {
    case kBytesList: {
      *mutable_bytes_list() = ::oneflow::cfg::BytesList(proto_feature.bytes_list());
      break;
  }
    case kFloatList: {
      *mutable_float_list() = ::oneflow::cfg::FloatList(proto_feature.float_list());
      break;
  }
    case kDoubleList: {
      *mutable_double_list() = ::oneflow::cfg::DoubleList(proto_feature.double_list());
      break;
  }
    case kInt32List: {
      *mutable_int32_list() = ::oneflow::cfg::Int32List(proto_feature.int32_list());
      break;
  }
    case kInt64List: {
      *mutable_int64_list() = ::oneflow::cfg::Int64List(proto_feature.int64_list());
      break;
  }
    case KIND_NOT_SET: {
      break;
    }
  }
      
}

void ConstFeature::_Feature_::ToProto(::oneflow::Feature* proto_feature) const {
  proto_feature->Clear();

  // oneof field: kind
  ::oneflow::Feature::KindCase kind_case = ::oneflow::Feature::KindCase(int(this->kind_case()));
  switch (kind_case) {
    case ::oneflow::Feature::kBytesList: {
      ::oneflow::BytesList of_proto_bytes_list;
      bytes_list().ToProto(&of_proto_bytes_list);
      proto_feature->mutable_bytes_list()->CopyFrom(of_proto_bytes_list);
      break;
    }
    case ::oneflow::Feature::kFloatList: {
      ::oneflow::FloatList of_proto_float_list;
      float_list().ToProto(&of_proto_float_list);
      proto_feature->mutable_float_list()->CopyFrom(of_proto_float_list);
      break;
    }
    case ::oneflow::Feature::kDoubleList: {
      ::oneflow::DoubleList of_proto_double_list;
      double_list().ToProto(&of_proto_double_list);
      proto_feature->mutable_double_list()->CopyFrom(of_proto_double_list);
      break;
    }
    case ::oneflow::Feature::kInt32List: {
      ::oneflow::Int32List of_proto_int32_list;
      int32_list().ToProto(&of_proto_int32_list);
      proto_feature->mutable_int32_list()->CopyFrom(of_proto_int32_list);
      break;
    }
    case ::oneflow::Feature::kInt64List: {
      ::oneflow::Int64List of_proto_int64_list;
      int64_list().ToProto(&of_proto_int64_list);
      proto_feature->mutable_int64_list()->CopyFrom(of_proto_int64_list);
      break;
    }
    case ::oneflow::Feature::KIND_NOT_SET: {
      break;
    }
  }
}

::std::string ConstFeature::_Feature_::DebugString() const {
  ::oneflow::Feature proto_feature;
  this->ToProto(&proto_feature);
  return proto_feature.DebugString();
}

void ConstFeature::_Feature_::Clear() {
  clear_kind();
}

void ConstFeature::_Feature_::CopyFrom(const _Feature_& other) {
  kind_copy_from(other);
}


// oneof field kind: bytes_list
bool ConstFeature::_Feature_::has_bytes_list() const {
  return kind_case() == kBytesList;
}
void ConstFeature::_Feature_::clear_bytes_list() {
  if (has_bytes_list()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BytesList>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.bytes_list_));
      ptr->~Shared_ptr();
    }
    kind_case_ = KIND_NOT_SET;
  }
}

const ::oneflow::cfg::BytesList& ConstFeature::_Feature_::bytes_list() const {
  if (has_bytes_list()) {
      const ::std::shared_ptr<::oneflow::cfg::BytesList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::BytesList>*>(&(kind_.bytes_list_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::BytesList> default_static_value = ::std::make_shared<::oneflow::cfg::BytesList>();
    return *default_static_value;
    }
}
::oneflow::cfg::BytesList* ConstFeature::_Feature_::mutable_bytes_list() {
  if(!has_bytes_list()) {
    clear_kind();
    new (&(kind_.bytes_list_)) ::std::shared_ptr<::oneflow::cfg::BytesList>(new ::oneflow::cfg::BytesList());
  }
  kind_case_ = kBytesList;
  ::std::shared_ptr<::oneflow::cfg::BytesList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::BytesList>*>(&(kind_.bytes_list_));
  return  (*ptr).get();
}

// oneof field kind: float_list
bool ConstFeature::_Feature_::has_float_list() const {
  return kind_case() == kFloatList;
}
void ConstFeature::_Feature_::clear_float_list() {
  if (has_float_list()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::FloatList>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.float_list_));
      ptr->~Shared_ptr();
    }
    kind_case_ = KIND_NOT_SET;
  }
}

const ::oneflow::cfg::FloatList& ConstFeature::_Feature_::float_list() const {
  if (has_float_list()) {
      const ::std::shared_ptr<::oneflow::cfg::FloatList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::FloatList>*>(&(kind_.float_list_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::FloatList> default_static_value = ::std::make_shared<::oneflow::cfg::FloatList>();
    return *default_static_value;
    }
}
::oneflow::cfg::FloatList* ConstFeature::_Feature_::mutable_float_list() {
  if(!has_float_list()) {
    clear_kind();
    new (&(kind_.float_list_)) ::std::shared_ptr<::oneflow::cfg::FloatList>(new ::oneflow::cfg::FloatList());
  }
  kind_case_ = kFloatList;
  ::std::shared_ptr<::oneflow::cfg::FloatList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::FloatList>*>(&(kind_.float_list_));
  return  (*ptr).get();
}

// oneof field kind: double_list
bool ConstFeature::_Feature_::has_double_list() const {
  return kind_case() == kDoubleList;
}
void ConstFeature::_Feature_::clear_double_list() {
  if (has_double_list()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DoubleList>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.double_list_));
      ptr->~Shared_ptr();
    }
    kind_case_ = KIND_NOT_SET;
  }
}

const ::oneflow::cfg::DoubleList& ConstFeature::_Feature_::double_list() const {
  if (has_double_list()) {
      const ::std::shared_ptr<::oneflow::cfg::DoubleList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::DoubleList>*>(&(kind_.double_list_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::DoubleList> default_static_value = ::std::make_shared<::oneflow::cfg::DoubleList>();
    return *default_static_value;
    }
}
::oneflow::cfg::DoubleList* ConstFeature::_Feature_::mutable_double_list() {
  if(!has_double_list()) {
    clear_kind();
    new (&(kind_.double_list_)) ::std::shared_ptr<::oneflow::cfg::DoubleList>(new ::oneflow::cfg::DoubleList());
  }
  kind_case_ = kDoubleList;
  ::std::shared_ptr<::oneflow::cfg::DoubleList>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::DoubleList>*>(&(kind_.double_list_));
  return  (*ptr).get();
}

// oneof field kind: int32_list
bool ConstFeature::_Feature_::has_int32_list() const {
  return kind_case() == kInt32List;
}
void ConstFeature::_Feature_::clear_int32_list() {
  if (has_int32_list()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::Int32List>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.int32_list_));
      ptr->~Shared_ptr();
    }
    kind_case_ = KIND_NOT_SET;
  }
}

const ::oneflow::cfg::Int32List& ConstFeature::_Feature_::int32_list() const {
  if (has_int32_list()) {
      const ::std::shared_ptr<::oneflow::cfg::Int32List>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::Int32List>*>(&(kind_.int32_list_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::Int32List> default_static_value = ::std::make_shared<::oneflow::cfg::Int32List>();
    return *default_static_value;
    }
}
::oneflow::cfg::Int32List* ConstFeature::_Feature_::mutable_int32_list() {
  if(!has_int32_list()) {
    clear_kind();
    new (&(kind_.int32_list_)) ::std::shared_ptr<::oneflow::cfg::Int32List>(new ::oneflow::cfg::Int32List());
  }
  kind_case_ = kInt32List;
  ::std::shared_ptr<::oneflow::cfg::Int32List>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::Int32List>*>(&(kind_.int32_list_));
  return  (*ptr).get();
}

// oneof field kind: int64_list
bool ConstFeature::_Feature_::has_int64_list() const {
  return kind_case() == kInt64List;
}
void ConstFeature::_Feature_::clear_int64_list() {
  if (has_int64_list()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::Int64List>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.int64_list_));
      ptr->~Shared_ptr();
    }
    kind_case_ = KIND_NOT_SET;
  }
}

const ::oneflow::cfg::Int64List& ConstFeature::_Feature_::int64_list() const {
  if (has_int64_list()) {
      const ::std::shared_ptr<::oneflow::cfg::Int64List>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::Int64List>*>(&(kind_.int64_list_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::Int64List> default_static_value = ::std::make_shared<::oneflow::cfg::Int64List>();
    return *default_static_value;
    }
}
::oneflow::cfg::Int64List* ConstFeature::_Feature_::mutable_int64_list() {
  if(!has_int64_list()) {
    clear_kind();
    new (&(kind_.int64_list_)) ::std::shared_ptr<::oneflow::cfg::Int64List>(new ::oneflow::cfg::Int64List());
  }
  kind_case_ = kInt64List;
  ::std::shared_ptr<::oneflow::cfg::Int64List>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::Int64List>*>(&(kind_.int64_list_));
  return  (*ptr).get();
}
ConstFeature::KindCase ConstFeature::_Feature_::kind_case() const {
  return kind_case_;
}
bool ConstFeature::_Feature_::has_kind() const {
  return kind_case_ != KIND_NOT_SET;
}
void ConstFeature::_Feature_::clear_kind() {
  switch (kind_case()) {
    case kBytesList: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::BytesList>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.bytes_list_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kFloatList: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::FloatList>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.float_list_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kDoubleList: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::DoubleList>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.double_list_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kInt32List: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::Int32List>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.int32_list_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kInt64List: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::Int64List>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(kind_.int64_list_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case KIND_NOT_SET: {
      break;
    }
  }
  kind_case_ = KIND_NOT_SET;
}
void ConstFeature::_Feature_::kind_copy_from(const _Feature_& other) {
  switch (other.kind_case()) {
    case kBytesList: {
      mutable_bytes_list()->CopyFrom(other.bytes_list());
      break;
    }
    case kFloatList: {
      mutable_float_list()->CopyFrom(other.float_list());
      break;
    }
    case kDoubleList: {
      mutable_double_list()->CopyFrom(other.double_list());
      break;
    }
    case kInt32List: {
      mutable_int32_list()->CopyFrom(other.int32_list());
      break;
    }
    case kInt64List: {
      mutable_int64_list()->CopyFrom(other.int64_list());
      break;
    }
    case KIND_NOT_SET: {
      clear_kind();
    }
  }
}


int ConstFeature::_Feature_::compare(const _Feature_& other) {
  if (!(kind_case() == other.kind_case())) {
    return kind_case() < other.kind_case() ? -1 : 1;
  }
  switch (kind_case()) {
    case kBytesList: {
      if (!(bytes_list() == other.bytes_list())) {
        return bytes_list() < other.bytes_list() ? -1 : 1;
      }
      break;
    }
    case kFloatList: {
      if (!(float_list() == other.float_list())) {
        return float_list() < other.float_list() ? -1 : 1;
      }
      break;
    }
    case kDoubleList: {
      if (!(double_list() == other.double_list())) {
        return double_list() < other.double_list() ? -1 : 1;
      }
      break;
    }
    case kInt32List: {
      if (!(int32_list() == other.int32_list())) {
        return int32_list() < other.int32_list() ? -1 : 1;
      }
      break;
    }
    case kInt64List: {
      if (!(int64_list() == other.int64_list())) {
        return int64_list() < other.int64_list() ? -1 : 1;
      }
      break;
    }
    case KIND_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstFeature::_Feature_::operator==(const _Feature_& other) const {
  return true
    && kind_case() == other.kind_case()
    && (kind_case() == kBytesList ? 
      bytes_list() == other.bytes_list() : true)
    && (kind_case() == kFloatList ? 
      float_list() == other.float_list() : true)
    && (kind_case() == kDoubleList ? 
      double_list() == other.double_list() : true)
    && (kind_case() == kInt32List ? 
      int32_list() == other.int32_list() : true)
    && (kind_case() == kInt64List ? 
      int64_list() == other.int64_list() : true)
  ;
}

std::size_t ConstFeature::_Feature_::__CalcHash__() const {
  return 0
    ^ static_cast<std::size_t>(kind_case())
    ^ (has_bytes_list() ? std::hash<::oneflow::cfg::BytesList>()(bytes_list()) : 0)
    ^ (has_float_list() ? std::hash<::oneflow::cfg::FloatList>()(float_list()) : 0)
    ^ (has_double_list() ? std::hash<::oneflow::cfg::DoubleList>()(double_list()) : 0)
    ^ (has_int32_list() ? std::hash<::oneflow::cfg::Int32List>()(int32_list()) : 0)
    ^ (has_int64_list() ? std::hash<::oneflow::cfg::Int64List>()(int64_list()) : 0)
  ;
}

bool ConstFeature::_Feature_::operator<(const _Feature_& other) const {
  return false
    || !(kind_case() == other.kind_case()) ? 
      kind_case() < other.kind_case() : false
    || ((kind_case() == kBytesList) && 
      !(bytes_list() == other.bytes_list())) ? 
        bytes_list() < other.bytes_list() : false
    || ((kind_case() == kFloatList) && 
      !(float_list() == other.float_list())) ? 
        float_list() < other.float_list() : false
    || ((kind_case() == kDoubleList) && 
      !(double_list() == other.double_list())) ? 
        double_list() < other.double_list() : false
    || ((kind_case() == kInt32List) && 
      !(int32_list() == other.int32_list())) ? 
        int32_list() < other.int32_list() : false
    || ((kind_case() == kInt64List) && 
      !(int64_list() == other.int64_list())) ? 
        int64_list() < other.int64_list() : false
  ;
}

using _Feature_ =  ConstFeature::_Feature_;
ConstFeature::ConstFeature(const ::std::shared_ptr<_Feature_>& data): data_(data) {}
ConstFeature::ConstFeature(): data_(::std::make_shared<_Feature_>()) {}
ConstFeature::ConstFeature(const ::oneflow::Feature& proto_feature) {
  BuildFromProto(proto_feature);
}
ConstFeature::ConstFeature(const ConstFeature&) = default;
ConstFeature::ConstFeature(ConstFeature&&) noexcept = default;
ConstFeature::~ConstFeature() = default;

void ConstFeature::ToProto(PbMessage* proto_feature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::Feature*>(proto_feature));
}
  
::std::string ConstFeature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstFeature::__Empty__() const {
  return !data_;
}

int ConstFeature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"bytes_list", 1},
    {"float_list", 2},
    {"double_list", 3},
    {"int32_list", 4},
    {"int64_list", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstFeature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstFeature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BytesList),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBytesList),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::FloatList),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstFloatList),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DoubleList),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstDoubleList),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::Int32List),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInt32List),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::Int64List),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstInt64List),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstFeature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &bytes_list();
    case 2: return &float_list();
    case 3: return &double_list();
    case 4: return &int32_list();
    case 5: return &int64_list();
    default: return nullptr;
  }
}

 // oneof field kind: bytes_list
bool ConstFeature::has_bytes_list() const {
  return __SharedPtrOrDefault__()->has_bytes_list();
}
const ::oneflow::cfg::BytesList& ConstFeature::bytes_list() const {
  return __SharedPtrOrDefault__()->bytes_list();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBytesList> ConstFeature::shared_const_bytes_list() const {
  return bytes_list().__SharedConst__();
}
 // oneof field kind: float_list
bool ConstFeature::has_float_list() const {
  return __SharedPtrOrDefault__()->has_float_list();
}
const ::oneflow::cfg::FloatList& ConstFeature::float_list() const {
  return __SharedPtrOrDefault__()->float_list();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstFloatList> ConstFeature::shared_const_float_list() const {
  return float_list().__SharedConst__();
}
 // oneof field kind: double_list
bool ConstFeature::has_double_list() const {
  return __SharedPtrOrDefault__()->has_double_list();
}
const ::oneflow::cfg::DoubleList& ConstFeature::double_list() const {
  return __SharedPtrOrDefault__()->double_list();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstDoubleList> ConstFeature::shared_const_double_list() const {
  return double_list().__SharedConst__();
}
 // oneof field kind: int32_list
bool ConstFeature::has_int32_list() const {
  return __SharedPtrOrDefault__()->has_int32_list();
}
const ::oneflow::cfg::Int32List& ConstFeature::int32_list() const {
  return __SharedPtrOrDefault__()->int32_list();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInt32List> ConstFeature::shared_const_int32_list() const {
  return int32_list().__SharedConst__();
}
 // oneof field kind: int64_list
bool ConstFeature::has_int64_list() const {
  return __SharedPtrOrDefault__()->has_int64_list();
}
const ::oneflow::cfg::Int64List& ConstFeature::int64_list() const {
  return __SharedPtrOrDefault__()->int64_list();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstInt64List> ConstFeature::shared_const_int64_list() const {
  return int64_list().__SharedConst__();
}
ConstFeature::KindCase ConstFeature::kind_case() const {
  return __SharedPtrOrDefault__()->kind_case();
}

bool ConstFeature::has_kind() const {
  return __SharedPtrOrDefault__()->has_kind();
}

::std::shared_ptr<ConstFeature> ConstFeature::__SharedConst__() const {
  return ::std::make_shared<ConstFeature>(data_);
}
int64_t ConstFeature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstFeature::operator==(const ConstFeature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstFeature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstFeature::operator<(const ConstFeature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_Feature_>& ConstFeature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_Feature_> default_ptr = std::make_shared<_Feature_>();
  return default_ptr;
}
const ::std::shared_ptr<_Feature_>& ConstFeature::__SharedPtr__() {
  if (!data_) { data_.reset(new _Feature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstFeature
void ConstFeature::BuildFromProto(const PbMessage& proto_feature) {
  data_ = ::std::make_shared<_Feature_>(dynamic_cast<const ::oneflow::Feature&>(proto_feature));
}

Feature::Feature(const ::std::shared_ptr<_Feature_>& data)
  : ConstFeature(data) {}
Feature::Feature(const Feature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<Feature> resize
Feature::Feature(Feature&&) noexcept = default; 
Feature::Feature(const ::oneflow::Feature& proto_feature) {
  InitFromProto(proto_feature);
}
Feature::Feature() = default;

Feature::~Feature() = default;

void Feature::InitFromProto(const PbMessage& proto_feature) {
  BuildFromProto(proto_feature);
}
  
void* Feature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_bytes_list();
    case 2: return mutable_float_list();
    case 3: return mutable_double_list();
    case 4: return mutable_int32_list();
    case 5: return mutable_int64_list();
    default: return nullptr;
  }
}

bool Feature::operator==(const Feature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t Feature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool Feature::operator<(const Feature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void Feature::Clear() {
  if (data_) { data_.reset(); }
}
void Feature::CopyFrom(const Feature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
Feature& Feature::operator=(const Feature& other) {
  CopyFrom(other);
  return *this;
}

void Feature::clear_bytes_list() {
  return __SharedPtr__()->clear_bytes_list();
}
::oneflow::cfg::BytesList* Feature::mutable_bytes_list() {
  return __SharedPtr__()->mutable_bytes_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BytesList> Feature::shared_mutable_bytes_list() {
  return mutable_bytes_list()->__SharedMutable__();
}
void Feature::clear_float_list() {
  return __SharedPtr__()->clear_float_list();
}
::oneflow::cfg::FloatList* Feature::mutable_float_list() {
  return __SharedPtr__()->mutable_float_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::FloatList> Feature::shared_mutable_float_list() {
  return mutable_float_list()->__SharedMutable__();
}
void Feature::clear_double_list() {
  return __SharedPtr__()->clear_double_list();
}
::oneflow::cfg::DoubleList* Feature::mutable_double_list() {
  return __SharedPtr__()->mutable_double_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::DoubleList> Feature::shared_mutable_double_list() {
  return mutable_double_list()->__SharedMutable__();
}
void Feature::clear_int32_list() {
  return __SharedPtr__()->clear_int32_list();
}
::oneflow::cfg::Int32List* Feature::mutable_int32_list() {
  return __SharedPtr__()->mutable_int32_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::Int32List> Feature::shared_mutable_int32_list() {
  return mutable_int32_list()->__SharedMutable__();
}
void Feature::clear_int64_list() {
  return __SharedPtr__()->clear_int64_list();
}
::oneflow::cfg::Int64List* Feature::mutable_int64_list() {
  return __SharedPtr__()->mutable_int64_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::Int64List> Feature::shared_mutable_int64_list() {
  return mutable_int64_list()->__SharedMutable__();
}

::std::shared_ptr<Feature> Feature::__SharedMutable__() {
  return ::std::make_shared<Feature>(__SharedPtr__());
}
ConstOFRecord::_OFRecord_::_OFRecord_() { Clear(); }
ConstOFRecord::_OFRecord_::_OFRecord_(const _OFRecord_& other) { CopyFrom(other); }
ConstOFRecord::_OFRecord_::_OFRecord_(const ::oneflow::OFRecord& proto_ofrecord) {
  InitFromProto(proto_ofrecord);
}
ConstOFRecord::_OFRecord_::_OFRecord_(_OFRecord_&& other) = default;
ConstOFRecord::_OFRecord_::~_OFRecord_() = default;

void ConstOFRecord::_OFRecord_::InitFromProto(const ::oneflow::OFRecord& proto_ofrecord) {
  Clear();
  // map field : feature
  if (!proto_ofrecord.feature().empty()) {
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_&  mut_feature = *mutable_feature();
    for (const auto& pair : proto_ofrecord.feature()) {
      mut_feature[pair.first] = ::oneflow::cfg::Feature(pair.second);
    }
  }
    
}

void ConstOFRecord::_OFRecord_::ToProto(::oneflow::OFRecord* proto_ofrecord) const {
  proto_ofrecord->Clear();
  // map field : feature
  if (!feature().empty()) {
    auto& mut_feature = *(proto_ofrecord->mutable_feature());
    for (const auto& pair : feature()) {
      ::oneflow::Feature proto_feature_value;
      pair.second.ToProto(&proto_feature_value);
      mut_feature[pair.first] = proto_feature_value;
    }
  }

}

::std::string ConstOFRecord::_OFRecord_::DebugString() const {
  ::oneflow::OFRecord proto_ofrecord;
  this->ToProto(&proto_ofrecord);
  return proto_ofrecord.DebugString();
}

void ConstOFRecord::_OFRecord_::Clear() {
  clear_feature();
}

void ConstOFRecord::_OFRecord_::CopyFrom(const _OFRecord_& other) {
  mutable_feature()->CopyFrom(other.feature());
}


::std::size_t ConstOFRecord::_OFRecord_::feature_size() const {
  if (!feature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>();
    return default_static_value->size();
  }
  return feature_->size();
}
const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& ConstOFRecord::_OFRecord_::feature() const {
  if (!feature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>();
    return *(default_static_value.get());
  }
  return *(feature_.get());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ * ConstOFRecord::_OFRecord_::mutable_feature() {
  if (!feature_) {
    feature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>();
  }
  return feature_.get();
}

const ::oneflow::cfg::Feature& ConstOFRecord::_OFRecord_::feature(::std::string key) const {
  if (!feature_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>();
    return default_static_value->at(key);
  }
  return feature_->at(key);
}

void ConstOFRecord::_OFRecord_::clear_feature() {
  if (!feature_) {
    feature_ = ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>();
  }
  return feature_->Clear();
}



int ConstOFRecord::_OFRecord_::compare(const _OFRecord_& other) {
  if (!(feature() == other.feature())) {
    return feature() < other.feature() ? -1 : 1;
  }
  return 0;
}

bool ConstOFRecord::_OFRecord_::operator==(const _OFRecord_& other) const {
  return true
    && feature() == other.feature()
  ;
}

std::size_t ConstOFRecord::_OFRecord_::__CalcHash__() const {
  return 0
    ^ feature().__CalcHash__()
  ;
}

bool ConstOFRecord::_OFRecord_::operator<(const _OFRecord_& other) const {
  return false
    || !(feature() == other.feature()) ? 
      feature() < other.feature() : false
  ;
}

using _OFRecord_ =  ConstOFRecord::_OFRecord_;
ConstOFRecord::ConstOFRecord(const ::std::shared_ptr<_OFRecord_>& data): data_(data) {}
ConstOFRecord::ConstOFRecord(): data_(::std::make_shared<_OFRecord_>()) {}
ConstOFRecord::ConstOFRecord(const ::oneflow::OFRecord& proto_ofrecord) {
  BuildFromProto(proto_ofrecord);
}
ConstOFRecord::ConstOFRecord(const ConstOFRecord&) = default;
ConstOFRecord::ConstOFRecord(ConstOFRecord&&) noexcept = default;
ConstOFRecord::~ConstOFRecord() = default;

void ConstOFRecord::ToProto(PbMessage* proto_ofrecord) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OFRecord*>(proto_ofrecord));
}
  
::std::string ConstOFRecord::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOFRecord::__Empty__() const {
  return !data_;
}

int ConstOFRecord::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"feature", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOFRecord::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOFRecord::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::Feature>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOFRecord::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &feature();
    default: return nullptr;
  }
}

// map field feature
::std::size_t ConstOFRecord::feature_size() const {
  return __SharedPtrOrDefault__()->feature_size();
}

const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& ConstOFRecord::feature() const {
  return __SharedPtrOrDefault__()->feature();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> ConstOFRecord::shared_const_feature() const {
  return feature().__SharedConst__();
}

::std::shared_ptr<ConstOFRecord> ConstOFRecord::__SharedConst__() const {
  return ::std::make_shared<ConstOFRecord>(data_);
}
int64_t ConstOFRecord::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOFRecord::operator==(const ConstOFRecord& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOFRecord::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOFRecord::operator<(const ConstOFRecord& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OFRecord_>& ConstOFRecord::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OFRecord_> default_ptr = std::make_shared<_OFRecord_>();
  return default_ptr;
}
const ::std::shared_ptr<_OFRecord_>& ConstOFRecord::__SharedPtr__() {
  if (!data_) { data_.reset(new _OFRecord_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOFRecord
void ConstOFRecord::BuildFromProto(const PbMessage& proto_ofrecord) {
  data_ = ::std::make_shared<_OFRecord_>(dynamic_cast<const ::oneflow::OFRecord&>(proto_ofrecord));
}

OFRecord::OFRecord(const ::std::shared_ptr<_OFRecord_>& data)
  : ConstOFRecord(data) {}
OFRecord::OFRecord(const OFRecord& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OFRecord> resize
OFRecord::OFRecord(OFRecord&&) noexcept = default; 
OFRecord::OFRecord(const ::oneflow::OFRecord& proto_ofrecord) {
  InitFromProto(proto_ofrecord);
}
OFRecord::OFRecord() = default;

OFRecord::~OFRecord() = default;

void OFRecord::InitFromProto(const PbMessage& proto_ofrecord) {
  BuildFromProto(proto_ofrecord);
}
  
void* OFRecord::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_feature();
    default: return nullptr;
  }
}

bool OFRecord::operator==(const OFRecord& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OFRecord::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OFRecord::operator<(const OFRecord& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OFRecord::Clear() {
  if (data_) { data_.reset(); }
}
void OFRecord::CopyFrom(const OFRecord& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OFRecord& OFRecord::operator=(const OFRecord& other) {
  CopyFrom(other);
  return *this;
}

// repeated field feature
void OFRecord::clear_feature() {
  return __SharedPtr__()->clear_feature();
}

const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ & OFRecord::feature() const {
  return __SharedConst__()->feature();
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_* OFRecord::mutable_feature() {
  return __SharedPtr__()->mutable_feature();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> OFRecord::shared_mutable_feature() {
  return mutable_feature()->__SharedMutable__();
}

::std::shared_ptr<OFRecord> OFRecord::__SharedMutable__() {
  return ::std::make_shared<OFRecord>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data): ::oneflow::cfg::_RepeatedField_<float>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_() = default;


bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<float>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) {
  ::oneflow::cfg::_RepeatedField_<float>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) {
  ::oneflow::cfg::_RepeatedField_<float>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<float>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data): ::oneflow::cfg::_RepeatedField_<double>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_() = default;


bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<double>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) {
  ::oneflow::cfg::_RepeatedField_<double>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) {
  ::oneflow::cfg::_RepeatedField_<double>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<double>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): ::oneflow::cfg::_RepeatedField_<int32_t>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_() = default;


bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int32_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int32_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): ::oneflow::cfg::_RepeatedField_<int64_t>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_() = default;


bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) {
  ::oneflow::cfg::_RepeatedField_<int64_t>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<int64_t>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::Feature>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::Feature>(data) {}
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_() = default;
Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_() = default;

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::Feature>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::Feature& Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstFeature> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ConstFeature> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ConstFeature> Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::Feature>>& data): Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_(data) {}
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_() = default;
_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_() = default;

void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::Feature>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::Feature>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::Feature>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::Feature> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ::oneflow::cfg::Feature> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ::oneflow::cfg::Feature> _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_::shared_mut_end() { return end(); }

}
} // namespace oneflow
