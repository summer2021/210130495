#ifndef CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H_
#define CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class BytesList;
class FloatList;
class DoubleList;
class Int32List;
class Int64List;
class Feature;
class OFRecord_FeatureEntry;
class OFRecord;

namespace cfg {


class BytesList;
class ConstBytesList;

class FloatList;
class ConstFloatList;

class DoubleList;
class ConstDoubleList;

class Int32List;
class ConstInt32List;

class Int64List;
class ConstInt64List;

class Feature;
class ConstFeature;

class OFRecord;
class ConstOFRecord;


class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_;

class ConstBytesList : public ::oneflow::cfg::Message {
 public:

  class _BytesList_ {
   public:
    _BytesList_();
    explicit _BytesList_(const _BytesList_& other);
    explicit _BytesList_(_BytesList_&& other);
    _BytesList_(const ::oneflow::BytesList& proto_byteslist);
    ~_BytesList_();

    void InitFromProto(const ::oneflow::BytesList& proto_byteslist);

    void ToProto(::oneflow::BytesList* proto_byteslist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BytesList_& other);
  
      // repeated field value
   public:
    ::std::size_t value_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& value() const;
    const ::std::string& value(::std::size_t index) const;
    void clear_value();
    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_* mutable_value();
    ::std::string* mutable_value(::std::size_t index);
      void add_value(const ::std::string& value);
    void set_value(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> value_;
         
   public:
    int compare(const _BytesList_& other);

    bool operator==(const _BytesList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BytesList_& other) const;
  };

  ConstBytesList(const ::std::shared_ptr<_BytesList_>& data);
  ConstBytesList(const ConstBytesList&);
  ConstBytesList(ConstBytesList&&) noexcept;
  ConstBytesList();
  ConstBytesList(const ::oneflow::BytesList& proto_byteslist);
  virtual ~ConstBytesList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_byteslist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field value
 public:
  ::std::size_t value_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& value() const;
  const ::std::string& value(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> shared_const_value() const;

 public:
  ::std::shared_ptr<ConstBytesList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBytesList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBytesList& other) const;
 protected:
  const ::std::shared_ptr<_BytesList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BytesList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBytesList
  void BuildFromProto(const PbMessage& proto_byteslist);
  
  ::std::shared_ptr<_BytesList_> data_;
};

class BytesList final : public ConstBytesList {
 public:
  BytesList(const ::std::shared_ptr<_BytesList_>& data);
  BytesList(const BytesList& other);
  // enable nothrow for ::std::vector<BytesList> resize 
  BytesList(BytesList&&) noexcept;
  BytesList();
  explicit BytesList(const ::oneflow::BytesList& proto_byteslist);

  ~BytesList() override;

  void InitFromProto(const PbMessage& proto_byteslist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BytesList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BytesList& other) const;
  void Clear();
  void CopyFrom(const BytesList& other);
  BytesList& operator=(const BytesList& other);

  // repeated field value
 public:
  void clear_value();
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_* mutable_value();
  ::std::string* mutable_value(::std::size_t index);
  void add_value(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> shared_mutable_value();
  void set_value(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<BytesList> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_;
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_;

class ConstFloatList : public ::oneflow::cfg::Message {
 public:

  class _FloatList_ {
   public:
    _FloatList_();
    explicit _FloatList_(const _FloatList_& other);
    explicit _FloatList_(_FloatList_&& other);
    _FloatList_(const ::oneflow::FloatList& proto_floatlist);
    ~_FloatList_();

    void InitFromProto(const ::oneflow::FloatList& proto_floatlist);

    void ToProto(::oneflow::FloatList* proto_floatlist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FloatList_& other);
  
      // repeated field value
   public:
    ::std::size_t value_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& value() const;
    const float& value(::std::size_t index) const;
    void clear_value();
    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_* mutable_value();
    float* mutable_value(::std::size_t index);
      void add_value(const float& value);
    void set_value(::std::size_t index, const float& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> value_;
         
   public:
    int compare(const _FloatList_& other);

    bool operator==(const _FloatList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FloatList_& other) const;
  };

  ConstFloatList(const ::std::shared_ptr<_FloatList_>& data);
  ConstFloatList(const ConstFloatList&);
  ConstFloatList(ConstFloatList&&) noexcept;
  ConstFloatList();
  ConstFloatList(const ::oneflow::FloatList& proto_floatlist);
  virtual ~ConstFloatList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_floatlist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field value
 public:
  ::std::size_t value_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& value() const;
  const float& value(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> shared_const_value() const;

 public:
  ::std::shared_ptr<ConstFloatList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFloatList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFloatList& other) const;
 protected:
  const ::std::shared_ptr<_FloatList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FloatList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFloatList
  void BuildFromProto(const PbMessage& proto_floatlist);
  
  ::std::shared_ptr<_FloatList_> data_;
};

class FloatList final : public ConstFloatList {
 public:
  FloatList(const ::std::shared_ptr<_FloatList_>& data);
  FloatList(const FloatList& other);
  // enable nothrow for ::std::vector<FloatList> resize 
  FloatList(FloatList&&) noexcept;
  FloatList();
  explicit FloatList(const ::oneflow::FloatList& proto_floatlist);

  ~FloatList() override;

  void InitFromProto(const PbMessage& proto_floatlist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FloatList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FloatList& other) const;
  void Clear();
  void CopyFrom(const FloatList& other);
  FloatList& operator=(const FloatList& other);

  // repeated field value
 public:
  void clear_value();
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_* mutable_value();
  float* mutable_value(::std::size_t index);
  void add_value(const float& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> shared_mutable_value();
  void set_value(::std::size_t index, const float& value);

  ::std::shared_ptr<FloatList> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_;
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_;

class ConstDoubleList : public ::oneflow::cfg::Message {
 public:

  class _DoubleList_ {
   public:
    _DoubleList_();
    explicit _DoubleList_(const _DoubleList_& other);
    explicit _DoubleList_(_DoubleList_&& other);
    _DoubleList_(const ::oneflow::DoubleList& proto_doublelist);
    ~_DoubleList_();

    void InitFromProto(const ::oneflow::DoubleList& proto_doublelist);

    void ToProto(::oneflow::DoubleList* proto_doublelist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DoubleList_& other);
  
      // repeated field value
   public:
    ::std::size_t value_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& value() const;
    const double& value(::std::size_t index) const;
    void clear_value();
    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_* mutable_value();
    double* mutable_value(::std::size_t index);
      void add_value(const double& value);
    void set_value(::std::size_t index, const double& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> value_;
         
   public:
    int compare(const _DoubleList_& other);

    bool operator==(const _DoubleList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DoubleList_& other) const;
  };

  ConstDoubleList(const ::std::shared_ptr<_DoubleList_>& data);
  ConstDoubleList(const ConstDoubleList&);
  ConstDoubleList(ConstDoubleList&&) noexcept;
  ConstDoubleList();
  ConstDoubleList(const ::oneflow::DoubleList& proto_doublelist);
  virtual ~ConstDoubleList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_doublelist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field value
 public:
  ::std::size_t value_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& value() const;
  const double& value(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> shared_const_value() const;

 public:
  ::std::shared_ptr<ConstDoubleList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDoubleList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDoubleList& other) const;
 protected:
  const ::std::shared_ptr<_DoubleList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DoubleList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDoubleList
  void BuildFromProto(const PbMessage& proto_doublelist);
  
  ::std::shared_ptr<_DoubleList_> data_;
};

class DoubleList final : public ConstDoubleList {
 public:
  DoubleList(const ::std::shared_ptr<_DoubleList_>& data);
  DoubleList(const DoubleList& other);
  // enable nothrow for ::std::vector<DoubleList> resize 
  DoubleList(DoubleList&&) noexcept;
  DoubleList();
  explicit DoubleList(const ::oneflow::DoubleList& proto_doublelist);

  ~DoubleList() override;

  void InitFromProto(const PbMessage& proto_doublelist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DoubleList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DoubleList& other) const;
  void Clear();
  void CopyFrom(const DoubleList& other);
  DoubleList& operator=(const DoubleList& other);

  // repeated field value
 public:
  void clear_value();
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_* mutable_value();
  double* mutable_value(::std::size_t index);
  void add_value(const double& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> shared_mutable_value();
  void set_value(::std::size_t index, const double& value);

  ::std::shared_ptr<DoubleList> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_;
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_;

class ConstInt32List : public ::oneflow::cfg::Message {
 public:

  class _Int32List_ {
   public:
    _Int32List_();
    explicit _Int32List_(const _Int32List_& other);
    explicit _Int32List_(_Int32List_&& other);
    _Int32List_(const ::oneflow::Int32List& proto_int32list);
    ~_Int32List_();

    void InitFromProto(const ::oneflow::Int32List& proto_int32list);

    void ToProto(::oneflow::Int32List* proto_int32list) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _Int32List_& other);
  
      // repeated field value
   public:
    ::std::size_t value_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& value() const;
    const int32_t& value(::std::size_t index) const;
    void clear_value();
    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_* mutable_value();
    int32_t* mutable_value(::std::size_t index);
      void add_value(const int32_t& value);
    void set_value(::std::size_t index, const int32_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> value_;
         
   public:
    int compare(const _Int32List_& other);

    bool operator==(const _Int32List_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _Int32List_& other) const;
  };

  ConstInt32List(const ::std::shared_ptr<_Int32List_>& data);
  ConstInt32List(const ConstInt32List&);
  ConstInt32List(ConstInt32List&&) noexcept;
  ConstInt32List();
  ConstInt32List(const ::oneflow::Int32List& proto_int32list);
  virtual ~ConstInt32List() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_int32list) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field value
 public:
  ::std::size_t value_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& value() const;
  const int32_t& value(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> shared_const_value() const;

 public:
  ::std::shared_ptr<ConstInt32List> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInt32List& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInt32List& other) const;
 protected:
  const ::std::shared_ptr<_Int32List_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_Int32List_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInt32List
  void BuildFromProto(const PbMessage& proto_int32list);
  
  ::std::shared_ptr<_Int32List_> data_;
};

class Int32List final : public ConstInt32List {
 public:
  Int32List(const ::std::shared_ptr<_Int32List_>& data);
  Int32List(const Int32List& other);
  // enable nothrow for ::std::vector<Int32List> resize 
  Int32List(Int32List&&) noexcept;
  Int32List();
  explicit Int32List(const ::oneflow::Int32List& proto_int32list);

  ~Int32List() override;

  void InitFromProto(const PbMessage& proto_int32list) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const Int32List& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Int32List& other) const;
  void Clear();
  void CopyFrom(const Int32List& other);
  Int32List& operator=(const Int32List& other);

  // repeated field value
 public:
  void clear_value();
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_* mutable_value();
  int32_t* mutable_value(::std::size_t index);
  void add_value(const int32_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> shared_mutable_value();
  void set_value(::std::size_t index, const int32_t& value);

  ::std::shared_ptr<Int32List> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_;
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_;

class ConstInt64List : public ::oneflow::cfg::Message {
 public:

  class _Int64List_ {
   public:
    _Int64List_();
    explicit _Int64List_(const _Int64List_& other);
    explicit _Int64List_(_Int64List_&& other);
    _Int64List_(const ::oneflow::Int64List& proto_int64list);
    ~_Int64List_();

    void InitFromProto(const ::oneflow::Int64List& proto_int64list);

    void ToProto(::oneflow::Int64List* proto_int64list) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _Int64List_& other);
  
      // repeated field value
   public:
    ::std::size_t value_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& value() const;
    const int64_t& value(::std::size_t index) const;
    void clear_value();
    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_* mutable_value();
    int64_t* mutable_value(::std::size_t index);
      void add_value(const int64_t& value);
    void set_value(::std::size_t index, const int64_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> value_;
         
   public:
    int compare(const _Int64List_& other);

    bool operator==(const _Int64List_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _Int64List_& other) const;
  };

  ConstInt64List(const ::std::shared_ptr<_Int64List_>& data);
  ConstInt64List(const ConstInt64List&);
  ConstInt64List(ConstInt64List&&) noexcept;
  ConstInt64List();
  ConstInt64List(const ::oneflow::Int64List& proto_int64list);
  virtual ~ConstInt64List() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_int64list) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field value
 public:
  ::std::size_t value_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& value() const;
  const int64_t& value(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> shared_const_value() const;

 public:
  ::std::shared_ptr<ConstInt64List> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInt64List& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInt64List& other) const;
 protected:
  const ::std::shared_ptr<_Int64List_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_Int64List_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInt64List
  void BuildFromProto(const PbMessage& proto_int64list);
  
  ::std::shared_ptr<_Int64List_> data_;
};

class Int64List final : public ConstInt64List {
 public:
  Int64List(const ::std::shared_ptr<_Int64List_>& data);
  Int64List(const Int64List& other);
  // enable nothrow for ::std::vector<Int64List> resize 
  Int64List(Int64List&&) noexcept;
  Int64List();
  explicit Int64List(const ::oneflow::Int64List& proto_int64list);

  ~Int64List() override;

  void InitFromProto(const PbMessage& proto_int64list) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const Int64List& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Int64List& other) const;
  void Clear();
  void CopyFrom(const Int64List& other);
  Int64List& operator=(const Int64List& other);

  // repeated field value
 public:
  void clear_value();
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_* mutable_value();
  int64_t* mutable_value(::std::size_t index);
  void add_value(const int64_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> shared_mutable_value();
  void set_value(::std::size_t index, const int64_t& value);

  ::std::shared_ptr<Int64List> __SharedMutable__();
};


class ConstFeature : public ::oneflow::cfg::Message {
 public:

 // oneof enum kind
 enum KindCase : unsigned int {
  KIND_NOT_SET = 0,
    kBytesList = 1,
    kFloatList = 2,
    kDoubleList = 3,
    kInt32List = 4,
    kInt64List = 5,
   };

  class _Feature_ {
   public:
    _Feature_();
    explicit _Feature_(const _Feature_& other);
    explicit _Feature_(_Feature_&& other);
    _Feature_(const ::oneflow::Feature& proto_feature);
    ~_Feature_();

    void InitFromProto(const ::oneflow::Feature& proto_feature);

    void ToProto(::oneflow::Feature* proto_feature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _Feature_& other);
  
     // oneof field kind: bytes_list
   public:
    bool has_bytes_list() const;
    void clear_bytes_list();
    const ::oneflow::cfg::BytesList& bytes_list() const;
      ::oneflow::cfg::BytesList* mutable_bytes_list();
      
     // oneof field kind: float_list
   public:
    bool has_float_list() const;
    void clear_float_list();
    const ::oneflow::cfg::FloatList& float_list() const;
      ::oneflow::cfg::FloatList* mutable_float_list();
      
     // oneof field kind: double_list
   public:
    bool has_double_list() const;
    void clear_double_list();
    const ::oneflow::cfg::DoubleList& double_list() const;
      ::oneflow::cfg::DoubleList* mutable_double_list();
      
     // oneof field kind: int32_list
   public:
    bool has_int32_list() const;
    void clear_int32_list();
    const ::oneflow::cfg::Int32List& int32_list() const;
      ::oneflow::cfg::Int32List* mutable_int32_list();
      
     // oneof field kind: int64_list
   public:
    bool has_int64_list() const;
    void clear_int64_list();
    const ::oneflow::cfg::Int64List& int64_list() const;
      ::oneflow::cfg::Int64List* mutable_int64_list();
           
   public:
    // oneof kind
    KindCase kind_case() const;
    bool has_kind() const;
   protected:
    void clear_kind();
    void kind_copy_from(const _Feature_& other);
    union KindUnion {
      // 64-bit aligned
      uint64_t __kind_for_padding_64bit__;
          char bytes_list_[sizeof(::std::shared_ptr<::oneflow::cfg::BytesList>)];
            char float_list_[sizeof(::std::shared_ptr<::oneflow::cfg::FloatList>)];
            char double_list_[sizeof(::std::shared_ptr<::oneflow::cfg::DoubleList>)];
            char int32_list_[sizeof(::std::shared_ptr<::oneflow::cfg::Int32List>)];
            char int64_list_[sizeof(::std::shared_ptr<::oneflow::cfg::Int64List>)];
        } kind_;
    KindCase kind_case_ = KIND_NOT_SET;
     
   public:
    int compare(const _Feature_& other);

    bool operator==(const _Feature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _Feature_& other) const;
  };

  ConstFeature(const ::std::shared_ptr<_Feature_>& data);
  ConstFeature(const ConstFeature&);
  ConstFeature(ConstFeature&&) noexcept;
  ConstFeature();
  ConstFeature(const ::oneflow::Feature& proto_feature);
  virtual ~ConstFeature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_feature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

 // oneof field kind: bytes_list
 public:
  bool has_bytes_list() const;
  const ::oneflow::cfg::BytesList& bytes_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBytesList> shared_const_bytes_list() const;
 // oneof field kind: float_list
 public:
  bool has_float_list() const;
  const ::oneflow::cfg::FloatList& float_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFloatList> shared_const_float_list() const;
 // oneof field kind: double_list
 public:
  bool has_double_list() const;
  const ::oneflow::cfg::DoubleList& double_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDoubleList> shared_const_double_list() const;
 // oneof field kind: int32_list
 public:
  bool has_int32_list() const;
  const ::oneflow::cfg::Int32List& int32_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInt32List> shared_const_int32_list() const;
 // oneof field kind: int64_list
 public:
  bool has_int64_list() const;
  const ::oneflow::cfg::Int64List& int64_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInt64List> shared_const_int64_list() const;
 public:
  KindCase kind_case() const;
 protected:
  bool has_kind() const;

 public:
  ::std::shared_ptr<ConstFeature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFeature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFeature& other) const;
 protected:
  const ::std::shared_ptr<_Feature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_Feature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFeature
  void BuildFromProto(const PbMessage& proto_feature);
  
  ::std::shared_ptr<_Feature_> data_;
};

class Feature final : public ConstFeature {
 public:
  Feature(const ::std::shared_ptr<_Feature_>& data);
  Feature(const Feature& other);
  // enable nothrow for ::std::vector<Feature> resize 
  Feature(Feature&&) noexcept;
  Feature();
  explicit Feature(const ::oneflow::Feature& proto_feature);

  ~Feature() override;

  void InitFromProto(const PbMessage& proto_feature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const Feature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Feature& other) const;
  void Clear();
  void CopyFrom(const Feature& other);
  Feature& operator=(const Feature& other);

  void clear_bytes_list();
  ::oneflow::cfg::BytesList* mutable_bytes_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BytesList> shared_mutable_bytes_list();
  void clear_float_list();
  ::oneflow::cfg::FloatList* mutable_float_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FloatList> shared_mutable_float_list();
  void clear_double_list();
  ::oneflow::cfg::DoubleList* mutable_double_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DoubleList> shared_mutable_double_list();
  void clear_int32_list();
  ::oneflow::cfg::Int32List* mutable_int32_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::Int32List> shared_mutable_int32_list();
  void clear_int64_list();
  ::oneflow::cfg::Int64List* mutable_int64_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::Int64List> shared_mutable_int64_list();

  ::std::shared_ptr<Feature> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_; 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_;

class ConstOFRecord : public ::oneflow::cfg::Message {
 public:

  class _OFRecord_ {
   public:
    _OFRecord_();
    explicit _OFRecord_(const _OFRecord_& other);
    explicit _OFRecord_(_OFRecord_&& other);
    _OFRecord_(const ::oneflow::OFRecord& proto_ofrecord);
    ~_OFRecord_();

    void InitFromProto(const ::oneflow::OFRecord& proto_ofrecord);

    void ToProto(::oneflow::OFRecord* proto_ofrecord) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OFRecord_& other);
  
     public:
    ::std::size_t feature_size() const;
    const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& feature() const;

    _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ * mutable_feature();

    const ::oneflow::cfg::Feature& feature(::std::string key) const;

    void clear_feature();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> feature_;
         
   public:
    int compare(const _OFRecord_& other);

    bool operator==(const _OFRecord_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OFRecord_& other) const;
  };

  ConstOFRecord(const ::std::shared_ptr<_OFRecord_>& data);
  ConstOFRecord(const ConstOFRecord&);
  ConstOFRecord(ConstOFRecord&&) noexcept;
  ConstOFRecord();
  ConstOFRecord(const ::oneflow::OFRecord& proto_ofrecord);
  virtual ~ConstOFRecord() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_ofrecord) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field feature
 public:
  ::std::size_t feature_size() const;
  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& feature() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> shared_const_feature() const;

 public:
  ::std::shared_ptr<ConstOFRecord> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOFRecord& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOFRecord& other) const;
 protected:
  const ::std::shared_ptr<_OFRecord_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OFRecord_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOFRecord
  void BuildFromProto(const PbMessage& proto_ofrecord);
  
  ::std::shared_ptr<_OFRecord_> data_;
};

class OFRecord final : public ConstOFRecord {
 public:
  OFRecord(const ::std::shared_ptr<_OFRecord_>& data);
  OFRecord(const OFRecord& other);
  // enable nothrow for ::std::vector<OFRecord> resize 
  OFRecord(OFRecord&&) noexcept;
  OFRecord();
  explicit OFRecord(const ::oneflow::OFRecord& proto_ofrecord);

  ~OFRecord() override;

  void InitFromProto(const PbMessage& proto_ofrecord) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OFRecord& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OFRecord& other) const;
  void Clear();
  void CopyFrom(const OFRecord& other);
  OFRecord& operator=(const OFRecord& other);

  // repeated field feature
 public:
  void clear_feature();

  const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ & feature() const;

  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_* mutable_feature();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> shared_mutable_feature();

  ::std::shared_ptr<OFRecord> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_ : public ::oneflow::cfg::_RepeatedField_<float> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_(const ::std::shared_ptr<::std::vector<float>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_float_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_ : public ::oneflow::cfg::_RepeatedField_<double> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_(const ::std::shared_ptr<::std::vector<double>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_double_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_ : public ::oneflow::cfg::_RepeatedField_<int32_t> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int32_t_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_ : public ::oneflow::cfg::_RepeatedField_<int64_t> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_(const ::std::shared_ptr<::std::vector<int64_t>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__RepeatedField_int64_t_> __SharedMutable__();
};







// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::Feature> {
 public:
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::Feature>>& data);
  Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_();
  ~Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::Feature& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstFeature> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ConstFeature>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ final : public Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_ {
 public:
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::Feature>>& data);
  _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_();
  ~_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::Feature> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H__MapField___std__string_Feature_, ::oneflow::cfg::Feature>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstBytesList> {
  std::size_t operator()(const ::oneflow::cfg::ConstBytesList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BytesList> {
  std::size_t operator()(const ::oneflow::cfg::BytesList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFloatList> {
  std::size_t operator()(const ::oneflow::cfg::ConstFloatList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FloatList> {
  std::size_t operator()(const ::oneflow::cfg::FloatList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDoubleList> {
  std::size_t operator()(const ::oneflow::cfg::ConstDoubleList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DoubleList> {
  std::size_t operator()(const ::oneflow::cfg::DoubleList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInt32List> {
  std::size_t operator()(const ::oneflow::cfg::ConstInt32List& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::Int32List> {
  std::size_t operator()(const ::oneflow::cfg::Int32List& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInt64List> {
  std::size_t operator()(const ::oneflow::cfg::ConstInt64List& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::Int64List> {
  std::size_t operator()(const ::oneflow::cfg::Int64List& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFeature> {
  std::size_t operator()(const ::oneflow::cfg::ConstFeature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::Feature> {
  std::size_t operator()(const ::oneflow::cfg::Feature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOFRecord> {
  std::size_t operator()(const ::oneflow::cfg::ConstOFRecord& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OFRecord> {
  std::size_t operator()(const ::oneflow::cfg::OFRecord& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_RECORD_RECORD_CFG_H_