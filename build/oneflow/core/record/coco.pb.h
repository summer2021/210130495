// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/record/coco.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2frecord_2fcoco_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2frecord_2fcoco_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include "oneflow/core/record/record.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2frecord_2fcoco_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_oneflow_2fcore_2frecord_2fcoco_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2frecord_2fcoco_2eproto;
namespace oneflow {
class PolygonList;
class PolygonListDefaultTypeInternal;
extern PolygonListDefaultTypeInternal _PolygonList_default_instance_;
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> ::oneflow::PolygonList* Arena::CreateMaybeMessage<::oneflow::PolygonList>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace oneflow {

// ===================================================================

class PolygonList :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.PolygonList) */ {
 public:
  PolygonList();
  virtual ~PolygonList();

  PolygonList(const PolygonList& from);
  PolygonList(PolygonList&& from) noexcept
    : PolygonList() {
    *this = ::std::move(from);
  }

  inline PolygonList& operator=(const PolygonList& from) {
    CopyFrom(from);
    return *this;
  }
  inline PolygonList& operator=(PolygonList&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const PolygonList& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const PolygonList* internal_default_instance() {
    return reinterpret_cast<const PolygonList*>(
               &_PolygonList_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(PolygonList& a, PolygonList& b) {
    a.Swap(&b);
  }
  inline void Swap(PolygonList* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline PolygonList* New() const final {
    return CreateMaybeMessage<PolygonList>(nullptr);
  }

  PolygonList* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<PolygonList>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const PolygonList& from);
  void MergeFrom(const PolygonList& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(PolygonList* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.PolygonList";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2frecord_2fcoco_2eproto);
    return ::descriptor_table_oneflow_2fcore_2frecord_2fcoco_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kPolygonsFieldNumber = 1,
  };
  // repeated .oneflow.FloatList polygons = 1;
  int polygons_size() const;
  void clear_polygons();
  ::oneflow::FloatList* mutable_polygons(int index);
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::FloatList >*
      mutable_polygons();
  const ::oneflow::FloatList& polygons(int index) const;
  ::oneflow::FloatList* add_polygons();
  const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::FloatList >&
      polygons() const;

  // @@protoc_insertion_point(class_scope:oneflow.PolygonList)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::FloatList > polygons_;
  friend struct ::TableStruct_oneflow_2fcore_2frecord_2fcoco_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// PolygonList

// repeated .oneflow.FloatList polygons = 1;
inline int PolygonList::polygons_size() const {
  return polygons_.size();
}
inline ::oneflow::FloatList* PolygonList::mutable_polygons(int index) {
  // @@protoc_insertion_point(field_mutable:oneflow.PolygonList.polygons)
  return polygons_.Mutable(index);
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::FloatList >*
PolygonList::mutable_polygons() {
  // @@protoc_insertion_point(field_mutable_list:oneflow.PolygonList.polygons)
  return &polygons_;
}
inline const ::oneflow::FloatList& PolygonList::polygons(int index) const {
  // @@protoc_insertion_point(field_get:oneflow.PolygonList.polygons)
  return polygons_.Get(index);
}
inline ::oneflow::FloatList* PolygonList::add_polygons() {
  // @@protoc_insertion_point(field_add:oneflow.PolygonList.polygons)
  return polygons_.Add();
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::oneflow::FloatList >&
PolygonList::polygons() const {
  // @@protoc_insertion_point(field_list:oneflow.PolygonList.polygons)
  return polygons_;
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace oneflow

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2frecord_2fcoco_2eproto
