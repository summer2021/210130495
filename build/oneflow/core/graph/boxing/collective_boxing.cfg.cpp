#include "oneflow/core/graph/boxing/collective_boxing.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/common/device_type.cfg.h"
#include "oneflow/core/graph/boxing/collective_boxing.pb.h"

namespace oneflow {
namespace boxing {
namespace collective {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstDeviceDesc::_DeviceDesc_::_DeviceDesc_() { Clear(); }
ConstDeviceDesc::_DeviceDesc_::_DeviceDesc_(const _DeviceDesc_& other) { CopyFrom(other); }
ConstDeviceDesc::_DeviceDesc_::_DeviceDesc_(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc) {
  InitFromProto(proto_devicedesc);
}
ConstDeviceDesc::_DeviceDesc_::_DeviceDesc_(_DeviceDesc_&& other) = default;
ConstDeviceDesc::_DeviceDesc_::~_DeviceDesc_() = default;

void ConstDeviceDesc::_DeviceDesc_::InitFromProto(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc) {
  Clear();
  // required_or_optional field: machine_id
  if (proto_devicedesc.has_machine_id()) {
    set_machine_id(proto_devicedesc.machine_id());
  }
  // required_or_optional field: device_type
  if (proto_devicedesc.has_device_type()) {
  set_device_type(static_cast<std::remove_reference<std::remove_const<decltype(device_type())>::type>::type>(proto_devicedesc.device_type()));
  }
  // required_or_optional field: device_id
  if (proto_devicedesc.has_device_id()) {
    set_device_id(proto_devicedesc.device_id());
  }
    
}

void ConstDeviceDesc::_DeviceDesc_::ToProto(::oneflow::boxing::collective::DeviceDesc* proto_devicedesc) const {
  proto_devicedesc->Clear();
  // required_or_optional field: machine_id
  if (this->has_machine_id()) {
    proto_devicedesc->set_machine_id(machine_id());
    }
  // required_or_optional field: device_type
  if (this->has_device_type()) {
    proto_devicedesc->set_device_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_devicedesc->device_type())>::type>::type>(device_type()));
    }
  // required_or_optional field: device_id
  if (this->has_device_id()) {
    proto_devicedesc->set_device_id(device_id());
    }

}

::std::string ConstDeviceDesc::_DeviceDesc_::DebugString() const {
  ::oneflow::boxing::collective::DeviceDesc proto_devicedesc;
  this->ToProto(&proto_devicedesc);
  return proto_devicedesc.DebugString();
}

void ConstDeviceDesc::_DeviceDesc_::Clear() {
  clear_machine_id();
  clear_device_type();
  clear_device_id();
}

void ConstDeviceDesc::_DeviceDesc_::CopyFrom(const _DeviceDesc_& other) {
  if (other.has_machine_id()) {
    set_machine_id(other.machine_id());
  } else {
    clear_machine_id();
  }
  if (other.has_device_type()) {
    set_device_type(other.device_type());
  } else {
    clear_device_type();
  }
  if (other.has_device_id()) {
    set_device_id(other.device_id());
  } else {
    clear_device_id();
  }
}


// optional field machine_id
bool ConstDeviceDesc::_DeviceDesc_::has_machine_id() const {
  return has_machine_id_;
}
const int64_t& ConstDeviceDesc::_DeviceDesc_::machine_id() const {
  if (has_machine_id_) { return machine_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstDeviceDesc::_DeviceDesc_::clear_machine_id() {
  has_machine_id_ = false;
}
void ConstDeviceDesc::_DeviceDesc_::set_machine_id(const int64_t& value) {
  machine_id_ = value;
  has_machine_id_ = true;
}
int64_t* ConstDeviceDesc::_DeviceDesc_::mutable_machine_id() {
  has_machine_id_ = true;
  return &machine_id_;
}

// optional field device_type
bool ConstDeviceDesc::_DeviceDesc_::has_device_type() const {
  return has_device_type_;
}
const ::oneflow::cfg::DeviceType& ConstDeviceDesc::_DeviceDesc_::device_type() const {
  if (has_device_type_) { return device_type_; }
  static const ::oneflow::cfg::DeviceType default_static_value = ::oneflow::cfg::DeviceType();
  return default_static_value;
}
void ConstDeviceDesc::_DeviceDesc_::clear_device_type() {
  has_device_type_ = false;
}
void ConstDeviceDesc::_DeviceDesc_::set_device_type(const ::oneflow::cfg::DeviceType& value) {
  device_type_ = value;
  has_device_type_ = true;
}
::oneflow::cfg::DeviceType* ConstDeviceDesc::_DeviceDesc_::mutable_device_type() {
  has_device_type_ = true;
  return &device_type_;
}

// optional field device_id
bool ConstDeviceDesc::_DeviceDesc_::has_device_id() const {
  return has_device_id_;
}
const int64_t& ConstDeviceDesc::_DeviceDesc_::device_id() const {
  if (has_device_id_) { return device_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstDeviceDesc::_DeviceDesc_::clear_device_id() {
  has_device_id_ = false;
}
void ConstDeviceDesc::_DeviceDesc_::set_device_id(const int64_t& value) {
  device_id_ = value;
  has_device_id_ = true;
}
int64_t* ConstDeviceDesc::_DeviceDesc_::mutable_device_id() {
  has_device_id_ = true;
  return &device_id_;
}


int ConstDeviceDesc::_DeviceDesc_::compare(const _DeviceDesc_& other) {
  if (!(has_machine_id() == other.has_machine_id())) {
    return has_machine_id() < other.has_machine_id() ? -1 : 1;
  } else if (!(machine_id() == other.machine_id())) {
    return machine_id() < other.machine_id() ? -1 : 1;
  }
  if (!(has_device_type() == other.has_device_type())) {
    return has_device_type() < other.has_device_type() ? -1 : 1;
  } else if (!(device_type() == other.device_type())) {
    return device_type() < other.device_type() ? -1 : 1;
  }
  if (!(has_device_id() == other.has_device_id())) {
    return has_device_id() < other.has_device_id() ? -1 : 1;
  } else if (!(device_id() == other.device_id())) {
    return device_id() < other.device_id() ? -1 : 1;
  }
  return 0;
}

bool ConstDeviceDesc::_DeviceDesc_::operator==(const _DeviceDesc_& other) const {
  return true
    && has_machine_id() == other.has_machine_id() 
    && machine_id() == other.machine_id()
    && has_device_type() == other.has_device_type() 
    && device_type() == other.device_type()
    && has_device_id() == other.has_device_id() 
    && device_id() == other.device_id()
  ;
}

std::size_t ConstDeviceDesc::_DeviceDesc_::__CalcHash__() const {
  return 0
    ^ (has_machine_id() ? std::hash<int64_t>()(machine_id()) : 0)
    ^ (has_device_type() ? std::hash<::oneflow::cfg::DeviceType>()(device_type()) : 0)
    ^ (has_device_id() ? std::hash<int64_t>()(device_id()) : 0)
  ;
}

bool ConstDeviceDesc::_DeviceDesc_::operator<(const _DeviceDesc_& other) const {
  return false
    || !(has_machine_id() == other.has_machine_id()) ? 
      has_machine_id() < other.has_machine_id() : false
    || !(machine_id() == other.machine_id()) ? 
      machine_id() < other.machine_id() : false
    || !(has_device_type() == other.has_device_type()) ? 
      has_device_type() < other.has_device_type() : false
    || !(device_type() == other.device_type()) ? 
      device_type() < other.device_type() : false
    || !(has_device_id() == other.has_device_id()) ? 
      has_device_id() < other.has_device_id() : false
    || !(device_id() == other.device_id()) ? 
      device_id() < other.device_id() : false
  ;
}

using _DeviceDesc_ =  ConstDeviceDesc::_DeviceDesc_;
ConstDeviceDesc::ConstDeviceDesc(const ::std::shared_ptr<_DeviceDesc_>& data): data_(data) {}
ConstDeviceDesc::ConstDeviceDesc(): data_(::std::make_shared<_DeviceDesc_>()) {}
ConstDeviceDesc::ConstDeviceDesc(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc) {
  BuildFromProto(proto_devicedesc);
}
ConstDeviceDesc::ConstDeviceDesc(const ConstDeviceDesc&) = default;
ConstDeviceDesc::ConstDeviceDesc(ConstDeviceDesc&&) noexcept = default;
ConstDeviceDesc::~ConstDeviceDesc() = default;

void ConstDeviceDesc::ToProto(PbMessage* proto_devicedesc) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::DeviceDesc*>(proto_devicedesc));
}
  
::std::string ConstDeviceDesc::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstDeviceDesc::__Empty__() const {
  return !data_;
}

int ConstDeviceDesc::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"machine_id", 1},
    {"device_type", 2},
    {"device_id", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstDeviceDesc::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstDeviceDesc::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DeviceType),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstDeviceDesc::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &machine_id();
    case 2: return &device_type();
    case 3: return &device_id();
    default: return nullptr;
  }
}

// required or optional field machine_id
bool ConstDeviceDesc::has_machine_id() const {
  return __SharedPtrOrDefault__()->has_machine_id();
}
const int64_t& ConstDeviceDesc::machine_id() const {
  return __SharedPtrOrDefault__()->machine_id();
}
// used by pybind11 only
// required or optional field device_type
bool ConstDeviceDesc::has_device_type() const {
  return __SharedPtrOrDefault__()->has_device_type();
}
const ::oneflow::cfg::DeviceType& ConstDeviceDesc::device_type() const {
  return __SharedPtrOrDefault__()->device_type();
}
// used by pybind11 only
// required or optional field device_id
bool ConstDeviceDesc::has_device_id() const {
  return __SharedPtrOrDefault__()->has_device_id();
}
const int64_t& ConstDeviceDesc::device_id() const {
  return __SharedPtrOrDefault__()->device_id();
}
// used by pybind11 only

::std::shared_ptr<ConstDeviceDesc> ConstDeviceDesc::__SharedConst__() const {
  return ::std::make_shared<ConstDeviceDesc>(data_);
}
int64_t ConstDeviceDesc::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstDeviceDesc::operator==(const ConstDeviceDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstDeviceDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstDeviceDesc::operator<(const ConstDeviceDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_DeviceDesc_>& ConstDeviceDesc::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_DeviceDesc_> default_ptr = std::make_shared<_DeviceDesc_>();
  return default_ptr;
}
const ::std::shared_ptr<_DeviceDesc_>& ConstDeviceDesc::__SharedPtr__() {
  if (!data_) { data_.reset(new _DeviceDesc_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstDeviceDesc
void ConstDeviceDesc::BuildFromProto(const PbMessage& proto_devicedesc) {
  data_ = ::std::make_shared<_DeviceDesc_>(dynamic_cast<const ::oneflow::boxing::collective::DeviceDesc&>(proto_devicedesc));
}

DeviceDesc::DeviceDesc(const ::std::shared_ptr<_DeviceDesc_>& data)
  : ConstDeviceDesc(data) {}
DeviceDesc::DeviceDesc(const DeviceDesc& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<DeviceDesc> resize
DeviceDesc::DeviceDesc(DeviceDesc&&) noexcept = default; 
DeviceDesc::DeviceDesc(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc) {
  InitFromProto(proto_devicedesc);
}
DeviceDesc::DeviceDesc() = default;

DeviceDesc::~DeviceDesc() = default;

void DeviceDesc::InitFromProto(const PbMessage& proto_devicedesc) {
  BuildFromProto(proto_devicedesc);
}
  
void* DeviceDesc::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_machine_id();
    case 2: return mutable_device_type();
    case 3: return mutable_device_id();
    default: return nullptr;
  }
}

bool DeviceDesc::operator==(const DeviceDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t DeviceDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool DeviceDesc::operator<(const DeviceDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void DeviceDesc::Clear() {
  if (data_) { data_.reset(); }
}
void DeviceDesc::CopyFrom(const DeviceDesc& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
DeviceDesc& DeviceDesc::operator=(const DeviceDesc& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field machine_id
void DeviceDesc::clear_machine_id() {
  return __SharedPtr__()->clear_machine_id();
}
void DeviceDesc::set_machine_id(const int64_t& value) {
  return __SharedPtr__()->set_machine_id(value);
}
int64_t* DeviceDesc::mutable_machine_id() {
  return  __SharedPtr__()->mutable_machine_id();
}
// required or optional field device_type
void DeviceDesc::clear_device_type() {
  return __SharedPtr__()->clear_device_type();
}
void DeviceDesc::set_device_type(const ::oneflow::cfg::DeviceType& value) {
  return __SharedPtr__()->set_device_type(value);
}
::oneflow::cfg::DeviceType* DeviceDesc::mutable_device_type() {
  return  __SharedPtr__()->mutable_device_type();
}
// required or optional field device_id
void DeviceDesc::clear_device_id() {
  return __SharedPtr__()->clear_device_id();
}
void DeviceDesc::set_device_id(const int64_t& value) {
  return __SharedPtr__()->set_device_id(value);
}
int64_t* DeviceDesc::mutable_device_id() {
  return  __SharedPtr__()->mutable_device_id();
}

::std::shared_ptr<DeviceDesc> DeviceDesc::__SharedMutable__() {
  return ::std::make_shared<DeviceDesc>(__SharedPtr__());
}
ConstDeviceSet::_DeviceSet_::_DeviceSet_() { Clear(); }
ConstDeviceSet::_DeviceSet_::_DeviceSet_(const _DeviceSet_& other) { CopyFrom(other); }
ConstDeviceSet::_DeviceSet_::_DeviceSet_(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset) {
  InitFromProto(proto_deviceset);
}
ConstDeviceSet::_DeviceSet_::_DeviceSet_(_DeviceSet_&& other) = default;
ConstDeviceSet::_DeviceSet_::~_DeviceSet_() = default;

void ConstDeviceSet::_DeviceSet_::InitFromProto(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset) {
  Clear();
  // repeated field: device
  if (!proto_deviceset.device().empty()) {
    for (const ::oneflow::boxing::collective::DeviceDesc& elem : proto_deviceset.device() ) {
      *mutable_device()->Add() = ::oneflow::boxing::collective::cfg::DeviceDesc(elem);
    }
  }
    
}

void ConstDeviceSet::_DeviceSet_::ToProto(::oneflow::boxing::collective::DeviceSet* proto_deviceset) const {
  proto_deviceset->Clear();
  // repeated field: device
  if (!device().empty()) {
    for (const ::oneflow::boxing::collective::cfg::DeviceDesc& elem : device() ) {
      ::oneflow::boxing::collective::DeviceDesc proto_device_elem;
      elem.ToProto(&proto_device_elem);
      *proto_deviceset->mutable_device()->Add() = proto_device_elem;
    }
  }

}

::std::string ConstDeviceSet::_DeviceSet_::DebugString() const {
  ::oneflow::boxing::collective::DeviceSet proto_deviceset;
  this->ToProto(&proto_deviceset);
  return proto_deviceset.DebugString();
}

void ConstDeviceSet::_DeviceSet_::Clear() {
  clear_device();
}

void ConstDeviceSet::_DeviceSet_::CopyFrom(const _DeviceSet_& other) {
  mutable_device()->CopyFrom(other.device());
}


// repeated field device
::std::size_t ConstDeviceSet::_DeviceSet_::device_size() const {
  if (!device_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
    return default_static_value->size();
  }
  return device_->size();
}
const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& ConstDeviceSet::_DeviceSet_::device() const {
  if (!device_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
    return *(default_static_value.get());
  }
  return *(device_.get());
}
const ::oneflow::boxing::collective::cfg::DeviceDesc& ConstDeviceSet::_DeviceSet_::device(::std::size_t index) const {
  if (!device_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
    return default_static_value->Get(index);
  }
  return device_->Get(index);
}
void ConstDeviceSet::_DeviceSet_::clear_device() {
  if (!device_) {
    device_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
  }
  return device_->Clear();
}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_* ConstDeviceSet::_DeviceSet_::mutable_device() {
  if (!device_) {
    device_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
  }
  return  device_.get();
}
::oneflow::boxing::collective::cfg::DeviceDesc* ConstDeviceSet::_DeviceSet_::mutable_device(::std::size_t index) {
  if (!device_) {
    device_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
  }
  return  device_->Mutable(index);
}
::oneflow::boxing::collective::cfg::DeviceDesc* ConstDeviceSet::_DeviceSet_::add_device() {
  if (!device_) {
    device_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>();
  }
  return device_->Add();
}


int ConstDeviceSet::_DeviceSet_::compare(const _DeviceSet_& other) {
  if (!(device() == other.device())) {
    return device() < other.device() ? -1 : 1;
  }
  return 0;
}

bool ConstDeviceSet::_DeviceSet_::operator==(const _DeviceSet_& other) const {
  return true
    && device() == other.device()
  ;
}

std::size_t ConstDeviceSet::_DeviceSet_::__CalcHash__() const {
  return 0
    ^ device().__CalcHash__()
  ;
}

bool ConstDeviceSet::_DeviceSet_::operator<(const _DeviceSet_& other) const {
  return false
    || !(device() == other.device()) ? 
      device() < other.device() : false
  ;
}

using _DeviceSet_ =  ConstDeviceSet::_DeviceSet_;
ConstDeviceSet::ConstDeviceSet(const ::std::shared_ptr<_DeviceSet_>& data): data_(data) {}
ConstDeviceSet::ConstDeviceSet(): data_(::std::make_shared<_DeviceSet_>()) {}
ConstDeviceSet::ConstDeviceSet(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset) {
  BuildFromProto(proto_deviceset);
}
ConstDeviceSet::ConstDeviceSet(const ConstDeviceSet&) = default;
ConstDeviceSet::ConstDeviceSet(ConstDeviceSet&&) noexcept = default;
ConstDeviceSet::~ConstDeviceSet() = default;

void ConstDeviceSet::ToProto(PbMessage* proto_deviceset) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::DeviceSet*>(proto_deviceset));
}
  
::std::string ConstDeviceSet::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstDeviceSet::__Empty__() const {
  return !data_;
}

int ConstDeviceSet::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"device", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstDeviceSet::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstDeviceSet::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::DeviceDesc>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstDeviceSet::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &device();
    default: return nullptr;
  }
}

// repeated field device
::std::size_t ConstDeviceSet::device_size() const {
  return __SharedPtrOrDefault__()->device_size();
}
const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& ConstDeviceSet::device() const {
  return __SharedPtrOrDefault__()->device();
}
const ::oneflow::boxing::collective::cfg::DeviceDesc& ConstDeviceSet::device(::std::size_t index) const {
  return __SharedPtrOrDefault__()->device(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> ConstDeviceSet::shared_const_device() const {
  return device().__SharedConst__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceDesc> ConstDeviceSet::shared_const_device(::std::size_t index) const {
  return device(index).__SharedConst__();
}

::std::shared_ptr<ConstDeviceSet> ConstDeviceSet::__SharedConst__() const {
  return ::std::make_shared<ConstDeviceSet>(data_);
}
int64_t ConstDeviceSet::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstDeviceSet::operator==(const ConstDeviceSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstDeviceSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstDeviceSet::operator<(const ConstDeviceSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_DeviceSet_>& ConstDeviceSet::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_DeviceSet_> default_ptr = std::make_shared<_DeviceSet_>();
  return default_ptr;
}
const ::std::shared_ptr<_DeviceSet_>& ConstDeviceSet::__SharedPtr__() {
  if (!data_) { data_.reset(new _DeviceSet_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstDeviceSet
void ConstDeviceSet::BuildFromProto(const PbMessage& proto_deviceset) {
  data_ = ::std::make_shared<_DeviceSet_>(dynamic_cast<const ::oneflow::boxing::collective::DeviceSet&>(proto_deviceset));
}

DeviceSet::DeviceSet(const ::std::shared_ptr<_DeviceSet_>& data)
  : ConstDeviceSet(data) {}
DeviceSet::DeviceSet(const DeviceSet& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<DeviceSet> resize
DeviceSet::DeviceSet(DeviceSet&&) noexcept = default; 
DeviceSet::DeviceSet(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset) {
  InitFromProto(proto_deviceset);
}
DeviceSet::DeviceSet() = default;

DeviceSet::~DeviceSet() = default;

void DeviceSet::InitFromProto(const PbMessage& proto_deviceset) {
  BuildFromProto(proto_deviceset);
}
  
void* DeviceSet::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_device();
    default: return nullptr;
  }
}

bool DeviceSet::operator==(const DeviceSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t DeviceSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool DeviceSet::operator<(const DeviceSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void DeviceSet::Clear() {
  if (data_) { data_.reset(); }
}
void DeviceSet::CopyFrom(const DeviceSet& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
DeviceSet& DeviceSet::operator=(const DeviceSet& other) {
  CopyFrom(other);
  return *this;
}

// repeated field device
void DeviceSet::clear_device() {
  return __SharedPtr__()->clear_device();
}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_* DeviceSet::mutable_device() {
  return __SharedPtr__()->mutable_device();
}
::oneflow::boxing::collective::cfg::DeviceDesc* DeviceSet::mutable_device(::std::size_t index) {
  return __SharedPtr__()->mutable_device(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> DeviceSet::shared_mutable_device() {
  return mutable_device()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> DeviceSet::shared_mutable_device(::std::size_t index) {
  return mutable_device(index)->__SharedMutable__();
}
::oneflow::boxing::collective::cfg::DeviceDesc* DeviceSet::add_device() {
  return __SharedPtr__()->add_device();
}

::std::shared_ptr<DeviceSet> DeviceSet::__SharedMutable__() {
  return ::std::make_shared<DeviceSet>(__SharedPtr__());
}
ConstOpDesc::_OpDesc_::_OpDesc_() { Clear(); }
ConstOpDesc::_OpDesc_::_OpDesc_(const _OpDesc_& other) { CopyFrom(other); }
ConstOpDesc::_OpDesc_::_OpDesc_(const ::oneflow::boxing::collective::OpDesc& proto_opdesc) {
  InitFromProto(proto_opdesc);
}
ConstOpDesc::_OpDesc_::_OpDesc_(_OpDesc_&& other) = default;
ConstOpDesc::_OpDesc_::~_OpDesc_() = default;

void ConstOpDesc::_OpDesc_::InitFromProto(const ::oneflow::boxing::collective::OpDesc& proto_opdesc) {
  Clear();
  // required_or_optional field: name
  if (proto_opdesc.has_name()) {
    set_name(proto_opdesc.name());
  }
  // required_or_optional field: op_type
  if (proto_opdesc.has_op_type()) {
  set_op_type(static_cast<std::remove_reference<std::remove_const<decltype(op_type())>::type>::type>(proto_opdesc.op_type()));
  }
  // required_or_optional field: reduce_method
  if (proto_opdesc.has_reduce_method()) {
  set_reduce_method(static_cast<std::remove_reference<std::remove_const<decltype(reduce_method())>::type>::type>(proto_opdesc.reduce_method()));
  }
  // required_or_optional field: root
  if (proto_opdesc.has_root()) {
    set_root(proto_opdesc.root());
  }
  // required_or_optional field: data_type
  if (proto_opdesc.has_data_type()) {
  set_data_type(static_cast<std::remove_reference<std::remove_const<decltype(data_type())>::type>::type>(proto_opdesc.data_type()));
  }
  // required_or_optional field: shape
  if (proto_opdesc.has_shape()) {
  *mutable_shape() = ::oneflow::cfg::ShapeProto(proto_opdesc.shape());      
  }
  // required_or_optional field: num_ranks
  if (proto_opdesc.has_num_ranks()) {
    set_num_ranks(proto_opdesc.num_ranks());
  }
  // required_or_optional field: backend
  if (proto_opdesc.has_backend()) {
  set_backend(static_cast<std::remove_reference<std::remove_const<decltype(backend())>::type>::type>(proto_opdesc.backend()));
  }
    
}

void ConstOpDesc::_OpDesc_::ToProto(::oneflow::boxing::collective::OpDesc* proto_opdesc) const {
  proto_opdesc->Clear();
  // required_or_optional field: name
  if (this->has_name()) {
    proto_opdesc->set_name(name());
    }
  // required_or_optional field: op_type
  if (this->has_op_type()) {
    proto_opdesc->set_op_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_opdesc->op_type())>::type>::type>(op_type()));
    }
  // required_or_optional field: reduce_method
  if (this->has_reduce_method()) {
    proto_opdesc->set_reduce_method(static_cast<std::remove_const<std::remove_reference<decltype(proto_opdesc->reduce_method())>::type>::type>(reduce_method()));
    }
  // required_or_optional field: root
  if (this->has_root()) {
    proto_opdesc->set_root(root());
    }
  // required_or_optional field: data_type
  if (this->has_data_type()) {
    proto_opdesc->set_data_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_opdesc->data_type())>::type>::type>(data_type()));
    }
  // required_or_optional field: shape
  if (this->has_shape()) {
    ::oneflow::ShapeProto proto_shape;
    shape().ToProto(&proto_shape);
    proto_opdesc->mutable_shape()->CopyFrom(proto_shape);
    }
  // required_or_optional field: num_ranks
  if (this->has_num_ranks()) {
    proto_opdesc->set_num_ranks(num_ranks());
    }
  // required_or_optional field: backend
  if (this->has_backend()) {
    proto_opdesc->set_backend(static_cast<std::remove_const<std::remove_reference<decltype(proto_opdesc->backend())>::type>::type>(backend()));
    }

}

::std::string ConstOpDesc::_OpDesc_::DebugString() const {
  ::oneflow::boxing::collective::OpDesc proto_opdesc;
  this->ToProto(&proto_opdesc);
  return proto_opdesc.DebugString();
}

void ConstOpDesc::_OpDesc_::Clear() {
  clear_name();
  clear_op_type();
  clear_reduce_method();
  clear_root();
  clear_data_type();
  clear_shape();
  clear_num_ranks();
  clear_backend();
}

void ConstOpDesc::_OpDesc_::CopyFrom(const _OpDesc_& other) {
  if (other.has_name()) {
    set_name(other.name());
  } else {
    clear_name();
  }
  if (other.has_op_type()) {
    set_op_type(other.op_type());
  } else {
    clear_op_type();
  }
  if (other.has_reduce_method()) {
    set_reduce_method(other.reduce_method());
  } else {
    clear_reduce_method();
  }
  if (other.has_root()) {
    set_root(other.root());
  } else {
    clear_root();
  }
  if (other.has_data_type()) {
    set_data_type(other.data_type());
  } else {
    clear_data_type();
  }
  if (other.has_shape()) {
    mutable_shape()->CopyFrom(other.shape());
  } else {
    clear_shape();
  }
  if (other.has_num_ranks()) {
    set_num_ranks(other.num_ranks());
  } else {
    clear_num_ranks();
  }
  if (other.has_backend()) {
    set_backend(other.backend());
  } else {
    clear_backend();
  }
}


// optional field name
bool ConstOpDesc::_OpDesc_::has_name() const {
  return has_name_;
}
const ::std::string& ConstOpDesc::_OpDesc_::name() const {
  if (has_name_) { return name_; }
  static const ::std::string default_static_value = ::std::string();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_name() {
  has_name_ = false;
}
void ConstOpDesc::_OpDesc_::set_name(const ::std::string& value) {
  name_ = value;
  has_name_ = true;
}
::std::string* ConstOpDesc::_OpDesc_::mutable_name() {
  has_name_ = true;
  return &name_;
}

// optional field op_type
bool ConstOpDesc::_OpDesc_::has_op_type() const {
  return has_op_type_;
}
const ::oneflow::boxing::collective::cfg::OpType& ConstOpDesc::_OpDesc_::op_type() const {
  if (has_op_type_) { return op_type_; }
  static const ::oneflow::boxing::collective::cfg::OpType default_static_value = ::oneflow::boxing::collective::cfg::OpType();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_op_type() {
  has_op_type_ = false;
}
void ConstOpDesc::_OpDesc_::set_op_type(const ::oneflow::boxing::collective::cfg::OpType& value) {
  op_type_ = value;
  has_op_type_ = true;
}
::oneflow::boxing::collective::cfg::OpType* ConstOpDesc::_OpDesc_::mutable_op_type() {
  has_op_type_ = true;
  return &op_type_;
}

// optional field reduce_method
bool ConstOpDesc::_OpDesc_::has_reduce_method() const {
  return has_reduce_method_;
}
const ::oneflow::boxing::collective::cfg::ReduceMethod& ConstOpDesc::_OpDesc_::reduce_method() const {
  if (has_reduce_method_) { return reduce_method_; }
  static const ::oneflow::boxing::collective::cfg::ReduceMethod default_static_value = ::oneflow::boxing::collective::cfg::ReduceMethod();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_reduce_method() {
  has_reduce_method_ = false;
}
void ConstOpDesc::_OpDesc_::set_reduce_method(const ::oneflow::boxing::collective::cfg::ReduceMethod& value) {
  reduce_method_ = value;
  has_reduce_method_ = true;
}
::oneflow::boxing::collective::cfg::ReduceMethod* ConstOpDesc::_OpDesc_::mutable_reduce_method() {
  has_reduce_method_ = true;
  return &reduce_method_;
}

// optional field root
bool ConstOpDesc::_OpDesc_::has_root() const {
  return has_root_;
}
const int64_t& ConstOpDesc::_OpDesc_::root() const {
  if (has_root_) { return root_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_root() {
  has_root_ = false;
}
void ConstOpDesc::_OpDesc_::set_root(const int64_t& value) {
  root_ = value;
  has_root_ = true;
}
int64_t* ConstOpDesc::_OpDesc_::mutable_root() {
  has_root_ = true;
  return &root_;
}

// optional field data_type
bool ConstOpDesc::_OpDesc_::has_data_type() const {
  return has_data_type_;
}
const ::oneflow::cfg::DataType& ConstOpDesc::_OpDesc_::data_type() const {
  if (has_data_type_) { return data_type_; }
  static const ::oneflow::cfg::DataType default_static_value = ::oneflow::cfg::DataType();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_data_type() {
  has_data_type_ = false;
}
void ConstOpDesc::_OpDesc_::set_data_type(const ::oneflow::cfg::DataType& value) {
  data_type_ = value;
  has_data_type_ = true;
}
::oneflow::cfg::DataType* ConstOpDesc::_OpDesc_::mutable_data_type() {
  has_data_type_ = true;
  return &data_type_;
}

// optional field shape
bool ConstOpDesc::_OpDesc_::has_shape() const {
  return has_shape_;
}
const ::oneflow::cfg::ShapeProto& ConstOpDesc::_OpDesc_::shape() const {
  if (!shape_) {
    static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
  }
  return *(shape_.get());
}
void ConstOpDesc::_OpDesc_::clear_shape() {
  if (shape_) {
    shape_->Clear();
  }
  has_shape_ = false;
}
::oneflow::cfg::ShapeProto* ConstOpDesc::_OpDesc_::mutable_shape() {
  if (!shape_) {
    shape_ = ::std::make_shared<::oneflow::cfg::ShapeProto>();
  }
  has_shape_ = true;
  return shape_.get();
}

// optional field num_ranks
bool ConstOpDesc::_OpDesc_::has_num_ranks() const {
  return has_num_ranks_;
}
const int64_t& ConstOpDesc::_OpDesc_::num_ranks() const {
  if (has_num_ranks_) { return num_ranks_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_num_ranks() {
  has_num_ranks_ = false;
}
void ConstOpDesc::_OpDesc_::set_num_ranks(const int64_t& value) {
  num_ranks_ = value;
  has_num_ranks_ = true;
}
int64_t* ConstOpDesc::_OpDesc_::mutable_num_ranks() {
  has_num_ranks_ = true;
  return &num_ranks_;
}

// optional field backend
bool ConstOpDesc::_OpDesc_::has_backend() const {
  return has_backend_;
}
const ::oneflow::boxing::collective::cfg::Backend& ConstOpDesc::_OpDesc_::backend() const {
  if (has_backend_) { return backend_; }
  static const ::oneflow::boxing::collective::cfg::Backend default_static_value = ::oneflow::boxing::collective::cfg::Backend();
  return default_static_value;
}
void ConstOpDesc::_OpDesc_::clear_backend() {
  has_backend_ = false;
}
void ConstOpDesc::_OpDesc_::set_backend(const ::oneflow::boxing::collective::cfg::Backend& value) {
  backend_ = value;
  has_backend_ = true;
}
::oneflow::boxing::collective::cfg::Backend* ConstOpDesc::_OpDesc_::mutable_backend() {
  has_backend_ = true;
  return &backend_;
}


int ConstOpDesc::_OpDesc_::compare(const _OpDesc_& other) {
  if (!(has_name() == other.has_name())) {
    return has_name() < other.has_name() ? -1 : 1;
  } else if (!(name() == other.name())) {
    return name() < other.name() ? -1 : 1;
  }
  if (!(has_op_type() == other.has_op_type())) {
    return has_op_type() < other.has_op_type() ? -1 : 1;
  } else if (!(op_type() == other.op_type())) {
    return op_type() < other.op_type() ? -1 : 1;
  }
  if (!(has_reduce_method() == other.has_reduce_method())) {
    return has_reduce_method() < other.has_reduce_method() ? -1 : 1;
  } else if (!(reduce_method() == other.reduce_method())) {
    return reduce_method() < other.reduce_method() ? -1 : 1;
  }
  if (!(has_root() == other.has_root())) {
    return has_root() < other.has_root() ? -1 : 1;
  } else if (!(root() == other.root())) {
    return root() < other.root() ? -1 : 1;
  }
  if (!(has_data_type() == other.has_data_type())) {
    return has_data_type() < other.has_data_type() ? -1 : 1;
  } else if (!(data_type() == other.data_type())) {
    return data_type() < other.data_type() ? -1 : 1;
  }
  if (!(has_shape() == other.has_shape())) {
    return has_shape() < other.has_shape() ? -1 : 1;
  } else if (!(shape() == other.shape())) {
    return shape() < other.shape() ? -1 : 1;
  }
  if (!(has_num_ranks() == other.has_num_ranks())) {
    return has_num_ranks() < other.has_num_ranks() ? -1 : 1;
  } else if (!(num_ranks() == other.num_ranks())) {
    return num_ranks() < other.num_ranks() ? -1 : 1;
  }
  if (!(has_backend() == other.has_backend())) {
    return has_backend() < other.has_backend() ? -1 : 1;
  } else if (!(backend() == other.backend())) {
    return backend() < other.backend() ? -1 : 1;
  }
  return 0;
}

bool ConstOpDesc::_OpDesc_::operator==(const _OpDesc_& other) const {
  return true
    && has_name() == other.has_name() 
    && name() == other.name()
    && has_op_type() == other.has_op_type() 
    && op_type() == other.op_type()
    && has_reduce_method() == other.has_reduce_method() 
    && reduce_method() == other.reduce_method()
    && has_root() == other.has_root() 
    && root() == other.root()
    && has_data_type() == other.has_data_type() 
    && data_type() == other.data_type()
    && has_shape() == other.has_shape() 
    && shape() == other.shape()
    && has_num_ranks() == other.has_num_ranks() 
    && num_ranks() == other.num_ranks()
    && has_backend() == other.has_backend() 
    && backend() == other.backend()
  ;
}

std::size_t ConstOpDesc::_OpDesc_::__CalcHash__() const {
  return 0
    ^ (has_name() ? std::hash<::std::string>()(name()) : 0)
    ^ (has_op_type() ? std::hash<::oneflow::boxing::collective::cfg::OpType>()(op_type()) : 0)
    ^ (has_reduce_method() ? std::hash<::oneflow::boxing::collective::cfg::ReduceMethod>()(reduce_method()) : 0)
    ^ (has_root() ? std::hash<int64_t>()(root()) : 0)
    ^ (has_data_type() ? std::hash<::oneflow::cfg::DataType>()(data_type()) : 0)
    ^ (has_shape() ? std::hash<::oneflow::cfg::ShapeProto>()(shape()) : 0)
    ^ (has_num_ranks() ? std::hash<int64_t>()(num_ranks()) : 0)
    ^ (has_backend() ? std::hash<::oneflow::boxing::collective::cfg::Backend>()(backend()) : 0)
  ;
}

bool ConstOpDesc::_OpDesc_::operator<(const _OpDesc_& other) const {
  return false
    || !(has_name() == other.has_name()) ? 
      has_name() < other.has_name() : false
    || !(name() == other.name()) ? 
      name() < other.name() : false
    || !(has_op_type() == other.has_op_type()) ? 
      has_op_type() < other.has_op_type() : false
    || !(op_type() == other.op_type()) ? 
      op_type() < other.op_type() : false
    || !(has_reduce_method() == other.has_reduce_method()) ? 
      has_reduce_method() < other.has_reduce_method() : false
    || !(reduce_method() == other.reduce_method()) ? 
      reduce_method() < other.reduce_method() : false
    || !(has_root() == other.has_root()) ? 
      has_root() < other.has_root() : false
    || !(root() == other.root()) ? 
      root() < other.root() : false
    || !(has_data_type() == other.has_data_type()) ? 
      has_data_type() < other.has_data_type() : false
    || !(data_type() == other.data_type()) ? 
      data_type() < other.data_type() : false
    || !(has_shape() == other.has_shape()) ? 
      has_shape() < other.has_shape() : false
    || !(shape() == other.shape()) ? 
      shape() < other.shape() : false
    || !(has_num_ranks() == other.has_num_ranks()) ? 
      has_num_ranks() < other.has_num_ranks() : false
    || !(num_ranks() == other.num_ranks()) ? 
      num_ranks() < other.num_ranks() : false
    || !(has_backend() == other.has_backend()) ? 
      has_backend() < other.has_backend() : false
    || !(backend() == other.backend()) ? 
      backend() < other.backend() : false
  ;
}

using _OpDesc_ =  ConstOpDesc::_OpDesc_;
ConstOpDesc::ConstOpDesc(const ::std::shared_ptr<_OpDesc_>& data): data_(data) {}
ConstOpDesc::ConstOpDesc(): data_(::std::make_shared<_OpDesc_>()) {}
ConstOpDesc::ConstOpDesc(const ::oneflow::boxing::collective::OpDesc& proto_opdesc) {
  BuildFromProto(proto_opdesc);
}
ConstOpDesc::ConstOpDesc(const ConstOpDesc&) = default;
ConstOpDesc::ConstOpDesc(ConstOpDesc&&) noexcept = default;
ConstOpDesc::~ConstOpDesc() = default;

void ConstOpDesc::ToProto(PbMessage* proto_opdesc) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::OpDesc*>(proto_opdesc));
}
  
::std::string ConstOpDesc::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpDesc::__Empty__() const {
  return !data_;
}

int ConstOpDesc::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"name", 1},
    {"op_type", 2},
    {"reduce_method", 3},
    {"root", 4},
    {"data_type", 5},
    {"shape", 6},
    {"num_ranks", 7},
    {"backend", 8},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpDesc::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpDesc::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::OpType),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::ReduceMethod),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 8: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::Backend),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpDesc::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &name();
    case 2: return &op_type();
    case 3: return &reduce_method();
    case 4: return &root();
    case 5: return &data_type();
    case 6: return &shape();
    case 7: return &num_ranks();
    case 8: return &backend();
    default: return nullptr;
  }
}

// required or optional field name
bool ConstOpDesc::has_name() const {
  return __SharedPtrOrDefault__()->has_name();
}
const ::std::string& ConstOpDesc::name() const {
  return __SharedPtrOrDefault__()->name();
}
// used by pybind11 only
// required or optional field op_type
bool ConstOpDesc::has_op_type() const {
  return __SharedPtrOrDefault__()->has_op_type();
}
const ::oneflow::boxing::collective::cfg::OpType& ConstOpDesc::op_type() const {
  return __SharedPtrOrDefault__()->op_type();
}
// used by pybind11 only
// required or optional field reduce_method
bool ConstOpDesc::has_reduce_method() const {
  return __SharedPtrOrDefault__()->has_reduce_method();
}
const ::oneflow::boxing::collective::cfg::ReduceMethod& ConstOpDesc::reduce_method() const {
  return __SharedPtrOrDefault__()->reduce_method();
}
// used by pybind11 only
// required or optional field root
bool ConstOpDesc::has_root() const {
  return __SharedPtrOrDefault__()->has_root();
}
const int64_t& ConstOpDesc::root() const {
  return __SharedPtrOrDefault__()->root();
}
// used by pybind11 only
// required or optional field data_type
bool ConstOpDesc::has_data_type() const {
  return __SharedPtrOrDefault__()->has_data_type();
}
const ::oneflow::cfg::DataType& ConstOpDesc::data_type() const {
  return __SharedPtrOrDefault__()->data_type();
}
// used by pybind11 only
// required or optional field shape
bool ConstOpDesc::has_shape() const {
  return __SharedPtrOrDefault__()->has_shape();
}
const ::oneflow::cfg::ShapeProto& ConstOpDesc::shape() const {
  return __SharedPtrOrDefault__()->shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstOpDesc::shared_const_shape() const {
  return shape().__SharedConst__();
}
// required or optional field num_ranks
bool ConstOpDesc::has_num_ranks() const {
  return __SharedPtrOrDefault__()->has_num_ranks();
}
const int64_t& ConstOpDesc::num_ranks() const {
  return __SharedPtrOrDefault__()->num_ranks();
}
// used by pybind11 only
// required or optional field backend
bool ConstOpDesc::has_backend() const {
  return __SharedPtrOrDefault__()->has_backend();
}
const ::oneflow::boxing::collective::cfg::Backend& ConstOpDesc::backend() const {
  return __SharedPtrOrDefault__()->backend();
}
// used by pybind11 only

::std::shared_ptr<ConstOpDesc> ConstOpDesc::__SharedConst__() const {
  return ::std::make_shared<ConstOpDesc>(data_);
}
int64_t ConstOpDesc::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpDesc::operator==(const ConstOpDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpDesc::operator<(const ConstOpDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpDesc_>& ConstOpDesc::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpDesc_> default_ptr = std::make_shared<_OpDesc_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpDesc_>& ConstOpDesc::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpDesc_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpDesc
void ConstOpDesc::BuildFromProto(const PbMessage& proto_opdesc) {
  data_ = ::std::make_shared<_OpDesc_>(dynamic_cast<const ::oneflow::boxing::collective::OpDesc&>(proto_opdesc));
}

OpDesc::OpDesc(const ::std::shared_ptr<_OpDesc_>& data)
  : ConstOpDesc(data) {}
OpDesc::OpDesc(const OpDesc& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpDesc> resize
OpDesc::OpDesc(OpDesc&&) noexcept = default; 
OpDesc::OpDesc(const ::oneflow::boxing::collective::OpDesc& proto_opdesc) {
  InitFromProto(proto_opdesc);
}
OpDesc::OpDesc() = default;

OpDesc::~OpDesc() = default;

void OpDesc::InitFromProto(const PbMessage& proto_opdesc) {
  BuildFromProto(proto_opdesc);
}
  
void* OpDesc::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_name();
    case 2: return mutable_op_type();
    case 3: return mutable_reduce_method();
    case 4: return mutable_root();
    case 5: return mutable_data_type();
    case 6: return mutable_shape();
    case 7: return mutable_num_ranks();
    case 8: return mutable_backend();
    default: return nullptr;
  }
}

bool OpDesc::operator==(const OpDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpDesc::operator<(const OpDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpDesc::Clear() {
  if (data_) { data_.reset(); }
}
void OpDesc::CopyFrom(const OpDesc& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpDesc& OpDesc::operator=(const OpDesc& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field name
void OpDesc::clear_name() {
  return __SharedPtr__()->clear_name();
}
void OpDesc::set_name(const ::std::string& value) {
  return __SharedPtr__()->set_name(value);
}
::std::string* OpDesc::mutable_name() {
  return  __SharedPtr__()->mutable_name();
}
// required or optional field op_type
void OpDesc::clear_op_type() {
  return __SharedPtr__()->clear_op_type();
}
void OpDesc::set_op_type(const ::oneflow::boxing::collective::cfg::OpType& value) {
  return __SharedPtr__()->set_op_type(value);
}
::oneflow::boxing::collective::cfg::OpType* OpDesc::mutable_op_type() {
  return  __SharedPtr__()->mutable_op_type();
}
// required or optional field reduce_method
void OpDesc::clear_reduce_method() {
  return __SharedPtr__()->clear_reduce_method();
}
void OpDesc::set_reduce_method(const ::oneflow::boxing::collective::cfg::ReduceMethod& value) {
  return __SharedPtr__()->set_reduce_method(value);
}
::oneflow::boxing::collective::cfg::ReduceMethod* OpDesc::mutable_reduce_method() {
  return  __SharedPtr__()->mutable_reduce_method();
}
// required or optional field root
void OpDesc::clear_root() {
  return __SharedPtr__()->clear_root();
}
void OpDesc::set_root(const int64_t& value) {
  return __SharedPtr__()->set_root(value);
}
int64_t* OpDesc::mutable_root() {
  return  __SharedPtr__()->mutable_root();
}
// required or optional field data_type
void OpDesc::clear_data_type() {
  return __SharedPtr__()->clear_data_type();
}
void OpDesc::set_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_data_type(value);
}
::oneflow::cfg::DataType* OpDesc::mutable_data_type() {
  return  __SharedPtr__()->mutable_data_type();
}
// required or optional field shape
void OpDesc::clear_shape() {
  return __SharedPtr__()->clear_shape();
}
::oneflow::cfg::ShapeProto* OpDesc::mutable_shape() {
  return __SharedPtr__()->mutable_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> OpDesc::shared_mutable_shape() {
  return mutable_shape()->__SharedMutable__();
}
// required or optional field num_ranks
void OpDesc::clear_num_ranks() {
  return __SharedPtr__()->clear_num_ranks();
}
void OpDesc::set_num_ranks(const int64_t& value) {
  return __SharedPtr__()->set_num_ranks(value);
}
int64_t* OpDesc::mutable_num_ranks() {
  return  __SharedPtr__()->mutable_num_ranks();
}
// required or optional field backend
void OpDesc::clear_backend() {
  return __SharedPtr__()->clear_backend();
}
void OpDesc::set_backend(const ::oneflow::boxing::collective::cfg::Backend& value) {
  return __SharedPtr__()->set_backend(value);
}
::oneflow::boxing::collective::cfg::Backend* OpDesc::mutable_backend() {
  return  __SharedPtr__()->mutable_backend();
}

::std::shared_ptr<OpDesc> OpDesc::__SharedMutable__() {
  return ::std::make_shared<OpDesc>(__SharedPtr__());
}
ConstRequestDesc::_RequestDesc_::_RequestDesc_() { Clear(); }
ConstRequestDesc::_RequestDesc_::_RequestDesc_(const _RequestDesc_& other) { CopyFrom(other); }
ConstRequestDesc::_RequestDesc_::_RequestDesc_(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc) {
  InitFromProto(proto_requestdesc);
}
ConstRequestDesc::_RequestDesc_::_RequestDesc_(_RequestDesc_&& other) = default;
ConstRequestDesc::_RequestDesc_::~_RequestDesc_() = default;

void ConstRequestDesc::_RequestDesc_::InitFromProto(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc) {
  Clear();
  // required_or_optional field: op_desc
  if (proto_requestdesc.has_op_desc()) {
  *mutable_op_desc() = ::oneflow::boxing::collective::cfg::OpDesc(proto_requestdesc.op_desc());      
  }
  // required_or_optional field: device_set
  if (proto_requestdesc.has_device_set()) {
  *mutable_device_set() = ::oneflow::boxing::collective::cfg::DeviceSet(proto_requestdesc.device_set());      
  }
  // required_or_optional field: order
  if (proto_requestdesc.has_order()) {
    set_order(proto_requestdesc.order());
  }
  // required_or_optional field: dependency_depth
  if (proto_requestdesc.has_dependency_depth()) {
    set_dependency_depth(proto_requestdesc.dependency_depth());
  }
    
}

void ConstRequestDesc::_RequestDesc_::ToProto(::oneflow::boxing::collective::RequestDesc* proto_requestdesc) const {
  proto_requestdesc->Clear();
  // required_or_optional field: op_desc
  if (this->has_op_desc()) {
    ::oneflow::boxing::collective::OpDesc proto_op_desc;
    op_desc().ToProto(&proto_op_desc);
    proto_requestdesc->mutable_op_desc()->CopyFrom(proto_op_desc);
    }
  // required_or_optional field: device_set
  if (this->has_device_set()) {
    ::oneflow::boxing::collective::DeviceSet proto_device_set;
    device_set().ToProto(&proto_device_set);
    proto_requestdesc->mutable_device_set()->CopyFrom(proto_device_set);
    }
  // required_or_optional field: order
  if (this->has_order()) {
    proto_requestdesc->set_order(order());
    }
  // required_or_optional field: dependency_depth
  if (this->has_dependency_depth()) {
    proto_requestdesc->set_dependency_depth(dependency_depth());
    }

}

::std::string ConstRequestDesc::_RequestDesc_::DebugString() const {
  ::oneflow::boxing::collective::RequestDesc proto_requestdesc;
  this->ToProto(&proto_requestdesc);
  return proto_requestdesc.DebugString();
}

void ConstRequestDesc::_RequestDesc_::Clear() {
  clear_op_desc();
  clear_device_set();
  clear_order();
  clear_dependency_depth();
}

void ConstRequestDesc::_RequestDesc_::CopyFrom(const _RequestDesc_& other) {
  if (other.has_op_desc()) {
    mutable_op_desc()->CopyFrom(other.op_desc());
  } else {
    clear_op_desc();
  }
  if (other.has_device_set()) {
    mutable_device_set()->CopyFrom(other.device_set());
  } else {
    clear_device_set();
  }
  if (other.has_order()) {
    set_order(other.order());
  } else {
    clear_order();
  }
  if (other.has_dependency_depth()) {
    set_dependency_depth(other.dependency_depth());
  } else {
    clear_dependency_depth();
  }
}


// optional field op_desc
bool ConstRequestDesc::_RequestDesc_::has_op_desc() const {
  return has_op_desc_;
}
const ::oneflow::boxing::collective::cfg::OpDesc& ConstRequestDesc::_RequestDesc_::op_desc() const {
  if (!op_desc_) {
    static const ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> default_static_value =
      ::std::make_shared<::oneflow::boxing::collective::cfg::OpDesc>();
    return *default_static_value;
  }
  return *(op_desc_.get());
}
void ConstRequestDesc::_RequestDesc_::clear_op_desc() {
  if (op_desc_) {
    op_desc_->Clear();
  }
  has_op_desc_ = false;
}
::oneflow::boxing::collective::cfg::OpDesc* ConstRequestDesc::_RequestDesc_::mutable_op_desc() {
  if (!op_desc_) {
    op_desc_ = ::std::make_shared<::oneflow::boxing::collective::cfg::OpDesc>();
  }
  has_op_desc_ = true;
  return op_desc_.get();
}

// optional field device_set
bool ConstRequestDesc::_RequestDesc_::has_device_set() const {
  return has_device_set_;
}
const ::oneflow::boxing::collective::cfg::DeviceSet& ConstRequestDesc::_RequestDesc_::device_set() const {
  if (!device_set_) {
    static const ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceSet> default_static_value =
      ::std::make_shared<::oneflow::boxing::collective::cfg::DeviceSet>();
    return *default_static_value;
  }
  return *(device_set_.get());
}
void ConstRequestDesc::_RequestDesc_::clear_device_set() {
  if (device_set_) {
    device_set_->Clear();
  }
  has_device_set_ = false;
}
::oneflow::boxing::collective::cfg::DeviceSet* ConstRequestDesc::_RequestDesc_::mutable_device_set() {
  if (!device_set_) {
    device_set_ = ::std::make_shared<::oneflow::boxing::collective::cfg::DeviceSet>();
  }
  has_device_set_ = true;
  return device_set_.get();
}

// optional field order
bool ConstRequestDesc::_RequestDesc_::has_order() const {
  return has_order_;
}
const int64_t& ConstRequestDesc::_RequestDesc_::order() const {
  if (has_order_) { return order_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstRequestDesc::_RequestDesc_::clear_order() {
  has_order_ = false;
}
void ConstRequestDesc::_RequestDesc_::set_order(const int64_t& value) {
  order_ = value;
  has_order_ = true;
}
int64_t* ConstRequestDesc::_RequestDesc_::mutable_order() {
  has_order_ = true;
  return &order_;
}

// optional field dependency_depth
bool ConstRequestDesc::_RequestDesc_::has_dependency_depth() const {
  return has_dependency_depth_;
}
const int64_t& ConstRequestDesc::_RequestDesc_::dependency_depth() const {
  if (has_dependency_depth_) { return dependency_depth_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstRequestDesc::_RequestDesc_::clear_dependency_depth() {
  has_dependency_depth_ = false;
}
void ConstRequestDesc::_RequestDesc_::set_dependency_depth(const int64_t& value) {
  dependency_depth_ = value;
  has_dependency_depth_ = true;
}
int64_t* ConstRequestDesc::_RequestDesc_::mutable_dependency_depth() {
  has_dependency_depth_ = true;
  return &dependency_depth_;
}


int ConstRequestDesc::_RequestDesc_::compare(const _RequestDesc_& other) {
  if (!(has_op_desc() == other.has_op_desc())) {
    return has_op_desc() < other.has_op_desc() ? -1 : 1;
  } else if (!(op_desc() == other.op_desc())) {
    return op_desc() < other.op_desc() ? -1 : 1;
  }
  if (!(has_device_set() == other.has_device_set())) {
    return has_device_set() < other.has_device_set() ? -1 : 1;
  } else if (!(device_set() == other.device_set())) {
    return device_set() < other.device_set() ? -1 : 1;
  }
  if (!(has_order() == other.has_order())) {
    return has_order() < other.has_order() ? -1 : 1;
  } else if (!(order() == other.order())) {
    return order() < other.order() ? -1 : 1;
  }
  if (!(has_dependency_depth() == other.has_dependency_depth())) {
    return has_dependency_depth() < other.has_dependency_depth() ? -1 : 1;
  } else if (!(dependency_depth() == other.dependency_depth())) {
    return dependency_depth() < other.dependency_depth() ? -1 : 1;
  }
  return 0;
}

bool ConstRequestDesc::_RequestDesc_::operator==(const _RequestDesc_& other) const {
  return true
    && has_op_desc() == other.has_op_desc() 
    && op_desc() == other.op_desc()
    && has_device_set() == other.has_device_set() 
    && device_set() == other.device_set()
    && has_order() == other.has_order() 
    && order() == other.order()
    && has_dependency_depth() == other.has_dependency_depth() 
    && dependency_depth() == other.dependency_depth()
  ;
}

std::size_t ConstRequestDesc::_RequestDesc_::__CalcHash__() const {
  return 0
    ^ (has_op_desc() ? std::hash<::oneflow::boxing::collective::cfg::OpDesc>()(op_desc()) : 0)
    ^ (has_device_set() ? std::hash<::oneflow::boxing::collective::cfg::DeviceSet>()(device_set()) : 0)
    ^ (has_order() ? std::hash<int64_t>()(order()) : 0)
    ^ (has_dependency_depth() ? std::hash<int64_t>()(dependency_depth()) : 0)
  ;
}

bool ConstRequestDesc::_RequestDesc_::operator<(const _RequestDesc_& other) const {
  return false
    || !(has_op_desc() == other.has_op_desc()) ? 
      has_op_desc() < other.has_op_desc() : false
    || !(op_desc() == other.op_desc()) ? 
      op_desc() < other.op_desc() : false
    || !(has_device_set() == other.has_device_set()) ? 
      has_device_set() < other.has_device_set() : false
    || !(device_set() == other.device_set()) ? 
      device_set() < other.device_set() : false
    || !(has_order() == other.has_order()) ? 
      has_order() < other.has_order() : false
    || !(order() == other.order()) ? 
      order() < other.order() : false
    || !(has_dependency_depth() == other.has_dependency_depth()) ? 
      has_dependency_depth() < other.has_dependency_depth() : false
    || !(dependency_depth() == other.dependency_depth()) ? 
      dependency_depth() < other.dependency_depth() : false
  ;
}

using _RequestDesc_ =  ConstRequestDesc::_RequestDesc_;
ConstRequestDesc::ConstRequestDesc(const ::std::shared_ptr<_RequestDesc_>& data): data_(data) {}
ConstRequestDesc::ConstRequestDesc(): data_(::std::make_shared<_RequestDesc_>()) {}
ConstRequestDesc::ConstRequestDesc(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc) {
  BuildFromProto(proto_requestdesc);
}
ConstRequestDesc::ConstRequestDesc(const ConstRequestDesc&) = default;
ConstRequestDesc::ConstRequestDesc(ConstRequestDesc&&) noexcept = default;
ConstRequestDesc::~ConstRequestDesc() = default;

void ConstRequestDesc::ToProto(PbMessage* proto_requestdesc) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::RequestDesc*>(proto_requestdesc));
}
  
::std::string ConstRequestDesc::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRequestDesc::__Empty__() const {
  return !data_;
}

int ConstRequestDesc::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_desc", 1},
    {"device_set", 2},
    {"order", 3},
    {"dependency_depth", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRequestDesc::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRequestDesc::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::OpDesc),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::boxing::collective::cfg::ConstOpDesc),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::DeviceSet),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::boxing::collective::cfg::ConstDeviceSet),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRequestDesc::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_desc();
    case 2: return &device_set();
    case 3: return &order();
    case 4: return &dependency_depth();
    default: return nullptr;
  }
}

// required or optional field op_desc
bool ConstRequestDesc::has_op_desc() const {
  return __SharedPtrOrDefault__()->has_op_desc();
}
const ::oneflow::boxing::collective::cfg::OpDesc& ConstRequestDesc::op_desc() const {
  return __SharedPtrOrDefault__()->op_desc();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstOpDesc> ConstRequestDesc::shared_const_op_desc() const {
  return op_desc().__SharedConst__();
}
// required or optional field device_set
bool ConstRequestDesc::has_device_set() const {
  return __SharedPtrOrDefault__()->has_device_set();
}
const ::oneflow::boxing::collective::cfg::DeviceSet& ConstRequestDesc::device_set() const {
  return __SharedPtrOrDefault__()->device_set();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceSet> ConstRequestDesc::shared_const_device_set() const {
  return device_set().__SharedConst__();
}
// required or optional field order
bool ConstRequestDesc::has_order() const {
  return __SharedPtrOrDefault__()->has_order();
}
const int64_t& ConstRequestDesc::order() const {
  return __SharedPtrOrDefault__()->order();
}
// used by pybind11 only
// required or optional field dependency_depth
bool ConstRequestDesc::has_dependency_depth() const {
  return __SharedPtrOrDefault__()->has_dependency_depth();
}
const int64_t& ConstRequestDesc::dependency_depth() const {
  return __SharedPtrOrDefault__()->dependency_depth();
}
// used by pybind11 only

::std::shared_ptr<ConstRequestDesc> ConstRequestDesc::__SharedConst__() const {
  return ::std::make_shared<ConstRequestDesc>(data_);
}
int64_t ConstRequestDesc::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRequestDesc::operator==(const ConstRequestDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRequestDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRequestDesc::operator<(const ConstRequestDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RequestDesc_>& ConstRequestDesc::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RequestDesc_> default_ptr = std::make_shared<_RequestDesc_>();
  return default_ptr;
}
const ::std::shared_ptr<_RequestDesc_>& ConstRequestDesc::__SharedPtr__() {
  if (!data_) { data_.reset(new _RequestDesc_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRequestDesc
void ConstRequestDesc::BuildFromProto(const PbMessage& proto_requestdesc) {
  data_ = ::std::make_shared<_RequestDesc_>(dynamic_cast<const ::oneflow::boxing::collective::RequestDesc&>(proto_requestdesc));
}

RequestDesc::RequestDesc(const ::std::shared_ptr<_RequestDesc_>& data)
  : ConstRequestDesc(data) {}
RequestDesc::RequestDesc(const RequestDesc& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RequestDesc> resize
RequestDesc::RequestDesc(RequestDesc&&) noexcept = default; 
RequestDesc::RequestDesc(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc) {
  InitFromProto(proto_requestdesc);
}
RequestDesc::RequestDesc() = default;

RequestDesc::~RequestDesc() = default;

void RequestDesc::InitFromProto(const PbMessage& proto_requestdesc) {
  BuildFromProto(proto_requestdesc);
}
  
void* RequestDesc::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_desc();
    case 2: return mutable_device_set();
    case 3: return mutable_order();
    case 4: return mutable_dependency_depth();
    default: return nullptr;
  }
}

bool RequestDesc::operator==(const RequestDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RequestDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RequestDesc::operator<(const RequestDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RequestDesc::Clear() {
  if (data_) { data_.reset(); }
}
void RequestDesc::CopyFrom(const RequestDesc& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RequestDesc& RequestDesc::operator=(const RequestDesc& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_desc
void RequestDesc::clear_op_desc() {
  return __SharedPtr__()->clear_op_desc();
}
::oneflow::boxing::collective::cfg::OpDesc* RequestDesc::mutable_op_desc() {
  return __SharedPtr__()->mutable_op_desc();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> RequestDesc::shared_mutable_op_desc() {
  return mutable_op_desc()->__SharedMutable__();
}
// required or optional field device_set
void RequestDesc::clear_device_set() {
  return __SharedPtr__()->clear_device_set();
}
::oneflow::boxing::collective::cfg::DeviceSet* RequestDesc::mutable_device_set() {
  return __SharedPtr__()->mutable_device_set();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceSet> RequestDesc::shared_mutable_device_set() {
  return mutable_device_set()->__SharedMutable__();
}
// required or optional field order
void RequestDesc::clear_order() {
  return __SharedPtr__()->clear_order();
}
void RequestDesc::set_order(const int64_t& value) {
  return __SharedPtr__()->set_order(value);
}
int64_t* RequestDesc::mutable_order() {
  return  __SharedPtr__()->mutable_order();
}
// required or optional field dependency_depth
void RequestDesc::clear_dependency_depth() {
  return __SharedPtr__()->clear_dependency_depth();
}
void RequestDesc::set_dependency_depth(const int64_t& value) {
  return __SharedPtr__()->set_dependency_depth(value);
}
int64_t* RequestDesc::mutable_dependency_depth() {
  return  __SharedPtr__()->mutable_dependency_depth();
}

::std::shared_ptr<RequestDesc> RequestDesc::__SharedMutable__() {
  return ::std::make_shared<RequestDesc>(__SharedPtr__());
}
ConstRequestSet::_RequestSet_::_RequestSet_() { Clear(); }
ConstRequestSet::_RequestSet_::_RequestSet_(const _RequestSet_& other) { CopyFrom(other); }
ConstRequestSet::_RequestSet_::_RequestSet_(const ::oneflow::boxing::collective::RequestSet& proto_requestset) {
  InitFromProto(proto_requestset);
}
ConstRequestSet::_RequestSet_::_RequestSet_(_RequestSet_&& other) = default;
ConstRequestSet::_RequestSet_::~_RequestSet_() = default;

void ConstRequestSet::_RequestSet_::InitFromProto(const ::oneflow::boxing::collective::RequestSet& proto_requestset) {
  Clear();
  // repeated field: request
  if (!proto_requestset.request().empty()) {
    for (const ::oneflow::boxing::collective::RequestDesc& elem : proto_requestset.request() ) {
      *mutable_request()->Add() = ::oneflow::boxing::collective::cfg::RequestDesc(elem);
    }
  }
    
}

void ConstRequestSet::_RequestSet_::ToProto(::oneflow::boxing::collective::RequestSet* proto_requestset) const {
  proto_requestset->Clear();
  // repeated field: request
  if (!request().empty()) {
    for (const ::oneflow::boxing::collective::cfg::RequestDesc& elem : request() ) {
      ::oneflow::boxing::collective::RequestDesc proto_request_elem;
      elem.ToProto(&proto_request_elem);
      *proto_requestset->mutable_request()->Add() = proto_request_elem;
    }
  }

}

::std::string ConstRequestSet::_RequestSet_::DebugString() const {
  ::oneflow::boxing::collective::RequestSet proto_requestset;
  this->ToProto(&proto_requestset);
  return proto_requestset.DebugString();
}

void ConstRequestSet::_RequestSet_::Clear() {
  clear_request();
}

void ConstRequestSet::_RequestSet_::CopyFrom(const _RequestSet_& other) {
  mutable_request()->CopyFrom(other.request());
}


// repeated field request
::std::size_t ConstRequestSet::_RequestSet_::request_size() const {
  if (!request_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
    return default_static_value->size();
  }
  return request_->size();
}
const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& ConstRequestSet::_RequestSet_::request() const {
  if (!request_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
    return *(default_static_value.get());
  }
  return *(request_.get());
}
const ::oneflow::boxing::collective::cfg::RequestDesc& ConstRequestSet::_RequestSet_::request(::std::size_t index) const {
  if (!request_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
    return default_static_value->Get(index);
  }
  return request_->Get(index);
}
void ConstRequestSet::_RequestSet_::clear_request() {
  if (!request_) {
    request_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
  }
  return request_->Clear();
}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_* ConstRequestSet::_RequestSet_::mutable_request() {
  if (!request_) {
    request_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
  }
  return  request_.get();
}
::oneflow::boxing::collective::cfg::RequestDesc* ConstRequestSet::_RequestSet_::mutable_request(::std::size_t index) {
  if (!request_) {
    request_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
  }
  return  request_->Mutable(index);
}
::oneflow::boxing::collective::cfg::RequestDesc* ConstRequestSet::_RequestSet_::add_request() {
  if (!request_) {
    request_ = ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>();
  }
  return request_->Add();
}


int ConstRequestSet::_RequestSet_::compare(const _RequestSet_& other) {
  if (!(request() == other.request())) {
    return request() < other.request() ? -1 : 1;
  }
  return 0;
}

bool ConstRequestSet::_RequestSet_::operator==(const _RequestSet_& other) const {
  return true
    && request() == other.request()
  ;
}

std::size_t ConstRequestSet::_RequestSet_::__CalcHash__() const {
  return 0
    ^ request().__CalcHash__()
  ;
}

bool ConstRequestSet::_RequestSet_::operator<(const _RequestSet_& other) const {
  return false
    || !(request() == other.request()) ? 
      request() < other.request() : false
  ;
}

using _RequestSet_ =  ConstRequestSet::_RequestSet_;
ConstRequestSet::ConstRequestSet(const ::std::shared_ptr<_RequestSet_>& data): data_(data) {}
ConstRequestSet::ConstRequestSet(): data_(::std::make_shared<_RequestSet_>()) {}
ConstRequestSet::ConstRequestSet(const ::oneflow::boxing::collective::RequestSet& proto_requestset) {
  BuildFromProto(proto_requestset);
}
ConstRequestSet::ConstRequestSet(const ConstRequestSet&) = default;
ConstRequestSet::ConstRequestSet(ConstRequestSet&&) noexcept = default;
ConstRequestSet::~ConstRequestSet() = default;

void ConstRequestSet::ToProto(PbMessage* proto_requestset) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::RequestSet*>(proto_requestset));
}
  
::std::string ConstRequestSet::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRequestSet::__Empty__() const {
  return !data_;
}

int ConstRequestSet::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"request", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRequestSet::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRequestSet::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::RequestDesc>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRequestSet::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &request();
    default: return nullptr;
  }
}

// repeated field request
::std::size_t ConstRequestSet::request_size() const {
  return __SharedPtrOrDefault__()->request_size();
}
const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& ConstRequestSet::request() const {
  return __SharedPtrOrDefault__()->request();
}
const ::oneflow::boxing::collective::cfg::RequestDesc& ConstRequestSet::request(::std::size_t index) const {
  return __SharedPtrOrDefault__()->request(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> ConstRequestSet::shared_const_request() const {
  return request().__SharedConst__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstRequestDesc> ConstRequestSet::shared_const_request(::std::size_t index) const {
  return request(index).__SharedConst__();
}

::std::shared_ptr<ConstRequestSet> ConstRequestSet::__SharedConst__() const {
  return ::std::make_shared<ConstRequestSet>(data_);
}
int64_t ConstRequestSet::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRequestSet::operator==(const ConstRequestSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRequestSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRequestSet::operator<(const ConstRequestSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RequestSet_>& ConstRequestSet::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RequestSet_> default_ptr = std::make_shared<_RequestSet_>();
  return default_ptr;
}
const ::std::shared_ptr<_RequestSet_>& ConstRequestSet::__SharedPtr__() {
  if (!data_) { data_.reset(new _RequestSet_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRequestSet
void ConstRequestSet::BuildFromProto(const PbMessage& proto_requestset) {
  data_ = ::std::make_shared<_RequestSet_>(dynamic_cast<const ::oneflow::boxing::collective::RequestSet&>(proto_requestset));
}

RequestSet::RequestSet(const ::std::shared_ptr<_RequestSet_>& data)
  : ConstRequestSet(data) {}
RequestSet::RequestSet(const RequestSet& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RequestSet> resize
RequestSet::RequestSet(RequestSet&&) noexcept = default; 
RequestSet::RequestSet(const ::oneflow::boxing::collective::RequestSet& proto_requestset) {
  InitFromProto(proto_requestset);
}
RequestSet::RequestSet() = default;

RequestSet::~RequestSet() = default;

void RequestSet::InitFromProto(const PbMessage& proto_requestset) {
  BuildFromProto(proto_requestset);
}
  
void* RequestSet::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_request();
    default: return nullptr;
  }
}

bool RequestSet::operator==(const RequestSet& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RequestSet::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RequestSet::operator<(const RequestSet& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RequestSet::Clear() {
  if (data_) { data_.reset(); }
}
void RequestSet::CopyFrom(const RequestSet& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RequestSet& RequestSet::operator=(const RequestSet& other) {
  CopyFrom(other);
  return *this;
}

// repeated field request
void RequestSet::clear_request() {
  return __SharedPtr__()->clear_request();
}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_* RequestSet::mutable_request() {
  return __SharedPtr__()->mutable_request();
}
::oneflow::boxing::collective::cfg::RequestDesc* RequestSet::mutable_request(::std::size_t index) {
  return __SharedPtr__()->mutable_request(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> RequestSet::shared_mutable_request() {
  return mutable_request()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> RequestSet::shared_mutable_request(::std::size_t index) {
  return mutable_request(index)->__SharedMutable__();
}
::oneflow::boxing::collective::cfg::RequestDesc* RequestSet::add_request() {
  return __SharedPtr__()->add_request();
}

::std::shared_ptr<RequestSet> RequestSet::__SharedMutable__() {
  return ::std::make_shared<RequestSet>(__SharedPtr__());
}
ConstRankDesc::_RankDesc_::_RankDesc_() { Clear(); }
ConstRankDesc::_RankDesc_::_RankDesc_(const _RankDesc_& other) { CopyFrom(other); }
ConstRankDesc::_RankDesc_::_RankDesc_(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc) {
  InitFromProto(proto_rankdesc);
}
ConstRankDesc::_RankDesc_::_RankDesc_(_RankDesc_&& other) = default;
ConstRankDesc::_RankDesc_::~_RankDesc_() = default;

void ConstRankDesc::_RankDesc_::InitFromProto(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc) {
  Clear();
  // required_or_optional field: op_desc
  if (proto_rankdesc.has_op_desc()) {
  *mutable_op_desc() = ::oneflow::boxing::collective::cfg::OpDesc(proto_rankdesc.op_desc());      
  }
  // required_or_optional field: rank
  if (proto_rankdesc.has_rank()) {
    set_rank(proto_rankdesc.rank());
  }
    
}

void ConstRankDesc::_RankDesc_::ToProto(::oneflow::boxing::collective::RankDesc* proto_rankdesc) const {
  proto_rankdesc->Clear();
  // required_or_optional field: op_desc
  if (this->has_op_desc()) {
    ::oneflow::boxing::collective::OpDesc proto_op_desc;
    op_desc().ToProto(&proto_op_desc);
    proto_rankdesc->mutable_op_desc()->CopyFrom(proto_op_desc);
    }
  // required_or_optional field: rank
  if (this->has_rank()) {
    proto_rankdesc->set_rank(rank());
    }

}

::std::string ConstRankDesc::_RankDesc_::DebugString() const {
  ::oneflow::boxing::collective::RankDesc proto_rankdesc;
  this->ToProto(&proto_rankdesc);
  return proto_rankdesc.DebugString();
}

void ConstRankDesc::_RankDesc_::Clear() {
  clear_op_desc();
  clear_rank();
}

void ConstRankDesc::_RankDesc_::CopyFrom(const _RankDesc_& other) {
  if (other.has_op_desc()) {
    mutable_op_desc()->CopyFrom(other.op_desc());
  } else {
    clear_op_desc();
  }
  if (other.has_rank()) {
    set_rank(other.rank());
  } else {
    clear_rank();
  }
}


// optional field op_desc
bool ConstRankDesc::_RankDesc_::has_op_desc() const {
  return has_op_desc_;
}
const ::oneflow::boxing::collective::cfg::OpDesc& ConstRankDesc::_RankDesc_::op_desc() const {
  if (!op_desc_) {
    static const ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> default_static_value =
      ::std::make_shared<::oneflow::boxing::collective::cfg::OpDesc>();
    return *default_static_value;
  }
  return *(op_desc_.get());
}
void ConstRankDesc::_RankDesc_::clear_op_desc() {
  if (op_desc_) {
    op_desc_->Clear();
  }
  has_op_desc_ = false;
}
::oneflow::boxing::collective::cfg::OpDesc* ConstRankDesc::_RankDesc_::mutable_op_desc() {
  if (!op_desc_) {
    op_desc_ = ::std::make_shared<::oneflow::boxing::collective::cfg::OpDesc>();
  }
  has_op_desc_ = true;
  return op_desc_.get();
}

// optional field rank
bool ConstRankDesc::_RankDesc_::has_rank() const {
  return has_rank_;
}
const int64_t& ConstRankDesc::_RankDesc_::rank() const {
  if (has_rank_) { return rank_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstRankDesc::_RankDesc_::clear_rank() {
  has_rank_ = false;
}
void ConstRankDesc::_RankDesc_::set_rank(const int64_t& value) {
  rank_ = value;
  has_rank_ = true;
}
int64_t* ConstRankDesc::_RankDesc_::mutable_rank() {
  has_rank_ = true;
  return &rank_;
}


int ConstRankDesc::_RankDesc_::compare(const _RankDesc_& other) {
  if (!(has_op_desc() == other.has_op_desc())) {
    return has_op_desc() < other.has_op_desc() ? -1 : 1;
  } else if (!(op_desc() == other.op_desc())) {
    return op_desc() < other.op_desc() ? -1 : 1;
  }
  if (!(has_rank() == other.has_rank())) {
    return has_rank() < other.has_rank() ? -1 : 1;
  } else if (!(rank() == other.rank())) {
    return rank() < other.rank() ? -1 : 1;
  }
  return 0;
}

bool ConstRankDesc::_RankDesc_::operator==(const _RankDesc_& other) const {
  return true
    && has_op_desc() == other.has_op_desc() 
    && op_desc() == other.op_desc()
    && has_rank() == other.has_rank() 
    && rank() == other.rank()
  ;
}

std::size_t ConstRankDesc::_RankDesc_::__CalcHash__() const {
  return 0
    ^ (has_op_desc() ? std::hash<::oneflow::boxing::collective::cfg::OpDesc>()(op_desc()) : 0)
    ^ (has_rank() ? std::hash<int64_t>()(rank()) : 0)
  ;
}

bool ConstRankDesc::_RankDesc_::operator<(const _RankDesc_& other) const {
  return false
    || !(has_op_desc() == other.has_op_desc()) ? 
      has_op_desc() < other.has_op_desc() : false
    || !(op_desc() == other.op_desc()) ? 
      op_desc() < other.op_desc() : false
    || !(has_rank() == other.has_rank()) ? 
      has_rank() < other.has_rank() : false
    || !(rank() == other.rank()) ? 
      rank() < other.rank() : false
  ;
}

using _RankDesc_ =  ConstRankDesc::_RankDesc_;
ConstRankDesc::ConstRankDesc(const ::std::shared_ptr<_RankDesc_>& data): data_(data) {}
ConstRankDesc::ConstRankDesc(): data_(::std::make_shared<_RankDesc_>()) {}
ConstRankDesc::ConstRankDesc(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc) {
  BuildFromProto(proto_rankdesc);
}
ConstRankDesc::ConstRankDesc(const ConstRankDesc&) = default;
ConstRankDesc::ConstRankDesc(ConstRankDesc&&) noexcept = default;
ConstRankDesc::~ConstRankDesc() = default;

void ConstRankDesc::ToProto(PbMessage* proto_rankdesc) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::boxing::collective::RankDesc*>(proto_rankdesc));
}
  
::std::string ConstRankDesc::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstRankDesc::__Empty__() const {
  return !data_;
}

int ConstRankDesc::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_desc", 1},
    {"rank", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstRankDesc::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstRankDesc::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::boxing::collective::cfg::OpDesc),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::boxing::collective::cfg::ConstOpDesc),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstRankDesc::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_desc();
    case 2: return &rank();
    default: return nullptr;
  }
}

// required or optional field op_desc
bool ConstRankDesc::has_op_desc() const {
  return __SharedPtrOrDefault__()->has_op_desc();
}
const ::oneflow::boxing::collective::cfg::OpDesc& ConstRankDesc::op_desc() const {
  return __SharedPtrOrDefault__()->op_desc();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstOpDesc> ConstRankDesc::shared_const_op_desc() const {
  return op_desc().__SharedConst__();
}
// required or optional field rank
bool ConstRankDesc::has_rank() const {
  return __SharedPtrOrDefault__()->has_rank();
}
const int64_t& ConstRankDesc::rank() const {
  return __SharedPtrOrDefault__()->rank();
}
// used by pybind11 only

::std::shared_ptr<ConstRankDesc> ConstRankDesc::__SharedConst__() const {
  return ::std::make_shared<ConstRankDesc>(data_);
}
int64_t ConstRankDesc::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstRankDesc::operator==(const ConstRankDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstRankDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstRankDesc::operator<(const ConstRankDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_RankDesc_>& ConstRankDesc::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_RankDesc_> default_ptr = std::make_shared<_RankDesc_>();
  return default_ptr;
}
const ::std::shared_ptr<_RankDesc_>& ConstRankDesc::__SharedPtr__() {
  if (!data_) { data_.reset(new _RankDesc_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstRankDesc
void ConstRankDesc::BuildFromProto(const PbMessage& proto_rankdesc) {
  data_ = ::std::make_shared<_RankDesc_>(dynamic_cast<const ::oneflow::boxing::collective::RankDesc&>(proto_rankdesc));
}

RankDesc::RankDesc(const ::std::shared_ptr<_RankDesc_>& data)
  : ConstRankDesc(data) {}
RankDesc::RankDesc(const RankDesc& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<RankDesc> resize
RankDesc::RankDesc(RankDesc&&) noexcept = default; 
RankDesc::RankDesc(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc) {
  InitFromProto(proto_rankdesc);
}
RankDesc::RankDesc() = default;

RankDesc::~RankDesc() = default;

void RankDesc::InitFromProto(const PbMessage& proto_rankdesc) {
  BuildFromProto(proto_rankdesc);
}
  
void* RankDesc::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_desc();
    case 2: return mutable_rank();
    default: return nullptr;
  }
}

bool RankDesc::operator==(const RankDesc& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t RankDesc::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool RankDesc::operator<(const RankDesc& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void RankDesc::Clear() {
  if (data_) { data_.reset(); }
}
void RankDesc::CopyFrom(const RankDesc& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
RankDesc& RankDesc::operator=(const RankDesc& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field op_desc
void RankDesc::clear_op_desc() {
  return __SharedPtr__()->clear_op_desc();
}
::oneflow::boxing::collective::cfg::OpDesc* RankDesc::mutable_op_desc() {
  return __SharedPtr__()->mutable_op_desc();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> RankDesc::shared_mutable_op_desc() {
  return mutable_op_desc()->__SharedMutable__();
}
// required or optional field rank
void RankDesc::clear_rank() {
  return __SharedPtr__()->clear_rank();
}
void RankDesc::set_rank(const int64_t& value) {
  return __SharedPtr__()->set_rank(value);
}
int64_t* RankDesc::mutable_rank() {
  return  __SharedPtr__()->mutable_rank();
}

::std::shared_ptr<RankDesc> RankDesc::__SharedMutable__() {
  return ::std::make_shared<RankDesc>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::DeviceDesc>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::DeviceDesc>(data) {}
Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_() = default;
Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::~Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_() = default;


bool Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::operator==(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::boxing::collective::cfg::DeviceDesc>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::operator<(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceDesc> Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::DeviceDesc>>& data): Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_(data) {}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_() = default;
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::~_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_() = default;

void _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::CopyFrom(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::DeviceDesc>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::CopyFrom(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::DeviceDesc>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::operator==(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::boxing::collective::cfg::DeviceDesc>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::operator<(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}
Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::RequestDesc>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::RequestDesc>(data) {}
Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_() = default;
Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::~Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_() = default;


bool Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::operator==(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::boxing::collective::cfg::RequestDesc>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::operator<(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstRequestDesc> Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::RequestDesc>>& data): Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_(data) {}
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_() = default;
_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::~_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_() = default;

void _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::CopyFrom(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::RequestDesc>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::CopyFrom(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::RequestDesc>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::operator==(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::boxing::collective::cfg::RequestDesc>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::operator<(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
} // namespace boxing
} // namespace collective
