#ifndef CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H_
#define CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}
namespace oneflow {
namespace cfg {
enum DeviceType : unsigned int;
}
}
namespace oneflow {
namespace boxing {
namespace collective {
namespace cfg {
enum Backend : unsigned int;
}
}
}
}
namespace oneflow {
namespace boxing {
namespace collective {
namespace cfg {
enum OpType : unsigned int;
}
}
}
}
namespace oneflow {
namespace boxing {
namespace collective {
namespace cfg {
enum ReduceMethod : unsigned int;
}
}
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {
namespace boxing {
namespace collective {

// forward declare proto class;
class DeviceDesc;
class DeviceSet;
class OpDesc;
class RequestDesc;
class RequestSet;
class RankDesc;

namespace cfg {


class DeviceDesc;
class ConstDeviceDesc;

class DeviceSet;
class ConstDeviceSet;

class OpDesc;
class ConstOpDesc;

class RequestDesc;
class ConstRequestDesc;

class RequestSet;
class ConstRequestSet;

class RankDesc;
class ConstRankDesc;

enum OpType : unsigned int {
  kOpTypeInvalid = 0,
  kOpTypeAllReduce = 1,
  kOpTypeReduceScatter = 2,
  kOpTypeAllGather = 3,
  kOpTypeReduce = 4,
  kOpTypeBroadcast = 5,
  kOpTypeAll2All = 6,
};

inline ::std::string OpType_Name(OpType value) {
  switch (value) {
  case kOpTypeInvalid: { return "kOpTypeInvalid"; }
  case kOpTypeAllReduce: { return "kOpTypeAllReduce"; }
  case kOpTypeReduceScatter: { return "kOpTypeReduceScatter"; }
  case kOpTypeAllGather: { return "kOpTypeAllGather"; }
  case kOpTypeReduce: { return "kOpTypeReduce"; }
  case kOpTypeBroadcast: { return "kOpTypeBroadcast"; }
  case kOpTypeAll2All: { return "kOpTypeAll2All"; }
  default:
    return "";
  }
}

enum ReduceMethod : unsigned int {
  kReduceMethodInvalid = 0,
  kReduceMethodSum = 1,
};

inline ::std::string ReduceMethod_Name(ReduceMethod value) {
  switch (value) {
  case kReduceMethodInvalid: { return "kReduceMethodInvalid"; }
  case kReduceMethodSum: { return "kReduceMethodSum"; }
  default:
    return "";
  }
}

enum Backend : unsigned int {
  kBackendInvalid = 0,
  kBackendNCCL = 1,
};

inline ::std::string Backend_Name(Backend value) {
  switch (value) {
  case kBackendInvalid: { return "kBackendInvalid"; }
  case kBackendNCCL: { return "kBackendNCCL"; }
  default:
    return "";
  }
}



class ConstDeviceDesc : public ::oneflow::cfg::Message {
 public:

  class _DeviceDesc_ {
   public:
    _DeviceDesc_();
    explicit _DeviceDesc_(const _DeviceDesc_& other);
    explicit _DeviceDesc_(_DeviceDesc_&& other);
    _DeviceDesc_(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc);
    ~_DeviceDesc_();

    void InitFromProto(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc);

    void ToProto(::oneflow::boxing::collective::DeviceDesc* proto_devicedesc) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DeviceDesc_& other);
  
      // optional field machine_id
     public:
    bool has_machine_id() const;
    const int64_t& machine_id() const;
    void clear_machine_id();
    void set_machine_id(const int64_t& value);
    int64_t* mutable_machine_id();
   protected:
    bool has_machine_id_ = false;
    int64_t machine_id_;
      
      // optional field device_type
     public:
    bool has_device_type() const;
    const ::oneflow::cfg::DeviceType& device_type() const;
    void clear_device_type();
    void set_device_type(const ::oneflow::cfg::DeviceType& value);
    ::oneflow::cfg::DeviceType* mutable_device_type();
   protected:
    bool has_device_type_ = false;
    ::oneflow::cfg::DeviceType device_type_;
      
      // optional field device_id
     public:
    bool has_device_id() const;
    const int64_t& device_id() const;
    void clear_device_id();
    void set_device_id(const int64_t& value);
    int64_t* mutable_device_id();
   protected:
    bool has_device_id_ = false;
    int64_t device_id_;
           
   public:
    int compare(const _DeviceDesc_& other);

    bool operator==(const _DeviceDesc_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DeviceDesc_& other) const;
  };

  ConstDeviceDesc(const ::std::shared_ptr<_DeviceDesc_>& data);
  ConstDeviceDesc(const ConstDeviceDesc&);
  ConstDeviceDesc(ConstDeviceDesc&&) noexcept;
  ConstDeviceDesc();
  ConstDeviceDesc(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc);
  virtual ~ConstDeviceDesc() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_devicedesc) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field machine_id
 public:
  bool has_machine_id() const;
  const int64_t& machine_id() const;
  // used by pybind11 only
  // required or optional field device_type
 public:
  bool has_device_type() const;
  const ::oneflow::cfg::DeviceType& device_type() const;
  // used by pybind11 only
  // required or optional field device_id
 public:
  bool has_device_id() const;
  const int64_t& device_id() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDeviceDesc> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDeviceDesc& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDeviceDesc& other) const;
 protected:
  const ::std::shared_ptr<_DeviceDesc_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DeviceDesc_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDeviceDesc
  void BuildFromProto(const PbMessage& proto_devicedesc);
  
  ::std::shared_ptr<_DeviceDesc_> data_;
};

class DeviceDesc final : public ConstDeviceDesc {
 public:
  DeviceDesc(const ::std::shared_ptr<_DeviceDesc_>& data);
  DeviceDesc(const DeviceDesc& other);
  // enable nothrow for ::std::vector<DeviceDesc> resize 
  DeviceDesc(DeviceDesc&&) noexcept;
  DeviceDesc();
  explicit DeviceDesc(const ::oneflow::boxing::collective::DeviceDesc& proto_devicedesc);

  ~DeviceDesc() override;

  void InitFromProto(const PbMessage& proto_devicedesc) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DeviceDesc& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DeviceDesc& other) const;
  void Clear();
  void CopyFrom(const DeviceDesc& other);
  DeviceDesc& operator=(const DeviceDesc& other);

  // required or optional field machine_id
 public:
  void clear_machine_id();
  void set_machine_id(const int64_t& value);
  int64_t* mutable_machine_id();
  // required or optional field device_type
 public:
  void clear_device_type();
  void set_device_type(const ::oneflow::cfg::DeviceType& value);
  ::oneflow::cfg::DeviceType* mutable_device_type();
  // required or optional field device_id
 public:
  void clear_device_id();
  void set_device_id(const int64_t& value);
  int64_t* mutable_device_id();

  ::std::shared_ptr<DeviceDesc> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_;
class Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_;

class ConstDeviceSet : public ::oneflow::cfg::Message {
 public:

  class _DeviceSet_ {
   public:
    _DeviceSet_();
    explicit _DeviceSet_(const _DeviceSet_& other);
    explicit _DeviceSet_(_DeviceSet_&& other);
    _DeviceSet_(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset);
    ~_DeviceSet_();

    void InitFromProto(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset);

    void ToProto(::oneflow::boxing::collective::DeviceSet* proto_deviceset) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DeviceSet_& other);
  
      // repeated field device
   public:
    ::std::size_t device_size() const;
    const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& device() const;
    const ::oneflow::boxing::collective::cfg::DeviceDesc& device(::std::size_t index) const;
    void clear_device();
    _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_* mutable_device();
    ::oneflow::boxing::collective::cfg::DeviceDesc* mutable_device(::std::size_t index);
      ::oneflow::boxing::collective::cfg::DeviceDesc* add_device();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> device_;
         
   public:
    int compare(const _DeviceSet_& other);

    bool operator==(const _DeviceSet_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DeviceSet_& other) const;
  };

  ConstDeviceSet(const ::std::shared_ptr<_DeviceSet_>& data);
  ConstDeviceSet(const ConstDeviceSet&);
  ConstDeviceSet(ConstDeviceSet&&) noexcept;
  ConstDeviceSet();
  ConstDeviceSet(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset);
  virtual ~ConstDeviceSet() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_deviceset) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field device
 public:
  ::std::size_t device_size() const;
  const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& device() const;
  const ::oneflow::boxing::collective::cfg::DeviceDesc& device(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> shared_const_device() const;
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceDesc> shared_const_device(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstDeviceSet> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDeviceSet& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDeviceSet& other) const;
 protected:
  const ::std::shared_ptr<_DeviceSet_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DeviceSet_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDeviceSet
  void BuildFromProto(const PbMessage& proto_deviceset);
  
  ::std::shared_ptr<_DeviceSet_> data_;
};

class DeviceSet final : public ConstDeviceSet {
 public:
  DeviceSet(const ::std::shared_ptr<_DeviceSet_>& data);
  DeviceSet(const DeviceSet& other);
  // enable nothrow for ::std::vector<DeviceSet> resize 
  DeviceSet(DeviceSet&&) noexcept;
  DeviceSet();
  explicit DeviceSet(const ::oneflow::boxing::collective::DeviceSet& proto_deviceset);

  ~DeviceSet() override;

  void InitFromProto(const PbMessage& proto_deviceset) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DeviceSet& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DeviceSet& other) const;
  void Clear();
  void CopyFrom(const DeviceSet& other);
  DeviceSet& operator=(const DeviceSet& other);

  // repeated field device
 public:
  void clear_device();
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_* mutable_device();
  ::oneflow::boxing::collective::cfg::DeviceDesc* mutable_device(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> shared_mutable_device();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> shared_mutable_device(::std::size_t index);
  ::oneflow::boxing::collective::cfg::DeviceDesc* add_device();

  ::std::shared_ptr<DeviceSet> __SharedMutable__();
};


class ConstOpDesc : public ::oneflow::cfg::Message {
 public:

  class _OpDesc_ {
   public:
    _OpDesc_();
    explicit _OpDesc_(const _OpDesc_& other);
    explicit _OpDesc_(_OpDesc_&& other);
    _OpDesc_(const ::oneflow::boxing::collective::OpDesc& proto_opdesc);
    ~_OpDesc_();

    void InitFromProto(const ::oneflow::boxing::collective::OpDesc& proto_opdesc);

    void ToProto(::oneflow::boxing::collective::OpDesc* proto_opdesc) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpDesc_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
      // optional field op_type
     public:
    bool has_op_type() const;
    const ::oneflow::boxing::collective::cfg::OpType& op_type() const;
    void clear_op_type();
    void set_op_type(const ::oneflow::boxing::collective::cfg::OpType& value);
    ::oneflow::boxing::collective::cfg::OpType* mutable_op_type();
   protected:
    bool has_op_type_ = false;
    ::oneflow::boxing::collective::cfg::OpType op_type_;
      
      // optional field reduce_method
     public:
    bool has_reduce_method() const;
    const ::oneflow::boxing::collective::cfg::ReduceMethod& reduce_method() const;
    void clear_reduce_method();
    void set_reduce_method(const ::oneflow::boxing::collective::cfg::ReduceMethod& value);
    ::oneflow::boxing::collective::cfg::ReduceMethod* mutable_reduce_method();
   protected:
    bool has_reduce_method_ = false;
    ::oneflow::boxing::collective::cfg::ReduceMethod reduce_method_;
      
      // optional field root
     public:
    bool has_root() const;
    const int64_t& root() const;
    void clear_root();
    void set_root(const int64_t& value);
    int64_t* mutable_root();
   protected:
    bool has_root_ = false;
    int64_t root_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field num_ranks
     public:
    bool has_num_ranks() const;
    const int64_t& num_ranks() const;
    void clear_num_ranks();
    void set_num_ranks(const int64_t& value);
    int64_t* mutable_num_ranks();
   protected:
    bool has_num_ranks_ = false;
    int64_t num_ranks_;
      
      // optional field backend
     public:
    bool has_backend() const;
    const ::oneflow::boxing::collective::cfg::Backend& backend() const;
    void clear_backend();
    void set_backend(const ::oneflow::boxing::collective::cfg::Backend& value);
    ::oneflow::boxing::collective::cfg::Backend* mutable_backend();
   protected:
    bool has_backend_ = false;
    ::oneflow::boxing::collective::cfg::Backend backend_;
           
   public:
    int compare(const _OpDesc_& other);

    bool operator==(const _OpDesc_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpDesc_& other) const;
  };

  ConstOpDesc(const ::std::shared_ptr<_OpDesc_>& data);
  ConstOpDesc(const ConstOpDesc&);
  ConstOpDesc(ConstOpDesc&&) noexcept;
  ConstOpDesc();
  ConstOpDesc(const ::oneflow::boxing::collective::OpDesc& proto_opdesc);
  virtual ~ConstOpDesc() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opdesc) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // required or optional field op_type
 public:
  bool has_op_type() const;
  const ::oneflow::boxing::collective::cfg::OpType& op_type() const;
  // used by pybind11 only
  // required or optional field reduce_method
 public:
  bool has_reduce_method() const;
  const ::oneflow::boxing::collective::cfg::ReduceMethod& reduce_method() const;
  // used by pybind11 only
  // required or optional field root
 public:
  bool has_root() const;
  const int64_t& root() const;
  // used by pybind11 only
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field num_ranks
 public:
  bool has_num_ranks() const;
  const int64_t& num_ranks() const;
  // used by pybind11 only
  // required or optional field backend
 public:
  bool has_backend() const;
  const ::oneflow::boxing::collective::cfg::Backend& backend() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstOpDesc> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpDesc& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpDesc& other) const;
 protected:
  const ::std::shared_ptr<_OpDesc_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpDesc_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpDesc
  void BuildFromProto(const PbMessage& proto_opdesc);
  
  ::std::shared_ptr<_OpDesc_> data_;
};

class OpDesc final : public ConstOpDesc {
 public:
  OpDesc(const ::std::shared_ptr<_OpDesc_>& data);
  OpDesc(const OpDesc& other);
  // enable nothrow for ::std::vector<OpDesc> resize 
  OpDesc(OpDesc&&) noexcept;
  OpDesc();
  explicit OpDesc(const ::oneflow::boxing::collective::OpDesc& proto_opdesc);

  ~OpDesc() override;

  void InitFromProto(const PbMessage& proto_opdesc) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpDesc& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpDesc& other) const;
  void Clear();
  void CopyFrom(const OpDesc& other);
  OpDesc& operator=(const OpDesc& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // required or optional field op_type
 public:
  void clear_op_type();
  void set_op_type(const ::oneflow::boxing::collective::cfg::OpType& value);
  ::oneflow::boxing::collective::cfg::OpType* mutable_op_type();
  // required or optional field reduce_method
 public:
  void clear_reduce_method();
  void set_reduce_method(const ::oneflow::boxing::collective::cfg::ReduceMethod& value);
  ::oneflow::boxing::collective::cfg::ReduceMethod* mutable_reduce_method();
  // required or optional field root
 public:
  void clear_root();
  void set_root(const int64_t& value);
  int64_t* mutable_root();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field num_ranks
 public:
  void clear_num_ranks();
  void set_num_ranks(const int64_t& value);
  int64_t* mutable_num_ranks();
  // required or optional field backend
 public:
  void clear_backend();
  void set_backend(const ::oneflow::boxing::collective::cfg::Backend& value);
  ::oneflow::boxing::collective::cfg::Backend* mutable_backend();

  ::std::shared_ptr<OpDesc> __SharedMutable__();
};


class ConstRequestDesc : public ::oneflow::cfg::Message {
 public:

  class _RequestDesc_ {
   public:
    _RequestDesc_();
    explicit _RequestDesc_(const _RequestDesc_& other);
    explicit _RequestDesc_(_RequestDesc_&& other);
    _RequestDesc_(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc);
    ~_RequestDesc_();

    void InitFromProto(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc);

    void ToProto(::oneflow::boxing::collective::RequestDesc* proto_requestdesc) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RequestDesc_& other);
  
      // optional field op_desc
     public:
    bool has_op_desc() const;
    const ::oneflow::boxing::collective::cfg::OpDesc& op_desc() const;
    void clear_op_desc();
    ::oneflow::boxing::collective::cfg::OpDesc* mutable_op_desc();
   protected:
    bool has_op_desc_ = false;
    ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> op_desc_;
      
      // optional field device_set
     public:
    bool has_device_set() const;
    const ::oneflow::boxing::collective::cfg::DeviceSet& device_set() const;
    void clear_device_set();
    ::oneflow::boxing::collective::cfg::DeviceSet* mutable_device_set();
   protected:
    bool has_device_set_ = false;
    ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceSet> device_set_;
      
      // optional field order
     public:
    bool has_order() const;
    const int64_t& order() const;
    void clear_order();
    void set_order(const int64_t& value);
    int64_t* mutable_order();
   protected:
    bool has_order_ = false;
    int64_t order_;
      
      // optional field dependency_depth
     public:
    bool has_dependency_depth() const;
    const int64_t& dependency_depth() const;
    void clear_dependency_depth();
    void set_dependency_depth(const int64_t& value);
    int64_t* mutable_dependency_depth();
   protected:
    bool has_dependency_depth_ = false;
    int64_t dependency_depth_;
           
   public:
    int compare(const _RequestDesc_& other);

    bool operator==(const _RequestDesc_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RequestDesc_& other) const;
  };

  ConstRequestDesc(const ::std::shared_ptr<_RequestDesc_>& data);
  ConstRequestDesc(const ConstRequestDesc&);
  ConstRequestDesc(ConstRequestDesc&&) noexcept;
  ConstRequestDesc();
  ConstRequestDesc(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc);
  virtual ~ConstRequestDesc() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_requestdesc) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_desc
 public:
  bool has_op_desc() const;
  const ::oneflow::boxing::collective::cfg::OpDesc& op_desc() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstOpDesc> shared_const_op_desc() const;
  // required or optional field device_set
 public:
  bool has_device_set() const;
  const ::oneflow::boxing::collective::cfg::DeviceSet& device_set() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceSet> shared_const_device_set() const;
  // required or optional field order
 public:
  bool has_order() const;
  const int64_t& order() const;
  // used by pybind11 only
  // required or optional field dependency_depth
 public:
  bool has_dependency_depth() const;
  const int64_t& dependency_depth() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRequestDesc> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRequestDesc& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRequestDesc& other) const;
 protected:
  const ::std::shared_ptr<_RequestDesc_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RequestDesc_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRequestDesc
  void BuildFromProto(const PbMessage& proto_requestdesc);
  
  ::std::shared_ptr<_RequestDesc_> data_;
};

class RequestDesc final : public ConstRequestDesc {
 public:
  RequestDesc(const ::std::shared_ptr<_RequestDesc_>& data);
  RequestDesc(const RequestDesc& other);
  // enable nothrow for ::std::vector<RequestDesc> resize 
  RequestDesc(RequestDesc&&) noexcept;
  RequestDesc();
  explicit RequestDesc(const ::oneflow::boxing::collective::RequestDesc& proto_requestdesc);

  ~RequestDesc() override;

  void InitFromProto(const PbMessage& proto_requestdesc) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RequestDesc& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RequestDesc& other) const;
  void Clear();
  void CopyFrom(const RequestDesc& other);
  RequestDesc& operator=(const RequestDesc& other);

  // required or optional field op_desc
 public:
  void clear_op_desc();
  ::oneflow::boxing::collective::cfg::OpDesc* mutable_op_desc();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> shared_mutable_op_desc();
  // required or optional field device_set
 public:
  void clear_device_set();
  ::oneflow::boxing::collective::cfg::DeviceSet* mutable_device_set();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceSet> shared_mutable_device_set();
  // required or optional field order
 public:
  void clear_order();
  void set_order(const int64_t& value);
  int64_t* mutable_order();
  // required or optional field dependency_depth
 public:
  void clear_dependency_depth();
  void set_dependency_depth(const int64_t& value);
  int64_t* mutable_dependency_depth();

  ::std::shared_ptr<RequestDesc> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_;
class Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_;

class ConstRequestSet : public ::oneflow::cfg::Message {
 public:

  class _RequestSet_ {
   public:
    _RequestSet_();
    explicit _RequestSet_(const _RequestSet_& other);
    explicit _RequestSet_(_RequestSet_&& other);
    _RequestSet_(const ::oneflow::boxing::collective::RequestSet& proto_requestset);
    ~_RequestSet_();

    void InitFromProto(const ::oneflow::boxing::collective::RequestSet& proto_requestset);

    void ToProto(::oneflow::boxing::collective::RequestSet* proto_requestset) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RequestSet_& other);
  
      // repeated field request
   public:
    ::std::size_t request_size() const;
    const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& request() const;
    const ::oneflow::boxing::collective::cfg::RequestDesc& request(::std::size_t index) const;
    void clear_request();
    _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_* mutable_request();
    ::oneflow::boxing::collective::cfg::RequestDesc* mutable_request(::std::size_t index);
      ::oneflow::boxing::collective::cfg::RequestDesc* add_request();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> request_;
         
   public:
    int compare(const _RequestSet_& other);

    bool operator==(const _RequestSet_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RequestSet_& other) const;
  };

  ConstRequestSet(const ::std::shared_ptr<_RequestSet_>& data);
  ConstRequestSet(const ConstRequestSet&);
  ConstRequestSet(ConstRequestSet&&) noexcept;
  ConstRequestSet();
  ConstRequestSet(const ::oneflow::boxing::collective::RequestSet& proto_requestset);
  virtual ~ConstRequestSet() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_requestset) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field request
 public:
  ::std::size_t request_size() const;
  const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& request() const;
  const ::oneflow::boxing::collective::cfg::RequestDesc& request(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> shared_const_request() const;
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstRequestDesc> shared_const_request(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstRequestSet> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRequestSet& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRequestSet& other) const;
 protected:
  const ::std::shared_ptr<_RequestSet_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RequestSet_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRequestSet
  void BuildFromProto(const PbMessage& proto_requestset);
  
  ::std::shared_ptr<_RequestSet_> data_;
};

class RequestSet final : public ConstRequestSet {
 public:
  RequestSet(const ::std::shared_ptr<_RequestSet_>& data);
  RequestSet(const RequestSet& other);
  // enable nothrow for ::std::vector<RequestSet> resize 
  RequestSet(RequestSet&&) noexcept;
  RequestSet();
  explicit RequestSet(const ::oneflow::boxing::collective::RequestSet& proto_requestset);

  ~RequestSet() override;

  void InitFromProto(const PbMessage& proto_requestset) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RequestSet& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RequestSet& other) const;
  void Clear();
  void CopyFrom(const RequestSet& other);
  RequestSet& operator=(const RequestSet& other);

  // repeated field request
 public:
  void clear_request();
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_* mutable_request();
  ::oneflow::boxing::collective::cfg::RequestDesc* mutable_request(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> shared_mutable_request();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> shared_mutable_request(::std::size_t index);
  ::oneflow::boxing::collective::cfg::RequestDesc* add_request();

  ::std::shared_ptr<RequestSet> __SharedMutable__();
};


class ConstRankDesc : public ::oneflow::cfg::Message {
 public:

  class _RankDesc_ {
   public:
    _RankDesc_();
    explicit _RankDesc_(const _RankDesc_& other);
    explicit _RankDesc_(_RankDesc_&& other);
    _RankDesc_(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc);
    ~_RankDesc_();

    void InitFromProto(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc);

    void ToProto(::oneflow::boxing::collective::RankDesc* proto_rankdesc) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _RankDesc_& other);
  
      // optional field op_desc
     public:
    bool has_op_desc() const;
    const ::oneflow::boxing::collective::cfg::OpDesc& op_desc() const;
    void clear_op_desc();
    ::oneflow::boxing::collective::cfg::OpDesc* mutable_op_desc();
   protected:
    bool has_op_desc_ = false;
    ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> op_desc_;
      
      // optional field rank
     public:
    bool has_rank() const;
    const int64_t& rank() const;
    void clear_rank();
    void set_rank(const int64_t& value);
    int64_t* mutable_rank();
   protected:
    bool has_rank_ = false;
    int64_t rank_;
           
   public:
    int compare(const _RankDesc_& other);

    bool operator==(const _RankDesc_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _RankDesc_& other) const;
  };

  ConstRankDesc(const ::std::shared_ptr<_RankDesc_>& data);
  ConstRankDesc(const ConstRankDesc&);
  ConstRankDesc(ConstRankDesc&&) noexcept;
  ConstRankDesc();
  ConstRankDesc(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc);
  virtual ~ConstRankDesc() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_rankdesc) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field op_desc
 public:
  bool has_op_desc() const;
  const ::oneflow::boxing::collective::cfg::OpDesc& op_desc() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstOpDesc> shared_const_op_desc() const;
  // required or optional field rank
 public:
  bool has_rank() const;
  const int64_t& rank() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstRankDesc> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstRankDesc& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstRankDesc& other) const;
 protected:
  const ::std::shared_ptr<_RankDesc_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_RankDesc_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstRankDesc
  void BuildFromProto(const PbMessage& proto_rankdesc);
  
  ::std::shared_ptr<_RankDesc_> data_;
};

class RankDesc final : public ConstRankDesc {
 public:
  RankDesc(const ::std::shared_ptr<_RankDesc_>& data);
  RankDesc(const RankDesc& other);
  // enable nothrow for ::std::vector<RankDesc> resize 
  RankDesc(RankDesc&&) noexcept;
  RankDesc();
  explicit RankDesc(const ::oneflow::boxing::collective::RankDesc& proto_rankdesc);

  ~RankDesc() override;

  void InitFromProto(const PbMessage& proto_rankdesc) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const RankDesc& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const RankDesc& other) const;
  void Clear();
  void CopyFrom(const RankDesc& other);
  RankDesc& operator=(const RankDesc& other);

  // required or optional field op_desc
 public:
  void clear_op_desc();
  ::oneflow::boxing::collective::cfg::OpDesc* mutable_op_desc();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::OpDesc> shared_mutable_op_desc();
  // required or optional field rank
 public:
  void clear_rank();
  void set_rank(const int64_t& value);
  int64_t* mutable_rank();

  ::std::shared_ptr<RankDesc> __SharedMutable__();
};







// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::DeviceDesc> {
 public:
  Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::DeviceDesc>>& data);
  Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_();
  ~Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstDeviceDesc> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_ final : public Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_ {
 public:
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::DeviceDesc>>& data);
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_();
  ~_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_DeviceDesc_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> __SharedAdd__();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::DeviceDesc> __SharedMutable__(::std::size_t index);
};










// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::boxing::collective::cfg::RequestDesc> {
 public:
  Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::RequestDesc>>& data);
  Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_();
  ~Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstRequestDesc> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_ final : public Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_ {
 public:
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_(const ::std::shared_ptr<::std::vector<::oneflow::boxing::collective::cfg::RequestDesc>>& data);
  _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_();
  ~_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H__RepeatedField_RequestDesc_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> __SharedAdd__();
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::RequestDesc> __SharedMutable__(::std::size_t index);
};






} //namespace cfg

} // namespace oneflow
} // namespace boxing
} // namespace collective

namespace std {

template<>
struct hash<::oneflow::boxing::collective::cfg::OpType> {
  std::size_t operator()(::oneflow::boxing::collective::cfg::OpType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};
template<>
struct hash<::oneflow::boxing::collective::cfg::ReduceMethod> {
  std::size_t operator()(::oneflow::boxing::collective::cfg::ReduceMethod enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};
template<>
struct hash<::oneflow::boxing::collective::cfg::Backend> {
  std::size_t operator()(::oneflow::boxing::collective::cfg::Backend enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::boxing::collective::cfg::ConstDeviceDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstDeviceDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::DeviceDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::DeviceDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::ConstDeviceSet> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstDeviceSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::DeviceSet> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::DeviceSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::ConstOpDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstOpDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::OpDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::OpDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::ConstRequestDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstRequestDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::RequestDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::RequestDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::ConstRequestSet> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstRequestSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::RequestSet> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::RequestSet& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::ConstRankDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::ConstRankDesc& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::boxing::collective::cfg::RankDesc> {
  std::size_t operator()(const ::oneflow::boxing::collective::cfg::RankDesc& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_GRAPH_BOXING_COLLECTIVE_BOXING_CFG_H_