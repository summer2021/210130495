#ifndef CFG_ONEFLOW_CORE_EAGER_EAGER_INSTRUCTION_CFG_H_
#define CFG_ONEFLOW_CORE_EAGER_EAGER_INSTRUCTION_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace vm {
namespace cfg {
class ConstEagerSymbolList;
class EagerSymbolList;
}
}
}
namespace oneflow {
namespace vm {
namespace cfg {
class ConstInstructionListProto;
class InstructionListProto;
}
}
}

namespace oneflow {
namespace vm {

// forward declare proto class;
class EagerInstruction;

namespace cfg {


class EagerInstruction;
class ConstEagerInstruction;



class ConstEagerInstruction : public ::oneflow::cfg::Message {
 public:

  class _EagerInstruction_ {
   public:
    _EagerInstruction_();
    explicit _EagerInstruction_(const _EagerInstruction_& other);
    explicit _EagerInstruction_(_EagerInstruction_&& other);
    _EagerInstruction_(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction);
    ~_EagerInstruction_();

    void InitFromProto(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction);

    void ToProto(::oneflow::vm::EagerInstruction* proto_eagerinstruction) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _EagerInstruction_& other);
  
      // optional field instruction_list
     public:
    bool has_instruction_list() const;
    const ::oneflow::vm::cfg::InstructionListProto& instruction_list() const;
    void clear_instruction_list();
    ::oneflow::vm::cfg::InstructionListProto* mutable_instruction_list();
   protected:
    bool has_instruction_list_ = false;
    ::std::shared_ptr<::oneflow::vm::cfg::InstructionListProto> instruction_list_;
      
      // optional field eager_symbol_list
     public:
    bool has_eager_symbol_list() const;
    const ::oneflow::vm::cfg::EagerSymbolList& eager_symbol_list() const;
    void clear_eager_symbol_list();
    ::oneflow::vm::cfg::EagerSymbolList* mutable_eager_symbol_list();
   protected:
    bool has_eager_symbol_list_ = false;
    ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbolList> eager_symbol_list_;
           
   public:
    int compare(const _EagerInstruction_& other);

    bool operator==(const _EagerInstruction_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _EagerInstruction_& other) const;
  };

  ConstEagerInstruction(const ::std::shared_ptr<_EagerInstruction_>& data);
  ConstEagerInstruction(const ConstEagerInstruction&);
  ConstEagerInstruction(ConstEagerInstruction&&) noexcept;
  ConstEagerInstruction();
  ConstEagerInstruction(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction);
  virtual ~ConstEagerInstruction() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_eagerinstruction) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field instruction_list
 public:
  bool has_instruction_list() const;
  const ::oneflow::vm::cfg::InstructionListProto& instruction_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionListProto> shared_const_instruction_list() const;
  // required or optional field eager_symbol_list
 public:
  bool has_eager_symbol_list() const;
  const ::oneflow::vm::cfg::EagerSymbolList& eager_symbol_list() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbolList> shared_const_eager_symbol_list() const;

 public:
  ::std::shared_ptr<ConstEagerInstruction> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstEagerInstruction& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstEagerInstruction& other) const;
 protected:
  const ::std::shared_ptr<_EagerInstruction_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_EagerInstruction_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstEagerInstruction
  void BuildFromProto(const PbMessage& proto_eagerinstruction);
  
  ::std::shared_ptr<_EagerInstruction_> data_;
};

class EagerInstruction final : public ConstEagerInstruction {
 public:
  EagerInstruction(const ::std::shared_ptr<_EagerInstruction_>& data);
  EagerInstruction(const EagerInstruction& other);
  // enable nothrow for ::std::vector<EagerInstruction> resize 
  EagerInstruction(EagerInstruction&&) noexcept;
  EagerInstruction();
  explicit EagerInstruction(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction);

  ~EagerInstruction() override;

  void InitFromProto(const PbMessage& proto_eagerinstruction) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const EagerInstruction& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const EagerInstruction& other) const;
  void Clear();
  void CopyFrom(const EagerInstruction& other);
  EagerInstruction& operator=(const EagerInstruction& other);

  // required or optional field instruction_list
 public:
  void clear_instruction_list();
  ::oneflow::vm::cfg::InstructionListProto* mutable_instruction_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::InstructionListProto> shared_mutable_instruction_list();
  // required or optional field eager_symbol_list
 public:
  void clear_eager_symbol_list();
  ::oneflow::vm::cfg::EagerSymbolList* mutable_eager_symbol_list();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbolList> shared_mutable_eager_symbol_list();

  ::std::shared_ptr<EagerInstruction> __SharedMutable__();
};






} //namespace cfg

} // namespace oneflow
} // namespace vm

namespace std {



template<>
struct hash<::oneflow::vm::cfg::ConstEagerInstruction> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstEagerInstruction& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::EagerInstruction> {
  std::size_t operator()(const ::oneflow::vm::cfg::EagerInstruction& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_EAGER_EAGER_INSTRUCTION_CFG_H_