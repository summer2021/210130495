// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/eager/eager_instruction.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2feager_2feager_5finstruction_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2feager_2feager_5finstruction_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include "oneflow/core/vm/instruction.pb.h"
#include "oneflow/core/eager/eager_symbol.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2feager_2feager_5finstruction_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_oneflow_2fcore_2feager_2feager_5finstruction_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2feager_2feager_5finstruction_2eproto;
namespace oneflow {
namespace vm {
class EagerInstruction;
class EagerInstructionDefaultTypeInternal;
extern EagerInstructionDefaultTypeInternal _EagerInstruction_default_instance_;
}  // namespace vm
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> ::oneflow::vm::EagerInstruction* Arena::CreateMaybeMessage<::oneflow::vm::EagerInstruction>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace oneflow {
namespace vm {

// ===================================================================

class EagerInstruction :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:oneflow.vm.EagerInstruction) */ {
 public:
  EagerInstruction();
  virtual ~EagerInstruction();

  EagerInstruction(const EagerInstruction& from);
  EagerInstruction(EagerInstruction&& from) noexcept
    : EagerInstruction() {
    *this = ::std::move(from);
  }

  inline EagerInstruction& operator=(const EagerInstruction& from) {
    CopyFrom(from);
    return *this;
  }
  inline EagerInstruction& operator=(EagerInstruction&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const EagerInstruction& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const EagerInstruction* internal_default_instance() {
    return reinterpret_cast<const EagerInstruction*>(
               &_EagerInstruction_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(EagerInstruction& a, EagerInstruction& b) {
    a.Swap(&b);
  }
  inline void Swap(EagerInstruction* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline EagerInstruction* New() const final {
    return CreateMaybeMessage<EagerInstruction>(nullptr);
  }

  EagerInstruction* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<EagerInstruction>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const EagerInstruction& from);
  void MergeFrom(const EagerInstruction& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const final;
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(EagerInstruction* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "oneflow.vm.EagerInstruction";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_oneflow_2fcore_2feager_2feager_5finstruction_2eproto);
    return ::descriptor_table_oneflow_2fcore_2feager_2feager_5finstruction_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kInstructionListFieldNumber = 1,
    kEagerSymbolListFieldNumber = 2,
  };
  // optional .oneflow.vm.InstructionListProto instruction_list = 1;
  bool has_instruction_list() const;
  void clear_instruction_list();
  const ::oneflow::vm::InstructionListProto& instruction_list() const;
  ::oneflow::vm::InstructionListProto* release_instruction_list();
  ::oneflow::vm::InstructionListProto* mutable_instruction_list();
  void set_allocated_instruction_list(::oneflow::vm::InstructionListProto* instruction_list);

  // optional .oneflow.vm.EagerSymbolList eager_symbol_list = 2;
  bool has_eager_symbol_list() const;
  void clear_eager_symbol_list();
  const ::oneflow::vm::EagerSymbolList& eager_symbol_list() const;
  ::oneflow::vm::EagerSymbolList* release_eager_symbol_list();
  ::oneflow::vm::EagerSymbolList* mutable_eager_symbol_list();
  void set_allocated_eager_symbol_list(::oneflow::vm::EagerSymbolList* eager_symbol_list);

  // @@protoc_insertion_point(class_scope:oneflow.vm.EagerInstruction)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::oneflow::vm::InstructionListProto* instruction_list_;
  ::oneflow::vm::EagerSymbolList* eager_symbol_list_;
  friend struct ::TableStruct_oneflow_2fcore_2feager_2feager_5finstruction_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// EagerInstruction

// optional .oneflow.vm.InstructionListProto instruction_list = 1;
inline bool EagerInstruction::has_instruction_list() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline const ::oneflow::vm::InstructionListProto& EagerInstruction::instruction_list() const {
  const ::oneflow::vm::InstructionListProto* p = instruction_list_;
  // @@protoc_insertion_point(field_get:oneflow.vm.EagerInstruction.instruction_list)
  return p != nullptr ? *p : *reinterpret_cast<const ::oneflow::vm::InstructionListProto*>(
      &::oneflow::vm::_InstructionListProto_default_instance_);
}
inline ::oneflow::vm::InstructionListProto* EagerInstruction::release_instruction_list() {
  // @@protoc_insertion_point(field_release:oneflow.vm.EagerInstruction.instruction_list)
  _has_bits_[0] &= ~0x00000001u;
  ::oneflow::vm::InstructionListProto* temp = instruction_list_;
  instruction_list_ = nullptr;
  return temp;
}
inline ::oneflow::vm::InstructionListProto* EagerInstruction::mutable_instruction_list() {
  _has_bits_[0] |= 0x00000001u;
  if (instruction_list_ == nullptr) {
    auto* p = CreateMaybeMessage<::oneflow::vm::InstructionListProto>(GetArenaNoVirtual());
    instruction_list_ = p;
  }
  // @@protoc_insertion_point(field_mutable:oneflow.vm.EagerInstruction.instruction_list)
  return instruction_list_;
}
inline void EagerInstruction::set_allocated_instruction_list(::oneflow::vm::InstructionListProto* instruction_list) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(instruction_list_);
  }
  if (instruction_list) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      instruction_list = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, instruction_list, submessage_arena);
    }
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  instruction_list_ = instruction_list;
  // @@protoc_insertion_point(field_set_allocated:oneflow.vm.EagerInstruction.instruction_list)
}

// optional .oneflow.vm.EagerSymbolList eager_symbol_list = 2;
inline bool EagerInstruction::has_eager_symbol_list() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline const ::oneflow::vm::EagerSymbolList& EagerInstruction::eager_symbol_list() const {
  const ::oneflow::vm::EagerSymbolList* p = eager_symbol_list_;
  // @@protoc_insertion_point(field_get:oneflow.vm.EagerInstruction.eager_symbol_list)
  return p != nullptr ? *p : *reinterpret_cast<const ::oneflow::vm::EagerSymbolList*>(
      &::oneflow::vm::_EagerSymbolList_default_instance_);
}
inline ::oneflow::vm::EagerSymbolList* EagerInstruction::release_eager_symbol_list() {
  // @@protoc_insertion_point(field_release:oneflow.vm.EagerInstruction.eager_symbol_list)
  _has_bits_[0] &= ~0x00000002u;
  ::oneflow::vm::EagerSymbolList* temp = eager_symbol_list_;
  eager_symbol_list_ = nullptr;
  return temp;
}
inline ::oneflow::vm::EagerSymbolList* EagerInstruction::mutable_eager_symbol_list() {
  _has_bits_[0] |= 0x00000002u;
  if (eager_symbol_list_ == nullptr) {
    auto* p = CreateMaybeMessage<::oneflow::vm::EagerSymbolList>(GetArenaNoVirtual());
    eager_symbol_list_ = p;
  }
  // @@protoc_insertion_point(field_mutable:oneflow.vm.EagerInstruction.eager_symbol_list)
  return eager_symbol_list_;
}
inline void EagerInstruction::set_allocated_eager_symbol_list(::oneflow::vm::EagerSymbolList* eager_symbol_list) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(eager_symbol_list_);
  }
  if (eager_symbol_list) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena = nullptr;
    if (message_arena != submessage_arena) {
      eager_symbol_list = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, eager_symbol_list, submessage_arena);
    }
    _has_bits_[0] |= 0x00000002u;
  } else {
    _has_bits_[0] &= ~0x00000002u;
  }
  eager_symbol_list_ = eager_symbol_list;
  // @@protoc_insertion_point(field_set_allocated:oneflow.vm.EagerInstruction.eager_symbol_list)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace vm
}  // namespace oneflow

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_oneflow_2fcore_2feager_2feager_5finstruction_2eproto
