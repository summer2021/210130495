#include "oneflow/core/eager/eager_symbol.cfg.h"
#include "oneflow/core/job/job_conf.cfg.h"
#include "oneflow/core/job/placement.cfg.h"
#include "oneflow/core/job/scope.cfg.h"
#include "oneflow/core/operator/op_conf.cfg.h"
#include "oneflow/core/operator/op_node_signature.cfg.h"
#include "oneflow/core/eager/eager_symbol.pb.h"

namespace oneflow {
namespace vm {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstEagerSymbol::_EagerSymbol_::_EagerSymbol_() { Clear(); }
ConstEagerSymbol::_EagerSymbol_::_EagerSymbol_(const _EagerSymbol_& other) { CopyFrom(other); }
ConstEagerSymbol::_EagerSymbol_::_EagerSymbol_(const ::oneflow::vm::EagerSymbol& proto_eagersymbol) {
  InitFromProto(proto_eagersymbol);
}
ConstEagerSymbol::_EagerSymbol_::_EagerSymbol_(_EagerSymbol_&& other) = default;
ConstEagerSymbol::_EagerSymbol_::~_EagerSymbol_() = default;

void ConstEagerSymbol::_EagerSymbol_::InitFromProto(const ::oneflow::vm::EagerSymbol& proto_eagersymbol) {
  Clear();
  // required_or_optional field: symbol_id
  if (proto_eagersymbol.has_symbol_id()) {
    set_symbol_id(proto_eagersymbol.symbol_id());
  }
  // oneof field: eager_symbol_type
  EagerSymbolTypeCase eager_symbol_type_case = EagerSymbolTypeCase(int(proto_eagersymbol.eager_symbol_type_case()));
  switch (eager_symbol_type_case) {
    case kStringSymbol: {
      set_string_symbol(proto_eagersymbol.string_symbol());
      break;
  }
    case kScopeSymbol: {
      *mutable_scope_symbol() = ::oneflow::cfg::ScopeProto(proto_eagersymbol.scope_symbol());
      break;
  }
    case kJobConfSymbol: {
      *mutable_job_conf_symbol() = ::oneflow::cfg::JobConfigProto(proto_eagersymbol.job_conf_symbol());
      break;
  }
    case kParallelConfSymbol: {
      *mutable_parallel_conf_symbol() = ::oneflow::cfg::ParallelConf(proto_eagersymbol.parallel_conf_symbol());
      break;
  }
    case kOpConfSymbol: {
      *mutable_op_conf_symbol() = ::oneflow::cfg::OperatorConf(proto_eagersymbol.op_conf_symbol());
      break;
  }
    case kOpNodeSignatureSymbol: {
      *mutable_op_node_signature_symbol() = ::oneflow::cfg::OpNodeSignature(proto_eagersymbol.op_node_signature_symbol());
      break;
  }
    case EAGER_SYMBOL_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstEagerSymbol::_EagerSymbol_::ToProto(::oneflow::vm::EagerSymbol* proto_eagersymbol) const {
  proto_eagersymbol->Clear();
  // required_or_optional field: symbol_id
  if (this->has_symbol_id()) {
    proto_eagersymbol->set_symbol_id(symbol_id());
    }

  // oneof field: eager_symbol_type
  ::oneflow::vm::EagerSymbol::EagerSymbolTypeCase eager_symbol_type_case = ::oneflow::vm::EagerSymbol::EagerSymbolTypeCase(int(this->eager_symbol_type_case()));
  switch (eager_symbol_type_case) {
    case ::oneflow::vm::EagerSymbol::kStringSymbol: {
      proto_eagersymbol->set_string_symbol(string_symbol());
      break;
    }
    case ::oneflow::vm::EagerSymbol::kScopeSymbol: {
      ::oneflow::ScopeProto of_proto_scope_symbol;
      scope_symbol().ToProto(&of_proto_scope_symbol);
      proto_eagersymbol->mutable_scope_symbol()->CopyFrom(of_proto_scope_symbol);
      break;
    }
    case ::oneflow::vm::EagerSymbol::kJobConfSymbol: {
      ::oneflow::JobConfigProto of_proto_job_conf_symbol;
      job_conf_symbol().ToProto(&of_proto_job_conf_symbol);
      proto_eagersymbol->mutable_job_conf_symbol()->CopyFrom(of_proto_job_conf_symbol);
      break;
    }
    case ::oneflow::vm::EagerSymbol::kParallelConfSymbol: {
      ::oneflow::ParallelConf of_proto_parallel_conf_symbol;
      parallel_conf_symbol().ToProto(&of_proto_parallel_conf_symbol);
      proto_eagersymbol->mutable_parallel_conf_symbol()->CopyFrom(of_proto_parallel_conf_symbol);
      break;
    }
    case ::oneflow::vm::EagerSymbol::kOpConfSymbol: {
      ::oneflow::OperatorConf of_proto_op_conf_symbol;
      op_conf_symbol().ToProto(&of_proto_op_conf_symbol);
      proto_eagersymbol->mutable_op_conf_symbol()->CopyFrom(of_proto_op_conf_symbol);
      break;
    }
    case ::oneflow::vm::EagerSymbol::kOpNodeSignatureSymbol: {
      ::oneflow::OpNodeSignature of_proto_op_node_signature_symbol;
      op_node_signature_symbol().ToProto(&of_proto_op_node_signature_symbol);
      proto_eagersymbol->mutable_op_node_signature_symbol()->CopyFrom(of_proto_op_node_signature_symbol);
      break;
    }
    case ::oneflow::vm::EagerSymbol::EAGER_SYMBOL_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstEagerSymbol::_EagerSymbol_::DebugString() const {
  ::oneflow::vm::EagerSymbol proto_eagersymbol;
  this->ToProto(&proto_eagersymbol);
  return proto_eagersymbol.DebugString();
}

void ConstEagerSymbol::_EagerSymbol_::Clear() {
  clear_symbol_id();
  clear_eager_symbol_type();
}

void ConstEagerSymbol::_EagerSymbol_::CopyFrom(const _EagerSymbol_& other) {
  if (other.has_symbol_id()) {
    set_symbol_id(other.symbol_id());
  } else {
    clear_symbol_id();
  }
  eager_symbol_type_copy_from(other);
}


// optional field symbol_id
bool ConstEagerSymbol::_EagerSymbol_::has_symbol_id() const {
  return has_symbol_id_;
}
const int64_t& ConstEagerSymbol::_EagerSymbol_::symbol_id() const {
  if (has_symbol_id_) { return symbol_id_; }
  static const int64_t default_static_value = int64_t();
  return default_static_value;
}
void ConstEagerSymbol::_EagerSymbol_::clear_symbol_id() {
  has_symbol_id_ = false;
}
void ConstEagerSymbol::_EagerSymbol_::set_symbol_id(const int64_t& value) {
  symbol_id_ = value;
  has_symbol_id_ = true;
}
int64_t* ConstEagerSymbol::_EagerSymbol_::mutable_symbol_id() {
  has_symbol_id_ = true;
  return &symbol_id_;
}

// oneof field eager_symbol_type: string_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_string_symbol() const {
  return eager_symbol_type_case() == kStringSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_string_symbol() {
  if (has_string_symbol()) {
    {
      using String = ::std::string;
      String* ptr = reinterpret_cast<String*>(&(eager_symbol_type_.string_symbol_)[0]);
      ptr->~String();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::std::string& ConstEagerSymbol::_EagerSymbol_::string_symbol() const {
  if (has_string_symbol()) {
      const ::std::string* ptr = reinterpret_cast<const ::std::string*>(&(eager_symbol_type_.string_symbol_)[0]);
    return *ptr;
    } else {
      static const ::std::string default_static_value = ::std::string();
    return default_static_value;
    }
}
void ConstEagerSymbol::_EagerSymbol_::set_string_symbol(const ::std::string& value) {
  if(!has_string_symbol()) {
    clear_eager_symbol_type();
      new (&(eager_symbol_type_.string_symbol_)) std::string();
    }
  eager_symbol_type_case_ = kStringSymbol;
    std::string* ptr = reinterpret_cast<std::string*>(&(eager_symbol_type_.string_symbol_)[0]);
  *ptr = value;
  }
::std::string* ConstEagerSymbol::_EagerSymbol_::mutable_string_symbol() {
  if(!has_string_symbol()) {
    clear_eager_symbol_type();
      new (&(eager_symbol_type_.string_symbol_)) std::string();
    }
    ::std::string* ptr = reinterpret_cast<::std::string*>(&(eager_symbol_type_.string_symbol_)[0]);
  return ptr;
  }

// oneof field eager_symbol_type: scope_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_scope_symbol() const {
  return eager_symbol_type_case() == kScopeSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_scope_symbol() {
  if (has_scope_symbol()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ScopeProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.scope_symbol_));
      ptr->~Shared_ptr();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ScopeProto& ConstEagerSymbol::_EagerSymbol_::scope_symbol() const {
  if (has_scope_symbol()) {
      const ::std::shared_ptr<::oneflow::cfg::ScopeProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ScopeProto>*>(&(eager_symbol_type_.scope_symbol_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ScopeProto> default_static_value = ::std::make_shared<::oneflow::cfg::ScopeProto>();
    return *default_static_value;
    }
}
::oneflow::cfg::ScopeProto* ConstEagerSymbol::_EagerSymbol_::mutable_scope_symbol() {
  if(!has_scope_symbol()) {
    clear_eager_symbol_type();
    new (&(eager_symbol_type_.scope_symbol_)) ::std::shared_ptr<::oneflow::cfg::ScopeProto>(new ::oneflow::cfg::ScopeProto());
  }
  eager_symbol_type_case_ = kScopeSymbol;
  ::std::shared_ptr<::oneflow::cfg::ScopeProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ScopeProto>*>(&(eager_symbol_type_.scope_symbol_));
  return  (*ptr).get();
}

// oneof field eager_symbol_type: job_conf_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_job_conf_symbol() const {
  return eager_symbol_type_case() == kJobConfSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_job_conf_symbol() {
  if (has_job_conf_symbol()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfigProto>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.job_conf_symbol_));
      ptr->~Shared_ptr();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::JobConfigProto& ConstEagerSymbol::_EagerSymbol_::job_conf_symbol() const {
  if (has_job_conf_symbol()) {
      const ::std::shared_ptr<::oneflow::cfg::JobConfigProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::JobConfigProto>*>(&(eager_symbol_type_.job_conf_symbol_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::JobConfigProto> default_static_value = ::std::make_shared<::oneflow::cfg::JobConfigProto>();
    return *default_static_value;
    }
}
::oneflow::cfg::JobConfigProto* ConstEagerSymbol::_EagerSymbol_::mutable_job_conf_symbol() {
  if(!has_job_conf_symbol()) {
    clear_eager_symbol_type();
    new (&(eager_symbol_type_.job_conf_symbol_)) ::std::shared_ptr<::oneflow::cfg::JobConfigProto>(new ::oneflow::cfg::JobConfigProto());
  }
  eager_symbol_type_case_ = kJobConfSymbol;
  ::std::shared_ptr<::oneflow::cfg::JobConfigProto>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::JobConfigProto>*>(&(eager_symbol_type_.job_conf_symbol_));
  return  (*ptr).get();
}

// oneof field eager_symbol_type: parallel_conf_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_parallel_conf_symbol() const {
  return eager_symbol_type_case() == kParallelConfSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_parallel_conf_symbol() {
  if (has_parallel_conf_symbol()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ParallelConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.parallel_conf_symbol_));
      ptr->~Shared_ptr();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::ParallelConf& ConstEagerSymbol::_EagerSymbol_::parallel_conf_symbol() const {
  if (has_parallel_conf_symbol()) {
      const ::std::shared_ptr<::oneflow::cfg::ParallelConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::ParallelConf>*>(&(eager_symbol_type_.parallel_conf_symbol_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::ParallelConf> default_static_value = ::std::make_shared<::oneflow::cfg::ParallelConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::ParallelConf* ConstEagerSymbol::_EagerSymbol_::mutable_parallel_conf_symbol() {
  if(!has_parallel_conf_symbol()) {
    clear_eager_symbol_type();
    new (&(eager_symbol_type_.parallel_conf_symbol_)) ::std::shared_ptr<::oneflow::cfg::ParallelConf>(new ::oneflow::cfg::ParallelConf());
  }
  eager_symbol_type_case_ = kParallelConfSymbol;
  ::std::shared_ptr<::oneflow::cfg::ParallelConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::ParallelConf>*>(&(eager_symbol_type_.parallel_conf_symbol_));
  return  (*ptr).get();
}

// oneof field eager_symbol_type: op_conf_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_op_conf_symbol() const {
  return eager_symbol_type_case() == kOpConfSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_op_conf_symbol() {
  if (has_op_conf_symbol()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OperatorConf>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.op_conf_symbol_));
      ptr->~Shared_ptr();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OperatorConf& ConstEagerSymbol::_EagerSymbol_::op_conf_symbol() const {
  if (has_op_conf_symbol()) {
      const ::std::shared_ptr<::oneflow::cfg::OperatorConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OperatorConf>*>(&(eager_symbol_type_.op_conf_symbol_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OperatorConf> default_static_value = ::std::make_shared<::oneflow::cfg::OperatorConf>();
    return *default_static_value;
    }
}
::oneflow::cfg::OperatorConf* ConstEagerSymbol::_EagerSymbol_::mutable_op_conf_symbol() {
  if(!has_op_conf_symbol()) {
    clear_eager_symbol_type();
    new (&(eager_symbol_type_.op_conf_symbol_)) ::std::shared_ptr<::oneflow::cfg::OperatorConf>(new ::oneflow::cfg::OperatorConf());
  }
  eager_symbol_type_case_ = kOpConfSymbol;
  ::std::shared_ptr<::oneflow::cfg::OperatorConf>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OperatorConf>*>(&(eager_symbol_type_.op_conf_symbol_));
  return  (*ptr).get();
}

// oneof field eager_symbol_type: op_node_signature_symbol
bool ConstEagerSymbol::_EagerSymbol_::has_op_node_signature_symbol() const {
  return eager_symbol_type_case() == kOpNodeSignatureSymbol;
}
void ConstEagerSymbol::_EagerSymbol_::clear_op_node_signature_symbol() {
  if (has_op_node_signature_symbol()) {
    {
      using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>;
      Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.op_node_signature_symbol_));
      ptr->~Shared_ptr();
    }
    eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
  }
}

const ::oneflow::cfg::OpNodeSignature& ConstEagerSymbol::_EagerSymbol_::op_node_signature_symbol() const {
  if (has_op_node_signature_symbol()) {
      const ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>* __attribute__((__may_alias__)) ptr = reinterpret_cast<const ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>*>(&(eager_symbol_type_.op_node_signature_symbol_));
    return *(*ptr);
    } else {
      static const ::std::shared_ptr<::oneflow::cfg::OpNodeSignature> default_static_value = ::std::make_shared<::oneflow::cfg::OpNodeSignature>();
    return *default_static_value;
    }
}
::oneflow::cfg::OpNodeSignature* ConstEagerSymbol::_EagerSymbol_::mutable_op_node_signature_symbol() {
  if(!has_op_node_signature_symbol()) {
    clear_eager_symbol_type();
    new (&(eager_symbol_type_.op_node_signature_symbol_)) ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>(new ::oneflow::cfg::OpNodeSignature());
  }
  eager_symbol_type_case_ = kOpNodeSignatureSymbol;
  ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>* __attribute__((__may_alias__)) ptr = reinterpret_cast<::std::shared_ptr<::oneflow::cfg::OpNodeSignature>*>(&(eager_symbol_type_.op_node_signature_symbol_));
  return  (*ptr).get();
}
ConstEagerSymbol::EagerSymbolTypeCase ConstEagerSymbol::_EagerSymbol_::eager_symbol_type_case() const {
  return eager_symbol_type_case_;
}
bool ConstEagerSymbol::_EagerSymbol_::has_eager_symbol_type() const {
  return eager_symbol_type_case_ != EAGER_SYMBOL_TYPE_NOT_SET;
}
void ConstEagerSymbol::_EagerSymbol_::clear_eager_symbol_type() {
  switch (eager_symbol_type_case()) {
    case kStringSymbol: {
      {
        using String = ::std::string;
        String* ptr = reinterpret_cast<String*>(&(eager_symbol_type_.string_symbol_)[0]);
        ptr->~String();
      }
      break;
    }
    case kScopeSymbol: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ScopeProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.scope_symbol_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kJobConfSymbol: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::JobConfigProto>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.job_conf_symbol_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kParallelConfSymbol: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::ParallelConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.parallel_conf_symbol_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kOpConfSymbol: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OperatorConf>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.op_conf_symbol_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case kOpNodeSignatureSymbol: {
      {
        using Shared_ptr = ::std::shared_ptr<::oneflow::cfg::OpNodeSignature>;
        Shared_ptr* __attribute__((__may_alias__)) ptr = reinterpret_cast<Shared_ptr*>(&(eager_symbol_type_.op_node_signature_symbol_));
        ptr->~Shared_ptr();
      }
      break;
    }
    case EAGER_SYMBOL_TYPE_NOT_SET: {
      break;
    }
  }
  eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
}
void ConstEagerSymbol::_EagerSymbol_::eager_symbol_type_copy_from(const _EagerSymbol_& other) {
  switch (other.eager_symbol_type_case()) {
    case kStringSymbol: {
      set_string_symbol(other.string_symbol());
      break;
    }
    case kScopeSymbol: {
      mutable_scope_symbol()->CopyFrom(other.scope_symbol());
      break;
    }
    case kJobConfSymbol: {
      mutable_job_conf_symbol()->CopyFrom(other.job_conf_symbol());
      break;
    }
    case kParallelConfSymbol: {
      mutable_parallel_conf_symbol()->CopyFrom(other.parallel_conf_symbol());
      break;
    }
    case kOpConfSymbol: {
      mutable_op_conf_symbol()->CopyFrom(other.op_conf_symbol());
      break;
    }
    case kOpNodeSignatureSymbol: {
      mutable_op_node_signature_symbol()->CopyFrom(other.op_node_signature_symbol());
      break;
    }
    case EAGER_SYMBOL_TYPE_NOT_SET: {
      clear_eager_symbol_type();
    }
  }
}


int ConstEagerSymbol::_EagerSymbol_::compare(const _EagerSymbol_& other) {
  if (!(has_symbol_id() == other.has_symbol_id())) {
    return has_symbol_id() < other.has_symbol_id() ? -1 : 1;
  } else if (!(symbol_id() == other.symbol_id())) {
    return symbol_id() < other.symbol_id() ? -1 : 1;
  }
  if (!(eager_symbol_type_case() == other.eager_symbol_type_case())) {
    return eager_symbol_type_case() < other.eager_symbol_type_case() ? -1 : 1;
  }
  switch (eager_symbol_type_case()) {
    case kStringSymbol: {
      if (!(string_symbol() == other.string_symbol())) {
        return string_symbol() < other.string_symbol() ? -1 : 1;
      }
      break;
    }
    case kScopeSymbol: {
      if (!(scope_symbol() == other.scope_symbol())) {
        return scope_symbol() < other.scope_symbol() ? -1 : 1;
      }
      break;
    }
    case kJobConfSymbol: {
      if (!(job_conf_symbol() == other.job_conf_symbol())) {
        return job_conf_symbol() < other.job_conf_symbol() ? -1 : 1;
      }
      break;
    }
    case kParallelConfSymbol: {
      if (!(parallel_conf_symbol() == other.parallel_conf_symbol())) {
        return parallel_conf_symbol() < other.parallel_conf_symbol() ? -1 : 1;
      }
      break;
    }
    case kOpConfSymbol: {
      if (!(op_conf_symbol() == other.op_conf_symbol())) {
        return op_conf_symbol() < other.op_conf_symbol() ? -1 : 1;
      }
      break;
    }
    case kOpNodeSignatureSymbol: {
      if (!(op_node_signature_symbol() == other.op_node_signature_symbol())) {
        return op_node_signature_symbol() < other.op_node_signature_symbol() ? -1 : 1;
      }
      break;
    }
    case EAGER_SYMBOL_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstEagerSymbol::_EagerSymbol_::operator==(const _EagerSymbol_& other) const {
  return true
    && has_symbol_id() == other.has_symbol_id() 
    && symbol_id() == other.symbol_id()
    && eager_symbol_type_case() == other.eager_symbol_type_case()
    && (eager_symbol_type_case() == kStringSymbol ? 
      string_symbol() == other.string_symbol() : true)
    && (eager_symbol_type_case() == kScopeSymbol ? 
      scope_symbol() == other.scope_symbol() : true)
    && (eager_symbol_type_case() == kJobConfSymbol ? 
      job_conf_symbol() == other.job_conf_symbol() : true)
    && (eager_symbol_type_case() == kParallelConfSymbol ? 
      parallel_conf_symbol() == other.parallel_conf_symbol() : true)
    && (eager_symbol_type_case() == kOpConfSymbol ? 
      op_conf_symbol() == other.op_conf_symbol() : true)
    && (eager_symbol_type_case() == kOpNodeSignatureSymbol ? 
      op_node_signature_symbol() == other.op_node_signature_symbol() : true)
  ;
}

std::size_t ConstEagerSymbol::_EagerSymbol_::__CalcHash__() const {
  return 0
    ^ (has_symbol_id() ? std::hash<int64_t>()(symbol_id()) : 0)
    ^ static_cast<std::size_t>(eager_symbol_type_case())
    ^ (has_string_symbol() ? std::hash<::std::string>()(string_symbol()) : 0)
    ^ (has_scope_symbol() ? std::hash<::oneflow::cfg::ScopeProto>()(scope_symbol()) : 0)
    ^ (has_job_conf_symbol() ? std::hash<::oneflow::cfg::JobConfigProto>()(job_conf_symbol()) : 0)
    ^ (has_parallel_conf_symbol() ? std::hash<::oneflow::cfg::ParallelConf>()(parallel_conf_symbol()) : 0)
    ^ (has_op_conf_symbol() ? std::hash<::oneflow::cfg::OperatorConf>()(op_conf_symbol()) : 0)
    ^ (has_op_node_signature_symbol() ? std::hash<::oneflow::cfg::OpNodeSignature>()(op_node_signature_symbol()) : 0)
  ;
}

bool ConstEagerSymbol::_EagerSymbol_::operator<(const _EagerSymbol_& other) const {
  return false
    || !(has_symbol_id() == other.has_symbol_id()) ? 
      has_symbol_id() < other.has_symbol_id() : false
    || !(symbol_id() == other.symbol_id()) ? 
      symbol_id() < other.symbol_id() : false
    || !(eager_symbol_type_case() == other.eager_symbol_type_case()) ? 
      eager_symbol_type_case() < other.eager_symbol_type_case() : false
    || ((eager_symbol_type_case() == kStringSymbol) && 
      !(string_symbol() == other.string_symbol())) ? 
        string_symbol() < other.string_symbol() : false
    || ((eager_symbol_type_case() == kScopeSymbol) && 
      !(scope_symbol() == other.scope_symbol())) ? 
        scope_symbol() < other.scope_symbol() : false
    || ((eager_symbol_type_case() == kJobConfSymbol) && 
      !(job_conf_symbol() == other.job_conf_symbol())) ? 
        job_conf_symbol() < other.job_conf_symbol() : false
    || ((eager_symbol_type_case() == kParallelConfSymbol) && 
      !(parallel_conf_symbol() == other.parallel_conf_symbol())) ? 
        parallel_conf_symbol() < other.parallel_conf_symbol() : false
    || ((eager_symbol_type_case() == kOpConfSymbol) && 
      !(op_conf_symbol() == other.op_conf_symbol())) ? 
        op_conf_symbol() < other.op_conf_symbol() : false
    || ((eager_symbol_type_case() == kOpNodeSignatureSymbol) && 
      !(op_node_signature_symbol() == other.op_node_signature_symbol())) ? 
        op_node_signature_symbol() < other.op_node_signature_symbol() : false
  ;
}

using _EagerSymbol_ =  ConstEagerSymbol::_EagerSymbol_;
ConstEagerSymbol::ConstEagerSymbol(const ::std::shared_ptr<_EagerSymbol_>& data): data_(data) {}
ConstEagerSymbol::ConstEagerSymbol(): data_(::std::make_shared<_EagerSymbol_>()) {}
ConstEagerSymbol::ConstEagerSymbol(const ::oneflow::vm::EagerSymbol& proto_eagersymbol) {
  BuildFromProto(proto_eagersymbol);
}
ConstEagerSymbol::ConstEagerSymbol(const ConstEagerSymbol&) = default;
ConstEagerSymbol::ConstEagerSymbol(ConstEagerSymbol&&) noexcept = default;
ConstEagerSymbol::~ConstEagerSymbol() = default;

void ConstEagerSymbol::ToProto(PbMessage* proto_eagersymbol) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::EagerSymbol*>(proto_eagersymbol));
}
  
::std::string ConstEagerSymbol::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstEagerSymbol::__Empty__() const {
  return !data_;
}

int ConstEagerSymbol::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"symbol_id", 1},
    {"string_symbol", 2},
    {"scope_symbol", 3},
    {"job_conf_symbol", 4},
    {"parallel_conf_symbol", 5},
    {"op_conf_symbol", 6},
    {"op_node_signature_symbol", 7},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstEagerSymbol::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstEagerSymbol::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(int64_t),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ScopeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstScopeProto),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::JobConfigProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstJobConfigProto),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConf),
      };
      return type_indices;
    }
    case 6: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OperatorConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOperatorConf),
      };
      return type_indices;
    }
    case 7: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OpNodeSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOpNodeSignature),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstEagerSymbol::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &symbol_id();
    case 2: return &string_symbol();
    case 3: return &scope_symbol();
    case 4: return &job_conf_symbol();
    case 5: return &parallel_conf_symbol();
    case 6: return &op_conf_symbol();
    case 7: return &op_node_signature_symbol();
    default: return nullptr;
  }
}

// required or optional field symbol_id
bool ConstEagerSymbol::has_symbol_id() const {
  return __SharedPtrOrDefault__()->has_symbol_id();
}
const int64_t& ConstEagerSymbol::symbol_id() const {
  return __SharedPtrOrDefault__()->symbol_id();
}
// used by pybind11 only
 // oneof field eager_symbol_type: string_symbol
bool ConstEagerSymbol::has_string_symbol() const {
  return __SharedPtrOrDefault__()->has_string_symbol();
}
const ::std::string& ConstEagerSymbol::string_symbol() const {
  return __SharedPtrOrDefault__()->string_symbol();
}

// used by pybind11 only
 // oneof field eager_symbol_type: scope_symbol
bool ConstEagerSymbol::has_scope_symbol() const {
  return __SharedPtrOrDefault__()->has_scope_symbol();
}
const ::oneflow::cfg::ScopeProto& ConstEagerSymbol::scope_symbol() const {
  return __SharedPtrOrDefault__()->scope_symbol();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstScopeProto> ConstEagerSymbol::shared_const_scope_symbol() const {
  return scope_symbol().__SharedConst__();
}
 // oneof field eager_symbol_type: job_conf_symbol
bool ConstEagerSymbol::has_job_conf_symbol() const {
  return __SharedPtrOrDefault__()->has_job_conf_symbol();
}
const ::oneflow::cfg::JobConfigProto& ConstEagerSymbol::job_conf_symbol() const {
  return __SharedPtrOrDefault__()->job_conf_symbol();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstJobConfigProto> ConstEagerSymbol::shared_const_job_conf_symbol() const {
  return job_conf_symbol().__SharedConst__();
}
 // oneof field eager_symbol_type: parallel_conf_symbol
bool ConstEagerSymbol::has_parallel_conf_symbol() const {
  return __SharedPtrOrDefault__()->has_parallel_conf_symbol();
}
const ::oneflow::cfg::ParallelConf& ConstEagerSymbol::parallel_conf_symbol() const {
  return __SharedPtrOrDefault__()->parallel_conf_symbol();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConf> ConstEagerSymbol::shared_const_parallel_conf_symbol() const {
  return parallel_conf_symbol().__SharedConst__();
}
 // oneof field eager_symbol_type: op_conf_symbol
bool ConstEagerSymbol::has_op_conf_symbol() const {
  return __SharedPtrOrDefault__()->has_op_conf_symbol();
}
const ::oneflow::cfg::OperatorConf& ConstEagerSymbol::op_conf_symbol() const {
  return __SharedPtrOrDefault__()->op_conf_symbol();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> ConstEagerSymbol::shared_const_op_conf_symbol() const {
  return op_conf_symbol().__SharedConst__();
}
 // oneof field eager_symbol_type: op_node_signature_symbol
bool ConstEagerSymbol::has_op_node_signature_symbol() const {
  return __SharedPtrOrDefault__()->has_op_node_signature_symbol();
}
const ::oneflow::cfg::OpNodeSignature& ConstEagerSymbol::op_node_signature_symbol() const {
  return __SharedPtrOrDefault__()->op_node_signature_symbol();
}

// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOpNodeSignature> ConstEagerSymbol::shared_const_op_node_signature_symbol() const {
  return op_node_signature_symbol().__SharedConst__();
}
ConstEagerSymbol::EagerSymbolTypeCase ConstEagerSymbol::eager_symbol_type_case() const {
  return __SharedPtrOrDefault__()->eager_symbol_type_case();
}

bool ConstEagerSymbol::has_eager_symbol_type() const {
  return __SharedPtrOrDefault__()->has_eager_symbol_type();
}

::std::shared_ptr<ConstEagerSymbol> ConstEagerSymbol::__SharedConst__() const {
  return ::std::make_shared<ConstEagerSymbol>(data_);
}
int64_t ConstEagerSymbol::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstEagerSymbol::operator==(const ConstEagerSymbol& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstEagerSymbol::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstEagerSymbol::operator<(const ConstEagerSymbol& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_EagerSymbol_>& ConstEagerSymbol::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_EagerSymbol_> default_ptr = std::make_shared<_EagerSymbol_>();
  return default_ptr;
}
const ::std::shared_ptr<_EagerSymbol_>& ConstEagerSymbol::__SharedPtr__() {
  if (!data_) { data_.reset(new _EagerSymbol_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstEagerSymbol
void ConstEagerSymbol::BuildFromProto(const PbMessage& proto_eagersymbol) {
  data_ = ::std::make_shared<_EagerSymbol_>(dynamic_cast<const ::oneflow::vm::EagerSymbol&>(proto_eagersymbol));
}

EagerSymbol::EagerSymbol(const ::std::shared_ptr<_EagerSymbol_>& data)
  : ConstEagerSymbol(data) {}
EagerSymbol::EagerSymbol(const EagerSymbol& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<EagerSymbol> resize
EagerSymbol::EagerSymbol(EagerSymbol&&) noexcept = default; 
EagerSymbol::EagerSymbol(const ::oneflow::vm::EagerSymbol& proto_eagersymbol) {
  InitFromProto(proto_eagersymbol);
}
EagerSymbol::EagerSymbol() = default;

EagerSymbol::~EagerSymbol() = default;

void EagerSymbol::InitFromProto(const PbMessage& proto_eagersymbol) {
  BuildFromProto(proto_eagersymbol);
}
  
void* EagerSymbol::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_symbol_id();
    case 2: return mutable_string_symbol();
    case 3: return mutable_scope_symbol();
    case 4: return mutable_job_conf_symbol();
    case 5: return mutable_parallel_conf_symbol();
    case 6: return mutable_op_conf_symbol();
    case 7: return mutable_op_node_signature_symbol();
    default: return nullptr;
  }
}

bool EagerSymbol::operator==(const EagerSymbol& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t EagerSymbol::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool EagerSymbol::operator<(const EagerSymbol& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void EagerSymbol::Clear() {
  if (data_) { data_.reset(); }
}
void EagerSymbol::CopyFrom(const EagerSymbol& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
EagerSymbol& EagerSymbol::operator=(const EagerSymbol& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field symbol_id
void EagerSymbol::clear_symbol_id() {
  return __SharedPtr__()->clear_symbol_id();
}
void EagerSymbol::set_symbol_id(const int64_t& value) {
  return __SharedPtr__()->set_symbol_id(value);
}
int64_t* EagerSymbol::mutable_symbol_id() {
  return  __SharedPtr__()->mutable_symbol_id();
}
void EagerSymbol::clear_string_symbol() {
  return __SharedPtr__()->clear_string_symbol();
}
void EagerSymbol::set_string_symbol(const ::std::string& value) {
  return __SharedPtr__()->set_string_symbol(value);
}
::std::string* EagerSymbol::mutable_string_symbol() {
  return  __SharedPtr__()->mutable_string_symbol();
}
void EagerSymbol::clear_scope_symbol() {
  return __SharedPtr__()->clear_scope_symbol();
}
::oneflow::cfg::ScopeProto* EagerSymbol::mutable_scope_symbol() {
  return __SharedPtr__()->mutable_scope_symbol();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ScopeProto> EagerSymbol::shared_mutable_scope_symbol() {
  return mutable_scope_symbol()->__SharedMutable__();
}
void EagerSymbol::clear_job_conf_symbol() {
  return __SharedPtr__()->clear_job_conf_symbol();
}
::oneflow::cfg::JobConfigProto* EagerSymbol::mutable_job_conf_symbol() {
  return __SharedPtr__()->mutable_job_conf_symbol();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::JobConfigProto> EagerSymbol::shared_mutable_job_conf_symbol() {
  return mutable_job_conf_symbol()->__SharedMutable__();
}
void EagerSymbol::clear_parallel_conf_symbol() {
  return __SharedPtr__()->clear_parallel_conf_symbol();
}
::oneflow::cfg::ParallelConf* EagerSymbol::mutable_parallel_conf_symbol() {
  return __SharedPtr__()->mutable_parallel_conf_symbol();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConf> EagerSymbol::shared_mutable_parallel_conf_symbol() {
  return mutable_parallel_conf_symbol()->__SharedMutable__();
}
void EagerSymbol::clear_op_conf_symbol() {
  return __SharedPtr__()->clear_op_conf_symbol();
}
::oneflow::cfg::OperatorConf* EagerSymbol::mutable_op_conf_symbol() {
  return __SharedPtr__()->mutable_op_conf_symbol();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OperatorConf> EagerSymbol::shared_mutable_op_conf_symbol() {
  return mutable_op_conf_symbol()->__SharedMutable__();
}
void EagerSymbol::clear_op_node_signature_symbol() {
  return __SharedPtr__()->clear_op_node_signature_symbol();
}
::oneflow::cfg::OpNodeSignature* EagerSymbol::mutable_op_node_signature_symbol() {
  return __SharedPtr__()->mutable_op_node_signature_symbol();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OpNodeSignature> EagerSymbol::shared_mutable_op_node_signature_symbol() {
  return mutable_op_node_signature_symbol()->__SharedMutable__();
}

::std::shared_ptr<EagerSymbol> EagerSymbol::__SharedMutable__() {
  return ::std::make_shared<EagerSymbol>(__SharedPtr__());
}
ConstEagerSymbolList::_EagerSymbolList_::_EagerSymbolList_() { Clear(); }
ConstEagerSymbolList::_EagerSymbolList_::_EagerSymbolList_(const _EagerSymbolList_& other) { CopyFrom(other); }
ConstEagerSymbolList::_EagerSymbolList_::_EagerSymbolList_(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist) {
  InitFromProto(proto_eagersymbollist);
}
ConstEagerSymbolList::_EagerSymbolList_::_EagerSymbolList_(_EagerSymbolList_&& other) = default;
ConstEagerSymbolList::_EagerSymbolList_::~_EagerSymbolList_() = default;

void ConstEagerSymbolList::_EagerSymbolList_::InitFromProto(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist) {
  Clear();
  // repeated field: eager_symbol
  if (!proto_eagersymbollist.eager_symbol().empty()) {
    for (const ::oneflow::vm::EagerSymbol& elem : proto_eagersymbollist.eager_symbol() ) {
      *mutable_eager_symbol()->Add() = ::oneflow::vm::cfg::EagerSymbol(elem);
    }
  }
    
}

void ConstEagerSymbolList::_EagerSymbolList_::ToProto(::oneflow::vm::EagerSymbolList* proto_eagersymbollist) const {
  proto_eagersymbollist->Clear();
  // repeated field: eager_symbol
  if (!eager_symbol().empty()) {
    for (const ::oneflow::vm::cfg::EagerSymbol& elem : eager_symbol() ) {
      ::oneflow::vm::EagerSymbol proto_eager_symbol_elem;
      elem.ToProto(&proto_eager_symbol_elem);
      *proto_eagersymbollist->mutable_eager_symbol()->Add() = proto_eager_symbol_elem;
    }
  }

}

::std::string ConstEagerSymbolList::_EagerSymbolList_::DebugString() const {
  ::oneflow::vm::EagerSymbolList proto_eagersymbollist;
  this->ToProto(&proto_eagersymbollist);
  return proto_eagersymbollist.DebugString();
}

void ConstEagerSymbolList::_EagerSymbolList_::Clear() {
  clear_eager_symbol();
}

void ConstEagerSymbolList::_EagerSymbolList_::CopyFrom(const _EagerSymbolList_& other) {
  mutable_eager_symbol()->CopyFrom(other.eager_symbol());
}


// repeated field eager_symbol
::std::size_t ConstEagerSymbolList::_EagerSymbolList_::eager_symbol_size() const {
  if (!eager_symbol_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
    return default_static_value->size();
  }
  return eager_symbol_->size();
}
const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& ConstEagerSymbolList::_EagerSymbolList_::eager_symbol() const {
  if (!eager_symbol_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
    return *(default_static_value.get());
  }
  return *(eager_symbol_.get());
}
const ::oneflow::vm::cfg::EagerSymbol& ConstEagerSymbolList::_EagerSymbolList_::eager_symbol(::std::size_t index) const {
  if (!eager_symbol_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
    return default_static_value->Get(index);
  }
  return eager_symbol_->Get(index);
}
void ConstEagerSymbolList::_EagerSymbolList_::clear_eager_symbol() {
  if (!eager_symbol_) {
    eager_symbol_ = ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
  }
  return eager_symbol_->Clear();
}
_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_* ConstEagerSymbolList::_EagerSymbolList_::mutable_eager_symbol() {
  if (!eager_symbol_) {
    eager_symbol_ = ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
  }
  return  eager_symbol_.get();
}
::oneflow::vm::cfg::EagerSymbol* ConstEagerSymbolList::_EagerSymbolList_::mutable_eager_symbol(::std::size_t index) {
  if (!eager_symbol_) {
    eager_symbol_ = ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
  }
  return  eager_symbol_->Mutable(index);
}
::oneflow::vm::cfg::EagerSymbol* ConstEagerSymbolList::_EagerSymbolList_::add_eager_symbol() {
  if (!eager_symbol_) {
    eager_symbol_ = ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>();
  }
  return eager_symbol_->Add();
}


int ConstEagerSymbolList::_EagerSymbolList_::compare(const _EagerSymbolList_& other) {
  if (!(eager_symbol() == other.eager_symbol())) {
    return eager_symbol() < other.eager_symbol() ? -1 : 1;
  }
  return 0;
}

bool ConstEagerSymbolList::_EagerSymbolList_::operator==(const _EagerSymbolList_& other) const {
  return true
    && eager_symbol() == other.eager_symbol()
  ;
}

std::size_t ConstEagerSymbolList::_EagerSymbolList_::__CalcHash__() const {
  return 0
    ^ eager_symbol().__CalcHash__()
  ;
}

bool ConstEagerSymbolList::_EagerSymbolList_::operator<(const _EagerSymbolList_& other) const {
  return false
    || !(eager_symbol() == other.eager_symbol()) ? 
      eager_symbol() < other.eager_symbol() : false
  ;
}

using _EagerSymbolList_ =  ConstEagerSymbolList::_EagerSymbolList_;
ConstEagerSymbolList::ConstEagerSymbolList(const ::std::shared_ptr<_EagerSymbolList_>& data): data_(data) {}
ConstEagerSymbolList::ConstEagerSymbolList(): data_(::std::make_shared<_EagerSymbolList_>()) {}
ConstEagerSymbolList::ConstEagerSymbolList(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist) {
  BuildFromProto(proto_eagersymbollist);
}
ConstEagerSymbolList::ConstEagerSymbolList(const ConstEagerSymbolList&) = default;
ConstEagerSymbolList::ConstEagerSymbolList(ConstEagerSymbolList&&) noexcept = default;
ConstEagerSymbolList::~ConstEagerSymbolList() = default;

void ConstEagerSymbolList::ToProto(PbMessage* proto_eagersymbollist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::EagerSymbolList*>(proto_eagersymbollist));
}
  
::std::string ConstEagerSymbolList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstEagerSymbolList::__Empty__() const {
  return !data_;
}

int ConstEagerSymbolList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"eager_symbol", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstEagerSymbolList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstEagerSymbolList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::EagerSymbol>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstEagerSymbolList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &eager_symbol();
    default: return nullptr;
  }
}

// repeated field eager_symbol
::std::size_t ConstEagerSymbolList::eager_symbol_size() const {
  return __SharedPtrOrDefault__()->eager_symbol_size();
}
const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& ConstEagerSymbolList::eager_symbol() const {
  return __SharedPtrOrDefault__()->eager_symbol();
}
const ::oneflow::vm::cfg::EagerSymbol& ConstEagerSymbolList::eager_symbol(::std::size_t index) const {
  return __SharedPtrOrDefault__()->eager_symbol(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> ConstEagerSymbolList::shared_const_eager_symbol() const {
  return eager_symbol().__SharedConst__();
}
::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbol> ConstEagerSymbolList::shared_const_eager_symbol(::std::size_t index) const {
  return eager_symbol(index).__SharedConst__();
}

::std::shared_ptr<ConstEagerSymbolList> ConstEagerSymbolList::__SharedConst__() const {
  return ::std::make_shared<ConstEagerSymbolList>(data_);
}
int64_t ConstEagerSymbolList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstEagerSymbolList::operator==(const ConstEagerSymbolList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstEagerSymbolList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstEagerSymbolList::operator<(const ConstEagerSymbolList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_EagerSymbolList_>& ConstEagerSymbolList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_EagerSymbolList_> default_ptr = std::make_shared<_EagerSymbolList_>();
  return default_ptr;
}
const ::std::shared_ptr<_EagerSymbolList_>& ConstEagerSymbolList::__SharedPtr__() {
  if (!data_) { data_.reset(new _EagerSymbolList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstEagerSymbolList
void ConstEagerSymbolList::BuildFromProto(const PbMessage& proto_eagersymbollist) {
  data_ = ::std::make_shared<_EagerSymbolList_>(dynamic_cast<const ::oneflow::vm::EagerSymbolList&>(proto_eagersymbollist));
}

EagerSymbolList::EagerSymbolList(const ::std::shared_ptr<_EagerSymbolList_>& data)
  : ConstEagerSymbolList(data) {}
EagerSymbolList::EagerSymbolList(const EagerSymbolList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<EagerSymbolList> resize
EagerSymbolList::EagerSymbolList(EagerSymbolList&&) noexcept = default; 
EagerSymbolList::EagerSymbolList(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist) {
  InitFromProto(proto_eagersymbollist);
}
EagerSymbolList::EagerSymbolList() = default;

EagerSymbolList::~EagerSymbolList() = default;

void EagerSymbolList::InitFromProto(const PbMessage& proto_eagersymbollist) {
  BuildFromProto(proto_eagersymbollist);
}
  
void* EagerSymbolList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_eager_symbol();
    default: return nullptr;
  }
}

bool EagerSymbolList::operator==(const EagerSymbolList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t EagerSymbolList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool EagerSymbolList::operator<(const EagerSymbolList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void EagerSymbolList::Clear() {
  if (data_) { data_.reset(); }
}
void EagerSymbolList::CopyFrom(const EagerSymbolList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
EagerSymbolList& EagerSymbolList::operator=(const EagerSymbolList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field eager_symbol
void EagerSymbolList::clear_eager_symbol() {
  return __SharedPtr__()->clear_eager_symbol();
}
_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_* EagerSymbolList::mutable_eager_symbol() {
  return __SharedPtr__()->mutable_eager_symbol();
}
::oneflow::vm::cfg::EagerSymbol* EagerSymbolList::mutable_eager_symbol(::std::size_t index) {
  return __SharedPtr__()->mutable_eager_symbol(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> EagerSymbolList::shared_mutable_eager_symbol() {
  return mutable_eager_symbol()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> EagerSymbolList::shared_mutable_eager_symbol(::std::size_t index) {
  return mutable_eager_symbol(index)->__SharedMutable__();
}
::oneflow::vm::cfg::EagerSymbol* EagerSymbolList::add_eager_symbol() {
  return __SharedPtr__()->add_eager_symbol();
}

::std::shared_ptr<EagerSymbolList> EagerSymbolList::__SharedMutable__() {
  return ::std::make_shared<EagerSymbolList>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::EagerSymbol>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::EagerSymbol>(data) {}
Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_() = default;
Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::~Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_() = default;


bool Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::operator==(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::EagerSymbol>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::operator<(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbol> Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::EagerSymbol>>& data): Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_(data) {}
_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_() = default;
_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::~_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_() = default;

void _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::CopyFrom(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::EagerSymbol>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::CopyFrom(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::EagerSymbol>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::operator==(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::vm::cfg::EagerSymbol>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::operator<(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
} // namespace vm
