#include "oneflow/core/eager/eager_instruction.cfg.h"
#include "oneflow/core/vm/instruction.cfg.h"
#include "oneflow/core/eager/eager_symbol.cfg.h"
#include "oneflow/core/eager/eager_instruction.pb.h"

namespace oneflow {
namespace vm {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstEagerInstruction::_EagerInstruction_::_EagerInstruction_() { Clear(); }
ConstEagerInstruction::_EagerInstruction_::_EagerInstruction_(const _EagerInstruction_& other) { CopyFrom(other); }
ConstEagerInstruction::_EagerInstruction_::_EagerInstruction_(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction) {
  InitFromProto(proto_eagerinstruction);
}
ConstEagerInstruction::_EagerInstruction_::_EagerInstruction_(_EagerInstruction_&& other) = default;
ConstEagerInstruction::_EagerInstruction_::~_EagerInstruction_() = default;

void ConstEagerInstruction::_EagerInstruction_::InitFromProto(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction) {
  Clear();
  // required_or_optional field: instruction_list
  if (proto_eagerinstruction.has_instruction_list()) {
  *mutable_instruction_list() = ::oneflow::vm::cfg::InstructionListProto(proto_eagerinstruction.instruction_list());      
  }
  // required_or_optional field: eager_symbol_list
  if (proto_eagerinstruction.has_eager_symbol_list()) {
  *mutable_eager_symbol_list() = ::oneflow::vm::cfg::EagerSymbolList(proto_eagerinstruction.eager_symbol_list());      
  }
    
}

void ConstEagerInstruction::_EagerInstruction_::ToProto(::oneflow::vm::EagerInstruction* proto_eagerinstruction) const {
  proto_eagerinstruction->Clear();
  // required_or_optional field: instruction_list
  if (this->has_instruction_list()) {
    ::oneflow::vm::InstructionListProto proto_instruction_list;
    instruction_list().ToProto(&proto_instruction_list);
    proto_eagerinstruction->mutable_instruction_list()->CopyFrom(proto_instruction_list);
    }
  // required_or_optional field: eager_symbol_list
  if (this->has_eager_symbol_list()) {
    ::oneflow::vm::EagerSymbolList proto_eager_symbol_list;
    eager_symbol_list().ToProto(&proto_eager_symbol_list);
    proto_eagerinstruction->mutable_eager_symbol_list()->CopyFrom(proto_eager_symbol_list);
    }

}

::std::string ConstEagerInstruction::_EagerInstruction_::DebugString() const {
  ::oneflow::vm::EagerInstruction proto_eagerinstruction;
  this->ToProto(&proto_eagerinstruction);
  return proto_eagerinstruction.DebugString();
}

void ConstEagerInstruction::_EagerInstruction_::Clear() {
  clear_instruction_list();
  clear_eager_symbol_list();
}

void ConstEagerInstruction::_EagerInstruction_::CopyFrom(const _EagerInstruction_& other) {
  if (other.has_instruction_list()) {
    mutable_instruction_list()->CopyFrom(other.instruction_list());
  } else {
    clear_instruction_list();
  }
  if (other.has_eager_symbol_list()) {
    mutable_eager_symbol_list()->CopyFrom(other.eager_symbol_list());
  } else {
    clear_eager_symbol_list();
  }
}


// optional field instruction_list
bool ConstEagerInstruction::_EagerInstruction_::has_instruction_list() const {
  return has_instruction_list_;
}
const ::oneflow::vm::cfg::InstructionListProto& ConstEagerInstruction::_EagerInstruction_::instruction_list() const {
  if (!instruction_list_) {
    static const ::std::shared_ptr<::oneflow::vm::cfg::InstructionListProto> default_static_value =
      ::std::make_shared<::oneflow::vm::cfg::InstructionListProto>();
    return *default_static_value;
  }
  return *(instruction_list_.get());
}
void ConstEagerInstruction::_EagerInstruction_::clear_instruction_list() {
  if (instruction_list_) {
    instruction_list_->Clear();
  }
  has_instruction_list_ = false;
}
::oneflow::vm::cfg::InstructionListProto* ConstEagerInstruction::_EagerInstruction_::mutable_instruction_list() {
  if (!instruction_list_) {
    instruction_list_ = ::std::make_shared<::oneflow::vm::cfg::InstructionListProto>();
  }
  has_instruction_list_ = true;
  return instruction_list_.get();
}

// optional field eager_symbol_list
bool ConstEagerInstruction::_EagerInstruction_::has_eager_symbol_list() const {
  return has_eager_symbol_list_;
}
const ::oneflow::vm::cfg::EagerSymbolList& ConstEagerInstruction::_EagerInstruction_::eager_symbol_list() const {
  if (!eager_symbol_list_) {
    static const ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbolList> default_static_value =
      ::std::make_shared<::oneflow::vm::cfg::EagerSymbolList>();
    return *default_static_value;
  }
  return *(eager_symbol_list_.get());
}
void ConstEagerInstruction::_EagerInstruction_::clear_eager_symbol_list() {
  if (eager_symbol_list_) {
    eager_symbol_list_->Clear();
  }
  has_eager_symbol_list_ = false;
}
::oneflow::vm::cfg::EagerSymbolList* ConstEagerInstruction::_EagerInstruction_::mutable_eager_symbol_list() {
  if (!eager_symbol_list_) {
    eager_symbol_list_ = ::std::make_shared<::oneflow::vm::cfg::EagerSymbolList>();
  }
  has_eager_symbol_list_ = true;
  return eager_symbol_list_.get();
}


int ConstEagerInstruction::_EagerInstruction_::compare(const _EagerInstruction_& other) {
  if (!(has_instruction_list() == other.has_instruction_list())) {
    return has_instruction_list() < other.has_instruction_list() ? -1 : 1;
  } else if (!(instruction_list() == other.instruction_list())) {
    return instruction_list() < other.instruction_list() ? -1 : 1;
  }
  if (!(has_eager_symbol_list() == other.has_eager_symbol_list())) {
    return has_eager_symbol_list() < other.has_eager_symbol_list() ? -1 : 1;
  } else if (!(eager_symbol_list() == other.eager_symbol_list())) {
    return eager_symbol_list() < other.eager_symbol_list() ? -1 : 1;
  }
  return 0;
}

bool ConstEagerInstruction::_EagerInstruction_::operator==(const _EagerInstruction_& other) const {
  return true
    && has_instruction_list() == other.has_instruction_list() 
    && instruction_list() == other.instruction_list()
    && has_eager_symbol_list() == other.has_eager_symbol_list() 
    && eager_symbol_list() == other.eager_symbol_list()
  ;
}

std::size_t ConstEagerInstruction::_EagerInstruction_::__CalcHash__() const {
  return 0
    ^ (has_instruction_list() ? std::hash<::oneflow::vm::cfg::InstructionListProto>()(instruction_list()) : 0)
    ^ (has_eager_symbol_list() ? std::hash<::oneflow::vm::cfg::EagerSymbolList>()(eager_symbol_list()) : 0)
  ;
}

bool ConstEagerInstruction::_EagerInstruction_::operator<(const _EagerInstruction_& other) const {
  return false
    || !(has_instruction_list() == other.has_instruction_list()) ? 
      has_instruction_list() < other.has_instruction_list() : false
    || !(instruction_list() == other.instruction_list()) ? 
      instruction_list() < other.instruction_list() : false
    || !(has_eager_symbol_list() == other.has_eager_symbol_list()) ? 
      has_eager_symbol_list() < other.has_eager_symbol_list() : false
    || !(eager_symbol_list() == other.eager_symbol_list()) ? 
      eager_symbol_list() < other.eager_symbol_list() : false
  ;
}

using _EagerInstruction_ =  ConstEagerInstruction::_EagerInstruction_;
ConstEagerInstruction::ConstEagerInstruction(const ::std::shared_ptr<_EagerInstruction_>& data): data_(data) {}
ConstEagerInstruction::ConstEagerInstruction(): data_(::std::make_shared<_EagerInstruction_>()) {}
ConstEagerInstruction::ConstEagerInstruction(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction) {
  BuildFromProto(proto_eagerinstruction);
}
ConstEagerInstruction::ConstEagerInstruction(const ConstEagerInstruction&) = default;
ConstEagerInstruction::ConstEagerInstruction(ConstEagerInstruction&&) noexcept = default;
ConstEagerInstruction::~ConstEagerInstruction() = default;

void ConstEagerInstruction::ToProto(PbMessage* proto_eagerinstruction) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::vm::EagerInstruction*>(proto_eagerinstruction));
}
  
::std::string ConstEagerInstruction::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstEagerInstruction::__Empty__() const {
  return !data_;
}

int ConstEagerInstruction::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"instruction_list", 1},
    {"eager_symbol_list", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstEagerInstruction::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstEagerInstruction::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::InstructionListProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstInstructionListProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::vm::cfg::EagerSymbolList),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::vm::cfg::ConstEagerSymbolList),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstEagerInstruction::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &instruction_list();
    case 2: return &eager_symbol_list();
    default: return nullptr;
  }
}

// required or optional field instruction_list
bool ConstEagerInstruction::has_instruction_list() const {
  return __SharedPtrOrDefault__()->has_instruction_list();
}
const ::oneflow::vm::cfg::InstructionListProto& ConstEagerInstruction::instruction_list() const {
  return __SharedPtrOrDefault__()->instruction_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstInstructionListProto> ConstEagerInstruction::shared_const_instruction_list() const {
  return instruction_list().__SharedConst__();
}
// required or optional field eager_symbol_list
bool ConstEagerInstruction::has_eager_symbol_list() const {
  return __SharedPtrOrDefault__()->has_eager_symbol_list();
}
const ::oneflow::vm::cfg::EagerSymbolList& ConstEagerInstruction::eager_symbol_list() const {
  return __SharedPtrOrDefault__()->eager_symbol_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbolList> ConstEagerInstruction::shared_const_eager_symbol_list() const {
  return eager_symbol_list().__SharedConst__();
}

::std::shared_ptr<ConstEagerInstruction> ConstEagerInstruction::__SharedConst__() const {
  return ::std::make_shared<ConstEagerInstruction>(data_);
}
int64_t ConstEagerInstruction::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstEagerInstruction::operator==(const ConstEagerInstruction& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstEagerInstruction::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstEagerInstruction::operator<(const ConstEagerInstruction& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_EagerInstruction_>& ConstEagerInstruction::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_EagerInstruction_> default_ptr = std::make_shared<_EagerInstruction_>();
  return default_ptr;
}
const ::std::shared_ptr<_EagerInstruction_>& ConstEagerInstruction::__SharedPtr__() {
  if (!data_) { data_.reset(new _EagerInstruction_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstEagerInstruction
void ConstEagerInstruction::BuildFromProto(const PbMessage& proto_eagerinstruction) {
  data_ = ::std::make_shared<_EagerInstruction_>(dynamic_cast<const ::oneflow::vm::EagerInstruction&>(proto_eagerinstruction));
}

EagerInstruction::EagerInstruction(const ::std::shared_ptr<_EagerInstruction_>& data)
  : ConstEagerInstruction(data) {}
EagerInstruction::EagerInstruction(const EagerInstruction& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<EagerInstruction> resize
EagerInstruction::EagerInstruction(EagerInstruction&&) noexcept = default; 
EagerInstruction::EagerInstruction(const ::oneflow::vm::EagerInstruction& proto_eagerinstruction) {
  InitFromProto(proto_eagerinstruction);
}
EagerInstruction::EagerInstruction() = default;

EagerInstruction::~EagerInstruction() = default;

void EagerInstruction::InitFromProto(const PbMessage& proto_eagerinstruction) {
  BuildFromProto(proto_eagerinstruction);
}
  
void* EagerInstruction::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_instruction_list();
    case 2: return mutable_eager_symbol_list();
    default: return nullptr;
  }
}

bool EagerInstruction::operator==(const EagerInstruction& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t EagerInstruction::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool EagerInstruction::operator<(const EagerInstruction& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void EagerInstruction::Clear() {
  if (data_) { data_.reset(); }
}
void EagerInstruction::CopyFrom(const EagerInstruction& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
EagerInstruction& EagerInstruction::operator=(const EagerInstruction& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field instruction_list
void EagerInstruction::clear_instruction_list() {
  return __SharedPtr__()->clear_instruction_list();
}
::oneflow::vm::cfg::InstructionListProto* EagerInstruction::mutable_instruction_list() {
  return __SharedPtr__()->mutable_instruction_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::InstructionListProto> EagerInstruction::shared_mutable_instruction_list() {
  return mutable_instruction_list()->__SharedMutable__();
}
// required or optional field eager_symbol_list
void EagerInstruction::clear_eager_symbol_list() {
  return __SharedPtr__()->clear_eager_symbol_list();
}
::oneflow::vm::cfg::EagerSymbolList* EagerInstruction::mutable_eager_symbol_list() {
  return __SharedPtr__()->mutable_eager_symbol_list();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::vm::cfg::EagerSymbolList> EagerInstruction::shared_mutable_eager_symbol_list() {
  return mutable_eager_symbol_list()->__SharedMutable__();
}

::std::shared_ptr<EagerInstruction> EagerInstruction::__SharedMutable__() {
  return ::std::make_shared<EagerInstruction>(__SharedPtr__());
}


}
} // namespace oneflow
} // namespace vm
