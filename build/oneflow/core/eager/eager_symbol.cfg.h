#ifndef CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H_
#define CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstJobConfigProto;
class JobConfigProto;
}
}
namespace oneflow {
namespace cfg {
class ConstOpNodeSignature;
class OpNodeSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstOperatorConf;
class OperatorConf;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelConf;
class ParallelConf;
}
}
namespace oneflow {
namespace cfg {
class ConstScopeProto;
class ScopeProto;
}
}

namespace oneflow {
namespace vm {

// forward declare proto class;
class EagerSymbol;
class EagerSymbolList;

namespace cfg {


class EagerSymbol;
class ConstEagerSymbol;

class EagerSymbolList;
class ConstEagerSymbolList;



class ConstEagerSymbol : public ::oneflow::cfg::Message {
 public:

 // oneof enum eager_symbol_type
 enum EagerSymbolTypeCase : unsigned int {
  EAGER_SYMBOL_TYPE_NOT_SET = 0,
    kStringSymbol = 2,
    kScopeSymbol = 3,
    kJobConfSymbol = 4,
    kParallelConfSymbol = 5,
    kOpConfSymbol = 6,
    kOpNodeSignatureSymbol = 7,
   };

  class _EagerSymbol_ {
   public:
    _EagerSymbol_();
    explicit _EagerSymbol_(const _EagerSymbol_& other);
    explicit _EagerSymbol_(_EagerSymbol_&& other);
    _EagerSymbol_(const ::oneflow::vm::EagerSymbol& proto_eagersymbol);
    ~_EagerSymbol_();

    void InitFromProto(const ::oneflow::vm::EagerSymbol& proto_eagersymbol);

    void ToProto(::oneflow::vm::EagerSymbol* proto_eagersymbol) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _EagerSymbol_& other);
  
      // optional field symbol_id
     public:
    bool has_symbol_id() const;
    const int64_t& symbol_id() const;
    void clear_symbol_id();
    void set_symbol_id(const int64_t& value);
    int64_t* mutable_symbol_id();
   protected:
    bool has_symbol_id_ = false;
    int64_t symbol_id_;
      
     // oneof field eager_symbol_type: string_symbol
   public:
    bool has_string_symbol() const;
    void clear_string_symbol();
    const ::std::string& string_symbol() const;
      void set_string_symbol(const ::std::string& value);
    ::std::string* mutable_string_symbol();
      
     // oneof field eager_symbol_type: scope_symbol
   public:
    bool has_scope_symbol() const;
    void clear_scope_symbol();
    const ::oneflow::cfg::ScopeProto& scope_symbol() const;
      ::oneflow::cfg::ScopeProto* mutable_scope_symbol();
      
     // oneof field eager_symbol_type: job_conf_symbol
   public:
    bool has_job_conf_symbol() const;
    void clear_job_conf_symbol();
    const ::oneflow::cfg::JobConfigProto& job_conf_symbol() const;
      ::oneflow::cfg::JobConfigProto* mutable_job_conf_symbol();
      
     // oneof field eager_symbol_type: parallel_conf_symbol
   public:
    bool has_parallel_conf_symbol() const;
    void clear_parallel_conf_symbol();
    const ::oneflow::cfg::ParallelConf& parallel_conf_symbol() const;
      ::oneflow::cfg::ParallelConf* mutable_parallel_conf_symbol();
      
     // oneof field eager_symbol_type: op_conf_symbol
   public:
    bool has_op_conf_symbol() const;
    void clear_op_conf_symbol();
    const ::oneflow::cfg::OperatorConf& op_conf_symbol() const;
      ::oneflow::cfg::OperatorConf* mutable_op_conf_symbol();
      
     // oneof field eager_symbol_type: op_node_signature_symbol
   public:
    bool has_op_node_signature_symbol() const;
    void clear_op_node_signature_symbol();
    const ::oneflow::cfg::OpNodeSignature& op_node_signature_symbol() const;
      ::oneflow::cfg::OpNodeSignature* mutable_op_node_signature_symbol();
           
   public:
    // oneof eager_symbol_type
    EagerSymbolTypeCase eager_symbol_type_case() const;
    bool has_eager_symbol_type() const;
   protected:
    void clear_eager_symbol_type();
    void eager_symbol_type_copy_from(const _EagerSymbol_& other);
    union EagerSymbolTypeUnion {
      // 64-bit aligned
      uint64_t __eager_symbol_type_for_padding_64bit__;
          char string_symbol_[sizeof(::std::string)];
            char scope_symbol_[sizeof(::std::shared_ptr<::oneflow::cfg::ScopeProto>)];
            char job_conf_symbol_[sizeof(::std::shared_ptr<::oneflow::cfg::JobConfigProto>)];
            char parallel_conf_symbol_[sizeof(::std::shared_ptr<::oneflow::cfg::ParallelConf>)];
            char op_conf_symbol_[sizeof(::std::shared_ptr<::oneflow::cfg::OperatorConf>)];
            char op_node_signature_symbol_[sizeof(::std::shared_ptr<::oneflow::cfg::OpNodeSignature>)];
        } eager_symbol_type_;
    EagerSymbolTypeCase eager_symbol_type_case_ = EAGER_SYMBOL_TYPE_NOT_SET;
     
   public:
    int compare(const _EagerSymbol_& other);

    bool operator==(const _EagerSymbol_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _EagerSymbol_& other) const;
  };

  ConstEagerSymbol(const ::std::shared_ptr<_EagerSymbol_>& data);
  ConstEagerSymbol(const ConstEagerSymbol&);
  ConstEagerSymbol(ConstEagerSymbol&&) noexcept;
  ConstEagerSymbol();
  ConstEagerSymbol(const ::oneflow::vm::EagerSymbol& proto_eagersymbol);
  virtual ~ConstEagerSymbol() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_eagersymbol) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field symbol_id
 public:
  bool has_symbol_id() const;
  const int64_t& symbol_id() const;
  // used by pybind11 only
 // oneof field eager_symbol_type: string_symbol
 public:
  bool has_string_symbol() const;
  const ::std::string& string_symbol() const;
  // used by pybind11 only
 // oneof field eager_symbol_type: scope_symbol
 public:
  bool has_scope_symbol() const;
  const ::oneflow::cfg::ScopeProto& scope_symbol() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstScopeProto> shared_const_scope_symbol() const;
 // oneof field eager_symbol_type: job_conf_symbol
 public:
  bool has_job_conf_symbol() const;
  const ::oneflow::cfg::JobConfigProto& job_conf_symbol() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstJobConfigProto> shared_const_job_conf_symbol() const;
 // oneof field eager_symbol_type: parallel_conf_symbol
 public:
  bool has_parallel_conf_symbol() const;
  const ::oneflow::cfg::ParallelConf& parallel_conf_symbol() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConf> shared_const_parallel_conf_symbol() const;
 // oneof field eager_symbol_type: op_conf_symbol
 public:
  bool has_op_conf_symbol() const;
  const ::oneflow::cfg::OperatorConf& op_conf_symbol() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> shared_const_op_conf_symbol() const;
 // oneof field eager_symbol_type: op_node_signature_symbol
 public:
  bool has_op_node_signature_symbol() const;
  const ::oneflow::cfg::OpNodeSignature& op_node_signature_symbol() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOpNodeSignature> shared_const_op_node_signature_symbol() const;
 public:
  EagerSymbolTypeCase eager_symbol_type_case() const;
 protected:
  bool has_eager_symbol_type() const;

 public:
  ::std::shared_ptr<ConstEagerSymbol> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstEagerSymbol& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstEagerSymbol& other) const;
 protected:
  const ::std::shared_ptr<_EagerSymbol_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_EagerSymbol_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstEagerSymbol
  void BuildFromProto(const PbMessage& proto_eagersymbol);
  
  ::std::shared_ptr<_EagerSymbol_> data_;
};

class EagerSymbol final : public ConstEagerSymbol {
 public:
  EagerSymbol(const ::std::shared_ptr<_EagerSymbol_>& data);
  EagerSymbol(const EagerSymbol& other);
  // enable nothrow for ::std::vector<EagerSymbol> resize 
  EagerSymbol(EagerSymbol&&) noexcept;
  EagerSymbol();
  explicit EagerSymbol(const ::oneflow::vm::EagerSymbol& proto_eagersymbol);

  ~EagerSymbol() override;

  void InitFromProto(const PbMessage& proto_eagersymbol) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const EagerSymbol& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const EagerSymbol& other) const;
  void Clear();
  void CopyFrom(const EagerSymbol& other);
  EagerSymbol& operator=(const EagerSymbol& other);

  // required or optional field symbol_id
 public:
  void clear_symbol_id();
  void set_symbol_id(const int64_t& value);
  int64_t* mutable_symbol_id();
  void clear_string_symbol();
  void set_string_symbol(const ::std::string& value);
  ::std::string* mutable_string_symbol();
  void clear_scope_symbol();
  ::oneflow::cfg::ScopeProto* mutable_scope_symbol();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ScopeProto> shared_mutable_scope_symbol();
  void clear_job_conf_symbol();
  ::oneflow::cfg::JobConfigProto* mutable_job_conf_symbol();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::JobConfigProto> shared_mutable_job_conf_symbol();
  void clear_parallel_conf_symbol();
  ::oneflow::cfg::ParallelConf* mutable_parallel_conf_symbol();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConf> shared_mutable_parallel_conf_symbol();
  void clear_op_conf_symbol();
  ::oneflow::cfg::OperatorConf* mutable_op_conf_symbol();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> shared_mutable_op_conf_symbol();
  void clear_op_node_signature_symbol();
  ::oneflow::cfg::OpNodeSignature* mutable_op_node_signature_symbol();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OpNodeSignature> shared_mutable_op_node_signature_symbol();

  ::std::shared_ptr<EagerSymbol> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_;
class Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_;

class ConstEagerSymbolList : public ::oneflow::cfg::Message {
 public:

  class _EagerSymbolList_ {
   public:
    _EagerSymbolList_();
    explicit _EagerSymbolList_(const _EagerSymbolList_& other);
    explicit _EagerSymbolList_(_EagerSymbolList_&& other);
    _EagerSymbolList_(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist);
    ~_EagerSymbolList_();

    void InitFromProto(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist);

    void ToProto(::oneflow::vm::EagerSymbolList* proto_eagersymbollist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _EagerSymbolList_& other);
  
      // repeated field eager_symbol
   public:
    ::std::size_t eager_symbol_size() const;
    const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& eager_symbol() const;
    const ::oneflow::vm::cfg::EagerSymbol& eager_symbol(::std::size_t index) const;
    void clear_eager_symbol();
    _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_* mutable_eager_symbol();
    ::oneflow::vm::cfg::EagerSymbol* mutable_eager_symbol(::std::size_t index);
      ::oneflow::vm::cfg::EagerSymbol* add_eager_symbol();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> eager_symbol_;
         
   public:
    int compare(const _EagerSymbolList_& other);

    bool operator==(const _EagerSymbolList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _EagerSymbolList_& other) const;
  };

  ConstEagerSymbolList(const ::std::shared_ptr<_EagerSymbolList_>& data);
  ConstEagerSymbolList(const ConstEagerSymbolList&);
  ConstEagerSymbolList(ConstEagerSymbolList&&) noexcept;
  ConstEagerSymbolList();
  ConstEagerSymbolList(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist);
  virtual ~ConstEagerSymbolList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_eagersymbollist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field eager_symbol
 public:
  ::std::size_t eager_symbol_size() const;
  const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& eager_symbol() const;
  const ::oneflow::vm::cfg::EagerSymbol& eager_symbol(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> shared_const_eager_symbol() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbol> shared_const_eager_symbol(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstEagerSymbolList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstEagerSymbolList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstEagerSymbolList& other) const;
 protected:
  const ::std::shared_ptr<_EagerSymbolList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_EagerSymbolList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstEagerSymbolList
  void BuildFromProto(const PbMessage& proto_eagersymbollist);
  
  ::std::shared_ptr<_EagerSymbolList_> data_;
};

class EagerSymbolList final : public ConstEagerSymbolList {
 public:
  EagerSymbolList(const ::std::shared_ptr<_EagerSymbolList_>& data);
  EagerSymbolList(const EagerSymbolList& other);
  // enable nothrow for ::std::vector<EagerSymbolList> resize 
  EagerSymbolList(EagerSymbolList&&) noexcept;
  EagerSymbolList();
  explicit EagerSymbolList(const ::oneflow::vm::EagerSymbolList& proto_eagersymbollist);

  ~EagerSymbolList() override;

  void InitFromProto(const PbMessage& proto_eagersymbollist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const EagerSymbolList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const EagerSymbolList& other) const;
  void Clear();
  void CopyFrom(const EagerSymbolList& other);
  EagerSymbolList& operator=(const EagerSymbolList& other);

  // repeated field eager_symbol
 public:
  void clear_eager_symbol();
  _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_* mutable_eager_symbol();
  ::oneflow::vm::cfg::EagerSymbol* mutable_eager_symbol(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> shared_mutable_eager_symbol();
  ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> shared_mutable_eager_symbol(::std::size_t index);
  ::oneflow::vm::cfg::EagerSymbol* add_eager_symbol();

  ::std::shared_ptr<EagerSymbolList> __SharedMutable__();
};







// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::vm::cfg::EagerSymbol> {
 public:
  Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::EagerSymbol>>& data);
  Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_();
  ~Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::vm::cfg::ConstEagerSymbol> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_ final : public Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_ {
 public:
  _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_(const ::std::shared_ptr<::std::vector<::oneflow::vm::cfg::EagerSymbol>>& data);
  _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_();
  ~_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H__RepeatedField_EagerSymbol_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> __SharedAdd__();
  ::std::shared_ptr<::oneflow::vm::cfg::EagerSymbol> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow
} // namespace vm

namespace std {



template<>
struct hash<::oneflow::vm::cfg::ConstEagerSymbol> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstEagerSymbol& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::EagerSymbol> {
  std::size_t operator()(const ::oneflow::vm::cfg::EagerSymbol& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::ConstEagerSymbolList> {
  std::size_t operator()(const ::oneflow::vm::cfg::ConstEagerSymbolList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::vm::cfg::EagerSymbolList> {
  std::size_t operator()(const ::oneflow::vm::cfg::EagerSymbolList& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_EAGER_EAGER_SYMBOL_CFG_H_