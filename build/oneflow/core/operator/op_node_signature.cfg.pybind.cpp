#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/operator/op_node_signature.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/job/parallel_signature.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.operator.op_node_signature", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<ConstOpNodeSignature, ::oneflow::cfg::Message, std::shared_ptr<ConstOpNodeSignature>> registry(m, "ConstOpNodeSignature");
    registry.def("__id__", &::oneflow::cfg::OpNodeSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpNodeSignature::DebugString);
    registry.def("__repr__", &ConstOpNodeSignature::DebugString);

    registry.def("has_sbp_signature", &ConstOpNodeSignature::has_sbp_signature);
    registry.def("sbp_signature", &ConstOpNodeSignature::shared_const_sbp_signature);

    registry.def("has_mirrored_signature", &ConstOpNodeSignature::has_mirrored_signature);
    registry.def("mirrored_signature", &ConstOpNodeSignature::shared_const_mirrored_signature);

    registry.def("has_logical_blob_desc_signature", &ConstOpNodeSignature::has_logical_blob_desc_signature);
    registry.def("logical_blob_desc_signature", &ConstOpNodeSignature::shared_const_logical_blob_desc_signature);

    registry.def("has_parallel_signature", &ConstOpNodeSignature::has_parallel_signature);
    registry.def("parallel_signature", &ConstOpNodeSignature::shared_const_parallel_signature);
  }
  {
    pybind11::class_<::oneflow::cfg::OpNodeSignature, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpNodeSignature>> registry(m, "OpNodeSignature");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpNodeSignature::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNodeSignature::*)(const ConstOpNodeSignature&))&::oneflow::cfg::OpNodeSignature::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNodeSignature::*)(const ::oneflow::cfg::OpNodeSignature&))&::oneflow::cfg::OpNodeSignature::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpNodeSignature::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpNodeSignature::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpNodeSignature::DebugString);



    registry.def("has_sbp_signature", &::oneflow::cfg::OpNodeSignature::has_sbp_signature);
    registry.def("clear_sbp_signature", &::oneflow::cfg::OpNodeSignature::clear_sbp_signature);
    registry.def("sbp_signature", &::oneflow::cfg::OpNodeSignature::shared_const_sbp_signature);
    registry.def("mutable_sbp_signature", &::oneflow::cfg::OpNodeSignature::shared_mutable_sbp_signature);

    registry.def("has_mirrored_signature", &::oneflow::cfg::OpNodeSignature::has_mirrored_signature);
    registry.def("clear_mirrored_signature", &::oneflow::cfg::OpNodeSignature::clear_mirrored_signature);
    registry.def("mirrored_signature", &::oneflow::cfg::OpNodeSignature::shared_const_mirrored_signature);
    registry.def("mutable_mirrored_signature", &::oneflow::cfg::OpNodeSignature::shared_mutable_mirrored_signature);

    registry.def("has_logical_blob_desc_signature", &::oneflow::cfg::OpNodeSignature::has_logical_blob_desc_signature);
    registry.def("clear_logical_blob_desc_signature", &::oneflow::cfg::OpNodeSignature::clear_logical_blob_desc_signature);
    registry.def("logical_blob_desc_signature", &::oneflow::cfg::OpNodeSignature::shared_const_logical_blob_desc_signature);
    registry.def("mutable_logical_blob_desc_signature", &::oneflow::cfg::OpNodeSignature::shared_mutable_logical_blob_desc_signature);

    registry.def("has_parallel_signature", &::oneflow::cfg::OpNodeSignature::has_parallel_signature);
    registry.def("clear_parallel_signature", &::oneflow::cfg::OpNodeSignature::clear_parallel_signature);
    registry.def("parallel_signature", &::oneflow::cfg::OpNodeSignature::shared_const_parallel_signature);
    registry.def("mutable_parallel_signature", &::oneflow::cfg::OpNodeSignature::shared_mutable_parallel_signature);
  }
}