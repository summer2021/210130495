#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/operator/op_conf.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/common/device_type.cfg.h"
#include "oneflow/core/record/record.cfg.h"
#include "oneflow/core/job/resource.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/register/tensor_slice_view.cfg.h"
#include "oneflow/core/framework/user_op_conf.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/graph/boxing/collective_boxing.cfg.h"
#include "oneflow/core/job/initializer_conf.cfg.h"
#include "oneflow/core/job/regularizer_conf.cfg.h"
#include "oneflow/core/job/learning_rate_schedule_conf.cfg.h"
#include "oneflow/core/operator/interface_blob_conf.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.operator.op_conf", m) {
  using namespace oneflow::cfg;
  {
    pybind11::enum_<::oneflow::cfg::ActivationType> enm(m, "ActivationType");
    enm.value("kNone", ::oneflow::cfg::kNone);
    enm.value("kTanH", ::oneflow::cfg::kTanH);
    enm.value("kSigmoid", ::oneflow::cfg::kSigmoid);
    enm.value("kRelu", ::oneflow::cfg::kRelu);
    m.attr("kNone") = enm.attr("kNone");
    m.attr("kTanH") = enm.attr("kTanH");
    m.attr("kSigmoid") = enm.attr("kSigmoid");
    m.attr("kRelu") = enm.attr("kRelu");
  }

  {
    pybind11::enum_<::oneflow::cfg::CopyHdOpConf_Type> enm(m, "CopyHdOpConf_Type");
    enm.value("H2D", ::oneflow::cfg::H2D);
    enm.value("D2H", ::oneflow::cfg::D2H);
    m.attr("H2D") = enm.attr("H2D");
    m.attr("D2H") = enm.attr("D2H");
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::*)(const int32_t&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstInt64List> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstInt64List> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(const ::oneflow::cfg::Int64List&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::Int64List> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::Int64List> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstVariableOpConf> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstVariableOpConf> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(const ::oneflow::cfg::VariableOpConf&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstTensorSliceViewProto> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstTensorSliceViewProto> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(const ::oneflow::cfg::TensorSliceViewProto&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstXrtLaunchOpConf_Argument> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstXrtLaunchOpConf_Argument> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(const ::oneflow::cfg::XrtLaunchOpConf_Argument&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstOperatorConf> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstOperatorConf> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(const ::oneflow::cfg::OperatorConf&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::OperatorConf> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::OperatorConf> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_::__SharedAdd__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__iter__", [](const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("items", [](const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__iter__", [](const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("items", [](const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__iter__", [](const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("items", [](const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__iter__", [](const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("items", [](const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ &s) { return pybind11::make_iterator(s.begin(), s.end()); }, pybind11::keep_alive<0, 1>());
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstSbpSignature>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstSbpSignature>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstSbpSignature> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<SbpSignature>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<SbpSignature>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::SbpSignature> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::size);
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstBlobDescProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<ConstBlobDescProto>>
    registry.def("items", [](const ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_const_begin(), s->shared_const_end()); });
    registry.def("__getitem__", (::std::shared_ptr<ConstBlobDescProto> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::*)(const ::std::string&) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::size);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::CopyFrom);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<BlobDescProto>>
    registry.def("__iter__", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    // lifetime safety is ensured by making iterators for std::pair<const ::std::string, std::shared_ptr<BlobDescProto>>
    registry.def("items", [](const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_>& s) { return pybind11::make_iterator(s->shared_mut_begin(), s->shared_mut_end()); });
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::BlobDescProto> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_::__SharedMutable__);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstOpNameGroups_OpNameGroup> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstOpNameGroups_OpNameGroup> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(const ::oneflow::cfg::OpNameGroups_OpNameGroup&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> (_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstDistributeConcatOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDistributeConcatOpConf>> registry(m, "ConstDistributeConcatOpConf");
    registry.def("__id__", &::oneflow::cfg::DistributeConcatOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDistributeConcatOpConf::DebugString);
    registry.def("__repr__", &ConstDistributeConcatOpConf::DebugString);

    registry.def("in_size", &ConstDistributeConcatOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDistributeConcatOpConf::*)() const)&ConstDistributeConcatOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstDistributeConcatOpConf::*)(::std::size_t) const)&ConstDistributeConcatOpConf::in);

    registry.def("has_out", &ConstDistributeConcatOpConf::has_out);
    registry.def("out", &ConstDistributeConcatOpConf::out);

    registry.def("has_axis", &ConstDistributeConcatOpConf::has_axis);
    registry.def("axis", &ConstDistributeConcatOpConf::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::DistributeConcatOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DistributeConcatOpConf>> registry(m, "DistributeConcatOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DistributeConcatOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeConcatOpConf::*)(const ConstDistributeConcatOpConf&))&::oneflow::cfg::DistributeConcatOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeConcatOpConf::*)(const ::oneflow::cfg::DistributeConcatOpConf&))&::oneflow::cfg::DistributeConcatOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DistributeConcatOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DistributeConcatOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DistributeConcatOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::DistributeConcatOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::DistributeConcatOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeConcatOpConf::*)())&::oneflow::cfg::DistributeConcatOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeConcatOpConf::*)() const)&::oneflow::cfg::DistributeConcatOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::DistributeConcatOpConf::*)(::std::size_t) const)&::oneflow::cfg::DistributeConcatOpConf::in);
    registry.def("add_in", &::oneflow::cfg::DistributeConcatOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::DistributeConcatOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::DistributeConcatOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DistributeConcatOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DistributeConcatOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DistributeConcatOpConf::set_out);

    registry.def("has_axis", &::oneflow::cfg::DistributeConcatOpConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::DistributeConcatOpConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::DistributeConcatOpConf::axis);
    registry.def("set_axis", &::oneflow::cfg::DistributeConcatOpConf::set_axis);
  }
  {
    pybind11::class_<ConstDistributeSplitOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDistributeSplitOpConf>> registry(m, "ConstDistributeSplitOpConf");
    registry.def("__id__", &::oneflow::cfg::DistributeSplitOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDistributeSplitOpConf::DebugString);
    registry.def("__repr__", &ConstDistributeSplitOpConf::DebugString);

    registry.def("has_in", &ConstDistributeSplitOpConf::has_in);
    registry.def("in", &ConstDistributeSplitOpConf::in);

    registry.def("out_size", &ConstDistributeSplitOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDistributeSplitOpConf::*)() const)&ConstDistributeSplitOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstDistributeSplitOpConf::*)(::std::size_t) const)&ConstDistributeSplitOpConf::out);

    registry.def("has_axis", &ConstDistributeSplitOpConf::has_axis);
    registry.def("axis", &ConstDistributeSplitOpConf::axis);

    registry.def("has_is_variable_ref", &ConstDistributeSplitOpConf::has_is_variable_ref);
    registry.def("is_variable_ref", &ConstDistributeSplitOpConf::is_variable_ref);
  }
  {
    pybind11::class_<::oneflow::cfg::DistributeSplitOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DistributeSplitOpConf>> registry(m, "DistributeSplitOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DistributeSplitOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeSplitOpConf::*)(const ConstDistributeSplitOpConf&))&::oneflow::cfg::DistributeSplitOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeSplitOpConf::*)(const ::oneflow::cfg::DistributeSplitOpConf&))&::oneflow::cfg::DistributeSplitOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DistributeSplitOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DistributeSplitOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DistributeSplitOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::DistributeSplitOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::DistributeSplitOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::DistributeSplitOpConf::in);
    registry.def("set_in", &::oneflow::cfg::DistributeSplitOpConf::set_in);

    registry.def("out_size", &::oneflow::cfg::DistributeSplitOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::DistributeSplitOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeSplitOpConf::*)())&::oneflow::cfg::DistributeSplitOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeSplitOpConf::*)() const)&::oneflow::cfg::DistributeSplitOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::DistributeSplitOpConf::*)(::std::size_t) const)&::oneflow::cfg::DistributeSplitOpConf::out);
    registry.def("add_out", &::oneflow::cfg::DistributeSplitOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::DistributeSplitOpConf::set_out);

    registry.def("has_axis", &::oneflow::cfg::DistributeSplitOpConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::DistributeSplitOpConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::DistributeSplitOpConf::axis);
    registry.def("set_axis", &::oneflow::cfg::DistributeSplitOpConf::set_axis);

    registry.def("has_is_variable_ref", &::oneflow::cfg::DistributeSplitOpConf::has_is_variable_ref);
    registry.def("clear_is_variable_ref", &::oneflow::cfg::DistributeSplitOpConf::clear_is_variable_ref);
    registry.def("is_variable_ref", &::oneflow::cfg::DistributeSplitOpConf::is_variable_ref);
    registry.def("set_is_variable_ref", &::oneflow::cfg::DistributeSplitOpConf::set_is_variable_ref);
  }
  {
    pybind11::class_<ConstDistributeCloneOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDistributeCloneOpConf>> registry(m, "ConstDistributeCloneOpConf");
    registry.def("__id__", &::oneflow::cfg::DistributeCloneOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDistributeCloneOpConf::DebugString);
    registry.def("__repr__", &ConstDistributeCloneOpConf::DebugString);

    registry.def("has_in", &ConstDistributeCloneOpConf::has_in);
    registry.def("in", &ConstDistributeCloneOpConf::in);

    registry.def("out_size", &ConstDistributeCloneOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDistributeCloneOpConf::*)() const)&ConstDistributeCloneOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstDistributeCloneOpConf::*)(::std::size_t) const)&ConstDistributeCloneOpConf::out);

    registry.def("has_is_variable_ref", &ConstDistributeCloneOpConf::has_is_variable_ref);
    registry.def("is_variable_ref", &ConstDistributeCloneOpConf::is_variable_ref);
  }
  {
    pybind11::class_<::oneflow::cfg::DistributeCloneOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DistributeCloneOpConf>> registry(m, "DistributeCloneOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DistributeCloneOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeCloneOpConf::*)(const ConstDistributeCloneOpConf&))&::oneflow::cfg::DistributeCloneOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeCloneOpConf::*)(const ::oneflow::cfg::DistributeCloneOpConf&))&::oneflow::cfg::DistributeCloneOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DistributeCloneOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DistributeCloneOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DistributeCloneOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::DistributeCloneOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::DistributeCloneOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::DistributeCloneOpConf::in);
    registry.def("set_in", &::oneflow::cfg::DistributeCloneOpConf::set_in);

    registry.def("out_size", &::oneflow::cfg::DistributeCloneOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::DistributeCloneOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeCloneOpConf::*)())&::oneflow::cfg::DistributeCloneOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeCloneOpConf::*)() const)&::oneflow::cfg::DistributeCloneOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::DistributeCloneOpConf::*)(::std::size_t) const)&::oneflow::cfg::DistributeCloneOpConf::out);
    registry.def("add_out", &::oneflow::cfg::DistributeCloneOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::DistributeCloneOpConf::set_out);

    registry.def("has_is_variable_ref", &::oneflow::cfg::DistributeCloneOpConf::has_is_variable_ref);
    registry.def("clear_is_variable_ref", &::oneflow::cfg::DistributeCloneOpConf::clear_is_variable_ref);
    registry.def("is_variable_ref", &::oneflow::cfg::DistributeCloneOpConf::is_variable_ref);
    registry.def("set_is_variable_ref", &::oneflow::cfg::DistributeCloneOpConf::set_is_variable_ref);
  }
  {
    pybind11::class_<ConstDistributeAddOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDistributeAddOpConf>> registry(m, "ConstDistributeAddOpConf");
    registry.def("__id__", &::oneflow::cfg::DistributeAddOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDistributeAddOpConf::DebugString);
    registry.def("__repr__", &ConstDistributeAddOpConf::DebugString);

    registry.def("in_size", &ConstDistributeAddOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDistributeAddOpConf::*)() const)&ConstDistributeAddOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstDistributeAddOpConf::*)(::std::size_t) const)&ConstDistributeAddOpConf::in);

    registry.def("has_out", &ConstDistributeAddOpConf::has_out);
    registry.def("out", &ConstDistributeAddOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::DistributeAddOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DistributeAddOpConf>> registry(m, "DistributeAddOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DistributeAddOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeAddOpConf::*)(const ConstDistributeAddOpConf&))&::oneflow::cfg::DistributeAddOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DistributeAddOpConf::*)(const ::oneflow::cfg::DistributeAddOpConf&))&::oneflow::cfg::DistributeAddOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DistributeAddOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DistributeAddOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DistributeAddOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::DistributeAddOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::DistributeAddOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeAddOpConf::*)())&::oneflow::cfg::DistributeAddOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DistributeAddOpConf::*)() const)&::oneflow::cfg::DistributeAddOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::DistributeAddOpConf::*)(::std::size_t) const)&::oneflow::cfg::DistributeAddOpConf::in);
    registry.def("add_in", &::oneflow::cfg::DistributeAddOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::DistributeAddOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::DistributeAddOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DistributeAddOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DistributeAddOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DistributeAddOpConf::set_out);
  }
  {
    pybind11::class_<ConstCopyCommNetOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCopyCommNetOpConf>> registry(m, "ConstCopyCommNetOpConf");
    registry.def("__id__", &::oneflow::cfg::CopyCommNetOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCopyCommNetOpConf::DebugString);
    registry.def("__repr__", &ConstCopyCommNetOpConf::DebugString);

    registry.def("has_lbi", &ConstCopyCommNetOpConf::has_lbi);
    registry.def("lbi", &ConstCopyCommNetOpConf::shared_const_lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::CopyCommNetOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CopyCommNetOpConf>> registry(m, "CopyCommNetOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CopyCommNetOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyCommNetOpConf::*)(const ConstCopyCommNetOpConf&))&::oneflow::cfg::CopyCommNetOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyCommNetOpConf::*)(const ::oneflow::cfg::CopyCommNetOpConf&))&::oneflow::cfg::CopyCommNetOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CopyCommNetOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CopyCommNetOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CopyCommNetOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::CopyCommNetOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::CopyCommNetOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::CopyCommNetOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::CopyCommNetOpConf::shared_mutable_lbi);
  }
  {
    pybind11::class_<ConstCopyHdOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCopyHdOpConf>> registry(m, "ConstCopyHdOpConf");
    registry.def("__id__", &::oneflow::cfg::CopyHdOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCopyHdOpConf::DebugString);
    registry.def("__repr__", &ConstCopyHdOpConf::DebugString);

    registry.def("has_type", &ConstCopyHdOpConf::has_type);
    registry.def("type", &ConstCopyHdOpConf::type);

    registry.def("has_lbi", &ConstCopyHdOpConf::has_lbi);
    registry.def("lbi", &ConstCopyHdOpConf::shared_const_lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::CopyHdOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CopyHdOpConf>> registry(m, "CopyHdOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CopyHdOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyHdOpConf::*)(const ConstCopyHdOpConf&))&::oneflow::cfg::CopyHdOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyHdOpConf::*)(const ::oneflow::cfg::CopyHdOpConf&))&::oneflow::cfg::CopyHdOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CopyHdOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CopyHdOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CopyHdOpConf::DebugString);



    registry.def("has_type", &::oneflow::cfg::CopyHdOpConf::has_type);
    registry.def("clear_type", &::oneflow::cfg::CopyHdOpConf::clear_type);
    registry.def("type", &::oneflow::cfg::CopyHdOpConf::type);
    registry.def("set_type", &::oneflow::cfg::CopyHdOpConf::set_type);

    registry.def("has_lbi", &::oneflow::cfg::CopyHdOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::CopyHdOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::CopyHdOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::CopyHdOpConf::shared_mutable_lbi);
  }
  {
    pybind11::class_<ConstBoxConcatConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxConcatConf>> registry(m, "ConstBoxConcatConf");
    registry.def("__id__", &::oneflow::cfg::BoxConcatConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxConcatConf::DebugString);
    registry.def("__repr__", &ConstBoxConcatConf::DebugString);

    registry.def("has_axis", &ConstBoxConcatConf::has_axis);
    registry.def("axis", &ConstBoxConcatConf::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxConcatConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxConcatConf>> registry(m, "BoxConcatConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxConcatConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxConcatConf::*)(const ConstBoxConcatConf&))&::oneflow::cfg::BoxConcatConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxConcatConf::*)(const ::oneflow::cfg::BoxConcatConf&))&::oneflow::cfg::BoxConcatConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxConcatConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxConcatConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxConcatConf::DebugString);



    registry.def("has_axis", &::oneflow::cfg::BoxConcatConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::BoxConcatConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::BoxConcatConf::axis);
    registry.def("set_axis", &::oneflow::cfg::BoxConcatConf::set_axis);
  }
  {
    pybind11::class_<ConstBoxAddConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxAddConf>> registry(m, "ConstBoxAddConf");
    registry.def("__id__", &::oneflow::cfg::BoxAddConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxAddConf::DebugString);
    registry.def("__repr__", &ConstBoxAddConf::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxAddConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxAddConf>> registry(m, "BoxAddConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxAddConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxAddConf::*)(const ConstBoxAddConf&))&::oneflow::cfg::BoxAddConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxAddConf::*)(const ::oneflow::cfg::BoxAddConf&))&::oneflow::cfg::BoxAddConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxAddConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxAddConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxAddConf::DebugString);


  }
  {
    pybind11::class_<ConstBoxSplitConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxSplitConf>> registry(m, "ConstBoxSplitConf");
    registry.def("__id__", &::oneflow::cfg::BoxSplitConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxSplitConf::DebugString);
    registry.def("__repr__", &ConstBoxSplitConf::DebugString);

    registry.def("has_axis", &ConstBoxSplitConf::has_axis);
    registry.def("axis", &ConstBoxSplitConf::axis);

    registry.def("part_num_size", &ConstBoxSplitConf::part_num_size);
    registry.def("part_num", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (ConstBoxSplitConf::*)() const)&ConstBoxSplitConf::shared_const_part_num);
    registry.def("part_num", (const int32_t& (ConstBoxSplitConf::*)(::std::size_t) const)&ConstBoxSplitConf::part_num);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxSplitConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxSplitConf>> registry(m, "BoxSplitConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxSplitConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxSplitConf::*)(const ConstBoxSplitConf&))&::oneflow::cfg::BoxSplitConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxSplitConf::*)(const ::oneflow::cfg::BoxSplitConf&))&::oneflow::cfg::BoxSplitConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxSplitConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxSplitConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxSplitConf::DebugString);



    registry.def("has_axis", &::oneflow::cfg::BoxSplitConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::BoxSplitConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::BoxSplitConf::axis);
    registry.def("set_axis", &::oneflow::cfg::BoxSplitConf::set_axis);

    registry.def("part_num_size", &::oneflow::cfg::BoxSplitConf::part_num_size);
    registry.def("clear_part_num", &::oneflow::cfg::BoxSplitConf::clear_part_num);
    registry.def("mutable_part_num", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::BoxSplitConf::*)())&::oneflow::cfg::BoxSplitConf::shared_mutable_part_num);
    registry.def("part_num", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::BoxSplitConf::*)() const)&::oneflow::cfg::BoxSplitConf::shared_const_part_num);
    registry.def("part_num", (const int32_t& (::oneflow::cfg::BoxSplitConf::*)(::std::size_t) const)&::oneflow::cfg::BoxSplitConf::part_num);
    registry.def("add_part_num", &::oneflow::cfg::BoxSplitConf::add_part_num);
    registry.def("set_part_num", &::oneflow::cfg::BoxSplitConf::set_part_num);
  }
  {
    pybind11::class_<ConstBoxCloneConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxCloneConf>> registry(m, "ConstBoxCloneConf");
    registry.def("__id__", &::oneflow::cfg::BoxCloneConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxCloneConf::DebugString);
    registry.def("__repr__", &ConstBoxCloneConf::DebugString);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxCloneConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxCloneConf>> registry(m, "BoxCloneConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxCloneConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxCloneConf::*)(const ConstBoxCloneConf&))&::oneflow::cfg::BoxCloneConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxCloneConf::*)(const ::oneflow::cfg::BoxCloneConf&))&::oneflow::cfg::BoxCloneConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxCloneConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxCloneConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxCloneConf::DebugString);


  }
  {
    pybind11::class_<ConstBoxingOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxingOpConf>> registry(m, "ConstBoxingOpConf");
    registry.def("__id__", &::oneflow::cfg::BoxingOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxingOpConf::DebugString);
    registry.def("__repr__", &ConstBoxingOpConf::DebugString);

    registry.def("has_lbi", &ConstBoxingOpConf::has_lbi);
    registry.def("lbi", &ConstBoxingOpConf::shared_const_lbi);

    registry.def("has_in_num", &ConstBoxingOpConf::has_in_num);
    registry.def("in_num", &ConstBoxingOpConf::in_num);

    registry.def("has_out_num", &ConstBoxingOpConf::has_out_num);
    registry.def("out_num", &ConstBoxingOpConf::out_num);

    registry.def("has_concat_box", &ConstBoxingOpConf::has_concat_box);
    registry.def("concat_box", &ConstBoxingOpConf::shared_const_concat_box);

    registry.def("has_add_box", &ConstBoxingOpConf::has_add_box);
    registry.def("add_box", &ConstBoxingOpConf::shared_const_add_box);

    registry.def("has_split_box", &ConstBoxingOpConf::has_split_box);
    registry.def("split_box", &ConstBoxingOpConf::shared_const_split_box);

    registry.def("has_clone_box", &ConstBoxingOpConf::has_clone_box);
    registry.def("clone_box", &ConstBoxingOpConf::shared_const_clone_box);
    registry.def("in_box_case",  &ConstBoxingOpConf::in_box_case);
    registry.def_property_readonly_static("IN_BOX_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::IN_BOX_NOT_SET; })
        .def_property_readonly_static("kConcatBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kConcatBox; })
        .def_property_readonly_static("kAddBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kAddBox; })
        ;
    registry.def("out_box_case",  &ConstBoxingOpConf::out_box_case);
    registry.def_property_readonly_static("OUT_BOX_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::OUT_BOX_NOT_SET; })
        .def_property_readonly_static("kSplitBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kSplitBox; })
        .def_property_readonly_static("kCloneBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kCloneBox; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::BoxingOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxingOpConf>> registry(m, "BoxingOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxingOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingOpConf::*)(const ConstBoxingOpConf&))&::oneflow::cfg::BoxingOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingOpConf::*)(const ::oneflow::cfg::BoxingOpConf&))&::oneflow::cfg::BoxingOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxingOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxingOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxingOpConf::DebugString);

    registry.def_property_readonly_static("IN_BOX_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::IN_BOX_NOT_SET; })
        .def_property_readonly_static("kConcatBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kConcatBox; })
        .def_property_readonly_static("kAddBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kAddBox; })
        ;
    registry.def_property_readonly_static("OUT_BOX_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::OUT_BOX_NOT_SET; })
        .def_property_readonly_static("kSplitBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kSplitBox; })
        .def_property_readonly_static("kCloneBox", [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kCloneBox; })
        ;


    registry.def("has_lbi", &::oneflow::cfg::BoxingOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::BoxingOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::BoxingOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::BoxingOpConf::shared_mutable_lbi);

    registry.def("has_in_num", &::oneflow::cfg::BoxingOpConf::has_in_num);
    registry.def("clear_in_num", &::oneflow::cfg::BoxingOpConf::clear_in_num);
    registry.def("in_num", &::oneflow::cfg::BoxingOpConf::in_num);
    registry.def("set_in_num", &::oneflow::cfg::BoxingOpConf::set_in_num);

    registry.def("has_out_num", &::oneflow::cfg::BoxingOpConf::has_out_num);
    registry.def("clear_out_num", &::oneflow::cfg::BoxingOpConf::clear_out_num);
    registry.def("out_num", &::oneflow::cfg::BoxingOpConf::out_num);
    registry.def("set_out_num", &::oneflow::cfg::BoxingOpConf::set_out_num);

    registry.def("has_concat_box", &::oneflow::cfg::BoxingOpConf::has_concat_box);
    registry.def("clear_concat_box", &::oneflow::cfg::BoxingOpConf::clear_concat_box);
    registry.def_property_readonly_static("kConcatBox",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kConcatBox; });
    registry.def("concat_box", &::oneflow::cfg::BoxingOpConf::concat_box);
    registry.def("mutable_concat_box", &::oneflow::cfg::BoxingOpConf::shared_mutable_concat_box);

    registry.def("has_add_box", &::oneflow::cfg::BoxingOpConf::has_add_box);
    registry.def("clear_add_box", &::oneflow::cfg::BoxingOpConf::clear_add_box);
    registry.def_property_readonly_static("kAddBox",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kAddBox; });
    registry.def("add_box", &::oneflow::cfg::BoxingOpConf::add_box);
    registry.def("mutable_add_box", &::oneflow::cfg::BoxingOpConf::shared_mutable_add_box);

    registry.def("has_split_box", &::oneflow::cfg::BoxingOpConf::has_split_box);
    registry.def("clear_split_box", &::oneflow::cfg::BoxingOpConf::clear_split_box);
    registry.def_property_readonly_static("kSplitBox",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kSplitBox; });
    registry.def("split_box", &::oneflow::cfg::BoxingOpConf::split_box);
    registry.def("mutable_split_box", &::oneflow::cfg::BoxingOpConf::shared_mutable_split_box);

    registry.def("has_clone_box", &::oneflow::cfg::BoxingOpConf::has_clone_box);
    registry.def("clear_clone_box", &::oneflow::cfg::BoxingOpConf::clear_clone_box);
    registry.def_property_readonly_static("kCloneBox",
        [](const pybind11::object&){ return ::oneflow::cfg::BoxingOpConf::kCloneBox; });
    registry.def("clone_box", &::oneflow::cfg::BoxingOpConf::clone_box);
    registry.def("mutable_clone_box", &::oneflow::cfg::BoxingOpConf::shared_mutable_clone_box);
    pybind11::enum_<::oneflow::cfg::BoxingOpConf::InBoxCase>(registry, "InBoxCase")
        .value("IN_BOX_NOT_SET", ::oneflow::cfg::BoxingOpConf::IN_BOX_NOT_SET)
        .value("kConcatBox", ::oneflow::cfg::BoxingOpConf::kConcatBox)
        .value("kAddBox", ::oneflow::cfg::BoxingOpConf::kAddBox)
        ;
    registry.def("in_box_case",  &::oneflow::cfg::BoxingOpConf::in_box_case);
    pybind11::enum_<::oneflow::cfg::BoxingOpConf::OutBoxCase>(registry, "OutBoxCase")
        .value("OUT_BOX_NOT_SET", ::oneflow::cfg::BoxingOpConf::OUT_BOX_NOT_SET)
        .value("kSplitBox", ::oneflow::cfg::BoxingOpConf::kSplitBox)
        .value("kCloneBox", ::oneflow::cfg::BoxingOpConf::kCloneBox)
        ;
    registry.def("out_box_case",  &::oneflow::cfg::BoxingOpConf::out_box_case);
  }
  {
    pybind11::class_<ConstDynamicReshapeOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDynamicReshapeOpConf>> registry(m, "ConstDynamicReshapeOpConf");
    registry.def("__id__", &::oneflow::cfg::DynamicReshapeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDynamicReshapeOpConf::DebugString);
    registry.def("__repr__", &ConstDynamicReshapeOpConf::DebugString);

    registry.def("has_in", &ConstDynamicReshapeOpConf::has_in);
    registry.def("in", &ConstDynamicReshapeOpConf::in);

    registry.def("has_out", &ConstDynamicReshapeOpConf::has_out);
    registry.def("out", &ConstDynamicReshapeOpConf::out);

    registry.def("has_shape", &ConstDynamicReshapeOpConf::has_shape);
    registry.def("shape", &ConstDynamicReshapeOpConf::shared_const_shape);
  }
  {
    pybind11::class_<::oneflow::cfg::DynamicReshapeOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DynamicReshapeOpConf>> registry(m, "DynamicReshapeOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DynamicReshapeOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicReshapeOpConf::*)(const ConstDynamicReshapeOpConf&))&::oneflow::cfg::DynamicReshapeOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicReshapeOpConf::*)(const ::oneflow::cfg::DynamicReshapeOpConf&))&::oneflow::cfg::DynamicReshapeOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DynamicReshapeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DynamicReshapeOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DynamicReshapeOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::DynamicReshapeOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::DynamicReshapeOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::DynamicReshapeOpConf::in);
    registry.def("set_in", &::oneflow::cfg::DynamicReshapeOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::DynamicReshapeOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DynamicReshapeOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DynamicReshapeOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DynamicReshapeOpConf::set_out);

    registry.def("has_shape", &::oneflow::cfg::DynamicReshapeOpConf::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::DynamicReshapeOpConf::clear_shape);
    registry.def("shape", &::oneflow::cfg::DynamicReshapeOpConf::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::DynamicReshapeOpConf::shared_mutable_shape);
  }
  {
    pybind11::class_<ConstDynamicReshapeLikeOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDynamicReshapeLikeOpConf>> registry(m, "ConstDynamicReshapeLikeOpConf");
    registry.def("__id__", &::oneflow::cfg::DynamicReshapeLikeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDynamicReshapeLikeOpConf::DebugString);
    registry.def("__repr__", &ConstDynamicReshapeLikeOpConf::DebugString);

    registry.def("has_x", &ConstDynamicReshapeLikeOpConf::has_x);
    registry.def("x", &ConstDynamicReshapeLikeOpConf::x);

    registry.def("has_y", &ConstDynamicReshapeLikeOpConf::has_y);
    registry.def("y", &ConstDynamicReshapeLikeOpConf::y);

    registry.def("has_like", &ConstDynamicReshapeLikeOpConf::has_like);
    registry.def("like", &ConstDynamicReshapeLikeOpConf::like);
  }
  {
    pybind11::class_<::oneflow::cfg::DynamicReshapeLikeOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DynamicReshapeLikeOpConf>> registry(m, "DynamicReshapeLikeOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DynamicReshapeLikeOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicReshapeLikeOpConf::*)(const ConstDynamicReshapeLikeOpConf&))&::oneflow::cfg::DynamicReshapeLikeOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DynamicReshapeLikeOpConf::*)(const ::oneflow::cfg::DynamicReshapeLikeOpConf&))&::oneflow::cfg::DynamicReshapeLikeOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DynamicReshapeLikeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DynamicReshapeLikeOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DynamicReshapeLikeOpConf::DebugString);



    registry.def("has_x", &::oneflow::cfg::DynamicReshapeLikeOpConf::has_x);
    registry.def("clear_x", &::oneflow::cfg::DynamicReshapeLikeOpConf::clear_x);
    registry.def("x", &::oneflow::cfg::DynamicReshapeLikeOpConf::x);
    registry.def("set_x", &::oneflow::cfg::DynamicReshapeLikeOpConf::set_x);

    registry.def("has_y", &::oneflow::cfg::DynamicReshapeLikeOpConf::has_y);
    registry.def("clear_y", &::oneflow::cfg::DynamicReshapeLikeOpConf::clear_y);
    registry.def("y", &::oneflow::cfg::DynamicReshapeLikeOpConf::y);
    registry.def("set_y", &::oneflow::cfg::DynamicReshapeLikeOpConf::set_y);

    registry.def("has_like", &::oneflow::cfg::DynamicReshapeLikeOpConf::has_like);
    registry.def("clear_like", &::oneflow::cfg::DynamicReshapeLikeOpConf::clear_like);
    registry.def("like", &::oneflow::cfg::DynamicReshapeLikeOpConf::like);
    registry.def("set_like", &::oneflow::cfg::DynamicReshapeLikeOpConf::set_like);
  }
  {
    pybind11::class_<ConstFeedInputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstFeedInputOpConf>> registry(m, "ConstFeedInputOpConf");
    registry.def("__id__", &::oneflow::cfg::FeedInputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstFeedInputOpConf::DebugString);
    registry.def("__repr__", &ConstFeedInputOpConf::DebugString);

    registry.def("has_in_0", &ConstFeedInputOpConf::has_in_0);
    registry.def("in_0", &ConstFeedInputOpConf::in_0);

    registry.def("has_out_0", &ConstFeedInputOpConf::has_out_0);
    registry.def("out_0", &ConstFeedInputOpConf::out_0);
  }
  {
    pybind11::class_<::oneflow::cfg::FeedInputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::FeedInputOpConf>> registry(m, "FeedInputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::FeedInputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::FeedInputOpConf::*)(const ConstFeedInputOpConf&))&::oneflow::cfg::FeedInputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::FeedInputOpConf::*)(const ::oneflow::cfg::FeedInputOpConf&))&::oneflow::cfg::FeedInputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::FeedInputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::FeedInputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::FeedInputOpConf::DebugString);



    registry.def("has_in_0", &::oneflow::cfg::FeedInputOpConf::has_in_0);
    registry.def("clear_in_0", &::oneflow::cfg::FeedInputOpConf::clear_in_0);
    registry.def("in_0", &::oneflow::cfg::FeedInputOpConf::in_0);
    registry.def("set_in_0", &::oneflow::cfg::FeedInputOpConf::set_in_0);

    registry.def("has_out_0", &::oneflow::cfg::FeedInputOpConf::has_out_0);
    registry.def("clear_out_0", &::oneflow::cfg::FeedInputOpConf::clear_out_0);
    registry.def("out_0", &::oneflow::cfg::FeedInputOpConf::out_0);
    registry.def("set_out_0", &::oneflow::cfg::FeedInputOpConf::set_out_0);
  }
  {
    pybind11::class_<ConstFeedVariableOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstFeedVariableOpConf>> registry(m, "ConstFeedVariableOpConf");
    registry.def("__id__", &::oneflow::cfg::FeedVariableOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstFeedVariableOpConf::DebugString);
    registry.def("__repr__", &ConstFeedVariableOpConf::DebugString);

    registry.def("has_in_0", &ConstFeedVariableOpConf::has_in_0);
    registry.def("in_0", &ConstFeedVariableOpConf::in_0);

    registry.def("has_out_0", &ConstFeedVariableOpConf::has_out_0);
    registry.def("out_0", &ConstFeedVariableOpConf::out_0);
  }
  {
    pybind11::class_<::oneflow::cfg::FeedVariableOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::FeedVariableOpConf>> registry(m, "FeedVariableOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::FeedVariableOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::FeedVariableOpConf::*)(const ConstFeedVariableOpConf&))&::oneflow::cfg::FeedVariableOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::FeedVariableOpConf::*)(const ::oneflow::cfg::FeedVariableOpConf&))&::oneflow::cfg::FeedVariableOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::FeedVariableOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::FeedVariableOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::FeedVariableOpConf::DebugString);



    registry.def("has_in_0", &::oneflow::cfg::FeedVariableOpConf::has_in_0);
    registry.def("clear_in_0", &::oneflow::cfg::FeedVariableOpConf::clear_in_0);
    registry.def("in_0", &::oneflow::cfg::FeedVariableOpConf::in_0);
    registry.def("set_in_0", &::oneflow::cfg::FeedVariableOpConf::set_in_0);

    registry.def("has_out_0", &::oneflow::cfg::FeedVariableOpConf::has_out_0);
    registry.def("clear_out_0", &::oneflow::cfg::FeedVariableOpConf::clear_out_0);
    registry.def("out_0", &::oneflow::cfg::FeedVariableOpConf::out_0);
    registry.def("set_out_0", &::oneflow::cfg::FeedVariableOpConf::set_out_0);
  }
  {
    pybind11::class_<ConstFetchOutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstFetchOutputOpConf>> registry(m, "ConstFetchOutputOpConf");
    registry.def("__id__", &::oneflow::cfg::FetchOutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstFetchOutputOpConf::DebugString);
    registry.def("__repr__", &ConstFetchOutputOpConf::DebugString);

    registry.def("has_in_0", &ConstFetchOutputOpConf::has_in_0);
    registry.def("in_0", &ConstFetchOutputOpConf::in_0);

    registry.def("has_out_0", &ConstFetchOutputOpConf::has_out_0);
    registry.def("out_0", &ConstFetchOutputOpConf::out_0);
  }
  {
    pybind11::class_<::oneflow::cfg::FetchOutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::FetchOutputOpConf>> registry(m, "FetchOutputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::FetchOutputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::FetchOutputOpConf::*)(const ConstFetchOutputOpConf&))&::oneflow::cfg::FetchOutputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::FetchOutputOpConf::*)(const ::oneflow::cfg::FetchOutputOpConf&))&::oneflow::cfg::FetchOutputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::FetchOutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::FetchOutputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::FetchOutputOpConf::DebugString);



    registry.def("has_in_0", &::oneflow::cfg::FetchOutputOpConf::has_in_0);
    registry.def("clear_in_0", &::oneflow::cfg::FetchOutputOpConf::clear_in_0);
    registry.def("in_0", &::oneflow::cfg::FetchOutputOpConf::in_0);
    registry.def("set_in_0", &::oneflow::cfg::FetchOutputOpConf::set_in_0);

    registry.def("has_out_0", &::oneflow::cfg::FetchOutputOpConf::has_out_0);
    registry.def("clear_out_0", &::oneflow::cfg::FetchOutputOpConf::clear_out_0);
    registry.def("out_0", &::oneflow::cfg::FetchOutputOpConf::out_0);
    registry.def("set_out_0", &::oneflow::cfg::FetchOutputOpConf::set_out_0);
  }
  {
    pybind11::class_<ConstInputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstInputOpConf>> registry(m, "ConstInputOpConf");
    registry.def("__id__", &::oneflow::cfg::InputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstInputOpConf::DebugString);
    registry.def("__repr__", &ConstInputOpConf::DebugString);

    registry.def("has_tick", &ConstInputOpConf::has_tick);
    registry.def("tick", &ConstInputOpConf::tick);

    registry.def("has_out", &ConstInputOpConf::has_out);
    registry.def("out", &ConstInputOpConf::out);

    registry.def("has_blob_conf", &ConstInputOpConf::has_blob_conf);
    registry.def("blob_conf", &ConstInputOpConf::shared_const_blob_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::InputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::InputOpConf>> registry(m, "InputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::InputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::InputOpConf::*)(const ConstInputOpConf&))&::oneflow::cfg::InputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::InputOpConf::*)(const ::oneflow::cfg::InputOpConf&))&::oneflow::cfg::InputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::InputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::InputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::InputOpConf::DebugString);



    registry.def("has_tick", &::oneflow::cfg::InputOpConf::has_tick);
    registry.def("clear_tick", &::oneflow::cfg::InputOpConf::clear_tick);
    registry.def("tick", &::oneflow::cfg::InputOpConf::tick);
    registry.def("set_tick", &::oneflow::cfg::InputOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::InputOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::InputOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::InputOpConf::out);
    registry.def("set_out", &::oneflow::cfg::InputOpConf::set_out);

    registry.def("has_blob_conf", &::oneflow::cfg::InputOpConf::has_blob_conf);
    registry.def("clear_blob_conf", &::oneflow::cfg::InputOpConf::clear_blob_conf);
    registry.def("blob_conf", &::oneflow::cfg::InputOpConf::shared_const_blob_conf);
    registry.def("mutable_blob_conf", &::oneflow::cfg::InputOpConf::shared_mutable_blob_conf);
  }
  {
    pybind11::class_<ConstForeignInputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstForeignInputOpConf>> registry(m, "ConstForeignInputOpConf");
    registry.def("__id__", &::oneflow::cfg::ForeignInputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstForeignInputOpConf::DebugString);
    registry.def("__repr__", &ConstForeignInputOpConf::DebugString);

    registry.def("has_tick", &ConstForeignInputOpConf::has_tick);
    registry.def("tick", &ConstForeignInputOpConf::tick);

    registry.def("has_out", &ConstForeignInputOpConf::has_out);
    registry.def("out", &ConstForeignInputOpConf::out);

    registry.def("has_blob_conf", &ConstForeignInputOpConf::has_blob_conf);
    registry.def("blob_conf", &ConstForeignInputOpConf::shared_const_blob_conf);

    registry.def("has_ofblob_buffer_name", &ConstForeignInputOpConf::has_ofblob_buffer_name);
    registry.def("ofblob_buffer_name", &ConstForeignInputOpConf::ofblob_buffer_name);
  }
  {
    pybind11::class_<::oneflow::cfg::ForeignInputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ForeignInputOpConf>> registry(m, "ForeignInputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ForeignInputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignInputOpConf::*)(const ConstForeignInputOpConf&))&::oneflow::cfg::ForeignInputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignInputOpConf::*)(const ::oneflow::cfg::ForeignInputOpConf&))&::oneflow::cfg::ForeignInputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ForeignInputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ForeignInputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ForeignInputOpConf::DebugString);



    registry.def("has_tick", &::oneflow::cfg::ForeignInputOpConf::has_tick);
    registry.def("clear_tick", &::oneflow::cfg::ForeignInputOpConf::clear_tick);
    registry.def("tick", &::oneflow::cfg::ForeignInputOpConf::tick);
    registry.def("set_tick", &::oneflow::cfg::ForeignInputOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::ForeignInputOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::ForeignInputOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::ForeignInputOpConf::out);
    registry.def("set_out", &::oneflow::cfg::ForeignInputOpConf::set_out);

    registry.def("has_blob_conf", &::oneflow::cfg::ForeignInputOpConf::has_blob_conf);
    registry.def("clear_blob_conf", &::oneflow::cfg::ForeignInputOpConf::clear_blob_conf);
    registry.def("blob_conf", &::oneflow::cfg::ForeignInputOpConf::shared_const_blob_conf);
    registry.def("mutable_blob_conf", &::oneflow::cfg::ForeignInputOpConf::shared_mutable_blob_conf);

    registry.def("has_ofblob_buffer_name", &::oneflow::cfg::ForeignInputOpConf::has_ofblob_buffer_name);
    registry.def("clear_ofblob_buffer_name", &::oneflow::cfg::ForeignInputOpConf::clear_ofblob_buffer_name);
    registry.def("ofblob_buffer_name", &::oneflow::cfg::ForeignInputOpConf::ofblob_buffer_name);
    registry.def("set_ofblob_buffer_name", &::oneflow::cfg::ForeignInputOpConf::set_ofblob_buffer_name);
  }
  {
    pybind11::class_<ConstReturnOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstReturnOpConf>> registry(m, "ConstReturnOpConf");
    registry.def("__id__", &::oneflow::cfg::ReturnOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstReturnOpConf::DebugString);
    registry.def("__repr__", &ConstReturnOpConf::DebugString);

    registry.def("has_in", &ConstReturnOpConf::has_in);
    registry.def("in", &ConstReturnOpConf::in);

    registry.def("has_out", &ConstReturnOpConf::has_out);
    registry.def("out", &ConstReturnOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::ReturnOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ReturnOpConf>> registry(m, "ReturnOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ReturnOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ReturnOpConf::*)(const ConstReturnOpConf&))&::oneflow::cfg::ReturnOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ReturnOpConf::*)(const ::oneflow::cfg::ReturnOpConf&))&::oneflow::cfg::ReturnOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ReturnOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ReturnOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ReturnOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::ReturnOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::ReturnOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::ReturnOpConf::in);
    registry.def("set_in", &::oneflow::cfg::ReturnOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::ReturnOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::ReturnOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::ReturnOpConf::out);
    registry.def("set_out", &::oneflow::cfg::ReturnOpConf::set_out);
  }
  {
    pybind11::class_<ConstOutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstOutputOpConf>> registry(m, "ConstOutputOpConf");
    registry.def("__id__", &::oneflow::cfg::OutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOutputOpConf::DebugString);
    registry.def("__repr__", &ConstOutputOpConf::DebugString);

    registry.def("has_in", &ConstOutputOpConf::has_in);
    registry.def("in", &ConstOutputOpConf::in);

    registry.def("has_out", &ConstOutputOpConf::has_out);
    registry.def("out", &ConstOutputOpConf::out);

    registry.def("has_blob_conf", &ConstOutputOpConf::has_blob_conf);
    registry.def("blob_conf", &ConstOutputOpConf::shared_const_blob_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::OutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OutputOpConf>> registry(m, "OutputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OutputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OutputOpConf::*)(const ConstOutputOpConf&))&::oneflow::cfg::OutputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OutputOpConf::*)(const ::oneflow::cfg::OutputOpConf&))&::oneflow::cfg::OutputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OutputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OutputOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::OutputOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::OutputOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::OutputOpConf::in);
    registry.def("set_in", &::oneflow::cfg::OutputOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::OutputOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::OutputOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::OutputOpConf::out);
    registry.def("set_out", &::oneflow::cfg::OutputOpConf::set_out);

    registry.def("has_blob_conf", &::oneflow::cfg::OutputOpConf::has_blob_conf);
    registry.def("clear_blob_conf", &::oneflow::cfg::OutputOpConf::clear_blob_conf);
    registry.def("blob_conf", &::oneflow::cfg::OutputOpConf::shared_const_blob_conf);
    registry.def("mutable_blob_conf", &::oneflow::cfg::OutputOpConf::shared_mutable_blob_conf);
  }
  {
    pybind11::class_<ConstForeignOutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstForeignOutputOpConf>> registry(m, "ConstForeignOutputOpConf");
    registry.def("__id__", &::oneflow::cfg::ForeignOutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstForeignOutputOpConf::DebugString);
    registry.def("__repr__", &ConstForeignOutputOpConf::DebugString);

    registry.def("has_in", &ConstForeignOutputOpConf::has_in);
    registry.def("in", &ConstForeignOutputOpConf::in);

    registry.def("has_ofblob_buffer_name", &ConstForeignOutputOpConf::has_ofblob_buffer_name);
    registry.def("ofblob_buffer_name", &ConstForeignOutputOpConf::ofblob_buffer_name);
  }
  {
    pybind11::class_<::oneflow::cfg::ForeignOutputOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ForeignOutputOpConf>> registry(m, "ForeignOutputOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ForeignOutputOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignOutputOpConf::*)(const ConstForeignOutputOpConf&))&::oneflow::cfg::ForeignOutputOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignOutputOpConf::*)(const ::oneflow::cfg::ForeignOutputOpConf&))&::oneflow::cfg::ForeignOutputOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ForeignOutputOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ForeignOutputOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ForeignOutputOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::ForeignOutputOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::ForeignOutputOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::ForeignOutputOpConf::in);
    registry.def("set_in", &::oneflow::cfg::ForeignOutputOpConf::set_in);

    registry.def("has_ofblob_buffer_name", &::oneflow::cfg::ForeignOutputOpConf::has_ofblob_buffer_name);
    registry.def("clear_ofblob_buffer_name", &::oneflow::cfg::ForeignOutputOpConf::clear_ofblob_buffer_name);
    registry.def("ofblob_buffer_name", &::oneflow::cfg::ForeignOutputOpConf::ofblob_buffer_name);
    registry.def("set_ofblob_buffer_name", &::oneflow::cfg::ForeignOutputOpConf::set_ofblob_buffer_name);
  }
  {
    pybind11::class_<ConstForeignWatchOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstForeignWatchOpConf>> registry(m, "ConstForeignWatchOpConf");
    registry.def("__id__", &::oneflow::cfg::ForeignWatchOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstForeignWatchOpConf::DebugString);
    registry.def("__repr__", &ConstForeignWatchOpConf::DebugString);

    registry.def("has_in", &ConstForeignWatchOpConf::has_in);
    registry.def("in", &ConstForeignWatchOpConf::in);

    registry.def("has_handler_uuid", &ConstForeignWatchOpConf::has_handler_uuid);
    registry.def("handler_uuid", &ConstForeignWatchOpConf::handler_uuid);
  }
  {
    pybind11::class_<::oneflow::cfg::ForeignWatchOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ForeignWatchOpConf>> registry(m, "ForeignWatchOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ForeignWatchOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignWatchOpConf::*)(const ConstForeignWatchOpConf&))&::oneflow::cfg::ForeignWatchOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ForeignWatchOpConf::*)(const ::oneflow::cfg::ForeignWatchOpConf&))&::oneflow::cfg::ForeignWatchOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ForeignWatchOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ForeignWatchOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ForeignWatchOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::ForeignWatchOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::ForeignWatchOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::ForeignWatchOpConf::in);
    registry.def("set_in", &::oneflow::cfg::ForeignWatchOpConf::set_in);

    registry.def("has_handler_uuid", &::oneflow::cfg::ForeignWatchOpConf::has_handler_uuid);
    registry.def("clear_handler_uuid", &::oneflow::cfg::ForeignWatchOpConf::clear_handler_uuid);
    registry.def("handler_uuid", &::oneflow::cfg::ForeignWatchOpConf::handler_uuid);
    registry.def("set_handler_uuid", &::oneflow::cfg::ForeignWatchOpConf::set_handler_uuid);
  }
  {
    pybind11::class_<ConstVariableOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstVariableOpConf>> registry(m, "ConstVariableOpConf");
    registry.def("__id__", &::oneflow::cfg::VariableOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstVariableOpConf::DebugString);
    registry.def("__repr__", &ConstVariableOpConf::DebugString);

    registry.def("has_tick", &ConstVariableOpConf::has_tick);
    registry.def("tick", &ConstVariableOpConf::tick);

    registry.def("has_out", &ConstVariableOpConf::has_out);
    registry.def("out", &ConstVariableOpConf::out);

    registry.def("has_shape", &ConstVariableOpConf::has_shape);
    registry.def("shape", &ConstVariableOpConf::shared_const_shape);

    registry.def("has_data_type", &ConstVariableOpConf::has_data_type);
    registry.def("data_type", &ConstVariableOpConf::data_type);

    registry.def("has_initializer", &ConstVariableOpConf::has_initializer);
    registry.def("initializer", &ConstVariableOpConf::shared_const_initializer);

    registry.def("has_initialize_with_snapshot", &ConstVariableOpConf::has_initialize_with_snapshot);
    registry.def("initialize_with_snapshot", &ConstVariableOpConf::shared_const_initialize_with_snapshot);

    registry.def("has_model_name", &ConstVariableOpConf::has_model_name);
    registry.def("model_name", &ConstVariableOpConf::model_name);

    registry.def("has_random_seed", &ConstVariableOpConf::has_random_seed);
    registry.def("random_seed", &ConstVariableOpConf::random_seed);

    registry.def("has_regularizer", &ConstVariableOpConf::has_regularizer);
    registry.def("regularizer", &ConstVariableOpConf::shared_const_regularizer);

    registry.def("has_trainable", &ConstVariableOpConf::has_trainable);
    registry.def("trainable", &ConstVariableOpConf::trainable);

    registry.def("parallel_distribution_size", &ConstVariableOpConf::parallel_distribution_size);
    registry.def("parallel_distribution", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstVariableOpConf::*)() const)&ConstVariableOpConf::shared_const_parallel_distribution);
    registry.def("parallel_distribution", (const ::std::string& (ConstVariableOpConf::*)(::std::size_t) const)&ConstVariableOpConf::parallel_distribution);
    registry.def("initialize_case",  &ConstVariableOpConf::initialize_case);
    registry.def_property_readonly_static("INITIALIZE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::INITIALIZE_NOT_SET; })
        .def_property_readonly_static("kInitializer", [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializer; })
        .def_property_readonly_static("kInitializeWithSnapshot", [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializeWithSnapshot; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::VariableOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::VariableOpConf>> registry(m, "VariableOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::VariableOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::VariableOpConf::*)(const ConstVariableOpConf&))&::oneflow::cfg::VariableOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::VariableOpConf::*)(const ::oneflow::cfg::VariableOpConf&))&::oneflow::cfg::VariableOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::VariableOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::VariableOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::VariableOpConf::DebugString);

    registry.def_property_readonly_static("INITIALIZE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::INITIALIZE_NOT_SET; })
        .def_property_readonly_static("kInitializer", [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializer; })
        .def_property_readonly_static("kInitializeWithSnapshot", [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializeWithSnapshot; })
        ;


    registry.def("has_tick", &::oneflow::cfg::VariableOpConf::has_tick);
    registry.def("clear_tick", &::oneflow::cfg::VariableOpConf::clear_tick);
    registry.def("tick", &::oneflow::cfg::VariableOpConf::tick);
    registry.def("set_tick", &::oneflow::cfg::VariableOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::VariableOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::VariableOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::VariableOpConf::out);
    registry.def("set_out", &::oneflow::cfg::VariableOpConf::set_out);

    registry.def("has_shape", &::oneflow::cfg::VariableOpConf::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::VariableOpConf::clear_shape);
    registry.def("shape", &::oneflow::cfg::VariableOpConf::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::VariableOpConf::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::VariableOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::VariableOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::VariableOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::VariableOpConf::set_data_type);

    registry.def("has_initializer", &::oneflow::cfg::VariableOpConf::has_initializer);
    registry.def("clear_initializer", &::oneflow::cfg::VariableOpConf::clear_initializer);
    registry.def_property_readonly_static("kInitializer",
        [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializer; });
    registry.def("initializer", &::oneflow::cfg::VariableOpConf::initializer);
    registry.def("mutable_initializer", &::oneflow::cfg::VariableOpConf::shared_mutable_initializer);

    registry.def("has_initialize_with_snapshot", &::oneflow::cfg::VariableOpConf::has_initialize_with_snapshot);
    registry.def("clear_initialize_with_snapshot", &::oneflow::cfg::VariableOpConf::clear_initialize_with_snapshot);
    registry.def_property_readonly_static("kInitializeWithSnapshot",
        [](const pybind11::object&){ return ::oneflow::cfg::VariableOpConf::kInitializeWithSnapshot; });
    registry.def("initialize_with_snapshot", &::oneflow::cfg::VariableOpConf::initialize_with_snapshot);
    registry.def("mutable_initialize_with_snapshot", &::oneflow::cfg::VariableOpConf::shared_mutable_initialize_with_snapshot);

    registry.def("has_model_name", &::oneflow::cfg::VariableOpConf::has_model_name);
    registry.def("clear_model_name", &::oneflow::cfg::VariableOpConf::clear_model_name);
    registry.def("model_name", &::oneflow::cfg::VariableOpConf::model_name);
    registry.def("set_model_name", &::oneflow::cfg::VariableOpConf::set_model_name);

    registry.def("has_random_seed", &::oneflow::cfg::VariableOpConf::has_random_seed);
    registry.def("clear_random_seed", &::oneflow::cfg::VariableOpConf::clear_random_seed);
    registry.def("random_seed", &::oneflow::cfg::VariableOpConf::random_seed);
    registry.def("set_random_seed", &::oneflow::cfg::VariableOpConf::set_random_seed);

    registry.def("has_regularizer", &::oneflow::cfg::VariableOpConf::has_regularizer);
    registry.def("clear_regularizer", &::oneflow::cfg::VariableOpConf::clear_regularizer);
    registry.def("regularizer", &::oneflow::cfg::VariableOpConf::shared_const_regularizer);
    registry.def("mutable_regularizer", &::oneflow::cfg::VariableOpConf::shared_mutable_regularizer);

    registry.def("has_trainable", &::oneflow::cfg::VariableOpConf::has_trainable);
    registry.def("clear_trainable", &::oneflow::cfg::VariableOpConf::clear_trainable);
    registry.def("trainable", &::oneflow::cfg::VariableOpConf::trainable);
    registry.def("set_trainable", &::oneflow::cfg::VariableOpConf::set_trainable);

    registry.def("parallel_distribution_size", &::oneflow::cfg::VariableOpConf::parallel_distribution_size);
    registry.def("clear_parallel_distribution", &::oneflow::cfg::VariableOpConf::clear_parallel_distribution);
    registry.def("mutable_parallel_distribution", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::VariableOpConf::*)())&::oneflow::cfg::VariableOpConf::shared_mutable_parallel_distribution);
    registry.def("parallel_distribution", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::VariableOpConf::*)() const)&::oneflow::cfg::VariableOpConf::shared_const_parallel_distribution);
    registry.def("parallel_distribution", (const ::std::string& (::oneflow::cfg::VariableOpConf::*)(::std::size_t) const)&::oneflow::cfg::VariableOpConf::parallel_distribution);
    registry.def("add_parallel_distribution", &::oneflow::cfg::VariableOpConf::add_parallel_distribution);
    registry.def("set_parallel_distribution", &::oneflow::cfg::VariableOpConf::set_parallel_distribution);
    pybind11::enum_<::oneflow::cfg::VariableOpConf::InitializeCase>(registry, "InitializeCase")
        .value("INITIALIZE_NOT_SET", ::oneflow::cfg::VariableOpConf::INITIALIZE_NOT_SET)
        .value("kInitializer", ::oneflow::cfg::VariableOpConf::kInitializer)
        .value("kInitializeWithSnapshot", ::oneflow::cfg::VariableOpConf::kInitializeWithSnapshot)
        ;
    registry.def("initialize_case",  &::oneflow::cfg::VariableOpConf::initialize_case);
  }
  {
    pybind11::class_<ConstDecodeRandomOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDecodeRandomOpConf>> registry(m, "ConstDecodeRandomOpConf");
    registry.def("__id__", &::oneflow::cfg::DecodeRandomOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDecodeRandomOpConf::DebugString);
    registry.def("__repr__", &ConstDecodeRandomOpConf::DebugString);

    registry.def("has_tick", &ConstDecodeRandomOpConf::has_tick);
    registry.def("tick", &ConstDecodeRandomOpConf::tick);

    registry.def("has_out", &ConstDecodeRandomOpConf::has_out);
    registry.def("out", &ConstDecodeRandomOpConf::out);

    registry.def("has_shape", &ConstDecodeRandomOpConf::has_shape);
    registry.def("shape", &ConstDecodeRandomOpConf::shared_const_shape);

    registry.def("has_data_type", &ConstDecodeRandomOpConf::has_data_type);
    registry.def("data_type", &ConstDecodeRandomOpConf::data_type);

    registry.def("has_data_initializer", &ConstDecodeRandomOpConf::has_data_initializer);
    registry.def("data_initializer", &ConstDecodeRandomOpConf::shared_const_data_initializer);

    registry.def("has_batch_size", &ConstDecodeRandomOpConf::has_batch_size);
    registry.def("batch_size", &ConstDecodeRandomOpConf::batch_size);
  }
  {
    pybind11::class_<::oneflow::cfg::DecodeRandomOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DecodeRandomOpConf>> registry(m, "DecodeRandomOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DecodeRandomOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DecodeRandomOpConf::*)(const ConstDecodeRandomOpConf&))&::oneflow::cfg::DecodeRandomOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DecodeRandomOpConf::*)(const ::oneflow::cfg::DecodeRandomOpConf&))&::oneflow::cfg::DecodeRandomOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DecodeRandomOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DecodeRandomOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DecodeRandomOpConf::DebugString);



    registry.def("has_tick", &::oneflow::cfg::DecodeRandomOpConf::has_tick);
    registry.def("clear_tick", &::oneflow::cfg::DecodeRandomOpConf::clear_tick);
    registry.def("tick", &::oneflow::cfg::DecodeRandomOpConf::tick);
    registry.def("set_tick", &::oneflow::cfg::DecodeRandomOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::DecodeRandomOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DecodeRandomOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DecodeRandomOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DecodeRandomOpConf::set_out);

    registry.def("has_shape", &::oneflow::cfg::DecodeRandomOpConf::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::DecodeRandomOpConf::clear_shape);
    registry.def("shape", &::oneflow::cfg::DecodeRandomOpConf::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::DecodeRandomOpConf::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::DecodeRandomOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::DecodeRandomOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::DecodeRandomOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::DecodeRandomOpConf::set_data_type);

    registry.def("has_data_initializer", &::oneflow::cfg::DecodeRandomOpConf::has_data_initializer);
    registry.def("clear_data_initializer", &::oneflow::cfg::DecodeRandomOpConf::clear_data_initializer);
    registry.def("data_initializer", &::oneflow::cfg::DecodeRandomOpConf::shared_const_data_initializer);
    registry.def("mutable_data_initializer", &::oneflow::cfg::DecodeRandomOpConf::shared_mutable_data_initializer);

    registry.def("has_batch_size", &::oneflow::cfg::DecodeRandomOpConf::has_batch_size);
    registry.def("clear_batch_size", &::oneflow::cfg::DecodeRandomOpConf::clear_batch_size);
    registry.def("batch_size", &::oneflow::cfg::DecodeRandomOpConf::batch_size);
    registry.def("set_batch_size", &::oneflow::cfg::DecodeRandomOpConf::set_batch_size);
  }
  {
    pybind11::class_<ConstTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstTickOpConf>> registry(m, "ConstTickOpConf");
    registry.def("__id__", &::oneflow::cfg::TickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstTickOpConf::DebugString);
    registry.def("__repr__", &ConstTickOpConf::DebugString);

    registry.def("tick_size", &ConstTickOpConf::tick_size);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstTickOpConf::*)() const)&ConstTickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (ConstTickOpConf::*)(::std::size_t) const)&ConstTickOpConf::tick);

    registry.def("has_out", &ConstTickOpConf::has_out);
    registry.def("out", &ConstTickOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::TickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::TickOpConf>> registry(m, "TickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::TickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::TickOpConf::*)(const ConstTickOpConf&))&::oneflow::cfg::TickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::TickOpConf::*)(const ::oneflow::cfg::TickOpConf&))&::oneflow::cfg::TickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::TickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::TickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::TickOpConf::DebugString);



    registry.def("tick_size", &::oneflow::cfg::TickOpConf::tick_size);
    registry.def("clear_tick", &::oneflow::cfg::TickOpConf::clear_tick);
    registry.def("mutable_tick", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TickOpConf::*)())&::oneflow::cfg::TickOpConf::shared_mutable_tick);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TickOpConf::*)() const)&::oneflow::cfg::TickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (::oneflow::cfg::TickOpConf::*)(::std::size_t) const)&::oneflow::cfg::TickOpConf::tick);
    registry.def("add_tick", &::oneflow::cfg::TickOpConf::add_tick);
    registry.def("set_tick", &::oneflow::cfg::TickOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::TickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::TickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::TickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::TickOpConf::set_out);
  }
  {
    pybind11::class_<ConstDeviceTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDeviceTickOpConf>> registry(m, "ConstDeviceTickOpConf");
    registry.def("__id__", &::oneflow::cfg::DeviceTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDeviceTickOpConf::DebugString);
    registry.def("__repr__", &ConstDeviceTickOpConf::DebugString);

    registry.def("tick_size", &ConstDeviceTickOpConf::tick_size);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDeviceTickOpConf::*)() const)&ConstDeviceTickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (ConstDeviceTickOpConf::*)(::std::size_t) const)&ConstDeviceTickOpConf::tick);

    registry.def("has_out", &ConstDeviceTickOpConf::has_out);
    registry.def("out", &ConstDeviceTickOpConf::out);

    registry.def("has_time_shape", &ConstDeviceTickOpConf::has_time_shape);
    registry.def("time_shape", &ConstDeviceTickOpConf::shared_const_time_shape);
  }
  {
    pybind11::class_<::oneflow::cfg::DeviceTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DeviceTickOpConf>> registry(m, "DeviceTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DeviceTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DeviceTickOpConf::*)(const ConstDeviceTickOpConf&))&::oneflow::cfg::DeviceTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DeviceTickOpConf::*)(const ::oneflow::cfg::DeviceTickOpConf&))&::oneflow::cfg::DeviceTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DeviceTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DeviceTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DeviceTickOpConf::DebugString);



    registry.def("tick_size", &::oneflow::cfg::DeviceTickOpConf::tick_size);
    registry.def("clear_tick", &::oneflow::cfg::DeviceTickOpConf::clear_tick);
    registry.def("mutable_tick", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DeviceTickOpConf::*)())&::oneflow::cfg::DeviceTickOpConf::shared_mutable_tick);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DeviceTickOpConf::*)() const)&::oneflow::cfg::DeviceTickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (::oneflow::cfg::DeviceTickOpConf::*)(::std::size_t) const)&::oneflow::cfg::DeviceTickOpConf::tick);
    registry.def("add_tick", &::oneflow::cfg::DeviceTickOpConf::add_tick);
    registry.def("set_tick", &::oneflow::cfg::DeviceTickOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::DeviceTickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DeviceTickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DeviceTickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DeviceTickOpConf::set_out);

    registry.def("has_time_shape", &::oneflow::cfg::DeviceTickOpConf::has_time_shape);
    registry.def("clear_time_shape", &::oneflow::cfg::DeviceTickOpConf::clear_time_shape);
    registry.def("time_shape", &::oneflow::cfg::DeviceTickOpConf::shared_const_time_shape);
    registry.def("mutable_time_shape", &::oneflow::cfg::DeviceTickOpConf::shared_mutable_time_shape);
  }
  {
    pybind11::class_<ConstWaitAndSendIdsOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstWaitAndSendIdsOpConf>> registry(m, "ConstWaitAndSendIdsOpConf");
    registry.def("__id__", &::oneflow::cfg::WaitAndSendIdsOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstWaitAndSendIdsOpConf::DebugString);
    registry.def("__repr__", &ConstWaitAndSendIdsOpConf::DebugString);

    registry.def("has_out", &ConstWaitAndSendIdsOpConf::has_out);
    registry.def("out", &ConstWaitAndSendIdsOpConf::out);

    registry.def("has_wait_buffer_name", &ConstWaitAndSendIdsOpConf::has_wait_buffer_name);
    registry.def("wait_buffer_name", &ConstWaitAndSendIdsOpConf::wait_buffer_name);

    registry.def("id_list_size", &ConstWaitAndSendIdsOpConf::id_list_size);
    registry.def("id_list", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (ConstWaitAndSendIdsOpConf::*)() const)&ConstWaitAndSendIdsOpConf::shared_const_id_list);
    registry.def("id_list", (::std::shared_ptr<ConstInt64List> (ConstWaitAndSendIdsOpConf::*)(::std::size_t) const)&ConstWaitAndSendIdsOpConf::shared_const_id_list);

    registry.def("has_data_type", &ConstWaitAndSendIdsOpConf::has_data_type);
    registry.def("data_type", &ConstWaitAndSendIdsOpConf::data_type);
  }
  {
    pybind11::class_<::oneflow::cfg::WaitAndSendIdsOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::WaitAndSendIdsOpConf>> registry(m, "WaitAndSendIdsOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::WaitAndSendIdsOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::WaitAndSendIdsOpConf::*)(const ConstWaitAndSendIdsOpConf&))&::oneflow::cfg::WaitAndSendIdsOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::WaitAndSendIdsOpConf::*)(const ::oneflow::cfg::WaitAndSendIdsOpConf&))&::oneflow::cfg::WaitAndSendIdsOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::WaitAndSendIdsOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::WaitAndSendIdsOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::WaitAndSendIdsOpConf::DebugString);



    registry.def("has_out", &::oneflow::cfg::WaitAndSendIdsOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::WaitAndSendIdsOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::WaitAndSendIdsOpConf::out);
    registry.def("set_out", &::oneflow::cfg::WaitAndSendIdsOpConf::set_out);

    registry.def("has_wait_buffer_name", &::oneflow::cfg::WaitAndSendIdsOpConf::has_wait_buffer_name);
    registry.def("clear_wait_buffer_name", &::oneflow::cfg::WaitAndSendIdsOpConf::clear_wait_buffer_name);
    registry.def("wait_buffer_name", &::oneflow::cfg::WaitAndSendIdsOpConf::wait_buffer_name);
    registry.def("set_wait_buffer_name", &::oneflow::cfg::WaitAndSendIdsOpConf::set_wait_buffer_name);

    registry.def("id_list_size", &::oneflow::cfg::WaitAndSendIdsOpConf::id_list_size);
    registry.def("clear_id_list", &::oneflow::cfg::WaitAndSendIdsOpConf::clear_id_list);
    registry.def("mutable_id_list", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (::oneflow::cfg::WaitAndSendIdsOpConf::*)())&::oneflow::cfg::WaitAndSendIdsOpConf::shared_mutable_id_list);
    registry.def("id_list", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (::oneflow::cfg::WaitAndSendIdsOpConf::*)() const)&::oneflow::cfg::WaitAndSendIdsOpConf::shared_const_id_list);
    registry.def("id_list", (::std::shared_ptr<ConstInt64List> (::oneflow::cfg::WaitAndSendIdsOpConf::*)(::std::size_t) const)&::oneflow::cfg::WaitAndSendIdsOpConf::shared_const_id_list);
    registry.def("mutable_id_list", (::std::shared_ptr<::oneflow::cfg::Int64List> (::oneflow::cfg::WaitAndSendIdsOpConf::*)(::std::size_t))&::oneflow::cfg::WaitAndSendIdsOpConf::shared_mutable_id_list);

    registry.def("has_data_type", &::oneflow::cfg::WaitAndSendIdsOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::WaitAndSendIdsOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::WaitAndSendIdsOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::WaitAndSendIdsOpConf::set_data_type);
  }
  {
    pybind11::class_<ConstCallbackNotifyOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCallbackNotifyOpConf>> registry(m, "ConstCallbackNotifyOpConf");
    registry.def("__id__", &::oneflow::cfg::CallbackNotifyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCallbackNotifyOpConf::DebugString);
    registry.def("__repr__", &ConstCallbackNotifyOpConf::DebugString);

    registry.def("has_in", &ConstCallbackNotifyOpConf::has_in);
    registry.def("in", &ConstCallbackNotifyOpConf::in);

    registry.def("callback_buffer_name_size", &ConstCallbackNotifyOpConf::callback_buffer_name_size);
    registry.def("callback_buffer_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstCallbackNotifyOpConf::*)() const)&ConstCallbackNotifyOpConf::shared_const_callback_buffer_name);
    registry.def("callback_buffer_name", (const ::std::string& (ConstCallbackNotifyOpConf::*)(::std::size_t) const)&ConstCallbackNotifyOpConf::callback_buffer_name);
  }
  {
    pybind11::class_<::oneflow::cfg::CallbackNotifyOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CallbackNotifyOpConf>> registry(m, "CallbackNotifyOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CallbackNotifyOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CallbackNotifyOpConf::*)(const ConstCallbackNotifyOpConf&))&::oneflow::cfg::CallbackNotifyOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CallbackNotifyOpConf::*)(const ::oneflow::cfg::CallbackNotifyOpConf&))&::oneflow::cfg::CallbackNotifyOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CallbackNotifyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CallbackNotifyOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CallbackNotifyOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::CallbackNotifyOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::CallbackNotifyOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::CallbackNotifyOpConf::in);
    registry.def("set_in", &::oneflow::cfg::CallbackNotifyOpConf::set_in);

    registry.def("callback_buffer_name_size", &::oneflow::cfg::CallbackNotifyOpConf::callback_buffer_name_size);
    registry.def("clear_callback_buffer_name", &::oneflow::cfg::CallbackNotifyOpConf::clear_callback_buffer_name);
    registry.def("mutable_callback_buffer_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::CallbackNotifyOpConf::*)())&::oneflow::cfg::CallbackNotifyOpConf::shared_mutable_callback_buffer_name);
    registry.def("callback_buffer_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::CallbackNotifyOpConf::*)() const)&::oneflow::cfg::CallbackNotifyOpConf::shared_const_callback_buffer_name);
    registry.def("callback_buffer_name", (const ::std::string& (::oneflow::cfg::CallbackNotifyOpConf::*)(::std::size_t) const)&::oneflow::cfg::CallbackNotifyOpConf::callback_buffer_name);
    registry.def("add_callback_buffer_name", &::oneflow::cfg::CallbackNotifyOpConf::add_callback_buffer_name);
    registry.def("set_callback_buffer_name", &::oneflow::cfg::CallbackNotifyOpConf::set_callback_buffer_name);
  }
  {
    pybind11::class_<ConstReentrantLockOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstReentrantLockOpConf>> registry(m, "ConstReentrantLockOpConf");
    registry.def("__id__", &::oneflow::cfg::ReentrantLockOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstReentrantLockOpConf::DebugString);
    registry.def("__repr__", &ConstReentrantLockOpConf::DebugString);

    registry.def("has_start", &ConstReentrantLockOpConf::has_start);
    registry.def("start", &ConstReentrantLockOpConf::start);

    registry.def("has_end", &ConstReentrantLockOpConf::has_end);
    registry.def("end", &ConstReentrantLockOpConf::end);

    registry.def("has_out", &ConstReentrantLockOpConf::has_out);
    registry.def("out", &ConstReentrantLockOpConf::out);

    registry.def("lock_id2intersecting_lock_ids_size", &ConstReentrantLockOpConf::lock_id2intersecting_lock_ids_size);
    registry.def("lock_id2intersecting_lock_ids", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (ConstReentrantLockOpConf::*)() const)&ConstReentrantLockOpConf::shared_const_lock_id2intersecting_lock_ids);
    registry.def("lock_id2intersecting_lock_ids", (::std::shared_ptr<ConstInt64List> (ConstReentrantLockOpConf::*)(::std::size_t) const)&ConstReentrantLockOpConf::shared_const_lock_id2intersecting_lock_ids);
  }
  {
    pybind11::class_<::oneflow::cfg::ReentrantLockOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ReentrantLockOpConf>> registry(m, "ReentrantLockOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ReentrantLockOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ReentrantLockOpConf::*)(const ConstReentrantLockOpConf&))&::oneflow::cfg::ReentrantLockOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ReentrantLockOpConf::*)(const ::oneflow::cfg::ReentrantLockOpConf&))&::oneflow::cfg::ReentrantLockOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ReentrantLockOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ReentrantLockOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ReentrantLockOpConf::DebugString);



    registry.def("has_start", &::oneflow::cfg::ReentrantLockOpConf::has_start);
    registry.def("clear_start", &::oneflow::cfg::ReentrantLockOpConf::clear_start);
    registry.def("start", &::oneflow::cfg::ReentrantLockOpConf::start);
    registry.def("set_start", &::oneflow::cfg::ReentrantLockOpConf::set_start);

    registry.def("has_end", &::oneflow::cfg::ReentrantLockOpConf::has_end);
    registry.def("clear_end", &::oneflow::cfg::ReentrantLockOpConf::clear_end);
    registry.def("end", &::oneflow::cfg::ReentrantLockOpConf::end);
    registry.def("set_end", &::oneflow::cfg::ReentrantLockOpConf::set_end);

    registry.def("has_out", &::oneflow::cfg::ReentrantLockOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::ReentrantLockOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::ReentrantLockOpConf::out);
    registry.def("set_out", &::oneflow::cfg::ReentrantLockOpConf::set_out);

    registry.def("lock_id2intersecting_lock_ids_size", &::oneflow::cfg::ReentrantLockOpConf::lock_id2intersecting_lock_ids_size);
    registry.def("clear_lock_id2intersecting_lock_ids", &::oneflow::cfg::ReentrantLockOpConf::clear_lock_id2intersecting_lock_ids);
    registry.def("mutable_lock_id2intersecting_lock_ids", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (::oneflow::cfg::ReentrantLockOpConf::*)())&::oneflow::cfg::ReentrantLockOpConf::shared_mutable_lock_id2intersecting_lock_ids);
    registry.def("lock_id2intersecting_lock_ids", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> (::oneflow::cfg::ReentrantLockOpConf::*)() const)&::oneflow::cfg::ReentrantLockOpConf::shared_const_lock_id2intersecting_lock_ids);
    registry.def("lock_id2intersecting_lock_ids", (::std::shared_ptr<ConstInt64List> (::oneflow::cfg::ReentrantLockOpConf::*)(::std::size_t) const)&::oneflow::cfg::ReentrantLockOpConf::shared_const_lock_id2intersecting_lock_ids);
    registry.def("mutable_lock_id2intersecting_lock_ids", (::std::shared_ptr<::oneflow::cfg::Int64List> (::oneflow::cfg::ReentrantLockOpConf::*)(::std::size_t))&::oneflow::cfg::ReentrantLockOpConf::shared_mutable_lock_id2intersecting_lock_ids);
  }
  {
    pybind11::class_<ConstSrcSubsetTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSrcSubsetTickOpConf>> registry(m, "ConstSrcSubsetTickOpConf");
    registry.def("__id__", &::oneflow::cfg::SrcSubsetTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSrcSubsetTickOpConf::DebugString);
    registry.def("__repr__", &ConstSrcSubsetTickOpConf::DebugString);

    registry.def("in_size", &ConstSrcSubsetTickOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstSrcSubsetTickOpConf::*)() const)&ConstSrcSubsetTickOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstSrcSubsetTickOpConf::*)(::std::size_t) const)&ConstSrcSubsetTickOpConf::in);

    registry.def("has_out", &ConstSrcSubsetTickOpConf::has_out);
    registry.def("out", &ConstSrcSubsetTickOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::SrcSubsetTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SrcSubsetTickOpConf>> registry(m, "SrcSubsetTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SrcSubsetTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SrcSubsetTickOpConf::*)(const ConstSrcSubsetTickOpConf&))&::oneflow::cfg::SrcSubsetTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SrcSubsetTickOpConf::*)(const ::oneflow::cfg::SrcSubsetTickOpConf&))&::oneflow::cfg::SrcSubsetTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SrcSubsetTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SrcSubsetTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SrcSubsetTickOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::SrcSubsetTickOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::SrcSubsetTickOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::SrcSubsetTickOpConf::*)())&::oneflow::cfg::SrcSubsetTickOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::SrcSubsetTickOpConf::*)() const)&::oneflow::cfg::SrcSubsetTickOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::SrcSubsetTickOpConf::*)(::std::size_t) const)&::oneflow::cfg::SrcSubsetTickOpConf::in);
    registry.def("add_in", &::oneflow::cfg::SrcSubsetTickOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::SrcSubsetTickOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::SrcSubsetTickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::SrcSubsetTickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::SrcSubsetTickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::SrcSubsetTickOpConf::set_out);
  }
  {
    pybind11::class_<ConstDstSubsetTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstDstSubsetTickOpConf>> registry(m, "ConstDstSubsetTickOpConf");
    registry.def("__id__", &::oneflow::cfg::DstSubsetTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstDstSubsetTickOpConf::DebugString);
    registry.def("__repr__", &ConstDstSubsetTickOpConf::DebugString);

    registry.def("in_size", &ConstDstSubsetTickOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstDstSubsetTickOpConf::*)() const)&ConstDstSubsetTickOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstDstSubsetTickOpConf::*)(::std::size_t) const)&ConstDstSubsetTickOpConf::in);

    registry.def("has_out", &ConstDstSubsetTickOpConf::has_out);
    registry.def("out", &ConstDstSubsetTickOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::DstSubsetTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::DstSubsetTickOpConf>> registry(m, "DstSubsetTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::DstSubsetTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::DstSubsetTickOpConf::*)(const ConstDstSubsetTickOpConf&))&::oneflow::cfg::DstSubsetTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::DstSubsetTickOpConf::*)(const ::oneflow::cfg::DstSubsetTickOpConf&))&::oneflow::cfg::DstSubsetTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::DstSubsetTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::DstSubsetTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::DstSubsetTickOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::DstSubsetTickOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::DstSubsetTickOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DstSubsetTickOpConf::*)())&::oneflow::cfg::DstSubsetTickOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::DstSubsetTickOpConf::*)() const)&::oneflow::cfg::DstSubsetTickOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::DstSubsetTickOpConf::*)(::std::size_t) const)&::oneflow::cfg::DstSubsetTickOpConf::in);
    registry.def("add_in", &::oneflow::cfg::DstSubsetTickOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::DstSubsetTickOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::DstSubsetTickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::DstSubsetTickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::DstSubsetTickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::DstSubsetTickOpConf::set_out);
  }
  {
    pybind11::class_<ConstSourceTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSourceTickOpConf>> registry(m, "ConstSourceTickOpConf");
    registry.def("__id__", &::oneflow::cfg::SourceTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSourceTickOpConf::DebugString);
    registry.def("__repr__", &ConstSourceTickOpConf::DebugString);

    registry.def("has_out", &ConstSourceTickOpConf::has_out);
    registry.def("out", &ConstSourceTickOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::SourceTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SourceTickOpConf>> registry(m, "SourceTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SourceTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SourceTickOpConf::*)(const ConstSourceTickOpConf&))&::oneflow::cfg::SourceTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SourceTickOpConf::*)(const ::oneflow::cfg::SourceTickOpConf&))&::oneflow::cfg::SourceTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SourceTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SourceTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SourceTickOpConf::DebugString);



    registry.def("has_out", &::oneflow::cfg::SourceTickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::SourceTickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::SourceTickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::SourceTickOpConf::set_out);
  }
  {
    pybind11::class_<ConstSinkTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSinkTickOpConf>> registry(m, "ConstSinkTickOpConf");
    registry.def("__id__", &::oneflow::cfg::SinkTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSinkTickOpConf::DebugString);
    registry.def("__repr__", &ConstSinkTickOpConf::DebugString);

    registry.def("tick_size", &ConstSinkTickOpConf::tick_size);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstSinkTickOpConf::*)() const)&ConstSinkTickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (ConstSinkTickOpConf::*)(::std::size_t) const)&ConstSinkTickOpConf::tick);

    registry.def("has_out", &ConstSinkTickOpConf::has_out);
    registry.def("out", &ConstSinkTickOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::SinkTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SinkTickOpConf>> registry(m, "SinkTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SinkTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SinkTickOpConf::*)(const ConstSinkTickOpConf&))&::oneflow::cfg::SinkTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SinkTickOpConf::*)(const ::oneflow::cfg::SinkTickOpConf&))&::oneflow::cfg::SinkTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SinkTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SinkTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SinkTickOpConf::DebugString);



    registry.def("tick_size", &::oneflow::cfg::SinkTickOpConf::tick_size);
    registry.def("clear_tick", &::oneflow::cfg::SinkTickOpConf::clear_tick);
    registry.def("mutable_tick", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::SinkTickOpConf::*)())&::oneflow::cfg::SinkTickOpConf::shared_mutable_tick);
    registry.def("tick", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::SinkTickOpConf::*)() const)&::oneflow::cfg::SinkTickOpConf::shared_const_tick);
    registry.def("tick", (const ::std::string& (::oneflow::cfg::SinkTickOpConf::*)(::std::size_t) const)&::oneflow::cfg::SinkTickOpConf::tick);
    registry.def("add_tick", &::oneflow::cfg::SinkTickOpConf::add_tick);
    registry.def("set_tick", &::oneflow::cfg::SinkTickOpConf::set_tick);

    registry.def("has_out", &::oneflow::cfg::SinkTickOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::SinkTickOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::SinkTickOpConf::out);
    registry.def("set_out", &::oneflow::cfg::SinkTickOpConf::set_out);
  }
  {
    pybind11::class_<ConstTotalLossInstanceNumOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstTotalLossInstanceNumOpConf>> registry(m, "ConstTotalLossInstanceNumOpConf");
    registry.def("__id__", &::oneflow::cfg::TotalLossInstanceNumOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstTotalLossInstanceNumOpConf::DebugString);
    registry.def("__repr__", &ConstTotalLossInstanceNumOpConf::DebugString);

    registry.def("in_size", &ConstTotalLossInstanceNumOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstTotalLossInstanceNumOpConf::*)() const)&ConstTotalLossInstanceNumOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstTotalLossInstanceNumOpConf::*)(::std::size_t) const)&ConstTotalLossInstanceNumOpConf::in);

    registry.def("has_out", &ConstTotalLossInstanceNumOpConf::has_out);
    registry.def("out", &ConstTotalLossInstanceNumOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::TotalLossInstanceNumOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::TotalLossInstanceNumOpConf>> registry(m, "TotalLossInstanceNumOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::TotalLossInstanceNumOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::TotalLossInstanceNumOpConf::*)(const ConstTotalLossInstanceNumOpConf&))&::oneflow::cfg::TotalLossInstanceNumOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::TotalLossInstanceNumOpConf::*)(const ::oneflow::cfg::TotalLossInstanceNumOpConf&))&::oneflow::cfg::TotalLossInstanceNumOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::TotalLossInstanceNumOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::TotalLossInstanceNumOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::TotalLossInstanceNumOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::TotalLossInstanceNumOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::TotalLossInstanceNumOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TotalLossInstanceNumOpConf::*)())&::oneflow::cfg::TotalLossInstanceNumOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::TotalLossInstanceNumOpConf::*)() const)&::oneflow::cfg::TotalLossInstanceNumOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::TotalLossInstanceNumOpConf::*)(::std::size_t) const)&::oneflow::cfg::TotalLossInstanceNumOpConf::in);
    registry.def("add_in", &::oneflow::cfg::TotalLossInstanceNumOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::TotalLossInstanceNumOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::TotalLossInstanceNumOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::TotalLossInstanceNumOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::TotalLossInstanceNumOpConf::out);
    registry.def("set_out", &::oneflow::cfg::TotalLossInstanceNumOpConf::set_out);
  }
  {
    pybind11::class_<ConstShapeElemCntAxisConf, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeElemCntAxisConf>> registry(m, "ConstShapeElemCntAxisConf");
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntAxisConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeElemCntAxisConf::DebugString);
    registry.def("__repr__", &ConstShapeElemCntAxisConf::DebugString);

    registry.def("axis_size", &ConstShapeElemCntAxisConf::axis_size);
    registry.def("axis", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (ConstShapeElemCntAxisConf::*)() const)&ConstShapeElemCntAxisConf::shared_const_axis);
    registry.def("axis", (const int32_t& (ConstShapeElemCntAxisConf::*)(::std::size_t) const)&ConstShapeElemCntAxisConf::axis);
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeElemCntAxisConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeElemCntAxisConf>> registry(m, "ShapeElemCntAxisConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeElemCntAxisConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntAxisConf::*)(const ConstShapeElemCntAxisConf&))&::oneflow::cfg::ShapeElemCntAxisConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntAxisConf::*)(const ::oneflow::cfg::ShapeElemCntAxisConf&))&::oneflow::cfg::ShapeElemCntAxisConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntAxisConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeElemCntAxisConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeElemCntAxisConf::DebugString);



    registry.def("axis_size", &::oneflow::cfg::ShapeElemCntAxisConf::axis_size);
    registry.def("clear_axis", &::oneflow::cfg::ShapeElemCntAxisConf::clear_axis);
    registry.def("mutable_axis", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::ShapeElemCntAxisConf::*)())&::oneflow::cfg::ShapeElemCntAxisConf::shared_mutable_axis);
    registry.def("axis", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> (::oneflow::cfg::ShapeElemCntAxisConf::*)() const)&::oneflow::cfg::ShapeElemCntAxisConf::shared_const_axis);
    registry.def("axis", (const int32_t& (::oneflow::cfg::ShapeElemCntAxisConf::*)(::std::size_t) const)&::oneflow::cfg::ShapeElemCntAxisConf::axis);
    registry.def("add_axis", &::oneflow::cfg::ShapeElemCntAxisConf::add_axis);
    registry.def("set_axis", &::oneflow::cfg::ShapeElemCntAxisConf::set_axis);
  }
  {
    pybind11::class_<ConstShapeElemCntRangeAxisConf, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeElemCntRangeAxisConf>> registry(m, "ConstShapeElemCntRangeAxisConf");
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntRangeAxisConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeElemCntRangeAxisConf::DebugString);
    registry.def("__repr__", &ConstShapeElemCntRangeAxisConf::DebugString);

    registry.def("has_begin_axis", &ConstShapeElemCntRangeAxisConf::has_begin_axis);
    registry.def("begin_axis", &ConstShapeElemCntRangeAxisConf::begin_axis);

    registry.def("has_end_axis", &ConstShapeElemCntRangeAxisConf::has_end_axis);
    registry.def("end_axis", &ConstShapeElemCntRangeAxisConf::end_axis);
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeElemCntRangeAxisConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeElemCntRangeAxisConf>> registry(m, "ShapeElemCntRangeAxisConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeElemCntRangeAxisConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntRangeAxisConf::*)(const ConstShapeElemCntRangeAxisConf&))&::oneflow::cfg::ShapeElemCntRangeAxisConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntRangeAxisConf::*)(const ::oneflow::cfg::ShapeElemCntRangeAxisConf&))&::oneflow::cfg::ShapeElemCntRangeAxisConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntRangeAxisConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeElemCntRangeAxisConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeElemCntRangeAxisConf::DebugString);



    registry.def("has_begin_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::has_begin_axis);
    registry.def("clear_begin_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::clear_begin_axis);
    registry.def("begin_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::begin_axis);
    registry.def("set_begin_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::set_begin_axis);

    registry.def("has_end_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::has_end_axis);
    registry.def("clear_end_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::clear_end_axis);
    registry.def("end_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::end_axis);
    registry.def("set_end_axis", &::oneflow::cfg::ShapeElemCntRangeAxisConf::set_end_axis);
  }
  {
    pybind11::class_<ConstShapeElemCntOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstShapeElemCntOpConf>> registry(m, "ConstShapeElemCntOpConf");
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstShapeElemCntOpConf::DebugString);
    registry.def("__repr__", &ConstShapeElemCntOpConf::DebugString);

    registry.def("has_x", &ConstShapeElemCntOpConf::has_x);
    registry.def("x", &ConstShapeElemCntOpConf::x);

    registry.def("has_y", &ConstShapeElemCntOpConf::has_y);
    registry.def("y", &ConstShapeElemCntOpConf::y);

    registry.def("has_data_type", &ConstShapeElemCntOpConf::has_data_type);
    registry.def("data_type", &ConstShapeElemCntOpConf::data_type);

    registry.def("has_exclude_axis_conf", &ConstShapeElemCntOpConf::has_exclude_axis_conf);
    registry.def("exclude_axis_conf", &ConstShapeElemCntOpConf::shared_const_exclude_axis_conf);

    registry.def("has_include_axis_conf", &ConstShapeElemCntOpConf::has_include_axis_conf);
    registry.def("include_axis_conf", &ConstShapeElemCntOpConf::shared_const_include_axis_conf);

    registry.def("has_range_axis_conf", &ConstShapeElemCntOpConf::has_range_axis_conf);
    registry.def("range_axis_conf", &ConstShapeElemCntOpConf::shared_const_range_axis_conf);
    registry.def("axis_conf_case",  &ConstShapeElemCntOpConf::axis_conf_case);
    registry.def_property_readonly_static("AXIS_CONF_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::AXIS_CONF_NOT_SET; })
        .def_property_readonly_static("kExcludeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kExcludeAxisConf; })
        .def_property_readonly_static("kIncludeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kIncludeAxisConf; })
        .def_property_readonly_static("kRangeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kRangeAxisConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::ShapeElemCntOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ShapeElemCntOpConf>> registry(m, "ShapeElemCntOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ShapeElemCntOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntOpConf::*)(const ConstShapeElemCntOpConf&))&::oneflow::cfg::ShapeElemCntOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ShapeElemCntOpConf::*)(const ::oneflow::cfg::ShapeElemCntOpConf&))&::oneflow::cfg::ShapeElemCntOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ShapeElemCntOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ShapeElemCntOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ShapeElemCntOpConf::DebugString);

    registry.def_property_readonly_static("AXIS_CONF_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::AXIS_CONF_NOT_SET; })
        .def_property_readonly_static("kExcludeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kExcludeAxisConf; })
        .def_property_readonly_static("kIncludeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kIncludeAxisConf; })
        .def_property_readonly_static("kRangeAxisConf", [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kRangeAxisConf; })
        ;


    registry.def("has_x", &::oneflow::cfg::ShapeElemCntOpConf::has_x);
    registry.def("clear_x", &::oneflow::cfg::ShapeElemCntOpConf::clear_x);
    registry.def("x", &::oneflow::cfg::ShapeElemCntOpConf::x);
    registry.def("set_x", &::oneflow::cfg::ShapeElemCntOpConf::set_x);

    registry.def("has_y", &::oneflow::cfg::ShapeElemCntOpConf::has_y);
    registry.def("clear_y", &::oneflow::cfg::ShapeElemCntOpConf::clear_y);
    registry.def("y", &::oneflow::cfg::ShapeElemCntOpConf::y);
    registry.def("set_y", &::oneflow::cfg::ShapeElemCntOpConf::set_y);

    registry.def("has_data_type", &::oneflow::cfg::ShapeElemCntOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::ShapeElemCntOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::ShapeElemCntOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::ShapeElemCntOpConf::set_data_type);

    registry.def("has_exclude_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::has_exclude_axis_conf);
    registry.def("clear_exclude_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::clear_exclude_axis_conf);
    registry.def_property_readonly_static("kExcludeAxisConf",
        [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kExcludeAxisConf; });
    registry.def("exclude_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::exclude_axis_conf);
    registry.def("mutable_exclude_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::shared_mutable_exclude_axis_conf);

    registry.def("has_include_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::has_include_axis_conf);
    registry.def("clear_include_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::clear_include_axis_conf);
    registry.def_property_readonly_static("kIncludeAxisConf",
        [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kIncludeAxisConf; });
    registry.def("include_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::include_axis_conf);
    registry.def("mutable_include_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::shared_mutable_include_axis_conf);

    registry.def("has_range_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::has_range_axis_conf);
    registry.def("clear_range_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::clear_range_axis_conf);
    registry.def_property_readonly_static("kRangeAxisConf",
        [](const pybind11::object&){ return ::oneflow::cfg::ShapeElemCntOpConf::kRangeAxisConf; });
    registry.def("range_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::range_axis_conf);
    registry.def("mutable_range_axis_conf", &::oneflow::cfg::ShapeElemCntOpConf::shared_mutable_range_axis_conf);
    pybind11::enum_<::oneflow::cfg::ShapeElemCntOpConf::AxisConfCase>(registry, "AxisConfCase")
        .value("AXIS_CONF_NOT_SET", ::oneflow::cfg::ShapeElemCntOpConf::AXIS_CONF_NOT_SET)
        .value("kExcludeAxisConf", ::oneflow::cfg::ShapeElemCntOpConf::kExcludeAxisConf)
        .value("kIncludeAxisConf", ::oneflow::cfg::ShapeElemCntOpConf::kIncludeAxisConf)
        .value("kRangeAxisConf", ::oneflow::cfg::ShapeElemCntOpConf::kRangeAxisConf)
        ;
    registry.def("axis_conf_case",  &::oneflow::cfg::ShapeElemCntOpConf::axis_conf_case);
  }
  {
    pybind11::class_<ConstAccTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstAccTickOpConf>> registry(m, "ConstAccTickOpConf");
    registry.def("__id__", &::oneflow::cfg::AccTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAccTickOpConf::DebugString);
    registry.def("__repr__", &ConstAccTickOpConf::DebugString);

    registry.def("has_one", &ConstAccTickOpConf::has_one);
    registry.def("one", &ConstAccTickOpConf::one);

    registry.def("has_acc", &ConstAccTickOpConf::has_acc);
    registry.def("acc", &ConstAccTickOpConf::acc);

    registry.def("has_max_acc_num", &ConstAccTickOpConf::has_max_acc_num);
    registry.def("max_acc_num", &ConstAccTickOpConf::max_acc_num);
  }
  {
    pybind11::class_<::oneflow::cfg::AccTickOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AccTickOpConf>> registry(m, "AccTickOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AccTickOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AccTickOpConf::*)(const ConstAccTickOpConf&))&::oneflow::cfg::AccTickOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AccTickOpConf::*)(const ::oneflow::cfg::AccTickOpConf&))&::oneflow::cfg::AccTickOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AccTickOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AccTickOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AccTickOpConf::DebugString);



    registry.def("has_one", &::oneflow::cfg::AccTickOpConf::has_one);
    registry.def("clear_one", &::oneflow::cfg::AccTickOpConf::clear_one);
    registry.def("one", &::oneflow::cfg::AccTickOpConf::one);
    registry.def("set_one", &::oneflow::cfg::AccTickOpConf::set_one);

    registry.def("has_acc", &::oneflow::cfg::AccTickOpConf::has_acc);
    registry.def("clear_acc", &::oneflow::cfg::AccTickOpConf::clear_acc);
    registry.def("acc", &::oneflow::cfg::AccTickOpConf::acc);
    registry.def("set_acc", &::oneflow::cfg::AccTickOpConf::set_acc);

    registry.def("has_max_acc_num", &::oneflow::cfg::AccTickOpConf::has_max_acc_num);
    registry.def("clear_max_acc_num", &::oneflow::cfg::AccTickOpConf::clear_max_acc_num);
    registry.def("max_acc_num", &::oneflow::cfg::AccTickOpConf::max_acc_num);
    registry.def("set_max_acc_num", &::oneflow::cfg::AccTickOpConf::set_max_acc_num);
  }
  {
    pybind11::class_<ConstModelInitOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelInitOpConf>> registry(m, "ConstModelInitOpConf");
    registry.def("__id__", &::oneflow::cfg::ModelInitOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelInitOpConf::DebugString);
    registry.def("__repr__", &ConstModelInitOpConf::DebugString);

    registry.def("has_tick", &ConstModelInitOpConf::has_tick);
    registry.def("tick", &ConstModelInitOpConf::tick);

    registry.def("out_size", &ConstModelInitOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelInitOpConf::*)() const)&ConstModelInitOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstModelInitOpConf::*)(::std::size_t) const)&ConstModelInitOpConf::out);

    registry.def("variable_op_name_size", &ConstModelInitOpConf::variable_op_name_size);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelInitOpConf::*)() const)&ConstModelInitOpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (ConstModelInitOpConf::*)(::std::size_t) const)&ConstModelInitOpConf::variable_op_name);

    registry.def("original_variable_conf_size", &ConstModelInitOpConf::original_variable_conf_size);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (ConstModelInitOpConf::*)() const)&ConstModelInitOpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (ConstModelInitOpConf::*)(::std::size_t) const)&ConstModelInitOpConf::shared_const_original_variable_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelInitOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelInitOpConf>> registry(m, "ModelInitOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelInitOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelInitOpConf::*)(const ConstModelInitOpConf&))&::oneflow::cfg::ModelInitOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelInitOpConf::*)(const ::oneflow::cfg::ModelInitOpConf&))&::oneflow::cfg::ModelInitOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelInitOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelInitOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelInitOpConf::DebugString);



    registry.def("has_tick", &::oneflow::cfg::ModelInitOpConf::has_tick);
    registry.def("clear_tick", &::oneflow::cfg::ModelInitOpConf::clear_tick);
    registry.def("tick", &::oneflow::cfg::ModelInitOpConf::tick);
    registry.def("set_tick", &::oneflow::cfg::ModelInitOpConf::set_tick);

    registry.def("out_size", &::oneflow::cfg::ModelInitOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::ModelInitOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitOpConf::*)())&::oneflow::cfg::ModelInitOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitOpConf::*)() const)&::oneflow::cfg::ModelInitOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::ModelInitOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitOpConf::out);
    registry.def("add_out", &::oneflow::cfg::ModelInitOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::ModelInitOpConf::set_out);

    registry.def("variable_op_name_size", &::oneflow::cfg::ModelInitOpConf::variable_op_name_size);
    registry.def("clear_variable_op_name", &::oneflow::cfg::ModelInitOpConf::clear_variable_op_name);
    registry.def("mutable_variable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitOpConf::*)())&::oneflow::cfg::ModelInitOpConf::shared_mutable_variable_op_name);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitOpConf::*)() const)&::oneflow::cfg::ModelInitOpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (::oneflow::cfg::ModelInitOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitOpConf::variable_op_name);
    registry.def("add_variable_op_name", &::oneflow::cfg::ModelInitOpConf::add_variable_op_name);
    registry.def("set_variable_op_name", &::oneflow::cfg::ModelInitOpConf::set_variable_op_name);

    registry.def("original_variable_conf_size", &::oneflow::cfg::ModelInitOpConf::original_variable_conf_size);
    registry.def("clear_original_variable_conf", &::oneflow::cfg::ModelInitOpConf::clear_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelInitOpConf::*)())&::oneflow::cfg::ModelInitOpConf::shared_mutable_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelInitOpConf::*)() const)&::oneflow::cfg::ModelInitOpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (::oneflow::cfg::ModelInitOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitOpConf::shared_const_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (::oneflow::cfg::ModelInitOpConf::*)(::std::size_t))&::oneflow::cfg::ModelInitOpConf::shared_mutable_original_variable_conf);
  }
  {
    pybind11::class_<ConstModelLoadOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelLoadOpConf>> registry(m, "ConstModelLoadOpConf");
    registry.def("__id__", &::oneflow::cfg::ModelLoadOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelLoadOpConf::DebugString);
    registry.def("__repr__", &ConstModelLoadOpConf::DebugString);

    registry.def("has_path", &ConstModelLoadOpConf::has_path);
    registry.def("path", &ConstModelLoadOpConf::path);

    registry.def("out_size", &ConstModelLoadOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelLoadOpConf::*)() const)&ConstModelLoadOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstModelLoadOpConf::*)(::std::size_t) const)&ConstModelLoadOpConf::out);

    registry.def("variable_op_name_size", &ConstModelLoadOpConf::variable_op_name_size);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelLoadOpConf::*)() const)&ConstModelLoadOpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (ConstModelLoadOpConf::*)(::std::size_t) const)&ConstModelLoadOpConf::variable_op_name);

    registry.def("original_variable_conf_size", &ConstModelLoadOpConf::original_variable_conf_size);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (ConstModelLoadOpConf::*)() const)&ConstModelLoadOpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (ConstModelLoadOpConf::*)(::std::size_t) const)&ConstModelLoadOpConf::shared_const_original_variable_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelLoadOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelLoadOpConf>> registry(m, "ModelLoadOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelLoadOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelLoadOpConf::*)(const ConstModelLoadOpConf&))&::oneflow::cfg::ModelLoadOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelLoadOpConf::*)(const ::oneflow::cfg::ModelLoadOpConf&))&::oneflow::cfg::ModelLoadOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelLoadOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelLoadOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelLoadOpConf::DebugString);



    registry.def("has_path", &::oneflow::cfg::ModelLoadOpConf::has_path);
    registry.def("clear_path", &::oneflow::cfg::ModelLoadOpConf::clear_path);
    registry.def("path", &::oneflow::cfg::ModelLoadOpConf::path);
    registry.def("set_path", &::oneflow::cfg::ModelLoadOpConf::set_path);

    registry.def("out_size", &::oneflow::cfg::ModelLoadOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::ModelLoadOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadOpConf::*)())&::oneflow::cfg::ModelLoadOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadOpConf::*)() const)&::oneflow::cfg::ModelLoadOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::ModelLoadOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadOpConf::out);
    registry.def("add_out", &::oneflow::cfg::ModelLoadOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::ModelLoadOpConf::set_out);

    registry.def("variable_op_name_size", &::oneflow::cfg::ModelLoadOpConf::variable_op_name_size);
    registry.def("clear_variable_op_name", &::oneflow::cfg::ModelLoadOpConf::clear_variable_op_name);
    registry.def("mutable_variable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadOpConf::*)())&::oneflow::cfg::ModelLoadOpConf::shared_mutable_variable_op_name);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadOpConf::*)() const)&::oneflow::cfg::ModelLoadOpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (::oneflow::cfg::ModelLoadOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadOpConf::variable_op_name);
    registry.def("add_variable_op_name", &::oneflow::cfg::ModelLoadOpConf::add_variable_op_name);
    registry.def("set_variable_op_name", &::oneflow::cfg::ModelLoadOpConf::set_variable_op_name);

    registry.def("original_variable_conf_size", &::oneflow::cfg::ModelLoadOpConf::original_variable_conf_size);
    registry.def("clear_original_variable_conf", &::oneflow::cfg::ModelLoadOpConf::clear_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelLoadOpConf::*)())&::oneflow::cfg::ModelLoadOpConf::shared_mutable_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelLoadOpConf::*)() const)&::oneflow::cfg::ModelLoadOpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (::oneflow::cfg::ModelLoadOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadOpConf::shared_const_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (::oneflow::cfg::ModelLoadOpConf::*)(::std::size_t))&::oneflow::cfg::ModelLoadOpConf::shared_mutable_original_variable_conf);
  }
  {
    pybind11::class_<ConstIdentityOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstIdentityOpConf>> registry(m, "ConstIdentityOpConf");
    registry.def("__id__", &::oneflow::cfg::IdentityOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstIdentityOpConf::DebugString);
    registry.def("__repr__", &ConstIdentityOpConf::DebugString);

    registry.def("has_in", &ConstIdentityOpConf::has_in);
    registry.def("in", &ConstIdentityOpConf::in);

    registry.def("has_out", &ConstIdentityOpConf::has_out);
    registry.def("out", &ConstIdentityOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::IdentityOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::IdentityOpConf>> registry(m, "IdentityOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::IdentityOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::IdentityOpConf::*)(const ConstIdentityOpConf&))&::oneflow::cfg::IdentityOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::IdentityOpConf::*)(const ::oneflow::cfg::IdentityOpConf&))&::oneflow::cfg::IdentityOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::IdentityOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::IdentityOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::IdentityOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::IdentityOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::IdentityOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::IdentityOpConf::in);
    registry.def("set_in", &::oneflow::cfg::IdentityOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::IdentityOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::IdentityOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::IdentityOpConf::out);
    registry.def("set_out", &::oneflow::cfg::IdentityOpConf::set_out);
  }
  {
    pybind11::class_<ConstCopyOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCopyOpConf>> registry(m, "ConstCopyOpConf");
    registry.def("__id__", &::oneflow::cfg::CopyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCopyOpConf::DebugString);
    registry.def("__repr__", &ConstCopyOpConf::DebugString);

    registry.def("has_in", &ConstCopyOpConf::has_in);
    registry.def("in", &ConstCopyOpConf::in);

    registry.def("has_out", &ConstCopyOpConf::has_out);
    registry.def("out", &ConstCopyOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::CopyOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CopyOpConf>> registry(m, "CopyOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CopyOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyOpConf::*)(const ConstCopyOpConf&))&::oneflow::cfg::CopyOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CopyOpConf::*)(const ::oneflow::cfg::CopyOpConf&))&::oneflow::cfg::CopyOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CopyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CopyOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CopyOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::CopyOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::CopyOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::CopyOpConf::in);
    registry.def("set_in", &::oneflow::cfg::CopyOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::CopyOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::CopyOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::CopyOpConf::out);
    registry.def("set_out", &::oneflow::cfg::CopyOpConf::set_out);
  }
  {
    pybind11::class_<ConstCastToMirroredOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCastToMirroredOpConf>> registry(m, "ConstCastToMirroredOpConf");
    registry.def("__id__", &::oneflow::cfg::CastToMirroredOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCastToMirroredOpConf::DebugString);
    registry.def("__repr__", &ConstCastToMirroredOpConf::DebugString);

    registry.def("has_in", &ConstCastToMirroredOpConf::has_in);
    registry.def("in", &ConstCastToMirroredOpConf::in);

    registry.def("has_out", &ConstCastToMirroredOpConf::has_out);
    registry.def("out", &ConstCastToMirroredOpConf::out);

    registry.def("has_sbp_parallel", &ConstCastToMirroredOpConf::has_sbp_parallel);
    registry.def("sbp_parallel", &ConstCastToMirroredOpConf::shared_const_sbp_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::CastToMirroredOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CastToMirroredOpConf>> registry(m, "CastToMirroredOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CastToMirroredOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CastToMirroredOpConf::*)(const ConstCastToMirroredOpConf&))&::oneflow::cfg::CastToMirroredOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CastToMirroredOpConf::*)(const ::oneflow::cfg::CastToMirroredOpConf&))&::oneflow::cfg::CastToMirroredOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CastToMirroredOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CastToMirroredOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CastToMirroredOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::CastToMirroredOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::CastToMirroredOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::CastToMirroredOpConf::in);
    registry.def("set_in", &::oneflow::cfg::CastToMirroredOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::CastToMirroredOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::CastToMirroredOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::CastToMirroredOpConf::out);
    registry.def("set_out", &::oneflow::cfg::CastToMirroredOpConf::set_out);

    registry.def("has_sbp_parallel", &::oneflow::cfg::CastToMirroredOpConf::has_sbp_parallel);
    registry.def("clear_sbp_parallel", &::oneflow::cfg::CastToMirroredOpConf::clear_sbp_parallel);
    registry.def("sbp_parallel", &::oneflow::cfg::CastToMirroredOpConf::shared_const_sbp_parallel);
    registry.def("mutable_sbp_parallel", &::oneflow::cfg::CastToMirroredOpConf::shared_mutable_sbp_parallel);
  }
  {
    pybind11::class_<ConstCastFromMirroredOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCastFromMirroredOpConf>> registry(m, "ConstCastFromMirroredOpConf");
    registry.def("__id__", &::oneflow::cfg::CastFromMirroredOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCastFromMirroredOpConf::DebugString);
    registry.def("__repr__", &ConstCastFromMirroredOpConf::DebugString);

    registry.def("has_in", &ConstCastFromMirroredOpConf::has_in);
    registry.def("in", &ConstCastFromMirroredOpConf::in);

    registry.def("has_out", &ConstCastFromMirroredOpConf::has_out);
    registry.def("out", &ConstCastFromMirroredOpConf::out);

    registry.def("has_sbp_parallel", &ConstCastFromMirroredOpConf::has_sbp_parallel);
    registry.def("sbp_parallel", &ConstCastFromMirroredOpConf::shared_const_sbp_parallel);
  }
  {
    pybind11::class_<::oneflow::cfg::CastFromMirroredOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CastFromMirroredOpConf>> registry(m, "CastFromMirroredOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CastFromMirroredOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CastFromMirroredOpConf::*)(const ConstCastFromMirroredOpConf&))&::oneflow::cfg::CastFromMirroredOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CastFromMirroredOpConf::*)(const ::oneflow::cfg::CastFromMirroredOpConf&))&::oneflow::cfg::CastFromMirroredOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CastFromMirroredOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CastFromMirroredOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CastFromMirroredOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::CastFromMirroredOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::CastFromMirroredOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::CastFromMirroredOpConf::in);
    registry.def("set_in", &::oneflow::cfg::CastFromMirroredOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::CastFromMirroredOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::CastFromMirroredOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::CastFromMirroredOpConf::out);
    registry.def("set_out", &::oneflow::cfg::CastFromMirroredOpConf::set_out);

    registry.def("has_sbp_parallel", &::oneflow::cfg::CastFromMirroredOpConf::has_sbp_parallel);
    registry.def("clear_sbp_parallel", &::oneflow::cfg::CastFromMirroredOpConf::clear_sbp_parallel);
    registry.def("sbp_parallel", &::oneflow::cfg::CastFromMirroredOpConf::shared_const_sbp_parallel);
    registry.def("mutable_sbp_parallel", &::oneflow::cfg::CastFromMirroredOpConf::shared_mutable_sbp_parallel);
  }
  {
    pybind11::class_<ConstCaseOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCaseOpConf>> registry(m, "ConstCaseOpConf");
    registry.def("__id__", &::oneflow::cfg::CaseOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCaseOpConf::DebugString);
    registry.def("__repr__", &ConstCaseOpConf::DebugString);

    registry.def("has_in", &ConstCaseOpConf::has_in);
    registry.def("in", &ConstCaseOpConf::in);

    registry.def("out_size", &ConstCaseOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstCaseOpConf::*)() const)&ConstCaseOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstCaseOpConf::*)(::std::size_t) const)&ConstCaseOpConf::out);
  }
  {
    pybind11::class_<::oneflow::cfg::CaseOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CaseOpConf>> registry(m, "CaseOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CaseOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CaseOpConf::*)(const ConstCaseOpConf&))&::oneflow::cfg::CaseOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CaseOpConf::*)(const ::oneflow::cfg::CaseOpConf&))&::oneflow::cfg::CaseOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CaseOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CaseOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CaseOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::CaseOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::CaseOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::CaseOpConf::in);
    registry.def("set_in", &::oneflow::cfg::CaseOpConf::set_in);

    registry.def("out_size", &::oneflow::cfg::CaseOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::CaseOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::CaseOpConf::*)())&::oneflow::cfg::CaseOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::CaseOpConf::*)() const)&::oneflow::cfg::CaseOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::CaseOpConf::*)(::std::size_t) const)&::oneflow::cfg::CaseOpConf::out);
    registry.def("add_out", &::oneflow::cfg::CaseOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::CaseOpConf::set_out);
  }
  {
    pybind11::class_<ConstEsacOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstEsacOpConf>> registry(m, "ConstEsacOpConf");
    registry.def("__id__", &::oneflow::cfg::EsacOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstEsacOpConf::DebugString);
    registry.def("__repr__", &ConstEsacOpConf::DebugString);

    registry.def("in_size", &ConstEsacOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstEsacOpConf::*)() const)&ConstEsacOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstEsacOpConf::*)(::std::size_t) const)&ConstEsacOpConf::in);

    registry.def("has_out", &ConstEsacOpConf::has_out);
    registry.def("out", &ConstEsacOpConf::out);

    registry.def("has_data_type", &ConstEsacOpConf::has_data_type);
    registry.def("data_type", &ConstEsacOpConf::data_type);
  }
  {
    pybind11::class_<::oneflow::cfg::EsacOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::EsacOpConf>> registry(m, "EsacOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::EsacOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::EsacOpConf::*)(const ConstEsacOpConf&))&::oneflow::cfg::EsacOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::EsacOpConf::*)(const ::oneflow::cfg::EsacOpConf&))&::oneflow::cfg::EsacOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::EsacOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::EsacOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::EsacOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::EsacOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::EsacOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::EsacOpConf::*)())&::oneflow::cfg::EsacOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::EsacOpConf::*)() const)&::oneflow::cfg::EsacOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::EsacOpConf::*)(::std::size_t) const)&::oneflow::cfg::EsacOpConf::in);
    registry.def("add_in", &::oneflow::cfg::EsacOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::EsacOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::EsacOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::EsacOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::EsacOpConf::out);
    registry.def("set_out", &::oneflow::cfg::EsacOpConf::set_out);

    registry.def("has_data_type", &::oneflow::cfg::EsacOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::EsacOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::EsacOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::EsacOpConf::set_data_type);
  }
  {
    pybind11::class_<ConstAssignOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstAssignOpConf>> registry(m, "ConstAssignOpConf");
    registry.def("__id__", &::oneflow::cfg::AssignOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstAssignOpConf::DebugString);
    registry.def("__repr__", &ConstAssignOpConf::DebugString);

    registry.def("has_ref", &ConstAssignOpConf::has_ref);
    registry.def("ref", &ConstAssignOpConf::ref);

    registry.def("has_value", &ConstAssignOpConf::has_value);
    registry.def("value", &ConstAssignOpConf::value);
  }
  {
    pybind11::class_<::oneflow::cfg::AssignOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::AssignOpConf>> registry(m, "AssignOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::AssignOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::AssignOpConf::*)(const ConstAssignOpConf&))&::oneflow::cfg::AssignOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::AssignOpConf::*)(const ::oneflow::cfg::AssignOpConf&))&::oneflow::cfg::AssignOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::AssignOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::AssignOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::AssignOpConf::DebugString);



    registry.def("has_ref", &::oneflow::cfg::AssignOpConf::has_ref);
    registry.def("clear_ref", &::oneflow::cfg::AssignOpConf::clear_ref);
    registry.def("ref", &::oneflow::cfg::AssignOpConf::ref);
    registry.def("set_ref", &::oneflow::cfg::AssignOpConf::set_ref);

    registry.def("has_value", &::oneflow::cfg::AssignOpConf::has_value);
    registry.def("clear_value", &::oneflow::cfg::AssignOpConf::clear_value);
    registry.def("value", &::oneflow::cfg::AssignOpConf::value);
    registry.def("set_value", &::oneflow::cfg::AssignOpConf::set_value);
  }
  {
    pybind11::class_<ConstModelSaveOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelSaveOpConf>> registry(m, "ConstModelSaveOpConf");
    registry.def("__id__", &::oneflow::cfg::ModelSaveOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelSaveOpConf::DebugString);
    registry.def("__repr__", &ConstModelSaveOpConf::DebugString);

    registry.def("has_path", &ConstModelSaveOpConf::has_path);
    registry.def("path", &ConstModelSaveOpConf::path);

    registry.def("in_size", &ConstModelSaveOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelSaveOpConf::*)() const)&ConstModelSaveOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstModelSaveOpConf::*)(::std::size_t) const)&ConstModelSaveOpConf::in);

    registry.def("key_size", &ConstModelSaveOpConf::key_size);
    registry.def("key", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelSaveOpConf::*)() const)&ConstModelSaveOpConf::shared_const_key);
    registry.def("key", (const ::std::string& (ConstModelSaveOpConf::*)(::std::size_t) const)&ConstModelSaveOpConf::key);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelSaveOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelSaveOpConf>> registry(m, "ModelSaveOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelSaveOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelSaveOpConf::*)(const ConstModelSaveOpConf&))&::oneflow::cfg::ModelSaveOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelSaveOpConf::*)(const ::oneflow::cfg::ModelSaveOpConf&))&::oneflow::cfg::ModelSaveOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelSaveOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelSaveOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelSaveOpConf::DebugString);



    registry.def("has_path", &::oneflow::cfg::ModelSaveOpConf::has_path);
    registry.def("clear_path", &::oneflow::cfg::ModelSaveOpConf::clear_path);
    registry.def("path", &::oneflow::cfg::ModelSaveOpConf::path);
    registry.def("set_path", &::oneflow::cfg::ModelSaveOpConf::set_path);

    registry.def("in_size", &::oneflow::cfg::ModelSaveOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::ModelSaveOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveOpConf::*)())&::oneflow::cfg::ModelSaveOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveOpConf::*)() const)&::oneflow::cfg::ModelSaveOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::ModelSaveOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelSaveOpConf::in);
    registry.def("add_in", &::oneflow::cfg::ModelSaveOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::ModelSaveOpConf::set_in);

    registry.def("key_size", &::oneflow::cfg::ModelSaveOpConf::key_size);
    registry.def("clear_key", &::oneflow::cfg::ModelSaveOpConf::clear_key);
    registry.def("mutable_key", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveOpConf::*)())&::oneflow::cfg::ModelSaveOpConf::shared_mutable_key);
    registry.def("key", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveOpConf::*)() const)&::oneflow::cfg::ModelSaveOpConf::shared_const_key);
    registry.def("key", (const ::std::string& (::oneflow::cfg::ModelSaveOpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelSaveOpConf::key);
    registry.def("add_key", &::oneflow::cfg::ModelSaveOpConf::add_key);
    registry.def("set_key", &::oneflow::cfg::ModelSaveOpConf::set_key);
  }
  {
    pybind11::class_<ConstLearningRateScheduleOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstLearningRateScheduleOpConf>> registry(m, "ConstLearningRateScheduleOpConf");
    registry.def("__id__", &::oneflow::cfg::LearningRateScheduleOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstLearningRateScheduleOpConf::DebugString);
    registry.def("__repr__", &ConstLearningRateScheduleOpConf::DebugString);

    registry.def("has_train_step", &ConstLearningRateScheduleOpConf::has_train_step);
    registry.def("train_step", &ConstLearningRateScheduleOpConf::train_step);

    registry.def("has_out", &ConstLearningRateScheduleOpConf::has_out);
    registry.def("out", &ConstLearningRateScheduleOpConf::out);

    registry.def("has_learning_rate", &ConstLearningRateScheduleOpConf::has_learning_rate);
    registry.def("learning_rate", &ConstLearningRateScheduleOpConf::learning_rate);

    registry.def("has_learning_rate_decay", &ConstLearningRateScheduleOpConf::has_learning_rate_decay);
    registry.def("learning_rate_decay", &ConstLearningRateScheduleOpConf::shared_const_learning_rate_decay);

    registry.def("has_warmup_conf", &ConstLearningRateScheduleOpConf::has_warmup_conf);
    registry.def("warmup_conf", &ConstLearningRateScheduleOpConf::shared_const_warmup_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::LearningRateScheduleOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::LearningRateScheduleOpConf>> registry(m, "LearningRateScheduleOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::LearningRateScheduleOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::LearningRateScheduleOpConf::*)(const ConstLearningRateScheduleOpConf&))&::oneflow::cfg::LearningRateScheduleOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::LearningRateScheduleOpConf::*)(const ::oneflow::cfg::LearningRateScheduleOpConf&))&::oneflow::cfg::LearningRateScheduleOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::LearningRateScheduleOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::LearningRateScheduleOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::LearningRateScheduleOpConf::DebugString);



    registry.def("has_train_step", &::oneflow::cfg::LearningRateScheduleOpConf::has_train_step);
    registry.def("clear_train_step", &::oneflow::cfg::LearningRateScheduleOpConf::clear_train_step);
    registry.def("train_step", &::oneflow::cfg::LearningRateScheduleOpConf::train_step);
    registry.def("set_train_step", &::oneflow::cfg::LearningRateScheduleOpConf::set_train_step);

    registry.def("has_out", &::oneflow::cfg::LearningRateScheduleOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::LearningRateScheduleOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::LearningRateScheduleOpConf::out);
    registry.def("set_out", &::oneflow::cfg::LearningRateScheduleOpConf::set_out);

    registry.def("has_learning_rate", &::oneflow::cfg::LearningRateScheduleOpConf::has_learning_rate);
    registry.def("clear_learning_rate", &::oneflow::cfg::LearningRateScheduleOpConf::clear_learning_rate);
    registry.def("learning_rate", &::oneflow::cfg::LearningRateScheduleOpConf::learning_rate);
    registry.def("set_learning_rate", &::oneflow::cfg::LearningRateScheduleOpConf::set_learning_rate);

    registry.def("has_learning_rate_decay", &::oneflow::cfg::LearningRateScheduleOpConf::has_learning_rate_decay);
    registry.def("clear_learning_rate_decay", &::oneflow::cfg::LearningRateScheduleOpConf::clear_learning_rate_decay);
    registry.def("learning_rate_decay", &::oneflow::cfg::LearningRateScheduleOpConf::shared_const_learning_rate_decay);
    registry.def("mutable_learning_rate_decay", &::oneflow::cfg::LearningRateScheduleOpConf::shared_mutable_learning_rate_decay);

    registry.def("has_warmup_conf", &::oneflow::cfg::LearningRateScheduleOpConf::has_warmup_conf);
    registry.def("clear_warmup_conf", &::oneflow::cfg::LearningRateScheduleOpConf::clear_warmup_conf);
    registry.def("warmup_conf", &::oneflow::cfg::LearningRateScheduleOpConf::shared_const_warmup_conf);
    registry.def("mutable_warmup_conf", &::oneflow::cfg::LearningRateScheduleOpConf::shared_mutable_warmup_conf);
  }
  {
    pybind11::class_<ConstSliceBoxingConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSliceBoxingConf>> registry(m, "ConstSliceBoxingConf");
    registry.def("__id__", &::oneflow::cfg::SliceBoxingConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSliceBoxingConf::DebugString);
    registry.def("__repr__", &ConstSliceBoxingConf::DebugString);

    registry.def("has_lbi", &ConstSliceBoxingConf::has_lbi);
    registry.def("lbi", &ConstSliceBoxingConf::shared_const_lbi);

    registry.def("in_slice_size", &ConstSliceBoxingConf::in_slice_size);
    registry.def("in_slice", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> (ConstSliceBoxingConf::*)() const)&ConstSliceBoxingConf::shared_const_in_slice);
    registry.def("in_slice", (::std::shared_ptr<ConstTensorSliceViewProto> (ConstSliceBoxingConf::*)(::std::size_t) const)&ConstSliceBoxingConf::shared_const_in_slice);

    registry.def("has_out_slice", &ConstSliceBoxingConf::has_out_slice);
    registry.def("out_slice", &ConstSliceBoxingConf::shared_const_out_slice);

    registry.def("has_out_shape", &ConstSliceBoxingConf::has_out_shape);
    registry.def("out_shape", &ConstSliceBoxingConf::shared_const_out_shape);
  }
  {
    pybind11::class_<::oneflow::cfg::SliceBoxingConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SliceBoxingConf>> registry(m, "SliceBoxingConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SliceBoxingConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingConf::*)(const ConstSliceBoxingConf&))&::oneflow::cfg::SliceBoxingConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingConf::*)(const ::oneflow::cfg::SliceBoxingConf&))&::oneflow::cfg::SliceBoxingConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SliceBoxingConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SliceBoxingConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SliceBoxingConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::SliceBoxingConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::SliceBoxingConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::SliceBoxingConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::SliceBoxingConf::shared_mutable_lbi);

    registry.def("in_slice_size", &::oneflow::cfg::SliceBoxingConf::in_slice_size);
    registry.def("clear_in_slice", &::oneflow::cfg::SliceBoxingConf::clear_in_slice);
    registry.def("mutable_in_slice", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> (::oneflow::cfg::SliceBoxingConf::*)())&::oneflow::cfg::SliceBoxingConf::shared_mutable_in_slice);
    registry.def("in_slice", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> (::oneflow::cfg::SliceBoxingConf::*)() const)&::oneflow::cfg::SliceBoxingConf::shared_const_in_slice);
    registry.def("in_slice", (::std::shared_ptr<ConstTensorSliceViewProto> (::oneflow::cfg::SliceBoxingConf::*)(::std::size_t) const)&::oneflow::cfg::SliceBoxingConf::shared_const_in_slice);
    registry.def("mutable_in_slice", (::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> (::oneflow::cfg::SliceBoxingConf::*)(::std::size_t))&::oneflow::cfg::SliceBoxingConf::shared_mutable_in_slice);

    registry.def("has_out_slice", &::oneflow::cfg::SliceBoxingConf::has_out_slice);
    registry.def("clear_out_slice", &::oneflow::cfg::SliceBoxingConf::clear_out_slice);
    registry.def("out_slice", &::oneflow::cfg::SliceBoxingConf::shared_const_out_slice);
    registry.def("mutable_out_slice", &::oneflow::cfg::SliceBoxingConf::shared_mutable_out_slice);

    registry.def("has_out_shape", &::oneflow::cfg::SliceBoxingConf::has_out_shape);
    registry.def("clear_out_shape", &::oneflow::cfg::SliceBoxingConf::clear_out_shape);
    registry.def("out_shape", &::oneflow::cfg::SliceBoxingConf::shared_const_out_shape);
    registry.def("mutable_out_shape", &::oneflow::cfg::SliceBoxingConf::shared_mutable_out_shape);
  }
  {
    pybind11::class_<ConstSliceBoxingCopyOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSliceBoxingCopyOpConf>> registry(m, "ConstSliceBoxingCopyOpConf");
    registry.def("__id__", &::oneflow::cfg::SliceBoxingCopyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSliceBoxingCopyOpConf::DebugString);
    registry.def("__repr__", &ConstSliceBoxingCopyOpConf::DebugString);

    registry.def("has_slice_boxing_conf", &ConstSliceBoxingCopyOpConf::has_slice_boxing_conf);
    registry.def("slice_boxing_conf", &ConstSliceBoxingCopyOpConf::shared_const_slice_boxing_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::SliceBoxingCopyOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SliceBoxingCopyOpConf>> registry(m, "SliceBoxingCopyOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SliceBoxingCopyOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingCopyOpConf::*)(const ConstSliceBoxingCopyOpConf&))&::oneflow::cfg::SliceBoxingCopyOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingCopyOpConf::*)(const ::oneflow::cfg::SliceBoxingCopyOpConf&))&::oneflow::cfg::SliceBoxingCopyOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SliceBoxingCopyOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SliceBoxingCopyOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SliceBoxingCopyOpConf::DebugString);



    registry.def("has_slice_boxing_conf", &::oneflow::cfg::SliceBoxingCopyOpConf::has_slice_boxing_conf);
    registry.def("clear_slice_boxing_conf", &::oneflow::cfg::SliceBoxingCopyOpConf::clear_slice_boxing_conf);
    registry.def("slice_boxing_conf", &::oneflow::cfg::SliceBoxingCopyOpConf::shared_const_slice_boxing_conf);
    registry.def("mutable_slice_boxing_conf", &::oneflow::cfg::SliceBoxingCopyOpConf::shared_mutable_slice_boxing_conf);
  }
  {
    pybind11::class_<ConstSliceBoxingAddOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSliceBoxingAddOpConf>> registry(m, "ConstSliceBoxingAddOpConf");
    registry.def("__id__", &::oneflow::cfg::SliceBoxingAddOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSliceBoxingAddOpConf::DebugString);
    registry.def("__repr__", &ConstSliceBoxingAddOpConf::DebugString);

    registry.def("has_slice_boxing_conf", &ConstSliceBoxingAddOpConf::has_slice_boxing_conf);
    registry.def("slice_boxing_conf", &ConstSliceBoxingAddOpConf::shared_const_slice_boxing_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::SliceBoxingAddOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SliceBoxingAddOpConf>> registry(m, "SliceBoxingAddOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SliceBoxingAddOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingAddOpConf::*)(const ConstSliceBoxingAddOpConf&))&::oneflow::cfg::SliceBoxingAddOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SliceBoxingAddOpConf::*)(const ::oneflow::cfg::SliceBoxingAddOpConf&))&::oneflow::cfg::SliceBoxingAddOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SliceBoxingAddOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SliceBoxingAddOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SliceBoxingAddOpConf::DebugString);



    registry.def("has_slice_boxing_conf", &::oneflow::cfg::SliceBoxingAddOpConf::has_slice_boxing_conf);
    registry.def("clear_slice_boxing_conf", &::oneflow::cfg::SliceBoxingAddOpConf::clear_slice_boxing_conf);
    registry.def("slice_boxing_conf", &::oneflow::cfg::SliceBoxingAddOpConf::shared_const_slice_boxing_conf);
    registry.def("mutable_slice_boxing_conf", &::oneflow::cfg::SliceBoxingAddOpConf::shared_mutable_slice_boxing_conf);
  }
  {
    pybind11::class_<ConstXrtLaunchOpConf_Argument, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtLaunchOpConf_Argument>> registry(m, "ConstXrtLaunchOpConf_Argument");
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf_Argument::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtLaunchOpConf_Argument::DebugString);
    registry.def("__repr__", &ConstXrtLaunchOpConf_Argument::DebugString);

    registry.def("has_name", &ConstXrtLaunchOpConf_Argument::has_name);
    registry.def("name", &ConstXrtLaunchOpConf_Argument::name);

    registry.def("has_value", &ConstXrtLaunchOpConf_Argument::has_value);
    registry.def("value", &ConstXrtLaunchOpConf_Argument::value);

    registry.def("has_device_type", &ConstXrtLaunchOpConf_Argument::has_device_type);
    registry.def("device_type", &ConstXrtLaunchOpConf_Argument::device_type);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtLaunchOpConf_Argument, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument>> registry(m, "XrtLaunchOpConf_Argument");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtLaunchOpConf_Argument::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf_Argument::*)(const ConstXrtLaunchOpConf_Argument&))&::oneflow::cfg::XrtLaunchOpConf_Argument::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf_Argument::*)(const ::oneflow::cfg::XrtLaunchOpConf_Argument&))&::oneflow::cfg::XrtLaunchOpConf_Argument::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf_Argument::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtLaunchOpConf_Argument::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtLaunchOpConf_Argument::DebugString);



    registry.def("has_name", &::oneflow::cfg::XrtLaunchOpConf_Argument::has_name);
    registry.def("clear_name", &::oneflow::cfg::XrtLaunchOpConf_Argument::clear_name);
    registry.def("name", &::oneflow::cfg::XrtLaunchOpConf_Argument::name);
    registry.def("set_name", &::oneflow::cfg::XrtLaunchOpConf_Argument::set_name);

    registry.def("has_value", &::oneflow::cfg::XrtLaunchOpConf_Argument::has_value);
    registry.def("clear_value", &::oneflow::cfg::XrtLaunchOpConf_Argument::clear_value);
    registry.def("value", &::oneflow::cfg::XrtLaunchOpConf_Argument::value);
    registry.def("set_value", &::oneflow::cfg::XrtLaunchOpConf_Argument::set_value);

    registry.def("has_device_type", &::oneflow::cfg::XrtLaunchOpConf_Argument::has_device_type);
    registry.def("clear_device_type", &::oneflow::cfg::XrtLaunchOpConf_Argument::clear_device_type);
    registry.def("device_type", &::oneflow::cfg::XrtLaunchOpConf_Argument::device_type);
    registry.def("set_device_type", &::oneflow::cfg::XrtLaunchOpConf_Argument::set_device_type);
  }
  {
    pybind11::class_<ConstXrtLaunchOpConf_Function, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtLaunchOpConf_Function>> registry(m, "ConstXrtLaunchOpConf_Function");
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf_Function::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtLaunchOpConf_Function::DebugString);
    registry.def("__repr__", &ConstXrtLaunchOpConf_Function::DebugString);

    registry.def("argument_size", &ConstXrtLaunchOpConf_Function::argument_size);
    registry.def("argument", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> (ConstXrtLaunchOpConf_Function::*)() const)&ConstXrtLaunchOpConf_Function::shared_const_argument);
    registry.def("argument", (::std::shared_ptr<ConstXrtLaunchOpConf_Argument> (ConstXrtLaunchOpConf_Function::*)(::std::size_t) const)&ConstXrtLaunchOpConf_Function::shared_const_argument);

    registry.def("node_size", &ConstXrtLaunchOpConf_Function::node_size);
    registry.def("node", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> (ConstXrtLaunchOpConf_Function::*)() const)&ConstXrtLaunchOpConf_Function::shared_const_node);
    registry.def("node", (::std::shared_ptr<ConstOperatorConf> (ConstXrtLaunchOpConf_Function::*)(::std::size_t) const)&ConstXrtLaunchOpConf_Function::shared_const_node);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtLaunchOpConf_Function, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Function>> registry(m, "XrtLaunchOpConf_Function");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtLaunchOpConf_Function::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf_Function::*)(const ConstXrtLaunchOpConf_Function&))&::oneflow::cfg::XrtLaunchOpConf_Function::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf_Function::*)(const ::oneflow::cfg::XrtLaunchOpConf_Function&))&::oneflow::cfg::XrtLaunchOpConf_Function::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf_Function::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtLaunchOpConf_Function::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtLaunchOpConf_Function::DebugString);



    registry.def("argument_size", &::oneflow::cfg::XrtLaunchOpConf_Function::argument_size);
    registry.def("clear_argument", &::oneflow::cfg::XrtLaunchOpConf_Function::clear_argument);
    registry.def("mutable_argument", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> (::oneflow::cfg::XrtLaunchOpConf_Function::*)())&::oneflow::cfg::XrtLaunchOpConf_Function::shared_mutable_argument);
    registry.def("argument", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> (::oneflow::cfg::XrtLaunchOpConf_Function::*)() const)&::oneflow::cfg::XrtLaunchOpConf_Function::shared_const_argument);
    registry.def("argument", (::std::shared_ptr<ConstXrtLaunchOpConf_Argument> (::oneflow::cfg::XrtLaunchOpConf_Function::*)(::std::size_t) const)&::oneflow::cfg::XrtLaunchOpConf_Function::shared_const_argument);
    registry.def("mutable_argument", (::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> (::oneflow::cfg::XrtLaunchOpConf_Function::*)(::std::size_t))&::oneflow::cfg::XrtLaunchOpConf_Function::shared_mutable_argument);

    registry.def("node_size", &::oneflow::cfg::XrtLaunchOpConf_Function::node_size);
    registry.def("clear_node", &::oneflow::cfg::XrtLaunchOpConf_Function::clear_node);
    registry.def("mutable_node", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> (::oneflow::cfg::XrtLaunchOpConf_Function::*)())&::oneflow::cfg::XrtLaunchOpConf_Function::shared_mutable_node);
    registry.def("node", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> (::oneflow::cfg::XrtLaunchOpConf_Function::*)() const)&::oneflow::cfg::XrtLaunchOpConf_Function::shared_const_node);
    registry.def("node", (::std::shared_ptr<ConstOperatorConf> (::oneflow::cfg::XrtLaunchOpConf_Function::*)(::std::size_t) const)&::oneflow::cfg::XrtLaunchOpConf_Function::shared_const_node);
    registry.def("mutable_node", (::std::shared_ptr<::oneflow::cfg::OperatorConf> (::oneflow::cfg::XrtLaunchOpConf_Function::*)(::std::size_t))&::oneflow::cfg::XrtLaunchOpConf_Function::shared_mutable_node);
  }
  {
    pybind11::class_<ConstXrtLaunchOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstXrtLaunchOpConf>> registry(m, "ConstXrtLaunchOpConf");
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstXrtLaunchOpConf::DebugString);
    registry.def("__repr__", &ConstXrtLaunchOpConf::DebugString);

    registry.def("in_size", &ConstXrtLaunchOpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstXrtLaunchOpConf::*)(::std::size_t) const)&ConstXrtLaunchOpConf::in);

    registry.def("out_size", &ConstXrtLaunchOpConf::out_size);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (ConstXrtLaunchOpConf::*)(::std::size_t) const)&ConstXrtLaunchOpConf::out);

    registry.def("has_function", &ConstXrtLaunchOpConf::has_function);
    registry.def("function", &ConstXrtLaunchOpConf::shared_const_function);

    registry.def("has_engine", &ConstXrtLaunchOpConf::has_engine);
    registry.def("engine", &ConstXrtLaunchOpConf::engine);

    registry.def("input_mutability_size", &ConstXrtLaunchOpConf::input_mutability_size);
    registry.def("input_mutability", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_input_mutability);

    registry.def("input_mutability", (const bool& (ConstXrtLaunchOpConf::*)(const ::std::string&) const)&ConstXrtLaunchOpConf::input_mutability);

    registry.def("input_output_mapping_size", &ConstXrtLaunchOpConf::input_output_mapping_size);
    registry.def("input_output_mapping", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_input_output_mapping);

    registry.def("input_output_mapping", (const ::std::string& (ConstXrtLaunchOpConf::*)(const ::std::string&) const)&ConstXrtLaunchOpConf::input_output_mapping);

    registry.def("sbp_signatures_size", &ConstXrtLaunchOpConf::sbp_signatures_size);
    registry.def("sbp_signatures", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_sbp_signatures);

    registry.def("sbp_signatures", (::std::shared_ptr<ConstSbpSignature> (ConstXrtLaunchOpConf::*)(const ::std::string&) const)&ConstXrtLaunchOpConf::shared_const_sbp_signatures);

    registry.def("has_model_update", &ConstXrtLaunchOpConf::has_model_update);
    registry.def("model_update", &ConstXrtLaunchOpConf::model_update);

    registry.def("lbn2logical_blob_desc_size", &ConstXrtLaunchOpConf::lbn2logical_blob_desc_size);
    registry.def("lbn2logical_blob_desc", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> (ConstXrtLaunchOpConf::*)() const)&ConstXrtLaunchOpConf::shared_const_lbn2logical_blob_desc);

    registry.def("lbn2logical_blob_desc", (::std::shared_ptr<ConstBlobDescProto> (ConstXrtLaunchOpConf::*)(const ::std::string&) const)&ConstXrtLaunchOpConf::shared_const_lbn2logical_blob_desc);
  }
  {
    pybind11::class_<::oneflow::cfg::XrtLaunchOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf>> registry(m, "XrtLaunchOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::XrtLaunchOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf::*)(const ConstXrtLaunchOpConf&))&::oneflow::cfg::XrtLaunchOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::XrtLaunchOpConf::*)(const ::oneflow::cfg::XrtLaunchOpConf&))&::oneflow::cfg::XrtLaunchOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::XrtLaunchOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::XrtLaunchOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::XrtLaunchOpConf::DebugString);



    registry.def("in_size", &::oneflow::cfg::XrtLaunchOpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::XrtLaunchOpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::XrtLaunchOpConf::*)(::std::size_t) const)&::oneflow::cfg::XrtLaunchOpConf::in);
    registry.def("add_in", &::oneflow::cfg::XrtLaunchOpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::XrtLaunchOpConf::set_in);

    registry.def("out_size", &::oneflow::cfg::XrtLaunchOpConf::out_size);
    registry.def("clear_out", &::oneflow::cfg::XrtLaunchOpConf::clear_out);
    registry.def("mutable_out", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_out);
    registry.def("out", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_out);
    registry.def("out", (const ::std::string& (::oneflow::cfg::XrtLaunchOpConf::*)(::std::size_t) const)&::oneflow::cfg::XrtLaunchOpConf::out);
    registry.def("add_out", &::oneflow::cfg::XrtLaunchOpConf::add_out);
    registry.def("set_out", &::oneflow::cfg::XrtLaunchOpConf::set_out);

    registry.def("has_function", &::oneflow::cfg::XrtLaunchOpConf::has_function);
    registry.def("clear_function", &::oneflow::cfg::XrtLaunchOpConf::clear_function);
    registry.def("function", &::oneflow::cfg::XrtLaunchOpConf::shared_const_function);
    registry.def("mutable_function", &::oneflow::cfg::XrtLaunchOpConf::shared_mutable_function);

    registry.def("has_engine", &::oneflow::cfg::XrtLaunchOpConf::has_engine);
    registry.def("clear_engine", &::oneflow::cfg::XrtLaunchOpConf::clear_engine);
    registry.def("engine", &::oneflow::cfg::XrtLaunchOpConf::engine);
    registry.def("set_engine", &::oneflow::cfg::XrtLaunchOpConf::set_engine);

    registry.def("input_mutability_size", &::oneflow::cfg::XrtLaunchOpConf::input_mutability_size);
    registry.def("input_mutability", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_input_mutability);
    registry.def("clear_input_mutability", &::oneflow::cfg::XrtLaunchOpConf::clear_input_mutability);
    registry.def("mutable_input_mutability", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_input_mutability);
    registry.def("input_mutability", (const bool& (::oneflow::cfg::XrtLaunchOpConf::*)(const ::std::string&) const)&::oneflow::cfg::XrtLaunchOpConf::input_mutability);

    registry.def("input_output_mapping_size", &::oneflow::cfg::XrtLaunchOpConf::input_output_mapping_size);
    registry.def("input_output_mapping", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_input_output_mapping);
    registry.def("clear_input_output_mapping", &::oneflow::cfg::XrtLaunchOpConf::clear_input_output_mapping);
    registry.def("mutable_input_output_mapping", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_input_output_mapping);
    registry.def("input_output_mapping", (const ::std::string& (::oneflow::cfg::XrtLaunchOpConf::*)(const ::std::string&) const)&::oneflow::cfg::XrtLaunchOpConf::input_output_mapping);

    registry.def("sbp_signatures_size", &::oneflow::cfg::XrtLaunchOpConf::sbp_signatures_size);
    registry.def("sbp_signatures", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_sbp_signatures);
    registry.def("clear_sbp_signatures", &::oneflow::cfg::XrtLaunchOpConf::clear_sbp_signatures);
    registry.def("mutable_sbp_signatures", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_sbp_signatures);
    registry.def("sbp_signatures", (::std::shared_ptr<ConstSbpSignature> (::oneflow::cfg::XrtLaunchOpConf::*)(const ::std::string&) const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_sbp_signatures);

    registry.def("has_model_update", &::oneflow::cfg::XrtLaunchOpConf::has_model_update);
    registry.def("clear_model_update", &::oneflow::cfg::XrtLaunchOpConf::clear_model_update);
    registry.def("model_update", &::oneflow::cfg::XrtLaunchOpConf::model_update);
    registry.def("set_model_update", &::oneflow::cfg::XrtLaunchOpConf::set_model_update);

    registry.def("lbn2logical_blob_desc_size", &::oneflow::cfg::XrtLaunchOpConf::lbn2logical_blob_desc_size);
    registry.def("lbn2logical_blob_desc", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> (::oneflow::cfg::XrtLaunchOpConf::*)() const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_lbn2logical_blob_desc);
    registry.def("clear_lbn2logical_blob_desc", &::oneflow::cfg::XrtLaunchOpConf::clear_lbn2logical_blob_desc);
    registry.def("mutable_lbn2logical_blob_desc", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> (::oneflow::cfg::XrtLaunchOpConf::*)())&::oneflow::cfg::XrtLaunchOpConf::shared_mutable_lbn2logical_blob_desc);
    registry.def("lbn2logical_blob_desc", (::std::shared_ptr<ConstBlobDescProto> (::oneflow::cfg::XrtLaunchOpConf::*)(const ::std::string&) const)&::oneflow::cfg::XrtLaunchOpConf::shared_const_lbn2logical_blob_desc);
  }
  {
    pybind11::class_<ConstModelInitV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelInitV2OpConf>> registry(m, "ConstModelInitV2OpConf");
    registry.def("__id__", &::oneflow::cfg::ModelInitV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelInitV2OpConf::DebugString);
    registry.def("__repr__", &ConstModelInitV2OpConf::DebugString);

    registry.def("ref_size", &ConstModelInitV2OpConf::ref_size);
    registry.def("ref", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelInitV2OpConf::*)() const)&ConstModelInitV2OpConf::shared_const_ref);
    registry.def("ref", (const ::std::string& (ConstModelInitV2OpConf::*)(::std::size_t) const)&ConstModelInitV2OpConf::ref);

    registry.def("variable_op_name_size", &ConstModelInitV2OpConf::variable_op_name_size);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelInitV2OpConf::*)() const)&ConstModelInitV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (ConstModelInitV2OpConf::*)(::std::size_t) const)&ConstModelInitV2OpConf::variable_op_name);

    registry.def("original_variable_conf_size", &ConstModelInitV2OpConf::original_variable_conf_size);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (ConstModelInitV2OpConf::*)() const)&ConstModelInitV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (ConstModelInitV2OpConf::*)(::std::size_t) const)&ConstModelInitV2OpConf::shared_const_original_variable_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelInitV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelInitV2OpConf>> registry(m, "ModelInitV2OpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelInitV2OpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelInitV2OpConf::*)(const ConstModelInitV2OpConf&))&::oneflow::cfg::ModelInitV2OpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelInitV2OpConf::*)(const ::oneflow::cfg::ModelInitV2OpConf&))&::oneflow::cfg::ModelInitV2OpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelInitV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelInitV2OpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelInitV2OpConf::DebugString);



    registry.def("ref_size", &::oneflow::cfg::ModelInitV2OpConf::ref_size);
    registry.def("clear_ref", &::oneflow::cfg::ModelInitV2OpConf::clear_ref);
    registry.def("mutable_ref", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitV2OpConf::*)())&::oneflow::cfg::ModelInitV2OpConf::shared_mutable_ref);
    registry.def("ref", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitV2OpConf::*)() const)&::oneflow::cfg::ModelInitV2OpConf::shared_const_ref);
    registry.def("ref", (const ::std::string& (::oneflow::cfg::ModelInitV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitV2OpConf::ref);
    registry.def("add_ref", &::oneflow::cfg::ModelInitV2OpConf::add_ref);
    registry.def("set_ref", &::oneflow::cfg::ModelInitV2OpConf::set_ref);

    registry.def("variable_op_name_size", &::oneflow::cfg::ModelInitV2OpConf::variable_op_name_size);
    registry.def("clear_variable_op_name", &::oneflow::cfg::ModelInitV2OpConf::clear_variable_op_name);
    registry.def("mutable_variable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitV2OpConf::*)())&::oneflow::cfg::ModelInitV2OpConf::shared_mutable_variable_op_name);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelInitV2OpConf::*)() const)&::oneflow::cfg::ModelInitV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (::oneflow::cfg::ModelInitV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitV2OpConf::variable_op_name);
    registry.def("add_variable_op_name", &::oneflow::cfg::ModelInitV2OpConf::add_variable_op_name);
    registry.def("set_variable_op_name", &::oneflow::cfg::ModelInitV2OpConf::set_variable_op_name);

    registry.def("original_variable_conf_size", &::oneflow::cfg::ModelInitV2OpConf::original_variable_conf_size);
    registry.def("clear_original_variable_conf", &::oneflow::cfg::ModelInitV2OpConf::clear_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelInitV2OpConf::*)())&::oneflow::cfg::ModelInitV2OpConf::shared_mutable_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelInitV2OpConf::*)() const)&::oneflow::cfg::ModelInitV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (::oneflow::cfg::ModelInitV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelInitV2OpConf::shared_const_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (::oneflow::cfg::ModelInitV2OpConf::*)(::std::size_t))&::oneflow::cfg::ModelInitV2OpConf::shared_mutable_original_variable_conf);
  }
  {
    pybind11::class_<ConstModelLoadV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelLoadV2OpConf>> registry(m, "ConstModelLoadV2OpConf");
    registry.def("__id__", &::oneflow::cfg::ModelLoadV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelLoadV2OpConf::DebugString);
    registry.def("__repr__", &ConstModelLoadV2OpConf::DebugString);

    registry.def("has_path", &ConstModelLoadV2OpConf::has_path);
    registry.def("path", &ConstModelLoadV2OpConf::path);

    registry.def("ref_size", &ConstModelLoadV2OpConf::ref_size);
    registry.def("ref", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelLoadV2OpConf::*)() const)&ConstModelLoadV2OpConf::shared_const_ref);
    registry.def("ref", (const ::std::string& (ConstModelLoadV2OpConf::*)(::std::size_t) const)&ConstModelLoadV2OpConf::ref);

    registry.def("variable_op_name_size", &ConstModelLoadV2OpConf::variable_op_name_size);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelLoadV2OpConf::*)() const)&ConstModelLoadV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (ConstModelLoadV2OpConf::*)(::std::size_t) const)&ConstModelLoadV2OpConf::variable_op_name);

    registry.def("original_variable_conf_size", &ConstModelLoadV2OpConf::original_variable_conf_size);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (ConstModelLoadV2OpConf::*)() const)&ConstModelLoadV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (ConstModelLoadV2OpConf::*)(::std::size_t) const)&ConstModelLoadV2OpConf::shared_const_original_variable_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelLoadV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelLoadV2OpConf>> registry(m, "ModelLoadV2OpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelLoadV2OpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelLoadV2OpConf::*)(const ConstModelLoadV2OpConf&))&::oneflow::cfg::ModelLoadV2OpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelLoadV2OpConf::*)(const ::oneflow::cfg::ModelLoadV2OpConf&))&::oneflow::cfg::ModelLoadV2OpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelLoadV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelLoadV2OpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelLoadV2OpConf::DebugString);



    registry.def("has_path", &::oneflow::cfg::ModelLoadV2OpConf::has_path);
    registry.def("clear_path", &::oneflow::cfg::ModelLoadV2OpConf::clear_path);
    registry.def("path", &::oneflow::cfg::ModelLoadV2OpConf::path);
    registry.def("set_path", &::oneflow::cfg::ModelLoadV2OpConf::set_path);

    registry.def("ref_size", &::oneflow::cfg::ModelLoadV2OpConf::ref_size);
    registry.def("clear_ref", &::oneflow::cfg::ModelLoadV2OpConf::clear_ref);
    registry.def("mutable_ref", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadV2OpConf::*)())&::oneflow::cfg::ModelLoadV2OpConf::shared_mutable_ref);
    registry.def("ref", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadV2OpConf::*)() const)&::oneflow::cfg::ModelLoadV2OpConf::shared_const_ref);
    registry.def("ref", (const ::std::string& (::oneflow::cfg::ModelLoadV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadV2OpConf::ref);
    registry.def("add_ref", &::oneflow::cfg::ModelLoadV2OpConf::add_ref);
    registry.def("set_ref", &::oneflow::cfg::ModelLoadV2OpConf::set_ref);

    registry.def("variable_op_name_size", &::oneflow::cfg::ModelLoadV2OpConf::variable_op_name_size);
    registry.def("clear_variable_op_name", &::oneflow::cfg::ModelLoadV2OpConf::clear_variable_op_name);
    registry.def("mutable_variable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadV2OpConf::*)())&::oneflow::cfg::ModelLoadV2OpConf::shared_mutable_variable_op_name);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelLoadV2OpConf::*)() const)&::oneflow::cfg::ModelLoadV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (::oneflow::cfg::ModelLoadV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadV2OpConf::variable_op_name);
    registry.def("add_variable_op_name", &::oneflow::cfg::ModelLoadV2OpConf::add_variable_op_name);
    registry.def("set_variable_op_name", &::oneflow::cfg::ModelLoadV2OpConf::set_variable_op_name);

    registry.def("original_variable_conf_size", &::oneflow::cfg::ModelLoadV2OpConf::original_variable_conf_size);
    registry.def("clear_original_variable_conf", &::oneflow::cfg::ModelLoadV2OpConf::clear_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelLoadV2OpConf::*)())&::oneflow::cfg::ModelLoadV2OpConf::shared_mutable_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelLoadV2OpConf::*)() const)&::oneflow::cfg::ModelLoadV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (::oneflow::cfg::ModelLoadV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelLoadV2OpConf::shared_const_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (::oneflow::cfg::ModelLoadV2OpConf::*)(::std::size_t))&::oneflow::cfg::ModelLoadV2OpConf::shared_mutable_original_variable_conf);
  }
  {
    pybind11::class_<ConstModelSaveV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstModelSaveV2OpConf>> registry(m, "ConstModelSaveV2OpConf");
    registry.def("__id__", &::oneflow::cfg::ModelSaveV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstModelSaveV2OpConf::DebugString);
    registry.def("__repr__", &ConstModelSaveV2OpConf::DebugString);

    registry.def("has_path", &ConstModelSaveV2OpConf::has_path);
    registry.def("path", &ConstModelSaveV2OpConf::path);

    registry.def("in_size", &ConstModelSaveV2OpConf::in_size);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelSaveV2OpConf::*)() const)&ConstModelSaveV2OpConf::shared_const_in);
    registry.def("in", (const ::std::string& (ConstModelSaveV2OpConf::*)(::std::size_t) const)&ConstModelSaveV2OpConf::in);

    registry.def("variable_op_name_size", &ConstModelSaveV2OpConf::variable_op_name_size);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstModelSaveV2OpConf::*)() const)&ConstModelSaveV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (ConstModelSaveV2OpConf::*)(::std::size_t) const)&ConstModelSaveV2OpConf::variable_op_name);

    registry.def("original_variable_conf_size", &ConstModelSaveV2OpConf::original_variable_conf_size);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (ConstModelSaveV2OpConf::*)() const)&ConstModelSaveV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (ConstModelSaveV2OpConf::*)(::std::size_t) const)&ConstModelSaveV2OpConf::shared_const_original_variable_conf);
  }
  {
    pybind11::class_<::oneflow::cfg::ModelSaveV2OpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ModelSaveV2OpConf>> registry(m, "ModelSaveV2OpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ModelSaveV2OpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelSaveV2OpConf::*)(const ConstModelSaveV2OpConf&))&::oneflow::cfg::ModelSaveV2OpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ModelSaveV2OpConf::*)(const ::oneflow::cfg::ModelSaveV2OpConf&))&::oneflow::cfg::ModelSaveV2OpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ModelSaveV2OpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ModelSaveV2OpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ModelSaveV2OpConf::DebugString);



    registry.def("has_path", &::oneflow::cfg::ModelSaveV2OpConf::has_path);
    registry.def("clear_path", &::oneflow::cfg::ModelSaveV2OpConf::clear_path);
    registry.def("path", &::oneflow::cfg::ModelSaveV2OpConf::path);
    registry.def("set_path", &::oneflow::cfg::ModelSaveV2OpConf::set_path);

    registry.def("in_size", &::oneflow::cfg::ModelSaveV2OpConf::in_size);
    registry.def("clear_in", &::oneflow::cfg::ModelSaveV2OpConf::clear_in);
    registry.def("mutable_in", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveV2OpConf::*)())&::oneflow::cfg::ModelSaveV2OpConf::shared_mutable_in);
    registry.def("in", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveV2OpConf::*)() const)&::oneflow::cfg::ModelSaveV2OpConf::shared_const_in);
    registry.def("in", (const ::std::string& (::oneflow::cfg::ModelSaveV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelSaveV2OpConf::in);
    registry.def("add_in", &::oneflow::cfg::ModelSaveV2OpConf::add_in);
    registry.def("set_in", &::oneflow::cfg::ModelSaveV2OpConf::set_in);

    registry.def("variable_op_name_size", &::oneflow::cfg::ModelSaveV2OpConf::variable_op_name_size);
    registry.def("clear_variable_op_name", &::oneflow::cfg::ModelSaveV2OpConf::clear_variable_op_name);
    registry.def("mutable_variable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveV2OpConf::*)())&::oneflow::cfg::ModelSaveV2OpConf::shared_mutable_variable_op_name);
    registry.def("variable_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::ModelSaveV2OpConf::*)() const)&::oneflow::cfg::ModelSaveV2OpConf::shared_const_variable_op_name);
    registry.def("variable_op_name", (const ::std::string& (::oneflow::cfg::ModelSaveV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelSaveV2OpConf::variable_op_name);
    registry.def("add_variable_op_name", &::oneflow::cfg::ModelSaveV2OpConf::add_variable_op_name);
    registry.def("set_variable_op_name", &::oneflow::cfg::ModelSaveV2OpConf::set_variable_op_name);

    registry.def("original_variable_conf_size", &::oneflow::cfg::ModelSaveV2OpConf::original_variable_conf_size);
    registry.def("clear_original_variable_conf", &::oneflow::cfg::ModelSaveV2OpConf::clear_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelSaveV2OpConf::*)())&::oneflow::cfg::ModelSaveV2OpConf::shared_mutable_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> (::oneflow::cfg::ModelSaveV2OpConf::*)() const)&::oneflow::cfg::ModelSaveV2OpConf::shared_const_original_variable_conf);
    registry.def("original_variable_conf", (::std::shared_ptr<ConstVariableOpConf> (::oneflow::cfg::ModelSaveV2OpConf::*)(::std::size_t) const)&::oneflow::cfg::ModelSaveV2OpConf::shared_const_original_variable_conf);
    registry.def("mutable_original_variable_conf", (::std::shared_ptr<::oneflow::cfg::VariableOpConf> (::oneflow::cfg::ModelSaveV2OpConf::*)(::std::size_t))&::oneflow::cfg::ModelSaveV2OpConf::shared_mutable_original_variable_conf);
  }
  {
    pybind11::class_<ConstConstantLikeOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstConstantLikeOpConf>> registry(m, "ConstConstantLikeOpConf");
    registry.def("__id__", &::oneflow::cfg::ConstantLikeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstConstantLikeOpConf::DebugString);
    registry.def("__repr__", &ConstConstantLikeOpConf::DebugString);

    registry.def("has_like", &ConstConstantLikeOpConf::has_like);
    registry.def("like", &ConstConstantLikeOpConf::like);

    registry.def("has_out", &ConstConstantLikeOpConf::has_out);
    registry.def("out", &ConstConstantLikeOpConf::out);

    registry.def("has_data_type", &ConstConstantLikeOpConf::has_data_type);
    registry.def("data_type", &ConstConstantLikeOpConf::data_type);

    registry.def("has_int_operand", &ConstConstantLikeOpConf::has_int_operand);
    registry.def("int_operand", &ConstConstantLikeOpConf::int_operand);

    registry.def("has_float_operand", &ConstConstantLikeOpConf::has_float_operand);
    registry.def("float_operand", &ConstConstantLikeOpConf::float_operand);
    registry.def("scalar_operand_case",  &ConstConstantLikeOpConf::scalar_operand_case);
    registry.def_property_readonly_static("SCALAR_OPERAND_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::SCALAR_OPERAND_NOT_SET; })
        .def_property_readonly_static("kIntOperand", [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kIntOperand; })
        .def_property_readonly_static("kFloatOperand", [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kFloatOperand; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::ConstantLikeOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ConstantLikeOpConf>> registry(m, "ConstantLikeOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ConstantLikeOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantLikeOpConf::*)(const ConstConstantLikeOpConf&))&::oneflow::cfg::ConstantLikeOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ConstantLikeOpConf::*)(const ::oneflow::cfg::ConstantLikeOpConf&))&::oneflow::cfg::ConstantLikeOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ConstantLikeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ConstantLikeOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ConstantLikeOpConf::DebugString);

    registry.def_property_readonly_static("SCALAR_OPERAND_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::SCALAR_OPERAND_NOT_SET; })
        .def_property_readonly_static("kIntOperand", [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kIntOperand; })
        .def_property_readonly_static("kFloatOperand", [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kFloatOperand; })
        ;


    registry.def("has_like", &::oneflow::cfg::ConstantLikeOpConf::has_like);
    registry.def("clear_like", &::oneflow::cfg::ConstantLikeOpConf::clear_like);
    registry.def("like", &::oneflow::cfg::ConstantLikeOpConf::like);
    registry.def("set_like", &::oneflow::cfg::ConstantLikeOpConf::set_like);

    registry.def("has_out", &::oneflow::cfg::ConstantLikeOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::ConstantLikeOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::ConstantLikeOpConf::out);
    registry.def("set_out", &::oneflow::cfg::ConstantLikeOpConf::set_out);

    registry.def("has_data_type", &::oneflow::cfg::ConstantLikeOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::ConstantLikeOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::ConstantLikeOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::ConstantLikeOpConf::set_data_type);

    registry.def("has_int_operand", &::oneflow::cfg::ConstantLikeOpConf::has_int_operand);
    registry.def("clear_int_operand", &::oneflow::cfg::ConstantLikeOpConf::clear_int_operand);
    registry.def_property_readonly_static("kIntOperand",
        [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kIntOperand; });
    registry.def("int_operand", &::oneflow::cfg::ConstantLikeOpConf::int_operand);
    registry.def("set_int_operand", &::oneflow::cfg::ConstantLikeOpConf::set_int_operand);

    registry.def("has_float_operand", &::oneflow::cfg::ConstantLikeOpConf::has_float_operand);
    registry.def("clear_float_operand", &::oneflow::cfg::ConstantLikeOpConf::clear_float_operand);
    registry.def_property_readonly_static("kFloatOperand",
        [](const pybind11::object&){ return ::oneflow::cfg::ConstantLikeOpConf::kFloatOperand; });
    registry.def("float_operand", &::oneflow::cfg::ConstantLikeOpConf::float_operand);
    registry.def("set_float_operand", &::oneflow::cfg::ConstantLikeOpConf::set_float_operand);
    pybind11::enum_<::oneflow::cfg::ConstantLikeOpConf::ScalarOperandCase>(registry, "ScalarOperandCase")
        .value("SCALAR_OPERAND_NOT_SET", ::oneflow::cfg::ConstantLikeOpConf::SCALAR_OPERAND_NOT_SET)
        .value("kIntOperand", ::oneflow::cfg::ConstantLikeOpConf::kIntOperand)
        .value("kFloatOperand", ::oneflow::cfg::ConstantLikeOpConf::kFloatOperand)
        ;
    registry.def("scalar_operand_case",  &::oneflow::cfg::ConstantLikeOpConf::scalar_operand_case);
  }
  {
    pybind11::class_<ConstSyncDynamicResizeOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstSyncDynamicResizeOpConf>> registry(m, "ConstSyncDynamicResizeOpConf");
    registry.def("__id__", &::oneflow::cfg::SyncDynamicResizeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstSyncDynamicResizeOpConf::DebugString);
    registry.def("__repr__", &ConstSyncDynamicResizeOpConf::DebugString);

    registry.def("has_in", &ConstSyncDynamicResizeOpConf::has_in);
    registry.def("in", &ConstSyncDynamicResizeOpConf::in);

    registry.def("has_size", &ConstSyncDynamicResizeOpConf::has_size);
    registry.def("size", &ConstSyncDynamicResizeOpConf::size);

    registry.def("has_out", &ConstSyncDynamicResizeOpConf::has_out);
    registry.def("out", &ConstSyncDynamicResizeOpConf::out);

    registry.def("has_axis", &ConstSyncDynamicResizeOpConf::has_axis);
    registry.def("axis", &ConstSyncDynamicResizeOpConf::axis);

    registry.def("has_eager", &ConstSyncDynamicResizeOpConf::has_eager);
    registry.def("eager", &ConstSyncDynamicResizeOpConf::eager);
  }
  {
    pybind11::class_<::oneflow::cfg::SyncDynamicResizeOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::SyncDynamicResizeOpConf>> registry(m, "SyncDynamicResizeOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::SyncDynamicResizeOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::SyncDynamicResizeOpConf::*)(const ConstSyncDynamicResizeOpConf&))&::oneflow::cfg::SyncDynamicResizeOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::SyncDynamicResizeOpConf::*)(const ::oneflow::cfg::SyncDynamicResizeOpConf&))&::oneflow::cfg::SyncDynamicResizeOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::SyncDynamicResizeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::SyncDynamicResizeOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::SyncDynamicResizeOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::SyncDynamicResizeOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::SyncDynamicResizeOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::SyncDynamicResizeOpConf::in);
    registry.def("set_in", &::oneflow::cfg::SyncDynamicResizeOpConf::set_in);

    registry.def("has_size", &::oneflow::cfg::SyncDynamicResizeOpConf::has_size);
    registry.def("clear_size", &::oneflow::cfg::SyncDynamicResizeOpConf::clear_size);
    registry.def("size", &::oneflow::cfg::SyncDynamicResizeOpConf::size);
    registry.def("set_size", &::oneflow::cfg::SyncDynamicResizeOpConf::set_size);

    registry.def("has_out", &::oneflow::cfg::SyncDynamicResizeOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::SyncDynamicResizeOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::SyncDynamicResizeOpConf::out);
    registry.def("set_out", &::oneflow::cfg::SyncDynamicResizeOpConf::set_out);

    registry.def("has_axis", &::oneflow::cfg::SyncDynamicResizeOpConf::has_axis);
    registry.def("clear_axis", &::oneflow::cfg::SyncDynamicResizeOpConf::clear_axis);
    registry.def("axis", &::oneflow::cfg::SyncDynamicResizeOpConf::axis);
    registry.def("set_axis", &::oneflow::cfg::SyncDynamicResizeOpConf::set_axis);

    registry.def("has_eager", &::oneflow::cfg::SyncDynamicResizeOpConf::has_eager);
    registry.def("clear_eager", &::oneflow::cfg::SyncDynamicResizeOpConf::clear_eager);
    registry.def("eager", &::oneflow::cfg::SyncDynamicResizeOpConf::eager);
    registry.def("set_eager", &::oneflow::cfg::SyncDynamicResizeOpConf::set_eager);
  }
  {
    pybind11::class_<ConstBroadcastToCompatibleWithOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBroadcastToCompatibleWithOpConf>> registry(m, "ConstBroadcastToCompatibleWithOpConf");
    registry.def("__id__", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBroadcastToCompatibleWithOpConf::DebugString);
    registry.def("__repr__", &ConstBroadcastToCompatibleWithOpConf::DebugString);

    registry.def("has_x", &ConstBroadcastToCompatibleWithOpConf::has_x);
    registry.def("x", &ConstBroadcastToCompatibleWithOpConf::x);

    registry.def("compatible_size", &ConstBroadcastToCompatibleWithOpConf::compatible_size);
    registry.def("compatible", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstBroadcastToCompatibleWithOpConf::*)() const)&ConstBroadcastToCompatibleWithOpConf::shared_const_compatible);
    registry.def("compatible", (const ::std::string& (ConstBroadcastToCompatibleWithOpConf::*)(::std::size_t) const)&ConstBroadcastToCompatibleWithOpConf::compatible);

    registry.def("has_y", &ConstBroadcastToCompatibleWithOpConf::has_y);
    registry.def("y", &ConstBroadcastToCompatibleWithOpConf::y);
  }
  {
    pybind11::class_<::oneflow::cfg::BroadcastToCompatibleWithOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BroadcastToCompatibleWithOpConf>> registry(m, "BroadcastToCompatibleWithOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BroadcastToCompatibleWithOpConf::*)(const ConstBroadcastToCompatibleWithOpConf&))&::oneflow::cfg::BroadcastToCompatibleWithOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BroadcastToCompatibleWithOpConf::*)(const ::oneflow::cfg::BroadcastToCompatibleWithOpConf&))&::oneflow::cfg::BroadcastToCompatibleWithOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::DebugString);



    registry.def("has_x", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::has_x);
    registry.def("clear_x", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::clear_x);
    registry.def("x", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::x);
    registry.def("set_x", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::set_x);

    registry.def("compatible_size", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::compatible_size);
    registry.def("clear_compatible", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::clear_compatible);
    registry.def("mutable_compatible", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::BroadcastToCompatibleWithOpConf::*)())&::oneflow::cfg::BroadcastToCompatibleWithOpConf::shared_mutable_compatible);
    registry.def("compatible", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::BroadcastToCompatibleWithOpConf::*)() const)&::oneflow::cfg::BroadcastToCompatibleWithOpConf::shared_const_compatible);
    registry.def("compatible", (const ::std::string& (::oneflow::cfg::BroadcastToCompatibleWithOpConf::*)(::std::size_t) const)&::oneflow::cfg::BroadcastToCompatibleWithOpConf::compatible);
    registry.def("add_compatible", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::add_compatible);
    registry.def("set_compatible", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::set_compatible);

    registry.def("has_y", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::has_y);
    registry.def("clear_y", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::clear_y);
    registry.def("y", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::y);
    registry.def("set_y", &::oneflow::cfg::BroadcastToCompatibleWithOpConf::set_y);
  }
  {
    pybind11::class_<ConstCollectiveBoxingGenericOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCollectiveBoxingGenericOpConf>> registry(m, "ConstCollectiveBoxingGenericOpConf");
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingGenericOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCollectiveBoxingGenericOpConf::DebugString);
    registry.def("__repr__", &ConstCollectiveBoxingGenericOpConf::DebugString);

    registry.def("has_lbi", &ConstCollectiveBoxingGenericOpConf::has_lbi);
    registry.def("lbi", &ConstCollectiveBoxingGenericOpConf::shared_const_lbi);

    registry.def("has_rank_desc", &ConstCollectiveBoxingGenericOpConf::has_rank_desc);
    registry.def("rank_desc", &ConstCollectiveBoxingGenericOpConf::shared_const_rank_desc);
  }
  {
    pybind11::class_<::oneflow::cfg::CollectiveBoxingGenericOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CollectiveBoxingGenericOpConf>> registry(m, "CollectiveBoxingGenericOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CollectiveBoxingGenericOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingGenericOpConf::*)(const ConstCollectiveBoxingGenericOpConf&))&::oneflow::cfg::CollectiveBoxingGenericOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingGenericOpConf::*)(const ::oneflow::cfg::CollectiveBoxingGenericOpConf&))&::oneflow::cfg::CollectiveBoxingGenericOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingGenericOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CollectiveBoxingGenericOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CollectiveBoxingGenericOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::CollectiveBoxingGenericOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::CollectiveBoxingGenericOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::CollectiveBoxingGenericOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::CollectiveBoxingGenericOpConf::shared_mutable_lbi);

    registry.def("has_rank_desc", &::oneflow::cfg::CollectiveBoxingGenericOpConf::has_rank_desc);
    registry.def("clear_rank_desc", &::oneflow::cfg::CollectiveBoxingGenericOpConf::clear_rank_desc);
    registry.def("rank_desc", &::oneflow::cfg::CollectiveBoxingGenericOpConf::shared_const_rank_desc);
    registry.def("mutable_rank_desc", &::oneflow::cfg::CollectiveBoxingGenericOpConf::shared_mutable_rank_desc);
  }
  {
    pybind11::class_<ConstBoxingIdentityOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxingIdentityOpConf>> registry(m, "ConstBoxingIdentityOpConf");
    registry.def("__id__", &::oneflow::cfg::BoxingIdentityOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxingIdentityOpConf::DebugString);
    registry.def("__repr__", &ConstBoxingIdentityOpConf::DebugString);

    registry.def("has_lbi", &ConstBoxingIdentityOpConf::has_lbi);
    registry.def("lbi", &ConstBoxingIdentityOpConf::shared_const_lbi);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxingIdentityOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxingIdentityOpConf>> registry(m, "BoxingIdentityOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxingIdentityOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingIdentityOpConf::*)(const ConstBoxingIdentityOpConf&))&::oneflow::cfg::BoxingIdentityOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingIdentityOpConf::*)(const ::oneflow::cfg::BoxingIdentityOpConf&))&::oneflow::cfg::BoxingIdentityOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxingIdentityOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxingIdentityOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxingIdentityOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::BoxingIdentityOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::BoxingIdentityOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::BoxingIdentityOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::BoxingIdentityOpConf::shared_mutable_lbi);
  }
  {
    pybind11::class_<ConstCollectiveBoxingPackOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCollectiveBoxingPackOpConf>> registry(m, "ConstCollectiveBoxingPackOpConf");
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingPackOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCollectiveBoxingPackOpConf::DebugString);
    registry.def("__repr__", &ConstCollectiveBoxingPackOpConf::DebugString);

    registry.def("has_lbi", &ConstCollectiveBoxingPackOpConf::has_lbi);
    registry.def("lbi", &ConstCollectiveBoxingPackOpConf::shared_const_lbi);

    registry.def("has_src_sbp_parallel", &ConstCollectiveBoxingPackOpConf::has_src_sbp_parallel);
    registry.def("src_sbp_parallel", &ConstCollectiveBoxingPackOpConf::shared_const_src_sbp_parallel);

    registry.def("has_dst_sbp_parallel", &ConstCollectiveBoxingPackOpConf::has_dst_sbp_parallel);
    registry.def("dst_sbp_parallel", &ConstCollectiveBoxingPackOpConf::shared_const_dst_sbp_parallel);

    registry.def("has_num_ranks", &ConstCollectiveBoxingPackOpConf::has_num_ranks);
    registry.def("num_ranks", &ConstCollectiveBoxingPackOpConf::num_ranks);

    registry.def("has_logical_shape", &ConstCollectiveBoxingPackOpConf::has_logical_shape);
    registry.def("logical_shape", &ConstCollectiveBoxingPackOpConf::shared_const_logical_shape);
  }
  {
    pybind11::class_<::oneflow::cfg::CollectiveBoxingPackOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CollectiveBoxingPackOpConf>> registry(m, "CollectiveBoxingPackOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CollectiveBoxingPackOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingPackOpConf::*)(const ConstCollectiveBoxingPackOpConf&))&::oneflow::cfg::CollectiveBoxingPackOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingPackOpConf::*)(const ::oneflow::cfg::CollectiveBoxingPackOpConf&))&::oneflow::cfg::CollectiveBoxingPackOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingPackOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CollectiveBoxingPackOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CollectiveBoxingPackOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::CollectiveBoxingPackOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::CollectiveBoxingPackOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_mutable_lbi);

    registry.def("has_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::has_src_sbp_parallel);
    registry.def("clear_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::clear_src_sbp_parallel);
    registry.def("src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_const_src_sbp_parallel);
    registry.def("mutable_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_mutable_src_sbp_parallel);

    registry.def("has_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::has_dst_sbp_parallel);
    registry.def("clear_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::clear_dst_sbp_parallel);
    registry.def("dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_const_dst_sbp_parallel);
    registry.def("mutable_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_mutable_dst_sbp_parallel);

    registry.def("has_num_ranks", &::oneflow::cfg::CollectiveBoxingPackOpConf::has_num_ranks);
    registry.def("clear_num_ranks", &::oneflow::cfg::CollectiveBoxingPackOpConf::clear_num_ranks);
    registry.def("num_ranks", &::oneflow::cfg::CollectiveBoxingPackOpConf::num_ranks);
    registry.def("set_num_ranks", &::oneflow::cfg::CollectiveBoxingPackOpConf::set_num_ranks);

    registry.def("has_logical_shape", &::oneflow::cfg::CollectiveBoxingPackOpConf::has_logical_shape);
    registry.def("clear_logical_shape", &::oneflow::cfg::CollectiveBoxingPackOpConf::clear_logical_shape);
    registry.def("logical_shape", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_const_logical_shape);
    registry.def("mutable_logical_shape", &::oneflow::cfg::CollectiveBoxingPackOpConf::shared_mutable_logical_shape);
  }
  {
    pybind11::class_<ConstCollectiveBoxingUnpackOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstCollectiveBoxingUnpackOpConf>> registry(m, "ConstCollectiveBoxingUnpackOpConf");
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstCollectiveBoxingUnpackOpConf::DebugString);
    registry.def("__repr__", &ConstCollectiveBoxingUnpackOpConf::DebugString);

    registry.def("has_lbi", &ConstCollectiveBoxingUnpackOpConf::has_lbi);
    registry.def("lbi", &ConstCollectiveBoxingUnpackOpConf::shared_const_lbi);

    registry.def("has_src_sbp_parallel", &ConstCollectiveBoxingUnpackOpConf::has_src_sbp_parallel);
    registry.def("src_sbp_parallel", &ConstCollectiveBoxingUnpackOpConf::shared_const_src_sbp_parallel);

    registry.def("has_dst_sbp_parallel", &ConstCollectiveBoxingUnpackOpConf::has_dst_sbp_parallel);
    registry.def("dst_sbp_parallel", &ConstCollectiveBoxingUnpackOpConf::shared_const_dst_sbp_parallel);

    registry.def("has_num_ranks", &ConstCollectiveBoxingUnpackOpConf::has_num_ranks);
    registry.def("num_ranks", &ConstCollectiveBoxingUnpackOpConf::num_ranks);

    registry.def("has_logical_shape", &ConstCollectiveBoxingUnpackOpConf::has_logical_shape);
    registry.def("logical_shape", &ConstCollectiveBoxingUnpackOpConf::shared_const_logical_shape);
  }
  {
    pybind11::class_<::oneflow::cfg::CollectiveBoxingUnpackOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::CollectiveBoxingUnpackOpConf>> registry(m, "CollectiveBoxingUnpackOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingUnpackOpConf::*)(const ConstCollectiveBoxingUnpackOpConf&))&::oneflow::cfg::CollectiveBoxingUnpackOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::CollectiveBoxingUnpackOpConf::*)(const ::oneflow::cfg::CollectiveBoxingUnpackOpConf&))&::oneflow::cfg::CollectiveBoxingUnpackOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_mutable_lbi);

    registry.def("has_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::has_src_sbp_parallel);
    registry.def("clear_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::clear_src_sbp_parallel);
    registry.def("src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_const_src_sbp_parallel);
    registry.def("mutable_src_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_mutable_src_sbp_parallel);

    registry.def("has_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::has_dst_sbp_parallel);
    registry.def("clear_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::clear_dst_sbp_parallel);
    registry.def("dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_const_dst_sbp_parallel);
    registry.def("mutable_dst_sbp_parallel", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_mutable_dst_sbp_parallel);

    registry.def("has_num_ranks", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::has_num_ranks);
    registry.def("clear_num_ranks", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::clear_num_ranks);
    registry.def("num_ranks", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::num_ranks);
    registry.def("set_num_ranks", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::set_num_ranks);

    registry.def("has_logical_shape", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::has_logical_shape);
    registry.def("clear_logical_shape", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::clear_logical_shape);
    registry.def("logical_shape", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_const_logical_shape);
    registry.def("mutable_logical_shape", &::oneflow::cfg::CollectiveBoxingUnpackOpConf::shared_mutable_logical_shape);
  }
  {
    pybind11::class_<ConstImageDecoderRandomCropResizeOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstImageDecoderRandomCropResizeOpConf>> registry(m, "ConstImageDecoderRandomCropResizeOpConf");
    registry.def("__id__", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstImageDecoderRandomCropResizeOpConf::DebugString);
    registry.def("__repr__", &ConstImageDecoderRandomCropResizeOpConf::DebugString);

    registry.def("has_in", &ConstImageDecoderRandomCropResizeOpConf::has_in);
    registry.def("in", &ConstImageDecoderRandomCropResizeOpConf::in);

    registry.def("has_out", &ConstImageDecoderRandomCropResizeOpConf::has_out);
    registry.def("out", &ConstImageDecoderRandomCropResizeOpConf::out);

    registry.def("has_target_width", &ConstImageDecoderRandomCropResizeOpConf::has_target_width);
    registry.def("target_width", &ConstImageDecoderRandomCropResizeOpConf::target_width);

    registry.def("has_target_height", &ConstImageDecoderRandomCropResizeOpConf::has_target_height);
    registry.def("target_height", &ConstImageDecoderRandomCropResizeOpConf::target_height);

    registry.def("has_num_workers", &ConstImageDecoderRandomCropResizeOpConf::has_num_workers);
    registry.def("num_workers", &ConstImageDecoderRandomCropResizeOpConf::num_workers);

    registry.def("has_max_num_pixels", &ConstImageDecoderRandomCropResizeOpConf::has_max_num_pixels);
    registry.def("max_num_pixels", &ConstImageDecoderRandomCropResizeOpConf::max_num_pixels);

    registry.def("has_warmup_size", &ConstImageDecoderRandomCropResizeOpConf::has_warmup_size);
    registry.def("warmup_size", &ConstImageDecoderRandomCropResizeOpConf::warmup_size);

    registry.def("has_seed", &ConstImageDecoderRandomCropResizeOpConf::has_seed);
    registry.def("seed", &ConstImageDecoderRandomCropResizeOpConf::seed);

    registry.def("has_num_attempts", &ConstImageDecoderRandomCropResizeOpConf::has_num_attempts);
    registry.def("num_attempts", &ConstImageDecoderRandomCropResizeOpConf::num_attempts);

    registry.def("has_random_area_min", &ConstImageDecoderRandomCropResizeOpConf::has_random_area_min);
    registry.def("random_area_min", &ConstImageDecoderRandomCropResizeOpConf::random_area_min);

    registry.def("has_random_area_max", &ConstImageDecoderRandomCropResizeOpConf::has_random_area_max);
    registry.def("random_area_max", &ConstImageDecoderRandomCropResizeOpConf::random_area_max);

    registry.def("has_random_aspect_ratio_min", &ConstImageDecoderRandomCropResizeOpConf::has_random_aspect_ratio_min);
    registry.def("random_aspect_ratio_min", &ConstImageDecoderRandomCropResizeOpConf::random_aspect_ratio_min);

    registry.def("has_random_aspect_ratio_max", &ConstImageDecoderRandomCropResizeOpConf::has_random_aspect_ratio_max);
    registry.def("random_aspect_ratio_max", &ConstImageDecoderRandomCropResizeOpConf::random_aspect_ratio_max);
  }
  {
    pybind11::class_<::oneflow::cfg::ImageDecoderRandomCropResizeOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::ImageDecoderRandomCropResizeOpConf>> registry(m, "ImageDecoderRandomCropResizeOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::*)(const ConstImageDecoderRandomCropResizeOpConf&))&::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::*)(const ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf&))&::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::DebugString);



    registry.def("has_in", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_in);
    registry.def("clear_in", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_in);
    registry.def("in", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::in);
    registry.def("set_in", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_in);

    registry.def("has_out", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_out);
    registry.def("clear_out", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_out);
    registry.def("out", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::out);
    registry.def("set_out", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_out);

    registry.def("has_target_width", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_target_width);
    registry.def("clear_target_width", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_target_width);
    registry.def("target_width", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::target_width);
    registry.def("set_target_width", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_target_width);

    registry.def("has_target_height", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_target_height);
    registry.def("clear_target_height", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_target_height);
    registry.def("target_height", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::target_height);
    registry.def("set_target_height", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_target_height);

    registry.def("has_num_workers", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_num_workers);
    registry.def("clear_num_workers", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_num_workers);
    registry.def("num_workers", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::num_workers);
    registry.def("set_num_workers", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_num_workers);

    registry.def("has_max_num_pixels", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_max_num_pixels);
    registry.def("clear_max_num_pixels", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_max_num_pixels);
    registry.def("max_num_pixels", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::max_num_pixels);
    registry.def("set_max_num_pixels", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_max_num_pixels);

    registry.def("has_warmup_size", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_warmup_size);
    registry.def("clear_warmup_size", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_warmup_size);
    registry.def("warmup_size", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::warmup_size);
    registry.def("set_warmup_size", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_warmup_size);

    registry.def("has_seed", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_seed);
    registry.def("clear_seed", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_seed);
    registry.def("seed", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::seed);
    registry.def("set_seed", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_seed);

    registry.def("has_num_attempts", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_num_attempts);
    registry.def("clear_num_attempts", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_num_attempts);
    registry.def("num_attempts", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::num_attempts);
    registry.def("set_num_attempts", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_num_attempts);

    registry.def("has_random_area_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_random_area_min);
    registry.def("clear_random_area_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_random_area_min);
    registry.def("random_area_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::random_area_min);
    registry.def("set_random_area_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_random_area_min);

    registry.def("has_random_area_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_random_area_max);
    registry.def("clear_random_area_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_random_area_max);
    registry.def("random_area_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::random_area_max);
    registry.def("set_random_area_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_random_area_max);

    registry.def("has_random_aspect_ratio_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_random_aspect_ratio_min);
    registry.def("clear_random_aspect_ratio_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_random_aspect_ratio_min);
    registry.def("random_aspect_ratio_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::random_aspect_ratio_min);
    registry.def("set_random_aspect_ratio_min", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_random_aspect_ratio_min);

    registry.def("has_random_aspect_ratio_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::has_random_aspect_ratio_max);
    registry.def("clear_random_aspect_ratio_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::clear_random_aspect_ratio_max);
    registry.def("random_aspect_ratio_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::random_aspect_ratio_max);
    registry.def("set_random_aspect_ratio_max", &::oneflow::cfg::ImageDecoderRandomCropResizeOpConf::set_random_aspect_ratio_max);
  }
  {
    pybind11::class_<ConstBoxingZerosOpConf, ::oneflow::cfg::Message, std::shared_ptr<ConstBoxingZerosOpConf>> registry(m, "ConstBoxingZerosOpConf");
    registry.def("__id__", &::oneflow::cfg::BoxingZerosOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstBoxingZerosOpConf::DebugString);
    registry.def("__repr__", &ConstBoxingZerosOpConf::DebugString);

    registry.def("has_lbi", &ConstBoxingZerosOpConf::has_lbi);
    registry.def("lbi", &ConstBoxingZerosOpConf::shared_const_lbi);

    registry.def("has_shape", &ConstBoxingZerosOpConf::has_shape);
    registry.def("shape", &ConstBoxingZerosOpConf::shared_const_shape);

    registry.def("has_data_type", &ConstBoxingZerosOpConf::has_data_type);
    registry.def("data_type", &ConstBoxingZerosOpConf::data_type);
  }
  {
    pybind11::class_<::oneflow::cfg::BoxingZerosOpConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::BoxingZerosOpConf>> registry(m, "BoxingZerosOpConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::BoxingZerosOpConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingZerosOpConf::*)(const ConstBoxingZerosOpConf&))&::oneflow::cfg::BoxingZerosOpConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::BoxingZerosOpConf::*)(const ::oneflow::cfg::BoxingZerosOpConf&))&::oneflow::cfg::BoxingZerosOpConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::BoxingZerosOpConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::BoxingZerosOpConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::BoxingZerosOpConf::DebugString);



    registry.def("has_lbi", &::oneflow::cfg::BoxingZerosOpConf::has_lbi);
    registry.def("clear_lbi", &::oneflow::cfg::BoxingZerosOpConf::clear_lbi);
    registry.def("lbi", &::oneflow::cfg::BoxingZerosOpConf::shared_const_lbi);
    registry.def("mutable_lbi", &::oneflow::cfg::BoxingZerosOpConf::shared_mutable_lbi);

    registry.def("has_shape", &::oneflow::cfg::BoxingZerosOpConf::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::BoxingZerosOpConf::clear_shape);
    registry.def("shape", &::oneflow::cfg::BoxingZerosOpConf::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::BoxingZerosOpConf::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::BoxingZerosOpConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::BoxingZerosOpConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::BoxingZerosOpConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::BoxingZerosOpConf::set_data_type);
  }
  {
    pybind11::class_<ConstOperatorConf, ::oneflow::cfg::Message, std::shared_ptr<ConstOperatorConf>> registry(m, "ConstOperatorConf");
    registry.def("__id__", &::oneflow::cfg::OperatorConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOperatorConf::DebugString);
    registry.def("__repr__", &ConstOperatorConf::DebugString);

    registry.def("has_name", &ConstOperatorConf::has_name);
    registry.def("name", &ConstOperatorConf::name);

    registry.def("has_device_tag", &ConstOperatorConf::has_device_tag);
    registry.def("device_tag", &ConstOperatorConf::device_tag);

    registry.def("ctrl_in_op_name_size", &ConstOperatorConf::ctrl_in_op_name_size);
    registry.def("ctrl_in_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstOperatorConf::*)() const)&ConstOperatorConf::shared_const_ctrl_in_op_name);
    registry.def("ctrl_in_op_name", (const ::std::string& (ConstOperatorConf::*)(::std::size_t) const)&ConstOperatorConf::ctrl_in_op_name);

    registry.def("has_scope_symbol_id", &ConstOperatorConf::has_scope_symbol_id);
    registry.def("scope_symbol_id", &ConstOperatorConf::scope_symbol_id);

    registry.def("has_stream_index_hint", &ConstOperatorConf::has_stream_index_hint);
    registry.def("stream_index_hint", &ConstOperatorConf::stream_index_hint);

    registry.def("has_pass_tag", &ConstOperatorConf::has_pass_tag);
    registry.def("pass_tag", &ConstOperatorConf::pass_tag);

    registry.def("has_decode_random_conf", &ConstOperatorConf::has_decode_random_conf);
    registry.def("decode_random_conf", &ConstOperatorConf::shared_const_decode_random_conf);

    registry.def("has_copy_hd_conf", &ConstOperatorConf::has_copy_hd_conf);
    registry.def("copy_hd_conf", &ConstOperatorConf::shared_const_copy_hd_conf);

    registry.def("has_copy_comm_net_conf", &ConstOperatorConf::has_copy_comm_net_conf);
    registry.def("copy_comm_net_conf", &ConstOperatorConf::shared_const_copy_comm_net_conf);

    registry.def("has_boxing_conf", &ConstOperatorConf::has_boxing_conf);
    registry.def("boxing_conf", &ConstOperatorConf::shared_const_boxing_conf);

    registry.def("has_variable_conf", &ConstOperatorConf::has_variable_conf);
    registry.def("variable_conf", &ConstOperatorConf::shared_const_variable_conf);

    registry.def("has_tick_conf", &ConstOperatorConf::has_tick_conf);
    registry.def("tick_conf", &ConstOperatorConf::shared_const_tick_conf);

    registry.def("has_total_loss_instance_num_conf", &ConstOperatorConf::has_total_loss_instance_num_conf);
    registry.def("total_loss_instance_num_conf", &ConstOperatorConf::shared_const_total_loss_instance_num_conf);

    registry.def("has_shape_elem_cnt_conf", &ConstOperatorConf::has_shape_elem_cnt_conf);
    registry.def("shape_elem_cnt_conf", &ConstOperatorConf::shared_const_shape_elem_cnt_conf);

    registry.def("has_src_subset_tick_conf", &ConstOperatorConf::has_src_subset_tick_conf);
    registry.def("src_subset_tick_conf", &ConstOperatorConf::shared_const_src_subset_tick_conf);

    registry.def("has_dst_subset_tick_conf", &ConstOperatorConf::has_dst_subset_tick_conf);
    registry.def("dst_subset_tick_conf", &ConstOperatorConf::shared_const_dst_subset_tick_conf);

    registry.def("has_source_tick_conf", &ConstOperatorConf::has_source_tick_conf);
    registry.def("source_tick_conf", &ConstOperatorConf::shared_const_source_tick_conf);

    registry.def("has_sink_tick_conf", &ConstOperatorConf::has_sink_tick_conf);
    registry.def("sink_tick_conf", &ConstOperatorConf::shared_const_sink_tick_conf);

    registry.def("has_input_conf", &ConstOperatorConf::has_input_conf);
    registry.def("input_conf", &ConstOperatorConf::shared_const_input_conf);

    registry.def("has_output_conf", &ConstOperatorConf::has_output_conf);
    registry.def("output_conf", &ConstOperatorConf::shared_const_output_conf);

    registry.def("has_wait_and_send_ids_conf", &ConstOperatorConf::has_wait_and_send_ids_conf);
    registry.def("wait_and_send_ids_conf", &ConstOperatorConf::shared_const_wait_and_send_ids_conf);

    registry.def("has_reentrant_lock_conf", &ConstOperatorConf::has_reentrant_lock_conf);
    registry.def("reentrant_lock_conf", &ConstOperatorConf::shared_const_reentrant_lock_conf);

    registry.def("has_callback_notify_conf", &ConstOperatorConf::has_callback_notify_conf);
    registry.def("callback_notify_conf", &ConstOperatorConf::shared_const_callback_notify_conf);

    registry.def("has_foreign_input_conf", &ConstOperatorConf::has_foreign_input_conf);
    registry.def("foreign_input_conf", &ConstOperatorConf::shared_const_foreign_input_conf);

    registry.def("has_foreign_output_conf", &ConstOperatorConf::has_foreign_output_conf);
    registry.def("foreign_output_conf", &ConstOperatorConf::shared_const_foreign_output_conf);

    registry.def("has_acc_tick_conf", &ConstOperatorConf::has_acc_tick_conf);
    registry.def("acc_tick_conf", &ConstOperatorConf::shared_const_acc_tick_conf);

    registry.def("has_return_conf", &ConstOperatorConf::has_return_conf);
    registry.def("return_conf", &ConstOperatorConf::shared_const_return_conf);

    registry.def("has_foreign_watch_conf", &ConstOperatorConf::has_foreign_watch_conf);
    registry.def("foreign_watch_conf", &ConstOperatorConf::shared_const_foreign_watch_conf);

    registry.def("has_distribute_concat_conf", &ConstOperatorConf::has_distribute_concat_conf);
    registry.def("distribute_concat_conf", &ConstOperatorConf::shared_const_distribute_concat_conf);

    registry.def("has_distribute_split_conf", &ConstOperatorConf::has_distribute_split_conf);
    registry.def("distribute_split_conf", &ConstOperatorConf::shared_const_distribute_split_conf);

    registry.def("has_distribute_clone_conf", &ConstOperatorConf::has_distribute_clone_conf);
    registry.def("distribute_clone_conf", &ConstOperatorConf::shared_const_distribute_clone_conf);

    registry.def("has_distribute_add_conf", &ConstOperatorConf::has_distribute_add_conf);
    registry.def("distribute_add_conf", &ConstOperatorConf::shared_const_distribute_add_conf);

    registry.def("has_device_tick_conf", &ConstOperatorConf::has_device_tick_conf);
    registry.def("device_tick_conf", &ConstOperatorConf::shared_const_device_tick_conf);

    registry.def("has_slice_boxing_copy_conf", &ConstOperatorConf::has_slice_boxing_copy_conf);
    registry.def("slice_boxing_copy_conf", &ConstOperatorConf::shared_const_slice_boxing_copy_conf);

    registry.def("has_slice_boxing_add_conf", &ConstOperatorConf::has_slice_boxing_add_conf);
    registry.def("slice_boxing_add_conf", &ConstOperatorConf::shared_const_slice_boxing_add_conf);

    registry.def("has_collective_boxing_generic_conf", &ConstOperatorConf::has_collective_boxing_generic_conf);
    registry.def("collective_boxing_generic_conf", &ConstOperatorConf::shared_const_collective_boxing_generic_conf);

    registry.def("has_boxing_identity_conf", &ConstOperatorConf::has_boxing_identity_conf);
    registry.def("boxing_identity_conf", &ConstOperatorConf::shared_const_boxing_identity_conf);

    registry.def("has_collective_boxing_pack_conf", &ConstOperatorConf::has_collective_boxing_pack_conf);
    registry.def("collective_boxing_pack_conf", &ConstOperatorConf::shared_const_collective_boxing_pack_conf);

    registry.def("has_collective_boxing_unpack_conf", &ConstOperatorConf::has_collective_boxing_unpack_conf);
    registry.def("collective_boxing_unpack_conf", &ConstOperatorConf::shared_const_collective_boxing_unpack_conf);

    registry.def("has_boxing_zeros_conf", &ConstOperatorConf::has_boxing_zeros_conf);
    registry.def("boxing_zeros_conf", &ConstOperatorConf::shared_const_boxing_zeros_conf);

    registry.def("has_user_conf", &ConstOperatorConf::has_user_conf);
    registry.def("user_conf", &ConstOperatorConf::shared_const_user_conf);

    registry.def("has_dynamic_reshape_conf", &ConstOperatorConf::has_dynamic_reshape_conf);
    registry.def("dynamic_reshape_conf", &ConstOperatorConf::shared_const_dynamic_reshape_conf);

    registry.def("has_dynamic_reshape_like_conf", &ConstOperatorConf::has_dynamic_reshape_like_conf);
    registry.def("dynamic_reshape_like_conf", &ConstOperatorConf::shared_const_dynamic_reshape_like_conf);

    registry.def("has_identity_conf", &ConstOperatorConf::has_identity_conf);
    registry.def("identity_conf", &ConstOperatorConf::shared_const_identity_conf);

    registry.def("has_case_conf", &ConstOperatorConf::has_case_conf);
    registry.def("case_conf", &ConstOperatorConf::shared_const_case_conf);

    registry.def("has_esac_conf", &ConstOperatorConf::has_esac_conf);
    registry.def("esac_conf", &ConstOperatorConf::shared_const_esac_conf);

    registry.def("has_model_init_conf", &ConstOperatorConf::has_model_init_conf);
    registry.def("model_init_conf", &ConstOperatorConf::shared_const_model_init_conf);

    registry.def("has_assign_conf", &ConstOperatorConf::has_assign_conf);
    registry.def("assign_conf", &ConstOperatorConf::shared_const_assign_conf);

    registry.def("has_model_save_conf", &ConstOperatorConf::has_model_save_conf);
    registry.def("model_save_conf", &ConstOperatorConf::shared_const_model_save_conf);

    registry.def("has_learning_rate_schedule_conf", &ConstOperatorConf::has_learning_rate_schedule_conf);
    registry.def("learning_rate_schedule_conf", &ConstOperatorConf::shared_const_learning_rate_schedule_conf);

    registry.def("has_model_load_conf", &ConstOperatorConf::has_model_load_conf);
    registry.def("model_load_conf", &ConstOperatorConf::shared_const_model_load_conf);

    registry.def("has_constant_like_conf", &ConstOperatorConf::has_constant_like_conf);
    registry.def("constant_like_conf", &ConstOperatorConf::shared_const_constant_like_conf);

    registry.def("has_sync_dynamic_resize_conf", &ConstOperatorConf::has_sync_dynamic_resize_conf);
    registry.def("sync_dynamic_resize_conf", &ConstOperatorConf::shared_const_sync_dynamic_resize_conf);

    registry.def("has_copy_conf", &ConstOperatorConf::has_copy_conf);
    registry.def("copy_conf", &ConstOperatorConf::shared_const_copy_conf);

    registry.def("has_cast_to_mirrored_conf", &ConstOperatorConf::has_cast_to_mirrored_conf);
    registry.def("cast_to_mirrored_conf", &ConstOperatorConf::shared_const_cast_to_mirrored_conf);

    registry.def("has_cast_from_mirrored_conf", &ConstOperatorConf::has_cast_from_mirrored_conf);
    registry.def("cast_from_mirrored_conf", &ConstOperatorConf::shared_const_cast_from_mirrored_conf);

    registry.def("has_model_init_v2_conf", &ConstOperatorConf::has_model_init_v2_conf);
    registry.def("model_init_v2_conf", &ConstOperatorConf::shared_const_model_init_v2_conf);

    registry.def("has_model_load_v2_conf", &ConstOperatorConf::has_model_load_v2_conf);
    registry.def("model_load_v2_conf", &ConstOperatorConf::shared_const_model_load_v2_conf);

    registry.def("has_model_save_v2_conf", &ConstOperatorConf::has_model_save_v2_conf);
    registry.def("model_save_v2_conf", &ConstOperatorConf::shared_const_model_save_v2_conf);

    registry.def("has_image_decoder_random_crop_resize_conf", &ConstOperatorConf::has_image_decoder_random_crop_resize_conf);
    registry.def("image_decoder_random_crop_resize_conf", &ConstOperatorConf::shared_const_image_decoder_random_crop_resize_conf);

    registry.def("has_xrt_launch_conf", &ConstOperatorConf::has_xrt_launch_conf);
    registry.def("xrt_launch_conf", &ConstOperatorConf::shared_const_xrt_launch_conf);

    registry.def("has_broadcast_to_compatible_with_conf", &ConstOperatorConf::has_broadcast_to_compatible_with_conf);
    registry.def("broadcast_to_compatible_with_conf", &ConstOperatorConf::shared_const_broadcast_to_compatible_with_conf);

    registry.def("has_feed_input_conf", &ConstOperatorConf::has_feed_input_conf);
    registry.def("feed_input_conf", &ConstOperatorConf::shared_const_feed_input_conf);

    registry.def("has_feed_variable_conf", &ConstOperatorConf::has_feed_variable_conf);
    registry.def("feed_variable_conf", &ConstOperatorConf::shared_const_feed_variable_conf);

    registry.def("has_fetch_output_conf", &ConstOperatorConf::has_fetch_output_conf);
    registry.def("fetch_output_conf", &ConstOperatorConf::shared_const_fetch_output_conf);
    registry.def("op_type_case",  &ConstOperatorConf::op_type_case);
    registry.def_property_readonly_static("OP_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::OP_TYPE_NOT_SET; })
        .def_property_readonly_static("kDecodeRandomConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDecodeRandomConf; })
        .def_property_readonly_static("kCopyHdConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyHdConf; })
        .def_property_readonly_static("kCopyCommNetConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyCommNetConf; })
        .def_property_readonly_static("kBoxingConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingConf; })
        .def_property_readonly_static("kVariableConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kVariableConf; })
        .def_property_readonly_static("kTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTickConf; })
        .def_property_readonly_static("kTotalLossInstanceNumConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTotalLossInstanceNumConf; })
        .def_property_readonly_static("kShapeElemCntConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kShapeElemCntConf; })
        .def_property_readonly_static("kSrcSubsetTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSrcSubsetTickConf; })
        .def_property_readonly_static("kDstSubsetTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDstSubsetTickConf; })
        .def_property_readonly_static("kSourceTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSourceTickConf; })
        .def_property_readonly_static("kSinkTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSinkTickConf; })
        .def_property_readonly_static("kInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kInputConf; })
        .def_property_readonly_static("kOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kOutputConf; })
        .def_property_readonly_static("kWaitAndSendIdsConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kWaitAndSendIdsConf; })
        .def_property_readonly_static("kReentrantLockConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReentrantLockConf; })
        .def_property_readonly_static("kCallbackNotifyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCallbackNotifyConf; })
        .def_property_readonly_static("kForeignInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignInputConf; })
        .def_property_readonly_static("kForeignOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignOutputConf; })
        .def_property_readonly_static("kAccTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAccTickConf; })
        .def_property_readonly_static("kReturnConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReturnConf; })
        .def_property_readonly_static("kForeignWatchConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignWatchConf; })
        .def_property_readonly_static("kDistributeConcatConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeConcatConf; })
        .def_property_readonly_static("kDistributeSplitConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeSplitConf; })
        .def_property_readonly_static("kDistributeCloneConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeCloneConf; })
        .def_property_readonly_static("kDistributeAddConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeAddConf; })
        .def_property_readonly_static("kDeviceTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDeviceTickConf; })
        .def_property_readonly_static("kSliceBoxingCopyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingCopyConf; })
        .def_property_readonly_static("kSliceBoxingAddConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingAddConf; })
        .def_property_readonly_static("kCollectiveBoxingGenericConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingGenericConf; })
        .def_property_readonly_static("kBoxingIdentityConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingIdentityConf; })
        .def_property_readonly_static("kCollectiveBoxingPackConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingPackConf; })
        .def_property_readonly_static("kCollectiveBoxingUnpackConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingUnpackConf; })
        .def_property_readonly_static("kBoxingZerosConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingZerosConf; })
        .def_property_readonly_static("kUserConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kUserConf; })
        .def_property_readonly_static("kDynamicReshapeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeConf; })
        .def_property_readonly_static("kDynamicReshapeLikeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeLikeConf; })
        .def_property_readonly_static("kIdentityConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kIdentityConf; })
        .def_property_readonly_static("kCaseConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCaseConf; })
        .def_property_readonly_static("kEsacConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kEsacConf; })
        .def_property_readonly_static("kModelInitConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitConf; })
        .def_property_readonly_static("kAssignConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAssignConf; })
        .def_property_readonly_static("kModelSaveConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveConf; })
        .def_property_readonly_static("kLearningRateScheduleConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kLearningRateScheduleConf; })
        .def_property_readonly_static("kModelLoadConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadConf; })
        .def_property_readonly_static("kConstantLikeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kConstantLikeConf; })
        .def_property_readonly_static("kSyncDynamicResizeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSyncDynamicResizeConf; })
        .def_property_readonly_static("kCopyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyConf; })
        .def_property_readonly_static("kCastToMirroredConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastToMirroredConf; })
        .def_property_readonly_static("kCastFromMirroredConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastFromMirroredConf; })
        .def_property_readonly_static("kModelInitV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitV2Conf; })
        .def_property_readonly_static("kModelLoadV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadV2Conf; })
        .def_property_readonly_static("kModelSaveV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveV2Conf; })
        .def_property_readonly_static("kImageDecoderRandomCropResizeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kImageDecoderRandomCropResizeConf; })
        .def_property_readonly_static("kXrtLaunchConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kXrtLaunchConf; })
        .def_property_readonly_static("kBroadcastToCompatibleWithConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBroadcastToCompatibleWithConf; })
        .def_property_readonly_static("kFeedInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedInputConf; })
        .def_property_readonly_static("kFeedVariableConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedVariableConf; })
        .def_property_readonly_static("kFetchOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFetchOutputConf; })
        ;
  }
  {
    pybind11::class_<::oneflow::cfg::OperatorConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OperatorConf>> registry(m, "OperatorConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OperatorConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OperatorConf::*)(const ConstOperatorConf&))&::oneflow::cfg::OperatorConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OperatorConf::*)(const ::oneflow::cfg::OperatorConf&))&::oneflow::cfg::OperatorConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OperatorConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OperatorConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OperatorConf::DebugString);

    registry.def_property_readonly_static("OP_TYPE_NOT_SET",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::OP_TYPE_NOT_SET; })
        .def_property_readonly_static("kDecodeRandomConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDecodeRandomConf; })
        .def_property_readonly_static("kCopyHdConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyHdConf; })
        .def_property_readonly_static("kCopyCommNetConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyCommNetConf; })
        .def_property_readonly_static("kBoxingConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingConf; })
        .def_property_readonly_static("kVariableConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kVariableConf; })
        .def_property_readonly_static("kTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTickConf; })
        .def_property_readonly_static("kTotalLossInstanceNumConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTotalLossInstanceNumConf; })
        .def_property_readonly_static("kShapeElemCntConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kShapeElemCntConf; })
        .def_property_readonly_static("kSrcSubsetTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSrcSubsetTickConf; })
        .def_property_readonly_static("kDstSubsetTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDstSubsetTickConf; })
        .def_property_readonly_static("kSourceTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSourceTickConf; })
        .def_property_readonly_static("kSinkTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSinkTickConf; })
        .def_property_readonly_static("kInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kInputConf; })
        .def_property_readonly_static("kOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kOutputConf; })
        .def_property_readonly_static("kWaitAndSendIdsConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kWaitAndSendIdsConf; })
        .def_property_readonly_static("kReentrantLockConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReentrantLockConf; })
        .def_property_readonly_static("kCallbackNotifyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCallbackNotifyConf; })
        .def_property_readonly_static("kForeignInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignInputConf; })
        .def_property_readonly_static("kForeignOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignOutputConf; })
        .def_property_readonly_static("kAccTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAccTickConf; })
        .def_property_readonly_static("kReturnConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReturnConf; })
        .def_property_readonly_static("kForeignWatchConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignWatchConf; })
        .def_property_readonly_static("kDistributeConcatConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeConcatConf; })
        .def_property_readonly_static("kDistributeSplitConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeSplitConf; })
        .def_property_readonly_static("kDistributeCloneConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeCloneConf; })
        .def_property_readonly_static("kDistributeAddConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeAddConf; })
        .def_property_readonly_static("kDeviceTickConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDeviceTickConf; })
        .def_property_readonly_static("kSliceBoxingCopyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingCopyConf; })
        .def_property_readonly_static("kSliceBoxingAddConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingAddConf; })
        .def_property_readonly_static("kCollectiveBoxingGenericConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingGenericConf; })
        .def_property_readonly_static("kBoxingIdentityConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingIdentityConf; })
        .def_property_readonly_static("kCollectiveBoxingPackConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingPackConf; })
        .def_property_readonly_static("kCollectiveBoxingUnpackConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingUnpackConf; })
        .def_property_readonly_static("kBoxingZerosConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingZerosConf; })
        .def_property_readonly_static("kUserConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kUserConf; })
        .def_property_readonly_static("kDynamicReshapeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeConf; })
        .def_property_readonly_static("kDynamicReshapeLikeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeLikeConf; })
        .def_property_readonly_static("kIdentityConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kIdentityConf; })
        .def_property_readonly_static("kCaseConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCaseConf; })
        .def_property_readonly_static("kEsacConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kEsacConf; })
        .def_property_readonly_static("kModelInitConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitConf; })
        .def_property_readonly_static("kAssignConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAssignConf; })
        .def_property_readonly_static("kModelSaveConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveConf; })
        .def_property_readonly_static("kLearningRateScheduleConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kLearningRateScheduleConf; })
        .def_property_readonly_static("kModelLoadConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadConf; })
        .def_property_readonly_static("kConstantLikeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kConstantLikeConf; })
        .def_property_readonly_static("kSyncDynamicResizeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSyncDynamicResizeConf; })
        .def_property_readonly_static("kCopyConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyConf; })
        .def_property_readonly_static("kCastToMirroredConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastToMirroredConf; })
        .def_property_readonly_static("kCastFromMirroredConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastFromMirroredConf; })
        .def_property_readonly_static("kModelInitV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitV2Conf; })
        .def_property_readonly_static("kModelLoadV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadV2Conf; })
        .def_property_readonly_static("kModelSaveV2Conf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveV2Conf; })
        .def_property_readonly_static("kImageDecoderRandomCropResizeConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kImageDecoderRandomCropResizeConf; })
        .def_property_readonly_static("kXrtLaunchConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kXrtLaunchConf; })
        .def_property_readonly_static("kBroadcastToCompatibleWithConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBroadcastToCompatibleWithConf; })
        .def_property_readonly_static("kFeedInputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedInputConf; })
        .def_property_readonly_static("kFeedVariableConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedVariableConf; })
        .def_property_readonly_static("kFetchOutputConf", [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFetchOutputConf; })
        ;


    registry.def("has_name", &::oneflow::cfg::OperatorConf::has_name);
    registry.def("clear_name", &::oneflow::cfg::OperatorConf::clear_name);
    registry.def("name", &::oneflow::cfg::OperatorConf::name);
    registry.def("set_name", &::oneflow::cfg::OperatorConf::set_name);

    registry.def("has_device_tag", &::oneflow::cfg::OperatorConf::has_device_tag);
    registry.def("clear_device_tag", &::oneflow::cfg::OperatorConf::clear_device_tag);
    registry.def("device_tag", &::oneflow::cfg::OperatorConf::device_tag);
    registry.def("set_device_tag", &::oneflow::cfg::OperatorConf::set_device_tag);

    registry.def("ctrl_in_op_name_size", &::oneflow::cfg::OperatorConf::ctrl_in_op_name_size);
    registry.def("clear_ctrl_in_op_name", &::oneflow::cfg::OperatorConf::clear_ctrl_in_op_name);
    registry.def("mutable_ctrl_in_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OperatorConf::*)())&::oneflow::cfg::OperatorConf::shared_mutable_ctrl_in_op_name);
    registry.def("ctrl_in_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OperatorConf::*)() const)&::oneflow::cfg::OperatorConf::shared_const_ctrl_in_op_name);
    registry.def("ctrl_in_op_name", (const ::std::string& (::oneflow::cfg::OperatorConf::*)(::std::size_t) const)&::oneflow::cfg::OperatorConf::ctrl_in_op_name);
    registry.def("add_ctrl_in_op_name", &::oneflow::cfg::OperatorConf::add_ctrl_in_op_name);
    registry.def("set_ctrl_in_op_name", &::oneflow::cfg::OperatorConf::set_ctrl_in_op_name);

    registry.def("has_scope_symbol_id", &::oneflow::cfg::OperatorConf::has_scope_symbol_id);
    registry.def("clear_scope_symbol_id", &::oneflow::cfg::OperatorConf::clear_scope_symbol_id);
    registry.def("scope_symbol_id", &::oneflow::cfg::OperatorConf::scope_symbol_id);
    registry.def("set_scope_symbol_id", &::oneflow::cfg::OperatorConf::set_scope_symbol_id);

    registry.def("has_stream_index_hint", &::oneflow::cfg::OperatorConf::has_stream_index_hint);
    registry.def("clear_stream_index_hint", &::oneflow::cfg::OperatorConf::clear_stream_index_hint);
    registry.def("stream_index_hint", &::oneflow::cfg::OperatorConf::stream_index_hint);
    registry.def("set_stream_index_hint", &::oneflow::cfg::OperatorConf::set_stream_index_hint);

    registry.def("has_pass_tag", &::oneflow::cfg::OperatorConf::has_pass_tag);
    registry.def("clear_pass_tag", &::oneflow::cfg::OperatorConf::clear_pass_tag);
    registry.def("pass_tag", &::oneflow::cfg::OperatorConf::pass_tag);
    registry.def("set_pass_tag", &::oneflow::cfg::OperatorConf::set_pass_tag);

    registry.def("has_decode_random_conf", &::oneflow::cfg::OperatorConf::has_decode_random_conf);
    registry.def("clear_decode_random_conf", &::oneflow::cfg::OperatorConf::clear_decode_random_conf);
    registry.def_property_readonly_static("kDecodeRandomConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDecodeRandomConf; });
    registry.def("decode_random_conf", &::oneflow::cfg::OperatorConf::decode_random_conf);
    registry.def("mutable_decode_random_conf", &::oneflow::cfg::OperatorConf::shared_mutable_decode_random_conf);

    registry.def("has_copy_hd_conf", &::oneflow::cfg::OperatorConf::has_copy_hd_conf);
    registry.def("clear_copy_hd_conf", &::oneflow::cfg::OperatorConf::clear_copy_hd_conf);
    registry.def_property_readonly_static("kCopyHdConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyHdConf; });
    registry.def("copy_hd_conf", &::oneflow::cfg::OperatorConf::copy_hd_conf);
    registry.def("mutable_copy_hd_conf", &::oneflow::cfg::OperatorConf::shared_mutable_copy_hd_conf);

    registry.def("has_copy_comm_net_conf", &::oneflow::cfg::OperatorConf::has_copy_comm_net_conf);
    registry.def("clear_copy_comm_net_conf", &::oneflow::cfg::OperatorConf::clear_copy_comm_net_conf);
    registry.def_property_readonly_static("kCopyCommNetConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyCommNetConf; });
    registry.def("copy_comm_net_conf", &::oneflow::cfg::OperatorConf::copy_comm_net_conf);
    registry.def("mutable_copy_comm_net_conf", &::oneflow::cfg::OperatorConf::shared_mutable_copy_comm_net_conf);

    registry.def("has_boxing_conf", &::oneflow::cfg::OperatorConf::has_boxing_conf);
    registry.def("clear_boxing_conf", &::oneflow::cfg::OperatorConf::clear_boxing_conf);
    registry.def_property_readonly_static("kBoxingConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingConf; });
    registry.def("boxing_conf", &::oneflow::cfg::OperatorConf::boxing_conf);
    registry.def("mutable_boxing_conf", &::oneflow::cfg::OperatorConf::shared_mutable_boxing_conf);

    registry.def("has_variable_conf", &::oneflow::cfg::OperatorConf::has_variable_conf);
    registry.def("clear_variable_conf", &::oneflow::cfg::OperatorConf::clear_variable_conf);
    registry.def_property_readonly_static("kVariableConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kVariableConf; });
    registry.def("variable_conf", &::oneflow::cfg::OperatorConf::variable_conf);
    registry.def("mutable_variable_conf", &::oneflow::cfg::OperatorConf::shared_mutable_variable_conf);

    registry.def("has_tick_conf", &::oneflow::cfg::OperatorConf::has_tick_conf);
    registry.def("clear_tick_conf", &::oneflow::cfg::OperatorConf::clear_tick_conf);
    registry.def_property_readonly_static("kTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTickConf; });
    registry.def("tick_conf", &::oneflow::cfg::OperatorConf::tick_conf);
    registry.def("mutable_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_tick_conf);

    registry.def("has_total_loss_instance_num_conf", &::oneflow::cfg::OperatorConf::has_total_loss_instance_num_conf);
    registry.def("clear_total_loss_instance_num_conf", &::oneflow::cfg::OperatorConf::clear_total_loss_instance_num_conf);
    registry.def_property_readonly_static("kTotalLossInstanceNumConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kTotalLossInstanceNumConf; });
    registry.def("total_loss_instance_num_conf", &::oneflow::cfg::OperatorConf::total_loss_instance_num_conf);
    registry.def("mutable_total_loss_instance_num_conf", &::oneflow::cfg::OperatorConf::shared_mutable_total_loss_instance_num_conf);

    registry.def("has_shape_elem_cnt_conf", &::oneflow::cfg::OperatorConf::has_shape_elem_cnt_conf);
    registry.def("clear_shape_elem_cnt_conf", &::oneflow::cfg::OperatorConf::clear_shape_elem_cnt_conf);
    registry.def_property_readonly_static("kShapeElemCntConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kShapeElemCntConf; });
    registry.def("shape_elem_cnt_conf", &::oneflow::cfg::OperatorConf::shape_elem_cnt_conf);
    registry.def("mutable_shape_elem_cnt_conf", &::oneflow::cfg::OperatorConf::shared_mutable_shape_elem_cnt_conf);

    registry.def("has_src_subset_tick_conf", &::oneflow::cfg::OperatorConf::has_src_subset_tick_conf);
    registry.def("clear_src_subset_tick_conf", &::oneflow::cfg::OperatorConf::clear_src_subset_tick_conf);
    registry.def_property_readonly_static("kSrcSubsetTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSrcSubsetTickConf; });
    registry.def("src_subset_tick_conf", &::oneflow::cfg::OperatorConf::src_subset_tick_conf);
    registry.def("mutable_src_subset_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_src_subset_tick_conf);

    registry.def("has_dst_subset_tick_conf", &::oneflow::cfg::OperatorConf::has_dst_subset_tick_conf);
    registry.def("clear_dst_subset_tick_conf", &::oneflow::cfg::OperatorConf::clear_dst_subset_tick_conf);
    registry.def_property_readonly_static("kDstSubsetTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDstSubsetTickConf; });
    registry.def("dst_subset_tick_conf", &::oneflow::cfg::OperatorConf::dst_subset_tick_conf);
    registry.def("mutable_dst_subset_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_dst_subset_tick_conf);

    registry.def("has_source_tick_conf", &::oneflow::cfg::OperatorConf::has_source_tick_conf);
    registry.def("clear_source_tick_conf", &::oneflow::cfg::OperatorConf::clear_source_tick_conf);
    registry.def_property_readonly_static("kSourceTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSourceTickConf; });
    registry.def("source_tick_conf", &::oneflow::cfg::OperatorConf::source_tick_conf);
    registry.def("mutable_source_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_source_tick_conf);

    registry.def("has_sink_tick_conf", &::oneflow::cfg::OperatorConf::has_sink_tick_conf);
    registry.def("clear_sink_tick_conf", &::oneflow::cfg::OperatorConf::clear_sink_tick_conf);
    registry.def_property_readonly_static("kSinkTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSinkTickConf; });
    registry.def("sink_tick_conf", &::oneflow::cfg::OperatorConf::sink_tick_conf);
    registry.def("mutable_sink_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_sink_tick_conf);

    registry.def("has_input_conf", &::oneflow::cfg::OperatorConf::has_input_conf);
    registry.def("clear_input_conf", &::oneflow::cfg::OperatorConf::clear_input_conf);
    registry.def_property_readonly_static("kInputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kInputConf; });
    registry.def("input_conf", &::oneflow::cfg::OperatorConf::input_conf);
    registry.def("mutable_input_conf", &::oneflow::cfg::OperatorConf::shared_mutable_input_conf);

    registry.def("has_output_conf", &::oneflow::cfg::OperatorConf::has_output_conf);
    registry.def("clear_output_conf", &::oneflow::cfg::OperatorConf::clear_output_conf);
    registry.def_property_readonly_static("kOutputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kOutputConf; });
    registry.def("output_conf", &::oneflow::cfg::OperatorConf::output_conf);
    registry.def("mutable_output_conf", &::oneflow::cfg::OperatorConf::shared_mutable_output_conf);

    registry.def("has_wait_and_send_ids_conf", &::oneflow::cfg::OperatorConf::has_wait_and_send_ids_conf);
    registry.def("clear_wait_and_send_ids_conf", &::oneflow::cfg::OperatorConf::clear_wait_and_send_ids_conf);
    registry.def_property_readonly_static("kWaitAndSendIdsConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kWaitAndSendIdsConf; });
    registry.def("wait_and_send_ids_conf", &::oneflow::cfg::OperatorConf::wait_and_send_ids_conf);
    registry.def("mutable_wait_and_send_ids_conf", &::oneflow::cfg::OperatorConf::shared_mutable_wait_and_send_ids_conf);

    registry.def("has_reentrant_lock_conf", &::oneflow::cfg::OperatorConf::has_reentrant_lock_conf);
    registry.def("clear_reentrant_lock_conf", &::oneflow::cfg::OperatorConf::clear_reentrant_lock_conf);
    registry.def_property_readonly_static("kReentrantLockConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReentrantLockConf; });
    registry.def("reentrant_lock_conf", &::oneflow::cfg::OperatorConf::reentrant_lock_conf);
    registry.def("mutable_reentrant_lock_conf", &::oneflow::cfg::OperatorConf::shared_mutable_reentrant_lock_conf);

    registry.def("has_callback_notify_conf", &::oneflow::cfg::OperatorConf::has_callback_notify_conf);
    registry.def("clear_callback_notify_conf", &::oneflow::cfg::OperatorConf::clear_callback_notify_conf);
    registry.def_property_readonly_static("kCallbackNotifyConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCallbackNotifyConf; });
    registry.def("callback_notify_conf", &::oneflow::cfg::OperatorConf::callback_notify_conf);
    registry.def("mutable_callback_notify_conf", &::oneflow::cfg::OperatorConf::shared_mutable_callback_notify_conf);

    registry.def("has_foreign_input_conf", &::oneflow::cfg::OperatorConf::has_foreign_input_conf);
    registry.def("clear_foreign_input_conf", &::oneflow::cfg::OperatorConf::clear_foreign_input_conf);
    registry.def_property_readonly_static("kForeignInputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignInputConf; });
    registry.def("foreign_input_conf", &::oneflow::cfg::OperatorConf::foreign_input_conf);
    registry.def("mutable_foreign_input_conf", &::oneflow::cfg::OperatorConf::shared_mutable_foreign_input_conf);

    registry.def("has_foreign_output_conf", &::oneflow::cfg::OperatorConf::has_foreign_output_conf);
    registry.def("clear_foreign_output_conf", &::oneflow::cfg::OperatorConf::clear_foreign_output_conf);
    registry.def_property_readonly_static("kForeignOutputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignOutputConf; });
    registry.def("foreign_output_conf", &::oneflow::cfg::OperatorConf::foreign_output_conf);
    registry.def("mutable_foreign_output_conf", &::oneflow::cfg::OperatorConf::shared_mutable_foreign_output_conf);

    registry.def("has_acc_tick_conf", &::oneflow::cfg::OperatorConf::has_acc_tick_conf);
    registry.def("clear_acc_tick_conf", &::oneflow::cfg::OperatorConf::clear_acc_tick_conf);
    registry.def_property_readonly_static("kAccTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAccTickConf; });
    registry.def("acc_tick_conf", &::oneflow::cfg::OperatorConf::acc_tick_conf);
    registry.def("mutable_acc_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_acc_tick_conf);

    registry.def("has_return_conf", &::oneflow::cfg::OperatorConf::has_return_conf);
    registry.def("clear_return_conf", &::oneflow::cfg::OperatorConf::clear_return_conf);
    registry.def_property_readonly_static("kReturnConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kReturnConf; });
    registry.def("return_conf", &::oneflow::cfg::OperatorConf::return_conf);
    registry.def("mutable_return_conf", &::oneflow::cfg::OperatorConf::shared_mutable_return_conf);

    registry.def("has_foreign_watch_conf", &::oneflow::cfg::OperatorConf::has_foreign_watch_conf);
    registry.def("clear_foreign_watch_conf", &::oneflow::cfg::OperatorConf::clear_foreign_watch_conf);
    registry.def_property_readonly_static("kForeignWatchConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kForeignWatchConf; });
    registry.def("foreign_watch_conf", &::oneflow::cfg::OperatorConf::foreign_watch_conf);
    registry.def("mutable_foreign_watch_conf", &::oneflow::cfg::OperatorConf::shared_mutable_foreign_watch_conf);

    registry.def("has_distribute_concat_conf", &::oneflow::cfg::OperatorConf::has_distribute_concat_conf);
    registry.def("clear_distribute_concat_conf", &::oneflow::cfg::OperatorConf::clear_distribute_concat_conf);
    registry.def_property_readonly_static("kDistributeConcatConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeConcatConf; });
    registry.def("distribute_concat_conf", &::oneflow::cfg::OperatorConf::distribute_concat_conf);
    registry.def("mutable_distribute_concat_conf", &::oneflow::cfg::OperatorConf::shared_mutable_distribute_concat_conf);

    registry.def("has_distribute_split_conf", &::oneflow::cfg::OperatorConf::has_distribute_split_conf);
    registry.def("clear_distribute_split_conf", &::oneflow::cfg::OperatorConf::clear_distribute_split_conf);
    registry.def_property_readonly_static("kDistributeSplitConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeSplitConf; });
    registry.def("distribute_split_conf", &::oneflow::cfg::OperatorConf::distribute_split_conf);
    registry.def("mutable_distribute_split_conf", &::oneflow::cfg::OperatorConf::shared_mutable_distribute_split_conf);

    registry.def("has_distribute_clone_conf", &::oneflow::cfg::OperatorConf::has_distribute_clone_conf);
    registry.def("clear_distribute_clone_conf", &::oneflow::cfg::OperatorConf::clear_distribute_clone_conf);
    registry.def_property_readonly_static("kDistributeCloneConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeCloneConf; });
    registry.def("distribute_clone_conf", &::oneflow::cfg::OperatorConf::distribute_clone_conf);
    registry.def("mutable_distribute_clone_conf", &::oneflow::cfg::OperatorConf::shared_mutable_distribute_clone_conf);

    registry.def("has_distribute_add_conf", &::oneflow::cfg::OperatorConf::has_distribute_add_conf);
    registry.def("clear_distribute_add_conf", &::oneflow::cfg::OperatorConf::clear_distribute_add_conf);
    registry.def_property_readonly_static("kDistributeAddConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDistributeAddConf; });
    registry.def("distribute_add_conf", &::oneflow::cfg::OperatorConf::distribute_add_conf);
    registry.def("mutable_distribute_add_conf", &::oneflow::cfg::OperatorConf::shared_mutable_distribute_add_conf);

    registry.def("has_device_tick_conf", &::oneflow::cfg::OperatorConf::has_device_tick_conf);
    registry.def("clear_device_tick_conf", &::oneflow::cfg::OperatorConf::clear_device_tick_conf);
    registry.def_property_readonly_static("kDeviceTickConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDeviceTickConf; });
    registry.def("device_tick_conf", &::oneflow::cfg::OperatorConf::device_tick_conf);
    registry.def("mutable_device_tick_conf", &::oneflow::cfg::OperatorConf::shared_mutable_device_tick_conf);

    registry.def("has_slice_boxing_copy_conf", &::oneflow::cfg::OperatorConf::has_slice_boxing_copy_conf);
    registry.def("clear_slice_boxing_copy_conf", &::oneflow::cfg::OperatorConf::clear_slice_boxing_copy_conf);
    registry.def_property_readonly_static("kSliceBoxingCopyConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingCopyConf; });
    registry.def("slice_boxing_copy_conf", &::oneflow::cfg::OperatorConf::slice_boxing_copy_conf);
    registry.def("mutable_slice_boxing_copy_conf", &::oneflow::cfg::OperatorConf::shared_mutable_slice_boxing_copy_conf);

    registry.def("has_slice_boxing_add_conf", &::oneflow::cfg::OperatorConf::has_slice_boxing_add_conf);
    registry.def("clear_slice_boxing_add_conf", &::oneflow::cfg::OperatorConf::clear_slice_boxing_add_conf);
    registry.def_property_readonly_static("kSliceBoxingAddConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSliceBoxingAddConf; });
    registry.def("slice_boxing_add_conf", &::oneflow::cfg::OperatorConf::slice_boxing_add_conf);
    registry.def("mutable_slice_boxing_add_conf", &::oneflow::cfg::OperatorConf::shared_mutable_slice_boxing_add_conf);

    registry.def("has_collective_boxing_generic_conf", &::oneflow::cfg::OperatorConf::has_collective_boxing_generic_conf);
    registry.def("clear_collective_boxing_generic_conf", &::oneflow::cfg::OperatorConf::clear_collective_boxing_generic_conf);
    registry.def_property_readonly_static("kCollectiveBoxingGenericConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingGenericConf; });
    registry.def("collective_boxing_generic_conf", &::oneflow::cfg::OperatorConf::collective_boxing_generic_conf);
    registry.def("mutable_collective_boxing_generic_conf", &::oneflow::cfg::OperatorConf::shared_mutable_collective_boxing_generic_conf);

    registry.def("has_boxing_identity_conf", &::oneflow::cfg::OperatorConf::has_boxing_identity_conf);
    registry.def("clear_boxing_identity_conf", &::oneflow::cfg::OperatorConf::clear_boxing_identity_conf);
    registry.def_property_readonly_static("kBoxingIdentityConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingIdentityConf; });
    registry.def("boxing_identity_conf", &::oneflow::cfg::OperatorConf::boxing_identity_conf);
    registry.def("mutable_boxing_identity_conf", &::oneflow::cfg::OperatorConf::shared_mutable_boxing_identity_conf);

    registry.def("has_collective_boxing_pack_conf", &::oneflow::cfg::OperatorConf::has_collective_boxing_pack_conf);
    registry.def("clear_collective_boxing_pack_conf", &::oneflow::cfg::OperatorConf::clear_collective_boxing_pack_conf);
    registry.def_property_readonly_static("kCollectiveBoxingPackConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingPackConf; });
    registry.def("collective_boxing_pack_conf", &::oneflow::cfg::OperatorConf::collective_boxing_pack_conf);
    registry.def("mutable_collective_boxing_pack_conf", &::oneflow::cfg::OperatorConf::shared_mutable_collective_boxing_pack_conf);

    registry.def("has_collective_boxing_unpack_conf", &::oneflow::cfg::OperatorConf::has_collective_boxing_unpack_conf);
    registry.def("clear_collective_boxing_unpack_conf", &::oneflow::cfg::OperatorConf::clear_collective_boxing_unpack_conf);
    registry.def_property_readonly_static("kCollectiveBoxingUnpackConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCollectiveBoxingUnpackConf; });
    registry.def("collective_boxing_unpack_conf", &::oneflow::cfg::OperatorConf::collective_boxing_unpack_conf);
    registry.def("mutable_collective_boxing_unpack_conf", &::oneflow::cfg::OperatorConf::shared_mutable_collective_boxing_unpack_conf);

    registry.def("has_boxing_zeros_conf", &::oneflow::cfg::OperatorConf::has_boxing_zeros_conf);
    registry.def("clear_boxing_zeros_conf", &::oneflow::cfg::OperatorConf::clear_boxing_zeros_conf);
    registry.def_property_readonly_static("kBoxingZerosConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBoxingZerosConf; });
    registry.def("boxing_zeros_conf", &::oneflow::cfg::OperatorConf::boxing_zeros_conf);
    registry.def("mutable_boxing_zeros_conf", &::oneflow::cfg::OperatorConf::shared_mutable_boxing_zeros_conf);

    registry.def("has_user_conf", &::oneflow::cfg::OperatorConf::has_user_conf);
    registry.def("clear_user_conf", &::oneflow::cfg::OperatorConf::clear_user_conf);
    registry.def_property_readonly_static("kUserConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kUserConf; });
    registry.def("user_conf", &::oneflow::cfg::OperatorConf::user_conf);
    registry.def("mutable_user_conf", &::oneflow::cfg::OperatorConf::shared_mutable_user_conf);

    registry.def("has_dynamic_reshape_conf", &::oneflow::cfg::OperatorConf::has_dynamic_reshape_conf);
    registry.def("clear_dynamic_reshape_conf", &::oneflow::cfg::OperatorConf::clear_dynamic_reshape_conf);
    registry.def_property_readonly_static("kDynamicReshapeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeConf; });
    registry.def("dynamic_reshape_conf", &::oneflow::cfg::OperatorConf::dynamic_reshape_conf);
    registry.def("mutable_dynamic_reshape_conf", &::oneflow::cfg::OperatorConf::shared_mutable_dynamic_reshape_conf);

    registry.def("has_dynamic_reshape_like_conf", &::oneflow::cfg::OperatorConf::has_dynamic_reshape_like_conf);
    registry.def("clear_dynamic_reshape_like_conf", &::oneflow::cfg::OperatorConf::clear_dynamic_reshape_like_conf);
    registry.def_property_readonly_static("kDynamicReshapeLikeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kDynamicReshapeLikeConf; });
    registry.def("dynamic_reshape_like_conf", &::oneflow::cfg::OperatorConf::dynamic_reshape_like_conf);
    registry.def("mutable_dynamic_reshape_like_conf", &::oneflow::cfg::OperatorConf::shared_mutable_dynamic_reshape_like_conf);

    registry.def("has_identity_conf", &::oneflow::cfg::OperatorConf::has_identity_conf);
    registry.def("clear_identity_conf", &::oneflow::cfg::OperatorConf::clear_identity_conf);
    registry.def_property_readonly_static("kIdentityConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kIdentityConf; });
    registry.def("identity_conf", &::oneflow::cfg::OperatorConf::identity_conf);
    registry.def("mutable_identity_conf", &::oneflow::cfg::OperatorConf::shared_mutable_identity_conf);

    registry.def("has_case_conf", &::oneflow::cfg::OperatorConf::has_case_conf);
    registry.def("clear_case_conf", &::oneflow::cfg::OperatorConf::clear_case_conf);
    registry.def_property_readonly_static("kCaseConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCaseConf; });
    registry.def("case_conf", &::oneflow::cfg::OperatorConf::case_conf);
    registry.def("mutable_case_conf", &::oneflow::cfg::OperatorConf::shared_mutable_case_conf);

    registry.def("has_esac_conf", &::oneflow::cfg::OperatorConf::has_esac_conf);
    registry.def("clear_esac_conf", &::oneflow::cfg::OperatorConf::clear_esac_conf);
    registry.def_property_readonly_static("kEsacConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kEsacConf; });
    registry.def("esac_conf", &::oneflow::cfg::OperatorConf::esac_conf);
    registry.def("mutable_esac_conf", &::oneflow::cfg::OperatorConf::shared_mutable_esac_conf);

    registry.def("has_model_init_conf", &::oneflow::cfg::OperatorConf::has_model_init_conf);
    registry.def("clear_model_init_conf", &::oneflow::cfg::OperatorConf::clear_model_init_conf);
    registry.def_property_readonly_static("kModelInitConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitConf; });
    registry.def("model_init_conf", &::oneflow::cfg::OperatorConf::model_init_conf);
    registry.def("mutable_model_init_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_init_conf);

    registry.def("has_assign_conf", &::oneflow::cfg::OperatorConf::has_assign_conf);
    registry.def("clear_assign_conf", &::oneflow::cfg::OperatorConf::clear_assign_conf);
    registry.def_property_readonly_static("kAssignConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kAssignConf; });
    registry.def("assign_conf", &::oneflow::cfg::OperatorConf::assign_conf);
    registry.def("mutable_assign_conf", &::oneflow::cfg::OperatorConf::shared_mutable_assign_conf);

    registry.def("has_model_save_conf", &::oneflow::cfg::OperatorConf::has_model_save_conf);
    registry.def("clear_model_save_conf", &::oneflow::cfg::OperatorConf::clear_model_save_conf);
    registry.def_property_readonly_static("kModelSaveConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveConf; });
    registry.def("model_save_conf", &::oneflow::cfg::OperatorConf::model_save_conf);
    registry.def("mutable_model_save_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_save_conf);

    registry.def("has_learning_rate_schedule_conf", &::oneflow::cfg::OperatorConf::has_learning_rate_schedule_conf);
    registry.def("clear_learning_rate_schedule_conf", &::oneflow::cfg::OperatorConf::clear_learning_rate_schedule_conf);
    registry.def_property_readonly_static("kLearningRateScheduleConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kLearningRateScheduleConf; });
    registry.def("learning_rate_schedule_conf", &::oneflow::cfg::OperatorConf::learning_rate_schedule_conf);
    registry.def("mutable_learning_rate_schedule_conf", &::oneflow::cfg::OperatorConf::shared_mutable_learning_rate_schedule_conf);

    registry.def("has_model_load_conf", &::oneflow::cfg::OperatorConf::has_model_load_conf);
    registry.def("clear_model_load_conf", &::oneflow::cfg::OperatorConf::clear_model_load_conf);
    registry.def_property_readonly_static("kModelLoadConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadConf; });
    registry.def("model_load_conf", &::oneflow::cfg::OperatorConf::model_load_conf);
    registry.def("mutable_model_load_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_load_conf);

    registry.def("has_constant_like_conf", &::oneflow::cfg::OperatorConf::has_constant_like_conf);
    registry.def("clear_constant_like_conf", &::oneflow::cfg::OperatorConf::clear_constant_like_conf);
    registry.def_property_readonly_static("kConstantLikeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kConstantLikeConf; });
    registry.def("constant_like_conf", &::oneflow::cfg::OperatorConf::constant_like_conf);
    registry.def("mutable_constant_like_conf", &::oneflow::cfg::OperatorConf::shared_mutable_constant_like_conf);

    registry.def("has_sync_dynamic_resize_conf", &::oneflow::cfg::OperatorConf::has_sync_dynamic_resize_conf);
    registry.def("clear_sync_dynamic_resize_conf", &::oneflow::cfg::OperatorConf::clear_sync_dynamic_resize_conf);
    registry.def_property_readonly_static("kSyncDynamicResizeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kSyncDynamicResizeConf; });
    registry.def("sync_dynamic_resize_conf", &::oneflow::cfg::OperatorConf::sync_dynamic_resize_conf);
    registry.def("mutable_sync_dynamic_resize_conf", &::oneflow::cfg::OperatorConf::shared_mutable_sync_dynamic_resize_conf);

    registry.def("has_copy_conf", &::oneflow::cfg::OperatorConf::has_copy_conf);
    registry.def("clear_copy_conf", &::oneflow::cfg::OperatorConf::clear_copy_conf);
    registry.def_property_readonly_static("kCopyConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCopyConf; });
    registry.def("copy_conf", &::oneflow::cfg::OperatorConf::copy_conf);
    registry.def("mutable_copy_conf", &::oneflow::cfg::OperatorConf::shared_mutable_copy_conf);

    registry.def("has_cast_to_mirrored_conf", &::oneflow::cfg::OperatorConf::has_cast_to_mirrored_conf);
    registry.def("clear_cast_to_mirrored_conf", &::oneflow::cfg::OperatorConf::clear_cast_to_mirrored_conf);
    registry.def_property_readonly_static("kCastToMirroredConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastToMirroredConf; });
    registry.def("cast_to_mirrored_conf", &::oneflow::cfg::OperatorConf::cast_to_mirrored_conf);
    registry.def("mutable_cast_to_mirrored_conf", &::oneflow::cfg::OperatorConf::shared_mutable_cast_to_mirrored_conf);

    registry.def("has_cast_from_mirrored_conf", &::oneflow::cfg::OperatorConf::has_cast_from_mirrored_conf);
    registry.def("clear_cast_from_mirrored_conf", &::oneflow::cfg::OperatorConf::clear_cast_from_mirrored_conf);
    registry.def_property_readonly_static("kCastFromMirroredConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kCastFromMirroredConf; });
    registry.def("cast_from_mirrored_conf", &::oneflow::cfg::OperatorConf::cast_from_mirrored_conf);
    registry.def("mutable_cast_from_mirrored_conf", &::oneflow::cfg::OperatorConf::shared_mutable_cast_from_mirrored_conf);

    registry.def("has_model_init_v2_conf", &::oneflow::cfg::OperatorConf::has_model_init_v2_conf);
    registry.def("clear_model_init_v2_conf", &::oneflow::cfg::OperatorConf::clear_model_init_v2_conf);
    registry.def_property_readonly_static("kModelInitV2Conf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelInitV2Conf; });
    registry.def("model_init_v2_conf", &::oneflow::cfg::OperatorConf::model_init_v2_conf);
    registry.def("mutable_model_init_v2_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_init_v2_conf);

    registry.def("has_model_load_v2_conf", &::oneflow::cfg::OperatorConf::has_model_load_v2_conf);
    registry.def("clear_model_load_v2_conf", &::oneflow::cfg::OperatorConf::clear_model_load_v2_conf);
    registry.def_property_readonly_static("kModelLoadV2Conf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelLoadV2Conf; });
    registry.def("model_load_v2_conf", &::oneflow::cfg::OperatorConf::model_load_v2_conf);
    registry.def("mutable_model_load_v2_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_load_v2_conf);

    registry.def("has_model_save_v2_conf", &::oneflow::cfg::OperatorConf::has_model_save_v2_conf);
    registry.def("clear_model_save_v2_conf", &::oneflow::cfg::OperatorConf::clear_model_save_v2_conf);
    registry.def_property_readonly_static("kModelSaveV2Conf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kModelSaveV2Conf; });
    registry.def("model_save_v2_conf", &::oneflow::cfg::OperatorConf::model_save_v2_conf);
    registry.def("mutable_model_save_v2_conf", &::oneflow::cfg::OperatorConf::shared_mutable_model_save_v2_conf);

    registry.def("has_image_decoder_random_crop_resize_conf", &::oneflow::cfg::OperatorConf::has_image_decoder_random_crop_resize_conf);
    registry.def("clear_image_decoder_random_crop_resize_conf", &::oneflow::cfg::OperatorConf::clear_image_decoder_random_crop_resize_conf);
    registry.def_property_readonly_static("kImageDecoderRandomCropResizeConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kImageDecoderRandomCropResizeConf; });
    registry.def("image_decoder_random_crop_resize_conf", &::oneflow::cfg::OperatorConf::image_decoder_random_crop_resize_conf);
    registry.def("mutable_image_decoder_random_crop_resize_conf", &::oneflow::cfg::OperatorConf::shared_mutable_image_decoder_random_crop_resize_conf);

    registry.def("has_xrt_launch_conf", &::oneflow::cfg::OperatorConf::has_xrt_launch_conf);
    registry.def("clear_xrt_launch_conf", &::oneflow::cfg::OperatorConf::clear_xrt_launch_conf);
    registry.def_property_readonly_static("kXrtLaunchConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kXrtLaunchConf; });
    registry.def("xrt_launch_conf", &::oneflow::cfg::OperatorConf::xrt_launch_conf);
    registry.def("mutable_xrt_launch_conf", &::oneflow::cfg::OperatorConf::shared_mutable_xrt_launch_conf);

    registry.def("has_broadcast_to_compatible_with_conf", &::oneflow::cfg::OperatorConf::has_broadcast_to_compatible_with_conf);
    registry.def("clear_broadcast_to_compatible_with_conf", &::oneflow::cfg::OperatorConf::clear_broadcast_to_compatible_with_conf);
    registry.def_property_readonly_static("kBroadcastToCompatibleWithConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kBroadcastToCompatibleWithConf; });
    registry.def("broadcast_to_compatible_with_conf", &::oneflow::cfg::OperatorConf::broadcast_to_compatible_with_conf);
    registry.def("mutable_broadcast_to_compatible_with_conf", &::oneflow::cfg::OperatorConf::shared_mutable_broadcast_to_compatible_with_conf);

    registry.def("has_feed_input_conf", &::oneflow::cfg::OperatorConf::has_feed_input_conf);
    registry.def("clear_feed_input_conf", &::oneflow::cfg::OperatorConf::clear_feed_input_conf);
    registry.def_property_readonly_static("kFeedInputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedInputConf; });
    registry.def("feed_input_conf", &::oneflow::cfg::OperatorConf::feed_input_conf);
    registry.def("mutable_feed_input_conf", &::oneflow::cfg::OperatorConf::shared_mutable_feed_input_conf);

    registry.def("has_feed_variable_conf", &::oneflow::cfg::OperatorConf::has_feed_variable_conf);
    registry.def("clear_feed_variable_conf", &::oneflow::cfg::OperatorConf::clear_feed_variable_conf);
    registry.def_property_readonly_static("kFeedVariableConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFeedVariableConf; });
    registry.def("feed_variable_conf", &::oneflow::cfg::OperatorConf::feed_variable_conf);
    registry.def("mutable_feed_variable_conf", &::oneflow::cfg::OperatorConf::shared_mutable_feed_variable_conf);

    registry.def("has_fetch_output_conf", &::oneflow::cfg::OperatorConf::has_fetch_output_conf);
    registry.def("clear_fetch_output_conf", &::oneflow::cfg::OperatorConf::clear_fetch_output_conf);
    registry.def_property_readonly_static("kFetchOutputConf",
        [](const pybind11::object&){ return ::oneflow::cfg::OperatorConf::kFetchOutputConf; });
    registry.def("fetch_output_conf", &::oneflow::cfg::OperatorConf::fetch_output_conf);
    registry.def("mutable_fetch_output_conf", &::oneflow::cfg::OperatorConf::shared_mutable_fetch_output_conf);
    pybind11::enum_<::oneflow::cfg::OperatorConf::OpTypeCase>(registry, "OpTypeCase")
        .value("OP_TYPE_NOT_SET", ::oneflow::cfg::OperatorConf::OP_TYPE_NOT_SET)
        .value("kDecodeRandomConf", ::oneflow::cfg::OperatorConf::kDecodeRandomConf)
        .value("kCopyHdConf", ::oneflow::cfg::OperatorConf::kCopyHdConf)
        .value("kCopyCommNetConf", ::oneflow::cfg::OperatorConf::kCopyCommNetConf)
        .value("kBoxingConf", ::oneflow::cfg::OperatorConf::kBoxingConf)
        .value("kVariableConf", ::oneflow::cfg::OperatorConf::kVariableConf)
        .value("kTickConf", ::oneflow::cfg::OperatorConf::kTickConf)
        .value("kTotalLossInstanceNumConf", ::oneflow::cfg::OperatorConf::kTotalLossInstanceNumConf)
        .value("kShapeElemCntConf", ::oneflow::cfg::OperatorConf::kShapeElemCntConf)
        .value("kSrcSubsetTickConf", ::oneflow::cfg::OperatorConf::kSrcSubsetTickConf)
        .value("kDstSubsetTickConf", ::oneflow::cfg::OperatorConf::kDstSubsetTickConf)
        .value("kSourceTickConf", ::oneflow::cfg::OperatorConf::kSourceTickConf)
        .value("kSinkTickConf", ::oneflow::cfg::OperatorConf::kSinkTickConf)
        .value("kInputConf", ::oneflow::cfg::OperatorConf::kInputConf)
        .value("kOutputConf", ::oneflow::cfg::OperatorConf::kOutputConf)
        .value("kWaitAndSendIdsConf", ::oneflow::cfg::OperatorConf::kWaitAndSendIdsConf)
        .value("kReentrantLockConf", ::oneflow::cfg::OperatorConf::kReentrantLockConf)
        .value("kCallbackNotifyConf", ::oneflow::cfg::OperatorConf::kCallbackNotifyConf)
        .value("kForeignInputConf", ::oneflow::cfg::OperatorConf::kForeignInputConf)
        .value("kForeignOutputConf", ::oneflow::cfg::OperatorConf::kForeignOutputConf)
        .value("kAccTickConf", ::oneflow::cfg::OperatorConf::kAccTickConf)
        .value("kReturnConf", ::oneflow::cfg::OperatorConf::kReturnConf)
        .value("kForeignWatchConf", ::oneflow::cfg::OperatorConf::kForeignWatchConf)
        .value("kDistributeConcatConf", ::oneflow::cfg::OperatorConf::kDistributeConcatConf)
        .value("kDistributeSplitConf", ::oneflow::cfg::OperatorConf::kDistributeSplitConf)
        .value("kDistributeCloneConf", ::oneflow::cfg::OperatorConf::kDistributeCloneConf)
        .value("kDistributeAddConf", ::oneflow::cfg::OperatorConf::kDistributeAddConf)
        .value("kDeviceTickConf", ::oneflow::cfg::OperatorConf::kDeviceTickConf)
        .value("kSliceBoxingCopyConf", ::oneflow::cfg::OperatorConf::kSliceBoxingCopyConf)
        .value("kSliceBoxingAddConf", ::oneflow::cfg::OperatorConf::kSliceBoxingAddConf)
        .value("kCollectiveBoxingGenericConf", ::oneflow::cfg::OperatorConf::kCollectiveBoxingGenericConf)
        .value("kBoxingIdentityConf", ::oneflow::cfg::OperatorConf::kBoxingIdentityConf)
        .value("kCollectiveBoxingPackConf", ::oneflow::cfg::OperatorConf::kCollectiveBoxingPackConf)
        .value("kCollectiveBoxingUnpackConf", ::oneflow::cfg::OperatorConf::kCollectiveBoxingUnpackConf)
        .value("kBoxingZerosConf", ::oneflow::cfg::OperatorConf::kBoxingZerosConf)
        .value("kUserConf", ::oneflow::cfg::OperatorConf::kUserConf)
        .value("kDynamicReshapeConf", ::oneflow::cfg::OperatorConf::kDynamicReshapeConf)
        .value("kDynamicReshapeLikeConf", ::oneflow::cfg::OperatorConf::kDynamicReshapeLikeConf)
        .value("kIdentityConf", ::oneflow::cfg::OperatorConf::kIdentityConf)
        .value("kCaseConf", ::oneflow::cfg::OperatorConf::kCaseConf)
        .value("kEsacConf", ::oneflow::cfg::OperatorConf::kEsacConf)
        .value("kModelInitConf", ::oneflow::cfg::OperatorConf::kModelInitConf)
        .value("kAssignConf", ::oneflow::cfg::OperatorConf::kAssignConf)
        .value("kModelSaveConf", ::oneflow::cfg::OperatorConf::kModelSaveConf)
        .value("kLearningRateScheduleConf", ::oneflow::cfg::OperatorConf::kLearningRateScheduleConf)
        .value("kModelLoadConf", ::oneflow::cfg::OperatorConf::kModelLoadConf)
        .value("kConstantLikeConf", ::oneflow::cfg::OperatorConf::kConstantLikeConf)
        .value("kSyncDynamicResizeConf", ::oneflow::cfg::OperatorConf::kSyncDynamicResizeConf)
        .value("kCopyConf", ::oneflow::cfg::OperatorConf::kCopyConf)
        .value("kCastToMirroredConf", ::oneflow::cfg::OperatorConf::kCastToMirroredConf)
        .value("kCastFromMirroredConf", ::oneflow::cfg::OperatorConf::kCastFromMirroredConf)
        .value("kModelInitV2Conf", ::oneflow::cfg::OperatorConf::kModelInitV2Conf)
        .value("kModelLoadV2Conf", ::oneflow::cfg::OperatorConf::kModelLoadV2Conf)
        .value("kModelSaveV2Conf", ::oneflow::cfg::OperatorConf::kModelSaveV2Conf)
        .value("kImageDecoderRandomCropResizeConf", ::oneflow::cfg::OperatorConf::kImageDecoderRandomCropResizeConf)
        .value("kXrtLaunchConf", ::oneflow::cfg::OperatorConf::kXrtLaunchConf)
        .value("kBroadcastToCompatibleWithConf", ::oneflow::cfg::OperatorConf::kBroadcastToCompatibleWithConf)
        .value("kFeedInputConf", ::oneflow::cfg::OperatorConf::kFeedInputConf)
        .value("kFeedVariableConf", ::oneflow::cfg::OperatorConf::kFeedVariableConf)
        .value("kFetchOutputConf", ::oneflow::cfg::OperatorConf::kFetchOutputConf)
        ;
    registry.def("op_type_case",  &::oneflow::cfg::OperatorConf::op_type_case);
  }
  {
    pybind11::class_<ConstOpNameRelations, ::oneflow::cfg::Message, std::shared_ptr<ConstOpNameRelations>> registry(m, "ConstOpNameRelations");
    registry.def("__id__", &::oneflow::cfg::OpNameRelations::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpNameRelations::DebugString);
    registry.def("__repr__", &ConstOpNameRelations::DebugString);

    registry.def("src_op_name2dst_op_name_size", &ConstOpNameRelations::src_op_name2dst_op_name_size);
    registry.def("src_op_name2dst_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (ConstOpNameRelations::*)() const)&ConstOpNameRelations::shared_const_src_op_name2dst_op_name);

    registry.def("src_op_name2dst_op_name", (const ::std::string& (ConstOpNameRelations::*)(const ::std::string&) const)&ConstOpNameRelations::src_op_name2dst_op_name);
  }
  {
    pybind11::class_<::oneflow::cfg::OpNameRelations, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpNameRelations>> registry(m, "OpNameRelations");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpNameRelations::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameRelations::*)(const ConstOpNameRelations&))&::oneflow::cfg::OpNameRelations::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameRelations::*)(const ::oneflow::cfg::OpNameRelations&))&::oneflow::cfg::OpNameRelations::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpNameRelations::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpNameRelations::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpNameRelations::DebugString);



    registry.def("src_op_name2dst_op_name_size", &::oneflow::cfg::OpNameRelations::src_op_name2dst_op_name_size);
    registry.def("src_op_name2dst_op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (::oneflow::cfg::OpNameRelations::*)() const)&::oneflow::cfg::OpNameRelations::shared_const_src_op_name2dst_op_name);
    registry.def("clear_src_op_name2dst_op_name", &::oneflow::cfg::OpNameRelations::clear_src_op_name2dst_op_name);
    registry.def("mutable_src_op_name2dst_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> (::oneflow::cfg::OpNameRelations::*)())&::oneflow::cfg::OpNameRelations::shared_mutable_src_op_name2dst_op_name);
    registry.def("src_op_name2dst_op_name", (const ::std::string& (::oneflow::cfg::OpNameRelations::*)(const ::std::string&) const)&::oneflow::cfg::OpNameRelations::src_op_name2dst_op_name);
  }
  {
    pybind11::class_<ConstOpNameGroups_OpNameGroup, ::oneflow::cfg::Message, std::shared_ptr<ConstOpNameGroups_OpNameGroup>> registry(m, "ConstOpNameGroups_OpNameGroup");
    registry.def("__id__", &::oneflow::cfg::OpNameGroups_OpNameGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpNameGroups_OpNameGroup::DebugString);
    registry.def("__repr__", &ConstOpNameGroups_OpNameGroup::DebugString);

    registry.def("op_name_size", &ConstOpNameGroups_OpNameGroup::op_name_size);
    registry.def("op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (ConstOpNameGroups_OpNameGroup::*)() const)&ConstOpNameGroups_OpNameGroup::shared_const_op_name);
    registry.def("op_name", (const ::std::string& (ConstOpNameGroups_OpNameGroup::*)(::std::size_t) const)&ConstOpNameGroups_OpNameGroup::op_name);
  }
  {
    pybind11::class_<::oneflow::cfg::OpNameGroups_OpNameGroup, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup>> registry(m, "OpNameGroups_OpNameGroup");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpNameGroups_OpNameGroup::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameGroups_OpNameGroup::*)(const ConstOpNameGroups_OpNameGroup&))&::oneflow::cfg::OpNameGroups_OpNameGroup::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameGroups_OpNameGroup::*)(const ::oneflow::cfg::OpNameGroups_OpNameGroup&))&::oneflow::cfg::OpNameGroups_OpNameGroup::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpNameGroups_OpNameGroup::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpNameGroups_OpNameGroup::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpNameGroups_OpNameGroup::DebugString);



    registry.def("op_name_size", &::oneflow::cfg::OpNameGroups_OpNameGroup::op_name_size);
    registry.def("clear_op_name", &::oneflow::cfg::OpNameGroups_OpNameGroup::clear_op_name);
    registry.def("mutable_op_name", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpNameGroups_OpNameGroup::*)())&::oneflow::cfg::OpNameGroups_OpNameGroup::shared_mutable_op_name);
    registry.def("op_name", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpNameGroups_OpNameGroup::*)() const)&::oneflow::cfg::OpNameGroups_OpNameGroup::shared_const_op_name);
    registry.def("op_name", (const ::std::string& (::oneflow::cfg::OpNameGroups_OpNameGroup::*)(::std::size_t) const)&::oneflow::cfg::OpNameGroups_OpNameGroup::op_name);
    registry.def("add_op_name", &::oneflow::cfg::OpNameGroups_OpNameGroup::add_op_name);
    registry.def("set_op_name", &::oneflow::cfg::OpNameGroups_OpNameGroup::set_op_name);
  }
  {
    pybind11::class_<ConstOpNameGroups, ::oneflow::cfg::Message, std::shared_ptr<ConstOpNameGroups>> registry(m, "ConstOpNameGroups");
    registry.def("__id__", &::oneflow::cfg::OpNameGroups::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpNameGroups::DebugString);
    registry.def("__repr__", &ConstOpNameGroups::DebugString);

    registry.def("op_name_group_size", &ConstOpNameGroups::op_name_group_size);
    registry.def("op_name_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> (ConstOpNameGroups::*)() const)&ConstOpNameGroups::shared_const_op_name_group);
    registry.def("op_name_group", (::std::shared_ptr<ConstOpNameGroups_OpNameGroup> (ConstOpNameGroups::*)(::std::size_t) const)&ConstOpNameGroups::shared_const_op_name_group);
  }
  {
    pybind11::class_<::oneflow::cfg::OpNameGroups, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpNameGroups>> registry(m, "OpNameGroups");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpNameGroups::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameGroups::*)(const ConstOpNameGroups&))&::oneflow::cfg::OpNameGroups::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpNameGroups::*)(const ::oneflow::cfg::OpNameGroups&))&::oneflow::cfg::OpNameGroups::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpNameGroups::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpNameGroups::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpNameGroups::DebugString);



    registry.def("op_name_group_size", &::oneflow::cfg::OpNameGroups::op_name_group_size);
    registry.def("clear_op_name_group", &::oneflow::cfg::OpNameGroups::clear_op_name_group);
    registry.def("mutable_op_name_group", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> (::oneflow::cfg::OpNameGroups::*)())&::oneflow::cfg::OpNameGroups::shared_mutable_op_name_group);
    registry.def("op_name_group", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> (::oneflow::cfg::OpNameGroups::*)() const)&::oneflow::cfg::OpNameGroups::shared_const_op_name_group);
    registry.def("op_name_group", (::std::shared_ptr<ConstOpNameGroups_OpNameGroup> (::oneflow::cfg::OpNameGroups::*)(::std::size_t) const)&::oneflow::cfg::OpNameGroups::shared_const_op_name_group);
    registry.def("mutable_op_name_group", (::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> (::oneflow::cfg::OpNameGroups::*)(::std::size_t))&::oneflow::cfg::OpNameGroups::shared_mutable_op_name_group);
  }
}