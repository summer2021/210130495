#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/operator/interface_blob_conf.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.operator.interface_blob_conf", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<ConstInterfaceBlobConf, ::oneflow::cfg::Message, std::shared_ptr<ConstInterfaceBlobConf>> registry(m, "ConstInterfaceBlobConf");
    registry.def("__id__", &::oneflow::cfg::InterfaceBlobConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstInterfaceBlobConf::DebugString);
    registry.def("__repr__", &ConstInterfaceBlobConf::DebugString);

    registry.def("has_shape", &ConstInterfaceBlobConf::has_shape);
    registry.def("shape", &ConstInterfaceBlobConf::shared_const_shape);

    registry.def("has_data_type", &ConstInterfaceBlobConf::has_data_type);
    registry.def("data_type", &ConstInterfaceBlobConf::data_type);

    registry.def("has_is_dynamic", &ConstInterfaceBlobConf::has_is_dynamic);
    registry.def("is_dynamic", &ConstInterfaceBlobConf::is_dynamic);

    registry.def("has_parallel_distribution", &ConstInterfaceBlobConf::has_parallel_distribution);
    registry.def("parallel_distribution", &ConstInterfaceBlobConf::shared_const_parallel_distribution);
  }
  {
    pybind11::class_<::oneflow::cfg::InterfaceBlobConf, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::InterfaceBlobConf>> registry(m, "InterfaceBlobConf");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::InterfaceBlobConf::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::InterfaceBlobConf::*)(const ConstInterfaceBlobConf&))&::oneflow::cfg::InterfaceBlobConf::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::InterfaceBlobConf::*)(const ::oneflow::cfg::InterfaceBlobConf&))&::oneflow::cfg::InterfaceBlobConf::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::InterfaceBlobConf::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::InterfaceBlobConf::DebugString);
    registry.def("__repr__", &::oneflow::cfg::InterfaceBlobConf::DebugString);



    registry.def("has_shape", &::oneflow::cfg::InterfaceBlobConf::has_shape);
    registry.def("clear_shape", &::oneflow::cfg::InterfaceBlobConf::clear_shape);
    registry.def("shape", &::oneflow::cfg::InterfaceBlobConf::shared_const_shape);
    registry.def("mutable_shape", &::oneflow::cfg::InterfaceBlobConf::shared_mutable_shape);

    registry.def("has_data_type", &::oneflow::cfg::InterfaceBlobConf::has_data_type);
    registry.def("clear_data_type", &::oneflow::cfg::InterfaceBlobConf::clear_data_type);
    registry.def("data_type", &::oneflow::cfg::InterfaceBlobConf::data_type);
    registry.def("set_data_type", &::oneflow::cfg::InterfaceBlobConf::set_data_type);

    registry.def("has_is_dynamic", &::oneflow::cfg::InterfaceBlobConf::has_is_dynamic);
    registry.def("clear_is_dynamic", &::oneflow::cfg::InterfaceBlobConf::clear_is_dynamic);
    registry.def("is_dynamic", &::oneflow::cfg::InterfaceBlobConf::is_dynamic);
    registry.def("set_is_dynamic", &::oneflow::cfg::InterfaceBlobConf::set_is_dynamic);

    registry.def("has_parallel_distribution", &::oneflow::cfg::InterfaceBlobConf::has_parallel_distribution);
    registry.def("clear_parallel_distribution", &::oneflow::cfg::InterfaceBlobConf::clear_parallel_distribution);
    registry.def("parallel_distribution", &::oneflow::cfg::InterfaceBlobConf::shared_const_parallel_distribution);
    registry.def("mutable_parallel_distribution", &::oneflow::cfg::InterfaceBlobConf::shared_mutable_parallel_distribution);
  }
}