#ifndef CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum CopyHdOpConf_Type : unsigned int;
}
}
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}
namespace oneflow {
namespace cfg {
enum DeviceType : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstBlobDescProto;
class BlobDescProto;
}
}
namespace oneflow {
namespace cfg {
class ConstInitializeWithSnapshotConf;
class InitializeWithSnapshotConf;
}
}
namespace oneflow {
namespace cfg {
class ConstInitializerConf;
class InitializerConf;
}
}
namespace oneflow {
namespace cfg {
class ConstInt64List;
class Int64List;
}
}
namespace oneflow {
namespace cfg {
class ConstInterfaceBlobConf;
class InterfaceBlobConf;
}
}
namespace oneflow {
namespace cfg {
class ConstLearningRateDecayConf;
class LearningRateDecayConf;
}
}
namespace oneflow {
namespace cfg {
class ConstLogicalBlobId;
class LogicalBlobId;
}
}
namespace oneflow {
namespace cfg {
class ConstRegularizerConf;
class RegularizerConf;
}
}
namespace oneflow {
namespace cfg {
class ConstSbpParallel;
class SbpParallel;
}
}
namespace oneflow {
namespace cfg {
class ConstSbpSignature;
class SbpSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}
namespace oneflow {
namespace cfg {
class ConstTensorSliceViewProto;
class TensorSliceViewProto;
}
}
namespace oneflow {
namespace cfg {
class ConstUserOpConf;
class UserOpConf;
}
}
namespace oneflow {
namespace cfg {
class ConstWarmupConf;
class WarmupConf;
}
}
namespace oneflow {
namespace boxing {
namespace collective {
namespace cfg {
class ConstRankDesc;
class RankDesc;
}
}
}
}

namespace oneflow {

// forward declare proto class;
class DistributeConcatOpConf;
class DistributeSplitOpConf;
class DistributeCloneOpConf;
class DistributeAddOpConf;
class CopyCommNetOpConf;
class CopyHdOpConf;
class BoxConcatConf;
class BoxAddConf;
class BoxSplitConf;
class BoxCloneConf;
class BoxingOpConf;
class DynamicReshapeOpConf;
class DynamicReshapeLikeOpConf;
class FeedInputOpConf;
class FeedVariableOpConf;
class FetchOutputOpConf;
class InputOpConf;
class ForeignInputOpConf;
class ReturnOpConf;
class OutputOpConf;
class ForeignOutputOpConf;
class ForeignWatchOpConf;
class VariableOpConf;
class DecodeRandomOpConf;
class TickOpConf;
class DeviceTickOpConf;
class WaitAndSendIdsOpConf;
class CallbackNotifyOpConf;
class ReentrantLockOpConf;
class SrcSubsetTickOpConf;
class DstSubsetTickOpConf;
class SourceTickOpConf;
class SinkTickOpConf;
class TotalLossInstanceNumOpConf;
class ShapeElemCntAxisConf;
class ShapeElemCntRangeAxisConf;
class ShapeElemCntOpConf;
class AccTickOpConf;
class ModelInitOpConf;
class ModelLoadOpConf;
class IdentityOpConf;
class CopyOpConf;
class CastToMirroredOpConf;
class CastFromMirroredOpConf;
class CaseOpConf;
class EsacOpConf;
class AssignOpConf;
class ModelSaveOpConf;
class LearningRateScheduleOpConf;
class SliceBoxingConf;
class SliceBoxingCopyOpConf;
class SliceBoxingAddOpConf;
class XrtLaunchOpConf_Argument;
class XrtLaunchOpConf_Function;
class XrtLaunchOpConf_InputMutabilityEntry;
class XrtLaunchOpConf_InputOutputMappingEntry;
class XrtLaunchOpConf_SbpSignaturesEntry;
class XrtLaunchOpConf_Lbn2logicalBlobDescEntry;
class XrtLaunchOpConf;
class ModelInitV2OpConf;
class ModelLoadV2OpConf;
class ModelSaveV2OpConf;
class ConstantLikeOpConf;
class SyncDynamicResizeOpConf;
class BroadcastToCompatibleWithOpConf;
class CollectiveBoxingGenericOpConf;
class BoxingIdentityOpConf;
class CollectiveBoxingPackOpConf;
class CollectiveBoxingUnpackOpConf;
class ImageDecoderRandomCropResizeOpConf;
class BoxingZerosOpConf;
class OperatorConf;
class OpNameRelations_SrcOpName2dstOpNameEntry;
class OpNameRelations;
class OpNameGroups_OpNameGroup;
class OpNameGroups;

namespace cfg {


class DistributeConcatOpConf;
class ConstDistributeConcatOpConf;

class DistributeSplitOpConf;
class ConstDistributeSplitOpConf;

class DistributeCloneOpConf;
class ConstDistributeCloneOpConf;

class DistributeAddOpConf;
class ConstDistributeAddOpConf;

class CopyCommNetOpConf;
class ConstCopyCommNetOpConf;

class CopyHdOpConf;
class ConstCopyHdOpConf;
enum CopyHdOpConf_Type : unsigned int {
  H2D = 0,
  D2H = 1,
};


class BoxConcatConf;
class ConstBoxConcatConf;

class BoxAddConf;
class ConstBoxAddConf;

class BoxSplitConf;
class ConstBoxSplitConf;

class BoxCloneConf;
class ConstBoxCloneConf;

class BoxingOpConf;
class ConstBoxingOpConf;

class DynamicReshapeOpConf;
class ConstDynamicReshapeOpConf;

class DynamicReshapeLikeOpConf;
class ConstDynamicReshapeLikeOpConf;

class FeedInputOpConf;
class ConstFeedInputOpConf;

class FeedVariableOpConf;
class ConstFeedVariableOpConf;

class FetchOutputOpConf;
class ConstFetchOutputOpConf;

class InputOpConf;
class ConstInputOpConf;

class ForeignInputOpConf;
class ConstForeignInputOpConf;

class ReturnOpConf;
class ConstReturnOpConf;

class OutputOpConf;
class ConstOutputOpConf;

class ForeignOutputOpConf;
class ConstForeignOutputOpConf;

class ForeignWatchOpConf;
class ConstForeignWatchOpConf;

class VariableOpConf;
class ConstVariableOpConf;

class DecodeRandomOpConf;
class ConstDecodeRandomOpConf;

class TickOpConf;
class ConstTickOpConf;

class DeviceTickOpConf;
class ConstDeviceTickOpConf;

class WaitAndSendIdsOpConf;
class ConstWaitAndSendIdsOpConf;

class CallbackNotifyOpConf;
class ConstCallbackNotifyOpConf;

class ReentrantLockOpConf;
class ConstReentrantLockOpConf;

class SrcSubsetTickOpConf;
class ConstSrcSubsetTickOpConf;

class DstSubsetTickOpConf;
class ConstDstSubsetTickOpConf;

class SourceTickOpConf;
class ConstSourceTickOpConf;

class SinkTickOpConf;
class ConstSinkTickOpConf;

class TotalLossInstanceNumOpConf;
class ConstTotalLossInstanceNumOpConf;

class ShapeElemCntAxisConf;
class ConstShapeElemCntAxisConf;

class ShapeElemCntRangeAxisConf;
class ConstShapeElemCntRangeAxisConf;

class ShapeElemCntOpConf;
class ConstShapeElemCntOpConf;

class AccTickOpConf;
class ConstAccTickOpConf;

class ModelInitOpConf;
class ConstModelInitOpConf;

class ModelLoadOpConf;
class ConstModelLoadOpConf;

class IdentityOpConf;
class ConstIdentityOpConf;

class CopyOpConf;
class ConstCopyOpConf;

class CastToMirroredOpConf;
class ConstCastToMirroredOpConf;

class CastFromMirroredOpConf;
class ConstCastFromMirroredOpConf;

class CaseOpConf;
class ConstCaseOpConf;

class EsacOpConf;
class ConstEsacOpConf;

class AssignOpConf;
class ConstAssignOpConf;

class ModelSaveOpConf;
class ConstModelSaveOpConf;

class LearningRateScheduleOpConf;
class ConstLearningRateScheduleOpConf;

class SliceBoxingConf;
class ConstSliceBoxingConf;

class SliceBoxingCopyOpConf;
class ConstSliceBoxingCopyOpConf;

class SliceBoxingAddOpConf;
class ConstSliceBoxingAddOpConf;

class XrtLaunchOpConf_Argument;
class ConstXrtLaunchOpConf_Argument;

class XrtLaunchOpConf_Function;
class ConstXrtLaunchOpConf_Function;

class XrtLaunchOpConf;
class ConstXrtLaunchOpConf;

class ModelInitV2OpConf;
class ConstModelInitV2OpConf;

class ModelLoadV2OpConf;
class ConstModelLoadV2OpConf;

class ModelSaveV2OpConf;
class ConstModelSaveV2OpConf;

class ConstantLikeOpConf;
class ConstConstantLikeOpConf;

class SyncDynamicResizeOpConf;
class ConstSyncDynamicResizeOpConf;

class BroadcastToCompatibleWithOpConf;
class ConstBroadcastToCompatibleWithOpConf;

class CollectiveBoxingGenericOpConf;
class ConstCollectiveBoxingGenericOpConf;

class BoxingIdentityOpConf;
class ConstBoxingIdentityOpConf;

class CollectiveBoxingPackOpConf;
class ConstCollectiveBoxingPackOpConf;

class CollectiveBoxingUnpackOpConf;
class ConstCollectiveBoxingUnpackOpConf;

class ImageDecoderRandomCropResizeOpConf;
class ConstImageDecoderRandomCropResizeOpConf;

class BoxingZerosOpConf;
class ConstBoxingZerosOpConf;

class OperatorConf;
class ConstOperatorConf;

class OpNameRelations;
class ConstOpNameRelations;

class OpNameGroups_OpNameGroup;
class ConstOpNameGroups_OpNameGroup;

class OpNameGroups;
class ConstOpNameGroups;

enum ActivationType : unsigned int {
  kNone = 0,
  kTanH = 1,
  kSigmoid = 2,
  kRelu = 3,
};

inline ::std::string ActivationType_Name(ActivationType value) {
  switch (value) {
  case kNone: { return "kNone"; }
  case kTanH: { return "kTanH"; }
  case kSigmoid: { return "kSigmoid"; }
  case kRelu: { return "kRelu"; }
  default:
    return "";
  }
}


class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_;

class ConstDistributeConcatOpConf : public ::oneflow::cfg::Message {
 public:

  class _DistributeConcatOpConf_ {
   public:
    _DistributeConcatOpConf_();
    explicit _DistributeConcatOpConf_(const _DistributeConcatOpConf_& other);
    explicit _DistributeConcatOpConf_(_DistributeConcatOpConf_&& other);
    _DistributeConcatOpConf_(const ::oneflow::DistributeConcatOpConf& proto_distributeconcatopconf);
    ~_DistributeConcatOpConf_();

    void InitFromProto(const ::oneflow::DistributeConcatOpConf& proto_distributeconcatopconf);

    void ToProto(::oneflow::DistributeConcatOpConf* proto_distributeconcatopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DistributeConcatOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field axis
     public:
    bool has_axis() const;
    const int32_t& axis() const;
    void clear_axis();
    void set_axis(const int32_t& value);
    int32_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int32_t axis_;
           
   public:
    int compare(const _DistributeConcatOpConf_& other);

    bool operator==(const _DistributeConcatOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DistributeConcatOpConf_& other) const;
  };

  ConstDistributeConcatOpConf(const ::std::shared_ptr<_DistributeConcatOpConf_>& data);
  ConstDistributeConcatOpConf(const ConstDistributeConcatOpConf&);
  ConstDistributeConcatOpConf(ConstDistributeConcatOpConf&&) noexcept;
  ConstDistributeConcatOpConf();
  ConstDistributeConcatOpConf(const ::oneflow::DistributeConcatOpConf& proto_distributeconcatopconf);
  virtual ~ConstDistributeConcatOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_distributeconcatopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field axis
 public:
  bool has_axis() const;
  const int32_t& axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDistributeConcatOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDistributeConcatOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDistributeConcatOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DistributeConcatOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DistributeConcatOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDistributeConcatOpConf
  void BuildFromProto(const PbMessage& proto_distributeconcatopconf);
  
  ::std::shared_ptr<_DistributeConcatOpConf_> data_;
};

class DistributeConcatOpConf final : public ConstDistributeConcatOpConf {
 public:
  DistributeConcatOpConf(const ::std::shared_ptr<_DistributeConcatOpConf_>& data);
  DistributeConcatOpConf(const DistributeConcatOpConf& other);
  // enable nothrow for ::std::vector<DistributeConcatOpConf> resize 
  DistributeConcatOpConf(DistributeConcatOpConf&&) noexcept;
  DistributeConcatOpConf();
  explicit DistributeConcatOpConf(const ::oneflow::DistributeConcatOpConf& proto_distributeconcatopconf);

  ~DistributeConcatOpConf() override;

  void InitFromProto(const PbMessage& proto_distributeconcatopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DistributeConcatOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DistributeConcatOpConf& other) const;
  void Clear();
  void CopyFrom(const DistributeConcatOpConf& other);
  DistributeConcatOpConf& operator=(const DistributeConcatOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int32_t& value);
  int32_t* mutable_axis();

  ::std::shared_ptr<DistributeConcatOpConf> __SharedMutable__();
};


class ConstDistributeSplitOpConf : public ::oneflow::cfg::Message {
 public:

  class _DistributeSplitOpConf_ {
   public:
    _DistributeSplitOpConf_();
    explicit _DistributeSplitOpConf_(const _DistributeSplitOpConf_& other);
    explicit _DistributeSplitOpConf_(_DistributeSplitOpConf_&& other);
    _DistributeSplitOpConf_(const ::oneflow::DistributeSplitOpConf& proto_distributesplitopconf);
    ~_DistributeSplitOpConf_();

    void InitFromProto(const ::oneflow::DistributeSplitOpConf& proto_distributesplitopconf);

    void ToProto(::oneflow::DistributeSplitOpConf* proto_distributesplitopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DistributeSplitOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
    
      // optional field axis
     public:
    bool has_axis() const;
    const int32_t& axis() const;
    void clear_axis();
    void set_axis(const int32_t& value);
    int32_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int32_t axis_;
      
      // optional field is_variable_ref
     public:
    bool has_is_variable_ref() const;
    const bool& is_variable_ref() const;
    void clear_is_variable_ref();
    void set_is_variable_ref(const bool& value);
    bool* mutable_is_variable_ref();
   protected:
    bool has_is_variable_ref_ = false;
    bool is_variable_ref_;
           
   public:
    int compare(const _DistributeSplitOpConf_& other);

    bool operator==(const _DistributeSplitOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DistributeSplitOpConf_& other) const;
  };

  ConstDistributeSplitOpConf(const ::std::shared_ptr<_DistributeSplitOpConf_>& data);
  ConstDistributeSplitOpConf(const ConstDistributeSplitOpConf&);
  ConstDistributeSplitOpConf(ConstDistributeSplitOpConf&&) noexcept;
  ConstDistributeSplitOpConf();
  ConstDistributeSplitOpConf(const ::oneflow::DistributeSplitOpConf& proto_distributesplitopconf);
  virtual ~ConstDistributeSplitOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_distributesplitopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;
  // required or optional field axis
 public:
  bool has_axis() const;
  const int32_t& axis() const;
  // used by pybind11 only
  // required or optional field is_variable_ref
 public:
  bool has_is_variable_ref() const;
  const bool& is_variable_ref() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDistributeSplitOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDistributeSplitOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDistributeSplitOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DistributeSplitOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DistributeSplitOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDistributeSplitOpConf
  void BuildFromProto(const PbMessage& proto_distributesplitopconf);
  
  ::std::shared_ptr<_DistributeSplitOpConf_> data_;
};

class DistributeSplitOpConf final : public ConstDistributeSplitOpConf {
 public:
  DistributeSplitOpConf(const ::std::shared_ptr<_DistributeSplitOpConf_>& data);
  DistributeSplitOpConf(const DistributeSplitOpConf& other);
  // enable nothrow for ::std::vector<DistributeSplitOpConf> resize 
  DistributeSplitOpConf(DistributeSplitOpConf&&) noexcept;
  DistributeSplitOpConf();
  explicit DistributeSplitOpConf(const ::oneflow::DistributeSplitOpConf& proto_distributesplitopconf);

  ~DistributeSplitOpConf() override;

  void InitFromProto(const PbMessage& proto_distributesplitopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DistributeSplitOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DistributeSplitOpConf& other) const;
  void Clear();
  void CopyFrom(const DistributeSplitOpConf& other);
  DistributeSplitOpConf& operator=(const DistributeSplitOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);
  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int32_t& value);
  int32_t* mutable_axis();
  // required or optional field is_variable_ref
 public:
  void clear_is_variable_ref();
  void set_is_variable_ref(const bool& value);
  bool* mutable_is_variable_ref();

  ::std::shared_ptr<DistributeSplitOpConf> __SharedMutable__();
};


class ConstDistributeCloneOpConf : public ::oneflow::cfg::Message {
 public:

  class _DistributeCloneOpConf_ {
   public:
    _DistributeCloneOpConf_();
    explicit _DistributeCloneOpConf_(const _DistributeCloneOpConf_& other);
    explicit _DistributeCloneOpConf_(_DistributeCloneOpConf_&& other);
    _DistributeCloneOpConf_(const ::oneflow::DistributeCloneOpConf& proto_distributecloneopconf);
    ~_DistributeCloneOpConf_();

    void InitFromProto(const ::oneflow::DistributeCloneOpConf& proto_distributecloneopconf);

    void ToProto(::oneflow::DistributeCloneOpConf* proto_distributecloneopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DistributeCloneOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
    
      // optional field is_variable_ref
     public:
    bool has_is_variable_ref() const;
    const bool& is_variable_ref() const;
    void clear_is_variable_ref();
    void set_is_variable_ref(const bool& value);
    bool* mutable_is_variable_ref();
   protected:
    bool has_is_variable_ref_ = false;
    bool is_variable_ref_;
           
   public:
    int compare(const _DistributeCloneOpConf_& other);

    bool operator==(const _DistributeCloneOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DistributeCloneOpConf_& other) const;
  };

  ConstDistributeCloneOpConf(const ::std::shared_ptr<_DistributeCloneOpConf_>& data);
  ConstDistributeCloneOpConf(const ConstDistributeCloneOpConf&);
  ConstDistributeCloneOpConf(ConstDistributeCloneOpConf&&) noexcept;
  ConstDistributeCloneOpConf();
  ConstDistributeCloneOpConf(const ::oneflow::DistributeCloneOpConf& proto_distributecloneopconf);
  virtual ~ConstDistributeCloneOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_distributecloneopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;
  // required or optional field is_variable_ref
 public:
  bool has_is_variable_ref() const;
  const bool& is_variable_ref() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDistributeCloneOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDistributeCloneOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDistributeCloneOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DistributeCloneOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DistributeCloneOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDistributeCloneOpConf
  void BuildFromProto(const PbMessage& proto_distributecloneopconf);
  
  ::std::shared_ptr<_DistributeCloneOpConf_> data_;
};

class DistributeCloneOpConf final : public ConstDistributeCloneOpConf {
 public:
  DistributeCloneOpConf(const ::std::shared_ptr<_DistributeCloneOpConf_>& data);
  DistributeCloneOpConf(const DistributeCloneOpConf& other);
  // enable nothrow for ::std::vector<DistributeCloneOpConf> resize 
  DistributeCloneOpConf(DistributeCloneOpConf&&) noexcept;
  DistributeCloneOpConf();
  explicit DistributeCloneOpConf(const ::oneflow::DistributeCloneOpConf& proto_distributecloneopconf);

  ~DistributeCloneOpConf() override;

  void InitFromProto(const PbMessage& proto_distributecloneopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DistributeCloneOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DistributeCloneOpConf& other) const;
  void Clear();
  void CopyFrom(const DistributeCloneOpConf& other);
  DistributeCloneOpConf& operator=(const DistributeCloneOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);
  // required or optional field is_variable_ref
 public:
  void clear_is_variable_ref();
  void set_is_variable_ref(const bool& value);
  bool* mutable_is_variable_ref();

  ::std::shared_ptr<DistributeCloneOpConf> __SharedMutable__();
};


class ConstDistributeAddOpConf : public ::oneflow::cfg::Message {
 public:

  class _DistributeAddOpConf_ {
   public:
    _DistributeAddOpConf_();
    explicit _DistributeAddOpConf_(const _DistributeAddOpConf_& other);
    explicit _DistributeAddOpConf_(_DistributeAddOpConf_&& other);
    _DistributeAddOpConf_(const ::oneflow::DistributeAddOpConf& proto_distributeaddopconf);
    ~_DistributeAddOpConf_();

    void InitFromProto(const ::oneflow::DistributeAddOpConf& proto_distributeaddopconf);

    void ToProto(::oneflow::DistributeAddOpConf* proto_distributeaddopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DistributeAddOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _DistributeAddOpConf_& other);

    bool operator==(const _DistributeAddOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DistributeAddOpConf_& other) const;
  };

  ConstDistributeAddOpConf(const ::std::shared_ptr<_DistributeAddOpConf_>& data);
  ConstDistributeAddOpConf(const ConstDistributeAddOpConf&);
  ConstDistributeAddOpConf(ConstDistributeAddOpConf&&) noexcept;
  ConstDistributeAddOpConf();
  ConstDistributeAddOpConf(const ::oneflow::DistributeAddOpConf& proto_distributeaddopconf);
  virtual ~ConstDistributeAddOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_distributeaddopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDistributeAddOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDistributeAddOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDistributeAddOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DistributeAddOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DistributeAddOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDistributeAddOpConf
  void BuildFromProto(const PbMessage& proto_distributeaddopconf);
  
  ::std::shared_ptr<_DistributeAddOpConf_> data_;
};

class DistributeAddOpConf final : public ConstDistributeAddOpConf {
 public:
  DistributeAddOpConf(const ::std::shared_ptr<_DistributeAddOpConf_>& data);
  DistributeAddOpConf(const DistributeAddOpConf& other);
  // enable nothrow for ::std::vector<DistributeAddOpConf> resize 
  DistributeAddOpConf(DistributeAddOpConf&&) noexcept;
  DistributeAddOpConf();
  explicit DistributeAddOpConf(const ::oneflow::DistributeAddOpConf& proto_distributeaddopconf);

  ~DistributeAddOpConf() override;

  void InitFromProto(const PbMessage& proto_distributeaddopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DistributeAddOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DistributeAddOpConf& other) const;
  void Clear();
  void CopyFrom(const DistributeAddOpConf& other);
  DistributeAddOpConf& operator=(const DistributeAddOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<DistributeAddOpConf> __SharedMutable__();
};


class ConstCopyCommNetOpConf : public ::oneflow::cfg::Message {
 public:

  class _CopyCommNetOpConf_ {
   public:
    _CopyCommNetOpConf_();
    explicit _CopyCommNetOpConf_(const _CopyCommNetOpConf_& other);
    explicit _CopyCommNetOpConf_(_CopyCommNetOpConf_&& other);
    _CopyCommNetOpConf_(const ::oneflow::CopyCommNetOpConf& proto_copycommnetopconf);
    ~_CopyCommNetOpConf_();

    void InitFromProto(const ::oneflow::CopyCommNetOpConf& proto_copycommnetopconf);

    void ToProto(::oneflow::CopyCommNetOpConf* proto_copycommnetopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CopyCommNetOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
           
   public:
    int compare(const _CopyCommNetOpConf_& other);

    bool operator==(const _CopyCommNetOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CopyCommNetOpConf_& other) const;
  };

  ConstCopyCommNetOpConf(const ::std::shared_ptr<_CopyCommNetOpConf_>& data);
  ConstCopyCommNetOpConf(const ConstCopyCommNetOpConf&);
  ConstCopyCommNetOpConf(ConstCopyCommNetOpConf&&) noexcept;
  ConstCopyCommNetOpConf();
  ConstCopyCommNetOpConf(const ::oneflow::CopyCommNetOpConf& proto_copycommnetopconf);
  virtual ~ConstCopyCommNetOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_copycommnetopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;

 public:
  ::std::shared_ptr<ConstCopyCommNetOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCopyCommNetOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCopyCommNetOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CopyCommNetOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CopyCommNetOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCopyCommNetOpConf
  void BuildFromProto(const PbMessage& proto_copycommnetopconf);
  
  ::std::shared_ptr<_CopyCommNetOpConf_> data_;
};

class CopyCommNetOpConf final : public ConstCopyCommNetOpConf {
 public:
  CopyCommNetOpConf(const ::std::shared_ptr<_CopyCommNetOpConf_>& data);
  CopyCommNetOpConf(const CopyCommNetOpConf& other);
  // enable nothrow for ::std::vector<CopyCommNetOpConf> resize 
  CopyCommNetOpConf(CopyCommNetOpConf&&) noexcept;
  CopyCommNetOpConf();
  explicit CopyCommNetOpConf(const ::oneflow::CopyCommNetOpConf& proto_copycommnetopconf);

  ~CopyCommNetOpConf() override;

  void InitFromProto(const PbMessage& proto_copycommnetopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CopyCommNetOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CopyCommNetOpConf& other) const;
  void Clear();
  void CopyFrom(const CopyCommNetOpConf& other);
  CopyCommNetOpConf& operator=(const CopyCommNetOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();

  ::std::shared_ptr<CopyCommNetOpConf> __SharedMutable__();
};


class ConstCopyHdOpConf : public ::oneflow::cfg::Message {
 public:

  class _CopyHdOpConf_ {
   public:
    _CopyHdOpConf_();
    explicit _CopyHdOpConf_(const _CopyHdOpConf_& other);
    explicit _CopyHdOpConf_(_CopyHdOpConf_&& other);
    _CopyHdOpConf_(const ::oneflow::CopyHdOpConf& proto_copyhdopconf);
    ~_CopyHdOpConf_();

    void InitFromProto(const ::oneflow::CopyHdOpConf& proto_copyhdopconf);

    void ToProto(::oneflow::CopyHdOpConf* proto_copyhdopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CopyHdOpConf_& other);
  
      // optional field type
     public:
    bool has_type() const;
    const ::oneflow::cfg::CopyHdOpConf_Type& type() const;
    void clear_type();
    void set_type(const ::oneflow::cfg::CopyHdOpConf_Type& value);
    ::oneflow::cfg::CopyHdOpConf_Type* mutable_type();
   protected:
    bool has_type_ = false;
    ::oneflow::cfg::CopyHdOpConf_Type type_;
      
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
           
   public:
    int compare(const _CopyHdOpConf_& other);

    bool operator==(const _CopyHdOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CopyHdOpConf_& other) const;
  };

  ConstCopyHdOpConf(const ::std::shared_ptr<_CopyHdOpConf_>& data);
  ConstCopyHdOpConf(const ConstCopyHdOpConf&);
  ConstCopyHdOpConf(ConstCopyHdOpConf&&) noexcept;
  ConstCopyHdOpConf();
  ConstCopyHdOpConf(const ::oneflow::CopyHdOpConf& proto_copyhdopconf);
  virtual ~ConstCopyHdOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_copyhdopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field type
 public:
  bool has_type() const;
  const ::oneflow::cfg::CopyHdOpConf_Type& type() const;
  // used by pybind11 only
  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;

 public:
  ::std::shared_ptr<ConstCopyHdOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCopyHdOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCopyHdOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CopyHdOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CopyHdOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCopyHdOpConf
  void BuildFromProto(const PbMessage& proto_copyhdopconf);
  
  ::std::shared_ptr<_CopyHdOpConf_> data_;
};

class CopyHdOpConf final : public ConstCopyHdOpConf {
 public:
  CopyHdOpConf(const ::std::shared_ptr<_CopyHdOpConf_>& data);
  CopyHdOpConf(const CopyHdOpConf& other);
  // enable nothrow for ::std::vector<CopyHdOpConf> resize 
  CopyHdOpConf(CopyHdOpConf&&) noexcept;
  CopyHdOpConf();
  explicit CopyHdOpConf(const ::oneflow::CopyHdOpConf& proto_copyhdopconf);

  ~CopyHdOpConf() override;

  void InitFromProto(const PbMessage& proto_copyhdopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CopyHdOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CopyHdOpConf& other) const;
  void Clear();
  void CopyFrom(const CopyHdOpConf& other);
  CopyHdOpConf& operator=(const CopyHdOpConf& other);

  // required or optional field type
 public:
  void clear_type();
  void set_type(const ::oneflow::cfg::CopyHdOpConf_Type& value);
  ::oneflow::cfg::CopyHdOpConf_Type* mutable_type();
  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();

  ::std::shared_ptr<CopyHdOpConf> __SharedMutable__();
};


class ConstBoxConcatConf : public ::oneflow::cfg::Message {
 public:

  class _BoxConcatConf_ {
   public:
    _BoxConcatConf_();
    explicit _BoxConcatConf_(const _BoxConcatConf_& other);
    explicit _BoxConcatConf_(_BoxConcatConf_&& other);
    _BoxConcatConf_(const ::oneflow::BoxConcatConf& proto_boxconcatconf);
    ~_BoxConcatConf_();

    void InitFromProto(const ::oneflow::BoxConcatConf& proto_boxconcatconf);

    void ToProto(::oneflow::BoxConcatConf* proto_boxconcatconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxConcatConf_& other);
  
      // optional field axis
     public:
    bool has_axis() const;
    const int32_t& axis() const;
    void clear_axis();
    void set_axis(const int32_t& value);
    int32_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int32_t axis_;
           
   public:
    int compare(const _BoxConcatConf_& other);

    bool operator==(const _BoxConcatConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxConcatConf_& other) const;
  };

  ConstBoxConcatConf(const ::std::shared_ptr<_BoxConcatConf_>& data);
  ConstBoxConcatConf(const ConstBoxConcatConf&);
  ConstBoxConcatConf(ConstBoxConcatConf&&) noexcept;
  ConstBoxConcatConf();
  ConstBoxConcatConf(const ::oneflow::BoxConcatConf& proto_boxconcatconf);
  virtual ~ConstBoxConcatConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxconcatconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field axis
 public:
  bool has_axis() const;
  const int32_t& axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstBoxConcatConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxConcatConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxConcatConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxConcatConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxConcatConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxConcatConf
  void BuildFromProto(const PbMessage& proto_boxconcatconf);
  
  ::std::shared_ptr<_BoxConcatConf_> data_;
};

class BoxConcatConf final : public ConstBoxConcatConf {
 public:
  BoxConcatConf(const ::std::shared_ptr<_BoxConcatConf_>& data);
  BoxConcatConf(const BoxConcatConf& other);
  // enable nothrow for ::std::vector<BoxConcatConf> resize 
  BoxConcatConf(BoxConcatConf&&) noexcept;
  BoxConcatConf();
  explicit BoxConcatConf(const ::oneflow::BoxConcatConf& proto_boxconcatconf);

  ~BoxConcatConf() override;

  void InitFromProto(const PbMessage& proto_boxconcatconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxConcatConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxConcatConf& other) const;
  void Clear();
  void CopyFrom(const BoxConcatConf& other);
  BoxConcatConf& operator=(const BoxConcatConf& other);

  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int32_t& value);
  int32_t* mutable_axis();

  ::std::shared_ptr<BoxConcatConf> __SharedMutable__();
};


class ConstBoxAddConf : public ::oneflow::cfg::Message {
 public:

  class _BoxAddConf_ {
   public:
    _BoxAddConf_();
    explicit _BoxAddConf_(const _BoxAddConf_& other);
    explicit _BoxAddConf_(_BoxAddConf_&& other);
    _BoxAddConf_(const ::oneflow::BoxAddConf& proto_boxaddconf);
    ~_BoxAddConf_();

    void InitFromProto(const ::oneflow::BoxAddConf& proto_boxaddconf);

    void ToProto(::oneflow::BoxAddConf* proto_boxaddconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxAddConf_& other);
       
   public:
    int compare(const _BoxAddConf_& other);

    bool operator==(const _BoxAddConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxAddConf_& other) const;
  };

  ConstBoxAddConf(const ::std::shared_ptr<_BoxAddConf_>& data);
  ConstBoxAddConf(const ConstBoxAddConf&);
  ConstBoxAddConf(ConstBoxAddConf&&) noexcept;
  ConstBoxAddConf();
  ConstBoxAddConf(const ::oneflow::BoxAddConf& proto_boxaddconf);
  virtual ~ConstBoxAddConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxaddconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstBoxAddConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxAddConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxAddConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxAddConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxAddConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxAddConf
  void BuildFromProto(const PbMessage& proto_boxaddconf);
  
  ::std::shared_ptr<_BoxAddConf_> data_;
};

class BoxAddConf final : public ConstBoxAddConf {
 public:
  BoxAddConf(const ::std::shared_ptr<_BoxAddConf_>& data);
  BoxAddConf(const BoxAddConf& other);
  // enable nothrow for ::std::vector<BoxAddConf> resize 
  BoxAddConf(BoxAddConf&&) noexcept;
  BoxAddConf();
  explicit BoxAddConf(const ::oneflow::BoxAddConf& proto_boxaddconf);

  ~BoxAddConf() override;

  void InitFromProto(const PbMessage& proto_boxaddconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxAddConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxAddConf& other) const;
  void Clear();
  void CopyFrom(const BoxAddConf& other);
  BoxAddConf& operator=(const BoxAddConf& other);


  ::std::shared_ptr<BoxAddConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_;

class ConstBoxSplitConf : public ::oneflow::cfg::Message {
 public:

  class _BoxSplitConf_ {
   public:
    _BoxSplitConf_();
    explicit _BoxSplitConf_(const _BoxSplitConf_& other);
    explicit _BoxSplitConf_(_BoxSplitConf_&& other);
    _BoxSplitConf_(const ::oneflow::BoxSplitConf& proto_boxsplitconf);
    ~_BoxSplitConf_();

    void InitFromProto(const ::oneflow::BoxSplitConf& proto_boxsplitconf);

    void ToProto(::oneflow::BoxSplitConf* proto_boxsplitconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxSplitConf_& other);
  
      // optional field axis
     public:
    bool has_axis() const;
    const int32_t& axis() const;
    void clear_axis();
    void set_axis(const int32_t& value);
    int32_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int32_t axis_;
      
      // repeated field part_num
   public:
    ::std::size_t part_num_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& part_num() const;
    const int32_t& part_num(::std::size_t index) const;
    void clear_part_num();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_* mutable_part_num();
    int32_t* mutable_part_num(::std::size_t index);
      void add_part_num(const int32_t& value);
    void set_part_num(::std::size_t index, const int32_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> part_num_;
         
   public:
    int compare(const _BoxSplitConf_& other);

    bool operator==(const _BoxSplitConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxSplitConf_& other) const;
  };

  ConstBoxSplitConf(const ::std::shared_ptr<_BoxSplitConf_>& data);
  ConstBoxSplitConf(const ConstBoxSplitConf&);
  ConstBoxSplitConf(ConstBoxSplitConf&&) noexcept;
  ConstBoxSplitConf();
  ConstBoxSplitConf(const ::oneflow::BoxSplitConf& proto_boxsplitconf);
  virtual ~ConstBoxSplitConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxsplitconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field axis
 public:
  bool has_axis() const;
  const int32_t& axis() const;
  // used by pybind11 only
  // repeated field part_num
 public:
  ::std::size_t part_num_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& part_num() const;
  const int32_t& part_num(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> shared_const_part_num() const;

 public:
  ::std::shared_ptr<ConstBoxSplitConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxSplitConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxSplitConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxSplitConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxSplitConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxSplitConf
  void BuildFromProto(const PbMessage& proto_boxsplitconf);
  
  ::std::shared_ptr<_BoxSplitConf_> data_;
};

class BoxSplitConf final : public ConstBoxSplitConf {
 public:
  BoxSplitConf(const ::std::shared_ptr<_BoxSplitConf_>& data);
  BoxSplitConf(const BoxSplitConf& other);
  // enable nothrow for ::std::vector<BoxSplitConf> resize 
  BoxSplitConf(BoxSplitConf&&) noexcept;
  BoxSplitConf();
  explicit BoxSplitConf(const ::oneflow::BoxSplitConf& proto_boxsplitconf);

  ~BoxSplitConf() override;

  void InitFromProto(const PbMessage& proto_boxsplitconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxSplitConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxSplitConf& other) const;
  void Clear();
  void CopyFrom(const BoxSplitConf& other);
  BoxSplitConf& operator=(const BoxSplitConf& other);

  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int32_t& value);
  int32_t* mutable_axis();
  // repeated field part_num
 public:
  void clear_part_num();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_* mutable_part_num();
  int32_t* mutable_part_num(::std::size_t index);
  void add_part_num(const int32_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> shared_mutable_part_num();
  void set_part_num(::std::size_t index, const int32_t& value);

  ::std::shared_ptr<BoxSplitConf> __SharedMutable__();
};


class ConstBoxCloneConf : public ::oneflow::cfg::Message {
 public:

  class _BoxCloneConf_ {
   public:
    _BoxCloneConf_();
    explicit _BoxCloneConf_(const _BoxCloneConf_& other);
    explicit _BoxCloneConf_(_BoxCloneConf_&& other);
    _BoxCloneConf_(const ::oneflow::BoxCloneConf& proto_boxcloneconf);
    ~_BoxCloneConf_();

    void InitFromProto(const ::oneflow::BoxCloneConf& proto_boxcloneconf);

    void ToProto(::oneflow::BoxCloneConf* proto_boxcloneconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxCloneConf_& other);
       
   public:
    int compare(const _BoxCloneConf_& other);

    bool operator==(const _BoxCloneConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxCloneConf_& other) const;
  };

  ConstBoxCloneConf(const ::std::shared_ptr<_BoxCloneConf_>& data);
  ConstBoxCloneConf(const ConstBoxCloneConf&);
  ConstBoxCloneConf(ConstBoxCloneConf&&) noexcept;
  ConstBoxCloneConf();
  ConstBoxCloneConf(const ::oneflow::BoxCloneConf& proto_boxcloneconf);
  virtual ~ConstBoxCloneConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxcloneconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;


 public:
  ::std::shared_ptr<ConstBoxCloneConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxCloneConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxCloneConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxCloneConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxCloneConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxCloneConf
  void BuildFromProto(const PbMessage& proto_boxcloneconf);
  
  ::std::shared_ptr<_BoxCloneConf_> data_;
};

class BoxCloneConf final : public ConstBoxCloneConf {
 public:
  BoxCloneConf(const ::std::shared_ptr<_BoxCloneConf_>& data);
  BoxCloneConf(const BoxCloneConf& other);
  // enable nothrow for ::std::vector<BoxCloneConf> resize 
  BoxCloneConf(BoxCloneConf&&) noexcept;
  BoxCloneConf();
  explicit BoxCloneConf(const ::oneflow::BoxCloneConf& proto_boxcloneconf);

  ~BoxCloneConf() override;

  void InitFromProto(const PbMessage& proto_boxcloneconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxCloneConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxCloneConf& other) const;
  void Clear();
  void CopyFrom(const BoxCloneConf& other);
  BoxCloneConf& operator=(const BoxCloneConf& other);


  ::std::shared_ptr<BoxCloneConf> __SharedMutable__();
};


class ConstBoxingOpConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum in_box
 enum InBoxCase : unsigned int {
  IN_BOX_NOT_SET = 0,
    kConcatBox = 4,
    kAddBox = 5,
   };

 // oneof enum out_box
 enum OutBoxCase : unsigned int {
  OUT_BOX_NOT_SET = 0,
    kSplitBox = 6,
    kCloneBox = 7,
   };

  class _BoxingOpConf_ {
   public:
    _BoxingOpConf_();
    explicit _BoxingOpConf_(const _BoxingOpConf_& other);
    explicit _BoxingOpConf_(_BoxingOpConf_&& other);
    _BoxingOpConf_(const ::oneflow::BoxingOpConf& proto_boxingopconf);
    ~_BoxingOpConf_();

    void InitFromProto(const ::oneflow::BoxingOpConf& proto_boxingopconf);

    void ToProto(::oneflow::BoxingOpConf* proto_boxingopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxingOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field in_num
     public:
    bool has_in_num() const;
    const int32_t& in_num() const;
    void clear_in_num();
    void set_in_num(const int32_t& value);
    int32_t* mutable_in_num();
   protected:
    bool has_in_num_ = false;
    int32_t in_num_;
      
      // optional field out_num
     public:
    bool has_out_num() const;
    const int32_t& out_num() const;
    void clear_out_num();
    void set_out_num(const int32_t& value);
    int32_t* mutable_out_num();
   protected:
    bool has_out_num_ = false;
    int32_t out_num_;
      
     // oneof field in_box: concat_box
   public:
    bool has_concat_box() const;
    void clear_concat_box();
    const ::oneflow::cfg::BoxConcatConf& concat_box() const;
      ::oneflow::cfg::BoxConcatConf* mutable_concat_box();
      
     // oneof field in_box: add_box
   public:
    bool has_add_box() const;
    void clear_add_box();
    const ::oneflow::cfg::BoxAddConf& add_box() const;
      ::oneflow::cfg::BoxAddConf* mutable_add_box();
      
     // oneof field out_box: split_box
   public:
    bool has_split_box() const;
    void clear_split_box();
    const ::oneflow::cfg::BoxSplitConf& split_box() const;
      ::oneflow::cfg::BoxSplitConf* mutable_split_box();
      
     // oneof field out_box: clone_box
   public:
    bool has_clone_box() const;
    void clear_clone_box();
    const ::oneflow::cfg::BoxCloneConf& clone_box() const;
      ::oneflow::cfg::BoxCloneConf* mutable_clone_box();
           
   public:
    // oneof in_box
    InBoxCase in_box_case() const;
    bool has_in_box() const;
   protected:
    void clear_in_box();
    void in_box_copy_from(const _BoxingOpConf_& other);
    union InBoxUnion {
      // 64-bit aligned
      uint64_t __in_box_for_padding_64bit__;
          char concat_box_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxConcatConf>)];
            char add_box_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxAddConf>)];
        } in_box_;
    InBoxCase in_box_case_ = IN_BOX_NOT_SET;
     
   public:
    // oneof out_box
    OutBoxCase out_box_case() const;
    bool has_out_box() const;
   protected:
    void clear_out_box();
    void out_box_copy_from(const _BoxingOpConf_& other);
    union OutBoxUnion {
      // 64-bit aligned
      uint64_t __out_box_for_padding_64bit__;
          char split_box_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxSplitConf>)];
            char clone_box_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxCloneConf>)];
        } out_box_;
    OutBoxCase out_box_case_ = OUT_BOX_NOT_SET;
     
   public:
    int compare(const _BoxingOpConf_& other);

    bool operator==(const _BoxingOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxingOpConf_& other) const;
  };

  ConstBoxingOpConf(const ::std::shared_ptr<_BoxingOpConf_>& data);
  ConstBoxingOpConf(const ConstBoxingOpConf&);
  ConstBoxingOpConf(ConstBoxingOpConf&&) noexcept;
  ConstBoxingOpConf();
  ConstBoxingOpConf(const ::oneflow::BoxingOpConf& proto_boxingopconf);
  virtual ~ConstBoxingOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxingopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field in_num
 public:
  bool has_in_num() const;
  const int32_t& in_num() const;
  // used by pybind11 only
  // required or optional field out_num
 public:
  bool has_out_num() const;
  const int32_t& out_num() const;
  // used by pybind11 only
 // oneof field in_box: concat_box
 public:
  bool has_concat_box() const;
  const ::oneflow::cfg::BoxConcatConf& concat_box() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxConcatConf> shared_const_concat_box() const;
 // oneof field in_box: add_box
 public:
  bool has_add_box() const;
  const ::oneflow::cfg::BoxAddConf& add_box() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxAddConf> shared_const_add_box() const;
 // oneof field out_box: split_box
 public:
  bool has_split_box() const;
  const ::oneflow::cfg::BoxSplitConf& split_box() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxSplitConf> shared_const_split_box() const;
 // oneof field out_box: clone_box
 public:
  bool has_clone_box() const;
  const ::oneflow::cfg::BoxCloneConf& clone_box() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxCloneConf> shared_const_clone_box() const;
 public:
  InBoxCase in_box_case() const;
 protected:
  bool has_in_box() const;
 public:
  OutBoxCase out_box_case() const;
 protected:
  bool has_out_box() const;

 public:
  ::std::shared_ptr<ConstBoxingOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxingOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxingOpConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxingOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxingOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxingOpConf
  void BuildFromProto(const PbMessage& proto_boxingopconf);
  
  ::std::shared_ptr<_BoxingOpConf_> data_;
};

class BoxingOpConf final : public ConstBoxingOpConf {
 public:
  BoxingOpConf(const ::std::shared_ptr<_BoxingOpConf_>& data);
  BoxingOpConf(const BoxingOpConf& other);
  // enable nothrow for ::std::vector<BoxingOpConf> resize 
  BoxingOpConf(BoxingOpConf&&) noexcept;
  BoxingOpConf();
  explicit BoxingOpConf(const ::oneflow::BoxingOpConf& proto_boxingopconf);

  ~BoxingOpConf() override;

  void InitFromProto(const PbMessage& proto_boxingopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxingOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxingOpConf& other) const;
  void Clear();
  void CopyFrom(const BoxingOpConf& other);
  BoxingOpConf& operator=(const BoxingOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field in_num
 public:
  void clear_in_num();
  void set_in_num(const int32_t& value);
  int32_t* mutable_in_num();
  // required or optional field out_num
 public:
  void clear_out_num();
  void set_out_num(const int32_t& value);
  int32_t* mutable_out_num();
  void clear_concat_box();
  ::oneflow::cfg::BoxConcatConf* mutable_concat_box();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxConcatConf> shared_mutable_concat_box();
  void clear_add_box();
  ::oneflow::cfg::BoxAddConf* mutable_add_box();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxAddConf> shared_mutable_add_box();
  void clear_split_box();
  ::oneflow::cfg::BoxSplitConf* mutable_split_box();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxSplitConf> shared_mutable_split_box();
  void clear_clone_box();
  ::oneflow::cfg::BoxCloneConf* mutable_clone_box();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxCloneConf> shared_mutable_clone_box();

  ::std::shared_ptr<BoxingOpConf> __SharedMutable__();
};


class ConstDynamicReshapeOpConf : public ::oneflow::cfg::Message {
 public:

  class _DynamicReshapeOpConf_ {
   public:
    _DynamicReshapeOpConf_();
    explicit _DynamicReshapeOpConf_(const _DynamicReshapeOpConf_& other);
    explicit _DynamicReshapeOpConf_(_DynamicReshapeOpConf_&& other);
    _DynamicReshapeOpConf_(const ::oneflow::DynamicReshapeOpConf& proto_dynamicreshapeopconf);
    ~_DynamicReshapeOpConf_();

    void InitFromProto(const ::oneflow::DynamicReshapeOpConf& proto_dynamicreshapeopconf);

    void ToProto(::oneflow::DynamicReshapeOpConf* proto_dynamicreshapeopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DynamicReshapeOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
           
   public:
    int compare(const _DynamicReshapeOpConf_& other);

    bool operator==(const _DynamicReshapeOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DynamicReshapeOpConf_& other) const;
  };

  ConstDynamicReshapeOpConf(const ::std::shared_ptr<_DynamicReshapeOpConf_>& data);
  ConstDynamicReshapeOpConf(const ConstDynamicReshapeOpConf&);
  ConstDynamicReshapeOpConf(ConstDynamicReshapeOpConf&&) noexcept;
  ConstDynamicReshapeOpConf();
  ConstDynamicReshapeOpConf(const ::oneflow::DynamicReshapeOpConf& proto_dynamicreshapeopconf);
  virtual ~ConstDynamicReshapeOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_dynamicreshapeopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;

 public:
  ::std::shared_ptr<ConstDynamicReshapeOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDynamicReshapeOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDynamicReshapeOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DynamicReshapeOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DynamicReshapeOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDynamicReshapeOpConf
  void BuildFromProto(const PbMessage& proto_dynamicreshapeopconf);
  
  ::std::shared_ptr<_DynamicReshapeOpConf_> data_;
};

class DynamicReshapeOpConf final : public ConstDynamicReshapeOpConf {
 public:
  DynamicReshapeOpConf(const ::std::shared_ptr<_DynamicReshapeOpConf_>& data);
  DynamicReshapeOpConf(const DynamicReshapeOpConf& other);
  // enable nothrow for ::std::vector<DynamicReshapeOpConf> resize 
  DynamicReshapeOpConf(DynamicReshapeOpConf&&) noexcept;
  DynamicReshapeOpConf();
  explicit DynamicReshapeOpConf(const ::oneflow::DynamicReshapeOpConf& proto_dynamicreshapeopconf);

  ~DynamicReshapeOpConf() override;

  void InitFromProto(const PbMessage& proto_dynamicreshapeopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DynamicReshapeOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DynamicReshapeOpConf& other) const;
  void Clear();
  void CopyFrom(const DynamicReshapeOpConf& other);
  DynamicReshapeOpConf& operator=(const DynamicReshapeOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();

  ::std::shared_ptr<DynamicReshapeOpConf> __SharedMutable__();
};


class ConstDynamicReshapeLikeOpConf : public ::oneflow::cfg::Message {
 public:

  class _DynamicReshapeLikeOpConf_ {
   public:
    _DynamicReshapeLikeOpConf_();
    explicit _DynamicReshapeLikeOpConf_(const _DynamicReshapeLikeOpConf_& other);
    explicit _DynamicReshapeLikeOpConf_(_DynamicReshapeLikeOpConf_&& other);
    _DynamicReshapeLikeOpConf_(const ::oneflow::DynamicReshapeLikeOpConf& proto_dynamicreshapelikeopconf);
    ~_DynamicReshapeLikeOpConf_();

    void InitFromProto(const ::oneflow::DynamicReshapeLikeOpConf& proto_dynamicreshapelikeopconf);

    void ToProto(::oneflow::DynamicReshapeLikeOpConf* proto_dynamicreshapelikeopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DynamicReshapeLikeOpConf_& other);
  
      // optional field x
     public:
    bool has_x() const;
    const ::std::string& x() const;
    void clear_x();
    void set_x(const ::std::string& value);
    ::std::string* mutable_x();
   protected:
    bool has_x_ = false;
    ::std::string x_;
      
      // optional field y
     public:
    bool has_y() const;
    const ::std::string& y() const;
    void clear_y();
    void set_y(const ::std::string& value);
    ::std::string* mutable_y();
   protected:
    bool has_y_ = false;
    ::std::string y_;
      
      // optional field like
     public:
    bool has_like() const;
    const ::std::string& like() const;
    void clear_like();
    void set_like(const ::std::string& value);
    ::std::string* mutable_like();
   protected:
    bool has_like_ = false;
    ::std::string like_;
           
   public:
    int compare(const _DynamicReshapeLikeOpConf_& other);

    bool operator==(const _DynamicReshapeLikeOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DynamicReshapeLikeOpConf_& other) const;
  };

  ConstDynamicReshapeLikeOpConf(const ::std::shared_ptr<_DynamicReshapeLikeOpConf_>& data);
  ConstDynamicReshapeLikeOpConf(const ConstDynamicReshapeLikeOpConf&);
  ConstDynamicReshapeLikeOpConf(ConstDynamicReshapeLikeOpConf&&) noexcept;
  ConstDynamicReshapeLikeOpConf();
  ConstDynamicReshapeLikeOpConf(const ::oneflow::DynamicReshapeLikeOpConf& proto_dynamicreshapelikeopconf);
  virtual ~ConstDynamicReshapeLikeOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_dynamicreshapelikeopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field x
 public:
  bool has_x() const;
  const ::std::string& x() const;
  // used by pybind11 only
  // required or optional field y
 public:
  bool has_y() const;
  const ::std::string& y() const;
  // used by pybind11 only
  // required or optional field like
 public:
  bool has_like() const;
  const ::std::string& like() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDynamicReshapeLikeOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDynamicReshapeLikeOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDynamicReshapeLikeOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DynamicReshapeLikeOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DynamicReshapeLikeOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDynamicReshapeLikeOpConf
  void BuildFromProto(const PbMessage& proto_dynamicreshapelikeopconf);
  
  ::std::shared_ptr<_DynamicReshapeLikeOpConf_> data_;
};

class DynamicReshapeLikeOpConf final : public ConstDynamicReshapeLikeOpConf {
 public:
  DynamicReshapeLikeOpConf(const ::std::shared_ptr<_DynamicReshapeLikeOpConf_>& data);
  DynamicReshapeLikeOpConf(const DynamicReshapeLikeOpConf& other);
  // enable nothrow for ::std::vector<DynamicReshapeLikeOpConf> resize 
  DynamicReshapeLikeOpConf(DynamicReshapeLikeOpConf&&) noexcept;
  DynamicReshapeLikeOpConf();
  explicit DynamicReshapeLikeOpConf(const ::oneflow::DynamicReshapeLikeOpConf& proto_dynamicreshapelikeopconf);

  ~DynamicReshapeLikeOpConf() override;

  void InitFromProto(const PbMessage& proto_dynamicreshapelikeopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DynamicReshapeLikeOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DynamicReshapeLikeOpConf& other) const;
  void Clear();
  void CopyFrom(const DynamicReshapeLikeOpConf& other);
  DynamicReshapeLikeOpConf& operator=(const DynamicReshapeLikeOpConf& other);

  // required or optional field x
 public:
  void clear_x();
  void set_x(const ::std::string& value);
  ::std::string* mutable_x();
  // required or optional field y
 public:
  void clear_y();
  void set_y(const ::std::string& value);
  ::std::string* mutable_y();
  // required or optional field like
 public:
  void clear_like();
  void set_like(const ::std::string& value);
  ::std::string* mutable_like();

  ::std::shared_ptr<DynamicReshapeLikeOpConf> __SharedMutable__();
};


class ConstFeedInputOpConf : public ::oneflow::cfg::Message {
 public:

  class _FeedInputOpConf_ {
   public:
    _FeedInputOpConf_();
    explicit _FeedInputOpConf_(const _FeedInputOpConf_& other);
    explicit _FeedInputOpConf_(_FeedInputOpConf_&& other);
    _FeedInputOpConf_(const ::oneflow::FeedInputOpConf& proto_feedinputopconf);
    ~_FeedInputOpConf_();

    void InitFromProto(const ::oneflow::FeedInputOpConf& proto_feedinputopconf);

    void ToProto(::oneflow::FeedInputOpConf* proto_feedinputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FeedInputOpConf_& other);
  
      // optional field in_0
     public:
    bool has_in_0() const;
    const ::std::string& in_0() const;
    void clear_in_0();
    void set_in_0(const ::std::string& value);
    ::std::string* mutable_in_0();
   protected:
    bool has_in_0_ = false;
    ::std::string in_0_;
      
      // optional field out_0
     public:
    bool has_out_0() const;
    const ::std::string& out_0() const;
    void clear_out_0();
    void set_out_0(const ::std::string& value);
    ::std::string* mutable_out_0();
   protected:
    bool has_out_0_ = false;
    ::std::string out_0_;
           
   public:
    int compare(const _FeedInputOpConf_& other);

    bool operator==(const _FeedInputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FeedInputOpConf_& other) const;
  };

  ConstFeedInputOpConf(const ::std::shared_ptr<_FeedInputOpConf_>& data);
  ConstFeedInputOpConf(const ConstFeedInputOpConf&);
  ConstFeedInputOpConf(ConstFeedInputOpConf&&) noexcept;
  ConstFeedInputOpConf();
  ConstFeedInputOpConf(const ::oneflow::FeedInputOpConf& proto_feedinputopconf);
  virtual ~ConstFeedInputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_feedinputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in_0
 public:
  bool has_in_0() const;
  const ::std::string& in_0() const;
  // used by pybind11 only
  // required or optional field out_0
 public:
  bool has_out_0() const;
  const ::std::string& out_0() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstFeedInputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFeedInputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFeedInputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_FeedInputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FeedInputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFeedInputOpConf
  void BuildFromProto(const PbMessage& proto_feedinputopconf);
  
  ::std::shared_ptr<_FeedInputOpConf_> data_;
};

class FeedInputOpConf final : public ConstFeedInputOpConf {
 public:
  FeedInputOpConf(const ::std::shared_ptr<_FeedInputOpConf_>& data);
  FeedInputOpConf(const FeedInputOpConf& other);
  // enable nothrow for ::std::vector<FeedInputOpConf> resize 
  FeedInputOpConf(FeedInputOpConf&&) noexcept;
  FeedInputOpConf();
  explicit FeedInputOpConf(const ::oneflow::FeedInputOpConf& proto_feedinputopconf);

  ~FeedInputOpConf() override;

  void InitFromProto(const PbMessage& proto_feedinputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FeedInputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FeedInputOpConf& other) const;
  void Clear();
  void CopyFrom(const FeedInputOpConf& other);
  FeedInputOpConf& operator=(const FeedInputOpConf& other);

  // required or optional field in_0
 public:
  void clear_in_0();
  void set_in_0(const ::std::string& value);
  ::std::string* mutable_in_0();
  // required or optional field out_0
 public:
  void clear_out_0();
  void set_out_0(const ::std::string& value);
  ::std::string* mutable_out_0();

  ::std::shared_ptr<FeedInputOpConf> __SharedMutable__();
};


class ConstFeedVariableOpConf : public ::oneflow::cfg::Message {
 public:

  class _FeedVariableOpConf_ {
   public:
    _FeedVariableOpConf_();
    explicit _FeedVariableOpConf_(const _FeedVariableOpConf_& other);
    explicit _FeedVariableOpConf_(_FeedVariableOpConf_&& other);
    _FeedVariableOpConf_(const ::oneflow::FeedVariableOpConf& proto_feedvariableopconf);
    ~_FeedVariableOpConf_();

    void InitFromProto(const ::oneflow::FeedVariableOpConf& proto_feedvariableopconf);

    void ToProto(::oneflow::FeedVariableOpConf* proto_feedvariableopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FeedVariableOpConf_& other);
  
      // optional field in_0
     public:
    bool has_in_0() const;
    const ::std::string& in_0() const;
    void clear_in_0();
    void set_in_0(const ::std::string& value);
    ::std::string* mutable_in_0();
   protected:
    bool has_in_0_ = false;
    ::std::string in_0_;
      
      // optional field out_0
     public:
    bool has_out_0() const;
    const ::std::string& out_0() const;
    void clear_out_0();
    void set_out_0(const ::std::string& value);
    ::std::string* mutable_out_0();
   protected:
    bool has_out_0_ = false;
    ::std::string out_0_;
           
   public:
    int compare(const _FeedVariableOpConf_& other);

    bool operator==(const _FeedVariableOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FeedVariableOpConf_& other) const;
  };

  ConstFeedVariableOpConf(const ::std::shared_ptr<_FeedVariableOpConf_>& data);
  ConstFeedVariableOpConf(const ConstFeedVariableOpConf&);
  ConstFeedVariableOpConf(ConstFeedVariableOpConf&&) noexcept;
  ConstFeedVariableOpConf();
  ConstFeedVariableOpConf(const ::oneflow::FeedVariableOpConf& proto_feedvariableopconf);
  virtual ~ConstFeedVariableOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_feedvariableopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in_0
 public:
  bool has_in_0() const;
  const ::std::string& in_0() const;
  // used by pybind11 only
  // required or optional field out_0
 public:
  bool has_out_0() const;
  const ::std::string& out_0() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstFeedVariableOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFeedVariableOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFeedVariableOpConf& other) const;
 protected:
  const ::std::shared_ptr<_FeedVariableOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FeedVariableOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFeedVariableOpConf
  void BuildFromProto(const PbMessage& proto_feedvariableopconf);
  
  ::std::shared_ptr<_FeedVariableOpConf_> data_;
};

class FeedVariableOpConf final : public ConstFeedVariableOpConf {
 public:
  FeedVariableOpConf(const ::std::shared_ptr<_FeedVariableOpConf_>& data);
  FeedVariableOpConf(const FeedVariableOpConf& other);
  // enable nothrow for ::std::vector<FeedVariableOpConf> resize 
  FeedVariableOpConf(FeedVariableOpConf&&) noexcept;
  FeedVariableOpConf();
  explicit FeedVariableOpConf(const ::oneflow::FeedVariableOpConf& proto_feedvariableopconf);

  ~FeedVariableOpConf() override;

  void InitFromProto(const PbMessage& proto_feedvariableopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FeedVariableOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FeedVariableOpConf& other) const;
  void Clear();
  void CopyFrom(const FeedVariableOpConf& other);
  FeedVariableOpConf& operator=(const FeedVariableOpConf& other);

  // required or optional field in_0
 public:
  void clear_in_0();
  void set_in_0(const ::std::string& value);
  ::std::string* mutable_in_0();
  // required or optional field out_0
 public:
  void clear_out_0();
  void set_out_0(const ::std::string& value);
  ::std::string* mutable_out_0();

  ::std::shared_ptr<FeedVariableOpConf> __SharedMutable__();
};


class ConstFetchOutputOpConf : public ::oneflow::cfg::Message {
 public:

  class _FetchOutputOpConf_ {
   public:
    _FetchOutputOpConf_();
    explicit _FetchOutputOpConf_(const _FetchOutputOpConf_& other);
    explicit _FetchOutputOpConf_(_FetchOutputOpConf_&& other);
    _FetchOutputOpConf_(const ::oneflow::FetchOutputOpConf& proto_fetchoutputopconf);
    ~_FetchOutputOpConf_();

    void InitFromProto(const ::oneflow::FetchOutputOpConf& proto_fetchoutputopconf);

    void ToProto(::oneflow::FetchOutputOpConf* proto_fetchoutputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _FetchOutputOpConf_& other);
  
      // optional field in_0
     public:
    bool has_in_0() const;
    const ::std::string& in_0() const;
    void clear_in_0();
    void set_in_0(const ::std::string& value);
    ::std::string* mutable_in_0();
   protected:
    bool has_in_0_ = false;
    ::std::string in_0_;
      
      // optional field out_0
     public:
    bool has_out_0() const;
    const ::std::string& out_0() const;
    void clear_out_0();
    void set_out_0(const ::std::string& value);
    ::std::string* mutable_out_0();
   protected:
    bool has_out_0_ = false;
    ::std::string out_0_;
           
   public:
    int compare(const _FetchOutputOpConf_& other);

    bool operator==(const _FetchOutputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _FetchOutputOpConf_& other) const;
  };

  ConstFetchOutputOpConf(const ::std::shared_ptr<_FetchOutputOpConf_>& data);
  ConstFetchOutputOpConf(const ConstFetchOutputOpConf&);
  ConstFetchOutputOpConf(ConstFetchOutputOpConf&&) noexcept;
  ConstFetchOutputOpConf();
  ConstFetchOutputOpConf(const ::oneflow::FetchOutputOpConf& proto_fetchoutputopconf);
  virtual ~ConstFetchOutputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_fetchoutputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in_0
 public:
  bool has_in_0() const;
  const ::std::string& in_0() const;
  // used by pybind11 only
  // required or optional field out_0
 public:
  bool has_out_0() const;
  const ::std::string& out_0() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstFetchOutputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstFetchOutputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstFetchOutputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_FetchOutputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_FetchOutputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstFetchOutputOpConf
  void BuildFromProto(const PbMessage& proto_fetchoutputopconf);
  
  ::std::shared_ptr<_FetchOutputOpConf_> data_;
};

class FetchOutputOpConf final : public ConstFetchOutputOpConf {
 public:
  FetchOutputOpConf(const ::std::shared_ptr<_FetchOutputOpConf_>& data);
  FetchOutputOpConf(const FetchOutputOpConf& other);
  // enable nothrow for ::std::vector<FetchOutputOpConf> resize 
  FetchOutputOpConf(FetchOutputOpConf&&) noexcept;
  FetchOutputOpConf();
  explicit FetchOutputOpConf(const ::oneflow::FetchOutputOpConf& proto_fetchoutputopconf);

  ~FetchOutputOpConf() override;

  void InitFromProto(const PbMessage& proto_fetchoutputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const FetchOutputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const FetchOutputOpConf& other) const;
  void Clear();
  void CopyFrom(const FetchOutputOpConf& other);
  FetchOutputOpConf& operator=(const FetchOutputOpConf& other);

  // required or optional field in_0
 public:
  void clear_in_0();
  void set_in_0(const ::std::string& value);
  ::std::string* mutable_in_0();
  // required or optional field out_0
 public:
  void clear_out_0();
  void set_out_0(const ::std::string& value);
  ::std::string* mutable_out_0();

  ::std::shared_ptr<FetchOutputOpConf> __SharedMutable__();
};


class ConstInputOpConf : public ::oneflow::cfg::Message {
 public:

  class _InputOpConf_ {
   public:
    _InputOpConf_();
    explicit _InputOpConf_(const _InputOpConf_& other);
    explicit _InputOpConf_(_InputOpConf_&& other);
    _InputOpConf_(const ::oneflow::InputOpConf& proto_inputopconf);
    ~_InputOpConf_();

    void InitFromProto(const ::oneflow::InputOpConf& proto_inputopconf);

    void ToProto(::oneflow::InputOpConf* proto_inputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InputOpConf_& other);
  
      // optional field tick
     public:
    bool has_tick() const;
    const ::std::string& tick() const;
    void clear_tick();
    void set_tick(const ::std::string& value);
    ::std::string* mutable_tick();
   protected:
    bool has_tick_ = false;
    ::std::string tick_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field blob_conf
     public:
    bool has_blob_conf() const;
    const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
    void clear_blob_conf();
    ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
   protected:
    bool has_blob_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> blob_conf_;
           
   public:
    int compare(const _InputOpConf_& other);

    bool operator==(const _InputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InputOpConf_& other) const;
  };

  ConstInputOpConf(const ::std::shared_ptr<_InputOpConf_>& data);
  ConstInputOpConf(const ConstInputOpConf&);
  ConstInputOpConf(ConstInputOpConf&&) noexcept;
  ConstInputOpConf();
  ConstInputOpConf(const ::oneflow::InputOpConf& proto_inputopconf);
  virtual ~ConstInputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_inputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field tick
 public:
  bool has_tick() const;
  const ::std::string& tick() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field blob_conf
 public:
  bool has_blob_conf() const;
  const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInterfaceBlobConf> shared_const_blob_conf() const;

 public:
  ::std::shared_ptr<ConstInputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_InputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInputOpConf
  void BuildFromProto(const PbMessage& proto_inputopconf);
  
  ::std::shared_ptr<_InputOpConf_> data_;
};

class InputOpConf final : public ConstInputOpConf {
 public:
  InputOpConf(const ::std::shared_ptr<_InputOpConf_>& data);
  InputOpConf(const InputOpConf& other);
  // enable nothrow for ::std::vector<InputOpConf> resize 
  InputOpConf(InputOpConf&&) noexcept;
  InputOpConf();
  explicit InputOpConf(const ::oneflow::InputOpConf& proto_inputopconf);

  ~InputOpConf() override;

  void InitFromProto(const PbMessage& proto_inputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InputOpConf& other) const;
  void Clear();
  void CopyFrom(const InputOpConf& other);
  InputOpConf& operator=(const InputOpConf& other);

  // required or optional field tick
 public:
  void clear_tick();
  void set_tick(const ::std::string& value);
  ::std::string* mutable_tick();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field blob_conf
 public:
  void clear_blob_conf();
  ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> shared_mutable_blob_conf();

  ::std::shared_ptr<InputOpConf> __SharedMutable__();
};


class ConstForeignInputOpConf : public ::oneflow::cfg::Message {
 public:

  class _ForeignInputOpConf_ {
   public:
    _ForeignInputOpConf_();
    explicit _ForeignInputOpConf_(const _ForeignInputOpConf_& other);
    explicit _ForeignInputOpConf_(_ForeignInputOpConf_&& other);
    _ForeignInputOpConf_(const ::oneflow::ForeignInputOpConf& proto_foreigninputopconf);
    ~_ForeignInputOpConf_();

    void InitFromProto(const ::oneflow::ForeignInputOpConf& proto_foreigninputopconf);

    void ToProto(::oneflow::ForeignInputOpConf* proto_foreigninputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ForeignInputOpConf_& other);
  
      // optional field tick
     public:
    bool has_tick() const;
    const ::std::string& tick() const;
    void clear_tick();
    void set_tick(const ::std::string& value);
    ::std::string* mutable_tick();
   protected:
    bool has_tick_ = false;
    ::std::string tick_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field blob_conf
     public:
    bool has_blob_conf() const;
    const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
    void clear_blob_conf();
    ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
   protected:
    bool has_blob_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> blob_conf_;
      
      // optional field ofblob_buffer_name
     public:
    bool has_ofblob_buffer_name() const;
    const ::std::string& ofblob_buffer_name() const;
    void clear_ofblob_buffer_name();
    void set_ofblob_buffer_name(const ::std::string& value);
    ::std::string* mutable_ofblob_buffer_name();
   protected:
    bool has_ofblob_buffer_name_ = false;
    ::std::string ofblob_buffer_name_;
           
   public:
    int compare(const _ForeignInputOpConf_& other);

    bool operator==(const _ForeignInputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ForeignInputOpConf_& other) const;
  };

  ConstForeignInputOpConf(const ::std::shared_ptr<_ForeignInputOpConf_>& data);
  ConstForeignInputOpConf(const ConstForeignInputOpConf&);
  ConstForeignInputOpConf(ConstForeignInputOpConf&&) noexcept;
  ConstForeignInputOpConf();
  ConstForeignInputOpConf(const ::oneflow::ForeignInputOpConf& proto_foreigninputopconf);
  virtual ~ConstForeignInputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_foreigninputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field tick
 public:
  bool has_tick() const;
  const ::std::string& tick() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field blob_conf
 public:
  bool has_blob_conf() const;
  const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInterfaceBlobConf> shared_const_blob_conf() const;
  // required or optional field ofblob_buffer_name
 public:
  bool has_ofblob_buffer_name() const;
  const ::std::string& ofblob_buffer_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstForeignInputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstForeignInputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstForeignInputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ForeignInputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ForeignInputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstForeignInputOpConf
  void BuildFromProto(const PbMessage& proto_foreigninputopconf);
  
  ::std::shared_ptr<_ForeignInputOpConf_> data_;
};

class ForeignInputOpConf final : public ConstForeignInputOpConf {
 public:
  ForeignInputOpConf(const ::std::shared_ptr<_ForeignInputOpConf_>& data);
  ForeignInputOpConf(const ForeignInputOpConf& other);
  // enable nothrow for ::std::vector<ForeignInputOpConf> resize 
  ForeignInputOpConf(ForeignInputOpConf&&) noexcept;
  ForeignInputOpConf();
  explicit ForeignInputOpConf(const ::oneflow::ForeignInputOpConf& proto_foreigninputopconf);

  ~ForeignInputOpConf() override;

  void InitFromProto(const PbMessage& proto_foreigninputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ForeignInputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ForeignInputOpConf& other) const;
  void Clear();
  void CopyFrom(const ForeignInputOpConf& other);
  ForeignInputOpConf& operator=(const ForeignInputOpConf& other);

  // required or optional field tick
 public:
  void clear_tick();
  void set_tick(const ::std::string& value);
  ::std::string* mutable_tick();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field blob_conf
 public:
  void clear_blob_conf();
  ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> shared_mutable_blob_conf();
  // required or optional field ofblob_buffer_name
 public:
  void clear_ofblob_buffer_name();
  void set_ofblob_buffer_name(const ::std::string& value);
  ::std::string* mutable_ofblob_buffer_name();

  ::std::shared_ptr<ForeignInputOpConf> __SharedMutable__();
};


class ConstReturnOpConf : public ::oneflow::cfg::Message {
 public:

  class _ReturnOpConf_ {
   public:
    _ReturnOpConf_();
    explicit _ReturnOpConf_(const _ReturnOpConf_& other);
    explicit _ReturnOpConf_(_ReturnOpConf_&& other);
    _ReturnOpConf_(const ::oneflow::ReturnOpConf& proto_returnopconf);
    ~_ReturnOpConf_();

    void InitFromProto(const ::oneflow::ReturnOpConf& proto_returnopconf);

    void ToProto(::oneflow::ReturnOpConf* proto_returnopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ReturnOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _ReturnOpConf_& other);

    bool operator==(const _ReturnOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ReturnOpConf_& other) const;
  };

  ConstReturnOpConf(const ::std::shared_ptr<_ReturnOpConf_>& data);
  ConstReturnOpConf(const ConstReturnOpConf&);
  ConstReturnOpConf(ConstReturnOpConf&&) noexcept;
  ConstReturnOpConf();
  ConstReturnOpConf(const ::oneflow::ReturnOpConf& proto_returnopconf);
  virtual ~ConstReturnOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_returnopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstReturnOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstReturnOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstReturnOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ReturnOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ReturnOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstReturnOpConf
  void BuildFromProto(const PbMessage& proto_returnopconf);
  
  ::std::shared_ptr<_ReturnOpConf_> data_;
};

class ReturnOpConf final : public ConstReturnOpConf {
 public:
  ReturnOpConf(const ::std::shared_ptr<_ReturnOpConf_>& data);
  ReturnOpConf(const ReturnOpConf& other);
  // enable nothrow for ::std::vector<ReturnOpConf> resize 
  ReturnOpConf(ReturnOpConf&&) noexcept;
  ReturnOpConf();
  explicit ReturnOpConf(const ::oneflow::ReturnOpConf& proto_returnopconf);

  ~ReturnOpConf() override;

  void InitFromProto(const PbMessage& proto_returnopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ReturnOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ReturnOpConf& other) const;
  void Clear();
  void CopyFrom(const ReturnOpConf& other);
  ReturnOpConf& operator=(const ReturnOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<ReturnOpConf> __SharedMutable__();
};


class ConstOutputOpConf : public ::oneflow::cfg::Message {
 public:

  class _OutputOpConf_ {
   public:
    _OutputOpConf_();
    explicit _OutputOpConf_(const _OutputOpConf_& other);
    explicit _OutputOpConf_(_OutputOpConf_&& other);
    _OutputOpConf_(const ::oneflow::OutputOpConf& proto_outputopconf);
    ~_OutputOpConf_();

    void InitFromProto(const ::oneflow::OutputOpConf& proto_outputopconf);

    void ToProto(::oneflow::OutputOpConf* proto_outputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OutputOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field blob_conf
     public:
    bool has_blob_conf() const;
    const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
    void clear_blob_conf();
    ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
   protected:
    bool has_blob_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> blob_conf_;
           
   public:
    int compare(const _OutputOpConf_& other);

    bool operator==(const _OutputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OutputOpConf_& other) const;
  };

  ConstOutputOpConf(const ::std::shared_ptr<_OutputOpConf_>& data);
  ConstOutputOpConf(const ConstOutputOpConf&);
  ConstOutputOpConf(ConstOutputOpConf&&) noexcept;
  ConstOutputOpConf();
  ConstOutputOpConf(const ::oneflow::OutputOpConf& proto_outputopconf);
  virtual ~ConstOutputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_outputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field blob_conf
 public:
  bool has_blob_conf() const;
  const ::oneflow::cfg::InterfaceBlobConf& blob_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInterfaceBlobConf> shared_const_blob_conf() const;

 public:
  ::std::shared_ptr<ConstOutputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOutputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOutputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_OutputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OutputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOutputOpConf
  void BuildFromProto(const PbMessage& proto_outputopconf);
  
  ::std::shared_ptr<_OutputOpConf_> data_;
};

class OutputOpConf final : public ConstOutputOpConf {
 public:
  OutputOpConf(const ::std::shared_ptr<_OutputOpConf_>& data);
  OutputOpConf(const OutputOpConf& other);
  // enable nothrow for ::std::vector<OutputOpConf> resize 
  OutputOpConf(OutputOpConf&&) noexcept;
  OutputOpConf();
  explicit OutputOpConf(const ::oneflow::OutputOpConf& proto_outputopconf);

  ~OutputOpConf() override;

  void InitFromProto(const PbMessage& proto_outputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OutputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OutputOpConf& other) const;
  void Clear();
  void CopyFrom(const OutputOpConf& other);
  OutputOpConf& operator=(const OutputOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field blob_conf
 public:
  void clear_blob_conf();
  ::oneflow::cfg::InterfaceBlobConf* mutable_blob_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InterfaceBlobConf> shared_mutable_blob_conf();

  ::std::shared_ptr<OutputOpConf> __SharedMutable__();
};


class ConstForeignOutputOpConf : public ::oneflow::cfg::Message {
 public:

  class _ForeignOutputOpConf_ {
   public:
    _ForeignOutputOpConf_();
    explicit _ForeignOutputOpConf_(const _ForeignOutputOpConf_& other);
    explicit _ForeignOutputOpConf_(_ForeignOutputOpConf_&& other);
    _ForeignOutputOpConf_(const ::oneflow::ForeignOutputOpConf& proto_foreignoutputopconf);
    ~_ForeignOutputOpConf_();

    void InitFromProto(const ::oneflow::ForeignOutputOpConf& proto_foreignoutputopconf);

    void ToProto(::oneflow::ForeignOutputOpConf* proto_foreignoutputopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ForeignOutputOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field ofblob_buffer_name
     public:
    bool has_ofblob_buffer_name() const;
    const ::std::string& ofblob_buffer_name() const;
    void clear_ofblob_buffer_name();
    void set_ofblob_buffer_name(const ::std::string& value);
    ::std::string* mutable_ofblob_buffer_name();
   protected:
    bool has_ofblob_buffer_name_ = false;
    ::std::string ofblob_buffer_name_;
           
   public:
    int compare(const _ForeignOutputOpConf_& other);

    bool operator==(const _ForeignOutputOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ForeignOutputOpConf_& other) const;
  };

  ConstForeignOutputOpConf(const ::std::shared_ptr<_ForeignOutputOpConf_>& data);
  ConstForeignOutputOpConf(const ConstForeignOutputOpConf&);
  ConstForeignOutputOpConf(ConstForeignOutputOpConf&&) noexcept;
  ConstForeignOutputOpConf();
  ConstForeignOutputOpConf(const ::oneflow::ForeignOutputOpConf& proto_foreignoutputopconf);
  virtual ~ConstForeignOutputOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_foreignoutputopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field ofblob_buffer_name
 public:
  bool has_ofblob_buffer_name() const;
  const ::std::string& ofblob_buffer_name() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstForeignOutputOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstForeignOutputOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstForeignOutputOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ForeignOutputOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ForeignOutputOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstForeignOutputOpConf
  void BuildFromProto(const PbMessage& proto_foreignoutputopconf);
  
  ::std::shared_ptr<_ForeignOutputOpConf_> data_;
};

class ForeignOutputOpConf final : public ConstForeignOutputOpConf {
 public:
  ForeignOutputOpConf(const ::std::shared_ptr<_ForeignOutputOpConf_>& data);
  ForeignOutputOpConf(const ForeignOutputOpConf& other);
  // enable nothrow for ::std::vector<ForeignOutputOpConf> resize 
  ForeignOutputOpConf(ForeignOutputOpConf&&) noexcept;
  ForeignOutputOpConf();
  explicit ForeignOutputOpConf(const ::oneflow::ForeignOutputOpConf& proto_foreignoutputopconf);

  ~ForeignOutputOpConf() override;

  void InitFromProto(const PbMessage& proto_foreignoutputopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ForeignOutputOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ForeignOutputOpConf& other) const;
  void Clear();
  void CopyFrom(const ForeignOutputOpConf& other);
  ForeignOutputOpConf& operator=(const ForeignOutputOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field ofblob_buffer_name
 public:
  void clear_ofblob_buffer_name();
  void set_ofblob_buffer_name(const ::std::string& value);
  ::std::string* mutable_ofblob_buffer_name();

  ::std::shared_ptr<ForeignOutputOpConf> __SharedMutable__();
};


class ConstForeignWatchOpConf : public ::oneflow::cfg::Message {
 public:

  class _ForeignWatchOpConf_ {
   public:
    _ForeignWatchOpConf_();
    explicit _ForeignWatchOpConf_(const _ForeignWatchOpConf_& other);
    explicit _ForeignWatchOpConf_(_ForeignWatchOpConf_&& other);
    _ForeignWatchOpConf_(const ::oneflow::ForeignWatchOpConf& proto_foreignwatchopconf);
    ~_ForeignWatchOpConf_();

    void InitFromProto(const ::oneflow::ForeignWatchOpConf& proto_foreignwatchopconf);

    void ToProto(::oneflow::ForeignWatchOpConf* proto_foreignwatchopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ForeignWatchOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field handler_uuid
     public:
    bool has_handler_uuid() const;
    const ::std::string& handler_uuid() const;
    void clear_handler_uuid();
    void set_handler_uuid(const ::std::string& value);
    ::std::string* mutable_handler_uuid();
   protected:
    bool has_handler_uuid_ = false;
    ::std::string handler_uuid_;
           
   public:
    int compare(const _ForeignWatchOpConf_& other);

    bool operator==(const _ForeignWatchOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ForeignWatchOpConf_& other) const;
  };

  ConstForeignWatchOpConf(const ::std::shared_ptr<_ForeignWatchOpConf_>& data);
  ConstForeignWatchOpConf(const ConstForeignWatchOpConf&);
  ConstForeignWatchOpConf(ConstForeignWatchOpConf&&) noexcept;
  ConstForeignWatchOpConf();
  ConstForeignWatchOpConf(const ::oneflow::ForeignWatchOpConf& proto_foreignwatchopconf);
  virtual ~ConstForeignWatchOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_foreignwatchopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field handler_uuid
 public:
  bool has_handler_uuid() const;
  const ::std::string& handler_uuid() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstForeignWatchOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstForeignWatchOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstForeignWatchOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ForeignWatchOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ForeignWatchOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstForeignWatchOpConf
  void BuildFromProto(const PbMessage& proto_foreignwatchopconf);
  
  ::std::shared_ptr<_ForeignWatchOpConf_> data_;
};

class ForeignWatchOpConf final : public ConstForeignWatchOpConf {
 public:
  ForeignWatchOpConf(const ::std::shared_ptr<_ForeignWatchOpConf_>& data);
  ForeignWatchOpConf(const ForeignWatchOpConf& other);
  // enable nothrow for ::std::vector<ForeignWatchOpConf> resize 
  ForeignWatchOpConf(ForeignWatchOpConf&&) noexcept;
  ForeignWatchOpConf();
  explicit ForeignWatchOpConf(const ::oneflow::ForeignWatchOpConf& proto_foreignwatchopconf);

  ~ForeignWatchOpConf() override;

  void InitFromProto(const PbMessage& proto_foreignwatchopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ForeignWatchOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ForeignWatchOpConf& other) const;
  void Clear();
  void CopyFrom(const ForeignWatchOpConf& other);
  ForeignWatchOpConf& operator=(const ForeignWatchOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field handler_uuid
 public:
  void clear_handler_uuid();
  void set_handler_uuid(const ::std::string& value);
  ::std::string* mutable_handler_uuid();

  ::std::shared_ptr<ForeignWatchOpConf> __SharedMutable__();
};


class ConstVariableOpConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum initialize
 enum InitializeCase : unsigned int {
  INITIALIZE_NOT_SET = 0,
    kInitializer = 5,
    kInitializeWithSnapshot = 6,
   };

  class _VariableOpConf_ {
   public:
    _VariableOpConf_();
    explicit _VariableOpConf_(const _VariableOpConf_& other);
    explicit _VariableOpConf_(_VariableOpConf_&& other);
    _VariableOpConf_(const ::oneflow::VariableOpConf& proto_variableopconf);
    ~_VariableOpConf_();

    void InitFromProto(const ::oneflow::VariableOpConf& proto_variableopconf);

    void ToProto(::oneflow::VariableOpConf* proto_variableopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _VariableOpConf_& other);
  
      // optional field tick
     public:
    bool has_tick() const;
    const ::std::string& tick() const;
    void clear_tick();
    void set_tick(const ::std::string& value);
    ::std::string* mutable_tick();
   protected:
    bool has_tick_ = false;
    ::std::string tick_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
     // oneof field initialize: initializer
   public:
    bool has_initializer() const;
    void clear_initializer();
    const ::oneflow::cfg::InitializerConf& initializer() const;
      ::oneflow::cfg::InitializerConf* mutable_initializer();
      
     // oneof field initialize: initialize_with_snapshot
   public:
    bool has_initialize_with_snapshot() const;
    void clear_initialize_with_snapshot();
    const ::oneflow::cfg::InitializeWithSnapshotConf& initialize_with_snapshot() const;
      ::oneflow::cfg::InitializeWithSnapshotConf* mutable_initialize_with_snapshot();
      
      // optional field model_name
     public:
    bool has_model_name() const;
    const ::std::string& model_name() const;
    void clear_model_name();
    void set_model_name(const ::std::string& value);
    ::std::string* mutable_model_name();
   protected:
    bool has_model_name_ = false;
    ::std::string model_name_;
      
      // optional field random_seed
     public:
    bool has_random_seed() const;
    const int64_t& random_seed() const;
    void clear_random_seed();
    void set_random_seed(const int64_t& value);
    int64_t* mutable_random_seed();
   protected:
    bool has_random_seed_ = false;
    int64_t random_seed_;
      
      // optional field regularizer
     public:
    bool has_regularizer() const;
    const ::oneflow::cfg::RegularizerConf& regularizer() const;
    void clear_regularizer();
    ::oneflow::cfg::RegularizerConf* mutable_regularizer();
   protected:
    bool has_regularizer_ = false;
    ::std::shared_ptr<::oneflow::cfg::RegularizerConf> regularizer_;
      
      // optional field trainable
     public:
    bool has_trainable() const;
    const bool& trainable() const;
    void clear_trainable();
    void set_trainable(const bool& value);
    bool* mutable_trainable();
   protected:
    bool has_trainable_ = false;
    bool trainable_;
      
      // repeated field parallel_distribution
   public:
    ::std::size_t parallel_distribution_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& parallel_distribution() const;
    const ::std::string& parallel_distribution(::std::size_t index) const;
    void clear_parallel_distribution();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_parallel_distribution();
    ::std::string* mutable_parallel_distribution(::std::size_t index);
      void add_parallel_distribution(const ::std::string& value);
    void set_parallel_distribution(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> parallel_distribution_;
         
   public:
    // oneof initialize
    InitializeCase initialize_case() const;
    bool has_initialize() const;
   protected:
    void clear_initialize();
    void initialize_copy_from(const _VariableOpConf_& other);
    union InitializeUnion {
      // 64-bit aligned
      uint64_t __initialize_for_padding_64bit__;
          char initializer_[sizeof(::std::shared_ptr<::oneflow::cfg::InitializerConf>)];
            char initialize_with_snapshot_[sizeof(::std::shared_ptr<::oneflow::cfg::InitializeWithSnapshotConf>)];
        } initialize_;
    InitializeCase initialize_case_ = INITIALIZE_NOT_SET;
     
   public:
    int compare(const _VariableOpConf_& other);

    bool operator==(const _VariableOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _VariableOpConf_& other) const;
  };

  ConstVariableOpConf(const ::std::shared_ptr<_VariableOpConf_>& data);
  ConstVariableOpConf(const ConstVariableOpConf&);
  ConstVariableOpConf(ConstVariableOpConf&&) noexcept;
  ConstVariableOpConf();
  ConstVariableOpConf(const ::oneflow::VariableOpConf& proto_variableopconf);
  virtual ~ConstVariableOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_variableopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field tick
 public:
  bool has_tick() const;
  const ::std::string& tick() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
 // oneof field initialize: initializer
 public:
  bool has_initializer() const;
  const ::oneflow::cfg::InitializerConf& initializer() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInitializerConf> shared_const_initializer() const;
 // oneof field initialize: initialize_with_snapshot
 public:
  bool has_initialize_with_snapshot() const;
  const ::oneflow::cfg::InitializeWithSnapshotConf& initialize_with_snapshot() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInitializeWithSnapshotConf> shared_const_initialize_with_snapshot() const;
  // required or optional field model_name
 public:
  bool has_model_name() const;
  const ::std::string& model_name() const;
  // used by pybind11 only
  // required or optional field random_seed
 public:
  bool has_random_seed() const;
  const int64_t& random_seed() const;
  // used by pybind11 only
  // required or optional field regularizer
 public:
  bool has_regularizer() const;
  const ::oneflow::cfg::RegularizerConf& regularizer() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstRegularizerConf> shared_const_regularizer() const;
  // required or optional field trainable
 public:
  bool has_trainable() const;
  const bool& trainable() const;
  // used by pybind11 only
  // repeated field parallel_distribution
 public:
  ::std::size_t parallel_distribution_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& parallel_distribution() const;
  const ::std::string& parallel_distribution(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_parallel_distribution() const;
 public:
  InitializeCase initialize_case() const;
 protected:
  bool has_initialize() const;

 public:
  ::std::shared_ptr<ConstVariableOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstVariableOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstVariableOpConf& other) const;
 protected:
  const ::std::shared_ptr<_VariableOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_VariableOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstVariableOpConf
  void BuildFromProto(const PbMessage& proto_variableopconf);
  
  ::std::shared_ptr<_VariableOpConf_> data_;
};

class VariableOpConf final : public ConstVariableOpConf {
 public:
  VariableOpConf(const ::std::shared_ptr<_VariableOpConf_>& data);
  VariableOpConf(const VariableOpConf& other);
  // enable nothrow for ::std::vector<VariableOpConf> resize 
  VariableOpConf(VariableOpConf&&) noexcept;
  VariableOpConf();
  explicit VariableOpConf(const ::oneflow::VariableOpConf& proto_variableopconf);

  ~VariableOpConf() override;

  void InitFromProto(const PbMessage& proto_variableopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const VariableOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const VariableOpConf& other) const;
  void Clear();
  void CopyFrom(const VariableOpConf& other);
  VariableOpConf& operator=(const VariableOpConf& other);

  // required or optional field tick
 public:
  void clear_tick();
  void set_tick(const ::std::string& value);
  ::std::string* mutable_tick();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  void clear_initializer();
  ::oneflow::cfg::InitializerConf* mutable_initializer();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InitializerConf> shared_mutable_initializer();
  void clear_initialize_with_snapshot();
  ::oneflow::cfg::InitializeWithSnapshotConf* mutable_initialize_with_snapshot();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InitializeWithSnapshotConf> shared_mutable_initialize_with_snapshot();
  // required or optional field model_name
 public:
  void clear_model_name();
  void set_model_name(const ::std::string& value);
  ::std::string* mutable_model_name();
  // required or optional field random_seed
 public:
  void clear_random_seed();
  void set_random_seed(const int64_t& value);
  int64_t* mutable_random_seed();
  // required or optional field regularizer
 public:
  void clear_regularizer();
  ::oneflow::cfg::RegularizerConf* mutable_regularizer();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::RegularizerConf> shared_mutable_regularizer();
  // required or optional field trainable
 public:
  void clear_trainable();
  void set_trainable(const bool& value);
  bool* mutable_trainable();
  // repeated field parallel_distribution
 public:
  void clear_parallel_distribution();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_parallel_distribution();
  ::std::string* mutable_parallel_distribution(::std::size_t index);
  void add_parallel_distribution(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_parallel_distribution();
  void set_parallel_distribution(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<VariableOpConf> __SharedMutable__();
};


class ConstDecodeRandomOpConf : public ::oneflow::cfg::Message {
 public:

  class _DecodeRandomOpConf_ {
   public:
    _DecodeRandomOpConf_();
    explicit _DecodeRandomOpConf_(const _DecodeRandomOpConf_& other);
    explicit _DecodeRandomOpConf_(_DecodeRandomOpConf_&& other);
    _DecodeRandomOpConf_(const ::oneflow::DecodeRandomOpConf& proto_decoderandomopconf);
    ~_DecodeRandomOpConf_();

    void InitFromProto(const ::oneflow::DecodeRandomOpConf& proto_decoderandomopconf);

    void ToProto(::oneflow::DecodeRandomOpConf* proto_decoderandomopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DecodeRandomOpConf_& other);
  
      // optional field tick
     public:
    bool has_tick() const;
    const ::std::string& tick() const;
    void clear_tick();
    void set_tick(const ::std::string& value);
    ::std::string* mutable_tick();
   protected:
    bool has_tick_ = false;
    ::std::string tick_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
      // optional field data_initializer
     public:
    bool has_data_initializer() const;
    const ::oneflow::cfg::InitializerConf& data_initializer() const;
    void clear_data_initializer();
    ::oneflow::cfg::InitializerConf* mutable_data_initializer();
   protected:
    bool has_data_initializer_ = false;
    ::std::shared_ptr<::oneflow::cfg::InitializerConf> data_initializer_;
      
      // optional field batch_size
     public:
    bool has_batch_size() const;
    const int64_t& batch_size() const;
    void clear_batch_size();
    void set_batch_size(const int64_t& value);
    int64_t* mutable_batch_size();
   protected:
    bool has_batch_size_ = false;
    int64_t batch_size_;
           
   public:
    int compare(const _DecodeRandomOpConf_& other);

    bool operator==(const _DecodeRandomOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DecodeRandomOpConf_& other) const;
  };

  ConstDecodeRandomOpConf(const ::std::shared_ptr<_DecodeRandomOpConf_>& data);
  ConstDecodeRandomOpConf(const ConstDecodeRandomOpConf&);
  ConstDecodeRandomOpConf(ConstDecodeRandomOpConf&&) noexcept;
  ConstDecodeRandomOpConf();
  ConstDecodeRandomOpConf(const ::oneflow::DecodeRandomOpConf& proto_decoderandomopconf);
  virtual ~ConstDecodeRandomOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_decoderandomopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field tick
 public:
  bool has_tick() const;
  const ::std::string& tick() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
  // required or optional field data_initializer
 public:
  bool has_data_initializer() const;
  const ::oneflow::cfg::InitializerConf& data_initializer() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInitializerConf> shared_const_data_initializer() const;
  // required or optional field batch_size
 public:
  bool has_batch_size() const;
  const int64_t& batch_size() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDecodeRandomOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDecodeRandomOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDecodeRandomOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DecodeRandomOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DecodeRandomOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDecodeRandomOpConf
  void BuildFromProto(const PbMessage& proto_decoderandomopconf);
  
  ::std::shared_ptr<_DecodeRandomOpConf_> data_;
};

class DecodeRandomOpConf final : public ConstDecodeRandomOpConf {
 public:
  DecodeRandomOpConf(const ::std::shared_ptr<_DecodeRandomOpConf_>& data);
  DecodeRandomOpConf(const DecodeRandomOpConf& other);
  // enable nothrow for ::std::vector<DecodeRandomOpConf> resize 
  DecodeRandomOpConf(DecodeRandomOpConf&&) noexcept;
  DecodeRandomOpConf();
  explicit DecodeRandomOpConf(const ::oneflow::DecodeRandomOpConf& proto_decoderandomopconf);

  ~DecodeRandomOpConf() override;

  void InitFromProto(const PbMessage& proto_decoderandomopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DecodeRandomOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DecodeRandomOpConf& other) const;
  void Clear();
  void CopyFrom(const DecodeRandomOpConf& other);
  DecodeRandomOpConf& operator=(const DecodeRandomOpConf& other);

  // required or optional field tick
 public:
  void clear_tick();
  void set_tick(const ::std::string& value);
  ::std::string* mutable_tick();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  // required or optional field data_initializer
 public:
  void clear_data_initializer();
  ::oneflow::cfg::InitializerConf* mutable_data_initializer();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InitializerConf> shared_mutable_data_initializer();
  // required or optional field batch_size
 public:
  void clear_batch_size();
  void set_batch_size(const int64_t& value);
  int64_t* mutable_batch_size();

  ::std::shared_ptr<DecodeRandomOpConf> __SharedMutable__();
};


class ConstTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _TickOpConf_ {
   public:
    _TickOpConf_();
    explicit _TickOpConf_(const _TickOpConf_& other);
    explicit _TickOpConf_(_TickOpConf_&& other);
    _TickOpConf_(const ::oneflow::TickOpConf& proto_tickopconf);
    ~_TickOpConf_();

    void InitFromProto(const ::oneflow::TickOpConf& proto_tickopconf);

    void ToProto(::oneflow::TickOpConf* proto_tickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TickOpConf_& other);
  
      // repeated field tick
   public:
    ::std::size_t tick_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
    const ::std::string& tick(::std::size_t index) const;
    void clear_tick();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
    ::std::string* mutable_tick(::std::size_t index);
      void add_tick(const ::std::string& value);
    void set_tick(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> tick_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _TickOpConf_& other);

    bool operator==(const _TickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TickOpConf_& other) const;
  };

  ConstTickOpConf(const ::std::shared_ptr<_TickOpConf_>& data);
  ConstTickOpConf(const ConstTickOpConf&);
  ConstTickOpConf(ConstTickOpConf&&) noexcept;
  ConstTickOpConf();
  ConstTickOpConf(const ::oneflow::TickOpConf& proto_tickopconf);
  virtual ~ConstTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_tickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field tick
 public:
  ::std::size_t tick_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
  const ::std::string& tick(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_tick() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_TickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTickOpConf
  void BuildFromProto(const PbMessage& proto_tickopconf);
  
  ::std::shared_ptr<_TickOpConf_> data_;
};

class TickOpConf final : public ConstTickOpConf {
 public:
  TickOpConf(const ::std::shared_ptr<_TickOpConf_>& data);
  TickOpConf(const TickOpConf& other);
  // enable nothrow for ::std::vector<TickOpConf> resize 
  TickOpConf(TickOpConf&&) noexcept;
  TickOpConf();
  explicit TickOpConf(const ::oneflow::TickOpConf& proto_tickopconf);

  ~TickOpConf() override;

  void InitFromProto(const PbMessage& proto_tickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TickOpConf& other) const;
  void Clear();
  void CopyFrom(const TickOpConf& other);
  TickOpConf& operator=(const TickOpConf& other);

  // repeated field tick
 public:
  void clear_tick();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
  ::std::string* mutable_tick(::std::size_t index);
  void add_tick(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_tick();
  void set_tick(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<TickOpConf> __SharedMutable__();
};


class ConstDeviceTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _DeviceTickOpConf_ {
   public:
    _DeviceTickOpConf_();
    explicit _DeviceTickOpConf_(const _DeviceTickOpConf_& other);
    explicit _DeviceTickOpConf_(_DeviceTickOpConf_&& other);
    _DeviceTickOpConf_(const ::oneflow::DeviceTickOpConf& proto_devicetickopconf);
    ~_DeviceTickOpConf_();

    void InitFromProto(const ::oneflow::DeviceTickOpConf& proto_devicetickopconf);

    void ToProto(::oneflow::DeviceTickOpConf* proto_devicetickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DeviceTickOpConf_& other);
  
      // repeated field tick
   public:
    ::std::size_t tick_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
    const ::std::string& tick(::std::size_t index) const;
    void clear_tick();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
    ::std::string* mutable_tick(::std::size_t index);
      void add_tick(const ::std::string& value);
    void set_tick(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> tick_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field time_shape
     public:
    bool has_time_shape() const;
    const ::oneflow::cfg::ShapeProto& time_shape() const;
    void clear_time_shape();
    ::oneflow::cfg::ShapeProto* mutable_time_shape();
   protected:
    bool has_time_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> time_shape_;
           
   public:
    int compare(const _DeviceTickOpConf_& other);

    bool operator==(const _DeviceTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DeviceTickOpConf_& other) const;
  };

  ConstDeviceTickOpConf(const ::std::shared_ptr<_DeviceTickOpConf_>& data);
  ConstDeviceTickOpConf(const ConstDeviceTickOpConf&);
  ConstDeviceTickOpConf(ConstDeviceTickOpConf&&) noexcept;
  ConstDeviceTickOpConf();
  ConstDeviceTickOpConf(const ::oneflow::DeviceTickOpConf& proto_devicetickopconf);
  virtual ~ConstDeviceTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_devicetickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field tick
 public:
  ::std::size_t tick_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
  const ::std::string& tick(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_tick() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field time_shape
 public:
  bool has_time_shape() const;
  const ::oneflow::cfg::ShapeProto& time_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_time_shape() const;

 public:
  ::std::shared_ptr<ConstDeviceTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDeviceTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDeviceTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DeviceTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DeviceTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDeviceTickOpConf
  void BuildFromProto(const PbMessage& proto_devicetickopconf);
  
  ::std::shared_ptr<_DeviceTickOpConf_> data_;
};

class DeviceTickOpConf final : public ConstDeviceTickOpConf {
 public:
  DeviceTickOpConf(const ::std::shared_ptr<_DeviceTickOpConf_>& data);
  DeviceTickOpConf(const DeviceTickOpConf& other);
  // enable nothrow for ::std::vector<DeviceTickOpConf> resize 
  DeviceTickOpConf(DeviceTickOpConf&&) noexcept;
  DeviceTickOpConf();
  explicit DeviceTickOpConf(const ::oneflow::DeviceTickOpConf& proto_devicetickopconf);

  ~DeviceTickOpConf() override;

  void InitFromProto(const PbMessage& proto_devicetickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DeviceTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DeviceTickOpConf& other) const;
  void Clear();
  void CopyFrom(const DeviceTickOpConf& other);
  DeviceTickOpConf& operator=(const DeviceTickOpConf& other);

  // repeated field tick
 public:
  void clear_tick();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
  ::std::string* mutable_tick(::std::size_t index);
  void add_tick(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_tick();
  void set_tick(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field time_shape
 public:
  void clear_time_shape();
  ::oneflow::cfg::ShapeProto* mutable_time_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_time_shape();

  ::std::shared_ptr<DeviceTickOpConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_;

class ConstWaitAndSendIdsOpConf : public ::oneflow::cfg::Message {
 public:

  class _WaitAndSendIdsOpConf_ {
   public:
    _WaitAndSendIdsOpConf_();
    explicit _WaitAndSendIdsOpConf_(const _WaitAndSendIdsOpConf_& other);
    explicit _WaitAndSendIdsOpConf_(_WaitAndSendIdsOpConf_&& other);
    _WaitAndSendIdsOpConf_(const ::oneflow::WaitAndSendIdsOpConf& proto_waitandsendidsopconf);
    ~_WaitAndSendIdsOpConf_();

    void InitFromProto(const ::oneflow::WaitAndSendIdsOpConf& proto_waitandsendidsopconf);

    void ToProto(::oneflow::WaitAndSendIdsOpConf* proto_waitandsendidsopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _WaitAndSendIdsOpConf_& other);
  
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field wait_buffer_name
     public:
    bool has_wait_buffer_name() const;
    const ::std::string& wait_buffer_name() const;
    void clear_wait_buffer_name();
    void set_wait_buffer_name(const ::std::string& value);
    ::std::string* mutable_wait_buffer_name();
   protected:
    bool has_wait_buffer_name_ = false;
    ::std::string wait_buffer_name_;
      
      // repeated field id_list
   public:
    ::std::size_t id_list_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& id_list() const;
    const ::oneflow::cfg::Int64List& id_list(::std::size_t index) const;
    void clear_id_list();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_* mutable_id_list();
    ::oneflow::cfg::Int64List* mutable_id_list(::std::size_t index);
      ::oneflow::cfg::Int64List* add_id_list();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> id_list_;
    
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
           
   public:
    int compare(const _WaitAndSendIdsOpConf_& other);

    bool operator==(const _WaitAndSendIdsOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _WaitAndSendIdsOpConf_& other) const;
  };

  ConstWaitAndSendIdsOpConf(const ::std::shared_ptr<_WaitAndSendIdsOpConf_>& data);
  ConstWaitAndSendIdsOpConf(const ConstWaitAndSendIdsOpConf&);
  ConstWaitAndSendIdsOpConf(ConstWaitAndSendIdsOpConf&&) noexcept;
  ConstWaitAndSendIdsOpConf();
  ConstWaitAndSendIdsOpConf(const ::oneflow::WaitAndSendIdsOpConf& proto_waitandsendidsopconf);
  virtual ~ConstWaitAndSendIdsOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_waitandsendidsopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field wait_buffer_name
 public:
  bool has_wait_buffer_name() const;
  const ::std::string& wait_buffer_name() const;
  // used by pybind11 only
  // repeated field id_list
 public:
  ::std::size_t id_list_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& id_list() const;
  const ::oneflow::cfg::Int64List& id_list(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> shared_const_id_list() const;
  ::std::shared_ptr<::oneflow::cfg::ConstInt64List> shared_const_id_list(::std::size_t index) const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstWaitAndSendIdsOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstWaitAndSendIdsOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstWaitAndSendIdsOpConf& other) const;
 protected:
  const ::std::shared_ptr<_WaitAndSendIdsOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_WaitAndSendIdsOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstWaitAndSendIdsOpConf
  void BuildFromProto(const PbMessage& proto_waitandsendidsopconf);
  
  ::std::shared_ptr<_WaitAndSendIdsOpConf_> data_;
};

class WaitAndSendIdsOpConf final : public ConstWaitAndSendIdsOpConf {
 public:
  WaitAndSendIdsOpConf(const ::std::shared_ptr<_WaitAndSendIdsOpConf_>& data);
  WaitAndSendIdsOpConf(const WaitAndSendIdsOpConf& other);
  // enable nothrow for ::std::vector<WaitAndSendIdsOpConf> resize 
  WaitAndSendIdsOpConf(WaitAndSendIdsOpConf&&) noexcept;
  WaitAndSendIdsOpConf();
  explicit WaitAndSendIdsOpConf(const ::oneflow::WaitAndSendIdsOpConf& proto_waitandsendidsopconf);

  ~WaitAndSendIdsOpConf() override;

  void InitFromProto(const PbMessage& proto_waitandsendidsopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const WaitAndSendIdsOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const WaitAndSendIdsOpConf& other) const;
  void Clear();
  void CopyFrom(const WaitAndSendIdsOpConf& other);
  WaitAndSendIdsOpConf& operator=(const WaitAndSendIdsOpConf& other);

  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field wait_buffer_name
 public:
  void clear_wait_buffer_name();
  void set_wait_buffer_name(const ::std::string& value);
  ::std::string* mutable_wait_buffer_name();
  // repeated field id_list
 public:
  void clear_id_list();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_* mutable_id_list();
  ::oneflow::cfg::Int64List* mutable_id_list(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> shared_mutable_id_list();
  ::std::shared_ptr<::oneflow::cfg::Int64List> shared_mutable_id_list(::std::size_t index);
  ::oneflow::cfg::Int64List* add_id_list();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();

  ::std::shared_ptr<WaitAndSendIdsOpConf> __SharedMutable__();
};


class ConstCallbackNotifyOpConf : public ::oneflow::cfg::Message {
 public:

  class _CallbackNotifyOpConf_ {
   public:
    _CallbackNotifyOpConf_();
    explicit _CallbackNotifyOpConf_(const _CallbackNotifyOpConf_& other);
    explicit _CallbackNotifyOpConf_(_CallbackNotifyOpConf_&& other);
    _CallbackNotifyOpConf_(const ::oneflow::CallbackNotifyOpConf& proto_callbacknotifyopconf);
    ~_CallbackNotifyOpConf_();

    void InitFromProto(const ::oneflow::CallbackNotifyOpConf& proto_callbacknotifyopconf);

    void ToProto(::oneflow::CallbackNotifyOpConf* proto_callbacknotifyopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CallbackNotifyOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // repeated field callback_buffer_name
   public:
    ::std::size_t callback_buffer_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& callback_buffer_name() const;
    const ::std::string& callback_buffer_name(::std::size_t index) const;
    void clear_callback_buffer_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_callback_buffer_name();
    ::std::string* mutable_callback_buffer_name(::std::size_t index);
      void add_callback_buffer_name(const ::std::string& value);
    void set_callback_buffer_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> callback_buffer_name_;
         
   public:
    int compare(const _CallbackNotifyOpConf_& other);

    bool operator==(const _CallbackNotifyOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CallbackNotifyOpConf_& other) const;
  };

  ConstCallbackNotifyOpConf(const ::std::shared_ptr<_CallbackNotifyOpConf_>& data);
  ConstCallbackNotifyOpConf(const ConstCallbackNotifyOpConf&);
  ConstCallbackNotifyOpConf(ConstCallbackNotifyOpConf&&) noexcept;
  ConstCallbackNotifyOpConf();
  ConstCallbackNotifyOpConf(const ::oneflow::CallbackNotifyOpConf& proto_callbacknotifyopconf);
  virtual ~ConstCallbackNotifyOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_callbacknotifyopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // repeated field callback_buffer_name
 public:
  ::std::size_t callback_buffer_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& callback_buffer_name() const;
  const ::std::string& callback_buffer_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_callback_buffer_name() const;

 public:
  ::std::shared_ptr<ConstCallbackNotifyOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCallbackNotifyOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCallbackNotifyOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CallbackNotifyOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CallbackNotifyOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCallbackNotifyOpConf
  void BuildFromProto(const PbMessage& proto_callbacknotifyopconf);
  
  ::std::shared_ptr<_CallbackNotifyOpConf_> data_;
};

class CallbackNotifyOpConf final : public ConstCallbackNotifyOpConf {
 public:
  CallbackNotifyOpConf(const ::std::shared_ptr<_CallbackNotifyOpConf_>& data);
  CallbackNotifyOpConf(const CallbackNotifyOpConf& other);
  // enable nothrow for ::std::vector<CallbackNotifyOpConf> resize 
  CallbackNotifyOpConf(CallbackNotifyOpConf&&) noexcept;
  CallbackNotifyOpConf();
  explicit CallbackNotifyOpConf(const ::oneflow::CallbackNotifyOpConf& proto_callbacknotifyopconf);

  ~CallbackNotifyOpConf() override;

  void InitFromProto(const PbMessage& proto_callbacknotifyopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CallbackNotifyOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CallbackNotifyOpConf& other) const;
  void Clear();
  void CopyFrom(const CallbackNotifyOpConf& other);
  CallbackNotifyOpConf& operator=(const CallbackNotifyOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // repeated field callback_buffer_name
 public:
  void clear_callback_buffer_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_callback_buffer_name();
  ::std::string* mutable_callback_buffer_name(::std::size_t index);
  void add_callback_buffer_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_callback_buffer_name();
  void set_callback_buffer_name(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<CallbackNotifyOpConf> __SharedMutable__();
};


class ConstReentrantLockOpConf : public ::oneflow::cfg::Message {
 public:

  class _ReentrantLockOpConf_ {
   public:
    _ReentrantLockOpConf_();
    explicit _ReentrantLockOpConf_(const _ReentrantLockOpConf_& other);
    explicit _ReentrantLockOpConf_(_ReentrantLockOpConf_&& other);
    _ReentrantLockOpConf_(const ::oneflow::ReentrantLockOpConf& proto_reentrantlockopconf);
    ~_ReentrantLockOpConf_();

    void InitFromProto(const ::oneflow::ReentrantLockOpConf& proto_reentrantlockopconf);

    void ToProto(::oneflow::ReentrantLockOpConf* proto_reentrantlockopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ReentrantLockOpConf_& other);
  
      // optional field start
     public:
    bool has_start() const;
    const ::std::string& start() const;
    void clear_start();
    void set_start(const ::std::string& value);
    ::std::string* mutable_start();
   protected:
    bool has_start_ = false;
    ::std::string start_;
      
      // optional field end
     public:
    bool has_end() const;
    const ::std::string& end() const;
    void clear_end();
    void set_end(const ::std::string& value);
    ::std::string* mutable_end();
   protected:
    bool has_end_ = false;
    ::std::string end_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // repeated field lock_id2intersecting_lock_ids
   public:
    ::std::size_t lock_id2intersecting_lock_ids_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& lock_id2intersecting_lock_ids() const;
    const ::oneflow::cfg::Int64List& lock_id2intersecting_lock_ids(::std::size_t index) const;
    void clear_lock_id2intersecting_lock_ids();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_* mutable_lock_id2intersecting_lock_ids();
    ::oneflow::cfg::Int64List* mutable_lock_id2intersecting_lock_ids(::std::size_t index);
      ::oneflow::cfg::Int64List* add_lock_id2intersecting_lock_ids();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> lock_id2intersecting_lock_ids_;
         
   public:
    int compare(const _ReentrantLockOpConf_& other);

    bool operator==(const _ReentrantLockOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ReentrantLockOpConf_& other) const;
  };

  ConstReentrantLockOpConf(const ::std::shared_ptr<_ReentrantLockOpConf_>& data);
  ConstReentrantLockOpConf(const ConstReentrantLockOpConf&);
  ConstReentrantLockOpConf(ConstReentrantLockOpConf&&) noexcept;
  ConstReentrantLockOpConf();
  ConstReentrantLockOpConf(const ::oneflow::ReentrantLockOpConf& proto_reentrantlockopconf);
  virtual ~ConstReentrantLockOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_reentrantlockopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field start
 public:
  bool has_start() const;
  const ::std::string& start() const;
  // used by pybind11 only
  // required or optional field end
 public:
  bool has_end() const;
  const ::std::string& end() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // repeated field lock_id2intersecting_lock_ids
 public:
  ::std::size_t lock_id2intersecting_lock_ids_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& lock_id2intersecting_lock_ids() const;
  const ::oneflow::cfg::Int64List& lock_id2intersecting_lock_ids(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> shared_const_lock_id2intersecting_lock_ids() const;
  ::std::shared_ptr<::oneflow::cfg::ConstInt64List> shared_const_lock_id2intersecting_lock_ids(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstReentrantLockOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstReentrantLockOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstReentrantLockOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ReentrantLockOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ReentrantLockOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstReentrantLockOpConf
  void BuildFromProto(const PbMessage& proto_reentrantlockopconf);
  
  ::std::shared_ptr<_ReentrantLockOpConf_> data_;
};

class ReentrantLockOpConf final : public ConstReentrantLockOpConf {
 public:
  ReentrantLockOpConf(const ::std::shared_ptr<_ReentrantLockOpConf_>& data);
  ReentrantLockOpConf(const ReentrantLockOpConf& other);
  // enable nothrow for ::std::vector<ReentrantLockOpConf> resize 
  ReentrantLockOpConf(ReentrantLockOpConf&&) noexcept;
  ReentrantLockOpConf();
  explicit ReentrantLockOpConf(const ::oneflow::ReentrantLockOpConf& proto_reentrantlockopconf);

  ~ReentrantLockOpConf() override;

  void InitFromProto(const PbMessage& proto_reentrantlockopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ReentrantLockOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ReentrantLockOpConf& other) const;
  void Clear();
  void CopyFrom(const ReentrantLockOpConf& other);
  ReentrantLockOpConf& operator=(const ReentrantLockOpConf& other);

  // required or optional field start
 public:
  void clear_start();
  void set_start(const ::std::string& value);
  ::std::string* mutable_start();
  // required or optional field end
 public:
  void clear_end();
  void set_end(const ::std::string& value);
  ::std::string* mutable_end();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // repeated field lock_id2intersecting_lock_ids
 public:
  void clear_lock_id2intersecting_lock_ids();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_* mutable_lock_id2intersecting_lock_ids();
  ::oneflow::cfg::Int64List* mutable_lock_id2intersecting_lock_ids(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> shared_mutable_lock_id2intersecting_lock_ids();
  ::std::shared_ptr<::oneflow::cfg::Int64List> shared_mutable_lock_id2intersecting_lock_ids(::std::size_t index);
  ::oneflow::cfg::Int64List* add_lock_id2intersecting_lock_ids();

  ::std::shared_ptr<ReentrantLockOpConf> __SharedMutable__();
};


class ConstSrcSubsetTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _SrcSubsetTickOpConf_ {
   public:
    _SrcSubsetTickOpConf_();
    explicit _SrcSubsetTickOpConf_(const _SrcSubsetTickOpConf_& other);
    explicit _SrcSubsetTickOpConf_(_SrcSubsetTickOpConf_&& other);
    _SrcSubsetTickOpConf_(const ::oneflow::SrcSubsetTickOpConf& proto_srcsubsettickopconf);
    ~_SrcSubsetTickOpConf_();

    void InitFromProto(const ::oneflow::SrcSubsetTickOpConf& proto_srcsubsettickopconf);

    void ToProto(::oneflow::SrcSubsetTickOpConf* proto_srcsubsettickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SrcSubsetTickOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _SrcSubsetTickOpConf_& other);

    bool operator==(const _SrcSubsetTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SrcSubsetTickOpConf_& other) const;
  };

  ConstSrcSubsetTickOpConf(const ::std::shared_ptr<_SrcSubsetTickOpConf_>& data);
  ConstSrcSubsetTickOpConf(const ConstSrcSubsetTickOpConf&);
  ConstSrcSubsetTickOpConf(ConstSrcSubsetTickOpConf&&) noexcept;
  ConstSrcSubsetTickOpConf();
  ConstSrcSubsetTickOpConf(const ::oneflow::SrcSubsetTickOpConf& proto_srcsubsettickopconf);
  virtual ~ConstSrcSubsetTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_srcsubsettickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSrcSubsetTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSrcSubsetTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSrcSubsetTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SrcSubsetTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SrcSubsetTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSrcSubsetTickOpConf
  void BuildFromProto(const PbMessage& proto_srcsubsettickopconf);
  
  ::std::shared_ptr<_SrcSubsetTickOpConf_> data_;
};

class SrcSubsetTickOpConf final : public ConstSrcSubsetTickOpConf {
 public:
  SrcSubsetTickOpConf(const ::std::shared_ptr<_SrcSubsetTickOpConf_>& data);
  SrcSubsetTickOpConf(const SrcSubsetTickOpConf& other);
  // enable nothrow for ::std::vector<SrcSubsetTickOpConf> resize 
  SrcSubsetTickOpConf(SrcSubsetTickOpConf&&) noexcept;
  SrcSubsetTickOpConf();
  explicit SrcSubsetTickOpConf(const ::oneflow::SrcSubsetTickOpConf& proto_srcsubsettickopconf);

  ~SrcSubsetTickOpConf() override;

  void InitFromProto(const PbMessage& proto_srcsubsettickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SrcSubsetTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SrcSubsetTickOpConf& other) const;
  void Clear();
  void CopyFrom(const SrcSubsetTickOpConf& other);
  SrcSubsetTickOpConf& operator=(const SrcSubsetTickOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<SrcSubsetTickOpConf> __SharedMutable__();
};


class ConstDstSubsetTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _DstSubsetTickOpConf_ {
   public:
    _DstSubsetTickOpConf_();
    explicit _DstSubsetTickOpConf_(const _DstSubsetTickOpConf_& other);
    explicit _DstSubsetTickOpConf_(_DstSubsetTickOpConf_&& other);
    _DstSubsetTickOpConf_(const ::oneflow::DstSubsetTickOpConf& proto_dstsubsettickopconf);
    ~_DstSubsetTickOpConf_();

    void InitFromProto(const ::oneflow::DstSubsetTickOpConf& proto_dstsubsettickopconf);

    void ToProto(::oneflow::DstSubsetTickOpConf* proto_dstsubsettickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _DstSubsetTickOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _DstSubsetTickOpConf_& other);

    bool operator==(const _DstSubsetTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _DstSubsetTickOpConf_& other) const;
  };

  ConstDstSubsetTickOpConf(const ::std::shared_ptr<_DstSubsetTickOpConf_>& data);
  ConstDstSubsetTickOpConf(const ConstDstSubsetTickOpConf&);
  ConstDstSubsetTickOpConf(ConstDstSubsetTickOpConf&&) noexcept;
  ConstDstSubsetTickOpConf();
  ConstDstSubsetTickOpConf(const ::oneflow::DstSubsetTickOpConf& proto_dstsubsettickopconf);
  virtual ~ConstDstSubsetTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_dstsubsettickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstDstSubsetTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstDstSubsetTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstDstSubsetTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_DstSubsetTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_DstSubsetTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstDstSubsetTickOpConf
  void BuildFromProto(const PbMessage& proto_dstsubsettickopconf);
  
  ::std::shared_ptr<_DstSubsetTickOpConf_> data_;
};

class DstSubsetTickOpConf final : public ConstDstSubsetTickOpConf {
 public:
  DstSubsetTickOpConf(const ::std::shared_ptr<_DstSubsetTickOpConf_>& data);
  DstSubsetTickOpConf(const DstSubsetTickOpConf& other);
  // enable nothrow for ::std::vector<DstSubsetTickOpConf> resize 
  DstSubsetTickOpConf(DstSubsetTickOpConf&&) noexcept;
  DstSubsetTickOpConf();
  explicit DstSubsetTickOpConf(const ::oneflow::DstSubsetTickOpConf& proto_dstsubsettickopconf);

  ~DstSubsetTickOpConf() override;

  void InitFromProto(const PbMessage& proto_dstsubsettickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const DstSubsetTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const DstSubsetTickOpConf& other) const;
  void Clear();
  void CopyFrom(const DstSubsetTickOpConf& other);
  DstSubsetTickOpConf& operator=(const DstSubsetTickOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<DstSubsetTickOpConf> __SharedMutable__();
};


class ConstSourceTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _SourceTickOpConf_ {
   public:
    _SourceTickOpConf_();
    explicit _SourceTickOpConf_(const _SourceTickOpConf_& other);
    explicit _SourceTickOpConf_(_SourceTickOpConf_&& other);
    _SourceTickOpConf_(const ::oneflow::SourceTickOpConf& proto_sourcetickopconf);
    ~_SourceTickOpConf_();

    void InitFromProto(const ::oneflow::SourceTickOpConf& proto_sourcetickopconf);

    void ToProto(::oneflow::SourceTickOpConf* proto_sourcetickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SourceTickOpConf_& other);
  
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _SourceTickOpConf_& other);

    bool operator==(const _SourceTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SourceTickOpConf_& other) const;
  };

  ConstSourceTickOpConf(const ::std::shared_ptr<_SourceTickOpConf_>& data);
  ConstSourceTickOpConf(const ConstSourceTickOpConf&);
  ConstSourceTickOpConf(ConstSourceTickOpConf&&) noexcept;
  ConstSourceTickOpConf();
  ConstSourceTickOpConf(const ::oneflow::SourceTickOpConf& proto_sourcetickopconf);
  virtual ~ConstSourceTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sourcetickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSourceTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSourceTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSourceTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SourceTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SourceTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSourceTickOpConf
  void BuildFromProto(const PbMessage& proto_sourcetickopconf);
  
  ::std::shared_ptr<_SourceTickOpConf_> data_;
};

class SourceTickOpConf final : public ConstSourceTickOpConf {
 public:
  SourceTickOpConf(const ::std::shared_ptr<_SourceTickOpConf_>& data);
  SourceTickOpConf(const SourceTickOpConf& other);
  // enable nothrow for ::std::vector<SourceTickOpConf> resize 
  SourceTickOpConf(SourceTickOpConf&&) noexcept;
  SourceTickOpConf();
  explicit SourceTickOpConf(const ::oneflow::SourceTickOpConf& proto_sourcetickopconf);

  ~SourceTickOpConf() override;

  void InitFromProto(const PbMessage& proto_sourcetickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SourceTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SourceTickOpConf& other) const;
  void Clear();
  void CopyFrom(const SourceTickOpConf& other);
  SourceTickOpConf& operator=(const SourceTickOpConf& other);

  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<SourceTickOpConf> __SharedMutable__();
};


class ConstSinkTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _SinkTickOpConf_ {
   public:
    _SinkTickOpConf_();
    explicit _SinkTickOpConf_(const _SinkTickOpConf_& other);
    explicit _SinkTickOpConf_(_SinkTickOpConf_&& other);
    _SinkTickOpConf_(const ::oneflow::SinkTickOpConf& proto_sinktickopconf);
    ~_SinkTickOpConf_();

    void InitFromProto(const ::oneflow::SinkTickOpConf& proto_sinktickopconf);

    void ToProto(::oneflow::SinkTickOpConf* proto_sinktickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SinkTickOpConf_& other);
  
      // repeated field tick
   public:
    ::std::size_t tick_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
    const ::std::string& tick(::std::size_t index) const;
    void clear_tick();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
    ::std::string* mutable_tick(::std::size_t index);
      void add_tick(const ::std::string& value);
    void set_tick(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> tick_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _SinkTickOpConf_& other);

    bool operator==(const _SinkTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SinkTickOpConf_& other) const;
  };

  ConstSinkTickOpConf(const ::std::shared_ptr<_SinkTickOpConf_>& data);
  ConstSinkTickOpConf(const ConstSinkTickOpConf&);
  ConstSinkTickOpConf(ConstSinkTickOpConf&&) noexcept;
  ConstSinkTickOpConf();
  ConstSinkTickOpConf(const ::oneflow::SinkTickOpConf& proto_sinktickopconf);
  virtual ~ConstSinkTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sinktickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field tick
 public:
  ::std::size_t tick_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& tick() const;
  const ::std::string& tick(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_tick() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSinkTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSinkTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSinkTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SinkTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SinkTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSinkTickOpConf
  void BuildFromProto(const PbMessage& proto_sinktickopconf);
  
  ::std::shared_ptr<_SinkTickOpConf_> data_;
};

class SinkTickOpConf final : public ConstSinkTickOpConf {
 public:
  SinkTickOpConf(const ::std::shared_ptr<_SinkTickOpConf_>& data);
  SinkTickOpConf(const SinkTickOpConf& other);
  // enable nothrow for ::std::vector<SinkTickOpConf> resize 
  SinkTickOpConf(SinkTickOpConf&&) noexcept;
  SinkTickOpConf();
  explicit SinkTickOpConf(const ::oneflow::SinkTickOpConf& proto_sinktickopconf);

  ~SinkTickOpConf() override;

  void InitFromProto(const PbMessage& proto_sinktickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SinkTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SinkTickOpConf& other) const;
  void Clear();
  void CopyFrom(const SinkTickOpConf& other);
  SinkTickOpConf& operator=(const SinkTickOpConf& other);

  // repeated field tick
 public:
  void clear_tick();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_tick();
  ::std::string* mutable_tick(::std::size_t index);
  void add_tick(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_tick();
  void set_tick(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<SinkTickOpConf> __SharedMutable__();
};


class ConstTotalLossInstanceNumOpConf : public ::oneflow::cfg::Message {
 public:

  class _TotalLossInstanceNumOpConf_ {
   public:
    _TotalLossInstanceNumOpConf_();
    explicit _TotalLossInstanceNumOpConf_(const _TotalLossInstanceNumOpConf_& other);
    explicit _TotalLossInstanceNumOpConf_(_TotalLossInstanceNumOpConf_&& other);
    _TotalLossInstanceNumOpConf_(const ::oneflow::TotalLossInstanceNumOpConf& proto_totallossinstancenumopconf);
    ~_TotalLossInstanceNumOpConf_();

    void InitFromProto(const ::oneflow::TotalLossInstanceNumOpConf& proto_totallossinstancenumopconf);

    void ToProto(::oneflow::TotalLossInstanceNumOpConf* proto_totallossinstancenumopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _TotalLossInstanceNumOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _TotalLossInstanceNumOpConf_& other);

    bool operator==(const _TotalLossInstanceNumOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _TotalLossInstanceNumOpConf_& other) const;
  };

  ConstTotalLossInstanceNumOpConf(const ::std::shared_ptr<_TotalLossInstanceNumOpConf_>& data);
  ConstTotalLossInstanceNumOpConf(const ConstTotalLossInstanceNumOpConf&);
  ConstTotalLossInstanceNumOpConf(ConstTotalLossInstanceNumOpConf&&) noexcept;
  ConstTotalLossInstanceNumOpConf();
  ConstTotalLossInstanceNumOpConf(const ::oneflow::TotalLossInstanceNumOpConf& proto_totallossinstancenumopconf);
  virtual ~ConstTotalLossInstanceNumOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_totallossinstancenumopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstTotalLossInstanceNumOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstTotalLossInstanceNumOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstTotalLossInstanceNumOpConf& other) const;
 protected:
  const ::std::shared_ptr<_TotalLossInstanceNumOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_TotalLossInstanceNumOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstTotalLossInstanceNumOpConf
  void BuildFromProto(const PbMessage& proto_totallossinstancenumopconf);
  
  ::std::shared_ptr<_TotalLossInstanceNumOpConf_> data_;
};

class TotalLossInstanceNumOpConf final : public ConstTotalLossInstanceNumOpConf {
 public:
  TotalLossInstanceNumOpConf(const ::std::shared_ptr<_TotalLossInstanceNumOpConf_>& data);
  TotalLossInstanceNumOpConf(const TotalLossInstanceNumOpConf& other);
  // enable nothrow for ::std::vector<TotalLossInstanceNumOpConf> resize 
  TotalLossInstanceNumOpConf(TotalLossInstanceNumOpConf&&) noexcept;
  TotalLossInstanceNumOpConf();
  explicit TotalLossInstanceNumOpConf(const ::oneflow::TotalLossInstanceNumOpConf& proto_totallossinstancenumopconf);

  ~TotalLossInstanceNumOpConf() override;

  void InitFromProto(const PbMessage& proto_totallossinstancenumopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const TotalLossInstanceNumOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const TotalLossInstanceNumOpConf& other) const;
  void Clear();
  void CopyFrom(const TotalLossInstanceNumOpConf& other);
  TotalLossInstanceNumOpConf& operator=(const TotalLossInstanceNumOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<TotalLossInstanceNumOpConf> __SharedMutable__();
};


class ConstShapeElemCntAxisConf : public ::oneflow::cfg::Message {
 public:

  class _ShapeElemCntAxisConf_ {
   public:
    _ShapeElemCntAxisConf_();
    explicit _ShapeElemCntAxisConf_(const _ShapeElemCntAxisConf_& other);
    explicit _ShapeElemCntAxisConf_(_ShapeElemCntAxisConf_&& other);
    _ShapeElemCntAxisConf_(const ::oneflow::ShapeElemCntAxisConf& proto_shapeelemcntaxisconf);
    ~_ShapeElemCntAxisConf_();

    void InitFromProto(const ::oneflow::ShapeElemCntAxisConf& proto_shapeelemcntaxisconf);

    void ToProto(::oneflow::ShapeElemCntAxisConf* proto_shapeelemcntaxisconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeElemCntAxisConf_& other);
  
      // repeated field axis
   public:
    ::std::size_t axis_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& axis() const;
    const int32_t& axis(::std::size_t index) const;
    void clear_axis();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_* mutable_axis();
    int32_t* mutable_axis(::std::size_t index);
      void add_axis(const int32_t& value);
    void set_axis(::std::size_t index, const int32_t& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> axis_;
         
   public:
    int compare(const _ShapeElemCntAxisConf_& other);

    bool operator==(const _ShapeElemCntAxisConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeElemCntAxisConf_& other) const;
  };

  ConstShapeElemCntAxisConf(const ::std::shared_ptr<_ShapeElemCntAxisConf_>& data);
  ConstShapeElemCntAxisConf(const ConstShapeElemCntAxisConf&);
  ConstShapeElemCntAxisConf(ConstShapeElemCntAxisConf&&) noexcept;
  ConstShapeElemCntAxisConf();
  ConstShapeElemCntAxisConf(const ::oneflow::ShapeElemCntAxisConf& proto_shapeelemcntaxisconf);
  virtual ~ConstShapeElemCntAxisConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapeelemcntaxisconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field axis
 public:
  ::std::size_t axis_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& axis() const;
  const int32_t& axis(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> shared_const_axis() const;

 public:
  ::std::shared_ptr<ConstShapeElemCntAxisConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeElemCntAxisConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeElemCntAxisConf& other) const;
 protected:
  const ::std::shared_ptr<_ShapeElemCntAxisConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeElemCntAxisConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeElemCntAxisConf
  void BuildFromProto(const PbMessage& proto_shapeelemcntaxisconf);
  
  ::std::shared_ptr<_ShapeElemCntAxisConf_> data_;
};

class ShapeElemCntAxisConf final : public ConstShapeElemCntAxisConf {
 public:
  ShapeElemCntAxisConf(const ::std::shared_ptr<_ShapeElemCntAxisConf_>& data);
  ShapeElemCntAxisConf(const ShapeElemCntAxisConf& other);
  // enable nothrow for ::std::vector<ShapeElemCntAxisConf> resize 
  ShapeElemCntAxisConf(ShapeElemCntAxisConf&&) noexcept;
  ShapeElemCntAxisConf();
  explicit ShapeElemCntAxisConf(const ::oneflow::ShapeElemCntAxisConf& proto_shapeelemcntaxisconf);

  ~ShapeElemCntAxisConf() override;

  void InitFromProto(const PbMessage& proto_shapeelemcntaxisconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeElemCntAxisConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeElemCntAxisConf& other) const;
  void Clear();
  void CopyFrom(const ShapeElemCntAxisConf& other);
  ShapeElemCntAxisConf& operator=(const ShapeElemCntAxisConf& other);

  // repeated field axis
 public:
  void clear_axis();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_* mutable_axis();
  int32_t* mutable_axis(::std::size_t index);
  void add_axis(const int32_t& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> shared_mutable_axis();
  void set_axis(::std::size_t index, const int32_t& value);

  ::std::shared_ptr<ShapeElemCntAxisConf> __SharedMutable__();
};


class ConstShapeElemCntRangeAxisConf : public ::oneflow::cfg::Message {
 public:

  class _ShapeElemCntRangeAxisConf_ {
   public:
    _ShapeElemCntRangeAxisConf_();
    explicit _ShapeElemCntRangeAxisConf_(const _ShapeElemCntRangeAxisConf_& other);
    explicit _ShapeElemCntRangeAxisConf_(_ShapeElemCntRangeAxisConf_&& other);
    _ShapeElemCntRangeAxisConf_(const ::oneflow::ShapeElemCntRangeAxisConf& proto_shapeelemcntrangeaxisconf);
    ~_ShapeElemCntRangeAxisConf_();

    void InitFromProto(const ::oneflow::ShapeElemCntRangeAxisConf& proto_shapeelemcntrangeaxisconf);

    void ToProto(::oneflow::ShapeElemCntRangeAxisConf* proto_shapeelemcntrangeaxisconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeElemCntRangeAxisConf_& other);
  
      // optional field begin_axis
     public:
    bool has_begin_axis() const;
    const int32_t& begin_axis() const;
    void clear_begin_axis();
    void set_begin_axis(const int32_t& value);
    int32_t* mutable_begin_axis();
   protected:
    bool has_begin_axis_ = false;
    int32_t begin_axis_;
      
      // optional field end_axis
     public:
    bool has_end_axis() const;
    const int32_t& end_axis() const;
    void clear_end_axis();
    void set_end_axis(const int32_t& value);
    int32_t* mutable_end_axis();
   protected:
    bool has_end_axis_ = false;
    int32_t end_axis_;
           
   public:
    int compare(const _ShapeElemCntRangeAxisConf_& other);

    bool operator==(const _ShapeElemCntRangeAxisConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeElemCntRangeAxisConf_& other) const;
  };

  ConstShapeElemCntRangeAxisConf(const ::std::shared_ptr<_ShapeElemCntRangeAxisConf_>& data);
  ConstShapeElemCntRangeAxisConf(const ConstShapeElemCntRangeAxisConf&);
  ConstShapeElemCntRangeAxisConf(ConstShapeElemCntRangeAxisConf&&) noexcept;
  ConstShapeElemCntRangeAxisConf();
  ConstShapeElemCntRangeAxisConf(const ::oneflow::ShapeElemCntRangeAxisConf& proto_shapeelemcntrangeaxisconf);
  virtual ~ConstShapeElemCntRangeAxisConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapeelemcntrangeaxisconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field begin_axis
 public:
  bool has_begin_axis() const;
  const int32_t& begin_axis() const;
  // used by pybind11 only
  // required or optional field end_axis
 public:
  bool has_end_axis() const;
  const int32_t& end_axis() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstShapeElemCntRangeAxisConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeElemCntRangeAxisConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeElemCntRangeAxisConf& other) const;
 protected:
  const ::std::shared_ptr<_ShapeElemCntRangeAxisConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeElemCntRangeAxisConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeElemCntRangeAxisConf
  void BuildFromProto(const PbMessage& proto_shapeelemcntrangeaxisconf);
  
  ::std::shared_ptr<_ShapeElemCntRangeAxisConf_> data_;
};

class ShapeElemCntRangeAxisConf final : public ConstShapeElemCntRangeAxisConf {
 public:
  ShapeElemCntRangeAxisConf(const ::std::shared_ptr<_ShapeElemCntRangeAxisConf_>& data);
  ShapeElemCntRangeAxisConf(const ShapeElemCntRangeAxisConf& other);
  // enable nothrow for ::std::vector<ShapeElemCntRangeAxisConf> resize 
  ShapeElemCntRangeAxisConf(ShapeElemCntRangeAxisConf&&) noexcept;
  ShapeElemCntRangeAxisConf();
  explicit ShapeElemCntRangeAxisConf(const ::oneflow::ShapeElemCntRangeAxisConf& proto_shapeelemcntrangeaxisconf);

  ~ShapeElemCntRangeAxisConf() override;

  void InitFromProto(const PbMessage& proto_shapeelemcntrangeaxisconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeElemCntRangeAxisConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeElemCntRangeAxisConf& other) const;
  void Clear();
  void CopyFrom(const ShapeElemCntRangeAxisConf& other);
  ShapeElemCntRangeAxisConf& operator=(const ShapeElemCntRangeAxisConf& other);

  // required or optional field begin_axis
 public:
  void clear_begin_axis();
  void set_begin_axis(const int32_t& value);
  int32_t* mutable_begin_axis();
  // required or optional field end_axis
 public:
  void clear_end_axis();
  void set_end_axis(const int32_t& value);
  int32_t* mutable_end_axis();

  ::std::shared_ptr<ShapeElemCntRangeAxisConf> __SharedMutable__();
};


class ConstShapeElemCntOpConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum axis_conf
 enum AxisConfCase : unsigned int {
  AXIS_CONF_NOT_SET = 0,
    kExcludeAxisConf = 4,
    kIncludeAxisConf = 5,
    kRangeAxisConf = 6,
   };

  class _ShapeElemCntOpConf_ {
   public:
    _ShapeElemCntOpConf_();
    explicit _ShapeElemCntOpConf_(const _ShapeElemCntOpConf_& other);
    explicit _ShapeElemCntOpConf_(_ShapeElemCntOpConf_&& other);
    _ShapeElemCntOpConf_(const ::oneflow::ShapeElemCntOpConf& proto_shapeelemcntopconf);
    ~_ShapeElemCntOpConf_();

    void InitFromProto(const ::oneflow::ShapeElemCntOpConf& proto_shapeelemcntopconf);

    void ToProto(::oneflow::ShapeElemCntOpConf* proto_shapeelemcntopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ShapeElemCntOpConf_& other);
  
      // optional field x
     public:
    bool has_x() const;
    const ::std::string& x() const;
    void clear_x();
    void set_x(const ::std::string& value);
    ::std::string* mutable_x();
   protected:
    bool has_x_ = false;
    ::std::string x_;
      
      // optional field y
     public:
    bool has_y() const;
    const ::std::string& y() const;
    void clear_y();
    void set_y(const ::std::string& value);
    ::std::string* mutable_y();
   protected:
    bool has_y_ = false;
    ::std::string y_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
     // oneof field axis_conf: exclude_axis_conf
   public:
    bool has_exclude_axis_conf() const;
    void clear_exclude_axis_conf();
    const ::oneflow::cfg::ShapeElemCntAxisConf& exclude_axis_conf() const;
      ::oneflow::cfg::ShapeElemCntAxisConf* mutable_exclude_axis_conf();
      
     // oneof field axis_conf: include_axis_conf
   public:
    bool has_include_axis_conf() const;
    void clear_include_axis_conf();
    const ::oneflow::cfg::ShapeElemCntAxisConf& include_axis_conf() const;
      ::oneflow::cfg::ShapeElemCntAxisConf* mutable_include_axis_conf();
      
     // oneof field axis_conf: range_axis_conf
   public:
    bool has_range_axis_conf() const;
    void clear_range_axis_conf();
    const ::oneflow::cfg::ShapeElemCntRangeAxisConf& range_axis_conf() const;
      ::oneflow::cfg::ShapeElemCntRangeAxisConf* mutable_range_axis_conf();
           
   public:
    // oneof axis_conf
    AxisConfCase axis_conf_case() const;
    bool has_axis_conf() const;
   protected:
    void clear_axis_conf();
    void axis_conf_copy_from(const _ShapeElemCntOpConf_& other);
    union AxisConfUnion {
      // 64-bit aligned
      uint64_t __axis_conf_for_padding_64bit__;
          char exclude_axis_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ShapeElemCntAxisConf>)];
            char include_axis_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ShapeElemCntAxisConf>)];
            char range_axis_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ShapeElemCntRangeAxisConf>)];
        } axis_conf_;
    AxisConfCase axis_conf_case_ = AXIS_CONF_NOT_SET;
     
   public:
    int compare(const _ShapeElemCntOpConf_& other);

    bool operator==(const _ShapeElemCntOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ShapeElemCntOpConf_& other) const;
  };

  ConstShapeElemCntOpConf(const ::std::shared_ptr<_ShapeElemCntOpConf_>& data);
  ConstShapeElemCntOpConf(const ConstShapeElemCntOpConf&);
  ConstShapeElemCntOpConf(ConstShapeElemCntOpConf&&) noexcept;
  ConstShapeElemCntOpConf();
  ConstShapeElemCntOpConf(const ::oneflow::ShapeElemCntOpConf& proto_shapeelemcntopconf);
  virtual ~ConstShapeElemCntOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_shapeelemcntopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field x
 public:
  bool has_x() const;
  const ::std::string& x() const;
  // used by pybind11 only
  // required or optional field y
 public:
  bool has_y() const;
  const ::std::string& y() const;
  // used by pybind11 only
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
 // oneof field axis_conf: exclude_axis_conf
 public:
  bool has_exclude_axis_conf() const;
  const ::oneflow::cfg::ShapeElemCntAxisConf& exclude_axis_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeElemCntAxisConf> shared_const_exclude_axis_conf() const;
 // oneof field axis_conf: include_axis_conf
 public:
  bool has_include_axis_conf() const;
  const ::oneflow::cfg::ShapeElemCntAxisConf& include_axis_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeElemCntAxisConf> shared_const_include_axis_conf() const;
 // oneof field axis_conf: range_axis_conf
 public:
  bool has_range_axis_conf() const;
  const ::oneflow::cfg::ShapeElemCntRangeAxisConf& range_axis_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeElemCntRangeAxisConf> shared_const_range_axis_conf() const;
 public:
  AxisConfCase axis_conf_case() const;
 protected:
  bool has_axis_conf() const;

 public:
  ::std::shared_ptr<ConstShapeElemCntOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstShapeElemCntOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstShapeElemCntOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ShapeElemCntOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ShapeElemCntOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstShapeElemCntOpConf
  void BuildFromProto(const PbMessage& proto_shapeelemcntopconf);
  
  ::std::shared_ptr<_ShapeElemCntOpConf_> data_;
};

class ShapeElemCntOpConf final : public ConstShapeElemCntOpConf {
 public:
  ShapeElemCntOpConf(const ::std::shared_ptr<_ShapeElemCntOpConf_>& data);
  ShapeElemCntOpConf(const ShapeElemCntOpConf& other);
  // enable nothrow for ::std::vector<ShapeElemCntOpConf> resize 
  ShapeElemCntOpConf(ShapeElemCntOpConf&&) noexcept;
  ShapeElemCntOpConf();
  explicit ShapeElemCntOpConf(const ::oneflow::ShapeElemCntOpConf& proto_shapeelemcntopconf);

  ~ShapeElemCntOpConf() override;

  void InitFromProto(const PbMessage& proto_shapeelemcntopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ShapeElemCntOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ShapeElemCntOpConf& other) const;
  void Clear();
  void CopyFrom(const ShapeElemCntOpConf& other);
  ShapeElemCntOpConf& operator=(const ShapeElemCntOpConf& other);

  // required or optional field x
 public:
  void clear_x();
  void set_x(const ::std::string& value);
  ::std::string* mutable_x();
  // required or optional field y
 public:
  void clear_y();
  void set_y(const ::std::string& value);
  ::std::string* mutable_y();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  void clear_exclude_axis_conf();
  ::oneflow::cfg::ShapeElemCntAxisConf* mutable_exclude_axis_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeElemCntAxisConf> shared_mutable_exclude_axis_conf();
  void clear_include_axis_conf();
  ::oneflow::cfg::ShapeElemCntAxisConf* mutable_include_axis_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeElemCntAxisConf> shared_mutable_include_axis_conf();
  void clear_range_axis_conf();
  ::oneflow::cfg::ShapeElemCntRangeAxisConf* mutable_range_axis_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeElemCntRangeAxisConf> shared_mutable_range_axis_conf();

  ::std::shared_ptr<ShapeElemCntOpConf> __SharedMutable__();
};


class ConstAccTickOpConf : public ::oneflow::cfg::Message {
 public:

  class _AccTickOpConf_ {
   public:
    _AccTickOpConf_();
    explicit _AccTickOpConf_(const _AccTickOpConf_& other);
    explicit _AccTickOpConf_(_AccTickOpConf_&& other);
    _AccTickOpConf_(const ::oneflow::AccTickOpConf& proto_acctickopconf);
    ~_AccTickOpConf_();

    void InitFromProto(const ::oneflow::AccTickOpConf& proto_acctickopconf);

    void ToProto(::oneflow::AccTickOpConf* proto_acctickopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AccTickOpConf_& other);
  
      // optional field one
     public:
    bool has_one() const;
    const ::std::string& one() const;
    void clear_one();
    void set_one(const ::std::string& value);
    ::std::string* mutable_one();
   protected:
    bool has_one_ = false;
    ::std::string one_;
      
      // optional field acc
     public:
    bool has_acc() const;
    const ::std::string& acc() const;
    void clear_acc();
    void set_acc(const ::std::string& value);
    ::std::string* mutable_acc();
   protected:
    bool has_acc_ = false;
    ::std::string acc_;
      
      // optional field max_acc_num
     public:
    bool has_max_acc_num() const;
    const int32_t& max_acc_num() const;
    void clear_max_acc_num();
    void set_max_acc_num(const int32_t& value);
    int32_t* mutable_max_acc_num();
   protected:
    bool has_max_acc_num_ = false;
    int32_t max_acc_num_;
           
   public:
    int compare(const _AccTickOpConf_& other);

    bool operator==(const _AccTickOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AccTickOpConf_& other) const;
  };

  ConstAccTickOpConf(const ::std::shared_ptr<_AccTickOpConf_>& data);
  ConstAccTickOpConf(const ConstAccTickOpConf&);
  ConstAccTickOpConf(ConstAccTickOpConf&&) noexcept;
  ConstAccTickOpConf();
  ConstAccTickOpConf(const ::oneflow::AccTickOpConf& proto_acctickopconf);
  virtual ~ConstAccTickOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_acctickopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field one
 public:
  bool has_one() const;
  const ::std::string& one() const;
  // used by pybind11 only
  // required or optional field acc
 public:
  bool has_acc() const;
  const ::std::string& acc() const;
  // used by pybind11 only
  // required or optional field max_acc_num
 public:
  bool has_max_acc_num() const;
  const int32_t& max_acc_num() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstAccTickOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAccTickOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAccTickOpConf& other) const;
 protected:
  const ::std::shared_ptr<_AccTickOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AccTickOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAccTickOpConf
  void BuildFromProto(const PbMessage& proto_acctickopconf);
  
  ::std::shared_ptr<_AccTickOpConf_> data_;
};

class AccTickOpConf final : public ConstAccTickOpConf {
 public:
  AccTickOpConf(const ::std::shared_ptr<_AccTickOpConf_>& data);
  AccTickOpConf(const AccTickOpConf& other);
  // enable nothrow for ::std::vector<AccTickOpConf> resize 
  AccTickOpConf(AccTickOpConf&&) noexcept;
  AccTickOpConf();
  explicit AccTickOpConf(const ::oneflow::AccTickOpConf& proto_acctickopconf);

  ~AccTickOpConf() override;

  void InitFromProto(const PbMessage& proto_acctickopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AccTickOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AccTickOpConf& other) const;
  void Clear();
  void CopyFrom(const AccTickOpConf& other);
  AccTickOpConf& operator=(const AccTickOpConf& other);

  // required or optional field one
 public:
  void clear_one();
  void set_one(const ::std::string& value);
  ::std::string* mutable_one();
  // required or optional field acc
 public:
  void clear_acc();
  void set_acc(const ::std::string& value);
  ::std::string* mutable_acc();
  // required or optional field max_acc_num
 public:
  void clear_max_acc_num();
  void set_max_acc_num(const int32_t& value);
  int32_t* mutable_max_acc_num();

  ::std::shared_ptr<AccTickOpConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_;

class ConstModelInitOpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelInitOpConf_ {
   public:
    _ModelInitOpConf_();
    explicit _ModelInitOpConf_(const _ModelInitOpConf_& other);
    explicit _ModelInitOpConf_(_ModelInitOpConf_&& other);
    _ModelInitOpConf_(const ::oneflow::ModelInitOpConf& proto_modelinitopconf);
    ~_ModelInitOpConf_();

    void InitFromProto(const ::oneflow::ModelInitOpConf& proto_modelinitopconf);

    void ToProto(::oneflow::ModelInitOpConf* proto_modelinitopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelInitOpConf_& other);
  
      // optional field tick
     public:
    bool has_tick() const;
    const ::std::string& tick() const;
    void clear_tick();
    void set_tick(const ::std::string& value);
    ::std::string* mutable_tick();
   protected:
    bool has_tick_ = false;
    ::std::string tick_;
      
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
    
      // repeated field variable_op_name
   public:
    ::std::size_t variable_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
    const ::std::string& variable_op_name(::std::size_t index) const;
    void clear_variable_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
    ::std::string* mutable_variable_op_name(::std::size_t index);
      void add_variable_op_name(const ::std::string& value);
    void set_variable_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> variable_op_name_;
    
      // repeated field original_variable_conf
   public:
    ::std::size_t original_variable_conf_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
    const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
    void clear_original_variable_conf();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
    ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
      ::oneflow::cfg::VariableOpConf* add_original_variable_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> original_variable_conf_;
         
   public:
    int compare(const _ModelInitOpConf_& other);

    bool operator==(const _ModelInitOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelInitOpConf_& other) const;
  };

  ConstModelInitOpConf(const ::std::shared_ptr<_ModelInitOpConf_>& data);
  ConstModelInitOpConf(const ConstModelInitOpConf&);
  ConstModelInitOpConf(ConstModelInitOpConf&&) noexcept;
  ConstModelInitOpConf();
  ConstModelInitOpConf(const ::oneflow::ModelInitOpConf& proto_modelinitopconf);
  virtual ~ConstModelInitOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelinitopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field tick
 public:
  bool has_tick() const;
  const ::std::string& tick() const;
  // used by pybind11 only
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;
  // repeated field variable_op_name
 public:
  ::std::size_t variable_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
  const ::std::string& variable_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_name() const;
  // repeated field original_variable_conf
 public:
  ::std::size_t original_variable_conf_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_const_original_variable_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_original_variable_conf(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstModelInitOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelInitOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelInitOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelInitOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelInitOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelInitOpConf
  void BuildFromProto(const PbMessage& proto_modelinitopconf);
  
  ::std::shared_ptr<_ModelInitOpConf_> data_;
};

class ModelInitOpConf final : public ConstModelInitOpConf {
 public:
  ModelInitOpConf(const ::std::shared_ptr<_ModelInitOpConf_>& data);
  ModelInitOpConf(const ModelInitOpConf& other);
  // enable nothrow for ::std::vector<ModelInitOpConf> resize 
  ModelInitOpConf(ModelInitOpConf&&) noexcept;
  ModelInitOpConf();
  explicit ModelInitOpConf(const ::oneflow::ModelInitOpConf& proto_modelinitopconf);

  ~ModelInitOpConf() override;

  void InitFromProto(const PbMessage& proto_modelinitopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelInitOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelInitOpConf& other) const;
  void Clear();
  void CopyFrom(const ModelInitOpConf& other);
  ModelInitOpConf& operator=(const ModelInitOpConf& other);

  // required or optional field tick
 public:
  void clear_tick();
  void set_tick(const ::std::string& value);
  ::std::string* mutable_tick();
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);
  // repeated field variable_op_name
 public:
  void clear_variable_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
  ::std::string* mutable_variable_op_name(::std::size_t index);
  void add_variable_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_name();
  void set_variable_op_name(::std::size_t index, const ::std::string& value);
  // repeated field original_variable_conf
 public:
  void clear_original_variable_conf();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_mutable_original_variable_conf();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_original_variable_conf(::std::size_t index);
  ::oneflow::cfg::VariableOpConf* add_original_variable_conf();

  ::std::shared_ptr<ModelInitOpConf> __SharedMutable__();
};


class ConstModelLoadOpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelLoadOpConf_ {
   public:
    _ModelLoadOpConf_();
    explicit _ModelLoadOpConf_(const _ModelLoadOpConf_& other);
    explicit _ModelLoadOpConf_(_ModelLoadOpConf_&& other);
    _ModelLoadOpConf_(const ::oneflow::ModelLoadOpConf& proto_modelloadopconf);
    ~_ModelLoadOpConf_();

    void InitFromProto(const ::oneflow::ModelLoadOpConf& proto_modelloadopconf);

    void ToProto(::oneflow::ModelLoadOpConf* proto_modelloadopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelLoadOpConf_& other);
  
      // optional field path
     public:
    bool has_path() const;
    const ::std::string& path() const;
    void clear_path();
    void set_path(const ::std::string& value);
    ::std::string* mutable_path();
   protected:
    bool has_path_ = false;
    ::std::string path_;
      
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
    
      // repeated field variable_op_name
   public:
    ::std::size_t variable_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
    const ::std::string& variable_op_name(::std::size_t index) const;
    void clear_variable_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
    ::std::string* mutable_variable_op_name(::std::size_t index);
      void add_variable_op_name(const ::std::string& value);
    void set_variable_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> variable_op_name_;
    
      // repeated field original_variable_conf
   public:
    ::std::size_t original_variable_conf_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
    const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
    void clear_original_variable_conf();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
    ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
      ::oneflow::cfg::VariableOpConf* add_original_variable_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> original_variable_conf_;
         
   public:
    int compare(const _ModelLoadOpConf_& other);

    bool operator==(const _ModelLoadOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelLoadOpConf_& other) const;
  };

  ConstModelLoadOpConf(const ::std::shared_ptr<_ModelLoadOpConf_>& data);
  ConstModelLoadOpConf(const ConstModelLoadOpConf&);
  ConstModelLoadOpConf(ConstModelLoadOpConf&&) noexcept;
  ConstModelLoadOpConf();
  ConstModelLoadOpConf(const ::oneflow::ModelLoadOpConf& proto_modelloadopconf);
  virtual ~ConstModelLoadOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelloadopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field path
 public:
  bool has_path() const;
  const ::std::string& path() const;
  // used by pybind11 only
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;
  // repeated field variable_op_name
 public:
  ::std::size_t variable_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
  const ::std::string& variable_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_name() const;
  // repeated field original_variable_conf
 public:
  ::std::size_t original_variable_conf_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_const_original_variable_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_original_variable_conf(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstModelLoadOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelLoadOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelLoadOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelLoadOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelLoadOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelLoadOpConf
  void BuildFromProto(const PbMessage& proto_modelloadopconf);
  
  ::std::shared_ptr<_ModelLoadOpConf_> data_;
};

class ModelLoadOpConf final : public ConstModelLoadOpConf {
 public:
  ModelLoadOpConf(const ::std::shared_ptr<_ModelLoadOpConf_>& data);
  ModelLoadOpConf(const ModelLoadOpConf& other);
  // enable nothrow for ::std::vector<ModelLoadOpConf> resize 
  ModelLoadOpConf(ModelLoadOpConf&&) noexcept;
  ModelLoadOpConf();
  explicit ModelLoadOpConf(const ::oneflow::ModelLoadOpConf& proto_modelloadopconf);

  ~ModelLoadOpConf() override;

  void InitFromProto(const PbMessage& proto_modelloadopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelLoadOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelLoadOpConf& other) const;
  void Clear();
  void CopyFrom(const ModelLoadOpConf& other);
  ModelLoadOpConf& operator=(const ModelLoadOpConf& other);

  // required or optional field path
 public:
  void clear_path();
  void set_path(const ::std::string& value);
  ::std::string* mutable_path();
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);
  // repeated field variable_op_name
 public:
  void clear_variable_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
  ::std::string* mutable_variable_op_name(::std::size_t index);
  void add_variable_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_name();
  void set_variable_op_name(::std::size_t index, const ::std::string& value);
  // repeated field original_variable_conf
 public:
  void clear_original_variable_conf();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_mutable_original_variable_conf();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_original_variable_conf(::std::size_t index);
  ::oneflow::cfg::VariableOpConf* add_original_variable_conf();

  ::std::shared_ptr<ModelLoadOpConf> __SharedMutable__();
};


class ConstIdentityOpConf : public ::oneflow::cfg::Message {
 public:

  class _IdentityOpConf_ {
   public:
    _IdentityOpConf_();
    explicit _IdentityOpConf_(const _IdentityOpConf_& other);
    explicit _IdentityOpConf_(_IdentityOpConf_&& other);
    _IdentityOpConf_(const ::oneflow::IdentityOpConf& proto_identityopconf);
    ~_IdentityOpConf_();

    void InitFromProto(const ::oneflow::IdentityOpConf& proto_identityopconf);

    void ToProto(::oneflow::IdentityOpConf* proto_identityopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _IdentityOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _IdentityOpConf_& other);

    bool operator==(const _IdentityOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _IdentityOpConf_& other) const;
  };

  ConstIdentityOpConf(const ::std::shared_ptr<_IdentityOpConf_>& data);
  ConstIdentityOpConf(const ConstIdentityOpConf&);
  ConstIdentityOpConf(ConstIdentityOpConf&&) noexcept;
  ConstIdentityOpConf();
  ConstIdentityOpConf(const ::oneflow::IdentityOpConf& proto_identityopconf);
  virtual ~ConstIdentityOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_identityopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstIdentityOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstIdentityOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstIdentityOpConf& other) const;
 protected:
  const ::std::shared_ptr<_IdentityOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_IdentityOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstIdentityOpConf
  void BuildFromProto(const PbMessage& proto_identityopconf);
  
  ::std::shared_ptr<_IdentityOpConf_> data_;
};

class IdentityOpConf final : public ConstIdentityOpConf {
 public:
  IdentityOpConf(const ::std::shared_ptr<_IdentityOpConf_>& data);
  IdentityOpConf(const IdentityOpConf& other);
  // enable nothrow for ::std::vector<IdentityOpConf> resize 
  IdentityOpConf(IdentityOpConf&&) noexcept;
  IdentityOpConf();
  explicit IdentityOpConf(const ::oneflow::IdentityOpConf& proto_identityopconf);

  ~IdentityOpConf() override;

  void InitFromProto(const PbMessage& proto_identityopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const IdentityOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const IdentityOpConf& other) const;
  void Clear();
  void CopyFrom(const IdentityOpConf& other);
  IdentityOpConf& operator=(const IdentityOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<IdentityOpConf> __SharedMutable__();
};


class ConstCopyOpConf : public ::oneflow::cfg::Message {
 public:

  class _CopyOpConf_ {
   public:
    _CopyOpConf_();
    explicit _CopyOpConf_(const _CopyOpConf_& other);
    explicit _CopyOpConf_(_CopyOpConf_&& other);
    _CopyOpConf_(const ::oneflow::CopyOpConf& proto_copyopconf);
    ~_CopyOpConf_();

    void InitFromProto(const ::oneflow::CopyOpConf& proto_copyopconf);

    void ToProto(::oneflow::CopyOpConf* proto_copyopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CopyOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
           
   public:
    int compare(const _CopyOpConf_& other);

    bool operator==(const _CopyOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CopyOpConf_& other) const;
  };

  ConstCopyOpConf(const ::std::shared_ptr<_CopyOpConf_>& data);
  ConstCopyOpConf(const ConstCopyOpConf&);
  ConstCopyOpConf(ConstCopyOpConf&&) noexcept;
  ConstCopyOpConf();
  ConstCopyOpConf(const ::oneflow::CopyOpConf& proto_copyopconf);
  virtual ~ConstCopyOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_copyopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstCopyOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCopyOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCopyOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CopyOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CopyOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCopyOpConf
  void BuildFromProto(const PbMessage& proto_copyopconf);
  
  ::std::shared_ptr<_CopyOpConf_> data_;
};

class CopyOpConf final : public ConstCopyOpConf {
 public:
  CopyOpConf(const ::std::shared_ptr<_CopyOpConf_>& data);
  CopyOpConf(const CopyOpConf& other);
  // enable nothrow for ::std::vector<CopyOpConf> resize 
  CopyOpConf(CopyOpConf&&) noexcept;
  CopyOpConf();
  explicit CopyOpConf(const ::oneflow::CopyOpConf& proto_copyopconf);

  ~CopyOpConf() override;

  void InitFromProto(const PbMessage& proto_copyopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CopyOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CopyOpConf& other) const;
  void Clear();
  void CopyFrom(const CopyOpConf& other);
  CopyOpConf& operator=(const CopyOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();

  ::std::shared_ptr<CopyOpConf> __SharedMutable__();
};


class ConstCastToMirroredOpConf : public ::oneflow::cfg::Message {
 public:

  class _CastToMirroredOpConf_ {
   public:
    _CastToMirroredOpConf_();
    explicit _CastToMirroredOpConf_(const _CastToMirroredOpConf_& other);
    explicit _CastToMirroredOpConf_(_CastToMirroredOpConf_&& other);
    _CastToMirroredOpConf_(const ::oneflow::CastToMirroredOpConf& proto_casttomirroredopconf);
    ~_CastToMirroredOpConf_();

    void InitFromProto(const ::oneflow::CastToMirroredOpConf& proto_casttomirroredopconf);

    void ToProto(::oneflow::CastToMirroredOpConf* proto_casttomirroredopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CastToMirroredOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field sbp_parallel
     public:
    bool has_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& sbp_parallel() const;
    void clear_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_sbp_parallel();
   protected:
    bool has_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> sbp_parallel_;
           
   public:
    int compare(const _CastToMirroredOpConf_& other);

    bool operator==(const _CastToMirroredOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CastToMirroredOpConf_& other) const;
  };

  ConstCastToMirroredOpConf(const ::std::shared_ptr<_CastToMirroredOpConf_>& data);
  ConstCastToMirroredOpConf(const ConstCastToMirroredOpConf&);
  ConstCastToMirroredOpConf(ConstCastToMirroredOpConf&&) noexcept;
  ConstCastToMirroredOpConf();
  ConstCastToMirroredOpConf(const ::oneflow::CastToMirroredOpConf& proto_casttomirroredopconf);
  virtual ~ConstCastToMirroredOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_casttomirroredopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field sbp_parallel
 public:
  bool has_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_sbp_parallel() const;

 public:
  ::std::shared_ptr<ConstCastToMirroredOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCastToMirroredOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCastToMirroredOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CastToMirroredOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CastToMirroredOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCastToMirroredOpConf
  void BuildFromProto(const PbMessage& proto_casttomirroredopconf);
  
  ::std::shared_ptr<_CastToMirroredOpConf_> data_;
};

class CastToMirroredOpConf final : public ConstCastToMirroredOpConf {
 public:
  CastToMirroredOpConf(const ::std::shared_ptr<_CastToMirroredOpConf_>& data);
  CastToMirroredOpConf(const CastToMirroredOpConf& other);
  // enable nothrow for ::std::vector<CastToMirroredOpConf> resize 
  CastToMirroredOpConf(CastToMirroredOpConf&&) noexcept;
  CastToMirroredOpConf();
  explicit CastToMirroredOpConf(const ::oneflow::CastToMirroredOpConf& proto_casttomirroredopconf);

  ~CastToMirroredOpConf() override;

  void InitFromProto(const PbMessage& proto_casttomirroredopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CastToMirroredOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CastToMirroredOpConf& other) const;
  void Clear();
  void CopyFrom(const CastToMirroredOpConf& other);
  CastToMirroredOpConf& operator=(const CastToMirroredOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field sbp_parallel
 public:
  void clear_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_sbp_parallel();

  ::std::shared_ptr<CastToMirroredOpConf> __SharedMutable__();
};


class ConstCastFromMirroredOpConf : public ::oneflow::cfg::Message {
 public:

  class _CastFromMirroredOpConf_ {
   public:
    _CastFromMirroredOpConf_();
    explicit _CastFromMirroredOpConf_(const _CastFromMirroredOpConf_& other);
    explicit _CastFromMirroredOpConf_(_CastFromMirroredOpConf_&& other);
    _CastFromMirroredOpConf_(const ::oneflow::CastFromMirroredOpConf& proto_castfrommirroredopconf);
    ~_CastFromMirroredOpConf_();

    void InitFromProto(const ::oneflow::CastFromMirroredOpConf& proto_castfrommirroredopconf);

    void ToProto(::oneflow::CastFromMirroredOpConf* proto_castfrommirroredopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CastFromMirroredOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field sbp_parallel
     public:
    bool has_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& sbp_parallel() const;
    void clear_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_sbp_parallel();
   protected:
    bool has_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> sbp_parallel_;
           
   public:
    int compare(const _CastFromMirroredOpConf_& other);

    bool operator==(const _CastFromMirroredOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CastFromMirroredOpConf_& other) const;
  };

  ConstCastFromMirroredOpConf(const ::std::shared_ptr<_CastFromMirroredOpConf_>& data);
  ConstCastFromMirroredOpConf(const ConstCastFromMirroredOpConf&);
  ConstCastFromMirroredOpConf(ConstCastFromMirroredOpConf&&) noexcept;
  ConstCastFromMirroredOpConf();
  ConstCastFromMirroredOpConf(const ::oneflow::CastFromMirroredOpConf& proto_castfrommirroredopconf);
  virtual ~ConstCastFromMirroredOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_castfrommirroredopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field sbp_parallel
 public:
  bool has_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_sbp_parallel() const;

 public:
  ::std::shared_ptr<ConstCastFromMirroredOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCastFromMirroredOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCastFromMirroredOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CastFromMirroredOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CastFromMirroredOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCastFromMirroredOpConf
  void BuildFromProto(const PbMessage& proto_castfrommirroredopconf);
  
  ::std::shared_ptr<_CastFromMirroredOpConf_> data_;
};

class CastFromMirroredOpConf final : public ConstCastFromMirroredOpConf {
 public:
  CastFromMirroredOpConf(const ::std::shared_ptr<_CastFromMirroredOpConf_>& data);
  CastFromMirroredOpConf(const CastFromMirroredOpConf& other);
  // enable nothrow for ::std::vector<CastFromMirroredOpConf> resize 
  CastFromMirroredOpConf(CastFromMirroredOpConf&&) noexcept;
  CastFromMirroredOpConf();
  explicit CastFromMirroredOpConf(const ::oneflow::CastFromMirroredOpConf& proto_castfrommirroredopconf);

  ~CastFromMirroredOpConf() override;

  void InitFromProto(const PbMessage& proto_castfrommirroredopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CastFromMirroredOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CastFromMirroredOpConf& other) const;
  void Clear();
  void CopyFrom(const CastFromMirroredOpConf& other);
  CastFromMirroredOpConf& operator=(const CastFromMirroredOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field sbp_parallel
 public:
  void clear_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_sbp_parallel();

  ::std::shared_ptr<CastFromMirroredOpConf> __SharedMutable__();
};


class ConstCaseOpConf : public ::oneflow::cfg::Message {
 public:

  class _CaseOpConf_ {
   public:
    _CaseOpConf_();
    explicit _CaseOpConf_(const _CaseOpConf_& other);
    explicit _CaseOpConf_(_CaseOpConf_&& other);
    _CaseOpConf_(const ::oneflow::CaseOpConf& proto_caseopconf);
    ~_CaseOpConf_();

    void InitFromProto(const ::oneflow::CaseOpConf& proto_caseopconf);

    void ToProto(::oneflow::CaseOpConf* proto_caseopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CaseOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
         
   public:
    int compare(const _CaseOpConf_& other);

    bool operator==(const _CaseOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CaseOpConf_& other) const;
  };

  ConstCaseOpConf(const ::std::shared_ptr<_CaseOpConf_>& data);
  ConstCaseOpConf(const ConstCaseOpConf&);
  ConstCaseOpConf(ConstCaseOpConf&&) noexcept;
  ConstCaseOpConf();
  ConstCaseOpConf(const ::oneflow::CaseOpConf& proto_caseopconf);
  virtual ~ConstCaseOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_caseopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;

 public:
  ::std::shared_ptr<ConstCaseOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCaseOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCaseOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CaseOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CaseOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCaseOpConf
  void BuildFromProto(const PbMessage& proto_caseopconf);
  
  ::std::shared_ptr<_CaseOpConf_> data_;
};

class CaseOpConf final : public ConstCaseOpConf {
 public:
  CaseOpConf(const ::std::shared_ptr<_CaseOpConf_>& data);
  CaseOpConf(const CaseOpConf& other);
  // enable nothrow for ::std::vector<CaseOpConf> resize 
  CaseOpConf(CaseOpConf&&) noexcept;
  CaseOpConf();
  explicit CaseOpConf(const ::oneflow::CaseOpConf& proto_caseopconf);

  ~CaseOpConf() override;

  void InitFromProto(const PbMessage& proto_caseopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CaseOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CaseOpConf& other) const;
  void Clear();
  void CopyFrom(const CaseOpConf& other);
  CaseOpConf& operator=(const CaseOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<CaseOpConf> __SharedMutable__();
};


class ConstEsacOpConf : public ::oneflow::cfg::Message {
 public:

  class _EsacOpConf_ {
   public:
    _EsacOpConf_();
    explicit _EsacOpConf_(const _EsacOpConf_& other);
    explicit _EsacOpConf_(_EsacOpConf_&& other);
    _EsacOpConf_(const ::oneflow::EsacOpConf& proto_esacopconf);
    ~_EsacOpConf_();

    void InitFromProto(const ::oneflow::EsacOpConf& proto_esacopconf);

    void ToProto(::oneflow::EsacOpConf* proto_esacopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _EsacOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
           
   public:
    int compare(const _EsacOpConf_& other);

    bool operator==(const _EsacOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _EsacOpConf_& other) const;
  };

  ConstEsacOpConf(const ::std::shared_ptr<_EsacOpConf_>& data);
  ConstEsacOpConf(const ConstEsacOpConf&);
  ConstEsacOpConf(ConstEsacOpConf&&) noexcept;
  ConstEsacOpConf();
  ConstEsacOpConf(const ::oneflow::EsacOpConf& proto_esacopconf);
  virtual ~ConstEsacOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_esacopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstEsacOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstEsacOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstEsacOpConf& other) const;
 protected:
  const ::std::shared_ptr<_EsacOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_EsacOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstEsacOpConf
  void BuildFromProto(const PbMessage& proto_esacopconf);
  
  ::std::shared_ptr<_EsacOpConf_> data_;
};

class EsacOpConf final : public ConstEsacOpConf {
 public:
  EsacOpConf(const ::std::shared_ptr<_EsacOpConf_>& data);
  EsacOpConf(const EsacOpConf& other);
  // enable nothrow for ::std::vector<EsacOpConf> resize 
  EsacOpConf(EsacOpConf&&) noexcept;
  EsacOpConf();
  explicit EsacOpConf(const ::oneflow::EsacOpConf& proto_esacopconf);

  ~EsacOpConf() override;

  void InitFromProto(const PbMessage& proto_esacopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const EsacOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const EsacOpConf& other) const;
  void Clear();
  void CopyFrom(const EsacOpConf& other);
  EsacOpConf& operator=(const EsacOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();

  ::std::shared_ptr<EsacOpConf> __SharedMutable__();
};


class ConstAssignOpConf : public ::oneflow::cfg::Message {
 public:

  class _AssignOpConf_ {
   public:
    _AssignOpConf_();
    explicit _AssignOpConf_(const _AssignOpConf_& other);
    explicit _AssignOpConf_(_AssignOpConf_&& other);
    _AssignOpConf_(const ::oneflow::AssignOpConf& proto_assignopconf);
    ~_AssignOpConf_();

    void InitFromProto(const ::oneflow::AssignOpConf& proto_assignopconf);

    void ToProto(::oneflow::AssignOpConf* proto_assignopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _AssignOpConf_& other);
  
      // optional field ref
     public:
    bool has_ref() const;
    const ::std::string& ref() const;
    void clear_ref();
    void set_ref(const ::std::string& value);
    ::std::string* mutable_ref();
   protected:
    bool has_ref_ = false;
    ::std::string ref_;
      
      // optional field value
     public:
    bool has_value() const;
    const ::std::string& value() const;
    void clear_value();
    void set_value(const ::std::string& value);
    ::std::string* mutable_value();
   protected:
    bool has_value_ = false;
    ::std::string value_;
           
   public:
    int compare(const _AssignOpConf_& other);

    bool operator==(const _AssignOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _AssignOpConf_& other) const;
  };

  ConstAssignOpConf(const ::std::shared_ptr<_AssignOpConf_>& data);
  ConstAssignOpConf(const ConstAssignOpConf&);
  ConstAssignOpConf(ConstAssignOpConf&&) noexcept;
  ConstAssignOpConf();
  ConstAssignOpConf(const ::oneflow::AssignOpConf& proto_assignopconf);
  virtual ~ConstAssignOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_assignopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field ref
 public:
  bool has_ref() const;
  const ::std::string& ref() const;
  // used by pybind11 only
  // required or optional field value
 public:
  bool has_value() const;
  const ::std::string& value() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstAssignOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstAssignOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstAssignOpConf& other) const;
 protected:
  const ::std::shared_ptr<_AssignOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_AssignOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstAssignOpConf
  void BuildFromProto(const PbMessage& proto_assignopconf);
  
  ::std::shared_ptr<_AssignOpConf_> data_;
};

class AssignOpConf final : public ConstAssignOpConf {
 public:
  AssignOpConf(const ::std::shared_ptr<_AssignOpConf_>& data);
  AssignOpConf(const AssignOpConf& other);
  // enable nothrow for ::std::vector<AssignOpConf> resize 
  AssignOpConf(AssignOpConf&&) noexcept;
  AssignOpConf();
  explicit AssignOpConf(const ::oneflow::AssignOpConf& proto_assignopconf);

  ~AssignOpConf() override;

  void InitFromProto(const PbMessage& proto_assignopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const AssignOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const AssignOpConf& other) const;
  void Clear();
  void CopyFrom(const AssignOpConf& other);
  AssignOpConf& operator=(const AssignOpConf& other);

  // required or optional field ref
 public:
  void clear_ref();
  void set_ref(const ::std::string& value);
  ::std::string* mutable_ref();
  // required or optional field value
 public:
  void clear_value();
  void set_value(const ::std::string& value);
  ::std::string* mutable_value();

  ::std::shared_ptr<AssignOpConf> __SharedMutable__();
};


class ConstModelSaveOpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelSaveOpConf_ {
   public:
    _ModelSaveOpConf_();
    explicit _ModelSaveOpConf_(const _ModelSaveOpConf_& other);
    explicit _ModelSaveOpConf_(_ModelSaveOpConf_&& other);
    _ModelSaveOpConf_(const ::oneflow::ModelSaveOpConf& proto_modelsaveopconf);
    ~_ModelSaveOpConf_();

    void InitFromProto(const ::oneflow::ModelSaveOpConf& proto_modelsaveopconf);

    void ToProto(::oneflow::ModelSaveOpConf* proto_modelsaveopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelSaveOpConf_& other);
  
      // optional field path
     public:
    bool has_path() const;
    const ::std::string& path() const;
    void clear_path();
    void set_path(const ::std::string& value);
    ::std::string* mutable_path();
   protected:
    bool has_path_ = false;
    ::std::string path_;
      
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // repeated field key
   public:
    ::std::size_t key_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& key() const;
    const ::std::string& key(::std::size_t index) const;
    void clear_key();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_key();
    ::std::string* mutable_key(::std::size_t index);
      void add_key(const ::std::string& value);
    void set_key(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> key_;
         
   public:
    int compare(const _ModelSaveOpConf_& other);

    bool operator==(const _ModelSaveOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelSaveOpConf_& other) const;
  };

  ConstModelSaveOpConf(const ::std::shared_ptr<_ModelSaveOpConf_>& data);
  ConstModelSaveOpConf(const ConstModelSaveOpConf&);
  ConstModelSaveOpConf(ConstModelSaveOpConf&&) noexcept;
  ConstModelSaveOpConf();
  ConstModelSaveOpConf(const ::oneflow::ModelSaveOpConf& proto_modelsaveopconf);
  virtual ~ConstModelSaveOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelsaveopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field path
 public:
  bool has_path() const;
  const ::std::string& path() const;
  // used by pybind11 only
  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // repeated field key
 public:
  ::std::size_t key_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& key() const;
  const ::std::string& key(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_key() const;

 public:
  ::std::shared_ptr<ConstModelSaveOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelSaveOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelSaveOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelSaveOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelSaveOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelSaveOpConf
  void BuildFromProto(const PbMessage& proto_modelsaveopconf);
  
  ::std::shared_ptr<_ModelSaveOpConf_> data_;
};

class ModelSaveOpConf final : public ConstModelSaveOpConf {
 public:
  ModelSaveOpConf(const ::std::shared_ptr<_ModelSaveOpConf_>& data);
  ModelSaveOpConf(const ModelSaveOpConf& other);
  // enable nothrow for ::std::vector<ModelSaveOpConf> resize 
  ModelSaveOpConf(ModelSaveOpConf&&) noexcept;
  ModelSaveOpConf();
  explicit ModelSaveOpConf(const ::oneflow::ModelSaveOpConf& proto_modelsaveopconf);

  ~ModelSaveOpConf() override;

  void InitFromProto(const PbMessage& proto_modelsaveopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelSaveOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelSaveOpConf& other) const;
  void Clear();
  void CopyFrom(const ModelSaveOpConf& other);
  ModelSaveOpConf& operator=(const ModelSaveOpConf& other);

  // required or optional field path
 public:
  void clear_path();
  void set_path(const ::std::string& value);
  ::std::string* mutable_path();
  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // repeated field key
 public:
  void clear_key();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_key();
  ::std::string* mutable_key(::std::size_t index);
  void add_key(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_key();
  void set_key(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<ModelSaveOpConf> __SharedMutable__();
};


class ConstLearningRateScheduleOpConf : public ::oneflow::cfg::Message {
 public:

  class _LearningRateScheduleOpConf_ {
   public:
    _LearningRateScheduleOpConf_();
    explicit _LearningRateScheduleOpConf_(const _LearningRateScheduleOpConf_& other);
    explicit _LearningRateScheduleOpConf_(_LearningRateScheduleOpConf_&& other);
    _LearningRateScheduleOpConf_(const ::oneflow::LearningRateScheduleOpConf& proto_learningratescheduleopconf);
    ~_LearningRateScheduleOpConf_();

    void InitFromProto(const ::oneflow::LearningRateScheduleOpConf& proto_learningratescheduleopconf);

    void ToProto(::oneflow::LearningRateScheduleOpConf* proto_learningratescheduleopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _LearningRateScheduleOpConf_& other);
  
      // optional field train_step
     public:
    bool has_train_step() const;
    const ::std::string& train_step() const;
    void clear_train_step();
    void set_train_step(const ::std::string& value);
    ::std::string* mutable_train_step();
   protected:
    bool has_train_step_ = false;
    ::std::string train_step_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field learning_rate
     public:
    bool has_learning_rate() const;
    const float& learning_rate() const;
    void clear_learning_rate();
    void set_learning_rate(const float& value);
    float* mutable_learning_rate();
   protected:
    bool has_learning_rate_ = false;
    float learning_rate_;
      
      // optional field learning_rate_decay
     public:
    bool has_learning_rate_decay() const;
    const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
    void clear_learning_rate_decay();
    ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
   protected:
    bool has_learning_rate_decay_ = false;
    ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> learning_rate_decay_;
      
      // optional field warmup_conf
     public:
    bool has_warmup_conf() const;
    const ::oneflow::cfg::WarmupConf& warmup_conf() const;
    void clear_warmup_conf();
    ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
   protected:
    bool has_warmup_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::WarmupConf> warmup_conf_;
           
   public:
    int compare(const _LearningRateScheduleOpConf_& other);

    bool operator==(const _LearningRateScheduleOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _LearningRateScheduleOpConf_& other) const;
  };

  ConstLearningRateScheduleOpConf(const ::std::shared_ptr<_LearningRateScheduleOpConf_>& data);
  ConstLearningRateScheduleOpConf(const ConstLearningRateScheduleOpConf&);
  ConstLearningRateScheduleOpConf(ConstLearningRateScheduleOpConf&&) noexcept;
  ConstLearningRateScheduleOpConf();
  ConstLearningRateScheduleOpConf(const ::oneflow::LearningRateScheduleOpConf& proto_learningratescheduleopconf);
  virtual ~ConstLearningRateScheduleOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_learningratescheduleopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field train_step
 public:
  bool has_train_step() const;
  const ::std::string& train_step() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field learning_rate
 public:
  bool has_learning_rate() const;
  const float& learning_rate() const;
  // used by pybind11 only
  // required or optional field learning_rate_decay
 public:
  bool has_learning_rate_decay() const;
  const ::oneflow::cfg::LearningRateDecayConf& learning_rate_decay() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLearningRateDecayConf> shared_const_learning_rate_decay() const;
  // required or optional field warmup_conf
 public:
  bool has_warmup_conf() const;
  const ::oneflow::cfg::WarmupConf& warmup_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWarmupConf> shared_const_warmup_conf() const;

 public:
  ::std::shared_ptr<ConstLearningRateScheduleOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstLearningRateScheduleOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstLearningRateScheduleOpConf& other) const;
 protected:
  const ::std::shared_ptr<_LearningRateScheduleOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_LearningRateScheduleOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstLearningRateScheduleOpConf
  void BuildFromProto(const PbMessage& proto_learningratescheduleopconf);
  
  ::std::shared_ptr<_LearningRateScheduleOpConf_> data_;
};

class LearningRateScheduleOpConf final : public ConstLearningRateScheduleOpConf {
 public:
  LearningRateScheduleOpConf(const ::std::shared_ptr<_LearningRateScheduleOpConf_>& data);
  LearningRateScheduleOpConf(const LearningRateScheduleOpConf& other);
  // enable nothrow for ::std::vector<LearningRateScheduleOpConf> resize 
  LearningRateScheduleOpConf(LearningRateScheduleOpConf&&) noexcept;
  LearningRateScheduleOpConf();
  explicit LearningRateScheduleOpConf(const ::oneflow::LearningRateScheduleOpConf& proto_learningratescheduleopconf);

  ~LearningRateScheduleOpConf() override;

  void InitFromProto(const PbMessage& proto_learningratescheduleopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const LearningRateScheduleOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const LearningRateScheduleOpConf& other) const;
  void Clear();
  void CopyFrom(const LearningRateScheduleOpConf& other);
  LearningRateScheduleOpConf& operator=(const LearningRateScheduleOpConf& other);

  // required or optional field train_step
 public:
  void clear_train_step();
  void set_train_step(const ::std::string& value);
  ::std::string* mutable_train_step();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field learning_rate
 public:
  void clear_learning_rate();
  void set_learning_rate(const float& value);
  float* mutable_learning_rate();
  // required or optional field learning_rate_decay
 public:
  void clear_learning_rate_decay();
  ::oneflow::cfg::LearningRateDecayConf* mutable_learning_rate_decay();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LearningRateDecayConf> shared_mutable_learning_rate_decay();
  // required or optional field warmup_conf
 public:
  void clear_warmup_conf();
  ::oneflow::cfg::WarmupConf* mutable_warmup_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WarmupConf> shared_mutable_warmup_conf();

  ::std::shared_ptr<LearningRateScheduleOpConf> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_;

class ConstSliceBoxingConf : public ::oneflow::cfg::Message {
 public:

  class _SliceBoxingConf_ {
   public:
    _SliceBoxingConf_();
    explicit _SliceBoxingConf_(const _SliceBoxingConf_& other);
    explicit _SliceBoxingConf_(_SliceBoxingConf_&& other);
    _SliceBoxingConf_(const ::oneflow::SliceBoxingConf& proto_sliceboxingconf);
    ~_SliceBoxingConf_();

    void InitFromProto(const ::oneflow::SliceBoxingConf& proto_sliceboxingconf);

    void ToProto(::oneflow::SliceBoxingConf* proto_sliceboxingconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SliceBoxingConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // repeated field in_slice
   public:
    ::std::size_t in_slice_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& in_slice() const;
    const ::oneflow::cfg::TensorSliceViewProto& in_slice(::std::size_t index) const;
    void clear_in_slice();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_* mutable_in_slice();
    ::oneflow::cfg::TensorSliceViewProto* mutable_in_slice(::std::size_t index);
      ::oneflow::cfg::TensorSliceViewProto* add_in_slice();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> in_slice_;
    
      // optional field out_slice
     public:
    bool has_out_slice() const;
    const ::oneflow::cfg::TensorSliceViewProto& out_slice() const;
    void clear_out_slice();
    ::oneflow::cfg::TensorSliceViewProto* mutable_out_slice();
   protected:
    bool has_out_slice_ = false;
    ::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> out_slice_;
      
      // optional field out_shape
     public:
    bool has_out_shape() const;
    const ::oneflow::cfg::ShapeProto& out_shape() const;
    void clear_out_shape();
    ::oneflow::cfg::ShapeProto* mutable_out_shape();
   protected:
    bool has_out_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> out_shape_;
           
   public:
    int compare(const _SliceBoxingConf_& other);

    bool operator==(const _SliceBoxingConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SliceBoxingConf_& other) const;
  };

  ConstSliceBoxingConf(const ::std::shared_ptr<_SliceBoxingConf_>& data);
  ConstSliceBoxingConf(const ConstSliceBoxingConf&);
  ConstSliceBoxingConf(ConstSliceBoxingConf&&) noexcept;
  ConstSliceBoxingConf();
  ConstSliceBoxingConf(const ::oneflow::SliceBoxingConf& proto_sliceboxingconf);
  virtual ~ConstSliceBoxingConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sliceboxingconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // repeated field in_slice
 public:
  ::std::size_t in_slice_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& in_slice() const;
  const ::oneflow::cfg::TensorSliceViewProto& in_slice(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> shared_const_in_slice() const;
  ::std::shared_ptr<::oneflow::cfg::ConstTensorSliceViewProto> shared_const_in_slice(::std::size_t index) const;
  // required or optional field out_slice
 public:
  bool has_out_slice() const;
  const ::oneflow::cfg::TensorSliceViewProto& out_slice() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTensorSliceViewProto> shared_const_out_slice() const;
  // required or optional field out_shape
 public:
  bool has_out_shape() const;
  const ::oneflow::cfg::ShapeProto& out_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_out_shape() const;

 public:
  ::std::shared_ptr<ConstSliceBoxingConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSliceBoxingConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSliceBoxingConf& other) const;
 protected:
  const ::std::shared_ptr<_SliceBoxingConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SliceBoxingConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSliceBoxingConf
  void BuildFromProto(const PbMessage& proto_sliceboxingconf);
  
  ::std::shared_ptr<_SliceBoxingConf_> data_;
};

class SliceBoxingConf final : public ConstSliceBoxingConf {
 public:
  SliceBoxingConf(const ::std::shared_ptr<_SliceBoxingConf_>& data);
  SliceBoxingConf(const SliceBoxingConf& other);
  // enable nothrow for ::std::vector<SliceBoxingConf> resize 
  SliceBoxingConf(SliceBoxingConf&&) noexcept;
  SliceBoxingConf();
  explicit SliceBoxingConf(const ::oneflow::SliceBoxingConf& proto_sliceboxingconf);

  ~SliceBoxingConf() override;

  void InitFromProto(const PbMessage& proto_sliceboxingconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SliceBoxingConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SliceBoxingConf& other) const;
  void Clear();
  void CopyFrom(const SliceBoxingConf& other);
  SliceBoxingConf& operator=(const SliceBoxingConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // repeated field in_slice
 public:
  void clear_in_slice();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_* mutable_in_slice();
  ::oneflow::cfg::TensorSliceViewProto* mutable_in_slice(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> shared_mutable_in_slice();
  ::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> shared_mutable_in_slice(::std::size_t index);
  ::oneflow::cfg::TensorSliceViewProto* add_in_slice();
  // required or optional field out_slice
 public:
  void clear_out_slice();
  ::oneflow::cfg::TensorSliceViewProto* mutable_out_slice();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> shared_mutable_out_slice();
  // required or optional field out_shape
 public:
  void clear_out_shape();
  ::oneflow::cfg::ShapeProto* mutable_out_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_out_shape();

  ::std::shared_ptr<SliceBoxingConf> __SharedMutable__();
};


class ConstSliceBoxingCopyOpConf : public ::oneflow::cfg::Message {
 public:

  class _SliceBoxingCopyOpConf_ {
   public:
    _SliceBoxingCopyOpConf_();
    explicit _SliceBoxingCopyOpConf_(const _SliceBoxingCopyOpConf_& other);
    explicit _SliceBoxingCopyOpConf_(_SliceBoxingCopyOpConf_&& other);
    _SliceBoxingCopyOpConf_(const ::oneflow::SliceBoxingCopyOpConf& proto_sliceboxingcopyopconf);
    ~_SliceBoxingCopyOpConf_();

    void InitFromProto(const ::oneflow::SliceBoxingCopyOpConf& proto_sliceboxingcopyopconf);

    void ToProto(::oneflow::SliceBoxingCopyOpConf* proto_sliceboxingcopyopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SliceBoxingCopyOpConf_& other);
  
      // optional field slice_boxing_conf
     public:
    bool has_slice_boxing_conf() const;
    const ::oneflow::cfg::SliceBoxingConf& slice_boxing_conf() const;
    void clear_slice_boxing_conf();
    ::oneflow::cfg::SliceBoxingConf* mutable_slice_boxing_conf();
   protected:
    bool has_slice_boxing_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::SliceBoxingConf> slice_boxing_conf_;
           
   public:
    int compare(const _SliceBoxingCopyOpConf_& other);

    bool operator==(const _SliceBoxingCopyOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SliceBoxingCopyOpConf_& other) const;
  };

  ConstSliceBoxingCopyOpConf(const ::std::shared_ptr<_SliceBoxingCopyOpConf_>& data);
  ConstSliceBoxingCopyOpConf(const ConstSliceBoxingCopyOpConf&);
  ConstSliceBoxingCopyOpConf(ConstSliceBoxingCopyOpConf&&) noexcept;
  ConstSliceBoxingCopyOpConf();
  ConstSliceBoxingCopyOpConf(const ::oneflow::SliceBoxingCopyOpConf& proto_sliceboxingcopyopconf);
  virtual ~ConstSliceBoxingCopyOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sliceboxingcopyopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field slice_boxing_conf
 public:
  bool has_slice_boxing_conf() const;
  const ::oneflow::cfg::SliceBoxingConf& slice_boxing_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSliceBoxingConf> shared_const_slice_boxing_conf() const;

 public:
  ::std::shared_ptr<ConstSliceBoxingCopyOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSliceBoxingCopyOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSliceBoxingCopyOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SliceBoxingCopyOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SliceBoxingCopyOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSliceBoxingCopyOpConf
  void BuildFromProto(const PbMessage& proto_sliceboxingcopyopconf);
  
  ::std::shared_ptr<_SliceBoxingCopyOpConf_> data_;
};

class SliceBoxingCopyOpConf final : public ConstSliceBoxingCopyOpConf {
 public:
  SliceBoxingCopyOpConf(const ::std::shared_ptr<_SliceBoxingCopyOpConf_>& data);
  SliceBoxingCopyOpConf(const SliceBoxingCopyOpConf& other);
  // enable nothrow for ::std::vector<SliceBoxingCopyOpConf> resize 
  SliceBoxingCopyOpConf(SliceBoxingCopyOpConf&&) noexcept;
  SliceBoxingCopyOpConf();
  explicit SliceBoxingCopyOpConf(const ::oneflow::SliceBoxingCopyOpConf& proto_sliceboxingcopyopconf);

  ~SliceBoxingCopyOpConf() override;

  void InitFromProto(const PbMessage& proto_sliceboxingcopyopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SliceBoxingCopyOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SliceBoxingCopyOpConf& other) const;
  void Clear();
  void CopyFrom(const SliceBoxingCopyOpConf& other);
  SliceBoxingCopyOpConf& operator=(const SliceBoxingCopyOpConf& other);

  // required or optional field slice_boxing_conf
 public:
  void clear_slice_boxing_conf();
  ::oneflow::cfg::SliceBoxingConf* mutable_slice_boxing_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SliceBoxingConf> shared_mutable_slice_boxing_conf();

  ::std::shared_ptr<SliceBoxingCopyOpConf> __SharedMutable__();
};


class ConstSliceBoxingAddOpConf : public ::oneflow::cfg::Message {
 public:

  class _SliceBoxingAddOpConf_ {
   public:
    _SliceBoxingAddOpConf_();
    explicit _SliceBoxingAddOpConf_(const _SliceBoxingAddOpConf_& other);
    explicit _SliceBoxingAddOpConf_(_SliceBoxingAddOpConf_&& other);
    _SliceBoxingAddOpConf_(const ::oneflow::SliceBoxingAddOpConf& proto_sliceboxingaddopconf);
    ~_SliceBoxingAddOpConf_();

    void InitFromProto(const ::oneflow::SliceBoxingAddOpConf& proto_sliceboxingaddopconf);

    void ToProto(::oneflow::SliceBoxingAddOpConf* proto_sliceboxingaddopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SliceBoxingAddOpConf_& other);
  
      // optional field slice_boxing_conf
     public:
    bool has_slice_boxing_conf() const;
    const ::oneflow::cfg::SliceBoxingConf& slice_boxing_conf() const;
    void clear_slice_boxing_conf();
    ::oneflow::cfg::SliceBoxingConf* mutable_slice_boxing_conf();
   protected:
    bool has_slice_boxing_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::SliceBoxingConf> slice_boxing_conf_;
           
   public:
    int compare(const _SliceBoxingAddOpConf_& other);

    bool operator==(const _SliceBoxingAddOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SliceBoxingAddOpConf_& other) const;
  };

  ConstSliceBoxingAddOpConf(const ::std::shared_ptr<_SliceBoxingAddOpConf_>& data);
  ConstSliceBoxingAddOpConf(const ConstSliceBoxingAddOpConf&);
  ConstSliceBoxingAddOpConf(ConstSliceBoxingAddOpConf&&) noexcept;
  ConstSliceBoxingAddOpConf();
  ConstSliceBoxingAddOpConf(const ::oneflow::SliceBoxingAddOpConf& proto_sliceboxingaddopconf);
  virtual ~ConstSliceBoxingAddOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_sliceboxingaddopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field slice_boxing_conf
 public:
  bool has_slice_boxing_conf() const;
  const ::oneflow::cfg::SliceBoxingConf& slice_boxing_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSliceBoxingConf> shared_const_slice_boxing_conf() const;

 public:
  ::std::shared_ptr<ConstSliceBoxingAddOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSliceBoxingAddOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSliceBoxingAddOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SliceBoxingAddOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SliceBoxingAddOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSliceBoxingAddOpConf
  void BuildFromProto(const PbMessage& proto_sliceboxingaddopconf);
  
  ::std::shared_ptr<_SliceBoxingAddOpConf_> data_;
};

class SliceBoxingAddOpConf final : public ConstSliceBoxingAddOpConf {
 public:
  SliceBoxingAddOpConf(const ::std::shared_ptr<_SliceBoxingAddOpConf_>& data);
  SliceBoxingAddOpConf(const SliceBoxingAddOpConf& other);
  // enable nothrow for ::std::vector<SliceBoxingAddOpConf> resize 
  SliceBoxingAddOpConf(SliceBoxingAddOpConf&&) noexcept;
  SliceBoxingAddOpConf();
  explicit SliceBoxingAddOpConf(const ::oneflow::SliceBoxingAddOpConf& proto_sliceboxingaddopconf);

  ~SliceBoxingAddOpConf() override;

  void InitFromProto(const PbMessage& proto_sliceboxingaddopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SliceBoxingAddOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SliceBoxingAddOpConf& other) const;
  void Clear();
  void CopyFrom(const SliceBoxingAddOpConf& other);
  SliceBoxingAddOpConf& operator=(const SliceBoxingAddOpConf& other);

  // required or optional field slice_boxing_conf
 public:
  void clear_slice_boxing_conf();
  ::oneflow::cfg::SliceBoxingConf* mutable_slice_boxing_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SliceBoxingConf> shared_mutable_slice_boxing_conf();

  ::std::shared_ptr<SliceBoxingAddOpConf> __SharedMutable__();
};


class ConstXrtLaunchOpConf_Argument : public ::oneflow::cfg::Message {
 public:

  class _XrtLaunchOpConf_Argument_ {
   public:
    _XrtLaunchOpConf_Argument_();
    explicit _XrtLaunchOpConf_Argument_(const _XrtLaunchOpConf_Argument_& other);
    explicit _XrtLaunchOpConf_Argument_(_XrtLaunchOpConf_Argument_&& other);
    _XrtLaunchOpConf_Argument_(const ::oneflow::XrtLaunchOpConf_Argument& proto_xrtlaunchopconf_argument);
    ~_XrtLaunchOpConf_Argument_();

    void InitFromProto(const ::oneflow::XrtLaunchOpConf_Argument& proto_xrtlaunchopconf_argument);

    void ToProto(::oneflow::XrtLaunchOpConf_Argument* proto_xrtlaunchopconf_argument) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtLaunchOpConf_Argument_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
      // optional field value
     public:
    bool has_value() const;
    const ::std::string& value() const;
    void clear_value();
    void set_value(const ::std::string& value);
    ::std::string* mutable_value();
   protected:
    bool has_value_ = false;
    ::std::string value_;
      
      // optional field device_type
     public:
    bool has_device_type() const;
    const ::oneflow::cfg::DeviceType& device_type() const;
    void clear_device_type();
    void set_device_type(const ::oneflow::cfg::DeviceType& value);
    ::oneflow::cfg::DeviceType* mutable_device_type();
   protected:
    bool has_device_type_ = false;
    ::oneflow::cfg::DeviceType device_type_;
           
   public:
    int compare(const _XrtLaunchOpConf_Argument_& other);

    bool operator==(const _XrtLaunchOpConf_Argument_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtLaunchOpConf_Argument_& other) const;
  };

  ConstXrtLaunchOpConf_Argument(const ::std::shared_ptr<_XrtLaunchOpConf_Argument_>& data);
  ConstXrtLaunchOpConf_Argument(const ConstXrtLaunchOpConf_Argument&);
  ConstXrtLaunchOpConf_Argument(ConstXrtLaunchOpConf_Argument&&) noexcept;
  ConstXrtLaunchOpConf_Argument();
  ConstXrtLaunchOpConf_Argument(const ::oneflow::XrtLaunchOpConf_Argument& proto_xrtlaunchopconf_argument);
  virtual ~ConstXrtLaunchOpConf_Argument() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtlaunchopconf_argument) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // required or optional field value
 public:
  bool has_value() const;
  const ::std::string& value() const;
  // used by pybind11 only
  // required or optional field device_type
 public:
  bool has_device_type() const;
  const ::oneflow::cfg::DeviceType& device_type() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstXrtLaunchOpConf_Argument> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtLaunchOpConf_Argument& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtLaunchOpConf_Argument& other) const;
 protected:
  const ::std::shared_ptr<_XrtLaunchOpConf_Argument_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtLaunchOpConf_Argument_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtLaunchOpConf_Argument
  void BuildFromProto(const PbMessage& proto_xrtlaunchopconf_argument);
  
  ::std::shared_ptr<_XrtLaunchOpConf_Argument_> data_;
};

class XrtLaunchOpConf_Argument final : public ConstXrtLaunchOpConf_Argument {
 public:
  XrtLaunchOpConf_Argument(const ::std::shared_ptr<_XrtLaunchOpConf_Argument_>& data);
  XrtLaunchOpConf_Argument(const XrtLaunchOpConf_Argument& other);
  // enable nothrow for ::std::vector<XrtLaunchOpConf_Argument> resize 
  XrtLaunchOpConf_Argument(XrtLaunchOpConf_Argument&&) noexcept;
  XrtLaunchOpConf_Argument();
  explicit XrtLaunchOpConf_Argument(const ::oneflow::XrtLaunchOpConf_Argument& proto_xrtlaunchopconf_argument);

  ~XrtLaunchOpConf_Argument() override;

  void InitFromProto(const PbMessage& proto_xrtlaunchopconf_argument) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtLaunchOpConf_Argument& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtLaunchOpConf_Argument& other) const;
  void Clear();
  void CopyFrom(const XrtLaunchOpConf_Argument& other);
  XrtLaunchOpConf_Argument& operator=(const XrtLaunchOpConf_Argument& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // required or optional field value
 public:
  void clear_value();
  void set_value(const ::std::string& value);
  ::std::string* mutable_value();
  // required or optional field device_type
 public:
  void clear_device_type();
  void set_device_type(const ::oneflow::cfg::DeviceType& value);
  ::oneflow::cfg::DeviceType* mutable_device_type();

  ::std::shared_ptr<XrtLaunchOpConf_Argument> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_;
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_;

class ConstXrtLaunchOpConf_Function : public ::oneflow::cfg::Message {
 public:

  class _XrtLaunchOpConf_Function_ {
   public:
    _XrtLaunchOpConf_Function_();
    explicit _XrtLaunchOpConf_Function_(const _XrtLaunchOpConf_Function_& other);
    explicit _XrtLaunchOpConf_Function_(_XrtLaunchOpConf_Function_&& other);
    _XrtLaunchOpConf_Function_(const ::oneflow::XrtLaunchOpConf_Function& proto_xrtlaunchopconf_function);
    ~_XrtLaunchOpConf_Function_();

    void InitFromProto(const ::oneflow::XrtLaunchOpConf_Function& proto_xrtlaunchopconf_function);

    void ToProto(::oneflow::XrtLaunchOpConf_Function* proto_xrtlaunchopconf_function) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtLaunchOpConf_Function_& other);
  
      // repeated field argument
   public:
    ::std::size_t argument_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& argument() const;
    const ::oneflow::cfg::XrtLaunchOpConf_Argument& argument(::std::size_t index) const;
    void clear_argument();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_* mutable_argument();
    ::oneflow::cfg::XrtLaunchOpConf_Argument* mutable_argument(::std::size_t index);
      ::oneflow::cfg::XrtLaunchOpConf_Argument* add_argument();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> argument_;
    
      // repeated field node
   public:
    ::std::size_t node_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& node() const;
    const ::oneflow::cfg::OperatorConf& node(::std::size_t index) const;
    void clear_node();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_* mutable_node();
    ::oneflow::cfg::OperatorConf* mutable_node(::std::size_t index);
      ::oneflow::cfg::OperatorConf* add_node();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> node_;
         
   public:
    int compare(const _XrtLaunchOpConf_Function_& other);

    bool operator==(const _XrtLaunchOpConf_Function_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtLaunchOpConf_Function_& other) const;
  };

  ConstXrtLaunchOpConf_Function(const ::std::shared_ptr<_XrtLaunchOpConf_Function_>& data);
  ConstXrtLaunchOpConf_Function(const ConstXrtLaunchOpConf_Function&);
  ConstXrtLaunchOpConf_Function(ConstXrtLaunchOpConf_Function&&) noexcept;
  ConstXrtLaunchOpConf_Function();
  ConstXrtLaunchOpConf_Function(const ::oneflow::XrtLaunchOpConf_Function& proto_xrtlaunchopconf_function);
  virtual ~ConstXrtLaunchOpConf_Function() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtlaunchopconf_function) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field argument
 public:
  ::std::size_t argument_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& argument() const;
  const ::oneflow::cfg::XrtLaunchOpConf_Argument& argument(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> shared_const_argument() const;
  ::std::shared_ptr<::oneflow::cfg::ConstXrtLaunchOpConf_Argument> shared_const_argument(::std::size_t index) const;
  // repeated field node
 public:
  ::std::size_t node_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& node() const;
  const ::oneflow::cfg::OperatorConf& node(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> shared_const_node() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> shared_const_node(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstXrtLaunchOpConf_Function> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtLaunchOpConf_Function& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtLaunchOpConf_Function& other) const;
 protected:
  const ::std::shared_ptr<_XrtLaunchOpConf_Function_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtLaunchOpConf_Function_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtLaunchOpConf_Function
  void BuildFromProto(const PbMessage& proto_xrtlaunchopconf_function);
  
  ::std::shared_ptr<_XrtLaunchOpConf_Function_> data_;
};

class XrtLaunchOpConf_Function final : public ConstXrtLaunchOpConf_Function {
 public:
  XrtLaunchOpConf_Function(const ::std::shared_ptr<_XrtLaunchOpConf_Function_>& data);
  XrtLaunchOpConf_Function(const XrtLaunchOpConf_Function& other);
  // enable nothrow for ::std::vector<XrtLaunchOpConf_Function> resize 
  XrtLaunchOpConf_Function(XrtLaunchOpConf_Function&&) noexcept;
  XrtLaunchOpConf_Function();
  explicit XrtLaunchOpConf_Function(const ::oneflow::XrtLaunchOpConf_Function& proto_xrtlaunchopconf_function);

  ~XrtLaunchOpConf_Function() override;

  void InitFromProto(const PbMessage& proto_xrtlaunchopconf_function) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtLaunchOpConf_Function& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtLaunchOpConf_Function& other) const;
  void Clear();
  void CopyFrom(const XrtLaunchOpConf_Function& other);
  XrtLaunchOpConf_Function& operator=(const XrtLaunchOpConf_Function& other);

  // repeated field argument
 public:
  void clear_argument();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_* mutable_argument();
  ::oneflow::cfg::XrtLaunchOpConf_Argument* mutable_argument(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> shared_mutable_argument();
  ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> shared_mutable_argument(::std::size_t index);
  ::oneflow::cfg::XrtLaunchOpConf_Argument* add_argument();
  // repeated field node
 public:
  void clear_node();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_* mutable_node();
  ::oneflow::cfg::OperatorConf* mutable_node(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> shared_mutable_node();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> shared_mutable_node(::std::size_t index);
  ::oneflow::cfg::OperatorConf* add_node();

  ::std::shared_ptr<XrtLaunchOpConf_Function> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_;
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_;
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_;
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_;

class ConstXrtLaunchOpConf : public ::oneflow::cfg::Message {
 public:

  class _XrtLaunchOpConf_ {
   public:
    _XrtLaunchOpConf_();
    explicit _XrtLaunchOpConf_(const _XrtLaunchOpConf_& other);
    explicit _XrtLaunchOpConf_(_XrtLaunchOpConf_&& other);
    _XrtLaunchOpConf_(const ::oneflow::XrtLaunchOpConf& proto_xrtlaunchopconf);
    ~_XrtLaunchOpConf_();

    void InitFromProto(const ::oneflow::XrtLaunchOpConf& proto_xrtlaunchopconf);

    void ToProto(::oneflow::XrtLaunchOpConf* proto_xrtlaunchopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _XrtLaunchOpConf_& other);
  
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // repeated field out
   public:
    ::std::size_t out_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
    const ::std::string& out(::std::size_t index) const;
    void clear_out();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
    ::std::string* mutable_out(::std::size_t index);
      void add_out(const ::std::string& value);
    void set_out(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> out_;
    
      // optional field function
     public:
    bool has_function() const;
    const ::oneflow::cfg::XrtLaunchOpConf_Function& function() const;
    void clear_function();
    ::oneflow::cfg::XrtLaunchOpConf_Function* mutable_function();
   protected:
    bool has_function_ = false;
    ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Function> function_;
      
      // optional field engine
     public:
    bool has_engine() const;
    const ::std::string& engine() const;
    void clear_engine();
    void set_engine(const ::std::string& value);
    ::std::string* mutable_engine();
   protected:
    bool has_engine_ = false;
    ::std::string engine_;
      
     public:
    ::std::size_t input_mutability_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& input_mutability() const;

    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ * mutable_input_mutability();

    const bool& input_mutability(::std::string key) const;

    void clear_input_mutability();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> input_mutability_;
    
     public:
    ::std::size_t input_output_mapping_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& input_output_mapping() const;

    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ * mutable_input_output_mapping();

    const ::std::string& input_output_mapping(::std::string key) const;

    void clear_input_output_mapping();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> input_output_mapping_;
    
     public:
    ::std::size_t sbp_signatures_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& sbp_signatures() const;

    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_ * mutable_sbp_signatures();

    const ::oneflow::cfg::SbpSignature& sbp_signatures(::std::string key) const;

    void clear_sbp_signatures();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> sbp_signatures_;
    
      // optional field model_update
     public:
    bool has_model_update() const;
    const bool& model_update() const;
    void clear_model_update();
    void set_model_update(const bool& value);
    bool* mutable_model_update();
   protected:
    bool has_model_update_ = false;
    bool model_update_;
      
     public:
    ::std::size_t lbn2logical_blob_desc_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& lbn2logical_blob_desc() const;

    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_ * mutable_lbn2logical_blob_desc();

    const ::oneflow::cfg::BlobDescProto& lbn2logical_blob_desc(::std::string key) const;

    void clear_lbn2logical_blob_desc();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> lbn2logical_blob_desc_;
         
   public:
    int compare(const _XrtLaunchOpConf_& other);

    bool operator==(const _XrtLaunchOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _XrtLaunchOpConf_& other) const;
  };

  ConstXrtLaunchOpConf(const ::std::shared_ptr<_XrtLaunchOpConf_>& data);
  ConstXrtLaunchOpConf(const ConstXrtLaunchOpConf&);
  ConstXrtLaunchOpConf(ConstXrtLaunchOpConf&&) noexcept;
  ConstXrtLaunchOpConf();
  ConstXrtLaunchOpConf(const ::oneflow::XrtLaunchOpConf& proto_xrtlaunchopconf);
  virtual ~ConstXrtLaunchOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_xrtlaunchopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // repeated field out
 public:
  ::std::size_t out_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& out() const;
  const ::std::string& out(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_out() const;
  // required or optional field function
 public:
  bool has_function() const;
  const ::oneflow::cfg::XrtLaunchOpConf_Function& function() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXrtLaunchOpConf_Function> shared_const_function() const;
  // required or optional field engine
 public:
  bool has_engine() const;
  const ::std::string& engine() const;
  // used by pybind11 only
  // map field input_mutability
 public:
  ::std::size_t input_mutability_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& input_mutability() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> shared_const_input_mutability() const;
  // map field input_output_mapping
 public:
  ::std::size_t input_output_mapping_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& input_output_mapping() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> shared_const_input_output_mapping() const;
  // map field sbp_signatures
 public:
  ::std::size_t sbp_signatures_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& sbp_signatures() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> shared_const_sbp_signatures() const;
  // required or optional field model_update
 public:
  bool has_model_update() const;
  const bool& model_update() const;
  // used by pybind11 only
  // map field lbn2logical_blob_desc
 public:
  ::std::size_t lbn2logical_blob_desc_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& lbn2logical_blob_desc() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> shared_const_lbn2logical_blob_desc() const;

 public:
  ::std::shared_ptr<ConstXrtLaunchOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstXrtLaunchOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstXrtLaunchOpConf& other) const;
 protected:
  const ::std::shared_ptr<_XrtLaunchOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_XrtLaunchOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstXrtLaunchOpConf
  void BuildFromProto(const PbMessage& proto_xrtlaunchopconf);
  
  ::std::shared_ptr<_XrtLaunchOpConf_> data_;
};

class XrtLaunchOpConf final : public ConstXrtLaunchOpConf {
 public:
  XrtLaunchOpConf(const ::std::shared_ptr<_XrtLaunchOpConf_>& data);
  XrtLaunchOpConf(const XrtLaunchOpConf& other);
  // enable nothrow for ::std::vector<XrtLaunchOpConf> resize 
  XrtLaunchOpConf(XrtLaunchOpConf&&) noexcept;
  XrtLaunchOpConf();
  explicit XrtLaunchOpConf(const ::oneflow::XrtLaunchOpConf& proto_xrtlaunchopconf);

  ~XrtLaunchOpConf() override;

  void InitFromProto(const PbMessage& proto_xrtlaunchopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const XrtLaunchOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const XrtLaunchOpConf& other) const;
  void Clear();
  void CopyFrom(const XrtLaunchOpConf& other);
  XrtLaunchOpConf& operator=(const XrtLaunchOpConf& other);

  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // repeated field out
 public:
  void clear_out();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_out();
  ::std::string* mutable_out(::std::size_t index);
  void add_out(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_out();
  void set_out(::std::size_t index, const ::std::string& value);
  // required or optional field function
 public:
  void clear_function();
  ::oneflow::cfg::XrtLaunchOpConf_Function* mutable_function();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Function> shared_mutable_function();
  // required or optional field engine
 public:
  void clear_engine();
  void set_engine(const ::std::string& value);
  ::std::string* mutable_engine();
  // repeated field input_mutability
 public:
  void clear_input_mutability();

  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ & input_mutability() const;

  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_* mutable_input_mutability();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> shared_mutable_input_mutability();
  // repeated field input_output_mapping
 public:
  void clear_input_output_mapping();

  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ & input_output_mapping() const;

  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_* mutable_input_output_mapping();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> shared_mutable_input_output_mapping();
  // repeated field sbp_signatures
 public:
  void clear_sbp_signatures();

  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_ & sbp_signatures() const;

  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_* mutable_sbp_signatures();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> shared_mutable_sbp_signatures();
  // required or optional field model_update
 public:
  void clear_model_update();
  void set_model_update(const bool& value);
  bool* mutable_model_update();
  // repeated field lbn2logical_blob_desc
 public:
  void clear_lbn2logical_blob_desc();

  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_ & lbn2logical_blob_desc() const;

  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_* mutable_lbn2logical_blob_desc();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> shared_mutable_lbn2logical_blob_desc();

  ::std::shared_ptr<XrtLaunchOpConf> __SharedMutable__();
};


class ConstModelInitV2OpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelInitV2OpConf_ {
   public:
    _ModelInitV2OpConf_();
    explicit _ModelInitV2OpConf_(const _ModelInitV2OpConf_& other);
    explicit _ModelInitV2OpConf_(_ModelInitV2OpConf_&& other);
    _ModelInitV2OpConf_(const ::oneflow::ModelInitV2OpConf& proto_modelinitv2opconf);
    ~_ModelInitV2OpConf_();

    void InitFromProto(const ::oneflow::ModelInitV2OpConf& proto_modelinitv2opconf);

    void ToProto(::oneflow::ModelInitV2OpConf* proto_modelinitv2opconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelInitV2OpConf_& other);
  
      // repeated field ref
   public:
    ::std::size_t ref_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ref() const;
    const ::std::string& ref(::std::size_t index) const;
    void clear_ref();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ref();
    ::std::string* mutable_ref(::std::size_t index);
      void add_ref(const ::std::string& value);
    void set_ref(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> ref_;
    
      // repeated field variable_op_name
   public:
    ::std::size_t variable_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
    const ::std::string& variable_op_name(::std::size_t index) const;
    void clear_variable_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
    ::std::string* mutable_variable_op_name(::std::size_t index);
      void add_variable_op_name(const ::std::string& value);
    void set_variable_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> variable_op_name_;
    
      // repeated field original_variable_conf
   public:
    ::std::size_t original_variable_conf_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
    const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
    void clear_original_variable_conf();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
    ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
      ::oneflow::cfg::VariableOpConf* add_original_variable_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> original_variable_conf_;
         
   public:
    int compare(const _ModelInitV2OpConf_& other);

    bool operator==(const _ModelInitV2OpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelInitV2OpConf_& other) const;
  };

  ConstModelInitV2OpConf(const ::std::shared_ptr<_ModelInitV2OpConf_>& data);
  ConstModelInitV2OpConf(const ConstModelInitV2OpConf&);
  ConstModelInitV2OpConf(ConstModelInitV2OpConf&&) noexcept;
  ConstModelInitV2OpConf();
  ConstModelInitV2OpConf(const ::oneflow::ModelInitV2OpConf& proto_modelinitv2opconf);
  virtual ~ConstModelInitV2OpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelinitv2opconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field ref
 public:
  ::std::size_t ref_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ref() const;
  const ::std::string& ref(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_ref() const;
  // repeated field variable_op_name
 public:
  ::std::size_t variable_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
  const ::std::string& variable_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_name() const;
  // repeated field original_variable_conf
 public:
  ::std::size_t original_variable_conf_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_const_original_variable_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_original_variable_conf(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstModelInitV2OpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelInitV2OpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelInitV2OpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelInitV2OpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelInitV2OpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelInitV2OpConf
  void BuildFromProto(const PbMessage& proto_modelinitv2opconf);
  
  ::std::shared_ptr<_ModelInitV2OpConf_> data_;
};

class ModelInitV2OpConf final : public ConstModelInitV2OpConf {
 public:
  ModelInitV2OpConf(const ::std::shared_ptr<_ModelInitV2OpConf_>& data);
  ModelInitV2OpConf(const ModelInitV2OpConf& other);
  // enable nothrow for ::std::vector<ModelInitV2OpConf> resize 
  ModelInitV2OpConf(ModelInitV2OpConf&&) noexcept;
  ModelInitV2OpConf();
  explicit ModelInitV2OpConf(const ::oneflow::ModelInitV2OpConf& proto_modelinitv2opconf);

  ~ModelInitV2OpConf() override;

  void InitFromProto(const PbMessage& proto_modelinitv2opconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelInitV2OpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelInitV2OpConf& other) const;
  void Clear();
  void CopyFrom(const ModelInitV2OpConf& other);
  ModelInitV2OpConf& operator=(const ModelInitV2OpConf& other);

  // repeated field ref
 public:
  void clear_ref();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ref();
  ::std::string* mutable_ref(::std::size_t index);
  void add_ref(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_ref();
  void set_ref(::std::size_t index, const ::std::string& value);
  // repeated field variable_op_name
 public:
  void clear_variable_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
  ::std::string* mutable_variable_op_name(::std::size_t index);
  void add_variable_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_name();
  void set_variable_op_name(::std::size_t index, const ::std::string& value);
  // repeated field original_variable_conf
 public:
  void clear_original_variable_conf();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_mutable_original_variable_conf();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_original_variable_conf(::std::size_t index);
  ::oneflow::cfg::VariableOpConf* add_original_variable_conf();

  ::std::shared_ptr<ModelInitV2OpConf> __SharedMutable__();
};


class ConstModelLoadV2OpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelLoadV2OpConf_ {
   public:
    _ModelLoadV2OpConf_();
    explicit _ModelLoadV2OpConf_(const _ModelLoadV2OpConf_& other);
    explicit _ModelLoadV2OpConf_(_ModelLoadV2OpConf_&& other);
    _ModelLoadV2OpConf_(const ::oneflow::ModelLoadV2OpConf& proto_modelloadv2opconf);
    ~_ModelLoadV2OpConf_();

    void InitFromProto(const ::oneflow::ModelLoadV2OpConf& proto_modelloadv2opconf);

    void ToProto(::oneflow::ModelLoadV2OpConf* proto_modelloadv2opconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelLoadV2OpConf_& other);
  
      // optional field path
     public:
    bool has_path() const;
    const ::std::string& path() const;
    void clear_path();
    void set_path(const ::std::string& value);
    ::std::string* mutable_path();
   protected:
    bool has_path_ = false;
    ::std::string path_;
      
      // repeated field ref
   public:
    ::std::size_t ref_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ref() const;
    const ::std::string& ref(::std::size_t index) const;
    void clear_ref();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ref();
    ::std::string* mutable_ref(::std::size_t index);
      void add_ref(const ::std::string& value);
    void set_ref(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> ref_;
    
      // repeated field variable_op_name
   public:
    ::std::size_t variable_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
    const ::std::string& variable_op_name(::std::size_t index) const;
    void clear_variable_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
    ::std::string* mutable_variable_op_name(::std::size_t index);
      void add_variable_op_name(const ::std::string& value);
    void set_variable_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> variable_op_name_;
    
      // repeated field original_variable_conf
   public:
    ::std::size_t original_variable_conf_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
    const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
    void clear_original_variable_conf();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
    ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
      ::oneflow::cfg::VariableOpConf* add_original_variable_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> original_variable_conf_;
         
   public:
    int compare(const _ModelLoadV2OpConf_& other);

    bool operator==(const _ModelLoadV2OpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelLoadV2OpConf_& other) const;
  };

  ConstModelLoadV2OpConf(const ::std::shared_ptr<_ModelLoadV2OpConf_>& data);
  ConstModelLoadV2OpConf(const ConstModelLoadV2OpConf&);
  ConstModelLoadV2OpConf(ConstModelLoadV2OpConf&&) noexcept;
  ConstModelLoadV2OpConf();
  ConstModelLoadV2OpConf(const ::oneflow::ModelLoadV2OpConf& proto_modelloadv2opconf);
  virtual ~ConstModelLoadV2OpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelloadv2opconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field path
 public:
  bool has_path() const;
  const ::std::string& path() const;
  // used by pybind11 only
  // repeated field ref
 public:
  ::std::size_t ref_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ref() const;
  const ::std::string& ref(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_ref() const;
  // repeated field variable_op_name
 public:
  ::std::size_t variable_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
  const ::std::string& variable_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_name() const;
  // repeated field original_variable_conf
 public:
  ::std::size_t original_variable_conf_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_const_original_variable_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_original_variable_conf(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstModelLoadV2OpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelLoadV2OpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelLoadV2OpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelLoadV2OpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelLoadV2OpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelLoadV2OpConf
  void BuildFromProto(const PbMessage& proto_modelloadv2opconf);
  
  ::std::shared_ptr<_ModelLoadV2OpConf_> data_;
};

class ModelLoadV2OpConf final : public ConstModelLoadV2OpConf {
 public:
  ModelLoadV2OpConf(const ::std::shared_ptr<_ModelLoadV2OpConf_>& data);
  ModelLoadV2OpConf(const ModelLoadV2OpConf& other);
  // enable nothrow for ::std::vector<ModelLoadV2OpConf> resize 
  ModelLoadV2OpConf(ModelLoadV2OpConf&&) noexcept;
  ModelLoadV2OpConf();
  explicit ModelLoadV2OpConf(const ::oneflow::ModelLoadV2OpConf& proto_modelloadv2opconf);

  ~ModelLoadV2OpConf() override;

  void InitFromProto(const PbMessage& proto_modelloadv2opconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelLoadV2OpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelLoadV2OpConf& other) const;
  void Clear();
  void CopyFrom(const ModelLoadV2OpConf& other);
  ModelLoadV2OpConf& operator=(const ModelLoadV2OpConf& other);

  // required or optional field path
 public:
  void clear_path();
  void set_path(const ::std::string& value);
  ::std::string* mutable_path();
  // repeated field ref
 public:
  void clear_ref();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ref();
  ::std::string* mutable_ref(::std::size_t index);
  void add_ref(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_ref();
  void set_ref(::std::size_t index, const ::std::string& value);
  // repeated field variable_op_name
 public:
  void clear_variable_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
  ::std::string* mutable_variable_op_name(::std::size_t index);
  void add_variable_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_name();
  void set_variable_op_name(::std::size_t index, const ::std::string& value);
  // repeated field original_variable_conf
 public:
  void clear_original_variable_conf();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_mutable_original_variable_conf();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_original_variable_conf(::std::size_t index);
  ::oneflow::cfg::VariableOpConf* add_original_variable_conf();

  ::std::shared_ptr<ModelLoadV2OpConf> __SharedMutable__();
};


class ConstModelSaveV2OpConf : public ::oneflow::cfg::Message {
 public:

  class _ModelSaveV2OpConf_ {
   public:
    _ModelSaveV2OpConf_();
    explicit _ModelSaveV2OpConf_(const _ModelSaveV2OpConf_& other);
    explicit _ModelSaveV2OpConf_(_ModelSaveV2OpConf_&& other);
    _ModelSaveV2OpConf_(const ::oneflow::ModelSaveV2OpConf& proto_modelsavev2opconf);
    ~_ModelSaveV2OpConf_();

    void InitFromProto(const ::oneflow::ModelSaveV2OpConf& proto_modelsavev2opconf);

    void ToProto(::oneflow::ModelSaveV2OpConf* proto_modelsavev2opconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ModelSaveV2OpConf_& other);
  
      // optional field path
     public:
    bool has_path() const;
    const ::std::string& path() const;
    void clear_path();
    void set_path(const ::std::string& value);
    ::std::string* mutable_path();
   protected:
    bool has_path_ = false;
    ::std::string path_;
      
      // repeated field in
   public:
    ::std::size_t in_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
    const ::std::string& in(::std::size_t index) const;
    void clear_in();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
    ::std::string* mutable_in(::std::size_t index);
      void add_in(const ::std::string& value);
    void set_in(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> in_;
    
      // repeated field variable_op_name
   public:
    ::std::size_t variable_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
    const ::std::string& variable_op_name(::std::size_t index) const;
    void clear_variable_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
    ::std::string* mutable_variable_op_name(::std::size_t index);
      void add_variable_op_name(const ::std::string& value);
    void set_variable_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> variable_op_name_;
    
      // repeated field original_variable_conf
   public:
    ::std::size_t original_variable_conf_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
    const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
    void clear_original_variable_conf();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
    ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
      ::oneflow::cfg::VariableOpConf* add_original_variable_conf();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> original_variable_conf_;
         
   public:
    int compare(const _ModelSaveV2OpConf_& other);

    bool operator==(const _ModelSaveV2OpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ModelSaveV2OpConf_& other) const;
  };

  ConstModelSaveV2OpConf(const ::std::shared_ptr<_ModelSaveV2OpConf_>& data);
  ConstModelSaveV2OpConf(const ConstModelSaveV2OpConf&);
  ConstModelSaveV2OpConf(ConstModelSaveV2OpConf&&) noexcept;
  ConstModelSaveV2OpConf();
  ConstModelSaveV2OpConf(const ::oneflow::ModelSaveV2OpConf& proto_modelsavev2opconf);
  virtual ~ConstModelSaveV2OpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_modelsavev2opconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field path
 public:
  bool has_path() const;
  const ::std::string& path() const;
  // used by pybind11 only
  // repeated field in
 public:
  ::std::size_t in_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& in() const;
  const ::std::string& in(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_in() const;
  // repeated field variable_op_name
 public:
  ::std::size_t variable_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& variable_op_name() const;
  const ::std::string& variable_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_variable_op_name() const;
  // repeated field original_variable_conf
 public:
  ::std::size_t original_variable_conf_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& original_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& original_variable_conf(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_const_original_variable_conf() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_original_variable_conf(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstModelSaveV2OpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstModelSaveV2OpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstModelSaveV2OpConf& other) const;
 protected:
  const ::std::shared_ptr<_ModelSaveV2OpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ModelSaveV2OpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstModelSaveV2OpConf
  void BuildFromProto(const PbMessage& proto_modelsavev2opconf);
  
  ::std::shared_ptr<_ModelSaveV2OpConf_> data_;
};

class ModelSaveV2OpConf final : public ConstModelSaveV2OpConf {
 public:
  ModelSaveV2OpConf(const ::std::shared_ptr<_ModelSaveV2OpConf_>& data);
  ModelSaveV2OpConf(const ModelSaveV2OpConf& other);
  // enable nothrow for ::std::vector<ModelSaveV2OpConf> resize 
  ModelSaveV2OpConf(ModelSaveV2OpConf&&) noexcept;
  ModelSaveV2OpConf();
  explicit ModelSaveV2OpConf(const ::oneflow::ModelSaveV2OpConf& proto_modelsavev2opconf);

  ~ModelSaveV2OpConf() override;

  void InitFromProto(const PbMessage& proto_modelsavev2opconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ModelSaveV2OpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ModelSaveV2OpConf& other) const;
  void Clear();
  void CopyFrom(const ModelSaveV2OpConf& other);
  ModelSaveV2OpConf& operator=(const ModelSaveV2OpConf& other);

  // required or optional field path
 public:
  void clear_path();
  void set_path(const ::std::string& value);
  ::std::string* mutable_path();
  // repeated field in
 public:
  void clear_in();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_in();
  ::std::string* mutable_in(::std::size_t index);
  void add_in(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_in();
  void set_in(::std::size_t index, const ::std::string& value);
  // repeated field variable_op_name
 public:
  void clear_variable_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_variable_op_name();
  ::std::string* mutable_variable_op_name(::std::size_t index);
  void add_variable_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_variable_op_name();
  void set_variable_op_name(::std::size_t index, const ::std::string& value);
  // repeated field original_variable_conf
 public:
  void clear_original_variable_conf();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_* mutable_original_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_original_variable_conf(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> shared_mutable_original_variable_conf();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_original_variable_conf(::std::size_t index);
  ::oneflow::cfg::VariableOpConf* add_original_variable_conf();

  ::std::shared_ptr<ModelSaveV2OpConf> __SharedMutable__();
};


class ConstConstantLikeOpConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum scalar_operand
 enum ScalarOperandCase : unsigned int {
  SCALAR_OPERAND_NOT_SET = 0,
    kIntOperand = 4,
    kFloatOperand = 5,
   };

  class _ConstantLikeOpConf_ {
   public:
    _ConstantLikeOpConf_();
    explicit _ConstantLikeOpConf_(const _ConstantLikeOpConf_& other);
    explicit _ConstantLikeOpConf_(_ConstantLikeOpConf_&& other);
    _ConstantLikeOpConf_(const ::oneflow::ConstantLikeOpConf& proto_constantlikeopconf);
    ~_ConstantLikeOpConf_();

    void InitFromProto(const ::oneflow::ConstantLikeOpConf& proto_constantlikeopconf);

    void ToProto(::oneflow::ConstantLikeOpConf* proto_constantlikeopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ConstantLikeOpConf_& other);
  
      // optional field like
     public:
    bool has_like() const;
    const ::std::string& like() const;
    void clear_like();
    void set_like(const ::std::string& value);
    ::std::string* mutable_like();
   protected:
    bool has_like_ = false;
    ::std::string like_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
     // oneof field scalar_operand: int_operand
   public:
    bool has_int_operand() const;
    void clear_int_operand();
    const int64_t& int_operand() const;
      void set_int_operand(const int64_t& value);
    int64_t* mutable_int_operand();
      
     // oneof field scalar_operand: float_operand
   public:
    bool has_float_operand() const;
    void clear_float_operand();
    const double& float_operand() const;
      void set_float_operand(const double& value);
    double* mutable_float_operand();
           
   public:
    // oneof scalar_operand
    ScalarOperandCase scalar_operand_case() const;
    bool has_scalar_operand() const;
   protected:
    void clear_scalar_operand();
    void scalar_operand_copy_from(const _ConstantLikeOpConf_& other);
    union ScalarOperandUnion {
      // 64-bit aligned
      uint64_t __scalar_operand_for_padding_64bit__;
          int64_t int_operand_;
            double float_operand_;
        } scalar_operand_;
    ScalarOperandCase scalar_operand_case_ = SCALAR_OPERAND_NOT_SET;
     
   public:
    int compare(const _ConstantLikeOpConf_& other);

    bool operator==(const _ConstantLikeOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ConstantLikeOpConf_& other) const;
  };

  ConstConstantLikeOpConf(const ::std::shared_ptr<_ConstantLikeOpConf_>& data);
  ConstConstantLikeOpConf(const ConstConstantLikeOpConf&);
  ConstConstantLikeOpConf(ConstConstantLikeOpConf&&) noexcept;
  ConstConstantLikeOpConf();
  ConstConstantLikeOpConf(const ::oneflow::ConstantLikeOpConf& proto_constantlikeopconf);
  virtual ~ConstConstantLikeOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_constantlikeopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field like
 public:
  bool has_like() const;
  const ::std::string& like() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
 // oneof field scalar_operand: int_operand
 public:
  bool has_int_operand() const;
  const int64_t& int_operand() const;
  // used by pybind11 only
 // oneof field scalar_operand: float_operand
 public:
  bool has_float_operand() const;
  const double& float_operand() const;
  // used by pybind11 only
 public:
  ScalarOperandCase scalar_operand_case() const;
 protected:
  bool has_scalar_operand() const;

 public:
  ::std::shared_ptr<ConstConstantLikeOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstConstantLikeOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstConstantLikeOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ConstantLikeOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ConstantLikeOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstConstantLikeOpConf
  void BuildFromProto(const PbMessage& proto_constantlikeopconf);
  
  ::std::shared_ptr<_ConstantLikeOpConf_> data_;
};

class ConstantLikeOpConf final : public ConstConstantLikeOpConf {
 public:
  ConstantLikeOpConf(const ::std::shared_ptr<_ConstantLikeOpConf_>& data);
  ConstantLikeOpConf(const ConstantLikeOpConf& other);
  // enable nothrow for ::std::vector<ConstantLikeOpConf> resize 
  ConstantLikeOpConf(ConstantLikeOpConf&&) noexcept;
  ConstantLikeOpConf();
  explicit ConstantLikeOpConf(const ::oneflow::ConstantLikeOpConf& proto_constantlikeopconf);

  ~ConstantLikeOpConf() override;

  void InitFromProto(const PbMessage& proto_constantlikeopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ConstantLikeOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ConstantLikeOpConf& other) const;
  void Clear();
  void CopyFrom(const ConstantLikeOpConf& other);
  ConstantLikeOpConf& operator=(const ConstantLikeOpConf& other);

  // required or optional field like
 public:
  void clear_like();
  void set_like(const ::std::string& value);
  ::std::string* mutable_like();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  void clear_int_operand();
  void set_int_operand(const int64_t& value);
  int64_t* mutable_int_operand();
  void clear_float_operand();
  void set_float_operand(const double& value);
  double* mutable_float_operand();

  ::std::shared_ptr<ConstantLikeOpConf> __SharedMutable__();
};


class ConstSyncDynamicResizeOpConf : public ::oneflow::cfg::Message {
 public:

  class _SyncDynamicResizeOpConf_ {
   public:
    _SyncDynamicResizeOpConf_();
    explicit _SyncDynamicResizeOpConf_(const _SyncDynamicResizeOpConf_& other);
    explicit _SyncDynamicResizeOpConf_(_SyncDynamicResizeOpConf_&& other);
    _SyncDynamicResizeOpConf_(const ::oneflow::SyncDynamicResizeOpConf& proto_syncdynamicresizeopconf);
    ~_SyncDynamicResizeOpConf_();

    void InitFromProto(const ::oneflow::SyncDynamicResizeOpConf& proto_syncdynamicresizeopconf);

    void ToProto(::oneflow::SyncDynamicResizeOpConf* proto_syncdynamicresizeopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _SyncDynamicResizeOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field size
     public:
    bool has_size() const;
    const ::std::string& size() const;
    void clear_size();
    void set_size(const ::std::string& value);
    ::std::string* mutable_size();
   protected:
    bool has_size_ = false;
    ::std::string size_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field axis
     public:
    bool has_axis() const;
    const int64_t& axis() const;
    void clear_axis();
    void set_axis(const int64_t& value);
    int64_t* mutable_axis();
   protected:
    bool has_axis_ = false;
    int64_t axis_;
      
      // optional field eager
     public:
    bool has_eager() const;
    const bool& eager() const;
    void clear_eager();
    void set_eager(const bool& value);
    bool* mutable_eager();
   protected:
    bool has_eager_ = false;
    bool eager_;
           
   public:
    int compare(const _SyncDynamicResizeOpConf_& other);

    bool operator==(const _SyncDynamicResizeOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _SyncDynamicResizeOpConf_& other) const;
  };

  ConstSyncDynamicResizeOpConf(const ::std::shared_ptr<_SyncDynamicResizeOpConf_>& data);
  ConstSyncDynamicResizeOpConf(const ConstSyncDynamicResizeOpConf&);
  ConstSyncDynamicResizeOpConf(ConstSyncDynamicResizeOpConf&&) noexcept;
  ConstSyncDynamicResizeOpConf();
  ConstSyncDynamicResizeOpConf(const ::oneflow::SyncDynamicResizeOpConf& proto_syncdynamicresizeopconf);
  virtual ~ConstSyncDynamicResizeOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_syncdynamicresizeopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field size
 public:
  bool has_size() const;
  const ::std::string& size() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field axis
 public:
  bool has_axis() const;
  const int64_t& axis() const;
  // used by pybind11 only
  // required or optional field eager
 public:
  bool has_eager() const;
  const bool& eager() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstSyncDynamicResizeOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstSyncDynamicResizeOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstSyncDynamicResizeOpConf& other) const;
 protected:
  const ::std::shared_ptr<_SyncDynamicResizeOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_SyncDynamicResizeOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstSyncDynamicResizeOpConf
  void BuildFromProto(const PbMessage& proto_syncdynamicresizeopconf);
  
  ::std::shared_ptr<_SyncDynamicResizeOpConf_> data_;
};

class SyncDynamicResizeOpConf final : public ConstSyncDynamicResizeOpConf {
 public:
  SyncDynamicResizeOpConf(const ::std::shared_ptr<_SyncDynamicResizeOpConf_>& data);
  SyncDynamicResizeOpConf(const SyncDynamicResizeOpConf& other);
  // enable nothrow for ::std::vector<SyncDynamicResizeOpConf> resize 
  SyncDynamicResizeOpConf(SyncDynamicResizeOpConf&&) noexcept;
  SyncDynamicResizeOpConf();
  explicit SyncDynamicResizeOpConf(const ::oneflow::SyncDynamicResizeOpConf& proto_syncdynamicresizeopconf);

  ~SyncDynamicResizeOpConf() override;

  void InitFromProto(const PbMessage& proto_syncdynamicresizeopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const SyncDynamicResizeOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const SyncDynamicResizeOpConf& other) const;
  void Clear();
  void CopyFrom(const SyncDynamicResizeOpConf& other);
  SyncDynamicResizeOpConf& operator=(const SyncDynamicResizeOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field size
 public:
  void clear_size();
  void set_size(const ::std::string& value);
  ::std::string* mutable_size();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field axis
 public:
  void clear_axis();
  void set_axis(const int64_t& value);
  int64_t* mutable_axis();
  // required or optional field eager
 public:
  void clear_eager();
  void set_eager(const bool& value);
  bool* mutable_eager();

  ::std::shared_ptr<SyncDynamicResizeOpConf> __SharedMutable__();
};


class ConstBroadcastToCompatibleWithOpConf : public ::oneflow::cfg::Message {
 public:

  class _BroadcastToCompatibleWithOpConf_ {
   public:
    _BroadcastToCompatibleWithOpConf_();
    explicit _BroadcastToCompatibleWithOpConf_(const _BroadcastToCompatibleWithOpConf_& other);
    explicit _BroadcastToCompatibleWithOpConf_(_BroadcastToCompatibleWithOpConf_&& other);
    _BroadcastToCompatibleWithOpConf_(const ::oneflow::BroadcastToCompatibleWithOpConf& proto_broadcasttocompatiblewithopconf);
    ~_BroadcastToCompatibleWithOpConf_();

    void InitFromProto(const ::oneflow::BroadcastToCompatibleWithOpConf& proto_broadcasttocompatiblewithopconf);

    void ToProto(::oneflow::BroadcastToCompatibleWithOpConf* proto_broadcasttocompatiblewithopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BroadcastToCompatibleWithOpConf_& other);
  
      // optional field x
     public:
    bool has_x() const;
    const ::std::string& x() const;
    void clear_x();
    void set_x(const ::std::string& value);
    ::std::string* mutable_x();
   protected:
    bool has_x_ = false;
    ::std::string x_;
      
      // repeated field compatible
   public:
    ::std::size_t compatible_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& compatible() const;
    const ::std::string& compatible(::std::size_t index) const;
    void clear_compatible();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_compatible();
    ::std::string* mutable_compatible(::std::size_t index);
      void add_compatible(const ::std::string& value);
    void set_compatible(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> compatible_;
    
      // optional field y
     public:
    bool has_y() const;
    const ::std::string& y() const;
    void clear_y();
    void set_y(const ::std::string& value);
    ::std::string* mutable_y();
   protected:
    bool has_y_ = false;
    ::std::string y_;
           
   public:
    int compare(const _BroadcastToCompatibleWithOpConf_& other);

    bool operator==(const _BroadcastToCompatibleWithOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BroadcastToCompatibleWithOpConf_& other) const;
  };

  ConstBroadcastToCompatibleWithOpConf(const ::std::shared_ptr<_BroadcastToCompatibleWithOpConf_>& data);
  ConstBroadcastToCompatibleWithOpConf(const ConstBroadcastToCompatibleWithOpConf&);
  ConstBroadcastToCompatibleWithOpConf(ConstBroadcastToCompatibleWithOpConf&&) noexcept;
  ConstBroadcastToCompatibleWithOpConf();
  ConstBroadcastToCompatibleWithOpConf(const ::oneflow::BroadcastToCompatibleWithOpConf& proto_broadcasttocompatiblewithopconf);
  virtual ~ConstBroadcastToCompatibleWithOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_broadcasttocompatiblewithopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field x
 public:
  bool has_x() const;
  const ::std::string& x() const;
  // used by pybind11 only
  // repeated field compatible
 public:
  ::std::size_t compatible_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& compatible() const;
  const ::std::string& compatible(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_compatible() const;
  // required or optional field y
 public:
  bool has_y() const;
  const ::std::string& y() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstBroadcastToCompatibleWithOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBroadcastToCompatibleWithOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBroadcastToCompatibleWithOpConf& other) const;
 protected:
  const ::std::shared_ptr<_BroadcastToCompatibleWithOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BroadcastToCompatibleWithOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBroadcastToCompatibleWithOpConf
  void BuildFromProto(const PbMessage& proto_broadcasttocompatiblewithopconf);
  
  ::std::shared_ptr<_BroadcastToCompatibleWithOpConf_> data_;
};

class BroadcastToCompatibleWithOpConf final : public ConstBroadcastToCompatibleWithOpConf {
 public:
  BroadcastToCompatibleWithOpConf(const ::std::shared_ptr<_BroadcastToCompatibleWithOpConf_>& data);
  BroadcastToCompatibleWithOpConf(const BroadcastToCompatibleWithOpConf& other);
  // enable nothrow for ::std::vector<BroadcastToCompatibleWithOpConf> resize 
  BroadcastToCompatibleWithOpConf(BroadcastToCompatibleWithOpConf&&) noexcept;
  BroadcastToCompatibleWithOpConf();
  explicit BroadcastToCompatibleWithOpConf(const ::oneflow::BroadcastToCompatibleWithOpConf& proto_broadcasttocompatiblewithopconf);

  ~BroadcastToCompatibleWithOpConf() override;

  void InitFromProto(const PbMessage& proto_broadcasttocompatiblewithopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BroadcastToCompatibleWithOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BroadcastToCompatibleWithOpConf& other) const;
  void Clear();
  void CopyFrom(const BroadcastToCompatibleWithOpConf& other);
  BroadcastToCompatibleWithOpConf& operator=(const BroadcastToCompatibleWithOpConf& other);

  // required or optional field x
 public:
  void clear_x();
  void set_x(const ::std::string& value);
  ::std::string* mutable_x();
  // repeated field compatible
 public:
  void clear_compatible();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_compatible();
  ::std::string* mutable_compatible(::std::size_t index);
  void add_compatible(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_compatible();
  void set_compatible(::std::size_t index, const ::std::string& value);
  // required or optional field y
 public:
  void clear_y();
  void set_y(const ::std::string& value);
  ::std::string* mutable_y();

  ::std::shared_ptr<BroadcastToCompatibleWithOpConf> __SharedMutable__();
};


class ConstCollectiveBoxingGenericOpConf : public ::oneflow::cfg::Message {
 public:

  class _CollectiveBoxingGenericOpConf_ {
   public:
    _CollectiveBoxingGenericOpConf_();
    explicit _CollectiveBoxingGenericOpConf_(const _CollectiveBoxingGenericOpConf_& other);
    explicit _CollectiveBoxingGenericOpConf_(_CollectiveBoxingGenericOpConf_&& other);
    _CollectiveBoxingGenericOpConf_(const ::oneflow::CollectiveBoxingGenericOpConf& proto_collectiveboxinggenericopconf);
    ~_CollectiveBoxingGenericOpConf_();

    void InitFromProto(const ::oneflow::CollectiveBoxingGenericOpConf& proto_collectiveboxinggenericopconf);

    void ToProto(::oneflow::CollectiveBoxingGenericOpConf* proto_collectiveboxinggenericopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CollectiveBoxingGenericOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field rank_desc
     public:
    bool has_rank_desc() const;
    const ::oneflow::boxing::collective::cfg::RankDesc& rank_desc() const;
    void clear_rank_desc();
    ::oneflow::boxing::collective::cfg::RankDesc* mutable_rank_desc();
   protected:
    bool has_rank_desc_ = false;
    ::std::shared_ptr<::oneflow::boxing::collective::cfg::RankDesc> rank_desc_;
           
   public:
    int compare(const _CollectiveBoxingGenericOpConf_& other);

    bool operator==(const _CollectiveBoxingGenericOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CollectiveBoxingGenericOpConf_& other) const;
  };

  ConstCollectiveBoxingGenericOpConf(const ::std::shared_ptr<_CollectiveBoxingGenericOpConf_>& data);
  ConstCollectiveBoxingGenericOpConf(const ConstCollectiveBoxingGenericOpConf&);
  ConstCollectiveBoxingGenericOpConf(ConstCollectiveBoxingGenericOpConf&&) noexcept;
  ConstCollectiveBoxingGenericOpConf();
  ConstCollectiveBoxingGenericOpConf(const ::oneflow::CollectiveBoxingGenericOpConf& proto_collectiveboxinggenericopconf);
  virtual ~ConstCollectiveBoxingGenericOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_collectiveboxinggenericopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field rank_desc
 public:
  bool has_rank_desc() const;
  const ::oneflow::boxing::collective::cfg::RankDesc& rank_desc() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::ConstRankDesc> shared_const_rank_desc() const;

 public:
  ::std::shared_ptr<ConstCollectiveBoxingGenericOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCollectiveBoxingGenericOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCollectiveBoxingGenericOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CollectiveBoxingGenericOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CollectiveBoxingGenericOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCollectiveBoxingGenericOpConf
  void BuildFromProto(const PbMessage& proto_collectiveboxinggenericopconf);
  
  ::std::shared_ptr<_CollectiveBoxingGenericOpConf_> data_;
};

class CollectiveBoxingGenericOpConf final : public ConstCollectiveBoxingGenericOpConf {
 public:
  CollectiveBoxingGenericOpConf(const ::std::shared_ptr<_CollectiveBoxingGenericOpConf_>& data);
  CollectiveBoxingGenericOpConf(const CollectiveBoxingGenericOpConf& other);
  // enable nothrow for ::std::vector<CollectiveBoxingGenericOpConf> resize 
  CollectiveBoxingGenericOpConf(CollectiveBoxingGenericOpConf&&) noexcept;
  CollectiveBoxingGenericOpConf();
  explicit CollectiveBoxingGenericOpConf(const ::oneflow::CollectiveBoxingGenericOpConf& proto_collectiveboxinggenericopconf);

  ~CollectiveBoxingGenericOpConf() override;

  void InitFromProto(const PbMessage& proto_collectiveboxinggenericopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CollectiveBoxingGenericOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CollectiveBoxingGenericOpConf& other) const;
  void Clear();
  void CopyFrom(const CollectiveBoxingGenericOpConf& other);
  CollectiveBoxingGenericOpConf& operator=(const CollectiveBoxingGenericOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field rank_desc
 public:
  void clear_rank_desc();
  ::oneflow::boxing::collective::cfg::RankDesc* mutable_rank_desc();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::boxing::collective::cfg::RankDesc> shared_mutable_rank_desc();

  ::std::shared_ptr<CollectiveBoxingGenericOpConf> __SharedMutable__();
};


class ConstBoxingIdentityOpConf : public ::oneflow::cfg::Message {
 public:

  class _BoxingIdentityOpConf_ {
   public:
    _BoxingIdentityOpConf_();
    explicit _BoxingIdentityOpConf_(const _BoxingIdentityOpConf_& other);
    explicit _BoxingIdentityOpConf_(_BoxingIdentityOpConf_&& other);
    _BoxingIdentityOpConf_(const ::oneflow::BoxingIdentityOpConf& proto_boxingidentityopconf);
    ~_BoxingIdentityOpConf_();

    void InitFromProto(const ::oneflow::BoxingIdentityOpConf& proto_boxingidentityopconf);

    void ToProto(::oneflow::BoxingIdentityOpConf* proto_boxingidentityopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxingIdentityOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
           
   public:
    int compare(const _BoxingIdentityOpConf_& other);

    bool operator==(const _BoxingIdentityOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxingIdentityOpConf_& other) const;
  };

  ConstBoxingIdentityOpConf(const ::std::shared_ptr<_BoxingIdentityOpConf_>& data);
  ConstBoxingIdentityOpConf(const ConstBoxingIdentityOpConf&);
  ConstBoxingIdentityOpConf(ConstBoxingIdentityOpConf&&) noexcept;
  ConstBoxingIdentityOpConf();
  ConstBoxingIdentityOpConf(const ::oneflow::BoxingIdentityOpConf& proto_boxingidentityopconf);
  virtual ~ConstBoxingIdentityOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxingidentityopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;

 public:
  ::std::shared_ptr<ConstBoxingIdentityOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxingIdentityOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxingIdentityOpConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxingIdentityOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxingIdentityOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxingIdentityOpConf
  void BuildFromProto(const PbMessage& proto_boxingidentityopconf);
  
  ::std::shared_ptr<_BoxingIdentityOpConf_> data_;
};

class BoxingIdentityOpConf final : public ConstBoxingIdentityOpConf {
 public:
  BoxingIdentityOpConf(const ::std::shared_ptr<_BoxingIdentityOpConf_>& data);
  BoxingIdentityOpConf(const BoxingIdentityOpConf& other);
  // enable nothrow for ::std::vector<BoxingIdentityOpConf> resize 
  BoxingIdentityOpConf(BoxingIdentityOpConf&&) noexcept;
  BoxingIdentityOpConf();
  explicit BoxingIdentityOpConf(const ::oneflow::BoxingIdentityOpConf& proto_boxingidentityopconf);

  ~BoxingIdentityOpConf() override;

  void InitFromProto(const PbMessage& proto_boxingidentityopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxingIdentityOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxingIdentityOpConf& other) const;
  void Clear();
  void CopyFrom(const BoxingIdentityOpConf& other);
  BoxingIdentityOpConf& operator=(const BoxingIdentityOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();

  ::std::shared_ptr<BoxingIdentityOpConf> __SharedMutable__();
};


class ConstCollectiveBoxingPackOpConf : public ::oneflow::cfg::Message {
 public:

  class _CollectiveBoxingPackOpConf_ {
   public:
    _CollectiveBoxingPackOpConf_();
    explicit _CollectiveBoxingPackOpConf_(const _CollectiveBoxingPackOpConf_& other);
    explicit _CollectiveBoxingPackOpConf_(_CollectiveBoxingPackOpConf_&& other);
    _CollectiveBoxingPackOpConf_(const ::oneflow::CollectiveBoxingPackOpConf& proto_collectiveboxingpackopconf);
    ~_CollectiveBoxingPackOpConf_();

    void InitFromProto(const ::oneflow::CollectiveBoxingPackOpConf& proto_collectiveboxingpackopconf);

    void ToProto(::oneflow::CollectiveBoxingPackOpConf* proto_collectiveboxingpackopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CollectiveBoxingPackOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field src_sbp_parallel
     public:
    bool has_src_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& src_sbp_parallel() const;
    void clear_src_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_src_sbp_parallel();
   protected:
    bool has_src_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> src_sbp_parallel_;
      
      // optional field dst_sbp_parallel
     public:
    bool has_dst_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& dst_sbp_parallel() const;
    void clear_dst_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_dst_sbp_parallel();
   protected:
    bool has_dst_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> dst_sbp_parallel_;
      
      // optional field num_ranks
     public:
    bool has_num_ranks() const;
    const int64_t& num_ranks() const;
    void clear_num_ranks();
    void set_num_ranks(const int64_t& value);
    int64_t* mutable_num_ranks();
   protected:
    bool has_num_ranks_ = false;
    int64_t num_ranks_;
      
      // optional field logical_shape
     public:
    bool has_logical_shape() const;
    const ::oneflow::cfg::ShapeProto& logical_shape() const;
    void clear_logical_shape();
    ::oneflow::cfg::ShapeProto* mutable_logical_shape();
   protected:
    bool has_logical_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> logical_shape_;
           
   public:
    int compare(const _CollectiveBoxingPackOpConf_& other);

    bool operator==(const _CollectiveBoxingPackOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CollectiveBoxingPackOpConf_& other) const;
  };

  ConstCollectiveBoxingPackOpConf(const ::std::shared_ptr<_CollectiveBoxingPackOpConf_>& data);
  ConstCollectiveBoxingPackOpConf(const ConstCollectiveBoxingPackOpConf&);
  ConstCollectiveBoxingPackOpConf(ConstCollectiveBoxingPackOpConf&&) noexcept;
  ConstCollectiveBoxingPackOpConf();
  ConstCollectiveBoxingPackOpConf(const ::oneflow::CollectiveBoxingPackOpConf& proto_collectiveboxingpackopconf);
  virtual ~ConstCollectiveBoxingPackOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_collectiveboxingpackopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field src_sbp_parallel
 public:
  bool has_src_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& src_sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_src_sbp_parallel() const;
  // required or optional field dst_sbp_parallel
 public:
  bool has_dst_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& dst_sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_dst_sbp_parallel() const;
  // required or optional field num_ranks
 public:
  bool has_num_ranks() const;
  const int64_t& num_ranks() const;
  // used by pybind11 only
  // required or optional field logical_shape
 public:
  bool has_logical_shape() const;
  const ::oneflow::cfg::ShapeProto& logical_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_logical_shape() const;

 public:
  ::std::shared_ptr<ConstCollectiveBoxingPackOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCollectiveBoxingPackOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCollectiveBoxingPackOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CollectiveBoxingPackOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CollectiveBoxingPackOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCollectiveBoxingPackOpConf
  void BuildFromProto(const PbMessage& proto_collectiveboxingpackopconf);
  
  ::std::shared_ptr<_CollectiveBoxingPackOpConf_> data_;
};

class CollectiveBoxingPackOpConf final : public ConstCollectiveBoxingPackOpConf {
 public:
  CollectiveBoxingPackOpConf(const ::std::shared_ptr<_CollectiveBoxingPackOpConf_>& data);
  CollectiveBoxingPackOpConf(const CollectiveBoxingPackOpConf& other);
  // enable nothrow for ::std::vector<CollectiveBoxingPackOpConf> resize 
  CollectiveBoxingPackOpConf(CollectiveBoxingPackOpConf&&) noexcept;
  CollectiveBoxingPackOpConf();
  explicit CollectiveBoxingPackOpConf(const ::oneflow::CollectiveBoxingPackOpConf& proto_collectiveboxingpackopconf);

  ~CollectiveBoxingPackOpConf() override;

  void InitFromProto(const PbMessage& proto_collectiveboxingpackopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CollectiveBoxingPackOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CollectiveBoxingPackOpConf& other) const;
  void Clear();
  void CopyFrom(const CollectiveBoxingPackOpConf& other);
  CollectiveBoxingPackOpConf& operator=(const CollectiveBoxingPackOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field src_sbp_parallel
 public:
  void clear_src_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_src_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_src_sbp_parallel();
  // required or optional field dst_sbp_parallel
 public:
  void clear_dst_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_dst_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_dst_sbp_parallel();
  // required or optional field num_ranks
 public:
  void clear_num_ranks();
  void set_num_ranks(const int64_t& value);
  int64_t* mutable_num_ranks();
  // required or optional field logical_shape
 public:
  void clear_logical_shape();
  ::oneflow::cfg::ShapeProto* mutable_logical_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_logical_shape();

  ::std::shared_ptr<CollectiveBoxingPackOpConf> __SharedMutable__();
};


class ConstCollectiveBoxingUnpackOpConf : public ::oneflow::cfg::Message {
 public:

  class _CollectiveBoxingUnpackOpConf_ {
   public:
    _CollectiveBoxingUnpackOpConf_();
    explicit _CollectiveBoxingUnpackOpConf_(const _CollectiveBoxingUnpackOpConf_& other);
    explicit _CollectiveBoxingUnpackOpConf_(_CollectiveBoxingUnpackOpConf_&& other);
    _CollectiveBoxingUnpackOpConf_(const ::oneflow::CollectiveBoxingUnpackOpConf& proto_collectiveboxingunpackopconf);
    ~_CollectiveBoxingUnpackOpConf_();

    void InitFromProto(const ::oneflow::CollectiveBoxingUnpackOpConf& proto_collectiveboxingunpackopconf);

    void ToProto(::oneflow::CollectiveBoxingUnpackOpConf* proto_collectiveboxingunpackopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _CollectiveBoxingUnpackOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field src_sbp_parallel
     public:
    bool has_src_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& src_sbp_parallel() const;
    void clear_src_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_src_sbp_parallel();
   protected:
    bool has_src_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> src_sbp_parallel_;
      
      // optional field dst_sbp_parallel
     public:
    bool has_dst_sbp_parallel() const;
    const ::oneflow::cfg::SbpParallel& dst_sbp_parallel() const;
    void clear_dst_sbp_parallel();
    ::oneflow::cfg::SbpParallel* mutable_dst_sbp_parallel();
   protected:
    bool has_dst_sbp_parallel_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpParallel> dst_sbp_parallel_;
      
      // optional field num_ranks
     public:
    bool has_num_ranks() const;
    const int64_t& num_ranks() const;
    void clear_num_ranks();
    void set_num_ranks(const int64_t& value);
    int64_t* mutable_num_ranks();
   protected:
    bool has_num_ranks_ = false;
    int64_t num_ranks_;
      
      // optional field logical_shape
     public:
    bool has_logical_shape() const;
    const ::oneflow::cfg::ShapeProto& logical_shape() const;
    void clear_logical_shape();
    ::oneflow::cfg::ShapeProto* mutable_logical_shape();
   protected:
    bool has_logical_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> logical_shape_;
           
   public:
    int compare(const _CollectiveBoxingUnpackOpConf_& other);

    bool operator==(const _CollectiveBoxingUnpackOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _CollectiveBoxingUnpackOpConf_& other) const;
  };

  ConstCollectiveBoxingUnpackOpConf(const ::std::shared_ptr<_CollectiveBoxingUnpackOpConf_>& data);
  ConstCollectiveBoxingUnpackOpConf(const ConstCollectiveBoxingUnpackOpConf&);
  ConstCollectiveBoxingUnpackOpConf(ConstCollectiveBoxingUnpackOpConf&&) noexcept;
  ConstCollectiveBoxingUnpackOpConf();
  ConstCollectiveBoxingUnpackOpConf(const ::oneflow::CollectiveBoxingUnpackOpConf& proto_collectiveboxingunpackopconf);
  virtual ~ConstCollectiveBoxingUnpackOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_collectiveboxingunpackopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field src_sbp_parallel
 public:
  bool has_src_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& src_sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_src_sbp_parallel() const;
  // required or optional field dst_sbp_parallel
 public:
  bool has_dst_sbp_parallel() const;
  const ::oneflow::cfg::SbpParallel& dst_sbp_parallel() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpParallel> shared_const_dst_sbp_parallel() const;
  // required or optional field num_ranks
 public:
  bool has_num_ranks() const;
  const int64_t& num_ranks() const;
  // used by pybind11 only
  // required or optional field logical_shape
 public:
  bool has_logical_shape() const;
  const ::oneflow::cfg::ShapeProto& logical_shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_logical_shape() const;

 public:
  ::std::shared_ptr<ConstCollectiveBoxingUnpackOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstCollectiveBoxingUnpackOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstCollectiveBoxingUnpackOpConf& other) const;
 protected:
  const ::std::shared_ptr<_CollectiveBoxingUnpackOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_CollectiveBoxingUnpackOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstCollectiveBoxingUnpackOpConf
  void BuildFromProto(const PbMessage& proto_collectiveboxingunpackopconf);
  
  ::std::shared_ptr<_CollectiveBoxingUnpackOpConf_> data_;
};

class CollectiveBoxingUnpackOpConf final : public ConstCollectiveBoxingUnpackOpConf {
 public:
  CollectiveBoxingUnpackOpConf(const ::std::shared_ptr<_CollectiveBoxingUnpackOpConf_>& data);
  CollectiveBoxingUnpackOpConf(const CollectiveBoxingUnpackOpConf& other);
  // enable nothrow for ::std::vector<CollectiveBoxingUnpackOpConf> resize 
  CollectiveBoxingUnpackOpConf(CollectiveBoxingUnpackOpConf&&) noexcept;
  CollectiveBoxingUnpackOpConf();
  explicit CollectiveBoxingUnpackOpConf(const ::oneflow::CollectiveBoxingUnpackOpConf& proto_collectiveboxingunpackopconf);

  ~CollectiveBoxingUnpackOpConf() override;

  void InitFromProto(const PbMessage& proto_collectiveboxingunpackopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const CollectiveBoxingUnpackOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const CollectiveBoxingUnpackOpConf& other) const;
  void Clear();
  void CopyFrom(const CollectiveBoxingUnpackOpConf& other);
  CollectiveBoxingUnpackOpConf& operator=(const CollectiveBoxingUnpackOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field src_sbp_parallel
 public:
  void clear_src_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_src_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_src_sbp_parallel();
  // required or optional field dst_sbp_parallel
 public:
  void clear_dst_sbp_parallel();
  ::oneflow::cfg::SbpParallel* mutable_dst_sbp_parallel();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpParallel> shared_mutable_dst_sbp_parallel();
  // required or optional field num_ranks
 public:
  void clear_num_ranks();
  void set_num_ranks(const int64_t& value);
  int64_t* mutable_num_ranks();
  // required or optional field logical_shape
 public:
  void clear_logical_shape();
  ::oneflow::cfg::ShapeProto* mutable_logical_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_logical_shape();

  ::std::shared_ptr<CollectiveBoxingUnpackOpConf> __SharedMutable__();
};


class ConstImageDecoderRandomCropResizeOpConf : public ::oneflow::cfg::Message {
 public:

  class _ImageDecoderRandomCropResizeOpConf_ {
   public:
    _ImageDecoderRandomCropResizeOpConf_();
    explicit _ImageDecoderRandomCropResizeOpConf_(const _ImageDecoderRandomCropResizeOpConf_& other);
    explicit _ImageDecoderRandomCropResizeOpConf_(_ImageDecoderRandomCropResizeOpConf_&& other);
    _ImageDecoderRandomCropResizeOpConf_(const ::oneflow::ImageDecoderRandomCropResizeOpConf& proto_imagedecoderrandomcropresizeopconf);
    ~_ImageDecoderRandomCropResizeOpConf_();

    void InitFromProto(const ::oneflow::ImageDecoderRandomCropResizeOpConf& proto_imagedecoderrandomcropresizeopconf);

    void ToProto(::oneflow::ImageDecoderRandomCropResizeOpConf* proto_imagedecoderrandomcropresizeopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ImageDecoderRandomCropResizeOpConf_& other);
  
      // optional field in
     public:
    bool has_in() const;
    const ::std::string& in() const;
    void clear_in();
    void set_in(const ::std::string& value);
    ::std::string* mutable_in();
   protected:
    bool has_in_ = false;
    ::std::string in_;
      
      // optional field out
     public:
    bool has_out() const;
    const ::std::string& out() const;
    void clear_out();
    void set_out(const ::std::string& value);
    ::std::string* mutable_out();
   protected:
    bool has_out_ = false;
    ::std::string out_;
      
      // optional field target_width
     public:
    bool has_target_width() const;
    const int64_t& target_width() const;
    void clear_target_width();
    void set_target_width(const int64_t& value);
    int64_t* mutable_target_width();
   protected:
    bool has_target_width_ = false;
    int64_t target_width_;
      
      // optional field target_height
     public:
    bool has_target_height() const;
    const int64_t& target_height() const;
    void clear_target_height();
    void set_target_height(const int64_t& value);
    int64_t* mutable_target_height();
   protected:
    bool has_target_height_ = false;
    int64_t target_height_;
      
      // optional field num_workers
     public:
    bool has_num_workers() const;
    const int64_t& num_workers() const;
    void clear_num_workers();
    void set_num_workers(const int64_t& value);
    int64_t* mutable_num_workers();
   protected:
    bool has_num_workers_ = false;
    int64_t num_workers_;
      
      // optional field max_num_pixels
     public:
    bool has_max_num_pixels() const;
    const int64_t& max_num_pixels() const;
    void clear_max_num_pixels();
    void set_max_num_pixels(const int64_t& value);
    int64_t* mutable_max_num_pixels();
   protected:
    bool has_max_num_pixels_ = false;
    int64_t max_num_pixels_;
      
      // optional field warmup_size
     public:
    bool has_warmup_size() const;
    const int64_t& warmup_size() const;
    void clear_warmup_size();
    void set_warmup_size(const int64_t& value);
    int64_t* mutable_warmup_size();
   protected:
    bool has_warmup_size_ = false;
    int64_t warmup_size_;
      
      // optional field seed
     public:
    bool has_seed() const;
    const int64_t& seed() const;
    void clear_seed();
    void set_seed(const int64_t& value);
    int64_t* mutable_seed();
   protected:
    bool has_seed_ = false;
    int64_t seed_;
      
      // optional field num_attempts
     public:
    bool has_num_attempts() const;
    const int64_t& num_attempts() const;
    void clear_num_attempts();
    void set_num_attempts(const int64_t& value);
    int64_t* mutable_num_attempts();
   protected:
    bool has_num_attempts_ = false;
    int64_t num_attempts_;
      
      // optional field random_area_min
     public:
    bool has_random_area_min() const;
    const float& random_area_min() const;
    void clear_random_area_min();
    void set_random_area_min(const float& value);
    float* mutable_random_area_min();
   protected:
    bool has_random_area_min_ = false;
    float random_area_min_;
      
      // optional field random_area_max
     public:
    bool has_random_area_max() const;
    const float& random_area_max() const;
    void clear_random_area_max();
    void set_random_area_max(const float& value);
    float* mutable_random_area_max();
   protected:
    bool has_random_area_max_ = false;
    float random_area_max_;
      
      // optional field random_aspect_ratio_min
     public:
    bool has_random_aspect_ratio_min() const;
    const float& random_aspect_ratio_min() const;
    void clear_random_aspect_ratio_min();
    void set_random_aspect_ratio_min(const float& value);
    float* mutable_random_aspect_ratio_min();
   protected:
    bool has_random_aspect_ratio_min_ = false;
    float random_aspect_ratio_min_;
      
      // optional field random_aspect_ratio_max
     public:
    bool has_random_aspect_ratio_max() const;
    const float& random_aspect_ratio_max() const;
    void clear_random_aspect_ratio_max();
    void set_random_aspect_ratio_max(const float& value);
    float* mutable_random_aspect_ratio_max();
   protected:
    bool has_random_aspect_ratio_max_ = false;
    float random_aspect_ratio_max_;
           
   public:
    int compare(const _ImageDecoderRandomCropResizeOpConf_& other);

    bool operator==(const _ImageDecoderRandomCropResizeOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ImageDecoderRandomCropResizeOpConf_& other) const;
  };

  ConstImageDecoderRandomCropResizeOpConf(const ::std::shared_ptr<_ImageDecoderRandomCropResizeOpConf_>& data);
  ConstImageDecoderRandomCropResizeOpConf(const ConstImageDecoderRandomCropResizeOpConf&);
  ConstImageDecoderRandomCropResizeOpConf(ConstImageDecoderRandomCropResizeOpConf&&) noexcept;
  ConstImageDecoderRandomCropResizeOpConf();
  ConstImageDecoderRandomCropResizeOpConf(const ::oneflow::ImageDecoderRandomCropResizeOpConf& proto_imagedecoderrandomcropresizeopconf);
  virtual ~ConstImageDecoderRandomCropResizeOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_imagedecoderrandomcropresizeopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field in
 public:
  bool has_in() const;
  const ::std::string& in() const;
  // used by pybind11 only
  // required or optional field out
 public:
  bool has_out() const;
  const ::std::string& out() const;
  // used by pybind11 only
  // required or optional field target_width
 public:
  bool has_target_width() const;
  const int64_t& target_width() const;
  // used by pybind11 only
  // required or optional field target_height
 public:
  bool has_target_height() const;
  const int64_t& target_height() const;
  // used by pybind11 only
  // required or optional field num_workers
 public:
  bool has_num_workers() const;
  const int64_t& num_workers() const;
  // used by pybind11 only
  // required or optional field max_num_pixels
 public:
  bool has_max_num_pixels() const;
  const int64_t& max_num_pixels() const;
  // used by pybind11 only
  // required or optional field warmup_size
 public:
  bool has_warmup_size() const;
  const int64_t& warmup_size() const;
  // used by pybind11 only
  // required or optional field seed
 public:
  bool has_seed() const;
  const int64_t& seed() const;
  // used by pybind11 only
  // required or optional field num_attempts
 public:
  bool has_num_attempts() const;
  const int64_t& num_attempts() const;
  // used by pybind11 only
  // required or optional field random_area_min
 public:
  bool has_random_area_min() const;
  const float& random_area_min() const;
  // used by pybind11 only
  // required or optional field random_area_max
 public:
  bool has_random_area_max() const;
  const float& random_area_max() const;
  // used by pybind11 only
  // required or optional field random_aspect_ratio_min
 public:
  bool has_random_aspect_ratio_min() const;
  const float& random_aspect_ratio_min() const;
  // used by pybind11 only
  // required or optional field random_aspect_ratio_max
 public:
  bool has_random_aspect_ratio_max() const;
  const float& random_aspect_ratio_max() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstImageDecoderRandomCropResizeOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstImageDecoderRandomCropResizeOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstImageDecoderRandomCropResizeOpConf& other) const;
 protected:
  const ::std::shared_ptr<_ImageDecoderRandomCropResizeOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ImageDecoderRandomCropResizeOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstImageDecoderRandomCropResizeOpConf
  void BuildFromProto(const PbMessage& proto_imagedecoderrandomcropresizeopconf);
  
  ::std::shared_ptr<_ImageDecoderRandomCropResizeOpConf_> data_;
};

class ImageDecoderRandomCropResizeOpConf final : public ConstImageDecoderRandomCropResizeOpConf {
 public:
  ImageDecoderRandomCropResizeOpConf(const ::std::shared_ptr<_ImageDecoderRandomCropResizeOpConf_>& data);
  ImageDecoderRandomCropResizeOpConf(const ImageDecoderRandomCropResizeOpConf& other);
  // enable nothrow for ::std::vector<ImageDecoderRandomCropResizeOpConf> resize 
  ImageDecoderRandomCropResizeOpConf(ImageDecoderRandomCropResizeOpConf&&) noexcept;
  ImageDecoderRandomCropResizeOpConf();
  explicit ImageDecoderRandomCropResizeOpConf(const ::oneflow::ImageDecoderRandomCropResizeOpConf& proto_imagedecoderrandomcropresizeopconf);

  ~ImageDecoderRandomCropResizeOpConf() override;

  void InitFromProto(const PbMessage& proto_imagedecoderrandomcropresizeopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ImageDecoderRandomCropResizeOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ImageDecoderRandomCropResizeOpConf& other) const;
  void Clear();
  void CopyFrom(const ImageDecoderRandomCropResizeOpConf& other);
  ImageDecoderRandomCropResizeOpConf& operator=(const ImageDecoderRandomCropResizeOpConf& other);

  // required or optional field in
 public:
  void clear_in();
  void set_in(const ::std::string& value);
  ::std::string* mutable_in();
  // required or optional field out
 public:
  void clear_out();
  void set_out(const ::std::string& value);
  ::std::string* mutable_out();
  // required or optional field target_width
 public:
  void clear_target_width();
  void set_target_width(const int64_t& value);
  int64_t* mutable_target_width();
  // required or optional field target_height
 public:
  void clear_target_height();
  void set_target_height(const int64_t& value);
  int64_t* mutable_target_height();
  // required or optional field num_workers
 public:
  void clear_num_workers();
  void set_num_workers(const int64_t& value);
  int64_t* mutable_num_workers();
  // required or optional field max_num_pixels
 public:
  void clear_max_num_pixels();
  void set_max_num_pixels(const int64_t& value);
  int64_t* mutable_max_num_pixels();
  // required or optional field warmup_size
 public:
  void clear_warmup_size();
  void set_warmup_size(const int64_t& value);
  int64_t* mutable_warmup_size();
  // required or optional field seed
 public:
  void clear_seed();
  void set_seed(const int64_t& value);
  int64_t* mutable_seed();
  // required or optional field num_attempts
 public:
  void clear_num_attempts();
  void set_num_attempts(const int64_t& value);
  int64_t* mutable_num_attempts();
  // required or optional field random_area_min
 public:
  void clear_random_area_min();
  void set_random_area_min(const float& value);
  float* mutable_random_area_min();
  // required or optional field random_area_max
 public:
  void clear_random_area_max();
  void set_random_area_max(const float& value);
  float* mutable_random_area_max();
  // required or optional field random_aspect_ratio_min
 public:
  void clear_random_aspect_ratio_min();
  void set_random_aspect_ratio_min(const float& value);
  float* mutable_random_aspect_ratio_min();
  // required or optional field random_aspect_ratio_max
 public:
  void clear_random_aspect_ratio_max();
  void set_random_aspect_ratio_max(const float& value);
  float* mutable_random_aspect_ratio_max();

  ::std::shared_ptr<ImageDecoderRandomCropResizeOpConf> __SharedMutable__();
};


class ConstBoxingZerosOpConf : public ::oneflow::cfg::Message {
 public:

  class _BoxingZerosOpConf_ {
   public:
    _BoxingZerosOpConf_();
    explicit _BoxingZerosOpConf_(const _BoxingZerosOpConf_& other);
    explicit _BoxingZerosOpConf_(_BoxingZerosOpConf_&& other);
    _BoxingZerosOpConf_(const ::oneflow::BoxingZerosOpConf& proto_boxingzerosopconf);
    ~_BoxingZerosOpConf_();

    void InitFromProto(const ::oneflow::BoxingZerosOpConf& proto_boxingzerosopconf);

    void ToProto(::oneflow::BoxingZerosOpConf* proto_boxingzerosopconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _BoxingZerosOpConf_& other);
  
      // optional field lbi
     public:
    bool has_lbi() const;
    const ::oneflow::cfg::LogicalBlobId& lbi() const;
    void clear_lbi();
    ::oneflow::cfg::LogicalBlobId* mutable_lbi();
   protected:
    bool has_lbi_ = false;
    ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> lbi_;
      
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
           
   public:
    int compare(const _BoxingZerosOpConf_& other);

    bool operator==(const _BoxingZerosOpConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _BoxingZerosOpConf_& other) const;
  };

  ConstBoxingZerosOpConf(const ::std::shared_ptr<_BoxingZerosOpConf_>& data);
  ConstBoxingZerosOpConf(const ConstBoxingZerosOpConf&);
  ConstBoxingZerosOpConf(ConstBoxingZerosOpConf&&) noexcept;
  ConstBoxingZerosOpConf();
  ConstBoxingZerosOpConf(const ::oneflow::BoxingZerosOpConf& proto_boxingzerosopconf);
  virtual ~ConstBoxingZerosOpConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_boxingzerosopconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field lbi
 public:
  bool has_lbi() const;
  const ::oneflow::cfg::LogicalBlobId& lbi() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLogicalBlobId> shared_const_lbi() const;
  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstBoxingZerosOpConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstBoxingZerosOpConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstBoxingZerosOpConf& other) const;
 protected:
  const ::std::shared_ptr<_BoxingZerosOpConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_BoxingZerosOpConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstBoxingZerosOpConf
  void BuildFromProto(const PbMessage& proto_boxingzerosopconf);
  
  ::std::shared_ptr<_BoxingZerosOpConf_> data_;
};

class BoxingZerosOpConf final : public ConstBoxingZerosOpConf {
 public:
  BoxingZerosOpConf(const ::std::shared_ptr<_BoxingZerosOpConf_>& data);
  BoxingZerosOpConf(const BoxingZerosOpConf& other);
  // enable nothrow for ::std::vector<BoxingZerosOpConf> resize 
  BoxingZerosOpConf(BoxingZerosOpConf&&) noexcept;
  BoxingZerosOpConf();
  explicit BoxingZerosOpConf(const ::oneflow::BoxingZerosOpConf& proto_boxingzerosopconf);

  ~BoxingZerosOpConf() override;

  void InitFromProto(const PbMessage& proto_boxingzerosopconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const BoxingZerosOpConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const BoxingZerosOpConf& other) const;
  void Clear();
  void CopyFrom(const BoxingZerosOpConf& other);
  BoxingZerosOpConf& operator=(const BoxingZerosOpConf& other);

  // required or optional field lbi
 public:
  void clear_lbi();
  ::oneflow::cfg::LogicalBlobId* mutable_lbi();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LogicalBlobId> shared_mutable_lbi();
  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();

  ::std::shared_ptr<BoxingZerosOpConf> __SharedMutable__();
};


class ConstOperatorConf : public ::oneflow::cfg::Message {
 public:

 // oneof enum op_type
 enum OpTypeCase : unsigned int {
  OP_TYPE_NOT_SET = 0,
    kDecodeRandomConf = 102,
    kCopyHdConf = 105,
    kCopyCommNetConf = 106,
    kBoxingConf = 108,
    kVariableConf = 122,
    kTickConf = 124,
    kTotalLossInstanceNumConf = 126,
    kShapeElemCntConf = 132,
    kSrcSubsetTickConf = 133,
    kDstSubsetTickConf = 134,
    kSourceTickConf = 135,
    kSinkTickConf = 136,
    kInputConf = 137,
    kOutputConf = 138,
    kWaitAndSendIdsConf = 139,
    kReentrantLockConf = 140,
    kCallbackNotifyConf = 141,
    kForeignInputConf = 142,
    kForeignOutputConf = 143,
    kAccTickConf = 144,
    kReturnConf = 146,
    kForeignWatchConf = 151,
    kDistributeConcatConf = 155,
    kDistributeSplitConf = 156,
    kDistributeCloneConf = 157,
    kDistributeAddConf = 158,
    kDeviceTickConf = 159,
    kSliceBoxingCopyConf = 166,
    kSliceBoxingAddConf = 167,
    kCollectiveBoxingGenericConf = 170,
    kBoxingIdentityConf = 171,
    kCollectiveBoxingPackConf = 174,
    kCollectiveBoxingUnpackConf = 175,
    kBoxingZerosConf = 176,
    kUserConf = 199,
    kDynamicReshapeConf = 203,
    kDynamicReshapeLikeConf = 287,
    kIdentityConf = 290,
    kCaseConf = 291,
    kEsacConf = 292,
    kModelInitConf = 293,
    kAssignConf = 296,
    kModelSaveConf = 297,
    kLearningRateScheduleConf = 298,
    kModelLoadConf = 301,
    kConstantLikeConf = 339,
    kSyncDynamicResizeConf = 340,
    kCopyConf = 343,
    kCastToMirroredConf = 344,
    kCastFromMirroredConf = 345,
    kModelInitV2Conf = 346,
    kModelLoadV2Conf = 347,
    kModelSaveV2Conf = 348,
    kImageDecoderRandomCropResizeConf = 349,
    kXrtLaunchConf = 410,
    kBroadcastToCompatibleWithConf = 525,
    kFeedInputConf = 600,
    kFeedVariableConf = 601,
    kFetchOutputConf = 602,
   };

  class _OperatorConf_ {
   public:
    _OperatorConf_();
    explicit _OperatorConf_(const _OperatorConf_& other);
    explicit _OperatorConf_(_OperatorConf_&& other);
    _OperatorConf_(const ::oneflow::OperatorConf& proto_operatorconf);
    ~_OperatorConf_();

    void InitFromProto(const ::oneflow::OperatorConf& proto_operatorconf);

    void ToProto(::oneflow::OperatorConf* proto_operatorconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OperatorConf_& other);
  
      // optional field name
     public:
    bool has_name() const;
    const ::std::string& name() const;
    void clear_name();
    void set_name(const ::std::string& value);
    ::std::string* mutable_name();
   protected:
    bool has_name_ = false;
    ::std::string name_;
      
      // optional field device_tag
     public:
    bool has_device_tag() const;
    const ::std::string& device_tag() const;
    void clear_device_tag();
    void set_device_tag(const ::std::string& value);
    ::std::string* mutable_device_tag();
   protected:
    bool has_device_tag_ = false;
    ::std::string device_tag_;
      
      // repeated field ctrl_in_op_name
   public:
    ::std::size_t ctrl_in_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ctrl_in_op_name() const;
    const ::std::string& ctrl_in_op_name(::std::size_t index) const;
    void clear_ctrl_in_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ctrl_in_op_name();
    ::std::string* mutable_ctrl_in_op_name(::std::size_t index);
      void add_ctrl_in_op_name(const ::std::string& value);
    void set_ctrl_in_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> ctrl_in_op_name_;
    
      // optional field scope_symbol_id
     public:
    bool has_scope_symbol_id() const;
    const int64_t& scope_symbol_id() const;
    void clear_scope_symbol_id();
    void set_scope_symbol_id(const int64_t& value);
    int64_t* mutable_scope_symbol_id();
   protected:
    bool has_scope_symbol_id_ = false;
    int64_t scope_symbol_id_;
      
      // optional field stream_index_hint
     public:
    bool has_stream_index_hint() const;
    const uint32_t& stream_index_hint() const;
    void clear_stream_index_hint();
    void set_stream_index_hint(const uint32_t& value);
    uint32_t* mutable_stream_index_hint();
   protected:
    bool has_stream_index_hint_ = false;
    uint32_t stream_index_hint_;
      
      // optional field pass_tag
     public:
    bool has_pass_tag() const;
    const ::std::string& pass_tag() const;
    void clear_pass_tag();
    void set_pass_tag(const ::std::string& value);
    ::std::string* mutable_pass_tag();
   protected:
    bool has_pass_tag_ = false;
    ::std::string pass_tag_;
      
     // oneof field op_type: decode_random_conf
   public:
    bool has_decode_random_conf() const;
    void clear_decode_random_conf();
    const ::oneflow::cfg::DecodeRandomOpConf& decode_random_conf() const;
      ::oneflow::cfg::DecodeRandomOpConf* mutable_decode_random_conf();
      
     // oneof field op_type: copy_hd_conf
   public:
    bool has_copy_hd_conf() const;
    void clear_copy_hd_conf();
    const ::oneflow::cfg::CopyHdOpConf& copy_hd_conf() const;
      ::oneflow::cfg::CopyHdOpConf* mutable_copy_hd_conf();
      
     // oneof field op_type: copy_comm_net_conf
   public:
    bool has_copy_comm_net_conf() const;
    void clear_copy_comm_net_conf();
    const ::oneflow::cfg::CopyCommNetOpConf& copy_comm_net_conf() const;
      ::oneflow::cfg::CopyCommNetOpConf* mutable_copy_comm_net_conf();
      
     // oneof field op_type: boxing_conf
   public:
    bool has_boxing_conf() const;
    void clear_boxing_conf();
    const ::oneflow::cfg::BoxingOpConf& boxing_conf() const;
      ::oneflow::cfg::BoxingOpConf* mutable_boxing_conf();
      
     // oneof field op_type: variable_conf
   public:
    bool has_variable_conf() const;
    void clear_variable_conf();
    const ::oneflow::cfg::VariableOpConf& variable_conf() const;
      ::oneflow::cfg::VariableOpConf* mutable_variable_conf();
      
     // oneof field op_type: tick_conf
   public:
    bool has_tick_conf() const;
    void clear_tick_conf();
    const ::oneflow::cfg::TickOpConf& tick_conf() const;
      ::oneflow::cfg::TickOpConf* mutable_tick_conf();
      
     // oneof field op_type: total_loss_instance_num_conf
   public:
    bool has_total_loss_instance_num_conf() const;
    void clear_total_loss_instance_num_conf();
    const ::oneflow::cfg::TotalLossInstanceNumOpConf& total_loss_instance_num_conf() const;
      ::oneflow::cfg::TotalLossInstanceNumOpConf* mutable_total_loss_instance_num_conf();
      
     // oneof field op_type: shape_elem_cnt_conf
   public:
    bool has_shape_elem_cnt_conf() const;
    void clear_shape_elem_cnt_conf();
    const ::oneflow::cfg::ShapeElemCntOpConf& shape_elem_cnt_conf() const;
      ::oneflow::cfg::ShapeElemCntOpConf* mutable_shape_elem_cnt_conf();
      
     // oneof field op_type: src_subset_tick_conf
   public:
    bool has_src_subset_tick_conf() const;
    void clear_src_subset_tick_conf();
    const ::oneflow::cfg::SrcSubsetTickOpConf& src_subset_tick_conf() const;
      ::oneflow::cfg::SrcSubsetTickOpConf* mutable_src_subset_tick_conf();
      
     // oneof field op_type: dst_subset_tick_conf
   public:
    bool has_dst_subset_tick_conf() const;
    void clear_dst_subset_tick_conf();
    const ::oneflow::cfg::DstSubsetTickOpConf& dst_subset_tick_conf() const;
      ::oneflow::cfg::DstSubsetTickOpConf* mutable_dst_subset_tick_conf();
      
     // oneof field op_type: source_tick_conf
   public:
    bool has_source_tick_conf() const;
    void clear_source_tick_conf();
    const ::oneflow::cfg::SourceTickOpConf& source_tick_conf() const;
      ::oneflow::cfg::SourceTickOpConf* mutable_source_tick_conf();
      
     // oneof field op_type: sink_tick_conf
   public:
    bool has_sink_tick_conf() const;
    void clear_sink_tick_conf();
    const ::oneflow::cfg::SinkTickOpConf& sink_tick_conf() const;
      ::oneflow::cfg::SinkTickOpConf* mutable_sink_tick_conf();
      
     // oneof field op_type: input_conf
   public:
    bool has_input_conf() const;
    void clear_input_conf();
    const ::oneflow::cfg::InputOpConf& input_conf() const;
      ::oneflow::cfg::InputOpConf* mutable_input_conf();
      
     // oneof field op_type: output_conf
   public:
    bool has_output_conf() const;
    void clear_output_conf();
    const ::oneflow::cfg::OutputOpConf& output_conf() const;
      ::oneflow::cfg::OutputOpConf* mutable_output_conf();
      
     // oneof field op_type: wait_and_send_ids_conf
   public:
    bool has_wait_and_send_ids_conf() const;
    void clear_wait_and_send_ids_conf();
    const ::oneflow::cfg::WaitAndSendIdsOpConf& wait_and_send_ids_conf() const;
      ::oneflow::cfg::WaitAndSendIdsOpConf* mutable_wait_and_send_ids_conf();
      
     // oneof field op_type: reentrant_lock_conf
   public:
    bool has_reentrant_lock_conf() const;
    void clear_reentrant_lock_conf();
    const ::oneflow::cfg::ReentrantLockOpConf& reentrant_lock_conf() const;
      ::oneflow::cfg::ReentrantLockOpConf* mutable_reentrant_lock_conf();
      
     // oneof field op_type: callback_notify_conf
   public:
    bool has_callback_notify_conf() const;
    void clear_callback_notify_conf();
    const ::oneflow::cfg::CallbackNotifyOpConf& callback_notify_conf() const;
      ::oneflow::cfg::CallbackNotifyOpConf* mutable_callback_notify_conf();
      
     // oneof field op_type: foreign_input_conf
   public:
    bool has_foreign_input_conf() const;
    void clear_foreign_input_conf();
    const ::oneflow::cfg::ForeignInputOpConf& foreign_input_conf() const;
      ::oneflow::cfg::ForeignInputOpConf* mutable_foreign_input_conf();
      
     // oneof field op_type: foreign_output_conf
   public:
    bool has_foreign_output_conf() const;
    void clear_foreign_output_conf();
    const ::oneflow::cfg::ForeignOutputOpConf& foreign_output_conf() const;
      ::oneflow::cfg::ForeignOutputOpConf* mutable_foreign_output_conf();
      
     // oneof field op_type: acc_tick_conf
   public:
    bool has_acc_tick_conf() const;
    void clear_acc_tick_conf();
    const ::oneflow::cfg::AccTickOpConf& acc_tick_conf() const;
      ::oneflow::cfg::AccTickOpConf* mutable_acc_tick_conf();
      
     // oneof field op_type: return_conf
   public:
    bool has_return_conf() const;
    void clear_return_conf();
    const ::oneflow::cfg::ReturnOpConf& return_conf() const;
      ::oneflow::cfg::ReturnOpConf* mutable_return_conf();
      
     // oneof field op_type: foreign_watch_conf
   public:
    bool has_foreign_watch_conf() const;
    void clear_foreign_watch_conf();
    const ::oneflow::cfg::ForeignWatchOpConf& foreign_watch_conf() const;
      ::oneflow::cfg::ForeignWatchOpConf* mutable_foreign_watch_conf();
      
     // oneof field op_type: distribute_concat_conf
   public:
    bool has_distribute_concat_conf() const;
    void clear_distribute_concat_conf();
    const ::oneflow::cfg::DistributeConcatOpConf& distribute_concat_conf() const;
      ::oneflow::cfg::DistributeConcatOpConf* mutable_distribute_concat_conf();
      
     // oneof field op_type: distribute_split_conf
   public:
    bool has_distribute_split_conf() const;
    void clear_distribute_split_conf();
    const ::oneflow::cfg::DistributeSplitOpConf& distribute_split_conf() const;
      ::oneflow::cfg::DistributeSplitOpConf* mutable_distribute_split_conf();
      
     // oneof field op_type: distribute_clone_conf
   public:
    bool has_distribute_clone_conf() const;
    void clear_distribute_clone_conf();
    const ::oneflow::cfg::DistributeCloneOpConf& distribute_clone_conf() const;
      ::oneflow::cfg::DistributeCloneOpConf* mutable_distribute_clone_conf();
      
     // oneof field op_type: distribute_add_conf
   public:
    bool has_distribute_add_conf() const;
    void clear_distribute_add_conf();
    const ::oneflow::cfg::DistributeAddOpConf& distribute_add_conf() const;
      ::oneflow::cfg::DistributeAddOpConf* mutable_distribute_add_conf();
      
     // oneof field op_type: device_tick_conf
   public:
    bool has_device_tick_conf() const;
    void clear_device_tick_conf();
    const ::oneflow::cfg::DeviceTickOpConf& device_tick_conf() const;
      ::oneflow::cfg::DeviceTickOpConf* mutable_device_tick_conf();
      
     // oneof field op_type: slice_boxing_copy_conf
   public:
    bool has_slice_boxing_copy_conf() const;
    void clear_slice_boxing_copy_conf();
    const ::oneflow::cfg::SliceBoxingCopyOpConf& slice_boxing_copy_conf() const;
      ::oneflow::cfg::SliceBoxingCopyOpConf* mutable_slice_boxing_copy_conf();
      
     // oneof field op_type: slice_boxing_add_conf
   public:
    bool has_slice_boxing_add_conf() const;
    void clear_slice_boxing_add_conf();
    const ::oneflow::cfg::SliceBoxingAddOpConf& slice_boxing_add_conf() const;
      ::oneflow::cfg::SliceBoxingAddOpConf* mutable_slice_boxing_add_conf();
      
     // oneof field op_type: collective_boxing_generic_conf
   public:
    bool has_collective_boxing_generic_conf() const;
    void clear_collective_boxing_generic_conf();
    const ::oneflow::cfg::CollectiveBoxingGenericOpConf& collective_boxing_generic_conf() const;
      ::oneflow::cfg::CollectiveBoxingGenericOpConf* mutable_collective_boxing_generic_conf();
      
     // oneof field op_type: boxing_identity_conf
   public:
    bool has_boxing_identity_conf() const;
    void clear_boxing_identity_conf();
    const ::oneflow::cfg::BoxingIdentityOpConf& boxing_identity_conf() const;
      ::oneflow::cfg::BoxingIdentityOpConf* mutable_boxing_identity_conf();
      
     // oneof field op_type: collective_boxing_pack_conf
   public:
    bool has_collective_boxing_pack_conf() const;
    void clear_collective_boxing_pack_conf();
    const ::oneflow::cfg::CollectiveBoxingPackOpConf& collective_boxing_pack_conf() const;
      ::oneflow::cfg::CollectiveBoxingPackOpConf* mutable_collective_boxing_pack_conf();
      
     // oneof field op_type: collective_boxing_unpack_conf
   public:
    bool has_collective_boxing_unpack_conf() const;
    void clear_collective_boxing_unpack_conf();
    const ::oneflow::cfg::CollectiveBoxingUnpackOpConf& collective_boxing_unpack_conf() const;
      ::oneflow::cfg::CollectiveBoxingUnpackOpConf* mutable_collective_boxing_unpack_conf();
      
     // oneof field op_type: boxing_zeros_conf
   public:
    bool has_boxing_zeros_conf() const;
    void clear_boxing_zeros_conf();
    const ::oneflow::cfg::BoxingZerosOpConf& boxing_zeros_conf() const;
      ::oneflow::cfg::BoxingZerosOpConf* mutable_boxing_zeros_conf();
      
     // oneof field op_type: user_conf
   public:
    bool has_user_conf() const;
    void clear_user_conf();
    const ::oneflow::cfg::UserOpConf& user_conf() const;
      ::oneflow::cfg::UserOpConf* mutable_user_conf();
      
     // oneof field op_type: dynamic_reshape_conf
   public:
    bool has_dynamic_reshape_conf() const;
    void clear_dynamic_reshape_conf();
    const ::oneflow::cfg::DynamicReshapeOpConf& dynamic_reshape_conf() const;
      ::oneflow::cfg::DynamicReshapeOpConf* mutable_dynamic_reshape_conf();
      
     // oneof field op_type: dynamic_reshape_like_conf
   public:
    bool has_dynamic_reshape_like_conf() const;
    void clear_dynamic_reshape_like_conf();
    const ::oneflow::cfg::DynamicReshapeLikeOpConf& dynamic_reshape_like_conf() const;
      ::oneflow::cfg::DynamicReshapeLikeOpConf* mutable_dynamic_reshape_like_conf();
      
     // oneof field op_type: identity_conf
   public:
    bool has_identity_conf() const;
    void clear_identity_conf();
    const ::oneflow::cfg::IdentityOpConf& identity_conf() const;
      ::oneflow::cfg::IdentityOpConf* mutable_identity_conf();
      
     // oneof field op_type: case_conf
   public:
    bool has_case_conf() const;
    void clear_case_conf();
    const ::oneflow::cfg::CaseOpConf& case_conf() const;
      ::oneflow::cfg::CaseOpConf* mutable_case_conf();
      
     // oneof field op_type: esac_conf
   public:
    bool has_esac_conf() const;
    void clear_esac_conf();
    const ::oneflow::cfg::EsacOpConf& esac_conf() const;
      ::oneflow::cfg::EsacOpConf* mutable_esac_conf();
      
     // oneof field op_type: model_init_conf
   public:
    bool has_model_init_conf() const;
    void clear_model_init_conf();
    const ::oneflow::cfg::ModelInitOpConf& model_init_conf() const;
      ::oneflow::cfg::ModelInitOpConf* mutable_model_init_conf();
      
     // oneof field op_type: assign_conf
   public:
    bool has_assign_conf() const;
    void clear_assign_conf();
    const ::oneflow::cfg::AssignOpConf& assign_conf() const;
      ::oneflow::cfg::AssignOpConf* mutable_assign_conf();
      
     // oneof field op_type: model_save_conf
   public:
    bool has_model_save_conf() const;
    void clear_model_save_conf();
    const ::oneflow::cfg::ModelSaveOpConf& model_save_conf() const;
      ::oneflow::cfg::ModelSaveOpConf* mutable_model_save_conf();
      
     // oneof field op_type: learning_rate_schedule_conf
   public:
    bool has_learning_rate_schedule_conf() const;
    void clear_learning_rate_schedule_conf();
    const ::oneflow::cfg::LearningRateScheduleOpConf& learning_rate_schedule_conf() const;
      ::oneflow::cfg::LearningRateScheduleOpConf* mutable_learning_rate_schedule_conf();
      
     // oneof field op_type: model_load_conf
   public:
    bool has_model_load_conf() const;
    void clear_model_load_conf();
    const ::oneflow::cfg::ModelLoadOpConf& model_load_conf() const;
      ::oneflow::cfg::ModelLoadOpConf* mutable_model_load_conf();
      
     // oneof field op_type: constant_like_conf
   public:
    bool has_constant_like_conf() const;
    void clear_constant_like_conf();
    const ::oneflow::cfg::ConstantLikeOpConf& constant_like_conf() const;
      ::oneflow::cfg::ConstantLikeOpConf* mutable_constant_like_conf();
      
     // oneof field op_type: sync_dynamic_resize_conf
   public:
    bool has_sync_dynamic_resize_conf() const;
    void clear_sync_dynamic_resize_conf();
    const ::oneflow::cfg::SyncDynamicResizeOpConf& sync_dynamic_resize_conf() const;
      ::oneflow::cfg::SyncDynamicResizeOpConf* mutable_sync_dynamic_resize_conf();
      
     // oneof field op_type: copy_conf
   public:
    bool has_copy_conf() const;
    void clear_copy_conf();
    const ::oneflow::cfg::CopyOpConf& copy_conf() const;
      ::oneflow::cfg::CopyOpConf* mutable_copy_conf();
      
     // oneof field op_type: cast_to_mirrored_conf
   public:
    bool has_cast_to_mirrored_conf() const;
    void clear_cast_to_mirrored_conf();
    const ::oneflow::cfg::CastToMirroredOpConf& cast_to_mirrored_conf() const;
      ::oneflow::cfg::CastToMirroredOpConf* mutable_cast_to_mirrored_conf();
      
     // oneof field op_type: cast_from_mirrored_conf
   public:
    bool has_cast_from_mirrored_conf() const;
    void clear_cast_from_mirrored_conf();
    const ::oneflow::cfg::CastFromMirroredOpConf& cast_from_mirrored_conf() const;
      ::oneflow::cfg::CastFromMirroredOpConf* mutable_cast_from_mirrored_conf();
      
     // oneof field op_type: model_init_v2_conf
   public:
    bool has_model_init_v2_conf() const;
    void clear_model_init_v2_conf();
    const ::oneflow::cfg::ModelInitV2OpConf& model_init_v2_conf() const;
      ::oneflow::cfg::ModelInitV2OpConf* mutable_model_init_v2_conf();
      
     // oneof field op_type: model_load_v2_conf
   public:
    bool has_model_load_v2_conf() const;
    void clear_model_load_v2_conf();
    const ::oneflow::cfg::ModelLoadV2OpConf& model_load_v2_conf() const;
      ::oneflow::cfg::ModelLoadV2OpConf* mutable_model_load_v2_conf();
      
     // oneof field op_type: model_save_v2_conf
   public:
    bool has_model_save_v2_conf() const;
    void clear_model_save_v2_conf();
    const ::oneflow::cfg::ModelSaveV2OpConf& model_save_v2_conf() const;
      ::oneflow::cfg::ModelSaveV2OpConf* mutable_model_save_v2_conf();
      
     // oneof field op_type: image_decoder_random_crop_resize_conf
   public:
    bool has_image_decoder_random_crop_resize_conf() const;
    void clear_image_decoder_random_crop_resize_conf();
    const ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf& image_decoder_random_crop_resize_conf() const;
      ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf* mutable_image_decoder_random_crop_resize_conf();
      
     // oneof field op_type: xrt_launch_conf
   public:
    bool has_xrt_launch_conf() const;
    void clear_xrt_launch_conf();
    const ::oneflow::cfg::XrtLaunchOpConf& xrt_launch_conf() const;
      ::oneflow::cfg::XrtLaunchOpConf* mutable_xrt_launch_conf();
      
     // oneof field op_type: broadcast_to_compatible_with_conf
   public:
    bool has_broadcast_to_compatible_with_conf() const;
    void clear_broadcast_to_compatible_with_conf();
    const ::oneflow::cfg::BroadcastToCompatibleWithOpConf& broadcast_to_compatible_with_conf() const;
      ::oneflow::cfg::BroadcastToCompatibleWithOpConf* mutable_broadcast_to_compatible_with_conf();
      
     // oneof field op_type: feed_input_conf
   public:
    bool has_feed_input_conf() const;
    void clear_feed_input_conf();
    const ::oneflow::cfg::FeedInputOpConf& feed_input_conf() const;
      ::oneflow::cfg::FeedInputOpConf* mutable_feed_input_conf();
      
     // oneof field op_type: feed_variable_conf
   public:
    bool has_feed_variable_conf() const;
    void clear_feed_variable_conf();
    const ::oneflow::cfg::FeedVariableOpConf& feed_variable_conf() const;
      ::oneflow::cfg::FeedVariableOpConf* mutable_feed_variable_conf();
      
     // oneof field op_type: fetch_output_conf
   public:
    bool has_fetch_output_conf() const;
    void clear_fetch_output_conf();
    const ::oneflow::cfg::FetchOutputOpConf& fetch_output_conf() const;
      ::oneflow::cfg::FetchOutputOpConf* mutable_fetch_output_conf();
           
   public:
    // oneof op_type
    OpTypeCase op_type_case() const;
    bool has_op_type() const;
   protected:
    void clear_op_type();
    void op_type_copy_from(const _OperatorConf_& other);
    union OpTypeUnion {
      // 64-bit aligned
      uint64_t __op_type_for_padding_64bit__;
          char decode_random_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DecodeRandomOpConf>)];
            char copy_hd_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CopyHdOpConf>)];
            char copy_comm_net_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CopyCommNetOpConf>)];
            char boxing_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxingOpConf>)];
            char variable_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::VariableOpConf>)];
            char tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::TickOpConf>)];
            char total_loss_instance_num_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::TotalLossInstanceNumOpConf>)];
            char shape_elem_cnt_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ShapeElemCntOpConf>)];
            char src_subset_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SrcSubsetTickOpConf>)];
            char dst_subset_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DstSubsetTickOpConf>)];
            char source_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SourceTickOpConf>)];
            char sink_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SinkTickOpConf>)];
            char input_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::InputOpConf>)];
            char output_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::OutputOpConf>)];
            char wait_and_send_ids_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::WaitAndSendIdsOpConf>)];
            char reentrant_lock_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ReentrantLockOpConf>)];
            char callback_notify_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CallbackNotifyOpConf>)];
            char foreign_input_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ForeignInputOpConf>)];
            char foreign_output_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ForeignOutputOpConf>)];
            char acc_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::AccTickOpConf>)];
            char return_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ReturnOpConf>)];
            char foreign_watch_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ForeignWatchOpConf>)];
            char distribute_concat_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DistributeConcatOpConf>)];
            char distribute_split_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DistributeSplitOpConf>)];
            char distribute_clone_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DistributeCloneOpConf>)];
            char distribute_add_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DistributeAddOpConf>)];
            char device_tick_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DeviceTickOpConf>)];
            char slice_boxing_copy_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SliceBoxingCopyOpConf>)];
            char slice_boxing_add_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SliceBoxingAddOpConf>)];
            char collective_boxing_generic_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CollectiveBoxingGenericOpConf>)];
            char boxing_identity_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxingIdentityOpConf>)];
            char collective_boxing_pack_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CollectiveBoxingPackOpConf>)];
            char collective_boxing_unpack_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CollectiveBoxingUnpackOpConf>)];
            char boxing_zeros_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::BoxingZerosOpConf>)];
            char user_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::UserOpConf>)];
            char dynamic_reshape_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DynamicReshapeOpConf>)];
            char dynamic_reshape_like_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::DynamicReshapeLikeOpConf>)];
            char identity_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::IdentityOpConf>)];
            char case_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CaseOpConf>)];
            char esac_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::EsacOpConf>)];
            char model_init_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelInitOpConf>)];
            char assign_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::AssignOpConf>)];
            char model_save_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelSaveOpConf>)];
            char learning_rate_schedule_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::LearningRateScheduleOpConf>)];
            char model_load_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelLoadOpConf>)];
            char constant_like_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ConstantLikeOpConf>)];
            char sync_dynamic_resize_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::SyncDynamicResizeOpConf>)];
            char copy_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CopyOpConf>)];
            char cast_to_mirrored_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CastToMirroredOpConf>)];
            char cast_from_mirrored_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::CastFromMirroredOpConf>)];
            char model_init_v2_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelInitV2OpConf>)];
            char model_load_v2_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelLoadV2OpConf>)];
            char model_save_v2_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ModelSaveV2OpConf>)];
            char image_decoder_random_crop_resize_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::ImageDecoderRandomCropResizeOpConf>)];
            char xrt_launch_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf>)];
            char broadcast_to_compatible_with_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::BroadcastToCompatibleWithOpConf>)];
            char feed_input_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::FeedInputOpConf>)];
            char feed_variable_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::FeedVariableOpConf>)];
            char fetch_output_conf_[sizeof(::std::shared_ptr<::oneflow::cfg::FetchOutputOpConf>)];
        } op_type_;
    OpTypeCase op_type_case_ = OP_TYPE_NOT_SET;
     
   public:
    int compare(const _OperatorConf_& other);

    bool operator==(const _OperatorConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OperatorConf_& other) const;
  };

  ConstOperatorConf(const ::std::shared_ptr<_OperatorConf_>& data);
  ConstOperatorConf(const ConstOperatorConf&);
  ConstOperatorConf(ConstOperatorConf&&) noexcept;
  ConstOperatorConf();
  ConstOperatorConf(const ::oneflow::OperatorConf& proto_operatorconf);
  virtual ~ConstOperatorConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_operatorconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field name
 public:
  bool has_name() const;
  const ::std::string& name() const;
  // used by pybind11 only
  // required or optional field device_tag
 public:
  bool has_device_tag() const;
  const ::std::string& device_tag() const;
  // used by pybind11 only
  // repeated field ctrl_in_op_name
 public:
  ::std::size_t ctrl_in_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& ctrl_in_op_name() const;
  const ::std::string& ctrl_in_op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_ctrl_in_op_name() const;
  // required or optional field scope_symbol_id
 public:
  bool has_scope_symbol_id() const;
  const int64_t& scope_symbol_id() const;
  // used by pybind11 only
  // required or optional field stream_index_hint
 public:
  bool has_stream_index_hint() const;
  const uint32_t& stream_index_hint() const;
  // used by pybind11 only
  // required or optional field pass_tag
 public:
  bool has_pass_tag() const;
  const ::std::string& pass_tag() const;
  // used by pybind11 only
 // oneof field op_type: decode_random_conf
 public:
  bool has_decode_random_conf() const;
  const ::oneflow::cfg::DecodeRandomOpConf& decode_random_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDecodeRandomOpConf> shared_const_decode_random_conf() const;
 // oneof field op_type: copy_hd_conf
 public:
  bool has_copy_hd_conf() const;
  const ::oneflow::cfg::CopyHdOpConf& copy_hd_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCopyHdOpConf> shared_const_copy_hd_conf() const;
 // oneof field op_type: copy_comm_net_conf
 public:
  bool has_copy_comm_net_conf() const;
  const ::oneflow::cfg::CopyCommNetOpConf& copy_comm_net_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCopyCommNetOpConf> shared_const_copy_comm_net_conf() const;
 // oneof field op_type: boxing_conf
 public:
  bool has_boxing_conf() const;
  const ::oneflow::cfg::BoxingOpConf& boxing_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxingOpConf> shared_const_boxing_conf() const;
 // oneof field op_type: variable_conf
 public:
  bool has_variable_conf() const;
  const ::oneflow::cfg::VariableOpConf& variable_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> shared_const_variable_conf() const;
 // oneof field op_type: tick_conf
 public:
  bool has_tick_conf() const;
  const ::oneflow::cfg::TickOpConf& tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTickOpConf> shared_const_tick_conf() const;
 // oneof field op_type: total_loss_instance_num_conf
 public:
  bool has_total_loss_instance_num_conf() const;
  const ::oneflow::cfg::TotalLossInstanceNumOpConf& total_loss_instance_num_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstTotalLossInstanceNumOpConf> shared_const_total_loss_instance_num_conf() const;
 // oneof field op_type: shape_elem_cnt_conf
 public:
  bool has_shape_elem_cnt_conf() const;
  const ::oneflow::cfg::ShapeElemCntOpConf& shape_elem_cnt_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeElemCntOpConf> shared_const_shape_elem_cnt_conf() const;
 // oneof field op_type: src_subset_tick_conf
 public:
  bool has_src_subset_tick_conf() const;
  const ::oneflow::cfg::SrcSubsetTickOpConf& src_subset_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSrcSubsetTickOpConf> shared_const_src_subset_tick_conf() const;
 // oneof field op_type: dst_subset_tick_conf
 public:
  bool has_dst_subset_tick_conf() const;
  const ::oneflow::cfg::DstSubsetTickOpConf& dst_subset_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDstSubsetTickOpConf> shared_const_dst_subset_tick_conf() const;
 // oneof field op_type: source_tick_conf
 public:
  bool has_source_tick_conf() const;
  const ::oneflow::cfg::SourceTickOpConf& source_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSourceTickOpConf> shared_const_source_tick_conf() const;
 // oneof field op_type: sink_tick_conf
 public:
  bool has_sink_tick_conf() const;
  const ::oneflow::cfg::SinkTickOpConf& sink_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSinkTickOpConf> shared_const_sink_tick_conf() const;
 // oneof field op_type: input_conf
 public:
  bool has_input_conf() const;
  const ::oneflow::cfg::InputOpConf& input_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstInputOpConf> shared_const_input_conf() const;
 // oneof field op_type: output_conf
 public:
  bool has_output_conf() const;
  const ::oneflow::cfg::OutputOpConf& output_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOutputOpConf> shared_const_output_conf() const;
 // oneof field op_type: wait_and_send_ids_conf
 public:
  bool has_wait_and_send_ids_conf() const;
  const ::oneflow::cfg::WaitAndSendIdsOpConf& wait_and_send_ids_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstWaitAndSendIdsOpConf> shared_const_wait_and_send_ids_conf() const;
 // oneof field op_type: reentrant_lock_conf
 public:
  bool has_reentrant_lock_conf() const;
  const ::oneflow::cfg::ReentrantLockOpConf& reentrant_lock_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReentrantLockOpConf> shared_const_reentrant_lock_conf() const;
 // oneof field op_type: callback_notify_conf
 public:
  bool has_callback_notify_conf() const;
  const ::oneflow::cfg::CallbackNotifyOpConf& callback_notify_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCallbackNotifyOpConf> shared_const_callback_notify_conf() const;
 // oneof field op_type: foreign_input_conf
 public:
  bool has_foreign_input_conf() const;
  const ::oneflow::cfg::ForeignInputOpConf& foreign_input_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstForeignInputOpConf> shared_const_foreign_input_conf() const;
 // oneof field op_type: foreign_output_conf
 public:
  bool has_foreign_output_conf() const;
  const ::oneflow::cfg::ForeignOutputOpConf& foreign_output_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstForeignOutputOpConf> shared_const_foreign_output_conf() const;
 // oneof field op_type: acc_tick_conf
 public:
  bool has_acc_tick_conf() const;
  const ::oneflow::cfg::AccTickOpConf& acc_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAccTickOpConf> shared_const_acc_tick_conf() const;
 // oneof field op_type: return_conf
 public:
  bool has_return_conf() const;
  const ::oneflow::cfg::ReturnOpConf& return_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstReturnOpConf> shared_const_return_conf() const;
 // oneof field op_type: foreign_watch_conf
 public:
  bool has_foreign_watch_conf() const;
  const ::oneflow::cfg::ForeignWatchOpConf& foreign_watch_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstForeignWatchOpConf> shared_const_foreign_watch_conf() const;
 // oneof field op_type: distribute_concat_conf
 public:
  bool has_distribute_concat_conf() const;
  const ::oneflow::cfg::DistributeConcatOpConf& distribute_concat_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDistributeConcatOpConf> shared_const_distribute_concat_conf() const;
 // oneof field op_type: distribute_split_conf
 public:
  bool has_distribute_split_conf() const;
  const ::oneflow::cfg::DistributeSplitOpConf& distribute_split_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDistributeSplitOpConf> shared_const_distribute_split_conf() const;
 // oneof field op_type: distribute_clone_conf
 public:
  bool has_distribute_clone_conf() const;
  const ::oneflow::cfg::DistributeCloneOpConf& distribute_clone_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDistributeCloneOpConf> shared_const_distribute_clone_conf() const;
 // oneof field op_type: distribute_add_conf
 public:
  bool has_distribute_add_conf() const;
  const ::oneflow::cfg::DistributeAddOpConf& distribute_add_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDistributeAddOpConf> shared_const_distribute_add_conf() const;
 // oneof field op_type: device_tick_conf
 public:
  bool has_device_tick_conf() const;
  const ::oneflow::cfg::DeviceTickOpConf& device_tick_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDeviceTickOpConf> shared_const_device_tick_conf() const;
 // oneof field op_type: slice_boxing_copy_conf
 public:
  bool has_slice_boxing_copy_conf() const;
  const ::oneflow::cfg::SliceBoxingCopyOpConf& slice_boxing_copy_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSliceBoxingCopyOpConf> shared_const_slice_boxing_copy_conf() const;
 // oneof field op_type: slice_boxing_add_conf
 public:
  bool has_slice_boxing_add_conf() const;
  const ::oneflow::cfg::SliceBoxingAddOpConf& slice_boxing_add_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSliceBoxingAddOpConf> shared_const_slice_boxing_add_conf() const;
 // oneof field op_type: collective_boxing_generic_conf
 public:
  bool has_collective_boxing_generic_conf() const;
  const ::oneflow::cfg::CollectiveBoxingGenericOpConf& collective_boxing_generic_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCollectiveBoxingGenericOpConf> shared_const_collective_boxing_generic_conf() const;
 // oneof field op_type: boxing_identity_conf
 public:
  bool has_boxing_identity_conf() const;
  const ::oneflow::cfg::BoxingIdentityOpConf& boxing_identity_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxingIdentityOpConf> shared_const_boxing_identity_conf() const;
 // oneof field op_type: collective_boxing_pack_conf
 public:
  bool has_collective_boxing_pack_conf() const;
  const ::oneflow::cfg::CollectiveBoxingPackOpConf& collective_boxing_pack_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCollectiveBoxingPackOpConf> shared_const_collective_boxing_pack_conf() const;
 // oneof field op_type: collective_boxing_unpack_conf
 public:
  bool has_collective_boxing_unpack_conf() const;
  const ::oneflow::cfg::CollectiveBoxingUnpackOpConf& collective_boxing_unpack_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCollectiveBoxingUnpackOpConf> shared_const_collective_boxing_unpack_conf() const;
 // oneof field op_type: boxing_zeros_conf
 public:
  bool has_boxing_zeros_conf() const;
  const ::oneflow::cfg::BoxingZerosOpConf& boxing_zeros_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBoxingZerosOpConf> shared_const_boxing_zeros_conf() const;
 // oneof field op_type: user_conf
 public:
  bool has_user_conf() const;
  const ::oneflow::cfg::UserOpConf& user_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstUserOpConf> shared_const_user_conf() const;
 // oneof field op_type: dynamic_reshape_conf
 public:
  bool has_dynamic_reshape_conf() const;
  const ::oneflow::cfg::DynamicReshapeOpConf& dynamic_reshape_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDynamicReshapeOpConf> shared_const_dynamic_reshape_conf() const;
 // oneof field op_type: dynamic_reshape_like_conf
 public:
  bool has_dynamic_reshape_like_conf() const;
  const ::oneflow::cfg::DynamicReshapeLikeOpConf& dynamic_reshape_like_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstDynamicReshapeLikeOpConf> shared_const_dynamic_reshape_like_conf() const;
 // oneof field op_type: identity_conf
 public:
  bool has_identity_conf() const;
  const ::oneflow::cfg::IdentityOpConf& identity_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstIdentityOpConf> shared_const_identity_conf() const;
 // oneof field op_type: case_conf
 public:
  bool has_case_conf() const;
  const ::oneflow::cfg::CaseOpConf& case_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCaseOpConf> shared_const_case_conf() const;
 // oneof field op_type: esac_conf
 public:
  bool has_esac_conf() const;
  const ::oneflow::cfg::EsacOpConf& esac_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstEsacOpConf> shared_const_esac_conf() const;
 // oneof field op_type: model_init_conf
 public:
  bool has_model_init_conf() const;
  const ::oneflow::cfg::ModelInitOpConf& model_init_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelInitOpConf> shared_const_model_init_conf() const;
 // oneof field op_type: assign_conf
 public:
  bool has_assign_conf() const;
  const ::oneflow::cfg::AssignOpConf& assign_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstAssignOpConf> shared_const_assign_conf() const;
 // oneof field op_type: model_save_conf
 public:
  bool has_model_save_conf() const;
  const ::oneflow::cfg::ModelSaveOpConf& model_save_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelSaveOpConf> shared_const_model_save_conf() const;
 // oneof field op_type: learning_rate_schedule_conf
 public:
  bool has_learning_rate_schedule_conf() const;
  const ::oneflow::cfg::LearningRateScheduleOpConf& learning_rate_schedule_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstLearningRateScheduleOpConf> shared_const_learning_rate_schedule_conf() const;
 // oneof field op_type: model_load_conf
 public:
  bool has_model_load_conf() const;
  const ::oneflow::cfg::ModelLoadOpConf& model_load_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelLoadOpConf> shared_const_model_load_conf() const;
 // oneof field op_type: constant_like_conf
 public:
  bool has_constant_like_conf() const;
  const ::oneflow::cfg::ConstantLikeOpConf& constant_like_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstConstantLikeOpConf> shared_const_constant_like_conf() const;
 // oneof field op_type: sync_dynamic_resize_conf
 public:
  bool has_sync_dynamic_resize_conf() const;
  const ::oneflow::cfg::SyncDynamicResizeOpConf& sync_dynamic_resize_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSyncDynamicResizeOpConf> shared_const_sync_dynamic_resize_conf() const;
 // oneof field op_type: copy_conf
 public:
  bool has_copy_conf() const;
  const ::oneflow::cfg::CopyOpConf& copy_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCopyOpConf> shared_const_copy_conf() const;
 // oneof field op_type: cast_to_mirrored_conf
 public:
  bool has_cast_to_mirrored_conf() const;
  const ::oneflow::cfg::CastToMirroredOpConf& cast_to_mirrored_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCastToMirroredOpConf> shared_const_cast_to_mirrored_conf() const;
 // oneof field op_type: cast_from_mirrored_conf
 public:
  bool has_cast_from_mirrored_conf() const;
  const ::oneflow::cfg::CastFromMirroredOpConf& cast_from_mirrored_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstCastFromMirroredOpConf> shared_const_cast_from_mirrored_conf() const;
 // oneof field op_type: model_init_v2_conf
 public:
  bool has_model_init_v2_conf() const;
  const ::oneflow::cfg::ModelInitV2OpConf& model_init_v2_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelInitV2OpConf> shared_const_model_init_v2_conf() const;
 // oneof field op_type: model_load_v2_conf
 public:
  bool has_model_load_v2_conf() const;
  const ::oneflow::cfg::ModelLoadV2OpConf& model_load_v2_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelLoadV2OpConf> shared_const_model_load_v2_conf() const;
 // oneof field op_type: model_save_v2_conf
 public:
  bool has_model_save_v2_conf() const;
  const ::oneflow::cfg::ModelSaveV2OpConf& model_save_v2_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstModelSaveV2OpConf> shared_const_model_save_v2_conf() const;
 // oneof field op_type: image_decoder_random_crop_resize_conf
 public:
  bool has_image_decoder_random_crop_resize_conf() const;
  const ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf& image_decoder_random_crop_resize_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstImageDecoderRandomCropResizeOpConf> shared_const_image_decoder_random_crop_resize_conf() const;
 // oneof field op_type: xrt_launch_conf
 public:
  bool has_xrt_launch_conf() const;
  const ::oneflow::cfg::XrtLaunchOpConf& xrt_launch_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstXrtLaunchOpConf> shared_const_xrt_launch_conf() const;
 // oneof field op_type: broadcast_to_compatible_with_conf
 public:
  bool has_broadcast_to_compatible_with_conf() const;
  const ::oneflow::cfg::BroadcastToCompatibleWithOpConf& broadcast_to_compatible_with_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBroadcastToCompatibleWithOpConf> shared_const_broadcast_to_compatible_with_conf() const;
 // oneof field op_type: feed_input_conf
 public:
  bool has_feed_input_conf() const;
  const ::oneflow::cfg::FeedInputOpConf& feed_input_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFeedInputOpConf> shared_const_feed_input_conf() const;
 // oneof field op_type: feed_variable_conf
 public:
  bool has_feed_variable_conf() const;
  const ::oneflow::cfg::FeedVariableOpConf& feed_variable_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFeedVariableOpConf> shared_const_feed_variable_conf() const;
 // oneof field op_type: fetch_output_conf
 public:
  bool has_fetch_output_conf() const;
  const ::oneflow::cfg::FetchOutputOpConf& fetch_output_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstFetchOutputOpConf> shared_const_fetch_output_conf() const;
 public:
  OpTypeCase op_type_case() const;
 protected:
  bool has_op_type() const;

 public:
  ::std::shared_ptr<ConstOperatorConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOperatorConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOperatorConf& other) const;
 protected:
  const ::std::shared_ptr<_OperatorConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OperatorConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOperatorConf
  void BuildFromProto(const PbMessage& proto_operatorconf);
  
  ::std::shared_ptr<_OperatorConf_> data_;
};

class OperatorConf final : public ConstOperatorConf {
 public:
  OperatorConf(const ::std::shared_ptr<_OperatorConf_>& data);
  OperatorConf(const OperatorConf& other);
  // enable nothrow for ::std::vector<OperatorConf> resize 
  OperatorConf(OperatorConf&&) noexcept;
  OperatorConf();
  explicit OperatorConf(const ::oneflow::OperatorConf& proto_operatorconf);

  ~OperatorConf() override;

  void InitFromProto(const PbMessage& proto_operatorconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OperatorConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OperatorConf& other) const;
  void Clear();
  void CopyFrom(const OperatorConf& other);
  OperatorConf& operator=(const OperatorConf& other);

  // required or optional field name
 public:
  void clear_name();
  void set_name(const ::std::string& value);
  ::std::string* mutable_name();
  // required or optional field device_tag
 public:
  void clear_device_tag();
  void set_device_tag(const ::std::string& value);
  ::std::string* mutable_device_tag();
  // repeated field ctrl_in_op_name
 public:
  void clear_ctrl_in_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_ctrl_in_op_name();
  ::std::string* mutable_ctrl_in_op_name(::std::size_t index);
  void add_ctrl_in_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_ctrl_in_op_name();
  void set_ctrl_in_op_name(::std::size_t index, const ::std::string& value);
  // required or optional field scope_symbol_id
 public:
  void clear_scope_symbol_id();
  void set_scope_symbol_id(const int64_t& value);
  int64_t* mutable_scope_symbol_id();
  // required or optional field stream_index_hint
 public:
  void clear_stream_index_hint();
  void set_stream_index_hint(const uint32_t& value);
  uint32_t* mutable_stream_index_hint();
  // required or optional field pass_tag
 public:
  void clear_pass_tag();
  void set_pass_tag(const ::std::string& value);
  ::std::string* mutable_pass_tag();
  void clear_decode_random_conf();
  ::oneflow::cfg::DecodeRandomOpConf* mutable_decode_random_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DecodeRandomOpConf> shared_mutable_decode_random_conf();
  void clear_copy_hd_conf();
  ::oneflow::cfg::CopyHdOpConf* mutable_copy_hd_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CopyHdOpConf> shared_mutable_copy_hd_conf();
  void clear_copy_comm_net_conf();
  ::oneflow::cfg::CopyCommNetOpConf* mutable_copy_comm_net_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CopyCommNetOpConf> shared_mutable_copy_comm_net_conf();
  void clear_boxing_conf();
  ::oneflow::cfg::BoxingOpConf* mutable_boxing_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxingOpConf> shared_mutable_boxing_conf();
  void clear_variable_conf();
  ::oneflow::cfg::VariableOpConf* mutable_variable_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> shared_mutable_variable_conf();
  void clear_tick_conf();
  ::oneflow::cfg::TickOpConf* mutable_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TickOpConf> shared_mutable_tick_conf();
  void clear_total_loss_instance_num_conf();
  ::oneflow::cfg::TotalLossInstanceNumOpConf* mutable_total_loss_instance_num_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::TotalLossInstanceNumOpConf> shared_mutable_total_loss_instance_num_conf();
  void clear_shape_elem_cnt_conf();
  ::oneflow::cfg::ShapeElemCntOpConf* mutable_shape_elem_cnt_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeElemCntOpConf> shared_mutable_shape_elem_cnt_conf();
  void clear_src_subset_tick_conf();
  ::oneflow::cfg::SrcSubsetTickOpConf* mutable_src_subset_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SrcSubsetTickOpConf> shared_mutable_src_subset_tick_conf();
  void clear_dst_subset_tick_conf();
  ::oneflow::cfg::DstSubsetTickOpConf* mutable_dst_subset_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DstSubsetTickOpConf> shared_mutable_dst_subset_tick_conf();
  void clear_source_tick_conf();
  ::oneflow::cfg::SourceTickOpConf* mutable_source_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SourceTickOpConf> shared_mutable_source_tick_conf();
  void clear_sink_tick_conf();
  ::oneflow::cfg::SinkTickOpConf* mutable_sink_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SinkTickOpConf> shared_mutable_sink_tick_conf();
  void clear_input_conf();
  ::oneflow::cfg::InputOpConf* mutable_input_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::InputOpConf> shared_mutable_input_conf();
  void clear_output_conf();
  ::oneflow::cfg::OutputOpConf* mutable_output_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OutputOpConf> shared_mutable_output_conf();
  void clear_wait_and_send_ids_conf();
  ::oneflow::cfg::WaitAndSendIdsOpConf* mutable_wait_and_send_ids_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::WaitAndSendIdsOpConf> shared_mutable_wait_and_send_ids_conf();
  void clear_reentrant_lock_conf();
  ::oneflow::cfg::ReentrantLockOpConf* mutable_reentrant_lock_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReentrantLockOpConf> shared_mutable_reentrant_lock_conf();
  void clear_callback_notify_conf();
  ::oneflow::cfg::CallbackNotifyOpConf* mutable_callback_notify_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CallbackNotifyOpConf> shared_mutable_callback_notify_conf();
  void clear_foreign_input_conf();
  ::oneflow::cfg::ForeignInputOpConf* mutable_foreign_input_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ForeignInputOpConf> shared_mutable_foreign_input_conf();
  void clear_foreign_output_conf();
  ::oneflow::cfg::ForeignOutputOpConf* mutable_foreign_output_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ForeignOutputOpConf> shared_mutable_foreign_output_conf();
  void clear_acc_tick_conf();
  ::oneflow::cfg::AccTickOpConf* mutable_acc_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AccTickOpConf> shared_mutable_acc_tick_conf();
  void clear_return_conf();
  ::oneflow::cfg::ReturnOpConf* mutable_return_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ReturnOpConf> shared_mutable_return_conf();
  void clear_foreign_watch_conf();
  ::oneflow::cfg::ForeignWatchOpConf* mutable_foreign_watch_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ForeignWatchOpConf> shared_mutable_foreign_watch_conf();
  void clear_distribute_concat_conf();
  ::oneflow::cfg::DistributeConcatOpConf* mutable_distribute_concat_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DistributeConcatOpConf> shared_mutable_distribute_concat_conf();
  void clear_distribute_split_conf();
  ::oneflow::cfg::DistributeSplitOpConf* mutable_distribute_split_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DistributeSplitOpConf> shared_mutable_distribute_split_conf();
  void clear_distribute_clone_conf();
  ::oneflow::cfg::DistributeCloneOpConf* mutable_distribute_clone_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DistributeCloneOpConf> shared_mutable_distribute_clone_conf();
  void clear_distribute_add_conf();
  ::oneflow::cfg::DistributeAddOpConf* mutable_distribute_add_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DistributeAddOpConf> shared_mutable_distribute_add_conf();
  void clear_device_tick_conf();
  ::oneflow::cfg::DeviceTickOpConf* mutable_device_tick_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DeviceTickOpConf> shared_mutable_device_tick_conf();
  void clear_slice_boxing_copy_conf();
  ::oneflow::cfg::SliceBoxingCopyOpConf* mutable_slice_boxing_copy_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SliceBoxingCopyOpConf> shared_mutable_slice_boxing_copy_conf();
  void clear_slice_boxing_add_conf();
  ::oneflow::cfg::SliceBoxingAddOpConf* mutable_slice_boxing_add_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SliceBoxingAddOpConf> shared_mutable_slice_boxing_add_conf();
  void clear_collective_boxing_generic_conf();
  ::oneflow::cfg::CollectiveBoxingGenericOpConf* mutable_collective_boxing_generic_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingGenericOpConf> shared_mutable_collective_boxing_generic_conf();
  void clear_boxing_identity_conf();
  ::oneflow::cfg::BoxingIdentityOpConf* mutable_boxing_identity_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxingIdentityOpConf> shared_mutable_boxing_identity_conf();
  void clear_collective_boxing_pack_conf();
  ::oneflow::cfg::CollectiveBoxingPackOpConf* mutable_collective_boxing_pack_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingPackOpConf> shared_mutable_collective_boxing_pack_conf();
  void clear_collective_boxing_unpack_conf();
  ::oneflow::cfg::CollectiveBoxingUnpackOpConf* mutable_collective_boxing_unpack_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CollectiveBoxingUnpackOpConf> shared_mutable_collective_boxing_unpack_conf();
  void clear_boxing_zeros_conf();
  ::oneflow::cfg::BoxingZerosOpConf* mutable_boxing_zeros_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BoxingZerosOpConf> shared_mutable_boxing_zeros_conf();
  void clear_user_conf();
  ::oneflow::cfg::UserOpConf* mutable_user_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::UserOpConf> shared_mutable_user_conf();
  void clear_dynamic_reshape_conf();
  ::oneflow::cfg::DynamicReshapeOpConf* mutable_dynamic_reshape_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DynamicReshapeOpConf> shared_mutable_dynamic_reshape_conf();
  void clear_dynamic_reshape_like_conf();
  ::oneflow::cfg::DynamicReshapeLikeOpConf* mutable_dynamic_reshape_like_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::DynamicReshapeLikeOpConf> shared_mutable_dynamic_reshape_like_conf();
  void clear_identity_conf();
  ::oneflow::cfg::IdentityOpConf* mutable_identity_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::IdentityOpConf> shared_mutable_identity_conf();
  void clear_case_conf();
  ::oneflow::cfg::CaseOpConf* mutable_case_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CaseOpConf> shared_mutable_case_conf();
  void clear_esac_conf();
  ::oneflow::cfg::EsacOpConf* mutable_esac_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::EsacOpConf> shared_mutable_esac_conf();
  void clear_model_init_conf();
  ::oneflow::cfg::ModelInitOpConf* mutable_model_init_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelInitOpConf> shared_mutable_model_init_conf();
  void clear_assign_conf();
  ::oneflow::cfg::AssignOpConf* mutable_assign_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::AssignOpConf> shared_mutable_assign_conf();
  void clear_model_save_conf();
  ::oneflow::cfg::ModelSaveOpConf* mutable_model_save_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelSaveOpConf> shared_mutable_model_save_conf();
  void clear_learning_rate_schedule_conf();
  ::oneflow::cfg::LearningRateScheduleOpConf* mutable_learning_rate_schedule_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::LearningRateScheduleOpConf> shared_mutable_learning_rate_schedule_conf();
  void clear_model_load_conf();
  ::oneflow::cfg::ModelLoadOpConf* mutable_model_load_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelLoadOpConf> shared_mutable_model_load_conf();
  void clear_constant_like_conf();
  ::oneflow::cfg::ConstantLikeOpConf* mutable_constant_like_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstantLikeOpConf> shared_mutable_constant_like_conf();
  void clear_sync_dynamic_resize_conf();
  ::oneflow::cfg::SyncDynamicResizeOpConf* mutable_sync_dynamic_resize_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SyncDynamicResizeOpConf> shared_mutable_sync_dynamic_resize_conf();
  void clear_copy_conf();
  ::oneflow::cfg::CopyOpConf* mutable_copy_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CopyOpConf> shared_mutable_copy_conf();
  void clear_cast_to_mirrored_conf();
  ::oneflow::cfg::CastToMirroredOpConf* mutable_cast_to_mirrored_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CastToMirroredOpConf> shared_mutable_cast_to_mirrored_conf();
  void clear_cast_from_mirrored_conf();
  ::oneflow::cfg::CastFromMirroredOpConf* mutable_cast_from_mirrored_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::CastFromMirroredOpConf> shared_mutable_cast_from_mirrored_conf();
  void clear_model_init_v2_conf();
  ::oneflow::cfg::ModelInitV2OpConf* mutable_model_init_v2_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelInitV2OpConf> shared_mutable_model_init_v2_conf();
  void clear_model_load_v2_conf();
  ::oneflow::cfg::ModelLoadV2OpConf* mutable_model_load_v2_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelLoadV2OpConf> shared_mutable_model_load_v2_conf();
  void clear_model_save_v2_conf();
  ::oneflow::cfg::ModelSaveV2OpConf* mutable_model_save_v2_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ModelSaveV2OpConf> shared_mutable_model_save_v2_conf();
  void clear_image_decoder_random_crop_resize_conf();
  ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf* mutable_image_decoder_random_crop_resize_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ImageDecoderRandomCropResizeOpConf> shared_mutable_image_decoder_random_crop_resize_conf();
  void clear_xrt_launch_conf();
  ::oneflow::cfg::XrtLaunchOpConf* mutable_xrt_launch_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf> shared_mutable_xrt_launch_conf();
  void clear_broadcast_to_compatible_with_conf();
  ::oneflow::cfg::BroadcastToCompatibleWithOpConf* mutable_broadcast_to_compatible_with_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BroadcastToCompatibleWithOpConf> shared_mutable_broadcast_to_compatible_with_conf();
  void clear_feed_input_conf();
  ::oneflow::cfg::FeedInputOpConf* mutable_feed_input_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FeedInputOpConf> shared_mutable_feed_input_conf();
  void clear_feed_variable_conf();
  ::oneflow::cfg::FeedVariableOpConf* mutable_feed_variable_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FeedVariableOpConf> shared_mutable_feed_variable_conf();
  void clear_fetch_output_conf();
  ::oneflow::cfg::FetchOutputOpConf* mutable_fetch_output_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::FetchOutputOpConf> shared_mutable_fetch_output_conf();

  ::std::shared_ptr<OperatorConf> __SharedMutable__();
};


class ConstOpNameRelations : public ::oneflow::cfg::Message {
 public:

  class _OpNameRelations_ {
   public:
    _OpNameRelations_();
    explicit _OpNameRelations_(const _OpNameRelations_& other);
    explicit _OpNameRelations_(_OpNameRelations_&& other);
    _OpNameRelations_(const ::oneflow::OpNameRelations& proto_opnamerelations);
    ~_OpNameRelations_();

    void InitFromProto(const ::oneflow::OpNameRelations& proto_opnamerelations);

    void ToProto(::oneflow::OpNameRelations* proto_opnamerelations) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNameRelations_& other);
  
     public:
    ::std::size_t src_op_name2dst_op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& src_op_name2dst_op_name() const;

    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ * mutable_src_op_name2dst_op_name();

    const ::std::string& src_op_name2dst_op_name(::std::string key) const;

    void clear_src_op_name2dst_op_name();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> src_op_name2dst_op_name_;
         
   public:
    int compare(const _OpNameRelations_& other);

    bool operator==(const _OpNameRelations_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNameRelations_& other) const;
  };

  ConstOpNameRelations(const ::std::shared_ptr<_OpNameRelations_>& data);
  ConstOpNameRelations(const ConstOpNameRelations&);
  ConstOpNameRelations(ConstOpNameRelations&&) noexcept;
  ConstOpNameRelations();
  ConstOpNameRelations(const ::oneflow::OpNameRelations& proto_opnamerelations);
  virtual ~ConstOpNameRelations() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnamerelations) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field src_op_name2dst_op_name
 public:
  ::std::size_t src_op_name2dst_op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& src_op_name2dst_op_name() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> shared_const_src_op_name2dst_op_name() const;

 public:
  ::std::shared_ptr<ConstOpNameRelations> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNameRelations& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNameRelations& other) const;
 protected:
  const ::std::shared_ptr<_OpNameRelations_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNameRelations_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNameRelations
  void BuildFromProto(const PbMessage& proto_opnamerelations);
  
  ::std::shared_ptr<_OpNameRelations_> data_;
};

class OpNameRelations final : public ConstOpNameRelations {
 public:
  OpNameRelations(const ::std::shared_ptr<_OpNameRelations_>& data);
  OpNameRelations(const OpNameRelations& other);
  // enable nothrow for ::std::vector<OpNameRelations> resize 
  OpNameRelations(OpNameRelations&&) noexcept;
  OpNameRelations();
  explicit OpNameRelations(const ::oneflow::OpNameRelations& proto_opnamerelations);

  ~OpNameRelations() override;

  void InitFromProto(const PbMessage& proto_opnamerelations) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNameRelations& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNameRelations& other) const;
  void Clear();
  void CopyFrom(const OpNameRelations& other);
  OpNameRelations& operator=(const OpNameRelations& other);

  // repeated field src_op_name2dst_op_name
 public:
  void clear_src_op_name2dst_op_name();

  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ & src_op_name2dst_op_name() const;

  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_* mutable_src_op_name2dst_op_name();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> shared_mutable_src_op_name2dst_op_name();

  ::std::shared_ptr<OpNameRelations> __SharedMutable__();
};


class ConstOpNameGroups_OpNameGroup : public ::oneflow::cfg::Message {
 public:

  class _OpNameGroups_OpNameGroup_ {
   public:
    _OpNameGroups_OpNameGroup_();
    explicit _OpNameGroups_OpNameGroup_(const _OpNameGroups_OpNameGroup_& other);
    explicit _OpNameGroups_OpNameGroup_(_OpNameGroups_OpNameGroup_&& other);
    _OpNameGroups_OpNameGroup_(const ::oneflow::OpNameGroups_OpNameGroup& proto_opnamegroups_opnamegroup);
    ~_OpNameGroups_OpNameGroup_();

    void InitFromProto(const ::oneflow::OpNameGroups_OpNameGroup& proto_opnamegroups_opnamegroup);

    void ToProto(::oneflow::OpNameGroups_OpNameGroup* proto_opnamegroups_opnamegroup) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNameGroups_OpNameGroup_& other);
  
      // repeated field op_name
   public:
    ::std::size_t op_name_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& op_name() const;
    const ::std::string& op_name(::std::size_t index) const;
    void clear_op_name();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_op_name();
    ::std::string* mutable_op_name(::std::size_t index);
      void add_op_name(const ::std::string& value);
    void set_op_name(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> op_name_;
         
   public:
    int compare(const _OpNameGroups_OpNameGroup_& other);

    bool operator==(const _OpNameGroups_OpNameGroup_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNameGroups_OpNameGroup_& other) const;
  };

  ConstOpNameGroups_OpNameGroup(const ::std::shared_ptr<_OpNameGroups_OpNameGroup_>& data);
  ConstOpNameGroups_OpNameGroup(const ConstOpNameGroups_OpNameGroup&);
  ConstOpNameGroups_OpNameGroup(ConstOpNameGroups_OpNameGroup&&) noexcept;
  ConstOpNameGroups_OpNameGroup();
  ConstOpNameGroups_OpNameGroup(const ::oneflow::OpNameGroups_OpNameGroup& proto_opnamegroups_opnamegroup);
  virtual ~ConstOpNameGroups_OpNameGroup() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnamegroups_opnamegroup) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_name
 public:
  ::std::size_t op_name_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& op_name() const;
  const ::std::string& op_name(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_const_op_name() const;

 public:
  ::std::shared_ptr<ConstOpNameGroups_OpNameGroup> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNameGroups_OpNameGroup& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNameGroups_OpNameGroup& other) const;
 protected:
  const ::std::shared_ptr<_OpNameGroups_OpNameGroup_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNameGroups_OpNameGroup_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNameGroups_OpNameGroup
  void BuildFromProto(const PbMessage& proto_opnamegroups_opnamegroup);
  
  ::std::shared_ptr<_OpNameGroups_OpNameGroup_> data_;
};

class OpNameGroups_OpNameGroup final : public ConstOpNameGroups_OpNameGroup {
 public:
  OpNameGroups_OpNameGroup(const ::std::shared_ptr<_OpNameGroups_OpNameGroup_>& data);
  OpNameGroups_OpNameGroup(const OpNameGroups_OpNameGroup& other);
  // enable nothrow for ::std::vector<OpNameGroups_OpNameGroup> resize 
  OpNameGroups_OpNameGroup(OpNameGroups_OpNameGroup&&) noexcept;
  OpNameGroups_OpNameGroup();
  explicit OpNameGroups_OpNameGroup(const ::oneflow::OpNameGroups_OpNameGroup& proto_opnamegroups_opnamegroup);

  ~OpNameGroups_OpNameGroup() override;

  void InitFromProto(const PbMessage& proto_opnamegroups_opnamegroup) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNameGroups_OpNameGroup& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNameGroups_OpNameGroup& other) const;
  void Clear();
  void CopyFrom(const OpNameGroups_OpNameGroup& other);
  OpNameGroups_OpNameGroup& operator=(const OpNameGroups_OpNameGroup& other);

  // repeated field op_name
 public:
  void clear_op_name();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_* mutable_op_name();
  ::std::string* mutable_op_name(::std::size_t index);
  void add_op_name(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> shared_mutable_op_name();
  void set_op_name(::std::size_t index, const ::std::string& value);

  ::std::shared_ptr<OpNameGroups_OpNameGroup> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_;

class ConstOpNameGroups : public ::oneflow::cfg::Message {
 public:

  class _OpNameGroups_ {
   public:
    _OpNameGroups_();
    explicit _OpNameGroups_(const _OpNameGroups_& other);
    explicit _OpNameGroups_(_OpNameGroups_&& other);
    _OpNameGroups_(const ::oneflow::OpNameGroups& proto_opnamegroups);
    ~_OpNameGroups_();

    void InitFromProto(const ::oneflow::OpNameGroups& proto_opnamegroups);

    void ToProto(::oneflow::OpNameGroups* proto_opnamegroups) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNameGroups_& other);
  
      // repeated field op_name_group
   public:
    ::std::size_t op_name_group_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& op_name_group() const;
    const ::oneflow::cfg::OpNameGroups_OpNameGroup& op_name_group(::std::size_t index) const;
    void clear_op_name_group();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_* mutable_op_name_group();
    ::oneflow::cfg::OpNameGroups_OpNameGroup* mutable_op_name_group(::std::size_t index);
      ::oneflow::cfg::OpNameGroups_OpNameGroup* add_op_name_group();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> op_name_group_;
         
   public:
    int compare(const _OpNameGroups_& other);

    bool operator==(const _OpNameGroups_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNameGroups_& other) const;
  };

  ConstOpNameGroups(const ::std::shared_ptr<_OpNameGroups_>& data);
  ConstOpNameGroups(const ConstOpNameGroups&);
  ConstOpNameGroups(ConstOpNameGroups&&) noexcept;
  ConstOpNameGroups();
  ConstOpNameGroups(const ::oneflow::OpNameGroups& proto_opnamegroups);
  virtual ~ConstOpNameGroups() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnamegroups) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_name_group
 public:
  ::std::size_t op_name_group_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& op_name_group() const;
  const ::oneflow::cfg::OpNameGroups_OpNameGroup& op_name_group(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> shared_const_op_name_group() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOpNameGroups_OpNameGroup> shared_const_op_name_group(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstOpNameGroups> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNameGroups& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNameGroups& other) const;
 protected:
  const ::std::shared_ptr<_OpNameGroups_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNameGroups_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNameGroups
  void BuildFromProto(const PbMessage& proto_opnamegroups);
  
  ::std::shared_ptr<_OpNameGroups_> data_;
};

class OpNameGroups final : public ConstOpNameGroups {
 public:
  OpNameGroups(const ::std::shared_ptr<_OpNameGroups_>& data);
  OpNameGroups(const OpNameGroups& other);
  // enable nothrow for ::std::vector<OpNameGroups> resize 
  OpNameGroups(OpNameGroups&&) noexcept;
  OpNameGroups();
  explicit OpNameGroups(const ::oneflow::OpNameGroups& proto_opnamegroups);

  ~OpNameGroups() override;

  void InitFromProto(const PbMessage& proto_opnamegroups) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNameGroups& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNameGroups& other) const;
  void Clear();
  void CopyFrom(const OpNameGroups& other);
  OpNameGroups& operator=(const OpNameGroups& other);

  // repeated field op_name_group
 public:
  void clear_op_name_group();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_* mutable_op_name_group();
  ::oneflow::cfg::OpNameGroups_OpNameGroup* mutable_op_name_group(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> shared_mutable_op_name_group();
  ::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> shared_mutable_op_name_group(::std::size_t index);
  ::oneflow::cfg::OpNameGroups_OpNameGroup* add_op_name_group();

  ::std::shared_ptr<OpNameGroups> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};

























// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_ : public ::oneflow::cfg::_RepeatedField_<int32_t> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_(const ::std::shared_ptr<::std::vector<int32_t>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_int32_t_> __SharedMutable__();
};























































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::Int64List> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::Int64List>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstInt64List> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::Int64List>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_Int64List_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::Int64List> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::Int64List> __SharedMutable__(::std::size_t index);
};





































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::VariableOpConf> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::VariableOpConf>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstVariableOpConf> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::VariableOpConf>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_VariableOpConf_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::VariableOpConf> __SharedMutable__(::std::size_t index);
};


































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::TensorSliceViewProto> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::TensorSliceViewProto>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstTensorSliceViewProto> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::TensorSliceViewProto>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_TensorSliceViewProto_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::TensorSliceViewProto> __SharedMutable__(::std::size_t index);
};













// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::XrtLaunchOpConf_Argument> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::XrtLaunchOpConf_Argument>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstXrtLaunchOpConf_Argument> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::XrtLaunchOpConf_Argument>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_XrtLaunchOpConf_Argument_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::XrtLaunchOpConf_Argument> __SharedMutable__(::std::size_t index);
};

// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OperatorConf> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OperatorConf>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OperatorConf_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> __SharedMutable__(::std::size_t index);
};




// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ : public ::oneflow::cfg::_MapField_<::std::string, bool> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other) const;
  // used by pybind11 only
  const bool& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_(const ::std::shared_ptr<::std::map<::std::string, bool>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_bool_> __SharedMutable__();

  void Set(const ::std::string& key, const bool& value);
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ : public ::oneflow::cfg::_MapField_<::std::string, ::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_(const ::std::shared_ptr<::std::map<::std::string, ::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other) const;
  // used by pybind11 only
  const ::std::string& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_(const ::std::shared_ptr<::std::map<::std::string, ::std::string>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string___std__string_> __SharedMutable__();

  void Set(const ::std::string& key, const ::std::string& value);
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::SbpSignature> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpSignature>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::SbpSignature& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstSbpSignature> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_, ConstSbpSignature>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::SbpSignature>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::SbpSignature> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_SbpSignature_, ::oneflow::cfg::SbpSignature>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::BlobDescProto> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::BlobDescProto& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstBlobDescProto> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_, ConstBlobDescProto>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::BlobDescProto>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::BlobDescProto> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__MapField___std__string_BlobDescProto_, ::oneflow::cfg::BlobDescProto>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};

















































// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpNameGroups_OpNameGroup> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpNameGroups_OpNameGroup>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOpNameGroups_OpNameGroup> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpNameGroups_OpNameGroup>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H__RepeatedField_OpNameGroups_OpNameGroup_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::OpNameGroups_OpNameGroup> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {

template<>
struct hash<::oneflow::cfg::ActivationType> {
  std::size_t operator()(::oneflow::cfg::ActivationType enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};


template<>
struct hash<::oneflow::cfg::ConstDistributeConcatOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDistributeConcatOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DistributeConcatOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DistributeConcatOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDistributeSplitOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDistributeSplitOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DistributeSplitOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DistributeSplitOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDistributeCloneOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDistributeCloneOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DistributeCloneOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DistributeCloneOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDistributeAddOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDistributeAddOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DistributeAddOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DistributeAddOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCopyCommNetOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCopyCommNetOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CopyCommNetOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CopyCommNetOpConf& s) const {
    return s.__CalcHash__();
  }
};
template<>
struct hash<::oneflow::cfg::CopyHdOpConf_Type> {
  std::size_t operator()(::oneflow::cfg::CopyHdOpConf_Type enum_value) const {
    return static_cast<std::size_t>(enum_value);
  }
};

template<>
struct hash<::oneflow::cfg::ConstCopyHdOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCopyHdOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CopyHdOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CopyHdOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxConcatConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxConcatConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxConcatConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxConcatConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxAddConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxAddConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxAddConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxAddConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxSplitConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxSplitConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxSplitConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxSplitConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxCloneConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxCloneConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxCloneConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxCloneConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxingOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxingOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxingOpConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxingOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDynamicReshapeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDynamicReshapeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DynamicReshapeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DynamicReshapeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDynamicReshapeLikeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDynamicReshapeLikeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DynamicReshapeLikeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DynamicReshapeLikeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFeedInputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstFeedInputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FeedInputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::FeedInputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFeedVariableOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstFeedVariableOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FeedVariableOpConf> {
  std::size_t operator()(const ::oneflow::cfg::FeedVariableOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstFetchOutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstFetchOutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::FetchOutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::FetchOutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstInputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstInputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::InputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstForeignInputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstForeignInputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ForeignInputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ForeignInputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstReturnOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstReturnOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ReturnOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ReturnOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstOutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::OutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstForeignOutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstForeignOutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ForeignOutputOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ForeignOutputOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstForeignWatchOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstForeignWatchOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ForeignWatchOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ForeignWatchOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstVariableOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstVariableOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::VariableOpConf> {
  std::size_t operator()(const ::oneflow::cfg::VariableOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDecodeRandomOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDecodeRandomOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DecodeRandomOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DecodeRandomOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::TickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDeviceTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDeviceTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DeviceTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DeviceTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstWaitAndSendIdsOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstWaitAndSendIdsOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::WaitAndSendIdsOpConf> {
  std::size_t operator()(const ::oneflow::cfg::WaitAndSendIdsOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCallbackNotifyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCallbackNotifyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CallbackNotifyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CallbackNotifyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstReentrantLockOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstReentrantLockOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ReentrantLockOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ReentrantLockOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSrcSubsetTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSrcSubsetTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SrcSubsetTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SrcSubsetTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstDstSubsetTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstDstSubsetTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::DstSubsetTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::DstSubsetTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSourceTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSourceTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SourceTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SourceTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSinkTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSinkTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SinkTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SinkTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstTotalLossInstanceNumOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstTotalLossInstanceNumOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::TotalLossInstanceNumOpConf> {
  std::size_t operator()(const ::oneflow::cfg::TotalLossInstanceNumOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstShapeElemCntAxisConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeElemCntAxisConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeElemCntAxisConf> {
  std::size_t operator()(const ::oneflow::cfg::ShapeElemCntAxisConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstShapeElemCntRangeAxisConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeElemCntRangeAxisConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeElemCntRangeAxisConf> {
  std::size_t operator()(const ::oneflow::cfg::ShapeElemCntRangeAxisConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstShapeElemCntOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstShapeElemCntOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ShapeElemCntOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ShapeElemCntOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAccTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstAccTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AccTickOpConf> {
  std::size_t operator()(const ::oneflow::cfg::AccTickOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelInitOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelInitOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelInitOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelInitOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelLoadOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelLoadOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelLoadOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelLoadOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstIdentityOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstIdentityOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::IdentityOpConf> {
  std::size_t operator()(const ::oneflow::cfg::IdentityOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCopyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCopyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CopyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CopyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCastToMirroredOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCastToMirroredOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CastToMirroredOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CastToMirroredOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCastFromMirroredOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCastFromMirroredOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CastFromMirroredOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CastFromMirroredOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCaseOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCaseOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CaseOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CaseOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstEsacOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstEsacOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::EsacOpConf> {
  std::size_t operator()(const ::oneflow::cfg::EsacOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstAssignOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstAssignOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::AssignOpConf> {
  std::size_t operator()(const ::oneflow::cfg::AssignOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelSaveOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelSaveOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelSaveOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelSaveOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstLearningRateScheduleOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstLearningRateScheduleOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::LearningRateScheduleOpConf> {
  std::size_t operator()(const ::oneflow::cfg::LearningRateScheduleOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSliceBoxingConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSliceBoxingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SliceBoxingConf> {
  std::size_t operator()(const ::oneflow::cfg::SliceBoxingConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSliceBoxingCopyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSliceBoxingCopyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SliceBoxingCopyOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SliceBoxingCopyOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSliceBoxingAddOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSliceBoxingAddOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SliceBoxingAddOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SliceBoxingAddOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtLaunchOpConf_Argument> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtLaunchOpConf_Argument& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtLaunchOpConf_Argument> {
  std::size_t operator()(const ::oneflow::cfg::XrtLaunchOpConf_Argument& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtLaunchOpConf_Function> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtLaunchOpConf_Function& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtLaunchOpConf_Function> {
  std::size_t operator()(const ::oneflow::cfg::XrtLaunchOpConf_Function& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstXrtLaunchOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstXrtLaunchOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::XrtLaunchOpConf> {
  std::size_t operator()(const ::oneflow::cfg::XrtLaunchOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelInitV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelInitV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelInitV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelInitV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelLoadV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelLoadV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelLoadV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelLoadV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstModelSaveV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstModelSaveV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ModelSaveV2OpConf> {
  std::size_t operator()(const ::oneflow::cfg::ModelSaveV2OpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstConstantLikeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstConstantLikeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstantLikeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstantLikeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstSyncDynamicResizeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstSyncDynamicResizeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::SyncDynamicResizeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::SyncDynamicResizeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBroadcastToCompatibleWithOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBroadcastToCompatibleWithOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BroadcastToCompatibleWithOpConf> {
  std::size_t operator()(const ::oneflow::cfg::BroadcastToCompatibleWithOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCollectiveBoxingGenericOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCollectiveBoxingGenericOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CollectiveBoxingGenericOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CollectiveBoxingGenericOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxingIdentityOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxingIdentityOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxingIdentityOpConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxingIdentityOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCollectiveBoxingPackOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCollectiveBoxingPackOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CollectiveBoxingPackOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CollectiveBoxingPackOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstCollectiveBoxingUnpackOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstCollectiveBoxingUnpackOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::CollectiveBoxingUnpackOpConf> {
  std::size_t operator()(const ::oneflow::cfg::CollectiveBoxingUnpackOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstImageDecoderRandomCropResizeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstImageDecoderRandomCropResizeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ImageDecoderRandomCropResizeOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ImageDecoderRandomCropResizeOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstBoxingZerosOpConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstBoxingZerosOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::BoxingZerosOpConf> {
  std::size_t operator()(const ::oneflow::cfg::BoxingZerosOpConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOperatorConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstOperatorConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OperatorConf> {
  std::size_t operator()(const ::oneflow::cfg::OperatorConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpNameRelations> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNameRelations& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNameRelations> {
  std::size_t operator()(const ::oneflow::cfg::OpNameRelations& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpNameGroups_OpNameGroup> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNameGroups_OpNameGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNameGroups_OpNameGroup> {
  std::size_t operator()(const ::oneflow::cfg::OpNameGroups_OpNameGroup& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpNameGroups> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNameGroups& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNameGroups> {
  std::size_t operator()(const ::oneflow::cfg::OpNameGroups& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_OPERATOR_OP_CONF_CFG_H_