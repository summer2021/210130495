#ifndef CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H_
#define CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstArgModifierSignature;
class ArgModifierSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstArgSignature;
class ArgSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstBlobBackwardUsedSignature;
class BlobBackwardUsedSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstBlobDescSignature;
class BlobDescSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstBlobLastUsedSignature;
class BlobLastUsedSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstMirroredSignature;
class MirroredSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstOperatorConf;
class OperatorConf;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelConfSignature;
class ParallelConfSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelDistributionSignature;
class ParallelDistributionSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelSignature;
class ParallelSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstSbpSignature;
class SbpSignature;
}
}

namespace oneflow {

// forward declare proto class;
class OpAttribute;
class OpAttributeList;

namespace cfg {


class OpAttribute;
class ConstOpAttribute;

class OpAttributeList;
class ConstOpAttributeList;


class _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_;

class ConstOpAttribute : public ::oneflow::cfg::Message {
 public:

  class _OpAttribute_ {
   public:
    _OpAttribute_();
    explicit _OpAttribute_(const _OpAttribute_& other);
    explicit _OpAttribute_(_OpAttribute_&& other);
    _OpAttribute_(const ::oneflow::OpAttribute& proto_opattribute);
    ~_OpAttribute_();

    void InitFromProto(const ::oneflow::OpAttribute& proto_opattribute);

    void ToProto(::oneflow::OpAttribute* proto_opattribute) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpAttribute_& other);
  
      // repeated field input_bns
   public:
    ::std::size_t input_bns_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& input_bns() const;
    const ::std::string& input_bns(::std::size_t index) const;
    void clear_input_bns();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_input_bns();
    ::std::string* mutable_input_bns(::std::size_t index);
      void add_input_bns(const ::std::string& value);
    void set_input_bns(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> input_bns_;
    
      // repeated field output_bns
   public:
    ::std::size_t output_bns_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& output_bns() const;
    const ::std::string& output_bns(::std::size_t index) const;
    void clear_output_bns();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_output_bns();
    ::std::string* mutable_output_bns(::std::size_t index);
      void add_output_bns(const ::std::string& value);
    void set_output_bns(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> output_bns_;
    
      // repeated field tmp_bns
   public:
    ::std::size_t tmp_bns_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& tmp_bns() const;
    const ::std::string& tmp_bns(::std::size_t index) const;
    void clear_tmp_bns();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_tmp_bns();
    ::std::string* mutable_tmp_bns(::std::size_t index);
      void add_tmp_bns(const ::std::string& value);
    void set_tmp_bns(::std::size_t index, const ::std::string& value);
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> tmp_bns_;
    
      // optional field op_conf
     public:
    bool has_op_conf() const;
    const ::oneflow::cfg::OperatorConf& op_conf() const;
    void clear_op_conf();
    ::oneflow::cfg::OperatorConf* mutable_op_conf();
   protected:
    bool has_op_conf_ = false;
    ::std::shared_ptr<::oneflow::cfg::OperatorConf> op_conf_;
      
      // optional field arg_signature
     public:
    bool has_arg_signature() const;
    const ::oneflow::cfg::ArgSignature& arg_signature() const;
    void clear_arg_signature();
    ::oneflow::cfg::ArgSignature* mutable_arg_signature();
   protected:
    bool has_arg_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ArgSignature> arg_signature_;
      
      // optional field arg_modifier_signature
     public:
    bool has_arg_modifier_signature() const;
    const ::oneflow::cfg::ArgModifierSignature& arg_modifier_signature() const;
    void clear_arg_modifier_signature();
    ::oneflow::cfg::ArgModifierSignature* mutable_arg_modifier_signature();
   protected:
    bool has_arg_modifier_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ArgModifierSignature> arg_modifier_signature_;
      
      // optional field blob_last_used_signature
     public:
    bool has_blob_last_used_signature() const;
    const ::oneflow::cfg::BlobLastUsedSignature& blob_last_used_signature() const;
    void clear_blob_last_used_signature();
    ::oneflow::cfg::BlobLastUsedSignature* mutable_blob_last_used_signature();
   protected:
    bool has_blob_last_used_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::BlobLastUsedSignature> blob_last_used_signature_;
      
      // optional field blob_backward_used_signature
     public:
    bool has_blob_backward_used_signature() const;
    const ::oneflow::cfg::BlobBackwardUsedSignature& blob_backward_used_signature() const;
    void clear_blob_backward_used_signature();
    ::oneflow::cfg::BlobBackwardUsedSignature* mutable_blob_backward_used_signature();
   protected:
    bool has_blob_backward_used_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::BlobBackwardUsedSignature> blob_backward_used_signature_;
      
      // optional field sbp_signature
     public:
    bool has_sbp_signature() const;
    const ::oneflow::cfg::SbpSignature& sbp_signature() const;
    void clear_sbp_signature();
    ::oneflow::cfg::SbpSignature* mutable_sbp_signature();
   protected:
    bool has_sbp_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpSignature> sbp_signature_;
      
      // optional field mirrored_signature
     public:
    bool has_mirrored_signature() const;
    const ::oneflow::cfg::MirroredSignature& mirrored_signature() const;
    void clear_mirrored_signature();
    ::oneflow::cfg::MirroredSignature* mutable_mirrored_signature();
   protected:
    bool has_mirrored_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::MirroredSignature> mirrored_signature_;
      
      // optional field logical_blob_desc_signature
     public:
    bool has_logical_blob_desc_signature() const;
    const ::oneflow::cfg::BlobDescSignature& logical_blob_desc_signature() const;
    void clear_logical_blob_desc_signature();
    ::oneflow::cfg::BlobDescSignature* mutable_logical_blob_desc_signature();
   protected:
    bool has_logical_blob_desc_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> logical_blob_desc_signature_;
      
      // optional field parallel_signature
     public:
    bool has_parallel_signature() const;
    const ::oneflow::cfg::ParallelSignature& parallel_signature() const;
    void clear_parallel_signature();
    ::oneflow::cfg::ParallelSignature* mutable_parallel_signature();
   protected:
    bool has_parallel_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelSignature> parallel_signature_;
      
      // optional field parallel_conf_signature
     public:
    bool has_parallel_conf_signature() const;
    const ::oneflow::cfg::ParallelConfSignature& parallel_conf_signature() const;
    void clear_parallel_conf_signature();
    ::oneflow::cfg::ParallelConfSignature* mutable_parallel_conf_signature();
   protected:
    bool has_parallel_conf_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelConfSignature> parallel_conf_signature_;
      
      // optional field parallel_distribution_signature
     public:
    bool has_parallel_distribution_signature() const;
    const ::oneflow::cfg::ParallelDistributionSignature& parallel_distribution_signature() const;
    void clear_parallel_distribution_signature();
    ::oneflow::cfg::ParallelDistributionSignature* mutable_parallel_distribution_signature();
   protected:
    bool has_parallel_distribution_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelDistributionSignature> parallel_distribution_signature_;
           
   public:
    int compare(const _OpAttribute_& other);

    bool operator==(const _OpAttribute_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpAttribute_& other) const;
  };

  ConstOpAttribute(const ::std::shared_ptr<_OpAttribute_>& data);
  ConstOpAttribute(const ConstOpAttribute&);
  ConstOpAttribute(ConstOpAttribute&&) noexcept;
  ConstOpAttribute();
  ConstOpAttribute(const ::oneflow::OpAttribute& proto_opattribute);
  virtual ~ConstOpAttribute() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opattribute) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field input_bns
 public:
  ::std::size_t input_bns_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& input_bns() const;
  const ::std::string& input_bns(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_const_input_bns() const;
  // repeated field output_bns
 public:
  ::std::size_t output_bns_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& output_bns() const;
  const ::std::string& output_bns(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_const_output_bns() const;
  // repeated field tmp_bns
 public:
  ::std::size_t tmp_bns_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& tmp_bns() const;
  const ::std::string& tmp_bns(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_const_tmp_bns() const;
  // required or optional field op_conf
 public:
  bool has_op_conf() const;
  const ::oneflow::cfg::OperatorConf& op_conf() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> shared_const_op_conf() const;
  // required or optional field arg_signature
 public:
  bool has_arg_signature() const;
  const ::oneflow::cfg::ArgSignature& arg_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstArgSignature> shared_const_arg_signature() const;
  // required or optional field arg_modifier_signature
 public:
  bool has_arg_modifier_signature() const;
  const ::oneflow::cfg::ArgModifierSignature& arg_modifier_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstArgModifierSignature> shared_const_arg_modifier_signature() const;
  // required or optional field blob_last_used_signature
 public:
  bool has_blob_last_used_signature() const;
  const ::oneflow::cfg::BlobLastUsedSignature& blob_last_used_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobLastUsedSignature> shared_const_blob_last_used_signature() const;
  // required or optional field blob_backward_used_signature
 public:
  bool has_blob_backward_used_signature() const;
  const ::oneflow::cfg::BlobBackwardUsedSignature& blob_backward_used_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobBackwardUsedSignature> shared_const_blob_backward_used_signature() const;
  // required or optional field sbp_signature
 public:
  bool has_sbp_signature() const;
  const ::oneflow::cfg::SbpSignature& sbp_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> shared_const_sbp_signature() const;
  // required or optional field mirrored_signature
 public:
  bool has_mirrored_signature() const;
  const ::oneflow::cfg::MirroredSignature& mirrored_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMirroredSignature> shared_const_mirrored_signature() const;
  // required or optional field logical_blob_desc_signature
 public:
  bool has_logical_blob_desc_signature() const;
  const ::oneflow::cfg::BlobDescSignature& logical_blob_desc_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobDescSignature> shared_const_logical_blob_desc_signature() const;
  // required or optional field parallel_signature
 public:
  bool has_parallel_signature() const;
  const ::oneflow::cfg::ParallelSignature& parallel_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelSignature> shared_const_parallel_signature() const;
  // required or optional field parallel_conf_signature
 public:
  bool has_parallel_conf_signature() const;
  const ::oneflow::cfg::ParallelConfSignature& parallel_conf_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelConfSignature> shared_const_parallel_conf_signature() const;
  // required or optional field parallel_distribution_signature
 public:
  bool has_parallel_distribution_signature() const;
  const ::oneflow::cfg::ParallelDistributionSignature& parallel_distribution_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelDistributionSignature> shared_const_parallel_distribution_signature() const;

 public:
  ::std::shared_ptr<ConstOpAttribute> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpAttribute& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpAttribute& other) const;
 protected:
  const ::std::shared_ptr<_OpAttribute_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpAttribute_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpAttribute
  void BuildFromProto(const PbMessage& proto_opattribute);
  
  ::std::shared_ptr<_OpAttribute_> data_;
};

class OpAttribute final : public ConstOpAttribute {
 public:
  OpAttribute(const ::std::shared_ptr<_OpAttribute_>& data);
  OpAttribute(const OpAttribute& other);
  // enable nothrow for ::std::vector<OpAttribute> resize 
  OpAttribute(OpAttribute&&) noexcept;
  OpAttribute();
  explicit OpAttribute(const ::oneflow::OpAttribute& proto_opattribute);

  ~OpAttribute() override;

  void InitFromProto(const PbMessage& proto_opattribute) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpAttribute& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpAttribute& other) const;
  void Clear();
  void CopyFrom(const OpAttribute& other);
  OpAttribute& operator=(const OpAttribute& other);

  // repeated field input_bns
 public:
  void clear_input_bns();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_input_bns();
  ::std::string* mutable_input_bns(::std::size_t index);
  void add_input_bns(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_mutable_input_bns();
  void set_input_bns(::std::size_t index, const ::std::string& value);
  // repeated field output_bns
 public:
  void clear_output_bns();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_output_bns();
  ::std::string* mutable_output_bns(::std::size_t index);
  void add_output_bns(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_mutable_output_bns();
  void set_output_bns(::std::size_t index, const ::std::string& value);
  // repeated field tmp_bns
 public:
  void clear_tmp_bns();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* mutable_tmp_bns();
  ::std::string* mutable_tmp_bns(::std::size_t index);
  void add_tmp_bns(const ::std::string& value);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> shared_mutable_tmp_bns();
  void set_tmp_bns(::std::size_t index, const ::std::string& value);
  // required or optional field op_conf
 public:
  void clear_op_conf();
  ::oneflow::cfg::OperatorConf* mutable_op_conf();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::OperatorConf> shared_mutable_op_conf();
  // required or optional field arg_signature
 public:
  void clear_arg_signature();
  ::oneflow::cfg::ArgSignature* mutable_arg_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ArgSignature> shared_mutable_arg_signature();
  // required or optional field arg_modifier_signature
 public:
  void clear_arg_modifier_signature();
  ::oneflow::cfg::ArgModifierSignature* mutable_arg_modifier_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ArgModifierSignature> shared_mutable_arg_modifier_signature();
  // required or optional field blob_last_used_signature
 public:
  void clear_blob_last_used_signature();
  ::oneflow::cfg::BlobLastUsedSignature* mutable_blob_last_used_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobLastUsedSignature> shared_mutable_blob_last_used_signature();
  // required or optional field blob_backward_used_signature
 public:
  void clear_blob_backward_used_signature();
  ::oneflow::cfg::BlobBackwardUsedSignature* mutable_blob_backward_used_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobBackwardUsedSignature> shared_mutable_blob_backward_used_signature();
  // required or optional field sbp_signature
 public:
  void clear_sbp_signature();
  ::oneflow::cfg::SbpSignature* mutable_sbp_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpSignature> shared_mutable_sbp_signature();
  // required or optional field mirrored_signature
 public:
  void clear_mirrored_signature();
  ::oneflow::cfg::MirroredSignature* mutable_mirrored_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MirroredSignature> shared_mutable_mirrored_signature();
  // required or optional field logical_blob_desc_signature
 public:
  void clear_logical_blob_desc_signature();
  ::oneflow::cfg::BlobDescSignature* mutable_logical_blob_desc_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> shared_mutable_logical_blob_desc_signature();
  // required or optional field parallel_signature
 public:
  void clear_parallel_signature();
  ::oneflow::cfg::ParallelSignature* mutable_parallel_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelSignature> shared_mutable_parallel_signature();
  // required or optional field parallel_conf_signature
 public:
  void clear_parallel_conf_signature();
  ::oneflow::cfg::ParallelConfSignature* mutable_parallel_conf_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelConfSignature> shared_mutable_parallel_conf_signature();
  // required or optional field parallel_distribution_signature
 public:
  void clear_parallel_distribution_signature();
  ::oneflow::cfg::ParallelDistributionSignature* mutable_parallel_distribution_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelDistributionSignature> shared_mutable_parallel_distribution_signature();

  ::std::shared_ptr<OpAttribute> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_;
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_;

class ConstOpAttributeList : public ::oneflow::cfg::Message {
 public:

  class _OpAttributeList_ {
   public:
    _OpAttributeList_();
    explicit _OpAttributeList_(const _OpAttributeList_& other);
    explicit _OpAttributeList_(_OpAttributeList_&& other);
    _OpAttributeList_(const ::oneflow::OpAttributeList& proto_opattributelist);
    ~_OpAttributeList_();

    void InitFromProto(const ::oneflow::OpAttributeList& proto_opattributelist);

    void ToProto(::oneflow::OpAttributeList* proto_opattributelist) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpAttributeList_& other);
  
      // repeated field op_attribute
   public:
    ::std::size_t op_attribute_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& op_attribute() const;
    const ::oneflow::cfg::OpAttribute& op_attribute(::std::size_t index) const;
    void clear_op_attribute();
    _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_* mutable_op_attribute();
    ::oneflow::cfg::OpAttribute* mutable_op_attribute(::std::size_t index);
      ::oneflow::cfg::OpAttribute* add_op_attribute();
     protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> op_attribute_;
         
   public:
    int compare(const _OpAttributeList_& other);

    bool operator==(const _OpAttributeList_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpAttributeList_& other) const;
  };

  ConstOpAttributeList(const ::std::shared_ptr<_OpAttributeList_>& data);
  ConstOpAttributeList(const ConstOpAttributeList&);
  ConstOpAttributeList(ConstOpAttributeList&&) noexcept;
  ConstOpAttributeList();
  ConstOpAttributeList(const ::oneflow::OpAttributeList& proto_opattributelist);
  virtual ~ConstOpAttributeList() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opattributelist) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // repeated field op_attribute
 public:
  ::std::size_t op_attribute_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& op_attribute() const;
  const ::oneflow::cfg::OpAttribute& op_attribute(::std::size_t index) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> shared_const_op_attribute() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOpAttribute> shared_const_op_attribute(::std::size_t index) const;

 public:
  ::std::shared_ptr<ConstOpAttributeList> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpAttributeList& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpAttributeList& other) const;
 protected:
  const ::std::shared_ptr<_OpAttributeList_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpAttributeList_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpAttributeList
  void BuildFromProto(const PbMessage& proto_opattributelist);
  
  ::std::shared_ptr<_OpAttributeList_> data_;
};

class OpAttributeList final : public ConstOpAttributeList {
 public:
  OpAttributeList(const ::std::shared_ptr<_OpAttributeList_>& data);
  OpAttributeList(const OpAttributeList& other);
  // enable nothrow for ::std::vector<OpAttributeList> resize 
  OpAttributeList(OpAttributeList&&) noexcept;
  OpAttributeList();
  explicit OpAttributeList(const ::oneflow::OpAttributeList& proto_opattributelist);

  ~OpAttributeList() override;

  void InitFromProto(const PbMessage& proto_opattributelist) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpAttributeList& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpAttributeList& other) const;
  void Clear();
  void CopyFrom(const OpAttributeList& other);
  OpAttributeList& operator=(const OpAttributeList& other);

  // repeated field op_attribute
 public:
  void clear_op_attribute();
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_* mutable_op_attribute();
  ::oneflow::cfg::OpAttribute* mutable_op_attribute(::std::size_t index);
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> shared_mutable_op_attribute();
  ::std::shared_ptr<::oneflow::cfg::OpAttribute> shared_mutable_op_attribute(::std::size_t index);
  ::oneflow::cfg::OpAttribute* add_op_attribute();

  ::std::shared_ptr<OpAttributeList> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_ : public ::oneflow::cfg::_RepeatedField_<::std::string> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> __SharedConst__() const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> __SharedMutable__();
};




// inheritance is helpful for avoiding container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_ : public ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpAttribute> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpAttribute>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> __SharedConst__() const;
  ::std::shared_ptr<::oneflow::cfg::ConstOpAttribute> __SharedConst__(::std::size_t index) const;
};
class _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpAttribute>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> __SharedMutable__();
  ::std::shared_ptr<::oneflow::cfg::OpAttribute> __SharedAdd__();
  ::std::shared_ptr<::oneflow::cfg::OpAttribute> __SharedMutable__(::std::size_t index);
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstOpAttribute> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpAttribute& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpAttribute> {
  std::size_t operator()(const ::oneflow::cfg::OpAttribute& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOpAttributeList> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpAttributeList& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpAttributeList> {
  std::size_t operator()(const ::oneflow::cfg::OpAttributeList& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H_