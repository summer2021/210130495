#include "oneflow/core/operator/op_node_signature.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/job/parallel_signature.cfg.h"
#include "oneflow/core/operator/op_node_signature.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstOpNodeSignature::_OpNodeSignature_::_OpNodeSignature_() { Clear(); }
ConstOpNodeSignature::_OpNodeSignature_::_OpNodeSignature_(const _OpNodeSignature_& other) { CopyFrom(other); }
ConstOpNodeSignature::_OpNodeSignature_::_OpNodeSignature_(const ::oneflow::OpNodeSignature& proto_opnodesignature) {
  InitFromProto(proto_opnodesignature);
}
ConstOpNodeSignature::_OpNodeSignature_::_OpNodeSignature_(_OpNodeSignature_&& other) = default;
ConstOpNodeSignature::_OpNodeSignature_::~_OpNodeSignature_() = default;

void ConstOpNodeSignature::_OpNodeSignature_::InitFromProto(const ::oneflow::OpNodeSignature& proto_opnodesignature) {
  Clear();
  // required_or_optional field: sbp_signature
  if (proto_opnodesignature.has_sbp_signature()) {
  *mutable_sbp_signature() = ::oneflow::cfg::SbpSignature(proto_opnodesignature.sbp_signature());      
  }
  // required_or_optional field: mirrored_signature
  if (proto_opnodesignature.has_mirrored_signature()) {
  *mutable_mirrored_signature() = ::oneflow::cfg::MirroredSignature(proto_opnodesignature.mirrored_signature());      
  }
  // required_or_optional field: logical_blob_desc_signature
  if (proto_opnodesignature.has_logical_blob_desc_signature()) {
  *mutable_logical_blob_desc_signature() = ::oneflow::cfg::BlobDescSignature(proto_opnodesignature.logical_blob_desc_signature());      
  }
  // required_or_optional field: parallel_signature
  if (proto_opnodesignature.has_parallel_signature()) {
  *mutable_parallel_signature() = ::oneflow::cfg::ParallelSignature(proto_opnodesignature.parallel_signature());      
  }
    
}

void ConstOpNodeSignature::_OpNodeSignature_::ToProto(::oneflow::OpNodeSignature* proto_opnodesignature) const {
  proto_opnodesignature->Clear();
  // required_or_optional field: sbp_signature
  if (this->has_sbp_signature()) {
    ::oneflow::SbpSignature proto_sbp_signature;
    sbp_signature().ToProto(&proto_sbp_signature);
    proto_opnodesignature->mutable_sbp_signature()->CopyFrom(proto_sbp_signature);
    }
  // required_or_optional field: mirrored_signature
  if (this->has_mirrored_signature()) {
    ::oneflow::MirroredSignature proto_mirrored_signature;
    mirrored_signature().ToProto(&proto_mirrored_signature);
    proto_opnodesignature->mutable_mirrored_signature()->CopyFrom(proto_mirrored_signature);
    }
  // required_or_optional field: logical_blob_desc_signature
  if (this->has_logical_blob_desc_signature()) {
    ::oneflow::BlobDescSignature proto_logical_blob_desc_signature;
    logical_blob_desc_signature().ToProto(&proto_logical_blob_desc_signature);
    proto_opnodesignature->mutable_logical_blob_desc_signature()->CopyFrom(proto_logical_blob_desc_signature);
    }
  // required_or_optional field: parallel_signature
  if (this->has_parallel_signature()) {
    ::oneflow::ParallelSignature proto_parallel_signature;
    parallel_signature().ToProto(&proto_parallel_signature);
    proto_opnodesignature->mutable_parallel_signature()->CopyFrom(proto_parallel_signature);
    }

}

::std::string ConstOpNodeSignature::_OpNodeSignature_::DebugString() const {
  ::oneflow::OpNodeSignature proto_opnodesignature;
  this->ToProto(&proto_opnodesignature);
  return proto_opnodesignature.DebugString();
}

void ConstOpNodeSignature::_OpNodeSignature_::Clear() {
  clear_sbp_signature();
  clear_mirrored_signature();
  clear_logical_blob_desc_signature();
  clear_parallel_signature();
}

void ConstOpNodeSignature::_OpNodeSignature_::CopyFrom(const _OpNodeSignature_& other) {
  if (other.has_sbp_signature()) {
    mutable_sbp_signature()->CopyFrom(other.sbp_signature());
  } else {
    clear_sbp_signature();
  }
  if (other.has_mirrored_signature()) {
    mutable_mirrored_signature()->CopyFrom(other.mirrored_signature());
  } else {
    clear_mirrored_signature();
  }
  if (other.has_logical_blob_desc_signature()) {
    mutable_logical_blob_desc_signature()->CopyFrom(other.logical_blob_desc_signature());
  } else {
    clear_logical_blob_desc_signature();
  }
  if (other.has_parallel_signature()) {
    mutable_parallel_signature()->CopyFrom(other.parallel_signature());
  } else {
    clear_parallel_signature();
  }
}


// optional field sbp_signature
bool ConstOpNodeSignature::_OpNodeSignature_::has_sbp_signature() const {
  return has_sbp_signature_;
}
const ::oneflow::cfg::SbpSignature& ConstOpNodeSignature::_OpNodeSignature_::sbp_signature() const {
  if (!sbp_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::SbpSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::SbpSignature>();
    return *default_static_value;
  }
  return *(sbp_signature_.get());
}
void ConstOpNodeSignature::_OpNodeSignature_::clear_sbp_signature() {
  if (sbp_signature_) {
    sbp_signature_->Clear();
  }
  has_sbp_signature_ = false;
}
::oneflow::cfg::SbpSignature* ConstOpNodeSignature::_OpNodeSignature_::mutable_sbp_signature() {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<::oneflow::cfg::SbpSignature>();
  }
  has_sbp_signature_ = true;
  return sbp_signature_.get();
}

// optional field mirrored_signature
bool ConstOpNodeSignature::_OpNodeSignature_::has_mirrored_signature() const {
  return has_mirrored_signature_;
}
const ::oneflow::cfg::MirroredSignature& ConstOpNodeSignature::_OpNodeSignature_::mirrored_signature() const {
  if (!mirrored_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::MirroredSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::MirroredSignature>();
    return *default_static_value;
  }
  return *(mirrored_signature_.get());
}
void ConstOpNodeSignature::_OpNodeSignature_::clear_mirrored_signature() {
  if (mirrored_signature_) {
    mirrored_signature_->Clear();
  }
  has_mirrored_signature_ = false;
}
::oneflow::cfg::MirroredSignature* ConstOpNodeSignature::_OpNodeSignature_::mutable_mirrored_signature() {
  if (!mirrored_signature_) {
    mirrored_signature_ = ::std::make_shared<::oneflow::cfg::MirroredSignature>();
  }
  has_mirrored_signature_ = true;
  return mirrored_signature_.get();
}

// optional field logical_blob_desc_signature
bool ConstOpNodeSignature::_OpNodeSignature_::has_logical_blob_desc_signature() const {
  return has_logical_blob_desc_signature_;
}
const ::oneflow::cfg::BlobDescSignature& ConstOpNodeSignature::_OpNodeSignature_::logical_blob_desc_signature() const {
  if (!logical_blob_desc_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::BlobDescSignature>();
    return *default_static_value;
  }
  return *(logical_blob_desc_signature_.get());
}
void ConstOpNodeSignature::_OpNodeSignature_::clear_logical_blob_desc_signature() {
  if (logical_blob_desc_signature_) {
    logical_blob_desc_signature_->Clear();
  }
  has_logical_blob_desc_signature_ = false;
}
::oneflow::cfg::BlobDescSignature* ConstOpNodeSignature::_OpNodeSignature_::mutable_logical_blob_desc_signature() {
  if (!logical_blob_desc_signature_) {
    logical_blob_desc_signature_ = ::std::make_shared<::oneflow::cfg::BlobDescSignature>();
  }
  has_logical_blob_desc_signature_ = true;
  return logical_blob_desc_signature_.get();
}

// optional field parallel_signature
bool ConstOpNodeSignature::_OpNodeSignature_::has_parallel_signature() const {
  return has_parallel_signature_;
}
const ::oneflow::cfg::ParallelSignature& ConstOpNodeSignature::_OpNodeSignature_::parallel_signature() const {
  if (!parallel_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelSignature>();
    return *default_static_value;
  }
  return *(parallel_signature_.get());
}
void ConstOpNodeSignature::_OpNodeSignature_::clear_parallel_signature() {
  if (parallel_signature_) {
    parallel_signature_->Clear();
  }
  has_parallel_signature_ = false;
}
::oneflow::cfg::ParallelSignature* ConstOpNodeSignature::_OpNodeSignature_::mutable_parallel_signature() {
  if (!parallel_signature_) {
    parallel_signature_ = ::std::make_shared<::oneflow::cfg::ParallelSignature>();
  }
  has_parallel_signature_ = true;
  return parallel_signature_.get();
}


int ConstOpNodeSignature::_OpNodeSignature_::compare(const _OpNodeSignature_& other) {
  if (!(has_sbp_signature() == other.has_sbp_signature())) {
    return has_sbp_signature() < other.has_sbp_signature() ? -1 : 1;
  } else if (!(sbp_signature() == other.sbp_signature())) {
    return sbp_signature() < other.sbp_signature() ? -1 : 1;
  }
  if (!(has_mirrored_signature() == other.has_mirrored_signature())) {
    return has_mirrored_signature() < other.has_mirrored_signature() ? -1 : 1;
  } else if (!(mirrored_signature() == other.mirrored_signature())) {
    return mirrored_signature() < other.mirrored_signature() ? -1 : 1;
  }
  if (!(has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature())) {
    return has_logical_blob_desc_signature() < other.has_logical_blob_desc_signature() ? -1 : 1;
  } else if (!(logical_blob_desc_signature() == other.logical_blob_desc_signature())) {
    return logical_blob_desc_signature() < other.logical_blob_desc_signature() ? -1 : 1;
  }
  if (!(has_parallel_signature() == other.has_parallel_signature())) {
    return has_parallel_signature() < other.has_parallel_signature() ? -1 : 1;
  } else if (!(parallel_signature() == other.parallel_signature())) {
    return parallel_signature() < other.parallel_signature() ? -1 : 1;
  }
  return 0;
}

bool ConstOpNodeSignature::_OpNodeSignature_::operator==(const _OpNodeSignature_& other) const {
  return true
    && has_sbp_signature() == other.has_sbp_signature() 
    && sbp_signature() == other.sbp_signature()
    && has_mirrored_signature() == other.has_mirrored_signature() 
    && mirrored_signature() == other.mirrored_signature()
    && has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature() 
    && logical_blob_desc_signature() == other.logical_blob_desc_signature()
    && has_parallel_signature() == other.has_parallel_signature() 
    && parallel_signature() == other.parallel_signature()
  ;
}

std::size_t ConstOpNodeSignature::_OpNodeSignature_::__CalcHash__() const {
  return 0
    ^ (has_sbp_signature() ? std::hash<::oneflow::cfg::SbpSignature>()(sbp_signature()) : 0)
    ^ (has_mirrored_signature() ? std::hash<::oneflow::cfg::MirroredSignature>()(mirrored_signature()) : 0)
    ^ (has_logical_blob_desc_signature() ? std::hash<::oneflow::cfg::BlobDescSignature>()(logical_blob_desc_signature()) : 0)
    ^ (has_parallel_signature() ? std::hash<::oneflow::cfg::ParallelSignature>()(parallel_signature()) : 0)
  ;
}

bool ConstOpNodeSignature::_OpNodeSignature_::operator<(const _OpNodeSignature_& other) const {
  return false
    || !(has_sbp_signature() == other.has_sbp_signature()) ? 
      has_sbp_signature() < other.has_sbp_signature() : false
    || !(sbp_signature() == other.sbp_signature()) ? 
      sbp_signature() < other.sbp_signature() : false
    || !(has_mirrored_signature() == other.has_mirrored_signature()) ? 
      has_mirrored_signature() < other.has_mirrored_signature() : false
    || !(mirrored_signature() == other.mirrored_signature()) ? 
      mirrored_signature() < other.mirrored_signature() : false
    || !(has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature()) ? 
      has_logical_blob_desc_signature() < other.has_logical_blob_desc_signature() : false
    || !(logical_blob_desc_signature() == other.logical_blob_desc_signature()) ? 
      logical_blob_desc_signature() < other.logical_blob_desc_signature() : false
    || !(has_parallel_signature() == other.has_parallel_signature()) ? 
      has_parallel_signature() < other.has_parallel_signature() : false
    || !(parallel_signature() == other.parallel_signature()) ? 
      parallel_signature() < other.parallel_signature() : false
  ;
}

using _OpNodeSignature_ =  ConstOpNodeSignature::_OpNodeSignature_;
ConstOpNodeSignature::ConstOpNodeSignature(const ::std::shared_ptr<_OpNodeSignature_>& data): data_(data) {}
ConstOpNodeSignature::ConstOpNodeSignature(): data_(::std::make_shared<_OpNodeSignature_>()) {}
ConstOpNodeSignature::ConstOpNodeSignature(const ::oneflow::OpNodeSignature& proto_opnodesignature) {
  BuildFromProto(proto_opnodesignature);
}
ConstOpNodeSignature::ConstOpNodeSignature(const ConstOpNodeSignature&) = default;
ConstOpNodeSignature::ConstOpNodeSignature(ConstOpNodeSignature&&) noexcept = default;
ConstOpNodeSignature::~ConstOpNodeSignature() = default;

void ConstOpNodeSignature::ToProto(PbMessage* proto_opnodesignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpNodeSignature*>(proto_opnodesignature));
}
  
::std::string ConstOpNodeSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpNodeSignature::__Empty__() const {
  return !data_;
}

int ConstOpNodeSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"sbp_signature", 1},
    {"mirrored_signature", 2},
    {"logical_blob_desc_signature", 3},
    {"parallel_signature", 5},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpNodeSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 5:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpNodeSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::SbpSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstSbpSignature),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MirroredSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMirroredSignature),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobDescSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobDescSignature),
      };
      return type_indices;
    }
    case 5: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelSignature),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpNodeSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &sbp_signature();
    case 2: return &mirrored_signature();
    case 3: return &logical_blob_desc_signature();
    case 5: return &parallel_signature();
    default: return nullptr;
  }
}

// required or optional field sbp_signature
bool ConstOpNodeSignature::has_sbp_signature() const {
  return __SharedPtrOrDefault__()->has_sbp_signature();
}
const ::oneflow::cfg::SbpSignature& ConstOpNodeSignature::sbp_signature() const {
  return __SharedPtrOrDefault__()->sbp_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> ConstOpNodeSignature::shared_const_sbp_signature() const {
  return sbp_signature().__SharedConst__();
}
// required or optional field mirrored_signature
bool ConstOpNodeSignature::has_mirrored_signature() const {
  return __SharedPtrOrDefault__()->has_mirrored_signature();
}
const ::oneflow::cfg::MirroredSignature& ConstOpNodeSignature::mirrored_signature() const {
  return __SharedPtrOrDefault__()->mirrored_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMirroredSignature> ConstOpNodeSignature::shared_const_mirrored_signature() const {
  return mirrored_signature().__SharedConst__();
}
// required or optional field logical_blob_desc_signature
bool ConstOpNodeSignature::has_logical_blob_desc_signature() const {
  return __SharedPtrOrDefault__()->has_logical_blob_desc_signature();
}
const ::oneflow::cfg::BlobDescSignature& ConstOpNodeSignature::logical_blob_desc_signature() const {
  return __SharedPtrOrDefault__()->logical_blob_desc_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobDescSignature> ConstOpNodeSignature::shared_const_logical_blob_desc_signature() const {
  return logical_blob_desc_signature().__SharedConst__();
}
// required or optional field parallel_signature
bool ConstOpNodeSignature::has_parallel_signature() const {
  return __SharedPtrOrDefault__()->has_parallel_signature();
}
const ::oneflow::cfg::ParallelSignature& ConstOpNodeSignature::parallel_signature() const {
  return __SharedPtrOrDefault__()->parallel_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelSignature> ConstOpNodeSignature::shared_const_parallel_signature() const {
  return parallel_signature().__SharedConst__();
}

::std::shared_ptr<ConstOpNodeSignature> ConstOpNodeSignature::__SharedConst__() const {
  return ::std::make_shared<ConstOpNodeSignature>(data_);
}
int64_t ConstOpNodeSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpNodeSignature::operator==(const ConstOpNodeSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpNodeSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpNodeSignature::operator<(const ConstOpNodeSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpNodeSignature_>& ConstOpNodeSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpNodeSignature_> default_ptr = std::make_shared<_OpNodeSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpNodeSignature_>& ConstOpNodeSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpNodeSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpNodeSignature
void ConstOpNodeSignature::BuildFromProto(const PbMessage& proto_opnodesignature) {
  data_ = ::std::make_shared<_OpNodeSignature_>(dynamic_cast<const ::oneflow::OpNodeSignature&>(proto_opnodesignature));
}

OpNodeSignature::OpNodeSignature(const ::std::shared_ptr<_OpNodeSignature_>& data)
  : ConstOpNodeSignature(data) {}
OpNodeSignature::OpNodeSignature(const OpNodeSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpNodeSignature> resize
OpNodeSignature::OpNodeSignature(OpNodeSignature&&) noexcept = default; 
OpNodeSignature::OpNodeSignature(const ::oneflow::OpNodeSignature& proto_opnodesignature) {
  InitFromProto(proto_opnodesignature);
}
OpNodeSignature::OpNodeSignature() = default;

OpNodeSignature::~OpNodeSignature() = default;

void OpNodeSignature::InitFromProto(const PbMessage& proto_opnodesignature) {
  BuildFromProto(proto_opnodesignature);
}
  
void* OpNodeSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_sbp_signature();
    case 2: return mutable_mirrored_signature();
    case 3: return mutable_logical_blob_desc_signature();
    case 5: return mutable_parallel_signature();
    default: return nullptr;
  }
}

bool OpNodeSignature::operator==(const OpNodeSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpNodeSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpNodeSignature::operator<(const OpNodeSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpNodeSignature::Clear() {
  if (data_) { data_.reset(); }
}
void OpNodeSignature::CopyFrom(const OpNodeSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpNodeSignature& OpNodeSignature::operator=(const OpNodeSignature& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field sbp_signature
void OpNodeSignature::clear_sbp_signature() {
  return __SharedPtr__()->clear_sbp_signature();
}
::oneflow::cfg::SbpSignature* OpNodeSignature::mutable_sbp_signature() {
  return __SharedPtr__()->mutable_sbp_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::SbpSignature> OpNodeSignature::shared_mutable_sbp_signature() {
  return mutable_sbp_signature()->__SharedMutable__();
}
// required or optional field mirrored_signature
void OpNodeSignature::clear_mirrored_signature() {
  return __SharedPtr__()->clear_mirrored_signature();
}
::oneflow::cfg::MirroredSignature* OpNodeSignature::mutable_mirrored_signature() {
  return __SharedPtr__()->mutable_mirrored_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MirroredSignature> OpNodeSignature::shared_mutable_mirrored_signature() {
  return mutable_mirrored_signature()->__SharedMutable__();
}
// required or optional field logical_blob_desc_signature
void OpNodeSignature::clear_logical_blob_desc_signature() {
  return __SharedPtr__()->clear_logical_blob_desc_signature();
}
::oneflow::cfg::BlobDescSignature* OpNodeSignature::mutable_logical_blob_desc_signature() {
  return __SharedPtr__()->mutable_logical_blob_desc_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobDescSignature> OpNodeSignature::shared_mutable_logical_blob_desc_signature() {
  return mutable_logical_blob_desc_signature()->__SharedMutable__();
}
// required or optional field parallel_signature
void OpNodeSignature::clear_parallel_signature() {
  return __SharedPtr__()->clear_parallel_signature();
}
::oneflow::cfg::ParallelSignature* OpNodeSignature::mutable_parallel_signature() {
  return __SharedPtr__()->mutable_parallel_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelSignature> OpNodeSignature::shared_mutable_parallel_signature() {
  return mutable_parallel_signature()->__SharedMutable__();
}

::std::shared_ptr<OpNodeSignature> OpNodeSignature::__SharedMutable__() {
  return ::std::make_shared<OpNodeSignature>(__SharedPtr__());
}


}
} // namespace oneflow
