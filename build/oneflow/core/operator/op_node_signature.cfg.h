#ifndef CFG_ONEFLOW_CORE_OPERATOR_OP_NODE_SIGNATURE_CFG_H_
#define CFG_ONEFLOW_CORE_OPERATOR_OP_NODE_SIGNATURE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstBlobDescSignature;
class BlobDescSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstMirroredSignature;
class MirroredSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstParallelSignature;
class ParallelSignature;
}
}
namespace oneflow {
namespace cfg {
class ConstSbpSignature;
class SbpSignature;
}
}

namespace oneflow {

// forward declare proto class;
class OpNodeSignature;

namespace cfg {


class OpNodeSignature;
class ConstOpNodeSignature;



class ConstOpNodeSignature : public ::oneflow::cfg::Message {
 public:

  class _OpNodeSignature_ {
   public:
    _OpNodeSignature_();
    explicit _OpNodeSignature_(const _OpNodeSignature_& other);
    explicit _OpNodeSignature_(_OpNodeSignature_&& other);
    _OpNodeSignature_(const ::oneflow::OpNodeSignature& proto_opnodesignature);
    ~_OpNodeSignature_();

    void InitFromProto(const ::oneflow::OpNodeSignature& proto_opnodesignature);

    void ToProto(::oneflow::OpNodeSignature* proto_opnodesignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OpNodeSignature_& other);
  
      // optional field sbp_signature
     public:
    bool has_sbp_signature() const;
    const ::oneflow::cfg::SbpSignature& sbp_signature() const;
    void clear_sbp_signature();
    ::oneflow::cfg::SbpSignature* mutable_sbp_signature();
   protected:
    bool has_sbp_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::SbpSignature> sbp_signature_;
      
      // optional field mirrored_signature
     public:
    bool has_mirrored_signature() const;
    const ::oneflow::cfg::MirroredSignature& mirrored_signature() const;
    void clear_mirrored_signature();
    ::oneflow::cfg::MirroredSignature* mutable_mirrored_signature();
   protected:
    bool has_mirrored_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::MirroredSignature> mirrored_signature_;
      
      // optional field logical_blob_desc_signature
     public:
    bool has_logical_blob_desc_signature() const;
    const ::oneflow::cfg::BlobDescSignature& logical_blob_desc_signature() const;
    void clear_logical_blob_desc_signature();
    ::oneflow::cfg::BlobDescSignature* mutable_logical_blob_desc_signature();
   protected:
    bool has_logical_blob_desc_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> logical_blob_desc_signature_;
      
      // optional field parallel_signature
     public:
    bool has_parallel_signature() const;
    const ::oneflow::cfg::ParallelSignature& parallel_signature() const;
    void clear_parallel_signature();
    ::oneflow::cfg::ParallelSignature* mutable_parallel_signature();
   protected:
    bool has_parallel_signature_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelSignature> parallel_signature_;
           
   public:
    int compare(const _OpNodeSignature_& other);

    bool operator==(const _OpNodeSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OpNodeSignature_& other) const;
  };

  ConstOpNodeSignature(const ::std::shared_ptr<_OpNodeSignature_>& data);
  ConstOpNodeSignature(const ConstOpNodeSignature&);
  ConstOpNodeSignature(ConstOpNodeSignature&&) noexcept;
  ConstOpNodeSignature();
  ConstOpNodeSignature(const ::oneflow::OpNodeSignature& proto_opnodesignature);
  virtual ~ConstOpNodeSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_opnodesignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field sbp_signature
 public:
  bool has_sbp_signature() const;
  const ::oneflow::cfg::SbpSignature& sbp_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> shared_const_sbp_signature() const;
  // required or optional field mirrored_signature
 public:
  bool has_mirrored_signature() const;
  const ::oneflow::cfg::MirroredSignature& mirrored_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstMirroredSignature> shared_const_mirrored_signature() const;
  // required or optional field logical_blob_desc_signature
 public:
  bool has_logical_blob_desc_signature() const;
  const ::oneflow::cfg::BlobDescSignature& logical_blob_desc_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstBlobDescSignature> shared_const_logical_blob_desc_signature() const;
  // required or optional field parallel_signature
 public:
  bool has_parallel_signature() const;
  const ::oneflow::cfg::ParallelSignature& parallel_signature() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelSignature> shared_const_parallel_signature() const;

 public:
  ::std::shared_ptr<ConstOpNodeSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOpNodeSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOpNodeSignature& other) const;
 protected:
  const ::std::shared_ptr<_OpNodeSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OpNodeSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOpNodeSignature
  void BuildFromProto(const PbMessage& proto_opnodesignature);
  
  ::std::shared_ptr<_OpNodeSignature_> data_;
};

class OpNodeSignature final : public ConstOpNodeSignature {
 public:
  OpNodeSignature(const ::std::shared_ptr<_OpNodeSignature_>& data);
  OpNodeSignature(const OpNodeSignature& other);
  // enable nothrow for ::std::vector<OpNodeSignature> resize 
  OpNodeSignature(OpNodeSignature&&) noexcept;
  OpNodeSignature();
  explicit OpNodeSignature(const ::oneflow::OpNodeSignature& proto_opnodesignature);

  ~OpNodeSignature() override;

  void InitFromProto(const PbMessage& proto_opnodesignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OpNodeSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OpNodeSignature& other) const;
  void Clear();
  void CopyFrom(const OpNodeSignature& other);
  OpNodeSignature& operator=(const OpNodeSignature& other);

  // required or optional field sbp_signature
 public:
  void clear_sbp_signature();
  ::oneflow::cfg::SbpSignature* mutable_sbp_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::SbpSignature> shared_mutable_sbp_signature();
  // required or optional field mirrored_signature
 public:
  void clear_mirrored_signature();
  ::oneflow::cfg::MirroredSignature* mutable_mirrored_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::MirroredSignature> shared_mutable_mirrored_signature();
  // required or optional field logical_blob_desc_signature
 public:
  void clear_logical_blob_desc_signature();
  ::oneflow::cfg::BlobDescSignature* mutable_logical_blob_desc_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> shared_mutable_logical_blob_desc_signature();
  // required or optional field parallel_signature
 public:
  void clear_parallel_signature();
  ::oneflow::cfg::ParallelSignature* mutable_parallel_signature();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelSignature> shared_mutable_parallel_signature();

  ::std::shared_ptr<OpNodeSignature> __SharedMutable__();
};






} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstOpNodeSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstOpNodeSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OpNodeSignature> {
  std::size_t operator()(const ::oneflow::cfg::OpNodeSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_OPERATOR_OP_NODE_SIGNATURE_CFG_H_