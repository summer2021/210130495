#include "oneflow/core/operator/op_attribute.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/operator/op_conf.cfg.h"
#include "oneflow/core/operator/arg_modifier_signature.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/job/blob_lifetime_signature.cfg.h"
#include "oneflow/core/job/parallel_signature.cfg.h"
#include "oneflow/core/job/parallel_conf_signature.cfg.h"
#include "oneflow/core/operator/op_attribute.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstOpAttribute::_OpAttribute_::_OpAttribute_() { Clear(); }
ConstOpAttribute::_OpAttribute_::_OpAttribute_(const _OpAttribute_& other) { CopyFrom(other); }
ConstOpAttribute::_OpAttribute_::_OpAttribute_(const ::oneflow::OpAttribute& proto_opattribute) {
  InitFromProto(proto_opattribute);
}
ConstOpAttribute::_OpAttribute_::_OpAttribute_(_OpAttribute_&& other) = default;
ConstOpAttribute::_OpAttribute_::~_OpAttribute_() = default;

void ConstOpAttribute::_OpAttribute_::InitFromProto(const ::oneflow::OpAttribute& proto_opattribute) {
  Clear();
  // repeated field: input_bns
  if (!proto_opattribute.input_bns().empty()) {
    for (const ::std::string& elem : proto_opattribute.input_bns()) {
      add_input_bns(elem);
    }
  }
  // repeated field: output_bns
  if (!proto_opattribute.output_bns().empty()) {
    for (const ::std::string& elem : proto_opattribute.output_bns()) {
      add_output_bns(elem);
    }
  }
  // repeated field: tmp_bns
  if (!proto_opattribute.tmp_bns().empty()) {
    for (const ::std::string& elem : proto_opattribute.tmp_bns()) {
      add_tmp_bns(elem);
    }
  }
  // required_or_optional field: op_conf
  if (proto_opattribute.has_op_conf()) {
  *mutable_op_conf() = ::oneflow::cfg::OperatorConf(proto_opattribute.op_conf());      
  }
  // required_or_optional field: arg_signature
  if (proto_opattribute.has_arg_signature()) {
  *mutable_arg_signature() = ::oneflow::cfg::ArgSignature(proto_opattribute.arg_signature());      
  }
  // required_or_optional field: arg_modifier_signature
  if (proto_opattribute.has_arg_modifier_signature()) {
  *mutable_arg_modifier_signature() = ::oneflow::cfg::ArgModifierSignature(proto_opattribute.arg_modifier_signature());      
  }
  // required_or_optional field: blob_last_used_signature
  if (proto_opattribute.has_blob_last_used_signature()) {
  *mutable_blob_last_used_signature() = ::oneflow::cfg::BlobLastUsedSignature(proto_opattribute.blob_last_used_signature());      
  }
  // required_or_optional field: blob_backward_used_signature
  if (proto_opattribute.has_blob_backward_used_signature()) {
  *mutable_blob_backward_used_signature() = ::oneflow::cfg::BlobBackwardUsedSignature(proto_opattribute.blob_backward_used_signature());      
  }
  // required_or_optional field: sbp_signature
  if (proto_opattribute.has_sbp_signature()) {
  *mutable_sbp_signature() = ::oneflow::cfg::SbpSignature(proto_opattribute.sbp_signature());      
  }
  // required_or_optional field: mirrored_signature
  if (proto_opattribute.has_mirrored_signature()) {
  *mutable_mirrored_signature() = ::oneflow::cfg::MirroredSignature(proto_opattribute.mirrored_signature());      
  }
  // required_or_optional field: logical_blob_desc_signature
  if (proto_opattribute.has_logical_blob_desc_signature()) {
  *mutable_logical_blob_desc_signature() = ::oneflow::cfg::BlobDescSignature(proto_opattribute.logical_blob_desc_signature());      
  }
  // required_or_optional field: parallel_signature
  if (proto_opattribute.has_parallel_signature()) {
  *mutable_parallel_signature() = ::oneflow::cfg::ParallelSignature(proto_opattribute.parallel_signature());      
  }
  // required_or_optional field: parallel_conf_signature
  if (proto_opattribute.has_parallel_conf_signature()) {
  *mutable_parallel_conf_signature() = ::oneflow::cfg::ParallelConfSignature(proto_opattribute.parallel_conf_signature());      
  }
  // required_or_optional field: parallel_distribution_signature
  if (proto_opattribute.has_parallel_distribution_signature()) {
  *mutable_parallel_distribution_signature() = ::oneflow::cfg::ParallelDistributionSignature(proto_opattribute.parallel_distribution_signature());      
  }
    
}

void ConstOpAttribute::_OpAttribute_::ToProto(::oneflow::OpAttribute* proto_opattribute) const {
  proto_opattribute->Clear();
  // repeated field: input_bns
  if (!input_bns().empty()) {
    for (const ::std::string& elem : input_bns()) {
      proto_opattribute->add_input_bns(elem);
    }
  }
  // repeated field: output_bns
  if (!output_bns().empty()) {
    for (const ::std::string& elem : output_bns()) {
      proto_opattribute->add_output_bns(elem);
    }
  }
  // repeated field: tmp_bns
  if (!tmp_bns().empty()) {
    for (const ::std::string& elem : tmp_bns()) {
      proto_opattribute->add_tmp_bns(elem);
    }
  }
  // required_or_optional field: op_conf
  if (this->has_op_conf()) {
    ::oneflow::OperatorConf proto_op_conf;
    op_conf().ToProto(&proto_op_conf);
    proto_opattribute->mutable_op_conf()->CopyFrom(proto_op_conf);
    }
  // required_or_optional field: arg_signature
  if (this->has_arg_signature()) {
    ::oneflow::ArgSignature proto_arg_signature;
    arg_signature().ToProto(&proto_arg_signature);
    proto_opattribute->mutable_arg_signature()->CopyFrom(proto_arg_signature);
    }
  // required_or_optional field: arg_modifier_signature
  if (this->has_arg_modifier_signature()) {
    ::oneflow::ArgModifierSignature proto_arg_modifier_signature;
    arg_modifier_signature().ToProto(&proto_arg_modifier_signature);
    proto_opattribute->mutable_arg_modifier_signature()->CopyFrom(proto_arg_modifier_signature);
    }
  // required_or_optional field: blob_last_used_signature
  if (this->has_blob_last_used_signature()) {
    ::oneflow::BlobLastUsedSignature proto_blob_last_used_signature;
    blob_last_used_signature().ToProto(&proto_blob_last_used_signature);
    proto_opattribute->mutable_blob_last_used_signature()->CopyFrom(proto_blob_last_used_signature);
    }
  // required_or_optional field: blob_backward_used_signature
  if (this->has_blob_backward_used_signature()) {
    ::oneflow::BlobBackwardUsedSignature proto_blob_backward_used_signature;
    blob_backward_used_signature().ToProto(&proto_blob_backward_used_signature);
    proto_opattribute->mutable_blob_backward_used_signature()->CopyFrom(proto_blob_backward_used_signature);
    }
  // required_or_optional field: sbp_signature
  if (this->has_sbp_signature()) {
    ::oneflow::SbpSignature proto_sbp_signature;
    sbp_signature().ToProto(&proto_sbp_signature);
    proto_opattribute->mutable_sbp_signature()->CopyFrom(proto_sbp_signature);
    }
  // required_or_optional field: mirrored_signature
  if (this->has_mirrored_signature()) {
    ::oneflow::MirroredSignature proto_mirrored_signature;
    mirrored_signature().ToProto(&proto_mirrored_signature);
    proto_opattribute->mutable_mirrored_signature()->CopyFrom(proto_mirrored_signature);
    }
  // required_or_optional field: logical_blob_desc_signature
  if (this->has_logical_blob_desc_signature()) {
    ::oneflow::BlobDescSignature proto_logical_blob_desc_signature;
    logical_blob_desc_signature().ToProto(&proto_logical_blob_desc_signature);
    proto_opattribute->mutable_logical_blob_desc_signature()->CopyFrom(proto_logical_blob_desc_signature);
    }
  // required_or_optional field: parallel_signature
  if (this->has_parallel_signature()) {
    ::oneflow::ParallelSignature proto_parallel_signature;
    parallel_signature().ToProto(&proto_parallel_signature);
    proto_opattribute->mutable_parallel_signature()->CopyFrom(proto_parallel_signature);
    }
  // required_or_optional field: parallel_conf_signature
  if (this->has_parallel_conf_signature()) {
    ::oneflow::ParallelConfSignature proto_parallel_conf_signature;
    parallel_conf_signature().ToProto(&proto_parallel_conf_signature);
    proto_opattribute->mutable_parallel_conf_signature()->CopyFrom(proto_parallel_conf_signature);
    }
  // required_or_optional field: parallel_distribution_signature
  if (this->has_parallel_distribution_signature()) {
    ::oneflow::ParallelDistributionSignature proto_parallel_distribution_signature;
    parallel_distribution_signature().ToProto(&proto_parallel_distribution_signature);
    proto_opattribute->mutable_parallel_distribution_signature()->CopyFrom(proto_parallel_distribution_signature);
    }

}

::std::string ConstOpAttribute::_OpAttribute_::DebugString() const {
  ::oneflow::OpAttribute proto_opattribute;
  this->ToProto(&proto_opattribute);
  return proto_opattribute.DebugString();
}

void ConstOpAttribute::_OpAttribute_::Clear() {
  clear_input_bns();
  clear_output_bns();
  clear_tmp_bns();
  clear_op_conf();
  clear_arg_signature();
  clear_arg_modifier_signature();
  clear_blob_last_used_signature();
  clear_blob_backward_used_signature();
  clear_sbp_signature();
  clear_mirrored_signature();
  clear_logical_blob_desc_signature();
  clear_parallel_signature();
  clear_parallel_conf_signature();
  clear_parallel_distribution_signature();
}

void ConstOpAttribute::_OpAttribute_::CopyFrom(const _OpAttribute_& other) {
  mutable_input_bns()->CopyFrom(other.input_bns());
  mutable_output_bns()->CopyFrom(other.output_bns());
  mutable_tmp_bns()->CopyFrom(other.tmp_bns());
  if (other.has_op_conf()) {
    mutable_op_conf()->CopyFrom(other.op_conf());
  } else {
    clear_op_conf();
  }
  if (other.has_arg_signature()) {
    mutable_arg_signature()->CopyFrom(other.arg_signature());
  } else {
    clear_arg_signature();
  }
  if (other.has_arg_modifier_signature()) {
    mutable_arg_modifier_signature()->CopyFrom(other.arg_modifier_signature());
  } else {
    clear_arg_modifier_signature();
  }
  if (other.has_blob_last_used_signature()) {
    mutable_blob_last_used_signature()->CopyFrom(other.blob_last_used_signature());
  } else {
    clear_blob_last_used_signature();
  }
  if (other.has_blob_backward_used_signature()) {
    mutable_blob_backward_used_signature()->CopyFrom(other.blob_backward_used_signature());
  } else {
    clear_blob_backward_used_signature();
  }
  if (other.has_sbp_signature()) {
    mutable_sbp_signature()->CopyFrom(other.sbp_signature());
  } else {
    clear_sbp_signature();
  }
  if (other.has_mirrored_signature()) {
    mutable_mirrored_signature()->CopyFrom(other.mirrored_signature());
  } else {
    clear_mirrored_signature();
  }
  if (other.has_logical_blob_desc_signature()) {
    mutable_logical_blob_desc_signature()->CopyFrom(other.logical_blob_desc_signature());
  } else {
    clear_logical_blob_desc_signature();
  }
  if (other.has_parallel_signature()) {
    mutable_parallel_signature()->CopyFrom(other.parallel_signature());
  } else {
    clear_parallel_signature();
  }
  if (other.has_parallel_conf_signature()) {
    mutable_parallel_conf_signature()->CopyFrom(other.parallel_conf_signature());
  } else {
    clear_parallel_conf_signature();
  }
  if (other.has_parallel_distribution_signature()) {
    mutable_parallel_distribution_signature()->CopyFrom(other.parallel_distribution_signature());
  } else {
    clear_parallel_distribution_signature();
  }
}


// repeated field input_bns
::std::size_t ConstOpAttribute::_OpAttribute_::input_bns_size() const {
  if (!input_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return input_bns_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::_OpAttribute_::input_bns() const {
  if (!input_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(input_bns_.get());
}
const ::std::string& ConstOpAttribute::_OpAttribute_::input_bns(::std::size_t index) const {
  if (!input_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return input_bns_->Get(index);
}
void ConstOpAttribute::_OpAttribute_::clear_input_bns() {
  if (!input_bns_) {
    input_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return input_bns_->Clear();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* ConstOpAttribute::_OpAttribute_::mutable_input_bns() {
  if (!input_bns_) {
    input_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  input_bns_.get();
}
::std::string* ConstOpAttribute::_OpAttribute_::mutable_input_bns(::std::size_t index) {
  if (!input_bns_) {
    input_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  input_bns_->Mutable(index);
}
void ConstOpAttribute::_OpAttribute_::add_input_bns(const ::std::string& value) {
  if (!input_bns_) {
    input_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return input_bns_->Add(value);
}
void ConstOpAttribute::_OpAttribute_::set_input_bns(::std::size_t index, const ::std::string& value) {
  if (!input_bns_) {
    input_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return input_bns_->Set(index, value);
}

// repeated field output_bns
::std::size_t ConstOpAttribute::_OpAttribute_::output_bns_size() const {
  if (!output_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return output_bns_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::_OpAttribute_::output_bns() const {
  if (!output_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(output_bns_.get());
}
const ::std::string& ConstOpAttribute::_OpAttribute_::output_bns(::std::size_t index) const {
  if (!output_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return output_bns_->Get(index);
}
void ConstOpAttribute::_OpAttribute_::clear_output_bns() {
  if (!output_bns_) {
    output_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return output_bns_->Clear();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* ConstOpAttribute::_OpAttribute_::mutable_output_bns() {
  if (!output_bns_) {
    output_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  output_bns_.get();
}
::std::string* ConstOpAttribute::_OpAttribute_::mutable_output_bns(::std::size_t index) {
  if (!output_bns_) {
    output_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  output_bns_->Mutable(index);
}
void ConstOpAttribute::_OpAttribute_::add_output_bns(const ::std::string& value) {
  if (!output_bns_) {
    output_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return output_bns_->Add(value);
}
void ConstOpAttribute::_OpAttribute_::set_output_bns(::std::size_t index, const ::std::string& value) {
  if (!output_bns_) {
    output_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return output_bns_->Set(index, value);
}

// repeated field tmp_bns
::std::size_t ConstOpAttribute::_OpAttribute_::tmp_bns_size() const {
  if (!tmp_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->size();
  }
  return tmp_bns_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::_OpAttribute_::tmp_bns() const {
  if (!tmp_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return *(default_static_value.get());
  }
  return *(tmp_bns_.get());
}
const ::std::string& ConstOpAttribute::_OpAttribute_::tmp_bns(::std::size_t index) const {
  if (!tmp_bns_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
    return default_static_value->Get(index);
  }
  return tmp_bns_->Get(index);
}
void ConstOpAttribute::_OpAttribute_::clear_tmp_bns() {
  if (!tmp_bns_) {
    tmp_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return tmp_bns_->Clear();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* ConstOpAttribute::_OpAttribute_::mutable_tmp_bns() {
  if (!tmp_bns_) {
    tmp_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  tmp_bns_.get();
}
::std::string* ConstOpAttribute::_OpAttribute_::mutable_tmp_bns(::std::size_t index) {
  if (!tmp_bns_) {
    tmp_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return  tmp_bns_->Mutable(index);
}
void ConstOpAttribute::_OpAttribute_::add_tmp_bns(const ::std::string& value) {
  if (!tmp_bns_) {
    tmp_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return tmp_bns_->Add(value);
}
void ConstOpAttribute::_OpAttribute_::set_tmp_bns(::std::size_t index, const ::std::string& value) {
  if (!tmp_bns_) {
    tmp_bns_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>();
  }
  return tmp_bns_->Set(index, value);
}

// optional field op_conf
bool ConstOpAttribute::_OpAttribute_::has_op_conf() const {
  return has_op_conf_;
}
const ::oneflow::cfg::OperatorConf& ConstOpAttribute::_OpAttribute_::op_conf() const {
  if (!op_conf_) {
    static const ::std::shared_ptr<::oneflow::cfg::OperatorConf> default_static_value =
      ::std::make_shared<::oneflow::cfg::OperatorConf>();
    return *default_static_value;
  }
  return *(op_conf_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_op_conf() {
  if (op_conf_) {
    op_conf_->Clear();
  }
  has_op_conf_ = false;
}
::oneflow::cfg::OperatorConf* ConstOpAttribute::_OpAttribute_::mutable_op_conf() {
  if (!op_conf_) {
    op_conf_ = ::std::make_shared<::oneflow::cfg::OperatorConf>();
  }
  has_op_conf_ = true;
  return op_conf_.get();
}

// optional field arg_signature
bool ConstOpAttribute::_OpAttribute_::has_arg_signature() const {
  return has_arg_signature_;
}
const ::oneflow::cfg::ArgSignature& ConstOpAttribute::_OpAttribute_::arg_signature() const {
  if (!arg_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ArgSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ArgSignature>();
    return *default_static_value;
  }
  return *(arg_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_arg_signature() {
  if (arg_signature_) {
    arg_signature_->Clear();
  }
  has_arg_signature_ = false;
}
::oneflow::cfg::ArgSignature* ConstOpAttribute::_OpAttribute_::mutable_arg_signature() {
  if (!arg_signature_) {
    arg_signature_ = ::std::make_shared<::oneflow::cfg::ArgSignature>();
  }
  has_arg_signature_ = true;
  return arg_signature_.get();
}

// optional field arg_modifier_signature
bool ConstOpAttribute::_OpAttribute_::has_arg_modifier_signature() const {
  return has_arg_modifier_signature_;
}
const ::oneflow::cfg::ArgModifierSignature& ConstOpAttribute::_OpAttribute_::arg_modifier_signature() const {
  if (!arg_modifier_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ArgModifierSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ArgModifierSignature>();
    return *default_static_value;
  }
  return *(arg_modifier_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_arg_modifier_signature() {
  if (arg_modifier_signature_) {
    arg_modifier_signature_->Clear();
  }
  has_arg_modifier_signature_ = false;
}
::oneflow::cfg::ArgModifierSignature* ConstOpAttribute::_OpAttribute_::mutable_arg_modifier_signature() {
  if (!arg_modifier_signature_) {
    arg_modifier_signature_ = ::std::make_shared<::oneflow::cfg::ArgModifierSignature>();
  }
  has_arg_modifier_signature_ = true;
  return arg_modifier_signature_.get();
}

// optional field blob_last_used_signature
bool ConstOpAttribute::_OpAttribute_::has_blob_last_used_signature() const {
  return has_blob_last_used_signature_;
}
const ::oneflow::cfg::BlobLastUsedSignature& ConstOpAttribute::_OpAttribute_::blob_last_used_signature() const {
  if (!blob_last_used_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::BlobLastUsedSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::BlobLastUsedSignature>();
    return *default_static_value;
  }
  return *(blob_last_used_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_blob_last_used_signature() {
  if (blob_last_used_signature_) {
    blob_last_used_signature_->Clear();
  }
  has_blob_last_used_signature_ = false;
}
::oneflow::cfg::BlobLastUsedSignature* ConstOpAttribute::_OpAttribute_::mutable_blob_last_used_signature() {
  if (!blob_last_used_signature_) {
    blob_last_used_signature_ = ::std::make_shared<::oneflow::cfg::BlobLastUsedSignature>();
  }
  has_blob_last_used_signature_ = true;
  return blob_last_used_signature_.get();
}

// optional field blob_backward_used_signature
bool ConstOpAttribute::_OpAttribute_::has_blob_backward_used_signature() const {
  return has_blob_backward_used_signature_;
}
const ::oneflow::cfg::BlobBackwardUsedSignature& ConstOpAttribute::_OpAttribute_::blob_backward_used_signature() const {
  if (!blob_backward_used_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::BlobBackwardUsedSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::BlobBackwardUsedSignature>();
    return *default_static_value;
  }
  return *(blob_backward_used_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_blob_backward_used_signature() {
  if (blob_backward_used_signature_) {
    blob_backward_used_signature_->Clear();
  }
  has_blob_backward_used_signature_ = false;
}
::oneflow::cfg::BlobBackwardUsedSignature* ConstOpAttribute::_OpAttribute_::mutable_blob_backward_used_signature() {
  if (!blob_backward_used_signature_) {
    blob_backward_used_signature_ = ::std::make_shared<::oneflow::cfg::BlobBackwardUsedSignature>();
  }
  has_blob_backward_used_signature_ = true;
  return blob_backward_used_signature_.get();
}

// optional field sbp_signature
bool ConstOpAttribute::_OpAttribute_::has_sbp_signature() const {
  return has_sbp_signature_;
}
const ::oneflow::cfg::SbpSignature& ConstOpAttribute::_OpAttribute_::sbp_signature() const {
  if (!sbp_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::SbpSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::SbpSignature>();
    return *default_static_value;
  }
  return *(sbp_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_sbp_signature() {
  if (sbp_signature_) {
    sbp_signature_->Clear();
  }
  has_sbp_signature_ = false;
}
::oneflow::cfg::SbpSignature* ConstOpAttribute::_OpAttribute_::mutable_sbp_signature() {
  if (!sbp_signature_) {
    sbp_signature_ = ::std::make_shared<::oneflow::cfg::SbpSignature>();
  }
  has_sbp_signature_ = true;
  return sbp_signature_.get();
}

// optional field mirrored_signature
bool ConstOpAttribute::_OpAttribute_::has_mirrored_signature() const {
  return has_mirrored_signature_;
}
const ::oneflow::cfg::MirroredSignature& ConstOpAttribute::_OpAttribute_::mirrored_signature() const {
  if (!mirrored_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::MirroredSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::MirroredSignature>();
    return *default_static_value;
  }
  return *(mirrored_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_mirrored_signature() {
  if (mirrored_signature_) {
    mirrored_signature_->Clear();
  }
  has_mirrored_signature_ = false;
}
::oneflow::cfg::MirroredSignature* ConstOpAttribute::_OpAttribute_::mutable_mirrored_signature() {
  if (!mirrored_signature_) {
    mirrored_signature_ = ::std::make_shared<::oneflow::cfg::MirroredSignature>();
  }
  has_mirrored_signature_ = true;
  return mirrored_signature_.get();
}

// optional field logical_blob_desc_signature
bool ConstOpAttribute::_OpAttribute_::has_logical_blob_desc_signature() const {
  return has_logical_blob_desc_signature_;
}
const ::oneflow::cfg::BlobDescSignature& ConstOpAttribute::_OpAttribute_::logical_blob_desc_signature() const {
  if (!logical_blob_desc_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::BlobDescSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::BlobDescSignature>();
    return *default_static_value;
  }
  return *(logical_blob_desc_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_logical_blob_desc_signature() {
  if (logical_blob_desc_signature_) {
    logical_blob_desc_signature_->Clear();
  }
  has_logical_blob_desc_signature_ = false;
}
::oneflow::cfg::BlobDescSignature* ConstOpAttribute::_OpAttribute_::mutable_logical_blob_desc_signature() {
  if (!logical_blob_desc_signature_) {
    logical_blob_desc_signature_ = ::std::make_shared<::oneflow::cfg::BlobDescSignature>();
  }
  has_logical_blob_desc_signature_ = true;
  return logical_blob_desc_signature_.get();
}

// optional field parallel_signature
bool ConstOpAttribute::_OpAttribute_::has_parallel_signature() const {
  return has_parallel_signature_;
}
const ::oneflow::cfg::ParallelSignature& ConstOpAttribute::_OpAttribute_::parallel_signature() const {
  if (!parallel_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelSignature>();
    return *default_static_value;
  }
  return *(parallel_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_parallel_signature() {
  if (parallel_signature_) {
    parallel_signature_->Clear();
  }
  has_parallel_signature_ = false;
}
::oneflow::cfg::ParallelSignature* ConstOpAttribute::_OpAttribute_::mutable_parallel_signature() {
  if (!parallel_signature_) {
    parallel_signature_ = ::std::make_shared<::oneflow::cfg::ParallelSignature>();
  }
  has_parallel_signature_ = true;
  return parallel_signature_.get();
}

// optional field parallel_conf_signature
bool ConstOpAttribute::_OpAttribute_::has_parallel_conf_signature() const {
  return has_parallel_conf_signature_;
}
const ::oneflow::cfg::ParallelConfSignature& ConstOpAttribute::_OpAttribute_::parallel_conf_signature() const {
  if (!parallel_conf_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelConfSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelConfSignature>();
    return *default_static_value;
  }
  return *(parallel_conf_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_parallel_conf_signature() {
  if (parallel_conf_signature_) {
    parallel_conf_signature_->Clear();
  }
  has_parallel_conf_signature_ = false;
}
::oneflow::cfg::ParallelConfSignature* ConstOpAttribute::_OpAttribute_::mutable_parallel_conf_signature() {
  if (!parallel_conf_signature_) {
    parallel_conf_signature_ = ::std::make_shared<::oneflow::cfg::ParallelConfSignature>();
  }
  has_parallel_conf_signature_ = true;
  return parallel_conf_signature_.get();
}

// optional field parallel_distribution_signature
bool ConstOpAttribute::_OpAttribute_::has_parallel_distribution_signature() const {
  return has_parallel_distribution_signature_;
}
const ::oneflow::cfg::ParallelDistributionSignature& ConstOpAttribute::_OpAttribute_::parallel_distribution_signature() const {
  if (!parallel_distribution_signature_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelDistributionSignature> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelDistributionSignature>();
    return *default_static_value;
  }
  return *(parallel_distribution_signature_.get());
}
void ConstOpAttribute::_OpAttribute_::clear_parallel_distribution_signature() {
  if (parallel_distribution_signature_) {
    parallel_distribution_signature_->Clear();
  }
  has_parallel_distribution_signature_ = false;
}
::oneflow::cfg::ParallelDistributionSignature* ConstOpAttribute::_OpAttribute_::mutable_parallel_distribution_signature() {
  if (!parallel_distribution_signature_) {
    parallel_distribution_signature_ = ::std::make_shared<::oneflow::cfg::ParallelDistributionSignature>();
  }
  has_parallel_distribution_signature_ = true;
  return parallel_distribution_signature_.get();
}


int ConstOpAttribute::_OpAttribute_::compare(const _OpAttribute_& other) {
  if (!(input_bns() == other.input_bns())) {
    return input_bns() < other.input_bns() ? -1 : 1;
  }
  if (!(output_bns() == other.output_bns())) {
    return output_bns() < other.output_bns() ? -1 : 1;
  }
  if (!(tmp_bns() == other.tmp_bns())) {
    return tmp_bns() < other.tmp_bns() ? -1 : 1;
  }
  if (!(has_op_conf() == other.has_op_conf())) {
    return has_op_conf() < other.has_op_conf() ? -1 : 1;
  } else if (!(op_conf() == other.op_conf())) {
    return op_conf() < other.op_conf() ? -1 : 1;
  }
  if (!(has_arg_signature() == other.has_arg_signature())) {
    return has_arg_signature() < other.has_arg_signature() ? -1 : 1;
  } else if (!(arg_signature() == other.arg_signature())) {
    return arg_signature() < other.arg_signature() ? -1 : 1;
  }
  if (!(has_arg_modifier_signature() == other.has_arg_modifier_signature())) {
    return has_arg_modifier_signature() < other.has_arg_modifier_signature() ? -1 : 1;
  } else if (!(arg_modifier_signature() == other.arg_modifier_signature())) {
    return arg_modifier_signature() < other.arg_modifier_signature() ? -1 : 1;
  }
  if (!(has_blob_last_used_signature() == other.has_blob_last_used_signature())) {
    return has_blob_last_used_signature() < other.has_blob_last_used_signature() ? -1 : 1;
  } else if (!(blob_last_used_signature() == other.blob_last_used_signature())) {
    return blob_last_used_signature() < other.blob_last_used_signature() ? -1 : 1;
  }
  if (!(has_blob_backward_used_signature() == other.has_blob_backward_used_signature())) {
    return has_blob_backward_used_signature() < other.has_blob_backward_used_signature() ? -1 : 1;
  } else if (!(blob_backward_used_signature() == other.blob_backward_used_signature())) {
    return blob_backward_used_signature() < other.blob_backward_used_signature() ? -1 : 1;
  }
  if (!(has_sbp_signature() == other.has_sbp_signature())) {
    return has_sbp_signature() < other.has_sbp_signature() ? -1 : 1;
  } else if (!(sbp_signature() == other.sbp_signature())) {
    return sbp_signature() < other.sbp_signature() ? -1 : 1;
  }
  if (!(has_mirrored_signature() == other.has_mirrored_signature())) {
    return has_mirrored_signature() < other.has_mirrored_signature() ? -1 : 1;
  } else if (!(mirrored_signature() == other.mirrored_signature())) {
    return mirrored_signature() < other.mirrored_signature() ? -1 : 1;
  }
  if (!(has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature())) {
    return has_logical_blob_desc_signature() < other.has_logical_blob_desc_signature() ? -1 : 1;
  } else if (!(logical_blob_desc_signature() == other.logical_blob_desc_signature())) {
    return logical_blob_desc_signature() < other.logical_blob_desc_signature() ? -1 : 1;
  }
  if (!(has_parallel_signature() == other.has_parallel_signature())) {
    return has_parallel_signature() < other.has_parallel_signature() ? -1 : 1;
  } else if (!(parallel_signature() == other.parallel_signature())) {
    return parallel_signature() < other.parallel_signature() ? -1 : 1;
  }
  if (!(has_parallel_conf_signature() == other.has_parallel_conf_signature())) {
    return has_parallel_conf_signature() < other.has_parallel_conf_signature() ? -1 : 1;
  } else if (!(parallel_conf_signature() == other.parallel_conf_signature())) {
    return parallel_conf_signature() < other.parallel_conf_signature() ? -1 : 1;
  }
  if (!(has_parallel_distribution_signature() == other.has_parallel_distribution_signature())) {
    return has_parallel_distribution_signature() < other.has_parallel_distribution_signature() ? -1 : 1;
  } else if (!(parallel_distribution_signature() == other.parallel_distribution_signature())) {
    return parallel_distribution_signature() < other.parallel_distribution_signature() ? -1 : 1;
  }
  return 0;
}

bool ConstOpAttribute::_OpAttribute_::operator==(const _OpAttribute_& other) const {
  return true
    && input_bns() == other.input_bns()
    && output_bns() == other.output_bns()
    && tmp_bns() == other.tmp_bns()
    && has_op_conf() == other.has_op_conf() 
    && op_conf() == other.op_conf()
    && has_arg_signature() == other.has_arg_signature() 
    && arg_signature() == other.arg_signature()
    && has_arg_modifier_signature() == other.has_arg_modifier_signature() 
    && arg_modifier_signature() == other.arg_modifier_signature()
    && has_blob_last_used_signature() == other.has_blob_last_used_signature() 
    && blob_last_used_signature() == other.blob_last_used_signature()
    && has_blob_backward_used_signature() == other.has_blob_backward_used_signature() 
    && blob_backward_used_signature() == other.blob_backward_used_signature()
    && has_sbp_signature() == other.has_sbp_signature() 
    && sbp_signature() == other.sbp_signature()
    && has_mirrored_signature() == other.has_mirrored_signature() 
    && mirrored_signature() == other.mirrored_signature()
    && has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature() 
    && logical_blob_desc_signature() == other.logical_blob_desc_signature()
    && has_parallel_signature() == other.has_parallel_signature() 
    && parallel_signature() == other.parallel_signature()
    && has_parallel_conf_signature() == other.has_parallel_conf_signature() 
    && parallel_conf_signature() == other.parallel_conf_signature()
    && has_parallel_distribution_signature() == other.has_parallel_distribution_signature() 
    && parallel_distribution_signature() == other.parallel_distribution_signature()
  ;
}

std::size_t ConstOpAttribute::_OpAttribute_::__CalcHash__() const {
  return 0
    ^ input_bns().__CalcHash__()
    ^ output_bns().__CalcHash__()
    ^ tmp_bns().__CalcHash__()
    ^ (has_op_conf() ? std::hash<::oneflow::cfg::OperatorConf>()(op_conf()) : 0)
    ^ (has_arg_signature() ? std::hash<::oneflow::cfg::ArgSignature>()(arg_signature()) : 0)
    ^ (has_arg_modifier_signature() ? std::hash<::oneflow::cfg::ArgModifierSignature>()(arg_modifier_signature()) : 0)
    ^ (has_blob_last_used_signature() ? std::hash<::oneflow::cfg::BlobLastUsedSignature>()(blob_last_used_signature()) : 0)
    ^ (has_blob_backward_used_signature() ? std::hash<::oneflow::cfg::BlobBackwardUsedSignature>()(blob_backward_used_signature()) : 0)
    ^ (has_sbp_signature() ? std::hash<::oneflow::cfg::SbpSignature>()(sbp_signature()) : 0)
    ^ (has_mirrored_signature() ? std::hash<::oneflow::cfg::MirroredSignature>()(mirrored_signature()) : 0)
    ^ (has_logical_blob_desc_signature() ? std::hash<::oneflow::cfg::BlobDescSignature>()(logical_blob_desc_signature()) : 0)
    ^ (has_parallel_signature() ? std::hash<::oneflow::cfg::ParallelSignature>()(parallel_signature()) : 0)
    ^ (has_parallel_conf_signature() ? std::hash<::oneflow::cfg::ParallelConfSignature>()(parallel_conf_signature()) : 0)
    ^ (has_parallel_distribution_signature() ? std::hash<::oneflow::cfg::ParallelDistributionSignature>()(parallel_distribution_signature()) : 0)
  ;
}

bool ConstOpAttribute::_OpAttribute_::operator<(const _OpAttribute_& other) const {
  return false
    || !(input_bns() == other.input_bns()) ? 
      input_bns() < other.input_bns() : false
    || !(output_bns() == other.output_bns()) ? 
      output_bns() < other.output_bns() : false
    || !(tmp_bns() == other.tmp_bns()) ? 
      tmp_bns() < other.tmp_bns() : false
    || !(has_op_conf() == other.has_op_conf()) ? 
      has_op_conf() < other.has_op_conf() : false
    || !(op_conf() == other.op_conf()) ? 
      op_conf() < other.op_conf() : false
    || !(has_arg_signature() == other.has_arg_signature()) ? 
      has_arg_signature() < other.has_arg_signature() : false
    || !(arg_signature() == other.arg_signature()) ? 
      arg_signature() < other.arg_signature() : false
    || !(has_arg_modifier_signature() == other.has_arg_modifier_signature()) ? 
      has_arg_modifier_signature() < other.has_arg_modifier_signature() : false
    || !(arg_modifier_signature() == other.arg_modifier_signature()) ? 
      arg_modifier_signature() < other.arg_modifier_signature() : false
    || !(has_blob_last_used_signature() == other.has_blob_last_used_signature()) ? 
      has_blob_last_used_signature() < other.has_blob_last_used_signature() : false
    || !(blob_last_used_signature() == other.blob_last_used_signature()) ? 
      blob_last_used_signature() < other.blob_last_used_signature() : false
    || !(has_blob_backward_used_signature() == other.has_blob_backward_used_signature()) ? 
      has_blob_backward_used_signature() < other.has_blob_backward_used_signature() : false
    || !(blob_backward_used_signature() == other.blob_backward_used_signature()) ? 
      blob_backward_used_signature() < other.blob_backward_used_signature() : false
    || !(has_sbp_signature() == other.has_sbp_signature()) ? 
      has_sbp_signature() < other.has_sbp_signature() : false
    || !(sbp_signature() == other.sbp_signature()) ? 
      sbp_signature() < other.sbp_signature() : false
    || !(has_mirrored_signature() == other.has_mirrored_signature()) ? 
      has_mirrored_signature() < other.has_mirrored_signature() : false
    || !(mirrored_signature() == other.mirrored_signature()) ? 
      mirrored_signature() < other.mirrored_signature() : false
    || !(has_logical_blob_desc_signature() == other.has_logical_blob_desc_signature()) ? 
      has_logical_blob_desc_signature() < other.has_logical_blob_desc_signature() : false
    || !(logical_blob_desc_signature() == other.logical_blob_desc_signature()) ? 
      logical_blob_desc_signature() < other.logical_blob_desc_signature() : false
    || !(has_parallel_signature() == other.has_parallel_signature()) ? 
      has_parallel_signature() < other.has_parallel_signature() : false
    || !(parallel_signature() == other.parallel_signature()) ? 
      parallel_signature() < other.parallel_signature() : false
    || !(has_parallel_conf_signature() == other.has_parallel_conf_signature()) ? 
      has_parallel_conf_signature() < other.has_parallel_conf_signature() : false
    || !(parallel_conf_signature() == other.parallel_conf_signature()) ? 
      parallel_conf_signature() < other.parallel_conf_signature() : false
    || !(has_parallel_distribution_signature() == other.has_parallel_distribution_signature()) ? 
      has_parallel_distribution_signature() < other.has_parallel_distribution_signature() : false
    || !(parallel_distribution_signature() == other.parallel_distribution_signature()) ? 
      parallel_distribution_signature() < other.parallel_distribution_signature() : false
  ;
}

using _OpAttribute_ =  ConstOpAttribute::_OpAttribute_;
ConstOpAttribute::ConstOpAttribute(const ::std::shared_ptr<_OpAttribute_>& data): data_(data) {}
ConstOpAttribute::ConstOpAttribute(): data_(::std::make_shared<_OpAttribute_>()) {}
ConstOpAttribute::ConstOpAttribute(const ::oneflow::OpAttribute& proto_opattribute) {
  BuildFromProto(proto_opattribute);
}
ConstOpAttribute::ConstOpAttribute(const ConstOpAttribute&) = default;
ConstOpAttribute::ConstOpAttribute(ConstOpAttribute&&) noexcept = default;
ConstOpAttribute::~ConstOpAttribute() = default;

void ConstOpAttribute::ToProto(PbMessage* proto_opattribute) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpAttribute*>(proto_opattribute));
}
  
::std::string ConstOpAttribute::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpAttribute::__Empty__() const {
  return !data_;
}

int ConstOpAttribute::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"input_bns", 1},
    {"output_bns", 2},
    {"tmp_bns", 3},
    {"op_conf", 50},
    {"arg_signature", 100},
    {"arg_modifier_signature", 101},
    {"blob_last_used_signature", 102},
    {"blob_backward_used_signature", 103},
    {"sbp_signature", 104},
    {"mirrored_signature", 105},
    {"logical_blob_desc_signature", 106},
    {"parallel_signature", 108},
    {"parallel_conf_signature", 109},
    {"parallel_distribution_signature", 110},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpAttribute::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 50:
    case 100:
    case 101:
    case 102:
    case 103:
    case 104:
    case 105:
    case 106:
    case 108:
    case 109:
    case 110:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpAttribute::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::std::string>)
      };
      return type_indices;
    }
    case 50: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::OperatorConf),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstOperatorConf),
      };
      return type_indices;
    }
    case 100: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ArgSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstArgSignature),
      };
      return type_indices;
    }
    case 101: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ArgModifierSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstArgModifierSignature),
      };
      return type_indices;
    }
    case 102: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobLastUsedSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobLastUsedSignature),
      };
      return type_indices;
    }
    case 103: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobBackwardUsedSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobBackwardUsedSignature),
      };
      return type_indices;
    }
    case 104: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::SbpSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstSbpSignature),
      };
      return type_indices;
    }
    case 105: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::MirroredSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstMirroredSignature),
      };
      return type_indices;
    }
    case 106: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::BlobDescSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstBlobDescSignature),
      };
      return type_indices;
    }
    case 108: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelSignature),
      };
      return type_indices;
    }
    case 109: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelConfSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelConfSignature),
      };
      return type_indices;
    }
    case 110: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelDistributionSignature),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelDistributionSignature),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpAttribute::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &input_bns();
    case 2: return &output_bns();
    case 3: return &tmp_bns();
    case 50: return &op_conf();
    case 100: return &arg_signature();
    case 101: return &arg_modifier_signature();
    case 102: return &blob_last_used_signature();
    case 103: return &blob_backward_used_signature();
    case 104: return &sbp_signature();
    case 105: return &mirrored_signature();
    case 106: return &logical_blob_desc_signature();
    case 108: return &parallel_signature();
    case 109: return &parallel_conf_signature();
    case 110: return &parallel_distribution_signature();
    default: return nullptr;
  }
}

// repeated field input_bns
::std::size_t ConstOpAttribute::input_bns_size() const {
  return __SharedPtrOrDefault__()->input_bns_size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::input_bns() const {
  return __SharedPtrOrDefault__()->input_bns();
}
const ::std::string& ConstOpAttribute::input_bns(::std::size_t index) const {
  return __SharedPtrOrDefault__()->input_bns(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> ConstOpAttribute::shared_const_input_bns() const {
  return input_bns().__SharedConst__();
}
// repeated field output_bns
::std::size_t ConstOpAttribute::output_bns_size() const {
  return __SharedPtrOrDefault__()->output_bns_size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::output_bns() const {
  return __SharedPtrOrDefault__()->output_bns();
}
const ::std::string& ConstOpAttribute::output_bns(::std::size_t index) const {
  return __SharedPtrOrDefault__()->output_bns(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> ConstOpAttribute::shared_const_output_bns() const {
  return output_bns().__SharedConst__();
}
// repeated field tmp_bns
::std::size_t ConstOpAttribute::tmp_bns_size() const {
  return __SharedPtrOrDefault__()->tmp_bns_size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& ConstOpAttribute::tmp_bns() const {
  return __SharedPtrOrDefault__()->tmp_bns();
}
const ::std::string& ConstOpAttribute::tmp_bns(::std::size_t index) const {
  return __SharedPtrOrDefault__()->tmp_bns(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> ConstOpAttribute::shared_const_tmp_bns() const {
  return tmp_bns().__SharedConst__();
}
// required or optional field op_conf
bool ConstOpAttribute::has_op_conf() const {
  return __SharedPtrOrDefault__()->has_op_conf();
}
const ::oneflow::cfg::OperatorConf& ConstOpAttribute::op_conf() const {
  return __SharedPtrOrDefault__()->op_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstOperatorConf> ConstOpAttribute::shared_const_op_conf() const {
  return op_conf().__SharedConst__();
}
// required or optional field arg_signature
bool ConstOpAttribute::has_arg_signature() const {
  return __SharedPtrOrDefault__()->has_arg_signature();
}
const ::oneflow::cfg::ArgSignature& ConstOpAttribute::arg_signature() const {
  return __SharedPtrOrDefault__()->arg_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstArgSignature> ConstOpAttribute::shared_const_arg_signature() const {
  return arg_signature().__SharedConst__();
}
// required or optional field arg_modifier_signature
bool ConstOpAttribute::has_arg_modifier_signature() const {
  return __SharedPtrOrDefault__()->has_arg_modifier_signature();
}
const ::oneflow::cfg::ArgModifierSignature& ConstOpAttribute::arg_modifier_signature() const {
  return __SharedPtrOrDefault__()->arg_modifier_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstArgModifierSignature> ConstOpAttribute::shared_const_arg_modifier_signature() const {
  return arg_modifier_signature().__SharedConst__();
}
// required or optional field blob_last_used_signature
bool ConstOpAttribute::has_blob_last_used_signature() const {
  return __SharedPtrOrDefault__()->has_blob_last_used_signature();
}
const ::oneflow::cfg::BlobLastUsedSignature& ConstOpAttribute::blob_last_used_signature() const {
  return __SharedPtrOrDefault__()->blob_last_used_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobLastUsedSignature> ConstOpAttribute::shared_const_blob_last_used_signature() const {
  return blob_last_used_signature().__SharedConst__();
}
// required or optional field blob_backward_used_signature
bool ConstOpAttribute::has_blob_backward_used_signature() const {
  return __SharedPtrOrDefault__()->has_blob_backward_used_signature();
}
const ::oneflow::cfg::BlobBackwardUsedSignature& ConstOpAttribute::blob_backward_used_signature() const {
  return __SharedPtrOrDefault__()->blob_backward_used_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobBackwardUsedSignature> ConstOpAttribute::shared_const_blob_backward_used_signature() const {
  return blob_backward_used_signature().__SharedConst__();
}
// required or optional field sbp_signature
bool ConstOpAttribute::has_sbp_signature() const {
  return __SharedPtrOrDefault__()->has_sbp_signature();
}
const ::oneflow::cfg::SbpSignature& ConstOpAttribute::sbp_signature() const {
  return __SharedPtrOrDefault__()->sbp_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstSbpSignature> ConstOpAttribute::shared_const_sbp_signature() const {
  return sbp_signature().__SharedConst__();
}
// required or optional field mirrored_signature
bool ConstOpAttribute::has_mirrored_signature() const {
  return __SharedPtrOrDefault__()->has_mirrored_signature();
}
const ::oneflow::cfg::MirroredSignature& ConstOpAttribute::mirrored_signature() const {
  return __SharedPtrOrDefault__()->mirrored_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstMirroredSignature> ConstOpAttribute::shared_const_mirrored_signature() const {
  return mirrored_signature().__SharedConst__();
}
// required or optional field logical_blob_desc_signature
bool ConstOpAttribute::has_logical_blob_desc_signature() const {
  return __SharedPtrOrDefault__()->has_logical_blob_desc_signature();
}
const ::oneflow::cfg::BlobDescSignature& ConstOpAttribute::logical_blob_desc_signature() const {
  return __SharedPtrOrDefault__()->logical_blob_desc_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstBlobDescSignature> ConstOpAttribute::shared_const_logical_blob_desc_signature() const {
  return logical_blob_desc_signature().__SharedConst__();
}
// required or optional field parallel_signature
bool ConstOpAttribute::has_parallel_signature() const {
  return __SharedPtrOrDefault__()->has_parallel_signature();
}
const ::oneflow::cfg::ParallelSignature& ConstOpAttribute::parallel_signature() const {
  return __SharedPtrOrDefault__()->parallel_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelSignature> ConstOpAttribute::shared_const_parallel_signature() const {
  return parallel_signature().__SharedConst__();
}
// required or optional field parallel_conf_signature
bool ConstOpAttribute::has_parallel_conf_signature() const {
  return __SharedPtrOrDefault__()->has_parallel_conf_signature();
}
const ::oneflow::cfg::ParallelConfSignature& ConstOpAttribute::parallel_conf_signature() const {
  return __SharedPtrOrDefault__()->parallel_conf_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelConfSignature> ConstOpAttribute::shared_const_parallel_conf_signature() const {
  return parallel_conf_signature().__SharedConst__();
}
// required or optional field parallel_distribution_signature
bool ConstOpAttribute::has_parallel_distribution_signature() const {
  return __SharedPtrOrDefault__()->has_parallel_distribution_signature();
}
const ::oneflow::cfg::ParallelDistributionSignature& ConstOpAttribute::parallel_distribution_signature() const {
  return __SharedPtrOrDefault__()->parallel_distribution_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelDistributionSignature> ConstOpAttribute::shared_const_parallel_distribution_signature() const {
  return parallel_distribution_signature().__SharedConst__();
}

::std::shared_ptr<ConstOpAttribute> ConstOpAttribute::__SharedConst__() const {
  return ::std::make_shared<ConstOpAttribute>(data_);
}
int64_t ConstOpAttribute::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpAttribute::operator==(const ConstOpAttribute& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpAttribute::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpAttribute::operator<(const ConstOpAttribute& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpAttribute_>& ConstOpAttribute::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpAttribute_> default_ptr = std::make_shared<_OpAttribute_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpAttribute_>& ConstOpAttribute::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpAttribute_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpAttribute
void ConstOpAttribute::BuildFromProto(const PbMessage& proto_opattribute) {
  data_ = ::std::make_shared<_OpAttribute_>(dynamic_cast<const ::oneflow::OpAttribute&>(proto_opattribute));
}

OpAttribute::OpAttribute(const ::std::shared_ptr<_OpAttribute_>& data)
  : ConstOpAttribute(data) {}
OpAttribute::OpAttribute(const OpAttribute& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpAttribute> resize
OpAttribute::OpAttribute(OpAttribute&&) noexcept = default; 
OpAttribute::OpAttribute(const ::oneflow::OpAttribute& proto_opattribute) {
  InitFromProto(proto_opattribute);
}
OpAttribute::OpAttribute() = default;

OpAttribute::~OpAttribute() = default;

void OpAttribute::InitFromProto(const PbMessage& proto_opattribute) {
  BuildFromProto(proto_opattribute);
}
  
void* OpAttribute::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_input_bns();
    case 2: return mutable_output_bns();
    case 3: return mutable_tmp_bns();
    case 50: return mutable_op_conf();
    case 100: return mutable_arg_signature();
    case 101: return mutable_arg_modifier_signature();
    case 102: return mutable_blob_last_used_signature();
    case 103: return mutable_blob_backward_used_signature();
    case 104: return mutable_sbp_signature();
    case 105: return mutable_mirrored_signature();
    case 106: return mutable_logical_blob_desc_signature();
    case 108: return mutable_parallel_signature();
    case 109: return mutable_parallel_conf_signature();
    case 110: return mutable_parallel_distribution_signature();
    default: return nullptr;
  }
}

bool OpAttribute::operator==(const OpAttribute& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpAttribute::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpAttribute::operator<(const OpAttribute& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpAttribute::Clear() {
  if (data_) { data_.reset(); }
}
void OpAttribute::CopyFrom(const OpAttribute& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpAttribute& OpAttribute::operator=(const OpAttribute& other) {
  CopyFrom(other);
  return *this;
}

// repeated field input_bns
void OpAttribute::clear_input_bns() {
  return __SharedPtr__()->clear_input_bns();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* OpAttribute::mutable_input_bns() {
  return __SharedPtr__()->mutable_input_bns();
}
::std::string* OpAttribute::mutable_input_bns(::std::size_t index) {
  return __SharedPtr__()->mutable_input_bns(index);
}
void OpAttribute::add_input_bns(const ::std::string& value) {
  return __SharedPtr__()->add_input_bns(value);
}
void OpAttribute::set_input_bns(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_input_bns(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> OpAttribute::shared_mutable_input_bns() {
  return mutable_input_bns()->__SharedMutable__();
}
// repeated field output_bns
void OpAttribute::clear_output_bns() {
  return __SharedPtr__()->clear_output_bns();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* OpAttribute::mutable_output_bns() {
  return __SharedPtr__()->mutable_output_bns();
}
::std::string* OpAttribute::mutable_output_bns(::std::size_t index) {
  return __SharedPtr__()->mutable_output_bns(index);
}
void OpAttribute::add_output_bns(const ::std::string& value) {
  return __SharedPtr__()->add_output_bns(value);
}
void OpAttribute::set_output_bns(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_output_bns(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> OpAttribute::shared_mutable_output_bns() {
  return mutable_output_bns()->__SharedMutable__();
}
// repeated field tmp_bns
void OpAttribute::clear_tmp_bns() {
  return __SharedPtr__()->clear_tmp_bns();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_* OpAttribute::mutable_tmp_bns() {
  return __SharedPtr__()->mutable_tmp_bns();
}
::std::string* OpAttribute::mutable_tmp_bns(::std::size_t index) {
  return __SharedPtr__()->mutable_tmp_bns(index);
}
void OpAttribute::add_tmp_bns(const ::std::string& value) {
  return __SharedPtr__()->add_tmp_bns(value);
}
void OpAttribute::set_tmp_bns(::std::size_t index, const ::std::string& value) {
  return __SharedPtr__()->set_tmp_bns(index, value);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> OpAttribute::shared_mutable_tmp_bns() {
  return mutable_tmp_bns()->__SharedMutable__();
}
// required or optional field op_conf
void OpAttribute::clear_op_conf() {
  return __SharedPtr__()->clear_op_conf();
}
::oneflow::cfg::OperatorConf* OpAttribute::mutable_op_conf() {
  return __SharedPtr__()->mutable_op_conf();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::OperatorConf> OpAttribute::shared_mutable_op_conf() {
  return mutable_op_conf()->__SharedMutable__();
}
// required or optional field arg_signature
void OpAttribute::clear_arg_signature() {
  return __SharedPtr__()->clear_arg_signature();
}
::oneflow::cfg::ArgSignature* OpAttribute::mutable_arg_signature() {
  return __SharedPtr__()->mutable_arg_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ArgSignature> OpAttribute::shared_mutable_arg_signature() {
  return mutable_arg_signature()->__SharedMutable__();
}
// required or optional field arg_modifier_signature
void OpAttribute::clear_arg_modifier_signature() {
  return __SharedPtr__()->clear_arg_modifier_signature();
}
::oneflow::cfg::ArgModifierSignature* OpAttribute::mutable_arg_modifier_signature() {
  return __SharedPtr__()->mutable_arg_modifier_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ArgModifierSignature> OpAttribute::shared_mutable_arg_modifier_signature() {
  return mutable_arg_modifier_signature()->__SharedMutable__();
}
// required or optional field blob_last_used_signature
void OpAttribute::clear_blob_last_used_signature() {
  return __SharedPtr__()->clear_blob_last_used_signature();
}
::oneflow::cfg::BlobLastUsedSignature* OpAttribute::mutable_blob_last_used_signature() {
  return __SharedPtr__()->mutable_blob_last_used_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobLastUsedSignature> OpAttribute::shared_mutable_blob_last_used_signature() {
  return mutable_blob_last_used_signature()->__SharedMutable__();
}
// required or optional field blob_backward_used_signature
void OpAttribute::clear_blob_backward_used_signature() {
  return __SharedPtr__()->clear_blob_backward_used_signature();
}
::oneflow::cfg::BlobBackwardUsedSignature* OpAttribute::mutable_blob_backward_used_signature() {
  return __SharedPtr__()->mutable_blob_backward_used_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobBackwardUsedSignature> OpAttribute::shared_mutable_blob_backward_used_signature() {
  return mutable_blob_backward_used_signature()->__SharedMutable__();
}
// required or optional field sbp_signature
void OpAttribute::clear_sbp_signature() {
  return __SharedPtr__()->clear_sbp_signature();
}
::oneflow::cfg::SbpSignature* OpAttribute::mutable_sbp_signature() {
  return __SharedPtr__()->mutable_sbp_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::SbpSignature> OpAttribute::shared_mutable_sbp_signature() {
  return mutable_sbp_signature()->__SharedMutable__();
}
// required or optional field mirrored_signature
void OpAttribute::clear_mirrored_signature() {
  return __SharedPtr__()->clear_mirrored_signature();
}
::oneflow::cfg::MirroredSignature* OpAttribute::mutable_mirrored_signature() {
  return __SharedPtr__()->mutable_mirrored_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::MirroredSignature> OpAttribute::shared_mutable_mirrored_signature() {
  return mutable_mirrored_signature()->__SharedMutable__();
}
// required or optional field logical_blob_desc_signature
void OpAttribute::clear_logical_blob_desc_signature() {
  return __SharedPtr__()->clear_logical_blob_desc_signature();
}
::oneflow::cfg::BlobDescSignature* OpAttribute::mutable_logical_blob_desc_signature() {
  return __SharedPtr__()->mutable_logical_blob_desc_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::BlobDescSignature> OpAttribute::shared_mutable_logical_blob_desc_signature() {
  return mutable_logical_blob_desc_signature()->__SharedMutable__();
}
// required or optional field parallel_signature
void OpAttribute::clear_parallel_signature() {
  return __SharedPtr__()->clear_parallel_signature();
}
::oneflow::cfg::ParallelSignature* OpAttribute::mutable_parallel_signature() {
  return __SharedPtr__()->mutable_parallel_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelSignature> OpAttribute::shared_mutable_parallel_signature() {
  return mutable_parallel_signature()->__SharedMutable__();
}
// required or optional field parallel_conf_signature
void OpAttribute::clear_parallel_conf_signature() {
  return __SharedPtr__()->clear_parallel_conf_signature();
}
::oneflow::cfg::ParallelConfSignature* OpAttribute::mutable_parallel_conf_signature() {
  return __SharedPtr__()->mutable_parallel_conf_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelConfSignature> OpAttribute::shared_mutable_parallel_conf_signature() {
  return mutable_parallel_conf_signature()->__SharedMutable__();
}
// required or optional field parallel_distribution_signature
void OpAttribute::clear_parallel_distribution_signature() {
  return __SharedPtr__()->clear_parallel_distribution_signature();
}
::oneflow::cfg::ParallelDistributionSignature* OpAttribute::mutable_parallel_distribution_signature() {
  return __SharedPtr__()->mutable_parallel_distribution_signature();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelDistributionSignature> OpAttribute::shared_mutable_parallel_distribution_signature() {
  return mutable_parallel_distribution_signature()->__SharedMutable__();
}

::std::shared_ptr<OpAttribute> OpAttribute::__SharedMutable__() {
  return ::std::make_shared<OpAttribute>(__SharedPtr__());
}
ConstOpAttributeList::_OpAttributeList_::_OpAttributeList_() { Clear(); }
ConstOpAttributeList::_OpAttributeList_::_OpAttributeList_(const _OpAttributeList_& other) { CopyFrom(other); }
ConstOpAttributeList::_OpAttributeList_::_OpAttributeList_(const ::oneflow::OpAttributeList& proto_opattributelist) {
  InitFromProto(proto_opattributelist);
}
ConstOpAttributeList::_OpAttributeList_::_OpAttributeList_(_OpAttributeList_&& other) = default;
ConstOpAttributeList::_OpAttributeList_::~_OpAttributeList_() = default;

void ConstOpAttributeList::_OpAttributeList_::InitFromProto(const ::oneflow::OpAttributeList& proto_opattributelist) {
  Clear();
  // repeated field: op_attribute
  if (!proto_opattributelist.op_attribute().empty()) {
    for (const ::oneflow::OpAttribute& elem : proto_opattributelist.op_attribute() ) {
      *mutable_op_attribute()->Add() = ::oneflow::cfg::OpAttribute(elem);
    }
  }
    
}

void ConstOpAttributeList::_OpAttributeList_::ToProto(::oneflow::OpAttributeList* proto_opattributelist) const {
  proto_opattributelist->Clear();
  // repeated field: op_attribute
  if (!op_attribute().empty()) {
    for (const ::oneflow::cfg::OpAttribute& elem : op_attribute() ) {
      ::oneflow::OpAttribute proto_op_attribute_elem;
      elem.ToProto(&proto_op_attribute_elem);
      *proto_opattributelist->mutable_op_attribute()->Add() = proto_op_attribute_elem;
    }
  }

}

::std::string ConstOpAttributeList::_OpAttributeList_::DebugString() const {
  ::oneflow::OpAttributeList proto_opattributelist;
  this->ToProto(&proto_opattributelist);
  return proto_opattributelist.DebugString();
}

void ConstOpAttributeList::_OpAttributeList_::Clear() {
  clear_op_attribute();
}

void ConstOpAttributeList::_OpAttributeList_::CopyFrom(const _OpAttributeList_& other) {
  mutable_op_attribute()->CopyFrom(other.op_attribute());
}


// repeated field op_attribute
::std::size_t ConstOpAttributeList::_OpAttributeList_::op_attribute_size() const {
  if (!op_attribute_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
    return default_static_value->size();
  }
  return op_attribute_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& ConstOpAttributeList::_OpAttributeList_::op_attribute() const {
  if (!op_attribute_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
    return *(default_static_value.get());
  }
  return *(op_attribute_.get());
}
const ::oneflow::cfg::OpAttribute& ConstOpAttributeList::_OpAttributeList_::op_attribute(::std::size_t index) const {
  if (!op_attribute_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
    return default_static_value->Get(index);
  }
  return op_attribute_->Get(index);
}
void ConstOpAttributeList::_OpAttributeList_::clear_op_attribute() {
  if (!op_attribute_) {
    op_attribute_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
  }
  return op_attribute_->Clear();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_* ConstOpAttributeList::_OpAttributeList_::mutable_op_attribute() {
  if (!op_attribute_) {
    op_attribute_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
  }
  return  op_attribute_.get();
}
::oneflow::cfg::OpAttribute* ConstOpAttributeList::_OpAttributeList_::mutable_op_attribute(::std::size_t index) {
  if (!op_attribute_) {
    op_attribute_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
  }
  return  op_attribute_->Mutable(index);
}
::oneflow::cfg::OpAttribute* ConstOpAttributeList::_OpAttributeList_::add_op_attribute() {
  if (!op_attribute_) {
    op_attribute_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>();
  }
  return op_attribute_->Add();
}


int ConstOpAttributeList::_OpAttributeList_::compare(const _OpAttributeList_& other) {
  if (!(op_attribute() == other.op_attribute())) {
    return op_attribute() < other.op_attribute() ? -1 : 1;
  }
  return 0;
}

bool ConstOpAttributeList::_OpAttributeList_::operator==(const _OpAttributeList_& other) const {
  return true
    && op_attribute() == other.op_attribute()
  ;
}

std::size_t ConstOpAttributeList::_OpAttributeList_::__CalcHash__() const {
  return 0
    ^ op_attribute().__CalcHash__()
  ;
}

bool ConstOpAttributeList::_OpAttributeList_::operator<(const _OpAttributeList_& other) const {
  return false
    || !(op_attribute() == other.op_attribute()) ? 
      op_attribute() < other.op_attribute() : false
  ;
}

using _OpAttributeList_ =  ConstOpAttributeList::_OpAttributeList_;
ConstOpAttributeList::ConstOpAttributeList(const ::std::shared_ptr<_OpAttributeList_>& data): data_(data) {}
ConstOpAttributeList::ConstOpAttributeList(): data_(::std::make_shared<_OpAttributeList_>()) {}
ConstOpAttributeList::ConstOpAttributeList(const ::oneflow::OpAttributeList& proto_opattributelist) {
  BuildFromProto(proto_opattributelist);
}
ConstOpAttributeList::ConstOpAttributeList(const ConstOpAttributeList&) = default;
ConstOpAttributeList::ConstOpAttributeList(ConstOpAttributeList&&) noexcept = default;
ConstOpAttributeList::~ConstOpAttributeList() = default;

void ConstOpAttributeList::ToProto(PbMessage* proto_opattributelist) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OpAttributeList*>(proto_opattributelist));
}
  
::std::string ConstOpAttributeList::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOpAttributeList::__Empty__() const {
  return !data_;
}

int ConstOpAttributeList::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"op_attribute", 1},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOpAttributeList::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOpAttributeList::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpAttribute>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOpAttributeList::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &op_attribute();
    default: return nullptr;
  }
}

// repeated field op_attribute
::std::size_t ConstOpAttributeList::op_attribute_size() const {
  return __SharedPtrOrDefault__()->op_attribute_size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& ConstOpAttributeList::op_attribute() const {
  return __SharedPtrOrDefault__()->op_attribute();
}
const ::oneflow::cfg::OpAttribute& ConstOpAttributeList::op_attribute(::std::size_t index) const {
  return __SharedPtrOrDefault__()->op_attribute(index);
}
// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> ConstOpAttributeList::shared_const_op_attribute() const {
  return op_attribute().__SharedConst__();
}
::std::shared_ptr<::oneflow::cfg::ConstOpAttribute> ConstOpAttributeList::shared_const_op_attribute(::std::size_t index) const {
  return op_attribute(index).__SharedConst__();
}

::std::shared_ptr<ConstOpAttributeList> ConstOpAttributeList::__SharedConst__() const {
  return ::std::make_shared<ConstOpAttributeList>(data_);
}
int64_t ConstOpAttributeList::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOpAttributeList::operator==(const ConstOpAttributeList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOpAttributeList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOpAttributeList::operator<(const ConstOpAttributeList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OpAttributeList_>& ConstOpAttributeList::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OpAttributeList_> default_ptr = std::make_shared<_OpAttributeList_>();
  return default_ptr;
}
const ::std::shared_ptr<_OpAttributeList_>& ConstOpAttributeList::__SharedPtr__() {
  if (!data_) { data_.reset(new _OpAttributeList_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOpAttributeList
void ConstOpAttributeList::BuildFromProto(const PbMessage& proto_opattributelist) {
  data_ = ::std::make_shared<_OpAttributeList_>(dynamic_cast<const ::oneflow::OpAttributeList&>(proto_opattributelist));
}

OpAttributeList::OpAttributeList(const ::std::shared_ptr<_OpAttributeList_>& data)
  : ConstOpAttributeList(data) {}
OpAttributeList::OpAttributeList(const OpAttributeList& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OpAttributeList> resize
OpAttributeList::OpAttributeList(OpAttributeList&&) noexcept = default; 
OpAttributeList::OpAttributeList(const ::oneflow::OpAttributeList& proto_opattributelist) {
  InitFromProto(proto_opattributelist);
}
OpAttributeList::OpAttributeList() = default;

OpAttributeList::~OpAttributeList() = default;

void OpAttributeList::InitFromProto(const PbMessage& proto_opattributelist) {
  BuildFromProto(proto_opattributelist);
}
  
void* OpAttributeList::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_op_attribute();
    default: return nullptr;
  }
}

bool OpAttributeList::operator==(const OpAttributeList& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OpAttributeList::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OpAttributeList::operator<(const OpAttributeList& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OpAttributeList::Clear() {
  if (data_) { data_.reset(); }
}
void OpAttributeList::CopyFrom(const OpAttributeList& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OpAttributeList& OpAttributeList::operator=(const OpAttributeList& other) {
  CopyFrom(other);
  return *this;
}

// repeated field op_attribute
void OpAttributeList::clear_op_attribute() {
  return __SharedPtr__()->clear_op_attribute();
}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_* OpAttributeList::mutable_op_attribute() {
  return __SharedPtr__()->mutable_op_attribute();
}
::oneflow::cfg::OpAttribute* OpAttributeList::mutable_op_attribute(::std::size_t index) {
  return __SharedPtr__()->mutable_op_attribute(index);
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> OpAttributeList::shared_mutable_op_attribute() {
  return mutable_op_attribute()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OpAttribute> OpAttributeList::shared_mutable_op_attribute(::std::size_t index) {
  return mutable_op_attribute(index)->__SharedMutable__();
}
::oneflow::cfg::OpAttribute* OpAttributeList::add_op_attribute() {
  return __SharedPtr__()->add_op_attribute();
}

::std::shared_ptr<OpAttributeList> OpAttributeList::__SharedMutable__() {
  return ::std::make_shared<OpAttributeList>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): ::oneflow::cfg::_RepeatedField_<::std::string>(data) {}
Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_() = default;
Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_() = default;


bool Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}

_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_(const ::std::shared_ptr<::std::vector<::std::string>>& data): Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_(data) {}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_() = default;
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::~_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_() = default;

void _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) {
  ::oneflow::cfg::_RepeatedField_<::std::string>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::std::string>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>(__SharedPtr__());
}
Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpAttribute>>& data): ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpAttribute>(data) {}
Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_() = default;
Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::~Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_() = default;


bool Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OpAttribute>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>(__SharedPtr__());
}
  ::std::shared_ptr<::oneflow::cfg::ConstOpAttribute> Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedConst__(::std::size_t index) const {
    return Get(index).__SharedConst__();
  }

_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_(const ::std::shared_ptr<::std::vector<::oneflow::cfg::OpAttribute>>& data): Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_(data) {}
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_() = default;
_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::~_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_() = default;

void _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpAttribute>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) {
  ::oneflow::cfg::_RepeatedField_<::oneflow::cfg::OpAttribute>::CopyFrom(other);
}
bool _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::operator==(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& hash = std::hash<::oneflow::cfg::OpAttribute>();
  for (const auto& elem : *__SharedPtr__()) { hash_value ^= hash(elem); }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::operator<(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>(__SharedPtr__());
}
::std::shared_ptr<::oneflow::cfg::OpAttribute> _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedAdd__() {
  return Add()->__SharedMutable__();
}
::std::shared_ptr<::oneflow::cfg::OpAttribute> _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedMutable__(::std::size_t index) {
  return Mutable(index)->__SharedMutable__();
}

}
} // namespace oneflow
