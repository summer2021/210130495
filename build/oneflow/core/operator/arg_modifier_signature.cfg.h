#ifndef CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H_
#define CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module

// forward declare class defined in other module

namespace oneflow {

// forward declare proto class;
class InputBlobModifier;
class OutputBlobModifier;
class ArgModifierSignature_Ibn2inputBlobModifierEntry;
class ArgModifierSignature_Obn2outputBlobModifierEntry;
class ArgModifierSignature;

namespace cfg {


class InputBlobModifier;
class ConstInputBlobModifier;

class OutputBlobModifier;
class ConstOutputBlobModifier;

class ArgModifierSignature;
class ConstArgModifierSignature;



class ConstInputBlobModifier : public ::oneflow::cfg::Message {
 public:

  class _InputBlobModifier_ {
   public:
    _InputBlobModifier_();
    explicit _InputBlobModifier_(const _InputBlobModifier_& other);
    explicit _InputBlobModifier_(_InputBlobModifier_&& other);
    _InputBlobModifier_(const ::oneflow::InputBlobModifier& proto_inputblobmodifier);
    ~_InputBlobModifier_();

    void InitFromProto(const ::oneflow::InputBlobModifier& proto_inputblobmodifier);

    void ToProto(::oneflow::InputBlobModifier* proto_inputblobmodifier) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InputBlobModifier_& other);
  
      // optional field is_mutable
     public:
    bool has_is_mutable() const;
    const bool& is_mutable() const;
    void clear_is_mutable();
    void set_is_mutable(const bool& value);
    bool* mutable_is_mutable();
   protected:
    bool has_is_mutable_ = false;
    bool is_mutable_;
      
      // optional field requires_grad
     public:
    bool has_requires_grad() const;
    const bool& requires_grad() const;
    void clear_requires_grad();
    void set_requires_grad(const bool& value);
    bool* mutable_requires_grad();
   protected:
    bool has_requires_grad_ = false;
    bool requires_grad_;
           
   public:
    int compare(const _InputBlobModifier_& other);

    bool operator==(const _InputBlobModifier_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InputBlobModifier_& other) const;
  };

  ConstInputBlobModifier(const ::std::shared_ptr<_InputBlobModifier_>& data);
  ConstInputBlobModifier(const ConstInputBlobModifier&);
  ConstInputBlobModifier(ConstInputBlobModifier&&) noexcept;
  ConstInputBlobModifier();
  ConstInputBlobModifier(const ::oneflow::InputBlobModifier& proto_inputblobmodifier);
  virtual ~ConstInputBlobModifier() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_inputblobmodifier) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field is_mutable
 public:
  bool has_is_mutable() const;
  const bool& is_mutable() const;
  // used by pybind11 only
  // required or optional field requires_grad
 public:
  bool has_requires_grad() const;
  const bool& requires_grad() const;
  // used by pybind11 only

 public:
  ::std::shared_ptr<ConstInputBlobModifier> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInputBlobModifier& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInputBlobModifier& other) const;
 protected:
  const ::std::shared_ptr<_InputBlobModifier_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InputBlobModifier_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInputBlobModifier
  void BuildFromProto(const PbMessage& proto_inputblobmodifier);
  
  ::std::shared_ptr<_InputBlobModifier_> data_;
};

class InputBlobModifier final : public ConstInputBlobModifier {
 public:
  InputBlobModifier(const ::std::shared_ptr<_InputBlobModifier_>& data);
  InputBlobModifier(const InputBlobModifier& other);
  // enable nothrow for ::std::vector<InputBlobModifier> resize 
  InputBlobModifier(InputBlobModifier&&) noexcept;
  InputBlobModifier();
  explicit InputBlobModifier(const ::oneflow::InputBlobModifier& proto_inputblobmodifier);

  ~InputBlobModifier() override;

  void InitFromProto(const PbMessage& proto_inputblobmodifier) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InputBlobModifier& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InputBlobModifier& other) const;
  void Clear();
  void CopyFrom(const InputBlobModifier& other);
  InputBlobModifier& operator=(const InputBlobModifier& other);

  // required or optional field is_mutable
 public:
  void clear_is_mutable();
  void set_is_mutable(const bool& value);
  bool* mutable_is_mutable();
  // required or optional field requires_grad
 public:
  void clear_requires_grad();
  void set_requires_grad(const bool& value);
  bool* mutable_requires_grad();

  ::std::shared_ptr<InputBlobModifier> __SharedMutable__();
};


class ConstOutputBlobModifier : public ::oneflow::cfg::Message {
 public:

 // oneof enum inplace_type
 enum InplaceTypeCase : unsigned int {
  INPLACE_TYPE_NOT_SET = 0,
    kMutableInplaceIbn = 20,
    kConstInplaceIbn = 21,
   };

  class _OutputBlobModifier_ {
   public:
    _OutputBlobModifier_();
    explicit _OutputBlobModifier_(const _OutputBlobModifier_& other);
    explicit _OutputBlobModifier_(_OutputBlobModifier_&& other);
    _OutputBlobModifier_(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier);
    ~_OutputBlobModifier_();

    void InitFromProto(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier);

    void ToProto(::oneflow::OutputBlobModifier* proto_outputblobmodifier) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _OutputBlobModifier_& other);
  
      // optional field is_mutable
     public:
    bool has_is_mutable() const;
    const bool& is_mutable() const;
    void clear_is_mutable();
    void set_is_mutable(const bool& value);
    bool* mutable_is_mutable();
   protected:
    bool has_is_mutable_ = false;
    bool is_mutable_;
      
      // optional field requires_grad
     public:
    bool has_requires_grad() const;
    const bool& requires_grad() const;
    void clear_requires_grad();
    void set_requires_grad(const bool& value);
    bool* mutable_requires_grad();
   protected:
    bool has_requires_grad_ = false;
    bool requires_grad_;
      
      // optional field header_infered_before_compute
     public:
    bool has_header_infered_before_compute() const;
    const bool& header_infered_before_compute() const;
    void clear_header_infered_before_compute();
    void set_header_infered_before_compute(const bool& value);
    bool* mutable_header_infered_before_compute();
   protected:
    bool has_header_infered_before_compute_ = false;
    bool header_infered_before_compute_;
      
     // oneof field inplace_type: mutable_inplace_ibn
   public:
    bool has_mutable_inplace_ibn() const;
    void clear_mutable_inplace_ibn();
    const ::std::string& mutable_inplace_ibn() const;
      void set_mutable_inplace_ibn(const ::std::string& value);
    ::std::string* mutable_mutable_inplace_ibn();
      
     // oneof field inplace_type: const_inplace_ibn
   public:
    bool has_const_inplace_ibn() const;
    void clear_const_inplace_ibn();
    const ::std::string& const_inplace_ibn() const;
      void set_const_inplace_ibn(const ::std::string& value);
    ::std::string* mutable_const_inplace_ibn();
           
   public:
    // oneof inplace_type
    InplaceTypeCase inplace_type_case() const;
    bool has_inplace_type() const;
   protected:
    void clear_inplace_type();
    void inplace_type_copy_from(const _OutputBlobModifier_& other);
    union InplaceTypeUnion {
      // 64-bit aligned
      uint64_t __inplace_type_for_padding_64bit__;
          char mutable_inplace_ibn_[sizeof(::std::string)];
            char const_inplace_ibn_[sizeof(::std::string)];
        } inplace_type_;
    InplaceTypeCase inplace_type_case_ = INPLACE_TYPE_NOT_SET;
     
   public:
    int compare(const _OutputBlobModifier_& other);

    bool operator==(const _OutputBlobModifier_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _OutputBlobModifier_& other) const;
  };

  ConstOutputBlobModifier(const ::std::shared_ptr<_OutputBlobModifier_>& data);
  ConstOutputBlobModifier(const ConstOutputBlobModifier&);
  ConstOutputBlobModifier(ConstOutputBlobModifier&&) noexcept;
  ConstOutputBlobModifier();
  ConstOutputBlobModifier(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier);
  virtual ~ConstOutputBlobModifier() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_outputblobmodifier) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field is_mutable
 public:
  bool has_is_mutable() const;
  const bool& is_mutable() const;
  // used by pybind11 only
  // required or optional field requires_grad
 public:
  bool has_requires_grad() const;
  const bool& requires_grad() const;
  // used by pybind11 only
  // required or optional field header_infered_before_compute
 public:
  bool has_header_infered_before_compute() const;
  const bool& header_infered_before_compute() const;
  // used by pybind11 only
 // oneof field inplace_type: mutable_inplace_ibn
 public:
  bool has_mutable_inplace_ibn() const;
  const ::std::string& mutable_inplace_ibn() const;
  // used by pybind11 only
 // oneof field inplace_type: const_inplace_ibn
 public:
  bool has_const_inplace_ibn() const;
  const ::std::string& const_inplace_ibn() const;
  // used by pybind11 only
 public:
  InplaceTypeCase inplace_type_case() const;
 protected:
  bool has_inplace_type() const;

 public:
  ::std::shared_ptr<ConstOutputBlobModifier> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstOutputBlobModifier& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstOutputBlobModifier& other) const;
 protected:
  const ::std::shared_ptr<_OutputBlobModifier_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_OutputBlobModifier_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstOutputBlobModifier
  void BuildFromProto(const PbMessage& proto_outputblobmodifier);
  
  ::std::shared_ptr<_OutputBlobModifier_> data_;
};

class OutputBlobModifier final : public ConstOutputBlobModifier {
 public:
  OutputBlobModifier(const ::std::shared_ptr<_OutputBlobModifier_>& data);
  OutputBlobModifier(const OutputBlobModifier& other);
  // enable nothrow for ::std::vector<OutputBlobModifier> resize 
  OutputBlobModifier(OutputBlobModifier&&) noexcept;
  OutputBlobModifier();
  explicit OutputBlobModifier(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier);

  ~OutputBlobModifier() override;

  void InitFromProto(const PbMessage& proto_outputblobmodifier) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const OutputBlobModifier& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const OutputBlobModifier& other) const;
  void Clear();
  void CopyFrom(const OutputBlobModifier& other);
  OutputBlobModifier& operator=(const OutputBlobModifier& other);

  // required or optional field is_mutable
 public:
  void clear_is_mutable();
  void set_is_mutable(const bool& value);
  bool* mutable_is_mutable();
  // required or optional field requires_grad
 public:
  void clear_requires_grad();
  void set_requires_grad(const bool& value);
  bool* mutable_requires_grad();
  // required or optional field header_infered_before_compute
 public:
  void clear_header_infered_before_compute();
  void set_header_infered_before_compute(const bool& value);
  bool* mutable_header_infered_before_compute();
  void clear_mutable_inplace_ibn();
  void set_mutable_inplace_ibn(const ::std::string& value);
  ::std::string* mutable_mutable_inplace_ibn();
  void clear_const_inplace_ibn();
  void set_const_inplace_ibn(const ::std::string& value);
  ::std::string* mutable_const_inplace_ibn();

  ::std::shared_ptr<OutputBlobModifier> __SharedMutable__();
};

class _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_;
class _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_; 
class Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_;

class ConstArgModifierSignature : public ::oneflow::cfg::Message {
 public:

  class _ArgModifierSignature_ {
   public:
    _ArgModifierSignature_();
    explicit _ArgModifierSignature_(const _ArgModifierSignature_& other);
    explicit _ArgModifierSignature_(_ArgModifierSignature_&& other);
    _ArgModifierSignature_(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature);
    ~_ArgModifierSignature_();

    void InitFromProto(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature);

    void ToProto(::oneflow::ArgModifierSignature* proto_argmodifiersignature) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _ArgModifierSignature_& other);
  
     public:
    ::std::size_t ibn2input_blob_modifier_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& ibn2input_blob_modifier() const;

    _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ * mutable_ibn2input_blob_modifier();

    const ::oneflow::cfg::InputBlobModifier& ibn2input_blob_modifier(::std::string key) const;

    void clear_ibn2input_blob_modifier();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> ibn2input_blob_modifier_;
    
     public:
    ::std::size_t obn2output_blob_modifier_size() const;
    const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& obn2output_blob_modifier() const;

    _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ * mutable_obn2output_blob_modifier();

    const ::oneflow::cfg::OutputBlobModifier& obn2output_blob_modifier(::std::string key) const;

    void clear_obn2output_blob_modifier();
       protected:
    ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> obn2output_blob_modifier_;
         
   public:
    int compare(const _ArgModifierSignature_& other);

    bool operator==(const _ArgModifierSignature_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _ArgModifierSignature_& other) const;
  };

  ConstArgModifierSignature(const ::std::shared_ptr<_ArgModifierSignature_>& data);
  ConstArgModifierSignature(const ConstArgModifierSignature&);
  ConstArgModifierSignature(ConstArgModifierSignature&&) noexcept;
  ConstArgModifierSignature();
  ConstArgModifierSignature(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature);
  virtual ~ConstArgModifierSignature() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_argmodifiersignature) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // map field ibn2input_blob_modifier
 public:
  ::std::size_t ibn2input_blob_modifier_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& ibn2input_blob_modifier() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> shared_const_ibn2input_blob_modifier() const;
  // map field obn2output_blob_modifier
 public:
  ::std::size_t obn2output_blob_modifier_size() const;
  const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& obn2output_blob_modifier() const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> shared_const_obn2output_blob_modifier() const;

 public:
  ::std::shared_ptr<ConstArgModifierSignature> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstArgModifierSignature& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstArgModifierSignature& other) const;
 protected:
  const ::std::shared_ptr<_ArgModifierSignature_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_ArgModifierSignature_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstArgModifierSignature
  void BuildFromProto(const PbMessage& proto_argmodifiersignature);
  
  ::std::shared_ptr<_ArgModifierSignature_> data_;
};

class ArgModifierSignature final : public ConstArgModifierSignature {
 public:
  ArgModifierSignature(const ::std::shared_ptr<_ArgModifierSignature_>& data);
  ArgModifierSignature(const ArgModifierSignature& other);
  // enable nothrow for ::std::vector<ArgModifierSignature> resize 
  ArgModifierSignature(ArgModifierSignature&&) noexcept;
  ArgModifierSignature();
  explicit ArgModifierSignature(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature);

  ~ArgModifierSignature() override;

  void InitFromProto(const PbMessage& proto_argmodifiersignature) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const ArgModifierSignature& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const ArgModifierSignature& other) const;
  void Clear();
  void CopyFrom(const ArgModifierSignature& other);
  ArgModifierSignature& operator=(const ArgModifierSignature& other);

  // repeated field ibn2input_blob_modifier
 public:
  void clear_ibn2input_blob_modifier();

  const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ & ibn2input_blob_modifier() const;

  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_* mutable_ibn2input_blob_modifier();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> shared_mutable_ibn2input_blob_modifier();
  // repeated field obn2output_blob_modifier
 public:
  void clear_obn2output_blob_modifier();

  const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ & obn2output_blob_modifier() const;

  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_* mutable_obn2output_blob_modifier();

  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> shared_mutable_obn2output_blob_modifier();

  ::std::shared_ptr<ArgModifierSignature> __SharedMutable__();
};










// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::InputBlobModifier> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::InputBlobModifier>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::InputBlobModifier& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstInputBlobModifier> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ConstInputBlobModifier>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::InputBlobModifier>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::InputBlobModifier> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ::oneflow::cfg::InputBlobModifier>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};

// inheritance is helpful for avoid container iterator boilerplate 
class Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ : public ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OutputBlobModifier> {
 public:
  Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OutputBlobModifier>>& data);
  Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_();
  ~Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_();

  bool operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const;
  // used by pybind11 only
  const ::oneflow::cfg::OutputBlobModifier& Get(const ::std::string& key) const;

  // used by pybind11 only
  ::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> __SharedConst__() const;
  // used by pybind11 only
  ::std::shared_ptr<ConstOutputBlobModifier> __SharedConst__(const ::std::string& key) const;
  // used by pybind11 only
  using shared_const_iterator = ::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ConstOutputBlobModifier>;
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_begin();
  // ensuring mapped data's lifetime safety
  shared_const_iterator shared_const_end();
};
class _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ final : public Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ {
 public:
  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OutputBlobModifier>>& data);
  _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_();
  ~_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_();
  void CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other);
  void CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other);
  bool operator==(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const;
  // used by pybind11 only
  ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> __SharedMutable__();

  ::std::shared_ptr<::oneflow::cfg::OutputBlobModifier> __SharedMutable__(const ::std::string& key);
  // used by pybind11 only
  using shared_mut_iterator = ::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ::oneflow::cfg::OutputBlobModifier>;
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_begin();
  // ensuring mapped data's lifetime safety
  shared_mut_iterator shared_mut_end();
};



} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstInputBlobModifier> {
  std::size_t operator()(const ::oneflow::cfg::ConstInputBlobModifier& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InputBlobModifier> {
  std::size_t operator()(const ::oneflow::cfg::InputBlobModifier& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstOutputBlobModifier> {
  std::size_t operator()(const ::oneflow::cfg::ConstOutputBlobModifier& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::OutputBlobModifier> {
  std::size_t operator()(const ::oneflow::cfg::OutputBlobModifier& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ConstArgModifierSignature> {
  std::size_t operator()(const ::oneflow::cfg::ConstArgModifierSignature& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::ArgModifierSignature> {
  std::size_t operator()(const ::oneflow::cfg::ArgModifierSignature& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H_