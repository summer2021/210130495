#include "oneflow/core/operator/interface_blob_conf.cfg.h"
#include "oneflow/core/common/shape.cfg.h"
#include "oneflow/core/common/data_type.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/operator/interface_blob_conf.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstInterfaceBlobConf::_InterfaceBlobConf_::_InterfaceBlobConf_() { Clear(); }
ConstInterfaceBlobConf::_InterfaceBlobConf_::_InterfaceBlobConf_(const _InterfaceBlobConf_& other) { CopyFrom(other); }
ConstInterfaceBlobConf::_InterfaceBlobConf_::_InterfaceBlobConf_(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf) {
  InitFromProto(proto_interfaceblobconf);
}
ConstInterfaceBlobConf::_InterfaceBlobConf_::_InterfaceBlobConf_(_InterfaceBlobConf_&& other) = default;
ConstInterfaceBlobConf::_InterfaceBlobConf_::~_InterfaceBlobConf_() = default;

void ConstInterfaceBlobConf::_InterfaceBlobConf_::InitFromProto(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf) {
  Clear();
  // required_or_optional field: shape
  if (proto_interfaceblobconf.has_shape()) {
  *mutable_shape() = ::oneflow::cfg::ShapeProto(proto_interfaceblobconf.shape());      
  }
  // required_or_optional field: data_type
  if (proto_interfaceblobconf.has_data_type()) {
  set_data_type(static_cast<std::remove_reference<std::remove_const<decltype(data_type())>::type>::type>(proto_interfaceblobconf.data_type()));
  }
  // required_or_optional field: is_dynamic
  if (proto_interfaceblobconf.has_is_dynamic()) {
    set_is_dynamic(proto_interfaceblobconf.is_dynamic());
  }
  // required_or_optional field: parallel_distribution
  if (proto_interfaceblobconf.has_parallel_distribution()) {
  *mutable_parallel_distribution() = ::oneflow::cfg::ParallelDistribution(proto_interfaceblobconf.parallel_distribution());      
  }
    
}

void ConstInterfaceBlobConf::_InterfaceBlobConf_::ToProto(::oneflow::InterfaceBlobConf* proto_interfaceblobconf) const {
  proto_interfaceblobconf->Clear();
  // required_or_optional field: shape
  if (this->has_shape()) {
    ::oneflow::ShapeProto proto_shape;
    shape().ToProto(&proto_shape);
    proto_interfaceblobconf->mutable_shape()->CopyFrom(proto_shape);
    }
  // required_or_optional field: data_type
  if (this->has_data_type()) {
    proto_interfaceblobconf->set_data_type(static_cast<std::remove_const<std::remove_reference<decltype(proto_interfaceblobconf->data_type())>::type>::type>(data_type()));
    }
  // required_or_optional field: is_dynamic
  if (this->has_is_dynamic()) {
    proto_interfaceblobconf->set_is_dynamic(is_dynamic());
    }
  // required_or_optional field: parallel_distribution
  if (this->has_parallel_distribution()) {
    ::oneflow::ParallelDistribution proto_parallel_distribution;
    parallel_distribution().ToProto(&proto_parallel_distribution);
    proto_interfaceblobconf->mutable_parallel_distribution()->CopyFrom(proto_parallel_distribution);
    }

}

::std::string ConstInterfaceBlobConf::_InterfaceBlobConf_::DebugString() const {
  ::oneflow::InterfaceBlobConf proto_interfaceblobconf;
  this->ToProto(&proto_interfaceblobconf);
  return proto_interfaceblobconf.DebugString();
}

void ConstInterfaceBlobConf::_InterfaceBlobConf_::Clear() {
  clear_shape();
  clear_data_type();
  clear_is_dynamic();
  clear_parallel_distribution();
}

void ConstInterfaceBlobConf::_InterfaceBlobConf_::CopyFrom(const _InterfaceBlobConf_& other) {
  if (other.has_shape()) {
    mutable_shape()->CopyFrom(other.shape());
  } else {
    clear_shape();
  }
  if (other.has_data_type()) {
    set_data_type(other.data_type());
  } else {
    clear_data_type();
  }
  if (other.has_is_dynamic()) {
    set_is_dynamic(other.is_dynamic());
  } else {
    clear_is_dynamic();
  }
  if (other.has_parallel_distribution()) {
    mutable_parallel_distribution()->CopyFrom(other.parallel_distribution());
  } else {
    clear_parallel_distribution();
  }
}


// optional field shape
bool ConstInterfaceBlobConf::_InterfaceBlobConf_::has_shape() const {
  return has_shape_;
}
const ::oneflow::cfg::ShapeProto& ConstInterfaceBlobConf::_InterfaceBlobConf_::shape() const {
  if (!shape_) {
    static const ::std::shared_ptr<::oneflow::cfg::ShapeProto> default_static_value =
      ::std::make_shared<::oneflow::cfg::ShapeProto>();
    return *default_static_value;
  }
  return *(shape_.get());
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::clear_shape() {
  if (shape_) {
    shape_->Clear();
  }
  has_shape_ = false;
}
::oneflow::cfg::ShapeProto* ConstInterfaceBlobConf::_InterfaceBlobConf_::mutable_shape() {
  if (!shape_) {
    shape_ = ::std::make_shared<::oneflow::cfg::ShapeProto>();
  }
  has_shape_ = true;
  return shape_.get();
}

// optional field data_type
bool ConstInterfaceBlobConf::_InterfaceBlobConf_::has_data_type() const {
  return has_data_type_;
}
const ::oneflow::cfg::DataType& ConstInterfaceBlobConf::_InterfaceBlobConf_::data_type() const {
  if (has_data_type_) { return data_type_; }
  static const ::oneflow::cfg::DataType default_static_value = ::oneflow::cfg::DataType();
  return default_static_value;
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::clear_data_type() {
  has_data_type_ = false;
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::set_data_type(const ::oneflow::cfg::DataType& value) {
  data_type_ = value;
  has_data_type_ = true;
}
::oneflow::cfg::DataType* ConstInterfaceBlobConf::_InterfaceBlobConf_::mutable_data_type() {
  has_data_type_ = true;
  return &data_type_;
}

// optional field is_dynamic
bool ConstInterfaceBlobConf::_InterfaceBlobConf_::has_is_dynamic() const {
  return has_is_dynamic_;
}
const bool& ConstInterfaceBlobConf::_InterfaceBlobConf_::is_dynamic() const {
  if (has_is_dynamic_) { return is_dynamic_; }
  static const bool default_static_value = bool();
  return default_static_value;
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::clear_is_dynamic() {
  has_is_dynamic_ = false;
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::set_is_dynamic(const bool& value) {
  is_dynamic_ = value;
  has_is_dynamic_ = true;
}
bool* ConstInterfaceBlobConf::_InterfaceBlobConf_::mutable_is_dynamic() {
  has_is_dynamic_ = true;
  return &is_dynamic_;
}

// optional field parallel_distribution
bool ConstInterfaceBlobConf::_InterfaceBlobConf_::has_parallel_distribution() const {
  return has_parallel_distribution_;
}
const ::oneflow::cfg::ParallelDistribution& ConstInterfaceBlobConf::_InterfaceBlobConf_::parallel_distribution() const {
  if (!parallel_distribution_) {
    static const ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> default_static_value =
      ::std::make_shared<::oneflow::cfg::ParallelDistribution>();
    return *default_static_value;
  }
  return *(parallel_distribution_.get());
}
void ConstInterfaceBlobConf::_InterfaceBlobConf_::clear_parallel_distribution() {
  if (parallel_distribution_) {
    parallel_distribution_->Clear();
  }
  has_parallel_distribution_ = false;
}
::oneflow::cfg::ParallelDistribution* ConstInterfaceBlobConf::_InterfaceBlobConf_::mutable_parallel_distribution() {
  if (!parallel_distribution_) {
    parallel_distribution_ = ::std::make_shared<::oneflow::cfg::ParallelDistribution>();
  }
  has_parallel_distribution_ = true;
  return parallel_distribution_.get();
}


int ConstInterfaceBlobConf::_InterfaceBlobConf_::compare(const _InterfaceBlobConf_& other) {
  if (!(has_shape() == other.has_shape())) {
    return has_shape() < other.has_shape() ? -1 : 1;
  } else if (!(shape() == other.shape())) {
    return shape() < other.shape() ? -1 : 1;
  }
  if (!(has_data_type() == other.has_data_type())) {
    return has_data_type() < other.has_data_type() ? -1 : 1;
  } else if (!(data_type() == other.data_type())) {
    return data_type() < other.data_type() ? -1 : 1;
  }
  if (!(has_is_dynamic() == other.has_is_dynamic())) {
    return has_is_dynamic() < other.has_is_dynamic() ? -1 : 1;
  } else if (!(is_dynamic() == other.is_dynamic())) {
    return is_dynamic() < other.is_dynamic() ? -1 : 1;
  }
  if (!(has_parallel_distribution() == other.has_parallel_distribution())) {
    return has_parallel_distribution() < other.has_parallel_distribution() ? -1 : 1;
  } else if (!(parallel_distribution() == other.parallel_distribution())) {
    return parallel_distribution() < other.parallel_distribution() ? -1 : 1;
  }
  return 0;
}

bool ConstInterfaceBlobConf::_InterfaceBlobConf_::operator==(const _InterfaceBlobConf_& other) const {
  return true
    && has_shape() == other.has_shape() 
    && shape() == other.shape()
    && has_data_type() == other.has_data_type() 
    && data_type() == other.data_type()
    && has_is_dynamic() == other.has_is_dynamic() 
    && is_dynamic() == other.is_dynamic()
    && has_parallel_distribution() == other.has_parallel_distribution() 
    && parallel_distribution() == other.parallel_distribution()
  ;
}

std::size_t ConstInterfaceBlobConf::_InterfaceBlobConf_::__CalcHash__() const {
  return 0
    ^ (has_shape() ? std::hash<::oneflow::cfg::ShapeProto>()(shape()) : 0)
    ^ (has_data_type() ? std::hash<::oneflow::cfg::DataType>()(data_type()) : 0)
    ^ (has_is_dynamic() ? std::hash<bool>()(is_dynamic()) : 0)
    ^ (has_parallel_distribution() ? std::hash<::oneflow::cfg::ParallelDistribution>()(parallel_distribution()) : 0)
  ;
}

bool ConstInterfaceBlobConf::_InterfaceBlobConf_::operator<(const _InterfaceBlobConf_& other) const {
  return false
    || !(has_shape() == other.has_shape()) ? 
      has_shape() < other.has_shape() : false
    || !(shape() == other.shape()) ? 
      shape() < other.shape() : false
    || !(has_data_type() == other.has_data_type()) ? 
      has_data_type() < other.has_data_type() : false
    || !(data_type() == other.data_type()) ? 
      data_type() < other.data_type() : false
    || !(has_is_dynamic() == other.has_is_dynamic()) ? 
      has_is_dynamic() < other.has_is_dynamic() : false
    || !(is_dynamic() == other.is_dynamic()) ? 
      is_dynamic() < other.is_dynamic() : false
    || !(has_parallel_distribution() == other.has_parallel_distribution()) ? 
      has_parallel_distribution() < other.has_parallel_distribution() : false
    || !(parallel_distribution() == other.parallel_distribution()) ? 
      parallel_distribution() < other.parallel_distribution() : false
  ;
}

using _InterfaceBlobConf_ =  ConstInterfaceBlobConf::_InterfaceBlobConf_;
ConstInterfaceBlobConf::ConstInterfaceBlobConf(const ::std::shared_ptr<_InterfaceBlobConf_>& data): data_(data) {}
ConstInterfaceBlobConf::ConstInterfaceBlobConf(): data_(::std::make_shared<_InterfaceBlobConf_>()) {}
ConstInterfaceBlobConf::ConstInterfaceBlobConf(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf) {
  BuildFromProto(proto_interfaceblobconf);
}
ConstInterfaceBlobConf::ConstInterfaceBlobConf(const ConstInterfaceBlobConf&) = default;
ConstInterfaceBlobConf::ConstInterfaceBlobConf(ConstInterfaceBlobConf&&) noexcept = default;
ConstInterfaceBlobConf::~ConstInterfaceBlobConf() = default;

void ConstInterfaceBlobConf::ToProto(PbMessage* proto_interfaceblobconf) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InterfaceBlobConf*>(proto_interfaceblobconf));
}
  
::std::string ConstInterfaceBlobConf::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInterfaceBlobConf::__Empty__() const {
  return !data_;
}

int ConstInterfaceBlobConf::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"shape", 1},
    {"data_type", 2},
    {"is_dynamic", 3},
    {"parallel_distribution", 4},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInterfaceBlobConf::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInterfaceBlobConf::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ShapeProto),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstShapeProto),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::DataType),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 4: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::ParallelDistribution),
        typeid(::oneflow::cfg::Message),
        typeid(::oneflow::cfg::ConstParallelDistribution),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInterfaceBlobConf::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &shape();
    case 2: return &data_type();
    case 3: return &is_dynamic();
    case 4: return &parallel_distribution();
    default: return nullptr;
  }
}

// required or optional field shape
bool ConstInterfaceBlobConf::has_shape() const {
  return __SharedPtrOrDefault__()->has_shape();
}
const ::oneflow::cfg::ShapeProto& ConstInterfaceBlobConf::shape() const {
  return __SharedPtrOrDefault__()->shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstShapeProto> ConstInterfaceBlobConf::shared_const_shape() const {
  return shape().__SharedConst__();
}
// required or optional field data_type
bool ConstInterfaceBlobConf::has_data_type() const {
  return __SharedPtrOrDefault__()->has_data_type();
}
const ::oneflow::cfg::DataType& ConstInterfaceBlobConf::data_type() const {
  return __SharedPtrOrDefault__()->data_type();
}
// used by pybind11 only
// required or optional field is_dynamic
bool ConstInterfaceBlobConf::has_is_dynamic() const {
  return __SharedPtrOrDefault__()->has_is_dynamic();
}
const bool& ConstInterfaceBlobConf::is_dynamic() const {
  return __SharedPtrOrDefault__()->is_dynamic();
}
// used by pybind11 only
// required or optional field parallel_distribution
bool ConstInterfaceBlobConf::has_parallel_distribution() const {
  return __SharedPtrOrDefault__()->has_parallel_distribution();
}
const ::oneflow::cfg::ParallelDistribution& ConstInterfaceBlobConf::parallel_distribution() const {
  return __SharedPtrOrDefault__()->parallel_distribution();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ConstParallelDistribution> ConstInterfaceBlobConf::shared_const_parallel_distribution() const {
  return parallel_distribution().__SharedConst__();
}

::std::shared_ptr<ConstInterfaceBlobConf> ConstInterfaceBlobConf::__SharedConst__() const {
  return ::std::make_shared<ConstInterfaceBlobConf>(data_);
}
int64_t ConstInterfaceBlobConf::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInterfaceBlobConf::operator==(const ConstInterfaceBlobConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInterfaceBlobConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInterfaceBlobConf::operator<(const ConstInterfaceBlobConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InterfaceBlobConf_>& ConstInterfaceBlobConf::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InterfaceBlobConf_> default_ptr = std::make_shared<_InterfaceBlobConf_>();
  return default_ptr;
}
const ::std::shared_ptr<_InterfaceBlobConf_>& ConstInterfaceBlobConf::__SharedPtr__() {
  if (!data_) { data_.reset(new _InterfaceBlobConf_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInterfaceBlobConf
void ConstInterfaceBlobConf::BuildFromProto(const PbMessage& proto_interfaceblobconf) {
  data_ = ::std::make_shared<_InterfaceBlobConf_>(dynamic_cast<const ::oneflow::InterfaceBlobConf&>(proto_interfaceblobconf));
}

InterfaceBlobConf::InterfaceBlobConf(const ::std::shared_ptr<_InterfaceBlobConf_>& data)
  : ConstInterfaceBlobConf(data) {}
InterfaceBlobConf::InterfaceBlobConf(const InterfaceBlobConf& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InterfaceBlobConf> resize
InterfaceBlobConf::InterfaceBlobConf(InterfaceBlobConf&&) noexcept = default; 
InterfaceBlobConf::InterfaceBlobConf(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf) {
  InitFromProto(proto_interfaceblobconf);
}
InterfaceBlobConf::InterfaceBlobConf() = default;

InterfaceBlobConf::~InterfaceBlobConf() = default;

void InterfaceBlobConf::InitFromProto(const PbMessage& proto_interfaceblobconf) {
  BuildFromProto(proto_interfaceblobconf);
}
  
void* InterfaceBlobConf::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_shape();
    case 2: return mutable_data_type();
    case 3: return mutable_is_dynamic();
    case 4: return mutable_parallel_distribution();
    default: return nullptr;
  }
}

bool InterfaceBlobConf::operator==(const InterfaceBlobConf& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InterfaceBlobConf::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InterfaceBlobConf::operator<(const InterfaceBlobConf& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InterfaceBlobConf::Clear() {
  if (data_) { data_.reset(); }
}
void InterfaceBlobConf::CopyFrom(const InterfaceBlobConf& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InterfaceBlobConf& InterfaceBlobConf::operator=(const InterfaceBlobConf& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field shape
void InterfaceBlobConf::clear_shape() {
  return __SharedPtr__()->clear_shape();
}
::oneflow::cfg::ShapeProto* InterfaceBlobConf::mutable_shape() {
  return __SharedPtr__()->mutable_shape();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ShapeProto> InterfaceBlobConf::shared_mutable_shape() {
  return mutable_shape()->__SharedMutable__();
}
// required or optional field data_type
void InterfaceBlobConf::clear_data_type() {
  return __SharedPtr__()->clear_data_type();
}
void InterfaceBlobConf::set_data_type(const ::oneflow::cfg::DataType& value) {
  return __SharedPtr__()->set_data_type(value);
}
::oneflow::cfg::DataType* InterfaceBlobConf::mutable_data_type() {
  return  __SharedPtr__()->mutable_data_type();
}
// required or optional field is_dynamic
void InterfaceBlobConf::clear_is_dynamic() {
  return __SharedPtr__()->clear_is_dynamic();
}
void InterfaceBlobConf::set_is_dynamic(const bool& value) {
  return __SharedPtr__()->set_is_dynamic(value);
}
bool* InterfaceBlobConf::mutable_is_dynamic() {
  return  __SharedPtr__()->mutable_is_dynamic();
}
// required or optional field parallel_distribution
void InterfaceBlobConf::clear_parallel_distribution() {
  return __SharedPtr__()->clear_parallel_distribution();
}
::oneflow::cfg::ParallelDistribution* InterfaceBlobConf::mutable_parallel_distribution() {
  return __SharedPtr__()->mutable_parallel_distribution();
}
// used by pybind11 only
::std::shared_ptr<::oneflow::cfg::ParallelDistribution> InterfaceBlobConf::shared_mutable_parallel_distribution() {
  return mutable_parallel_distribution()->__SharedMutable__();
}

::std::shared_ptr<InterfaceBlobConf> InterfaceBlobConf::__SharedMutable__() {
  return ::std::make_shared<InterfaceBlobConf>(__SharedPtr__());
}


}
} // namespace oneflow
