// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oneflow/core/operator/op_node_signature.proto

#include "oneflow/core/operator/op_node_signature.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
extern PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fregister_2fblob_5fdesc_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_BlobDescSignature_oneflow_2fcore_2fregister_2fblob_5fdesc_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fjob_2fmirrored_5fparallel_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_MirroredSignature_oneflow_2fcore_2fjob_2fmirrored_5fparallel_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fjob_2fparallel_5fsignature_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_ParallelSignature_oneflow_2fcore_2fjob_2fparallel_5fsignature_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_oneflow_2fcore_2fjob_2fsbp_5fparallel_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_SbpSignature_oneflow_2fcore_2fjob_2fsbp_5fparallel_2eproto;
namespace oneflow {
class OpNodeSignatureDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<OpNodeSignature> _instance;
} _OpNodeSignature_default_instance_;
}  // namespace oneflow
static void InitDefaultsscc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::oneflow::_OpNodeSignature_default_instance_;
    new (ptr) ::oneflow::OpNodeSignature();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::oneflow::OpNodeSignature::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<4> scc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 4, InitDefaultsscc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto}, {
      &scc_info_SbpSignature_oneflow_2fcore_2fjob_2fsbp_5fparallel_2eproto.base,
      &scc_info_MirroredSignature_oneflow_2fcore_2fjob_2fmirrored_5fparallel_2eproto.base,
      &scc_info_BlobDescSignature_oneflow_2fcore_2fregister_2fblob_5fdesc_2eproto.base,
      &scc_info_ParallelSignature_oneflow_2fcore_2fjob_2fparallel_5fsignature_2eproto.base,}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto[1];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, sbp_signature_),
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, mirrored_signature_),
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, logical_blob_desc_signature_),
  PROTOBUF_FIELD_OFFSET(::oneflow::OpNodeSignature, parallel_signature_),
  0,
  1,
  2,
  3,
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 9, sizeof(::oneflow::OpNodeSignature)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::oneflow::_OpNodeSignature_default_instance_),
};

const char descriptor_table_protodef_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n-oneflow/core/operator/op_node_signatur"
  "e.proto\022\007oneflow\032#oneflow/core/job/sbp_p"
  "arallel.proto\032(oneflow/core/job/mirrored"
  "_parallel.proto\032%oneflow/core/register/b"
  "lob_desc.proto\032)oneflow/core/job/paralle"
  "l_signature.proto\"\360\001\n\017OpNodeSignature\022,\n"
  "\rsbp_signature\030\001 \001(\0132\025.oneflow.SbpSignat"
  "ure\0226\n\022mirrored_signature\030\002 \001(\0132\032.oneflo"
  "w.MirroredSignature\022\?\n\033logical_blob_desc"
  "_signature\030\003 \001(\0132\032.oneflow.BlobDescSigna"
  "ture\0226\n\022parallel_signature\030\005 \001(\0132\032.onefl"
  "ow.ParallelSignature"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_deps[4] = {
  &::descriptor_table_oneflow_2fcore_2fjob_2fmirrored_5fparallel_2eproto,
  &::descriptor_table_oneflow_2fcore_2fjob_2fparallel_5fsignature_2eproto,
  &::descriptor_table_oneflow_2fcore_2fjob_2fsbp_5fparallel_2eproto,
  &::descriptor_table_oneflow_2fcore_2fregister_2fblob_5fdesc_2eproto,
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_sccs[1] = {
  &scc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_once;
static bool descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto = {
  &descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_initialized, descriptor_table_protodef_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto, "oneflow/core/operator/op_node_signature.proto", 460,
  &descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_once, descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_sccs, descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto_deps, 1, 4,
  schemas, file_default_instances, TableStruct_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto::offsets,
  file_level_metadata_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto, 1, file_level_enum_descriptors_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto, file_level_service_descriptors_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto), true);
namespace oneflow {

// ===================================================================

void OpNodeSignature::InitAsDefaultInstance() {
  ::oneflow::_OpNodeSignature_default_instance_._instance.get_mutable()->sbp_signature_ = const_cast< ::oneflow::SbpSignature*>(
      ::oneflow::SbpSignature::internal_default_instance());
  ::oneflow::_OpNodeSignature_default_instance_._instance.get_mutable()->mirrored_signature_ = const_cast< ::oneflow::MirroredSignature*>(
      ::oneflow::MirroredSignature::internal_default_instance());
  ::oneflow::_OpNodeSignature_default_instance_._instance.get_mutable()->logical_blob_desc_signature_ = const_cast< ::oneflow::BlobDescSignature*>(
      ::oneflow::BlobDescSignature::internal_default_instance());
  ::oneflow::_OpNodeSignature_default_instance_._instance.get_mutable()->parallel_signature_ = const_cast< ::oneflow::ParallelSignature*>(
      ::oneflow::ParallelSignature::internal_default_instance());
}
class OpNodeSignature::_Internal {
 public:
  using HasBits = decltype(std::declval<OpNodeSignature>()._has_bits_);
  static const ::oneflow::SbpSignature& sbp_signature(const OpNodeSignature* msg);
  static void set_has_sbp_signature(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
  static const ::oneflow::MirroredSignature& mirrored_signature(const OpNodeSignature* msg);
  static void set_has_mirrored_signature(HasBits* has_bits) {
    (*has_bits)[0] |= 2u;
  }
  static const ::oneflow::BlobDescSignature& logical_blob_desc_signature(const OpNodeSignature* msg);
  static void set_has_logical_blob_desc_signature(HasBits* has_bits) {
    (*has_bits)[0] |= 4u;
  }
  static const ::oneflow::ParallelSignature& parallel_signature(const OpNodeSignature* msg);
  static void set_has_parallel_signature(HasBits* has_bits) {
    (*has_bits)[0] |= 8u;
  }
};

const ::oneflow::SbpSignature&
OpNodeSignature::_Internal::sbp_signature(const OpNodeSignature* msg) {
  return *msg->sbp_signature_;
}
const ::oneflow::MirroredSignature&
OpNodeSignature::_Internal::mirrored_signature(const OpNodeSignature* msg) {
  return *msg->mirrored_signature_;
}
const ::oneflow::BlobDescSignature&
OpNodeSignature::_Internal::logical_blob_desc_signature(const OpNodeSignature* msg) {
  return *msg->logical_blob_desc_signature_;
}
const ::oneflow::ParallelSignature&
OpNodeSignature::_Internal::parallel_signature(const OpNodeSignature* msg) {
  return *msg->parallel_signature_;
}
void OpNodeSignature::clear_sbp_signature() {
  if (sbp_signature_ != nullptr) sbp_signature_->Clear();
  _has_bits_[0] &= ~0x00000001u;
}
void OpNodeSignature::clear_mirrored_signature() {
  if (mirrored_signature_ != nullptr) mirrored_signature_->Clear();
  _has_bits_[0] &= ~0x00000002u;
}
void OpNodeSignature::clear_logical_blob_desc_signature() {
  if (logical_blob_desc_signature_ != nullptr) logical_blob_desc_signature_->Clear();
  _has_bits_[0] &= ~0x00000004u;
}
void OpNodeSignature::clear_parallel_signature() {
  if (parallel_signature_ != nullptr) parallel_signature_->Clear();
  _has_bits_[0] &= ~0x00000008u;
}
OpNodeSignature::OpNodeSignature()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:oneflow.OpNodeSignature)
}
OpNodeSignature::OpNodeSignature(const OpNodeSignature& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  if (from.has_sbp_signature()) {
    sbp_signature_ = new ::oneflow::SbpSignature(*from.sbp_signature_);
  } else {
    sbp_signature_ = nullptr;
  }
  if (from.has_mirrored_signature()) {
    mirrored_signature_ = new ::oneflow::MirroredSignature(*from.mirrored_signature_);
  } else {
    mirrored_signature_ = nullptr;
  }
  if (from.has_logical_blob_desc_signature()) {
    logical_blob_desc_signature_ = new ::oneflow::BlobDescSignature(*from.logical_blob_desc_signature_);
  } else {
    logical_blob_desc_signature_ = nullptr;
  }
  if (from.has_parallel_signature()) {
    parallel_signature_ = new ::oneflow::ParallelSignature(*from.parallel_signature_);
  } else {
    parallel_signature_ = nullptr;
  }
  // @@protoc_insertion_point(copy_constructor:oneflow.OpNodeSignature)
}

void OpNodeSignature::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto.base);
  ::memset(&sbp_signature_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&parallel_signature_) -
      reinterpret_cast<char*>(&sbp_signature_)) + sizeof(parallel_signature_));
}

OpNodeSignature::~OpNodeSignature() {
  // @@protoc_insertion_point(destructor:oneflow.OpNodeSignature)
  SharedDtor();
}

void OpNodeSignature::SharedDtor() {
  if (this != internal_default_instance()) delete sbp_signature_;
  if (this != internal_default_instance()) delete mirrored_signature_;
  if (this != internal_default_instance()) delete logical_blob_desc_signature_;
  if (this != internal_default_instance()) delete parallel_signature_;
}

void OpNodeSignature::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const OpNodeSignature& OpNodeSignature::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_OpNodeSignature_oneflow_2fcore_2foperator_2fop_5fnode_5fsignature_2eproto.base);
  return *internal_default_instance();
}


void OpNodeSignature::Clear() {
// @@protoc_insertion_point(message_clear_start:oneflow.OpNodeSignature)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    if (cached_has_bits & 0x00000001u) {
      GOOGLE_DCHECK(sbp_signature_ != nullptr);
      sbp_signature_->Clear();
    }
    if (cached_has_bits & 0x00000002u) {
      GOOGLE_DCHECK(mirrored_signature_ != nullptr);
      mirrored_signature_->Clear();
    }
    if (cached_has_bits & 0x00000004u) {
      GOOGLE_DCHECK(logical_blob_desc_signature_ != nullptr);
      logical_blob_desc_signature_->Clear();
    }
    if (cached_has_bits & 0x00000008u) {
      GOOGLE_DCHECK(parallel_signature_ != nullptr);
      parallel_signature_->Clear();
    }
  }
  _has_bits_.Clear();
  _internal_metadata_.Clear();
}

#if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
const char* OpNodeSignature::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // optional .oneflow.SbpSignature sbp_signature = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          ptr = ctx->ParseMessage(mutable_sbp_signature(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional .oneflow.MirroredSignature mirrored_signature = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          ptr = ctx->ParseMessage(mutable_mirrored_signature(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional .oneflow.BlobDescSignature logical_blob_desc_signature = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 26)) {
          ptr = ctx->ParseMessage(mutable_logical_blob_desc_signature(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // optional .oneflow.ParallelSignature parallel_signature = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 42)) {
          ptr = ctx->ParseMessage(mutable_parallel_signature(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}
#else  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
bool OpNodeSignature::MergePartialFromCodedStream(
    ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!PROTOBUF_PREDICT_TRUE(EXPRESSION)) goto failure
  ::PROTOBUF_NAMESPACE_ID::uint32 tag;
  // @@protoc_insertion_point(parse_start:oneflow.OpNodeSignature)
  for (;;) {
    ::std::pair<::PROTOBUF_NAMESPACE_ID::uint32, bool> p = input->ReadTagWithCutoffNoLastTag(127u);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional .oneflow.SbpSignature sbp_signature = 1;
      case 1: {
        if (static_cast< ::PROTOBUF_NAMESPACE_ID::uint8>(tag) == (10 & 0xFF)) {
          DO_(::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::ReadMessage(
               input, mutable_sbp_signature()));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional .oneflow.MirroredSignature mirrored_signature = 2;
      case 2: {
        if (static_cast< ::PROTOBUF_NAMESPACE_ID::uint8>(tag) == (18 & 0xFF)) {
          DO_(::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::ReadMessage(
               input, mutable_mirrored_signature()));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional .oneflow.BlobDescSignature logical_blob_desc_signature = 3;
      case 3: {
        if (static_cast< ::PROTOBUF_NAMESPACE_ID::uint8>(tag) == (26 & 0xFF)) {
          DO_(::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::ReadMessage(
               input, mutable_logical_blob_desc_signature()));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // optional .oneflow.ParallelSignature parallel_signature = 5;
      case 5: {
        if (static_cast< ::PROTOBUF_NAMESPACE_ID::uint8>(tag) == (42 & 0xFF)) {
          DO_(::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::ReadMessage(
               input, mutable_parallel_signature()));
        } else {
          goto handle_unusual;
        }
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0) {
          goto success;
        }
        DO_(::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SkipField(
              input, tag, _internal_metadata_.mutable_unknown_fields()));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:oneflow.OpNodeSignature)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:oneflow.OpNodeSignature)
  return false;
#undef DO_
}
#endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER

void OpNodeSignature::SerializeWithCachedSizes(
    ::PROTOBUF_NAMESPACE_ID::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:oneflow.OpNodeSignature)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional .oneflow.SbpSignature sbp_signature = 1;
  if (cached_has_bits & 0x00000001u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteMessageMaybeToArray(
      1, _Internal::sbp_signature(this), output);
  }

  // optional .oneflow.MirroredSignature mirrored_signature = 2;
  if (cached_has_bits & 0x00000002u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteMessageMaybeToArray(
      2, _Internal::mirrored_signature(this), output);
  }

  // optional .oneflow.BlobDescSignature logical_blob_desc_signature = 3;
  if (cached_has_bits & 0x00000004u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteMessageMaybeToArray(
      3, _Internal::logical_blob_desc_signature(this), output);
  }

  // optional .oneflow.ParallelSignature parallel_signature = 5;
  if (cached_has_bits & 0x00000008u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteMessageMaybeToArray(
      5, _Internal::parallel_signature(this), output);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SerializeUnknownFields(
        _internal_metadata_.unknown_fields(), output);
  }
  // @@protoc_insertion_point(serialize_end:oneflow.OpNodeSignature)
}

::PROTOBUF_NAMESPACE_ID::uint8* OpNodeSignature::InternalSerializeWithCachedSizesToArray(
    ::PROTOBUF_NAMESPACE_ID::uint8* target) const {
  // @@protoc_insertion_point(serialize_to_array_start:oneflow.OpNodeSignature)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional .oneflow.SbpSignature sbp_signature = 1;
  if (cached_has_bits & 0x00000001u) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessageToArray(
        1, _Internal::sbp_signature(this), target);
  }

  // optional .oneflow.MirroredSignature mirrored_signature = 2;
  if (cached_has_bits & 0x00000002u) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessageToArray(
        2, _Internal::mirrored_signature(this), target);
  }

  // optional .oneflow.BlobDescSignature logical_blob_desc_signature = 3;
  if (cached_has_bits & 0x00000004u) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessageToArray(
        3, _Internal::logical_blob_desc_signature(this), target);
  }

  // optional .oneflow.ParallelSignature parallel_signature = 5;
  if (cached_has_bits & 0x00000008u) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessageToArray(
        5, _Internal::parallel_signature(this), target);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:oneflow.OpNodeSignature)
  return target;
}

size_t OpNodeSignature::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:oneflow.OpNodeSignature)
  size_t total_size = 0;

  if (_internal_metadata_.have_unknown_fields()) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::ComputeUnknownFieldsSize(
        _internal_metadata_.unknown_fields());
  }
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    // optional .oneflow.SbpSignature sbp_signature = 1;
    if (cached_has_bits & 0x00000001u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *sbp_signature_);
    }

    // optional .oneflow.MirroredSignature mirrored_signature = 2;
    if (cached_has_bits & 0x00000002u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *mirrored_signature_);
    }

    // optional .oneflow.BlobDescSignature logical_blob_desc_signature = 3;
    if (cached_has_bits & 0x00000004u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *logical_blob_desc_signature_);
    }

    // optional .oneflow.ParallelSignature parallel_signature = 5;
    if (cached_has_bits & 0x00000008u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
          *parallel_signature_);
    }

  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void OpNodeSignature::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:oneflow.OpNodeSignature)
  GOOGLE_DCHECK_NE(&from, this);
  const OpNodeSignature* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<OpNodeSignature>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:oneflow.OpNodeSignature)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:oneflow.OpNodeSignature)
    MergeFrom(*source);
  }
}

void OpNodeSignature::MergeFrom(const OpNodeSignature& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:oneflow.OpNodeSignature)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    if (cached_has_bits & 0x00000001u) {
      mutable_sbp_signature()->::oneflow::SbpSignature::MergeFrom(from.sbp_signature());
    }
    if (cached_has_bits & 0x00000002u) {
      mutable_mirrored_signature()->::oneflow::MirroredSignature::MergeFrom(from.mirrored_signature());
    }
    if (cached_has_bits & 0x00000004u) {
      mutable_logical_blob_desc_signature()->::oneflow::BlobDescSignature::MergeFrom(from.logical_blob_desc_signature());
    }
    if (cached_has_bits & 0x00000008u) {
      mutable_parallel_signature()->::oneflow::ParallelSignature::MergeFrom(from.parallel_signature());
    }
  }
}

void OpNodeSignature::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:oneflow.OpNodeSignature)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void OpNodeSignature::CopyFrom(const OpNodeSignature& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:oneflow.OpNodeSignature)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool OpNodeSignature::IsInitialized() const {
  if (has_sbp_signature()) {
    if (!this->sbp_signature_->IsInitialized()) return false;
  }
  if (has_logical_blob_desc_signature()) {
    if (!this->logical_blob_desc_signature_->IsInitialized()) return false;
  }
  return true;
}

void OpNodeSignature::InternalSwap(OpNodeSignature* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  swap(sbp_signature_, other->sbp_signature_);
  swap(mirrored_signature_, other->mirrored_signature_);
  swap(logical_blob_desc_signature_, other->logical_blob_desc_signature_);
  swap(parallel_signature_, other->parallel_signature_);
}

::PROTOBUF_NAMESPACE_ID::Metadata OpNodeSignature::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace oneflow
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::oneflow::OpNodeSignature* Arena::CreateMaybeMessage< ::oneflow::OpNodeSignature >(Arena* arena) {
  return Arena::CreateInternal< ::oneflow::OpNodeSignature >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
