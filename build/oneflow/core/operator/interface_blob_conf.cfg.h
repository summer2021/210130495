#ifndef CFG_ONEFLOW_CORE_OPERATOR_INTERFACE_BLOB_CONF_CFG_H_
#define CFG_ONEFLOW_CORE_OPERATOR_INTERFACE_BLOB_CONF_CFG_H_

#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <google/protobuf/message.h>
#include "oneflow/cfg/repeated_field.h"
#include "oneflow/cfg/map_field.h"
#include "oneflow/cfg/message.h"
#include "oneflow/cfg/shared_pair_iterator.h"

// forward declare enum defined in other module
namespace oneflow {
namespace cfg {
enum DataType : unsigned int;
}
}

// forward declare class defined in other module
namespace oneflow {
namespace cfg {
class ConstParallelDistribution;
class ParallelDistribution;
}
}
namespace oneflow {
namespace cfg {
class ConstShapeProto;
class ShapeProto;
}
}

namespace oneflow {

// forward declare proto class;
class InterfaceBlobConf;

namespace cfg {


class InterfaceBlobConf;
class ConstInterfaceBlobConf;



class ConstInterfaceBlobConf : public ::oneflow::cfg::Message {
 public:

  class _InterfaceBlobConf_ {
   public:
    _InterfaceBlobConf_();
    explicit _InterfaceBlobConf_(const _InterfaceBlobConf_& other);
    explicit _InterfaceBlobConf_(_InterfaceBlobConf_&& other);
    _InterfaceBlobConf_(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf);
    ~_InterfaceBlobConf_();

    void InitFromProto(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf);

    void ToProto(::oneflow::InterfaceBlobConf* proto_interfaceblobconf) const;
    ::std::string DebugString() const;

    void Clear();
    void CopyFrom(const _InterfaceBlobConf_& other);
  
      // optional field shape
     public:
    bool has_shape() const;
    const ::oneflow::cfg::ShapeProto& shape() const;
    void clear_shape();
    ::oneflow::cfg::ShapeProto* mutable_shape();
   protected:
    bool has_shape_ = false;
    ::std::shared_ptr<::oneflow::cfg::ShapeProto> shape_;
      
      // optional field data_type
     public:
    bool has_data_type() const;
    const ::oneflow::cfg::DataType& data_type() const;
    void clear_data_type();
    void set_data_type(const ::oneflow::cfg::DataType& value);
    ::oneflow::cfg::DataType* mutable_data_type();
   protected:
    bool has_data_type_ = false;
    ::oneflow::cfg::DataType data_type_;
      
      // optional field is_dynamic
     public:
    bool has_is_dynamic() const;
    const bool& is_dynamic() const;
    void clear_is_dynamic();
    void set_is_dynamic(const bool& value);
    bool* mutable_is_dynamic();
   protected:
    bool has_is_dynamic_ = false;
    bool is_dynamic_;
      
      // optional field parallel_distribution
     public:
    bool has_parallel_distribution() const;
    const ::oneflow::cfg::ParallelDistribution& parallel_distribution() const;
    void clear_parallel_distribution();
    ::oneflow::cfg::ParallelDistribution* mutable_parallel_distribution();
   protected:
    bool has_parallel_distribution_ = false;
    ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> parallel_distribution_;
           
   public:
    int compare(const _InterfaceBlobConf_& other);

    bool operator==(const _InterfaceBlobConf_& other) const;
    std::size_t __CalcHash__() const;

    bool operator<(const _InterfaceBlobConf_& other) const;
  };

  ConstInterfaceBlobConf(const ::std::shared_ptr<_InterfaceBlobConf_>& data);
  ConstInterfaceBlobConf(const ConstInterfaceBlobConf&);
  ConstInterfaceBlobConf(ConstInterfaceBlobConf&&) noexcept;
  ConstInterfaceBlobConf();
  ConstInterfaceBlobConf(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf);
  virtual ~ConstInterfaceBlobConf() override;

  using PbMessage = ::google::protobuf::Message;
  void ToProto(PbMessage* proto_interfaceblobconf) const override;
  
  ::std::string DebugString() const;

  bool __Empty__() const;

  int FieldNumber4FieldName(const ::std::string& field_name) const override;

  bool FieldDefined4FieldNumber(int field_number) const override;

  const ::std::set<::std::type_index>& ValidTypeIndices4FieldNumber(int field_number) const override;

  const void* FieldPtr4FieldNumber(int field_number) const override;

  // required or optional field shape
 public:
  bool has_shape() const;
  const ::oneflow::cfg::ShapeProto& shape() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstShapeProto> shared_const_shape() const;
  // required or optional field data_type
 public:
  bool has_data_type() const;
  const ::oneflow::cfg::DataType& data_type() const;
  // used by pybind11 only
  // required or optional field is_dynamic
 public:
  bool has_is_dynamic() const;
  const bool& is_dynamic() const;
  // used by pybind11 only
  // required or optional field parallel_distribution
 public:
  bool has_parallel_distribution() const;
  const ::oneflow::cfg::ParallelDistribution& parallel_distribution() const;
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ConstParallelDistribution> shared_const_parallel_distribution() const;

 public:
  ::std::shared_ptr<ConstInterfaceBlobConf> __SharedConst__() const;
  int64_t __Id__() const;
  bool operator==(const ConstInterfaceBlobConf& other) const;
  std::size_t __CalcHash__() const;

  bool operator<(const ConstInterfaceBlobConf& other) const;
 protected:
  const ::std::shared_ptr<_InterfaceBlobConf_>& __SharedPtrOrDefault__() const;
  const ::std::shared_ptr<_InterfaceBlobConf_>& __SharedPtr__();
  // use a protected member method to avoid someone change member variable(data_) by ConstInterfaceBlobConf
  void BuildFromProto(const PbMessage& proto_interfaceblobconf);
  
  ::std::shared_ptr<_InterfaceBlobConf_> data_;
};

class InterfaceBlobConf final : public ConstInterfaceBlobConf {
 public:
  InterfaceBlobConf(const ::std::shared_ptr<_InterfaceBlobConf_>& data);
  InterfaceBlobConf(const InterfaceBlobConf& other);
  // enable nothrow for ::std::vector<InterfaceBlobConf> resize 
  InterfaceBlobConf(InterfaceBlobConf&&) noexcept;
  InterfaceBlobConf();
  explicit InterfaceBlobConf(const ::oneflow::InterfaceBlobConf& proto_interfaceblobconf);

  ~InterfaceBlobConf() override;

  void InitFromProto(const PbMessage& proto_interfaceblobconf) override;
  
  void* MutableFieldPtr4FieldNumber(int field_number) override;


  bool operator==(const InterfaceBlobConf& other) const;
  std::size_t __CalcHash__() const;
  bool operator<(const InterfaceBlobConf& other) const;
  void Clear();
  void CopyFrom(const InterfaceBlobConf& other);
  InterfaceBlobConf& operator=(const InterfaceBlobConf& other);

  // required or optional field shape
 public:
  void clear_shape();
  ::oneflow::cfg::ShapeProto* mutable_shape();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ShapeProto> shared_mutable_shape();
  // required or optional field data_type
 public:
  void clear_data_type();
  void set_data_type(const ::oneflow::cfg::DataType& value);
  ::oneflow::cfg::DataType* mutable_data_type();
  // required or optional field is_dynamic
 public:
  void clear_is_dynamic();
  void set_is_dynamic(const bool& value);
  bool* mutable_is_dynamic();
  // required or optional field parallel_distribution
 public:
  void clear_parallel_distribution();
  ::oneflow::cfg::ParallelDistribution* mutable_parallel_distribution();
  // used by pybind11 only
  ::std::shared_ptr<::oneflow::cfg::ParallelDistribution> shared_mutable_parallel_distribution();

  ::std::shared_ptr<InterfaceBlobConf> __SharedMutable__();
};






} //namespace cfg

} // namespace oneflow

namespace std {



template<>
struct hash<::oneflow::cfg::ConstInterfaceBlobConf> {
  std::size_t operator()(const ::oneflow::cfg::ConstInterfaceBlobConf& s) const {
    return s.__CalcHash__();
  }
};

template<>
struct hash<::oneflow::cfg::InterfaceBlobConf> {
  std::size_t operator()(const ::oneflow::cfg::InterfaceBlobConf& s) const {
    return s.__CalcHash__();
  }
};

}

#endif  // CFG_ONEFLOW_CORE_OPERATOR_INTERFACE_BLOB_CONF_CFG_H_