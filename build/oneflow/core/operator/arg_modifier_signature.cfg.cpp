#include "oneflow/core/operator/arg_modifier_signature.cfg.h"
#include "oneflow/core/operator/arg_modifier_signature.pb.h"

namespace oneflow {
namespace cfg {
using PbMessage = ::google::protobuf::Message;

ConstInputBlobModifier::_InputBlobModifier_::_InputBlobModifier_() { Clear(); }
ConstInputBlobModifier::_InputBlobModifier_::_InputBlobModifier_(const _InputBlobModifier_& other) { CopyFrom(other); }
ConstInputBlobModifier::_InputBlobModifier_::_InputBlobModifier_(const ::oneflow::InputBlobModifier& proto_inputblobmodifier) {
  InitFromProto(proto_inputblobmodifier);
}
ConstInputBlobModifier::_InputBlobModifier_::_InputBlobModifier_(_InputBlobModifier_&& other) = default;
ConstInputBlobModifier::_InputBlobModifier_::~_InputBlobModifier_() = default;

void ConstInputBlobModifier::_InputBlobModifier_::InitFromProto(const ::oneflow::InputBlobModifier& proto_inputblobmodifier) {
  Clear();
  // required_or_optional field: is_mutable
  if (proto_inputblobmodifier.has_is_mutable()) {
    set_is_mutable(proto_inputblobmodifier.is_mutable());
  }
  // required_or_optional field: requires_grad
  if (proto_inputblobmodifier.has_requires_grad()) {
    set_requires_grad(proto_inputblobmodifier.requires_grad());
  }
    
}

void ConstInputBlobModifier::_InputBlobModifier_::ToProto(::oneflow::InputBlobModifier* proto_inputblobmodifier) const {
  proto_inputblobmodifier->Clear();
  // required_or_optional field: is_mutable
  if (this->has_is_mutable()) {
    proto_inputblobmodifier->set_is_mutable(is_mutable());
    }
  // required_or_optional field: requires_grad
  if (this->has_requires_grad()) {
    proto_inputblobmodifier->set_requires_grad(requires_grad());
    }

}

::std::string ConstInputBlobModifier::_InputBlobModifier_::DebugString() const {
  ::oneflow::InputBlobModifier proto_inputblobmodifier;
  this->ToProto(&proto_inputblobmodifier);
  return proto_inputblobmodifier.DebugString();
}

void ConstInputBlobModifier::_InputBlobModifier_::Clear() {
  clear_is_mutable();
  clear_requires_grad();
}

void ConstInputBlobModifier::_InputBlobModifier_::CopyFrom(const _InputBlobModifier_& other) {
  if (other.has_is_mutable()) {
    set_is_mutable(other.is_mutable());
  } else {
    clear_is_mutable();
  }
  if (other.has_requires_grad()) {
    set_requires_grad(other.requires_grad());
  } else {
    clear_requires_grad();
  }
}


// optional field is_mutable
bool ConstInputBlobModifier::_InputBlobModifier_::has_is_mutable() const {
  return has_is_mutable_;
}
const bool& ConstInputBlobModifier::_InputBlobModifier_::is_mutable() const {
  if (has_is_mutable_) { return is_mutable_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstInputBlobModifier::_InputBlobModifier_::clear_is_mutable() {
  has_is_mutable_ = false;
}
void ConstInputBlobModifier::_InputBlobModifier_::set_is_mutable(const bool& value) {
  is_mutable_ = value;
  has_is_mutable_ = true;
}
bool* ConstInputBlobModifier::_InputBlobModifier_::mutable_is_mutable() {
  has_is_mutable_ = true;
  return &is_mutable_;
}

// optional field requires_grad
bool ConstInputBlobModifier::_InputBlobModifier_::has_requires_grad() const {
  return has_requires_grad_;
}
const bool& ConstInputBlobModifier::_InputBlobModifier_::requires_grad() const {
  if (has_requires_grad_) { return requires_grad_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstInputBlobModifier::_InputBlobModifier_::clear_requires_grad() {
  has_requires_grad_ = false;
}
void ConstInputBlobModifier::_InputBlobModifier_::set_requires_grad(const bool& value) {
  requires_grad_ = value;
  has_requires_grad_ = true;
}
bool* ConstInputBlobModifier::_InputBlobModifier_::mutable_requires_grad() {
  has_requires_grad_ = true;
  return &requires_grad_;
}


int ConstInputBlobModifier::_InputBlobModifier_::compare(const _InputBlobModifier_& other) {
  if (!(has_is_mutable() == other.has_is_mutable())) {
    return has_is_mutable() < other.has_is_mutable() ? -1 : 1;
  } else if (!(is_mutable() == other.is_mutable())) {
    return is_mutable() < other.is_mutable() ? -1 : 1;
  }
  if (!(has_requires_grad() == other.has_requires_grad())) {
    return has_requires_grad() < other.has_requires_grad() ? -1 : 1;
  } else if (!(requires_grad() == other.requires_grad())) {
    return requires_grad() < other.requires_grad() ? -1 : 1;
  }
  return 0;
}

bool ConstInputBlobModifier::_InputBlobModifier_::operator==(const _InputBlobModifier_& other) const {
  return true
    && has_is_mutable() == other.has_is_mutable() 
    && is_mutable() == other.is_mutable()
    && has_requires_grad() == other.has_requires_grad() 
    && requires_grad() == other.requires_grad()
  ;
}

std::size_t ConstInputBlobModifier::_InputBlobModifier_::__CalcHash__() const {
  return 0
    ^ (has_is_mutable() ? std::hash<bool>()(is_mutable()) : 0)
    ^ (has_requires_grad() ? std::hash<bool>()(requires_grad()) : 0)
  ;
}

bool ConstInputBlobModifier::_InputBlobModifier_::operator<(const _InputBlobModifier_& other) const {
  return false
    || !(has_is_mutable() == other.has_is_mutable()) ? 
      has_is_mutable() < other.has_is_mutable() : false
    || !(is_mutable() == other.is_mutable()) ? 
      is_mutable() < other.is_mutable() : false
    || !(has_requires_grad() == other.has_requires_grad()) ? 
      has_requires_grad() < other.has_requires_grad() : false
    || !(requires_grad() == other.requires_grad()) ? 
      requires_grad() < other.requires_grad() : false
  ;
}

using _InputBlobModifier_ =  ConstInputBlobModifier::_InputBlobModifier_;
ConstInputBlobModifier::ConstInputBlobModifier(const ::std::shared_ptr<_InputBlobModifier_>& data): data_(data) {}
ConstInputBlobModifier::ConstInputBlobModifier(): data_(::std::make_shared<_InputBlobModifier_>()) {}
ConstInputBlobModifier::ConstInputBlobModifier(const ::oneflow::InputBlobModifier& proto_inputblobmodifier) {
  BuildFromProto(proto_inputblobmodifier);
}
ConstInputBlobModifier::ConstInputBlobModifier(const ConstInputBlobModifier&) = default;
ConstInputBlobModifier::ConstInputBlobModifier(ConstInputBlobModifier&&) noexcept = default;
ConstInputBlobModifier::~ConstInputBlobModifier() = default;

void ConstInputBlobModifier::ToProto(PbMessage* proto_inputblobmodifier) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::InputBlobModifier*>(proto_inputblobmodifier));
}
  
::std::string ConstInputBlobModifier::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstInputBlobModifier::__Empty__() const {
  return !data_;
}

int ConstInputBlobModifier::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"is_mutable", 1},
    {"requires_grad", 3},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstInputBlobModifier::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstInputBlobModifier::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstInputBlobModifier::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &is_mutable();
    case 3: return &requires_grad();
    default: return nullptr;
  }
}

// required or optional field is_mutable
bool ConstInputBlobModifier::has_is_mutable() const {
  return __SharedPtrOrDefault__()->has_is_mutable();
}
const bool& ConstInputBlobModifier::is_mutable() const {
  return __SharedPtrOrDefault__()->is_mutable();
}
// used by pybind11 only
// required or optional field requires_grad
bool ConstInputBlobModifier::has_requires_grad() const {
  return __SharedPtrOrDefault__()->has_requires_grad();
}
const bool& ConstInputBlobModifier::requires_grad() const {
  return __SharedPtrOrDefault__()->requires_grad();
}
// used by pybind11 only

::std::shared_ptr<ConstInputBlobModifier> ConstInputBlobModifier::__SharedConst__() const {
  return ::std::make_shared<ConstInputBlobModifier>(data_);
}
int64_t ConstInputBlobModifier::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstInputBlobModifier::operator==(const ConstInputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstInputBlobModifier::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstInputBlobModifier::operator<(const ConstInputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_InputBlobModifier_>& ConstInputBlobModifier::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_InputBlobModifier_> default_ptr = std::make_shared<_InputBlobModifier_>();
  return default_ptr;
}
const ::std::shared_ptr<_InputBlobModifier_>& ConstInputBlobModifier::__SharedPtr__() {
  if (!data_) { data_.reset(new _InputBlobModifier_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstInputBlobModifier
void ConstInputBlobModifier::BuildFromProto(const PbMessage& proto_inputblobmodifier) {
  data_ = ::std::make_shared<_InputBlobModifier_>(dynamic_cast<const ::oneflow::InputBlobModifier&>(proto_inputblobmodifier));
}

InputBlobModifier::InputBlobModifier(const ::std::shared_ptr<_InputBlobModifier_>& data)
  : ConstInputBlobModifier(data) {}
InputBlobModifier::InputBlobModifier(const InputBlobModifier& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<InputBlobModifier> resize
InputBlobModifier::InputBlobModifier(InputBlobModifier&&) noexcept = default; 
InputBlobModifier::InputBlobModifier(const ::oneflow::InputBlobModifier& proto_inputblobmodifier) {
  InitFromProto(proto_inputblobmodifier);
}
InputBlobModifier::InputBlobModifier() = default;

InputBlobModifier::~InputBlobModifier() = default;

void InputBlobModifier::InitFromProto(const PbMessage& proto_inputblobmodifier) {
  BuildFromProto(proto_inputblobmodifier);
}
  
void* InputBlobModifier::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_is_mutable();
    case 3: return mutable_requires_grad();
    default: return nullptr;
  }
}

bool InputBlobModifier::operator==(const InputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t InputBlobModifier::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool InputBlobModifier::operator<(const InputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void InputBlobModifier::Clear() {
  if (data_) { data_.reset(); }
}
void InputBlobModifier::CopyFrom(const InputBlobModifier& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
InputBlobModifier& InputBlobModifier::operator=(const InputBlobModifier& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field is_mutable
void InputBlobModifier::clear_is_mutable() {
  return __SharedPtr__()->clear_is_mutable();
}
void InputBlobModifier::set_is_mutable(const bool& value) {
  return __SharedPtr__()->set_is_mutable(value);
}
bool* InputBlobModifier::mutable_is_mutable() {
  return  __SharedPtr__()->mutable_is_mutable();
}
// required or optional field requires_grad
void InputBlobModifier::clear_requires_grad() {
  return __SharedPtr__()->clear_requires_grad();
}
void InputBlobModifier::set_requires_grad(const bool& value) {
  return __SharedPtr__()->set_requires_grad(value);
}
bool* InputBlobModifier::mutable_requires_grad() {
  return  __SharedPtr__()->mutable_requires_grad();
}

::std::shared_ptr<InputBlobModifier> InputBlobModifier::__SharedMutable__() {
  return ::std::make_shared<InputBlobModifier>(__SharedPtr__());
}
ConstOutputBlobModifier::_OutputBlobModifier_::_OutputBlobModifier_() { Clear(); }
ConstOutputBlobModifier::_OutputBlobModifier_::_OutputBlobModifier_(const _OutputBlobModifier_& other) { CopyFrom(other); }
ConstOutputBlobModifier::_OutputBlobModifier_::_OutputBlobModifier_(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier) {
  InitFromProto(proto_outputblobmodifier);
}
ConstOutputBlobModifier::_OutputBlobModifier_::_OutputBlobModifier_(_OutputBlobModifier_&& other) = default;
ConstOutputBlobModifier::_OutputBlobModifier_::~_OutputBlobModifier_() = default;

void ConstOutputBlobModifier::_OutputBlobModifier_::InitFromProto(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier) {
  Clear();
  // required_or_optional field: is_mutable
  if (proto_outputblobmodifier.has_is_mutable()) {
    set_is_mutable(proto_outputblobmodifier.is_mutable());
  }
  // required_or_optional field: requires_grad
  if (proto_outputblobmodifier.has_requires_grad()) {
    set_requires_grad(proto_outputblobmodifier.requires_grad());
  }
  // required_or_optional field: header_infered_before_compute
  if (proto_outputblobmodifier.has_header_infered_before_compute()) {
    set_header_infered_before_compute(proto_outputblobmodifier.header_infered_before_compute());
  }
  // oneof field: inplace_type
  InplaceTypeCase inplace_type_case = InplaceTypeCase(int(proto_outputblobmodifier.inplace_type_case()));
  switch (inplace_type_case) {
    case kMutableInplaceIbn: {
      set_mutable_inplace_ibn(proto_outputblobmodifier.mutable_inplace_ibn());
      break;
  }
    case kConstInplaceIbn: {
      set_const_inplace_ibn(proto_outputblobmodifier.const_inplace_ibn());
      break;
  }
    case INPLACE_TYPE_NOT_SET: {
      break;
    }
  }
      
}

void ConstOutputBlobModifier::_OutputBlobModifier_::ToProto(::oneflow::OutputBlobModifier* proto_outputblobmodifier) const {
  proto_outputblobmodifier->Clear();
  // required_or_optional field: is_mutable
  if (this->has_is_mutable()) {
    proto_outputblobmodifier->set_is_mutable(is_mutable());
    }
  // required_or_optional field: requires_grad
  if (this->has_requires_grad()) {
    proto_outputblobmodifier->set_requires_grad(requires_grad());
    }
  // required_or_optional field: header_infered_before_compute
  if (this->has_header_infered_before_compute()) {
    proto_outputblobmodifier->set_header_infered_before_compute(header_infered_before_compute());
    }

  // oneof field: inplace_type
  ::oneflow::OutputBlobModifier::InplaceTypeCase inplace_type_case = ::oneflow::OutputBlobModifier::InplaceTypeCase(int(this->inplace_type_case()));
  switch (inplace_type_case) {
    case ::oneflow::OutputBlobModifier::kMutableInplaceIbn: {
      proto_outputblobmodifier->set_mutable_inplace_ibn(mutable_inplace_ibn());
      break;
    }
    case ::oneflow::OutputBlobModifier::kConstInplaceIbn: {
      proto_outputblobmodifier->set_const_inplace_ibn(const_inplace_ibn());
      break;
    }
    case ::oneflow::OutputBlobModifier::INPLACE_TYPE_NOT_SET: {
      break;
    }
  }
}

::std::string ConstOutputBlobModifier::_OutputBlobModifier_::DebugString() const {
  ::oneflow::OutputBlobModifier proto_outputblobmodifier;
  this->ToProto(&proto_outputblobmodifier);
  return proto_outputblobmodifier.DebugString();
}

void ConstOutputBlobModifier::_OutputBlobModifier_::Clear() {
  clear_is_mutable();
  clear_requires_grad();
  clear_header_infered_before_compute();
  clear_inplace_type();
}

void ConstOutputBlobModifier::_OutputBlobModifier_::CopyFrom(const _OutputBlobModifier_& other) {
  if (other.has_is_mutable()) {
    set_is_mutable(other.is_mutable());
  } else {
    clear_is_mutable();
  }
  if (other.has_requires_grad()) {
    set_requires_grad(other.requires_grad());
  } else {
    clear_requires_grad();
  }
  if (other.has_header_infered_before_compute()) {
    set_header_infered_before_compute(other.header_infered_before_compute());
  } else {
    clear_header_infered_before_compute();
  }
  inplace_type_copy_from(other);
}


// optional field is_mutable
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_is_mutable() const {
  return has_is_mutable_;
}
const bool& ConstOutputBlobModifier::_OutputBlobModifier_::is_mutable() const {
  if (has_is_mutable_) { return is_mutable_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_is_mutable() {
  has_is_mutable_ = false;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::set_is_mutable(const bool& value) {
  is_mutable_ = value;
  has_is_mutable_ = true;
}
bool* ConstOutputBlobModifier::_OutputBlobModifier_::mutable_is_mutable() {
  has_is_mutable_ = true;
  return &is_mutable_;
}

// optional field requires_grad
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_requires_grad() const {
  return has_requires_grad_;
}
const bool& ConstOutputBlobModifier::_OutputBlobModifier_::requires_grad() const {
  if (has_requires_grad_) { return requires_grad_; }
  static const bool default_static_value =
    bool(false);
  return default_static_value;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_requires_grad() {
  has_requires_grad_ = false;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::set_requires_grad(const bool& value) {
  requires_grad_ = value;
  has_requires_grad_ = true;
}
bool* ConstOutputBlobModifier::_OutputBlobModifier_::mutable_requires_grad() {
  has_requires_grad_ = true;
  return &requires_grad_;
}

// optional field header_infered_before_compute
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_header_infered_before_compute() const {
  return has_header_infered_before_compute_;
}
const bool& ConstOutputBlobModifier::_OutputBlobModifier_::header_infered_before_compute() const {
  if (has_header_infered_before_compute_) { return header_infered_before_compute_; }
  static const bool default_static_value =
    bool(true);
  return default_static_value;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_header_infered_before_compute() {
  has_header_infered_before_compute_ = false;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::set_header_infered_before_compute(const bool& value) {
  header_infered_before_compute_ = value;
  has_header_infered_before_compute_ = true;
}
bool* ConstOutputBlobModifier::_OutputBlobModifier_::mutable_header_infered_before_compute() {
  has_header_infered_before_compute_ = true;
  return &header_infered_before_compute_;
}

// oneof field inplace_type: mutable_inplace_ibn
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_mutable_inplace_ibn() const {
  return inplace_type_case() == kMutableInplaceIbn;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_mutable_inplace_ibn() {
  if (has_mutable_inplace_ibn()) {
    {
      using String = ::std::string;
      String* ptr = reinterpret_cast<String*>(&(inplace_type_.mutable_inplace_ibn_)[0]);
      ptr->~String();
    }
    inplace_type_case_ = INPLACE_TYPE_NOT_SET;
  }
}

const ::std::string& ConstOutputBlobModifier::_OutputBlobModifier_::mutable_inplace_ibn() const {
  if (has_mutable_inplace_ibn()) {
      const ::std::string* ptr = reinterpret_cast<const ::std::string*>(&(inplace_type_.mutable_inplace_ibn_)[0]);
    return *ptr;
    } else {
      static const ::std::string default_static_value = ::std::string();
    return default_static_value;
    }
}
void ConstOutputBlobModifier::_OutputBlobModifier_::set_mutable_inplace_ibn(const ::std::string& value) {
  if(!has_mutable_inplace_ibn()) {
    clear_inplace_type();
      new (&(inplace_type_.mutable_inplace_ibn_)) std::string();
    }
  inplace_type_case_ = kMutableInplaceIbn;
    std::string* ptr = reinterpret_cast<std::string*>(&(inplace_type_.mutable_inplace_ibn_)[0]);
  *ptr = value;
  }
::std::string* ConstOutputBlobModifier::_OutputBlobModifier_::mutable_mutable_inplace_ibn() {
  if(!has_mutable_inplace_ibn()) {
    clear_inplace_type();
      new (&(inplace_type_.mutable_inplace_ibn_)) std::string();
    }
    ::std::string* ptr = reinterpret_cast<::std::string*>(&(inplace_type_.mutable_inplace_ibn_)[0]);
  return ptr;
  }

// oneof field inplace_type: const_inplace_ibn
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_const_inplace_ibn() const {
  return inplace_type_case() == kConstInplaceIbn;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_const_inplace_ibn() {
  if (has_const_inplace_ibn()) {
    {
      using String = ::std::string;
      String* ptr = reinterpret_cast<String*>(&(inplace_type_.const_inplace_ibn_)[0]);
      ptr->~String();
    }
    inplace_type_case_ = INPLACE_TYPE_NOT_SET;
  }
}

const ::std::string& ConstOutputBlobModifier::_OutputBlobModifier_::const_inplace_ibn() const {
  if (has_const_inplace_ibn()) {
      const ::std::string* ptr = reinterpret_cast<const ::std::string*>(&(inplace_type_.const_inplace_ibn_)[0]);
    return *ptr;
    } else {
      static const ::std::string default_static_value = ::std::string();
    return default_static_value;
    }
}
void ConstOutputBlobModifier::_OutputBlobModifier_::set_const_inplace_ibn(const ::std::string& value) {
  if(!has_const_inplace_ibn()) {
    clear_inplace_type();
      new (&(inplace_type_.const_inplace_ibn_)) std::string();
    }
  inplace_type_case_ = kConstInplaceIbn;
    std::string* ptr = reinterpret_cast<std::string*>(&(inplace_type_.const_inplace_ibn_)[0]);
  *ptr = value;
  }
::std::string* ConstOutputBlobModifier::_OutputBlobModifier_::mutable_const_inplace_ibn() {
  if(!has_const_inplace_ibn()) {
    clear_inplace_type();
      new (&(inplace_type_.const_inplace_ibn_)) std::string();
    }
    ::std::string* ptr = reinterpret_cast<::std::string*>(&(inplace_type_.const_inplace_ibn_)[0]);
  return ptr;
  }
ConstOutputBlobModifier::InplaceTypeCase ConstOutputBlobModifier::_OutputBlobModifier_::inplace_type_case() const {
  return inplace_type_case_;
}
bool ConstOutputBlobModifier::_OutputBlobModifier_::has_inplace_type() const {
  return inplace_type_case_ != INPLACE_TYPE_NOT_SET;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::clear_inplace_type() {
  switch (inplace_type_case()) {
    case kMutableInplaceIbn: {
      {
        using String = ::std::string;
        String* ptr = reinterpret_cast<String*>(&(inplace_type_.mutable_inplace_ibn_)[0]);
        ptr->~String();
      }
      break;
    }
    case kConstInplaceIbn: {
      {
        using String = ::std::string;
        String* ptr = reinterpret_cast<String*>(&(inplace_type_.const_inplace_ibn_)[0]);
        ptr->~String();
      }
      break;
    }
    case INPLACE_TYPE_NOT_SET: {
      break;
    }
  }
  inplace_type_case_ = INPLACE_TYPE_NOT_SET;
}
void ConstOutputBlobModifier::_OutputBlobModifier_::inplace_type_copy_from(const _OutputBlobModifier_& other) {
  switch (other.inplace_type_case()) {
    case kMutableInplaceIbn: {
      set_mutable_inplace_ibn(other.mutable_inplace_ibn());
      break;
    }
    case kConstInplaceIbn: {
      set_const_inplace_ibn(other.const_inplace_ibn());
      break;
    }
    case INPLACE_TYPE_NOT_SET: {
      clear_inplace_type();
    }
  }
}


int ConstOutputBlobModifier::_OutputBlobModifier_::compare(const _OutputBlobModifier_& other) {
  if (!(has_is_mutable() == other.has_is_mutable())) {
    return has_is_mutable() < other.has_is_mutable() ? -1 : 1;
  } else if (!(is_mutable() == other.is_mutable())) {
    return is_mutable() < other.is_mutable() ? -1 : 1;
  }
  if (!(has_requires_grad() == other.has_requires_grad())) {
    return has_requires_grad() < other.has_requires_grad() ? -1 : 1;
  } else if (!(requires_grad() == other.requires_grad())) {
    return requires_grad() < other.requires_grad() ? -1 : 1;
  }
  if (!(has_header_infered_before_compute() == other.has_header_infered_before_compute())) {
    return has_header_infered_before_compute() < other.has_header_infered_before_compute() ? -1 : 1;
  } else if (!(header_infered_before_compute() == other.header_infered_before_compute())) {
    return header_infered_before_compute() < other.header_infered_before_compute() ? -1 : 1;
  }
  if (!(inplace_type_case() == other.inplace_type_case())) {
    return inplace_type_case() < other.inplace_type_case() ? -1 : 1;
  }
  switch (inplace_type_case()) {
    case kMutableInplaceIbn: {
      if (!(mutable_inplace_ibn() == other.mutable_inplace_ibn())) {
        return mutable_inplace_ibn() < other.mutable_inplace_ibn() ? -1 : 1;
      }
      break;
    }
    case kConstInplaceIbn: {
      if (!(const_inplace_ibn() == other.const_inplace_ibn())) {
        return const_inplace_ibn() < other.const_inplace_ibn() ? -1 : 1;
      }
      break;
    }
    case INPLACE_TYPE_NOT_SET: {
      break;
    }
  }
  return 0;
}

bool ConstOutputBlobModifier::_OutputBlobModifier_::operator==(const _OutputBlobModifier_& other) const {
  return true
    && has_is_mutable() == other.has_is_mutable() 
    && is_mutable() == other.is_mutable()
    && has_requires_grad() == other.has_requires_grad() 
    && requires_grad() == other.requires_grad()
    && has_header_infered_before_compute() == other.has_header_infered_before_compute() 
    && header_infered_before_compute() == other.header_infered_before_compute()
    && inplace_type_case() == other.inplace_type_case()
    && (inplace_type_case() == kMutableInplaceIbn ? 
      mutable_inplace_ibn() == other.mutable_inplace_ibn() : true)
    && (inplace_type_case() == kConstInplaceIbn ? 
      const_inplace_ibn() == other.const_inplace_ibn() : true)
  ;
}

std::size_t ConstOutputBlobModifier::_OutputBlobModifier_::__CalcHash__() const {
  return 0
    ^ (has_is_mutable() ? std::hash<bool>()(is_mutable()) : 0)
    ^ (has_requires_grad() ? std::hash<bool>()(requires_grad()) : 0)
    ^ (has_header_infered_before_compute() ? std::hash<bool>()(header_infered_before_compute()) : 0)
    ^ static_cast<std::size_t>(inplace_type_case())
    ^ (has_mutable_inplace_ibn() ? std::hash<::std::string>()(mutable_inplace_ibn()) : 0)
    ^ (has_const_inplace_ibn() ? std::hash<::std::string>()(const_inplace_ibn()) : 0)
  ;
}

bool ConstOutputBlobModifier::_OutputBlobModifier_::operator<(const _OutputBlobModifier_& other) const {
  return false
    || !(has_is_mutable() == other.has_is_mutable()) ? 
      has_is_mutable() < other.has_is_mutable() : false
    || !(is_mutable() == other.is_mutable()) ? 
      is_mutable() < other.is_mutable() : false
    || !(has_requires_grad() == other.has_requires_grad()) ? 
      has_requires_grad() < other.has_requires_grad() : false
    || !(requires_grad() == other.requires_grad()) ? 
      requires_grad() < other.requires_grad() : false
    || !(has_header_infered_before_compute() == other.has_header_infered_before_compute()) ? 
      has_header_infered_before_compute() < other.has_header_infered_before_compute() : false
    || !(header_infered_before_compute() == other.header_infered_before_compute()) ? 
      header_infered_before_compute() < other.header_infered_before_compute() : false
    || !(inplace_type_case() == other.inplace_type_case()) ? 
      inplace_type_case() < other.inplace_type_case() : false
    || ((inplace_type_case() == kMutableInplaceIbn) && 
      !(mutable_inplace_ibn() == other.mutable_inplace_ibn())) ? 
        mutable_inplace_ibn() < other.mutable_inplace_ibn() : false
    || ((inplace_type_case() == kConstInplaceIbn) && 
      !(const_inplace_ibn() == other.const_inplace_ibn())) ? 
        const_inplace_ibn() < other.const_inplace_ibn() : false
  ;
}

using _OutputBlobModifier_ =  ConstOutputBlobModifier::_OutputBlobModifier_;
ConstOutputBlobModifier::ConstOutputBlobModifier(const ::std::shared_ptr<_OutputBlobModifier_>& data): data_(data) {}
ConstOutputBlobModifier::ConstOutputBlobModifier(): data_(::std::make_shared<_OutputBlobModifier_>()) {}
ConstOutputBlobModifier::ConstOutputBlobModifier(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier) {
  BuildFromProto(proto_outputblobmodifier);
}
ConstOutputBlobModifier::ConstOutputBlobModifier(const ConstOutputBlobModifier&) = default;
ConstOutputBlobModifier::ConstOutputBlobModifier(ConstOutputBlobModifier&&) noexcept = default;
ConstOutputBlobModifier::~ConstOutputBlobModifier() = default;

void ConstOutputBlobModifier::ToProto(PbMessage* proto_outputblobmodifier) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::OutputBlobModifier*>(proto_outputblobmodifier));
}
  
::std::string ConstOutputBlobModifier::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstOutputBlobModifier::__Empty__() const {
  return !data_;
}

int ConstOutputBlobModifier::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"is_mutable", 1},
    {"requires_grad", 2},
    {"header_infered_before_compute", 3},
    {"mutable_inplace_ibn", 20},
    {"const_inplace_ibn", 21},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstOutputBlobModifier::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
    case 3:
    case 20:
    case 21:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstOutputBlobModifier::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 3: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(bool),
      };
      return type_indices;
    }
    case 20: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    case 21: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::std::string),
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstOutputBlobModifier::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &is_mutable();
    case 2: return &requires_grad();
    case 3: return &header_infered_before_compute();
    case 20: return &mutable_inplace_ibn();
    case 21: return &const_inplace_ibn();
    default: return nullptr;
  }
}

// required or optional field is_mutable
bool ConstOutputBlobModifier::has_is_mutable() const {
  return __SharedPtrOrDefault__()->has_is_mutable();
}
const bool& ConstOutputBlobModifier::is_mutable() const {
  return __SharedPtrOrDefault__()->is_mutable();
}
// used by pybind11 only
// required or optional field requires_grad
bool ConstOutputBlobModifier::has_requires_grad() const {
  return __SharedPtrOrDefault__()->has_requires_grad();
}
const bool& ConstOutputBlobModifier::requires_grad() const {
  return __SharedPtrOrDefault__()->requires_grad();
}
// used by pybind11 only
// required or optional field header_infered_before_compute
bool ConstOutputBlobModifier::has_header_infered_before_compute() const {
  return __SharedPtrOrDefault__()->has_header_infered_before_compute();
}
const bool& ConstOutputBlobModifier::header_infered_before_compute() const {
  return __SharedPtrOrDefault__()->header_infered_before_compute();
}
// used by pybind11 only
 // oneof field inplace_type: mutable_inplace_ibn
bool ConstOutputBlobModifier::has_mutable_inplace_ibn() const {
  return __SharedPtrOrDefault__()->has_mutable_inplace_ibn();
}
const ::std::string& ConstOutputBlobModifier::mutable_inplace_ibn() const {
  return __SharedPtrOrDefault__()->mutable_inplace_ibn();
}

// used by pybind11 only
 // oneof field inplace_type: const_inplace_ibn
bool ConstOutputBlobModifier::has_const_inplace_ibn() const {
  return __SharedPtrOrDefault__()->has_const_inplace_ibn();
}
const ::std::string& ConstOutputBlobModifier::const_inplace_ibn() const {
  return __SharedPtrOrDefault__()->const_inplace_ibn();
}

// used by pybind11 only
ConstOutputBlobModifier::InplaceTypeCase ConstOutputBlobModifier::inplace_type_case() const {
  return __SharedPtrOrDefault__()->inplace_type_case();
}

bool ConstOutputBlobModifier::has_inplace_type() const {
  return __SharedPtrOrDefault__()->has_inplace_type();
}

::std::shared_ptr<ConstOutputBlobModifier> ConstOutputBlobModifier::__SharedConst__() const {
  return ::std::make_shared<ConstOutputBlobModifier>(data_);
}
int64_t ConstOutputBlobModifier::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstOutputBlobModifier::operator==(const ConstOutputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstOutputBlobModifier::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstOutputBlobModifier::operator<(const ConstOutputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_OutputBlobModifier_>& ConstOutputBlobModifier::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_OutputBlobModifier_> default_ptr = std::make_shared<_OutputBlobModifier_>();
  return default_ptr;
}
const ::std::shared_ptr<_OutputBlobModifier_>& ConstOutputBlobModifier::__SharedPtr__() {
  if (!data_) { data_.reset(new _OutputBlobModifier_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstOutputBlobModifier
void ConstOutputBlobModifier::BuildFromProto(const PbMessage& proto_outputblobmodifier) {
  data_ = ::std::make_shared<_OutputBlobModifier_>(dynamic_cast<const ::oneflow::OutputBlobModifier&>(proto_outputblobmodifier));
}

OutputBlobModifier::OutputBlobModifier(const ::std::shared_ptr<_OutputBlobModifier_>& data)
  : ConstOutputBlobModifier(data) {}
OutputBlobModifier::OutputBlobModifier(const OutputBlobModifier& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<OutputBlobModifier> resize
OutputBlobModifier::OutputBlobModifier(OutputBlobModifier&&) noexcept = default; 
OutputBlobModifier::OutputBlobModifier(const ::oneflow::OutputBlobModifier& proto_outputblobmodifier) {
  InitFromProto(proto_outputblobmodifier);
}
OutputBlobModifier::OutputBlobModifier() = default;

OutputBlobModifier::~OutputBlobModifier() = default;

void OutputBlobModifier::InitFromProto(const PbMessage& proto_outputblobmodifier) {
  BuildFromProto(proto_outputblobmodifier);
}
  
void* OutputBlobModifier::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_is_mutable();
    case 2: return mutable_requires_grad();
    case 3: return mutable_header_infered_before_compute();
    case 20: return mutable_mutable_inplace_ibn();
    case 21: return mutable_const_inplace_ibn();
    default: return nullptr;
  }
}

bool OutputBlobModifier::operator==(const OutputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t OutputBlobModifier::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool OutputBlobModifier::operator<(const OutputBlobModifier& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void OutputBlobModifier::Clear() {
  if (data_) { data_.reset(); }
}
void OutputBlobModifier::CopyFrom(const OutputBlobModifier& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
OutputBlobModifier& OutputBlobModifier::operator=(const OutputBlobModifier& other) {
  CopyFrom(other);
  return *this;
}

// required or optional field is_mutable
void OutputBlobModifier::clear_is_mutable() {
  return __SharedPtr__()->clear_is_mutable();
}
void OutputBlobModifier::set_is_mutable(const bool& value) {
  return __SharedPtr__()->set_is_mutable(value);
}
bool* OutputBlobModifier::mutable_is_mutable() {
  return  __SharedPtr__()->mutable_is_mutable();
}
// required or optional field requires_grad
void OutputBlobModifier::clear_requires_grad() {
  return __SharedPtr__()->clear_requires_grad();
}
void OutputBlobModifier::set_requires_grad(const bool& value) {
  return __SharedPtr__()->set_requires_grad(value);
}
bool* OutputBlobModifier::mutable_requires_grad() {
  return  __SharedPtr__()->mutable_requires_grad();
}
// required or optional field header_infered_before_compute
void OutputBlobModifier::clear_header_infered_before_compute() {
  return __SharedPtr__()->clear_header_infered_before_compute();
}
void OutputBlobModifier::set_header_infered_before_compute(const bool& value) {
  return __SharedPtr__()->set_header_infered_before_compute(value);
}
bool* OutputBlobModifier::mutable_header_infered_before_compute() {
  return  __SharedPtr__()->mutable_header_infered_before_compute();
}
void OutputBlobModifier::clear_mutable_inplace_ibn() {
  return __SharedPtr__()->clear_mutable_inplace_ibn();
}
void OutputBlobModifier::set_mutable_inplace_ibn(const ::std::string& value) {
  return __SharedPtr__()->set_mutable_inplace_ibn(value);
}
::std::string* OutputBlobModifier::mutable_mutable_inplace_ibn() {
  return  __SharedPtr__()->mutable_mutable_inplace_ibn();
}
void OutputBlobModifier::clear_const_inplace_ibn() {
  return __SharedPtr__()->clear_const_inplace_ibn();
}
void OutputBlobModifier::set_const_inplace_ibn(const ::std::string& value) {
  return __SharedPtr__()->set_const_inplace_ibn(value);
}
::std::string* OutputBlobModifier::mutable_const_inplace_ibn() {
  return  __SharedPtr__()->mutable_const_inplace_ibn();
}

::std::shared_ptr<OutputBlobModifier> OutputBlobModifier::__SharedMutable__() {
  return ::std::make_shared<OutputBlobModifier>(__SharedPtr__());
}
ConstArgModifierSignature::_ArgModifierSignature_::_ArgModifierSignature_() { Clear(); }
ConstArgModifierSignature::_ArgModifierSignature_::_ArgModifierSignature_(const _ArgModifierSignature_& other) { CopyFrom(other); }
ConstArgModifierSignature::_ArgModifierSignature_::_ArgModifierSignature_(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature) {
  InitFromProto(proto_argmodifiersignature);
}
ConstArgModifierSignature::_ArgModifierSignature_::_ArgModifierSignature_(_ArgModifierSignature_&& other) = default;
ConstArgModifierSignature::_ArgModifierSignature_::~_ArgModifierSignature_() = default;

void ConstArgModifierSignature::_ArgModifierSignature_::InitFromProto(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature) {
  Clear();
  // map field : ibn2input_blob_modifier
  if (!proto_argmodifiersignature.ibn2input_blob_modifier().empty()) {
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_&  mut_ibn2input_blob_modifier = *mutable_ibn2input_blob_modifier();
    for (const auto& pair : proto_argmodifiersignature.ibn2input_blob_modifier()) {
      mut_ibn2input_blob_modifier[pair.first] = ::oneflow::cfg::InputBlobModifier(pair.second);
    }
  }
  // map field : obn2output_blob_modifier
  if (!proto_argmodifiersignature.obn2output_blob_modifier().empty()) {
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_&  mut_obn2output_blob_modifier = *mutable_obn2output_blob_modifier();
    for (const auto& pair : proto_argmodifiersignature.obn2output_blob_modifier()) {
      mut_obn2output_blob_modifier[pair.first] = ::oneflow::cfg::OutputBlobModifier(pair.second);
    }
  }
    
}

void ConstArgModifierSignature::_ArgModifierSignature_::ToProto(::oneflow::ArgModifierSignature* proto_argmodifiersignature) const {
  proto_argmodifiersignature->Clear();
  // map field : ibn2input_blob_modifier
  if (!ibn2input_blob_modifier().empty()) {
    auto& mut_ibn2input_blob_modifier = *(proto_argmodifiersignature->mutable_ibn2input_blob_modifier());
    for (const auto& pair : ibn2input_blob_modifier()) {
      ::oneflow::InputBlobModifier proto_ibn2input_blob_modifier_value;
      pair.second.ToProto(&proto_ibn2input_blob_modifier_value);
      mut_ibn2input_blob_modifier[pair.first] = proto_ibn2input_blob_modifier_value;
    }
  }
  // map field : obn2output_blob_modifier
  if (!obn2output_blob_modifier().empty()) {
    auto& mut_obn2output_blob_modifier = *(proto_argmodifiersignature->mutable_obn2output_blob_modifier());
    for (const auto& pair : obn2output_blob_modifier()) {
      ::oneflow::OutputBlobModifier proto_obn2output_blob_modifier_value;
      pair.second.ToProto(&proto_obn2output_blob_modifier_value);
      mut_obn2output_blob_modifier[pair.first] = proto_obn2output_blob_modifier_value;
    }
  }

}

::std::string ConstArgModifierSignature::_ArgModifierSignature_::DebugString() const {
  ::oneflow::ArgModifierSignature proto_argmodifiersignature;
  this->ToProto(&proto_argmodifiersignature);
  return proto_argmodifiersignature.DebugString();
}

void ConstArgModifierSignature::_ArgModifierSignature_::Clear() {
  clear_ibn2input_blob_modifier();
  clear_obn2output_blob_modifier();
}

void ConstArgModifierSignature::_ArgModifierSignature_::CopyFrom(const _ArgModifierSignature_& other) {
  mutable_ibn2input_blob_modifier()->CopyFrom(other.ibn2input_blob_modifier());
  mutable_obn2output_blob_modifier()->CopyFrom(other.obn2output_blob_modifier());
}


::std::size_t ConstArgModifierSignature::_ArgModifierSignature_::ibn2input_blob_modifier_size() const {
  if (!ibn2input_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>();
    return default_static_value->size();
  }
  return ibn2input_blob_modifier_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& ConstArgModifierSignature::_ArgModifierSignature_::ibn2input_blob_modifier() const {
  if (!ibn2input_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>();
    return *(default_static_value.get());
  }
  return *(ibn2input_blob_modifier_.get());
}

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ * ConstArgModifierSignature::_ArgModifierSignature_::mutable_ibn2input_blob_modifier() {
  if (!ibn2input_blob_modifier_) {
    ibn2input_blob_modifier_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>();
  }
  return ibn2input_blob_modifier_.get();
}

const ::oneflow::cfg::InputBlobModifier& ConstArgModifierSignature::_ArgModifierSignature_::ibn2input_blob_modifier(::std::string key) const {
  if (!ibn2input_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>();
    return default_static_value->at(key);
  }
  return ibn2input_blob_modifier_->at(key);
}

void ConstArgModifierSignature::_ArgModifierSignature_::clear_ibn2input_blob_modifier() {
  if (!ibn2input_blob_modifier_) {
    ibn2input_blob_modifier_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>();
  }
  return ibn2input_blob_modifier_->Clear();
}


::std::size_t ConstArgModifierSignature::_ArgModifierSignature_::obn2output_blob_modifier_size() const {
  if (!obn2output_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>();
    return default_static_value->size();
  }
  return obn2output_blob_modifier_->size();
}
const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& ConstArgModifierSignature::_ArgModifierSignature_::obn2output_blob_modifier() const {
  if (!obn2output_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>();
    return *(default_static_value.get());
  }
  return *(obn2output_blob_modifier_.get());
}

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ * ConstArgModifierSignature::_ArgModifierSignature_::mutable_obn2output_blob_modifier() {
  if (!obn2output_blob_modifier_) {
    obn2output_blob_modifier_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>();
  }
  return obn2output_blob_modifier_.get();
}

const ::oneflow::cfg::OutputBlobModifier& ConstArgModifierSignature::_ArgModifierSignature_::obn2output_blob_modifier(::std::string key) const {
  if (!obn2output_blob_modifier_) {
    static const ::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> default_static_value =
      ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>();
    return default_static_value->at(key);
  }
  return obn2output_blob_modifier_->at(key);
}

void ConstArgModifierSignature::_ArgModifierSignature_::clear_obn2output_blob_modifier() {
  if (!obn2output_blob_modifier_) {
    obn2output_blob_modifier_ = ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>();
  }
  return obn2output_blob_modifier_->Clear();
}



int ConstArgModifierSignature::_ArgModifierSignature_::compare(const _ArgModifierSignature_& other) {
  if (!(ibn2input_blob_modifier() == other.ibn2input_blob_modifier())) {
    return ibn2input_blob_modifier() < other.ibn2input_blob_modifier() ? -1 : 1;
  }
  if (!(obn2output_blob_modifier() == other.obn2output_blob_modifier())) {
    return obn2output_blob_modifier() < other.obn2output_blob_modifier() ? -1 : 1;
  }
  return 0;
}

bool ConstArgModifierSignature::_ArgModifierSignature_::operator==(const _ArgModifierSignature_& other) const {
  return true
    && ibn2input_blob_modifier() == other.ibn2input_blob_modifier()
    && obn2output_blob_modifier() == other.obn2output_blob_modifier()
  ;
}

std::size_t ConstArgModifierSignature::_ArgModifierSignature_::__CalcHash__() const {
  return 0
    ^ ibn2input_blob_modifier().__CalcHash__()
    ^ obn2output_blob_modifier().__CalcHash__()
  ;
}

bool ConstArgModifierSignature::_ArgModifierSignature_::operator<(const _ArgModifierSignature_& other) const {
  return false
    || !(ibn2input_blob_modifier() == other.ibn2input_blob_modifier()) ? 
      ibn2input_blob_modifier() < other.ibn2input_blob_modifier() : false
    || !(obn2output_blob_modifier() == other.obn2output_blob_modifier()) ? 
      obn2output_blob_modifier() < other.obn2output_blob_modifier() : false
  ;
}

using _ArgModifierSignature_ =  ConstArgModifierSignature::_ArgModifierSignature_;
ConstArgModifierSignature::ConstArgModifierSignature(const ::std::shared_ptr<_ArgModifierSignature_>& data): data_(data) {}
ConstArgModifierSignature::ConstArgModifierSignature(): data_(::std::make_shared<_ArgModifierSignature_>()) {}
ConstArgModifierSignature::ConstArgModifierSignature(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature) {
  BuildFromProto(proto_argmodifiersignature);
}
ConstArgModifierSignature::ConstArgModifierSignature(const ConstArgModifierSignature&) = default;
ConstArgModifierSignature::ConstArgModifierSignature(ConstArgModifierSignature&&) noexcept = default;
ConstArgModifierSignature::~ConstArgModifierSignature() = default;

void ConstArgModifierSignature::ToProto(PbMessage* proto_argmodifiersignature) const {
  __SharedPtrOrDefault__()->ToProto(dynamic_cast<::oneflow::ArgModifierSignature*>(proto_argmodifiersignature));
}
  
::std::string ConstArgModifierSignature::DebugString() const {
  return __SharedPtrOrDefault__()->DebugString();
}

bool ConstArgModifierSignature::__Empty__() const {
  return !data_;
}

int ConstArgModifierSignature::FieldNumber4FieldName(const ::std::string& field_name) const  {
  static const ::std::map<::std::string, int> field_name2field_number{
    {"ibn2input_blob_modifier", 1},
    {"obn2output_blob_modifier", 2},
  };
  const auto& iter = field_name2field_number.find(field_name);
  if (iter != field_name2field_number.end()) { return iter->second; }
  return 0;
}

bool ConstArgModifierSignature::FieldDefined4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::std::set<::std::type_index>& ConstArgModifierSignature::ValidTypeIndices4FieldNumber(int field_number) const {
  switch (field_number) {
    case 1: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::InputBlobModifier>)
      };
      return type_indices;
    }
    case 2: {
      static const ::std::set<::std::type_index> type_indices{
        typeid(::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OutputBlobModifier>)
      };
      return type_indices;
    }
    default: {
      static const ::std::set<::std::type_index> empty;
      return empty;
    }
  }
}

const void* ConstArgModifierSignature::FieldPtr4FieldNumber(int field_number) const  {
  switch (field_number) {
    case 1: return &ibn2input_blob_modifier();
    case 2: return &obn2output_blob_modifier();
    default: return nullptr;
  }
}

// map field ibn2input_blob_modifier
::std::size_t ConstArgModifierSignature::ibn2input_blob_modifier_size() const {
  return __SharedPtrOrDefault__()->ibn2input_blob_modifier_size();
}

const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& ConstArgModifierSignature::ibn2input_blob_modifier() const {
  return __SharedPtrOrDefault__()->ibn2input_blob_modifier();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> ConstArgModifierSignature::shared_const_ibn2input_blob_modifier() const {
  return ibn2input_blob_modifier().__SharedConst__();
}
// map field obn2output_blob_modifier
::std::size_t ConstArgModifierSignature::obn2output_blob_modifier_size() const {
  return __SharedPtrOrDefault__()->obn2output_blob_modifier_size();
}

const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& ConstArgModifierSignature::obn2output_blob_modifier() const {
  return __SharedPtrOrDefault__()->obn2output_blob_modifier();
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> ConstArgModifierSignature::shared_const_obn2output_blob_modifier() const {
  return obn2output_blob_modifier().__SharedConst__();
}

::std::shared_ptr<ConstArgModifierSignature> ConstArgModifierSignature::__SharedConst__() const {
  return ::std::make_shared<ConstArgModifierSignature>(data_);
}
int64_t ConstArgModifierSignature::__Id__() const { return reinterpret_cast<int64_t>(data_.get()); }


bool ConstArgModifierSignature::operator==(const ConstArgModifierSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ConstArgModifierSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ConstArgModifierSignature::operator<(const ConstArgModifierSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}

const ::std::shared_ptr<_ArgModifierSignature_>& ConstArgModifierSignature::__SharedPtrOrDefault__() const {
  if (data_) { return data_; }
  static const ::std::shared_ptr<_ArgModifierSignature_> default_ptr = std::make_shared<_ArgModifierSignature_>();
  return default_ptr;
}
const ::std::shared_ptr<_ArgModifierSignature_>& ConstArgModifierSignature::__SharedPtr__() {
  if (!data_) { data_.reset(new _ArgModifierSignature_()); }
  return data_;
}
// use a protected member method to avoid someone change member variable(data_) by ConstArgModifierSignature
void ConstArgModifierSignature::BuildFromProto(const PbMessage& proto_argmodifiersignature) {
  data_ = ::std::make_shared<_ArgModifierSignature_>(dynamic_cast<const ::oneflow::ArgModifierSignature&>(proto_argmodifiersignature));
}

ArgModifierSignature::ArgModifierSignature(const ::std::shared_ptr<_ArgModifierSignature_>& data)
  : ConstArgModifierSignature(data) {}
ArgModifierSignature::ArgModifierSignature(const ArgModifierSignature& other) { CopyFrom(other); }
// enable nothrow for ::std::vector<ArgModifierSignature> resize
ArgModifierSignature::ArgModifierSignature(ArgModifierSignature&&) noexcept = default; 
ArgModifierSignature::ArgModifierSignature(const ::oneflow::ArgModifierSignature& proto_argmodifiersignature) {
  InitFromProto(proto_argmodifiersignature);
}
ArgModifierSignature::ArgModifierSignature() = default;

ArgModifierSignature::~ArgModifierSignature() = default;

void ArgModifierSignature::InitFromProto(const PbMessage& proto_argmodifiersignature) {
  BuildFromProto(proto_argmodifiersignature);
}
  
void* ArgModifierSignature::MutableFieldPtr4FieldNumber(int field_number) {
  switch (field_number) {
    case 1: return mutable_ibn2input_blob_modifier();
    case 2: return mutable_obn2output_blob_modifier();
    default: return nullptr;
  }
}

bool ArgModifierSignature::operator==(const ArgModifierSignature& other) const {
  return *__SharedPtrOrDefault__() == *other.__SharedPtrOrDefault__();
}

std::size_t ArgModifierSignature::__CalcHash__() const {
  return __SharedPtrOrDefault__()->__CalcHash__();
}

bool ArgModifierSignature::operator<(const ArgModifierSignature& other) const {
  return *__SharedPtrOrDefault__() < *other.__SharedPtrOrDefault__();
}
void ArgModifierSignature::Clear() {
  if (data_) { data_.reset(); }
}
void ArgModifierSignature::CopyFrom(const ArgModifierSignature& other) {
  if (other.__Empty__()) {
    Clear();
  } else {
    __SharedPtr__()->CopyFrom(*other.data_);
  }
}
ArgModifierSignature& ArgModifierSignature::operator=(const ArgModifierSignature& other) {
  CopyFrom(other);
  return *this;
}

// repeated field ibn2input_blob_modifier
void ArgModifierSignature::clear_ibn2input_blob_modifier() {
  return __SharedPtr__()->clear_ibn2input_blob_modifier();
}

const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_ & ArgModifierSignature::ibn2input_blob_modifier() const {
  return __SharedConst__()->ibn2input_blob_modifier();
}

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_* ArgModifierSignature::mutable_ibn2input_blob_modifier() {
  return __SharedPtr__()->mutable_ibn2input_blob_modifier();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> ArgModifierSignature::shared_mutable_ibn2input_blob_modifier() {
  return mutable_ibn2input_blob_modifier()->__SharedMutable__();
}
// repeated field obn2output_blob_modifier
void ArgModifierSignature::clear_obn2output_blob_modifier() {
  return __SharedPtr__()->clear_obn2output_blob_modifier();
}

const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_ & ArgModifierSignature::obn2output_blob_modifier() const {
  return __SharedConst__()->obn2output_blob_modifier();
}

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_* ArgModifierSignature::mutable_obn2output_blob_modifier() {
  return __SharedPtr__()->mutable_obn2output_blob_modifier();
}

  // used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> ArgModifierSignature::shared_mutable_obn2output_blob_modifier() {
  return mutable_obn2output_blob_modifier()->__SharedMutable__();
}

::std::shared_ptr<ArgModifierSignature> ArgModifierSignature::__SharedMutable__() {
  return ::std::make_shared<ArgModifierSignature>(__SharedPtr__());
}

Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::InputBlobModifier>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::InputBlobModifier>(data) {}
Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_() = default;
Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::~Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_() = default;

bool Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::InputBlobModifier>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::InputBlobModifier& Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstInputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ConstInputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ConstInputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::InputBlobModifier>>& data): Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_(data) {}
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_() = default;
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::~_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_() = default;

void _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::InputBlobModifier>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::InputBlobModifier>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::operator==(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::InputBlobModifier>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::operator<(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::InputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ::oneflow::cfg::InputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_, ::oneflow::cfg::InputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_InputBlobModifier_::shared_mut_end() { return end(); }
Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OutputBlobModifier>>& data): ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OutputBlobModifier>(data) {}
Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_() = default;
Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::~Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_() = default;

bool Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::operator==(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::OutputBlobModifier>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::operator<(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
const ::oneflow::cfg::OutputBlobModifier& Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::Get(const ::std::string& key) const {
return at(key);
}

// used by pybind11 only
::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__SharedConst__() const {
  return ::std::make_shared<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>(__SharedPtr__());
}

// used by pybind11 only
::std::shared_ptr<ConstOutputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__SharedConst__(const ::std::string& key) const {
  return at(key).__SharedConst__();
}

// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ConstOutputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::shared_const_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedConstPairIterator_<Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ConstOutputBlobModifier> Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::shared_const_end() { return end(); }

_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_(const ::std::shared_ptr<::std::map<::std::string, ::oneflow::cfg::OutputBlobModifier>>& data): Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_(data) {}
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_() = default;
_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::~_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_() = default;

void _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::CopyFrom(const Const_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OutputBlobModifier>::CopyFrom(other);
}
void _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::CopyFrom(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) {
  ::oneflow::cfg::_MapField_<::std::string, ::oneflow::cfg::OutputBlobModifier>::CopyFrom(other);
}

bool _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::operator==(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const {
  return *__SharedPtr__() == *other.__SharedPtr__();
}

std::size_t _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__CalcHash__() const {
  std::size_t hash_value = 0;
  const auto& key_hash = std::hash<::std::string>();
  const auto& value_hash = std::hash<::oneflow::cfg::OutputBlobModifier>();
  for (const auto& pair : *__SharedPtr__()) {
    hash_value ^= key_hash(pair.first) ^ value_hash(pair.second); 
  }
  return hash_value;
}

bool _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::operator<(const _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_& other) const {
  return *__SharedPtr__() < *other.__SharedPtr__();
}
// used by pybind11 only
::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__SharedMutable__() {
  return ::std::make_shared<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_>(__SharedPtr__());
}

::std::shared_ptr<::oneflow::cfg::OutputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::__SharedMutable__(const ::std::string& key) {
  return (*this)[key].__SharedMutable__();
}
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ::oneflow::cfg::OutputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::shared_mut_begin() { return begin(); }
// ensuring mapped data's lifetime safety
::oneflow::cfg::_SharedMutPairIterator_<_CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_, ::oneflow::cfg::OutputBlobModifier> _CFG_ONEFLOW_CORE_OPERATOR_ARG_MODIFIER_SIGNATURE_CFG_H__MapField___std__string_OutputBlobModifier_::shared_mut_end() { return end(); }

}
} // namespace oneflow
