#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include "oneflow/cfg/pybind_module_registry.h"
#include "oneflow/core/operator/op_attribute.cfg.h"
#include "oneflow/core/register/logical_blob_id.cfg.h"
#include "oneflow/core/register/blob_desc.cfg.h"
#include "oneflow/core/operator/op_conf.cfg.h"
#include "oneflow/core/operator/arg_modifier_signature.cfg.h"
#include "oneflow/core/job/sbp_parallel.cfg.h"
#include "oneflow/core/job/mirrored_parallel.cfg.h"
#include "oneflow/core/job/blob_lifetime_signature.cfg.h"
#include "oneflow/core/job/parallel_signature.cfg.h"
#include "oneflow/core/job/parallel_conf_signature.cfg.h"

ONEFLOW_CFG_PYBIND11_MODULE("oneflow.core.operator.op_attribute", m) {
  using namespace oneflow::cfg;


  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Get);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::*)(const ::std::string&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("Get", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Get);
    registry.def("__setitem__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_::Set);
  }

  {
    pybind11::class_<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_, std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>> registry(m, "Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_");
    registry.def("__len__", &Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::size);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def("__getitem__", (::std::shared_ptr<ConstOpAttribute> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedConst__);
    registry.def("Get", (::std::shared_ptr<ConstOpAttribute> (Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(::std::size_t) const)&Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedConst__);
  }
  {
    pybind11::class_<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_, std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_>> registry(m, "_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_");
    registry.def("__len__", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::size);
    registry.def("Set", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::Set);
    registry.def("Clear", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::Clear);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(const Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::CopyFrom);
    registry.def("CopyFrom", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(const _CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::CopyFrom);
    registry.def("Add", (void (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(const ::oneflow::cfg::OpAttribute&))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::Add);
    
    registry.def(pybind11::self == pybind11::self);
    registry.def(pybind11::self < pybind11::self);
    registry.def("__getitem__", (::std::shared_ptr<::oneflow::cfg::OpAttribute> (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedMutable__);
    registry.def("Get", (::std::shared_ptr<::oneflow::cfg::OpAttribute> (_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::*)(::std::size_t))&_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedMutable__);
    registry.def("Add", &_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_::__SharedAdd__);
  }

  {
    pybind11::class_<ConstOpAttribute, ::oneflow::cfg::Message, std::shared_ptr<ConstOpAttribute>> registry(m, "ConstOpAttribute");
    registry.def("__id__", &::oneflow::cfg::OpAttribute::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpAttribute::DebugString);
    registry.def("__repr__", &ConstOpAttribute::DebugString);

    registry.def("input_bns_size", &ConstOpAttribute::input_bns_size);
    registry.def("input_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (ConstOpAttribute::*)() const)&ConstOpAttribute::shared_const_input_bns);
    registry.def("input_bns", (const ::std::string& (ConstOpAttribute::*)(::std::size_t) const)&ConstOpAttribute::input_bns);

    registry.def("output_bns_size", &ConstOpAttribute::output_bns_size);
    registry.def("output_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (ConstOpAttribute::*)() const)&ConstOpAttribute::shared_const_output_bns);
    registry.def("output_bns", (const ::std::string& (ConstOpAttribute::*)(::std::size_t) const)&ConstOpAttribute::output_bns);

    registry.def("tmp_bns_size", &ConstOpAttribute::tmp_bns_size);
    registry.def("tmp_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (ConstOpAttribute::*)() const)&ConstOpAttribute::shared_const_tmp_bns);
    registry.def("tmp_bns", (const ::std::string& (ConstOpAttribute::*)(::std::size_t) const)&ConstOpAttribute::tmp_bns);

    registry.def("has_op_conf", &ConstOpAttribute::has_op_conf);
    registry.def("op_conf", &ConstOpAttribute::shared_const_op_conf);

    registry.def("has_arg_signature", &ConstOpAttribute::has_arg_signature);
    registry.def("arg_signature", &ConstOpAttribute::shared_const_arg_signature);

    registry.def("has_arg_modifier_signature", &ConstOpAttribute::has_arg_modifier_signature);
    registry.def("arg_modifier_signature", &ConstOpAttribute::shared_const_arg_modifier_signature);

    registry.def("has_blob_last_used_signature", &ConstOpAttribute::has_blob_last_used_signature);
    registry.def("blob_last_used_signature", &ConstOpAttribute::shared_const_blob_last_used_signature);

    registry.def("has_blob_backward_used_signature", &ConstOpAttribute::has_blob_backward_used_signature);
    registry.def("blob_backward_used_signature", &ConstOpAttribute::shared_const_blob_backward_used_signature);

    registry.def("has_sbp_signature", &ConstOpAttribute::has_sbp_signature);
    registry.def("sbp_signature", &ConstOpAttribute::shared_const_sbp_signature);

    registry.def("has_mirrored_signature", &ConstOpAttribute::has_mirrored_signature);
    registry.def("mirrored_signature", &ConstOpAttribute::shared_const_mirrored_signature);

    registry.def("has_logical_blob_desc_signature", &ConstOpAttribute::has_logical_blob_desc_signature);
    registry.def("logical_blob_desc_signature", &ConstOpAttribute::shared_const_logical_blob_desc_signature);

    registry.def("has_parallel_signature", &ConstOpAttribute::has_parallel_signature);
    registry.def("parallel_signature", &ConstOpAttribute::shared_const_parallel_signature);

    registry.def("has_parallel_conf_signature", &ConstOpAttribute::has_parallel_conf_signature);
    registry.def("parallel_conf_signature", &ConstOpAttribute::shared_const_parallel_conf_signature);

    registry.def("has_parallel_distribution_signature", &ConstOpAttribute::has_parallel_distribution_signature);
    registry.def("parallel_distribution_signature", &ConstOpAttribute::shared_const_parallel_distribution_signature);
  }
  {
    pybind11::class_<::oneflow::cfg::OpAttribute, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpAttribute>> registry(m, "OpAttribute");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpAttribute::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpAttribute::*)(const ConstOpAttribute&))&::oneflow::cfg::OpAttribute::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpAttribute::*)(const ::oneflow::cfg::OpAttribute&))&::oneflow::cfg::OpAttribute::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpAttribute::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpAttribute::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpAttribute::DebugString);



    registry.def("input_bns_size", &::oneflow::cfg::OpAttribute::input_bns_size);
    registry.def("clear_input_bns", &::oneflow::cfg::OpAttribute::clear_input_bns);
    registry.def("mutable_input_bns", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)())&::oneflow::cfg::OpAttribute::shared_mutable_input_bns);
    registry.def("input_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)() const)&::oneflow::cfg::OpAttribute::shared_const_input_bns);
    registry.def("input_bns", (const ::std::string& (::oneflow::cfg::OpAttribute::*)(::std::size_t) const)&::oneflow::cfg::OpAttribute::input_bns);
    registry.def("add_input_bns", &::oneflow::cfg::OpAttribute::add_input_bns);
    registry.def("set_input_bns", &::oneflow::cfg::OpAttribute::set_input_bns);

    registry.def("output_bns_size", &::oneflow::cfg::OpAttribute::output_bns_size);
    registry.def("clear_output_bns", &::oneflow::cfg::OpAttribute::clear_output_bns);
    registry.def("mutable_output_bns", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)())&::oneflow::cfg::OpAttribute::shared_mutable_output_bns);
    registry.def("output_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)() const)&::oneflow::cfg::OpAttribute::shared_const_output_bns);
    registry.def("output_bns", (const ::std::string& (::oneflow::cfg::OpAttribute::*)(::std::size_t) const)&::oneflow::cfg::OpAttribute::output_bns);
    registry.def("add_output_bns", &::oneflow::cfg::OpAttribute::add_output_bns);
    registry.def("set_output_bns", &::oneflow::cfg::OpAttribute::set_output_bns);

    registry.def("tmp_bns_size", &::oneflow::cfg::OpAttribute::tmp_bns_size);
    registry.def("clear_tmp_bns", &::oneflow::cfg::OpAttribute::clear_tmp_bns);
    registry.def("mutable_tmp_bns", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)())&::oneflow::cfg::OpAttribute::shared_mutable_tmp_bns);
    registry.def("tmp_bns", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField___std__string_> (::oneflow::cfg::OpAttribute::*)() const)&::oneflow::cfg::OpAttribute::shared_const_tmp_bns);
    registry.def("tmp_bns", (const ::std::string& (::oneflow::cfg::OpAttribute::*)(::std::size_t) const)&::oneflow::cfg::OpAttribute::tmp_bns);
    registry.def("add_tmp_bns", &::oneflow::cfg::OpAttribute::add_tmp_bns);
    registry.def("set_tmp_bns", &::oneflow::cfg::OpAttribute::set_tmp_bns);

    registry.def("has_op_conf", &::oneflow::cfg::OpAttribute::has_op_conf);
    registry.def("clear_op_conf", &::oneflow::cfg::OpAttribute::clear_op_conf);
    registry.def("op_conf", &::oneflow::cfg::OpAttribute::shared_const_op_conf);
    registry.def("mutable_op_conf", &::oneflow::cfg::OpAttribute::shared_mutable_op_conf);

    registry.def("has_arg_signature", &::oneflow::cfg::OpAttribute::has_arg_signature);
    registry.def("clear_arg_signature", &::oneflow::cfg::OpAttribute::clear_arg_signature);
    registry.def("arg_signature", &::oneflow::cfg::OpAttribute::shared_const_arg_signature);
    registry.def("mutable_arg_signature", &::oneflow::cfg::OpAttribute::shared_mutable_arg_signature);

    registry.def("has_arg_modifier_signature", &::oneflow::cfg::OpAttribute::has_arg_modifier_signature);
    registry.def("clear_arg_modifier_signature", &::oneflow::cfg::OpAttribute::clear_arg_modifier_signature);
    registry.def("arg_modifier_signature", &::oneflow::cfg::OpAttribute::shared_const_arg_modifier_signature);
    registry.def("mutable_arg_modifier_signature", &::oneflow::cfg::OpAttribute::shared_mutable_arg_modifier_signature);

    registry.def("has_blob_last_used_signature", &::oneflow::cfg::OpAttribute::has_blob_last_used_signature);
    registry.def("clear_blob_last_used_signature", &::oneflow::cfg::OpAttribute::clear_blob_last_used_signature);
    registry.def("blob_last_used_signature", &::oneflow::cfg::OpAttribute::shared_const_blob_last_used_signature);
    registry.def("mutable_blob_last_used_signature", &::oneflow::cfg::OpAttribute::shared_mutable_blob_last_used_signature);

    registry.def("has_blob_backward_used_signature", &::oneflow::cfg::OpAttribute::has_blob_backward_used_signature);
    registry.def("clear_blob_backward_used_signature", &::oneflow::cfg::OpAttribute::clear_blob_backward_used_signature);
    registry.def("blob_backward_used_signature", &::oneflow::cfg::OpAttribute::shared_const_blob_backward_used_signature);
    registry.def("mutable_blob_backward_used_signature", &::oneflow::cfg::OpAttribute::shared_mutable_blob_backward_used_signature);

    registry.def("has_sbp_signature", &::oneflow::cfg::OpAttribute::has_sbp_signature);
    registry.def("clear_sbp_signature", &::oneflow::cfg::OpAttribute::clear_sbp_signature);
    registry.def("sbp_signature", &::oneflow::cfg::OpAttribute::shared_const_sbp_signature);
    registry.def("mutable_sbp_signature", &::oneflow::cfg::OpAttribute::shared_mutable_sbp_signature);

    registry.def("has_mirrored_signature", &::oneflow::cfg::OpAttribute::has_mirrored_signature);
    registry.def("clear_mirrored_signature", &::oneflow::cfg::OpAttribute::clear_mirrored_signature);
    registry.def("mirrored_signature", &::oneflow::cfg::OpAttribute::shared_const_mirrored_signature);
    registry.def("mutable_mirrored_signature", &::oneflow::cfg::OpAttribute::shared_mutable_mirrored_signature);

    registry.def("has_logical_blob_desc_signature", &::oneflow::cfg::OpAttribute::has_logical_blob_desc_signature);
    registry.def("clear_logical_blob_desc_signature", &::oneflow::cfg::OpAttribute::clear_logical_blob_desc_signature);
    registry.def("logical_blob_desc_signature", &::oneflow::cfg::OpAttribute::shared_const_logical_blob_desc_signature);
    registry.def("mutable_logical_blob_desc_signature", &::oneflow::cfg::OpAttribute::shared_mutable_logical_blob_desc_signature);

    registry.def("has_parallel_signature", &::oneflow::cfg::OpAttribute::has_parallel_signature);
    registry.def("clear_parallel_signature", &::oneflow::cfg::OpAttribute::clear_parallel_signature);
    registry.def("parallel_signature", &::oneflow::cfg::OpAttribute::shared_const_parallel_signature);
    registry.def("mutable_parallel_signature", &::oneflow::cfg::OpAttribute::shared_mutable_parallel_signature);

    registry.def("has_parallel_conf_signature", &::oneflow::cfg::OpAttribute::has_parallel_conf_signature);
    registry.def("clear_parallel_conf_signature", &::oneflow::cfg::OpAttribute::clear_parallel_conf_signature);
    registry.def("parallel_conf_signature", &::oneflow::cfg::OpAttribute::shared_const_parallel_conf_signature);
    registry.def("mutable_parallel_conf_signature", &::oneflow::cfg::OpAttribute::shared_mutable_parallel_conf_signature);

    registry.def("has_parallel_distribution_signature", &::oneflow::cfg::OpAttribute::has_parallel_distribution_signature);
    registry.def("clear_parallel_distribution_signature", &::oneflow::cfg::OpAttribute::clear_parallel_distribution_signature);
    registry.def("parallel_distribution_signature", &::oneflow::cfg::OpAttribute::shared_const_parallel_distribution_signature);
    registry.def("mutable_parallel_distribution_signature", &::oneflow::cfg::OpAttribute::shared_mutable_parallel_distribution_signature);
  }
  {
    pybind11::class_<ConstOpAttributeList, ::oneflow::cfg::Message, std::shared_ptr<ConstOpAttributeList>> registry(m, "ConstOpAttributeList");
    registry.def("__id__", &::oneflow::cfg::OpAttributeList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &ConstOpAttributeList::DebugString);
    registry.def("__repr__", &ConstOpAttributeList::DebugString);

    registry.def("op_attribute_size", &ConstOpAttributeList::op_attribute_size);
    registry.def("op_attribute", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> (ConstOpAttributeList::*)() const)&ConstOpAttributeList::shared_const_op_attribute);
    registry.def("op_attribute", (::std::shared_ptr<ConstOpAttribute> (ConstOpAttributeList::*)(::std::size_t) const)&ConstOpAttributeList::shared_const_op_attribute);
  }
  {
    pybind11::class_<::oneflow::cfg::OpAttributeList, ::oneflow::cfg::Message, std::shared_ptr<::oneflow::cfg::OpAttributeList>> registry(m, "OpAttributeList");
    registry.def(pybind11::init<>());
    registry.def("Clear", &::oneflow::cfg::OpAttributeList::Clear);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpAttributeList::*)(const ConstOpAttributeList&))&::oneflow::cfg::OpAttributeList::CopyFrom);
    registry.def("CopyFrom", (void (::oneflow::cfg::OpAttributeList::*)(const ::oneflow::cfg::OpAttributeList&))&::oneflow::cfg::OpAttributeList::CopyFrom);
    registry.def("__id__", &::oneflow::cfg::OpAttributeList::__Id__);
    registry.def(pybind11::self == pybind11:: self);
    registry.def(pybind11::self < pybind11:: self);
    registry.def(pybind11::hash(pybind11::self));
    registry.def("__str__", &::oneflow::cfg::OpAttributeList::DebugString);
    registry.def("__repr__", &::oneflow::cfg::OpAttributeList::DebugString);



    registry.def("op_attribute_size", &::oneflow::cfg::OpAttributeList::op_attribute_size);
    registry.def("clear_op_attribute", &::oneflow::cfg::OpAttributeList::clear_op_attribute);
    registry.def("mutable_op_attribute", (::std::shared_ptr<_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> (::oneflow::cfg::OpAttributeList::*)())&::oneflow::cfg::OpAttributeList::shared_mutable_op_attribute);
    registry.def("op_attribute", (::std::shared_ptr<Const_CFG_ONEFLOW_CORE_OPERATOR_OP_ATTRIBUTE_CFG_H__RepeatedField_OpAttribute_> (::oneflow::cfg::OpAttributeList::*)() const)&::oneflow::cfg::OpAttributeList::shared_const_op_attribute);
    registry.def("op_attribute", (::std::shared_ptr<ConstOpAttribute> (::oneflow::cfg::OpAttributeList::*)(::std::size_t) const)&::oneflow::cfg::OpAttributeList::shared_const_op_attribute);
    registry.def("mutable_op_attribute", (::std::shared_ptr<::oneflow::cfg::OpAttribute> (::oneflow::cfg::OpAttributeList::*)(::std::size_t))&::oneflow::cfg::OpAttributeList::shared_mutable_op_attribute);
  }
}