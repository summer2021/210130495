
set(CMAKE_C_COMPILER_LAUNCHER "" CACHE STRING "Initial cache" FORCE)
set(CMAKE_CXX_COMPILER_LAUNCHER "" CACHE STRING "Initial cache" FORCE)
set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Initial cache" FORCE)
set(CMAKE_VERBOSE_MAKEFILE "OFF" CACHE BOOL "Initial cache" FORCE)
set(CMAKE_INSTALL_PREFIX "/home/wangqing/myTest/oneflow/build/third_party_install/eigen" CACHE STRING "Initial cache" FORCE)
set(CMAKE_CXX_FLAGS_DEBUG "-g -std=c++11 -Wall -Wno-sign-compare -Wno-unused-function -fPIC -Werror=return-type" CACHE STRING "Initial cache" FORCE)
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -std=c++11 -Wall -Wno-sign-compare -Wno-unused-function -fPIC -Werror=return-type" CACHE STRING "Initial cache" FORCE)
set(BUILD_TESTING "OFF" CACHE BOOL "Initial cache" FORCE)