# Install script for directory: /home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/wangqing/myTest/oneflow/build/third_party_install/eigen")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Cholesky"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/CholmodSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Core"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Dense"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Eigen"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Eigenvalues"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Geometry"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Householder"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/IterativeLinearSolvers"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Jacobi"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/KLUSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/LU"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/MetisSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/OrderingMethods"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/PaStiXSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/PardisoSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/QR"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/QtAlignedMalloc"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SPQRSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SVD"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/Sparse"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SparseCholesky"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SparseCore"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SparseLU"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SparseQR"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/StdDeque"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/StdList"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/StdVector"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/SuperLUSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/UmfPackSupport"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

