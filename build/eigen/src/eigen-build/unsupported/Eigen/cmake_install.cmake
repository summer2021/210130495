# Install script for directory: /home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/wangqing/myTest/oneflow/build/third_party_install/eigen")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE FILE FILES
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/AdolcForward"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/AlignedVector3"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/ArpackSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/AutoDiff"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/BVH"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/EulerAngles"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/FFT"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/IterativeSolvers"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/KroneckerProduct"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/LevenbergMarquardt"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/MatrixFunctions"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/MoreVectorization"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/MPRealSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/NonLinearOptimization"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/NumericalDiff"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/OpenGLSupport"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/Polynomials"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/Skyline"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/SparseExtra"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/SpecialFunctions"
    "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/Splines"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE DIRECTORY FILES "/home/wangqing/myTest/oneflow/build/eigen/src/eigen/unsupported/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/wangqing/myTest/oneflow/build/eigen/src/eigen-build/unsupported/Eigen/CXX11/cmake_install.cmake")

endif()

