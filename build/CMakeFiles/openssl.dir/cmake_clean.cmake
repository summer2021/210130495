file(REMOVE_RECURSE
  "CMakeFiles/openssl"
  "CMakeFiles/openssl-complete"
  "openssl/src/openssl-stamp/openssl-install"
  "openssl/src/openssl-stamp/openssl-mkdir"
  "openssl/src/openssl-stamp/openssl-download"
  "openssl/src/openssl-stamp/openssl-update"
  "openssl/src/openssl-stamp/openssl-patch"
  "openssl/src/openssl-stamp/openssl-configure"
  "openssl/src/openssl-stamp/openssl-build"
  "third_party_install/openssl/lib/libssl.a"
  "third_party_install/openssl/lib/libcrypto.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/openssl.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
