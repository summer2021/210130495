file(REMOVE_RECURSE
  "CMakeFiles/cares"
  "CMakeFiles/cares-complete"
  "cares/src/cares-stamp/cares-install"
  "cares/src/cares-stamp/cares-mkdir"
  "cares/src/cares-stamp/cares-download"
  "cares/src/cares-stamp/cares-update"
  "cares/src/cares-stamp/cares-patch"
  "cares/src/cares-stamp/cares-configure"
  "cares/src/cares-stamp/cares-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/cares.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
