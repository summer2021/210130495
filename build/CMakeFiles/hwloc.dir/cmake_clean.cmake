file(REMOVE_RECURSE
  "CMakeFiles/hwloc"
  "CMakeFiles/hwloc-complete"
  "hwloc/src/hwloc-stamp/hwloc-install"
  "hwloc/src/hwloc-stamp/hwloc-mkdir"
  "hwloc/src/hwloc-stamp/hwloc-download"
  "hwloc/src/hwloc-stamp/hwloc-update"
  "hwloc/src/hwloc-stamp/hwloc-patch"
  "hwloc/src/hwloc-stamp/hwloc-configure"
  "hwloc/src/hwloc-stamp/hwloc-build"
  "third_party_install/hwloc/lib/libhwloc.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/hwloc.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
