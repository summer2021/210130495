file(REMOVE_RECURSE
  "CMakeFiles/grpc"
  "CMakeFiles/grpc-complete"
  "grpc/src/grpc-stamp/grpc-install"
  "grpc/src/grpc-stamp/grpc-mkdir"
  "grpc/src/grpc-stamp/grpc-download"
  "grpc/src/grpc-stamp/grpc-update"
  "grpc/src/grpc-stamp/grpc-patch"
  "grpc/src/grpc-stamp/grpc-configure"
  "grpc/src/grpc-stamp/grpc-build"
  "third_party_install/grpc/lib/libgrpc++_unsecure.a"
  "third_party_install/grpc/lib/libgrpc_unsecure.a"
  "third_party_install/grpc/lib/libgpr.a"
  "third_party_install/grpc/lib/libupb.a"
  "third_party_install/grpc/lib/libaddress_sorting.a"
  "third_party_install/grpc/lib/libcares.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/grpc.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
