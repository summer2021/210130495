file(REMOVE_RECURSE
  "CMakeFiles/flatbuffers"
  "CMakeFiles/flatbuffers-complete"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-install"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-mkdir"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-download"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-update"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-patch"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-configure"
  "flatbuffers/src/flatbuffers-stamp/flatbuffers-build"
  "third_party_install/flatbuffers/lib/libflatbuffers.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/flatbuffers.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
