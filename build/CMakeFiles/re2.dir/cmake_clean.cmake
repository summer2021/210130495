file(REMOVE_RECURSE
  "CMakeFiles/re2"
  "CMakeFiles/re2-complete"
  "re2/src/re2-stamp/re2-install"
  "re2/src/re2-stamp/re2-mkdir"
  "re2/src/re2-stamp/re2-download"
  "re2/src/re2-stamp/re2-update"
  "re2/src/re2-stamp/re2-patch"
  "re2/src/re2-stamp/re2-configure"
  "re2/src/re2-stamp/re2-build"
  "third_party_install/re2/lib/libre2.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/re2.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
