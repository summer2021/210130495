file(REMOVE_RECURSE
  "CMakeFiles/opencv"
  "CMakeFiles/opencv-complete"
  "opencv/src/opencv-stamp/opencv-install"
  "opencv/src/opencv-stamp/opencv-mkdir"
  "opencv/src/opencv-stamp/opencv-download"
  "opencv/src/opencv-stamp/opencv-update"
  "opencv/src/opencv-stamp/opencv-patch"
  "opencv/src/opencv-stamp/opencv-configure"
  "opencv/src/opencv-stamp/opencv-build"
  "third_party_install/opencv/lib/libopencv_imgproc.a"
  "third_party_install/opencv/lib/libopencv_imgcodecs.a"
  "third_party_install/opencv/lib/libopencv_core.a"
  "third_party_install/opencv/lib/libIlmImf.a"
  "third_party_install/opencv/lib/liblibjasper.a"
  "third_party_install/opencv/lib/liblibpng.a"
  "third_party_install/opencv/lib/liblibtiff.a"
  "third_party_install/opencv/lib/liblibwebp.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/opencv.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
