file(REMOVE_RECURSE
  "CMakeFiles/cub"
  "CMakeFiles/cub-complete"
  "cub/src/cub-stamp/cub-install"
  "cub/src/cub-stamp/cub-mkdir"
  "cub/src/cub-stamp/cub-download"
  "cub/src/cub-stamp/cub-update"
  "cub/src/cub-stamp/cub-patch"
  "cub/src/cub-stamp/cub-configure"
  "cub/src/cub-stamp/cub-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/cub.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
