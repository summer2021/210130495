# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.14

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/wangqing/myTest/oneflow

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/wangqing/myTest/oneflow/build

# Utility rule file for cub.

# Include the progress variables for this target.
include CMakeFiles/cub.dir/progress.make

CMakeFiles/cub: CMakeFiles/cub-complete


CMakeFiles/cub-complete: cub/src/cub-stamp/cub-install
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-mkdir
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-download
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-update
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-patch
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-configure
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-build
CMakeFiles/cub-complete: cub/src/cub-stamp/cub-install
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Completed 'cub'"
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/CMakeFiles
	/usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/CMakeFiles/cub-complete
	/usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-done

cub/src/cub-stamp/cub-install: cub/src/cub-stamp/cub-build
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "No install step for 'cub'"
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E echo_append
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-install

cub/src/cub-stamp/cub-mkdir:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Creating directories for 'cub'"
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/src/cub
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/src/cub-build
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/tmp
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/src
	/usr/local/bin/cmake -E make_directory /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp
	/usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-mkdir

cub/src/cub-stamp/cub-download: cub/src/cub-stamp/cub-urlinfo.txt
cub/src/cub-stamp/cub-download: cub/src/cub-stamp/cub-mkdir
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Performing download step (download, verify and extract) for 'cub'"
	cd /home/wangqing/myTest/oneflow/build/cub/src && /usr/local/bin/cmake -P /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/download-cub.cmake
	cd /home/wangqing/myTest/oneflow/build/cub/src && /usr/local/bin/cmake -P /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/verify-cub.cmake
	cd /home/wangqing/myTest/oneflow/build/cub/src && /usr/local/bin/cmake -P /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/extract-cub.cmake
	cd /home/wangqing/myTest/oneflow/build/cub/src && /usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-download

cub/src/cub-stamp/cub-update: cub/src/cub-stamp/cub-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "No update step for 'cub'"
	/usr/local/bin/cmake -E echo_append
	/usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-update

cub/src/cub-stamp/cub-patch: cub/src/cub-stamp/cub-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "No patch step for 'cub'"
	/usr/local/bin/cmake -E echo_append
	/usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-patch

cub/src/cub-stamp/cub-configure: cub/tmp/cub-cfgcmd.txt
cub/src/cub-stamp/cub-configure: cub/src/cub-stamp/cub-update
cub/src/cub-stamp/cub-configure: cub/src/cub-stamp/cub-patch
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "No configure step for 'cub'"
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E echo_append
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-configure

cub/src/cub-stamp/cub-build: cub/src/cub-stamp/cub-configure
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/wangqing/myTest/oneflow/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "No build step for 'cub'"
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E echo_append
	cd /home/wangqing/myTest/oneflow/build/cub/src/cub-build && /usr/local/bin/cmake -E touch /home/wangqing/myTest/oneflow/build/cub/src/cub-stamp/cub-build

cub: CMakeFiles/cub
cub: CMakeFiles/cub-complete
cub: cub/src/cub-stamp/cub-install
cub: cub/src/cub-stamp/cub-mkdir
cub: cub/src/cub-stamp/cub-download
cub: cub/src/cub-stamp/cub-update
cub: cub/src/cub-stamp/cub-patch
cub: cub/src/cub-stamp/cub-configure
cub: cub/src/cub-stamp/cub-build
cub: CMakeFiles/cub.dir/build.make

.PHONY : cub

# Rule to build all files generated by this target.
CMakeFiles/cub.dir/build: cub

.PHONY : CMakeFiles/cub.dir/build

CMakeFiles/cub.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/cub.dir/cmake_clean.cmake
.PHONY : CMakeFiles/cub.dir/clean

CMakeFiles/cub.dir/depend:
	cd /home/wangqing/myTest/oneflow/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/wangqing/myTest/oneflow /home/wangqing/myTest/oneflow /home/wangqing/myTest/oneflow/build /home/wangqing/myTest/oneflow/build /home/wangqing/myTest/oneflow/build/CMakeFiles/cub.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/cub.dir/depend

