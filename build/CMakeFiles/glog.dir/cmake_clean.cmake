file(REMOVE_RECURSE
  "CMakeFiles/glog"
  "CMakeFiles/glog-complete"
  "glog/src/glog-stamp/glog-install"
  "glog/src/glog-stamp/glog-mkdir"
  "glog/src/glog-stamp/glog-download"
  "glog/src/glog-stamp/glog-update"
  "glog/src/glog-stamp/glog-patch"
  "glog/src/glog-stamp/glog-configure"
  "glog/src/glog-stamp/glog-build"
  "third_party_install/glog/install/lib/libglog.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/glog.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
