file(REMOVE_RECURSE
  "CMakeFiles/json"
  "CMakeFiles/json-complete"
  "json/src/json-stamp/json-install"
  "json/src/json-stamp/json-mkdir"
  "json/src/json-stamp/json-download"
  "json/src/json-stamp/json-update"
  "json/src/json-stamp/json-patch"
  "json/src/json-stamp/json-configure"
  "json/src/json-stamp/json-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/json.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
