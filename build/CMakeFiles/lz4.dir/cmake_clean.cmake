file(REMOVE_RECURSE
  "CMakeFiles/lz4"
  "CMakeFiles/lz4-complete"
  "lz4/src/lz4-stamp/lz4-install"
  "lz4/src/lz4-stamp/lz4-mkdir"
  "lz4/src/lz4-stamp/lz4-download"
  "lz4/src/lz4-stamp/lz4-update"
  "lz4/src/lz4-stamp/lz4-patch"
  "lz4/src/lz4-stamp/lz4-configure"
  "lz4/src/lz4-stamp/lz4-build"
  "third_party_install/lz4/lib/liblz4.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/lz4.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
