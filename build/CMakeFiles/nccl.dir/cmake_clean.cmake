file(REMOVE_RECURSE
  "CMakeFiles/nccl"
  "CMakeFiles/nccl-complete"
  "nccl/src/nccl-stamp/nccl-install"
  "nccl/src/nccl-stamp/nccl-mkdir"
  "nccl/src/nccl-stamp/nccl-download"
  "nccl/src/nccl-stamp/nccl-update"
  "nccl/src/nccl-stamp/nccl-patch"
  "nccl/src/nccl-stamp/nccl-configure"
  "nccl/src/nccl-stamp/nccl-build"
  "third_party_install/nccl/lib/libnccl_static.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/nccl.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
