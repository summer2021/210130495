# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wangqing/myTest/oneflow/oneflow/extension/python/numpy.cpp" "/home/wangqing/myTest/oneflow/build/CMakeFiles/of_pyext_obj.dir/oneflow/extension/python/numpy.cpp.o"
  "/home/wangqing/myTest/oneflow/oneflow/extension/python/py_compute.cpp" "/home/wangqing/myTest/oneflow/build/CMakeFiles/of_pyext_obj.dir/oneflow/extension/python/py_compute.cpp.o"
  "/home/wangqing/myTest/oneflow/oneflow/extension/python/py_kernel_caller.cpp" "/home/wangqing/myTest/oneflow/build/CMakeFiles/of_pyext_obj.dir/oneflow/extension/python/py_kernel_caller.cpp.o"
  "/home/wangqing/myTest/oneflow/oneflow/extension/python/py_kernel_registry.cpp" "/home/wangqing/myTest/oneflow/build/CMakeFiles/of_pyext_obj.dir/oneflow/extension/python/py_kernel_registry.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EIGEN_NO_AUTOMATIC_RESIZING"
  "EIGEN_NO_MALLOC"
  "EIGEN_USE_GPU"
  "HALF_ENABLE_CPP11_USER_LITERALS=0"
  "OF_SOFTMAX_USE_FAST_MATH"
  "ONEFLOW_CMAKE_BUILD_TYPE=Release"
  "RPC_BACKEND_GRPC"
  "RPC_BACKEND_LOCAL"
  "WITH_COCOAPI"
  "WITH_CUDA"
  "WITH_HWLOC"
  "_GLIBCXX_USE_CXX11_ABI=0"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "third_party_install/zlib/include"
  "third_party_install/gflags/install/include"
  "third_party_install/glog/install/include"
  "third_party_install/googletest/include"
  "third_party_install/googlemock/include"
  "third_party_install/protobuf/include"
  "third_party_install/grpc/include"
  "third_party_install/libjpeg-turbo/include"
  "third_party_install/opencv/include"
  "third_party_install/libpng/include"
  "third_party_install/eigen/include/eigen3"
  "third_party_install/cocoapi/include"
  "third_party_install/half/include"
  "third_party_install/json/include"
  "third_party_install/absl/include"
  "third_party_install/openssl/include"
  "third_party_install/flatbuffers/include"
  "third_party_install/lz4/include"
  "third_party_install/re2/include"
  "/usr/local/cuda/include"
  "third_party_install/cub/include"
  "third_party_install/nccl/include"
  "third_party_install/hwloc/include"
  "_deps/pybind11-src/include"
  "../tools/cfg/include"
  "../"
  "."
  "/home/wangqing/miniconda3/include/python3.9"
  "/home/wangqing/miniconda3/lib/python3.9/site-packages/numpy/core/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangqing/myTest/oneflow/build/CMakeFiles/of_ccobj.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
