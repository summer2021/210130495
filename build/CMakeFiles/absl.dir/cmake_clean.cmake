file(REMOVE_RECURSE
  "CMakeFiles/absl"
  "CMakeFiles/absl-complete"
  "absl/src/absl-stamp/absl-install"
  "absl/src/absl-stamp/absl-mkdir"
  "absl/src/absl-stamp/absl-download"
  "absl/src/absl-stamp/absl-update"
  "absl/src/absl-stamp/absl-patch"
  "absl/src/absl-stamp/absl-configure"
  "absl/src/absl-stamp/absl-build"
  "third_party_install/absl/lib64/libabsl_base.a"
  "third_party_install/absl/lib64/libabsl_spinlock_wait.a"
  "third_party_install/absl/lib64/libabsl_dynamic_annotations.a"
  "third_party_install/absl/lib64/libabsl_malloc_internal.a"
  "third_party_install/absl/lib64/libabsl_throw_delegate.a"
  "third_party_install/absl/lib64/libabsl_int128.a"
  "third_party_install/absl/lib64/libabsl_strings.a"
  "third_party_install/absl/lib64/libabsl_str_format_internal.a"
  "third_party_install/absl/lib64/libabsl_time.a"
  "third_party_install/absl/lib64/libabsl_bad_optional_access.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/absl.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
