file(REMOVE_RECURSE
  "CMakeFiles/googletest"
  "CMakeFiles/googletest-complete"
  "googletest/src/googletest-stamp/googletest-install"
  "googletest/src/googletest-stamp/googletest-mkdir"
  "googletest/src/googletest-stamp/googletest-download"
  "googletest/src/googletest-stamp/googletest-update"
  "googletest/src/googletest-stamp/googletest-patch"
  "googletest/src/googletest-stamp/googletest-configure"
  "googletest/src/googletest-stamp/googletest-build"
  "third_party_install/googletest/lib/libgtest.a"
  "third_party_install/googletest/lib/libgtest_main.a"
  "third_party_install/googlemock/lib/libgmock.a"
  "third_party_install/googlemock/lib/libgmock_main.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/googletest.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
