file(REMOVE_RECURSE
  "CMakeFiles/protobuf"
  "CMakeFiles/protobuf-complete"
  "protobuf/src/protobuf-stamp/protobuf-install"
  "protobuf/src/protobuf-stamp/protobuf-mkdir"
  "protobuf/src/protobuf-stamp/protobuf-download"
  "protobuf/src/protobuf-stamp/protobuf-update"
  "protobuf/src/protobuf-stamp/protobuf-patch"
  "protobuf/src/protobuf-stamp/protobuf-configure"
  "protobuf/src/protobuf-stamp/protobuf-build"
  "third_party_install/protobuf/lib/libprotobuf.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/protobuf.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
