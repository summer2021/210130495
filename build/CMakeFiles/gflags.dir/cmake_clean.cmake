file(REMOVE_RECURSE
  "CMakeFiles/gflags"
  "CMakeFiles/gflags-complete"
  "gflags/src/gflags-stamp/gflags-install"
  "gflags/src/gflags-stamp/gflags-mkdir"
  "gflags/src/gflags-stamp/gflags-download"
  "gflags/src/gflags-stamp/gflags-update"
  "gflags/src/gflags-stamp/gflags-patch"
  "gflags/src/gflags-stamp/gflags-configure"
  "gflags/src/gflags-stamp/gflags-build"
  "third_party_install/gflags/install/lib/libgflags.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/gflags.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
