file(REMOVE_RECURSE
  "CMakeFiles/cocoapi"
  "CMakeFiles/cocoapi-complete"
  "cocoapi/src/cocoapi-stamp/cocoapi-install"
  "cocoapi/src/cocoapi-stamp/cocoapi-mkdir"
  "cocoapi/src/cocoapi-stamp/cocoapi-download"
  "cocoapi/src/cocoapi-stamp/cocoapi-update"
  "cocoapi/src/cocoapi-stamp/cocoapi-patch"
  "cocoapi/src/cocoapi-stamp/cocoapi-configure"
  "cocoapi/src/cocoapi-stamp/cocoapi-build"
  "third_party_install/cocoapi/lib/libcocoapi_static.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/cocoapi.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
