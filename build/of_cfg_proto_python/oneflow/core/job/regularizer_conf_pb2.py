# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: oneflow/core/job/regularizer_conf.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='oneflow/core/job/regularizer_conf.proto',
  package='oneflow',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\'oneflow/core/job/regularizer_conf.proto\x12\x07oneflow\"3\n\x13L1L2RegularizerConf\x12\r\n\x02l1\x18\x01 \x01(\x02:\x01\x30\x12\r\n\x02l2\x18\x02 \x01(\x02:\x01\x30\"M\n\x0fRegularizerConf\x12\x32\n\nl1_l2_conf\x18\x01 \x01(\x0b\x32\x1c.oneflow.L1L2RegularizerConfH\x00\x42\x06\n\x04type')
)




_L1L2REGULARIZERCONF = _descriptor.Descriptor(
  name='L1L2RegularizerConf',
  full_name='oneflow.L1L2RegularizerConf',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='l1', full_name='oneflow.L1L2RegularizerConf.l1', index=0,
      number=1, type=2, cpp_type=6, label=1,
      has_default_value=True, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='l2', full_name='oneflow.L1L2RegularizerConf.l2', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=True, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=52,
  serialized_end=103,
)


_REGULARIZERCONF = _descriptor.Descriptor(
  name='RegularizerConf',
  full_name='oneflow.RegularizerConf',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='l1_l2_conf', full_name='oneflow.RegularizerConf.l1_l2_conf', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='type', full_name='oneflow.RegularizerConf.type',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=105,
  serialized_end=182,
)

_REGULARIZERCONF.fields_by_name['l1_l2_conf'].message_type = _L1L2REGULARIZERCONF
_REGULARIZERCONF.oneofs_by_name['type'].fields.append(
  _REGULARIZERCONF.fields_by_name['l1_l2_conf'])
_REGULARIZERCONF.fields_by_name['l1_l2_conf'].containing_oneof = _REGULARIZERCONF.oneofs_by_name['type']
DESCRIPTOR.message_types_by_name['L1L2RegularizerConf'] = _L1L2REGULARIZERCONF
DESCRIPTOR.message_types_by_name['RegularizerConf'] = _REGULARIZERCONF
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

L1L2RegularizerConf = _reflection.GeneratedProtocolMessageType('L1L2RegularizerConf', (_message.Message,), {
  'DESCRIPTOR' : _L1L2REGULARIZERCONF,
  '__module__' : 'oneflow.core.job.regularizer_conf_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.L1L2RegularizerConf)
  })
_sym_db.RegisterMessage(L1L2RegularizerConf)

RegularizerConf = _reflection.GeneratedProtocolMessageType('RegularizerConf', (_message.Message,), {
  'DESCRIPTOR' : _REGULARIZERCONF,
  '__module__' : 'oneflow.core.job.regularizer_conf_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.RegularizerConf)
  })
_sym_db.RegisterMessage(RegularizerConf)


# @@protoc_insertion_point(module_scope)
