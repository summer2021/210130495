
set(CMAKE_C_COMPILER_LAUNCHER "" CACHE STRING "Initial cache" FORCE)
set(CMAKE_CXX_COMPILER_LAUNCHER "" CACHE STRING "Initial cache" FORCE)
set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Initial cache" FORCE)
set(CMAKE_CXX_FLAGS_DEBUG "-g -std=c++11 -Wall -Wno-sign-compare -Wno-unused-function -fPIC -Werror=return-type" CACHE STRING "Initial cache" FORCE)
set(CMAKE_POSITION_INDEPENDENT_CODE "ON" CACHE BOOL "Initial cache" FORCE)