# Install script for directory: /home/wangqing/myTest/oneflow/build/absl/src/absl/absl

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/wangqing/myTest/oneflow/build/third_party_install/absl")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/base/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/algorithm/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/container/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/debugging/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/flags/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/hash/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/memory/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/meta/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/numeric/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/strings/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/synchronization/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/time/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/types/cmake_install.cmake")
  include("/home/wangqing/myTest/oneflow/build/absl/src/absl-build/absl/utility/cmake_install.cmake")

endif()

