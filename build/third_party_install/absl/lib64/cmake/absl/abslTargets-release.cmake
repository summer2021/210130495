#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "absl::spinlock_wait" for configuration "Release"
set_property(TARGET absl::spinlock_wait APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::spinlock_wait PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_spinlock_wait.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::spinlock_wait )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::spinlock_wait "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_spinlock_wait.a" )

# Import target "absl::dynamic_annotations" for configuration "Release"
set_property(TARGET absl::dynamic_annotations APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::dynamic_annotations PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_dynamic_annotations.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::dynamic_annotations )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::dynamic_annotations "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_dynamic_annotations.a" )

# Import target "absl::malloc_internal" for configuration "Release"
set_property(TARGET absl::malloc_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::malloc_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_malloc_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::malloc_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::malloc_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_malloc_internal.a" )

# Import target "absl::base" for configuration "Release"
set_property(TARGET absl::base APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::base PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_base.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::base )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::base "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_base.a" )

# Import target "absl::throw_delegate" for configuration "Release"
set_property(TARGET absl::throw_delegate APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::throw_delegate PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_throw_delegate.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::throw_delegate )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::throw_delegate "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_throw_delegate.a" )

# Import target "absl::scoped_set_env" for configuration "Release"
set_property(TARGET absl::scoped_set_env APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::scoped_set_env PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_scoped_set_env.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::scoped_set_env )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::scoped_set_env "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_scoped_set_env.a" )

# Import target "absl::hashtablez_sampler" for configuration "Release"
set_property(TARGET absl::hashtablez_sampler APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::hashtablez_sampler PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_hashtablez_sampler.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::hashtablez_sampler )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::hashtablez_sampler "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_hashtablez_sampler.a" )

# Import target "absl::raw_hash_set" for configuration "Release"
set_property(TARGET absl::raw_hash_set APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::raw_hash_set PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_raw_hash_set.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::raw_hash_set )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::raw_hash_set "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_raw_hash_set.a" )

# Import target "absl::stacktrace" for configuration "Release"
set_property(TARGET absl::stacktrace APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::stacktrace PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_stacktrace.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::stacktrace )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::stacktrace "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_stacktrace.a" )

# Import target "absl::symbolize" for configuration "Release"
set_property(TARGET absl::symbolize APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::symbolize PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_symbolize.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::symbolize )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::symbolize "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_symbolize.a" )

# Import target "absl::examine_stack" for configuration "Release"
set_property(TARGET absl::examine_stack APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::examine_stack PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_examine_stack.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::examine_stack )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::examine_stack "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_examine_stack.a" )

# Import target "absl::failure_signal_handler" for configuration "Release"
set_property(TARGET absl::failure_signal_handler APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::failure_signal_handler PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_failure_signal_handler.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::failure_signal_handler )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::failure_signal_handler "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_failure_signal_handler.a" )

# Import target "absl::debugging_internal" for configuration "Release"
set_property(TARGET absl::debugging_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::debugging_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_debugging_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::debugging_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::debugging_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_debugging_internal.a" )

# Import target "absl::demangle_internal" for configuration "Release"
set_property(TARGET absl::demangle_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::demangle_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_demangle_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::demangle_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::demangle_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_demangle_internal.a" )

# Import target "absl::leak_check" for configuration "Release"
set_property(TARGET absl::leak_check APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::leak_check PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_leak_check.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::leak_check )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::leak_check "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_leak_check.a" )

# Import target "absl::leak_check_disable" for configuration "Release"
set_property(TARGET absl::leak_check_disable APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::leak_check_disable PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_leak_check_disable.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::leak_check_disable )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::leak_check_disable "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_leak_check_disable.a" )

# Import target "absl::flags_internal" for configuration "Release"
set_property(TARGET absl::flags_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_internal.a" )

# Import target "absl::flags_config" for configuration "Release"
set_property(TARGET absl::flags_config APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_config PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_config.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_config )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_config "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_config.a" )

# Import target "absl::flags_marshalling" for configuration "Release"
set_property(TARGET absl::flags_marshalling APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_marshalling PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_marshalling.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_marshalling )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_marshalling "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_marshalling.a" )

# Import target "absl::flags_handle" for configuration "Release"
set_property(TARGET absl::flags_handle APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_handle PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_handle.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_handle )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_handle "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_handle.a" )

# Import target "absl::flags_registry" for configuration "Release"
set_property(TARGET absl::flags_registry APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_registry PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_registry.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_registry )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_registry "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_registry.a" )

# Import target "absl::flags" for configuration "Release"
set_property(TARGET absl::flags APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags.a" )

# Import target "absl::flags_usage" for configuration "Release"
set_property(TARGET absl::flags_usage APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_usage PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_usage.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_usage )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_usage "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_usage.a" )

# Import target "absl::flags_parse" for configuration "Release"
set_property(TARGET absl::flags_parse APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::flags_parse PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_parse.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::flags_parse )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::flags_parse "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_flags_parse.a" )

# Import target "absl::hash" for configuration "Release"
set_property(TARGET absl::hash APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::hash PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_hash.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::hash )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::hash "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_hash.a" )

# Import target "absl::city" for configuration "Release"
set_property(TARGET absl::city APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::city PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_city.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::city )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::city "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_city.a" )

# Import target "absl::int128" for configuration "Release"
set_property(TARGET absl::int128 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::int128 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_int128.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::int128 )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::int128 "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_int128.a" )

# Import target "absl::strings" for configuration "Release"
set_property(TARGET absl::strings APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::strings PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_strings.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::strings )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::strings "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_strings.a" )

# Import target "absl::strings_internal" for configuration "Release"
set_property(TARGET absl::strings_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::strings_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_strings_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::strings_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::strings_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_strings_internal.a" )

# Import target "absl::str_format_internal" for configuration "Release"
set_property(TARGET absl::str_format_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::str_format_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_str_format_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::str_format_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::str_format_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_str_format_internal.a" )

# Import target "absl::graphcycles_internal" for configuration "Release"
set_property(TARGET absl::graphcycles_internal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::graphcycles_internal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_graphcycles_internal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::graphcycles_internal )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::graphcycles_internal "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_graphcycles_internal.a" )

# Import target "absl::synchronization" for configuration "Release"
set_property(TARGET absl::synchronization APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::synchronization PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_synchronization.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::synchronization )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::synchronization "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_synchronization.a" )

# Import target "absl::time" for configuration "Release"
set_property(TARGET absl::time APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::time PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_time.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::time )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::time "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_time.a" )

# Import target "absl::civil_time" for configuration "Release"
set_property(TARGET absl::civil_time APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::civil_time PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_civil_time.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::civil_time )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::civil_time "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_civil_time.a" )

# Import target "absl::time_zone" for configuration "Release"
set_property(TARGET absl::time_zone APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::time_zone PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_time_zone.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::time_zone )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::time_zone "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_time_zone.a" )

# Import target "absl::bad_any_cast_impl" for configuration "Release"
set_property(TARGET absl::bad_any_cast_impl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::bad_any_cast_impl PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_any_cast_impl.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::bad_any_cast_impl )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::bad_any_cast_impl "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_any_cast_impl.a" )

# Import target "absl::bad_optional_access" for configuration "Release"
set_property(TARGET absl::bad_optional_access APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::bad_optional_access PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_optional_access.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::bad_optional_access )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::bad_optional_access "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_optional_access.a" )

# Import target "absl::bad_variant_access" for configuration "Release"
set_property(TARGET absl::bad_variant_access APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(absl::bad_variant_access PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_variant_access.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS absl::bad_variant_access )
list(APPEND _IMPORT_CHECK_FILES_FOR_absl::bad_variant_access "/home/wangqing/myTest/oneflow/build/third_party_install/absl/lib64/libabsl_bad_variant_access.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
