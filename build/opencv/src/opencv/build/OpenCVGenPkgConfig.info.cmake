
set(BUILD_SHARED_LIBS "OFF")

set(CMAKE_BINARY_DIR "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build")

set(CMAKE_INSTALL_PREFIX "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/install")

set(OpenCV_SOURCE_DIR "/home/wangqing/myTest/oneflow/build/opencv/src/opencv")

set(OPENCV_PC_FILE_NAME "opencv.pc")

set(OPENCV_VERSION_PLAIN "3.3.1")

set(OPENCV_LIB_INSTALL_PATH "lib64")

set(OPENCV_INCLUDE_INSTALL_PATH "include")

set(OPENCV_3P_LIB_INSTALL_PATH "share/OpenCV/3rdparty/lib64")

set(_modules "opencv_imgcodecs;opencv_imgproc;opencv_core")

set(_extra "/home/wangqing/myTest/oneflow/build/third_party_install/zlib/lib/libz.so;/home/wangqing/myTest/oneflow/build/third_party_install/libjpeg-turbo/lib/libturbojpeg.a;stdc++;dl;m;pthread;rt")

set(_3rdparty "libwebp;libpng;libtiff;libjasper;IlmImf")

set(TARGET_LOCATION_opencv_imgcodecs "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/lib/libopencv_imgcodecs.a")

set(TARGET_LOCATION_opencv_imgproc "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/lib/libopencv_imgproc.a")

set(TARGET_LOCATION_opencv_core "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/lib/libopencv_core.a")

set(TARGET_LOCATION_libwebp "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/3rdparty/lib/liblibwebp.a")

set(TARGET_LOCATION_libpng "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/3rdparty/lib/liblibpng.a")

set(TARGET_LOCATION_libtiff "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/3rdparty/lib/liblibtiff.a")

set(TARGET_LOCATION_libjasper "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/3rdparty/lib/liblibjasper.a")

set(TARGET_LOCATION_IlmImf "/home/wangqing/myTest/oneflow/build/opencv/src/opencv/build/3rdparty/lib/libIlmImf.a")
