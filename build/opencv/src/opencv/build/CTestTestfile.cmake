# CMake generated Testfile for 
# Source directory: /home/wangqing/myTest/oneflow/build/opencv/src/opencv
# Build directory: /home/wangqing/myTest/oneflow/build/opencv/src/opencv/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("3rdparty/libtiff")
subdirs("3rdparty/libwebp")
subdirs("3rdparty/libjasper")
subdirs("3rdparty/libpng")
subdirs("3rdparty/openexr")
subdirs("include")
subdirs("modules")
subdirs("doc")
subdirs("data")
