# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: oneflow/core/job/parallel_signature.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='oneflow/core/job/parallel_signature.proto',
  package='oneflow',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n)oneflow/core/job/parallel_signature.proto\x12\x07oneflow\"\xe2\x01\n\x11ParallelSignature\x12\"\n\x1aop_parallel_desc_symbol_id\x18\x01 \x01(\x03\x12\x65\n bn_in_op2parallel_desc_symbol_id\x18\x02 \x03(\x0b\x32;.oneflow.ParallelSignature.BnInOp2parallelDescSymbolIdEntry\x1a\x42\n BnInOp2parallelDescSymbolIdEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x03:\x02\x38\x01')
)




_PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY = _descriptor.Descriptor(
  name='BnInOp2parallelDescSymbolIdEntry',
  full_name='oneflow.ParallelSignature.BnInOp2parallelDescSymbolIdEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='oneflow.ParallelSignature.BnInOp2parallelDescSymbolIdEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='oneflow.ParallelSignature.BnInOp2parallelDescSymbolIdEntry.value', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=_b('8\001'),
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=215,
  serialized_end=281,
)

_PARALLELSIGNATURE = _descriptor.Descriptor(
  name='ParallelSignature',
  full_name='oneflow.ParallelSignature',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='op_parallel_desc_symbol_id', full_name='oneflow.ParallelSignature.op_parallel_desc_symbol_id', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='bn_in_op2parallel_desc_symbol_id', full_name='oneflow.ParallelSignature.bn_in_op2parallel_desc_symbol_id', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY, ],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=55,
  serialized_end=281,
)

_PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY.containing_type = _PARALLELSIGNATURE
_PARALLELSIGNATURE.fields_by_name['bn_in_op2parallel_desc_symbol_id'].message_type = _PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY
DESCRIPTOR.message_types_by_name['ParallelSignature'] = _PARALLELSIGNATURE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ParallelSignature = _reflection.GeneratedProtocolMessageType('ParallelSignature', (_message.Message,), {

  'BnInOp2parallelDescSymbolIdEntry' : _reflection.GeneratedProtocolMessageType('BnInOp2parallelDescSymbolIdEntry', (_message.Message,), {
    'DESCRIPTOR' : _PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY,
    '__module__' : 'oneflow.core.job.parallel_signature_pb2'
    # @@protoc_insertion_point(class_scope:oneflow.ParallelSignature.BnInOp2parallelDescSymbolIdEntry)
    })
  ,
  'DESCRIPTOR' : _PARALLELSIGNATURE,
  '__module__' : 'oneflow.core.job.parallel_signature_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.ParallelSignature)
  })
_sym_db.RegisterMessage(ParallelSignature)
_sym_db.RegisterMessage(ParallelSignature.BnInOp2parallelDescSymbolIdEntry)


_PARALLELSIGNATURE_BNINOP2PARALLELDESCSYMBOLIDENTRY._options = None
# @@protoc_insertion_point(module_scope)
