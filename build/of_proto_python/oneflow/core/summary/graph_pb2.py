# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: oneflow/core/summary/graph.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from oneflow.core.framework import user_op_attr_pb2 as oneflow_dot_core_dot_framework_dot_user__op__attr__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='oneflow/core/summary/graph.proto',
  package='oneflow.summary',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n oneflow/core/summary/graph.proto\x12\x0foneflow.summary\x1a)oneflow/core/framework/user_op_attr.proto\"G\n\x08GraphDef\x12&\n\x04node\x18\x01 \x03(\x0b\x32\x18.oneflow.summary.NodeDef\x12\x13\n\x07version\x18\x02 \x02(\x05\x42\x02\x18\x01\"\xb5\x01\n\x07NodeDef\x12\x0c\n\x04name\x18\x01 \x02(\t\x12\n\n\x02op\x18\x02 \x02(\t\x12\r\n\x05input\x18\x03 \x03(\t\x12\x0e\n\x06\x64\x65vice\x18\x04 \x01(\t\x12\x30\n\x04\x61ttr\x18\x05 \x03(\x0b\x32\".oneflow.summary.NodeDef.AttrEntry\x1a?\n\tAttrEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12!\n\x05value\x18\x02 \x01(\x0b\x32\x12.oneflow.AttrValue:\x02\x38\x01')
  ,
  dependencies=[oneflow_dot_core_dot_framework_dot_user__op__attr__pb2.DESCRIPTOR,])




_GRAPHDEF = _descriptor.Descriptor(
  name='GraphDef',
  full_name='oneflow.summary.GraphDef',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='node', full_name='oneflow.summary.GraphDef.node', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='version', full_name='oneflow.summary.GraphDef.version', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=_b('\030\001'), file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=96,
  serialized_end=167,
)


_NODEDEF_ATTRENTRY = _descriptor.Descriptor(
  name='AttrEntry',
  full_name='oneflow.summary.NodeDef.AttrEntry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='oneflow.summary.NodeDef.AttrEntry.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='oneflow.summary.NodeDef.AttrEntry.value', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=_b('8\001'),
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=288,
  serialized_end=351,
)

_NODEDEF = _descriptor.Descriptor(
  name='NodeDef',
  full_name='oneflow.summary.NodeDef',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='oneflow.summary.NodeDef.name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='op', full_name='oneflow.summary.NodeDef.op', index=1,
      number=2, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='input', full_name='oneflow.summary.NodeDef.input', index=2,
      number=3, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='device', full_name='oneflow.summary.NodeDef.device', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='attr', full_name='oneflow.summary.NodeDef.attr', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_NODEDEF_ATTRENTRY, ],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=170,
  serialized_end=351,
)

_GRAPHDEF.fields_by_name['node'].message_type = _NODEDEF
_NODEDEF_ATTRENTRY.fields_by_name['value'].message_type = oneflow_dot_core_dot_framework_dot_user__op__attr__pb2._ATTRVALUE
_NODEDEF_ATTRENTRY.containing_type = _NODEDEF
_NODEDEF.fields_by_name['attr'].message_type = _NODEDEF_ATTRENTRY
DESCRIPTOR.message_types_by_name['GraphDef'] = _GRAPHDEF
DESCRIPTOR.message_types_by_name['NodeDef'] = _NODEDEF
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GraphDef = _reflection.GeneratedProtocolMessageType('GraphDef', (_message.Message,), {
  'DESCRIPTOR' : _GRAPHDEF,
  '__module__' : 'oneflow.core.summary.graph_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.summary.GraphDef)
  })
_sym_db.RegisterMessage(GraphDef)

NodeDef = _reflection.GeneratedProtocolMessageType('NodeDef', (_message.Message,), {

  'AttrEntry' : _reflection.GeneratedProtocolMessageType('AttrEntry', (_message.Message,), {
    'DESCRIPTOR' : _NODEDEF_ATTRENTRY,
    '__module__' : 'oneflow.core.summary.graph_pb2'
    # @@protoc_insertion_point(class_scope:oneflow.summary.NodeDef.AttrEntry)
    })
  ,
  'DESCRIPTOR' : _NODEDEF,
  '__module__' : 'oneflow.core.summary.graph_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.summary.NodeDef)
  })
_sym_db.RegisterMessage(NodeDef)
_sym_db.RegisterMessage(NodeDef.AttrEntry)


_GRAPHDEF.fields_by_name['version']._options = None
_NODEDEF_ATTRENTRY._options = None
# @@protoc_insertion_point(module_scope)
