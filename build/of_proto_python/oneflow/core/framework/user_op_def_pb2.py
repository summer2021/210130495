# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: oneflow/core/framework/user_op_def.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from oneflow.core.framework import user_op_attr_pb2 as oneflow_dot_core_dot_framework_dot_user__op__attr__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='oneflow/core/framework/user_op_def.proto',
  package='oneflow',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n(oneflow/core/framework/user_op_def.proto\x12\x07oneflow\x1a)oneflow/core/framework/user_op_attr.proto\"\xd0\x02\n\tUserOpDef\x12\x0c\n\x04name\x18\x01 \x02(\t\x12(\n\x05input\x18\x02 \x03(\x0b\x32\x19.oneflow.UserOpDef.ArgDef\x12)\n\x06output\x18\x03 \x03(\x0b\x32\x19.oneflow.UserOpDef.ArgDef\x12(\n\x04\x61ttr\x18\x04 \x03(\x0b\x32\x1a.oneflow.UserOpDef.AttrDef\x1aS\n\x06\x41rgDef\x12\x0c\n\x04name\x18\x01 \x02(\t\x12\x1a\n\x0bis_optional\x18\x02 \x01(\x08:\x05\x66\x61lse\x12\x0b\n\x03num\x18\x03 \x02(\x05\x12\x12\n\nnum_as_min\x18\x04 \x02(\x08\x1a\x61\n\x07\x41ttrDef\x12\x0c\n\x04name\x18\x01 \x02(\t\x12\x1f\n\x04type\x18\x02 \x02(\x0e\x32\x11.oneflow.AttrType\x12\'\n\x0b\x64\x65\x66\x61ult_val\x18\x03 \x01(\x0b\x32\x12.oneflow.AttrValue')
  ,
  dependencies=[oneflow_dot_core_dot_framework_dot_user__op__attr__pb2.DESCRIPTOR,])




_USEROPDEF_ARGDEF = _descriptor.Descriptor(
  name='ArgDef',
  full_name='oneflow.UserOpDef.ArgDef',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='oneflow.UserOpDef.ArgDef.name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='is_optional', full_name='oneflow.UserOpDef.ArgDef.is_optional', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='num', full_name='oneflow.UserOpDef.ArgDef.num', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='num_as_min', full_name='oneflow.UserOpDef.ArgDef.num_as_min', index=3,
      number=4, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=251,
  serialized_end=334,
)

_USEROPDEF_ATTRDEF = _descriptor.Descriptor(
  name='AttrDef',
  full_name='oneflow.UserOpDef.AttrDef',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='oneflow.UserOpDef.AttrDef.name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='type', full_name='oneflow.UserOpDef.AttrDef.type', index=1,
      number=2, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='default_val', full_name='oneflow.UserOpDef.AttrDef.default_val', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=336,
  serialized_end=433,
)

_USEROPDEF = _descriptor.Descriptor(
  name='UserOpDef',
  full_name='oneflow.UserOpDef',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='oneflow.UserOpDef.name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='input', full_name='oneflow.UserOpDef.input', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output', full_name='oneflow.UserOpDef.output', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='attr', full_name='oneflow.UserOpDef.attr', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_USEROPDEF_ARGDEF, _USEROPDEF_ATTRDEF, ],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=97,
  serialized_end=433,
)

_USEROPDEF_ARGDEF.containing_type = _USEROPDEF
_USEROPDEF_ATTRDEF.fields_by_name['type'].enum_type = oneflow_dot_core_dot_framework_dot_user__op__attr__pb2._ATTRTYPE
_USEROPDEF_ATTRDEF.fields_by_name['default_val'].message_type = oneflow_dot_core_dot_framework_dot_user__op__attr__pb2._ATTRVALUE
_USEROPDEF_ATTRDEF.containing_type = _USEROPDEF
_USEROPDEF.fields_by_name['input'].message_type = _USEROPDEF_ARGDEF
_USEROPDEF.fields_by_name['output'].message_type = _USEROPDEF_ARGDEF
_USEROPDEF.fields_by_name['attr'].message_type = _USEROPDEF_ATTRDEF
DESCRIPTOR.message_types_by_name['UserOpDef'] = _USEROPDEF
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

UserOpDef = _reflection.GeneratedProtocolMessageType('UserOpDef', (_message.Message,), {

  'ArgDef' : _reflection.GeneratedProtocolMessageType('ArgDef', (_message.Message,), {
    'DESCRIPTOR' : _USEROPDEF_ARGDEF,
    '__module__' : 'oneflow.core.framework.user_op_def_pb2'
    # @@protoc_insertion_point(class_scope:oneflow.UserOpDef.ArgDef)
    })
  ,

  'AttrDef' : _reflection.GeneratedProtocolMessageType('AttrDef', (_message.Message,), {
    'DESCRIPTOR' : _USEROPDEF_ATTRDEF,
    '__module__' : 'oneflow.core.framework.user_op_def_pb2'
    # @@protoc_insertion_point(class_scope:oneflow.UserOpDef.AttrDef)
    })
  ,
  'DESCRIPTOR' : _USEROPDEF,
  '__module__' : 'oneflow.core.framework.user_op_def_pb2'
  # @@protoc_insertion_point(class_scope:oneflow.UserOpDef)
  })
_sym_db.RegisterMessage(UserOpDef)
_sym_db.RegisterMessage(UserOpDef.ArgDef)
_sym_db.RegisterMessage(UserOpDef.AttrDef)


# @@protoc_insertion_point(module_scope)
